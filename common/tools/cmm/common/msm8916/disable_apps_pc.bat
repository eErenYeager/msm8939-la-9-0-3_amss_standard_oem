adb root
adb wait-for-devices
adb shell "echo N > /sys/module/lpm_levels/system/cpu0/standalone_pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu1/standalone_pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu2/standalone_pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu3/standalone_pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu0/pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu1/pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu2/pc/idle_enabled"
adb shell "echo N > /sys/module/lpm_levels/system/cpu3/pc/idle_enabled"
exit
