//============================================================================
//  Name:                                                                     
//    std_loadsim_mpss.cmm 
//
//  Description:                                                              
//    Script to load MPSS logs
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 10/02/2013 AJCheriyan    Change for newer SW installations
// 09/06/2012 AJCheriyan    Added USB RAM dump support
// 07/10/2012 AJCheriyan    Created for B-family 
//

// We support the following arguments
// ARG0 - Chipset Name. Can be used to setup sub-system according to target if needed 
// Using this argument should be avoided unless absolutely necessary
// ARG1 - LOGTYPE 
// ARG2 - BUILD
// ARG3 - LOGLOCATION
//

ENTRY &ARG0 &ARG1 &ARG2 &ARG3

LOCAL &LOGTYPE &BUILD &LOGLOCATION
MAIN:
	&LOGTYPE="&ARG1"
	&BUILD="&ARG2"
	&LOGLOCATION="&ARG3"

	// Load the memory map
	do std_memorymap

	// Setup the environment
	do std_setupenv

	// First, all the sanity checks
	GOSUB CHECKBINARIES
	
	// Binaries look good. Else, we wouldn't be here	
	GOSUB SETUPSIM
	
	// Load the binaries
	GOSUB LOADBIN

	// Load the symbols
	do std_loadsyms_ipa_cm3 &BUILD

	// Load the "state" at the time of the crash
	GOSUB RESTORESTATE

	// Off you go..
	GOTO EXIT

// Set the simulator for the processor we want
SETUPSIM:


	// Set the CPU and we are ready to roll
	SYS.CPU CORTEXM3
	SYS.UP
	

	RETURN

   
////////////////////////////////////////
//
//          CHECKBINARIES
//          Private function
//          Checks if the binaries for the system are present in the location
//          Loglocation should not be empty and assumes memory map is loaded
//          Expected input: None. Uses global variables
//          &LOGTYPE=<AUTODETECT,JTAG,USB> 
//
/////////////////////////////////////////
CHECKBINARIES:
LOCAL &file1 &file2 &file3 &file4 &file5 &file6 &file7 &file8 &file9 &LOGCLASS


    // This is the best place to determine the type of the log too
    IF ("&LOGTYPE"=="AUTODETECT")
    (
    
      &LOGCLASS="&LOGTYPE"
        
        
        IF ("&LOGCLASS"!="USB")
        (     
            // Check for USB logs
            do std_utils FILEXIST EXIT &LOGLOCATION &IPA_IRAM_log
                ENTRY &file1
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DRAM_log 
				ENTRY &file2
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_SRAM_log 
				ENTRY &file3
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_MBOX_log 
				ENTRY &file4
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_HRAM_log 
				ENTRY &file5
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DICT_log 
				ENTRY &file6
								
            // Check for both USB binaries. 
            IF (("&file1"=="TRUE")&&("&file2"=="TRUE")&&("&file3"=="TRUE")&&("&file4"=="TRUE")&&("&file5"=="TRUE")&&("&file6"=="TRUE"))
            (
                 &LOGCLASS="USB"
            )
        )

        IF ("&LOGCLASS"!="JTAG")
        (
            
            // Check for JTAG logs-  the IPA_REG* logs are present only for JTAG
            do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG1_log
				ENTRY &file1
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_IRAM_log 
				ENTRY &file2
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DRAM_log 
				ENTRY &file3
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_SRAM_log 
				ENTRY &file4
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_MBOX_log 
				ENTRY &file5
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG2_log 
				ENTRY &file6
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG3_log 
				ENTRY &file7
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_HRAM_log 
				ENTRY &file8
			do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DICT_log 
				ENTRY &file9
				
            IF (("&file1"=="TRUE")&&("&file2"=="TRUE")&&("&file3"=="TRUE")&&("&file4"=="TRUE")&&("&file5"=="TRUE")&&("&file6"=="TRUE")&&("&file7"=="TRUE")&&("&file8"=="TRUE")&&("&file9"=="TRUE"))
            (
                 &LOGCLASS="JTAG"
            )
        )



        // If we even after that, we are stuck with "AUTODETECT" we have a problem
        IF ("&LOGCLASS"=="AUTODETECT")
        (
            PRINT %ERROR "Neither USB nor JTAG logs present in folder: &LOGLOCATION"
            GOTO FATALEXIT
        )
        ELSE
        (
            // Safe to change the logtype
            &LOGTYPE="&LOGCLASS"
            PRINT "Detected &LOGTYPE logs in folder: &LOGLOCATION"
        )
    )
            
    IF ("&LOGTYPE"=="JTAG")
	(
		
		// Check for JTAG logs-  the IPA_REG* logs are present only for JTAG
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG1_log
			ENTRY &file1
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_IRAM_log 
			ENTRY &file2
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DRAM_log 
			ENTRY &file3
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_SRAM_log 
			ENTRY &file4
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_MBOX_log 
			ENTRY &file5
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG2_log 
			ENTRY &file6
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_REG3_log 
			ENTRY &file7
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_HRAM_log 
			ENTRY &file8
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DICT_log 
			ENTRY &file9
		IF (("&file1"=="TRUE")&&("&file2"=="TRUE")&&("&file3"=="TRUE")&&("&file4"=="TRUE")&&("&file5"=="TRUE")&&("&file6"=="TRUE")&&("&file7"=="TRUE")&&("&file8"=="TRUE")&&("&file9"=="TRUE"))
		(
			 &LOGCLASS="JTAG"
		)
        ELSE 
        (
            PRINT %ERROR "JTAG logs not present in folder: &LOGLOCATION"
            GOTO FATALEXIT
        )
	)

    IF ("&LOGTYPE"=="USB")
	(
		// Check for USB logs
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_IRAM_log
			ENTRY &file1
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DRAM_log 
			ENTRY &file2
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_SRAM_log 
			ENTRY &file3
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_MBOX_log 
			ENTRY &file4
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_HRAM_log 
			ENTRY &file5
		do std_utils FILEXIST EXIT &LOGLOCATION &IPA_DICT_log 
			ENTRY &file6
		// Check for both USB binaries. 
		IF (("&file1"=="TRUE")&&("&file2"=="TRUE")&&("&file3"=="TRUE")&&("&file4"=="TRUE")&&("&file5"=="TRUE")&&("&file6"=="TRUE"))
		(
			 &LOGCLASS="USB"
		)
        ELSE 
        (
            PRINT %ERROR "USB logs not present in folder: &LOGLOCATION"
            GOTO FATALEXIT
        )
	)
        
    RETURN
	
	////////////////////////////////////////
//
//          LOADBIN
//          Private function
//          Loads the saved binaries
//          Expected input: None. Uses global variables
//          &LOGTYPE=<JTAG,USB> 
//
/////////////////////////////////////////

LOADBIN:
    
    IF ("&LOGTYPE"=="JTAG")
    (
        // Load memories and registers
        do std_utils LOADBIN &LOGLOCATION &IPA_IRAM_log &IPA_IRAM_IPA_start
        do std_utils LOADBIN &LOGLOCATION &IPA_DRAM_log &IPA_DRAM_IPA_start
        do std_utils LOADBIN &LOGLOCATION &IPA_SRAM_log &IPA_SRAM_start
		do std_utils LOADBIN &LOGLOCATION &IPA_HRAM_log &IPA_HRAM_start
		do std_utils LOADBIN &LOGLOCATION &IPA_DICT_log &IPA_DICT_start
        do std_utils LOADBIN &LOGLOCATION &IPA_MBOX_log &IPA_MBOX_start
        do std_utils LOADBIN &LOGLOCATION &IPA_REG1_log &IPA_REG1_start
        do std_utils LOADBIN &LOGLOCATION &IPA_REG2_log &IPA_REG2_start
        do std_utils LOADBIN &LOGLOCATION &IPA_REG3_log &IPA_REG3_start

    )
    
    IF ("&LOGTYPE"=="USB")
    (
        // We only have IPA memories
        do std_utils LOADBIN &LOGLOCATION &IPA_IRAM_log &IPA_IRAM_IPA_start
        do std_utils LOADBIN &LOGLOCATION &IPA_DRAM_log &IPA_DRAM_IPA_start
        do std_utils LOADBIN &LOGLOCATION &IPA_SRAM_log &IPA_SRAM_start
        do std_utils LOADBIN &LOGLOCATION &IPA_MBOX_log &IPA_MBOX_start
		do std_utils LOADBIN &LOGLOCATION &IPA_HRAM_log &IPA_HRAM_start
		do std_utils LOADBIN &LOGLOCATION &IPA_DICT_log &IPA_DICT_start
    )
    
    RETURN

    
////////////////////////////////////////
//
//          RESTORESTATE
//          Private function
//          To load the error information from the saved logs
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//          Expects various files to be present
//
/////////////////////////////////////////
RESTORESTATE:
    
	 IF FILE.EXIST("&BUILD/modem_proc/datamodem/driver/ipa_uc/8952/scripts/std_load_uC_CPU_Regs.cmm")
    (
          // Now restore the context
		do std_utils EXECUTESCRIPT EXIT &BUILD/modem_proc/datamodem/driver/ipa_uc/8952/scripts/std_load_uC_CPU_Regs.cmm 
	  
    )
	   ELSE
    (
        PRINT %ERROR "Missing datamodem/driver/ipa_uc/8952/scripts/std_load_uC_CPU_Regs.cmm. Context loading will fail"
        RETURN
    )
    
	


    RETURN

FATALEXIT:
	END

EXIT:
	ENDDO


