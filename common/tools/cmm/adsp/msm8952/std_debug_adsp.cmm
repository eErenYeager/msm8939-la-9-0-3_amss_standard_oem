//============================================================================
//  Name:                                                                     
//    std_debug_adsp.cmm 
//
//  Description:                                                              
//    ADSP debug script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describinccg changes made to the module.
//  Notice that changes are listed icn reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 02/18/2014 JBILLING		Bring load symbol logic to std_debug side due to T32 issues with dialog()
// 01/06/2014 JBILLING          Logic update
// 07/03/2013 AJCheriyan        Changed debug logic
// 02/21/2013 AJCheriyan	Disable clock gating
// 12/05/2012 AJCheriyan	Added multi-pd support for Audio / Sensors Image
// 10/19/2012 AJCheriyan	Re-ordered the sleep enable / disable logic
// 10/10/2012 AJCheriyan	Added breakpoints support
// 07/19/2012 AJCheriyan    Created for B-family.

// Following arguments are supported. 
// ARG0 - Image to debug. 
// ARG1 - En/Disable Sleep. lpm_enable / lpm_disable
// ARG2 - Entry point for this debug session. will default to image entry point
//        if nothing is specified.
// ARGn - n > 2 - Any other arguments to be passed to the script
//
ENTRY &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9


LOCAL &SubSystem &Lpm_Option &CTI_enabled &Qurt_Breakpoints &PDOPTION &ExtraOption &fromfastboot
LOCAL &Trace_Mode &TraceSource &CycleAccurate_Command &CycleAccurate_Mode &TraceConfiguration &PortSize

//trace specific configurations
local &Trace_Mode &TraceSource &CycleAccurate_Command &TraceConfiguration &PortSize 
//extra configurations
local &Lpm_Option &ExtraOption &fromfastboot

Local &ADSP_SENSOR_ELF &ADSP_BUILDMSDIR_dialog

////////////////////////////////////////
//
//			MAIN
//			Main std_debug_adsp logic
//
/////////////////////////////////////////
MAIN:
	&IMAGE="&ARG0"
	&LPM_OPTION="&ARG1"
	
	GOSUB ParseArguments &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9
		entry &SubSystem &Lpm_Option &CTI_enabled &Qurt_Breakpoints &PDOPTION &ExtraOption &fromfastboot
        
    GOSUB ParseTraceArguments &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9
        entry &Trace_Mode &TraceSource &CycleAccurate_Command &CycleAccurate_Mode &TraceConfiguration &PortSize &PortSizeCommand

	// The arguments are valid at this point and the required T32 sessions are open
	// Go about business without error checking
    
	GOSUB Setup_ADSP_T32_Session
 
    // Let RPM run too
	
	if (&fromfastboot!=1)
	(
		INTERCOM.EXECUTE &CLUSTER1_APPS0_PORT GO
		WAIT.1s
	)

    // Set the magic number appropriately
    IF ("&IMAGE"=="adsp")
    (
		Y.CREATE.L HWIO_GCC_LPASS_SWAY_CBCR_CLK_ENABLE_BMSK 0x1
		Y.CREATE.L HWIO_GCC_LPASS_SWAY_CBCR_CLK_ENABLE_SHFT 0x0
		
		Y.CREATE.L HWIO_LPASS_Q6SS_AHB_LFABIF_CBCR_CLK_ENABLE_BMSK 0x1
		Y.CREATE.L HWIO_LPASS_Q6SS_AHB_LFABIF_CBCR_CLK_ENABLE_SHFT 0x0
		
		Y.CREATE.L HWIO_LPASS_QDSP6SS_XO_CBCR_CLKOFF_BMSK 0x80000000
		Y.CREATE.L HWIO_LPASS_QDSP6SS_XO_CBCR_CLKOFF_SHFT 0x1f
		Y.CREATE.L HWIO_LPASS_QDSP6SS_XO_CBCR_CLKEN_BMSK 0x1
		Y.CREATE.L HWIO_LPASS_QDSP6SS_XO_CBCR_CLKEN_SHFT 0x0
		
		Y.CREATE.L HWIO_LPASS_QDSP6SS_DBG_CFG_DBG_SW_REG_BMSK 0xff000000
		Y.CREATE.L HWIO_LPASS_QDSP6SS_DBG_CFG_DBG_SW_REG_SHFT 0x18
		y.create.done
			
		D.S E&ACCESS:&ADSP_DEBUG_COOKIE %LONG %LE &DEBUG_COOKIE_VALUE
		do std_utils HWIO_OUTF GCC_LPASS_SWAY_CBCR CLK_ENABLE 1
		do std_utils HWIO_OUTF LPASS_Q6SS_AHB_LFABIF_CBCR CLK_ENABLE 0x1
		do std_utils HWIO_OUTF LPASS_QDSP6SS_XO_CBCR CLKEN 0x1
		wait.200ms
		do std_utils HWIO_OUTF LPASS_QDSP6SS_DBG_CFG DBG_SW_REG 0x20 
    )

	IF (1==&CTI_enabled)
	(
		do std_cti adsp rpm apps0 apps1 apps2 apps3
	)
    ELSE
    (
       // do std_cti adsp rpm
    )
    
	if (&fromfastboot==1)
	(
		print "in fastboot mode. Exectuting FASTBOOT CONTINUE. Make sure device attached or script will hang here!"
		os.area fastboot continue
	)

	
    //wait at least 4 seconds to allow system to boot, then attempt to break repeatedly
    WAIT 2.s
    //d.s EZAXI:0x0:0xFC4AA004 %LE %LONG 0x80000006
	// Now we can attach and break. It should be welcoming us with a "brkpt"
	//this will poll for 10 seconds
    print "Attempting to break on ADSP. Will poll for 20 seconds"
    
    &counter=0
    
    while (STATE.RUN()&&(&counter<0x128))
    (
        wait.100ms
        &counter=&counter+1
    )
    do std_intercom_do &ADSP_PORT std_utils BREAKPROC
    INTERCOM.EVALUATE &ADSP_PORT SYSTEM.MODE()
    &ADSP_T32_attachstate=EVAL()
    IF (0==&ADSP_T32_attachstate)
    (
        print %error "Could not attach to ADSP during bootup. Try coldbooting device, restart ADSP T32 window, and try again"
        GOTO EXIT
    )
    
	IF (1!=&CTI_enabled)
    (
        //let RPM go again
       // INTERCOM.EXECUTE &CLUSTER1_APPS0_PORT GO
        //clear CTI's.
       // do std_utils ClearCTITrig ADSP
        //do std_utils ClearCTITrig RPM
    )
    

	GOSUB SetSleepCommand


	// Now go to the desired point
	// assumed that adsp is stopped and attached by this point
    //logic improvement: need to remove the 'haltatdesiredbp' and just set the breakpoint 
    //after it hits main and after you've loaded the sensor elf
    
    GOSUB SetupADSPcore
    
    GOTO EXIT
	
////////////////////////////////////////
//
//			Setup_ADSP_T32_Session
//			Setup the remote ADSP T32 window 
//
/////////////////////////////////////////
Setup_ADSP_T32_Session:

	IF !y.exist(HWIO_QDSS_CTI0_CTICONTROL_ADDR)
	(
		do hwio
	)

	INTERCOM.EXECUTE &ADSP_PORT y.reset
    
	// Load the symbols before processor starts
	IF ("&IMAGE"=="adsp")
	(
        //Finding consistent T32 stack issues when running dialog.file in remote window
        //workaround is to run commands from RPM for now instead
        
		//do std_intercom_do &ADSP_PORT std_loadsyms_adsp &ADSP_BUILDROOT
		//INTERCOM.WAIT &ADSP_PORT
		
        LOCAL &ELFFILE &ADSP_BUILDROOT1
        
		INTERCOM.EVALUATE &ADSP_PORT Y.EXIST(g_sleepAllowLowPowerModes)
		IF (1!=EVAL())
		(
			//print %error "Error ocurred during ADSP symbol load. Running from RPM"
			INTERCOM.EXECUTE &ADSP_PORT end
			
			IF (FILE.EXIST("&ADSP_BUILDROOT/adsp_proc/build/ms/target.builds"))
			(
				DIALOG.FILE "&ADSP_BUILDROOT/adsp_proc/build/ms/*_reloc.elf" 
				ENTRY &ELFFILE

			)
			ELSE
			(
				DIALOG.FILE "*_reloc.elf" 
				ENTRY &ELFFILE

			)
                
            &ADSP_BUILDMSDIR_dialog=OS.FILE.PATH(&ELFFILE)
            &ADSP_BUILDROOT1="&ADSP_BUILDMSDIR_dialog\..\..\.."
			INTERCOM.EXECUTE &ADSP_PORT d.load.elf &ELFFILE /nocode /noclear /strippart "adsp_proc" /sourcepath "ADSP_BUILDROOT1/adsp_proc"
            INTERCOM.WAIT &ADSP_PORT
            INTERCOM.EXECUTE &ADSP_PORT y.spath.srd ++ "&ADSP_BUILDROOT1/adsp_proc/core/kernel"
            INTERCOM.WAIT &ADSP_PORT
            &ADSP_SENSOR_ELF=STRING.REPLACE("&ELFFILE","_reloc.elf","_SENSOR_reloc.elf",0)        
	    ) 	
	)
	ELSE
	(
		
		PRINT %error "std_debug_adsp, no image defined, should be adsp"
		GOTO FATALEXIT
	)

    
	RETURN	

////////////////////////////////////////
//
//			SetSleepCommand
//			
//			Set sleep command
//
/////////////////////////////////////////
SetSleepCommand:

	// Check if sleep is to be enabled or not
	INTERCOM.EVALUATE &ADSP_PORT Y.EXIST(g_sleepAllowLowPowerModes)
	IF (1==EVAL())
	(
		IF ("&LPM_OPTION"=="lpm_disable")
		(
			&CMD="v g_sleepAllowLowPowerModes = 0"

		)
		IF ("&LPM_OPTION"=="lpm_enable")
		(
			&CMD="v g_sleepAllowLowPowerModes = 1"
		)
	)
    ELSE
    (
            
            PRINT %error "Error loading ADSP Symbols. Exiting"
            GOTO FATALEXIT
        )

	RETURN

////////////////////////////////////////
//
//			SetupADSPcore
//			
//			Setup ADSP core once attached 
//			and halted at ADSP coldboot.
//			This is where breakpoints etc 
//			are set up and where tracing 
//			is enabled, if desired.
//
/////////////////////////////////////////
SetupADSPcore:

        
        INTERCOM.EVALUATE &ADSP_PORT Y.EXIST("&ARG2")    
        IF (1==EVAL()) //if breakpoint is known, stop at that breakpoint
        (
            do std_intercom_do &ADSP_PORT std_utils SetBreakpoints onchip &ARG2
        )
        
		do std_intercom_do &ADSP_PORT std_utils SetBreakpoints onchip main
        
		//Set up tracing if specified
		IF (("&Trace_Mode"=="ETB"))
		(
			do std_intercom_do &ADSP_PORT std_trace_adsp adsp &Trace_Mode &CycleAccurate_Mode
		)
		IF (("&Trace_Mode"=="TPIU")||("&Trace_Mode"=="SDCARD"))
		(
           // do std_intercom_do &ADSP_PORT std_trace_adsp adsp &Trace_Mode NoGPIOs &CycleAccurate_Mode
		)
   
		IF ("&ExtraOption"=="NoWarmbootBreakpoint")
		(
			//qurt kernel guys put a warmboot breakpoint in. temporary disablement
			INTERCOM.EXECUTE &ADSP_PORT D.A P:0xDB00808 nop
            
		)
        
		//go on ADSP from coldboot breakpoint
		 INTERCOM.EXECUTE &ADSP_PORT GO
		
        //Go on RPM
		//INTERCOM.EXECUTE &CLUSTER1_APPS0_PORT GO
		
		wait.650ms
        INTERCOM.WAIT &ADSP_PORT
        
        INTERCOM.EVALUATE &ADSP_PORT SYSTEM.MODE()
        &ADSP_T32_attachstate=EVAL()
        IF (0==&ADSP_T32_attachstate)
        (
            print %error "Was not able to stop. Check if symbols match"
        )
        				
		// Set the low power mode option
		INTERCOM.EXECUTE &ADSP_PORT &CMD                
		IF ("&LPM_OPTION"=="lpm_disable")
		(
			//disable clock gating
			do std_utils HWIO_OUTF LPASS_QDSP6SS_CGC_OVERRIDE CORE_RCLK_EN 0x1
			do std_utils HWIO_OUTF LPASS_QDSP6SS_CGC_OVERRIDE CORE_CLK_EN 0x1
		)
      
        GOSUB Setup_MMU_Livetarget
        
        //wait.500ms

		IF (1==&Qurt_Breakpoints)
		(
			do std_intercom_do &ADSP_PORT std_utils SetBreakpoints Soft QURTK_handle_error QURTK_tlb_crash 
			do std_intercom_do &ADSP_PORT std_utils SetBreakpoints Soft QURTK_error err_SaveFatal3 err_fatal_handler QURTK_user_fatal_exit 
			do std_intercom_do &ADSP_PORT std_utils SetBreakpoints Soft err_fatal_lock err_fatal_jettison_core QURTK_handle_nmi
		)

		//clear debug cookie
		do std_exitfastdebug adsp
                
		//if CTI is specified, run it again so that RPM and APPS0 will resume on adsp resuming.
		IF (1==&CTI_enabled)
		(
			do std_cti adsp rpm apps0 apps1 apps2 apps3 apps1 apps2 apps3
            wait.1s
            do std_utils InsertTrigPulseOnChannel ADSP 0
		)

        //need some better logic check as to if breakpoint hit already or not
        //so that system will stop if breakpoint is before main, but continue if breakpoint is after main        
        
        INTERCOM.EVALUATE &ADSP_PORT Y.EXIST("&ARG2")    
        //IF (1!=EVAL()&&("&ARG2"!="main")&&("&ARG2"!="QURTOS_init")) //if breakpoint is known, don't let ADSP go as user wants system stopped at that location
        IF (("&ARG2"!="main")&&("&ARG2"!="QURTOS_init")) 
        (
            INTERCOM.EXECUTE &ADSP_PORT GO
        )

	INTERCOM.EVALUATE &ADSP_PORT Y.EXIST("&ARG2") 
	IF (1==EVAL())
	(
		do std_intercom_do &ADSP_PORT std_utils SetBreakpoints onchip &ARG2
	)
	
    IF ("&PDOPTION"=="MULTIPD")
    (

        LOCAL &ELFFILE &ADSP_BUILDROOT1
        //do std_intercom_do &ADSP_PORT std_loadsyms_sensors &ADSP_BUILDROOT
        //INTERCOM.WAIT &ADSP_PORT
        INTERCOM.EXECUTE &ADSP_PORT end
        
        
            IF (FILE.EXIST("&ADSP_SENSOR_ELF"))
            (
                &ELFFILE="&ADSP_SENSOR_ELF"
                &ADSP_BUILDMSDIR_dialog=OS.FILE.PATH(&ELFFILE)
                
            )
            ELSE IF (FILE.EXIST("&ADSP_BUILDMSDIR_dialog/adsp_proc/build/ms/target.builds"))
            (
            
                PRINT "Point to the ADSP .elf file."
                DIALOG.FILE "&ADSP_BUILDMSDIR_dialog/adsp_proc/build/ms/*_SENSOR_reloc.elf"
                ENTRY &ELFFILE
                //&ADSP_BUILDMSDIR_dialog=OS.FILE.PATH(&ELFFILE)
            )
            ELSE
            (

                PRINT "Point to the ADSP .elf file."
                DIALOG.FILE "*_SENSOR_reloc.elf"
                ENTRY &ELFFILE
                &ADSP_BUILDMSDIR_dialog=OS.FILE.PATH(&ELFFILE)
            )
            &ADSP_BUILDROOT1="&ADSP_BUILDMSDIR_dialog\..\..\.."
            INTERCOM.EXECUTE &ADSP_PORT D.LOAD.ELF &ELFFILE /nocode /noclear /strippart "adsp_proc" /sourcepath "&ADSP_BUILDROOT1/adsp_proc"

   )


    IF (("&Trace_Mode"=="TPIU")||("&Trace_Mode"=="TPIU_A")||("&Trace_Mode"=="TPIU_B"))
    (
        wait.1s
        
        
        &count=0
        
        while (&count!=3)
        (
            GOSUB TPIU_Dialog

            do std_trace ONLYGPIOS
            do std_intercom_do &ADSP_PORT std_trace_&SubSystem &SubSystem &Trace_Mode aftergpios &CycleAccurate_Command
            INTERCOM.WAIT &ADSP_PORT
            
            &count=&count+1
        )     
    )
	
    RETURN 	
    
    
TPIU_Dialog:
   
    DIALOG
    (
        HEADER "TPIU SETUP USER INTERACTION"
        //POS width, height, length
        POS 1. 0. 40
         TEXT "Setting up the TPIU requires &SubSystem to be attach-able and halt-able"
        POS 1. 1. 35
         TEXT "One way to do this is to play music or send a diag stress test (go/diag)"
        POS 1. 2. 31
         TEXT "Press TRY to commence setup. If it fails, press TRY again"
        POS 1. 3. 31
         TEXT "Make sure that rotary switch is set up"
         //POSX
        POS 1. 5. 60.
         TEXT "Press TRY only once system fully booted, and processor stop-able"
         
        //POS 1. 5. 5.
        //DEFBUTTON "GO" "CONTinue"
        POS 1. 7. 10.
        DEFBUTTON "TRY" "CONTINUE"
        POS 13. 7. 15.
        DEFBUTTON "EXIT" "goto dialogexit"
    )
    STOP

    DIALOG.END
    
    RETURN

dialogexit:
    dialog.end
    goto EXIT
    
    
////////////////////////////////////////
//
//			Setup_MMU_Livetarget
//			
//			Configure the MMU on remote ADSP T32 session.
//
/////////////////////////////////////////        
Setup_MMU_Livetarget: 

    INTERCOM.EXECUTE &ADSP_PORT mmu.off
    INTERCOM.EXECUTE &ADSP_PORT mmu.reset
    INTERCOM.EXECUTE &ADSP_PORT mmu.tlb.scan
    INTERCOM.WAIT &ADSP_PORT
    INTERCOM.EXECUTE &ADSP_PORT mmu.on
    

    RETURN
	
	

    
////////////////////////////////////////
//
//			ParseArguments
//			Parse incoming arguments and set global vars
//
/////////////////////////////////////////
ParseArguments:
	entry &loc_ARG0 &loc_ARG1 &loc_ARG2 &loc_ARG3 &loc_ARG4 &loc_ARG5 &loc_ARG6 &loc_ARG7 &loc_ARG8 &loc_ARG9
	
	local &Loc_ARG0 &Loc_ARG1 &Loc_ARG2 &Loc_ARG3 &Loc_ARG4 &Loc_ARG5 &Loc_ARG6 &Loc_ARG7 &Loc_ARG8 &Loc_ARG9

	local &Loc_SubSystem &Loc_Lpm_Option &Loc_CTI_enabled &Loc_Qurt_Breakpoints &Loc_PDOPTION &Loc_ExtraOption &Loc_fromfastboot
    
	GOSUB GetTargetSpecificConfigurationDefaults
        entry &Loc_SubSystem &Loc_Lpm_Option &Loc_CTI_enabled &Loc_Qurt_Breakpoints &Loc_PDOPTION &Loc_ExtraOption &Loc_fromfastboot
	
	
	&Loc_ARG0=string.upr("&loc_ARG0")
	&Loc_ARG1=string.upr("&loc_ARG1")
	&Loc_ARG2=string.upr("&loc_ARG2")
	&Loc_ARG3=string.upr("&loc_ARG3")
	&Loc_ARG4=string.upr("&loc_ARG4")
	&Loc_ARG5=string.upr("&loc_ARG5")
	&Loc_ARG6=string.upr("&loc_ARG6")
	&Loc_ARG7=string.upr("&loc_ARG7")
	&Loc_ARG8=string.upr("&loc_ARG8")
	&Loc_ARG9=string.upr("&loc_ARG9")
	
    
	//additional options
	IF (("&Loc_ARG2"=="CTI")||("&Loc_ARG3"=="CTI")||("&Loc_ARG4"=="CTI")||("&Loc_ARG5"=="CTI")||("&Loc_ARG6"=="CTI")||("&Loc_ARG7"=="CTI")||("&Loc_ARG8"=="CTI")||("&Loc_ARG9"=="CTI"))
	(
		&Loc_CTI_enabled=1
	)
	IF (("&Loc_ARG2"=="QURTERRORS")||("&Loc_ARG3"=="QURTERRORS")||("&Loc_ARG3"=="QURTBREAKPOINTS")||("&Loc_ARG3"=="QURTBP")||("&Loc_ARG4"=="QURTERRORS")||("&Loc_ARG4"=="QURTBREAKPOINTS")||("&Loc_ARG4"=="QURTBP")||("&Loc_ARG5"=="QURTERRORS")||("&Loc_ARG5"=="QURTBREAKPOINTS")||("&Loc_ARG5"=="QURTBP")||("&Loc_ARG6"=="QURTERRORS")||("&Loc_ARG6"=="QURTBREAKPOINTS")||("&Loc_ARG6"=="QURTBP")||("&Loc_ARG57"=="QURTERRORS")||("&Loc_ARG7"=="QURTBREAKPOINTS")||("&Loc_ARG7"=="QURTBP")||("&Loc_ARG8"=="QURTERRORS")||("&Loc_ARG8"=="QURTBREAKPOINTS")||("&Loc_ARG8"=="QURTBP")||("&Loc_ARG9"=="QURTERRORS")||("&Loc_ARG9"=="QURTBREAKPOINTS")||("&Loc_ARG9"=="QURTBP"))
	(
		&Loc_Qurt_Breakpoints=1
	)
	IF (("&Loc_ARG0"=="LPM_ENABLE")||("&Loc_ARG1"=="LPM_ENABLE")||("&Loc_ARG2"=="LPM_ENABLE")||("&Loc_ARG3"=="LPM_ENABLE")||("&Loc_ARG4"=="LPM_ENABLE")||("&Loc_ARG5"=="LPM_ENABLE"))
	(
		&Loc_Lpm_Option="lpm_enable"
	)
	IF (("&Loc_ARG3"=="MPD")||("&Loc_ARG3"=="MULTIPD")||("&Loc_ARG3"=="SENSORS")||("&Loc_ARG4"=="MPD")||("&Loc_ARG4"=="MULTIPD")||("&Loc_ARG4"=="SENSORS")||("&Loc_ARG5"=="MPD")||("&Loc_ARG5"=="MULTIPD")||("&Loc_ARG5"=="SENSORS"))
	(
		&Loc_PDOPTION="MultiPD"
	)
	IF (("&Loc_ARG3"=="SPD")||("&Loc_ARG3"=="SINGLEPD")||("&Loc_ARG4"=="SPD")||("&Loc_ARG4"=="SINGLEPD")||("&Loc_ARG5"=="SPD")||("&Loc_ARG5"=="SINGLEPD"))
	(
		&Loc_PDOPTION="SinglePD"
	)    
	IF (("&Loc_ARG0"=="NOWARMBOOTBREAKPOINT")||("&Loc_ARG1"=="NOWARMBOOTBREAKPOINT")||("&Loc_ARG2"=="NOWARMBOOTBREAKPOINT")||("&Loc_ARG3"=="NOWARMBOOTBREAKPOINT")||("&Loc_ARG4"=="NOWARMBOOTBREAKPOINT")||("&Loc_ARG5"=="NOWARMBOOTBREAKPOINT"))
	(
		&Loc_ExtraOption="NoWarmbootBreakpoint"
	)
	IF (("&Loc_ARG2"=="FROMFASTBOOT")||("&Loc_ARG3"=="FROMFASTBOOT")||("&Loc_ARG4"=="FROMFASTBOOT")||("&Loc_ARG5"=="FROMFASTBOOT")||("&Loc_ARG6"=="FROMFASTBOOT")||("&Loc_ARG7"=="FROMFASTBOOT")||("&Loc_ARG8"=="FROMFASTBOOT")||("&Loc_ARG9"=="FROMFASTBOOT"))
	(
		&Loc_fromfastboot=1
	)
    
    RETURN &Loc_SubSystem &Loc_Lpm_Option &Loc_CTI_enabled &Loc_Qurt_Breakpoints &Loc_PDOPTION &Loc_ExtraOption &Loc_fromfastboot
	
////////////////////////////////////////
//
//			GetTargetSpecificConfigurationDefaults
//			Set defaults depending on target
//
/////////////////////////////////////////
GetTargetSpecificConfigurationDefaults:
		
	local &target_SubSystem &target_Lpm_Option &target_CTI_enabled &target_Qurt_Breakpoints &target_PDOPTION &target_ExtraOption &target_fromfastboot

    IF (("&CHIPSET"=="8084")||("&CHIPSET"=="8974")||("&CHIPSET"=="8x26")||("&CHIPSET"=="8x10")||("&CHIPSET"=="msm8952"))
    (
        &target_SubSystem="ADSP"
        &target_Lpm_Option="lpm_disable"
        &target_CTI_enabled=0 
        &target_Qurt_Breakpoints=0
        &target_PDOPTION="MULTIPD" //MPD for most targets
        &target_ExtraOption="NONE"
        &target_fromfastboot=0
    ) 
    ELSE IF (("&CHIPSET"=="8092"))
    (
        &target_SubSystem="ADSP"
        &target_Lpm_Option="lpm_disable"
        &target_CTI_enabled=0 
        &target_Qurt_Breakpoints=0
        &target_PDOPTION="SINGLEPD" //SPD for 8092
        &target_ExtraOption="NONE"
        &target_fromfastboot=0
    )
    ELSE
    (
        print "Target not recognized. Resorting to 8974 Trace defaults"
        &target_SubSystem="ADSP"
        &target_Lpm_Option="lpm_disable"
        &target_CTI_enabled=0 
        &target_Qurt_Breakpoints=0
        &target_PDOPTION="MULTIPD" //MPD for default
        &target_ExtraOption="NONE"
        &target_fromfastboot=0

    )
   
    RETURN &target_SubSystem &target_Lpm_Option &target_CTI_enabled &target_Qurt_Breakpoints &target_PDOPTION &target_ExtraOption &target_fromfastboot
	
        

    
////////////////////////////////////////
//
//			ParseTraceArguments
//			Parse incoming arguments and set global vars
//
/////////////////////////////////////////

ParseTraceArguments:
	entry &loc_ARG0 &loc_ARG1 &loc_ARG2 &loc_ARG3 &loc_ARG4 &loc_ARG5 &loc_ARG6 &loc_ARG7 &loc_ARG8 &loc_ARG9
	
	//local &Loc_ARG0 &Loc_ARG1 &Loc_ARG2 &Loc_ARG3 &Loc_ARG4 &Loc_ARG5 &Loc_ARG6 &Loc_ARG7 &Loc_ARG8 &Loc_ARG9
	local &Loc_Trace_Mode &Loc_TraceSource &Loc_CycleAccurate_Command &Loc_CycleAccurate_Mode &Loc_TraceConfiguration &Loc_PortSize &Loc_PortSizeCommand &Loc_DefaultTraceSetting
    
	
	GOSUB GetTargetSpecificTraceConfigurationDefaults
        entry &Loc_Trace_Mode &Loc_TraceSource &Loc_CycleAccurate_Command &Loc_CycleAccurate_Mode &Loc_TraceConfiguration &Loc_PortSize &Loc_PortSizeCommand &Loc_DefaultTraceSetting




	&Loc_ARG0=string.upr("&loc_ARG0")
	&Loc_ARG1=string.upr("&loc_ARG1")
	&Loc_ARG2=string.upr("&loc_ARG2")
	&Loc_ARG3=string.upr("&loc_ARG3")
	&Loc_ARG4=string.upr("&loc_ARG4")
	&Loc_ARG5=string.upr("&loc_ARG5")
	&Loc_ARG6=string.upr("&loc_ARG6")
	&Loc_ARG7=string.upr("&loc_ARG7")
	&Loc_ARG8=string.upr("&loc_ARG8")
	&Loc_ARG9=string.upr("&loc_ARG9")
	
	//do std_utils CHECKARGS FATALEXIT &Loc_ARG1 "ADSP"
	
	//args 0,1,2 are all set (need to be adsp, lpm option, breakpoint).
	//check options after that.
	
	//Specify Usecase Configuration. Do this before TPIU setting
    //Specify Usecase Configuration. onlygpios is for std_trace, 'hotattach', 'aftergpios', 'default' are for std_trace_<subsystem>
    //hotattach and aftergpios is the same usecase. these attempt to break the processor, configure, then resume processor
    //default just sets up the configuration without stopping/resuming.
	IF (("&Loc_ARG0"=="ONLYGPIOS")||("&Loc_ARG1"=="ONLYGPIOS")||("&Loc_ARG2"=="ONLYGPIOS")||("&Loc_ARG3"=="ONLYGPIOS")||("&Loc_ARG4"=="ONLYGPIOS")||("&Loc_ARG5"=="ONLYGPIOS"))
	(
		&Loc_TraceConfiguration="ONLYGPIOS"
		//default setting. can be overwritten if user specifies TPIU settings, set below
		&Loc_Trace_Mode="&Loc_DefaultTraceSetting"

	)
	//IF (("&Loc_ARG0"=="NOGPIOS")||("&Loc_ARG1"=="NOGPIOS")||("&Loc_ARG2"=="NOGPIOS")||("&Loc_ARG3"=="NOGPIOS")||("&Loc_ARG4"=="NOGPIOS")||("&Loc_ARG5"=="NOGPIOS"))
	//(
	//	&Loc_TraceConfiguration="NOGPIOS"
	//	//default setting. GPIO arguments implies that TPIU being used
	//	//can be overwritten if user specifies TPIU settings, set below
	//	&Loc_Trace_Mode="&Loc_DefaultTraceSetting"
//
	//)
	IF (("&Loc_ARG0"=="AFTERGPIOS")||("&Loc_ARG1"=="AFTERGPIOS")||("&Loc_ARG2"=="AFTERGPIOS")||("&Loc_ARG3"=="AFTERGPIOS")||("&Loc_ARG4"=="AFTERGPIOS")||("&Loc_ARG5"=="AFTERGPIOS")||("&Loc_ARG0"=="HOTATTACH")||("&Loc_ARG1"=="HOTATTACH")||("&Loc_ARG2"=="HOTATTACH")||("&Loc_ARG3"=="HOTATTACH")||("&Loc_ARG4"=="HOTATTACH")||("&Loc_ARG5"=="HOTATTACH"))
	(
		&Loc_TraceConfiguration="HOTATTACH"
		//default setting. GPIO arguments implies that TPIU being used
		//can be overwritten if user specifies TPIU settings, set below
		&Loc_Trace_Mode="&Loc_DefaultTraceSetting"

	)
	
	
	//Specify ETM mode (ETB, TPIU_A, TPIU_B, SDcard)
	IF (("&Loc_ARG0"=="ETB")||("&Loc_ARG1"=="ETB")||("&Loc_ARG2"=="ETB")||("&Loc_ARG3"=="ETB")||("&Loc_ARG4"=="ETB")||("&Loc_ARG5"=="ETB")||("&Loc_ARG6"=="ETB")||("&Loc_ARG7"=="ETB")||("&Loc_ARG8"=="ETB")||("&Loc_ARG9"=="ETB"))
	(
		&Loc_Trace_Mode="ETB"
	)
	IF (("&Loc_ARG0"=="TPIU")||("&Loc_ARG1"=="TPIU")||("&Loc_ARG2"=="TPIU")||("&Loc_ARG3"=="TPIU")||("&Loc_ARG4"=="TPIU")||("&Loc_ARG5"=="TPIU")||("&Loc_ARG6"=="TPIU")||("&Loc_ARG7"=="TPIU")||("&Loc_ARG8"=="TPIU")||("&Loc_ARG9"=="TPIU"))
	(
		&Loc_Trace_Mode="&Loc_DefaultTraceSetting" //default to use TPIU_A
	)
    
	IF (("&Loc_ARG0"=="TPIUA")||("&Loc_ARG1"=="TPIUA")||("&Loc_ARG2"=="TPIUA")||("&Loc_ARG3"=="TPIUA")||("&Loc_ARG4"=="TPIUA")||("&Loc_ARG5"=="TPIUA")||("&Loc_ARG6"=="TPIUA")||("&Loc_ARG7"=="TPIUA")||("&Loc_ARG8"=="TPIUA")||("&Loc_ARG9"=="TPIUA")||("&Loc_ARG0"=="TPIU_A")||("&Loc_ARG1"=="TPIU_A")||("&Loc_ARG2"=="TPIU_A")||("&Loc_ARG3"=="TPIU_A")||("&Loc_ARG4"=="TPIU_A")||("&Loc_ARG5"=="TPIU_A")||("&Loc_ARG6"=="TPIU_A")||("&Loc_ARG7"=="TPIU_A")||("&Loc_ARG58"=="TPIU_A")||("&Loc_ARG9"=="TPIU_A"))
	(
		&Loc_Trace_Mode="TPIU_A" 
	)
    
	IF (("&Loc_ARG0"=="TPIUB")||("&Loc_ARG1"=="TPIUB")||("&Loc_ARG2"=="TPIUB")||("&Loc_ARG3"=="TPIUB")||("&Loc_ARG4"=="TPIUB")||("&Loc_ARG5"=="TPIUB")||("&Loc_ARG6"=="TPIUB")||("&Loc_ARG7"=="TPIUB")||("&Loc_ARG8"=="TPIUB")||("&Loc_ARG9"=="TPIUB")||("&Loc_ARG0"=="TPIU_B")||("&Loc_ARG1"=="TPIU_B")||("&Loc_ARG2"=="TPIU_B")||("&Loc_ARG3"=="TPIU_B")||("&Loc_ARG4"=="TPIU_B")||("&Loc_ARG5"=="TPIU_B")||("&Loc_ARG6"=="TPIU_B")||("&Loc_ARG7"=="TPIU_B")||("&Loc_ARG58"=="TPIU_B")||("&Loc_ARG9"=="TPIU_B"))
	(
		&Loc_Trace_Mode="TPIU_B"
	)
	
	
	IF (("&Loc_ARG0"=="SDCARD")||("&Loc_ARG1"=="SDCARD")||("&Loc_ARG2"=="SDCARD")||("&Loc_ARG3"=="SDCARD")||("&Loc_ARG4"=="SDCARD")||("&Loc_ARG5"=="SDCARD"))
	(
		&Loc_Trace_Mode="SDCARD"
		&PortSize="4"
	)

	

	//Specify Usecase Configuration
	IF (("&Loc_ARG1"=="SAVEETB")||("&Loc_ARG2"=="SAVEETB")||("&Loc_ARG3"=="SAVEETB")||("&Loc_ARG4"=="SAVEETB")||("&Loc_ARG5"=="SAVEETB"))
	(
		&Loc_TraceConfiguration="SAVEETB"
	)

	
	
	//Configure ETM Portsize
	IF (("&Loc_ARG0"=="4BIT")||("&Loc_ARG1"=="4BIT")||("&Loc_ARG2"=="4BIT")||("&Loc_ARG3"=="4BIT")||("&Loc_ARG4"=="4BIT")||("&Loc_ARG5"=="4BIT"))
	(
		&Loc_PortSize="4"
		&Loc_PortSizeCommand="4BIT"
	)
	IF (("&Loc_ARG0"=="8BIT")||("&Loc_ARG1"=="8BIT")||("&Loc_ARG2"=="8BIT")||("&Loc_ARG3"=="8BIT")||("&Loc_ARG4"=="8BIT")||("&Loc_ARG5"=="8BIT"))
	(
		&Loc_PortSize="8"
		&Loc_PortSizeCommand="8BIT"
	)
	IF (("&Loc_ARG0"=="16BIT")||("&Loc_ARG1"=="16BIT")||("&Loc_ARG2"=="16BIT")||("&Loc_ARG3"=="16BIT")||("&Loc_ARG4"=="16BIT")||("&Loc_ARG5"=="16BIT"))
	(
		&Loc_PortSize="16"
		&Loc_PortSizeCommand="4BIT"
	)
	
	
	//Specify which core to trace
	IF (("&Loc_ARG0"=="RPM")||("&Loc_ARG1"=="RPM")||("&Loc_ARG2"=="RPM")||("&Loc_ARG3"=="RPM")||("&Loc_ARG4"=="RPM")||("&Loc_ARG5"=="RPM"))
	(
		&Loc_TraceSource="RPM"
	)
	IF (("&Loc_ARG0"=="APPS")||("&Loc_ARG1"=="APPS")||("&Loc_ARG2"=="APPS")||("&Loc_ARG3"=="APPS")||("&Loc_ARG4"=="APPS")||("&Loc_ARG5"=="APPS"))
	(
		&Loc_TraceSource="APPS"
	)
	IF (("&Loc_ARG0"=="MPSS")||("&Loc_ARG1"=="MPSS")||("&Loc_ARG2"=="MPSS")||("&Loc_ARG3"=="MPSS")||("&Loc_ARG4"=="MPSS")||("&Loc_ARG5"=="MPSS"))
	(
		&Loc_TraceSource="MPSS"
	)
	IF (("&Loc_ARG0"=="ADSP")||("&Loc_ARG1"=="ADSP")||("&Loc_ARG2"=="ADSP")||("&Loc_ARG3"=="ADSP")||("&Loc_ARG4"=="ADSP")||("&Loc_ARG5"=="ADSP"))
	(
		&Loc_TraceSource="ADSP"
	)
	IF (("&Loc_ARG0"=="WCNSS")||("&Loc_ARG1"=="WCNSS")||("&Loc_ARG2"=="WCNSS")||("&Loc_ARG3"=="WCNSS")||("&Loc_ARG4"=="WCNSS")||("&Loc_ARG5"=="WCNSS")||("&Loc_ARG0"=="PRONTO")||("&Loc_ARG1"=="PRONTO")||("&Loc_ARG2"=="PRONTO")||("&Loc_ARG3"=="PRONTO")||("&Loc_ARG4"=="PRONTO")||("&Loc_ARG5"=="PRONTO"))
	(
		&Loc_TraceSource="WCNSS"
	)
	IF (("&Loc_ARG0"=="MAPLE")||("&Loc_ARG1"=="MAPLE")||("&Loc_ARG2"=="MAPLE")||("&Loc_ARG3"=="MAPLE")||("&Loc_ARG4"=="MAPLE")||("&Loc_ARG5"=="MAPLE")||("&Loc_ARG0"=="VPU")||("&Loc_ARG1"=="VPU")||("&Loc_ARG2"=="VPU")||("&Loc_ARG3"=="VPU")||("&Loc_ARG4"=="VPU")||("&Loc_ARG5"=="VPU"))
	(
		&Loc_TraceSource="MAPLE"
	)
	IF (("&Loc_ARG0"=="VENUS")||("&Loc_ARG1"=="VENUS")||("&Loc_ARG2"=="VENUS")||("&Loc_ARG3"=="VENUS")||("&Loc_ARG4"=="VENUS")||("&Loc_ARG5"=="VENUS")||("&Loc_ARG0"=="VSS")||("&Loc_ARG1"=="VSS")||("&Loc_ARG2"=="VSS")||("&Loc_ARG3"=="VSS")||("&Loc_ARG4"=="VSS")||("&Loc_ARG5"=="VSS"))
	(
		&Loc_TraceSource="VENUS"
	)
	
	//Specify CycleAccurate Mode (Takes up more data bandwidth, but very accurate timestamps)
	IF (("&Loc_ARG1"=="CYCLEACCURATE")||("&Loc_ARG2"=="CYCLEACCURATE")||("&Loc_ARG3"=="CYCLEACCURATE")||("&Loc_ARG4"=="CYCLEACCURATE")||("&Loc_ARG5"=="CYCLEACCURATE")||("&Loc_ARG56"=="CYCLEACCURATE")||("&Loc_ARG7"=="CYCLEACCURATE")||("&Loc_ARG8"=="CYCLEACCURATE")||("&Loc_ARG9"=="CYCLEACCURATE"))
	(
		&Loc_CycleAccurate_Command="CYCLEACCURATE"
        &Loc_CycleAccurate_Mode="ON"
	)


        RETURN &Loc_Trace_Mode &Loc_TraceSource &Loc_CycleAccurate_Command &Loc_CycleAccurate_Mode &Loc_TraceConfiguration &Loc_PortSize &Loc_PortSizeCommand
    
    
////////////////////////////////////////
//
//			GetTargetSpecificTraceConfigurationDefaults
//			Set defaults depending on target
//
/////////////////////////////////////////    
GetTargetSpecificTraceConfigurationDefaults:
		
	local &target_Trace_Mode &target_TraceSource &target_CycleAccurate_Command &target_CycleAccurate_Mode &target_TraceConfiguration &target_PortSize &target_PortSizeCommand &target_DefaultTraceSetting
    
    IF (("&CHIPSET"=="8084")||("&CHIPSET"=="8092")||("&CHIPSET"=="8974")||("&CHIPSET"=="8x26")||("&CHIPSET"=="8x10")||("&CHIPSET"=="msm8952"))
    (
        &target_Trace_Mode="NONE"
        &target_TraceSource="RPM"
        &target_CycleAccurate_Command="NONE"
        &target_CycleAccurate_Mode="OFF"
        &target_TraceConfiguration="DEFAULT"
        &target_PortSize="8" //8bit for 
	&target_PortSizeCommand="8BIT"
        &target_DefaultTraceSetting="TPIU_B"
    )
    ELSE
    (
        print "Target not recognized. Resorting to 8974 Trace defaults"
        &target_Trace_Mode="NONE"
        &target_TraceSource="RPM"
        &target_CycleAccurate_Command="NONE"
        &target_CycleAccurate_Mode="OFF"
        &target_TraceConfiguration="DEFAULT"
        &target_PortSize="8" //8bit for 8084
	&target_PortSizeCommand="8BIT"
        &target_DefaultTraceSetting="TPIU_B"


    )
   
    RETURN &target_Trace_Mode &target_TraceSource &target_CycleAccurate_Command &target_CycleAccurate_Mode &target_TraceConfiguration &target_PortSize &target_PortSizeCommand &target_DefaultTraceSetting
	
    
EXIT:
	ENDDO

FATALEXIT:
	END
