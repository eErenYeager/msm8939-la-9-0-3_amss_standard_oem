Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.

FuseBlower can be used to generate and validate sec.dat file.

<sectools>/
| sectools.py (main tool launcher command interface)
|
| -- config/  			     (chipset-specific config template directory)
| -- config/<chipset>/ 		 (pre-configured templates directory)
| -- config/xsd 			 (xsd for config xml)
|
| -- sectools/features/fbc/fuseblower.py  	       (main FuseBlower python script)
| -- sectools/features/fbc/ 			           (main FuseBlower core code)
|  
| -- sectools/features/fbc/datamodel/              (data model code)
| -- sectools/features/fbc/parsegen/               (data parsers/generators)
| -- sectools/features/fbc/parsegen/fm_config      (parser for Fuse Master layout file)
| -- sectools/features/fbc/parsegen/fb_config      (parser/generator for OEM/QC/USER config files)
| -- sectools/features/fbc/parsegen/secdat         (parser/generator for sec.dat binary file)
| -- sectools/features/fbc/validator/ 			   (built-in validators) 
| --
| -- sectools/common/core       (infrastructure)
| -- sectools/common/crypto     (cryptographic services)
| -- sectools/common/utils      (core utilities)
|  
| -- resources/					 (resources directory)
| -- resources/testpki			 (test pki files)

------------------------------------------------------------------------------
Example Usage of the tool 
------------------------------------------------------------------------------
Quick Help:

python sectools.py fuseblower -h
Options:
  --version             show program's version number and exit
  -h, --help            show this help message
  -v, --verbose         enable more logging.

  Generating sec.dat:
    -u <file>, --user_config_path=<file>
                        path to the user config file.
    -e <file>, --oem_config_path=<file>
                        path to the oem config file.
    -q <file>, --qc_config_path=<file>
                        path to the qc config file.

  Validating sec.dat:
    -s <file>, --secdat=<file>
                        path to the sec dat file to be validated.

  Specifying output location:
    -o <dir>, --output_dir=<dir>
                        directory to store output files. DEFAULT:
                        "./fuseblower_output"

  Operations:
    -g, --generate      generates the sec.dat file.
    -a, --validate      validate the sec.dat file.
    -l, --verify_inputs
                        verify the command line options.
