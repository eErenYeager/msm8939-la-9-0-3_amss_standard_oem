#===============================================================================
#
# Copyright (c) 2014 Qualcomm Technologies, Inc. All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#===============================================================================
import binascii

import defines as df
from sectools.common.parsegen.dbgp.header import extract_header, create_empty_header
from sectools.common.utils.c_misc import store_debug_data_to_file, properties_repr


class ParseGenDbgp(object):

    def __init__(self, data=None,
                 debug_dir=None,
                 debug_prefix=None,
                 debug_suffix=None,
                 ):
        # Public properties
        self.debug_dir = debug_dir
        self.debug_prefix = debug_prefix
        self.debug_suffix = debug_suffix

        # Handle empty dbgp creation
        if data is None:
            data = self._create_empty_dbgp()

        # Store the original image
        self.store_debug_data(df.FILE_DATA_IN, data)

        # Extract the header
        data, self.header = extract_header(data)

        # Extract the root certificate hash
        data, self.rootcerthash_array = extract_rootcerthash(data, self.header)
        self.store_debug_data(df.FILE_ROOTCERTHASH_IN, data)

    def __repr__(self):
        return ('Debug Policy File: ' + '\n' + repr(self.header) + rch_repr(self.rootcerthash_array))

    def store_debug_data(self, file_name, data, prefix=None, suffix=None):
        if prefix is None:
            prefix = self.debug_prefix
        if suffix is None:
            suffix = self.debug_suffix
        if prefix is not None and suffix is not None:
            store_debug_data_to_file(prefix + '_' + file_name + suffix,
                                     data, self.debug_dir)

    def _create_empty_dbgp(self):
        header = create_empty_header()
        return header.pack()

    def get_data(self):
        rch_data = pack_rootcerthash(self.rootcerthash_array)
        return self.header.pack() + rch_data

    @property
    def rootcerthash_array(self):
        return self._rootcerthash_array

    @rootcerthash_array.setter
    def rootcerthash_array(self, value):
        for i in range(len(value)):
            if len(value[i]) != 64:
                raise RuntimeError('rootcerthash is not equal to 256 bits, len = '+str(len(value[i])))

        self._rootcerthash_array = value
        self.header.rootcerthash_count = len(value)


def extract_rootcerthash(data, header):
    rootcerthash_array = []
    rch_data = data

    if len(data) < header.rootcerthash_count*32:
        raise RuntimeError('rootcerthash array data size is lessthan the actual data size for the available rootcerthash count ')

    for rootcerthash in range(header.rootcerthash_count):
        rch_listdata = data[:32]
        values = binascii.hexlify(rch_listdata)
        rootcerthash_array.append(values)
        data = data[32:]

    return rch_data, rootcerthash_array

def pack_rootcerthash(rootcerthash_array):
    data = ''

    for rootcerthash in range(len(rootcerthash_array)):
        data = data + binascii.unhexlify(rootcerthash_array[rootcerthash])

    return data

def _repr_properties(rch_array):
    properties = []
    for i in rch_array:
        properties = properties + [('Root Certificate Hash '+'\t ', i)]
    return [(attr, val) for attr, val in properties]

def rch_repr(rch_array):
    return properties_repr(_repr_properties(rch_array))
