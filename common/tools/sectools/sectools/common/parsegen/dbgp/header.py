#===============================================================================
#
# Copyright (c) 2014 Qualcomm Technologies, Inc. All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#===============================================================================


import defines as df
from sectools.common.utils.c_misc import properties_repr
from sectools.common.utils.struct_base import StructBase


class DbgpHdr(StructBase):

    def _unpack_data_list(self, unpacked):
        self._magic = unpacked[0]
        self._data_size = unpacked[1]
        self._version = unpacked[2]
        self._sernum_start = unpacked[3]
        self._sernum_end = unpacked[4]
        self.reserved = unpacked[5]
        self._flags = unpacked[6]
        self._imgid_count = unpacked[7]
        self._imgid_array = unpacked[8:8+32]
        self.rootcerthash_count = unpacked[40]

    def _pack_data_list(self):
        values = [self.magic,
                  self.data_size,
                  self.version,
                  self.sernum_start,
                  self.sernum_end,
                  self.reserved,
                  self.flags,
                  self.imgid_count] + list(self.imgid_array) + list([0] * (32-len(self.imgid_array)))

        values.append(self.rootcerthash_count)
        return values

    @classmethod
    def get_format(cls):
        return ('4s5IQI33I')

    def validate(self):
        # Check that the magic is correct
        if self.magic not in df.DBGPMAG_DESCRIPTION.keys():
            raise RuntimeError('Invalid magic in the Debug Policy file: ' + self.magic_number + '\n'
                               'This is not a Debug Policy file.')

        # Check that the version is correct
        if self.version not in df.DBGPVER_DESCRIPTION.keys():
            raise RuntimeError('Unsupported version in the Debug Policy file: ' + hex(self.version))

        # Check the flags are correct
        if self.flags > df.FLAGS_MAX_VALUE:
            raise RuntimeError('Unsupported flags in the Debug Policy file')

    def _repr_properties(self):
        properties = [
                      ('Magic', self.magic),
                      ('Size', self.data_size),
                      ('Revision', self.version),
                      ('Serial Number Start', self.sernum_start),
                      ('Serial Number End', self.sernum_end),
                      ('Reserved', self.reserved),
                      ('Flags', hex(self.flags)[2:].rstrip('L')),
                      ('Image ID Count', self.imgid_count)]

        if self.imgid_count!=0:
            for i in self.imgid_array:
                properties = properties + [('Image ID', hex(i)[2:].rstrip('L'))]

        properties = properties + [('Root Certificate Hash Count', self.rootcerthash_count)]
        return [(attr, val) for attr, val in properties]

    def __repr__(self):
        return properties_repr(self._repr_properties())

    def _range_check(self, value, bcnt, tag):
        max_val = int('0x' + 'FF'*bcnt, 16)
        if value < 0 or value > max_val:
            raise RuntimeError(tag + ' value: ' + value + ' is out of range: 0x0-' + hex(max_val))

    @property
    def magic(self):
        return self._magic

    @magic.setter
    def magic(self, value):
        if value not in df.DBGPMAG_DESCRIPTION.keys():
            raise RuntimeError('Invalid magic: ' + value)
        self._magic = value

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, value):
        if value not in df.DBGPVER_DESCRIPTION.keys():
            raise RuntimeError('Unsupported version: ' + hex(value))
        self._version = value

    @property
    def flags(self):
        return self._flags

    @flags.setter
    def flags(self, value):
        if value > df.FLAGS_MAX_VALUE:
            raise RuntimeError('Flags value supported range is from 0x0000000000000000 to 0x000000000000001F' + 'got value as ' + hex(value).strip('L'))
        self._flags = value

    @property
    def sernum_start(self):
        return self._sernum_start

    @property
    def sernum_end(self):
        return self._sernum_end

    def set_sernum(self, sernum_start, sernum_end):
        self._range_check(sernum_start, 4, 'sernum_start')
        self._range_check(sernum_start, 4, 'sernum_end')

        if df.SERNUM_START_END_MATCH:
            if sernum_start != sernum_end:
                raise RuntimeError('Serial Number Start is not same as Serial Number End' + '\nSerial Number Start = ' + str(sernum_start) + '\nSerial Number End = ' + str(sernum_end))
        else:
            if sernum_start > sernum_end:
                raise RuntimeError('Serial Number Start is greater than Serial Number End'+ '\nSerial Number Start = ' + str(sernum_start) + '\nSerial Number End = ' + str(sernum_end))

        self._sernum_start = sernum_start
        self._sernum_end = sernum_end

    @property
    def imgid_array(self):
        return self._imgid_array

    @imgid_array.setter
    def imgid_array(self, value):
        for i in range(len(value)):
            self._range_check(value[i], 4, 'imgid_array')

        self._imgid_count = len(value)
        self._imgid_array = value

    @property
    def imgid_count(self):
        return self._imgid_count

    @property
    def data_size(self):
        return self.get_size() + self.rootcerthash_count*32

def extract_header(data):
    header = DbgpHdr(data)
    data = data[header.size:]
    return data, header

def create_empty_header():
    header = DbgpHdr()
    header.magic = df.DBGPMAG
    header.version = df.DBGPVERONE
    return header
