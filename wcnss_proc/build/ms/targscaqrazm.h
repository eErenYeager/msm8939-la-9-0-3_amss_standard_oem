#ifndef TARGSCAQRAZM_H
#define TARGSCAQRAZM_H
/* ========================================================================
FILE: TARGSCAQRAZM

Copyright (c) 2001-2017 by QUALCOMM Incorporated.  All Rights Reserved.        
=========================================================================== */

#define T_MSM6700
#define T_MSM6800
#define T_MSM6800B
#define T_MSM7500
#define T_MDM8900
#define T_MDM8974
#define T_MSM8626
#define T_MSM8916
#define T_MSM8929
#define T_MSM8936
#define T_MSM8909
#define CONFIG_NOT_USED
#define CONFIG_NOT_USED




#endif /* TARGSCAQRAZM_H */
