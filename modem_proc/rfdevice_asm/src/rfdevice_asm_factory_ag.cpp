
/*
WARNING: This file is auto-generated.

Generated using: asm_autogen.pl
Generated from:  v2.3.127 of RFDevice_ASM.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2009, 2010, 2011, 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.

$Header: //source/qcom/qct/modem/rfdevice/asm/main/latest/etc/asm_autogen.pl#10 n

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#ifdef FEATURE_RF_HAS_TP_CARDS
#error code not present
#endif

#include "rfdevice_asm_cxa4422gc_2_ts1_data_ag.h"
#include "rfdevice_asm_cxa4422gc_0_data_ag.h"
#include "rfdevice_asm_cxm3658k_data_ag.h"
#include "rfdevice_asm_cxm3659k_data_ag.h"
#include "rfdevice_asm_cxm3653er_data_ag.h"
#include "rfdevice_asm_cxm3653er_3_data_ag.h"
#include "rfdevice_asm_cxa4414gc_data_ag.h"
#include "rfdevice_asm_cxa4414gc_es_data_ag.h"
#include "rfdevice_asm_lmspfbqh_f65_data_ag.h"
#include "rfdevice_asm_lmsp32qq_f31_data_ag.h"
#include "rfdevice_asm_rfmd1658_data_ag.h"
#include "rfdevice_asm_rfmd1498a_0x28_data_ag.h"
#include "rfdevice_asm_rfmd1660p_data_ag.h"
#include "rfdevice_asm_xmss1t3g0pa_013_data_ag.h"
#include "rfdevice_asm_xmssgd3g0pa_005_data_ag.h"
#include "rfdevice_asm_xmssgf3g0pa_009_data_ag.h"
#include "rfdevice_asm_lmsp2pqd_g22_data_ag.h"
#include "rfdevice_asm_lmsp2pqd_g89_data_ag.h"
#include "rfdevice_asm_lmsp32qp_e10_data_ag.h"
#include "rfdevice_asm_lmsp32qp_f03_data_ag.h"
#include "rfdevice_asm_cxm3641er_data_ag.h"
#include "rfdevice_asm_cxa4416gc_data_ag.h"
#include "rfdevice_asm_cxm3655k_data_ag.h"
#include "rfdevice_asm_cxm3656k_data_ag.h"
#include "rfdevice_asm_cxm3654ur_data_ag.h"
#include "rfdevice_asm_cxm3642k_data_ag.h"
#include "rfdevice_asm_cxm3637k_data_ag.h"
#include "rfdevice_asm_cxm3637ak_data_ag.h"
#include "rfdevice_asm_cxm3643k_es03_data_ag.h"
#include "rfdevice_asm_cxm3643k_es02_data_ag.h"
#include "rfdevice_asm_cxm3640k_data_ag.h"
#include "rfdevice_asm_cxm3641k_data_ag.h"
#include "rfdevice_asm_cxm3657k_data_ag.h"
#include "rfdevice_asm_cxm3657k_ts2_data_ag.h"
#include "rfdevice_asm_cxm3632er_data_ag.h"
#include "rfdevice_asm_cxm3617er_data_ag.h"
#include "rfdevice_asm_cxm3807k_data_ag.h"
#include "rfdevice_asm_dgm366_data_ag.h"
#include "rfdevice_asm_hfqswejua224_data_ag.h"
#include "rfdevice_asm_murata_hfqswkcua212_20c_data_ag.h"
#include "rfdevice_asm_murata_hfqswkcua212_23e_data_ag.h"


rfdevice_asm_data* rfdevice_asm_data_create (uint16 mfg_id, uint8 prd_id, uint8 prd_rev)
{
  rfdevice_asm_data * asm_data = NULL;

  if ( mfg_id ==  0x01B0 && prd_id == 0x00  && prd_rev == 2)
  {
    asm_data = rfdevice_asm_cxa4422gc_2_ts1_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x23  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxa4422gc_0_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x2D  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3658k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x2E  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3659k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x26  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3653er_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x00  && prd_rev == 3)
  {
    asm_data = rfdevice_asm_cxm3653er_3_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x02  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxa4414gc_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x00  && prd_rev == 1)
  {
    asm_data = rfdevice_asm_cxa4414gc_es_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x06  && prd_rev == 3)
  {
    asm_data = rfdevice_asm_lmspfbqh_f65_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x0E  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_lmsp32qq_f31_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x134 && prd_id == 0x25  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_rfmd1658_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x134 && prd_id == 0x28  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_rfmd1498a_0x28_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x134 && prd_id == 0x1A  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_rfmd1660p_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x83  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_xmss1t3g0pa_013_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x88  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_xmssgd3g0pa_005_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x81  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_xmssgf3g0pa_009_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x06  && prd_rev == 2)
  {
    asm_data = rfdevice_asm_lmsp2pqd_g22_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x020  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_lmsp2pqd_g89_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x09  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_lmsp32qp_e10_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x08  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_lmsp32qp_f03_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x13  && prd_rev == 1)
  {
    asm_data = rfdevice_asm_cxm3641er_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x01  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxa4416gc_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x21  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3655k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x22  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3656k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x20  && prd_rev == 1)
  {
    asm_data = rfdevice_asm_cxm3654ur_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x11  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3642k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x0E  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3637k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x1A  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3637ak_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x14  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3643k_es03_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x00  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3643k_es02_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x12  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3640k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x13  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3641k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x20  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3657k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x2C  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3657k_ts2_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x07  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3632er_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x06  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3617er_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x01B0 && prd_id == 0x05  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_cxm3807k_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x03E && prd_id == 0x0A  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_dgm366_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x06  && prd_rev == 1)
  {
    asm_data = rfdevice_asm_hfqswejua224_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x020C && prd_id == 0x0B  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_murata_hfqswkcua212_20c_data_ag::get_instance();
  }

  else if ( mfg_id ==  0x023E && prd_id == 0x0A  && prd_rev == 0)
  {
    asm_data = rfdevice_asm_murata_hfqswkcua212_23e_data_ag::get_instance();
  }

#ifdef FEATURE_RF_HAS_TP_CARDS
  #error code not present
#endif

  return asm_data;
}

#ifdef T_WINNT
#error code not present
#endif /* T_WINNT */

