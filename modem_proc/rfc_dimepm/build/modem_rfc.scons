#====include path optimized=====================================================
#
# GENERAL DESCRIPTION
#
# Copyright (c) 2012 - 2014 QUALCOMM Technologies Inc. All Rights Reserved
#
# Qualcomm Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfc_dimepm/build/modem_rfc.scons#2 $
# $DateTime: 2015/02/05 03:33:17 $
#
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 10/15/14   sd      Adding WTR4905V100 support
# 06/09/14   dr      Adding QFE1100 for JOLOKIA 
# 03/14/14   sd      Initial Version
#===============================================================================

Import('env')

env = env.Clone()

import os

if ('USES_RFA_D3925' not in env):
    env.Append(CFLAGS = ['-G0'])

rfc_public_api_list =[
    ('BUSES',                    'CORE'),
    ('DAL',                      'CORE'),
    ('DEBUGTOOLS',               'CORE'),
    ('HWENGINES',                'CORE'),
    ('KERNEL',                   'CORE'),
    ('MEMORY',                   'CORE'),
    ('MPROC',                    'CORE'),
    ('POWER',                    'CORE'),
    ('SERVICES',                 'CORE'),
    ('STORAGE',                  'CORE'),
    ('SYSTEMDRIVERS',            'CORE'),
    ('C2K',                      'FW'),
    ('COMMON' ,                  'FW'),
    ('FW',                       'FW'),
    ('GERAN',                    'FW'),
    ('LTE',                      'FW'),
    ('RF',                       'FW'),
    ('TDSCDMA',                  'FW'),
    ('WCDMA',                    'FW'),
    ('GERAN',                    'GERAN'),
    ('GPS',                      'GPS'),
    ('MCFG',                     'MCFG'),
    ('MCS',                      'MCS'),
    ('MMCP',                     'MMCP'),
    ('PUBLIC',                   'MMCP'),
    ('MMODE',                    'MODEM'),
    ('ONEX',                     'MODEM'),
    ('RFA',                      'MODEM'),
    ('ONEX',                     'ONEX'),
    ('PUBLIC',                   'ONEX'),
    ('CDMA',                     'RFA'),
    ('COMMON',                   'RFA'),
    ('GNSS',                     'RFA'),
    ('GSM',                      'RFA'),
    ('LM',                       'RFA'),
    ('LTE',                      'RFA'),
    ('MEAS',                     'RFA'),
    ('RF_TUNER',                 'RFA'),
    ('RFA',                      'RFA'),
    ('TDSCDMA',                  'RFA'),
    ('WCDMA',                    'RFA'),
    ('RFDEVICE_ASM',             'RFDEVICE_ASM'),
    ('RFDEVICE_INTERFACE',       'RFDEVICE_INTERFACE'),
    ('RFDEVICE_PA',              'RFDEVICE_PA'),
    ('RFDEVICE_PAPM',            'RFDEVICE_PAPM'),
    ('RFDEVICE_QCOUPLER',        'RFDEVICE_QCOUPLER'),
    ('RFDEVICE_QFE1100',         'RFDEVICE_QFE1100'),
    ('RFDEVICE_QFE1520',         'RFDEVICE_QFE1520'),
    ('RFDEVICE_QFE1550',         'RFDEVICE_QFE1550'),
    ('RFDEVICE_QFE2320',         'RFDEVICE_QFE2320'),
    ('RFDEVICE_QFE2340',         'RFDEVICE_QFE2340'),
    ('RFDEVICE_QFE2520',         'RFDEVICE_QFE2520'),
    ('RFDEVICE_QFE2550',         'RFDEVICE_QFE2550'),
    ('RFDEVICE_QTUNER',          'RFDEVICE_QTUNER'),
    ('RFDEVICE_WTR2605',         'RFDEVICE_WTR2605'),
    ('RFDEVICE_WTR4905',         'RFDEVICE_WTR4905'),
    ('RFDEVICE_WTR4905V100',     'RFDEVICE_WTR4905V100'),
    ('RFNV',                     'RFNV'),
    ('RFLM',                     'RFLM'),
    ('RFTECH_CDMA',              'RFTECH_CDMA'),
    ('RFTECH_CDMA_INCONVENIENT', 'RFTECH_CDMA'),
    ('RFTECH_GNSS',              'RFTECH_GNSS'),
    ('RFTECH_GSM',               'RFTECH_GSM'),
    ('RFTECH_LTE',               'RFTECH_LTE'),
    ('RFTECH_TDSCDMA',           'RFTECH_TDSCDMA'),
    ('RFTECH_WCDMA',             'RFTECH_WCDMA'),
    ('STUBS',                    'STUBS'),
    ] 

for api_name,api_area in rfc_public_api_list:
    env.RequirePublicApi([api_name], area=api_area)

env.PublishProtectedApi( 'RF_COMMON_INC', [
    '${RFA_ROOT}/rf/common/rf/atuner/protected',
    '${RFA_ROOT}/rf/common/ftm/inc',
    '${RFA_ROOT}/rf/common/hwtc/inc',
    '${RFA_ROOT}/rf/common/hwtc/protected',
    '${RFA_ROOT}/rf/common/rf/atuner/inc',
    '${RFA_ROOT}/rf/common/rf/atuner/inc',
    '${RFA_ROOT}/rf/common/rf/atuner/src',
    '${RFA_ROOT}/rf/common/rf/atuner/test',
    '${RFA_ROOT}/rf/common/rf/core/inc',
    '${RFA_ROOT}/rf/common/rf/core/src',
    '${RFA_ROOT}/rf/common/rf/mc/inc',
    '${RFA_ROOT}/rf/common/rf/nv/inc',
    '${RFA_ROOT}/rf/common/rf/rfc/inc',
    '${RFA_ROOT}/rf/common/rf/zlib/inc',
    '${RFA_ROOT}/rf/common/rf/zlib/src',
    '${RFA_ROOT}/rf/common/stubs/inc',
    '${RFA_ROOT}/rf/common/rf/atuner/test',
    '${RFA_ROOT}/rf/common/ftm/inc',
    '${RFA_ROOT}/rf/common/ftm/protected',
    '${RFA_ROOT}/rf/common/hwtc/inc',
    '${RFA_ROOT}/rf/common/hwtc/protected',
    '${RFA_ROOT}/rf/common/rf/atuner/inc',
    '${RFA_ROOT}/rf/common/rf/core/inc',
    '${RFA_ROOT}/rf/common/rf/core/src',
    '${RFA_ROOT}/rf/common/rf/mc/inc',
    '${RFA_ROOT}/rf/common/rf/nv/inc',
    '${RFA_ROOT}/rf/common/rf/rfc/inc',
    '${RFA_ROOT}/rf/common/rf/zlib/inc',
    '${RFA_ROOT}/rf/common/stubs/inc',
    '${RFA_ROOT}/rf/hal/common/inc',
    '${RFA_ROOT}/rf/hal/gnss/gen8a/inc',
    '${RFA_ROOT}/rf/mdsp/qdsp6_common/inc',
    '${RFA_ROOT}/rf/rfd/common/inc',
    '${RFA_ROOT}/rf/rfd/nikel/inc',
    '${RFA_ROOT}/rf/task/common/inc',
    '${RFA_ROOT}/variation/inc',
    ])

env.PublishProtectedApi( 'RFC_COMMON_INC', [
    '${RFA_ROOT}/rfc/common/inc',
    '${RFA_ROOT}/rfc/vreg_mgr/common/inc',
    '${RFA_ROOT}/rfc/vreg_mgr/rfc_configurable_test_vreg_mgr/inc',
    '${RFA_ROOT}/rfc/vreg_mgr/wtr1605_sv/inc',
    ])

env.PublishProtectedApi( 'RFC_RF_COMMON_CARD_INC', [
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/cdma/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/common/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/gnss/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/gsm/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/lte/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/tdscdma/inc',            
    '${RFA_ROOT}/rfc/rf_card/rfc_configurable_test_card/wcdma/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/cdma/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/common/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/gnss/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/gsm/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/lte/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/tdscdma/inc',
    '${RFA_ROOT}/rfc/rf_card/rfc_efs_card/wcdma/inc',
    ])

env.PublishProtectedApi( 'RFC_RF_CARD_INC', [
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_asdiv_drx/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_asdiv_drx_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_asdiv_prx/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_sawless/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_sawless_h250/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_sawless_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_sxlte_dsda/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chile_sxlte_dsda_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_china/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_china_sawless_3m/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_china_sawless_3m_v1/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_china_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_chl_sxlte_dsda_h150/common/inc',
	'${RFC_ROOT}/rf_card/rfc_wtr4905_msm8929_cmcc_3m/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_jp/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_jp_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_na2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_na2_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_svlte_na1/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_svlte_na1_v2/common/inc',
    '${RFC_ROOT}/rf_card/rfc_wtr4905_svlte_na1_v2_h111/common/inc',
	'${RFC_ROOT}/rf_card/rfc_wtr4905_msm8929_ct_4m/common/inc',
    ])

env.PublishProtectedApi( 'RF_DEVICE_INC', [
    '${INC_ROOT}/rfdevice_qfe1520/common/inc',
    '${INC_ROOT}/rfdevice_qfe1550/common/inc',
    '${RFA_ROOT}/device/asm/inc',
    '${RFA_ROOT}/device/pa/inc',
    '${RFA_ROOT}/device/qfe_intf/inc',
    '${RFA_ROOT}/device/rfdev_intf/inc',
    '${RFA_ROOT}/device/rffe/inc',
    ])

env.PublishProtectedApi( 'RFMODEM_COMMON_INC', [ '${RFMODEM_ROOT}/hal/common/inc', ])

env.PublishProtectedApi( 'RFC_TARGET_INC', [ '${RFC_ROOT}/target/${CHIPSET}/inc', ])

env.RequireProtectedApi([
    'RF_COMMON_INC',
    'RF_DEVICE_INC',
    'RFC_COMMON_INC',
    'RFC_RF_CARD_INC',
    'RFC_RF_COMMON_CARD_INC',
    'RFC_TARGET_INC',
    'RFMODEM_COMMON_INC',
    ])

#-------------------------------------------------------------------------------
# Override CCFLAGS for this Module. Make all Warnings to Errors!
#-------------------------------------------------------------------------------
# Enable warnings -> errors for all, except LLVM toolchain (6.x.x) during migration
if not os.environ.get('HEXAGON_RTOS_RELEASE').startswith('6') and 'USES_INTERNAL_BUILD' in env:
   env = env.Clone(HEXAGONCC_WARN = "${HEXAGONCC_WARN} -Werror", 
                   HEXAGONCXX_WARN = "${HEXAGONCCXX_WARN} -Werror")  
  
if env.get('CHIPSET') == 'msm8926':
    env.Append(CPPDEFINES = ['FEATURE_RF_SUPPORTS_DIME_LP'])

# WTR1625-WFR1620
if('USES_WTR1625' in env):
  env.Append(CPPDEFINES = ['FEATURE_RF_HAS_WTR1625_WFR1620'])

if 'USES_FEATURE_QFE1510_HDET' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1510_HDET'])

if 'USES_FEATURE_QTUNER' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QTUNER'])
		
if 'USES_FEATURE_QFE1510' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1510'])		
		
if 'USES_FEATURE_QFE1520' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1520'])		
		
if 'USES_FEATURE_QFE1550' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1550'])			

if 'USES_FEATURE_DPD' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_DPD'])

if 'USES_FEATURE_XPT' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_XPT_SUPPORT'])
   
if 'USES_FEATURE_XPT_CDMA_TEMP' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_XPT_CDMA_TEMP'])
   
if 'USES_1X_EPT_CAL' in env:
        env.Append(CPPDEFINES = ['RF_1X_EPT_CAL_TEMP'])
   
if 'USES_1X_DIME_SAMPLE_CAPTURE' in env:
        env.Append(CPPDEFINES = ['RF_1X_DIME_SAMPLE_CAPTURE'])
   
if 'USES_COMMON_DIME_SAMPLE_CAPTURE' in env:
        env.Append(CPPDEFINES = ['RF_COMMON_DIME_SAMPLE_CAPTURE'])
           
if 'USES_FEATURE_TX_INT_CAL' in env:
        env.Append(CPPDEFINES = ['FEATURE_RF_SUPPORTS_TX_INT_CAL'])

if (('USES_DIME_1_0' in env or 'USES_DIME_2_0' in env)) or ('USES_BOLT_1_0' in env ):
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QTUNER'])
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1520'])	
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1550'])	
 
# Enable the features for QFE1520 and QFE1550 only on Dime 1.0.1/Dime 2.0		
if ((('USES_DIME_1_0' in env  and 'USES_WTR1625' in env) or ('USES_DIME_2_0' in env))) and ('USES_BOLT_1_0' not in env ):
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1550'])	
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1520'])	
        env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE1510'])
 
env.Append(CPPDEFINES = ['FEATURE_RF_HAS_QFE2340'])

# antenna switch diversity
if ('USES_MODEM_ANTENNA_SWITCH_DIVERSITY' in env ):
   env.Append(CPPDEFINES = ['FEATURE_RF_ASDIV'])

if ('USES_RFA_D3925' in env):
        env.Append(CPPDEFINES = ['FEATURE_RFA_D3925'])

env.LoadSoftwareUnits()
