
/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  v4.32.916 of DPM2_RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2014 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfc_dimepm/rf_card/rfc_wtr4905_chile_sxlte_dsda/tdscdma/src/rfc_wtr4905_chile_sxlte_dsda_tdscdma_config_data_ag.c#2 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_wtr4905_chile_sxlte_dsda_cmn_ag.h" 
#include "rfc_common.h" 
#include "rfcom.h" 
#include "wtr4905v100_typedef_ag.h" 
#include "wtr2605_typedef_ag.h" 
#include "rfdevice_qtuner_api.h" 
#include "rfdevice_coupler.h" 



rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_init_tdscdma_rx_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  0 /* Warning: Not Specified */,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  1,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0 /*Warning: Not specified*/,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        0  /* Invalid TRx port*/ ,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_init_tdscdma_rx_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_init_tdscdma_tx_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  0 /* Warning: Not Specified */,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  1,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0 /*Warning: Not specified*/,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        0  /* Invalid TRx port*/ ,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_init_tdscdma_tx_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b34_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  4,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_PRXLGY1_BAND34_PMB1,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2320EPT,  /* NAME */
      1,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x20 << 14)/*prd_id*/ | (10)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b34_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -7 }, {RFC_LOW, -8 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_14,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_06,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b34_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_1,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  1,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  3,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      1,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_DRXLGY1_BAND34_DMB2,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP12T */,  /* NAME */
      4,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x81 << 14)/*prd_id*/ | (7)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2550,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b34_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -7 }, {RFC_LOW, -8 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b34_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_TX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  8,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_TX_BAND34_THMLB5,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_VALID | WTR4905V100_LP_LUT_TYPE << RFDEVICE_PA_STATE_0_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_1_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_2_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_3_BSHFT ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_LOW_ATTN_MODE,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PA,
      GEN_PA /* B34_B39_PA */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x107 << 22)/*mfg_id*/ | (0x07 << 14)/*prd_id*/ | (0)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PAPM,
      QFE2101,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x31 << 14)/*prd_id*/ | (10)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2320EPT,  /* NAME */
      1,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x20 << 14)/*prd_id*/ | (7)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_HDET,
      TRX_HDET,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_COUPLER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x3 << 14)/*prd_id*/ | (0)/*port_num*/,  /* PORT_NUM */
        (int)RFDEVICE_COUPLER_ATTENUATION_6_DB,  /* ATTEN_FWD */
        (int)RFDEVICE_COUPLER_ATTENUATION_INVALID,  /* ATTEN_REV */
        (int)RFDEVICE_COUPLER_DIRECTION_FWD,  /* POSITION */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b34_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -8 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_RANGE,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 3 }, {RFC_LOW, -8 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -24 }, {RFC_LOW, -2 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_TX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, 2 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_RX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 0 }, {RFC_LOW, 12 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -24 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -2 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_14,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_08,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_INTERNAL_GNSS_BLANK_CONCURRENCY,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TX_GTR_TH,   { RFC_CONFIG_ONLY, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b39_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  4,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_PRXLGY1_BAND39_PMB1,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2320EPT,  /* NAME */
      1,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x20 << 14)/*prd_id*/ | (10)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b39_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -7 }, {RFC_LOW, -8 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_14,   { RFC_LOW, -6 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_06,   { RFC_HIGH, -6 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b39_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_1,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  1,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  3,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      1,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_DRXLGY1_BAND39_DMB2,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP12T */,  /* NAME */
      4,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x81 << 14)/*prd_id*/ | (7)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2550,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b39_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -7 }, {RFC_LOW, -8 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b39_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_TX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  8,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_TX_BAND39_THMLB5,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_VALID | WTR4905V100_LP_LUT_TYPE << RFDEVICE_PA_STATE_0_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_1_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_2_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_3_BSHFT ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_LOW_ATTN_MODE,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PA,
      GEN_PA /* B34_B39_PA */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x107 << 22)/*mfg_id*/ | (0x07 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PAPM,
      QFE2101,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x31 << 14)/*prd_id*/ | (10)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2320EPT,  /* NAME */
      1,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x20 << 14)/*prd_id*/ | (7)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_HDET,
      TRX_HDET,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_COUPLER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x3 << 14)/*prd_id*/ | (0)/*port_num*/,  /* PORT_NUM */
        (int)RFDEVICE_COUPLER_ATTENUATION_6_DB,  /* ATTEN_FWD */
        (int)RFDEVICE_COUPLER_ATTENUATION_INVALID,  /* ATTEN_REV */
        (int)RFDEVICE_COUPLER_DIRECTION_FWD,  /* POSITION */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b39_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -8 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_RANGE,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 3 }, {RFC_LOW, -8 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -24 }, {RFC_LOW, -2 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_TX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, 2 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_RX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 0 }, {RFC_LOW, 12 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -24 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -2 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_14,   { RFC_LOW, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_08,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_INTERNAL_GNSS_BLANK_CONCURRENCY,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TX_GTR_TH,   { RFC_CONFIG_ONLY, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b40_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  4,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_PRXLGY1_BAND40_PHB1,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2340,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x21 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx0_tdscdma_b40_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b40_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_1,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  1,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  3,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      1,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_DRXLGY1_BAND40_DHB1,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP12T */,  /* NAME */
      4,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x81 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2550,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_rx1_tdscdma_b40_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b40_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_TX_MODEM_CHAIN_0,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  8,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        (int)WTR4905V100_TDSCDMA_TX_BAND40_THMLB3,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_VALID | WTR4905V100_LP_LUT_TYPE << RFDEVICE_PA_STATE_0_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_1_BSHFT | WTR4905V100_HP_LUT_TYPE << RFDEVICE_PA_STATE_2_BSHFT | WTR4905V100_LUT_PWR_INVALID << RFDEVICE_PA_STATE_3_BSHFT ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_LOW_ATTN_MODE,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PA,
      QFE2340,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x21 << 14)/*prd_id*/ | (1)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_PAPM,
      QFE2101,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x31 << 14)/*prd_id*/ | (25)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      QFE2340,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x0217 << 22)/*mfg_id*/ | (0x21 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_ASM,
      GEN_ASM /* ASM_SP6T */,  /* NAME */
      2,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x020C << 22)/*mfg_id*/ | (0x06 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        (0x020C << 22)/*mfg_id*/ | (0x020 << 14)/*prd_id*/ | (4)/*port_num*/,  /* PORT_NUM */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_HDET,
      TRX_HDET,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_COUPLER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (0x217 << 22)/*mfg_id*/ | (0x3 << 14)/*prd_id*/ | (0)/*port_num*/,  /* PORT_NUM */
        (int)RFDEVICE_COUPLER_ATTENUATION_6_DB,  /* ATTEN_FWD */
        (int)RFDEVICE_COUPLER_ATTENUATION_INVALID,  /* ATTEN_REV */
        (int)RFDEVICE_COUPLER_DIRECTION_FWD,  /* POSITION */
        0,  /* Array Filler */
      },
    },
    {
      RFDEVICE_TUNER,
      QFE2520,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        0  /* Orig setting:  */,  /* DISTORION_CONFIG */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_tx0_tdscdma_b40_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -8 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_RANGE,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_LOW, -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 3 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -15 }, {RFC_LOW, -12 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_TX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_LOW, 2 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_TX_RX_RF_ON0,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , 0 }, {RFC_LOW, 12 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_ASM_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -5 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PA_TRIGGER,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -2 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -4 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_OFF_TX_RX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -10 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TIMING_PAPM_TX_TX_CTL,   { RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }, {RFC_CONFIG_ONLY  /*Warning: Not specified*/ , -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_06,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_08,   { RFC_HIGH, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_TX_GTR_TH,   { RFC_CONFIG_ONLY, -5 }, {RFC_LOW, -6 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_tdscdma_en_et_cal0_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_1,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  1,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        0  /* Invalid TRx port*/ ,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_tdscdma_en_et_cal0_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_08,   { RFC_HIGH, 0 }, {RFC_LOW, 0 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


rfc_device_info_type rf_card_wtr4905_chile_sxlte_dsda_tdscdma_dis_et_cal0_device_info = 
{
  0x04200394,   /* Revision: v4.32.916 */
  RFC_RX_MODEM_CHAIN_1,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* ET Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  1,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    {
      RFDEVICE_TRANSCEIVER,
      WTR4905V100,  /* NAME */
      0,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0,  /* PHY_PATH_NUM */
      {
        0 /*Warning: Not specified*/,  /* INTF_REV */
        0  /* Invalid TRx port*/ ,  /* PORT */
        ( RFDEVICE_PA_LUT_MAPPING_INVALID ),  /* RF_ASIC_BAND_PA_LUT_MAP */
        FALSE,  /* TXAGC_LUT */
        WTR4905V100_FBRX_ATTN_DEFAULT,  /* FBRX_ATTN_STATE */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr4905_chile_sxlte_dsda_tdscdma_dis_et_cal0_sig_cfg = 
{
  0x04200394,   /* Revision: v4.32.916 */
  {
    { (int)RFC_WTR4905_CHILE_SXLTE_DSDA_RF_PATH_SEL_08,   { RFC_HIGH, 0 }, {RFC_LOW, 0 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


