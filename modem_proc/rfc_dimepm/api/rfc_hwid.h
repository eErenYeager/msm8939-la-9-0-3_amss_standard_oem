#ifndef RFC_HWID_H
#define RFC_HWID_H

/*===========================================================================


      R F  D r i v e r  C o m m o n  H a r d w a r e  I d e n t i f i e r

                            H e a d e r  F i l e

DESCRIPTION
  This file contains the Hardware IDs for RF Cards used across multiple
  layers of the RF driver.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 1999 - 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfc_dimepm/api/rfc_hwid.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/23/14   sd      Initial revision for DimePM 2.0 RFC HW IDs.

============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "comdef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/* -------------------------------------------------------
** The RF Card Id used in the target
** Note: The Id needs to be sequential
** ------------------------------------------------------- */
typedef enum {
  RF_HW_UNDEFINED                         = (uint8)0,
  RF_TARGET_NONE                          = RF_HW_UNDEFINED,
  RF_HW_WTR4905_CHINA                     = (uint8)29,
  RF_HW_WTR4905_CHILE_SxLTE_DSDA          = (uint8)49,
  RF_HW_WTR4905_CHILE_SAWLESS             = (uint8)50,
  RF_HW_WTR4905_SVLTE_NA1                 = (uint8)51,
  RF_HW_WTR4905_NA2                       = (uint8)52,
  RF_HW_WTR4905_CHILE_ASDIV_PRx           = (uint8)53,
  RF_HW_WTR4905_CHILE_ASDIV_DRx           = (uint8)54,
  RF_HW_WTR4905_CHINA_SAWLESS_3M          = (uint8)66,
  RF_HW_WTR4905_CHINA_SAWLESS_3M_V1       = (uint8)69,
  RF_HW_WTR4905_JP                        = (uint8)68,
  RF_HW_WTR4905_MSM8929_CMCC_3M           = (uint8)76,
  RF_HW_WTR4905_MSM8929_CT_4M	          = (uint8)77,
  RF_HW_WTR4905_CHL_SXLTE_DSDA_H150       = (uint8)145,
  RF_HW_WTR4905_CHILE_SAWLESS_H250        = (uint8)146,
  RF_HW_WTR4905_CHILE_SxLTE_DSDA_V2       = (uint8)149,
  RF_HW_WTR4905_CHILE_SAWLESS_V2          = (uint8)150,
  RF_HW_WTR4905_CHINA_V2                  = (uint8)151,
  RF_HW_WTR4905_NA2_V2                    = (uint8)153,
  RF_HW_WTR4905_CHILE_ASDIV_DRx_V2        = (uint8)154,
  RF_HW_WTR4905_JP_V2                     = (uint8)168,
  RF_HW_WTR4905_SVLTE_NA1_V2              = (uint8)251,
  RF_HW_WTR4905_SVLTE_NA1_V2_H111         = (uint8)351,

  /* Add any new HW ID before this line */
  RF_HW_MAX,
  RF_HW_DEFAULT = RF_HW_MAX, /* Default card for Off-Target Builds */
  RF_HW_EFS_CARD                          = (uint8)191, /* EFS Card */

  /* The following definitions are not needed and will need to be removed  */
  RF_HW_WTR1605_SGLTE                     = RF_HW_MAX,
  RF_HW_WTR1605_SGLTE_DSDA                = RF_HW_MAX,
  RF_HW_WTR3925_TP130                     = RF_HW_MAX,
  RF_HW_WTR3925_TP130_2                   = RF_HW_MAX,
  RF_HW_WTR3925_TP130_2_CA                = RF_HW_MAX,  
  RF_HW_WTR3925_TP130_CA                  = RF_HW_MAX,
} rf_hw_type;

#ifdef __cplusplus
}
#endif

#endif  /* RFC_HWID_H */
