#ifndef RFDEVICE_TRX_TDSCDMA_RX_ADAPTER_H
#define RFDEVICE_TRX_TDSCDMA_RX_ADAPTER_H
/*!
   @file
   rfdevice_trx_tdscdma_rx_adapter.h

   @brief

*/

/*===========================================================================

Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_interface/api/rfdevice_trx_tdscdma_rx_adapter.h#1 $

when       who    what, where, why
--------   ---   ------------------------------------------------------------------- 
07/17/14   ms    Sawless RX support
11/19/13   zg    Added stub for new iRAT API.
11/07/13   rp    Added and API to return common device class pointer.
08/27/13   zg    Initial version
============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/

#include "comdef.h"
#include "rf_buffer_intf.h"
#include "rfdevice_class.h"

#ifdef FEATURE_TDSCDMA
#include "rfdevice_tdscdma_type_defs.h"
#include "rfdevice_trx_tdscdma_rx.h"

class rfdevice_trx_tdscdma_rx_adapter:public rfdevice_trx_tdscdma_rx
{
public:
  // Constructor
  rfdevice_trx_tdscdma_rx_adapter( rfdevice_rxtx_common_class *cmn_rxtx_dev,
                               rfdevice_trx_phy_path_enum_type phy_path);

  // Destructor
  ~rfdevice_trx_tdscdma_rx_adapter();

  /*----------------------------------------------------------------------------*/
  boolean rx_init
  ( 
    rfm_device_enum_type device,  
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    rf_device_execution_type execution_type,
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_enter_mode
  ( 
    rfm_device_enum_type device, 
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_enable
  ( 
    rfm_device_enum_type device, 
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band, 
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
   );

  /*----------------------------------------------------------------------------*/
  boolean rx_tune_chan
  ( 
    rfm_device_enum_type device, 
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    uint16 chan,
    rf_device_execution_type execution_type, 
    rf_buffer_intf *script_buffer,
    uint8 rx_lin_state
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_tune_multi_chan
  ( 
    rfm_device_enum_type device,  
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    uint16 *chan,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_sleep
  ( 
    rfm_device_enum_type device, 
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_cmd_dispatch
  ( 
    rfm_device_enum_type device,  
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    rfdevice_cmd_enum_type cmd, 
    void *data
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_exit
  ( 
    rfm_device_enum_type device,
    rfcom_mode_enum_type mode,
    rfcom_tdscdma_band_type rf_band,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_get_stg_channel
  (
    rfm_device_enum_type rfm_dev,
    rfcom_tdscdma_band_type rf_band,
    uint16 *channel,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  ); 

  /*----------------------------------------------------------------------------*/
  boolean rx_configure_stg
  (
    rfm_device_enum_type rfm_dev,
    rfcom_tdscdma_band_type rf_band,
    void *data,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  );

  /*----------------------------------------------------------------------------*/
  boolean rx_disable_stg
  (
    rfm_device_enum_type rfm_dev,
    rfcom_tdscdma_band_type rf_band,
    rf_device_execution_type execution_type, 
    rf_buffer_intf* script_buffer
  ); 

  /*----------------------------------------------------------------------------*/
  boolean rx_get_rsb_nv_params
  ( 
    rfm_device_enum_type rfm_dev,
    rfcom_tdscdma_band_type rf_band, 
    uint64   i_sq,
    uint64   q_sq,
    int64    i_q,
    int16    therm_data, 
    int16*   sin_theta_final, 
    int16*   cos_theta_final, 
    int16*   gain_inv_final
  );

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Interface to enter critical section

  @details
  This function will acquire lock to assign critical section for the successding 
  code. The code will be set to critical section  until the lock is released.
  Thus there should not be an attempt to re-acquire the same lock without 
  releasing it. This will result in Deadlock scenario.

  @return
  Success/failure status flag
  */
  boolean enter_critical_section();

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Disable Critical Section

  @details
  This function will release lock which was acquired for the preceecing code.

  @return
  Success/failure status flag
  */
  boolean leave_critical_section();

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Get the lock variable used for critical section
   
  @details
  This fucntion returns lock varibale for critical section.
  This is used in IRAT scenarios to deterine whether both techs are on the same WTR.
  Will be replaced by get_instance_id. This is a temp implementation for staying 
  consistant with other Techs 
   
  @return
  rf_lock_data pointer for the device
  */
  void* get_device_critical_section_lock();

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Get rfdevice_rxtx_common_class pointer associated with this instance of device.
  
  @return 
  Pointer to rxtx common device associated with current instance of device.
  */
  rfdevice_rxtx_common_class* get_common_device_pointer(void); 

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Create measurement (IRAT) script for tdscdma
  
  @details
  Generates preload and trigger scripts for tdscdma measurement - tune away or
  tune back.
  
  @param band
  Rx Band for which the script is to be generated
 
  @param chan
  Rx Channel for which the script is to be generated
 
  @param meas_type
  Indicates whether this is a startup (tune away) script or
  cleanup (tune back) script

  @param *script_data_ptr
  Structure with input/output parameters needed for IRAT scripts.
  
  script_data_ptr->preload_action
  is an input param indicating whether preload_script should be executed
  internally by device, returned in buffer provided or skipped
  altogether (preload was already done)
  
  script_data_ptr->preload_buf
  Buffer ptr where preload script should be populated IF
  script_data_ptr->preload_action == RFDEVICE_MEAS_CREATE_SCRIPT.
  Preload script can be executed before the measurement gap without
  disrupting the source RAT.
  
  script_data_ptr->meas_action
  is an input param indicating whether trigger_script should be executed
  internally by device, returned in buffer provided or skipped
  altogether (meas script will be generated later)
  
  script_data_ptr->meas_buf
  Buffer ptr where meas script should be populated if
  script_data_ptr->meas_action == RFDEVICE_MEAS_CREATE_SCRIPT.
  Meas script should be executed within the IRAT measurment gap as its
  execution will immediately disrupt source RAT and tune for away measurement.
 
  @return
  Success/Failure flag
  */
  boolean create_meas_script
  (
    rfm_mode_enum_type mode,
    rfcom_tdscdma_band_type band,
    uint16 chan,
    rfdevice_meas_scenario_type meas_type,
    rfdevice_meas_script_data_type *script_data_ptr
  );


private:

  rftdscdma_device_rx_type *rx_device;

  rfdevice_rxtx_common_class* common_rxtx_dev;
};

#endif /*FEATURE_TDSCDMA*/
#endif /*RFDEVICE_TRX_TDSCDMA_TX_ADAPTER_H*/
