#ifndef __RFDEVICE_TRX_LTE_TX_ADAPTER_H_INCL__
#define __RFDEVICE_TRX_LTE_TX_ADAPTER_H_INCL__

/*!
   @file
   rfdevice_trx_lte_tx_adapter.h

   @brief

*/

/*===========================================================================

  Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_interface/api/rfdevice_trx_lte_tx_adapter.h#1 $

when       who    what, where, why
--------   ---   -------------------------------------------------------------------
08/29/14   aks    Compiler Warning fix 
08/26/14   aks    Updated get_tune_script to accept cell_idx as an argument
08/01/14   aks    Updated Tx Disable to accept rf_band as an input argument
07/07/14   aks    Updated cmd_dispatch to accept rf_band as an input argument
07/01/14   aks    Updated tune_to_chan to accept a flag to disable spur mitigation
04/29/14   xsh    Modify the fbrx rsb cal API to ensure calibrated rsb cal can pass back
01/02/14   aks    Removed set_port from rfdevice_trx_lte_tx class 
11/20/13   aks    Device driver updates to enable AFC on all bands
11/15/13   aks    Device driver updates to enable AFC
11/07/13   rp     Added and API to return common device class pointer.
10/25/13   aka    LTE FB path RSB cal
09/10/13   aks    Initial version
============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/

#include "comdef.h"
#include "rf_buffer_intf.h"
#include "rfdevice_class.h"

#ifdef FEATURE_LTE
#include "rfdevice_lte_type_defs.h"
#include "rfdevice_trx_lte_tx.h"

class rfdevice_trx_lte_tx_adapter:public rfdevice_trx_lte_tx
{

public:
  /* Constructor */
  rfdevice_trx_lte_tx_adapter(rfdevice_rxtx_common_class* cmn_rxtx_dev,
                              rfdevice_trx_phy_path_enum_type phy_path );


  /* Destructor */
  ~rfdevice_trx_lte_tx_adapter();

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  It places the RF Device to parked state.

  @details
  The RTR device is initialized with values independent of Mode.
  This function must be called at power up and at anytime the
  device needs to be placed in its parked state.

  @param 
  band The LTE band to be initialized to

  @return
  Success/failure status flag
 
  @pre
  SBI clock regime must be enabled.
 
  @post
  RTR will be in its parked state.

  @todo 
  This might need to be merged to the common rf_device_interface.h
  as it is common for all Modes.
  */

  virtual boolean tx_init(rfcom_lte_band_type band,
                          rfdevice_lte_script_data_type* script_data_ptr,
                          rfm_device_enum_type rfm_device);

  /*----------------------------------------------------------------------------*/
  
  /*!
  @brief
  It configure the RF Device for the requested mode of operation.

  @details
  It configure the RF device chain for the specified mode or technology.

  @return
  Success/failure status flag
 
  @pre
  SBI clock regime must be enabled.
 
  @post
  RTR will be in its default state.
  */

  virtual boolean set_mode(rfcom_lte_band_type rf_band,
                           rfdevice_lte_script_data_type* script_data_ptr,
                           rfm_device_enum_type rfm_device);

  /*----------------------------------------------------------------------------*/
 /*! 
  @brief
  Programs the RTR for Tx mode of operation in a specific band.

  @details
  RTR device is configured to receive or transmit in a specific band.
  Places the RTR into either Rx mode if we are enabling the Rx.
  OR into Tx mode if we are enabling the Tx.
  The concept of device driver is device oriented not chip oriented. 
  So there will be rxdevice and txdevice not chip device instances. 
  In RTR device, we have RX and TX device independently. 
 
  @return
  Success/failure status flag 
   
  @pre
 
  @post
  Rf Device will be placed either Rx or Tx or RxTx mode of operation
  depending on the use of RTR.
  */

  virtual boolean config_band (rfcom_lte_band_type rf_band,
                               rfdevice_lte_script_data_type* script_data_ptr,
                               rfm_device_enum_type rfm_device);

  /*----------------------------------------------------------------------------*/
  /*! 
  @brief 
  Program the PLL based on the band and frequency. 
   
  @details 
  Programs the RF synthesizer (PLL) as per the band and channel. 
   
  @param band : Specifies the RF band the PLL is to be loaded for 

  @param disable_spur_mitigation: Flag to disable spur mitigation
  
  @return
  Success/failure status flag
   
  @pre 
  RF Device init(), enter_mode(), and config_band() must have been called 
  prior to calling this function. 
   
  @post 
  RF synthesizer will be tuned for the requested band and channle. 
  */ 

  virtual boolean tune_to_chan (rfcom_lte_band_type rf_band, 
                                uint32 freq, 
                                int32 rx_freq_error,
                                rfdevice_dpd_enum_type dpd_state,
                                rfdevice_lte_script_data_type* script_data_ptr,
                                boolean ftm_mode,
                                rfm_device_enum_type rfm_device,
                                boolean disable_spur_mitigation );

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Query API to indicate if a particular LTE band uses SAW or SAWless port on
  the device

  @param rfm_device
  Logical device for which port info is queried
 
  @param band
  LTE rfcom band whose port info is needed
 
  @retval RFDEVICE_TX_INVALID_PORT
  Either the band is invalid or not configured in this device yet
 
  @retval RFDEVICE_TX_SAWLESS_PORT
  Band does not use Saw port in device
 
  @retval RFDEVICE_TX_SAW_PORT
  Band uses port with saw on this device
*/
  virtual rfdevice_tx_saw_port_type get_port_type( rfm_device_enum_type rfm_device,
                                                   rfcom_lte_band_type band );


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Returns the ET delay compensation to be applied for a given LTE band and
  bandwidth

  @details
  ET delay compensation value depends on RC error, process, TX port being used
  and the bandwidth of LTE operation.
 
  This device API will return the delay comp based on these inputs
 
  @param rfm_device
  Logical rfm device of operation. A place holder - not used currently
 
  @param band
  LTE band of operation.
 
  @param bw
  LTE bandwidth being used. Delay comp depends on the bandwidth
 
  @param delay_comp_x10ns
  Delay compensation to be applied in 1/10th of a ns unit
 
  @retval TRUE
  Indicates the API was successful and a correct delay comp value has been
  returned
 
  @retval FALSE
  Indicates API failed due to bad parameters
*/
  virtual boolean get_et_delay_comp ( rfm_device_enum_type rfm_device,
                                      rfcom_lte_band_type band,
                                      rfcom_lte_bw_type bw,
                                      int16* delay_comp_x10ns);

  /*----------------------------------------------------------------------------*/
   virtual boolean get_tune_script (
    rfcom_lte_band_type rf_band, 
    uint32 freq_khz, 
    rfcom_lte_bw_type rfcom_bw,
    int32 tx_freq_error_hz,
    rfdevice_dpd_enum_type dpd_state,
    rflte_mc_cell_idx_type cell_idx,
    rfdevice_meas_script_data_type* script_data_ptr, 
    boolean ftm_mode,
    rfm_device_enum_type rfm_device,
    boolean disable_spur_mitigation
  );

  /*!
  @brief
  Disables the Rx or Tx mode of operation.

  @details
  Puts PRX, DRX, or TX to sleep.
  Reset the RX_ENABLE or TX_ENABLE bit for PRX and TX.
  
  @return
  Success/failure status flag
  
  @pre
  RF device init() must be called.
  
  @post
  Rf Device will be placed either Rx or Tx or Sleep mode of operation
  depending on the use of RTR.

  @todo
  Add support for DRX. 
  For implemenation details, we may need further investigations. 
  For WCDMA, the enable bit is set in the last comments in pll tune. 
  As we talked to Christian, we will have config_before_enable( ) 
  and config_after_enable( ). We can update the RTR device interface 
  and driver when we are clear about this implemenation and have the 
  SSBI spreadsheet with columns supporting config before enable 
  and after enable.
  */

  virtual boolean disable(  rfm_device_enum_type rfm_device,
                            rfcom_lte_band_type rf_band, 
                            rfdevice_lte_script_data_type* script_data_ptr );

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Provides generic get and set functionality for the device specific
  parameters.

  @details
  It provides IOCTL kind of interface to get and set device specific
  parameters.

  @param cmd 
  Specifies the command to performed by the device. 
   
  @param data 
  Input and/or Output parameter for the command.

  @retval ret_val : generic return value which depends on the 'cmd' parameter.
  
  @pre
  It depends on the 'cmd' parameter. For some commands device specific init() 
  function need to be called. For some there may not be any pre-condition prior 
  to using this function.
  
  @post
  */
  virtual int32 cmd_dispatch(int cmd,
                             void *data,
                             rfcom_lte_band_type rf_band,
                             rfdevice_lte_script_data_type* script_data_ptr,
                             rfm_device_enum_type rfm_device );

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Set the BW for TX or RX of the RTR device.

  @details
  Config the RTR device for TX/RX BW settings. LTE only.
  
  */
  virtual boolean set_bw (rfcom_lte_bw_type bw,
                          rfdevice_lte_script_data_type* script_data_ptr,
                          rfm_device_enum_type rfm_device );

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Push Tx band specific information to device
  
  @details
  Does a version check to ensure it is valid. Then band_port enum to be
  used for Tx on given band is stored in device
  
  @param band
  LTE band whose data is being pushed
 
  @param data_array
  Array of int32 containing device Tx data for band
 
  @param array_size
  Size of array of int32 data to be pushed to device
  */
  virtual boolean set_band_data ( rfcom_lte_band_type band,
                                  int32* data_array,
                                  uint8 array_size,
                                  rfm_device_enum_type rfm_device);
  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  This function provides the BB filter response array from auto-gen for different BW.  

  @details
  Provide access to BB filter response array to upper layer.
  
  @retval
  *pointer to the array.
  
  @post
  LTE tech will use this value to compensate the Linearizer for a specific BW.
  */

  virtual const int8* get_bbf_resp (rfcom_lte_band_type current_lte_band,
                                    rfcom_lte_bw_type current_lte_bw,
                                    rfm_device_enum_type rfm_device);
  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  This function provides the Tx timing info based on timing parameter  
 
  @details
  All timings are in Ts and relative to the LTE subframe boundary. If timing
  is negative, it means that it is before the sub frame boundary. If it is
  positive, it means that is after the sub frame boundary.
 
  @param rfm_device
  rfm device for which timing is required
 
  @param current_lte_band
  band for which timing is required
 
  @param tx_timing_type
  timing parameter to indicate what timing to retrieve
 
  @param timing_result
  store timing in the result; output parameter; this timing is
  valid only if status is TRUE
 
  @return
  Returns true if timing is found and no errors; false otherwise

  */
  virtual boolean get_timing_info (rfm_device_enum_type rfm_device,
                                   rfcom_lte_band_type current_lte_band,
                                   rfdevice_lte_tx_timing_enum_type tx_timing_type,
                                   int16 *timing_result);

/*----------------------------------------------------------------------------*/


 virtual boolean tune_to_default_port (rfcom_lte_band_type rf_band, 
                                       uint32 freq, 
                                       int32 rx_freq_error,
                                       rfdevice_dpd_enum_type dpd_state,
                                       rfdevice_lte_script_data_type* script_data_ptr,
                                       boolean ftm_mode,
                                       rfm_device_enum_type rfm_device );


  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Get DCOC value for the band and device
   
  @param band
  enum variable which indicates the current band
 
  @param *dcoc_value
  This is the pointer to the IQ value for which we have the smallest TxLO leakage
 
  @param *nv_active
  This is the pointer to flag indicating whether this EFS is active
  
  @return status
  Return TRUE if returned data is valid else return FALSE 
  */
  virtual boolean get_txlo_dcoc_value (rfcom_lte_band_type band,
                                       uint32 *dcoc_value,
                                       boolean *nv_active );


  /*----------------------------------------------------------------------------*/
  /*!
  @brief: Get fbrx rsb nv params
	 
  @param rfm_dev: RFM Device for which FBRX RSB params are desired
  
  @param rf_band: Band in use
	 
  @param *rsb_data
   pointer to rsb data type, input I/Q, output the final mismathed sin_theta, cos_theta and gain_inv
  
  
  @return
  Success/failure status flag
  */
  virtual boolean calc_fbrx_rsb_nv_params ( rfm_device_enum_type rfm_dev,
    									rfcom_lte_band_type rf_band,
									    rfcom_lte_bw_type tx_bw,
										rfdevice_rsb_cal_data_type *rsb_data);

  virtual boolean get_fbrx_rsb_coeff ( rfdevice_lte_rx_rsb_offset_type *rsb_data,
                                  rfm_device_enum_type rfm_device);

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
 
  @param rfm_dev
  rfm device for which timing is required
  
  @param band
  enum variable which indicates the current band
 
  @param *data
 
  @param script_data_ptr
 
  @return
  Success/failure status flag
  */

  virtual boolean get_stg_chan ( rfm_device_enum_type rfm_dev, 
                                 rfcom_lte_band_type band, 
                                 rfcom_lte_earfcn_type *channel,
                                 boolean *data_valid_status, 
                                 rfdevice_lte_script_data_type* script_data_ptr ); 


  virtual boolean config_stg_chan ( rfm_device_enum_type rfm_dev, 
                                    rfcom_lte_band_type band,
                                    uint8 *stg_is_bad_fuse,
                                    void *data,
                                    rfdevice_lte_script_data_type* script_data_ptr ); 

/*----------------------------------------------------------------------------*/
  /*!
  @brief
  Helper function to save TxLO cal data for band on a particular device path
  to lte instance and EFS

  @details
  Used by TxLO leakage cal API to save calibration results. Data is saved in instance and
  entire structure is copied to efs file path initialized at bootup by RFC
 
  @param band
  LTE band whose data is being saved
 
  @param dcoc_value
  This is IQ value for which we have the smallest TxLO leakage
 
  @return
  Flag indicating success or failure of EFS write
  */
  virtual boolean save_txlo_dcoc_value ( rfcom_lte_band_type band,
                                         uint32 dcoc_value );

  virtual  boolean 
  get_tx_pll_script
  (
    int32 rx_freq_error_in_hz,
    rfcom_lte_earfcn_type rf_chan_rx, 
    rfcom_lte_earfcn_type rf_chan_tx,   
    rfcom_lte_band_type rf_band,
    uint8* tx_pll_tuned_flag,   
    rfdevice_lte_script_data_type *script_data_ptr
  );

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Interface to enter critical section

  @details
  This function will acquire lock to assign critical section for the successding 
  code. The code will be set to critical section  until the lock is released.
  Thus there should not be an attempt to re-acquire the same lock without 
  releasing it. This will result in Deadlock scenario.
 
  @return
  Success/failure status flag
  */
  virtual boolean enter_critical_section();

  /*----------------------------------------------------------------------------*/
  /*!
  @brief
  Disable Critical Section

  @details
  This function will release lock which was acquired for the preceecing code.
 
  @return
  Success/failure status flag
  */
  virtual boolean leave_critical_section();

  /*----------------------------------------------------------------------------*/
  rfdevice_rxtx_common_class* get_common_device_pointer(void);

private:
  
  rfdevice_lte_tx_type* tx_device;

  rfdevice_rxtx_common_class* common_rxtx_dev;
};

#endif /* ifdef FEATURE_LTE */
#endif // __RFDEVICE_TRX_LTE_TX_ADAPTER_H_INCL__

