#ifndef RFDEVICE_TRX_LTE_RX__H
#define RFDEVICE_TRX_LTE_RX__H

/*!
  @file
  rfdevice_trx_lte_rx.h

  @brief
  Base class for LTE RF device interface 

*/

/*===========================================================================

Copyright (c) 2012 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_interface/api/rfdevice_trx_lte_rx.h#1 $
when       who    what, where, why
--------   ---   ------------------------------------------------------------------- 
08/29/14   aks   Compiler Warning fix 
08/26/14   aks   Updated get_tune_script to accept cell_idx as an argument
08/26/14   aks   Added API to check if addition of an Rx Chain requires retune of an existing
                 Rx/Tx Chain
08/08/14   sma     Added commit to efs for writing RSB data once 
08/05/14   sma   Changed the default gain state to gain state 2 for Rx RSB 
08/01/14   ndb     Added support for extended EARFCN for LTE
07/30/14   aks   Added API to return tune Script as separate Preload and Trigger Scripts 
07/16/14   aks   Added API to get AFC adjusted Notch Frequency
07/07/14   aks   Updated Rx Cmd Dispatch to accept rf_band as an input argument
07/07/14   aks   Updated tune_to_chan to accept Call Back API from tech driver 
                 for LTE CA Spur Mitigation
07/01/14   aks   Updated tune_to_chan to accept a flag to disable spur mitigation
07/01/14   aks   Added rx_agc_gain_state as an argument to get_rsb_coeff and calc_rsb_nv_params
06/12/14   aks   Added API to get AFC adjusted Notch Frequency
06/06/14   aks   Updated API get_lna_gain_script to accept intra_band_ca_flag 
                 as an argument
03/12/14   npi   Check for Rx intraband CA support
12/17/13   rp    Added an interface to retrieve common device pointer associated with 
                 given tech trx class.
12/16/13   aks   Added new API to return RX_EN script 
11/24/13   aks   Added arg in device IRAT API for startup vs cleanup
11/19/13   aks   Overloaded function build_meas_script to support IRAT on Bolt+WTR3925
11/18/13   aks   Added a new API to return LNA gain script
11/07/13   rp    Added and API to return common device class pointer.
09/26/13   kab   Fix for using stg_is_bad_fuse value from actual STG involved in RSB Cal
09/18/13   php   Pick LO Div ratio based Type 1 Spur present
09/17/13   aks   Added function rfdevice_lte_rx_get_critical_section_ptr
08/20/13   aks   Initial version
============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "comdef.h"

#ifdef FEATURE_LTE
#include "rf_buffer_intf.h"
#include "rfdevice_lte_type_defs.h"
#include "rfdevice_type_defs.h"
#include "rflte_ext_mc.h"

#ifdef __cplusplus
#include "rfdevice_class.h"
#include "rfdevice_rxtx_common_class.h"

class rfdevice_trx_lte_rx: public rfdevice_class 
{
public:

  /* Constructor */
  rfdevice_trx_lte_rx();

  /* Destructor */
  ~rfdevice_trx_lte_rx();

  /*----------------------------------------------------------------------------*/
  /*! 
   
   @brief
   It places the RF Device to parked state.
   
   @details
   The RTR device is initialized with values independent of Mode.
   This function must be called at power up and at anytime the
   device needs to be placed in its parked state.
   
   @param band
   TE band to be initialized to
   
   @retval None.
   
   @pre
   SBI clock regime must be enabled.
   
   @post
   RTR will be in its parked state.
   
   @todo
   This might need to be merged to the common rf_device_interface.h
   as it is common for all Modes.
   
   */

  virtual boolean rx_init( 
    rfcom_lte_band_type band,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device 
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   It configure the RF Device for the requested mode of operation.

   @details
   It configure the RF device chain for the specified mode or technology.

   @retval None.
   
   @pre
   SBI clock regime must be enabled.
   
   @post
   RTR will be in its default state.
   
   */
  virtual boolean set_mode(
    rfcom_lte_band_type rf_band,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Programs the RTR for Rx mode of operation in a specific band.

   @details
   RTR device is configured to receive or transmit in a specific band.
   Places the RTR into either Rx mode if we are enabling the Rx.
   OR into Tx mode if we are enabling the Tx.
   The concept of device driver is device oriented not chip oriented. 
   So there will be rxdevice and txdevice not chip device instances. 
   In RTR device, we have RX and TX device independently. 

   @param path : RF Path of the device to be enabled.

   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver

   @param *tx_device
   "this" pointer to Tx Device pertaining to the current instance of device 
   driver
   
   @retval None
   
   @pre
   
   @post
   Rf Device will be placed either Rx or Tx or RxTx mode of operation
   depending on the use of RTR.
   
   */
  virtual boolean config_band(
    rfcom_lte_band_type rf_band,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Programs the PLL based on the band and frequency.

   @details
   Programs the RF synthesizer (PLL) as per the band and channel.

   @param rf_band
   Specifies the RF band the PLL is to be loaded for
   
   @param freq_khz
   Frequency(in KHz) to which PLL is to be tuned to
   
   @param bw
   Bandwidth to be tuned to as RFCOM enum
   
   @param rfm_device
   RFM Device being tuned 

   @param intra_band_CA_status
   Flag to indicate intra band CA status
  
   @param disable_spur_mitigation
   Flag to disable spur mitigation
   
   @param script_data_ptr
   rfdevice_meas_script_data_type ptr which holds buffer for
   preload and trigger scripts.
   
   @param spur_mitigation_params
   Structure containing Notch Frequencies and corresponding Notch Depths
   on Home Band and the Other Band
   
   @param rf_lte_spur_mitigation_cb
   Call back API passed by the Tech Driver for Spur Mitigation
   
   @return
   Success/failure status flag
   
   */
  virtual boolean get_tune_script(
    rfcom_lte_band_type rf_band,
    uint32 freq_khz,
    rfcom_lte_bw_type bw,
    rfm_device_enum_type rfm_device,
    rflte_mc_cell_idx_type cell_idx,
    boolean intra_band_CA_status,
    boolean disable_spur_mitigation,
    rfdevice_meas_script_data_type* script_data_ptr,
    rfdevice_lte_spur_mitigation_type *spur_mitigation_params,
    rfdevice_lte_spur_mitigation_cb rf_lte_spur_mitigation_cb
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Program the PLL based on the band and frequency.

   @details
   Programs the RF synthesizer (PLL) as per the band and channel.

   @param rf_band
   Specifies the RF band the PLL is to be loaded for
   
   @param chan 
   Specifies the RF uplink channel within the specified RF band

   @param disable_spur_mitigation
   Flag to disable spur mitigation

   @param intra_band_CA_status
   Flag to indicate intra band CA status

   @param script_data_ptr
   - Contains Script to be populated if write_to_hw= FALSE
   - Write_to_hw flag

   @param rf_lte_spur_mitigation_cb
   Call Back API passed from the LTE driver to WTR to execute spur
   mitigation requirements such as programming notches, restoring gain state etc.

   @retval
   Success/Failure flag
   
   @pre
   RF Device init(), enter_mode(), and config_band() must have been called 
   prior to calling this function.
   
   @post
   RF synthesizer will be tuned for the requested band and channel.
   */
  virtual boolean tune_to_chan(
    rfcom_lte_band_type rf_band, 
    uint32 freq,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device,
    boolean intra_band_CA_status,
    boolean disable_spur_mitigation,
    rfdevice_lte_spur_mitigation_type *spur_mitigation_params,
    rfdevice_lte_spur_mitigation_cb rf_lte_spur_mitigation_cb
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Disables the Rx or Tx mode of operation.

   @details
   Puts PRX, DRX, or TX to sleep.
   Reset the RX_ENABLE or TX_ENABLE bit for PRX and TX.
   
   @param path : RF Path of the device to be disabled.

   @retval None
   
   */
  virtual boolean disable( 
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device,
    rfcom_lte_band_type rf_band 
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Returns RX_EN script .

   @param script_data_ptr
   Script to be populated
   
   @param rfm_device
   RFM_device for which the Script is queried
   
   @param rf_band
   Band for which the script is queried
   
   @retval
   Success/ Failure Flag
   */
  virtual boolean enable( 
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device,
    rfcom_lte_band_type rf_band 
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Provides generic get and set functionality for the device specific
   parameters.

   @details
   It provides IOCTL kind of interface to get and set device specific
   parameters.

   @param cmd
   Specifies the command to performed by the device.
   
   @param data
   Input and/or Output parameter for the command.
   
   @param rf_band
   Band to which Rx Chain is tuned to 

   @param script_data_ptr
   Structure containing:
   - Write to HW Flag
   - Script to be populated if write_to_hw= FALSE

   @retval ret_val : generic return value which depends on the 'cmd' parameter.
   
   @pre
   It depends on the 'cmd' parameter. For some commands device specific init() 
   function need to be called. For some there may not be any pre-condition prior 
   to using this function.
   
   */
  virtual int32 cmd_dispatch(
    int cmd,
    void *data,
    rfcom_lte_band_type rf_band,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Set the BW for TX or RX of the RTR device.

   @details
   Config the RTR device for TX/RX BW settings. LTE only.

   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
   
   */
  virtual boolean set_bw(
    rfcom_lte_bw_type bw,
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device 
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   This function wakes up the RTR from sleep.

   @details
   Wakeup PRx or DRx from sleep mode

   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
   
   @retval None
   
   @pre
   RF device init() must be called.

   @post
   Rf Device will not be in sleep mode.
   */

  virtual boolean wakeup(
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   This function exits RTR from LTE mode

   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver

   @details
   Exit LTE mode
   
   @retval None
   
   @post
   RF Device will not be in LTE mode.
   */

  virtual boolean exit(
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

/*----------------------------------------------------------------------------*/

  /*!
   @brief
   Push Rx band specific information to device
   
   @details
   Does a version check to ensure it is valid. Then band_port enum to be
   used for Rx on given band is stored in device
   
   @param *rx_device
   "this" pointer to RX device that needs to be configured
   
   @param band
   LTE band whose data is being pushed
   
   @param data_array
   Array of int32 containing device Rx data for band
   
   @param array_size
   Size of array of int32 data to be pushed to device
   
   */
  virtual boolean set_band_data(
    rfcom_lte_band_type band,
    int32* data_array,
    uint8 array_size,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   This function provides the Rx timing info based on timing parameter  
 
   @details
   All timings are in Ts and relative to the LTE subframe boundary. If timing
   is negative, it means that it is before the sub frame boundary. If it is
   positive, it means that is after the sub frame boundary.
 
   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
 
   @param rfm_device
   rfm device for which timing is required
 
   @param current_lte_band
   band for which timing is required
 
   @param rx_timing_type
   timing parameter to indicate what timing to retrieve
 
   @param timing_result
   store timing in the result; output parameter; this timing is
   valid only if status is TRUE
 
   @return
   Returns true if timing is found and no errors; false otherwise
   
   */
  virtual boolean get_timing_info(
    rfm_device_enum_type rfm_device,
    rfcom_lte_band_type current_lte_band,
    rfdevice_lte_rx_timing_enum_type rx_timing_type,
    int16 *timing_result
  )=0;

  /*----------------------------------------------------------------------------*/

  /*! 
   @brief
   Build LTE measurement script in rf_buffer_intf format
   
   @details
   Similar to RFDEVICE_GET_MEAS_START_SCRIPTS but builds script in
   rf_buffer_intf format. Checks various irat parameters and builds start or
   cleanup script based on arguments.
   
   This API builds the script only for the rx_device provided. So if PRx and DRx
   scripts need to be built this API needs to be called twice - first with PRx
   device and then with DRx device pointer
   
   @param rx_device
   "this" pointer to LTE RX device whose measurement script needs to be built.
   
   @param band
   LTE band whose script is requested
   
   @param freq
   RX tune frequence
   
   @param bw
   LTE receive bandwidth
   
   @param buf
   Pointer to rf_buffer_intf object that will be populated with LTE meas script
   
   */
  virtual boolean build_meas_script(
    rfcom_lte_band_type band,
    uint32 freq,
    rfcom_lte_bw_type bw,
    rf_buffer_intf* buf,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Build LTE measurement script in rf_buffer_intf format
   
   @details
   API checks the preload and meas action and accordingly populates
   the preload and meas buffers.
   
   @param band
   LTE band whose script is requested
   
   @param freq
   RX tune frequency
   
   @param bw
   LTE receive bandwidth
   
   @param script_data_ptr
   rfdevice_meas_script_data_type ptr which holds buffer for
   preload and trigger scripts
   
   */

  virtual boolean build_meas_script(
    rfcom_lte_band_type band,
    uint32 freq,
    rfcom_lte_bw_type bw,
    rfdevice_meas_scenario_type meas_scenario,
    rfdevice_meas_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/

  /*! 
   @details
   All timings are in Ts and relative to the LTE subframe boundary. If timing
   is negative, it means that it is before the sub frame boundary. If it is
   positive, it means that is after the sub frame boundary.
 
   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
 
   @param lna_gain_state_tbl
   This is the RF Device structure which store the info for band, bw, bus, addr,
   num of gain states available, settings. This structure is populated and sent back
   for MC/Core to program the SMEM.
   
   @param rfm_device
   rfm device for which timing is required
 
   @return
   Returns true if timing is found and no errors; false otherwise
   
   */
  virtual boolean get_gain_table(
    rfdevice_lte_get_rx_gain_state_tbl_settings_type *lna_gain_state_tbl,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @details
   Returns the script to load the desired gain word
   
   @param rf_band
   LTE band whose script is requested
   
   @param rx_agc_gain_state
   Gain word requested
   
   @param script_data_ptr
   Script to which settings are copied
   
   @param intra_band_ca_status
   Intra Band CA Flag
   
   @retval TRUE
   LNA gain word script was successfully populated for the arguments provided
   
   @retval FALSE
   LNA gain word script population failed! script_data_ptr was not updated
   
   */
  virtual boolean get_lna_gain_script(
    rfcom_lte_band_type rf_band,
    uint8 rx_agc_gain_state,
    rfdevice_lte_script_data_type* script_data_ptr,
    boolean intra_band_ca_status= FALSE
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   This function provides the Rx timing info based on timing parameter  
 
   @details
   All timings are in Ts and relative to the LTE subframe boundary. If timing
   is negative, it means that it is before the sub frame boundary. If it is
   positive, it means that is after the sub frame boundary.
 
   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
 
   @param lna_gain_state_tbl
   This is the RF Device structure which store the info for band, bw, bus, addr,
   num of gain states available, settings. This structure is populated and sent back
   for MC/Core to program the SMEM.
 
   @param rfm_device
   rfm device for which timing is required
 
   @return
   Returns true if timing is found and no errors; false otherwise
 
   */
  virtual boolean set_gain_table( 
    rfdevice_lte_set_rx_gain_state_type *lna_gain_state_tbl,
    rfm_device_enum_type rfm_device 
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   This function calculates rsb NV params
 
   @details
   
   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
 
   @param *rsb_data
   This is the RF Device structure which store the info for band, bw, spectral inversion,
   rsb_a and rsb_b coefficient. This structure is populated and sent back
   for MC/Core to program the MSM.

   @param rx_agc_gain_state
   Rx AGC Gain State for which RSB coefficients are queried
 
   @param rfm_device
   rfm device for which rsb data is desired to be calculated
 
   @return
   Returns true if timing is found and no errors; false otherwise
   
   */
  virtual boolean get_rsb_coeff(
    rfdevice_lte_rx_rsb_offset_type *rsb_data,
    rfm_device_enum_type rfm_device,
    rfdevice_lte_spur_mitigation_type *spur_mitigation_params,
    uint8 rx_agc_gain_state=2 
  )=0;

/*----------------------------------------------------------------------------*/
  /*!
   @brief Get rsb nv params
   
   @param rfm_dev: RFM Device for which RSB params are desired
   
   @param rf_band: Band in use
   
   @param i_sq: I square calculated from RSB Func#1
   
   @param q_sq: Q square calculated from RSB Func#1
   
   @param i_q: I_Q calculated from RSB Func#1
   
   @param therm_data: Raw Therm read data
   
   @param *sin_theta_final: Parameter that will be returned by this function
   
   @param *cos_theta_final: Parameter that will be returned by this function
   
   @param *gain_inv_final: Parameter that will be returned by this function

   @param rx_agc_gain_state Rx AGC Gain State being calibrated
   
   */
  virtual boolean calc_rsb_nv_params( 
    rfm_device_enum_type rfm_dev,
    rfcom_lte_band_type rf_band,
    rfcom_lte_bw_type rx_bw,
    uint64   i_sq,
    uint64   q_sq,
    int64    i_q,
    uint16   therm_data,
    int16*   sin_theta_final,
    int16*   cos_theta_final, 
    int16*   gain_inv_final,
    uint8    stg_is_bad_fuse,
    uint8    rx_agc_gain_state
  )=0;

  /*----------------------------------------------------------------------------*/
  /*!
  @brief commit_rsb_data_to_efs
   
   @param rfm_dev: RFM Device for which RSB params are desired
   
   @param rf_band: Band in use
   
   @param calibrated_list: list of calibrated devcie, band and gain state
 
   */


  virtual boolean commit_rsb_data_to_efs (
    rfm_device_enum_type rfm_dev,
    rfcom_lte_band_type rf_band, 
    rfdevice_rsb_cal_verfication_data_type* calibrated_list
    ) = 0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   Program the common LTE setting

   @details
   Programs the common, non-band specific LTE settings

   @param *rx_device
   "this" pointer to Rx Device pertaining to the current instance of device 
   driver
   @param rf_band : Specifies the RF band 
   @param script_data_ptr : buffer point of which the setting script is to be returned
   @param rfm_device_enum_type : the logical device requested
   
   @retval None.
   
   */

  virtual boolean set_common( 
    rfcom_lte_band_type rf_band, 
    rfdevice_lte_script_data_type* script_data_ptr,
    rfm_device_enum_type rfm_device
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   
   @param rfm_dev
   
   @param *rx_device
   
   @param band
   
   @param *data
   
   @param script_data_ptr
   
   @return
   NULL
   
   */
  virtual boolean config_stg(
    rfm_device_enum_type rfm_dev, 
    rfcom_lte_band_type band, 
    uint8 *stg_is_bad_fuse,
    void *data,
    rfdevice_lte_script_data_type* script_data_ptr 
  )= 0;

  /*----------------------------------------------------------------------------*/
  /*!
   @brief
   
   @param rfm_dev
   
   @param band
   
   @param *channel
   
   @param *data_valid_status
   
   @param *script_data_ptr
   
   @return NULL
   */

  virtual boolean get_stg_chan(
    rfm_device_enum_type rfm_dev,
    rfcom_lte_band_type band, 
    rfcom_lte_earfcn_type *channel,
    boolean *data_valid_status, 
    rfdevice_lte_script_data_type* script_data_ptr 
  )=0; 

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief Disables stg
   
   @param rfm_dev
   
   @param script_data_ptr
   
   */

  virtual boolean disable_stg(
    rfm_device_enum_type rfm_dev,
    rfdevice_lte_script_data_type* script_data_ptr
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Interface to enter critical section

   @details
   This function will acquire lock to assign critical section for the successding 
   code. The code will be set to critical section  until the lock is released.
   Thus there should not be an attempt to re-acquire the same lock without 
   releasing it. This will result in Deadlock scenario.
   
   @return
   Success/failure status flag
   */

  virtual boolean enter_critical_section()=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Disable Critical Section
   
   @details
   This function will release lock which was acquired for the preceecing code.
   
   @return
   Success/failure status flag
   
   */

  virtual boolean leave_critical_section()=0;

  /*----------------------------------------------------------------------------*/

  /*! 
   @brief
   Returns Critical Section Ptr
   
   @details
   
   @return
   Success/failure status flag
   */

  virtual rf_lock_data_type* get_critical_section_ptr()=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Get rfdevice_rxtx_common_class pointer associated with this instance of device.
   
   @return
   Pointer to rxtx common device associated with current instance of device.
  */

  virtual rfdevice_rxtx_common_class* get_common_device_pointer() = 0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Obtain support info for rx intraband CA
   
   @details
   Determines if split CA mode is supported for the specified rx device and
   the maximum Bandwidth the specified Rx port of the device supports
   
   @param band
   LTE band for intraband CA
   
   @param ca_info
   Pointer to a structure containing fields to return the requested info - 
    - a boolean indicating whether split CA is supported
    - the max BW supported by the device's rx_port
   
   @return
   Success/failure status flag
   
   */
  virtual boolean get_rx_intra_ca_support(
    rfcom_lte_band_type band,
    rfdevice_lte_get_rx_intra_ca_info_type* ca_info
  )=0;

  /*----------------------------------------------------------------------------*/
  /*! 
   @brief
   Applies the AFC correction on the notches returned from WTR
   
   @param rf_band
   Band to which Rx Chain is tuned to
   
   @param tx_freq_error_hz
   Tx Freq Error in Hz
   
   @param notch_cnt
   No of notches
   
   @param notch_freq_list
   Notch Freq list in Hz 
   
   @param updated_notch_req_list
   Updated Notch Freq list in Hz 
   
   @return
   Returns if the API was succesful or not for the inputs passed 
   
   */

  virtual boolean get_afc_adjusted_notch_freq(
    rfcom_lte_band_type rf_band,
    int32 tx_freq_error_hz, 
    uint8 notch_cnt,
    int32* notch_freq_list_hz,
    int32* updated_notch_freq_list_hz
  )=0;


  /*----------------------------------------------------------------------------*/
  virtual boolean check_retune_reqd(
    rfcom_lte_band_type rf_band,
    uint32 freq_khz,
    rfcom_lte_bw_type rfcom_bw,
    rfm_device_enum_type rfm_device,
    boolean intra_band_CA_status,
    uint8 array_size,
    rfdevice_lte_retune_reqd_type* retune_array,
    rfdevice_lte_retune_reqd_event_type retune_event
  )=0;

private:
  rfdevice_lte_rx_type *rx_device;
};

#else

struct rfdev_lte_rx;
typedef struct rfdev_lte_rx rfdevice_trx_lte_rx;


rf_lock_data_type* 
rfdevice_lte_rx_get_critical_section_ptr
( 
  rfdevice_trx_lte_rx * rx_device
);

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Get rfdevice_rxtx_common_class pointer associated with this instance of device.
  
  @return 
  Pointer to rxtx common device associated with current instance of device.
*/
rfdevice_rxtx_common_class* 
rfdevice_lte_rx_get_common_device_pointer(rfdevice_trx_lte_rx* rx_device);


#endif /* #ifdef __cplusplus */
#endif /* %ifdef FEATURE_LTE */
#endif /* RFDEVICE_TRX_LTE_RX_H */

