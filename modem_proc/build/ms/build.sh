#!/bin/sh
#===============================================================================
#
# Image Top-Level Build Script
#
# General Description
#    Image Top-Level Build Script
#
# Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/build/ms/build.sh#1 $
# $DateTime: 2015/01/27 06:42:19 $
# $Change: 7351256 $
#
#===============================================================================

cd `dirname $0`

# Call script to setup build environment, if it exists.
if [ -e setenv.sh ]; then
source ./setenv.sh
fi

# Call the main build command
python build_variant.py $*
build_result=$?
if [ "${build_result}" != "0" ] ; then
    exit ${build_result}
fi

