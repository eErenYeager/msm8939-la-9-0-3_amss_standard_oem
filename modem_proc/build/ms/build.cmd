@echo off
rem ==========================================================================
rem
rem  Image Top-Level Build Script
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/build/ms/build.cmd#1 $
rem $DateTime: 2015/01/27 06:42:19 $
rem $Change: 7351256 $
rem
rem ==========================================================================

rem -- Call script to setup build environment, if it exists.
if exist setenv.cmd call setenv.cmd

rem -- Call the main build command
python build_variant.py %*
@exit /B %ERRORLEVEL%
