#==============================================================================#
#
# MPSS Cleanpack SCons Script
#
# GENERAL DESCRIPTION
#    Scons cleanpack script for MPSS
#
# Copyright (c) 2012 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#------------------------------------------------------------------------------#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/build/pack/build_cleanpack.py#1 $
#------------------------------------------------------------------------------#
#
#==============================================================================#

import os
from glob import glob

def exists(env):
   return env.Detect('lte')

def generate(env):
   # It's better to manually create a "white list" of files that need to be
   # copied to the HY11 directory, and then calculate the "black list" of
   # files from the white list.  If we accidentally leave a file out of the
   # white list, the compile test build will probably fail, so we'll be made
   # aware of the problem.
   # 
   # However, if we were to try to manually maintain the black list of files,
   # and we accidentally left a file out, then this file would get shipped to
   # customers, and we likely would never become aware of it.
   #
   # Also, SCons doesn't like it when we call CleanPack on a file that it
   # already knows how to build, and clean.  Therefore, we need to also
   # manually create a list of files that would normally get cleaned, so that
   # we can avoid calling CleanPack on these files as well.  If we make a
   # mistake in this list, SCons will complain about having multiple ways to
   # build the same target, thus giving us a chance to fix this problem.

   #-----------------------------------------------------------------
   # Pack build component first
   #-----------------------------------------------------------------

   white_list = [
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/cust_config.xml',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/image_cfg.xml',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/${MSM_ID}_QuRT_MODEM_PROC.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/bss_overlay.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/tcm_overlay.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/tcm_overlay_check.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/tcm_overlay_data.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/tcm_overlay_text.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_proc/qdsp6/*/tcm_static.lcs',
         '${BUILD_ROOT}/build/cust/*',
         '${BUILD_ROOT}/build/ms/build.cmd',
         '${BUILD_ROOT}/build/ms/build.sh',
         '${BUILD_ROOT}/build/ms/*.h',
         '${BUILD_ROOT}/build/ms/build_cfg.py',
         '${BUILD_ROOT}/build/ms/build_parms*.py',
         '${BUILD_ROOT}/build/ms/build_variant.py',
         '${BUILD_ROOT}/build/ms/oem_pack.py',
         '${BUILD_ROOT}/build/ms/tbc_*.py',
         '${BUILD_ROOT}/build/ms/tcfg_*.py',
         '${BUILD_ROOT}/build/ms/build_cfg.xml',
         '${BUILD_ROOT}/build/ms/cust_config.xml',
         '${BUILD_ROOT}/build/ms/image_cfg.xml',
         '${BUILD_ROOT}/build/ms/msm_jtag_mapping.xml',
         '${BUILD_ROOT}/build/ms/pl_cfg.xml',
         '${BUILD_ROOT}/build/ms/DEVCFG_IMG.lcs.template',
         '${BUILD_ROOT}/build/ms/${MSM_ID}_QuRT_MODEM_PROC.lcs',
         '${BUILD_ROOT}/build/ms/bss_overlay.lcs',
         '${BUILD_ROOT}/build/ms/tcm_overlay.lcs',
         '${BUILD_ROOT}/build/ms/tcm_overlay_check.lcs',
         '${BUILD_ROOT}/build/ms/tcm_overlay_data.lcs',
         '${BUILD_ROOT}/build/ms/tcm_overlay_text.lcs',
         '${BUILD_ROOT}/build/ms/tcm_static.lcs',
         '${BUILD_ROOT}/build/ms/obj-local.lst',
         '${BUILD_ROOT}/build/ms/compress.lst',
         '${BUILD_ROOT}/build/ms/compress_rw.lst',
         '${BUILD_ROOT}/build/ms/dynrec.lst',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_gensecimage.cfg.template',
         '${BUILD_ROOT}/build/bsp/*/build/*.scons',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/modem_signingattr_qpsa.cfg.template',
         '${BUILD_ROOT}/build/bsp/*/build/*.csv',
         '${BUILD_ROOT}/build/manifest.xml',
         '${BUILD_ROOT}/build/build/config.scons',
         '${BUILD_ROOT}/build/pack/build_cleanpack.py',
         '${BUILD_ROOT}/build/myps/elfManipulator/*.py',
         '${BUILD_ROOT}/build/myps/elfManipulator/include/*.py',
         '${BUILD_ROOT}/build/myps/elfManipulator/include/*.pyd',
         '${BUILD_ROOT}/build/myps/elfManipulator/include/*.so',
         '${BUILD_ROOT}/build/myps/elfManipulator/include/zip32bits/*',
         '${BUILD_ROOT}/build/myps/elfManipulator/include/zip64bits/*',
         '${BUILD_ROOT}/build/myps/qshrink/*.py',
         '${BUILD_ROOT}/build/myps/qshrink/msg_hash.txt',
         '${BUILD_ROOT}/build/myps/tools/*.py',
         '${BUILD_ROOT}/build/myps/tools/pplkcmd.*',
         '${BUILD_ROOT}/build/build/*/qdsp6/*/qc_version.o',
         '${BUILD_ROOT}/build/scripts/build_utils.py'
      ]
      
   if 'USES_PACK_SETENV' in env:
      white_list.append('${BUILD_ROOT}/build/ms/setenv.cmd')
      white_list.append('${BUILD_ROOT}/build/ms/setenv.sh')
      white_list.append('${BUILD_ROOT}/build/ms/setenv.py')
      
   already_cleaned_list = [
         '${BUILD_ROOT}/build/bsp/*/build/*/*.txt',
         '${BUILD_ROOT}/build/bsp/*/build/*.txt',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${BUILDPATH}/*.elf',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${BUILDPATH}/*.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${BUILDPATH}/*.lf',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${BUILDPATH}/*.map',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${SHORT_BUILDPATH}/*.elf',
         '${BUILD_ROOT}/build/bsp/mapss_b/build/${SHORT_BUILDPATH}/*.elf',
         '${BUILD_ROOT}/build/bsp/mapss_b/build/${SHORT_BUILDPATH}/*.lcs',
         '${BUILD_ROOT}/build/bsp/mapss_b/build/${SHORT_BUILDPATH}/*.lf',
         '${BUILD_ROOT}/build/bsp/mapss_b/build/${SHORT_BUILDPATH}/*.map',
         '${BUILD_ROOT}/build/bsp/mapss_b/build/${SHORT_BUILDPATH}/*.elf',
         '${BUILD_ROOT}/build/ms/bin/*/*/*/*/*/*',
         '${BUILD_ROOT}/build/ms/bin/*/*/*/*/*',
         '${BUILD_ROOT}/build/ms/bin/*/*/*/*',
         '${BUILD_ROOT}/build/ms/bin/*/*/*',
         '${BUILD_ROOT}/build/ms/bin/*/*',
         '${BUILD_ROOT}/build/ms/*.elf',
         '${BUILD_ROOT}/build/ms/*.i',
         '${BUILD_ROOT}/build/ms/*.lcs',
         '${BUILD_ROOT}/build/ms/*.lf',
         '${BUILD_ROOT}/build/ms/*.map',
         '${BUILD_ROOT}/build/ms/*.mbn',
         '${BUILD_ROOT}/build/ms/*.pp',
         '${BUILD_ROOT}/build/ms/DEVCFG_IMG.lcs',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${SHORT_BUILDPATH}/oem_uuid.c',
         '${BUILD_ROOT}/build/bsp/modem_proc_img/build/${SHORT_BUILDPATH}/oem_uuid.o',
         '${BUILD_ROOT}/build/build/*/qdsp6/*/qc_version.c',
         '${BUILD_ROOT}/build/build/*/qdsp6/*/oem_version.c',
         '${BUILD_ROOT}/build/build/*/qdsp6/*/oem_version.o'
      ]

   # Resolve wild-cards
   # (The 'sorted' function is only being used here to force a copy
   # of white_list so that we can manipulate white_list from within
   # the 'for' loop.)
   for lst in [already_cleaned_list, white_list]:
      for file_1 in sorted(lst):
         file_2 = env.subst(file_1)
         files_3 = glob(file_2)
         lst.remove(file_1)
         lst += [x.replace('\\', '/') for x in files_3]

   # Now walk the build directory and call CleanPack on everything that's
   # not in the white_list.

   for root, dirs, files in os.walk(env.subst('${BUILD_ROOT}/build')):
      for file in files:
         abs_path = os.path.join(root, file).replace('\\', '/')
         if abs_path not in white_list and abs_path not in already_cleaned_list:
            env.CleanPack("CLEANPACK_TARGET", abs_path)

   #-----------------------------------------------------------------
   # Pack Config component
   #
   # Only pack the config component if VARIANT_LIST is defined in the
   # OS environment.  This signifies that we're doing a client workflow
   # build.  Otherwise we fall back on the legacy behavior and let
   # the config component pack itself.
   #-----------------------------------------------------------------

   if not env.PathExists('${BUILD_ROOT}/config/bsp/config_img/build/config_cleanpack.scons'):
      if 'VARIANT_LIST' in os.environ:

         # Generate a white list of all of the config files for the specified
         # client, and then call cleanpack on all other files.
         env.LoadToolScript('build_utils', toolpath = ['${BUILD_ROOT}/build/scripts'])

         white_list = []

         # Legacy search for BO and earlier SIs (this is forwards-compatible with
         # later SIs, so we don't need an explicit check for SI name:

         for variant in os.environ['VARIANT_LIST'].split(','):
            white_list += (env.FindConfigFiles('image_cfg.xml', variant) +
                           env.FindConfigFiles('cust_config.xml', variant) +
                           env.FindConfigFiles('compress.lst', variant) +
                           env.FindConfigFiles('compress_rw.lst', variant) +
                           env.FindConfigFiles('dynrec.lst', variant) +
                           env.FindConfigFiles('DEVCFG_IMG.lcs.template', variant) +
                           env.FindConfigFiles('*.lcs', variant) +
                           env.FindConfigFiles('*.csv', variant))

         # Newer search for DPM and later SIs (backwards compatable, so no need to
         # check SI name):

         def FindCfgFilesFromRootDir (root_dir):
            return [x.replace('\\', '/')
                    for x in env.FindFiles("*", '${BUILD_ROOT}/config/' + root_dir)
                    if not x.endswith('internal_build_cfg.xml')]

         white_list += FindCfgFilesFromRootDir('default')

         for variant in os.environ['VARIANT_LIST'].split(','):
            chip, flavor, purpose = variant.split('.')
            white_list += FindCfgFilesFromRootDir(chip)
            white_list += FindCfgFilesFromRootDir(flavor)
            white_list += FindCfgFilesFromRootDir(chip + '/' + flavor)

         all_files = [x.replace('\\', '/') for x in env.FindFiles('*', "${BUILD_ROOT}/config")]
         clean_list = list(set(all_files) - set(white_list))

         env.CleanPack("CLEANPACK_TARGET", clean_list)

      else:
         env.CleanPack("CLEANPACK_TARGET", env.FindFiles('*', "${BUILD_ROOT}/config"))

   # Else let the config_cleanpack.scons take care of packing the config module.

# End of generate()
