##  @file       dynRec_manip.py.
#   @author     hzhi.
#   @brief      This script provides a generic manipulator for dynamic reclaiming segment.
#   @version    1.0.
#   @todo       Currently has hard-coded constant numbers which needs be optimized.
#===============================================================================
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/build/myps/elfManipulator/dynRec_manip.py#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#  $Author: mplp4svc $
#  $Change: 7351256 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when      who     ver     what, where, why
# --------  ------  ----    ----------------------------------------------------
# 09/24/14  skotalwa 1.0    fix for CR729716.Veneer reuse
# 09/04/14  hzhi    1.0     Creation for FR20842 Dynamic Reclaiming feature.
#-------------------------------------------------------------------------------

import include.elfUtils as utils
import include.elfConstants as const
import include.elfFileClass as elfFileClass
import sys
import re

##
# @brief    Read contents from the specified address to specifed size
# @param    elf          The elfFileClass object to read from
# @param    address      The starting point to read from
# @param    size        amount specified to be read (max of 4 bytes)
# @pre      returns what was read in string format or returns a non-zero value
def readDataByAddress(elf, address, size):
 for section in elf.sectionHeaderTable:
  if (address >= section.sh_addr and 
  address <= section.sh_addr + section.sh_size):
   offset = address - section.sh_addr
   return utils.convertFromELF(section.contents[offset:offset+size],size)
 return const.RC_ERROR

##
# @brief    Write contents starting at the specified address
# @param    elf          The elfFileClass object to read from
# @param    address      The starting point to read from
# @param    size         size of the contents that are being written (max of 4 bytes)
# @pre      returns 0 on successful write otherwise a non-zero value
def setDataByAddress(elf, address, size, number):
 for section in elf.sectionHeaderTable:
  if (address >= section.sh_addr and 
  address <= section.sh_addr + section.sh_size):
   offset = address - section.sh_addr 
   section.contents = "".join([section.contents[:offset],
   utils.convertToELF(size, number),
   section.contents[offset+size:]])
   return const.RC_SUCCESS
 return const.RC_ERROR

##
# @brief    get symbols that have the specified expression in their string names
# @param    elf          The elfFileClass object to read from
# @param    expression   string to look for in each symbol string name
# @pre      returns all symbols with matching expression or non-zero value otherwise
def getLinkerVars(elf, expression, symtab, strtab, symbols):
 symbolList = []
 if (symtab != const.RC_ERROR and strtab != const.RC_ERROR):
  for symbol in symbols:
  # Inefficient, but assumed that symbolNameList is relatively small
   if re.match( expression, symbol.st_nameStr, flags=0):
    print "S: " + symbol.st_nameStr + "  V: " + str(hex(symbol.st_value))
    symbolList.append(symbol)
 else:
  print("Missing .symtab and/or .strtab section")
  return const.RC_ERROR
 return symbolList
		
def getSectionName(elf, expression):
       SectionNameList = []
       for sh in elf.sectionHeaderTable:
           if re.match(expression, sh.sh_nameStr, flags=0):
               SectionNameList.append(sh.sh_nameStr)
               if(len(SectionNameList)!= 0):
	         return SectionNameList
       return const.RC_ERROR

def main():
#{{{{
  print "Running..."
  elf_handle = elfFileClass.elfFile(sys.argv[1])

  Dict = {}
  #NameList = getSectionName(elf_handle, ".dynamicReclaim.*")
  #print(NameList)
  sh_src = elf_handle.getSectionByName(".dynamicReclaim")
  if sh_src == const.RC_ERROR:
  #{
    print "No Dynamic Reclaiming segment found, aborting..."
    sys.exit()
  #}
  Dict["old_dynrec_base_address"] = sh_src.sh_addr

  elf_handle.moveupSectionByName(".dynamicReclaim")
  sh_src = elf_handle.getSectionByName(".dynamicReclaim")


  Dict["dynrec_base_address"] = sh_src.sh_addr
  Dict["dynrec_section_size"] = sh_src.sh_size

  Dict["image_vend"] = (sh_src.sh_addr + sh_src.sh_size + 4096) & (~4095)

  #==========================================================================================================================
  # updated to remove dupllicates from list.
  #==========================================================================================================================
  # dynrecSymbol_text_start_list = sorted(getLinkerVars(elf_handle, "__dynrec_text_start.*"), key=lambda x: x.st_value, reverse=False)
  # dynrecSymbol_text_end_list = sorted(getLinkerVars(elf_handle, "__dynrec_text_end.*"),key=lambda x: x.st_value, reverse=False)
  # dynredSymbol_data_start_list = sorted(getLinkerVars(elf_handle, "__dynrec_data_start.*"),key=lambda x: x.st_value, reverse=False)
  # dynredSymbol_data_end_list = sorted(getLinkerVars(elf_handle, "__dynrec_data_end.*"),key=lambda x: x.st_value, reverse=False)

  elf = elf_handle
  symtab = elf.getSectionByName(".symtab")
  strtab = elf.getSectionByName(".strtab")
  print "\nGetting symbols..."
  symbols = elfFileClass.Elf32_SymGenerator(symtab, strtab)

  SymbolList = getLinkerVars(elf, "__dynrec_.*_rfc_.*", symtab, strtab, symbols)
  if len(SymbolList) == 0:
  #{
    print "No RFC symbols found, abort execution..."
    sys.exit()
  #}

  print "\nGetting text _starts_ ..."  
  dynrecSymbol_text_start_preorder_list = []
  for symbol in SymbolList:
  #}
    if re.match( ".*dynrec_text_start.*", symbol.st_nameStr, flags=0) != None:
    #{
      print "S: " + symbol.st_nameStr + "  V: " + str(hex(symbol.st_value))
      dynrecSymbol_text_start_preorder_list.append(symbol)
    #}
  #}

  dynrecAddrTableSize = len(dynrecSymbol_text_start_preorder_list)
  if dynrecAddrTableSize == 0:
  #{
    print "\nNo RFC text starts symbols found, abort execution..."
    sys.exit()
  #}

  print "\nsorting..."
  dynrecSymbol_text_start_preorder_list = sorted(dynrecSymbol_text_start_preorder_list, key=lambda x: x.st_value, reverse=False)
  print dynrecSymbol_text_start_preorder_list
    
  print "\nGetting text _ends_ data _starts_ and data _ends_ ..."

  dynrecSymbol_text_end_preorder_list = []
  dynredSymbol_data_start_preorder_list = []
  dynredSymbol_data_end_preorder_list = []
  for j in range(0, dynrecAddrTableSize):
  #{
    symbolTextStart = dynrecSymbol_text_start_preorder_list[j].st_nameStr
    symbolTextEnd = symbolTextStart.replace("_start_", "_end_")
    symbolDataStart = symbolTextStart.replace("_text_", "_data_")
    symbolDataEnd = symbolDataStart.replace("_start_", "_end_")

    for symbol in SymbolList:
    #{
      if symbolTextEnd == symbol.st_nameStr:
      #{
        dynrecSymbol_text_end_preorder_list.append(symbol)
      #}
      elif symbolDataStart == symbol.st_nameStr:
      #{
        dynredSymbol_data_start_preorder_list.append(symbol)
      #}
      elif symbolDataEnd == symbol.st_nameStr:
      #{
        dynredSymbol_data_end_preorder_list.append(symbol)
      #}
    #}
  #}


  if len(dynrecSymbol_text_end_preorder_list) != dynrecAddrTableSize or len(dynredSymbol_data_start_preorder_list) != dynrecAddrTableSize or len(dynredSymbol_data_end_preorder_list) != dynrecAddrTableSize:
  #{
    print "\nmissing addr symbols for text ends, data starts or data ends, abort execution..."
    sys.exit()
  #}

  print "\nPrinting pre-order list entries:"
  for x in range(0, dynrecAddrTableSize):
  #{
    print "S: " + dynrecSymbol_text_start_preorder_list[x].st_nameStr + "  V: " + str(hex(dynrecSymbol_text_start_preorder_list[x].st_value))
    print "S: " + dynrecSymbol_text_end_preorder_list[x].st_nameStr + "  V: " + str(hex(dynrecSymbol_text_end_preorder_list[x].st_value))
    print "S: " + dynredSymbol_data_start_preorder_list[x].st_nameStr + "  V: " + str(hex(dynredSymbol_data_start_preorder_list[x].st_value))
    print "S: " + dynredSymbol_data_end_preorder_list[x].st_nameStr + "  V: " + str(hex(dynredSymbol_data_end_preorder_list[x].st_value))
  #}

  dynrecSymbol_text_start_list = []
  dynrecSymbol_text_end_list = []
  dynredSymbol_data_start_list = []
  dynredSymbol_data_end_list = []

  print "\nRemoving empty modules..."
  for j in range(0, dynrecAddrTableSize):
  #{{
    symbol_va_start_text = dynrecSymbol_text_start_preorder_list[j]
    symbol_va_end_text = dynrecSymbol_text_end_preorder_list[j]
    symbol_va_start_data = dynredSymbol_data_start_preorder_list[j]
    symbol_va_end_data = dynredSymbol_data_end_preorder_list[j]
    if(symbol_va_start_text.st_value < symbol_va_end_text.st_value and symbol_va_start_data.st_value < symbol_va_end_data.st_value):
    #{
      print "ok"
      print "S: %s %s %s %s"%(symbol_va_start_text.st_nameStr, symbol_va_end_text.st_nameStr, symbol_va_start_data.st_nameStr, symbol_va_end_data.st_nameStr)
      print "V: %08X %08X %08X %08X"%(symbol_va_start_text.st_value, symbol_va_end_text.st_value, symbol_va_start_data.st_value, symbol_va_end_data.st_value)
      dynrecSymbol_text_start_list.append(symbol_va_start_text)
      dynrecSymbol_text_end_list.append(symbol_va_end_text)
      dynredSymbol_data_start_list.append(symbol_va_start_data)
      dynredSymbol_data_end_list.append(symbol_va_end_data)
    #}
  #}}
  
  print "\nRemoved empty modules..."
  
  if(len(dynrecSymbol_text_start_list) == 0):
  #{{
    print "\nNo module to be dynamic reclaimed, aborting..."
    sys.exit()
  #}}
  
  if(dynrecSymbol_text_start_list[0].st_nameStr.find("_preload") != -1):
  #{{
    dynrecSymbol_text_start_list[0].st_nameStr = dynrecSymbol_text_start_list[0].st_nameStr.rstrip("_preload")
    dynrecSymbol_text_end_list[0].st_nameStr = dynrecSymbol_text_end_list[0].st_nameStr.rstrip("_preload")
    dynredSymbol_data_start_list[0].st_nameStr = dynredSymbol_data_start_list[0].st_nameStr.rstrip("_preload")
    dynredSymbol_data_end_list[0].st_nameStr = dynredSymbol_data_end_list[0].st_nameStr.rstrip("_preload")
    print "\nStripped first preloaded card back to original name..."
  #}}
  else:
  #{{
    print "\nDid not find \"_preload\" pattern in first card, maybe it's empty..."
  #}}
  print "\nDone..."
  #=========================================================================================================================

  Dict["dynrec_numentries"] = len(dynrecSymbol_text_start_list)

  elf_handle.updateSymbolValuesByDict(Dict)

  dynrec_linker_var_arr = elf_handle.getSymbolByName("dynrec_linker_var_arr")
  if(dynrec_linker_var_arr == const.RC_ERROR):
    elf_handle.writeOutELF(sys.argv[2])
    sys.exit()


  data_ptr = dynrec_linker_var_arr.st_value
  val = readDataByAddress(elf_handle, data_ptr + 1, 1)


  for i  in range(0,len(dynrecSymbol_text_start_list)):
   marker = re.sub('__dynrec_text_start_', '', dynrecSymbol_text_start_list[i].st_nameStr, count=1)
   if (setDataByAddress(elf_handle, data_ptr,1,1) == const.RC_ERROR):
    print("failed to set")
   data_ptr = data_ptr + 1
   str_ptr = data_ptr 
   for char in marker:
    setDataByAddress(elf_handle, str_ptr,1,ord(char))
    str_ptr = str_ptr + 1
    
   data_ptr = data_ptr + 51
   setDataByAddress(elf_handle, data_ptr,4,dynrecSymbol_text_start_list[i].st_value)
   data_ptr = data_ptr + 4
   setDataByAddress(elf_handle, data_ptr,4,dynrecSymbol_text_end_list[i].st_value)
   data_ptr = data_ptr + 4
   setDataByAddress(elf_handle, data_ptr,4,dynredSymbol_data_start_list[i].st_value)
   data_ptr = data_ptr + 4
   setDataByAddress(elf_handle, data_ptr,4,dynredSymbol_data_end_list[i].st_value)
   data_ptr = data_ptr + 4
   
  elf_handle.writeOutELF(sys.argv[2])

  return 0
  
#}}}}

if __name__ == '__main__':
#{{{{
  main()
#}}}}
