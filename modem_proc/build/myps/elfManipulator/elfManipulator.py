##  @file       elfManipulator.py
#   @author     Christopher Ahn (cahn@qualcomm.com)
#   @brief      This script provides a generic manipulator for ELF files.
#   @version    1.09
#   @todo       The elf manipulator currently only supports a sub-class of ELF
#               files. Specifically, it is limited to 32-bit little-endian
#               executable targeted for QDSP6V5, with PT_LOAD segments only,
#               no dynamic linking, and no relocations. It is a future action
#               item to broaden the scope of this script.

#===============================================================================
# Copyright (c) 2013 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/build/myps/elfManipulator/elfManipulator.py#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#  $Author: mplp4svc $
#  $Change: 7351256 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when      who     ver     what, where, why
# --------  ------  ----    ----------------------------------------------------
# 09/12/14  ao      1.09    Fixed setting end_va_compressed_text bug introduced in ver 1.06
# 09/11/14  ao      1.08    Remove use of updateSymbolValueByName and printSymbolInfoByName
#                           for faster execution.
# 07/22/14  ao      1.06    Added RW compression.
# 03/13/14  cahn    1.05    Removing executable flag from q6zip section/segment.
# 02/24/14  cahn    1.04    Added -M option for overlay_mem_dump feature.
# 01/20/14  ao      1.03    Import compressor only if there is a candidate_compress_section
# 01/20/14  ao      1.02    Added q6zip3 compressor call
# 12/11/13  cahn    1.01    Moving q6zip include only if section is present.
# 11/21/13  cahn    1.00    Optimizations and speedup.
# 04/05/13  cahn    0.17    Added support for getting only 1 symbol's info.
# 04/03/13  cahn    0.16    Fixed bug wherein the image_vend was being updated
#                           based on the last segment in the linker instead
#                           of segment with the highest virtual address. Also
#                           some cosmetic changes.
# 03/29/13  cahn    0.15    Various new APIs added. Changed the name of functions.
# 03/26/13  cahn    0.14    Support for ro_fatal compression.
# 03/18/13  cahn    0.13    Added __init__.py and more sample modifications
# 03/18/13  cahn    0.12    Added support for resizing of sections and updating
#                           symbol value by name.
# 03/15/13  cahn    0.11    Updated usage text.
# 03/15/13  cahn    0.10    ALPHA release. Added support for removal of section.
# 03/10/13  cahn    0.09    Added functionality for writing out modified ELF.
# 03/07/13  cahn    0.08    Specification review, cleanup and added Doxygen
#                           comments.
# 03/07/13  cahn    0.07    Code abstracted to elfFileClass.py.
# 03/06/13  cahn    0.06    Added support for reading the symbol table.
#                           Added debug print, partially modeled from readelf.
# 03/05/13  cahn    0.05    Added mapping of section names from .shstrtab index.
# 03/05/13  cahn    0.04    Added support for reading the section header table.
# 03/05/13  cahn    0.03    Added support for reading the program header table.
# 03/04/13  cahn    0.02    Added support for reading the ELF Header.
# 03/04/13  cahn    0.01    Creation.
#===============================================================================

# Imports
import optparse
import os
import sys
import time
import inspect
import include.elfConstants as const
import include.elfFileClass as elfFileClass
import include.elfUtils as utils

##
# @brief    Main entry point
def main():
    # Use optparse to set up usage
    use = "Usage: python %prog [options] <Input ELF> <Output ELF>"
    use += "\n       python %prog -s symbolName <ELF>"
    parser = optparse.OptionParser(usage = use, version="%prog 1.09")
    parser.add_option(  "-d", action="store_true", dest="debug",
                        help="enable debug")
    parser.add_option(  "-r", action="store", dest="removeSection", type="str",
                        help="Section to remove")
    parser.add_option(  "-t", action="store_true", dest="timing",
                        help="print program execution time")
    parser.add_option(  "-V", action="store_true", dest="verification",
                        help="enable ELF verification (currently none)")
    parser.add_option(  "-R", action="store_true", dest="ro_fatal",
                        help="Enable ro fatal section removal/compression")
    parser.add_option(  "-M", action="store_true", dest="overlay_mem_dump",
                        help="Enable memory dump overlay")
    parser.add_option(  "-c", action="store", dest="coreRoot",
                        help="Specify the root folder of CoreBSP")
    options, arguments = parser.parse_args()

    # Check arguments
    if len(arguments) != 2:
        parser.error("Unexpected argument length")
        exit(const.RC_ERROR)

    baseELF = arguments[0]
    modifiedELF = arguments[1]
    if not os.path.exists(baseELF):
        parser.error("Specified ELF file does not exist.")
        exit(const.RC_ERROR)

    # Print configuration
    if not options.debug:
        print "================================================================"
        print " elfManipulator.py - Generic manipulator for ELF files"
        print "----------------------------------------------------------------"
        print " Base ELF: ".ljust(20) + baseELF
        print " Modified ELF: ".ljust(20) + modifiedELF
        print " Debug: ".ljust(20) + str(options.debug)
        print " Verification: ".ljust(20) + str(options.verification)
        print "================================================================"

    # Record the starting time
    if options.timing:
        start_time = time.time()

    # Get the elf file as an elfFile object
    if not options.debug:
        print "----------------------------------------------------------------"
        print " Getting ELF data from input ELF..."
        print "----------------------------------------------------------------"
    elf = elfFileClass.elfFile(baseELF)

    # If debug is enabled, show ELF contents similarly to readelf -a
    if options.debug:
        elf.printInfo()

    if not options.debug:
        print "----------------------------------------------------------------"
        print " Applying requested ELF modifications..."
        print "----------------------------------------------------------------"

    """
    HERE IS WHERE ALL THE ELF MODIFICATIONS SHOULD HAPPEN
    """
    # -r option: Remove specified section
    if options.removeSection:
        if not options.debug:
            print "Attempting to remove section '" + options.removeSection + "'"
        elf.removeSectionByName(options.removeSection)

    symbolDict = {}

    # -M option: Remove BSS section for 'overlay_mem_dump' feature
    overlay_mem_dumpSize = -1
    if options.overlay_mem_dump:
        print "OVERLAY_MEM_DUMP"
        overlay_mem_dumpSh = elf.getSectionByName("overlay_mem_dump")
        ro_fatalSh = elf.getSectionByName("ro_fatal")
        if overlay_mem_dumpSh != const.RC_ERROR and ro_fatalSh != const.RC_ERROR:
            # If an overlay_mem_dump section is found
            print "\t'overlay_mem_dump' section found"
            print "\tAttempting to remove 'overlay_mem_dump' BSS section."
            overlay_mem_dumpSize = overlay_mem_dumpSh.sh_size
            elf.removeBssOverlaySectionByName("overlay_mem_dump")
            if overlay_mem_dumpSize > ro_fatalSh.sh_size:
                # It is expected that size of ro_fatal section matches the 
                # program segment size in subsequent steps. elfManipulator is
                # currently written to manipulate single section segments,
                # with the EXCEPTION of removing BSS sections.
                print "\t'overlay_mem_dump' exceeded 'ro_fatal' size. Re-sizing."
                elf.resizeSectionByName("ro_fatal", len(ro_fatalSh.contents))
        else:
            if overlay_mem_dumpSh == const.RC_ERROR:
                print "\tNo 'overlay_mem_dump' section found. No-op."
            if ro_fatalSh == const.RC_ERROR:
                print "\tNo 'ro_fatal' section found. No-op."

    # -R option: Remove or compress 'ro_fatal' section
    if options.ro_fatal:
        print
        print "RO_FATAL COMPRESSION"
        # Get the section in question
        sh = elf.getSectionByName("ro_fatal")
        if sh != const.RC_ERROR:
            # If an ro_fatal section is found
            print "\t'ro_fatal' section found"
            if sh.sh_size == 0:
                # If 0 size section is found, remove the section for mbn
                print "\tZero-sized 'ro_fatal' section. Attempting removal."
                elf.removeSectionByName("ro_fatal")
            else:
                # Move up the ro_fatal section and then compress it
                print "\tMoving up 'ro_fatal' to closest section"
                symbolDict["__ro_fatal_old_start"] = sh.sh_addr
                oldSize = sh.sh_size
                elf.moveupSectionByName("ro_fatal")
                print "\tCompressing 'ro_fatal' contents using zlib"
                sh = elf.compressSectionByName("ro_fatal")
                if (sh != const.RC_ERROR):
                    # If compression is successful, update symbols accordingly
                    symbolDict["__ro_fatal_new_start"] = sh.sh_addr
                    symbolDict["__ro_fatal_new_end"] = sh.sh_addr + len(sh.contents)
                else:
                    utils.raiseElfManipulatorError("Failed to compress ro_fatal")
                # Print out statistics
                print "\tOld Size: " + str(oldSize)
                print "\tNew Size: " + str(sh.sh_size)

        else:
            # The last segment is still created due to linker script
            print "\tNo 'ro_fatal' section found, checking for zero-sized segment"
            if ((elf.programHeaderTable[-1].p_memsz == 0) and
                (elf.programHeaderTable[-1].p_vaddr == 0)):
                print "\tRemoving zero-sized segment"
                elf.elfHeader.e_phnum -= 1
                elf.programHeaderTable.pop()
  
    #=============================================
    # START: Q6_ZIP CHANGES
    #=============================================
    q6_roSection = ".candidate_compress_section"
    q6_rwSection = ".rw_candidate_compress_section"
    pageSize = 4096
    
    # determine where the compressors reside
    compressorDirs = [
        "../../core/kernel/dlpager/compressor",
        "../../../core/kernel/dlpager/compressor",
        "./include"]

    compressorDirs = [os.path.join(*i.split('/')) for i in compressorDirs]

    my_d = os.path.split(
        inspect.getframeinfo(inspect.currentframe()).filename)[0]

    if options.coreRoot:
        compressorDirs.insert(0, options.coreRoot)

    for d in compressorDirs:
        d = os.path.abspath(os.path.join(my_d, d))

        if os.path.isdir(d) and 'q6zip_compress.py' in os.listdir(d):
            sys.path.insert(0, d)
            break

    print
    print "Q6ZIP FEATURE (RO)"
    
    # Get the ELF section containing the code to compress
    sh = elf.getSectionByName(q6_roSection)    
    # Check if the section exists
    if (sh != const.RC_ERROR):

        # Save the old VA before ANY modifications to the section/segment
        oldVA = sh.sh_addr
        print "oldVA = " + hex(oldVA)
        print "Size of " + q6_roSection + " is " + str(sh.sh_size)
        symbolDict["start_va_uncompressed_text"] = sh.sh_addr
        symbolDict["end_va_uncompressed_text"] = sh.sh_addr + sh.sh_size

        # Move up the vaddr of the section, corresponding segment and mark all
        # its symbols as SHN_UNDEF. Will fail if q6_roSection is part of multi-
        # section segment in the ELF.
        # Save the new VA after moving up the section/segment
        print "Moving up " + q6_roSection + " to closest segment"
        elf.moveupSectionByName(q6_roSection)
        newVA = sh.sh_addr
        print "newVA = " + hex(newVA)

        print "Compress " + q6_roSection
        import q6zip_compress
        sh.contents = q6zip_compress.compress(pageSize, newVA, sh.contents)

        # Alignment needed for Q6ZIP RW in the next section
        alignedSize = pageSize * (len(sh.contents)/pageSize+1)
        for i in xrange(alignedSize - len(sh.contents)):
            sh.contents += '\0'
        elf.resizeSectionByName(q6_roSection, alignedSize)
        symbolDict["start_va_compressed_text"] = newVA
        symbolDict["end_va_compressed_text"] = sh.sh_addr + sh.sh_size

        # Change permissions (remove execute)
        print "\tRemoving X from " + q6_roSection
        sh.sh_flags = sh.sh_flags & ~const.sectionFlags.SHF_EXECINSTR
        ph = elf.getProgramHeaderBySectionName(q6_roSection)
        if ph != const.RC_ERROR:
            ph.p_flags &= ~const.segmentFlags.PF_X
        else:
            utils.raiseElfManipulatorError("Unexpected error while changing permissions for " + q6_roSection)
            

        if (sh != const.RC_ERROR):
            print "Success compressing " + q6_roSection
        else:
            utils.raiseElfManipulatorError("Failed to compress " + q6_roSection)
    else:
        # Like ro_fatal, need to check for zero-sized segment
        print "No " + q6_roSection + " section found, checking for zero-sized segment"
        if ((elf.programHeaderTable[-1].p_memsz == 0) and
                (elf.programHeaderTable[-1].p_vaddr == 0)):
                print "Removing zero-sized segment"
                elf.elfHeader.e_phnum -= 1
                elf.programHeaderTable.pop()

    print
    print "Q6ZIP FEATURE (RW)"
    # Get the ELF section containing the code to compress
    sh = elf.getSectionByName(q6_rwSection)    
    # Check if the section exists
    if (sh != const.RC_ERROR):
        print "\t'" + q6_rwSection + " section found"
        # Save the old VA before ANY modifications to the section/segment
        oldSize = sh.sh_size
        bss_common_start = elf.getSymbolByName("__swapped_segments_bss_start__").st_value
        print "\bss_common_start = " + hex(bss_common_start)
        symbolDict["start_va_uncompressed_rw"] = sh.sh_addr
        symbolDict["end_va_uncompressed_rw"] = sh.sh_addr + sh.sh_size
        rw_contents =  bss_common_start - sh.sh_addr
        print "\t rw_contents = " + hex(rw_contents)

        # Move up the section
        print "\tMoving up " + q6_rwSection + " to closest section"
        elf.moveupSectionByName(q6_rwSection)
        newVA = sh.sh_addr

        print "\tCompressing " + q6_rwSection + " contents using q6 compressor"
        import rw_py_compress
        sh.contents = rw_py_compress.rw_py_compress(pageSize, newVA, sh.contents[0:rw_contents])
        #pad with 0s till pageSize
        rem = len(sh.contents) % pageSize
        if rem != 0:
            sh.contents = sh.contents.ljust(len(sh.contents) + pageSize - rem,'0')

        # Resize the section 
        elf.resizeSectionByName(q6_rwSection, len(sh.contents))

        symbolDict["start_va_compressed_rw"] = newVA
        symbolDict["end_va_compressed_rw"] = sh.sh_addr + sh.sh_size

        # Change permissions (remove execute)
        print "\tRemoving X from " + q6_rwSection
        sh.sh_flags = sh.sh_flags & ~const.sectionFlags.SHF_EXECINSTR
        ph = elf.getProgramHeaderBySectionName(q6_rwSection)
        if ph != const.RC_ERROR:
            ph.p_flags &= ~const.segmentFlags.PF_X
        else:
            utils.raiseElfManipulatorError("Unexpected error while changing permissions for " + q6_rwSection)

        # Print out statistics
        print "\tOld Size: " + str(oldSize)
        print "\tNew Size: " + str(sh.sh_size)
    else:
        # Like ro_fatal, need to check for zero-sized segment
        print "\tNo " + q6_rwSection + " section found, checking for zero-sized segment"
        if ((elf.programHeaderTable[-1].p_memsz == 0) and
                (elf.programHeaderTable[-1].p_vaddr == 0)):
                print "\tRemoving zero-sized segment"
                elf.elfHeader.e_phnum -= 1
                elf.programHeaderTable.pop()

    #=============================================
    # END: Q6_ZIP CHANGES 
    #=============================================

    # -M option: Remove BSS section for 'overlay_mem_dump' feature
    if options.overlay_mem_dump and overlay_mem_dumpSize >= 0:
        # Need to check that the overlay_memdump does not exceed ro_fatal +
        # candidate_compress_section AFTER manipulations.
        roFatalSh = elf.getSectionByName("ro_fatal")
        roCompressSh = elf.getSectionByName(q6_roSection)
        rwCompressSh = elf.getSectionByName(q6_rwSection)

        roFatalSz = 0
        roCompressSz = 0
        rwCompressSz = 0

        if roFatalSh != const.RC_ERROR:
            roFatalSz = roFatalSh.sh_size

        if roCompressSh != const.RC_ERROR:
            roCompressSz = roCompressSh.sh_size

        if rwCompressSh != const.RC_ERROR:
            rwCompressSz = rwCompressSh.sh_size

        if overlay_mem_dumpSize > (roFatalSz + roCompressSz + rwCompressSz):
            print "FATAL ERROR: memdump exceeds ro_fatal + ro/rw candidate_compress_sections."
            print "ro_fatal size: %s bytes" % str(roFatalSz)
            print "ro candidate_compress_section size: %s bytes" % str(roCompressSz)
            print "rw candidate_compress_section size: %s bytes" % str(rwCompressSz)
            print "FW memdump overlay size: %s bytes" % str(overlay_mem_dumpSize)
            print "Short by: %s bytes" % ((roFatalSz + roCompressSz + rwCompressSz) -
                                          overlay_mem_dumpSize)
            print "Aborting"
            exit(const.RC_ERROR)

    # After all modifications, update image_vend pointing to the end of the
    # last program segment, aligned using integer division
    lastPh = elf.programHeaderTable[-1]
    for ph in elf.programHeaderTable:
        if ph.p_vaddr > lastPh.p_vaddr:
            lastPh = ph
    endAddress = lastPh.p_vaddr + lastPh.p_align * (lastPh.p_memsz/lastPh.p_align+1)
    symbolDict["image_vend"] = endAddress

    # Update all symbols
    if symbolDict:
        print "----------------------------------------------------------------"
        print " Updating symbol values from all ELF modifications..."
        print "----------------------------------------------------------------"
        elf.updateSymbolValuesByDict(symbolDict)

    # If verification is enabled, enable verification on
    if options.verification:
        print "----------------------------------------------------------------"
        print " Verifying modified ELF data..."
        print "----------------------------------------------------------------"
        elf.verify()

    # Write out the ELF file based on the elfFile object
    if not options.debug:
        print "----------------------------------------------------------------"
        print " Writing out modified ELF..."
        print "----------------------------------------------------------------"
    elf.writeOutELF(modifiedELF)

    # Record the starting time
    if options.timing:
        print ("Execution time: %.2f seconds" % (time.time() - start_time))

    # elfManipulator ran to completed, exit with return code 0
    exit(const.RC_SUCCESS)

if __name__ == "__main__":
    main()

# vim: set ts=4 sw=4 et ai nosi:
