#!/usr/bin/env python
'''mc.py - memory checker

Give an estimate of the memory consumed by the MPSS image stored in an ELF

Designed to work with Q6 ELF files.
'''

# imports {{{
import os
import re
import sys
import struct
import inspect
import subprocess

file_path = inspect.getframeinfo(inspect.currentframe()).filename
file_dir, file_name = os.path.split(file_path)

# import elfManipualtor {{{
elfManipulator_path = os.path.abspath(
  os.path.join(
    file_dir,
    '..',
    'elfManipulator',
    'include'))

sys.path.insert(0, elfManipulator_path)

import elfConstants
import elfFileClass
import elfStructs
# }}}
# }}}

last_seg_alignment = 0.25 * (1 << 20)

verbose = False

candidate_compress_section_start_base = 0xd0000000

symbols = [
  'qurtos_app_heap_cfg',
  'qurtos_main_stack_size',
  'QURTK_trace_buffer_size',
]

q6zip_int_syms = [
  'q6zip_swap_pool_size',
  'q6zip_rw_swap_pool_size',
]

q6zip_abs_syms = [
  '__swapped_segments_start__',
  '__swapped_segments_bss_start__',
  '__swapped_segments_end__',
]

q6zip_symbols = q6zip_int_syms + q6zip_abs_syms

dsm_symbol_re = r'^dsm_.+_array$'


class SuperSymbolTable(dict):  # {{{
  def get(self, sym_name):  # {{{
    '''Return the first strong symbol out of symbol_list

    Override weak entries with strong ones

    Caveat: will not validate weak/strong symbols (e.g. only 2 weak symbols w/
    same name, 2 strong symbols w/ same name)'''
    strongest_symbol = None

    for i in self[sym_name]:
      if not strongest_symbol:
        strongest_symbol = i

      elif not i.isWeak():
        strongest_symbol = i
        break

    return strongest_symbol
  # }}}

  def get_list(self, sym_list):  # {{{
    '''Return all symbols specified by sym_list. Calls get() in the
    background, ensuring that the strongest symbol is returned'''
    return dict(zip(sym_list, [self.get(sym_name) for sym_name in sym_list]))
  # }}}

  def re_get(self, re_sym):  # {{{
    '''Return the strongest symbol matching re_sym. Calls get() in the
    background.'''
    sym = None

    for sym_name in self.iterkeys():
      if re.search(re_sym, sym_name):
        sym = self.get(sym_name)
        break

    return sym
  # }}}

  def re_get_list(self, re_sym_list):  # {{{
    '''Return all symbols specified by re_sym_list. Calls re_get() in the
    background.'''
    return [self.re_get(re_sym) for re_sym in re_sym_list]

  def re_get_all(self, re_sym):  # {{{
    '''Return all symbols matching re_sym. Calls get() in the background.'''
    matching_syms = {}

    for sym_name in self.iterkeys():
      if re.search(re_sym, sym_name):
        matching_syms[sym_name] = self.get(sym_name)

    return matching_syms
  # }}}
# }}}


def dp(string):  # {{{
  if verbose:
    sys.stderr.write('%s\n' % string)
# }}}


def call(args,  # {{{
         input=None,
         merge_stderr=False,
         cwd=None,
         env=None):
  '''Execute an external command

Optionally pass input into its stdin.

Optionally merge stderr into stdout.

Optionally specify a different cwd and environment.

Return its return code, stdout, and stderr.
'''
  # http://docs.python.org/2/library/subprocess.html#popen-constructor
  if not merge_stderr:
    process = subprocess.Popen(args, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               cwd=cwd, env=env, universal_newlines=True)

  else:
    process = subprocess.Popen(args, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               cwd=cwd, env=env, universal_newlines=True)

# http://docs.python.org/2/library/subprocess.html#subprocess.Popen.communicate
  (stdoutdata, stderrdata) = process.communicate(input=input)

  return (process.returncode, stdoutdata, stderrdata)
# }}}


def in_range(outer_start, outer_size, inner_start, inner_size):  # {{{
  return (
    outer_start <= inner_start and
    outer_start + outer_size >= inner_start + inner_size)
# }}}


def get_full_symbol_table(elf):  # {{{
  symtab = elf.getSectionByName('.symtab')
  strtab = elf.getSectionByName('.strtab')

  symbol_table = []

  if (symtab != elfConstants.RC_ERROR and
          strtab != elfConstants.RC_ERROR):
    assert len(symtab.contents) % symtab.sh_entsize == 0

    for i in xrange(0, len(symtab.contents), symtab.sh_entsize):
      symbol_table.append(
        elfStructs.Elf32_Sym(
          symtab.contents[i:i + symtab.sh_entsize],
          strtab))

  return symbol_table  # list
# }}}


def get_reduced_symbol_table(elf):  # {{{
  symbol_table = get_full_symbol_table(elf)
  symbol_table = [i for i in symbol_table if i.st_nameStr != '']

  reduced_symbol_table = {}

  for i in symbol_table:
    if i.st_nameStr not in reduced_symbol_table:
      reduced_symbol_table[i.st_nameStr] = []

    reduced_symbol_table[i.st_nameStr].append(i)

  return SuperSymbolTable(reduced_symbol_table)  # dict, -ish
# }}}


def get_raw_symbol_value(elf, symbol):  # {{{
  section_offset = (
    symbol.st_value - elf.sectionHeaderTable[symbol.st_shndx].sh_addr)

  return elf.sectionHeaderTable[symbol.st_shndx].contents[
    section_offset:
    section_offset + symbol.st_size]
# }}}


def get_little_endian_uint_sym(elf, symbol, default_value=-1):  # {{{
  if not symbol:
    return default_value

  symbol_value = get_raw_symbol_value(elf, symbol)

  assert len(symbol_value) == 4, ''.join([
    'Symbol %s cannot be interpreted as a uint' % symbol.st_nameStr])

  return struct.unpack('<I', symbol_value)[0]
# }}}


def align(number, alignment):  # {{{
  return (number + int(alignment) - 1) & ~(int(alignment) - 1)
# }}}


def meg(number):  # {{{
  return float(number) / float(1 << 20)
# }}}


def print_err(me, string):  # {{{
  sys.stderr.write('Error: %s\n' % string)
  sys.stderr.write(' '.join([
    'Usage: python %s [--hexagon-path <path_to_hexagon_tools>]' % me,
    '<elf_path>\n']))
  sys.stderr.write(' '.join([
    '\n  e.g. python %s --hexagon-path C:\\Qualcomm\\HEXAGON_Tools\\5.1' % me,
    'D:\\builds\\M8x10BAAAANWAQ201042.elf\n']))
  return 1
# }}}


def get_image_and_pad_size(elf):  # {{{
  image_size = 0
  pad = 0
  seg_overlay = 0
  alignment_pad = 0
  jumped_to_q6zip_segment = False

  # clone the program header table, so we can manipulate it
  ph_table = list(elf.programHeaderTable)

  # only process loadable segments
  ph_table = [i for i in ph_table if i.p_type ==
              elfConstants.segmentTypes.PT_LOAD]

  # only process non-zero segments
  ph_table = [i for i in ph_table if i.p_memsz > 0]

  # sort by increasing address size
  ph_table.sort(key=lambda i: i.p_paddr)

  # drop the last segment if its file size is 0
  # (assume that it's the dummy segment)
  if ph_table[-1].p_filesz == 0:
    ph_table.pop()

  prev_start = -1
  prev_size = -1

  for ph in ph_table:
    start = ph.p_paddr
    size = ph.p_memsz

    if prev_start > 0 and prev_size > 0:
      potential_pad = start - (prev_start + prev_size)

      if potential_pad >= 0:
        if (start >= candidate_compress_section_start_base and not
                jumped_to_q6zip_segment):
          # ignore the mega jump to .candidate_compress_section
          jumped_to_q6zip_segment = True

        else:
          dp(''.join([
            'Padding of 0x%08x between segments 0x%08x and 0x%08x' % (
              potential_pad,
              prev_start,
              start)]))
          pad += potential_pad

      else:
        seg_overlay += potential_pad

    image_size += size

    prev_start = start
    prev_size = size

  assert prev_start == start, 'Uh oh #1'
  assert prev_size == size, 'Uh oh #2'

  # we have to account for the last segment
  aligned_ending_address = align(prev_start + prev_size, last_seg_alignment)
  alignment_pad = aligned_ending_address - (prev_start + prev_size)
  dp('Alignment padding of 0x%08x added' % alignment_pad)

  return image_size, pad, seg_overlay, alignment_pad
# }}}


def calc_q6zip_swap_pool(elf, sym_dict):  # {{{
  # for the logic behind this, see CR 695530
  return_vals = {
    'size': 0,
    'type': 'dynamic, disabled/not found'}

  defaults = {
    'q6zip_swap_pool_size': 2 * (1 << 20),  # 2 MiB
    'q6zip_rw_swap_pool_size': 1 * (1 << 20)}  # 1 MiB

  q6zip_values = dict(zip(q6zip_symbols, [0] * len(q6zip_symbols)))

  for n in q6zip_int_syms:
    if sym_dict[n].st_value > 0:
      # symbol exists, store its size
      q6zip_values[n] = get_little_endian_uint_sym(elf, sym_dict[n])

    if q6zip_values[n] == 0 and n in defaults:
      # apply default values if we have 0 as the size
      q6zip_values[n] = defaults[n]

  for n in q6zip_abs_syms:
     q6zip_values[n] = sym_dict[n].st_value

  if (q6zip_values['__swapped_segments_start__'] ==
          q6zip_values['__swapped_segments_end__']):
    # q6zip compression disabled, no-op
    pass

  else:
    # q6zip compression enabled, add RO swap pool size
    return_vals['type'] = 'dynamic'
    return_vals['size'] += q6zip_values['q6zip_swap_pool_size']

    if (q6zip_values['__swapped_segments_bss_start__'] == 0 or
        (q6zip_values['__swapped_segments_bss_start__'] ==
         q6zip_values['__swapped_segments_end__'])):
      # q6zip RW compression disabled, no-op
      pass

    else:
      # q6zip RW compression enabled, add RW swap pool size
      return_vals['size'] += q6zip_values['q6zip_rw_swap_pool_size']

  return return_vals
# }}}


def get_sym_sizes(elf, symbol_table, hexagon_path=None):  # {{{
  syms = {}
  sym_sizes = {}
  sym_list = symbols + q6zip_symbols

  for i in sym_list:
    try:
      syms[i] = symbol_table.get(i)

    except KeyError:
      if i in q6zip_symbols:
        # any combination of these symbols may not be present, don't break if
        # we don't find them
        if len(syms) < 1:
          raise Exception('No other symbols to create a copy from')

        # avoid __init__ by calling __new__
        syms[i] = object.__new__(elfStructs.Elf32_Sym)

        # populate it with default values
        syms[i].__dict__ = dict(
          syms[i].__dict__.items() +
          {
            'st_name': 0,
            'st_value': 0,
            'st_size': 0,
            'st_info': elfConstants.symbolTypes.STT_NOTYPE,
            'st_other': elfConstants.symVisibility.STV_DEFAULT,
            'st_shndx': elfConstants.specialSectionsIndexes.SHN_ABS,
            'st_nameStr': '',
          }.items()
        )

      else:
        sys.stderr.write('Error: could not find a crucial ELF symbol\n')
        raise

  # modem_proc/qdsp6/*/qurt_config.c:
  # struct config_heap_info {
  #    enum config_heap_type type;
  #    void *vaddr;
  #    unsigned size;
  # };
  amss_heap_cfg = get_raw_symbol_value(elf, syms['qurtos_app_heap_cfg'])

  # enum size may be chosen by the compiler
  # (http://stackoverflow.com/a/366026), so let's work backwards
  #
  # use I for void * vaddr, since P is unavailable for non-native byte ordering
  # use I for unsigned size
  enum_size = (syms['qurtos_app_heap_cfg'].st_size -
               struct.calcsize('II'))

  s = struct.Struct(''.join(['<', str(enum_size), 'BII']))

  unpacked_data = list(s.unpack(amss_heap_cfg))

  sym_sizes['amss_heap'] = {
    'start': unpacked_data[-2],
    'size': unpacked_data[-1]}

  for dsm_sym_name, sym in symbol_table.re_get_all(dsm_symbol_re).iteritems():
    sym_sizes[dsm_sym_name] = {
      'start': sym.st_value,
      'size': sym.st_size}

  sym_sizes['kernel_stack'] = {
    'size': get_little_endian_uint_sym(elf, syms['qurtos_main_stack_size'])}

  sym_sizes['trace_buffer'] = {
    'size': get_little_endian_uint_sym(elf, syms['QURTK_trace_buffer_size'])}

  sym_sizes['q6zip_swap_pool'] = calc_q6zip_swap_pool(elf, syms)

  assert 'DL_SWAP_POOL0_SIZE' not in symbol_table, 'Unhandled case: swap pool'

  # MPSS heap
  # The MPSS heap can be statically or dynamically allocated
  #
  # In static allocation, the symbol is called heap_buffer. The compiler may
  # change the symbol name into these permutations:
  # - modem_mem_init_heap.heap_buffer
  # - r'heap_buffer\.\d+'
  #
  # If that's the case we can simply search the symbol table for the address
  # and the size of the heap buffer
  #
  # In the case of dynamic allocation, a request was placed to store the size
  # of the heap into a symbol called modem_mem_heap_size. Search the symbol
  # table for that symbol, and use its value if found.
  #
  # If we dynamically allocate and modem_mem_heap_size can't be found, we have
  # to disassemble the ELF to look at how much we malloc for the MPSS heap.
  mpss_heap_symbols = symbol_table.re_get_list(
    [r'^modem_mem_init_heap\.heap_buffer$',
     r'^heap_buffer\.[0-9]+$'])

  # clear blank entries
  mpss_heap_symbols = [i for i in mpss_heap_symbols if i]

  if len(mpss_heap_symbols) > 1:
    raise Exception(
      'Multiple MPSS heap symbols found: %r' % (mpss_heap_symbols,))

  if len(mpss_heap_symbols) > 0:
    sym_sizes['mpss_heap'] = {
      'start': mpss_heap_symbols[0].st_value,
      'size': mpss_heap_symbols[0].st_size,
      'found': True}

  elif 'modem_mem_heap_size' in symbol_table:
    sym_sizes['mpss_heap'] = {
      'start': -1,
      'size': get_little_endian_uint_sym(
        elf,
        symbol_table.get('modem_mem_heap_size')),
      'found': True}

  else:
    sym_sizes['mpss_heap'] = disassemble_mpss_heap_size(
      elf,
      symbol_table,
      hexagon_path=hexagon_path)

  return sym_sizes
# }}}


def disassemble_mpss_heap_size(elf, symbol_table, hexagon_path=None):  # {{{
  values = {
    'found': False}

  elf_path = elf.elf

  if not hexagon_path:
    hexagon_paths = [
      r'\\QCTDFSRT\qctasw\Hexagon',
      '/pkg/qct/software/hexagon/releases/tools']

    hexagon_path = None

    # prerequisite checks {{{
    for path in hexagon_paths:
      if os.path.exists(path):
        hexagon_path = path
        break

    if not hexagon_path:
      # can't find hexagon tools, abort
      return values
    # }}}

    # get the highest numerical hexagon version; assume that it will work
    hexagon_versions = [i for i in os.listdir(hexagon_path)
                        if re.search(r'^[^.][0-9.]+$', i)]

    # versions 7 and up use hexagon-llvm-objdump, which doesn't support
    # --start-address and --stop-address; trim those
    hexagon_versions = [i for i in hexagon_versions
                        if int(i[0:i.find('.')], 10) < 7]

    hexagon_version = sorted(hexagon_versions)[-1]

    hexagon_path = os.path.join(hexagon_path, hexagon_version)

    hexagon_version = None
    hexagon_versions = None
    del hexagon_version
    del hexagon_versions

  functions_to_disassemble = (
    'modem_mem_init_heap',
    'modem_mem_check_heap_initialized')

  function = None

  for i in functions_to_disassemble:
    try:
      function = {
        'name': symbol_table.get(i).st_nameStr,
        'start': symbol_table.get(i).st_value,
        'end': symbol_table.get(i).st_value + symbol_table.get(i).st_size}

      break

    except KeyError:
      continue

  if not function:
    # don't know what function to disassemble, abort
    return values

  # we can allow hexagon_path to point to objdump itself, or the whole set of
  # GNU tools
  objdump_paths = (
    hexagon_path,
    os.path.join(hexagon_path, 'Tools', 'bin', 'hexagon-objdump'),
    os.path.join(hexagon_path, 'gnu', 'bin', 'hexagon-objdump'),
    'hexagon-objdump')

  objdump_path = None

  for path in objdump_paths:
    try:
      call([path])

      objdump_path = path
      break

    except OSError:
      continue

  if not objdump_path:
    # can't find hexagon-objdump, abort
    return values

  return_code, stdout, _ = call(
    [
      objdump_path,
      '-d',
      '--start-address=0x%08x' % function['start'],
      '--stop-address=0x%08x' % function['end'],
      elf_path],
    merge_stderr=True)

  if return_code != 0:
    # hexagon-objdump barfed, abort
    return values

  stdout = stdout.splitlines()

  alloc_line = -1

  for i, line in enumerate(stdout):
    if 'call' in line and 'mem_init_heap_alloc' in line:
      alloc_line = i
      break

  if alloc_line < 0:
    # the call to mem_init_heap_alloc doesn't exist, abort
    return values

  # work backwards from the designated line until we find r1 being set
  for line in reversed(stdout[0:alloc_line]):
    if re.search(r'\br1\b', line):
      # Looking for:
      #  974876c: 02 28 01 28   28012802       r1 = ##4194304 ; r2 = #0 }
      #
      # If the absolute number (##) isn't there, we'll need more advanced
      # logic to reconstruct r1. Save that for another time.
      m = re.search(r'r1 = ##([0-9]+)', line)

      if m:
        values = {
          'start': 0,
          'size': int(m.groups()[0]),
          'found': True}

      break

  return values
# }}}


def get_modem_budget(elf, symbol_table):  # {{{
  modem_budget = -1

  pool_configs = symbol_table.get('pool_configs')

  assert pool_configs, 'Could not find pool configuration information'

  pool_configs_content = get_raw_symbol_value(elf, pool_configs)

  # struct definition of pool_configs: struct phys_mem_pool_config
  # modem_proc/core/kernel/qurt/install/.../cust_config.c
  s = struct.Struct('<32c' + 'II' * 16)
  config_entry_size = s.size

  for i in xrange(0, pool_configs.st_size, config_entry_size):
    unpacked_data = list(
      s.unpack(
        pool_configs_content[
          i:
          i + config_entry_size]))

    pool_entry_name = ''.join(unpacked_data[0:32]).split('\x00')[0]
    pool_entry_addr_pairs = unpacked_data[32:]

    if pool_entry_name == 'DEFAULT_PHYSPOOL':
      # get even/odd entries of a list:
      # http://stackoverflow.com/a/11702449
      # see also: http://docs.python.org/2/whatsnew/2.3.html#extended-slices
      modem_budget = sum(pool_entry_addr_pairs[1::2])
      break

  return modem_budget
# }}}


def mc_format(name, value):  # {{{
  return '%-22s %6s MiB' % (
    '%s:' % name,
    '%.2f' % meg(value))
# }}}


def distribute_sizes(dest_dict, src_dict):  # {{{
  '''Apply a transform on src_dict, storing the results into dest_dict'''
  src_dict_copy = dict(src_dict)

  for src_key in src_dict_copy.keys():
    for dest_key in dest_dict.keys():
      if dest_key in src_key:
        if 'size' in src_dict_copy[src_key]:
          dest_dict[dest_key] += src_dict_copy[src_key]['size']

        del src_dict_copy[src_key]
        break

  # add up everything we missed into 'other'
  for src_key, src_value in src_dict_copy.iteritems():
    if 'size' in src_value:
      dest_dict['other'] += src_value['size']
# }}}


def main(argv):  # {{{
  hexagon_path = None
  elf_path = None

  i = 0
  while i < len(argv):
    arg = argv[i]

    if arg.lower() == '--hexagon-path':
      if not (i + 1 < len(argv)):
        return print_err(file_name, 'Expecting argument after --hexagon-path')

      hexagon_path = argv[i + 1]

      i += 2
      continue

    elif arg.lower().startswith('--hexagon-path='):
      path = '='.join(arg.split('=')[1:])

      if len(path) < 1:
        return print_err(file_name, 'Expecting argument after --hexagon-path=')

      hexagon_path = path

      i += 1
      continue

    elif arg.lower() == '-v' or arg.lower() == '--verbose':
      global verbose
      verbose = True

    else:
      elf_path = arg

    i += 1

  if not elf_path:
    return print_err(file_name, 'No ELF path given')

  if not os.path.exists(elf_path):
    return print_err(file_name, 'ELF does not exist: %s' % elf_path)

  # get elf data
  elf = elfFileClass.elfFile(elf_path)
  symbol_table = get_reduced_symbol_table(elf)

  # turn that into something useful
  image_size, pad, seg_overlay, alignment_pad = get_image_and_pad_size(elf)
  sym_sizes = get_sym_sizes(elf, symbol_table, hexagon_path=hexagon_path)
  start_address = symbol_table.get('start').st_value
  modem_budget = get_modem_budget(elf, symbol_table)
  # TODO: do something with qurtos_mmap_table

  # reduce the image size for statically allocated items {{{
  if in_range(start_address,
              modem_budget,
              sym_sizes['amss_heap']['start'],
              sym_sizes['amss_heap']['size']):
    image_size -= sym_sizes['amss_heap']['size']
    sym_sizes['amss_heap']['type'] = 'static'

  else:
    sym_sizes['amss_heap']['type'] = 'dynamic'

  if ('mpss_heap' in sym_sizes and
      'start' in sym_sizes['mpss_heap'] and
          'size' in sym_sizes['mpss_heap']):
    if in_range(start_address,
                modem_budget,
                sym_sizes['mpss_heap']['start'],
                sym_sizes['mpss_heap']['size']):
      image_size -= sym_sizes['mpss_heap']['size']

      sym_sizes['mpss_heap']['type'] = 'static'

    else:
      sym_sizes['mpss_heap']['type'] = 'dynamic'

  else:
    sym_sizes['mpss_heap']['type'] = 'dynamic; not found'

  # could probably avoid a second re_get_all if we saved the results from the
  # first one in get_sym_sizes()
  for dsm_sym_name in symbol_table.re_get_all(dsm_symbol_re).iterkeys():
    if in_range(start_address,
                modem_budget,
                sym_sizes[dsm_sym_name]['start'],
                sym_sizes[dsm_sym_name]['size']):
      image_size -= sym_sizes[dsm_sym_name]['size']

  # break down the supplementary sizes
  sizes = {
    'amss_heap': 0,
    'mpss_heap': 0,
    'dsm': 0,
    'kernel_stack': 0,
    'trace_buffer': 0,
    'q6zip_swap_pool': 0,
    'other': 0}

  distribute_sizes(sizes, sym_sizes)

  extra_size = 400 * (1 << 10)  # unknown; originally called 'QRegionReq'
  extra_size += sizes['trace_buffer']
  extra_size += sizes['other']

  del sizes['trace_buffer']
  del sizes['other']

  total_size = sum(sizes.values() + [
    image_size,
    extra_size,
    pad,
    seg_overlay,
    alignment_pad])

  available_size = modem_budget - total_size

  dp('')  # separate verbose data from regular data

  # construct what would be printed
  to_print = [
    ('Image', image_size),
    ('AMSS Heap', sizes['amss_heap'], sym_sizes['amss_heap']['type']),
    ('MPSS Heap', sizes['mpss_heap'], sym_sizes['mpss_heap']['type']),
    ('DSM Pools', sizes['dsm']),
    ('Q6Zip Swap Pool', sizes['q6zip_swap_pool'],
     sym_sizes['q6zip_swap_pool']['type']),
    ('Kernel Stack', sizes['kernel_stack']),
    ('Extra', extra_size),
    ('Padding', pad),
    ('Segment Overlay', seg_overlay),
    ('End Address Alignment', alignment_pad),
    ('Total', total_size),
    ('Available', available_size),
  ]

  print 'Image loaded in region 0x%08x' % start_address

  for item in to_print:
    try:
      name, size, extra = item

    except ValueError:
      name, size = item
      extra = None

    if ('%.2f' % (meg(size),)) != '0.00':
      # we have something to display
      if name == 'Total':
        # extra line for total
        print

      print mc_format(name, size),

      if extra:
        print '(%s)' % (extra,),

      print

  if ('mpss_heap' in sym_sizes and
          not sym_sizes['mpss_heap']['found']):
    print ''
    print ''.join([
      'Note: Could not find MPSS heap info, so displaying it as 0'])

  if available_size < 0:
    sys.stderr.write(''.join([
      'Error: region ',
      '0x%08x' % start_address,
      ', size ',
      '%.2f' % meg(modem_budget),
      ' MiB is too small (',
      '%i' % available_size,
      ' bytes)\n']))

    return 1

  return 0
# }}}

if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))

# vim: set ts=2 sw=2 et ai nosi fen fdm=marker:
