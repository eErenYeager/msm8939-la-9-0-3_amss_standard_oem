#!/usr/bin/env python
# vim: set ts=2 sts=0 sw=2 et fen fdm=marker:

'''Post-process link command. Call post-link optimization routines.

This script's original purpose was to selectively relocate object files into a
hot zone within the modem image, exploiting the hot zone's locality to limit
the amount of veneers (long jumps, trampolines) within the neighbouring 8 MiB.

Over time it has evolved as a starting point to call other optimization
routines, losing its original purpose in the process.

Current post-link operations include:
  - ERR_FATAL zlib compression
  - code and data q6zip compression
  - firmware overlay section tweaks
  - QShrink (F3 message hashing and externalization)
  - TLB MMU generation
  - default physpool reduction
  - memory usage summary
  - memory report generation

Copyright (c) 2014 by QUALCOMM Technologies, Inc. All Rights Reserved.
'''

__author__ = ('adambre', 'alexh')

# imports {{{
import os
import re
import sys
import time
import base64
import shutil
import inspect
import threading
import subprocess

file_path = inspect.getframeinfo(inspect.currentframe()).filename
file_dir, file_name = os.path.split(file_path)

# import elfManipualtor {{{
elfManipulator_path = os.path.abspath(
  os.path.join(
    file_dir,
    '..',
    'elfManipulator',
    'include'))

sys.path.insert(0, elfManipulator_path)

import elfFileClass
# }}}
# }}}

debug = False

# 'private' globals  {{{
log_version = 2

if sys.platform.lower().startswith('win'):
  timer = time.clock

else:
  timer = time.time
# }}}


# helper functions {{{
def ap(path):
  return os.path.abspath(path)


def pj(*path):
  return os.path.join(*path)


def ps(path):
  return os.path.split(path)


def t_str(seconds):  # {{{
  hours = int(seconds / 3600)
  minutes = int((seconds % 3600) / 60)
  seconds = seconds % 60

  s = []

  if hours > 0:
    if hours == 1:
      s.append('%d hour' % hours)

    else:
      s.append('%d hours' % hours)

  if minutes > 0:
    if minutes == 1:
      s.append('%d minute' % minutes)

    else:
      s.append('%d minutes' % minutes)

  if seconds > 0.0:
    if seconds == 1.0:
      s.append('%.2f second' % seconds)

    else:
      s.append('%.2f seconds' % seconds)

  return ' '.join(s)
# }}}
# }}}


class Pplk(object):  # {{{
  '''Class to wrap all of our goodies around'''
  def __init__(self, target, source, env):  # {{{
    '''Take everything we need from 'env' and prep it for our subroutines.

    Note: to find env items:
    print [i for i in env._dict.keys() if 'search_string' in i.lower()]
    '''
    self.elfpath = ap(str(source[0]))
    self.outpath = ap(ps(self.elfpath)[0])
    self.buildms = ap(env.subst('${BUILD_MS_ROOT}'))
    self.memreport = (env.subst('${MEMREPORT}') == '1' or
                      ('USES_INTERNAL_BUILD' in env and
                       # last dir of $TARGET_ROOT is 'b'
                       ps(env.subst('${TARGET_ROOT}'))[-1] == 'b'))
    self.buildroot = env.subst('${TARGET_ROOT}')
    self.objcopy = env.subst('${OBJCOPY}')
    self.threads = []
    self.log_handle = None
    self.python = env.subst('${PYTHONCMD}')
    self.short_buildpath = env.subst('${SHORT_BUILDPATH}')
    self.cust_config = env.FindConfigFiles('cust_config.xml')[0]
    self.core_root = env.subst('${COREBSP_ROOT}')

    self.mypspath = ap(
      pj(
        ps(inspect.getfile(inspect.currentframe()))[0],
        '..'))

    if env.subst('${FAKE_ENV}') == '1':
      self.replay = True

    else:
      self.replay = False

    self.log('pplkcmd log version: %s' % str(log_version))
    self.log('pplkcmd __dict__: %s' % base64.b64encode(repr(self.__dict__)))
  # }}}

  def p(self, string=''):  # {{{
    if not self.log_handle:
      self.log_handle = open(
        ap(pj(self.buildms, 'pplk-%s.log' % self.short_buildpath)),
        'wb')

    self.log_handle.write('%s\n' % string)
    print string
  # }}}

  def dp(self, string=''):  # {{{
    if debug:
      if not self.log_handle:
        self.log_handle = open(
          ap(pj(self.buildms, 'pplk-%s.log' % self.short_buildpath)),
          'wb')

      self.log_handle.write('Debug: %s\n' % string)
      sys.stderr.write('Debug: %s\n' % string)
  # }}}

  def log(self, string=''):  # {{{
    if not self.log_handle:
      self.log_handle = open(
        ap(pj(self.buildms, 'pplk-%s.log' % self.short_buildpath)),
        'wb')

    self.log_handle.write('%s\n' % string)
  # }}}

  def call(self,  # {{{
           args,
           input=None,
           merge_stderr=False,
           cwd=None,
           env=None):
    '''Execute an external command

Optionally pass input into its stdin.

Optionally merge stderr into stdout.

Optionally specify a different cwd and environment.

Return its return code, stdout, and stderr.
'''
    self.dp('call(): %s' % repr(args))

    # http://docs.python.org/2/library/subprocess.html#popen-constructor
    if not merge_stderr:
      process = subprocess.Popen(args, stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 cwd=cwd, env=env, universal_newlines=True)

    else:
      process = subprocess.Popen(args, stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT,
                                 cwd=cwd, env=env, universal_newlines=True)

# http://docs.python.org/2/library/subprocess.html#subprocess.Popen.communicate
    (stdoutdata, stderrdata) = process.communicate(input=input)

    return (process.returncode, stdoutdata, stderrdata)
  # }}}

  def parallel_call(self, function, parameter, chunksize=-1):  # {{{
    '''Perform a parallel call

Call function once per parameter, in parallel.

The smaller the chunksize, the more things will be processed in parallel.'''
    if not debug:
      from multiprocessing import Pool

# http://docs.python.org/2/library/multiprocessing.html#using-a-pool-of-workers
      pool = Pool(processes=len(parameter))

      if chunksize < 0:
        # by default, split it into 2 chunks
        n_chunks = 2

        chunksize = (len(parameter) + n_chunks - 1) / n_chunks

      # multiprocessing with ctrl+c: http://stackoverflow.com/a/1408476
      results = pool.map_async(function, parameter, chunksize).get(1e100)

    else:
      results = [function(i) for i in parameter]

    return results
  # }}}

  def test_executable(self, exe_name):  # {{{
    ok = True

    try:
      self.call([exe_name])

    except OSError:
      ok = False

    return ok
  # }}}

  def gen_memreport(self):  # {{{
    '''Call mpssmem.py and QMAT'''
    t1 = timer()
    base_outdir = ap(pj(self.buildms, 'reports'))

    self.p('\nGenerating MPSS memory reports in %s' % base_outdir)

    memreport = {
      'mpssmem': {
        'paths': [
          pj('reports', 'mpssmem-v3', 'mpssmem.py')],

        'arguments': [
          '-b',
          ap(self.buildroot),
          '-f',
          '-v',
          self.short_buildpath],

        'pyscript': True},

      'qcmat': {
        'paths': [
          pj('reports', 'QMAT', 'rel', 'QMemoryAnalyzer_unix_cmd'),
          pj('reports', 'QMAT', 'rel', 'QMemoryAnalyser_cmd.exe')],

        'arguments': [
          '-b',
          ap(self.buildroot)],

        'pyscript': False}}

    for report_tool, items in memreport.iteritems():
      self.p('  Generating the "%s" memory report' % report_tool)

      command = None

      for path in items['paths']:
        if items['pyscript']:
          potential_command = self.python

        else:
          potential_command = ap(pj(self.mypspath, path))

        if self.test_executable(potential_command):
          command = [potential_command]
          break

      if command:
        outdir = ap(pj(base_outdir, report_tool))

        if not os.path.exists(outdir):
          os.makedirs(outdir)

        if memreport[report_tool]['pyscript']:
          retcode, stdout, stderr = self.call(
            command +
            [ap(pj(self.mypspath, memreport[report_tool]['paths'][0]))] +
            memreport[report_tool]['arguments'] +
            ['-o', outdir])

        else:
          retcode, stdout, stderr = self.call(
            command +
            memreport[report_tool]['arguments'] +
            ['-o', outdir])

        self.log('memreport - %s:' % report_tool)
        self.log('return code: %s' % repr(retcode))
        self.log('stdout:\n%s' % stdout)
        self.log('stderr:\n%s' % stderr)
        self.log()

      else:
        self.p('  Could not execute the "%s" tool' % report_tool)

    t2 = timer()
    t_diff = t2 - t1

    self.p('  Memory report execution time: %s' % t_str(t_diff))
  # }}}

  def threaded(self, function, args=(), kwargs={}):  # {{{
    self.dp('Preparing a thread to execute %s' % repr(function))

    self.threads.append(
      threading.Thread(
        target=function,
        args=args,
        kwargs=kwargs))

    self.dp('Thread %s created. Starting.' % repr(self.threads[-1]))

    self.threads[-1].start()
  # }}}

  def qshrink(self, inpath, outpath):  # {{{
    t1 = timer()
    sh_table = list(elfFileClass.elfFile(inpath).sectionHeaderTable)
    retcode = 0

    if 'QSR_STRING' in [i.sh_nameStr for i in sh_table]:
      # qshrink section exists, run qshrink
      self.p('\nRunning QShrink')

      qspath = pj(self.mypspath, 'qshrink')

      retcode, stdout, stderr = self.call([
        self.python,
        '-O',
        pj(qspath, 'Qshrink20.py'),
        inpath,
        '--image_end_from_elfheader',
        ''.join(['--output=', outpath]),
        ''.join(['--hashfile=', pj(qspath, 'msg_hash.txt')]),
        ''.join(['--log=', pj(self.buildms, ''.join([
          'qshrink-',
          self.short_buildpath]))])])

      if retcode != 0:
        self.p('QShrink failed. Logs below:')
        self.p('stdout: %s' % stdout)
        self.p('stderr: %s' % stderr)

      else:
        self.log('QShrink logs:')
        self.log('stdout: %s' % stdout)
        self.log('stderr: %s' % stderr)

    else:
      self.p('Skipping QShrink: no QSR_STRING section found')

    t2 = timer()
    t_diff = t2 - t1

    self.p('  QShrink execution time: %s' % t_str(t_diff))

    return retcode
  # }}}

  def elfmanip(self, inpath, outpath):  # {{{
    t1 = timer()
    retcode = 0

    self.p('\nRunning ELF Manipulator')

    retcode, stdout, stderr = self.call([
      self.python,
      '-O',
      pj(self.mypspath, 'elfManipulator', 'elfManipulator.py'),
      '-R',
      '-M',
      '-c',
      self.core_root,
      inpath,
      outpath])

    if retcode != 0:
      self.p('ELF Manipulator failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

    else:
      self.log('ELF Manipulator logs:')
      self.log('stdout: %s' % stdout)
      self.log('stderr: %s' % stderr)

    t2 = timer()
    t_diff = t2 - t1

    self.p('  ELF Manipulator execution time: %s' % t_str(t_diff))

    return retcode
  # }}}
  
  def dynrecManip(self, inpath, outpath):
  # {{{
    t1 = timer()
    retcode = 0
    self.p('\nRunning Dynamic Reclaiming.')
    retcode, stdout, stderr = self.call([self.python,'-O', pj(self.mypspath,'elfManipulator', 'dynRec_manip.py'), inpath, outpath])
    if retcode != 0:
     self.p('failed dynRecManip')
     self.p('stdout: %s' % stdout)
     self.p('stderr: %s' % stderr)
    else:
     self.log('passed dynRecManip')
     self.log('stdout: %s' % stdout)
     self.log('stderr: %s' % stderr)
    t2 = timer()
    t_diff = t2 - t1
    self.p('  Dynamic Reclaiming execution time: %s' % t_str(t_diff))
    return retcode
  # }}}

  def tlbupdt(self, inpath, outpath):  # {{{
    t1 = timer()
    retcode = 0

    self.p('\nRunning TLB update')

    # note the trailing slash
    tempdir = pj(*[
      self.outpath,
      ''.join(['tlbupdt-', self.short_buildpath]),
      ''])

    shutil.rmtree(tempdir, ignore_errors=True)
    os.makedirs(tempdir)

    retcode, stdout, stderr = self.call([
      self.python,
      '-O',
      pj(self.mypspath, 'tools', 'tlbupdt.py'),
      inpath,
      '-t',
      tempdir,
      '-q'])

    if retcode != 0:
      self.p('TLB update failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

      return retcode

    else:
      self.log('TLB update logs:')
      self.log('stdout: %s' % stdout)
      self.log('stderr: %s' % stderr)

      locked_tlb_entries = '(unknown)'

      m = re.search(r'(\d+) locked entries', stdout)
      if m:
        locked_tlb_entries = m.groups()[0]

      self.p('  %s locked TLB entries' % locked_tlb_entries)

    _elfpath = ps(inpath)
    _elfpath = pj(_elfpath[0], ''.join(['_', _elfpath[1]]))

    shutil.move(_elfpath, outpath)

    if retcode != 0:
      self.p('Post-TLB update objcopy failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

    else:
      self.log('Post-TLB update objcopy logs:')
      self.log('stdout: %s' % stdout)
      self.log('stderr: %s' % stderr)

      shutil.rmtree(tempdir, ignore_errors=True)

    t2 = timer()
    t_diff = t2 - t1

    self.p('  TLB update execution time: %s' % t_str(t_diff))

    return retcode
  # }}}

  def patch_fw_overlay(self, inpath, outpath):  # {{{
    t1 = timer()
    retcode = 0

    self.p('\nRunning firmware overlay patch')

    shutil.copyfile(inpath, outpath)

    retcode, stdout, stderr = self.call([
      self.python,
      '-O',
      pj(self.mypspath, 'tools', 'PatchFWOverlay.py'),
      outpath])

    if retcode != 0:
      self.p('Firmware overlay patch failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

    else:
      self.log('Firmware overlay patch logs:')
      self.log('stdout: %s' % stdout)
      self.log('stderr: %s' % stderr)

    t2 = timer()
    t_diff = t2 - t1

    self.p('  Firmware overlay patch execution time: %s' % t_str(t_diff))

    return retcode
  # }}}

  def patch_default_physpool(self, inpath, outpath):  # {{{
    t1 = timer()
    retcode = 0

    self.p('\nRunning default physpool patch')

    shutil.copyfile(inpath, outpath)

    retcode, stdout, stderr = self.call([
      self.python,
      '-O',
      pj(self.mypspath, 'tools', 'patch_default_physpool.py'),
      outpath,
      self.cust_config])

    if retcode != 0:
      self.p('Default physpool patch failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

    else:
      self.log('Default physpool patch logs:')
      self.log('stdout: %s' % stdout)
      self.log('stderr: %s' % stderr)

    t2 = timer()
    t_diff = t2 - t1

    self.p('  Default physpool patch execution time: %s' % t_str(t_diff))

    return retcode
  # }}}

  def mc(self, inpath, second_pass=False):  # {{{
    t1 = timer()
    retcode = 0

    self.p('\nRunning memory checker on %s' %
           '.'.join(ps(inpath)[-1].split('.')[0:2]))

    objdump = self.objcopy
    objdump = ps(objdump)
    objdump = (objdump[0], objdump[1].lower().replace('objcopy', 'objdump'))
    objdump = pj(*objdump)

    retcode, stdout, stderr = self.call([
      self.python,
      '-O',
      pj(self.mypspath, 'tools', 'mc.py'),
      '--hexagon-path',
      objdump,
      inpath])

    if retcode != 0:
      self.p('Memory checker failed. Logs below:')
      self.p('stdout: %s' % stdout)
      self.p('stderr: %s' % stderr)

    elif not second_pass:
      self.p()
      self.p('=' * 80)
      self.p()
      self.p('%s' % stdout)
      self.p('=' * 80)
      self.p()

    t2 = timer()
    t_diff = t2 - t1

    self.p('  Memory checker execution time: %s' % t_str(t_diff))

    return retcode
  # }}}

  def wait_for_threads(self):  # {{{
    self.dp('Waiting for all threads to finish execution')
    for thread in self.threads:
      if thread.is_alive():
        thread.join()
  # }}}
# }}}


def main(target, source, env):  # {{{
  '''Success: return 0 or none; failure: return non-zero'''

  # Load needed tools
  env.LoadToolScript('build_utils', toolpath=['${BUILD_ROOT}/build/scripts'])

  t1 = timer()
  p = Pplk(target, source, env)

  p.p('Running pplkcmd on %s' % p.elfpath)

  r = p.elfmanip(p.elfpath,
                 '.'.join([p.elfpath,
                           'elfmanip']))
  if r != 0:
    return r

  r = p.qshrink('.'.join([p.elfpath,
                          'elfmanip']),
                '.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink']))
  if r != 0:
    return r
  r = p.dynrecManip('.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink']),
                '.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink']))
  if r != 0:
    return r

  r = p.tlbupdt('.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink']),
                '.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink',
                          'tlbupdt']))
  if r != 0:
    return r

  r = p.patch_fw_overlay('.'.join([p.elfpath,
                                   'elfmanip',
                                   'qshrink',
                                   'tlbupdt']),
                         '.'.join([p.elfpath,
                                   'elfmanip',
                                   'qshrink',
                                   'tlbupdt',
                                   'fwoverlay']))
  if r != 0:
    return r

  # run mc.py before adjusting the size of DEFAULT_PHYSPOOL in pool_configs,
  # so we can get an accurate representation of our available space
  r = p.mc('.'.join([p.elfpath,
                     'elfmanip',
                     'qshrink',
                     'tlbupdt',
                     'fwoverlay']))

  if r != 0:
    return r

  r = p.patch_default_physpool('.'.join([p.elfpath,
                                         'elfmanip',
                                         'qshrink',
                                         'tlbupdt',
                                         'fwoverlay']),
                               '.'.join([p.elfpath,
                                         'elfmanip',
                                         'qshrink',
                                         'tlbupdt',
                                         'fwoverlay',
                                         'default_physpool']))
  if r != 0:
    return r

  if os.path.exists(str(target[0])):
    p.p('Warning: Output ELF already exists at %s, overwriting' %
        str(target[0]))

    os.unlink(str(target[0]))

  if not debug:
    # remove temp files
    name = p.elfpath
    for addition in ('elfmanip', 'qshrink', 'tlbupdt', 'fwoverlay'):
      name = '.'.join([name, addition])
      os.unlink(name)

    # restore the original filename
    shutil.move('.'.join([p.elfpath,
                          'elfmanip',
                          'qshrink',
                          'tlbupdt',
                          'fwoverlay',
                          'default_physpool']),
                str(target[0]))

  else:
    shutil.copyfile('.'.join([p.elfpath,
                              'elfmanip',
                              'qshrink',
                              'tlbupdt',
                              'fwoverlay',
                              'default_physpool']),
                    str(target[0]))

  # run mc.py again to verify that the new DEFAULT_PHYSPOOL isn't exceeded
  r = p.mc(str(target[0]), second_pass=True)

  if r != 0:
    return r

  if p.memreport:
    try:
      p.gen_memreport()

    except Exception:
      # don't break the build
      p.dp('An exception occured while generating the memory reports')

  p.wait_for_threads()

  t2 = timer()
  t_diff = t2 - t1

  p.p('\nTotal pplkcmd execution time: %s\n' % t_str(t_diff))

  return 0
# }}}


if __name__ == '__main__':
  raise Exception(''.join([
    'pplkcmd.py is meant to be run from within SCons. ',
    'Please retry your build with --linkonly to exclusively call pplkcmd.py']))
