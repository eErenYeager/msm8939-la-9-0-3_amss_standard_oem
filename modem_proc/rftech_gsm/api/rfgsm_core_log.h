#ifndef RFGSM_CORE_LOG_H
#define RFGSM_CORE_LOG_H
/*===========================================================================

           GSM  FTM   LOGGING    H E A D E R    F I L E

DESCRIPTION
  This file contains declarations associated with the GSM FTM logging.

REFERENCES
  None

Copyright (c) 2003-2007, 2009, 2010, 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/* <EJECT> */
/*===========================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rftech_gsm/api/rfgsm_core_log.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------       
02/10/12   sb      Changes to support FTM logging for GSM
06/03/12   sb      Included correct header file to enableGSM FTM logging
============================================================================*/
#include "rfa_variation.h"
#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "queue.h"
#include "msg.h"
#include "rfgsm_core_types.h"
#include "rfcom.h"
#include "ftm_common.h"

#ifdef FTM_HAS_LOGGING
#include "ftm_log.h"

#define RFGSM_LOG_BUFFERS   10

#define MAX_NUM_SLOTS 8


// Rf to FTM_TASK message structure

typedef struct
{
  q_link_type       link;
  rfgsm_core_tx_log_type      msg;

} rfgsm_core_log_data;

typedef struct
{

    q_type       rfgsm_log_q;
    q_type       rfgsm_log_free_q;
    rfgsm_core_log_data   rfgsm_buffers[ RFGSM_LOG_BUFFERS];

} rfgsm_log_info_type;




typedef PACK(struct)
{
  ftm_log_gen_type header;
  rfgsm_core_tx_log_type data;
} rfgsm_core_log_type;

/*!
  @details
  This function initializes resources for the FTM task queue
  Rx burst.
 
  @param void

  @retval void

*/
void rfgsm_core_log_q_init( void);

/*!
  @details
  This function "Allocates" a buffer for RFGSM to send messages to FTM and returns a pointer for the free buffer.
 
  @param void

  @retval void*

*/

void *rfgsm_core_log_get_buf( void);

/*!
  @details
  This function sends a populated buffer (message structure) to ftm task.
 
  @param void

  @retval void*

*/

void rfgsm_core_log_send_to_ftm_task( rfm_device_enum_type rfm_dev );

/*!
  @details
  This function dequeues items (head first) from the FTM task buffer and populates log packet before sending it to DiaG.
 
  @param void

  @retval void*

*/
void rfgsm_core_log_handle_q_from_rf( void );


#endif /* FTM_HAS_LOGGING */
#endif 

