/*!
  @file
  rflm_tds_ccs.h

  @brief
  RFLM TDSCDMA CCS header file
  
  @detail
  Define type defintion and function prototype.
  
  
*/

/*==============================================================================

  Copyright (c) 2013, 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rflm/rftech/tdscdma/inc/rflm_tds_ccs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/15/14   jyu     Added support for head-start
01/24/14   jyu     Updated the code to get TQs
01/23/14   jyu     Added support to execute Rx wakeup/burst scripts
01/06/13   jyu     Added initial support for IRAT
12/15/13   jps     Fixes for RFLM based TXAGC override
11/13/13   jyu     Expose rflm_tds_ustmr_add_offset()
11/11/13   jps     Hookup TDS CCS in txagc override mode
10/16/13   jps     Add TXAGC seq CCS commit API
10/15/13   jps     Add TX TQ info data structure for common txagc
10/03/13   jps     Add utility API to return TQ memory usage
09/18/13   jyu     Initial version 
==============================================================================*/

#ifndef RFLM_TDS_CCS_H
#define RFLM_TDS_CCS_H


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include "rflm_ccs_rf_intf.h"
#include "rflm_ccs_rf_event_intf.h"


/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

#define RFLM_TDS_USE_CCS_TQ_PAIR0                 0
#define RFLM_TDS_USE_CCS_TQ_PAIR1                 1

#define RFLM_TDS_8BIT_32BIT_SHFT                  2
#define RFLM_TDS_DIVISION_BY_FOUR_CARRYOVER_VAL   3
/* This Macro transfer Memory usage in bytes into number of 32 bits aligned memory chunk */
#define RFLM_TDS_CCS_MEM_USAGE(memsize) ((memsize+RFLM_TDS_DIVISION_BY_FOUR_CARRYOVER_VAL)>>RFLM_TDS_8BIT_32BIT_SHFT)

/* Max CCS memory (in bytes) allocated to RFLM TDS*/
#define RFLM_TDS_RF_CCS_MEM_SIZE_IN_BYTES         (CCS_RF_TASKQ_PAIRS_DATA_BUF_NUM_WORDS * 4) //3K memory



/*! @brief Structure to send CCS RF task related information to techs as a pair */
typedef struct 
{
  /*! @brief CCS RF TQ 0 taskQ pointer */
  rflm_ccs_rf_tq_handle_t *tq0_handle;
  /*! @brief CCS RF TQ 1 taskQ pointer */
  rflm_ccs_rf_tq_handle_t *tq1_handle;
  /*! @brief CCS RF TQ data memory pointer. The 2 tasks Q above
  need to share the data memory below, caller can partition it appropriately. */
  uint32 *tq_data_ptr;
  /*! @brief Length (in bytes) of the data memory */
  uint16 tq_data_bytes;
  /*! @brief TQ index corresponding to tq0_handle */
  uint8 tq0_index;
  /*! @brief TQ index corresponding to tq1_handle */
  uint8 tq1_index;
} rflm_tds_ccs_rf_pair_info_s;


/*! @brief tdscdma ccs taskQ structure for common txagc */
typedef struct 
{
  /*! @brief CCS RF TQ  taskQ pointer */
  rflm_ccs_rf_tq_handle_t *tq_handle;
  /*! @brief CCS RF TQ data memory pointer */
  uint32 *tq_data_ptr;
  /*! @brief Length (in bytes) of the data memory */
  uint16 tq_data_bytes;
  /*! @brief TQ index corresponding to tq_handle */
  uint8 tq_index;
} rflm_tds_ccs_tq_t;

typedef struct
{
  uint32 *currPtr;
  uint32 *maxPtr;
  uint32 *minPtr;
} rflm_tds_rf_ccs_slot_dest_ptr_t;

typedef struct
{
  rflm_tds_rf_ccs_slot_dest_ptr_t  slotCcsDest;
} rflm_tds_rf_ccs_dest_ptr_set_t;

/* rflm tds ccs interface structure */
typedef struct 
{
  rflm_tds_ccs_rf_pair_info_s     ccsRfPairPtr;
  uint32                          tqPairIdx;
  boolean                         isUpPchSlot;
  rflm_tds_rf_ccs_dest_ptr_set_t  prev;
  rflm_tds_rf_ccs_dest_ptr_set_t  curr;
} rflm_tds_rf_ccs_intf_s;


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

void rflm_tds_rf_ccs_tq_lock(uint8 device);

void rflm_tds_rf_ccs_tq_free(uint32 tqPairIdx);

void rflm_tds_rf_ccs_task_check( uint32 tqPairIdx ); 

void rflm_tds_rf_ccs_tq_reset(uint32 tqPairIdx);

void rflm_tds_rf_event_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_pdet_trigger_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_therm_trigger_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_pdet_read_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_jdet_read_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_therm_read_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_rf_last_event_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_hs_tuning_event_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_irat_cleanup_event_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

void rflm_tds_exec_rf_internal_script_callback(rflm_ccs_rf_tq_handle_t *tq, uint32 task_idx, ccs_rf_task_status_t status, void *arg);

uint32* rflm_tds_ccs_issue_seq
(
  rflm_ccs_rf_event_header_task_type *evtPtr,
  uint32 *ccsDestPtr,
  rflm_ccs_rf_tq_handle_t *ccsTqPtr,
  rflm_ccs_rf_task_callback_t callback,
  uint32 actTime,
  void *callbackArg 
);

uint32* rflm_tds_ccs_event_commit
(
  rflm_ccs_rf_event_header_task_type *evtScptPtr,
  uint32 *ccsDestPtr,
  rflm_ccs_rf_tq_handle_t *tqPtr,
  uint32 actTime,
  void *callbackFunctionPtr
);

uint32* rflm_tds_ccs_commit_txagc_seq
(
  rflm_ccs_rf_seq_t *issue_seq_ptr,
  rflm_ccs_rf_task_callback_t callbackFunctionPtr,
  uint32 *ccsDestPtr,
  uint32 ustmr_action_time
);

uint32 rflm_tds_ccs_get_txagc_seq_action_time(void);

uint32 rflm_tds_ccs_util_get_curr_tq_mem_usage(void);

rflm_tds_ccs_tq_t* rflm_tds_get_ccs_tx_tq(void);
uint32 rflm_tds_ustmr_add_offset(uint32 time0, int32 offset);

uint32 rflm_tds_get_ccs_mem_addr(void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* RFLM_TDS_CCS_H */
