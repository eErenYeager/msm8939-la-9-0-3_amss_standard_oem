
/*!
  @file
  rflm_tds_log.h

  @brief
  RF LOG API definitions for TDS 
  
  @detail
  This file will contain all definitios and declarations to be
  shared with SW and LM for TDS LM Logging
 
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc. (QTI). All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rflm/rftech/tdscdma/inc/rflm_tds_log.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/20/14   jps     Initial Version


==============================================================================*/

#ifndef RFLM_TDS_LOG_H
#define RFLM_TDS_LOG_H


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "rflm.h"
#include "rflm_diag_log.h"
#include "log.h"

/*==============================================================================

                EXTERNAL DEFINITIONS AND TYPES : MACROS

==============================================================================*/

/************************************************************/
/*                LOG PACKET IDENTIFICATION                 */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*! @brief RFLM TDS log packet ID */
#define RFLM_LOG_PACKET_TDS_UL_REPORT_ID                 0x1868
#define RFLM_LOG_PACKET_TDS_DL_REPORT_ID                 0x1869
#define RFLM_LOG_PACKET_TDS_DEBUG_ID                     0x186A 

/************************************************************/
/*                   LOG PACKET VERSIONS                    */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*!@brief Enumeration indicating the TDS DebugL Log Packet version */
typedef enum
{
  RFLM_TDS_DEBUG_LOG_VER_1 = 1, /*!< TDS Debug Log Packet Version 1 */

} rflm_tds_debug_log_version_type;

/*----------------------------------------------------------------------------*/

/*==============================================================================

              EXTERNAL DEFINITIONS AND TYPES : ENUMS

==============================================================================*/


/*==============================================================================

            EXTERNAL DEFINITIONS AND TYPES : STRUCTURES

==============================================================================*/

/************************************************************/
/*              LOG PACKET PAYLOAD DEFINITION               */
/************************************************************/

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==============================================================================
              API input/output function definitions -- TDS 
==============================================================================*/

/************************************************************/
/*           DIAG LOG SUBPACKET API DEFINITIONS             */
/************************************************************/

/*----------------------------------------------------------------------------*/
boolean rflm_tds_debug_log_subpacket (void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* RFLM_TDS_LOG_H */
