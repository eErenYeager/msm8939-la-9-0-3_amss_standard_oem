/*!
   @file
   dtr.cpp

   @brief
*/

/*===========================================================================

Copyright (c) 2010 - 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rflm/dtr/src/rflm_dtr.cpp#1 $

when       who     what, where, why
------------------------------------------------------------------------------- 
01/08/14   cvd     Initial version.

============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/
#include "rflm_dtr.h"
#include <stringl/stringl.h>


/*===========================================================================
                           GLOBAL VARIABLES
===========================================================================*/
#if (defined(TEST_FRAMEWORK) && defined(FEATURE_BOLT_MODEM))
#error code not present
#endif
qurt_mutex_t rflm_dtr_handle_locks[RXLM_MAX_BUFFERS];
#if (defined(TEST_FRAMEWORK) && defined(FEATURE_BOLT_MODEM))
#error code not present
#endif


/*----------------------------------------------------------------------------*/
/*!
   @brief
      Print out a Memory Chunk.
 
   @details
      Print out a Memory Chunk addr upto a length of size.
      Prints 5 words a message, therby reducing the number of messages.

   @retval
      void
*/

void dtr_print_memory_chunk(uint8* addr, uint32 size)
{
//Loop variable for addr
  uint32 i = 0;
//Variable that sets how far to print out.
  uint32 j = 0;
//Loop variable for addr_print
  uint32 k = 0;

  for (i = 0; i < size;)
  {
    j+=5;
    if (j > size)
    {
      j = size;
    }

    uint8 addr_print[5] = { 0 };
    
    // Go 5 more steps or until i = size
    for (k = 0; i < j; i++, k++)
    {
      addr_print[k] = addr[i];
    }

    RFLM_DIAG_TRACE_LOW(RFLM_DIAG_MSG_SSID_NONE, "addr -> 0x%X 0x%X 0x%X 0x%X 0x%X", addr_print[0], addr_print[1], addr_print[2], addr_print[3], addr_print[4]);
  }
}

