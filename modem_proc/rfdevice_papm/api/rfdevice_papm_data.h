#ifndef RFDEVICE_PAPM_DATA
#define RFDEVICE_PAPM_DATA

/*!
  @file
  rfdevice_papm_data.h

  @brief
  This file contains the class definition for the rfdevice_papm_data,
  which serves as base class for the vendor specific PAPM settings data and configuration.

*/

/*===========================================================================

Copyright (c) 2011-13 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_papm/api/rfdevice_papm_data.h#1 $

when       who     what, where, why
-------------------------------------------------------------------------------   
10/30/13   sb      Added support for delay after standby
03/22/13   sr      init version
============================================================================*/
#include "comdef.h"
#include "rfa.h" 
#include "rfdevice_papm_types.h"

class rfdevice_papm_data : public rfa
{
public:
    virtual boolean settings_data_get( rfdevice_papm_cfg_params_type *cfg, 
                                       rfdevice_reg_settings_type *settings) = 0;

    virtual boolean sequence_data_get( rfdevice_papm_cfg_params_type *cfg, 
                                       rfdevice_papm_cmd_seq_type *cmd_seq);

    virtual boolean timing_info_get( rfdevice_papm_timing_info_type *papm_timing_info );

    // Destructor
    virtual ~rfdevice_papm_data();

protected:
  rfdevice_papm_data(void);  /*  Constructor  */

private:
};


#endif


