/*
 * Copyright (c) 2013 QUALCOMM Technologies Inc. All Rights Reserved.
 * Qualcomm Technologies Confidential and Proprietary
 *
 */
#ifndef LISTENER_H
#define LISTENER_H

boolean listener_exists(void);

#endif //LISTENER_H
