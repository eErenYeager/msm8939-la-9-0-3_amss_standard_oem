#ifndef RFSA_DETECT_H
#define RFSA_DETECT_H


boolean rfsa_detect_exists(void);
int rfsa_detect_register(void(*detectcb)(void*), void* ctx);

#endif // RFSA_DETECT_H
