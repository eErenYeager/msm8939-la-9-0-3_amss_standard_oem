#ifndef GEN_APPS_STD_H
#define GEN_APPS_STD_H
#include "apps_std.h"

//TODO: deprecate this interface once we no longer care about rfsa

int gen_apps_std_fopen(const char* name, const char* mode, apps_std_FILE* psout);
int gen_apps_std_fopen_with_env(const char* env, const char *delim, const char* name, const char* mode, apps_std_FILE* psout); 
int gen_apps_std_flen(apps_std_FILE sin, uint64* len);
int gen_apps_std_fread(apps_std_FILE sin, byte* buf, int bufLen, int* bytesRead, int* bEOF);
int gen_apps_std_fclose(apps_std_FILE sin);
int gen_apps_std_fseek(apps_std_FILE sin, int offset, apps_std_SEEK whence);

#endif //GEN_APPS_STD_H
