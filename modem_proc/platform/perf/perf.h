#ifndef PERF_H
#define PERF_H

#include "HAP_perf.h"

int perf_init(char* name, int* pix);

#define perf_enabled(ix) (ix != 0)

//should be the value in *pix
int perf_add_usec(int ix, long long usec);

//for example
//add this to perf.h
//extern int perf_fread;

//in your code do
//PERF(fread, VERIFY(map_pages()))
//or more complex
/*
  PERF(fread,
  if(blah) {
     VERIFY(map_pages());
  }
  (void)0);
*/
//in perf.c add 
//int perf_fread;
// and  in perf.c:perf_pl_init
//perf_fread = 0;
//VERIFY( 0 == perf_init("perf_fread", &perf_fread));

extern unsigned long long qurt_sysclock_get_hw_ticks (void);

static __inline unsigned long long perf_get_time_us(void)
{
  return (unsigned long long)((qurt_sysclock_get_hw_ticks()) * 10ull/192ull);
}


#define PERF(ix, ff) \
   do {\
      if(!perf_enabled(perf_##ix)) {\
         ff ;\
      } else {\
         long long start = perf_get_time_us();\
         ff ;\
         (void)perf_add_usec(perf_##ix, perf_get_time_us() - start);\
      }\
   } while(0)


#endif //PERF_H
