********************************************************************************
  Hexagon Access Program (HAP) SDK Utilities Library
********************************************************************************

The Hexagon Access Program SDK Utilities Library provides implementation
of the HAP utilites such as FARF, HAP_malloc, etc...

Customers intending to use FARF, HAP_malloc, HAP_free, etc.. on targets that
don't inherently support those utilities must link this library into the aDSP
image.

To begin, open documentation file /docs/index.html.  The 'Quick Start' section
is a good place to start.

