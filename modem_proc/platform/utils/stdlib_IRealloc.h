#ifndef STDLIB_IREALLOC_H
#define STDLIB_IREALLOC_H

#include "qtest_stdlib.h"
#include "AEEIRealloc.h"
#include "AEEStdErr.h"
#include <assert.h>

static uint32 stdlib_IRealloc_AddRef(IRealloc* pif) {
   return 1;
}
static uint32 stdlib_IRealloc_Release(IRealloc* pif) {
   return 0;
}
static int stdlib_IRealloc_QueryInterface(IRealloc* pif,  AEEIID iid, void** ppOut) {
   switch(iid) {
      case AEEIID_IQI:
      case AEEIID_IRealloc:
         *ppOut = pif;
         return AEE_SUCCESS;
   }
   return AEE_EUNSUPPORTED;
}
static int stdlib_IRealloc_ErrRealloc(IRealloc* pif, int nSize, void** ppOut) {
   if(nSize == 0) {
      FREE(*ppOut);
      *ppOut = 0;
      return AEE_SUCCESS;
   } else if(*ppOut == 0) {
      assert(nSize > 0);
      *ppOut = CALLOC(nSize, 1); 
      return *ppOut == 0 ? AEE_ENOMEMORY : AEE_SUCCESS;
   }
   return AEE_EUNSUPPORTED;
}

static int stdlib_IRealloc_ErrReallocName(IRealloc* pif, int nSize, void** ppOut, const char* cpszName) {
   return stdlib_IRealloc_ErrRealloc(pif, nSize, ppOut);
}
static int stdlib_IRealloc_ErrReallocNoZI(IRealloc* pif, int nSize, void** ppOut) {
   void* po = *ppOut;
   *ppOut = REALLOC(po, nSize); 
   return nSize > 0 && *ppOut == 0 ? AEE_ENOMEMORY : AEE_SUCCESS;
}
static int stdlib_IRealloc_ErrReallocNameNoZI(IRealloc* pif, int nSize, void** ppOut, const char* cpszName) {
   return stdlib_IRealloc_ErrReallocNoZI(pif, nSize, ppOut);
}

static __inline IRealloc* stdlib_IRealloc(void) {
   struct IRealloc {
      const AEEVTBL(IRealloc)* pvt; 
   };

   static const AEEVTBL(IRealloc) stdlib_IReallocVT = {
       stdlib_IRealloc_AddRef
      ,stdlib_IRealloc_Release
      ,stdlib_IRealloc_QueryInterface
      ,stdlib_IRealloc_ErrRealloc
      ,stdlib_IRealloc_ErrReallocName
      ,stdlib_IRealloc_ErrReallocNoZI
      ,stdlib_IRealloc_ErrReallocNameNoZI
   };

   static const struct IRealloc stdlib_IRealloc = { &stdlib_IReallocVT  };
   return (IRealloc*) & stdlib_IRealloc ;
}


#endif // STDLIB_IREALLOC_H
