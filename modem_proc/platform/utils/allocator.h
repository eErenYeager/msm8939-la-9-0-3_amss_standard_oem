#ifndef ALLOCATOR_H
#define ALLOCATOR_H
/*==============================================================================

Copyright (c) 2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include "qtest_stdlib.h"
#include "AEEStdDef.h"

typedef struct _heap _heap;
struct _heap {
   _heap* pPrev;
   const char* loc;
   uint64 buf;
};

typedef struct allocator {
   _heap* pheap;
   byte* stack;
   byte* stackEnd;
   int nSize;
} allocator;

static __inline int _heap_alloc(_heap** ppa, const char* loc, int size, void** ppbuf) {
   _heap* pn = 0;
   pn = MALLOC(size + sizeof(_heap) - sizeof(uint64));
   if(pn != 0) {
      pn->pPrev = *ppa;
      pn->loc = loc;
      *ppa = pn;
      *ppbuf = (void*)&(pn->buf);
      return 0;
   } else {
      return -1;
   }
}

#define _ALIGN_SIZE(sz, al) (al != 0 ? ((uint32)(sz) % (al) == 0 ? (sz) : (sz) + (al - (((uint32)(sz)) % al))) : sz)

static __inline int allocator_alloc(allocator* me, 
                                    const char* loc, 
                                    int size, 
                                    int al, 
                                    void** ppbuf) {
   if(size < 0) {
      return -1;
   } else if (size == 0) {
      *ppbuf = 0;
      return 0;
   }
   if((_ALIGN_SIZE(me->stackEnd, al) + size) < me->stack + me->nSize) {
      *ppbuf = _ALIGN_SIZE(me->stackEnd, al);
      me->stackEnd = _ALIGN_SIZE(me->stackEnd, al) + size;
      return 0;
   } else {
      return _heap_alloc(&me->pheap, loc, size, ppbuf);
   }
}

static __inline void allocator_deinit(allocator* me) {
   _heap* pa = me->pheap;
   while(pa != 0) {
      _heap* pn = pa;
      const char* loc = pn->loc;
      (void)loc;
      pa = pn->pPrev;
      FREE(pn); 
   }
}

static __inline void allocator_init(allocator* me, byte* stack, int stackSize) {
   me->stack =  stack;
   me->stackEnd =  stack + stackSize;
   me->nSize = stackSize;
   me->pheap = 0;
}


#endif // ALLOCATOR_H
