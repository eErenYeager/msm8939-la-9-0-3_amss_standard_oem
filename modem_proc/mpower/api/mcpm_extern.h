#ifndef __MCPM_EXTERN_H__
#define __MCPM_EXTERN_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R 
               
                E X T E R N A L   H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the external interface functions for the client outside of MODEM SW and definitions for
    Modem Clock and Power Manager (MCPM).
  
  EXTERNALIZED FUNCTIONS
    MCPM_Init
      
  INITIALIZATION AND SEQUENCING REQUIREMENTS
    Invoke the MCPM_Init function to initialize the MCPM.
  
        Copyright (c) 2010 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mpower/api/mcpm_extern.h#1 $

when       who     what, where, why 
--------   ---     --------------------------------------------------------- 
04/19/11   ps       Added support for FW power collapse
02/07/11   yz       Created based on MG's mcpm_api.h
11/17/10   mg       Add define MCPM_NO_UPDATE for no update
11/15/10   mg       Initial Revision.


==========================================================================*/ 


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/



#include "comdef.h"
#include "msg.h"

/*=========================================================================
      Typedefs
==========================================================================*/


/*==========================================================================
               FUNCTION DECLARATIONS FOR MODULE
==========================================================================*/


/*==========================================================================

  FUNCTION      MCPM_INIT

  DESCRIPTION   This function is called once at powerup. This function 
                initializes MCPM variables.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

extern void MCPM_Init(void);

/*==========================================================================

  FUNCTION      MCPM_Get_FW_TCM_Restore_Overhead

  DESCRIPTION   This function will return power collapse timeline adjustments
                to L1.

  PARAMETERS    None

  DEPENDENCIES  

  RETURN VALUE  uint32.

  SIDE EFFECTS  None.

==========================================================================*/
extern uint32 MCPM_Get_FW_TCM_Restore_Overhead(void);


#endif /* __MCPM_EXTERN_H__ */

