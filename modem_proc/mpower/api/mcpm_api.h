#ifndef __MCPM_API_H__
#define __MCPM_API_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R 
               
                E X T E R N A L   H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the external interface functions and definitions for
    Modem Clock and Power Manager (MCPM).
  
  EXTERNALIZED FUNCTIONS
    MCPM_Init
    MCPM_Config_Modem
  
  INITIALIZATION AND SEQUENCING REQUIREMENTS
    Invoke the MCPM_Init function to initialize the MCPM.
  
        Copyright (c) 2010 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mpower/api/mcpm_api.h#1 $

when       who     what, where, why 
--------   ---     --------------------------------------------------------- 
10/06/14   sc       Added req to support bump down of resources (Q6)*
08/26/14   sc       added API-s for multi-tech bump up
06/16/14   sc       Added enums to support LTE INIT state
04/25/14   sc       Prioritize Scheduled over RUDE wakeups
05/21/14   sc       Added new API for L1 Techs to call in to get Power Optimization level
05/20/14   sc       Adding blocks for CB when they are turned ON
03/13/14   sc       Added enums for WCDMA CDRX/EDRX light Sleep
01/23/14   pc       TSTS support
08/29/13   ls       Added LTE light sleep no modem freeze req/states.
08/20/13   pc       volte modem freeze changes
08/14/13   ys       New api interface for DSDS with FEATURE_DUAL_SIM
08/14/13   ys       DSDS support
06/26/13   ls       Adding new LTE requests for VoLTE, etc and make LTE APIs generic
06/20/13   ls       Adding LTE FDD/TDD request for LTE data 20Mhz
04/18/13   ls       Adding LTE CA 5+5 state
04/12/13   ys       update for interface for offline callback
02/01/13   ys       release param update support
12/10/12   sr       MCPM RF Client support
12/10/12   ys       MCVS apis
08/27/12   pg       support for MCPM_GPS_PARMS_UPDATE_REQ to send next wakeup time
07/27/12   HK       New macro added to remove A2 out of Normal muxing.
07/20/12   ys       GPS changes
07/09/12   ys       MCVS support
06/29/12   HK       New modes in Dimes added  to the API: RF BW, UL/DL bit rates 
05/23/12   ys       Clock bus Speed Structure updated
05/03/12   ys       Updated Wakeuptime field to use Uint64
02/02/12   yz       Added support of High Offline clk Cfg
12/09/11   yz       Type define for offline clk speed
09/29/11   ps       Added a new emum for MCPM_Send_RF_Req API
09/22/11   ps       Fixed compilation issue after reverting the a version
                    Removed extern for header declarations
09/22/11   ps       MCPM support for RF-SW to allow direct NPA calls. 
                    This is needed to choose beween 
                    scheduled NPA requests or direct requests
08/21/11   ps       Add support for GSTMR-MCPM mutex,
                    GSM clk src always at PL11 Disable DCO in sleep and
                    Disable SW PC for LTE
08/10/11   mg       Added MCPM_FCW_DCO_RESET_UPDATE update field
06/20/11   ps       Added callback registration mechanism in SHDR/Hybrid modes
06/09/11   yz       Added MCPMDRV_enable_all_pwron_api so the tech team can call it to enable 
05/31/11   mg       Added MCPM_TDSCDMA_ACQ_REQ request for TDS 
05/16/11   ps       Added UIM support for RPM optimization
05/16/11   ps       Add support for UIM 
04/28/11   ps       Added 
                    1. Call flows to get RF settings/Bus reg
                    2. Added calls to TRM
                    3. Added logic to calculate minimum wakeup time
                    4. Added functions to set RFM_TECH_INFO
04/26/11   ps       Adding initial support for RPM optimization - 
                    minimal API needs for RF/GPS/L1tech to integrate their software
                    1. Modified "params" for MCPM_Config_Modem
                    2. Added MCPM_Send_Bus_Request API
4/26/11    mg       Add MCPM_1X_GO_TO_PSEUDO_SLEEP_REQ request
11/17/10   mg       Add define MCPM_NO_UPDATE for no update
11/15/10   mg       Initial Revision.


==========================================================================*/ 


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/


#include "customer.h"
#include "comdef.h"
#include "msg.h"
#include "npa.h"
#include "icbarb.h"
#include "rfm_mode_types.h"
/*=========================================================================
      Macros for the MCPM clk/pll dump
==========================================================================*/
#define MCPM_DUMP_CLK	0x00000001
#define MCPM_DUMP_BLK	0x00000002
#define MCPM_DUMP_PWRUP	0x00000004

/*=========================================================================
      Typedefs
==========================================================================*/

/*
 * mcpm_request_type
 *
 * Requests to MCPM
 *
 * START_REQ should be sent when a technology is activated.
 *
 * STOP_REQ should be sent when a technology is deactivated.
 *
 * WAKE_UP_REQ should be sent when coming out sleep to decode a page indicator
 *
 * GO_TO_SLEEP_REQ should be sent when going into sleep.
 *
 * GO_TO_PSEUDO_SLEEP should be sent by 1X tech if RTC needs to be kept ON during sleep
 *
 * IDLE_REQ should be sent when the UE moves from page indicator decode 
 * to setup paging channel. 1X technology also calls this to move from 
 * 1X VOICE and 1X DATA calls. 
 *
 * VOICE_START_REQ should be sent on start of a voice call.
 *
 * VOICE_STOP_REQ should be sent on end of a voice call.
 *
 * DATA_START_REQ should be sent on start of a data call
 *
 * DATA_STOP_REQ should be sent on end of a data call
 *
 */

typedef enum
{
  /* 1X Requests */
  MCPM_1X_START_REQ                                   = 0,  
  MCPM_1X_STOP_REQ                                    = 1,  
  MCPM_1X_WAKE_UP_REQ                                 = 2,  
  MCPM_1X_GO_TO_SLEEP_REQ                             = 3,  
  MCPM_1X_GO_TO_PSEUDO_SLEEP_REQ                      = 4,  
  MCPM_1X_IDLE_REQ                                    = 5,  
  MCPM_1X_VOICE_REQ                                   = 6,  
  MCPM_1X_DATA_REQ                                    = 7,  
  MCPM_1X_PARMS_UPDATE_REQ                            = 8,  
                                                            
  /* DO Requests */                                         
  MCPM_DO_START_REQ                                   = 9,  
  MCPM_DO_STOP_REQ                                    = 10, 
  MCPM_DO_WAKE_UP_REQ                                 = 11, 
  MCPM_DO_GO_TO_SLEEP_REQ                             = 12, 
  MCPM_DO_IDLE_REQ                                    = 13, 
  MCPM_DO_START_DATA_REQ                              = 14, 
  MCPM_DO_STOP_DATA_REQ                               = 15, 
  MCPM_DO_PARMS_UPDATE_REQ                            = 16, 
                                                            
  /* GERAN Requests */                                      
  MCPM_GERAN_START_REQ                                = 17, 
  MCPM_GERAN_STOP_REQ                                 = 18, 
  MCPM_GERAN_WAKE_UP_REQ                              = 19, 
  MCPM_GERAN_RUDE_WAKE_UP_REQ                         = 20, 
  MCPM_GERAN_GO_TO_SLEEP_REQ                          = 21, 
  MCPM_GERAN_IDLE_REQ                                 = 22, 
  MCPM_GERAN_VOICE_START_REQ                          = 23, 
  MCPM_GERAN_VOICE_STOP_REQ                           = 24, 
  MCPM_GERAN_DATA_START_REQ                           = 25, 
  MCPM_GERAN_DATA_STOP_REQ                            = 26, 
  MCPM_GERAN_PARMS_UPDATE_REQ                         = 27, 
                                               
  /* GERAN1 Requests */
  MCPM_GERAN1_START_REQ                               = 28,
  MCPM_GERAN1_STOP_REQ                                = 29,
  MCPM_GERAN1_WAKE_UP_REQ                             = 30,
  MCPM_GERAN1_RUDE_WAKE_UP_REQ                        = 31,
  MCPM_GERAN1_GO_TO_SLEEP_REQ                         = 32,
  MCPM_GERAN1_IDLE_REQ                                = 33,
  MCPM_GERAN1_VOICE_START_REQ                         = 34,
  MCPM_GERAN1_VOICE_STOP_REQ                          = 35,
  MCPM_GERAN1_DATA_START_REQ                          = 36,
  MCPM_GERAN1_DATA_STOP_REQ                           = 37,
  MCPM_GERAN1_PARMS_UPDATE_REQ                        = 38,
                                               
  /* GERAN2 Requests */
  MCPM_GERAN2_START_REQ                               = 39,
  MCPM_GERAN2_STOP_REQ                                = 40,
  MCPM_GERAN2_WAKE_UP_REQ                             = 41,
  MCPM_GERAN2_RUDE_WAKE_UP_REQ                        = 42,
  MCPM_GERAN2_GO_TO_SLEEP_REQ                         = 43,
  MCPM_GERAN2_IDLE_REQ                                = 44,
  MCPM_GERAN2_VOICE_START_REQ                         = 45,
  MCPM_GERAN2_VOICE_STOP_REQ                          = 46,
  MCPM_GERAN2_DATA_START_REQ                          = 47,
  MCPM_GERAN2_DATA_STOP_REQ                           = 48,
  MCPM_GERAN2_PARMS_UPDATE_REQ                        = 49,
                                               
  /* LTE Requests */                           
  MCPM_LTE_START_REQ                                  = 50, 
  MCPM_LTE_STOP_REQ                                   = 51, 
  MCPM_LTE_ACQ_REQ                                    = 52, 
  MCPM_LTE_WAKE_UP_REQ                                = 53, 
  MCPM_LTE_GO_TO_SLEEP_REQ                            = 54, 
  MCPM_LTE_IDLE_REQ                                   = 55, 
  MCPM_LTE_DATA_START_REQ                             = 56,
  MCPM_LTE_TDD_DATA_START_REQ                         = 57, 
  MCPM_LTE_FDD_DATA_START_REQ                         = 58,
  MCPM_LTE_TDD_VOLTE_DATA_START_REQ                   = 59,
  MCPM_LTE_FDD_VOLTE_DATA_START_REQ                   = 60, 
  MCPM_LTE_GO_TO_LIGHT_SLEEP_REQ                      = 61,
  MCPM_LTE_GO_TO_LONG_LIGHT_SLEEP_REQ                 = 62,
  MCPM_LTE_GO_TO_LIGHT_SLEEP_NO_MODEM_FREEZE_REQ      = 63, 
  MCPM_LTE_DATA_STOP_REQ                              = 64, 
  MCPM_LTE_PARMS_UPDATE_REQ                           = 65, 
                                                            
  /* TDSCDMA Requests */                                    
  MCPM_TDSCDMA_START_REQ                              = 66, 
  MCPM_TDSCDMA_STOP_REQ                               = 67, 
  MCPM_TDSCDMA_ACQ_REQ                                = 68, 
  MCPM_TDSCDMA_WAKE_UP_REQ                            = 69, 
  MCPM_TDSCDMA_GO_TO_SLEEP_REQ                        = 70, 
  MCPM_TDSCDMA_IDLE_REQ                               = 71, 
  MCPM_TDSCDMA_VOICE_START_REQ                        = 72, 
  MCPM_TDSCDMA_VOICE_STOP_REQ                         = 73, 
  MCPM_TDSCDMA_DATA_START_REQ                         = 74, 
  MCPM_TDSCDMA_DATA_STOP_REQ                          = 75, 
  MCPM_TDSCDMA_PARMS_UPDATE_REQ                       = 76, 
                                                           
  /* WCDMA Requests */                                      
  MCPM_WCDMA_START_REQ                                = 77, 
  MCPM_WCDMA_STOP_REQ                                 = 78, 
  MCPM_WCDMA_WAKE_UP_REQ                              = 79, 
  MCPM_WCDMA_GO_TO_SLEEP_REQ                          = 80, 
  MCPM_WCDMA_IDLE_REQ                                 = 81, 
  MCPM_WCDMA_CDRX_GO_TO_LIGHT_SLEEP_REQ               = 82,
  MCPM_WCDMA_EDRX_GO_TO_LIGHT_SLEEP_REQ               = 83,
  MCPM_WCDMA_EDRX_GO_TO_SLEEP_REQ                     = 84,
  MCPM_WCDMA_EDRX_WAKEUP_REQ                          = 85,
  MCPM_WCDMA_VOICE_START_REQ                          = 86, 
  MCPM_WCDMA_VOICE_STOP_REQ                           = 87, 
  MCPM_WCDMA_DATA_START_REQ                           = 88, 
  MCPM_WCDMA_DATA_STOP_REQ                            = 89, 
  MCPM_WCDMA_PARMS_UPDATE_REQ                         = 90, 
                                                            
  /* GPS Requests */                                        
  MCPM_GPS_STOP_REQ                                   = 91, 
  MCPM_GPS_ACQ_REQ                                    = 92, 
  MCPM_GPS_NON_DPO_REQ                                = 93, 
  MCPM_GPS_DPO_ON_REQ                                 = 94, 
  MCPM_GPS_PARMS_UPDATE_REQ                           = 95, 
                                                           
/* RF Requests */                                           
  MCPM_RF_START_REQ                                   = 96, 
  MCPM_RF_STOP_REQ                                    = 97, 
                                                            
  /* A2 Requests */                                         
  MCPM_A2_START_REQ                                   = 98, 
  MCPM_A2_STOP_REQ                                    = 99, 

  /* GSM CIPHERING Requests */                            
  MCPM_GSM_CIPHERING_START_REQ                        = 100, 
  MCPM_GSM_CIPHERING_STOP_REQ                         = 101,
                                                    
    /* GSM1 CIPHERING Requests */                             
  MCPM_GSM1_CIPHERING_START_REQ                       = 102, 
  MCPM_GSM1_CIPHERING_STOP_REQ                        = 103,

      /* GSM2 CIPHERING Requests */                             
  MCPM_GSM2_CIPHERING_START_REQ                       = 104, 
  MCPM_GSM2_CIPHERING_STOP_REQ                        = 105,

  MCPM_LTE_INIT_REQ                                   = 106,

      /* Multi-Tech Requests */
  MCPM_THREE_TECH_REQ                                 = 107,
  MCPM_MULTI_TECH_STOP_REQ                            = 108,

  /*Req to support bump down of resources (Q6)*/
  MCPM_LTE_Q6_BUMP_DOWN_REQ                            = 109,

  MCPM_TECH_MAX_REQ
} mcpm_request_type;

#define MCPM_DO_NOP_REQ            MCPM_DO_PARMS_UPDATE_REQ
#define MCPM_DO_START_DATA_TX_REQ  MCPM_DO_START_DATA_REQ
#define MCPM_DO_STOP_DATA_TX_REQ   MCPM_DO_STOP_DATA_REQ

/*
 * Bit 0: "RECEIVER_CONFIG" field is updated
 * Bit 1: "NEIGHBOR_MEAS" field is updated
 * Bit 2: "DL_DATA_RATE" field is updated
 * Bit 3: "UL_DATA_RATE" field is updated
 * Bit 4: "RF_BANDWIDTH" field is updated
 */
#define MCPM_NO_UPDATE                 0x000
#define MCPM_RECEIVER_CONFIG_UPDATE    0x001
#define MCPM_NEIGHBOR_MEAS_UPDATE      0x002
#define MCPM_DL_DATA_RATE_UPDATE       0x004
#define MCPM_UL_DATA_RATE_UPDATE       0x008
#define MCPM_RF_BANDWIDTH_UPDATE       0x010
#define MCPM_WKTIME_RF_UPDATE          0x020
#define MCPM_WKTIME_RSRC_UPDATE        0x040
#define MCPM_FCW_DCO_RESET_UPDATE      0x080
#define MCPM_MODEM_PERF_MODE_UPDATE    0x100
#define MCPM_MCVS_SCALE_UPDATE         0x200

//MCVS Param update for Dime
#define MCPM_MCVS_VPE_UPDATE            0x001
#define MCPM_MCVS_CLKBUS_UPDATE        0x002
#define MCPM_MCVS_Q6_CLK_UPDATE        0x004
typedef enum
{
//This parameter captures total types of mcvs resources configurable, must sync with -
//#define MCPM_MCVS_VPE_UPDATE            0x001
//#define MCPM_MCVS_CLKBUS_UPDATE        0x002
//#define MCPM_MCVS_Q6_CLK_UPDATE        0x004
  MCVS_VPE_RESRC,
  MCVS_CLKBUS_RESRC,
  MCVS_Q6_RESRC,
  MCVS_MAX_RESRC
} eMcvsResrcs;

/*
 * RECEIVER_CONFIG
 * Bit 0: RXD (ON/OFF)
 * Bit 1: EQ (ON/OFF)
 * Bit 2: QICE (ON/OFF)
 * Bit 3: W-MIMO (ON/OFF)
 */

#define MCPM_RECEIVER_CONFIG_RXD       0x01  /* 1 rxd ON, 0 rxd OFF */
#define MCPM_RECEIVER_CONFIG_EQ        0x02  /* 1 EQ ON, 0 EQ OFF */
#define MCPM_RECEIVER_CONFIG_QICE      0x04  /* 1 QICE ON, 0 QICE OFF */
#define MCPM_RECEIVER_CONFIG_W_MIMO    0x08  /* 1 W MIMO CALL enabled */


#define MCPM_RECEIVER_CONFIG_RXD_SHIFT    0 
#define MCPM_RECEIVER_CONFIG_EQ_SHIFT     1   
#define MCPM_RECEIVER_CONFIG_QICE_SHIFT   2   
#define MCPM_RECEIVER_CONFIG_W_MIMO_SHIFT 3   


/*
 * mcpm_nbr_meas_type
 *
 * The neighbor measurement type
 */

typedef enum
{
  /* 1X Neighbors */
  MCPM_1X2L_MEAS_START,
  MCPM_1X2L_MEAS_STOP,
  
  /* DO Neighbors */
  MCPM_DO2L_MEAS_START,
  MCPM_DO2L_MEAS_STOP,
  MCPM_DO2G_MEAS_START,
  MCPM_DO2G_MEAS_STOP,
  
  /* GERAN Neighbors */
  MCPM_G2L_MEAS_START,
  MCPM_G2L_MEAS_STOP,
  MCPM_G2W_MEAS_START,
  MCPM_G2W_MEAS_STOP,
  MCPM_G2TDS_MEAS_START,
  MCPM_G2TDS_MEAS_STOP,
  
  /* LTE Neighbors */
  MCPM_L2G_MEAS_START,
  MCPM_L2G_MEAS_STOP,
  MCPM_L2W_MEAS_START, 
  MCPM_L2W_MEAS_STOP, 
  MCPM_L21XDO_MEAS_START,
  MCPM_L21XDO_MEAS_STOP,
  MCPM_L2TDSCDMA_MEAS_START,
  MCPM_L2TDSCDMA_MEAS_STOP,
  
  /* TDSCDMA Neighbors */
  MCPM_TDSCDMA2G_MEAS_START,
  MCPM_TDSCDMA2G_MEAS_STOP,
  MCPM_TDSCDMA2L_MEAS_START,
  MCPM_TDSCDMA2L_MEAS_STOP,
  
  /* WCDMA Neighbors */
  MCPM_W2G_MEAS_START,
  MCPM_W2G_MEAS_STOP,
  MCPM_W2L_MEAS_START,
  MCPM_W2L_MEAS_STOP
  
} mcpm_nbr_meas_type;


/*
 * mcpm_rf_bandwidth_type
 *
 * The configured RF bandwidth
 */

typedef enum
{
  /* DO */
  MCPM_DO,
  MCPM_MCDO,
 
  /* LTE */
  MCPM_LTE_1p4MHZ,
  MCPM_LTE_3MHZ,
  MCPM_LTE_5MHZ,
  MCPM_LTE_10MHZ,
  MCPM_LTE_15MHZ,
  MCPM_LTE_20MHZ,
  MCPM_LTE_5MHZ_5MHZ,
  MCPM_LTE_10MHZ_10MHZ,
  
  /* WCDMA */
  MCPM_WCDMA,
  MCPM_DC_WCDMA,
  MCPM_3C_WCDMA,
  MCPM_BW_NA,
  MCPM_NUM_BW
  
} mcpm_rf_bandwidth_type;

/*
 * MCPM_dl_datarate_type
 *
 * Downlink data rate.
 */

typedef enum
{

  /* 1X */
    
  /* DO */
  MCPM_DO_DL_2P4_MBPS,
  MCPM_DO_DL_3P1_MBPS,
  MCPM_DO_DL_9P3_MBPS,
  MCPM_DO_DL_14P7_MBPS,
    
  /* GERAN */
  MCPM_GSM_DL_EV,
   
  /* GPS */
   
  /* LTE */
  MCPM_LTE_DL_RATE_L10,
  MCPM_LTE_DL_RATE_L36,
  MCPM_LTE_DL_RATE_L50,
  MCPM_LTE_DL_RATE_L73,
  MCPM_LTE_DL_RATE_L100,
  MCPM_LTE_DL_RATE_L110,
  MCPM_LTE_DL_RATE_L146,
  MCPM_LTE_DL_RATE_L150,
   
  /* TDSCDMA */
  
  /* WCDMA */
  MCPM_W_DL_R99DATA,
  MCPM_W_DL_3p6_MBPS,
  MCPM_W_DL_7p2_MBPS,
  MCPM_W_DL_14p4_MBPS,
  MCPM_W_DL_21_MBPS,
  MCPM_W_DL_28_MBPS,
  MCPM_W_DL_42_MBPS,
  MCPM_W_DL_63_MBPS,
  MCPM_W_DL_84_MBPS,
  MCPM_DL_RATE_NA,
  MCPM_NUM_DL_RATES
  
} MCPM_dl_datarate_type;

/*
 * mcpm_ul_datarate_type
 *
 * uplink datarate
 */

typedef enum
{
  /* 1X */
 
  /* DO */
  MCPM_DO_UL_153KBPS,
  MCPM_DO_UL_1P8MBPS,
  MCPM_DO_UL_5P4MBPS,
 
  /* GERAN */
  MCPM_GSM_UL_EV,

  /* GPS */

  /* LTE */
  MCPM_LTE_UL_RATE_L5,
  MCPM_LTE_UL_RATE_L11,
  MCPM_LTE_UL_RATE_L13,
  MCPM_LTE_UL_RATE_L23,
  MCPM_LTE_UL_RATE_L25,
  MCPM_LTE_UL_RATE_L40,
  MCPM_LTE_UL_RATE_L50,
  
  /* TDSCDMA */

  /* WCDMA */
  MCPM_W_UL_R99DATA,
  MCPM_W_UL_1P46_MBPS,
  MCPM_W_UL_2MBPS,
  MCPM_W_UL_2P93_MBPS,
  MCPM_W_UL_5P76_MBPS,
  MCPM_W_UL_11P5_MBPS,
  MCPM_W_UL_22_MBPS,
  MCPM_UL_RATE_NA,
  MCPM_NUM_UL_RATES

} mcpm_ul_datarate_type;

#define MCPM_MODEM_WCDMA_PERF_NONE      0x0
#define MCPM_MODEM_WCDMA_HIGH_PERF_MODE 0x1 /* OFFLINE clock to 144 for Nikel */

typedef enum 
{
    MCVS_REQUEST_NONE = 0, 
    MCVS_PRE_SCALE_REQUEST,  /* to prescale Cx/Mx based on clock requests to come in future */
    MCVS_FULL_REQUEST,       /* to update clocks - clk bus/VPE/Q6 - as requested possibly even causing RPM interaction */
    MCVS_FAST_CAP_REQUEST,   /* to update clocks as far as possible but return quickly - No RPM interaction*/
    MCVS_RELEASE_REQUEST     /* To release any request made in past */
} mcvs_request_type;
/*
 * mcpm_mcvsrequest_parms_type
 *
 *  Structure containing the MCPM MCVS Request and Request Attributes.
 *  mcvs_req_type                 - Indicates if request is pre-scale (0) - slow (1) or fast(2), other fileds reserved for future
 *  mcvs_update_info              - Mask that indicates which of the attrbutes are updated.
 * 
 *  modem_vpe_KHz                  - Provides the MP update Info
 *  modem_clk_bus_KHz  - Indiactes the Clk bus update
 *  q6_clock_KHz                       - Indicates Q6 clock update
 */
typedef struct
{
  mcvs_request_type      mcvs_req_type;
  uint32                 mcvs_update_info;
  uint32                 modem_vpe_KHz;
  uint32                 modem_clk_bus_KHz;
  uint32                 q6_clock_KHz;
} mcpm_mcvsrequest_parms_type;
 
/*
 * mcpm_request_parms_type
 *
 * Structure containing the MCPM Request and Request Attributes.
 *
 *  update_info        - Indicates which of the attrbutes are updated.
 *  receiver_config    - Provides the receiver config information.
 *  neighbor_meas      - Indiactes the type of neighbor measurement
 *  dl_datarate        - Provides the downlink data rate.
 *  ul_datarate        - Provides the uplink data rate.
 *  rf_bandwidth       - Provides the RF bandwidth.
 *  wkup_time_for_rf   - Time when RF resource needed (in usec)
 *  wkup_time          - Time when bus or any other resource request needed.(in usec)
 *  modem_perf_mode    - Modem performance mode
 *  mcvs_request - L1s can send an MCVS request along with other parameter updates
 *                 MCVS request could be to pre-scale voltage rails based on clock frequency, or make immediate clk bus/Q6/VPE updates
 */
typedef struct
{

  uint32                        update_info;
  uint32                        receiver_config;
  mcpm_nbr_meas_type            neighbor_meas;
  MCPM_dl_datarate_type         dl_datarate;
  mcpm_ul_datarate_type         ul_datarate;
  mcpm_rf_bandwidth_type        rf_bandwidth;
  uint64                        wkup_time_for_rf;
  uint64                        wkup_time;
  uint32                        modem_perf_mode;
  mcpm_mcvsrequest_parms_type   mcvs_request;
} mcpm_request_parms_type;

/*----------------------------------------------------------------------------
  MODEM block register restore callback function prototype
----------------------------------------------------------------------------*/

typedef void (*mcpm_block_restore_callback_type) (void);

/*----------------------------------------------------------------------------
  L1 techs register sleep timeline callback function prototype
  Default 1X and DO will extend timeline
  TRUE in param implies Optimized timeline
  FALSE in param implies Extended timeline
----------------------------------------------------------------------------*/

typedef void (*mcpm_npa_slp_tmln_callback_type) (boolean);


/*----------------------------------------------------------------------------
  Typedef of modem clocks.
----------------------------------------------------------------------------*/

typedef enum
{

  /* MCPM Offline clk */
  MCPM_CLK_MODEM,

  /* MCPM TDEC clk */
  MCPM_CLK_TDEC,

  /* MCPM total number of clocks */
  MCPM_NUM_CLK
}
mcpm_modem_clk_type;

/*----------------------------------------------------------------------------
  Typedef of MCPM how to trigger call-back.
----------------------------------------------------------------------------*/

typedef enum
{

  /* Trigger call back after clock change in MCPM */
  MCPM_CB_Trigger_AFTER_CLK_Change,

  /* Trigger call back before clock change in MCPM */
  MCPM_CB_Trigger_BEFORE_CLK_Change,
}
mcpm_clk_callback_trigger_type;

/*----------------------------------------------------------------------------
  Typedef of MCPM supported Technologies
----------------------------------------------------------------------------*/

typedef enum
{

  /* MCPM 1X technolgy definition */
  MCPM_1X_TECH,

  /* MCPM GERAN technolgy definition */
  MCPM_GERAN_TECH,

 /* MCPM GERAN1 technolgy definition */
  MCPM_GERAN1_TECH,

   /* MCPM GERAN2 technolgy definition */
  MCPM_GERAN2_TECH,

  /* MCPM DO technolgy definition */
  MCPM_DO_TECH,

  /* MCPM WCDMA technolgy definition */
  MCPM_WCDMA_TECH,

  /* MCPM LTE technolgy definition */
  MCPM_LTE_TECH,

  /* MCPM TDSCDMA technolgy definition */
  MCPM_TDSCDMA_TECH,

  /* MCPM GPS technolgy definition */
  MCPM_GPS_TECH,

  /* MCPM RF technology definition */
  MCPM_RF_TECH,

  /* MCPM GSM CIPHERING definition */
  MCPM_GSM_CIPHERING_TECH,

  /* MCPM GSM1 CIPHERING definition */
  MCPM_GSM1_CIPHERING_TECH,

    /* MCPM GSM2 CIPHERING definition */
  MCPM_GSM2_CIPHERING_TECH,

    /* MCPM Multiple technolgy definition */
  MCPM_MULTI_TECH,

  /* MCPM A2 technolgy definition */
  MCPM_A2_TECH,

  /* number fo techs used for boundary checks  */
  MCPM_NUM_TECH
}
mcpm_tech_type;

/*----------------------------------------------------------------------------
  A2 resources need be setup as fast as possible. Pulling out of A2 from the
  normal path of muxing.
----------------------------------------------------------------------------*/
#define MCPMDRV_NUM_MUX_TECH  (MCPM_A2_TECH)

/*----------------------------------------------------------------------------
  Typedef of modem blocks.
----------------------------------------------------------------------------*/

typedef enum
{

  /* MCPM EDGE block */
  MCPM_EDGE_BLOCK,

  /* MCPM EDGE1 block */
  MCPM_EDGE_G1_BLOCK,

    /* MCPM EDGE2 block */
  MCPM_EDGE_G2_BLOCK,

  /* MCPM TX block */
  MCPM_TX_BLOCK,

  /* MCPM Turbo decoder block */
  MCPM_TDEC_BLOCK,

  /* MCPM DemBack block */
  MCPM_DEMBACK_BLOCK,

  /* MCPM VPE block */
  MCPM_VPE_BLOCK,

  /* MCPM MEMSS block */
  MCPM_MEMSS_BLOCK,

  /* MCPM DEMSS block */
  MCPM_DEMSS_BLOCK,

  /* MCPM RXFE block */
  MCPM_RXFE_BLOCK,

  /* MCPM RXFE block */
  MCPM_STMR_BLOCK,

  /* MCPM TX block toggle */
  MCPM_TX_BLOCK_TOGGLE,

  /* MCPM Turbo decoder block toggle */
  MCPM_TDEC_BLOCK_TOGGLE,

  /* MCPM DemBack block toggle */
  MCPM_DEMBACK_BLOCK_TOGGLE,

  /* MCPM DEMSS block toggle */
  MCPM_DEMSS_BLOCK_TOGGLE,

  /* MCPM total number of blocks */
  MCPM_NUM_BLOCK
}
mcpm_modem_block_type;

/* Return values to check for by RF/other SW modules using MCPM_NPA */
typedef enum
{
  MCPMNPA_STAT_NULL,
  MCPMNPA_STAT_OPT_SUCCESS = 1,
  MCPMNPA_STAT_OPT_FAILURE = 2,
  MCPMNPA_STAT_OPT_IMM_REQ_SENT = 3,
  MCPMNPA_STAT_OPT_ENABLE = 4
} MCPMNPA_StatusType;

/*----------------------------------------------------------------------------
  Typedef of modem offline clk speeds.
----------------------------------------------------------------------------*/

typedef enum
{
  MCPM_CLK_NULL, 
  MCPM_CLK_19_2M,
  MCPM_CLK_72M,
  MCPM_CLK_144M,
  MCPM_NUM_CLKSPEED
} MCPM_ClkSpeedType;

/*----------------------------------------------------------------------------
  Typedef of core cpu vdd vote.
----------------------------------------------------------------------------*/

typedef enum
{
MCPM_MODEM_PLUS_Q6_PC_VOTE,      /*vote = 0*/
MCPM_MODEM_PLUS_Q6_NOT_PC_VOTE,   /*vote = 1*/
MCPM_Q6_ONLY_PC_VOTE            /*MODEM FREEZE - vote =2*/
} MCPM_core_cpuvdd_vote_type;



/*==========================================================================
               FUNCTION DECLARATIONS FOR MODULE
==========================================================================*/


/*==========================================================================

  FUNCTION      MCPM_CONFIG_MODEM

  DESCRIPTION   This function is the main interface function.
                Modem SW should call this function when the modem mode is 
                changed. MCPM expects the call made for the following mode
                changes.
                - Activate and deactivate the Portocal stack
                - Start and stop of voice call
                - Start and stop of data call
                - Change in RF bandwidth
                - Start and Stop of nbr measurement
                - Rxd/qice/EQ start and stop.

  PARAMETERS    req      - Modem SW mode change request.
                parms    - Attributes associated with req.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  Clocks will be enabled and disabled during the test.

==========================================================================*/

extern void MCPM_Config_Modem(mcpm_request_type req, mcpm_request_parms_type *parms);

/*==========================================================================

  FUNCTION      MCPM_GET_PWROPT_LEVEL

  DESCRIPTION   This function is the interface function for L1-s to get the level of Pwr Opt.
                Modem SW should call this function when the level of pwr opt is needed by L1 

  PARAMETERS    tech      - Modem SW tech type.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  The level of power optimization for that tech

  SIDE EFFECTS  None

==========================================================================*/
uint8 MCPM_Get_Pwropt_Level(mcpm_tech_type tech);

/*==========================================================================

  FUNCTION      MCPM_MCVSConfig_Modem

  DESCRIPTION   This function is the interface function for MCVS requests.
                Modem SW should call this function when the modem mode mcvs update is required. 
                MCPM expects the call made for the following mode
                changes.
                - VPE Clock update
                - CLK BUS Clock update
                - Q6 Clock update                

  PARAMETERS    tech      - Modem SW tech type.
                parms    - Attributes associated with req.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  Clocks will be Updated during this without change in Config. If the request attribute type in parms is
                set to type immediate - then no RPM requests will be sent out to scale the voltages. If voltages have 
                not been pre-scaled, then frequencies will be capped.If request type is type slow - this call will 
                block if (mx,cx) voltage scaling is required and may take longer to finish. This call needs to be
                accompanied by a call to Release the MCVS Request

==========================================================================*/

void MCPM_MCVSConfig_Modem(mcpm_tech_type tech, mcpm_mcvsrequest_parms_type *parms);




/*==========================================================================

  FUNCTION      MCPM_MCVSConfig_Release
  DESCRIPTION   This function is the interface function for releasing the MCVS requests .
                Modem SW should call this function when the modem mode mcvs update request has been made. 
                MCPM expects the call made for the following mode
                changes.
                - VPE Clock update
                - CLK BUS Clock update
                - Q6 Clock update                

  PARAMETERS    tech      - Modem SW tech type.
                

  DEPENDENCIES  MCPM must be initialized. A prior call to MCPM_MCVSConfig_Modem from same tech type should have been made.
 

  RETURN VALUE  NONE.

  SIDE EFFECTS : This will bring down the clock configs to pre-mcvs level.
  
 
*/
void MCPM_MCVSConfig_Release(mcpm_tech_type tech);


/*============================================================================

FUNCTION MCPM_SET_BLOCK_RESTORE_CALLBACK

DESCRIPTION
  This function sets the callback function to call when modem block is bought
  out of power gating (power collapse)

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_Set_Block_Restore_Callback
(
  /* The tech that is registering the block */
  mcpm_tech_type tech,

  /* Modem block  */
  mcpm_modem_block_type block,

  /* register restore callback */
  mcpm_block_restore_callback_type restore_cb
);

/*----------------------------------------------------------------------------
  MODEM clock speed change callback function prototype
----------------------------------------------------------------------------*/

typedef void (*mcpm_clk_speed_change_callback_type) (void);

/*============================================================================

FUNCTION MCPM_Set_Clock_Change_Callback

DESCRIPTION   See mcpm_api.h

============================================================================*/

void MCPM_Set_Clock_Change_Callback
(
  /* The tech that is registering the call-back */
  mcpm_tech_type tech,

  /* Modem clk  */
  mcpm_modem_clk_type clk_type,

  /* register clock change callback */
  mcpm_clk_speed_change_callback_type clk_speed_change_cb
);

/*==========================================================================
FUNCTION  MCPMDRV_CFG_Dump

DESCRIPTION: 
  	The MCPM configuration API to dump all real time MCPM clk/registers
	
==========================================================================*/
void MCPMDRV_CFG_Dump( uint32 mcpm_dump_val);

/*==========================================================================

  FUNCTION      MCPM_Send_Bus_Req

  DESCRIPTION   This function can be called by RF drivers to send the RF requests
                to MCPM.

  PARAMETERS    tech                - Tech that is making this request.
                Bus_Speed_in_MHz    - Bus speed in MHz that is needed
                time_rsrc_needed    - Time when this resource is needed (in ticks) (not used for now)
                sleep_req           - Bool to indicate if this is a sleep or awake request

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/
ICBArb_ErrorType MCPM_Send_Bus_Req
(
  /* Tech that is making this request. */
  mcpm_tech_type tech,
  /* Bus speed in MHz required */
  ICBArb_RequestType *Bus_Speed, 
  /* NPA handle to be used by MCPM */
  npa_client_handle npa_handle, 
  /* Time in ticks when this is needed. Use 0 if need to be applied immendiately. */
  uint64 time_rsrc_needed_in_usec,
  /* Flag to indicate MCPM if this is a sleep request or awake */
  boolean sleep_req
);

/*==========================================================================

  FUNCTION      MCPM_Send_RF_Req

  DESCRIPTION   This function can be called by RF drivers to send the RF requests
                to MCPM.

  PARAMETERS    mode                - Tech/RF mode that is making this request.
                npa_id              - PAM Id that MCPM will use in NPA request
                npa_handle          - NPA handle that MCPM should use to
                pass this request to PMIC
                time_rsrc_needed    - Time when this resource is needed (not used for now)
                sleep_req           - Bool to indicate if this is a sleep or awake request

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/
MCPMNPA_StatusType MCPM_Send_RF_Req
(
  /* Tech that is making this request. */
  rfm_mode_enum_type mode, 
  /* NPA Id sent by RF driver */
  uint8 npa_id, 
  /* NPA handle that is sent by RF driver */
  npa_client_handle npa_handle, 
  /* Time when this resource is needed. */
  uint64 time_rsrc_needed,
  /* Sleep or Awake request. */
  boolean sleep_req
);

/*============================================================================

FUNCTION MCPM_NPA_SET_SLEEP_TIMELINE_CALLBACK

DESCRIPTION
  This function sets the callback function to call when sleep timeline has to
  changed from optimized to extended and vice versa

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None.

============================================================================*/

void MCPM_NPA_Set_Sleep_Timeline_Callback
(
  /* The tech that is registering the block */
  mcpm_tech_type tech,

  /* register sleep timeline callback */
  mcpm_npa_slp_tmln_callback_type slp_tmln_cb
);


/*==========================================================================
FUNCTION  MCPMDRV_Get_Modem_Offline_Clock

DESCRIPTION: 
    MCPM API to returen offline clock setting to the caller
  
==========================================================================*/
MCPM_ClkSpeedType MCPMDRV_Get_Modem_Offline_Clock(mcpm_tech_type current_tech);

#if defined FEATURE_DUAL_SIM || defined FEATURE_TRIPLE_SIM
/*==========================================================================
FUNCTION  MCPMDRV_is_clk_on_api

DESCRIPTION: 
  	The extern API to be used by GSTMR to check GSM CLK On before processing ISR
	@params: tech type
	
==========================================================================*/

boolean MCPMDRV_ifclk_on_get_gstmr_mutex(mcpm_tech_type tech);

/*==========================================================================
FUNCTION  MCPMDRV_is_clk_on_api

DESCRIPTION: 
  	The extern API to be used by GSTMR to check GSM CLK On before processing ISR
	@params: tech type
	
==========================================================================*/

void MCPMDRV_release_gstmr_mutex(mcpm_tech_type tech);

/*==========================================================================

  FUNCTION      mcpm_gsm_ciphering_on()

  DESCRIPTION   This function sets up the GSM ciphering resouces.
  @params: tech type

  @return     None
==========================================================================*/

void mcpm_gsm_ciphering_on(mcpm_tech_type tech);


/*==========================================================================

  FUNCTION      mcpm_gsm_ciphering_off()

  DESCRIPTION   This function disables the GSM ciphering resouces.
  @params: tech type

  @return     None
==========================================================================*/

void mcpm_gsm_ciphering_off(mcpm_tech_type tech);

#else

/*==========================================================================
FUNCTION  MCPMDRV_is_clk_on_api

DESCRIPTION: 
  	The extern API to be used by GSTMR to check GSM CLK On before processing ISR
	@params: None
	
==========================================================================*/
boolean MCPMDRV_ifclk_on_get_gstmr_mutex(void);


/*==========================================================================
FUNCTION  MCPMDRV_release_gstmr_mutex

DESCRIPTION: 
  	The extern API to be used by GSTMR to release MCPM-GSTMR mutex
	@params: None
	
==========================================================================*/
void MCPMDRV_release_gstmr_mutex(void);



/*==========================================================================

  FUNCTION      mcpm_gsm_ciphering_on()

  DESCRIPTION   This function sets up the GSM ciphering resouces.
                1. MSS enable
                2. MTC enable
                3. DEMSS block in MODEM PWRUP register
                4. EDGE block
  @return     None
==========================================================================*/
void mcpm_gsm_ciphering_on(void);


/*==========================================================================

  FUNCTION      mcpm_gsm_ciphering_off()

  DESCRIPTION   This function disables the GSM ciphering resouces.
                1. MSS disable
                2. MTC disable
                3. DEMSS disable in MODEM PWRUP register
                4. EDGE disable
  @return     None
==========================================================================*/
void mcpm_gsm_ciphering_off(void);

#endif

#endif /* __MCPM_API_H__ */

