#!/usr/bin/env python
# ----------------------------------------------------------------------------------
#	MCPM Unit Test Scenario Generator Script [SENGEN] Pre-Processor Module
#	Sougata Chatterjee
#	P.O.C : sougatac@qti.qualcomm.com
# ----------------------------------------------------------------------------------
# Revision Changelog : preprocessor.py
#
# 08/15/2013 Release_1.0 Enables input to be either a single scenario file or folder containing multiple files
#
# TO DO
# [ ] Change location of combined scenario to output directory specified
# [ ] Sanitize scenario to ensure all mcpm requests are valid
# [ ] Sanitize scenario to ensure all mcpm techs specified are valid
# [ ] Sanitize scenario to ensure all state transitions are valid
# ----------------------------------------------------------------------------------

# imports
import os, inspect, sys

def pre_processing(arg):

  
  SCRIPT_PATH    = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
  #ip_arg_name = sys.argv[-1]
  #FOLDER_PATH	= SCRIPT_PATH +'/'+ip_arg_name
  file_op = open("C:\Temp\combined_scenerio.txt", "w+")
  
  if os.path.isdir(arg) == True:
      print arg, "is a directory"
      print "\nScenario files are stored in : %s", arg
      dirList=os.listdir(arg)
  
      for fname in dirList:
          fo = open(arg+'/'+fname, "r+")
          print "File added from scenario can: ", fo.name
          str_line = fo.readlines();
          fo.close()
          for each_line in str_line:
              file_op.write(each_line)
          file_op.write('\n')

  else:
      print arg, "is a file"
      fo = open(arg, "r+")
      str_line = fo.readlines();
      fo.close()
      for each_line in str_line:
          file_op.write(each_line)
      file_op.write('\n')
  
  # Close opened file
  file_op.close()
  return_arg="C:\Temp\combined_scenerio.txt"
  return return_arg





