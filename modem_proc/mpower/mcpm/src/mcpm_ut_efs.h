/*=========================================================================


  M O D E M   C L O C K   A N D   P O W E R   M A N A G E R 

  U N I T  T E S T  S C E N A R I O  G E N E R A T O R


  GENERAL DESCRIPTION

  This header file defines the static data structures used by UT. 
  In scenario mode, this structures are pre-populated at compile time,
  In EFS mode, these are dynamically populated at run-time.
  This file is imported by mcpm_ut_scenario.h

Copyright (c) 2010 - 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

==========================================================================*/

#ifndef __MCPM_UT_EFS_H__
#define __MCPM_UT_EFS_H__
#include <mcpm_api.h> 

typedef struct
{
  mcpm_request_type scr_req;
  uint32 scr_cntrlflag;
  uint32 scr_mcvsThidx;
  mcpm_request_parms_type scr_param;
} mcpm_scr_data;

typedef struct
{
  mcpm_scr_data *reqData;
  uint32 startTick; //when to send the req.
} mcpm_scenario;

typedef struct
{
  mcpm_scenario *scrToRun;
  uint32 scrSz;
  uint32 loopInf;
  uint32 absTime:1;
  uint32 loopIsCount:1;
  uint32 writeReport:1;
  uint32 :29;
} mcpm_scenario_list;

extern void driver_efs_mode(void);
extern mcpm_scenario_list *ut_all_scenarios_efsMode;
extern mcpm_scr_data *ut_data_input_efsMode;
extern uint32 max_num_scrs_efsMode;
#endif

