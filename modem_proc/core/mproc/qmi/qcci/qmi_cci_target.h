#ifndef QMI_CCI_TARGET_H
#define QMI_CCI_TARGET_H
/******************************************************************************
  @file    qmi_cci_target.h
  @brief   OS Specific routines internal to QCCI.

  DESCRIPTION
  This header provides an OS abstraction to QCCI.

  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "rex.h"
#include "smem_log.h"
#include "err.h"
#include "assert.h"
#include "msg.h"
#include "qmi_common.h"
#include "qmi_cci_target_ext.h"
#include "qmi_idl_lib_internal.h"

#ifdef FEATURE_QMI_NATIVE_REX
/* stub macro on native rex */
#define rex_del_crit_sect(ptr)
#endif

typedef rex_crit_sect_type qmi_cci_lock_type;
#define LOCK(ptr)        rex_enter_crit_sect(ptr)
#define UNLOCK(ptr)      rex_leave_crit_sect(ptr)
#define LOCK_INIT(ptr)   rex_init_crit_sect(ptr)
#define LOCK_DEINIT(ptr) rex_del_crit_sect(ptr)

#define MALLOC(size)      malloc(size)
#define CALLOC(num, size) calloc(num, size)
#define FREE(ptr)         free(ptr)
#define REALLOC(ptr,size) realloc(ptr, size)

#define QMI_CCI_LOG_EVENT_TX            (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x00)
#define QMI_CCI_LOG_EVENT_TX_EXT        (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x04)
#define QMI_CCI_LOG_EVENT_RX            (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x01)
#define QMI_CCI_LOG_EVENT_RX_EXT        (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x05)
#define QMI_CCI_LOG_EVENT_ERROR         (SMEM_LOG_QMI_CCI_EVENT_BASE + 0x03)

#ifdef QMI_CCI_SIGNAL_INITED_SUPPORTED
#define QMI_CCI_OS_SIGNAL_VALID(ptr) ((ptr) ? ((ptr)->inited == TRUE ? 1 : 0) : 0)
#define QMI_CCI_OS_EXT_SIGNAL_VALID(ptr) QMI_CCI_OS_SIGNAL_VALID(ptr)

#define QMI_CCI_SIGNAL_SET_INITED(ptr) (ptr)->inited = TRUE
#define QMI_CCI_SIGNAL_CLEAR_INITED(ptr) (ptr)->inited = FALSE

#else
#define QMI_CCI_SIGNAL_SET_INITED(ptr) 
#define QMI_CCI_SIGNAL_CLEAR_INITED(ptr) 
#endif

/* Decouple from target_ext.h */
#ifndef is_pow_2
#define is_pow_2(val) (((val) & ((val) - 1)) == 0 ? TRUE : FALSE)
#endif

#define QMI_CCI_LOG_ERR(fmt, val1, val2, val3) MSG_HIGH(fmt, val1, val2, val3)

#define QMI_CCI_OS_EXT_SIGNAL_INIT(ptr, os_params) \
  do { \
    /* we don't touch the timer, so the user can choose to use it */ \
    if(!os_params)  \
      break;  \
    ptr = os_params; \
    QMI_CCI_SIGNAL_CLEAR_INITED(ptr); \
    if(!(os_params) || !(os_params)->tcb || !(os_params)->sig || !is_pow_2((os_params)->sig)) \
      break;  \
    (ptr)->timed_out = 0; \
    (ptr)->timer_inited = FALSE; \
    if((os_params)->timer_sig) { \
      if(is_pow_2((os_params)->timer_sig)) {  \
        (ptr)->timer_inited = TRUE; \
        rex_def_timer(&((ptr)->timer), (ptr)->tcb, (ptr)->timer_sig); \
      } else {  \
        MSG_ERROR("QCCI: Invalid timer signal value: 0x%x for tcb:0x%x used in initialization. Timeout will be disabled", (ptr)->timer_sig, (ptr)->tcb, 0); \
      } \
    } \
    QMI_CCI_SIGNAL_SET_INITED(ptr); \
  } while(0)

#define QMI_CCI_OS_SIGNAL_INIT_SELF(ptr, os_params) \
  do {  \
    (ptr)->tcb = rex_self();  \
    if((ptr)->tcb != (os_params)->tcb) {  \
        MSG_MED("Warning: QCCI Handle for tcb:%x being freed from tcb:%x. Behavior is not thread Safe", (os_params)->tcb, (ptr)->tcb, 0); \
    } \
    (ptr)->sig = (os_params)->sig;  \
    (ptr)->timed_out = 0; \
    if((os_params)->timer_inited == TRUE) { \
      (ptr)->timer_sig = (os_params)->timer_sig;  \
      (ptr)->timer_inited = TRUE; \
      rex_def_timer(&(ptr)->timer, (ptr)->tcb, (ptr)->timer_sig); \
    } else {  \
      (ptr)->timer_sig = 0; \
      (ptr)->timer_inited = FALSE;  \
    } \
    QMI_CCI_SIGNAL_SET_INITED(ptr); \
  } while(0)

/* Assumes addr_len is MAX_ADDR_LEN. Change needed if qmi_cci_common.c changes
 * to invalidate this assumption */
#define QMI_CCI_OS_LOG_TX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len) \
  do {  \
    uint32_t *int_addr = (uint32_t *)(addr);  \
    SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_TX_EXT, (cntl_flag) << 16 | (txn_id), (msg_id) << 16 | (msg_len),    (svc_obj)->service_id, int_addr[0], int_addr[1], int_addr[2]); \
  } while(0)

/* Assumes addr_len is MAX_ADDR_LEN. Change needed if qmi_cci_common.c changes
 * to invalidate this assumption */
#define QMI_CCI_OS_LOG_RX_EXT(svc_obj, cntl_flag, txn_id, msg_id, msg_len, addr, addr_len) \
  do {  \
    uint32_t *int_addr = (uint32_t *)(addr);  \
    SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_RX_EXT, (cntl_flag) << 16 | (txn_id), (msg_id) << 16 | (msg_len),    (svc_obj)->service_id, int_addr[0], int_addr[1], int_addr[2]); \
  } while(0)

#define QMI_CCI_OS_LOG_ERROR() qcci_log_error(__FILE__, __LINE__)

size_t strlcpy(char *dst, const char *src, size_t siz);
static __inline void qcci_log_error(char *filename, unsigned int line)
{
  uint32 name[5];
  char *last;
  last = strrchr(filename, '/');
  if(!last)
    last = strrchr(filename, '\\');
  last = last ? (last+1) : filename;
  strlcpy((char *)name, last, sizeof(name));
  SMEM_LOG_EVENT6(QMI_CCI_LOG_EVENT_ERROR, name[0], name[1], name[2], name[3], 
      name[4], line);
  MSG_ERROR("Runtime error. File 0x%s, Line: %d", filename, line, 0);
}

#endif
