#ifndef QMI_CSI_TARGET_H
#define QMI_CSI_TARGET_H
/******************************************************************************
  @file    qmi_csi_target.h
  @brief   Rex OS Specific routines internal to QCSI.

  DESCRIPTION
  This header provides an OS (Rex) abstraction to QCSI.

  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "rex.h"
#include "smem_log.h"
#include "err.h"
#include "assert.h"
#include "qmi_common.h"
#include "qmi_idl_lib_internal.h"

#ifdef FEATURE_QMI_NATIVE_REX
/* stub macro on native rex */
#define rex_del_crit_sect(ptr)
#endif

typedef rex_crit_sect_type qmi_csi_lock_type;

#define LOCK(ptr)        rex_enter_crit_sect(ptr)
#define UNLOCK(ptr)      rex_leave_crit_sect(ptr)
#define LOCK_INIT(ptr)   rex_init_crit_sect(ptr)
#define LOCK_DEINIT(ptr) rex_del_crit_sect(ptr)

#define MALLOC(size)      malloc(size)
#define CALLOC(num, size) calloc(num, size)
#define FREE(ptr)         free(ptr)
#define REALLOC(ptr,size) realloc(ptr, size)

#define QMI_CSI_OS_SIGNAL_SET(s) rex_set_sigs((s)->tcb, (s)->sig)
#define QMI_CSI_OS_SIGNAL_WAIT(s) rex_wait((s)->sig)
#define QMI_CSI_OS_SIGNAL_CLEAR(s) rex_clr_sigs((s)->tcb, (s)->sig)

#define QMI_CSI_LOG_EVENT_TX            (SMEM_LOG_QMI_CSI_EVENT_BASE + 0x00)
#define QMI_CSI_LOG_EVENT_TX_EXT        (SMEM_LOG_QMI_CSI_EVENT_BASE + 0x04)
#define QMI_CSI_LOG_EVENT_RX            (SMEM_LOG_QMI_CSI_EVENT_BASE + 0x01)
#define QMI_CSI_LOG_EVENT_RX_EXT        (SMEM_LOG_QMI_CSI_EVENT_BASE + 0x05)
#define QMI_CSI_LOG_EVENT_ERROR         (SMEM_LOG_QMI_CSI_EVENT_BASE + 0x03)

#define QMI_CSI_OS_LOG_TX(header) \
  do { \
    uint8_t cntl_flag; \
    uint16_t txn_id, msg_id, msg_len; \
    decode_header(header, &cntl_flag, &txn_id, &msg_id, &msg_len); \
    SMEM_LOG_EVENT(QMI_CSI_LOG_EVENT_TX, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, 0); \
  } while(0)

#define QMI_CSI_OS_LOG_RX(header) \
  do { \
    uint8_t cntl_flag; \
    uint16_t txn_id, msg_id, msg_len; \
    decode_header(header, &cntl_flag, &txn_id, &msg_id, &msg_len); \
    SMEM_LOG_EVENT(QMI_CSI_LOG_EVENT_RX, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, 0); \
  } while(0)

#define QMI_CSI_OS_LOG_TX_EXT(header, svc_id, addr, addr_len) \
  do { \
    uint8_t cntl_flag; \
    uint16_t txn_id, msg_id, msg_len; \
    uint32_t __addr[MAX_ADDR_LEN/4] = {0};\
    QMI_MEM_COPY(__addr, MAX_ADDR_LEN, addr, addr_len); \
    decode_header(header, &cntl_flag, &txn_id, &msg_id, &msg_len); \
    SMEM_LOG_EVENT6(QMI_CSI_LOG_EVENT_TX_EXT, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, svc_id, __addr[0], __addr[1], __addr[2]); \
  } while(0)

#define QMI_CSI_OS_LOG_RX_EXT(header, svc_id, addr, addr_len) \
  do { \
    uint8_t cntl_flag; \
    uint16_t txn_id, msg_id, msg_len; \
    uint32_t __addr[MAX_ADDR_LEN/4] = {0};\
    QMI_MEM_COPY(__addr, MAX_ADDR_LEN, addr, addr_len); \
    decode_header(header, &cntl_flag, &txn_id, &msg_id, &msg_len); \
    SMEM_LOG_EVENT6(QMI_CSI_LOG_EVENT_RX_EXT, cntl_flag << 16 | txn_id, msg_id << 16 | msg_len, svc_id, __addr[0], __addr[1], __addr[2]); \
  } while(0)

#define QMI_CSI_OS_LOG_ERROR() qcsi_log_error(__FILE__, __LINE__)

size_t strlcpy(char *dst, const char *src, size_t siz);
static __inline void qcsi_log_error(char *filename, unsigned int line)
{
  uint32 name[5];
  char *last;
  last = strrchr(filename, '/');
  if(!last)
    last = strrchr(filename, '\\');
  last = last ? (last+1) : filename;
  strlcpy((char *)name, last, sizeof(name));
  SMEM_LOG_EVENT6(QMI_CSI_LOG_EVENT_ERROR, name[0], name[1], name[2], name[3], 
      name[4], line);
  MSG_ERROR("Runtime error. File 0x%s, Line: %d", filename, line, 0);
}
#endif
