/******************************************************************************
  @file    qmi_ping_svc_main.c
  @brief   Rex OS implementation of the QMI Test service

  DESCRIPTION
  Rex OS implementation of the QMI test service.
  
  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/

#include "comdef.h"
#include "msg.h"
#include "err.h"
#include "task.h"
#include "rex.h"
#include <string.h>
#include "qmi_idl_lib.h"
#include "qmi_csi.h"
#include "test_service_v01.h"
#ifdef QMI_PING_SERVICE_SAP
#include "qmi_sap.h"
#endif

#ifdef FEATURE_QMI_NATIVE_REX
typedef unsigned char *rex_stack_pointer_type;
typedef unsigned long rex_task_func_param_type;
#endif

#define QMI_SVC_WAIT_SIG 0x00010000

#define QMI_PING_SERVICE_STACK_SIZE  (2048)
rex_tcb_type        qmi_ping_service_tcb;
rex_stack_word_type qmi_ping_service_stack[QMI_PING_SERVICE_STACK_SIZE];

extern void *qmi_ping_register_service(qmi_csi_os_params *os_params);
static void qmi_ping_service_thread(uint32 handle)
{
  qmi_csi_os_params os_params;
  void *sp;

  os_params.tcb = rex_self();
  os_params.sig = QMI_SVC_WAIT_SIG;

  sp = qmi_ping_register_service(&os_params);

  if(!sp)
  {
    MSG_HIGH("Unable to start ping service!", 0,0,0);
  }
  #ifdef QMI_PING_SERVICE_SAP
  else
  {
    qmi_sap_client_handle qsap_handle;
    if(QMI_SAP_NO_ERR != qmi_sap_register(test_get_service_object_v01(), NULL, &qsap_handle))
    {
      MSG_HIGH("Unable to register ping service with QSAP!\n", 0, 0, 0);
    }
  }
  #endif

  while(1)
  {
    rex_wait(QMI_SVC_WAIT_SIG);
    rex_clr_sigs(os_params.tcb, QMI_SVC_WAIT_SIG);
    if(sp)
    {
      qmi_csi_handle_event(sp, &os_params);
    }
  }
}

void qmi_ping_service_start(void)
{
  rex_def_task_ext( &qmi_ping_service_tcb,
      (rex_stack_pointer_type)&qmi_ping_service_stack,
      QMI_PING_SERVICE_STACK_SIZE,
      10,
      qmi_ping_service_thread,
      0, "QMI_PING_SVC", FALSE );
}
