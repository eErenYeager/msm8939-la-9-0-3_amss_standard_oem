/*===========================================================================
                      I P C    R O U T E R    X A L    S I O 

DESCRIPTION
   This file creates an OS independent implementation of the SIO component
   of Transport Abstraction Layer for the IPC router.

  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_sio.c#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/17/10   ih      Initial revision.

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#define FEATURE_DSM_WM_CB
#include "ipc_router_os.h"
#include "ipc_router_types.h"
#include "dsm.h"
#include "sio.h"
#include "ipc_router_xal.h"
#include "ipc_router_packet.h"
#include "dsm_init.h"
#include "ipc_router_xal_sio.h"

/*===========================================================================
                  CONSTANT / MACRO DACLARATIONS
===========================================================================*/
#define SIO_LO_WM  200
#define SIO_HI_WM  1500
#define SIO_DNE_WM 0x7FFFFFFF

/*===========================================================================
                        TYPE DECLARATIONS
===========================================================================*/
typedef struct
{
  /* SIO related stuff */
  sio_stream_id_type     stream_id;
  dsm_watermark_type     wm_frm_sio;
  dsm_watermark_type     wm_to_sio;
  q_type                 wm_q_frm;
  q_type                 wm_q_to;
  boolean                paused;
  void                  *cb_handle;
  boolean                rx_start;
  dsm_item_type         *rx_leftover;
  ipc_router_xal_sio_ops_type ops;
}
ipc_router_xal_port_type;

/*===========================================================================
  STATIC VARIABLES
  ===========================================================================*/

/*===========================================================================
  INTERNAL FUNCTIONS
  ===========================================================================*/
static dsm_item_type *ipc_router_packet_to_dsm_item(ipc_router_xal_port_type *port, ipc_router_packet_type *pkt)
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  dsm_item_type *item = NULL;
  struct native_packet_s { /* The minimal native packet */
    struct native_packet_s *next;
    uint32 length;
    void *buffer;
  } *i = (struct native_packet_s *)pkt;

  while(i)
  {
    if(dsm_pushdown_tail_long(&item, i->buffer, i->length, port->ops.pool_id) != i->length)
    {
      dsm_free_packet(&item);
      ipc_router_packet_free(&pkt);
      return NULL;
    }
    i = i->next;
  }
  ipc_router_packet_free(&pkt);
  return item;
#else
  return (dsm_item_type *)pkt;
#endif
}

static ipc_router_packet_type *dsm_item_to_ipc_router_packet(dsm_item_type *item)
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  ipc_router_packet_type *pkt = ipc_router_packet_new();
  dsm_item_type *i = item;
  if(!pkt)
  {
    return NULL;
  }
  while(i)
  {
    if(ipc_router_packet_copy_payload(&pkt, i->data_ptr, i->used) != i->used)
    {
      ipc_router_packet_free(&pkt);
      return NULL;
    }
    i = i->pkt_ptr;
  }
  dsm_free_packet(&item);
  return pkt;
#else
  return (ipc_router_packet_type *)item;
#endif
}

/*===========================================================================
  FUNCTION ipc_router_xal_sio_nonempty_cb

  DESCRIPTION
  Process receive queue

  RETURN VALUE
  None

  SIDE EFFECTS
  ===========================================================================*/
static void ipc_router_xal_sio_nonempty_cb
(
 struct dsm_watermark_type_s * wm,
 void *data
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)data;
  dsm_item_type *item;
  
  if(!port || !port->rx_start)
    return;

  if(port->rx_leftover)
  {
    ipc_router_packet_type *pkt = dsm_item_to_ipc_router_packet(port->rx_leftover);
    if(!pkt)
    {
      /* TODO: Print error message */
      return;
    }
    ipc_router_xal_recv(pkt, port->cb_handle);
    port->rx_leftover = NULL;
  }

  while((item = dsm_dequeue(&port->wm_frm_sio)) != NULL)
  {
    ipc_router_packet_type *pkt = dsm_item_to_ipc_router_packet(item);
    if(pkt)
    {
      ipc_router_xal_recv(pkt, port->cb_handle);
    }
    else
    {
      /* TODO: Print error message */
      port->rx_leftover = item;
      break;
    }
  }
}


/*===========================================================================
  FUNCTION ipc_router_xal_sio_dtr_callback

  DESCRIPTION
  Handle DTR callback from SIO and inform any transport errors through the
  registered ipc_router_xal_err_cb.

  RETURN VALUE
  None

  SIDE EFFECTS
  ===========================================================================*/
static void ipc_router_xal_sio_dtr_callback(void *cb_data)
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)cb_data;

  if(port)
  {
    sio_ioctl_param_type param;
    boolean dte_asserted = FALSE;

    param.dte_ready_asserted = &dte_asserted;
    port->ops.ioctl(port->stream_id, SIO_IOCTL_DTE_READY_ASSERTED, &param);

    if (!dte_asserted)
    {
      if(port->paused == FALSE)
      {
        port->paused = TRUE;
        ipc_router_xal_error(port->cb_handle);
        /* TODO: Local cleanup */
      }
    }
    else 
    {
      if(port->paused == TRUE)
      {
        port->paused = FALSE;
        ipc_router_xal_resume(port->cb_handle);
      }
    }
  }
}

/*===========================================================================
  FUNCTION ipc_router_xal_sio_open

  DESCRIPTION
  Open the specified SIO port.

  RETURN VALUE
  Handle to xport

  SIDE EFFECTS
  ===========================================================================*/
static void *ipc_router_xal_sio_open
(
 void *xport_params,
 void *cb_handle
 )
{
  sio_open_type sio_port;
  ipc_router_xal_port_type *port;
  sio_ioctl_param_type param;

  port = (ipc_router_xal_port_type *)ipc_router_os_malloc(sizeof(*port));
  if (NULL == port)
   return NULL;

  ipc_router_os_mem_set(port, 0, sizeof(*port));
  ipc_router_os_mem_set(&sio_port, 0, sizeof(sio_port));

  port->cb_handle = cb_handle;
  port->paused = FALSE;

  /* copy ops table as it may be defined on the stack */
  ipc_router_os_mem_copy(&port->ops, 
      sizeof(ipc_router_xal_sio_ops_type),
      xport_params, 
      sizeof(ipc_router_xal_sio_ops_type));
  if(port->ops.pool_id == 0)
  {
    port->ops.pool_id = DSM_DS_SMALL_ITEM_POOL;
  }

  /* Set up the watermark from sio */
  dsm_queue_init( &port->wm_frm_sio,
      SIO_DNE_WM,
      &port->wm_q_frm );
  port->wm_frm_sio.lo_watermark = SIO_LO_WM;
  port->wm_frm_sio.hi_watermark = SIO_HI_WM;
  port->wm_frm_sio.non_empty_func_ptr = ipc_router_xal_sio_nonempty_cb;
  port->wm_frm_sio.non_empty_func_data = port;

  /* Set up the watermark to sio */
  dsm_queue_init( &port->wm_to_sio,
      SIO_DNE_WM,
      &port->wm_q_to );
  port->wm_to_sio.lo_watermark = SIO_LO_WM;
  port->wm_to_sio.hi_watermark = SIO_HI_WM;

  sio_port.stream_mode    = SIO_GENERIC_MODE;
  sio_port.rx_queue       = &port->wm_frm_sio;
  sio_port.tx_queue       = &port->wm_to_sio;
  sio_port.rx_bitrate     = SIO_BITRATE_BEST;
  sio_port.tx_bitrate     = SIO_BITRATE_BEST;
  sio_port.port_id        = port->ops.port_id;
  sio_port.tail_char_used = FALSE;
  sio_port.tail_char      = 0;
  sio_port.rx_func_ptr    = NULL;
  sio_port.rx_flow        = SIO_FCTL_BEST;
  sio_port.tx_flow        = SIO_FCTL_BEST;

  
  if(SIO_NO_STREAM_ID == (port->stream_id = port->ops.open(&sio_port)))
  {
    IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR, 
        "ipc_router_xal_sio_open: unable to open port %d\n", 
        (int)sio_port.port_id, 0,0);
    ipc_router_os_free(port);
    return NULL;
  }

  port->ops.ioctl(port->stream_id, SIO_IOCTL_INBOUND_FLOW_ENABLE, NULL);
  port->ops.ioctl(port->stream_id, SIO_IOCTL_CD_ASSERT, NULL);
  port->ops.ioctl(port->stream_id, SIO_IOCTL_DSR_ASSERT, NULL);
  port->ops.ioctl(port->stream_id, SIO_IOCTL_RI_DEASSERT, NULL);

  param.enable_dte_ready_event_ext.cb_func = ipc_router_xal_sio_dtr_callback;
  param.enable_dte_ready_event_ext.cb_data = port;
  port->ops.ioctl(port->stream_id, SIO_IOCTL_ENABLE_DTR_EVENT_EXT, &param);

  return port;
}

/*===========================================================================
  FUNCTION ipc_router_xal_sio_close

  DESCRIPTION
  Close the specified SIO port.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED
  IPC_ROUTER_STATUS_TIMEOUT

  SIDE EFFECTS
  ===========================================================================*/
static int ipc_router_xal_sio_close
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  if(port)
  {
    port->ops.close(port->stream_id, NULL);
    /* cheap way of preventing race to nonempty_cb */
    port->rx_start = FALSE;
    ipc_router_xal_closed(port->cb_handle);
    ipc_router_os_free(port);
  }
  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_sio_start_read

  DESCRIPTION
  Start processing rx data for the specified port.

  Once the function is invoked, the registered ipc_router_xal_rx_cb can
  happen at any time, even before this function returns.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static int ipc_router_xal_sio_start_read
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  if (NULL == port)
    return IPC_ROUTER_STATUS_INVALID_PARAM;

  port->rx_start = TRUE;

  /* Now check to see if there is any data in the Rx watermark */
  ipc_router_xal_sio_nonempty_cb(&port->wm_frm_sio, handle);

  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_sio_write

  DESCRIPTION
  Write data to the specified port.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static int ipc_router_xal_sio_write
(
 void *handle,
 ipc_router_packet_type *msg
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  dsm_item_type *item;

  if (!port)
    return IPC_ROUTER_STATUS_INVALID_PARAM;

  item = ipc_router_packet_to_dsm_item(port, msg);
  if(!item)
  {
    return IPC_ROUTER_STATUS_NO_MEM;
  }

  port->ops.transmit(port->stream_id, item);

  return IPC_ROUTER_STATUS_SUCCESS;
}

ipc_router_xal_ops_type ipc_router_xal_sio =
{
   ipc_router_xal_sio_open,
   ipc_router_xal_sio_start_read,
   ipc_router_xal_sio_close,
   ipc_router_xal_sio_write,
};

