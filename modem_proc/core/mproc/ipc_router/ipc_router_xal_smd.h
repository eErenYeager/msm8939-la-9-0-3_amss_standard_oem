#ifndef IPC_ROUTER_XAL_SMD_H
#define IPC_ROUTER_XAL_SMD_H
/*===========================================================================
                      I P C    R O U T E R    X A L    S M D

DESCRIPTION
   This file specifies the interface of an OS independent implementation of
   the SMD component of Transport Abstraction Layer for the IPC router.
   This XAL is currently under deprecation.

  ---------------------------------------------------------------------------
  Copyright (c) 2007-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_smd.h#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include "smd.h"
#include "ipc_router_xal.h"

/*===========================================================================
                        EXPORTED FUNCTION PROTOTYPES
===========================================================================*/
extern ipc_router_xal_ops_type ipc_router_xal_smd;
#endif /* IPC_ROUTER_XAL_SMD_H */
