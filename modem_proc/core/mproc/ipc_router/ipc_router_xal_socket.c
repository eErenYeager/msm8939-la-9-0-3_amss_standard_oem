/*===========================================================================
                   I P C    R O U T E R    X A L    S O C K E T

DESCRIPTION
   This file creates an OS independent implementation of the socket component
   of Transport Abstarction Layer for the IPC router.

Copyright (c) 2007-2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_socket.c#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include <netinet/in.h>
#include <unistd.h>
#include "ipc_router_types.h"
#include "ipc_router_xal.h"
#include <sys/socket.h>
#include <pthread.h>
#include <errno.h>

/*===========================================================================
                  CONSTANT / MACRO DACLARATIONS
===========================================================================*/

/*===========================================================================
                        TYPE DECLARATIONS
===========================================================================*/
typedef struct
{
  /* socket related stuff */
  int sd;
  pthread_t tid;
  void *cb_handle;
  boolean rx_start;
  ipc_router_os_sem lock;
}
ipc_router_xal_port_type;

/*===========================================================================
  STATIC VARIABLES
===========================================================================*/

static int read_exact(int fd, void *_data, unsigned int len)
{
  size_t siz = 0;
  size_t rem = len;
  uint8_t *data = (uint8_t *)_data;
  while(siz < len)
  {
    int ret_sz;
    ret_sz = read(fd, data, rem);
    /* EOF */
    if(ret_sz == 0)
    {
      return -1;
    }
    if(ret_sz < 0)
    {
      if(errno == EINTR)
      {
        continue;
      }
      return -1;
    }
    data += ret_sz;
    siz += ret_sz;
    rem -= ret_sz;
  }
  return 0;
}
static int write_exact(int fd, void *_data, unsigned int len)
{
  size_t siz = 0;
  size_t rem = len;
  uint8_t *data = (uint8_t *)_data;
  while(siz < len)
  {
    int ret_sz = write(fd, data, rem);
    if(ret_sz < 0)
    {
      if(errno == EINTR)
      {
        continue;
      }
      return -1;
    }
    data += ret_sz;
    siz += ret_sz;
    rem -= ret_sz;
  }
  return 0;
}

/*===========================================================================
  FUNCTION ipc_router_xal_socket_open

  DESCRIPTION
    Open the specified socket port.

  RETURN VALUE
    Handle to xport

  SIDE EFFECTS
===========================================================================*/
static void *ipc_router_xal_socket_open
(
 void *xport_params,
 void *cb_handle
 )
{
  ipc_router_xal_port_type *port;
  int sd;
  struct sockaddr_in addr = {0};
  unsigned long port_num = (unsigned long)xport_params;

  if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("Socket");
    goto open_bail;
  }

  port = (ipc_router_xal_port_type *)ipc_router_os_malloc(sizeof(*port));
  if (NULL == port)
    goto open_bail;

  ipc_router_os_mem_set(port, 0, sizeof(*port));

  port->cb_handle = cb_handle;
  port->sd = sd;
  ipc_router_os_sem_init(&port->lock);

  addr.sin_family = AF_INET;
  addr.sin_port = htons((short)port_num); 
  addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  if(connect(sd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
  {
    perror("Connect");
    goto open_unlock_free_bail;
  }

  return port;

open_unlock_free_bail:
  ipc_router_os_free(port);

open_bail:
  return NULL;
}

/*===========================================================================
  FUNCTION ipc_router_xal_socket_close

  DESCRIPTION
  Close the specified socket port.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED
  IPC_ROUTER_STATUS_TIMEOUT

  SIDE EFFECTS
  ===========================================================================*/
static int ipc_router_xal_socket_close
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  if(port)
  {
    shutdown(port->sd, SHUT_RDWR);
    ipc_router_xal_closed(port->cb_handle);
    free(port);
  }
  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_socket_start_read

  DESCRIPTION
  Start processing rx data for the specified port.

  Once the function is invoked, the registered ipc_router_xal_rx_cb can
  happen at any time, even before this function returns.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static char rxbuf[65536];
static void *reader_thread(void *arg)
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)arg;
  uint32_t len;

  while(1)
  {
    ipc_router_packet_type *msg=NULL;
    if(read_exact(port->sd, &len, sizeof(len)))
    {
      perror("Read header");
      ipc_router_xal_error(port->cb_handle);
      return NULL;
    }
    if(len == 0) {
       printf("Xport error! Restarting xport\n");
       ipc_router_xal_error(port->cb_handle);
       ipc_router_xal_resume(port->cb_handle);
       continue;
    }
    if(read_exact(port->sd, rxbuf, len))
    {
      perror("Read payload");
      ipc_router_xal_error(port->cb_handle);
      return NULL;
    } 
    ipc_router_packet_copy_payload(&msg, rxbuf, len);

    /* got the entire packet */
    ipc_router_xal_recv(msg, port->cb_handle);
  }
  return NULL;
}

static int ipc_router_xal_socket_start_read
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;

  if (NULL == port)
  {
    return IPC_ROUTER_STATUS_INVALID_PARAM;
  }

  port->rx_start = TRUE;

  /* Create reader thread */
  pthread_create(&port->tid, NULL, reader_thread, port);
  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_socket_write

  DESCRIPTION
  Write data to the specified port.

  This is an asychronous call.  Once the function is invoked, the
  corresponding ipc_router_xal_tx_cb can happen at any time, even before
  this function returns.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static char txbuf[65536];
static int ipc_router_xal_socket_write
(
 void *handle,
 ipc_router_packet_type *msg
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  uint32 len;
  int rc = IPC_ROUTER_STATUS_INTERRUPTED;

  if (!port)
  {
    return IPC_ROUTER_STATUS_INVALID_PARAM;
  }

  len = ipc_router_packet_length(msg);
  /* write the header */
  ipc_router_os_sem_lock(&port->lock);
  if(write_exact(port->sd, &len, sizeof(uint32_t))) {
    printf("Write failed!\n");
    fflush(stdout);
    goto bailout;
  }
  ipc_router_packet_read(&msg, txbuf, len); 
  if(write_exact(port->sd, txbuf, len)) {
    printf("Write failed!\n");
    fflush(stdout);
    goto bailout;
  }
  rc = IPC_ROUTER_STATUS_SUCCESS;
  ipc_router_packet_free(&msg);

bailout:
  ipc_router_os_sem_unlock(&port->lock);

  return rc;
}

ipc_router_xal_ops_type ipc_router_xal_socket =
{
  ipc_router_xal_socket_open,
  ipc_router_xal_socket_start_read,
  ipc_router_xal_socket_close,
  ipc_router_xal_socket_write,
};

