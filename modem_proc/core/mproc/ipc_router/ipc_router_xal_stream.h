#ifndef IPC_ROUTER_XAL_STREAM_H
#define IPC_ROUTER_XAL_STREAM_H
/*===========================================================================
                      I P C    R O U T E R    X A L

DESCRIPTION

Copyright (c) 2007, 2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_stream.h#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include "ipc_router_xal.h"

/*===========================================================================
                        EXPORTED FUNCTION PROTOTYPES
===========================================================================*/
extern ipc_router_xal_ops_type ipc_router_xal_stream;
#endif /* IPC_ROUTER_XAL_STREAM_H */
