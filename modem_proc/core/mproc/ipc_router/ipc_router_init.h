#ifndef IPC_ROUTER_INIT_H
#define IPC_ROUTER_INIT_H
/*===========================================================================

                      I P C    R O U T E R    I N I T
                          H E A D E R    F I L E

   This file describes the OS-dependent interface to initialize the IPC Router.

  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_init.h#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

void ipc_router_init(void);

#endif
