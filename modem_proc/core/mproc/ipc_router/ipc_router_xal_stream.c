/*===========================================================================
                   I P C    R O U T E R    X A L

DESCRIPTION
   This file creates an OS independent implementation of the stream component
   of Transport Abstarction Layer for the IPC router.

Copyright (c) 2007-2009 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_stream.c#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "ipc_router_types.h"
#include "ipc_router_xal.h"
#include <pthread.h>
#include <errno.h>

/*===========================================================================
                  CONSTANT / MACRO DACLARATIONS
===========================================================================*/

/*===========================================================================
                        TYPE DECLARATIONS
===========================================================================*/
typedef struct
{
  /* stream related stuff */
  int fd;
  pthread_t tid;
  void *cb_handle;
  boolean rx_start;
  ipc_router_os_sem lock;
} ipc_router_xal_port_type;

/*===========================================================================
  STATIC VARIABLES
===========================================================================*/
/*===========================================================================
  FUNCTION ipc_router_xal_stream_open

  DESCRIPTION
    Open the specified stream port.

  RETURN VALUE
    Handle to xport

  SIDE EFFECTS
===========================================================================*/
static void *ipc_router_xal_stream_open
(
 void *xport_params,
 void *cb_handle
 )
{
  ipc_router_xal_port_type *port;
  int fd;

  if((fd = open((char *)xport_params, O_RDWR, 0)) < 0)
  {
    perror("open");
    goto open_bail;
  }

  port = (ipc_router_xal_port_type *)ipc_router_os_malloc(sizeof(*port));
  if (NULL == port)
    goto open_bail;

  ipc_router_os_mem_set(port, 0, sizeof(*port));

  port->cb_handle = cb_handle;
  port->fd = fd;
  ipc_router_os_sem_init(&port->lock);

  return port;

open_bail:
  return NULL;
}

/*===========================================================================
  FUNCTION ipc_router_xal_stream_close

  DESCRIPTION
  Close the specified stream port.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED
  IPC_ROUTER_STATUS_TIMEOUT

  SIDE EFFECTS
  ===========================================================================*/
static int ipc_router_xal_stream_close
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  if(port)
  {
    close(port->fd);
    ipc_router_xal_closed(port->cb_handle);
    free(port);
  }
  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_stream_start_read

  DESCRIPTION
  Start processing rx data for the specified port.

  Once the function is invoked, the registered ipc_router_xal_rx_cb can
  happen at any time, even before this function returns.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static char buf[65536];
static void *reader_thread(void *arg)
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)arg;
  uint32_t len;

  printf("IPC reader started\n");
  while(1)
  {
    len = read(port->fd, buf, sizeof(buf));
    printf("Read %d bytes\n", len);
    if(len <= 0)
    {
      ipc_router_xal_error(port->cb_handle);
      ipc_router_xal_resume(port->cb_handle);
    }
    else
    {
      ipc_router_packet_type *msg=NULL;
      ipc_router_packet_copy_payload(&msg, buf, len);
      ipc_router_xal_recv(msg, port->cb_handle);
    }
  }
  return NULL;
}

static int ipc_router_xal_stream_start_read
(
 void *handle
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;

  if (NULL == port)
  {
    return IPC_ROUTER_STATUS_INVALID_PARAM;
  }

  port->rx_start = TRUE;

  /* Create reader thread */
  pthread_create(&port->tid, NULL, reader_thread, port);
  return IPC_ROUTER_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION ipc_router_xal_stream_write

  DESCRIPTION
  Write data to the specified port.

  This is an asychronous call.  Once the function is invoked, the
  corresponding ipc_router_xal_tx_cb can happen at any time, even before
  this function returns.

  RETURN VALUE
  IPC_ROUTER_STATUS_SUCCESS
  IPC_ROUTER_STATUS_INVALID_PARAM
  IPC_ROUTER_STATUS_INTERRUPTED

  SIDE EFFECTS
  ===========================================================================*/
static char outbuf[65536];
static int ipc_router_xal_stream_write
(
 void *handle,
 ipc_router_packet_type *msg
 )
{
  ipc_router_xal_port_type *port = (ipc_router_xal_port_type *)handle;
  uint32 len;

  if (!port)
    return IPC_ROUTER_STATUS_INVALID_PARAM;

  ipc_router_os_sem_lock(&port->lock);
  len = ipc_router_packet_length(msg);
  printf("Write %d bytes\n", len);
  ipc_router_packet_read(&msg, outbuf, len);

  if(write(port->fd, outbuf, len) != len)
  {
    ipc_router_os_sem_unlock(&port->lock);
    perror("write");
    return IPC_ROUTER_STATUS_IO_ERROR;
  }

  ipc_router_os_sem_unlock(&port->lock);
 
  ipc_router_packet_free(&msg);

  return IPC_ROUTER_STATUS_SUCCESS;
}

ipc_router_xal_ops_type ipc_router_xal_stream =
{
  ipc_router_xal_stream_open,
  ipc_router_xal_stream_start_read,
  ipc_router_xal_stream_close,
  ipc_router_xal_stream_write,
};

