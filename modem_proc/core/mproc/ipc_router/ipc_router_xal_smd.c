/*===========================================================================
                      I P C    R O U T E R    X A L    S M D

DESCRIPTION
   This file creates an OS independent implementation of the SMD component
   of Transport Abstraction Layer for the IPC router.
   This XAL is currently under deprecation.

  ---------------------------------------------------------------------------
  Copyright (c) 2007-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/mproc/ipc_router/ipc_router_xal_smd.c#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/02/07   sa      Initial revision.

===========================================================================*/


/*===========================================================================
                          INCLUDE FILES
===========================================================================*/
#include "ipc_router_types.h"
#include "ipc_router_xal.h"
#include "ipc_router_protocol.h"
#include "smd.h"
#include "dsm.h"
#include "assert.h"

/*===========================================================================
                  CONSTANT / MACRO DACLARATIONS
===========================================================================*/
#define SMD_RXTX_HEADER         0
#define SMD_RXTX_PACKET         1

/*===========================================================================
                        TYPE DECLARATIONS
===========================================================================*/
typedef struct
{
  smd_port_id_type port_id;
  q_type tx_q;
  dsm_item_type *tx_buf;

  dsm_item_type *rx_head;
  dsm_item_type *rx_buf;
  int rx_state;
  int rx_len;
  byte *rx_ptr;
  ipc_router_header_type *rx_header;

  void *cb_handle;
  boolean rx_start;
} ipc_router_xal_smd_port_type;

/*===========================================================================
                        STATIC VARIABLES
===========================================================================*/

/*===========================================================================
                        INTERNAL FUNCTIONS
===========================================================================*/
#ifndef IPC_ROUTER_NATIVE_PACKET
/*===========================================================================
FUNCTION ipc_router_xal_smd_rx_cb

DESCRIPTION
   Handle rx callback from SMD and invoke XAL callback.

RETURN VALUE
   None

SIDE EFFECTS
===========================================================================*/
static void ipc_router_xal_smd_rx_cb
(
 void              *cb_data,
 uint32             bytes_read,
 byte             **buf_ptr,
 uint32            *size
 )
{
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)cb_data;
  uint32 left;

  if (!port || !port->rx_start)
  {
    *size = 0;
    *buf_ptr = NULL;
    return;
  }

  /* Default on error */
  *size = 0;
  *buf_ptr = NULL;

  switch(port->rx_state)
  {
    case SMD_RXTX_HEADER:
      if(bytes_read)
      {
        if(bytes_read != port->rx_len)
        {
          IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
              "Dropping unexpected partial packet exp:%d received:%d\n",
              port->rx_len, bytes_read, 0);
          ipc_router_packet_free((ipc_router_packet_type **)&port->rx_head);
          port->rx_head = port->rx_buf = NULL;
        }
        else
        {
          port->rx_buf->used += bytes_read;
          /* Hand off message buffer for processing */
          ipc_router_xal_recv((ipc_router_packet_type *)port->rx_head, 
              port->cb_handle);
        }
      }
      port->rx_head = port->rx_buf = (dsm_item_type *)ipc_router_packet_new();
      if(!port->rx_head)
      {
        IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
            "Packet allocation failed!\n", 0,0,0);
        break;
      }

      *buf_ptr = port->rx_ptr = port->rx_buf->data_ptr;
      *size = port->rx_len = sizeof(ipc_router_header_type);
      port->rx_header = NULL;
      port->rx_state = SMD_RXTX_PACKET;
      break;

    case SMD_RXTX_PACKET:
      if(bytes_read == 0)
      {
        /* did not actually get the header, possible due to port closure */
        /* try to recover and stay in the same state */
        *buf_ptr = port->rx_buf->data_ptr;
        *size = port->rx_len = sizeof(ipc_router_header_type);
        return;
      }

      /* march pointers forward */
      port->rx_buf->used += bytes_read;

      if(!port->rx_header)
      {
        /* got the header */
        if(bytes_read != port->rx_len)
        {
          IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
            "Unexpected bytes in header read: exp:%d read:%d!\n",
            port->rx_len, bytes_read, 0);
          break;
        }
        port->rx_header = (ipc_router_header_type *)port->rx_ptr;
        port->rx_ptr += bytes_read;
        port->rx_len = (port->rx_header->msg_size + 3) & (~0x3L);
      }
      else
      {
        /* already have the header & still reading pkt, allocate new item */
        dsm_item_type *new_item = (dsm_item_type *)ipc_router_packet_new();
        if(!new_item)
        {
          IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
            "Packet allocation failed!\n", 0,0,0);
          break;
        }
        if(bytes_read >= port->rx_len)
        {
          /* We should never be here. 
             bytes_read > port->rx_len means that SMD overrun our buffer!
             bytes_read == port->rx_len means we read the entire packet
             which would mean we should be in SMD_RXTX_HEADER state and
             not the packet state */
          IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
            "SMD Wrote more than required exp: less than %d wrote:%d!\n",
            port->rx_len, bytes_read, 0);
          break;
        }
        port->rx_len -= bytes_read;
        port->rx_buf = new_item;
        port->rx_ptr = new_item->data_ptr;
        dsm_append(&port->rx_head, &new_item);
      }
      
      left = port->rx_buf->size - port->rx_buf->used;
      if(port->rx_len > left)
      {
        /* too much data to fit into the item */
        *size = left;
      }
      else
      {
        /* everything fit, go back to the original state */
        *size = port->rx_len;
        port->rx_state = SMD_RXTX_HEADER;
      }
      *buf_ptr = port->rx_ptr;
      break;
    default:
      break;
  }
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_tx_cb

DESCRIPTION
   Handle tx callback from SMD and invoke XAL callback.

RETURN VALUE
   None

SIDE EFFECTS
===========================================================================*/
static void ipc_router_xal_smd_tx_cb
(
 void              *cb_data,
 byte             **buf_ptr,
 uint32            *size
 )
{
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)cb_data;

  if(port->tx_buf)
  {
    /* just transmitted a packet - go to the next one and free this one */
    dsm_item_type *to_free = port->tx_buf;
    port->tx_buf = port->tx_buf->pkt_ptr;
    dsm_free_buffer(to_free);
  }

  if(!port->tx_buf)
  {
    port->tx_buf = q_get(&port->tx_q);
    if(!port->tx_buf)
    {
      /* nothing to send */
      *buf_ptr = NULL;
      *size = 0;
      return;
    }
  }

  /* got something to send */
  *buf_ptr = port->tx_buf->data_ptr;
  *size = port->tx_buf->used;
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_flowctl_cb

DESCRIPTION
   Handle flow control callback from SMD.

RETURN VALUE
   0

SIDE EFFECTS
===========================================================================*/
static uint32 ipc_router_xal_smd_flowctl_cb
(
 void            *cb_data,
 uint32           byte_count
)
{
  return 0;
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_dtr_callback

DESCRIPTION
   Handle DTR callback from SMD and inform any transport errors through the
   registered ipc_router_xal_err_cb.

RETURN VALUE
   None

SIDE EFFECTS
===========================================================================*/
static void ipc_router_xal_smd_dtr_callback(void *cb_data)
{
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)cb_data;

  if (NULL != port)
  {
    smd_ioctl_param_type param;
    boolean              dte_asserted = FALSE;

    param.dte_ready_asserted = &dte_asserted;
    smd_ioctl(port->port_id, SMD_IOCTL_DTE_READY_ASSERTED, &param);

    if (!dte_asserted)
    {
      dsm_item_type *item;

      /* Request ipc router to cleanup */
      ipc_router_xal_error(port->cb_handle);

      /* Cleanup local storage */

      port->rx_start = FALSE; /* temp disable recv */
      port->rx_state = SMD_RXTX_HEADER;
      port->rx_header = NULL;
      port->rx_len = 0;

      /* free rx cached buffers */
      if(port->rx_head) {
        dsm_free_packet(&port->rx_head);
      }
      port->rx_head = NULL;
      port->rx_buf = NULL;
      port->rx_ptr = NULL;

      /* free the tx cached buffers */
      if(port->tx_buf) {
        dsm_free_packet(&port->tx_buf);
      }
      port->tx_buf = NULL;

      /* Flush the tx queue and drop all of them */
      while(NULL != (item = q_get(&port->tx_q))) {
        dsm_free_packet(&item);
      }

      /* Allow RX again assume SMD has done the right thing 
       * and cleaned up all the internal states */
      port->rx_start = TRUE;
    }
    else
    {
      ipc_router_xal_resume(port->cb_handle);
    }
  }
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_close_cb

DESCRIPTION
   Handle close callback from SMD and signal that the port is closed.

RETURN VALUE
   None

SIDE EFFECTS
===========================================================================*/
static void ipc_router_xal_smd_close_cb
(
 void *data
 )
{
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)data;

  if (NULL != port)
  {
    ipc_router_xal_closed(port->cb_handle);
    ipc_router_os_free(port);
  }
}
#endif

/*===========================================================================
FUNCTION ipc_router_xal_smd_open

DESCRIPTION
   Open the specified SMD port.

RETURN VALUE
   Xport handle or NULL on error

SIDE EFFECTS
===========================================================================*/
static void *ipc_router_xal_smd_open
(
 void *xport_params,
 void *cb_handle
)
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  IPC_ROUTER_OS_MESSAGE(IPC_ROUTER_MSG_ERR,
      "ipc_router_xal_smd: Not supported on native packet interface.\n",0,0,0);
  return NULL;
#else
   smd_port_id_type port_id; 
   ipc_router_xal_smd_port_type *port;
   smd_ioctl_param_type param;

   port = (ipc_router_xal_smd_port_type *)ipc_router_os_malloc(sizeof(*port));
   if (NULL == port)
   {
     return NULL;
   }

   ipc_router_os_mem_set(port, 0, sizeof(*port));

   q_init(&port->tx_q);

   port->cb_handle = cb_handle;

   port->port_id = port_id = smd_open_memcpy_2(
       (const char *)xport_params, 
       SMD_APPS_MODEM, 
       ipc_router_xal_smd_tx_cb, port,
       ipc_router_xal_smd_rx_cb, port, 
       ipc_router_xal_smd_flowctl_cb, port);

   ipc_router_os_mem_set(&param, 0, sizeof(param));

   param.full_bufs_asserted = TRUE;
   smd_ioctl(port_id, SMD_IOCTL_SET_FULL_RCV_BUFS, &param);

   smd_ioctl(port_id, SMD_IOCTL_INBOUND_FLOW_ENABLE, NULL);
   smd_ioctl(port_id, SMD_IOCTL_CD_ASSERT, NULL);
   smd_ioctl(port_id, SMD_IOCTL_DSR_ASSERT, NULL);
   smd_ioctl(port_id, SMD_IOCTL_RI_DEASSERT, NULL);

   param.enable_dte_ready_event_ext.cb_func = ipc_router_xal_smd_dtr_callback;
   param.enable_dte_ready_event_ext.cb_data = port;
   smd_ioctl(port_id, SMD_IOCTL_ENABLE_DTR_EVENT_EXT, &param);

   return (void *)port;
#endif
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_close

DESCRIPTION
   Close the specified SMD port.

RETURN VALUE
   IPC_ROUTER_STATUS_SUCCESS
   IPC_ROUTER_STATUS_INVALID_PARAM
   IPC_ROUTER_STATUS_INTERRUPTED
   IPC_ROUTER_STATUS_TIMEOUT

SIDE EFFECTS
===========================================================================*/
static int ipc_router_xal_smd_close
(
 void *handle
)
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  return IPC_ROUTER_STATUS_FAILURE;
#else
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)handle;

  port->rx_start = FALSE;
  smd_close(port->port_id, ipc_router_xal_smd_close_cb, port, 0);

  return IPC_ROUTER_STATUS_SUCCESS;
#endif
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_start_read

DESCRIPTION
   Start processing rx data for the specified port.

   Once the function is invoked, the registered ipc_router_xal_rx_cb can
   happen at any time, even before this function returns.

RETURN VALUE
   IPC_ROUTER_STATUS_SUCCESS
   IPC_ROUTER_STATUS_INVALID_PARAM
   IPC_ROUTER_STATUS_INTERRUPTED

SIDE EFFECTS
===========================================================================*/
static int ipc_router_xal_smd_start_read
(
 void *handle 
 )
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  return IPC_ROUTER_STATUS_FAILURE;
#else
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)handle;

  if (NULL == port)
  {
    return IPC_ROUTER_STATUS_INVALID_PARAM;
  }

  port->rx_start = TRUE;

  smd_notify_read(port->port_id);

  return IPC_ROUTER_STATUS_SUCCESS;
#endif
}

/*===========================================================================
FUNCTION ipc_router_xal_smd_write

DESCRIPTION
   Write data to the specified port.

   This is an asychronous call.  Once the function is invoked, the
   corresponding ipc_router_xal_tx_cb can happen at any time, even before
   this function returns.

RETURN VALUE
   IPC_ROUTER_STATUS_SUCCESS
   IPC_ROUTER_STATUS_INVALID_PARAM
   IPC_ROUTER_STATUS_INTERRUPTED

SIDE EFFECTS
===========================================================================*/
static int ipc_router_xal_smd_write
(
 void *handle,
 ipc_router_packet_type *msg
 )
{
#ifdef IPC_ROUTER_NATIVE_PACKET
  return IPC_ROUTER_STATUS_FAILURE;
#else
  ipc_router_xal_smd_port_type *port = (ipc_router_xal_smd_port_type *)handle;
  dsm_item_type *item = (dsm_item_type *)msg;

  if (NULL == port)
  {
    return IPC_ROUTER_STATUS_INVALID_PARAM;
  }

  q_put(&port->tx_q, &item->link);

  smd_notify_write(port->port_id);

  return IPC_ROUTER_STATUS_SUCCESS;
#endif
}

ipc_router_xal_ops_type ipc_router_xal_smd =
{
  ipc_router_xal_smd_open,
  ipc_router_xal_smd_start_read,
  ipc_router_xal_smd_close,
  ipc_router_xal_smd_write,
};
