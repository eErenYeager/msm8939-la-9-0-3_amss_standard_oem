#ifndef MEMHEAP_V_H
#define MEMHEAP_V_H
/**
  @file memheap_v.h
  @brief 
    Contains the heap manager internal interface.

*/
/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/services/utils/src/memheap_v.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/07/14   rks      Initial Version
===========================================================================*/


/*=============================================================================

                           INCLUDE FILES

=============================================================================*/



/*=============================================================================

                        MACRO DEFINITIONS

=============================================================================*/




/*=============================================================================

                             FUNCTION DEFINITIONS

=============================================================================*/


/** 
Create a heap region of requested size 

  @param[in] size         size of the region to be cearted .
  @param[in] mem_region   mem_region variable refrence

@return
returns the heap region address if successful or  NULL if failure.

@dependencies
None.
*/
void* heap_create_region(uint32 size, qurt_mem_region_t *mem_region);

#endif /* MEMHEAP_V_H */
