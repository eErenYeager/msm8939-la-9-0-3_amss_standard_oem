#ifndef QMI_SSREQ_CLIENT_H
#define QMI_SSREQ_CLIENT_H
/*===========================================================================

           QMI_SSREQ_CLIENT . H

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/debugtools/sys_m/src/qmi_ssreq_client.h#1 $

/*   when       who     what, where, why
   --------   ---     ---------------------------------------------------------- 
   10/09/14   rks     added API ssreq_ser_check_for_shutdown_support()
   06/26/14   rks      Initial Version
   
   ===========================================================================*/

/*===========================================================================
   
                        INCLUDE FILES FOR MODULE
   
   ===========================================================================*/

#include "qmi_csi.h"
#include "sys_m_reset.h"
#include "subsystem_request_v01.h"

typedef enum _ssreq_cmd_type{
   SSREQ_SYSTEM_SHUTDOWN    = QMI_SSREQ_SYSTEM_SHUTDOWN_REQ_V01,
   SSREQ_SYSTEM_RESTART     = QMI_SSREQ_SYSTEM_RESTART_REQ_V01,
   SSREQ_PERIPHERAL_RESTART = QMI_SSREQ_PERIPHERAL_RESTART_REQ_V01,
}ssreq_cmd_type;

/*===========================================================================

  FUNCTION:  ssreq_client_init

===========================================================================*/
/*!
    @brief
    Initialize the SSREQ QMI client for sending request to the SSREQ service 
    on APSS

    @detail

    @return
    SSREQ_SUCCESS : Initialization was succesful
    SSREQ_FAILURE : Initialization failed

    *Note*: This API is used only in sys_m.c and called inside SYS_M_LOCK lock
    So right now do not have its own mutex created for concurrency protection
*/
/*=========================================================================*/

ssreq_status_t ssreq_client_init(void);

/*===========================================================================

  FUNCTION:  ssreq_process_request

===========================================================================*/
/*!
    @brief
    Send a request to the SSREQ server at APSS to process SSREQ cmd

    @detail

    @return
    SSREQ_SUCCESS   : request was succesful
    SSREQ_FAILURE   : request failed
    SSREQ_IN_PROCESS: already a request is in process
    
    *Note*: This API is used only in sys_m.c and called inside SYS_M_LOCK lock
    So right now do not have its own mutex created for concurrency protection
*/
/*=========================================================================*/
ssreq_status_t ssreq_process_request(ssreq_cmd_type cmd, ssreq_qmi_ind_cb_fn_type *ind_cb_fn);

/*===========================================================================

  FUNCTION:  ssreq_ser_check_for_shutdown_support

===========================================================================*/

/*!
   @brief
    Check if SSREQ Service on HLOS supports the shutdown request or not
    based on the SSREQ IDL minor version

    @detail

    @return
    TRUE  : if supports
    FALSE : if don't support

*/
boolean ssreq_ser_check_for_shutdown_support(void);

#endif  /* QMI_SSREQ_CLIENT_H */
