/*============================================================================
  @file qmi_ssreq_client.c

  Sub System Request(SSREQ) QMI cleint implementation

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/debugtools/sys_m/src/lib/mpss/qmi_ssreq_client.c#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/09/14   rks     added API ssreq_ser_check_for_shutdown_support()
06/26/14   rks     Initial Version

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "subsystem_request_v01.h"
#include "msg.h"
#include "sys_m.h"
#include "qmi_client.h"
#include "qmi_cci_target_ext.h"
#include "qmi_ssreq_client.h"
#include "timer.h"
#include "DDIChipInfo.h"
#include "DDIPlatformInfo.h"
#include "sys_m_reset.h"

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#define QMI_CLNT_WAIT_SIG      (0x1 << 5)
#define QMI_CLNT_TIMER_SIG     (0x1 << 6)
#define QMI_TIMEOUT_MS 5000
#define SSREQ_REQ_TIMEOUT_MS 1000
#define SSREQ_SERVICE_MINOR_VER_FOR_SHUTDOWN_SUPPORT 99

static qmi_idl_service_object_type   ssreq_service_object;
static qmi_cci_os_signal_type        os_params;
static qmi_client_type               ssreq_client_handle,ssreq_notifier;
static qmi_service_info              info;
static boolean                ssreq_client_init_status = FALSE;
boolean                       ssreq_req_in_process = FALSE;

/*store the function pointer to be called once the indication received 
  for the SSREQ QMI request*/
ssreq_qmi_ind_cb_fn_type      *ssreq_qmi_ind_cb = NULL;

static timer_type             ssreq_ind_timer;
static timer_group_type       ssreq_timer_group = {0};

/*===========================================================================

  FUNCTION:  ssreq_client_ind_cb

===========================================================================*/
/*!
    @brief
    Helper function for the client indication callback

    @detail
    This function send back the indication result to the 
    orignal request initiator client if registered the
    ssreq_qmi_ind_cb callback

    @return
    None

*/
/*=========================================================================*/

void ssreq_client_ind_cb
(
 qmi_client_type                client_handle,
 unsigned int                   msg_id,
 void                           *ind_buf,
 unsigned int                   ind_buf_len,
 void                           *ind_cb_data
)
{
   void *ind_msg;
   timetick_type responce_time = 0;
   qmi_client_error_type qmi_err;
   uint32_t decoded_size;
   qmi_ssreq_system_shutdown_ind_msg_v01* indication = NULL;

   responce_time = timer_clr(&ssreq_ind_timer,T_MSEC);

   MSG_3(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ:ssreq_client_ind_cb request client_handle = %d, msg_id =%d, responce_time = 0x%x",client_handle,msg_id,responce_time);
  
   qmi_err = qmi_idl_get_message_c_struct_len(
                ssreq_service_object,
                QMI_IDL_INDICATION, msg_id, &decoded_size
             );

   if( QMI_NO_ERR != qmi_err )
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:Received error from QMI framework call %d", qmi_err);
      return ;
   }

   ind_msg = malloc(decoded_size);
   if(!ind_msg) 
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:indication buff allocation failed");
      return;
   }
   qmi_err = qmi_client_message_decode( client_handle, QMI_IDL_INDICATION,
               msg_id, ind_buf, ind_buf_len, ind_msg, decoded_size );

   if (qmi_err != QMI_NO_ERR)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:Received error from QMI framework call %d" , qmi_err);
      free(ind_msg);
      return;
   }

   indication = (qmi_ssreq_system_shutdown_ind_msg_v01*)ind_msg;

   if(ssreq_qmi_ind_cb){
      ssreq_ind_result res = SSREQ_REQUEST_NOT_SERVED;

      if(indication->status == SSREQ_QMI_REQUEST_SERVICED_V01){
         res = SSREQ_REQUEST_SERVED;
      }

      ssreq_qmi_ind_cb(res);/*call the function registered by the client to indicate the result 
                                 of requested QMI command */
      ssreq_qmi_ind_cb = NULL; /*set the call back to NULL for this particlur client after the request is completed */
   }
        
   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ:ssreq_client_ind_cb request status = %d",indication->status);
   free(ind_msg);
   ssreq_req_in_process = FALSE;
   return;
}
/*===========================================================================

  FUNCTION:  ssreq_request_timout_fn

===========================================================================*/
/*!
    @brief
    Callback function called once the request timeout occure

    @detail
    Set ssreq_req_in_process to FALSE so that next requests can be processed
    And notify to the user via its registered callback.
    
    @return
    None

*/
/*=========================================================================*/
void ssreq_request_timout_fn(timer_cb_data_type data)
{
   MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ indication time out");
   if(ssreq_qmi_ind_cb){
      ssreq_qmi_ind_cb(SSREQ_REQUEST_NOT_SERVED);/*call the function registered by the client to indicate the result 
                                                            of requested QMI command */
      ssreq_qmi_ind_cb = NULL; /*set the call back to NULL for this particlur client after the request is completed */

      ssreq_req_in_process = FALSE;
   }
}
/*===========================================================================

  FUNCTION:  ssreq_client_init

===========================================================================*/
/*!
    @brief
    Initialize the SSREQ QMI client for sending request to the SSREQ service
    on APSS

    @detail

    @return
    SSREQ_SUCCESS : Initialization was succesful
    SSREQ_FAILURE : Initialization failed
    
    *Note*: This API is used only in sys_m.c and called inside SYS_M_LOCK lock
    So right now do not have its own mutex created for concurrency protection
*/
/*=========================================================================*/

ssreq_status_t ssreq_client_init(void)
{
   unsigned int num_services;
   unsigned int ser_instance_id = SSREQ_QMI_SERVICE_INSTANCE_APSS_V01 + SYSM_CHIP_ID * QMI_SSREQ_SERVICE_INSTANCE_OFFSET_V01;
   os_params.tcb = rex_self();
   os_params.sig = QMI_CLNT_WAIT_SIG;
   os_params.timer_sig = QMI_CLNT_TIMER_SIG;
   qmi_client_error_type ret;

   if(ssreq_client_init_status){
      return SSREQ_SUCCESS;
   }

   ssreq_service_object = ssreq_get_service_object_v01();
   if (!ssreq_service_object)
   {
      MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:ssreq_service_object in NULL \n");
      return SSREQ_FAILURE;
   }

   /* Initialize the qmi client notifier */
   ret = qmi_client_notifier_init(ssreq_service_object, &os_params, &ssreq_notifier);
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:qmi_client_notifier_init ret = %d ",ret);
      return SSREQ_FAILURE;
   }

   /* wait for server to come up */
   QMI_CCI_OS_SIGNAL_WAIT(&os_params, QMI_TIMEOUT_MS);
   if(os_params.timed_out)
   {
       MSG(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: Service Discovery timeout");
       qmi_client_release(ssreq_notifier);
       return SSREQ_FAILURE;
   }
   QMI_CCI_OS_SIGNAL_CLEAR(&os_params);

   ret = qmi_client_get_service_list(ssreq_service_object, NULL, NULL, &num_services);
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:qmi_client_get_service_list ret = %d ",ret);
      qmi_client_release(ssreq_notifier);
      return SSREQ_FAILURE;
   }

   ret = qmi_client_get_service_instance( ssreq_service_object,ser_instance_id, &info);
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ:qmi_client_get_service_instance returned failure ret = %d",ret);
      qmi_client_release(ssreq_notifier);
      return SSREQ_FAILURE;
   }

   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ ser_instance_id found %d",ser_instance_id);

   ret = qmi_client_init(&info, ssreq_service_object, ssreq_client_ind_cb, NULL, &os_params, &ssreq_client_handle);
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: qmi_client_init returned failure ret = %d\n",ret);
      qmi_client_release(ssreq_notifier);
      return SSREQ_FAILURE;
   }
   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ qmi_client_init SUCCESS \n");

   ssreq_client_init_status = TRUE;
   timer_def(&ssreq_ind_timer, &ssreq_timer_group, NULL, 0, &ssreq_request_timout_fn, 0);
   return SSREQ_SUCCESS;
}

/*===========================================================================

  FUNCTION:  ssreq_shutdown_system

===========================================================================*/
/*!
    @brief
    Send a request to the server to shutdown system

    @detail
    

    @return
    SSREQ_SUCCESS : request for memory was succesful
    SSREQ_FAILURE: request for memory failed

*/
/*=========================================================================*/
static ssreq_status_t ssreq_shutdown_system(void)
{
   int ret = 0;
   qmi_ssreq_system_shutdown_req_msg_v01 ssreq_shutdown_req;
   qmi_ssreq_system_shutdown_resp_msg_v01 ssreq_shutdown_resp;
   ssreq_shutdown_req.ss_client_id = SSREQ_QMI_CLIENT_INSTANCE_MPSS_V01;


   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ ssreq_shutdown_system\n");

   /* QMI call to send shutdown request */
   ret = qmi_client_send_msg_sync(ssreq_client_handle, SSREQ_SYSTEM_SHUTDOWN,
                                  (void*)&ssreq_shutdown_req,sizeof(ssreq_shutdown_req),
                                  (void*)&ssreq_shutdown_resp,sizeof(ssreq_shutdown_resp),0 );
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR ret= %d\n",ret);
      return SSREQ_FAILURE;
   }

   if(ssreq_shutdown_resp.resp.result != QMI_RESULT_SUCCESS_V01)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR , responce = %d\n",ssreq_shutdown_resp.resp.error);
      return SSREQ_FAILURE;
   }

   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ Shutdown Success\n");

   return SSREQ_SUCCESS;
}


/*===========================================================================

  FUNCTION:  ssreq_restart_system

===========================================================================*/
/*!
    @brief
    Send a request to the server to restart system

    @detail

    @return
    SSREQ_SUCCESS : request for memory was succesful
    SSREQ_FAILURE: request for memory failed

*/
/*=========================================================================*/
static ssreq_status_t ssreq_restart_system(void)
{
   int ret = 0;
   qmi_ssreq_system_restart_req_msg_v01 ssreq_restart_req;
   qmi_ssreq_system_restart_resp_msg_v01 ssreq_restart_resp;
   ssreq_restart_req.ss_client_id = SSREQ_QMI_CLIENT_INSTANCE_MPSS_V01;

   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ ssreq_restart_system\n");

   /* QMI call to send restart request */
   ret = qmi_client_send_msg_sync(ssreq_client_handle, SSREQ_SYSTEM_RESTART,
                                  (void*)&ssreq_restart_req,sizeof(ssreq_restart_req),
                                  (void*)&ssreq_restart_resp,sizeof(ssreq_restart_resp),0 );
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR ret= %d\n",ret);
      return SSREQ_FAILURE;
   }

   if(ssreq_restart_resp.resp.result != QMI_RESULT_SUCCESS_V01)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR , ssreq_restart_resp.resp = %d\n",ssreq_restart_resp.resp.error);
      return SSREQ_FAILURE;
   }

   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ restart success");

  return SSREQ_SUCCESS;
}

/*===========================================================================

  FUNCTION:  ssreq_restart_peripheral

===========================================================================*/
/*!
    @brief
    Send a request to the server to restart system

    @detail

    @return
    ssreq_SUCCESS : request for memory was succesful
    ssreq_FAILURE: request for memory failed

*/
/*=========================================================================*/
static ssreq_status_t ssreq_restart_peripheral(void)
{
   int ret = 0;
   qmi_ssreq_peripheral_restart_req_msg_v01 ssreq_p_restart_req;
   qmi_ssreq_peripheral_restart_resp_msg_v01 ssreq_p_restart_resp;
   ssreq_p_restart_req.ss_client_id = SSREQ_QMI_CLIENT_INSTANCE_MPSS_V01;

   /* QMI call to send restart request */
   ret = qmi_client_send_msg_sync(ssreq_client_handle, SSREQ_PERIPHERAL_RESTART,
                                  (void*)&ssreq_p_restart_req,sizeof(ssreq_p_restart_req),
                                  (void*)&ssreq_p_restart_resp,sizeof(ssreq_p_restart_resp),0 );
   if(QMI_NO_ERR != ret)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR ret= %d\n",ret);
      return SSREQ_FAILURE;
   }

   if(ssreq_p_restart_resp.resp.result != QMI_RESULT_SUCCESS_V01)
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: ERROR , responce = %d\n",ssreq_p_restart_resp.resp.error);
      return SSREQ_FAILURE;
   }

   MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ peripheral restart success");

  return SSREQ_SUCCESS;
}

/*===========================================================================

  FUNCTION:  ssreq_process_request

===========================================================================*/
/*!
    @brief
    Send a request to the ssreq server at APSS to process ssreq cmd

    @detail

    @return
    SSREQ_SUCCESS   : request for memory was succesful
    SSREQ_FAILURE   : request for memory failed
    SSREQ_IN_PROCESS: already a request is in process
    
    *Note*: This API is used only in sys_m.c and called inside SYS_M_LOCK lock
    So right now do not have its own mutex created for concurrency protection
*/
/*=========================================================================*/
ssreq_status_t ssreq_process_request(ssreq_cmd_type cmd, ssreq_qmi_ind_cb_fn_type *ind_cb_fn)
{
 
    if(TRUE == ssreq_req_in_process)
    {
      return SSREQ_IN_PROCESS;
    }
    ssreq_req_in_process = TRUE;
    ssreq_qmi_ind_cb = ind_cb_fn;
    timer_set(&ssreq_ind_timer, (timetick_type)SSREQ_REQ_TIMEOUT_MS, 0, T_MSEC);

    switch(cmd){
        case SSREQ_SYSTEM_SHUTDOWN:
            return ssreq_shutdown_system();

        case SSREQ_SYSTEM_RESTART:
            return ssreq_restart_system();

        case SSREQ_PERIPHERAL_RESTART:
            return ssreq_restart_peripheral();

        default:
           MSG_1(MSG_SSID_TMS, MSG_LEGACY_ERROR,PROC_ID"SSREQ: Unhandled Request cmd = %d", cmd);
    }
    return SSREQ_FAILURE;
}

/*===========================================================================

  FUNCTION:  ssreq_ser_check_for_shutdown_support

===========================================================================*/
boolean ssreq_ser_check_for_shutdown_support(void)
{
   uint32 minor_vers = 0;
   qmi_client_error_type ret;
   ssreq_service_object = ssreq_get_service_object_v01();
   
   ret = qmi_idl_get_idl_minor_version(ssreq_service_object, &minor_vers);
   if(ret != QMI_IDL_LIB_NO_ERR)
   {
      minor_vers = 0;
   }
   /*SYSM_CHIP_ID will be 1 for MDMs with TN APPS and there SSREQ service supports
    shutdown over SSREQ
    For HLOS SYSM_CHIP_ID will be 0, thats defined in sys_m.scons */
   MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"SSREQ: service minor version = %d, SYSM_CHIP_ID = %d", minor_vers, SYSM_CHIP_ID);
   if ((minor_vers < SSREQ_SERVICE_MINOR_VER_FOR_SHUTDOWN_SUPPORT)&&(SYSM_CHIP_ID==0))
   {
      return FALSE;
   }
   return TRUE;
}


