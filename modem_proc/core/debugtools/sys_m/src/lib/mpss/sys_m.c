/**
@file sys_m.c
@brief This file contains the API for the System Monitor Framework API 1.0 service.
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2013 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.mpss/3.7.24/debugtools/sys_m/src/lib/mpss/sys_m.c#1 $
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
===============================================================================*/

#include "dog_messages.h"
#include "task.h"
#include "err.h"
#include "msg.h"
#include "dog.h"
#include "rex.h"
#include "sys_m.h"
#include "sys_m_reset.h"
#include "sys_m_messages.h"
#include "smd_lite.h"
#include "rcecb.h"
#include "rcevt_internal.h"
#include "rcinit.h"
#include "smem.h"
#include "smsm.h"
#include "rcevt_rex.h"
#include "smsm.h"
#include "sys_m_smsm.h"
#include "qurt.h"
#include "sys_m_fatal.h"
#include "stringl/stringl.h"
#include "qmi_ssreq_client.h"

extern void err_halt_execution(void);
extern void dog_intentional_timeout_stm(void);

TASK_EXTERN(sys_m)

#define SYS_M_BUFFER_LEN 50

char sys_m_buffer[SYS_M_BUFFER_LEN];

smdl_handle_type sys_m_port_MODEM_AP = NULL;
int sys_m_notification_id = -1;
int sys_m_read_return = -1;
int sys_m_write_return = -1;
int sys_m_xpu_registered = 0;

int sys_m_forced_shutdown = 0;

#define SYS_M_VER_STR "info:version:0:1"

char *accepted_messages[] = {
    SYS_M_SSR_LPASS_BEFORE_SHUTDOWN,
    SYS_M_SSR_WCNSS_BEFORE_SHUTDOWN,
    SYS_M_SSR_DSPS_BEFORE_SHUTDOWN,
    SYS_M_SSR_ADSP_BEFORE_SHUTDOWN,
    SYS_M_SSR_EXT_MODEM_BEFORE_SHUTDOWN,

    SYS_M_SSR_LPASS_AFTER_POWERUP,
    SYS_M_SSR_WCNSS_AFTER_POWERUP,
    SYS_M_SSR_DSPS_AFTER_POWERUP,
    SYS_M_SSR_ADSP_AFTER_POWERUP,
    SYS_M_SSR_EXT_MODEM_AFTER_POWERUP,

    SYS_M_SSR_LPASS_BEFORE_POWERUP,
    SYS_M_SSR_WCNSS_BEFORE_POWERUP,
    SYS_M_SSR_DSPS_BEFORE_POWERUP,
    SYS_M_SSR_ADSP_BEFORE_POWERUP,
    SYS_M_SSR_EXT_MODEM_BEFORE_POWERUP,

    SYS_M_SSR_LPASS_AFTER_SHUTDOWN,
    SYS_M_SSR_WCNSS_AFTER_SHUTDOWN,
    SYS_M_SSR_DSPS_AFTER_SHUTDOWN,
    SYS_M_SSR_ADSP_AFTER_SHUTDOWN,
    SYS_M_SSR_EXT_MODEM_AFTER_SHUTDOWN

};
#define SYS_M_NUM_NOTIFICATION sizeof(accepted_messages)/sizeof(char*)

char *sys_m_msg_types[] = {
    "ssr",
    "info"
};
#define SYS_M_NUM_MSGTYPES sizeof(sys_m_msg_types)/sizeof(char*)

void sys_m_signal_modules(void);
void sys_m_unknown_msgs(void);
void sys_m_register_fatal_notification(void);

static qurt_mutex_t sys_m_lock_mutex;
rex_timer_type sys_m_retry_timer;
int sys_m_lock_inited = 0;

/**
 *
 * @brief SYS_M_LOCK_INIT
 *
 * Initialize sys_m mutex
 *
*/
void SYS_M_LOCK_INIT(void)
{
   if (!sys_m_lock_inited)
   {
      qurt_pimutex_init(&sys_m_lock_mutex);
      sys_m_lock_inited = 1;
   }
}

/**
 *
 * @brief SYS_M_LOCK
 *
 * Lock sys_m mutex
 *
*/
void SYS_M_LOCK(void)
{
   if (sys_m_lock_inited)
   {
      qurt_pimutex_lock(&sys_m_lock_mutex);
   }
}

/**
 *
 * @brief SYS_M_UNLOCK
 *
 * Unlock sys_m mutex
 *
*/
void SYS_M_UNLOCK(void)
{
   if (sys_m_lock_inited)
   {
      qurt_pimutex_unlock(&sys_m_lock_mutex);
   }
}

/**
 * @brief q6_smd_cb
 *
 * System monitor SMD callback function.   Internal to System Monitor
 *
 * @param port: SMD port
 * @param event: SMD event
 * @param data: CB data
 *
*/
void q6_smd_cb(smdl_handle_type port, smdl_event_type event, void *data)
{
   if (event == SMDL_EVENT_READ)
   {
      rex_set_sigs(TASK_HANDLE(sys_m), SYS_M_AP_NOTIFY_RCV);
   }
   return;
}

/**
 *
 * @brief sys_m_apps_errfatal_cb
 *
 * Callback for a forced error fatal from apps
 *
 * @param[in] entry: SMSM type
 * @param[in] prev_state: SMSM's previous state
 * @param[in] curr_state: SMSM's current state
 * @param[in] *cb_data: callback data
*/
void sys_m_apps_errfatal_cb(smsm_entry_type entry, uint32 prev_state, uint32 curr_state, void * cb_data)
{
   ERR_FATAL("APPS error fatal", 0, 0, 0);
}

/**
 *
 * @brief sys_m_init
 *
 * System monitor initialization function
 *
*/
void sys_m_init(void)
{
   smsm_cb_register( SMSM_APPS_STATE, SMSM_RESET, sys_m_apps_errfatal_cb, NULL);
   sys_m_port_MODEM_AP = smdl_open(sys_m_smd_port, SMD_APPS_MODEM, 0, SMD_MIN_FIFO, q6_smd_cb, NULL);
   SYS_M_LOCK_INIT();
}

/**
 *
 * @brief sys_m_task
 *
 * System Monitor main task
 *
 * @param param: Task init parameter
 *
*/
void sys_m_task(dword param)
{
   rex_sigs_type sigs;
   int i = 0, retry_count = 0;
   SYS_M_SPECIAL_MSG_TYPE sys_m_msg_type;
   RCEVT_SIGEX_SIGREX sig;

   //register kernel fatal notification handler
   sys_m_register_fatal_notification();

   sig.signal = rex_self();

   // sig.mask = SYS_M_TASKS_COMPLETE;
   // rcevt_register_sigex_name("rcinit:initgroups",  RCEVT_SIGEX_TYPE_SIGREX, &sig);

   sig.mask = SYS_M_DOG_READY;
   rcevt_register_sigex_name(DOG_HW_INIT_COMPLETE, RCEVT_SIGEX_TYPE_SIGREX, &sig);  //register for dog HW initialization

   //define timer
   rex_def_timer( &sys_m_retry_timer, rex_self(), SYS_M_DOG_READY );

   rcinit_handshake_startup();   //block for start signal

   //task forever loop
   for (;;)
   {
      sigs = rex_wait(SYS_M_AP_NOTIFY_RCV | SYS_M_TASKS_COMPLETE | SYS_M_DOG_READY);

      if ( sigs & SYS_M_AP_NOTIFY_RCV )
      {
         (void) rex_clr_sigs( rex_self(), SYS_M_AP_NOTIFY_RCV );
         //clear buffer before reading
         secure_memset(sys_m_buffer, 0, sizeof(sys_m_buffer));
         sys_m_read_return = smdl_read(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, sys_m_buffer, 0);
         if (sys_m_read_return < 0)
         {
            ERR_FATAL("Sys_m smdl_read failed", 0, 0, 0);
         }

         //data available to read.  ignore 0 length packets
         else if (sys_m_read_return > 0)
         {
            sys_m_msg_type = SYS_M_UNKNOWN_MSG;
            //look for nonspecial messages
            for (i = 0; i < SYS_M_NUM_NOTIFICATION; i++)
            {
               if (strncmp(sys_m_buffer, accepted_messages[i], SYS_M_BUFFER_LEN) == 0)
               {
                  sys_m_notification_id = i;
                  sys_m_msg_type = SYS_M_NONSPECIAL_MSG;
                  break;
               }
            }
            //look for special messages
            if (strncmp(sys_m_buffer, SYS_M_SHUTDOWN, SYS_M_BUFFER_LEN) == 0)
            {
               sys_m_msg_type = SYS_M_SHUTDOWN_MSG;
            }

            switch(sys_m_msg_type)
            {
               case SYS_M_NONSPECIAL_MSG:
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"smdl msg notify modules");
                  sys_m_signal_modules();       // contains a blocking wait
                  break;
               case SYS_M_SHUTDOWN_MSG:
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"smdl msg initiate shutdown");
                  sys_m_shutdown(SYS_M_SMD);    // contains a blocking wait
                  break;
               case SYS_M_UNKNOWN_MSG:
                  MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"smdl msg unknown message");
                  sys_m_unknown_msgs();
                  break;
            }
         }
      }

      if (sigs & SYS_M_TASKS_COMPLETE)
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"tasks complete");

         (void) rex_clr_sigs(rex_self(), SYS_M_TASKS_COMPLETE);
      }

      if (sigs & SYS_M_DOG_READY)
      {
         MSG(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"dog ready");

         (void) rex_clr_sigs(rex_self(), SYS_M_DOG_READY);
         smsm_state_set(SMSM_MODEM_STATE, SMSM_SUBSYS2AP_STATUS);

         //if bit cannot be set, retry every 100 ms at a max of 10 times
         if (sys_m_smsm_apps_set(SYS_M_SUBSYS2AP_SMSM_ERR_HDL_RDY) != SYS_M_SMP2P_SUCCESS)
         {
            if (retry_count < 10)
            {
               rex_set_timer(&sys_m_retry_timer, 100);
               retry_count++;
            }
         }
      }
   }
}

/**
 *
 * @brief sys_m_unknown_msgs
 *
 * Handle messages that could not be understood.  Internal to system monitor
 *
*/
void sys_m_unknown_msgs(void)
{
   char *savePtr, *retStr;
   int i = 0;
   //version
   if (strstr(sys_m_buffer, SYS_M_VER) != NULL)
   {
      sys_m_write_return = smdl_write(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, SYS_M_VER_STR, SMDL_WRITE_FLAGS_NONE);
      return;
   }
   //unknown msg
   retStr = strtok_r(sys_m_buffer, ":", &savePtr);
   if (retStr != NULL)
   {
      for (i = 0; i < SYS_M_NUM_MSGTYPES; i++)
      {
         if (strncmp(retStr, sys_m_msg_types[i], SYS_M_BUFFER_LEN) == 0)
         {
            strlcat(retStr, ":unknown", SYS_M_BUFFER_LEN);
            sys_m_write_return = smdl_write(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, retStr, SMDL_WRITE_FLAGS_NONE);
            return;
         }
      }
   }
   sys_m_write_return = smdl_write(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, "err:unknown", SMDL_WRITE_FLAGS_NONE);
   return;
}

/**
 *
 * @brief sys_m_signal_modules
 *
 * Signal interested modules of a system monitor event.  After all clients have completed
 * notification, acknowledge to apps
 *
*/
void sys_m_signal_modules(void)
{
   int orig_ack_count = 0;
   int req_count = 0;

   //callbacks
   rcecb_signal_name(accepted_messages[sys_m_notification_id]);

   //get count of current clients
   req_count = rcevt_getcontexts_name(accepted_messages[sys_m_notification_id]);
   orig_ack_count = rcevt_getcount_name(SYS_M_ACK);
   //signal
   rcevt_signal_name(accepted_messages[sys_m_notification_id]);
   //wait for all clients to ack
   rcevt_wait_count_name(SYS_M_ACK, req_count + orig_ack_count); // blocking wait
   //tell AP that notification is serviced
   sys_m_notification_id = -1;
   sys_m_write_return = smdl_write(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, SYS_M_ACK, SMDL_WRITE_FLAGS_NONE);
}

void sys_m_reset(void)
{
   // NULL
}

/**
 *
 * @brief sys_m_begin_shutdown
 *
 * Begin shutdown process
 *
*/
void sys_m_shutdown(SYS_M_COMMUNICATION_TYPE sys_m_comm)
{
   int32 return_val = 0;

   // commanded shutdown begins, this is a time sensitive operation, with
   // the time of the operation not under control of the peripheral shutting down.
   // all operations must be expedient and complete in the minimum amount of time
   // possible. technology areas must not prolong their "cleanup," the shutdown
   // process must be considered a software exception, and the technology areas
   // do not get infinite time to handle their part of the software exception.

   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_shutdown process begins, expect degraded runtime operation sys_m_comm = %d", sys_m_comm);

   SYS_M_LOCK();

   // shutdown
   sys_m_shutdown_internal();

   // completion indication
   switch(sys_m_comm)
   {
      case SYS_M_SMSM_P2P:
         //ACKing using SMP2P bit after shutdown completion
         return_val = sys_m_smsm_apps_set(SYS_M_SUBSYS2AP_SMSM_SHUT_DWN_ACK);
         break;
      case SYS_M_SMD:
         return_val = smdl_write(sys_m_port_MODEM_AP, SYS_M_BUFFER_LEN, SYS_M_SHUTDOWN_ACK, SMDL_WRITE_FLAGS_NONE);
         break;
      case SYS_M_QMI:
         break;
      default:
         break;
   }

   SYS_M_UNLOCK();

   // at this point, the peripheral is operating in a degraded state. technology
   // areas cannot make any assumptions about the availability of any specific
   // services. all technology areas at this point should have parked their services
   // into a logical non-processing state.

   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_shutdown process ends, expect degraded runtime operation return_val= %d", return_val);
}

/**
 *
 * @brief sys_m_shutdown
 *
 * Signal interested modules to prepare for shutdown.  After all clients have completed
 * notification, acknowledge to apps
 *
*/
void sys_m_shutdown_internal(void)
{
   int req_count = 0;
   int orig_ack_count = 0;

   //callbacks
   rcecb_signal_name(SYS_M_SHUTDOWN);

   req_count = rcevt_getcontexts_name(SYS_M_SHUTDOWN);
   orig_ack_count = rcevt_getcount_name(SYS_M_SHUTDOWN_ACK);
   rcevt_signal_name(SYS_M_SHUTDOWN);
   rcevt_wait_count_name(SYS_M_SHUTDOWN_ACK, req_count + orig_ack_count); // blocking wait
}

/**
 *
 * @brief sys_m_initiate_shutdown
 *
 * Client Exposed API
 * Request apps for a reset of the device
 *
 *
*/
void sys_m_initiate_shutdown(void)
{

   SYS_M_LOCK(); // Concurrency protection
   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"Enter sys_m_initiate_shutdown from task_tid contex :", rex_self());

   /*check if the shutdonw request can be forwarded to APSS(HLOS or TN ) using SSREQ QMI*/
   if((ssreq_ser_check_for_shutdown_support())&&(SSREQ_SUCCESS == ssreq_client_init()))
   {
       ssreq_status_t ret;
       ret = ssreq_process_request(SSREQ_SYSTEM_SHUTDOWN, NULL);
       MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_shutdown using SSREQ QMI ret = %d, task_tid 0x%x", ret, rex_self());
   }
   // Check for the case where a forwarding callback has been registered. Specific use
   // case example is HLOS to MPSS has established a AT Command Forward channel that
   // is utilized to forward SHUTDOWN REQUEST back to HLOS.
   else if (0 < rcecb_getcontexts_name(SYS_M_RCECB_FWD_SHUTDOWN)) // count of registrants
   {
       MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_shutdown forward request task_tid %x", rex_self());
       rcecb_signal_name(SYS_M_RCECB_FWD_SHUTDOWN); // signal executes callbacks for registrants
   }
   // Else the behavior is normal standalone behavior. Specific use case example
   // is that the APPS_TN gets the command request by SMSM state bits.
   else
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_shutdown standalone request task_tid %x", rex_self());

      smsm_state_set(SMSM_MODEM_STATE, SMSM_SYSTEM_REBOOT);
   }

   SYS_M_UNLOCK();
}

/**
 *
 * @brief sys_m_initiate_poweroff
 *
 * Client Exposed API
 * Request apps for a poweroff of the device
 *
 *
 *
*/
void sys_m_initiate_poweroff(void)
{

   SYS_M_LOCK(); // Concurrency protection

   MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"Enter sys_m_initiate_poweroff from task_tid contex :", rex_self());

   /*check if the shutdonw request can be forwarded to APSS(HLOS or TN ) using SSREQ QMI*/
   if((ssreq_ser_check_for_shutdown_support())&&(SSREQ_SUCCESS == ssreq_client_init()))
   {
       ssreq_status_t ret;
       ret = ssreq_process_request(SSREQ_SYSTEM_SHUTDOWN, NULL);
       MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_poweroff using SSREQ QMI ret = %d, task_tid 0x%x", ret, rex_self());
   }
   // Check for the case where a forwarding callback has been registered. Specific use
   // case example is HLOS to MPSS has established a AT Command Forward channel that
   // is utilized to forward SHUTDOWN REQUEST back to HLOS.
   else if (0 < rcecb_getcontexts_name(SYS_M_RCECB_FWD_POWEROFF)) // count of registrants
   {
       MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_poweroff forward request task_tid %x", rex_self());
      rcecb_signal_name(SYS_M_RCECB_FWD_POWEROFF); // signal executes callbacks for registrants
   }
   // Else the behavior is normal standalone behavior. Specific use case example
   // is that the APPS_TN gets the command request by SMSM state bits.
   else
   {
      MSG_1(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_poweroff standalone request task_tid %x", rex_self());

      smsm_state_set(SMSM_MODEM_STATE, SMSM_SYSTEM_DOWNLOAD);
   }

   SYS_M_UNLOCK();
}

/**
  Initiates reset of the system using SSREQ QMI

  @return
   SSREQ_SUCCESS
   SSREQ_FAILURE
   SSREQ_IN_PROCESS

  @dependencies
  None.
*/
ssreq_status_t sys_m_initiate_restart_ssreq(ssreq_qmi_ind_cb_fn_type *ind_cb_fn)
{
   ssreq_status_t ssreq_ret;

   SYS_M_LOCK(); // Concurrency protection

   /*check if the shutdonw request can be forwarded to APSS(HLOS or TN ) using SSREQ QMI*/
   ssreq_ret = ssreq_client_init();
   if(SSREQ_SUCCESS == ssreq_ret)
   {
       ssreq_ret = ssreq_process_request(SSREQ_SYSTEM_RESTART, ind_cb_fn);
       MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_initiate_restart_ssreq using SSREQ QMI ret = %d, task_tid 0x%x", ssreq_ret, rex_self());
   }
   SYS_M_UNLOCK();
   return ssreq_ret;
}

/**
  Initiates the peripheral restart using SSREQ QMI
  And notify about the indication responce status to 
  the user of this API via its callback function(ind_cb_fn) if provided
  IF it dont find the SSREQ server than use the hack to do the modem SSR 
  bypassing the RAM dumps collection
  
  @return
   SSREQ_SUCCESS
   SSREQ_FAILURE
   SSREQ_IN_PROCESS

  @dependencies
  None.
*/
ssreq_status_t sys_m_request_peripheral_restart_ssreq(ssreq_qmi_ind_cb_fn_type *ind_cb_fn)
{
   ssreq_status_t ssreq_ret;

   SYS_M_LOCK(); // Concurrency protection

   /*check if the shutdonw request can be forwarded to APSS(HLOS or TN ) using SSREQ QMI*/
   ssreq_ret = ssreq_client_init();
   if(SSREQ_SUCCESS == ssreq_ret)
   {
       ssreq_ret = ssreq_process_request(SSREQ_PERIPHERAL_RESTART, ind_cb_fn);
       MSG_2(MSG_SSID_TMS, MSG_LEGACY_HIGH,PROC_ID"sys_m_request_peripheral_restart_ssreq ret = %d, task_tid 0x%x", ssreq_ret, rex_self());
   }
   else
   {
       int32 ret;
       ret = sys_m_smsm_apps_set(SYS_M_SUBSYS2AP_SMSM_RAM_DUMP_BYPASS_MODIFIER);
       ERR_FATAL("THIS IS INTENTIONAL RESET, NO RAMDUMP EXPECTED, smp2p write ret = %d, task_tid 0x%x",ret,rex_self(),0);
   }
   SYS_M_UNLOCK();
   return ssreq_ret;
}

/**
 *
 * @brief sys_m_init_sfr_buffer
 *
 * Initialize the SFR buffer.  Only error services should call
 * this function
 *
 *  @return
 *  NULL if smem_boot_init has not been called yet
 *  Otherwise, returns either a valid SMEM address to the requested buffer or a
 *  fatal error
 *
*/
void *sys_m_init_sfr_buffer(void)
{
   return smem_alloc (SMEM_SSR_REASON_MSS0, ERR_SSR_REASON_SIZE_BYTES);
}

/**
 *
 * @brief sys_m_register_preflush_cb
 *
 * Register CB with the preflush event
 *
 * @param cb: callback func
 *
*/
void sys_m_register_preflush_cb(void* cb)
{
   SYS_M_LOCK();
   rcecb_register_context_name(SYS_M_ERR_CB_PREFLUSH, cb);
   SYS_M_UNLOCK();
}

/**
 *
 * @brief sys_m_register_postflush_cb
 *
 * Register CB with the preflush event
 *
 * @param cb: callback func
 *
*/
void sys_m_register_postflush_cb(void* cb)
{
   SYS_M_LOCK();
   rcecb_register_context_name(SYS_M_ERR_CB_POSTFLUSH, cb);
   SYS_M_UNLOCK();
}

/**
 *
 * @brief sys_m_err_cb_preflush
 *
 * Issue callbacks before cache has been flushed in an ERR_FATAL
 *
*/
void sys_m_err_cb_preflush(void)
{
   rcecb_signal_name_nolocks(SYS_M_ERR_CB_PREFLUSH);
}

/**
 *
 * @brief sys_m_err_cb_postflush
 *
 * Issue callbacks after cache has been flushed in an ERR_FATAL
 *
*/
void sys_m_err_cb_postflush(void)
{
   rcecb_signal_name_nolocks(SYS_M_ERR_CB_POSTFLUSH);
}

/**
 *
 * @brief sys_m_qurt_fatal_notification
 *
 * Special function run in a unique context by qurt post error
 * handling to notify apps of failure
 *
*/
void sys_m_qurt_fatal_notification(void* no_param)
{
   static uint32 wait_counter = 10;

   // prevent warnings
   no_param = NULL;

   //special callback for XPU handling
   rcecb_signal_name_nolocks(SYS_M_XPU_ERR_STRING);

   smsm_set_reset(0);
   if (!sys_m_forced_shutdown)
   {
        sys_m_smsm_apps_set_stm(SYS_M_SUBSYS2AP_SMSM_ERRFATAL);
   }
   else
   {
        sys_m_smsm_apps_set_stm(SYS_M_SUBSYS2AP_SMSM_SHUT_DWN_ACK);
   }

   // Per requirement, delay 2 seconds before letting wdog expire
   while (--wait_counter > 0)
   {
      dog_force_kick();
      DALSYS_BusyWait(200000); //wait 200ms
   }
   dog_intentional_timeout_stm();

   while (1)
   {
      DALSYS_BusyWait(200000); //wait 200ms
   }
}

/**
 *
 * @brief sys_m_register_fatal_notification
 *
 * Registers fatal notification handler with the kernel
 *
 * @param func: callback func
 *
*/
void sys_m_xpu_handler_register(RCECB_CONTEXT func)
{
   SYS_M_LOCK();

   if (sys_m_xpu_registered == 0)
   {
      rcecb_register_context_name(SYS_M_XPU_ERR_STRING , func);
      sys_m_xpu_registered = 1;
   }
   else
   {
      ERR_FATAL("Multiple XPU registrants", 0, 0, 0);
   }

   SYS_M_UNLOCK();
}

/**
 *
 * @brief sys_m_register_fatal_notification
 *
 * Registers fatal notification handler with the kernel
 *
*/
void sys_m_register_fatal_notification(void)
{
   // as long as we do not get an error, we are done
   if ( QURT_EVAL == qurt_exception_register_fatal_notification(sys_m_qurt_fatal_notification, NULL) )
   {
      ERR_FATAL("Could not register qurt fatal notification handler", 0, 0, 0);
   }
}
