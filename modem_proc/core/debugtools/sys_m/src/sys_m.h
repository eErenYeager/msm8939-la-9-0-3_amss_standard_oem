#ifndef SYS_M_H
#define SYS_M_H
/*===========================================================================

           S Y S _ M . H

DESCRIPTION

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================
DESCRIPTION
High level system monitor
===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/debugtools/sys_m/src/sys_m.h#1 $

===========================================================================*/
#include "qmi_csi.h"

typedef enum SYS_M_SPECIAL_MSG_T
{
    SYS_M_NONSPECIAL_MSG = 0,
    SYS_M_SHUTDOWN_MSG   = 1,
    SYS_M_UNKNOWN_MSG    = 3
} SYS_M_SPECIAL_MSG_TYPE;

typedef enum SYS_M_COMMUNICATION_T
{
    SYS_M_SMSM_P2P       = 0,
    SYS_M_SMD            = 1,
    SYS_M_QMI            = 2
} SYS_M_COMMUNICATION_TYPE;

#define PROC_ID "MPSS:"

//init function
void sys_m_init(void);

//main task loop
void sys_m_task(dword param);

//reset function
void sys_m_reset(void);

//begin shutdown
void sys_m_shutdown(SYS_M_COMMUNICATION_TYPE);

//shutdown function
void sys_m_shutdown_internal(void);

//spin function, for apps break
void sys_m_spin(void);

//function for error handler to initialize SFR buffer
void *sys_m_init_sfr_buffer(void);

//Issue callbacks before cache has been flushed in an ERR_FATAL
void sys_m_err_cb_preflush(void);

//Issue callbacks after cache has been flushed in an ERR_FATAL
void sys_m_err_cb_postflush(void);

//Register preflush CB
void sys_m_register_preflush_cb(void *cb);

//Register postflush CB
void sys_m_register_postflush_cb(void *cb);


//internal QMI init function
qmi_csi_service_handle ssctl_qmi_ser_init(qmi_csi_os_params *os_params);


#define SYS_M_AP_NOTIFY_RCV   0x00000002
#define SYS_M_TASKS_COMPLETE  0x00000004
#define SYS_M_DOG_READY       0x00000008
#define SYS_M_RESET           0x00000100
#define SYS_M_MODE_RESET      0x00001000
#define SYS_M_QMI_SIG         0x00010000

#define sys_m_smd_port              "sys_mon"
#define SYS_M_SMP2P_PORT_OUT        "slave-kernel"
#define SYS_M_SMP2P_PORT_IN         "master-kernel"
#define SYS_M_XPU_ERR_STRING        "sys_m:xpu_error"

#define ERR_SSR_REASON_SIZE_BYTES   80

#endif  /* SYS_M_H */
