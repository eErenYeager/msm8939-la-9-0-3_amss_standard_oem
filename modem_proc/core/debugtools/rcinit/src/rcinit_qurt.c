/**
@file rcinit_qurt.c
@brief This file contains the API for the Run Control Framework API 2.0 service.
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2013 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_qurt.c#1 $
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
===============================================================================*/

#include "err.h"
#include "msg.h"
#include "stringl/stringl.h"

#if defined(RCINIT_TRACER_SWEVT)
#include "tracer.h"
#include "rcinit_tracer_swe.h"
#endif

#include "rcinit_qurt.h"
#include "rcinit_internal.h"

qurt_thread_t rcinit_lookup_qurttask_info(RCINIT_INFO info)
{
   rcinit_info_t* rcinit_p = (rcinit_info_t*)info;

   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle && RCINIT_TASK_QURTTASK == rcinit_p->type)
   {
      return((qurt_thread_t)(rcinit_p->handle->tid_p));
   }

   return(RCINIT_ZERO);
}

qurt_thread_t rcinit_lookup_qurttask(RCINIT_NAME name)
{
   RCINIT_INFO info = rcinit_lookup(name);

   if (RCINIT_NULL != info)
   {
      return(rcinit_lookup_qurttask_info(info));
   }

   return(RCINIT_ZERO);
}

static void TASK_ENTRY(void* argv)
{
   unsigned long task_hash;
   char task_name[RCINIT_NAME_MAX];
   const rcinit_info_t* rcinit_p = (const rcinit_info_t*)argv;
   task_hash = rcinit_internal_task_name(task_name, sizeof(task_name));
   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle)
   {
      void (*entry)(void*) = (void(*)(void*))rcinit_p->handle->entry;
      rcinit_internal_tls_set_specific(rcinit_internal.tls_key, (void*)rcinit_p);
      if (RCINIT_NULL != entry && RCINIT_ENTRY_NONE != entry)
      {
         rcinit_internal_hs_list_add(rcinit_internal.hs_init, rcinit_internal.group_curr, task_name);
#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_RN, 2, rcinit_internal.group_curr, task_hash);
#endif
         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "task begins group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);
         RCXH_TRY
         {
         entry(RCINIT_NULL);
         }
         RCXH_ENDTRY;
#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_XT, 2, rcinit_internal.group_curr, task_hash);
#endif
         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "task ends group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);
      }
   }
}

void rcinit_internal_start_qurttask(const rcinit_info_t* rcinit_p, void* entry)
{
   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle)
   {
      qurt_thread_attr_t qurt_attr;

      if (RCINIT_NULL != rcinit_p->handle->stack)
      {
         secure_memset(rcinit_p->handle->stack, RCINIT_STACK_INIT, rcinit_p->stksz);
      }

      qurt_thread_attr_init(&qurt_attr);
      qurt_thread_attr_set_name(&qurt_attr, (char*)rcinit_p->name);
      qurt_thread_attr_set_stack_size(&qurt_attr, rcinit_p->stksz);
      qurt_thread_attr_set_stack_addr(&qurt_attr, rcinit_p->handle->stack);
      qurt_thread_attr_set_priority(&qurt_attr, RCINIT_MAP_PRIO_QURT(rcinit_p->prio)); // qurt priority is 0 = high, invert to match 0 = low
      qurt_thread_attr_set_affinity(&qurt_attr, rcinit_p->cpu_affinity);

      rcinit_p->handle->entry = (rcinit_entry_p)entry;

      qurt_thread_create((qurt_thread_t*)(&(rcinit_p->handle->tid_p)), &qurt_attr, TASK_ENTRY, (void*)rcinit_p);
   }
}
