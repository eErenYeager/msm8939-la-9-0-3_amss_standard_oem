/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_ut.c#1 $

 ===========================================================================*/

#include "string.h"
#include "stringl.h"
#include "rcinit_internal.h"

/////////////////////////////////////////////////////////////////////
// Localized Type Declarations
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Localized Storage
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Implementation
/////////////////////////////////////////////////////////////////////

// Black List : Remove

void rcinit_ut_blacklist(unsigned long list_count, RCINIT_NAME list[])
{
   unsigned long cnt, offset;

   // Iterate internal dictionary
   for (offset = 0; offset < rcinit_internal_name_map_size - 2; offset++)
   {
      // Iterate list
      for (cnt = 0; cnt < list_count; cnt++)
      {
         // Check if dictionary entry on list
         if (0 == strncmp(list[cnt], rcinit_internal_name_map[offset].name, strlen(rcinit_internal_name_map[offset].name)))
         {
            const rcinit_info_t* rcinit_p = rcinit_internal_name_map[offset].info_p;

            // Check if dictionary entry has an info entry
            if (RCINIT_NULL != rcinit_p &&
                (RCINIT_TASK_DALTASK == rcinit_p->type || RCINIT_TASK_POSIX == rcinit_p->type ||
                 RCINIT_TASK_QURTTASK == rcinit_p->type || RCINIT_TASK_REXTASK == rcinit_p->type) &&
                RCINIT_NULL != rcinit_p->handle)
            {
               // Remove Entry
               rcinit_p->handle->entry = RCINIT_ENTRY_NONE;
            }
         }
      }
   }
}

// White List : Retain

void rcinit_ut_whitelist(unsigned long list_count, RCINIT_NAME list[])
{
   unsigned long cnt, offset;

   // Iterate internal dictionary
   for (offset = 0; offset < rcinit_internal_name_map_size - 2; offset++)
   {
      // Iterate list
      for (cnt = 0; cnt < list_count; cnt++)
      {
         // Check if dictionary entry on list
         if (0 == strncmp(list[cnt], rcinit_internal_name_map[offset].name, strlen(rcinit_internal_name_map[offset].name)))
         {
            // Exit Loop
            break;
         }
      }

      // Check if dictionary entry found on list
      if (cnt >= list_count)
      {
         const rcinit_info_t* rcinit_p = rcinit_internal_name_map[offset].info_p;

         // Check if dictionary entry has an info entry
         if (RCINIT_NULL != rcinit_p &&
             (RCINIT_TASK_DALTASK == rcinit_p->type || RCINIT_TASK_POSIX == rcinit_p->type ||
              RCINIT_TASK_QURTTASK == rcinit_p->type || RCINIT_TASK_REXTASK == rcinit_p->type) &&
             RCINIT_NULL != rcinit_p->handle)
         {
            // Remove Entry
            rcinit_p->handle->entry = RCINIT_ENTRY_NONE;
         }
      }
   }
}
