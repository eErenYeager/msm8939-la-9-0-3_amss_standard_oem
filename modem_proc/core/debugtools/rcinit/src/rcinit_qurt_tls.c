/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_qurt_tls.c#1 $

 ===========================================================================*/

#include "rcinit_qurt.h"
#include "rcinit_internal.h"

void qurt_tls_init(void); // forward reference

void rcinit_internal_tls_init(void)
{
}

int rcinit_internal_tls_create_key(int* x, void(*y)(void*))
{
   return qurt_tls_create_key(x, y);
}

int rcinit_internal_tls_set_specific(int x, void* y)
{
   return qurt_tls_set_specific(x, y);
}

void* rcinit_internal_tls_get_specific(int x)
{
   return qurt_tls_get_specific(x);
}
