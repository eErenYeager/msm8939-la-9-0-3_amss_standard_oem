/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_posix_tls.c#1 $

 ===========================================================================*/

#include "rcinit_posix.h"
#include "rcinit_internal.h"

void rcinit_internal_tls_init(void)
{
}

int rcinit_internal_tls_create_key(int* x, void(*y)(void*))
{
   return pthread_key_create((pthread_key_t*)x, y);
}

int rcinit_internal_tls_set_specific(int x, void* y)
{
   return pthread_setspecific((pthread_key_t)x, y);
}

void* rcinit_internal_tls_get_specific(int x)
{
   return pthread_getspecific((pthread_key_t)x);
}
