/**
@file rcinit_rex.c
@brief This file contains the API for the Run Control Framework API 2.0 service.
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2013 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_rex.c#1 $
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
===============================================================================*/

#include "err.h"
#include "msg.h"
#include "stringl/stringl.h"

#if defined(RCINIT_TRACER_SWEVT)
#include "tracer.h"
#include "rcinit_tracer_swe.h"
#endif

#include "rcinit_rex.h"
#include "rcinit_internal.h"

rex_tcb_type* rcinit_lookup_rextask_info(RCINIT_INFO info)
{
   rcinit_info_t* rcinit_p = (rcinit_info_t*)info;

   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle &&
       ((RCINIT_TASK_REXTASK == rcinit_p->type) || (RCINIT_TASK_LEGACY == rcinit_p->type)))
   {
      return((rex_tcb_type*)rcinit_p->handle->tid_p);
   }

   return(RCINIT_NULL);
}

rex_tcb_type* rcinit_lookup_rextask(RCINIT_NAME name)
{
   RCINIT_INFO info = rcinit_lookup(name);

   if (RCINIT_NULL != info)
   {
      return(rcinit_lookup_rextask_info(info));
   }

   return(RCINIT_NULL);
}

RCINIT_GROUP rcinit_lookup_group_rextask(rex_tcb_type* tid)
{
   RCINIT_GROUP rc = RCINIT_GROUP_0;
   const rcinit_info_t*** rcinit_sequence_group;

   rcinit_sequence_group = rcinit_internal_groups[rcinit_internal.policy_curr];

   while (RCINIT_NULL != *rcinit_sequence_group)
   {
      const rcinit_info_t** rcinit_group = *rcinit_sequence_group;

      while (RCINIT_NULL != *rcinit_group)
      {
         const rcinit_info_t* rcinit_p = *rcinit_group;

         if ((RCINIT_NULL != rcinit_p->handle) &&                 // must have a context handle
             ((RCINIT_TASK_REXTASK == rcinit_p->type) || (RCINIT_TASK_LEGACY == rcinit_p->type)) &&
             ((rex_tcb_type*)rcinit_p->handle->tid_p == tid))        // tcb pointer associates with group
         {
            return (rc);
         }

         rcinit_group++; // next; this group
      }

      rc++; // processing next group

      rcinit_sequence_group++; // next; sequence group
   }

   return (RCINIT_GROUP_NONE); // no task context in framework processing
}

RCINIT_INFO rcinit_lookup_info_rextask(rex_tcb_type* tid)
{
   RCINIT_INFO rc = RCINIT_NULL;
   const rcinit_info_t*** rcinit_sequence_group;

   rcinit_sequence_group = rcinit_internal_groups[rcinit_internal.policy_curr];

   while (RCINIT_NULL != *rcinit_sequence_group)
   {
      const rcinit_info_t** rcinit_group = *rcinit_sequence_group;

      while (RCINIT_NULL != *rcinit_group)
      {
         const rcinit_info_t* rcinit_p = *rcinit_group;

         if ((RCINIT_NULL != rcinit_p->handle) &&                 // must have a context handle
             ((RCINIT_TASK_REXTASK == rcinit_p->type) || (RCINIT_TASK_LEGACY == rcinit_p->type)) &&
             ((rex_tcb_type*)rcinit_p->handle->tid_p == tid))        // tcb pointer associates with group
         {
            return ((RCINIT_INFO)rcinit_p);
         }

         rcinit_group++; // next; this group
      }

      rcinit_sequence_group++; // next; sequence group
   }

   return (rc); // no task context in framework processing
}

static void TASK_ENTRY(unsigned long argv)
{
   unsigned long task_hash;
   char task_name[RCINIT_NAME_MAX];
   const rcinit_info_t* rcinit_p = (const rcinit_info_t*)argv;
   task_hash = rcinit_internal_task_name(task_name, sizeof(task_name));
   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle)
   {
      void (*entry)(unsigned long) = (void(*)(unsigned long))rcinit_p->handle->entry;
      rcinit_internal_tls_set_specific(rcinit_internal.tls_key, (void*)rcinit_p);
      if (RCINIT_NULL != entry && RCINIT_ENTRY_NONE != entry)
      {
         rcinit_internal_hs_list_add(rcinit_internal.hs_init, rcinit_internal.group_curr, task_name);
#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_RN, 2, rcinit_internal.group_curr, task_hash);
#endif
         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "task begins group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);
         RCXH_TRY
         {
         entry(RCINIT_ZERO);
         }
         RCXH_ENDTRY;
#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_XT, 2, rcinit_internal.group_curr, task_hash);
#endif
         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "task ends group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);
      }
   }
}

void rcinit_internal_start_rextask(const rcinit_info_t* rcinit_p, void* entry)
{
   if (RCINIT_NULL != rcinit_p && RCINIT_NULL != rcinit_p->handle)
   {
      if (RCINIT_NULL == rcinit_p->handle->tid_p)
      {
         if (NULL == (rcinit_p->handle->tid_p = (rcinit_tid_p)rcinit_internal_malloc(sizeof(rex_tcb_type))))
         {
            ERR_FATAL("rextask context creation", 0, 0, 0);
         }
      }

      secure_memset(rcinit_p->handle->tid_p, 0, sizeof(rex_tcb_type));

      if (RCINIT_NULL != rcinit_p->handle->stack)
      {
         secure_memset(rcinit_p->handle->stack, 0, rcinit_p->stksz); // rex trace32 extensions requires stack to be zero
      }

      rcinit_p->handle->entry = (rcinit_entry_p)entry;

      RCINIT_MAP_REX_DEF_TASK(rcinit_p->cpu_affinity, rcinit_p->handle->tid_p,
                              (unsigned char*)rcinit_p->handle->stack, rcinit_p->stksz,
                              RCINIT_MAP_PRIO_REX(rcinit_p->prio), TASK_ENTRY, (unsigned long)rcinit_p, (char*)rcinit_p->name,
                              FALSE, -1);
   }
}
