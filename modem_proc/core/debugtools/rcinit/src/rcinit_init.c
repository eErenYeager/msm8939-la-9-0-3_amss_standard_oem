/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_init.c#1 $

 ===========================================================================*/

#include "err.h"
#include "msg.h"
#include "stdint.h"

#include "DALSysTypes.h"
#include "DALPropDef.h"

#if defined(RCINIT_TRACER_SWEVT)
#include "tracer.h"
#include "rcinit_tracer_swe.h"
#endif

#include "rcinit_dal.h"
#include "rcinit_internal.h"

/////////////////////////////////////////////////////////////////////
// Localized Type Declarations
/////////////////////////////////////////////////////////////////////

#define dwMaxNumEvents 2

#if defined(RCINIT_TRACER_SWEVT)
extern const tracer_event_id_t rcinit_swe_event_init[RCINIT_GROUP_MAX];          // internal
#endif

/////////////////////////////////////////////////////////////////////
// Localized Storage
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Init Function Support
/////////////////////////////////////////////////////////////////////

/*===========================================================================

 FUNCTION worker

 DESCRIPTION
 internal shell functions

 DEPENDENCIES
 none

 RETURN VALUE
 operation success

 SIDE EFFECTS
 shuts down by registration to system monitor
 ===========================================================================*/
static DALResult rcworker(DALSYSEventHandle hUnused, void* tid)
{
   const rcinit_info_t** rcinit_group_p = rcinit_internal.worker_argv;

   while (RCINIT_NULL != *rcinit_group_p)
   {
      const rcinit_info_t* rcinit_p = *rcinit_group_p;

      if (IS_INITFN(rcinit_p))
      {
         // STALLING HERE?

         // BLOCKING HERE OCCURS ONLY WHEN THE INITFN IS USING KERNEL BLOCKING
         // MECHANISMS. CHECK WITH THE TECH AREA OWNING THE CALLBACK.

         rcinit_internal.initfn_curr = (void(*)(void))(rcinit_p->entry);

#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_FUNC_RN, 2, rcinit_internal.group_curr, rcinit_internal.initfn_curr);
#endif

         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "function enter group %x func_hash %x func_name %s", rcinit_internal.group_curr, 0, rcinit_p->name);

         rcinit_internal.initfn_curr();

#if defined(RCINIT_TRACER_SWEVT)
         tracer_event_simple_vargs(RCINIT_SWE_INIT_FUNC_XT, 2, rcinit_internal.group_curr, rcinit_internal.initfn_curr);
#endif

         MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "function exit group %x func_hash %x func_name %s", rcinit_internal.group_curr, 0, rcinit_p->name);

         rcinit_internal.initfn_curr = (void(*)(void))RCINIT_NULL;
      }

      rcinit_group_p++;
   }

   if (DAL_SUCCESS != DALSYS_EventCtrl(rcinit_internal.hEventWorkLoopAck, DALSYS_EVENT_CTRL_TRIGGER))
   {
      ERR_FATAL("worker ack", 0, 0, 0);
   }

   hUnused = hUnused;
   tid = tid;

   return(DAL_SUCCESS);
}

void rcinit_dal_loop_worker_create(void)
{
   if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT, &rcinit_internal.hEventWorkLoop, NULL))
   {
      ERR_FATAL("worker event creation", 0, 0, 0);
   }
   if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_NORMAL, &rcinit_internal.hEventWorkLoopAck, NULL))
   {
      ERR_FATAL("worker create event initfn", 0, 0, 0);
   }
   if (DAL_SUCCESS != DALSYS_RegisterWorkLoopEx("rcworker", rcinit_lookup_stksz("rcinit"), rcinit_lookup_prio("rcinit"), dwMaxNumEvents, &rcinit_internal.hWorkLoop, NULL))
   {
      ERR_FATAL("worker work loop registration", 0, 0, 0);
   }
   if (DAL_SUCCESS != DALSYS_AddEventToWorkLoop(rcinit_internal.hWorkLoop, rcworker, RCINIT_NULL, rcinit_internal.hEventWorkLoop, NULL))
   {
      ERR_FATAL("worker work loop event addition", 0, 0, 0);
   }
}

/*===========================================================================

 FUNCTION rcinit_handshake_init

 DESCRIPTION
 *required* task handshake to rcinit; all tasks managed by the framework *must*
 *call this api or they will block startup. this is by design.

 DEPENDENCIES
 none

 RETURN VALUE
 operation success

 SIDE EFFECTS
 none
 ===========================================================================*/
void rcinit_handshake_init(void) // preferred API
{
   unsigned long task_hash;
   char task_name[RCINIT_NAME_MAX];

   task_hash = rcinit_internal_task_name(task_name, sizeof(task_name));

   // edge case group 0, initfn starting a task, must call rcinit_handshake

   if ((RCINIT_GROUP_0 == rcinit_internal.group_curr) &&                         // processing group 0 (only)
       (rcinit_process_group_init_fn == rcinit_internal.process_state))          // processing initfns (only)
   {
      rcinit_internal_hs_list_hs(rcinit_internal.hs_init, rcinit_internal.group_curr, task_name);

#if defined(RCINIT_TRACER_SWEVT)
      tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_HS, 2, rcinit_internal.group_curr, task_hash); // HS accepted
#endif

      MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "handshake group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);

      if (DAL_SUCCESS != DALSYS_EventCtrl(rcinit_internal.hEventInitFnSpawn, DALSYS_EVENT_CTRL_TRIGGER))
      {
         ERR_FATAL("worker event initfn trigger", 0, 0, 0);
      }
   }

   // all other use case collect and wait for the defineack

   else
   {
      RCEVT_THRESHOLD count = rcevt_getcount_handle(rcinit_internal.defineack) + 1;

      rcinit_internal_hs_list_hs(rcinit_internal.hs_init, rcinit_internal.group_curr, task_name);

#if defined(RCINIT_TRACER_SWEVT)
      tracer_event_simple_vargs(RCINIT_SWE_INIT_TASK_HS, 2, rcinit_internal.group_curr, task_hash); // HS accepted
#endif

      MSG_SPRINTF_3(MSG_SSID_TMS, MSG_LEGACY_HIGH, "handshake group %x task_hash %x task_name %s", rcinit_internal.group_curr, task_hash, task_name);

      rcevt_signal_handle(rcinit_internal.define); // signals rcinit_task this task init complete

      rcevt_wait_count_handle(rcinit_internal.defineack, count);
   }
}

void rcinit_handshake_startup(void)
{
   rcinit_handshake_init(); // use preferred API
}

/*===========================================================================

 FUNCTION rcinit_initfn_spawn_task

 DESCRIPTION
 required wrapper for an init function to spawn a task (very specialized edge
 case found during sequence group 0, this should be the exception)

 DEPENDENCIES
 Requires existing RCINIT task database information where the task entry
 point is NULL. The static data is preallocated, and this call binds the
 entry point to the context and starts it. Edge case; only allowed from
 Group 0 initfn processing. all other use cases are not supported and
 are considered a bug.

 RETURN VALUE
 none

 SIDE EFFECTS
 task must call rcinit_handshake_startup(); else startup process will block
 as designed

 ===========================================================================*/
void rcinit_initfn_spawn_task(RCINIT_INFO info_p, void *entry)
{
   boolean started = FALSE;
   rcinit_info_t* rcinit_p = (rcinit_info_t*)info_p;

   if ((RCINIT_GROUP_0 == rcinit_lookup_group(rcinit_p->name)) &&                // must be in group 0 (only)
       (RCINIT_GROUP_0 == rcinit_internal.group_curr) &&                         // processing group 0 (only)
       (rcinit_process_group_init_fn == rcinit_internal.process_state) &&        // processing initfns (only)
       (RCINIT_NULL == rcinit_p->entry) &&                                       // must not have an entry (specialized)
       (RCINIT_NULL != entry))                                                   // must have an entry point argument
   {
      // DAL Event Objects Are Protected on Context Allowed to Wait (Create/Wait/Destroy)
      if (DAL_SUCCESS != DALSYS_EventCreate(DALSYS_EVENT_ATTR_NORMAL, &rcinit_internal.hEventInitFnSpawn, NULL))
      {
         ERR_FATAL("initfnspawn event", 0, 0, 0);
      }

      if (RCINIT_ENTRY_NONE != rcinit_p->handle->entry)
      {
      if (RCINIT_TASK_DALTASK == rcinit_p->type)
      {
         rcinit_internal_start_daltask(rcinit_p, entry);
         started = TRUE;
      }

      else if (RCINIT_TASK_POSIX == rcinit_p->type)
      {
         rcinit_internal_start_posix(rcinit_p, entry);
         started = TRUE;
      }

      else if (RCINIT_TASK_QURTTASK == rcinit_p->type)
      {
         rcinit_internal_start_qurttask(rcinit_p, entry);
         started = TRUE;
      }

         else if (RCINIT_TASK_REXTASK == rcinit_p->type || RCINIT_TASK_LEGACY == rcinit_p->type)
      {
         rcinit_internal_start_rextask(rcinit_p, entry);
         started = TRUE;
      }
      }

      // STALLING HERE?

      // BLOCKING HERE OCCURS UNTIL THE SINGLE TASK STARTED PERFORMS ITS HANDSHAKE.
      // THIS IS THE NORMAL MECHANISM. CHECK WITH THE TECH AREA OWNING THE CONTEXT
      // WHEN BLOCKING DURATION BECOMES EXCESSIVE WAITING FOR THE HANDSHAKE.

      if (TRUE == started)
      {
         // DAL Event Objects Are Protected on Context Allowed to Wait (Create/Wait/Destroy)
         if (DAL_SUCCESS != DALSYS_EventWait(rcinit_internal.hEventInitFnSpawn))
         {
            ERR_FATAL("initfnspawn event", 0, 0, 0);
         }
      }
      // DAL Event Objects Are Protected on Context Allowed to Wait (Create/Wait/Destroy)
      if (DAL_SUCCESS != DALSYS_DestroyObject(rcinit_internal.hEventInitFnSpawn))
      {
         ERR_FATAL("initfnspawn event", 0, 0, 0);
      }
   }
   else
   {
      ERR_FATAL("client does not meet requirements", 0, 0, 0);
   }
}

/*===========================================================================

 FUNCTION rcinit_internal_process_groups

 DESCRIPTION
 init function processing
 task define and start signaling

 DEPENDENCIES
 none

 RETURN VALUE
 none

 SIDE EFFECTS
 none

 ===========================================================================*/
void rcinit_internal_process_groups(void)
{
   const rcinit_info_t* rcinit_p;

   rcinit_info_p rcinit_info_rcinit_p = rcinit_lookup("rcinit");

   if (RCINIT_NULL == rcinit_info_rcinit_p)
   {
      ERR_FATAL("rcinit task info not available", 0, 0, 0);
   }

   rcinit_internal.policy_base = rcinit_internal_groups[rcinit_internal.policy_curr];           // initialize before a transition

   ////////////////////////////////////////
   // Process Groups
   ////////////////////////////////////////

   while (rcinit_internal.group_curr < RCINIT_GROUP_MAX)
   {
      unsigned long resource_size;

#if defined(RCINIT_TRACER_SWEVT)
      tracer_event_simple(rcinit_swe_event_init[rcinit_internal.group_curr]); // software event
#endif

      MSG_SPRINTF_1(MSG_SSID_TMS, MSG_LEGACY_HIGH, "initialization begins group %x", rcinit_internal.group_curr);

      ////////////////////////////////////////
      // Policy Filter Application
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_policy_fn;

      rcinit_internal.group_base = rcinit_internal.policy_base[rcinit_internal.group_curr];                     // initialize before a transition

      for (rcinit_internal.group_curr_idx = 0; RCINIT_NULL != rcinit_internal.group_base[rcinit_internal.group_curr_idx]; rcinit_internal.group_curr_idx++)
      {
         rcinit_p = rcinit_internal.group_base[rcinit_internal.group_curr_idx];

         if ((RCINIT_NULL == rcinit_p->handle) &&
             (RCINIT_STKSZ_ZERO == rcinit_p->stksz) &&
             (RCINIT_TASK_POLICYFN == rcinit_p->type) &&
             (RCINIT_NULL != rcinit_p->entry))
         {
            static boolean policy_oneshot = FALSE;

            if (FALSE == policy_oneshot)
            {
               RCINIT_NAME (*policyfn)(const RCINIT_NAME[]);
               RCINIT_NAME policy_name;
               RCINIT_POLICY policy;

               policy_oneshot = TRUE;

               policyfn = (RCINIT_NAME(*)(const RCINIT_NAME[]))rcinit_p->entry;
               policy_name = policyfn(rcinit_internal_policy_list);
               policy = rcinit_lookup_policy(policy_name);

               if (RCINIT_POLICY_NONE != policy)
               {
                  rcinit_internal.policy_curr = policy;

                  rcinit_internal.policy_base = rcinit_internal_groups[rcinit_internal.policy_curr];
               }
            }

            break;
         }
      }

      rcinit_internal.group_base = rcinit_internal.policy_base[rcinit_internal.group_curr];                     // after transition

      ////////////////////////////////////////
      // Static Resource Sizing
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_sizing;

      resource_size = 0; // initial group resource allocation

      for (rcinit_internal.group_curr_idx = 0; RCINIT_NULL != rcinit_internal.group_base[rcinit_internal.group_curr_idx]; rcinit_internal.group_curr_idx++)
      {
         rcinit_p = rcinit_internal.group_base[rcinit_internal.group_curr_idx];

         if ((RCINIT_NULL != rcinit_p->handle) &&                                // must have a context handle
             /* RCINIT_NULL != rcinit_p->handle->stack && */                     // must not be .bss allocated
             (RCINIT_STKSZ_ZERO != rcinit_p->stksz) &&                           // must have a stack size
             (rcinit_info_rcinit_p != rcinit_p))                                 // must not be rcinit, rcinit is the bootstrap
         {
            if (RCINIT_TASK_DALTASK == rcinit_p->type)
            {
               resource_size += (rcinit_p->stksz/sizeof(rcinit_stack_t));        // tech area specified resource
               resource_size += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t));  // overhead: exception unwind structure (per task)
            }

            else if (RCINIT_TASK_POSIX == rcinit_p->type)
            {
               resource_size += (rcinit_p->stksz/sizeof(rcinit_stack_t));        // tech area specified resource
               resource_size += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t));  // overhead: exception unwind structure (per task)
            }

            else if (RCINIT_TASK_QURTTASK == rcinit_p->type)
            {
               resource_size += (rcinit_p->stksz/sizeof(rcinit_stack_t));        // tech area specified resource
               resource_size += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t));  // overhead: exception unwind structure (per task)
            }

            else if ((RCINIT_TASK_REXTASK == rcinit_p->type) || (RCINIT_TASK_LEGACY == rcinit_p->type))
            {
               resource_size += (rcinit_p->stksz/sizeof(rcinit_stack_t));        // tech area specified resource
               resource_size += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t));  // overhead: exception unwind structure (per task)
            }
         }
      }

      ////////////////////////////////////////
      // Static Resource Allocation
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_allocation;

      if (0 != resource_size) // do not allocate a zero size resource
      {
         if (RCINIT_NULL == (rcinit_internal.stacks[rcinit_internal.group_curr] = (rcinit_stack_t*)rcinit_internal_malloc(resource_size * sizeof(rcinit_stack_t))))
         {
            ERR_FATAL("static resource allocation failure", 0, 0, 0);
         }

         rcinit_internal.stacks_size += resource_size;
      }

      ////////////////////////////////////////
      // Static Resource Assignments
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_assignment;

      if (0 != resource_size) // do not allocate a zero size resource
      {
         for (rcinit_internal.group_curr_idx = 0; RCINIT_NULL != rcinit_internal.group_base[rcinit_internal.group_curr_idx]; rcinit_internal.group_curr_idx++)
         {
            rcinit_p = rcinit_internal.group_base[rcinit_internal.group_curr_idx];

            if ((RCINIT_NULL != rcinit_p->handle) &&                                // must have a context handle
                /* RCINIT_NULL != rcinit_p->handle->stack && */                     // must not be .bss allocated
                (RCINIT_STKSZ_ZERO != rcinit_p->stksz) &&                           // must have a stack size
                (rcinit_info_rcinit_p != rcinit_p))                                 // must not be rcinit, rcinit is the bootstrap
            {
               if (RCINIT_TASK_DALTASK == rcinit_p->type)
               {
                  rcinit_p->handle->stack = rcinit_internal.stacks[rcinit_internal.group_curr];
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (rcinit_p->stksz/sizeof(rcinit_stack_t)); // tech area specified resource
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t)); // overhead: exception unwind structure (per task)
               }

               else if (RCINIT_TASK_POSIX == rcinit_p->type)
               {
                  rcinit_p->handle->stack = rcinit_internal.stacks[rcinit_internal.group_curr];
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (rcinit_p->stksz/sizeof(rcinit_stack_t)); // tech area specified resource
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t)); // overhead: exception unwind structure (per task)
               }

               else if (RCINIT_TASK_QURTTASK == rcinit_p->type)
               {
                  rcinit_p->handle->stack = rcinit_internal.stacks[rcinit_internal.group_curr];
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (rcinit_p->stksz/sizeof(rcinit_stack_t)); // tech area specified resource
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t)); // overhead: exception unwind structure (per task)
               }

               else if ((RCINIT_TASK_REXTASK == rcinit_p->type) || (RCINIT_TASK_LEGACY == rcinit_p->type))
               {
                  rcinit_p->handle->stack = rcinit_internal.stacks[rcinit_internal.group_curr];
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (rcinit_p->stksz/sizeof(rcinit_stack_t)); // tech area specified resource
                  rcinit_internal.stacks[rcinit_internal.group_curr] += (sizeof(_rcxh_scope_t)/sizeof(rcinit_stack_t)); // overhead: exception unwind structure (per task)
               }
            }
         }
      }

      ////////////////////////////////////////
      // Function Process
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_init_fn;

      for (rcinit_internal.group_curr_idx = 0; RCINIT_NULL != rcinit_internal.group_base[rcinit_internal.group_curr_idx]; rcinit_internal.group_curr_idx++)
      {
         rcinit_p = rcinit_internal.group_base[rcinit_internal.group_curr_idx];

         if (IS_INITFN(rcinit_p))
         {
            rcinit_internal.worker_argv = rcinit_internal.group_base;

            if ((DAL_SUCCESS != DALSYS_EventCtrl(rcinit_internal.hEventWorkLoop, DALSYS_EVENT_CTRL_TRIGGER)) ||
                (DAL_SUCCESS != DALSYS_EventWait(rcinit_internal.hEventWorkLoopAck)) ||
                (DAL_SUCCESS != DALSYS_EventCtrl(rcinit_internal.hEventWorkLoopAck, DALSYS_EVENT_CTRL_RESET)))
            {
               ERR_FATAL("rcworker trigger", 0, 0, 0);
            }

            break;
         }
      }

      ////////////////////////////////////////
      // Task Process
      ////////////////////////////////////////

      rcinit_internal.process_state = rcinit_process_group_init_task;

      rcinit_internal.def.curr = 0;                                                     // currently defined
      rcinit_internal.def.prev = rcevt_getcount_handle(rcinit_internal.define);         // previously defined

      for (rcinit_internal.group_curr_idx = 0; RCINIT_NULL != rcinit_internal.group_base[rcinit_internal.group_curr_idx]; rcinit_internal.group_curr_idx++)
      {
         rcinit_p = rcinit_internal.group_base[rcinit_internal.group_curr_idx];

         if (IS_TASK(rcinit_p))
         {
            if (RCINIT_ENTRY_NONE != rcinit_p->handle->entry)
            {
               if (RCINIT_TASK_DALTASK == rcinit_p->type)
               {
                  rcinit_internal_start_daltask(rcinit_p, rcinit_p->entry);
                  rcinit_internal.def.curr++;
               }

               else if (RCINIT_TASK_POSIX == rcinit_p->type)
               {
                  rcinit_internal_start_posix(rcinit_p, rcinit_p->entry);
                  rcinit_internal.def.curr++;
               }

               else if (RCINIT_TASK_QURTTASK == rcinit_p->type)
               {
                  rcinit_internal_start_qurttask(rcinit_p, rcinit_p->entry);
                  rcinit_internal.def.curr++;
               }

               else if (RCINIT_TASK_REXTASK == rcinit_p->type || RCINIT_TASK_LEGACY == rcinit_p->type)
               {
                  rcinit_internal_start_rextask(rcinit_p, rcinit_p->entry);
                  rcinit_internal.def.curr++;
               }
            }
         }
      }

      if (0 != rcinit_internal.def.curr)                                                // only wait when tasks were started
      {
         rcinit_internal.process_state = rcinit_process_group_init_blocking;

         // async signal arrival order; wait for all to complete that were started

         // STALLING HERE?

         // BLOCKING HERE OCCURS UNTIL ALL THE TASKS STARTED PERFORM THEIR HANDSHAKES.
         // THIS IS THE NORMAL MECHANISM. CHECK WITH THE TECH AREA OWNING A SPECIFIC
         // CONTEXT WHEN BLOCKING DURATION BECOMES EXCESSIVE WAITING FOR THE HANDSHAKE.
         // TASKS ARE LOGICALLY STARTED IN PARALLEL, AND ALLOWED TO BE SCHEDULED AND
         // RUN CONCURRENTLY.

         rcevt_wait_count_handle(rcinit_internal.define, rcinit_internal.def.prev + rcinit_internal.def.curr); // wait for all defined tasks handshake; this group

         // TASKS BLOCKING ON THIS HANDLE ARE WAITING CORRECTLY; ANY TASK BLOCKING
         // ELSEWHERE IS NOT PERFORMING STARTUP CORRECTLY.

         rcinit_internal.process_state = rcinit_process_group_run;

         rcevt_signal_handle(rcinit_internal.defineack);                         // issue start signal to all defined tasks; this group
      }

      else
      {
         // No blocking required, no tasks started
      }

      rcinit_internal.def.curr = 0; // reset instruments
      rcinit_internal.def.prev = 0; // reset instruments

      rcinit_internal.group_curr++;
   }

   ////////////////////////////////////////
   // Group Processing Complete
   ////////////////////////////////////////

   rcinit_internal.group_curr = RCINIT_GROUP_NONE;                              // current group
   rcinit_internal.process_state = rcinit_process_none;
}

/*===========================================================================

 FUNCTION rcinit_init

 DESCRIPTION
 prepare internal data storage setup

 DEPENDENCIES
 none

 RETURN VALUE
 none

 SIDE EFFECTS
 none

 ===========================================================================*/
void rcinit_init(void)
{
   rcinit_internal.policy_curr = rcinit_lookup_policy(RCINIT_POLICY_NAME_DEFAULT);

   if (RCINIT_POLICY_NONE != rcinit_internal.policy_curr)
   {
      int group;

      rcinit_internal_devcfg_check_load();                                          // load devcfg configs first, potentially allows a runtime override later

      rcevt_init();                                                                 // rcevt events service

      rcinit_internal_hs_list_init();

      rcxh_init();

      // internal events

      for (group = 0; group < RCINIT_GROUP_MAX; group++)
      {
         // rcinit_internal.rcevt_handle.init_event[group] = (rce_nde_p)rcevt_create_name(rcinit_term_rcevt[group]);
         rcinit_internal.rcevt_handle.term_event[group] = (rce_nde_p)rcevt_create_name(rcinit_term_rcevt[group]);
      }

      rcinit_internal.define     = (rce_nde_p)rcevt_create_name(RCINIT_RCEVT_DEFINE); // handle to define event, internal, ok to observe
      rcinit_internal.defineack  = (rce_nde_p)rcevt_create_name(RCINIT_RCEVT_DEFINEACK); // handle to defineack event, internal, ok to observe

      if ((RCINIT_NULL == rcinit_internal.define) ||                                // must have rcevt allocated
          (RCINIT_NULL == rcinit_internal.defineack))                               // must have rcevt allocated
      {
         ERR_FATAL("initialization", 0, 0, 0);
      }

      rcinit_internal.policy_base = rcinit_internal_groups[rcinit_internal.policy_curr];

      rcinit_internal.group_curr = RCINIT_GROUP_0;

      rcinit_internal.group_base = rcinit_internal.policy_base[rcinit_internal.group_curr];

      rcinit_internal_tls_init();                                                   // no error return

      rcinit_internal_tls_create_key(&rcinit_internal.tls_key, RCINIT_NULL);

      rcinit_dal_loop_worker_create();                                              // internal worker thread

      rcinit_internal_process_groups();                                             // sequence groups
   }

   else
   {
      ERR_FATAL("default policy not available", 0, 0, 0);
   }
}

// DALCFG Image Payload May Contain Specific Initialization

void rcinit_internal_devcfg_check_load(void)
{
   DALSYSPropertyVar propValue;
   DALSYS_PROPERTY_HANDLE_DECLARE(pHandle);

   DALSYS_GetDALPropertyHandleStr("tms_rcinit", pHandle);

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle, "rcinit_term_err_fatal_enable", 0, &propValue))
   {
      rcinit_term_err_fatal_set(propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle, "rcinit_term_latency_enable", 0, &propValue))
   {
      rcinit_term_latency_enable_set(propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout", 0, &propValue))
   {
      rcinit_term_timeout_set(propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_0", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_0, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_1", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_1, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_2", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_2, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_3", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_3, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_4", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_4, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_5", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_5, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_6", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_6, propValue.Val.dwVal);
   }

   if (DAL_SUCCESS == DALSYS_GetPropertyValue(pHandle,"rcinit_term_timeout_group_7", 0, &propValue))
   {
      rcinit_term_timeout_group_set(RCINIT_GROUP_7, propValue.Val.dwVal);
   }
}
