/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_posix_void.c#1 $

 ===========================================================================*/

typedef int pthread_t;

#include "rcinit_internal.h"

pthread_t rcinit_lookup_pthread_info(RCINIT_INFO info)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return(RCINIT_ZERO);
}

pthread_t rcinit_lookup_pthread(RCINIT_NAME name)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return(RCINIT_ZERO);
}

void rcinit_internal_start_pthread(const rcinit_info_t* rcinit_p, void* entry)
{
   ERR_FATAL("no implementation", 0, 0, 0);
}
