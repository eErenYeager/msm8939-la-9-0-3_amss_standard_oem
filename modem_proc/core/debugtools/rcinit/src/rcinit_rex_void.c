/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

 RCINIT SOURCE MODULE

 GENERAL DESCRIPTION
 Implementation of RCINIT Framework API 2.0

 EXTERNALIZED FUNCTIONS
 yes

 INITIALIZATION AND SEQUENCING REQUIREMENTS
 yes

 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

 EDIT HISTORY FOR MODULE

 $Header: //components/rel/core.mpss/3.7.24/debugtools/rcinit/src/rcinit_rex_void.c#1 $

 ===========================================================================*/

typedef int rex_tcb_type;

#include "rcinit_internal.h"

rex_tcb_type* rcinit_lookup_rextask_info(RCINIT_INFO info)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return(RCINIT_NULL);
}

rex_tcb_type* rcinit_lookup_rextask(RCINIT_NAME name)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return(RCINIT_NULL);
}

RCINIT_GROUP rcinit_lookup_group_rextask(rex_tcb_type* tid)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return (RCINIT_GROUP_NONE);
}

RCINIT_INFO rcinit_lookup_info_rextask(rex_tcb_type* tid)
{
   ERR_FATAL("no implementation", 0, 0, 0);
   return (RCINIT_NULL);
}

void rcinit_internal_start_rextask(const rcinit_info_t* rcinit_p, void* entry)
{
   ERR_FATAL("no implementation", 0, 0, 0);
}
