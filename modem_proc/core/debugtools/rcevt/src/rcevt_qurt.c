/**
@file rcevt_qurt.c
@brief This file contains the API for the Run Control Event Notification v2.0 service.
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2013 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.mpss/3.7.24/debugtools/rcevt/src/rcevt_qurt.c#1 $
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
===============================================================================*/

#include "rcevt_qurt.h"
#include "rcevt_internal.h"

RCEVT_HANDLE rcevt_register_handle_qurt(RCEVT_HANDLE const handle, qurt_anysignal_t* signal, unsigned int mask)
{
   RCEVT_SIGEX_SIGQURT rcevt_sigex;
   rcevt_sigex.signal = signal;
   rcevt_sigex.mask = mask;
   return rcevt_register_sigex_handle(handle, RCEVT_SIGEX_TYPE_SIGQURT, &rcevt_sigex);
}

RCEVT_HANDLE rcevt_register_name_qurt(RCEVT_NAME const name, qurt_anysignal_t* signal, unsigned int mask)
{
   RCEVT_SIGEX_SIGQURT rcevt_sigex;
   rcevt_sigex.signal = signal;
   rcevt_sigex.mask = mask;
   return rcevt_register_sigex_name(name, RCEVT_SIGEX_TYPE_SIGQURT, &rcevt_sigex);
}

RCEVT_HANDLE rcevt_unregister_handle_qurt(RCEVT_HANDLE const handle, qurt_anysignal_t* signal, unsigned int mask)
{
   RCEVT_SIGEX_SIGQURT rcevt_sigex;
   rcevt_sigex.signal = signal;
   rcevt_sigex.mask = mask;
   return rcevt_unregister_sigex_handle(handle, RCEVT_SIGEX_TYPE_SIGQURT, &rcevt_sigex);
}

RCEVT_HANDLE rcevt_unregister_name_qurt(RCEVT_NAME const name, qurt_anysignal_t* signal, unsigned int mask)
{
   RCEVT_SIGEX_SIGQURT rcevt_sigex;
   rcevt_sigex.signal = signal;
   rcevt_sigex.mask = mask;
   return rcevt_unregister_sigex_name(name, RCEVT_SIGEX_TYPE_SIGQURT, &rcevt_sigex);
}
