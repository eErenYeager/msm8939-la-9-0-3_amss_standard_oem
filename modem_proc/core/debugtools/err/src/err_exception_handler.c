/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        M A I N   M O D U L E

GENERAL DESCRIPTION
  This module contains the AMSS exception handler

EXTERNALIZED FUNCTIONS
  None

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright(c) 2007-2009        by Qualcomm Technologies, Incorporated. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_exception_handler.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/22/08   tbg     Merged in changes to support FEATURE_QUARTZ_20
07/25/07   tbg     Initial verion (pulled code from mobile.c)
===========================================================================*/


#include <stdio.h>
#include "comdef.h"
#include "err.h"
#include "msg.h"
#include "errlog.h"
#include "qurt/err_qurt.h"
#include "tms_utils.h"


#define SWI_NUM_DIVIDE_BY_0     0xD0  /* SWI # used in __default_signal_handler
                                         for DivideByZero exception */
/* The following offset is calculated out from SP(stack pointer) to the saved 
   pointer/address in stack, which points to the next instruction after the
   instruction of <blx __rt_sdiv>. */
/* Note: === This might change if __rt_sdiv() changes. === */
#define SWI_DIVISION_OFFSET     0x14

typedef enum {
  EXCP_UNKNOWN,
  EXCP_SWI,
  EXCP_UNDEF_INST,
  EXCP_MIS_ALIGN,
  EXCP_PAGE_FAULT,
  EXCP_EXE_FAULT,
  EXCP_DIV_BY_0,
  EXCP_MAX_NUM
} exception_type;

char  exception_info[EXCP_MAX_NUM][REX_TASK_NAME_LEN+1+1] = {
  "    :Excep  ",
  "    :No SWI ",
  "    :Undef  ",
  "    :MisAlgn",
  "    :PFault ",
  "    :XFault ",
  "    :DivBy0 ",
};

char              qxdm_dbg_msg[80];

extern coredump_type coredump;
extern void err_fatal_jettison_core (unsigned int line, const char *file_name,
const char *format, uint32 param1, uint32 param2, uint32 param3);

extern err_cb_ptr err_next_to_STM_cb;

void err_exception_handler( void );

/*===========================================================================
FUNCTION err_exception_handler

DESCRIPTION
  Handle IPC from QURT Kernel when exceptions occur.

===========================================================================*/

#define MAX_FAILURE_COUNT 10

void err_exception_handler(void)
{
  unsigned int     tid, ip, sp, badva, cause;
  union arch_coredump_union *p_regs=&coredump.arch.regs;
  unsigned int failure_count=0;
  int written_val = 0;
  
  for (;;)
  {
    /* Register self as Exception_Handler. */
    tid = ERR_REG_EXHNDLR(&ip, &sp, &badva, &cause);

    if (-1==tid)
    {
      written_val = tms_utils_fmt(qxdm_dbg_msg, sizeof(qxdm_dbg_msg),
        "Failed to register exception handler: tid=%x", tid);
      written_val = written_val; /*added to resolve compiler error :unsused(and ignored) return written_val */
      MSG_FATAL("Failed to register exception handler: tid=%x", tid, 0, 0);
      failure_count++;
      if(failure_count >= MAX_FAILURE_COUNT)
      {
        ERR_FATAL("Failed to register exception handler",0,0,0);
      }
      continue;
    }

    p_regs->name.pc = ip;
    p_regs->name.sp = sp;
    p_regs->name.badva = badva;
    p_regs->name.ssr = cause;
    
    written_val = tms_utils_fmt(qxdm_dbg_msg, sizeof(qxdm_dbg_msg),
      "ExIPC: Exception recieved tid=%x inst=%x", tid, ip);
    written_val = written_val; /*added to resolve compiler error :unsused(and ignored) return written_val */
    /* enter critical section */
    err_fatal_lock();
    /* Halt other cores */
    ERR_FATAL_ENTER_SINGLE_THREADED_MODE();

    if(err_next_to_STM_cb){
       err_next_to_STM_cb();
    }
    /* perform higher level error logging - no return */                                                    \
    err_fatal_jettison_core ( 0, exception_info[EXCP_UNKNOWN], "Exception detected", 0, 0, 0);
  }
} /* end of err_exception_handler */



