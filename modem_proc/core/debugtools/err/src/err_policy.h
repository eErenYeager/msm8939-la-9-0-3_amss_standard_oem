#ifndef ERR_POLICY_H
#define ERR_POLICY_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                      ERR Service Policy Module
                
GENERAL DESCRIPTION
  Repository for code asociated with policy decisions (security etc)

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE
$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_policy.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/14/13   mcg     File created.

===========================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/*===========================================================================

                     EXTERNAL DATA DECLARATIONS

===========================================================================*/
typedef enum
{
  ERR_POLICY_DENIED,
  ERR_POLICY_ALLOWED
} err_policy_result;

/*===========================================================================

                     EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
err_policy_result err_policy_allow_override(void);





#endif /* ERR_POLICY_H */
