/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                      ERR Service Policy Module
                
GENERAL DESCRIPTION
  Repository for code asociated with policy decisions (security etc)

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE
$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_policy.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/14/13   mcg     File created.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "err_policy.h"




/*===========================================================================

                     EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/




/*===========================================================================

                     INTERNAL DATA DECLARATIONS

===========================================================================*/


/*===========================================================================

                        Private function prototypes

===========================================================================*/




/*===========================================================================

                              Function definitions

===========================================================================*/

/*===========================================================================

FUNCTION err_policy_allow_override

DESCRIPTION
  Determines whether current policy allows test code to override err_fatal

DEPENDENCIES
  SSM (Secure Service Module) support pending

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
err_policy_result err_policy_allow_override (void)
{

  return ERR_POLICY_ALLOWED;
                                         
}

