/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                      ERR Service Policy Module
                
GENERAL DESCRIPTION
  Repository for code asociated with policy decisions (security etc)

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE
$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_override.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/14/13   mcg     File created.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "err_policy.h"
#include "err_override.h"
#include "err_decompress.h"
#ifdef ERR_OS_QURT
  #include "err_qurt.h"
#endif //ERR_OS_QURT




/*===========================================================================

                     EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
extern void err_emergency_error_recovery(void);



/*===========================================================================

                     INTERNAL DATA DECLARATIONS

===========================================================================*/
static err_override_func_ptr err_override_func = 0;

/*===========================================================================

                        Private function prototypes

===========================================================================*/




/*===========================================================================

                              Function definitions

===========================================================================*/


/*=========================================================================

FUNCTION err_override_reg

DESCRIPTION
 Registers an override function to facilitate automated testing.

DEPENDENCIES
  None

RETURN VALUE 
  ERR_OVERRIDE_SUCCESS  if registered successfully 
  ERR_OVERRIDE_BADINPUT if null pointer passed in
  ERR_OVERRIDE_CONFLICT if a different function is already registered
  ERR_OVERRIDE_DENIED   if blocked by current security policies

SIDE EFFECTS
  None

===========================================================================*/
err_override_result err_override_reg(err_override_func_ptr func)
{
  if(func == 0)
  {
    return ERR_OVERRIDE_BADINPUT;
  }
  if((err_override_func != 0)&&(err_override_func != func))
  {
    return ERR_OVERRIDE_CONFLICT;
  }
  if(ERR_POLICY_ALLOWED == err_policy_allow_override())
  {
    err_override_func = func;
    return ERR_OVERRIDE_SUCCESS;
  }

  err_override_func = 0;
  return ERR_OVERRIDE_DENIED;
}

/*=========================================================================

FUNCTION err_override_dereg

DESCRIPTION
 Deregisters the override function.

DEPENDENCIES
  None

RETURN VALUE
  ERR_OVERRIDE_SUCCESS  if deregistered successfully
  ERR_OVERRIDE_BADINPUT if null pointer passed in
  ERR_OVERRIDE_CONFLICT if pointer does not match registered function

SIDE EFFECTS
  None

===========================================================================*/
err_override_result err_override_dereg(err_override_func_ptr func)
{
  if(func == 0)
  {
    return ERR_OVERRIDE_BADINPUT;
  }
  if(err_override_func != func)
  {
    return ERR_OVERRIDE_CONFLICT;
  }

  err_override_func = 0;
  return ERR_OVERRIDE_SUCCESS;
}

/*=========================================================================

FUNCTION err_override

DESCRIPTION
 Execute override if apropos
 MAY NOT RETURN

DEPENDENCIES
  None

RETURN VALUE 
  None 

SIDE EFFECTS
  Will unregister the err_override_func
  Will unlock err_mutex if exitting error

===========================================================================*/
void err_override( ERR_MUTEX_TYPE * err_mutex, err_fatal_params_type const * const_params)
{
  // nominal case  always keep this first for optimal performance
  if(err_override_func == 0)
  {
    return;
  }
  if(ERR_POLICY_ALLOWED == err_policy_allow_override())
  {
    //copy the data in case another err_fatal happens after we unlock the mutex
    static err_fatal_params_type params;
    static err_decompress_struct_type decomp_data;

    /* decompress err struct into decomp_data*/
    err_decompress_msg_const(const_params->msg_const_ptr, &decomp_data);

    /* copy data to params struct */
    params.line=decomp_data.msg_blk.desc.line;

    params.file_name=decomp_data.msg_blk.fname;
    params.format=decomp_data.msg_blk.fmt;
    params.param1=const_params->param1;
    params.param2=const_params->param2;
    params.param3=const_params->param3;
    params.msg_const_ptr=const_params->msg_const_ptr;

    err_override_func_ptr temp_func_ptr = err_override_func;
    err_override_func = 0;
    ERR_MUTEX_UNLOCK(err_mutex);
    temp_func_ptr(&params);
    
    // If we get here, confusion ensues
    err_emergency_error_recovery();
  }
 }
