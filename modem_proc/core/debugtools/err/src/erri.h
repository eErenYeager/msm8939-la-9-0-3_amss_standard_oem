#ifndef ERRI_H
#define ERRI_H

/*===========================================================================

                    Error Handling Service Internal Header File

Description

Copyright (c) 2009 by Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/erri.h#1 $

===========================================================================*/

#include "core_variation.h"

#include "comdef.h"
#include "err.h"
#include "errlog.h"
#include "err_types.h"

#if defined(ERR_HW_QDSP6)
  #include "err_hw_qdsp6.h"
#else
  #include "err_hw_arm.h"
#endif

#ifdef ERR_OS_QURT
  #include "err_qurt.h"
#endif //ERR_OS_QURT

#if defined(ERR_IMG_MPSS)
  #include "err_img_mpss.h"
#endif



boolean err_log_store ( word , const char* , boolean );
void err_nv_log_init ( void );
void err_pause_usec(uint32 usec);
boolean err_get_online_status ( void );

boolean err_data_is_valid( void );


#endif /* ERRI_H */
