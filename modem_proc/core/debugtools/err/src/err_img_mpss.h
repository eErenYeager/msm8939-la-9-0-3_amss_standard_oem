#ifndef ERR_IMG_MPSS_H
#define ERR_IMG_MPSS_H
 
/*===========================================================================

                    Error Handling Service Internal Header File

Description
 
Copyright (c) 2012 by Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_img_mpss.h#1 $
 
===========================================================================*/



#include "core_variation.h"
#ifndef ERR_IMG_MPSS
#error BAD CONFIGURATION: CHECK DEFINITIONS
#endif



#define ERR_DIAG_PROC_ID       DIAG_MODEM_PROC
#define ERR_DIAG_PROC_BASE     ERR_DIAG_PROC_MPSS

#define ERR_SSR_REASON_SMEM_TYPE SMEM_SSR_REASON_MSS0

#define ERR_MSG_PREFIX "MPSS "


#endif //ERR_IMG_WCNSS_H
