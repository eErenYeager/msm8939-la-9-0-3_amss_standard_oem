#ifndef ERR_TYPES_H
#define ERR_TYPES_H

/*===========================================================================

                    Error Handling Service Typedefs Header File

Description

Copyright (c) 2013 by Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_types.h#1 $

===========================================================================*/

#include "errlog.h"
#include "msg.h"

typedef unsigned long long err_data_flag_type;
typedef unsigned long err_param_t;

#define ERR_FILE_NAME_LEN 8
typedef struct
{
  byte address;         /* Storage address 0 to ERR_MAX_LOG-1 */
  byte err_count;       /* Number of occurances (0=empty,FF=full) */
  byte file_name[ERR_FILE_NAME_LEN];
  word line_num;
  boolean fatal;
} err_log_type;

/* Maximum size of the extended info written out to smem. */
#ifndef ERR_DATA_MAX_SIZE
#define ERR_DATA_MAX_SIZE 0x4000
#endif
typedef struct {
  err_data_flag_type flag;
  err_log_type nv_log;
  unsigned long length;
  char   data[ERR_DATA_MAX_SIZE];
} err_data_log_type;

typedef struct {
  err_data_flag_type reset_flag;
  err_data_log_type err_log;
} err_data_type;

typedef struct {
  unsigned int line;     /* __LINE__ */
  const char *file_name; /* __FILENAME__ */
  const char *format;    /* printf format err message */
  err_param_t param1;    /* printf arg1 */
  err_param_t param2;    /* printf arg2 */
  err_param_t param3;    /* printf arg3 */
  const msg_const_type* msg_const_ptr; /* orignal (compressed) msg_const */
} err_fatal_params_type;


#endif /* ERR_TYPES_H */
