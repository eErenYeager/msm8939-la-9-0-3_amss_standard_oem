#ifndef ERR_OVERRIDE_H
#define ERR_OVERRIDE_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                      ERR Service Policy Module
                
GENERAL DESCRIPTION
  Repository for code asociated with policy decisions (security etc)

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE
$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err_override.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/14/13   mcg     File created.

===========================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "err_types.h"

/*===========================================================================

                     TYPE DEFINITIONS

===========================================================================*/
typedef enum
{
  ERR_OVERRIDE_SUCCESS,
  ERR_OVERRIDE_CONFLICT,
  ERR_OVERRIDE_BADINPUT,
  ERR_OVERRIDE_DENIED
} err_override_result;


//This is a function pointer definition, if confused ask google or Stroustrup
typedef void (*err_override_func_ptr)( err_fatal_params_type const * );

//FOR REFERENCE (found in err_types.h)
// typedef unsigned long err_param_t;
// typedef struct {
//   unsigned int line;     /* __LINE__ */
//   char *file_name;       /* __FILENAME__ */
//   char *format;          /* printf format err message */
//   err_param_t param1;    /* printf arg1 */
//   err_param_t param2;    /* printf arg2 */
//   err_param_t param3;    /* printf arg3 */
// } err_fatal_params_type;


/*===========================================================================

                     EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/


/*=========================================================================

FUNCTION err_override_reg

DESCRIPTION
 Registers an override function to facilitate automated testing.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if registered
  FALSE otherwise

SIDE EFFECTS
  None

===========================================================================*/
err_override_result err_override_reg(err_override_func_ptr func);

/*=========================================================================

FUNCTION err_override_dereg

DESCRIPTION
 Deregisters the override function.

DEPENDENCIES
  None

RETURN VALUE
  ERR_OVERRIDE_BADINPUT if null pointer passed in
  ERR_OVERRIDE_CONFLICT if pointer does not match registered function
  ERR_OVERRIDE_SUCCESS  if deregistered successfully

SIDE EFFECTS
  None

===========================================================================*/
err_override_result err_override_dereg(err_override_func_ptr func);



#endif /* ERR_OVERRIDE_H */
