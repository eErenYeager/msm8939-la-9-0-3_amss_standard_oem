/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         E R R O R     R E P O R T I N G    S E R V I C E S

GENERAL DESCRIPTION
  This module provides error reporting services for both fatal and
  non-fatal errors.  This module is not a task, but rather a set of
  callable procedures which run in the context of the calling task.

Copyright (c) 1992-2013 by Qualcomm Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        Edit History

$Header: //components/rel/core.mpss/3.7.24/debugtools/err/src/err.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/18/13   rks     CR589332:Adding an un-cached variable to figure out faulty 
                   call back functionin error handling
09/30/13   rks     CR526960: replace memcpy with memscpy
12/10/08   tbg     Removed INTLOCK usage for QDSP6
09/15/08   tbg     Integration changes for MDM8200
07/15/08   tbg     Code clean up to remove older features
04/04/08   tbg     Added support for native rex environment
11/30/07   tbg     Updated automation settings for FEATURE_ERR_DEBUG_HANDLER.
10/08/07   tbg     Some code cleanup + added 6k support.
02/07/07   as      Fixed lint critical.
02/01/07   bfc     Fixed a reference to an obsolete SMEM API
09/26/07   hwu     Use new APIs to reduce multiprocessor specific code.
01/08/07   tbg     Added support for new err_auto_action API.  Also updated
                   ERR_DEBUG_HANDLER to support THIN_UI builds.
12/15/06   as      Fixed critical lint errors.
11/02/06   tbg     Fixed additional issues with MDP driver in new error handler
10/19/06   tbg     Updated ERR_EXTENDED_STORE to work on 7k MSMs.  Also added
                   new feature FEATURE_ERR_DEBUG_HANDLER.
09/26/06   ptm     Use new APIs to reduce multiprocessor specific code.
01/19/06   pc      Removed data in all records in err_cache on err_clr.
12/12/05   tbg     Switch flag for mem_info from boolean to 32bit.
                   Changed sprintf calls to snprintf.
11/11/05   tbg     Changed ERR_EXTENDED_STORE to share NV item for max files
                   with debug trace (if feature is defined)
10/31/05   as      Fixed lint errors.
08/30/05   ih      Added return of 1 to err_reset_check when a reset is
                   detected.
08/11/05   as      Fixed complier warnings.
06/06/05   as      Added checks to do brew heap dump: if err_reset_flag is
                   true and if BREW heap variables are initialized.
04/17/04   ash     Added (FEATURE_MEM_USAGE_AT_EXCEPTION)
                   for application memory usage during crash
02/16/05   as      Added a check in err_log_store to write to NV only if
                   tasks are not locked.
02/15/05   as      Added FEATURE_DIAG_DEBUG_6275. This temporary feature will
                   be used until Raven code is stabilized.
11/30/04   as      Added FEATURE_AUTO_RESET. This feature resets the phone
                   automatically without user intervention if the phone
                   encounters a critical failure
10/07/04   tbg     Added FEATURE_ERR_SAVE_CORE.
08/03/04   tbg     Extended FEATURE_ERR_EXTENDED_STORE to support multiple
                   files on EFS, as well as multiple logs per session.
05/14/04   eav     Added FEATURE_SAVE_DEBUG_TRACE.
04/27/04   eav     Added FEATURE_SAVE_DEBUG_TRACE.  Moved ERR_DATA_MAGIC_NUMBER
                   and ERR_DATA_RESET_MAGIC_NUMBER to err.h, so they are
                   accessible in msg.c.
04/06/04   tbg     Added FEATURE_ERR_EXTENDED_STORE
10/28/03   as      Changes to fix errors when compiling with RVCT2.0
10/08/03   gr      Redefined ERR_FATAL to make one function call instead of
                   three to save code space.
07/24/03   as      Added ERR_FATAL_REPORT(   ) macro
03/04/03   djm     WPLT only modification to display LED uptime counter when
                   ERR_FATAL is encountered.
08/20/02   lad     PLATFORM_LTK and F_DIAG_COUPLED_ARCHITECTURE support.
06/10/02   cs      Updated err_init to read all the NV log records.
02/12/02    gr     Modified err_fatal_put_log to support five digit line
                   numbers.
06/27/01   lad     Cosmetic changes.
04/06/01   lad     Moved local definition of nv_err_log_type.
                   Changed use of NV_MAX_ERR_LOG to ERR_MAX_LOG.
02/23/01   lad     Featurized coupled code and did some cleanup.
08/28/00   lad     Added T_O to condition for ERR_HAS_LCD_MESSAGE.
05/12/99   lad     Fixed err_log_store() to give fatal errors prescedence
                   over non-fatal errors.
11/15/98   jct     Removed pragmas at top of file, moved _disable prototype
                   inside a check to see if the method is already defined
11/05/98   jct     Merged in changes for ARM
09/02/98   mk      Added hw_power_off() to err_fatal_put_log() for MSM3000.
05/15/98   bns     Added support for SSS2 phones.
08/08/97   dak     Fixed ERR_FATAL handling for WLL targets. Jump to download
                   if phone is on hook. Define ERR_FORCE_DOWNLOAD to jump
                   to downloader on very early ERR_FATALs.
07/23/97   jah     Turn off ringer on MSM 2 phones in err_fatal_put_log().
07/02/97   jjn     Change on 05/23/97 was for Module hardware with 20pin
                   SAMTEC connector.  SYS_WAKE is on a different GPIO pin
                   on 30pin SAMTEC connector for X2 Module hardware.
                   Software has to be changed to accompany that HW change.
06/17/97   dhh     Added target T_Q for the Q Phones.
05/23/97   jjn     Assert SYS_WAKE when ERR_FATAL occurs (for the Module only)
01/22/97   rdh     Used Pascal directive on err_*_put_log to save ROM.
11/02/96   jah     Configured for TGP (T_T), added ERR_HAS_LCD_MESSAGE
05/24/96    ls     Fixed reentrancy problems and other errors.
07/21/95   ras     disabled interrupts in err_fatal_put_log
07/05/95   jah     Added lcd_message() for Gemini
07/16/93   jah     Changed LCD_MESSAGE() to lcd_message(), and updated
                   comments, per code review.
06/29/93   jah     Added LCD_MESSAGE support for LCD display of fatal errors.
04/27/93   jah     Made T_P changes to support fatal error test code download.
10/29/92    ip     Updated for full error logging and reporting.
08/13/92    ip     Release with interim indefinite wait for ERR_FATAL.
03/07/92    ip     Initial creation.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "core_variation.h"
#include "comdef.h"
#include "err.h"
#include "errlog.h"
#include "erri.h"
#include "err_types.h"
#include "err_reset_detect.h"
#include "err_override.h"
#include "err_decompress.h"
#include "msg.h"
#include <stdio.h>
#include "stringl.h"
#include "stdlib.h"
#include "fs_public.h"
#include "dog.h"
#include "DALSys.h"
#include "DDITimetick.h"
#include "timetick.h"
#include "sys_m.h"
#include "tms_utils.h"
#ifdef FEATURE_NV
  #include "nv.h"
#endif //FEATURE_NV

#ifdef ERR_INJECT_CRASH
  #include "err_inject_crash.h"
#endif //ERR_INJECT_CRASH

#ifdef FEATURE_SMEM
#include "smem.h"
#endif /* FEATURE_SMEM */

#ifdef FEATURE_SMEM_LOG
#include "smem_log.h"
#endif /* FEATURE_SMEM */

#ifdef IMAGE_QDSP6_PROC
#include "qurt.h"
#include "qube.h"
#endif /* IMAGE_QDSP6_PROC */

#ifdef FEATURE_L4
#include <l4/cust/msm_syscall.h>
#endif

/*===========================================================================

                      Prototypes for internal functions

===========================================================================*/
volatile err_cb_ptr err_current_cb= NULL;
#define ERR_SET_AND_FLUSH_PTR(PTR, VAL) \
   do { \
PTR = VAL; \
asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &PTR )));\
   } while (0)
void err_emergency_error_recovery(void);
static void err_resource_init(void);
static void err_smem_log(void);
static void err_format_message(void);
static void err_halt(void) ERR_NORETURN_ATTRIBUTE;
void err_halt_execution(void) ERR_NORETURN_ATTRIBUTE;  //asm function
static void err_raise_to_kernel(void);
void err_store_info(void);
static void err_log_ssr_failure_reason(void);
static void err_reentrancy_violated(void);
void err_fatal_handler(void) ERR_NORETURN_ATTRIBUTE;

extern void err_override( ERR_MUTEX_TYPE * err_mutex, err_fatal_params_type const * params);
/*===========================================================================

                 Defines and variable declarations for module

===========================================================================*/
err_data_type err_data;

#ifdef FEATURE_GENERIC_ERR_FATAL_MSG
/* Used to replace all ERR_FATAL message strings.
 * Reduces code size by eliminating other const strings used for errors.
 */
const char err_generic_msg[] = "Error Fatal, parameters: %d %d %d";
const char err_generic_msg_dynamic[] = "Error Fatal, check coredump.err.aux_msg";
#endif


/* Struct used to hold coredump data */
coredump_type coredump;
uint32 coredump_count=0;
timetick_type err_handler_start_time;
err_fatal_params_type err_fatal_params;
err_decompress_struct_type err_decomp_struct;

/* Ptr used by assembly routines to grab registers */
/*  (update this as needed if struct changes)      */
arch_coredump_type* arch_coredump_ptr = (arch_coredump_type*)(&coredump.arch.regs.array);

#if defined(ERR_F3_TRACE_TO_SMEM)
  /* Pointer to buffer used for SMEM crash log */
  static char *err_smem_log_buf;
#endif /* ERR_F3_TRACE_TO_SMEM */

#ifdef IMAGE_QDSP6_PROC
static ERR_MUTEX_TYPE err_fatal_mutex;
static boolean err_fatal_mutex_init = FALSE;
extern int qdsp6_user_assert(void);
#endif /* IMAGE_QDSP6_PROC */

static boolean err_services_init_complete = FALSE;

#if !defined(ERR_SINGLEPROC)
/* Static for LCD_MESSAGE (also saved to shared memory even without LCD
 * in multiprocessor builds) */
/* 112 is added to fix a critical lint error. */
#define ERR_SCRN_WIDE 12
#define ERR_SCRN_HIGH 4
/* Number of digits in error line number information. */
#define ERR_LINENUM_DIGITS 5
static char message[ ERR_SCRN_HIGH * ERR_SCRN_WIDE + 1 + 112];
#endif /* !ERR_SINGLEPROC */

static DalDeviceHandle* phTimetickHandle = NULL;

//SFR (Subsystem restart Failure Reason) decl's
#ifdef FEATURE_SMEM
  #define ERR_SSR_REASON_SIZE_BYTES 80
  char* err_ssr_smem_buf;
  static const char err_sfr_init_string[]="SFR Init: wdog or kernel error suspected.";
  static boolean err_sfr_locked = FALSE;
#endif //FEATURE_SMEM

//dynamic message scratch buffer
#ifdef FEATURE_SMEM
  // must fit into both SFR and coredump.err.aux_msg
  #define ERR_DYNAMIC_MSG_SIZE ( MIN(ERR_SSR_REASON_SIZE_BYTES, ERR_LOG_MAX_MSG_LEN) )
#else
  #define ERR_DYNAMIC_MSG_SIZE ( ERR_LOG_MAX_MSG_LEN )
#endif  //FEATURE_SMEM
static char* err_dynamic_msg_scratch_buf = 0;

err_cb_ptr err_next_to_STM_cb = NULL;

/* The function tables below are processed by the error handler
 * in the following order:
 *   1. err_preflush_internal (one time)
 *   2. err_preflush_external (one time)
 *   3. err_flush_internal    (one time - transitions to non-returning kernel code)
 */

static const err_cb_ptr err_preflush_internal[] =
{
  err_resource_init,
  err_smem_log,
  err_store_info,
  err_format_message,
  err_log_ssr_failure_reason,
  /* NULL must be last in the array */
  NULL
};

#define ERR_MAX_PREFLUSH_CB 20
static err_cb_ptr err_preflush_external[ERR_MAX_PREFLUSH_CB] = {NULL};

static const err_cb_ptr err_flush_internal[] =
{
  err_raise_to_kernel,   /* Does not return */
  /* NULL must be last in the array */
  NULL
};

#ifdef FEATURE_SMEM_LOG
/* Event defines for using SMEM to log what occurs during sleep and
 * power collapse.
 */
#define ERR_ERROR_FATAL                (SMEM_LOG_ERROR_EVENT_BASE + 1)
#define ERR_ERROR_FATAL_TASK           (SMEM_LOG_ERROR_EVENT_BASE + 2)

#endif /* FEATURE_SMEM_LOG */

/*===========================================================================

                              Function definitions

===========================================================================*/


/*===========================================================================

FUNCTION ERR_INIT

DESCRIPTION
  This function checks if NV has been built.  If so then it loads up the
  error log cache from NV, it initializes error counts and local variables,
  and it sets error services to online.  If NV has not been built then
  error log is not loaded and error services remain offline and error
  logging does not take place.  Any access to NV is performed here directly,
  without relying on the NV task.  Thus error service can be initialized
  and used early in the life of the DMSS and right after boot.  Note
  that if NV has not been built, error services provide no error logging
  capabilities.  However NV is built once, after initial factory startup,
  and rarely thereafter.  Thus except for the first ever powerup of the
  unit after NV build or re-build, error logging will take place as usual.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  Error log is loaded from NVM into RAM resident cache.

===========================================================================*/

void err_init (void)
{
#ifdef FEATURE_NV
  nv_err_fatal_options_type option;
#endif //FEATURE_NV

  /* Init Timer */
  DalTimetick_Attach("SystemTimer", &phTimetickHandle);

#ifdef IMAGE_QDSP6_PROC
  if(!err_fatal_mutex_init)
  {
    ERR_MUTEX_INIT(&err_fatal_mutex);
    err_fatal_mutex_init=TRUE;
  }
#endif

#ifdef FEATURE_SMEM
  err_ssr_smem_buf = sys_m_init_sfr_buffer();
  if (NULL != err_ssr_smem_buf)
  {
    strlcpy (err_ssr_smem_buf, err_sfr_init_string, ERR_SSR_REASON_SIZE_BYTES);
  }
#endif

#ifdef ERR_INJECT_CRASH
  err_inject_crash_init();
#endif //ERR_INJECT_CRASH

  err_nv_log_init();

#if defined(ERR_F3_TRACE_TO_SMEM)
  err_smem_log_buf = (char *)smem_alloc(SMEM_ERR_CRASH_LOG,ERR_DATA_MAX_SIZE);
#endif /* ERR_F3_TRACE_TO_SMEM */

  //We use a scratch buf from the heap to minimize risk of an 
  //overwrite corrupting the coredump structure or stack
  err_dynamic_msg_scratch_buf = (char*)malloc(ERR_DYNAMIC_MSG_SIZE);
  if (err_dynamic_msg_scratch_buf)
  {
    memset (err_dynamic_msg_scratch_buf, 0, ERR_DYNAMIC_MSG_SIZE);
  }

  MSG_LOW( "Err service initialized.",0,0,0);

  err_services_init_complete = TRUE;

} /* err_init */

/*===========================================================================

FUNCTION ERR_FORMAT_MESSAGE
DESCRIPTION
  Creates error message in the global 'message'.
  Used as part of SMEM logging, also passed to lcd_message in single processor
  targets.
============================================================================*/
static void err_format_message()
{
#if !defined(ERR_SINGLEPROC)
  word line = coredump.err.linenum;
  char *file_ptr = coredump.err.filename;
  int i;
  int len;
  int msg_line;

  {
    /* Copy the filename to the LCD message buffer, max ERR_SCRN_WIDE */
    #ifdef ERR_MSG_PREFIX
    /* Subtract one for NULL character */
    i = sizeof(ERR_MSG_PREFIX)-1;
    memscpy( message, sizeof(message), ERR_MSG_PREFIX, i);
    #else
    i = 0;
    #endif /* if ERR_MSG_PREFIX */

    /* Copy as much of the file name as possible,
     but leave at least 1 text line for displaying the line number */
    len = strlen( file_ptr );
    if(len > (ERR_SCRN_HIGH - 1) * ERR_SCRN_WIDE - i )
    {
      len = (ERR_SCRN_HIGH - 1) * ERR_SCRN_WIDE - i;
    }

    memscpy( &message[i], sizeof(message) - i, file_ptr, len );

    i += len;
    msg_line = (i + ERR_SCRN_WIDE - 1)/ERR_SCRN_WIDE;

    /* Pad the rest of the line with spaces */
    for ( ; i < msg_line * ERR_SCRN_WIDE; i++)
      message[i] = ' ';

    /* Print a line number on the next line - there's always at least 1 */
    for (i += ERR_LINENUM_DIGITS-1; i >= msg_line * ERR_SCRN_WIDE; i--)
    {
      message[ i ] = '0' + (line % 10);
      line /= 10;
    }

    /* Set up i to pad the message with spaces */
    i = msg_line * ERR_SCRN_WIDE + ERR_LINENUM_DIGITS;

    /* Pad the message with spaces */
    for ( ; i < (ERR_SCRN_HIGH * ERR_SCRN_WIDE); i++)
    {
      message[i] = ' ';
    }

    message[i] = '\0';            /* NUL terminate the string */

  }

#endif /* !ERR_SINGLEPROC || FEATURE_ERR_LCD */
}

/*=========================================================================

FUNCTION ERR_SMEM_LOG
DESCRIPTION
  Creates error event in SMEM log containing filename and taskname.
===========================================================================*/
static void err_smem_log(void)
{
#ifdef FEATURE_SMEM_LOG
  uint32 name[5];  //this is intentionally uint32, not char, for smem macro
  rex_tcb_type *tcb_ptr = (rex_tcb_type*)coredump.os.tcb_ptr;
#ifdef FEATURE_REX_OPAQUE_TCB_APIS
  size_t name_len;
  char rex_task_name[REX_TASK_NAME_LEN+1];
#else
  uint32 name_len;
#endif //FEATURE_REX_OPAQUE_TCB_APIS

  if(tcb_ptr)
  {
#ifdef FEATURE_REX_OPAQUE_TCB_APIS
    rex_get_task_name(tcb_ptr, rex_task_name, sizeof(rex_task_name), &name_len);

    name_len = MIN( sizeof(name), sizeof(rex_task_name) );
    strlcpy ((char*)name, rex_task_name, name_len);
#else
    name_len = MIN( sizeof(name), sizeof(tcb_ptr->task_name) );
    memscpy (name, sizeof(name), (void*)(tcb_ptr->task_name), name_len);
#endif //FEATURE_REX_OPAQUE_TCB_APIS
    SMEM_LOG_EVENT6_STM(ERR_ERROR_FATAL_TASK,
      name[0], name[1], name[2], name[3], name[4], (uint32) tcb_ptr);

    name_len = 1 + strlen(coredump.err.filename);
    memscpy (name, sizeof(name), coredump.err.filename, name_len);
    SMEM_LOG_EVENT6_STM(ERR_ERROR_FATAL,
      name[0], name[1], name[2], name[3], name[4], coredump.err.linenum);
  }
#endif /* FEATURE_SMEM_LOG */

}


/*===========================================================================

FUNCTION ERROR_FATAL_HANDLER

DESCRIPTION
  This function is invoked from err_fatal_jettison_core. When using JTAG,
  default breakpoint for ERR_FATAL should be placed at this function.
  Will log error to SMEM, kill the PA, and copy the coredump data into
  the err_data structure in unintialized memory.


DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS
  **************************************************************
  ************ THERE IS NO RETURN FROM THIS FUNCTION ***********
  **************************************************************

===========================================================================*/
void err_fatal_handler ( void )
{
  int fptr_index;
  static uint32 err_count=0;

  err_count++;

  if((err_count>1) || (err_services_init_complete!=TRUE))
  {
    /* May not return */
    err_emergency_error_recovery();
  }

  fptr_index=0;
  while(err_preflush_internal[fptr_index] != NULL)
  {
    /* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_preflush_internal[fptr_index]); 
    err_current_cb();
    fptr_index++;
  }

  for(fptr_index=0; fptr_index<ERR_MAX_PREFLUSH_CB; fptr_index++)
  {
    /* Cycle through external functions */
    if(err_preflush_external[fptr_index]!= 0)
    {
      ERR_SET_AND_FLUSH_PTR(err_current_cb, err_preflush_external[fptr_index]); 
      err_current_cb();
    }
  }

  /* Main loop (cache flush happens here, along with other
   * one-time post-flush operations */
  fptr_index=0;
  while(err_flush_internal[fptr_index] != NULL)
  {
    /* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_flush_internal[fptr_index]); 
    err_current_cb();
    fptr_index++;
  }

  // We must not reach here
  err_halt();

} /* err_fatal_handler */


/*===========================================================================

FUNCTION ERR_FATAL_LOCK
DESCRIPTION
  Gets mutex for err_fatal to prevent multiple and/or cascading errors
============================================================================*/
void err_fatal_lock(void)
{
  static boolean err_reentrancy_flag = FALSE;
  if(err_fatal_mutex_init==TRUE)
  {
    ERR_MUTEX_LOCK(&err_fatal_mutex);

    //mutex does not prevent the same thread being routed back into err_fatal by a bad callback
    if(err_reentrancy_flag)
    {
      //does not return
      err_reentrancy_violated();
    }
    else
    {
      err_reentrancy_flag = TRUE;
    }
  }
  else
  {
    /* If not intialized then this is an early ERR_FATAL */
    /* Proceed anyway so it can be handled */
  }

  /* Get timestamp of error (early for accuracy) */
  time_get((unsigned long *)&coredump.err.timestamp);
}


/*===========================================================================

FUNCTION ERR_FATAL_CORE_DUMP
DESCRIPTION
  Logs fatal error information, including a core dump.

  NOTE: There is no return from this function.
============================================================================*/
void err_fatal_core_dump (
  unsigned int line,      /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format   /* format string */
)
{
  err_fatal_lock();
  err_fatal_jettison_core(line, file_name, format, 0, 0, 0);
}


/*===========================================================================

FUNCTION ERR_FATAL_JETTISON_CORE
DESCRIPTION
  Logs fatal error information, including a core dump.
  Not to be called directly by outside code -- for that, use the function
  err_fatal_core_dump().

  NOTE: There is no return from this function.
============================================================================*/
void err_fatal_jettison_core (
  unsigned int line,       /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format,    /* format string */
  uint32 param1,
  uint32 param2,
  uint32 param3
)
{
  /* NOTE: register information should already be saved prior to
   * calling this function.
   */

  /* Set type and version values */
  coredump.version = ERR_COREDUMP_VERSION;
  coredump.arch.type = ERR_ARCH_COREDUMP_TYPE;
  coredump.arch.version = ERR_ARCH_COREDUMP_VER;
  coredump.os.type = ERR_OS_COREDUMP_TYPE;
  coredump.os.version = ERR_OS_COREDUMP_VER;
  coredump.err.version = ERR_COREDUMP_VER;

  /* Get tcb_ptr (if not pre-filled by err_exception_handler) */
  if (!coredump.os.tcb_ptr) {
  coredump.os.tcb_ptr = (ERR_OS_TCB_TYPE*)rex_self();
  }

  /* Store line number */
  coredump.err.linenum = line;

  /* Copy file name */
  if(file_name != 0)
  {
    (void) strlcpy((char *)coredump.err.filename,
                       (char *)file_name,
                       ERR_LOG_MAX_FILE_LEN);
  }

  /* Copy message string */
  if(format != 0)
  {
    (void) strlcpy((char *)coredump.err.message,
                       (char *)format,
                       ERR_LOG_MAX_MSG_LEN);
  }

  coredump.err.param[0]=param1;
  coredump.err.param[1]=param2;
  coredump.err.param[2]=param3;

  /* Call ERR_FATAL handler (no return) */
  err_fatal_handler();

}


/*===========================================================================

FUNCTION ERR_DATA_IS_VALID

DESCRIPTION
  False dependency from diag_f3_trace
============================================================================*/
boolean err_data_is_valid()
{
  return FALSE;
}

/*===========================================================================

FUNCTION ERR_STORE_INFO
DESCRIPTION
  Writes some crash related information to a designated RAM buffer.
============================================================================*/
#define ERR_DATA_MAGIC_NUMBER       (uint64)0x5252452041544144ULL
void err_store_info ()
{
  char *buf = NULL;

  /* If magic numbers are present at the front of the buffer, then previous
   * data has not been written out yet. Return without doing anything in
   * this case. -- this is probably obsolete without nzi, but cheap protection
   */
  if(err_data.err_log.flag == ERR_DATA_MAGIC_NUMBER)
  {
    /* Buffer is already full */
    return;
  }
  err_data.err_log.flag = ERR_DATA_MAGIC_NUMBER;

  /* Copy data needed to store NV log */
  err_data.err_log.nv_log.line_num = coredump.err.linenum;
  err_data.err_log.nv_log.fatal = TRUE;
  (void) strlcpy((char *)err_data.err_log.nv_log.file_name,
					       (char *)coredump.err.filename,
                 MIN( sizeof(err_data.err_log.nv_log.file_name), sizeof(coredump.err.filename) ));

  buf = (char *) err_data.err_log.data;

  /* Interrupts are disabled at this point -- need to make sure we don't
   * timeout.  Use force_kick to avoid intlock attempt in isr (possibly)
   */
  dog_force_kick();
  (void) memset (buf, 0x00, ERR_DATA_MAX_SIZE);
  err_log_init(buf, ERR_DATA_MAX_SIZE);
  err_data.err_log.length = err_generate_log();

#if defined (ERR_F3_TRACE_TO_SMEM)
  /* If this is the first error logged, copy it to the smem buffer */
  if(i==0)
  {
    /* If we have ptr to ERR smem log region, copy log buffer */
    if(err_smem_log_buf!=NULL)
    {
      memscpy((void*)err_smem_log_buf, ERR_DATA_MAX_SIZE, (void*)buf, ERR_DATA_MAX_SIZE);
    }
    //actual f3 trace is handled exclusively by diag now (using err_crash_cb_register)
  }
#endif /* ERR_F3_TRACE_TO_SMEM */

  return;
} /* err_store_info */

/*=========================================================================

FUNCTION err_resource_init

DESCRIPTION
  Any misc code needed before error handling can proceed

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

static void err_resource_init( void )
{
  /* Misc code needed before ERR handling can proceed */

  if (NULL != phTimetickHandle)
  {
    //best effort, no failure action
    DalTimetick_Get(phTimetickHandle, &(err_handler_start_time));
  }

}


/*=========================================================================

FUNCTION err_emergency_error_recovery

DESCRIPTION
  Action to be taken when more than one error has occurred, or if an
  error occurs before err_init has completed.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  No return from this function

===========================================================================*/

void err_emergency_error_recovery( void )
{
  /* Define action to be taken when multiple crashes occur */
  err_raise_to_kernel();  // Give it to kernel

}

/*=========================================================================

FUNCTION err_halt

DESCRIPTION
  Stop processor from running

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void err_halt( void )
{
  /* Stops HW thread execution. No return */
  err_halt_execution();
}


/*=========================================================================

FUNCTION err_crash_cb_register

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called after an ERR_FATAL
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.

  !!!These functions MUST NOT call ERR_FATAL/ASSERT under ANY circumstances!!!

DEPENDENCIES
  None

RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_register(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(err_preflush_external[i] == NULL)
	{
	  err_preflush_external[i] = cb;
	  rval = TRUE;
	  break;
	}
  }

  return rval;
}


/*=========================================================================

FUNCTION err_crash_cb_dereg

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(err_preflush_external[i] == cb)
	{
	  err_preflush_external[i] = NULL;
	  rval = TRUE;
	  break;
	}
  }

  return rval;
}
/*=========================================================================

FUNCTION err_crash_cb_reg_next_to_STM

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called immediately after
  STM API call 
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.
 
  !!!These functions MUST NOT call ERR_FATAL under ANY circumstances!!!

DEPENDENCIES
  None
 
RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.

SIDE EFFECTS
  Only one registration of such API is supported so if its used by more than one
  clients than it will overwrite the old registered callback,
  this API was provided only for special case handling to stop ULT Audio Core
  in DPM PL
===========================================================================*/
boolean err_crash_cb_reg_next_to_STM(err_cb_ptr cb)
{
  if(NULL == err_next_to_STM_cb){/*check if already a callback registered*/
    err_next_to_STM_cb = cb;
    return TRUE;
  }
  else{
     return FALSE;
  }
}
/*=========================================================================

FUNCTION err_crash_cb_dereg

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg_next_to_STM(err_cb_ptr cb)
{
  if(err_next_to_STM_cb == cb){
    err_next_to_STM_cb = NULL;
    return TRUE;
  }
  else{
    return FALSE;
  }
}

/*=========================================================================

FUNCTION err_raise_to_kernel

DESCRIPTION
  Function which will terminate user space handling and raise to kernel

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void err_raise_to_kernel(void)
{
  #ifdef ERR_FATAL_FLUSH_CACHE_NO_RETURN
    ERR_FATAL_FLUSH_CACHE_NO_RETURN();
  #else
    #warning ERR_FATAL_FLUSH_CACHE_NO_RETURN not defined
  #endif

  /* Above should not have returned, let dog expire */
  while(1)
  {
    //compiler bug workaround for RVCT 4.1 P 631 & static analysis
    static volatile int err_always_false = 0;
    if (err_always_false)
    {
      return; /* this must NEVER return */
    }
  };

}

/*=========================================================================

FUNCTION err_log_ssr_failure_reason

DESCRIPTION
  Used to log a minimal set of failure reason to smem.  Primarily to assist
  locating faulting subsystem in many-subsystem architectures.

DEPENDENCIES
  smem

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void err_log_ssr_failure_reason(void)
{
#ifdef FEATURE_SMEM
  if (!err_sfr_locked)
  {
    word sfr_line = coredump.err.linenum;
    const char *sfr_file_ptr = coredump.err.filename;
    const char *sfr_msg_ptr = coredump.err.message;
    uint32 sfr_p0 = coredump.err.param[0];
    uint32 sfr_p1 = coredump.err.param[1];
    uint32 sfr_p2 = coredump.err.param[2];
    char *sfr_buf_ptr = err_ssr_smem_buf;
    int sfr_written = 0;

    if (sfr_buf_ptr && sfr_file_ptr)
    {
      // write "__MODULE__:__LINE:"
      sfr_written = tms_utils_fmt(sfr_buf_ptr, ERR_SSR_REASON_SIZE_BYTES, "%s:%d:", sfr_file_ptr, sfr_line);

      if ((sfr_written >= 0) && sfr_msg_ptr)
      {
        // append err fatal message
        if(sfr_written > TMS_UTILS_BUF_SZ_ZERO){
           sfr_buf_ptr += (sfr_written-1); /*-1 since tms_utils_fmt() API returns written length with NULL charector
                                              increment and overwrite previous null-term*/
        }
        sfr_written += tms_utils_fmt(sfr_buf_ptr, (ERR_SSR_REASON_SIZE_BYTES-sfr_written), sfr_msg_ptr, sfr_p0, sfr_p1, sfr_p2);
      }
      //commit the write before proceeding
      ERR_MEMORY_BARRIER();

      err_sfr_locked = TRUE;
    } //(sfr_buf_ptr && sfr_file_ptr)
  } //(!err_sfr_locked)

#endif // FEATURE_SMEM
}

/*===========================================================================

FUNCTION       err_SaveFatal3

DESCRIPTION
  Do not call any of these functions directly.  They should be accessed by
  macros defined in err.h exclusively.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void err_SaveFatal3 (const msg_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3)
{
 /* enter critical section */
 err_fatal_lock();
 /* capture registers */
 jettison_core();
  /* fill parameter struct - other fields require decompression */
 err_fatal_params.param1=code1;
 err_fatal_params.param2=code2;
 err_fatal_params.param3=code3;
 err_fatal_params.msg_const_ptr=const_blk;
 /* propose override - may not return */
 err_override(&err_fatal_mutex, &err_fatal_params);
 /* Halt other cores */
 ERR_FATAL_ENTER_SINGLE_THREADED_MODE();

 if(err_next_to_STM_cb){
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_next_to_STM_cb); 
    err_current_cb();
 }
 /* decompress err struct */
 err_decompress_msg_const(const_blk, &err_decomp_struct);
 /* Log failure reason */
 #if defined (FEATURE_SAVE_DEBUG_TRACE)
   msg_save_3(&(err_decomp_struct.msg_blk), code1, code2, code3, NULL);
 #endif /* FEATURE_SAVE_DEBUG_TRACE */
 /* Perform higher level error logging - no return */
 err_fatal_jettison_core (err_decomp_struct.msg_blk.desc.line,
                          err_decomp_struct.msg_blk.fname,
                          err_decomp_struct.msg_blk.fmt,
                          code1, code2, code3);

}

void err_SaveFatal2 (const msg_const_type* const_blk, uint32 code1, uint32 code2)
{

 /* enter critical section */
 err_fatal_lock();
 /* capture registers */
 jettison_core();
  /* fill parameter struct - other fields require decompression */
 err_fatal_params.param1=code1;
 err_fatal_params.param2=code2;
 err_fatal_params.param3=0;
 err_fatal_params.msg_const_ptr=const_blk;
 /* propose override - may not return */
 err_override(&err_fatal_mutex, &err_fatal_params);
 /* Halt other cores */
 ERR_FATAL_ENTER_SINGLE_THREADED_MODE();

 if(err_next_to_STM_cb){
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_next_to_STM_cb); 
    err_current_cb();
 }
 /* decompress err struct */
 err_decompress_msg_const(const_blk, &err_decomp_struct);
 /* Log failure reason */
 #if defined (FEATURE_SAVE_DEBUG_TRACE)
   msg_save_3(&(err_decomp_struct.msg_blk), code1, code2, 0, NULL);
 #endif /* FEATURE_SAVE_DEBUG_TRACE */
 /* Perform higher level error logging - no return */
 err_fatal_jettison_core (err_decomp_struct.msg_blk.desc.line,
                          err_decomp_struct.msg_blk.fname,
                          err_decomp_struct.msg_blk.fmt,
                          code1, code2, 0);

}

void err_SaveFatal1 (const msg_const_type* const_blk, uint32 code1)
{
 /* enter critical section */
 err_fatal_lock();
 /* capture registers */
 jettison_core();
  /* fill parameter struct - other fields require decompression */
 err_fatal_params.param1=code1;
 err_fatal_params.param2=0;
 err_fatal_params.param3=0;
 err_fatal_params.msg_const_ptr=const_blk;
 /* propose override - may not return */
 err_override(&err_fatal_mutex, &err_fatal_params);
 /* Halt other cores */
 ERR_FATAL_ENTER_SINGLE_THREADED_MODE();
 
 if(err_next_to_STM_cb){
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_next_to_STM_cb); 
    err_current_cb();
 }
 /* decompress err struct */
 err_decompress_msg_const(const_blk, &err_decomp_struct);
 /* Log failure reason */
 #if defined (FEATURE_SAVE_DEBUG_TRACE)
   msg_save_3(&(err_decomp_struct.msg_blk), code1, 0, 0, NULL);
 #endif /* FEATURE_SAVE_DEBUG_TRACE */
 /* perform higher level error logging - no return */
 err_fatal_jettison_core (err_decomp_struct.msg_blk.desc.line,
                          err_decomp_struct.msg_blk.fname,
                          err_decomp_struct.msg_blk.fmt,
                          code1, 0, 0);

}

void err_SaveFatal0 (const msg_const_type* const_blk)
{
 /* enter critical section */
 err_fatal_lock();
 /* capture registers */
 jettison_core();
  /* fill parameter struct - other fields require decompression */
 err_fatal_params.param1=0;
 err_fatal_params.param2=0;
 err_fatal_params.param3=0;
 err_fatal_params.msg_const_ptr=const_blk;
 /* propose override - may not return */
 err_override(&err_fatal_mutex, &err_fatal_params);
 /* Halt other cores */
 ERR_FATAL_ENTER_SINGLE_THREADED_MODE();
 
 if(err_next_to_STM_cb){
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_next_to_STM_cb); 
    err_current_cb();
 }
 /* decompress err struct */
 err_decompress_msg_const(const_blk, &err_decomp_struct);
 /* Log failure reason */
 #if defined (FEATURE_SAVE_DEBUG_TRACE)
   msg_save_3(&(err_decomp_struct.msg_blk), 0, 0, 0, NULL);
 #endif /* FEATURE_SAVE_DEBUG_TRACE */
 /* perform higher level error logging - no return */
 err_fatal_jettison_core (err_decomp_struct.msg_blk.desc.line,
                          err_decomp_struct.msg_blk.fname,
                          err_decomp_struct.msg_blk.fmt,
                          0, 0, 0);

}

/*===========================================================================

FUNCTION       err_Fatal_dynamic

DESCRIPTION
  Do not call any of these functions directly.  They should be accessed by
  macros defined in err.h exclusively.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void err_process_dynamic_callback(err_dynamic_msg_cb callback)
{
  if (err_services_init_complete && callback && err_dynamic_msg_scratch_buf)
  {
    callback(err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);

  //was something written?
  if (err_dynamic_msg_scratch_buf[0] != 0)
  {
    //guarantee null termination
    err_dynamic_msg_scratch_buf[ERR_DYNAMIC_MSG_SIZE-1]=0;

    //copy to relevant structures
    memscpy(coredump.err.aux_msg, sizeof(coredump.err.aux_msg), err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
#ifdef FEATURE_SMEM
    if (err_ssr_smem_buf&& !err_sfr_locked)
    {
      memscpy(err_ssr_smem_buf, ERR_SSR_REASON_SIZE_BYTES, err_dynamic_msg_scratch_buf, ERR_DYNAMIC_MSG_SIZE);
      err_sfr_locked = TRUE;
    }
#endif //FEATURE_SMEM
  }
}
}


void err_SaveFatal_dynamic (const msg_const_type* const_blk, err_dynamic_msg_cb callback)
{

 err_fatal_lock();
 jettison_core();
 /* fill parameter struct - other fields require decompression*/
 err_fatal_params.param1=0;
 err_fatal_params.param2=0;
 err_fatal_params.param3=0;
 err_fatal_params.msg_const_ptr=const_blk;
 /* propose override - may not return */
 err_override(&err_fatal_mutex, &err_fatal_params);
 /* Halt other cores */
 ERR_FATAL_ENTER_SINGLE_THREADED_MODE();
 
 if(err_next_to_STM_cb){
    ERR_SET_AND_FLUSH_PTR(err_current_cb, err_next_to_STM_cb); 
    err_current_cb();
 }
 /* Allow callee to fill SFR and aux_msg */
 err_process_dynamic_callback(callback);
 /* decompress err struct */
 err_decompress_msg_const(const_blk, &err_decomp_struct);
 /* Log failure reason */
 #if defined (FEATURE_SAVE_DEBUG_TRACE)
   msg_save_3(&(err_decomp_struct.msg_blk), 0, 0, 0, NULL);
 #endif /* FEATURE_SAVE_DEBUG_TRACE */
 /* Perform higher level error logging - no return */
 err_fatal_jettison_core (err_decomp_struct.msg_blk.desc.line,
                          err_decomp_struct.msg_blk.fname,
                          err_decomp_struct.msg_blk.fmt,
                          0, 0, 0);

}

/*=========================================================================

FUNCTION err_reentrancy_violated

DESCRIPTION
  This will only be called when ERR_FATAL is called while processing an
  ERR_FATAL.  It usually means that somone has registered a non-compliant
  callback function using 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
#define ERR_REENTRANCY_STRING "ERR_FATAL reentrancy violation, remove cb until resolved"
static void err_reentrancy_violated(void)
{
  /* Record secondary failure to coredump */
  strlcpy(coredump.err.aux_msg, ERR_REENTRANCY_STRING, sizeof(ERR_REENTRANCY_STRING));

  err_emergency_error_recovery();

}

