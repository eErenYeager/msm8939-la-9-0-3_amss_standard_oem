;============================================================================
;  Name:
;    spmi_bus.cmm
;
;  Description:
;    Tool to do R/W a byte on SPMI Bus
;
;  Execution:
;    Inside the debugger, type at the command line: 
;    For Read:  do spmi_bus.cmm Read  OwnerChannel[0-5] SlaveId[0-0xF] RegisterAddress[0-0xFFFF]"
;    For Write: do spmi_bus.cmm Write OwnerChannel[0-5] SlaveId[0-0xF] RegisterAddress[0-0xFFFF] DataByte[0-0xFF]
;
;
;  Copyright (c) 2012 Qualcomm Technologies Incorporated. 
;  All Rights Reserved.
;  Qualcomm Confidential and Proprietary
;----------------------------------------------------------------------------
;============================================================================
;
;                        EDIT HISTORY FOR MODULE
;
; $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/scripts/9x25/spmi_bus.cmm#1 $
; $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 11/09/12   PS     Returns read data now
; 07/23/12   PS     Initial revision
;============================================================================;

AREA.Create SPMIBUS 120. 1000.
AREA.Select SPMIBUS
AREA.View SPMIBUS
winresize 120. 40.

ENTRY &Operation &OwnerChannel &SlaveId &RegisterAddress &DataByte
GLOBAL &CHNLn_CMD_ADDR_BASE  &CHNLn_STATUS_ADDR_BASE &CHNLn_WDATA0_ADDR_BASE &CHNLn_WDATA1_ADDR_BASE &CHNLn_RDATA0_ADDR_BASE &CHNLn_RDATA1_ADDR_BASE &CHNLn_OFFSET 
GLOBAL &NUM_MAX_OWNER_CHANNEL &NUM_MAX_SLAVE_ID &NUM_MAX_REGISTER_ADDRESS &param_check_status &read_status &write_status &NumBytes &ReadValue &main_status
LOCAL  &oper_id
LOCAL &DATA

&CHNLn_CMD_ADDR_BASE=0xFC4CF800
&CHNLn_STATUS_ADDR_BASE=0xFC4CF808
&CHNLn_WDATA0_ADDR_BASE=0xFC4CF810
&CHNLn_WDATA1_ADDR_BASE=0xFC4CF814
&CHNLn_RDATA0_ADDR_BASE=0xFC4CF818
&CHNLn_RDATA1_ADDR_BASE=0xFC4CF81C
&CHNLn_OFFSET=0x80
&NUM_MAX_OWNER_CHANNEL=0x6
&NUM_MAX_SLAVE_ID=16
&NUM_MAX_REGISTER_ADDRESS=0x10000
&NumBytes=1

&oper_id=0
&main_status="true"
if "&Operation"=="" 
(
      print "Error: SPMI opearation not specified!"
      &main_status="false"
      goto MAIN_RETURN
)
if ("&Operation"=="Read")||("&Operation"=="READ")||("&Operation"=="read") 
(
      &oper_id=1
      GOSUB READ &OwnerChannel &SlaveId &RegisterAddress
      ENTRY &DATA
      goto MAIN_RETURN
)
else if ("&Operation"=="Write")||("&Operation"=="WRITE")||("&Operation"=="write")
(
      &oper_id=2
      GOSUB WRITE &OwnerChannel &SlaveId &RegisterAddress &DataByte
      goto MAIN_RETURN
)
if (&oper_id==0)
(
      print "Error: SPMI opearation invalid1!"
      &main_status="false"
      goto MAIN_RETURN
)

MAIN_RETURN:
if ("&main_status"=="false")
(
   GOSUB USAGE
   ENDDO "SPMIDEAD"
)
ELSE
(
    ENDDO &DATA
)

;-----------------------------------------------
; Functions 
;-----------------------------------------------
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;USAGE - This is internal function   ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
USAGE:
print "Usage:"
Print "For Read:  do spmi_bus.cmm Read OwnerChannel[0-5] SlaveId[0-0xF] RegisterAddress[0-0xFFFF]"
Print "For Write: do spmi_bus.cmm Write OwnerChannel[0-5] SlaveId[0-0xF] RegisterAddress[0-0xFFFF] DataByte[0-0xFF]"
Print "OwnerChannel - 0 A5, 1 A5_TZ, 2 MSS, 3 LPASS, 4 RPM"

RETURN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;PARAMETER_CHECK - This is internal function ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PARAMETER_CHECK: 
ENTRY &Operation &OwnerChannel &SlaveId &RegisterAddress
&param_check_status="true"
if "&Operation"=="" 
(
      print "Error: SPMI opearation not specified!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if "&Operation"!="Read"&&"&Operation"!="READ"&&"&Operation"!="read"&&"&Operation"=="Write"&&"&Operation"=="WRITE"&&"&Operation"=="write" 
(
      print "Error: SPMI opearation invalid!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if ("&OwnerChannel"=="")
(
      print "Error: SPMI OwnerChannel not specified!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if (&OwnerChannel>=&NUM_MAX_OWNER_CHANNEL)||(&OwnerChannel<0)
(
      print "Error: SPMI OwnerChannel invalid!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if ("&SlaveId"=="")
(
      print "Error: SPMI Slave Id not specified!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if (&SlaveId>=&NUM_MAX_SLAVE_ID)||(&SlaveId<0)
(
      print "Error: SPMI Slave Id invalid!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if ("&RegisterAddress"=="") 
(
      print "Error: SPMI Register address not specified!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)
if (&RegisterAddress>=&NUM_MAX_REGISTER_ADDRESS)||(&RegisterAddress<0)
(
      print "Error: SPMI Register address invalid!"
      &param_check_status="false"
      goto PARAMETER_CHECK_RETURN
)

PARAMETER_CHECK_RETURN:
RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;READ                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
READ:
   ENTRY &OwnerChannel &SlaveId &RegisterAddress
   LOCAL &utemp &status
   LOCAL &read
   &read="READ"

   &read_status="true"

   GOSUB PARAMETER_CHECK &read &OwnerChannel &SlaveId &RegisterAddress
   if ("&param_check_status"=="false")
   (
      &read_status="false"
      goto READ_RETURN
   )
; Perform Read now
   
   &utemp=(0x1<<0x1B)|(&SlaveId<<0x14)|(&RegisterAddress<<0x4)|(&NumBytes-1)
   print "Command = &utemp"
   data.set az:&CHNLn_CMD_ADDR_BASE+&CHNLn_OFFSET*&OwnerChannel %LONG &utemp

   &status=0
   while (&status==0) ;wait for status register to change to non zero value
   (
      wait 1.ms
      &status=data.long(az:&CHNLn_STATUS_ADDR_BASE+&CHNLn_OFFSET*&OwnerChannel)
   )
   if (&status==1)
   (
      &ReadValue=data.long(az:&CHNLn_RDATA0_ADDR_BASE+&CHNLn_OFFSET*&OwnerChannel)
      print "Success: Read value: &ReadValue"
   )
   else
   (
      print "Failure: Read failed"
      &read_status="false"
      goto READ_RETURN
   )

READ_RETURN:
if ("&read_status"=="false")
(
   GOSUB USAGE
)
ELSE
(
    RETURN &ReadValue
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;WRITE                           ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
WRITE:
   ENTRY &OwnerChannel &SlaveId &RegisterAddress &WriteByte
   LOCAL &utemp &status

   &write_status="true"

   GOSUB PARAMETER_CHECK "Write" &OwnerChannel &SlaveId &RegisterAddress

   if ("&param_check_status"=="false")
   (
      &write_status="false"
      goto WRITE_RETURN
   )

   if ("&WriteByte"=="")
   (
      print "Error: SPMI DataByte not specified!"
      &write_status="false"
      goto WRITE_RETURN
   )

   if (&WriteByte>=0x100)||(&WriteByte<0)
   (
      print "Error: SPMI DataByte Invalid!"
      &write_status="false"
      goto WRITE_RETURN
   )

; Perform Write now
   
   data.set  az:0xFC4CF810+&CHNLn_OFFSET*&OwnerChannel %LONG (&WriteByte&0xFF)
   &utemp=(0x0<<0x1B)|(&SlaveId<<0x14)|(&RegisterAddress<<0x4)|(&NumBytes-1)
   print "Command = &utemp"
   data.set  az:&CHNLn_CMD_ADDR_BASE+&CHNLn_OFFSET*&OwnerChannel %LONG &utemp

   &status=0
   while (&status==0) ;wait for status register to change to non zero value
   (
      wait 1.ms
      &status=data.long(az:&CHNLn_STATUS_ADDR_BASE+&CHNLn_OFFSET*&OwnerChannel)
   )
   if (&status==1)
   (
      print "Success: Write Successful"
   )
   else
   (
      print "Failure: Write failed"
      &write_status="false"
      goto WRITE_RETURN
   )

WRITE_RETURN:
if ("&write_status"=="false")
(
   GOSUB USAGE
)
RETURN


