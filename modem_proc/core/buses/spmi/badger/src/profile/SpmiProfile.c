/*=========================================================================*/
/**
@file: SpmiProfile.c

@brief: This module provides the profiling functionality for SPMI driver. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/profile/SpmiProfile.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
03/15/13   PS      Initial revision.

===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "Spmi_ComDefs.h"
#include "SpmiLogs.h"
#include "SpmiProfile.h"
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DDITimetick.h"
/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
static DalDeviceHandle *hTimeTick = NULL;
static uint32 isTimeTickSupported = FALSE;

/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/
void SpmiProfile_Init()
{
    DALResult result;
    if(!hTimeTick)
    {
        /* If DAL Time tick driver is available, we will open a handle to it */
        result = DalTimetick_Attach("SystemTimer", &hTimeTick );
        if((DAL_SUCCESS == result) && (hTimeTick))
        {
            isTimeTickSupported = TRUE;
        }
    }

    SPMI_LOG(INFO,
        "SpmiProfile: SpmiProfile_Init: Profile Support status : %d",
        isTimeTickSupported);
}

void SpmiProfile_Deinit()
{
    if(hTimeTick)
    {
        DAL_DeviceDetach(hTimeTick);
        hTimeTick = 0;
    }
    isTimeTickSupported = 0;
}

uint64 SpmiProfile_GetTimeTick(void)
{
    uint64 u64TimeTick = 0;

    if((hTimeTick) && 
        (isTimeTickSupported) && 
        (DAL_SUCCESS == DalTimetick_GetTimetick64(hTimeTick, &u64TimeTick)))
    {
        return u64TimeTick;
    }
    else
    {
        return (uint64) 0;
    }
}
