#ifndef __SPMI_PROFILE_H_
#define __SPMI_PROFILE_H_
/*=========================================================================*/
/**
@file: SpmiProfile.h

@brief: This module provides the profiling functionality for SPMI driver. 
 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/profile/SpmiProfile.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
03/06/13   PS      Initial revision.

===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
#define SPMI_PROFILE_GET_TIMETICK() SpmiProfile_GetTimeTick()

#define SPMI_PROFILE(a)  /* Map to nothing. Only used when profiling needs to be done */
/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/
void SpmiProfile_Init(void);
void SpmiProfile_Deinit(void);
uint64 SpmiProfile_GetTimeTick(void);

#endif /* __SPMI_PROFILE_H_ */
