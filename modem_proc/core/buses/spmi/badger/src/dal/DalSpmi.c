/*=========================================================================*/
/**
@file:  DalSpmi.c

@brief: This module provides the DAL driver implementation of SPMI Bus and 
Spmi Interrupt Controller functionalities
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/dal/DalSpmi.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
06/19/13   MQ      Added PPID to APID api
11/09/12   PS      Added Logging capability
09/04/12   PS      Added ReadModifyWrite api support
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral 
08/17/12   PS      Changed DALDevice.h to DalDevice.h 
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================	
*/

#include "DALFramework.h"
#include "DalDevice.h"
#include "DDISpmi.h"
#include "string.h" /* for memset warning */
#include "SpmiBus.h"
#include "SpmiIntrCtlr.h"
#include "SpmiLogs.h"

#define MAX_INTR_SLOTS_ALLOCATED 64
#define SPMI_DAL_DEBUG_MAX_CLIENT_HANDLES 16

/*------------------------------------------------------------------------------
Declaring a "Spmi" Driver,Device and Client Context
------------------------------------------------------------------------------*/
typedef struct SpmiDrvCtxt SpmiDrvCtxt;
typedef struct SpmiDevCtxt SpmiDevCtxt;
typedef struct SpmiClientCtxt SpmiClientCtxt;

/*------------------------------------------------------------------------------
Global variable
------------------------------------------------------------------------------*/
SpmiDevCtxt *pSpmiDalDeviceCtxt = 0; /* This pointer is just used for facilitating debug */

/*------------------------------------------------------------------------------
Declaring a private "Spmi" Vtable
------------------------------------------------------------------------------*/
typedef struct
{
    SpmiPhyPeriphIdType periphID;
    uint32 uExtendedIntrMask;
    SpmiClientCtxt *pClientCtxt;
}Spmi_RegisteredIntrType;


typedef struct SpmiDALVtbl SpmiDALVtbl;
struct SpmiDALVtbl
{
    int (*Spmi_DriverInit)(SpmiDrvCtxt *);
    int (*Spmi_DriverDeInit)(SpmiDrvCtxt *);
};

struct SpmiDevCtxt
{
    //Base Members
    uint32 dwRefs;
    uint32 dwDevCtxtRefIdx;
    SpmiDrvCtxt *pSpmiDrvCtxt;
    DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
    const char *strDeviceName;
    uint32 Reserved[16];
    //Spmi Dev state can be added by developers here
    DALSYSSyncHandle hSpmiBusDeviceSynchronization;
    DALSYS_SYNC_OBJECT(SpmiBusDeviceSyncObject);
    DALSYSSyncHandle hSpmiIntrCtlrDeviceSynchronization;
    DALSYS_SYNC_OBJECT(SpmiIntrCtlrDeviceSyncObject);
    Spmi_RegisteredIntrType registeredIntrs[MAX_INTR_SLOTS_ALLOCATED];
    uint32 uDeviceInitDone;

    /* Debug registers */
    DalSpmiHandle *pSpmiHandles[SPMI_DAL_DEBUG_MAX_CLIENT_HANDLES];
    uint32 uHandlesCurrentlyActive;

};

struct SpmiDrvCtxt
{
    //Base Members
    SpmiDALVtbl SpmiDALVtbl;
    uint32 dwNumDev;
    uint32 dwSizeDevCtxt;
    uint32 bInit;
    uint32 dwRefs;
    SpmiDevCtxt SpmiDevCtxt[1];
    //Spmi Drv state can be added by developers here
};

/*------------------------------------------------------------------------------
Declaring a "Spmi" Client Context
------------------------------------------------------------------------------*/
struct SpmiClientCtxt
{
    //Base Members
    uint32 dwRefs;
    uint32 dwAccessMode;
    void *pPortCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DalSpmiHandle DalSpmiHandle;
    //Spmi Client state can be added by developers here
    uint32 uOpened;
};

/*------------------------------------------------------------------------------
Driver functions
------------------------------------------------------------------------------*/
DALResult
Spmi_DeviceAttach(const char *, DALDEVICEID,DalDeviceHandle **);



/*------------------------------------------------------------------------------
Info file
------------------------------------------------------------------------------*/

const DALREG_DriverInfo
DALSpmi_DriverInfo = { Spmi_DeviceAttach, 0, NULL };


/*------------------------------------------------------------------------------
Static functions
------------------------------------------------------------------------------*/
static DALResult Spmi_AddRegisteredInterrupts(DalDeviceHandle * h,
                                              SpmiPhyPeriphIdType periphID,
                                              uint32 uExtendedIntrMask)
{
    SpmiClientCtxt *pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    SpmiDevCtxt *pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;
    uint32 bUpdated = FALSE;
    int32 nFirstUnOccupiedSlot = -1;
    uint32 uNewSlot;
    uint32 i;

    for(i=0; i<MAX_INTR_SLOTS_ALLOCATED; i++)
    {
        if((pSpmiDevCtxt->registeredIntrs[i].pClientCtxt == pClientCtxt) &&
            (pSpmiDevCtxt->registeredIntrs[i].periphID == periphID))
        {
            pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask |= uExtendedIntrMask;
            bUpdated = TRUE;
            break;
        }
        else if((-1 == nFirstUnOccupiedSlot) && (0 == pSpmiDevCtxt->registeredIntrs[i].pClientCtxt))
        {
            nFirstUnOccupiedSlot = i;
        }
    }

    if(!bUpdated)
    {
        if(-1 == nFirstUnOccupiedSlot)
        {
            SPMI_LOG(ERROR,
                "DalSpmi: Spmi_AddRegisteredInterrupts: ***SPMI_ERROR*** No slot space left");
            return DAL_ERROR;
        }
        else
        {
            uNewSlot = nFirstUnOccupiedSlot;
        }
        pSpmiDevCtxt->registeredIntrs[uNewSlot].pClientCtxt = pClientCtxt;
        pSpmiDevCtxt->registeredIntrs[uNewSlot].periphID = periphID;
        pSpmiDevCtxt->registeredIntrs[uNewSlot].uExtendedIntrMask = uExtendedIntrMask;
    }

    return DAL_SUCCESS;
}


static DALResult Spmi_RemoveRegisteredInterrupts(DalDeviceHandle * h,
                                                 SpmiPhyPeriphIdType periphID,
                                                 uint32 uExtendedIntrMask)
{
    SpmiClientCtxt *pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    SpmiDevCtxt *pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;
    uint32 bUpdated = FALSE;
    uint32 i;

    for(i=0; i<MAX_INTR_SLOTS_ALLOCATED; i++)
    {
        if((pSpmiDevCtxt->registeredIntrs[i].pClientCtxt == pClientCtxt) &&
            (pSpmiDevCtxt->registeredIntrs[i].periphID == periphID))
        {
            pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask &= ~uExtendedIntrMask;
            if(!pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask)
            {
                pSpmiDevCtxt->registeredIntrs[i].pClientCtxt = 0;
                pSpmiDevCtxt->registeredIntrs[i].periphID = 0;
            }
            bUpdated = TRUE;
            break;
        }
    }

    if(!bUpdated)
    {
        return DAL_ERROR;
    }

    return DAL_SUCCESS;
}

static DALResult Spmi_UnregisterRemainingRegisteredInterrupts(DalDeviceHandle * h)
{
    SpmiClientCtxt *pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    SpmiDevCtxt *pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;
    uint32 i;
    DALResult result;

    for(i=0; i<MAX_INTR_SLOTS_ALLOCATED; i++)
    {
        if(pSpmiDevCtxt->registeredIntrs[i].pClientCtxt == pClientCtxt)
        {
            result = SpmiIntrCtlr_UnregisterIsr(pSpmiDevCtxt->registeredIntrs[i].periphID,
                pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask);
            if(DAL_SUCCESS != result)
            {
                return result;
            }

            pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask = 0;
            pSpmiDevCtxt->registeredIntrs[i].pClientCtxt = 0;
            pSpmiDevCtxt->registeredIntrs[i].periphID = 0;
        }
    }
    return DAL_SUCCESS;
}

static uint32 Spmi_IsRegisteredInterrupts(DalDeviceHandle * h,
                                          SpmiPhyPeriphIdType periphID,
                                          uint32 uExtendedIntrMask)
{
    SpmiClientCtxt *pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    SpmiDevCtxt *pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;
    uint32 bFound = FALSE;
    uint32 i;

    for(i=0; i<MAX_INTR_SLOTS_ALLOCATED; i++)
    {
        if((pSpmiDevCtxt->registeredIntrs[i].pClientCtxt == pClientCtxt) &&
            (pSpmiDevCtxt->registeredIntrs[i].periphID == periphID))
        {
            if((pSpmiDevCtxt->registeredIntrs[i].uExtendedIntrMask & uExtendedIntrMask) ==
                uExtendedIntrMask)
            {
                bFound = TRUE;
            }
            break;
        }
    }

    return bFound;
}

/*------------------------------------------------------------------------------
Driver functions
------------------------------------------------------------------------------*/


static uint32 Spmi_AddRef(DalSpmiHandle* h)
{
    return DALFW_AddRef((DALClientCtxt *)(h->pClientCtxt));
}

/*------------------------------------------------------------------------------
Following functions are for DALDriver specific functionality
------------------------------------------------------------------------------*/
DALResult Spmi_DriverInit(SpmiDrvCtxt *pdrvCtxt)
{
    return DAL_SUCCESS;
}

DALResult Spmi_DriverDeInit(SpmiDrvCtxt *pdrvCtxt)
{
    return DAL_SUCCESS;
}


/*------------------------------------------------------------------------------
Following functions are defined in DalDevice DAL Interface.
------------------------------------------------------------------------------*/
static uint32 Spmi_DeviceDetach(DalDeviceHandle* h)
{

    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    uint32 dwref;
    uint32 i;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDetach func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_DeviceDetach: ***SPMI_ERROR*** DAL handle passed is NULL");
        return (uint32)DAL_ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(pSpmiDevCtxt->uDeviceInitDone) /* Do this only if the device is not de-inited already */
    {
        SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDetach func Entry, handle: 0x%x", h);
        (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
        if(DAL_SUCCESS != Spmi_UnregisterRemainingRegisteredInterrupts(h))
        {
            (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
            SPMI_LOG(ERROR,
                "DalSpmi: Spmi_DeviceDetach: ***SPMI_ERROR*** Spmi_UnregisterRemainingRegisteredInterrupts returned error");
            return (uint32)DAL_ERROR;
        }
        (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    }

    dwref = DALFW_Release((DALClientCtxt *)pClientCtxt);
    if(0 == dwref)
    {
        for(i=0; i<SPMI_DAL_DEBUG_MAX_CLIENT_HANDLES; i++)
        {
            if(pSpmiDevCtxt->pSpmiHandles[i] == (DalSpmiHandle *) h)
            {
                pSpmiDevCtxt->pSpmiHandles[i] = 0;
                break;
            }
        }
        pSpmiDevCtxt->uHandlesCurrentlyActive--;
        /* Release the client ctxt */
        DALSYS_Free(pClientCtxt);
        SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDetach: Freeing the client ctxt with handle 0x%x", h);
    }

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDetach func Exit, dwref: %d", dwref);

    return dwref;
}

static DALResult Spmi_DeviceInit(DalDeviceHandle *h)
{

    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result = DAL_SUCCESS;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceInit func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_DeviceInit: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;


    /* Initialize Resource synchronization object. This sync object is used
    to synchronize between client calls on same device */
    if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
        &(pSpmiDevCtxt->hSpmiBusDeviceSynchronization),
        &(pSpmiDevCtxt->SpmiBusDeviceSyncObject) ))
    {
        pSpmiDevCtxt->hSpmiBusDeviceSynchronization = 0;
        result = DAL_ERROR;
        SPMI_LOG(FATAL_ERROR,
            "DalSpmi: Spmi_DeviceInit: ***SPMI_FATAL_ERROR*** Unable to create resource synchronization object");
        goto ERROR;
    }

    /* Initialize Resource synchronization object. This sync object is used
    to synchronize between client calls on same device */
    if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
        &(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization),
        &(pSpmiDevCtxt->SpmiIntrCtlrDeviceSyncObject) ))
    {
        pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization = 0;
        DALSYS_DestroyObject( pSpmiDevCtxt->hSpmiBusDeviceSynchronization );
        pSpmiDevCtxt->hSpmiBusDeviceSynchronization = NULL;
        result = DAL_ERROR;
        SPMI_LOG(FATAL_ERROR,
            "DalSpmi: Spmi_DeviceInit: ***SPMI_FATAL_ERROR*** Unable to create resource synchronization object");
        goto ERROR;
    }


    /* Initialise Spmi Bus driver */
    if(DAL_SUCCESS != (result = (DALResult)SpmiBus_Init()))
    {
        SPMI_LOG(FATAL_ERROR,
            "DalSpmi: Spmi_DeviceInit: ***SPMI_FATAL_ERROR*** SpmiBus_Init failed with status %d", result);
        goto ERROR;
    }

    /* Initialise Spmi Interrupt Controller driver */
    if(DAL_SUCCESS != (result = (DALResult)SpmiIntrCtlr_Init()))
    {
        SPMI_LOG(FATAL_ERROR,
            "DalSpmi: Spmi_DeviceInit: ***SPMI_FATAL_ERROR*** SpmiBus_Init failed with status %d", result);
        goto ERROR;
    }

    /* Memset the registeredIntrs table */
    DALSYS_memset(pSpmiDevCtxt->registeredIntrs,
        0,
        sizeof(Spmi_RegisteredIntrType) * MAX_INTR_SLOTS_ALLOCATED);

    pSpmiDevCtxt->uDeviceInitDone = TRUE;

    SPMI_LOG(INFO, "DalSpmi: Spmi_DeviceInit: Device initialized successfully");

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceInit func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_DeviceDeInit(DalDeviceHandle *h)
{

    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result = DAL_SUCCESS;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDeInit func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_DeviceDeInit: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;


    /* Destroy the Synchronization objects for this device */
    DALSYS_DestroyObject( pSpmiDevCtxt->hSpmiBusDeviceSynchronization );
    pSpmiDevCtxt->hSpmiBusDeviceSynchronization = NULL;

    DALSYS_DestroyObject( pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization );
    pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization = NULL;

    /* De-Initialise Spmi Bus driver */
    SpmiBus_DeInit();

    /* De-Initialise Spmi Interrupt Controller driver */
    SpmiIntrCtlr_DeInit();

    pSpmiDevCtxt->uDeviceInitDone = FALSE;


ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceDeInit func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_DevicePowerEvent(
                                       DalDeviceHandle *h,
                                       DalPowerCmd PowerCmd,
                                       DalPowerDomain PowerDomain )
{
    return DAL_SUCCESS;

}

static DALResult Spmi_DeviceOpen(DalDeviceHandle* h, uint32 mode)
{

    SpmiClientCtxt *pClientCtxt;
    DALResult result = DAL_SUCCESS;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceOpen func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_DeviceOpen: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);

    pClientCtxt->uOpened = TRUE;

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceOpen func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_DeviceClose(DalDeviceHandle* h)
{
    SpmiClientCtxt *pClientCtxt;
    DALResult result = DAL_SUCCESS;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceClose func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_DeviceClose: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }
    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);

    pClientCtxt->uOpened = FALSE;

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceClose func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_DeviceInfo(
                                 DalDeviceHandle* h,
                                 DalDeviceInfo* info,
                                 uint32 infoSize)
{
    info->Version = DALSPMI_INTERFACE_VERSION;
    return DAL_SUCCESS;
}

static DALResult Spmi_DeviceSysRequest(
                                       DalDeviceHandle* h,
                                       DalSysReq ReqIdx,
                                       const void* SrcBuf,
                                       uint32 SrcBufLen,
                                       void* DestBuf,
                                       uint32 DestBufLen,
                                       uint32* DestBufLenReq)
{
    return DAL_SUCCESS;
}


/*------------------------------------------------------------------------------
Following functions are extended in DalSpmi Interface.
------------------------------------------------------------------------------*/
static DALResult Spmi_ReadLong(
                               DalDeviceHandle *h,
                               uint32 uSlaveId,
                               SpmiBus_AccessPriorityType eAccessPriority,
                               uint32 uRegisterAddress,
                               unsigned char* pucData,
                               uint32 uDataLen,
                               uint32* puTotalBytesRead)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ReadLong func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ReadLong: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ReadLong: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ReadLong: Entered critical section for client with device handle 0x%x",
        h);

    /* Call underlying SPMI driver api */
    result = (DALResult) SpmiBus_ReadLong(uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen,
        puTotalBytesRead);

    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ReadLong: Exited critical section for client with device handle 0x%x",
        h);

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ReadLong func Exit, Status: %d", result);
    return result;

}


static DALResult Spmi_WriteLong(
                                DalDeviceHandle *h,
                                uint32 uSlaveId,
                                SpmiBus_AccessPriorityType eAccessPriority,
                                uint32 uRegisterAddress,
                                unsigned char* pucData,
                                uint32 uDataLen)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_WriteLong func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_WriteLong: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_WriteLong: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_WriteLong: Entered critical section for client with device handle 0x%x",
        h);

    /* Call underlying SPMI driver api */
    result = (DALResult) SpmiBus_WriteLong(uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen);

    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_WriteLong: Exited critical section for client with device handle 0x%x",
        h);

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_WriteLong func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_ReadModifyWriteLongByte(
    DalDeviceHandle *h,
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    uint32 uData,
    uint32 uMask,
    uint8 *pucDataWritten)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ReadModifyWriteLongByte func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ReadModifyWriteLongByte: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ReadModifyWriteLongByte: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ReadModifyWriteLongByte: Entered critical section for client with device handle 0x%x",
        h);

    /* Call underlying SPMI driver api */
    result = (DALResult) SpmiBus_ReadModifyWriteLongByte(
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        uData,
        uMask,
        pucDataWritten);

    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ReadModifyWriteLongByte: Exited critical section for client with device handle 0x%x",
        h);

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ReadModifyWriteLongByte func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_Command(
                              DalDeviceHandle *h,
                              uint32 uSlaveId,
                              SpmiBus_AccessPriorityType eAccessPriority,
                              SpmiBus_CommandType eSpmiCommand)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_Command func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_Command: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_Command: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_Command: Entered critical section for client with device handle 0x%x",
        h);

    /* Call underlying SPMI driver api */
    result = (DALResult) SpmiBus_Command(uSlaveId,
        eAccessPriority, eSpmiCommand);

    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiBusDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_Command: Exited critical section for client with device handle 0x%x",
        h);

ERROR:
    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_Command func Exit, Status: %d", result);
    return result;

}

static DALResult Spmi_RegisterIsr(
                                  DalDeviceHandle *h,
                                  SpmiPhyPeriphIdType periphID,
                                  uint32 uExtendedIntrMask,
                                  const SpmiIsr isr,
                                  const SpmiIsrCtxt ctxt)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_RegisterIsr func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_RegisterIsr: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_RegisterIsr: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_RegisterIsr: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_RegisterIsr(periphID, uExtendedIntrMask, isr, ctxt);

    if(DAL_SUCCESS == result)
    {
        Spmi_AddRegisteredInterrupts(h, periphID, uExtendedIntrMask);
    }

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_RegisterIsr: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_RegisterIsr func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_UnregisterIsr(
                                    DalDeviceHandle *h,
                                    SpmiPhyPeriphIdType periphID,
                                    uint32 uExtendedIntrMask)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_UnregisterIsr func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_UnregisterIsr: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_UnregisterIsr: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_UnregisterIsr: Entered critical section for client with device handle 0x%x",
        h);
    do
    {
        if(!Spmi_IsRegisteredInterrupts(h, periphID, uExtendedIntrMask))
        {
            result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_REGISTERED;
            SPMI_LOG(ERROR,
                "DalSpmi: Spmi_UnregisterIsr: ***SPMI_ERROR*** One of the interrupt bit in mask: 0x%x for periphID: 0x%x is not registered by the client with handle 0x%x",
                uExtendedIntrMask,
                periphID,
                h);
            break;
        }

        if(DAL_SUCCESS != (result = SpmiIntrCtlr_UnregisterIsr(periphID,
            uExtendedIntrMask)))
        {
            break;
        }

        if(DAL_SUCCESS != (result = Spmi_RemoveRegisteredInterrupts(h,
            periphID,
            uExtendedIntrMask)))
        {
            SPMI_LOG(ERROR,
                "DalSpmi: Spmi_UnregisterIsr: ***SPMI_ERROR*** Spmi_RemoveRegisteredInterrupts failed",
                uExtendedIntrMask,
                periphID,
                h);
            break;
        }
    }while(0);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_UnregisterIsr: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_UnregisterIsr func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_InterruptEnable(
                                      DalDeviceHandle * h,
                                      SpmiPhyPeriphIdType periphID)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptEnable func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptEnable: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptEnable: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptEnable: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_InterruptEnable(periphID);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptEnable: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptEnable func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_InterruptDisable(
                                       DalDeviceHandle * h,
                                       SpmiPhyPeriphIdType periphID)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptDisable func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptDisable: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptDisable: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptDisable: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_InterruptDisable(periphID);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptDisable: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptDisable func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_IsInterruptEnabled(
    DalDeviceHandle * h,
    SpmiPhyPeriphIdType periphID,
    uint32 * bState)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_IsInterruptEnabled func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_IsInterruptEnabled: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_IsInterruptEnabled: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_IsInterruptEnabled: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_IsInterruptEnabled(periphID, bState);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_IsInterruptEnabled: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_IsInterruptEnabled func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_InterruptDone(
                                    DalDeviceHandle * h,
                                    SpmiPhyPeriphIdType periphID,
                                    uint32 uExtendedIntrMask)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptDone func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptDone: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_InterruptDone: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptDone: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_InterruptDone(periphID, uExtendedIntrMask);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_InterruptDone: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_InterruptDone func Exit, Status: %d", result);
    return result;
}


static DALResult Spmi_ExtendedInterruptStatus(
    DalDeviceHandle * h,
    SpmiPhyPeriphIdType periphID,
    uint32 * puIntrStatusMask)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ExtendedInterruptStatus func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ExtendedInterruptStatus: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ExtendedInterruptStatus: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ExtendedInterruptStatus: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_ExtendedInterruptStatus(periphID,puIntrStatusMask);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ExtendedInterruptStatus: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ExtendedInterruptStatus func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_ExtendedInterruptClear(
    DalDeviceHandle * h,
    SpmiPhyPeriphIdType periphID,
    uint32 uIntrClearMask)
{
    SpmiClientCtxt *pClientCtxt;
    SpmiDevCtxt *pSpmiDevCtxt;
    DALResult result;

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ExtendedInterruptClear func Entry, handle: 0x%x", h);

    if(NULL == h)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ExtendedInterruptClear: ***SPMI_ERROR*** DAL handle passed is NULL");
        goto ERROR;
    }

    pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
    pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

    if(!pClientCtxt->uOpened)
    {
        result = DAL_ERROR;
        SPMI_LOG(ERROR,
            "DalSpmi: Spmi_ExtendedInterruptClear: ***SPMI_ERROR*** Client with device handle 0x%x has not called device open call",
            h);
        goto ERROR;
    }

    /* Enter the Critical Section now */
    (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ExtendedInterruptClear: Entered critical section for client with device handle 0x%x",
        h);

    result = (DALResult) SpmiIntrCtlr_ExtendedInterruptClear(periphID,uIntrClearMask);

    /* Leave the critical section now */
    (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);
    SPMI_LOG(VERBOSE,
        "DalSpmi: Spmi_ExtendedInterruptClear: Exited critical section for client with device handle 0x%x",
        h);

ERROR:

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_ExtendedInterruptClear func Exit, Status: %d", result);
    return result;
}

static DALResult Spmi_GetLogicalIdFromPpid(DalDeviceHandle* h,
                                           SpmiPhyPeriphIdType periphId, 
                                           uint32* logicalId)
{
   if(NULL == h)
   {
      return DAL_ERROR;
   }

   SpmiIntrCtlr_ResultType rslt;
   SpmiClientCtxt* pClientCtxt = (SpmiClientCtxt *)(((DalSpmiHandle *)h)->pClientCtxt);
   SpmiDevCtxt* pSpmiDevCtxt = pClientCtxt->pSpmiDevCtxt;

   if(!pClientCtxt->uOpened)
   {
      return DAL_ERROR;
   }

   /* Enter the Critical Section now */
   (void)DALSYS_SyncEnter(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

   rslt = SpmiIntrCtlr_GetLogicalIdFromPpid( periphId, logicalId );

   /* Leave the critical section now */
   (void)DALSYS_SyncLeave(pSpmiDevCtxt->hSpmiIntrCtlrDeviceSynchronization);

   return (DALResult)rslt;
}

static void
Spmi_InitInterface(SpmiClientCtxt* pclientCtxt)
{
    static const DalSpmi vtbl =
    {
        {
            Spmi_DeviceAttach,
                Spmi_DeviceDetach,
                Spmi_DeviceInit,
                Spmi_DeviceDeInit,
                Spmi_DeviceOpen,
                Spmi_DeviceClose,
                Spmi_DeviceInfo,
                Spmi_DevicePowerEvent,
                Spmi_DeviceSysRequest
        },

        Spmi_ReadLong,
        Spmi_WriteLong,
        Spmi_ReadModifyWriteLongByte,
        Spmi_Command,
        Spmi_RegisterIsr,
        Spmi_UnregisterIsr,
        Spmi_InterruptEnable,
        Spmi_InterruptDisable,
        Spmi_IsInterruptEnabled,
        Spmi_InterruptDone,
        Spmi_ExtendedInterruptStatus,
        Spmi_ExtendedInterruptClear,
        Spmi_GetLogicalIdFromPpid
    };
    /*--------------------------------------------------------------------------
    Depending upon client type setup the vtables (entry points)
    --------------------------------------------------------------------------*/
    pclientCtxt->DalSpmiHandle.dwDalHandleId = DALDEVICE_INTERFACE_HANDLE_ID;
    pclientCtxt->DalSpmiHandle.pVtbl = &vtbl;
    pclientCtxt->DalSpmiHandle.pClientCtxt = pclientCtxt;
}

DALResult Spmi_DeviceAttach(
                            const char *pszArg,
                            DALDEVICEID DeviceId,
                            DalDeviceHandle **phDalDevice)
{
    DALResult nErr;
    uint32 i;
    static SpmiDrvCtxt drvCtxt = {{Spmi_DriverInit,
        Spmi_DriverDeInit
    },1,
    sizeof(SpmiDevCtxt)};

    SpmiClientCtxt *pclientCtxt = NULL;

    pSpmiDalDeviceCtxt = &(drvCtxt.SpmiDevCtxt[0]); /* This pointer is just used for facilitating debug */

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceAttach func Entry");

    nErr = DALSYS_Malloc(sizeof(SpmiClientCtxt),(void **)&pclientCtxt);
    DALSYS_memset(pclientCtxt,0,sizeof(SpmiClientCtxt));

    *phDalDevice = NULL;

    if (DAL_SUCCESS == nErr) {
        nErr = DALFW_AttachToStringDevice(pszArg,(DALDrvCtxt *)&drvCtxt,
            (DALClientCtxt *)pclientCtxt);
        if (DAL_SUCCESS == nErr)
        {
            Spmi_InitInterface(pclientCtxt);
            Spmi_AddRef(&(pclientCtxt->DalSpmiHandle));
            *phDalDevice = (DalDeviceHandle *)&(pclientCtxt->DalSpmiHandle);
            for(i=0; i<SPMI_DAL_DEBUG_MAX_CLIENT_HANDLES; i++)
            {
                if(!pclientCtxt->pSpmiDevCtxt->pSpmiHandles[i])
                {
                    pclientCtxt->pSpmiDevCtxt->pSpmiHandles[i] = (DalSpmiHandle *) &(pclientCtxt->DalSpmiHandle);
                    break;
                }
            }
            pclientCtxt->pSpmiDevCtxt->uHandlesCurrentlyActive++;
            SPMI_LOG(INFO, "DalSpmi: Spmi_DeviceAttach: New client ctxt created at 0x%x with handle 0x%x", pclientCtxt, *phDalDevice);
        }
    }

    SPMI_LOG(VERBOSE, "DalSpmi: Spmi_DeviceAttach func Exit, Status: %d", nErr);
    return nErr;
}



