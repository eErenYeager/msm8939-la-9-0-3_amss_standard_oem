/**
 * @file: SpmiLogsImpl.h
 * 
 * @brief: This module provides logging functionality for the SPMI driver
 *         using ULog
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/01/27 06:04:57 $
 * $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/logs/SpmiLogsImpl.h#1 $
 * $Change: 7351156 $
 */

#ifndef __SPMI_LOGS_IMPL_H_
#define __SPMI_LOGS_IMPL_H_

#include "ULogFront.h"

#define SPMI_LOG_INIT SpmiLogs_Init

#define SPMI_LOG_FATAL_ERROR_IMPL(format_str, args...) \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 0, "SPMI_LOG_LEVEL_FATAL_ERROR"); \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 10, format_str, ##args)
    
#define SPMI_LOG_ERROR_IMPL(format_str, args...) \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 0, "SPMI_LOG_LEVEL_ERROR"); \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 10, format_str, ##args)
    
#define SPMI_LOG_WARNING_IMPL(format_str, args...) \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 0, "SPMI_LOG_LEVEL_WARNING"); \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 10, format_str, ##args)

#define SPMI_LOG_INFO_IMPL(format_str, args...) \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 0, "SPMI_LOG_LEVEL_INFO"); \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 10, format_str, ##args)

#define SPMI_LOG_VERBOSE_IMPL(format_str, args...) \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 1, "SPMI_LOG_LEVEL_VERBOSE"); \
    ULogFront_RealTimePrintf(SpmiLogs_GetUlogHandle(), 10, format_str, ##args)

void SpmiLogs_Init(void);
ULogHandle SpmiLogs_GetUlogHandle(void);

#endif
