/**
 * @file: SpmiLogs.c
 * 
 * @brief: This module provides common logging functionality for SPMI driver
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/01/27 06:04:57 $
 * $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/logs/SpmiLogs.c#1 $
 * $Change: 7351156 $
 */

#include "Spmi_ComDefs.h"
#include "SpmiLogs.h"

uint32 guSpmiLogLevel = SPMI_LOG_LEVEL_INFO; /* By default set to SPMI_LOG_LEVEL_INFO */

uint32 SpmiLogs_GetLogLevel(void)
{
    return guSpmiLogLevel;
}

void SpmiLogs_SetLogLevel(uint32 uSpmiLogLevel)
{
    guSpmiLogLevel = uSpmiLogLevel > SPMI_LOG_LEVEL_MAX ? SPMI_LOG_LEVEL_MAX : uSpmiLogLevel;
}

