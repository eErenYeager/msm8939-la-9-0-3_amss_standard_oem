/**
 * @file: SpmiLogs.h
 * 
 * @brief: This module provides common logging functionality for SPMI driver
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/01/27 06:04:57 $
 * $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/logs/SpmiLogs.h#1 $
 * $Change: 7351156 $
 */

#ifndef __SPMI_LOGS_H_
#define __SPMI_LOGS_H_

#include "Spmi_ComDefs.h"
#include "SpmiLogsImpl.h"

#define SPMI_LOG_LEVEL_MAX SPMI_LOG_LEVEL_VERBOSE
#define SPMI_LOG_LEVEL_VERBOSE 5
#define SPMI_LOG_LEVEL_INFO 4
#define SPMI_LOG_LEVEL_WARNING 3
#define SPMI_LOG_LEVEL_ERROR 2
#define SPMI_LOG_LEVEL_FATAL_ERROR 1
#define SPMI_LOG_LEVEL_NONE 0

// This comes from Sconscript or Builds file
#ifndef SPMI_LOG_COMPILE_LEVEL
    #error "Spmi compile Log Level is not defined"
#endif

// Not all platforms need initialization
#ifndef SPMI_LOG_INIT
    #define SPMI_LOG_INIT()
#endif

#define SPMI_LOG(severity, format_str, args...) SPMI_LOG_##severity(format_str, ##args)

/* SPMI_LOG_LEVEL_FATAL_ERROR */
#if (SPMI_LOG_COMPILE_LEVEL >= SPMI_LOG_LEVEL_FATAL_ERROR)
    #define SPMI_LOG_FATAL_ERROR(format_str, args...) \
        if(SpmiLogs_GetLogLevel() >= SPMI_LOG_LEVEL_FATAL_ERROR) \
        { \
            SPMI_LOG_FATAL_ERROR_IMPL(format_str, ##args); \
        }
#else
    #define SPMI_LOG_FATAL_ERROR(format_str, args...)
#endif

/* SPMI_LOG_LEVEL_ERROR */
#if (SPMI_LOG_COMPILE_LEVEL >= SPMI_LOG_LEVEL_ERROR)
    #define SPMI_LOG_ERROR(format_str, args...) \
        if(SpmiLogs_GetLogLevel() >= SPMI_LOG_LEVEL_ERROR) \
        { \
            SPMI_LOG_ERROR_IMPL(format_str, ##args); \
        }
#else
    #define SPMI_LOG_ERROR(format_str, args...)
#endif


/* SPMI_LOG_LEVEL_WARNING */
#if (SPMI_LOG_COMPILE_LEVEL >= SPMI_LOG_LEVEL_WARNING)
    #define SPMI_LOG_WARNING(format_str, args...) \
        if(SpmiLogs_GetLogLevel() >= SPMI_LOG_LEVEL_WARNING) \
        { \
            SPMI_LOG_WARNING_IMPL(format_str, ##args); \
        }
#else
    #define SPMI_LOG_WARNING(format_str, args...)
#endif


/* SPMI_LOG_LEVEL_INFO */
#if (SPMI_LOG_COMPILE_LEVEL >= SPMI_LOG_LEVEL_INFO)
    #define SPMI_LOG_INFO(format_str, args...) \
        if(SpmiLogs_GetLogLevel() >= SPMI_LOG_LEVEL_INFO) \
        { \
            SPMI_LOG_INFO_IMPL(format_str, ##args); \
        }
#else
    #define SPMI_LOG_INFO(format_str, args...)
#endif


/* SPMI_LOG_LEVEL_VERBOSE */
#if (SPMI_LOG_COMPILE_LEVEL >= SPMI_LOG_LEVEL_VERBOSE)
    #define SPMI_LOG_VERBOSE(format_str, args...) \
    if(SpmiLogs_GetLogLevel() >= SPMI_LOG_LEVEL_VERBOSE) \
    { \
        SPMI_LOG_VERBOSE_IMPL(format_str, ##args); \
    }
#else
    #define SPMI_LOG_VERBOSE(format_str, args...)
#endif

uint32 SpmiLogs_GetLogLevel(void);
void SpmiLogs_SetLogLevel(uint32 uSpmiLogLevel);

#endif /* __SPMI_LOGS_H_ */
