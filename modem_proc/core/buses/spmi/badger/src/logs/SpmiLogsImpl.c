/**
 * @file: SpmiLogs.c
 * 
 * @brief: This module provides logging functionality for SPMI driver
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/01/27 06:04:57 $
 * $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/logs/SpmiLogsImpl.c#1 $
 * $Change: 7351156 $
 */

#include "Spmi_ComDefs.h"
#include "SpmiLogsImpl.h"

#define ULOG_SPMI_LOG_NAME "SPMI_LOG"
#define ULOG_SPMI_LOG_SIZE 2048

static ULogHandle gSpmiDynLogHandle = 0;

void SpmiLogs_Init(void)
{
    ULogFront_RealTimeInit(&gSpmiDynLogHandle, /* Log Handle*/
                           ULOG_SPMI_LOG_NAME, /* Name of the log */
                           ULOG_SPMI_LOG_SIZE, /* Initial Buffer. Normally set to 0 for a disabled log. */
                           ULOG_MEMORY_LOCAL,  /* Local memory (normal case) */
                           ULOG_LOCK_OS);      /* Will use an OS lock when writing */
}

ULogHandle SpmiLogs_GetUlogHandle(void)
{
    return gSpmiDynLogHandle;
}