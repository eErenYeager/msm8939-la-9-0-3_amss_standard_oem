/*=========================================================================*/
/**
@file SpmiIntrCtlr_Os.c

This module provides the interface to the Pmic Arbiter Configuration
driver software. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiIntrCtlr/SpmiIntrCtlr_Os.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
03/19/12   PS      Added Profiling capability
10/24/12   PS      Added Logging capability
08/20/12   PS      Removed clk calls. Clk should be accessed from RPM only
08/13/12   PS      ZI malloced memory.
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "SpmiIntrCtlr_Internal.h"
#include "DALDeviceId.h"
#include "DDIInterruptController.h"
#include "DDIHWIO.h"
#include "DALSys.h"
#include "SpmiLogs.h"
#include "SpmiProfile.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/* This structure will hold all the data needed by this file */
typedef struct
{
    DalDeviceHandle *hInterruptControllerHandle;
    uint32 uInterruptNumber;
    SpmiIntrCtlr_IsrType isr;
}SpmiIntrCtlr_OsCfgType;
/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
static SpmiIntrCtlr_OsCfgType osCfgHandle;
static DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
static DalDeviceHandle *hHWIO = NULL;
/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/
static SpmiIntrCtlr_ResultType SpmiIntrCtlr_ChipInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{
    DALSYSPropertyVar PropVar;
    SpmiIntrCtlr_ResultType spmiResult = SPMI_INTR_CTLR_SUCCESS;

    do
    {
        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "this_owner_number", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            pDeviceInfo->chipConfig.uThisOwnerNumber = (uint32)PropVar.Val.dwVal;
            SPMI_LOG(INFO,
                "SpmiIntrCtlr: SpmiIntrCtlr_ChipInit : Read this_owner_number: %d",
                pDeviceInfo->chipConfig.uThisOwnerNumber);
        }
        else
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_ChipInit : ***SPMI_ERROR*** Unable to read this_owner_number from properties");
            break;
        }
    }while(0);

    return spmiResult;
}

static void SpmiIntrCtlr_ChipDeInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{
    pDeviceInfo->chipConfig.uThisOwnerNumber = 0;
}

static SpmiIntrCtlr_ResultType SpmiIntrCtlr_SwInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{
    DALResult result;
    DALSYSPropertyVar PropVar;
    char *pszCoreBaseName;
    SpmiIntrCtlr_ResultType spmiResult = SPMI_INTR_CTLR_SUCCESS;
    uint32 uLoggingLevel;

    do
    {
        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "logging_level", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            uLoggingLevel = (uint32)PropVar.Val.dwVal;
            SpmiLogs_SetLogLevel(uLoggingLevel);

            SPMI_LOG(INFO,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : Read logging_level: %d",
                uLoggingLevel);
        }

        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "core_base_name", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            pszCoreBaseName = PropVar.Val.pszVal;
        }
        else
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_FATAL_ERROR*** Unable to read core_base_name from properties");
            break;
        }

        /* Determine the HWIO base for the Spmi registers */
        result = DAL_HWIODeviceAttach(DALDEVICEID_HWIO, &hHWIO);
        if((DAL_SUCCESS != result) || (!hHWIO))
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_FATAL_ERROR*** DAL_HWIODeviceAttach failed with status %d",
                result);
            break;
        }

        result = DalDevice_Open(hHWIO, DAL_OPEN_SHARED);
        if(DAL_SUCCESS != result)
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_FATAL_ERROR*** DalDevice_Open on HWIO failed with status %d",
                result);
            break;
        }

        result = DalHWIO_MapRegion(hHWIO, pszCoreBaseName,
            (uint8**)&(pDeviceInfo->swConfig.puSpmiIntrCtlrVirtualAddress));
        if(DAL_SUCCESS != result)
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_FATAL_ERROR*** DalHWIO_MapRegion on HWIO failed with status %d",
                result);
            break;
        }
        result = DalDevice_Close(hHWIO);
        if(DAL_SUCCESS != result)
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(WARNING,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_WARNING*** DalDevice_Close on HWIO failed with status %d",
                result);
            break;
        }
        if(0 == pDeviceInfo->swConfig.puSpmiIntrCtlrVirtualAddress)
        {
            spmiResult = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_SwInit : ***SPMI_FATAL_ERROR*** Mapped Virtual Address is NULL");
            break;
        }

        /* Now init the Spmi Profile driver */
        SpmiProfile_Init();

    }while(0);

    return spmiResult;
}

static void SpmiIntrCtlr_SwDeInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{

    if(hHWIO)
    {
        DalDevice_Open(hHWIO, DAL_OPEN_SHARED);
        DalHWIO_UnMapRegion(hHWIO,
            (uint8*) pDeviceInfo->swConfig.puSpmiIntrCtlrVirtualAddress);
        DalDevice_Close(hHWIO);
        DAL_DeviceDetach(hHWIO);
        hHWIO = 0;
    }

    /* Now Deinit the Spmi Profile driver */
    SpmiProfile_Deinit();

    return;
}
/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsInit(
    SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{
    /*
    This function shall fill in following fields of the DeviceInfo strcuture
    1. chipConfig -> uThisOwnerNumber
    2. swConfig -> puSpmiIntrCtlrVirtualAddress
    3. Set the logging level using SpmiLogs_SetLogLevel
    */

    SpmiIntrCtlr_ResultType spmiResult;

    if( DAL_SUCCESS != DALSYS_GetDALPropertyHandleStr("DALDEVICEID_SPMI_DEVICE",
        hProp))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_OsInit : ***SPMI_FATAL_ERROR*** Unable to get property handle");
        return SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
    }

    /* Call SW init function */
    if (SPMI_INTR_CTLR_SUCCESS != (spmiResult = SpmiIntrCtlr_SwInit(pDeviceInfo)))
    {
        return spmiResult;
    }

    /* Call Chip Init function */
    if (SPMI_INTR_CTLR_SUCCESS != (spmiResult = SpmiIntrCtlr_ChipInit(pDeviceInfo)))
    {
        return spmiResult;
    }

    return SPMI_INTR_CTLR_SUCCESS;

}

void *SpmiIntrCtlr_OsIsr(void *pArg)
{
    if(osCfgHandle.isr)
    {
        return (*osCfgHandle.isr)(pArg);
    }
    else
    {
        SPMI_LOG(WARNING, "SpmiIntrCtlr: SpmiIntrCtlr_OsIsr: ***SPMI_WARNING*** Entered OS ISR routine without driver isr initialized");
        return (void *)0;
    }
}


SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsRegisterISR(
    SpmiIntrCtlr_IsrType isrRoutine,
    void *pargs)
{

    DALSYSPropertyVar PropVar;
    DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    osCfgHandle.isr = isrRoutine;

    do
    {

        if( DAL_SUCCESS == DALSYS_GetDALPropertyHandleStr("DALDEVICEID_SPMI_DEVICE", hProp))
        {
            if(DAL_SUCCESS == DALSYS_GetPropertyValue
                (hProp, //Prop handle
                "interrupt_number", //Prop Name String
                0, //Not interested in Prop Id
                &PropVar)) //Output
            {
                osCfgHandle.uInterruptNumber = (uint32)PropVar.Val.dwVal;
                SPMI_LOG(INFO,
                    "SpmiIntrCtlr: SpmiIntrCtlr_OsRegisterISR : Read interrupt_number: %d",
                    osCfgHandle.uInterruptNumber);
            }
            else
            {
                result = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
                SPMI_LOG(FATAL_ERROR,
                    "SpmiIntrCtlr: SpmiIntrCtlr_OsRegisterISR : ***SPMI_FATAL_ERROR*** Unable to get interrupt_number");
                break;
            }
        }
        else
        {
            result = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_OsRegisterISR : ***SPMI_FATAL_ERROR*** Unable to get property handle");
            break;
        }



        if( DAL_SUCCESS != DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER,
            &(osCfgHandle.hInterruptControllerHandle)))
        {
            result = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_OsRegisterISR : ***SPMI_FATAL_ERROR*** DAL_DeviceAttach to interrupt controller failed with status %d",
                result);
            break;
        }

        if(DAL_SUCCESS != DalInterruptController_RegisterISR(
            osCfgHandle.hInterruptControllerHandle,
            osCfgHandle.uInterruptNumber,
            SpmiIntrCtlr_OsIsr,
            pargs,
            DALINTRCTRL_ENABLE_LEVEL_HIGH_TRIGGER))
        {
            result = SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiIntrCtlr: SpmiIntrCtlr_OsRegisterISR : ***SPMI_FATAL_ERROR*** DalInterruptController_RegisterISR for interrupt number %d failed with status %d",
                osCfgHandle.uInterruptNumber,
                result);
            break;
        }

    }while(0);

    return result;
}

void SpmiIntrCtlr_OsUnregisterIsr()
{
    DalInterruptController_Unregister(osCfgHandle.hInterruptControllerHandle,
        osCfgHandle.uInterruptNumber);
    DAL_DeviceDetach(osCfgHandle.hInterruptControllerHandle);
    osCfgHandle.isr = NULL;
    osCfgHandle.hInterruptControllerHandle = NULL;
}

void SpmiIntrCtlr_OsDeInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo)
{
    /* Call Chip De-init function */
    SpmiIntrCtlr_ChipDeInit(pDeviceInfo);

    /* Call SW De-init function */
    SpmiIntrCtlr_SwDeInit(pDeviceInfo);

    return;
}


SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsMalloc(uint32 size, void **pMemPtr)
{
    DALResult result;
    if(DAL_SUCCESS != (result = DALSYS_Malloc(size, pMemPtr)))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_OsMalloc : ***SPMI_FATAL_ERROR*** DALSYS_Malloc failed with status %d",
            result);
        return SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
    }
    /* Zero init the Buffer */
    DALSYS_memset(*pMemPtr, 0, size);

    return SPMI_INTR_CTLR_SUCCESS;
}


void SpmiIntrCtlr_OsFree(void *pMemPtr)
{
    DALSYS_Free(pMemPtr);
    return;
}
