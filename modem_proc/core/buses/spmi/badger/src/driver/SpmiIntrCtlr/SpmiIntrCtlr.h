#ifndef __SPMI_INTR_CTLR_H__
#define __SPMI_INTR_CTLR_H__
/*=========================================================================*/
/**
@file:  SpmiIntrCtlr.h

@brief: This module provides the interface to the SPMI Interrupt Controller
software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiIntrCtlr/SpmiIntrCtlr.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================	
*/
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "SpmiIntrCtlr_Defs.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/
/**
@brief Initialize the driver

This function initializes the Spmi Interrupt controller driver. It is mandatory
to call this function before using any other APIs of this driver

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see SpmiIntrCtlr_DeInit
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_Init(void);


/**
@brief De-initialize the driver

This function de-initializes the Spmi Interrupt controller driver.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see SpmiIntrCtlr_Init
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_DeInit(void);


/**
@brief Regsiters an ISR

This function regsiters an ISR with SPMI Interrupt Controller
driver.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means register for Intr bit 0 of this peripheral
0x3 means register for Intr bit 0 and bit 1 of this peripheral
0x5 means register for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are registering for all the interrupts
within this peripheral

@param[in] isr ISR routine associated with the Interrupt id.

@param[in] ctxt ISR context which will be copied as a
parameter on isr invocation

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_Unregister()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_RegisterIsr(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask,
    const SpmiIsr isr,
    const SpmiIsrCtxt ctxt);

/**
@brief Unregsiters an ISR

This function unregsiters an previoulsy registered ISR with
SPMI Interrupt Controller driver.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means unregister for Intr bit 0 of this peripheral
0x3 means unregister for Intr bit 0 and bit 1 of this peripheral
0x5 means unregister for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are unregistering for all the interrupts
within this peripheral

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_Register()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_UnregisterIsr(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask);


/**
@brief Enables an Interrupt

This function enables an Interrupt on SPMI Interrupt
Controller

****Note**** : This function shouldn't be used by the clients
who are using SpmiIntrCtlr_Register() api for ISR management

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptDisable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptEnable(
    SpmiPhyPeriphIdType periphID);


/**
@brief Disables an Interrupt

This function disables an Interrupt on SPMI Interrupt
Controller

****Note**** : This function shouldn't be used by the clients
who are using SpmiIntrCtlr_Register() api for ISR management

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptEnable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptDisable(
    SpmiPhyPeriphIdType periphID);


/**
@brief Is Interrupt Enabled

This function returns whether or not an Interrupt is enabled
on SPMI Interrupt Controller

****Note**** : This function shouldn't be used by the clients
who are using SpmiIntrCtlr_Register() api for ISR management

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] bState Pointer to a variable which will hold the
return value of TRUE or FALSE

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptEnable(),
SpmiIntrCtlr_InterruptDisable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_IsInterruptEnabled(
    SpmiPhyPeriphIdType periphID,
    uint32 *bState);

/**
@brief Interrupt Done

This function needs to be called by the client when Interrupt
processing is done for this interrupt id.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptDone(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask);

/**
Get all the Peripheral accumulated Interrupt status

@param[out] puAccIntrStatusArr - Pointer to an array of 8 words which will
hold the the accumulated IntrStatus. Each bit set in this
parameter will indicates that the corresponding peripheral number
interrupt is set. Interrupt bit will be set if the interrupt is
asserted and if the peripheral is owned by this processor.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None

@attention None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_GetAccumulatedIntrStatus(
    uint32 *puAccIntrStatusArr);


/* Extension APIs to read peripheral extended interrupts */


/**
@brief Get the Extended Interrupt Status

This function returns the extended Interrupt Status associated with
the intrId

****Note**** : This function shouldn't be used by the clients
who are using SpmiIntrCtlr_Register() api for ISR management

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] puIntrStatusMask Pointer to the uint32 which will be
populated by this function reflecting the status of the extended
interrupts. Each set bit in this parameter, represents that
extended Interrupts is set.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_ExtendedInterruptStatus(
    SpmiPhyPeriphIdType periphID,
    uint32 *puIntrStatusMask);


/**
@brief Clear the Extended Interrupt Status

This function clears the extended Interrupt Status associated
with the intrId

****Note**** : This function shouldn't be used by the clients
who are using SpmiIntrCtlr_Register() api for ISR management

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uIntrClearMask This is a mask of bits which
reprsents Interrupts to be cleared.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_ExtendedInterruptClear(
    SpmiPhyPeriphIdType periphID,
    uint32 uIntrClearMask);


/* Logical Physical Conversion APIs */

/**
@brief Get the Logical(APID) from a physical peripheral Id (PPID)

This function returns the logical ID of a peripheral from the given
PPID

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] puLogicalPeriphId Pointer to the uint32 which will be
populated by this function. The value will be the corresponding APID

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_GetLogicalIdFromPpid(
    SpmiPhyPeriphIdType periphID,
    uint32 *puLogicalPeriphId);


#endif /* __SPMI_INTR_CTLR_H__ */

