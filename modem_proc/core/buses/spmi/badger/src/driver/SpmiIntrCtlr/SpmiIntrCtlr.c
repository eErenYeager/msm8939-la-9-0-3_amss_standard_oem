/*=========================================================================*/
/**
@file SpmiIntrCtlr.c

This module provides the driver to the SPMI Interrupt Controller HW
*/
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiIntrCtlr/SpmiIntrCtlr.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
04/12/13   PS      Fixed a bug in unregistered interrupt handling 
03/11/13   PS      Added Profiling hooks
02/28/13   PS      Added support for Intr Ownership check
10/24/12   PS      Added Logging capability
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "SpmiIntrCtlr.h"
#include "HalSpmi.h"
#include "SpmiIntrCtlr_Internal.h"
#include "SpmiLogs.h"
#include "SpmiProfile.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
#define SPMI_INTERRUPTS_PER_PERIPHERAL 8
#define SPMI_MAX_PHY_PERIPH_ID 0xFFF

/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
SpmiIntrCtlr_DeviceInfoType gSpmiIntrCtlrDeviceInfo; /* Kept global for easy debugging */

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/
__inline static bool32 SpmiIntrCtlr_IsAccumulatedIntrSet(uint32 *puAccumulatedIntr,
                                                         uint32 uPeriph)
{
    if((puAccumulatedIntr[uPeriph/32] >> (uPeriph%32)) & 0x1)
    {
        return TRUE;
    }
    return FALSE;
}

__inline static bool32 SpmiIntrCtlr_IsIntrRegistered(uint32 uPeriph)
{
    uint32 i;
    for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
    {
        if(gSpmiIntrCtlrDeviceInfo.pIsrTable[(uPeriph*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered)
        {
            return TRUE;
        }
    }
    return FALSE;
}

__inline static void SpmiIntrCtlr_DebugIntrStart(uint32 uPeriph, uint32 uIntrStatus)
{
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucFinish = 0;
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].u64EndTimeTick = 0;

    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucStart = 1;
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].u64StartTimeTick = SPMI_PROFILE_GET_TIMETICK();
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucApid = (uint8)uPeriph;
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucIntrStatus = (uint8)uIntrStatus;
}

__inline static void SpmiIntrCtlr_DebugIntrFinish(uint32 uPeriph)
{
    if(gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucApid != uPeriph)
    {
        SPMI_LOG(WARNING, "SpmiIntrCtlr: SpmiIntrCtlr_DebugIntrFinish: ***SPMI_WARNING*** Debug start and finish periph id doesn't match. Start: 0x%x, Finish 0x%x", 
            gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucApid, 
            uPeriph);
        gSpmiIntrCtlrDeviceInfo.ucUnreliable = 1; /* Debug data is not reliable */
    }

    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].ucFinish = 1;
    gSpmiIntrCtlrDeviceInfo.debugIntr[gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex].u64EndTimeTick = SPMI_PROFILE_GET_TIMETICK();
    gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex = (gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex+1)%SPMI_INTR_DEBUG_MAX_INTRS;
}

static uint32 SpmiIntrCtlr_IsIntrOwnedByEE(uint32 uApid)
{
    /* Check if this interrupt is owned by this EE */
    if(HAL_Spmi_CfgGetPeripheralOwner(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle, uApid) !=
        gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber)
    {
        return FALSE;
    }
    return TRUE;
}


static void *SpmiIntrCtlr_Isr(void *pArg)
{
    uint32 uAccumulatedIntr[8];
    uint32 uPeriph;
    uint32 uIntrStatus;
    uint32 i;

    SPMI_PROFILE("SpmiIntrCtlr: SpmiIntrCtlr_Isr func Entry");

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_Isr func Entry");

    pArg = pArg; /* Unreferenced Parameter */

    /* Check if the driver is initialized in the ISR mode */
    if((!gSpmiIntrCtlrDeviceInfo.bInitialized) || (!gSpmiIntrCtlrDeviceInfo.bIsrInitialized))
    {
        SPMI_LOG(WARNING, "SpmiIntrCtlr: SpmiIntrCtlr_Isr: ***SPMI_WARNING*** Entered ISR routine without driver initialized");
        goto ERROR;
    }

    HAL_Spmi_PicGetPeriphAccumulatedIntrStatus(
        gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber,
        &uAccumulatedIntr[0]);

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_Isr: Accumulated Interrupt Status 0x%x-0x%x-0x%x-0x%x-0x%x-0x%x-0x%x-0x%x",
        uAccumulatedIntr[7],
        uAccumulatedIntr[6],
        uAccumulatedIntr[5],
        uAccumulatedIntr[4],
        uAccumulatedIntr[3],
        uAccumulatedIntr[2],
        uAccumulatedIntr[1],
        uAccumulatedIntr[0]);


    for(uPeriph = 0; uPeriph < gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported;
        uPeriph++)
    {
        if(SpmiIntrCtlr_IsAccumulatedIntrSet(&uAccumulatedIntr[0], uPeriph))
        {
            if(!SpmiIntrCtlr_IsIntrRegistered(uPeriph))
            {
                SPMI_LOG(WARNING, 
                    "SpmiIntrCtlr: SpmiIntrCtlr_Isr: ***SPMI_WARNING*** Accumulated interrupt is set for uApid %d, which is not registered. Attention needed.",
                    uPeriph);
                continue; /* Continue to the next interrupt */
            }

            uIntrStatus = HAL_Spmi_PicGetPeriphIntrStatus(
                gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
                uPeriph);

            SpmiIntrCtlr_DebugIntrStart(uPeriph, uIntrStatus);

            SPMI_LOG(VERBOSE, 
                "SpmiIntrCtlr: SpmiIntrCtlr_Isr: Interrupt Status: 0x%x for uApid: %d",
                uIntrStatus, 
                uPeriph);

            HAL_Spmi_PicPeriphIntrClear(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
                uPeriph,
                uIntrStatus);
            while((HAL_Spmi_PicGetPeriphIntrStatus(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
                uPeriph) &
                uIntrStatus) != 0); /* Wait until the interrupt is actually cleared */

            /* Call the ISR routine if registered */
            for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
            {
                if(!uIntrStatus)
                {
                    /* No Intr bits remained to be processed */
                    SPMI_LOG(VERBOSE, 
                        "SpmiIntrCtlr: SpmiIntrCtlr_Isr: Done processing all the extended intr bits for for uApid: %d",
                        uPeriph);
                    break;
                }
                if((uIntrStatus>>i) & 0x1) /* Check if Intr bit is set ? */
                {
                    /* Check if the INTR is registered ? */
                    if(gSpmiIntrCtlrDeviceInfo.pIsrTable[(uPeriph*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered)
                    {                        
                        uint32 uIsrTableIndex = (uPeriph*SPMI_INTERRUPTS_PER_PERIPHERAL)+i;
                        uint32 uIsrCallMask = uIntrStatus & gSpmiIntrCtlrDeviceInfo.pIsrTable[uIsrTableIndex].ucMask;
                        SpmiIsrCtxt ctxt = gSpmiIntrCtlrDeviceInfo.pIsrTable[uIsrTableIndex].ctxt;
                        SpmiIsr isr = gSpmiIntrCtlrDeviceInfo.pIsrTable[uIsrTableIndex].isr;
                        SPMI_LOG(VERBOSE, 
                            "SpmiIntrCtlr: SpmiIntrCtlr_Isr: Calling client isr routine for uApid: %d with mask: 0x%x",
                            uPeriph,
                            uIsrCallMask);

                        SPMI_PROFILE("SpmiIntrCtlr: SpmiIntrCtlr_Isr: Calling ISR routine");

                        /* Call the ISR routine registered with this Intr bit(and Mask) */
                        isr(ctxt, uIsrCallMask);

                        SPMI_PROFILE("SpmiIntrCtlr: SpmiIntrCtlr_Isr: Done calling ISR routine");

                        SPMI_LOG(INFO, 
                            "SpmiIntrCtlr: SpmiIntrCtlr_Isr: Done calling client isr routine for uApid: %d with mask: 0x%x",
                            uPeriph,
                            uIsrCallMask);
                        uIntrStatus = uIntrStatus & ~uIsrCallMask;
                    }
                }
            }
            SpmiIntrCtlr_DebugIntrFinish(uPeriph);

        }
    }

ERROR:
    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_Isr func Exit, Status: %d", 0);

    SPMI_PROFILE("SpmiIntrCtlr: SpmiIntrCtlr_Isr func Exit");

    return (void*)1;
}


static SpmiIntrCtlr_ResultType SpmiIntrCtlr_Initialize(void)
{
    char *pcVersion, ppszPmicArbCfgVersion[20];
    uint32 uNumMapTableEntries;
    uint32 i;

    pcVersion = &ppszPmicArbCfgVersion[0];

    /* Initialise OS specific information */
    if(SpmiIntrCtlr_OsInit(&gSpmiIntrCtlrDeviceInfo)!= SPMI_INTR_CTLR_SUCCESS)
    {
        SPMI_LOG(ERROR, "SpmiIntrCtlr: SpmiIntrCtlr_Initialize: ***SPMI_ERROR*** SpmiIntrCtlr_OsInit failed");   
        return SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
    }

    gSpmiIntrCtlrDeviceInfo.swConfig.halHandle =
        (HAL_Spmi_HandleType)gSpmiIntrCtlrDeviceInfo.swConfig.puSpmiIntrCtlrVirtualAddress;

    /* Initialise PMIC Arbiter HAL */

    HAL_Spmi_Init(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle, &pcVersion);

    gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported =
        HAL_Spmi_CfgGetNumPeriphs(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle);

    uNumMapTableEntries = HAL_Spmi_CfgGetNumMappingTableEntries(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle);

    for(i=0; i<uNumMapTableEntries;i++)
    {
        gSpmiIntrCtlrDeviceInfo.uMapTable[i] = HAL_Spmi_CfgGetPpidToApidMappingTableEntry(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle, i);
    }

    /* Initialize debug variable */
    gSpmiIntrCtlrDeviceInfo.uDebugIntrCurrentIndex = 0;
    gSpmiIntrCtlrDeviceInfo. ucUnreliable = 0;

    gSpmiIntrCtlrDeviceInfo.bInitialized = TRUE;

    return SPMI_INTR_CTLR_SUCCESS;
}

static SpmiIntrCtlr_ResultType SpmiIntrCtlr_IsrInit(void)
{

    if(SPMI_INTR_CTLR_FAILURE_INIT_FAILED ==
        SpmiIntrCtlr_OsMalloc(
        (gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported *
        SPMI_INTERRUPTS_PER_PERIPHERAL *
        sizeof(SpmiIntrCtlr_IsrConfigType)),
        (void *)&(gSpmiIntrCtlrDeviceInfo.pIsrTable)))
    {
        SPMI_LOG(FATAL_ERROR, "SpmiIntrCtlr: SpmiIntrCtlr_IsrInit: ***SPMI_ERROR*** SpmiIntrCtlr_OsMalloc failed");  
        return SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
    }

    /* Register for an Interrupt */
    if(SPMI_INTR_CTLR_FAILURE_INIT_FAILED ==
        SpmiIntrCtlr_OsRegisterISR(&SpmiIntrCtlr_Isr, (void *) &gSpmiIntrCtlrDeviceInfo))
    {
        SPMI_LOG(FATAL_ERROR, "SpmiIntrCtlr: SpmiIntrCtlr_IsrInit: ***SPMI_ERROR*** SpmiIntrCtlr_OsRegisterISR failed");  
        return SPMI_INTR_CTLR_FAILURE_INIT_FAILED;
    }

    gSpmiIntrCtlrDeviceInfo.bIsrInitialized = TRUE;

    return SPMI_INTR_CTLR_SUCCESS;
}


static SpmiIntrCtlr_ResultType SpmiIntrCtlr_InitializeIfNeeded(
    bool32 bIsrInitNeeded)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;

    /* Initialize the driver if this is the first call */
    if(!gSpmiIntrCtlrDeviceInfo.bInitialized)
    {
        result = SpmiIntrCtlr_Initialize();
        SPMI_LOG(INFO, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InitializeIfNeeded : SpmiIntrCtlr_Initialize finished with status: %d", 
            result);
    }
    else
    {
        result = SPMI_INTR_CTLR_SUCCESS;
        SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InitializeIfNeeded:  Driver already initialized");
    }

    if(result == SPMI_INTR_CTLR_SUCCESS)
    {
        if(bIsrInitNeeded)
        {
            if(!gSpmiIntrCtlrDeviceInfo.bIsrInitialized)           
            {
                result = SpmiIntrCtlr_IsrInit();
                SPMI_LOG(INFO, 
                    "SpmiIntrCtlr: SpmiIntrCtlr_InitializeIfNeeded : SpmiIntrCtlr_IsrInit finished with status: %d", 
                    result);
            }
            else
            { 
                result = SPMI_INTR_CTLR_SUCCESS;
                SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InitializeIfNeeded: ISR already initialized");
            }
        }
    }
    return result;
}

/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/

/**
@brief Initialize the driver

This function initializes the Spmi Interrupt controller driver. It is mandatory
to call this function before using any other APIs of this driver

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see SpmiIntrCtlr_DeInit
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_Init(void)
{
    SpmiIntrCtlr_ResultType result;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_Init func Entry");

    /* Initialize the driver if this is the first call */
    result = SpmiIntrCtlr_InitializeIfNeeded(FALSE);

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_Init func Exit, Status: %d", result);

    return result;
}


/**
@brief De-initialize the driver

This function de-initializes the Spmi Interrupt controller driver.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see SpmiIntrCtlr_Init
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_DeInit(void)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_DeInit func Entry");

    /* DeInitialize the driver if it is initialized */
    if(gSpmiIntrCtlrDeviceInfo.bInitialized)
    {
        if(gSpmiIntrCtlrDeviceInfo.bIsrInitialized)
        {
            SpmiIntrCtlr_OsUnregisterIsr();
            SpmiIntrCtlr_OsFree(gSpmiIntrCtlrDeviceInfo.pIsrTable);
            gSpmiIntrCtlrDeviceInfo.bIsrInitialized = FALSE;
            SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_DeInit: ISR deinited");
        }

        SpmiIntrCtlr_OsDeInit(&gSpmiIntrCtlrDeviceInfo);
        gSpmiIntrCtlrDeviceInfo.bInitialized = FALSE;
    }
    else
    {
        result = SPMI_INTR_CTLR_SUCCESS;
        SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_DeInit: Driver in not initialized");
    }

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_DeInit func Exit, Status: %d", result);

    return result;
}


/**
@brief Regsiters an ISR

This function regsiters an ISR with SPMI Interrupt Controller
driver.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means register for Intr bit 0 of this peripheral
0x3 means register for Intr bit 0 and bit 1 of this peripheral
0x5 means register for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are registering for all the interrupts
within this peripheral

@param[in] isr ISR routine associated with the Interrupt id.

@param[in] ctxt ISR context which will be copied as a
parameter on isr invocation

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_Unregister()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_RegisterIsr(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask,
    const SpmiIsr isr,
    const SpmiIsrCtxt ctxt)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;
    uint32 i;
    uint32 uAnyOtherIntrRegistered = 0;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr: Parameters: periphID: 0x%x, uExtendedIntrMask: 0x%x, isr: 0x%x, ctxt: 0x%x", 
        periphID, 
        uExtendedIntrMask, 
        isr, 
        ctxt);

    /* validate parameters */
    if((SPMI_MAX_PHY_PERIPH_ID < periphID) ||
        (NULL == isr) ||
        (0 == uExtendedIntrMask) ||
        (0xFF < uExtendedIntrMask))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, isr: 0x%x, uExtendedIntrMask: 0x%x", 
            periphID, 
            isr, 
            uExtendedIntrMask);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(TRUE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d", 
            periphID, 
            result);
        goto ERROR;
    }


    /* Verify if the APID is in correct range */
    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    /* Check if this interrupt is owned by this EE */
    if(!SpmiIntrCtlr_IsIntrOwnedByEE(uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_OWNED_BY_THIS_EE;
        SPMI_LOG(ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Interrupt for periphId: 0x%x, uApid: %d, is not owned by this EE: %d",
            periphID,
            uApid,
            gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber);
        goto ERROR;
    }

    /* Check if the interrupt is already registered */
    for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
    {
        if((uExtendedIntrMask>>i) & 1)
        {
            if(gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered)
            {
                result = SPMI_INTR_CTLR_FAILURE_INTR_ALREADY_REGISTERED;
                SPMI_LOG(ERROR, 
                    "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : ***SPMI_ERROR*** Interrupt Mask 0x%x already registered for APID %d, periphID: 0x%x, result: %d", 
                    0x1<<i,
                    uApid,
                    periphID,
                    result);
                goto ERROR;
            }
        }
        else
        {
            uAnyOtherIntrRegistered = uAnyOtherIntrRegistered +
                gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered;
        }
    }

    /* Register the interrupts */
    for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
    {
        if((uExtendedIntrMask>>i) & 1)
        {
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered = TRUE;
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].isr = isr;
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].ctxt = ctxt;
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].ucMask = (uint8)uExtendedIntrMask;
        }
    }

    /* Enable this interrupt in the SPMI Interrupt controller, if it was not enabled before */
    if(!uAnyOtherIntrRegistered)
    {
        HAL_Spmi_PicPeriphAccumulatedIntrEnable(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
            uApid);
        SPMI_LOG(VERBOSE, 
            "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr : Enabled the accumulated interrupt for uApid: %d", 
            uApid);

    }

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_RegisterIsr func Exit, Status: %d", result);

    return result;
}

/**
@brief Unregsiters an ISR

This function unregsiters an previoulsy registered ISR with
SPMI Interrupt Controller driver.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means unregister for Intr bit 0 of this peripheral
0x3 means unregister for Intr bit 0 and bit 1 of this peripheral
0x5 means unregister for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are unregistering for all the interrupts
within this peripheral

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_Register()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_UnregisterIsr(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask)
{

    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;
    uint32 i,j;
    uint32 uAnyOtherIntrRegistered = 0;
    uint8 ucExtendedIntrMaskRemaining;
    uint8 ucOtherAffectedInterrupts, ucOtherAffectedInterruptsRemaining;
    uint8 ucMaskFixed = 0;
    uint8 ucOldMask;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr: Parameters: periphID: 0x%x, uExtendedIntrMask: 0x%x", 
        periphID, 
        uExtendedIntrMask);

    /* Validate the parameters */
    if((SPMI_MAX_PHY_PERIPH_ID < periphID) ||
        (uExtendedIntrMask == 0) ||
        (0xFF < uExtendedIntrMask))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, uExtendedIntrMask: 0x%x",
            periphID, 
            uExtendedIntrMask);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(TRUE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d", 
            periphID, 
            result);
        goto ERROR;
    }

    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d" ,
            periphID,
            uApid,
            result);
        goto ERROR;

    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    /* Check if the interrupt is registered */
    for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
    {
        if((uExtendedIntrMask>>i) & 1)
        {
            if(!(gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered))
            {
                result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_REGISTERED;
                SPMI_LOG(ERROR, 
                    "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : ***SPMI_ERROR*** Interrupt Mask 0x%x is not registered for APID %d, periphID: 0x%x, result: %d", 
                    0x1<<i,
                    uApid,
                    periphID,
                    result);
            }
        }
        else
        {
            uAnyOtherIntrRegistered = uAnyOtherIntrRegistered +
                gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered;
        }
    }

    /* Disable the peripheral interrupt in SPMI interrupt controller
    if none of the interrupts are registered */
    if(!uAnyOtherIntrRegistered)
    {
        HAL_Spmi_PicPeriphAccumulatedIntrDisable(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
            uApid);
        SPMI_LOG(VERBOSE, 
            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : Disabled the accumulated interrupt for uApid: %d", 
            uApid);
    }

    ucExtendedIntrMaskRemaining = (uint8)uExtendedIntrMask;
    for(i=0; i<SPMI_INTERRUPTS_PER_PERIPHERAL; i++)
    {
        if(!ucExtendedIntrMaskRemaining)
        {
            /* No other extended Intr Bits left to be unregister */
            break;
        }

        if((uExtendedIntrMask>>i) & 1) /* Check if this interrupt bit needs to be unregistered */
        {
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].bIsRegistered = FALSE;
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].isr = 0;
            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].ctxt = 0;

            /* Get the mask information associated with this Intr bit */
            ucOldMask = gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].ucMask;

            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+i].ucMask = 0;

            SPMI_LOG(VERBOSE, 
                "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : Cleared the combined mask for bit: %d, uApid: %d from old mask: 0x%x",
                i,
                uApid,
                ucOldMask);

            /* If the old mask is not same as the new mask provided, we will have to update
            all the other interrupts mask field. For example, if the old mask was b101
            and the new clear mask is b001, we will have to update the mask of
            interrupt bit 2 to reflect the new mask */

            if(ucOldMask != (uExtendedIntrMask & ucOldMask))
            {
                /* Finds which other interrupts mask needs to be updated */
                ucOtherAffectedInterrupts = (ucOldMask ^ uExtendedIntrMask) & ucOldMask;

                /* Do not update the mask which is already updated */
                ucOtherAffectedInterrupts = ucOtherAffectedInterrupts & ~ucMaskFixed;

                ucOtherAffectedInterruptsRemaining = ucOtherAffectedInterrupts;

                for(j=0; j<SPMI_INTERRUPTS_PER_PERIPHERAL; j++)
                {
                    if(!ucOtherAffectedInterruptsRemaining)
                    {
                        /* No other affected Intr Bits left to be updated */
                        break;
                    }

                    if((ucOtherAffectedInterrupts >> j) & 1) /* For each affected interrupts, update the Mask */
                    {
                        uint8 ucOldMaskTemp = gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+j].ucMask;
                        ucOldMaskTemp = ucOldMaskTemp; /* To avoid compiler warning when logging is compiled out */
                        gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+j].ucMask = ucOtherAffectedInterrupts;
                        ucOtherAffectedInterruptsRemaining = ucOtherAffectedInterruptsRemaining & (~(1 << j));
                        ucMaskFixed = ucMaskFixed | (1 << j);
                        SPMI_LOG(VERBOSE, 
                            "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr : Updated the combined mask for bit : %d of uApid: %d from old mask: 0x%x to new mask: 0x%x",
                            j,
                            uApid,
                            ucOldMaskTemp,
                            gSpmiIntrCtlrDeviceInfo.pIsrTable[(uApid*SPMI_INTERRUPTS_PER_PERIPHERAL)+j].ucMask);
                    }
                }
            }
        }
        ucExtendedIntrMaskRemaining = ucExtendedIntrMaskRemaining & (~(1 << i));
    }


ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_UnregisterIsr func Exit, Status: %d", result);

    return result;
}

/**
@brief Enables an Interrupt

This function enables an Interrupt on SPMI Interrupt
Controller

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptDisable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptEnable(
    SpmiPhyPeriphIdType periphID)
{

    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable: Parameters: periphID: 0x%x", 
        periphID);


    if(SPMI_MAX_PHY_PERIPH_ID < periphID)
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x",
            periphID);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d",
            periphID, 
            result);
        goto ERROR;
    }

    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    /* Check if this interrupt is owned by this EE */
    if(!SpmiIntrCtlr_IsIntrOwnedByEE(uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_OWNED_BY_THIS_EE;
        SPMI_LOG(ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable : ***SPMI_ERROR*** Interrupt for periphId: 0x%x, uApid: %d, is not owned by this EE: %d",
            periphID,
            uApid,
            gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber);
        goto ERROR;
    }

    /* Enable this interrupt in the SPMI Interrupt controller */
    HAL_Spmi_PicPeriphAccumulatedIntrEnable(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle, uApid);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InterruptEnable func Exit, Status: %d", result);

    return result;
}

/**
@brief Disables an Interrupt

This function disables an Interrupt on SPMI Interrupt
Controller

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptEnable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptDisable(
    SpmiPhyPeriphIdType periphID)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable: Parameters: periphID: 0x%x", 
        periphID);

    if(SPMI_MAX_PHY_PERIPH_ID < periphID)
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x",
            periphID);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d", 
            periphID, 
            result);
        goto ERROR;
    }

    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO,
        "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : periphID: 0x%x, uApid: %d",
        periphID,
        uApid);

    /* Check if this interrupt is owned by this EE */
    if(!SpmiIntrCtlr_IsIntrOwnedByEE(uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_OWNED_BY_THIS_EE;
        SPMI_LOG(ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable : ***SPMI_ERROR*** Interrupt for periphId: 0x%x, uApid: %d, is not owned by this EE: %d",
            periphID,
            uApid,
            gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber);
        goto ERROR;
    }

    /* Disable this interrupt in the SPMI Interrupt controller */
    HAL_Spmi_PicPeriphAccumulatedIntrDisable(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        uApid);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDisable func Exit, Status: %d", result);

    return result;
}

/**
@brief Is Interrupt Enabled

This function returns whether or not an Interrupt is enabled
on SPMI Interrupt Controller

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] bState Pointer to a variable which will hold the
return value of TRUE or FALSE

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see SpmiIntrCtlr_InterruptEnable(),
SpmiIntrCtlr_InterruptDisable()
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_IsInterruptEnabled(
    SpmiPhyPeriphIdType periphID,
    uint32 *bState)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled: Parameters: periphID: 0x%x, bState: %d",
        periphID, 
        bState);

    if((SPMI_MAX_PHY_PERIPH_ID < periphID)|| (NULL == bState))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, bState: %d", 
            periphID,
            bState);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d", 
            periphID, 
            result);
        goto ERROR;
    }


    if(gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid)
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO,
        "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : periphID: 0x%x, uApid: %d",
        periphID,
        uApid);

    /* Check if this interrupt is owned by this EE */
    if(!SpmiIntrCtlr_IsIntrOwnedByEE(uApid))
    {
        result = SPMI_INTR_CTLR_FAILURE_INTR_NOT_OWNED_BY_THIS_EE;
        SPMI_LOG(ERROR,
            "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled : ***SPMI_ERROR*** Interrupt for periphId: 0x%x, uApid: %d, is not owned by this EE: %d",
            periphID,
            uApid,
            gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber);
        goto ERROR;
    }

    *bState = HAL_Spmi_PicIsPeriphAccumulatedIntrEnabled(
        gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        uApid);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_IsInterruptEnabled func Exit, Status: %d", result);

    return result;
}

/**
@brief Interrupt Done

This function needs to be called by the client when Interrupt
processing is done for this interrupt id.

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_InterruptDone(
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask)
{
    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDone func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_InterruptDone: Parameters: periphID: 0x%x, uExtendedIntrMask: 0x%x",
        periphID, 
        uExtendedIntrMask);

    periphID = periphID; /* Unreferenced Parameter */
    uExtendedIntrMask = uExtendedIntrMask; /* Unreferenced Parameter */
    return SPMI_INTR_CTLR_SUCCESS;
}

/**
* Get all the Peripheral accumulated Interrupt status
*
* @param[out] puAccIntrStatusArr - Pointer to an array of 8 words which will
* hold the the accumulated IntrStatus. Each bit set in this
* parameter will indicates that the corresponding peripheral number
* interrupt is set. Interrupt bit will be set if the interrupt is
* asserted and if the peripheral is owned by this processor.
*
* @return SPMI_INTR_CTLR_SUCCESS on success, error code on error
*
* @see None
*
* @attention None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_GetAccumulatedIntrStatus(
    uint32 *puAccIntrStatusArr)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_GetAccumulatedIntrStatus func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_GetAccumulatedIntrStatus: Parameters: puAccIntrStatusArr: 0x%d",
        puAccIntrStatusArr);

    if(NULL == puAccIntrStatusArr)
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetAccumulatedIntrStatus : ***SPMI_ERROR*** Invalid Parameters: puAccIntrStatusArr: 0x%x",
            puAccIntrStatusArr);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetAccumulatedIntrStatus : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    HAL_Spmi_PicGetPeriphAccumulatedIntrStatus(
        gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        gSpmiIntrCtlrDeviceInfo.chipConfig.uThisOwnerNumber,
        puAccIntrStatusArr);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_GetAccumulatedIntrStatus func Exit, Status: %d", result);

    return result;
}


/* Extension APIs to read peripheral extended interrupts */


/**
@brief Get the Extended Interrupt Status

This function returns the extended Interrupt Status associated with
the intrId

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] puIntrStatusMask Pointer to the uint32 which will be
populated by this function reflecting the status of the extended
interrupts. Each set bit in this parameter, represents that
extended Interrupts is set.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_ExtendedInterruptStatus(
    SpmiPhyPeriphIdType periphID,
    uint32 *puIntrStatusMask)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus: Parameters: periphID: 0x%x, puIntrStatusMask: 0x%x",
        periphID, 
        puIntrStatusMask);

    if((SPMI_MAX_PHY_PERIPH_ID < periphID)||
        (NULL == puIntrStatusMask))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, puIntrStatusMask: 0x%x", 
            periphID,
            puIntrStatusMask);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d", 
            periphID, 
            result);
        goto ERROR;
    }

    if(gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid)
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    *puIntrStatusMask = HAL_Spmi_PicGetPeriphIntrStatus(
        gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        uApid);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptStatus func Exit, Status: %d", result);

    return result;
}


/**
@brief Clear the Extended Interrupt Status

This function clears the extended Interrupt Status associated
with the intrId

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uIntrClearMask This is a mask of bits which
reprsents Interrupts to be cleared.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_ExtendedInterruptClear(
    SpmiPhyPeriphIdType periphID,
    uint32 uIntrClearMask)
{
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear: Parameters: periphID: 0x%x, uIntrClearMask: 0x%x",
        periphID, 
        uIntrClearMask);

    if((SPMI_MAX_PHY_PERIPH_ID < periphID) || (!uIntrClearMask))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, uIntrClearMask: 0x%x", 
            periphID,
            uIntrClearMask);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    /* Get the APID corresponding to the PPID */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_GetLogicalIdFromPpid(periphID, &uApid)))
    {
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear : ***SPMI_ERROR*** Unable to get APID from PPID, periphID: 0x%x, result: %d",
            periphID, 
            result);
        goto ERROR;
    }

    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    HAL_Spmi_PicPeriphIntrClear(gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        uApid,
        uIntrClearMask);

ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_ExtendedInterruptClear func Exit, Status: %d", result);

    return result;
}


/**
@brief Get the Logical(APID) from a physical peripheral Id (PPID)

This function returns the logical ID of a peripheral from the given
PPID

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] puLogicalPeriphId Pointer to the uint32 which will be
populated by this function. The value will be the corresponding APID

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None
*/
SpmiIntrCtlr_ResultType SpmiIntrCtlr_GetLogicalIdFromPpid(
    SpmiPhyPeriphIdType periphID,
    uint32 *puLogicalPeriphId)
{
    int32 nResult = 0;
    SpmiIntrCtlr_ResultType result = SPMI_INTR_CTLR_SUCCESS;
    uint32 uApid;

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid func Entry");

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid: Parameters: periphID: 0x%x, puLogicalPeriphId: 0x%x",
        periphID, 
        puLogicalPeriphId);

    /* Verify the input parameters */
    if ((SPMI_MAX_PHY_PERIPH_ID < periphID) || (NULL == puLogicalPeriphId))
    {
        result = SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid : ***SPMI_ERROR*** Invalid Parameters: periphID: 0x%x, puLogicalPeriphId: %d", 
            periphID,
            puLogicalPeriphId);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_INTR_CTLR_SUCCESS != (result = SpmiIntrCtlr_InitializeIfNeeded(FALSE)))
    {
        SPMI_LOG(FATAL_ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid : ***SPMI_ERROR*** Spmi Intr Ctlr Init failed, Status: %d", 
            result);
        goto ERROR;
    }

    if (-1 == (nResult = HAL_Spmi_CfgGetApidFromPpidUsingRadixMap(
        gSpmiIntrCtlrDeviceInfo.swConfig.halHandle,
        &gSpmiIntrCtlrDeviceInfo.uMapTable[0],
        periphID)))
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid : ***SPMI_ERROR*** Unable to get APID for periphID: 0x%x", 
            periphID,
            puLogicalPeriphId);
        goto ERROR;
    }

    uApid = (uint32)nResult;

    if((gSpmiIntrCtlrDeviceInfo.controllerConfig.uMaxPeriphSupported <= uApid))
    {
        result =  SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND;
        SPMI_LOG(ERROR, 
            "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid : ***SPMI_ERROR*** Invalid APID from PPID, periphID: 0x%x, uApid: %d, result: %d", 
            periphID,
            uApid,
            result);
        goto ERROR;
    }

    SPMI_LOG(INFO, 
        "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid : periphID: 0x%x, uApid: %d",
        periphID, 
        uApid);

    *puLogicalPeriphId = uApid;


ERROR:

    SPMI_LOG(VERBOSE, "SpmiIntrCtlr: SpmiIntrCtlr_GetLogicalIdFromPpid func Exit, Status: %d", result);

    return result;
}

