#ifndef __SPMI_INTR_CTLR_DEFS_H__
#define __SPMI_INTR_CTLR_DEFS_H__
/*=========================================================================*/
/**
@file:  SpmiIntrCtlrDefs.h

@brief: This module provides the type definition to the SPMI Interrupt 
Controller software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiIntrCtlr/SpmiIntrCtlr_Defs.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
02/28/13   PS      Added support for Intr Ownership check
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================	
*/

#include "Spmi_ComDefs.h"

/** SPMI ISR Context type */
typedef void *SpmiIsrCtxt;

/** SPMI ISR type */
typedef void * (*SpmiIsr)(
    SpmiIsrCtxt,
    uint32/*Peripheral extended Interrupts mask*/);

/** SPMI Physical Peripheral Id type */
typedef uint32 SpmiPhyPeriphIdType;

typedef enum
{
    SPMI_INTR_CTLR_SUCCESS = 0, /**< Success */
    SPMI_INTR_CTLR_FAILURE_INIT_FAILED,/**< Driver Initialization failed */
    SPMI_INTR_CTLR_FAILURE_INVALID_PARAMETER, /**< Invalid Parameter passed to an API */
    SPMI_INTR_CTLR_FAILURE_GENERAL_FAILURE, /**< General Failure */
    SPMI_INTR_CTLR_FAILURE_INTR_ALREADY_REGISTERED, /**< ISR already registered */
    SPMI_INTR_CTLR_FAILURE_INTR_NOT_REGISTERED, /**< ISR not registered */
    SPMI_INTR_CTLR_FAILURE_APID_MAPPING_NOT_FOUND, /**< PPID-APID mapping missing */
    SPMI_INTR_CTLR_FAILURE_INTR_NOT_OWNED_BY_THIS_EE, /**< Intr is not owned by this EE */

    SPMI_INTR_CTLR_RESULT_TYPE_32_BIT_ALIGNMENT = 0x7FFFFFFF,
}SpmiIntrCtlr_ResultType;

#endif /* __SPMI_INTR_CTLR_DEFS_H__ */

