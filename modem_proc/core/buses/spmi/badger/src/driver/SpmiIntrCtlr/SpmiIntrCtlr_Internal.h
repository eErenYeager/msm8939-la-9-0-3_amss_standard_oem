#ifndef __SPMI_INTR_CTLR_INTERNAL_H__
#define __SPMI_INTR_CTLR_INTERNAL_H__
/*=========================================================================*/
/**
@file:  SpmiIntrCtlrInternal.h

@brief: This is an internal header file to the Spmi Interrupt Controller
driver software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiIntrCtlr/SpmiIntrCtlr_Internal.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
03/15/13   PS      Added profiling capability
10/24/12   PS      Added Logging capability
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "HalSpmi.h"
#include "SpmiIntrCtlr.h"
#define SPMI_INTR_DEBUG_MAX_INTRS 10

/** SPMI ISR type */
typedef void *(*SpmiIntrCtlr_IsrType)(void *parg);

typedef struct
{
    SpmiIsr isr;
    SpmiIsrCtxt ctxt;
    uint8 bIsRegistered;
    uint8 ucMask;
}SpmiIntrCtlr_IsrConfigType;

typedef struct
{
    uint32 uThisOwnerNumber;
}SpmiIntrCtlr_ChipConfigType;

typedef struct
{
    uint64 *puSpmiIntrCtlrVirtualAddress;
    HAL_Spmi_HandleType halHandle;
}SpmiIntrCtlr_SwConfigType;

typedef struct
{
    uint32 uDummy;
}SpmiIntrCtlr_PlatformConfigType;

typedef struct
{
    uint32 uMaxPeriphSupported;
}SpmiIntrCtlr_ControllerConfigType;

typedef struct
{
    uint8 ucStart;
    uint8 ucFinish;
    uint8 ucApid;
    uint8 ucIntrStatus;
	uint64 u64StartTimeTick;
    uint64 u64EndTimeTick;
}SpmiIntrCtlr_DebugIntrType;

typedef struct
{
    SpmiIntrCtlr_ChipConfigType chipConfig;
    SpmiIntrCtlr_SwConfigType swConfig;
    SpmiIntrCtlr_PlatformConfigType platformConfig;
    SpmiIntrCtlr_ControllerConfigType controllerConfig;
    bool32 bInitialized;
    SpmiIntrCtlr_IsrConfigType *pIsrTable;
    bool32 bIsrInitialized;
    uint32 uMapTable[256];

    /* debug variables */
    SpmiIntrCtlr_DebugIntrType debugIntr[SPMI_INTR_DEBUG_MAX_INTRS]; /* Store last SPMI_INTR_DEBUG_MAX_INTRS interrupt details */
    uint32 uDebugIntrCurrentIndex;
    uint8 ucUnreliable; /* 1 means unreliable data */

}SpmiIntrCtlr_DeviceInfoType;

SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsInit(SpmiIntrCtlr_DeviceInfoType *);
SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsRegisterISR(
    SpmiIntrCtlr_IsrType isrRoutine,
    void *pargs);
SpmiIntrCtlr_ResultType SpmiIntrCtlr_OsMalloc(uint32 size, void **pMemPtr);
void SpmiIntrCtlr_OsUnregisterIsr(void);
void SpmiIntrCtlr_OsDeInit(SpmiIntrCtlr_DeviceInfoType *pDeviceInfo);
void SpmiIntrCtlr_OsFree(void *pMemPtr);



#endif /* __SPMI_INTR_CTLR_INTERNAL_H__ */


