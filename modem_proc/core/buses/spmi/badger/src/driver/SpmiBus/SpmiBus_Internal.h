#ifndef __SPMI_BUS_INTERNAL_H__
#define __SPMI_BUS_INTERNAL_H__
/*=========================================================================*/
/**
@file:  SpmiBus_Internal.h

@brief: This is an internal header file to the Spmi Bus driver software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiBus/SpmiBus_Internal.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
10/24/13   rc      Added the write and read error count varaibles 
05/14/13   PS      Moved debug entries to OS file
02/28/13   PS      Added Timeout, profiling and diagnostic mechanism
10/24/12   PS      Added Logging capability
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "HalPmicArb.h"
#include "SpmiBus.h"

typedef enum
{
    SPMI_READ = 1,
    SPMI_WRITE,
    SPMI_READ_MODIFY_WRITE,
    SPMI_COMMAND,
}SpmiBus_DebugTransAccessType;

typedef struct
{
    uint8 ucStart;
    uint8 ucFinish;
    uint8 ucResult;
    uint8 uTransAccess;
    uint32 uSlaveId;
    uint32 uRegisterAddress;
    uint32 uDataLen;
    uint64 u64EndTimeTick;
    uint64 u64StartTimeTick;
}SpmiBus_DebugTransactionType;

typedef struct
{
    uint32 uThisOwnerNumber;
}SpmiBus_ChipConfigType;


typedef struct
{
    uint64 *puPmicArbSpmiVirtualAddress;
    bool32 bPollingMode;
    HAL_PmicArb_HandleType halHandle;
}SpmiBus_SwConfigType;

typedef struct
{
    uint32 Dummy;
}SpmiBus_PlatformConfigType;

typedef struct
{
    uint32 uMaxAllowedReadWriteBytes;
}SpmiBus_ControllerConfigType;

typedef struct
{
    SpmiBus_ChipConfigType chipConfig;
    SpmiBus_SwConfigType swConfig;
    SpmiBus_PlatformConfigType platformConfig;
    SpmiBus_ControllerConfigType controllerConfig;

    bool32 bInitialized;

    /* Debug variables */
    SpmiBus_DebugTransactionType *pDebugTrans; /* Store last few transaction details */
    uint32 uNumDebugTransEntries;
    uint32 uDebugTransCurrentIndex;
    uint8 ucUnreliable;  /* Whether the data in debugTrans is reliable or not */

    SpmiBus_DebugTransactionType debugLastErrorTrans; /* Last error transaction seen */
    SpmiBus_DebugTransactionType debugFirstErrorTrans; /* First error transaction seen */
    uint32 uDebugReadErrorCount;
    uint32 uDebugWriteErrorCount;
}SpmiBus_DeviceInfoType;


typedef enum
{
    SPMI_CLK_DIAGNOSTIC_NOT_SUPPORTED,
    SPMI_CLK_IS_OFF,
    SPMI_CLK_IS_ON,
}SpmiBus_ClkDiagnosticType;


SpmiBus_ResultType SpmiBus_OsInit(SpmiBus_DeviceInfoType *);
void SpmiBus_OsDeInit(SpmiBus_DeviceInfoType *pDeviceInfo);
void SpmiBus_OsBusyWait(uint32 uUsec);
SpmiBus_ClkDiagnosticType SpmiBus_OsGetSerialClkState(void);
SpmiBus_ClkDiagnosticType SpmiBus_OsGetAhbClkState(void);

#endif /* __SPMI_BUS_INTERNAL_H__ */


