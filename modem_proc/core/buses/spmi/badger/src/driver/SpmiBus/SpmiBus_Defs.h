#ifndef __SPMI_BUS_DEFS_H__
#define __SPMI_BUS_DEFS_H__
/*=========================================================================*/
/**
@file:  SpmiBusDefs.h

@brief: This module provides the type definition to Spmi Bus driver software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiBus/SpmiBus_Defs.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
06/11/13   PS      Added SPMI_BUS_FAILURE_BUS_BUSY
03/11/12   PS      Added SPMI_BUS_FAILURE_TRANSACTION_TIMEOUT, 
                   SPMI_BUS_SERIAL_CLK_IS_OFF, SPMI_BUS_AHB_CLK_IS_OFF
07/05/12   UR      Moved Spmi_ComDefs.h from this file to SpmiBus.h
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2011 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/** Enumeration of SPMI Bus Result type */
typedef enum
{
    SPMI_BUS_SUCCESS = 0, /**< Success */
    SPMI_BUS_FAILURE_INIT_FAILED, /**< Driver Initialization failed */
    SPMI_BUS_FAILURE_INVALID_PARAMETER,/**< Invalid Parameter passed to an API */
    SPMI_BUS_FAILURE_GENERAL_FAILURE,/**< General Failure */
    SPMI_BUS_FAILURE_TRANSACTION_FAILED, /**< Spmi Transaction failed*/
    SPMI_BUS_FAILURE_TRANSACTION_DENIED,/**< Spmi transaction denied */
    SPMI_BUS_FAILURE_TRANSACTION_DROPPED,/**< Spmi transaction dropped */
    SPMI_BUS_FAILURE_TRANSACTION_TIMEOUT, /**< Spmi transaction timed out */
    SPMI_BUS_FAILURE_SERIAL_CLK_IS_OFF, /**< Spmi serial clk is off */
    SPMI_BUS_FAILURE_AHB_CLK_IS_OFF, /**< Spmi AHB clk is off */
    SPMI_BUS_FAILURE_BUS_BUSY, /**< Spmi Bus is busy */


    SPMI_BUS_RESULT_TYPE_32_BIT_ALIGNMENT = 0x7FFFFFFF,
}SpmiBus_ResultType;

/** Enumeration of SPMI Bus Access Prioirty type */
typedef enum
{
    SPMI_BUS_ACCESS_PRIORITY_LOW = 0,  /**< Low Priority */
    SPMI_BUS_ACCESS_PRIORITY_HIGH = 1, /**< High Priority */

    SPMI_BUS_ACCESS_PRIORITY_COUNT = 2, /**< Total priority count */
    SPMI_BUS_ACCESS_PRIORITY_TYPE_32_BIT_ALIGNMENT = 0x7FFFFFFF,
}SpmiBus_AccessPriorityType;


/** Enumeration of SPMI Bus Command Type */
typedef enum
{
    SPMI_BUS_COMMAND_RESET = 0,    /**< Reset command */
    SPMI_BUS_COMMAND_SLEEP = 1,    /**< Sleep command */
    SPMI_BUS_COMMAND_SHUTDOWN = 2, /**< Shutdown command */
    SPMI_BUS_COMMAND_WAKEUP = 3,   /**< Wakeup command */

    SPMI_BUS_COMMAND_COUNT = 4, /**< Total command count */
    SPMI_BUS_COMMAND_TYPE_32_BIT_ALIGNMENT = 0x7FFFFFFF,
}SpmiBus_CommandType;

#endif /* __SPMI_BUS_DEFS_H__ */
