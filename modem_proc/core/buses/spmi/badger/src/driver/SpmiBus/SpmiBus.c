/*=========================================================================*/
/**
@file SpmiBus.c

This module provides the driver Software for the SpmiBus driver software.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiBus/SpmiBus.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
10/24/13   rc      Add a running count of write and read errors encountered
                   and Error Fatal on timeout    
07/25/13   mq      Added API to query protocol status
06/11/13   PS      Check for Done bit being set before starting a new 
                   transaction
02/28/13   PS      Added Timeout, profiling and diagnostic mechanism
10/24/12   PS      Added Logging capability
09/04/12   PS      Added ReadModifyWrite api support
08/20/12   PS      Initialize the driver before parameter validation
07/13/11   PS      Fixed compiler warnings
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "SpmiBus.h"
#include "HalSpmi.h"
#include "HalPmicArb.h"
#include "SpmiBus_Internal.h"
#include "SpmiLogs.h"
#include "SpmiProfile.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

#define UNUSED(x) ((void)(x))

#define SPMI_BUS_MAX_NUM_SLAVES 16
#define SPMI_BUS_MAX_REGISTER_SPACE_LONG 65536
#define SPMI_BUS_TIMEOUT_USEC 400

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
SpmiBus_DeviceInfoType gSpmiBusDeviceInfo; /* Kept global for easy debugging */

/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
static uint32 ZERO = 0; /* To avoid while(0) warning */

/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/

static void SpmiBus_DebugTransStart(SpmiBus_DebugTransAccessType access,
                                    uint32 uSlaveId,
                                    uint32 uRegisterAddress,
                                    uint32 uDataLen)
{
    if((gSpmiBusDeviceInfo.pDebugTrans) && 
        (gSpmiBusDeviceInfo.uNumDebugTransEntries))
    {
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].ucFinish = 0;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].u64EndTimeTick = 0;

        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].ucStart = 1;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].u64StartTimeTick = SPMI_PROFILE_GET_TIMETICK();
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].ucResult = 0;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uTransAccess = (uint8) access;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uSlaveId = uSlaveId;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uRegisterAddress = uRegisterAddress;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uDataLen = uDataLen;
    }
}

static void SpmiBus_DebugTransFinish(SpmiBus_DebugTransAccessType access,
                                     uint32 uSlaveId,
                                     uint32 uRegisterAddress,
                                     uint32 uDataLen,
                                     SpmiBus_ResultType result)
{
    if((gSpmiBusDeviceInfo.pDebugTrans) && 
        (gSpmiBusDeviceInfo.uNumDebugTransEntries))
    {
        if((access != gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uTransAccess) ||
            (uSlaveId != gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uSlaveId) ||
            (uRegisterAddress != gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uRegisterAddress) ||
            (uDataLen != gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uDataLen))
        {
            SPMI_LOG(WARNING, "SpmiBus: SpmiBus_DebugTransFinish: ***SPMI_WARNING*** Debug start and finish data doesn't match. Start: %d 0x%x 0x%x %d, Finish: %d 0x%x 0x%x %d",
                gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uTransAccess,
                gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uSlaveId,
                gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uRegisterAddress,
                gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].uDataLen,
                access,
                uSlaveId,
                uRegisterAddress,
                uDataLen);
            gSpmiBusDeviceInfo.ucUnreliable = 1; /* Debug data is not reliable */
        }

        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].ucFinish = 1;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].ucResult = (uint8)result;
        gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex].u64EndTimeTick = SPMI_PROFILE_GET_TIMETICK();

        /* Now set the first and last error trans, if this trans is error */
        if(SPMI_BUS_SUCCESS != result)
        {
            if(access == SPMI_READ)
            {
                gSpmiBusDeviceInfo.uDebugReadErrorCount++;
            }  
            else if(access == SPMI_WRITE)
            {
                gSpmiBusDeviceInfo.uDebugWriteErrorCount++;
            }

            gSpmiBusDeviceInfo.debugLastErrorTrans = gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex];

            if(gSpmiBusDeviceInfo.debugFirstErrorTrans.ucStart != 1) /* If this is the first failure */
            {
                gSpmiBusDeviceInfo.debugFirstErrorTrans = gSpmiBusDeviceInfo.pDebugTrans[gSpmiBusDeviceInfo.uDebugTransCurrentIndex];
            }

            if(SPMI_BUS_FAILURE_TRANSACTION_TIMEOUT == result)
            {
                SPMI_LOG(FATAL_ERROR, "SPMI transaction timeout");
            }
        }

        /* Increment the current index to point to the next */
        gSpmiBusDeviceInfo.uDebugTransCurrentIndex = (gSpmiBusDeviceInfo.uDebugTransCurrentIndex+1)%gSpmiBusDeviceInfo.uNumDebugTransEntries;
    }
}


static SpmiBus_ResultType SpmiBus_Initialize(void)
{
    uint32 spmiStatus;
    char *pcVersion, ppszPmicArbSpmiVersion[20];
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;

    pcVersion = &ppszPmicArbSpmiVersion[0];

    /* Initialise OS specific information. Read all the configurations */
    if(SPMI_BUS_SUCCESS != (result = SpmiBus_OsInit(&gSpmiBusDeviceInfo)))
    {        
        SPMI_LOG(FATAL_ERROR, "SpmiBus: SpmiBus_Initialize: ***SPMI_FATAL_ERROR*** SpmiBus_OsInit failed, Result: %d", result);
        return result;
    }

    gSpmiBusDeviceInfo.swConfig.halHandle = (HAL_PmicArb_HandleType)gSpmiBusDeviceInfo.swConfig.puPmicArbSpmiVirtualAddress;

    /* Initialise PMIC Arbiter HAL */
    HAL_PmicArb_Init(gSpmiBusDeviceInfo.swConfig.halHandle, &pcVersion);

    /* Read controller information */
    gSpmiBusDeviceInfo.controllerConfig.uMaxAllowedReadWriteBytes = HAL_PmicArb_ChannelGetMaxAllowedReadWriteBytes(gSpmiBusDeviceInfo.swConfig.halHandle);

    /* Initialize debug variable */
    gSpmiBusDeviceInfo.uDebugTransCurrentIndex = 0;
    gSpmiBusDeviceInfo. ucUnreliable = 0;

    gSpmiBusDeviceInfo.bInitialized = TRUE;
    
    spmiStatus = HAL_Spmi_GetProtocolStatus(gSpmiBusDeviceInfo.swConfig.halHandle);
    UNUSED(spmiStatus);
    SPMI_LOG(VERBOSE, "SPMI protocol status at init: 0x%lx", spmiStatus);
    
    return SPMI_BUS_SUCCESS;
}

__inline static SpmiBus_ResultType SpmiBus_InitializeIfNeeded(void)
{
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;

    /* Initialize the driver if this is the first call */
    if(!gSpmiBusDeviceInfo.bInitialized)
    {
        result = SpmiBus_Initialize();
        SPMI_LOG(INFO,
            "SpmiBus: SpmiBus_InitializeIfNeeded : SpmiBus_Initialize finished with status: %d",
            result);
        return result;
    }
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_InitializeIfNeeded : Bus already initialized");
    return result;
}


static SpmiBus_ResultType SpmiBus_WaitForStatusDone(void)
{
    HAL_PmicArb_ChannelStatusFieldMaskType status = (HAL_PmicArb_ChannelStatusFieldMaskType)0;
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;
    uint32 uTimeOutCount = SPMI_BUS_TIMEOUT_USEC;
    SpmiBus_ClkDiagnosticType uSerialClkState = SPMI_CLK_DIAGNOSTIC_NOT_SUPPORTED;
    SpmiBus_ClkDiagnosticType uAhbClkState = SPMI_CLK_DIAGNOSTIC_NOT_SUPPORTED;

    if(gSpmiBusDeviceInfo.swConfig.bPollingMode)
    {
        while(uTimeOutCount-- > 0) 
        {
            status = HAL_PmicArb_ChannelGetStatus(gSpmiBusDeviceInfo.swConfig.halHandle,
                gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber);
            if ((status & HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK)||
                (status & HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK))
            {        
                break;
            }
            else
            {
                SpmiBus_OsBusyWait(1); /* Wait for 1 usec at a time*/
            }
        }
    }
    else /* Intr mode */
    {
        /* Block the current thread and wait for interrupt to release it */
        status = HAL_PmicArb_ChannelGetStatus(gSpmiBusDeviceInfo.swConfig.halHandle,gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber);
    }

    if(status & HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK)
    {
        result = SPMI_BUS_FAILURE_TRANSACTION_DROPPED;
    }
    else if(status & HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK)
    {
        if(status & HAL_PMIC_ARB_CHANNEL_STATUS_FAILURE_MASK)
        {
            result = SPMI_BUS_FAILURE_TRANSACTION_FAILED;
        }
        else if(status & HAL_PMIC_ARB_CHANNEL_STATUS_DENIED_MASK)
        {
            result = SPMI_BUS_FAILURE_TRANSACTION_DENIED;
        }
    }
    else /* If done mask is not set, transaction timed out. */
    {
        result = SPMI_BUS_FAILURE_TRANSACTION_TIMEOUT;
    }

    if(SPMI_BUS_SUCCESS != result)
    {
        uSerialClkState = SpmiBus_OsGetSerialClkState();
        uAhbClkState = SpmiBus_OsGetAhbClkState();

        if(SPMI_CLK_IS_OFF == uSerialClkState)
        {
            result =  SPMI_BUS_FAILURE_SERIAL_CLK_IS_OFF;
        }
        else if(SPMI_CLK_IS_OFF == uAhbClkState)
        {
            result =  SPMI_BUS_FAILURE_AHB_CLK_IS_OFF;
        }

        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_WaitForStatusDone: ***SPMI_ERROR*** Result: %d, Protocol Status: 0x%lx, Channel Status: 0x%x, Serial Clk State: %d, AHB Clk State: %d",
            result,
            HAL_Spmi_GetProtocolStatus(gSpmiBusDeviceInfo.swConfig.halHandle),
            status, 
            uSerialClkState, 
            uAhbClkState);
    }
    return result;
}

static SpmiBus_ResultType SpmiBus_ReadLongInternal(uint32 uSlaveId,
                                                   SpmiBus_AccessPriorityType eAccessPriority,
                                                   uint32 uRegisterAddress,
                                                   uint8 *pucData,
                                                   uint32 uDataLen,
                                                   uint32 *puTotalBytesRead)
{
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;
    uint32 uBytesRead = 0;
    uint32 uBytesToReadInThisIteration;

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal func Entry");

    do
    {
        if((uDataLen - uBytesRead) > gSpmiBusDeviceInfo.controllerConfig.uMaxAllowedReadWriteBytes)
        {
            uBytesToReadInThisIteration = gSpmiBusDeviceInfo.controllerConfig.uMaxAllowedReadWriteBytes;
        }
        else
        {
            uBytesToReadInThisIteration = uDataLen - uBytesRead;
        }

        /* Check if the Done bit is set. If not, previous transaction is stuck */
        if(!(HAL_PmicArb_ChannelGetStatus(gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber) &
            HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK))
        {
            result = SPMI_BUS_FAILURE_BUS_BUSY;
            break;
        }


        SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal: Queueing the command");

        HAL_PmicArb_SpmiCommand( gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber,
            uSlaveId,
            HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG,
            (HAL_PmicArb_SpmiAccessPrioirtyType) eAccessPriority,
            uRegisterAddress + uBytesRead,
            uBytesToReadInThisIteration);

        SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal: Queued the command");

        if(SPMI_BUS_SUCCESS != (result = SpmiBus_WaitForStatusDone()))
        {
            break;
        }

        SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal: Wait for status done");

        HAL_PmicArb_SpmiReadInDataBuffer(gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber,
            &pucData[uBytesRead],
            uBytesToReadInThisIteration);

        SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal: Read data from buffer");

        uBytesRead = uBytesRead + uBytesToReadInThisIteration;

        SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_ReadLongInternal: Finished reading %d bytes", uBytesRead);

    }while(uBytesRead < uDataLen);

    if(puTotalBytesRead)
    {
        *puTotalBytesRead = uBytesRead;
    }

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadLongInternal func Exit");

    return result;
}

static SpmiBus_ResultType SpmiBus_WriteLongInternal(uint32 uSlaveId,
                                                    SpmiBus_AccessPriorityType eAccessPriority,
                                                    uint32 uRegisterAddress,
                                                    uint8 *pucData,
                                                    uint32 uDataLen)
{
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;
    uint32 uBytesWrote = 0;
    uint32 uBytesToWriteInThisIteration;

    SPMI_PROFILE("SpmiBus: SpmiBus_WriteLongInternal func Entry");

    do
    {
        if((uDataLen - uBytesWrote) > gSpmiBusDeviceInfo.controllerConfig.uMaxAllowedReadWriteBytes)
        {
            uBytesToWriteInThisIteration = gSpmiBusDeviceInfo.controllerConfig.uMaxAllowedReadWriteBytes;
        }
        else
        {
            uBytesToWriteInThisIteration = uDataLen - uBytesWrote;
        }

        /* Check if the Done bit is set. If not, previous transaction is stuck */
        if(!(HAL_PmicArb_ChannelGetStatus(gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber) &
            HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK))
        {
            result = SPMI_BUS_FAILURE_BUS_BUSY;
            break;
        }

        SPMI_PROFILE("SpmiBus: SpmiBus_WriteLongInternal: Queueing the command");

        HAL_PmicArb_SpmiWriteOutDataBuffer(gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber,
            &pucData[uBytesWrote],
            uBytesToWriteInThisIteration);


        HAL_PmicArb_SpmiCommand( gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber,
            uSlaveId,
            HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE_LONG,
            (HAL_PmicArb_SpmiAccessPrioirtyType) eAccessPriority,
            uRegisterAddress + uBytesWrote,
            uBytesToWriteInThisIteration);

        SPMI_PROFILE("SpmiBus: SpmiBus: SpmiBus_WriteLongInternal: Queued the command");

        if(SPMI_BUS_SUCCESS != (result = SpmiBus_WaitForStatusDone()))
        {
            break;
        }

        SPMI_PROFILE("SpmiBus: SpmiBus_WriteLongInternal: Wait for status done");

        uBytesWrote = uBytesWrote + uBytesToWriteInThisIteration;

        SPMI_LOG(VERBOSE,
            "SpmiBus: SpmiBus_WriteLongInternal: Finished writing %d bytes",
            uBytesWrote);

    }while(uBytesWrote < uDataLen);

    SPMI_PROFILE("SpmiBus: SpmiBus_WriteLongInternal func Exit");

    return result;
}
/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/
/**
@brief Initialize the driver

This function initializes the Spmi Bus driver. It is mandatory to call this
function before using any other APIs of this driver

@return SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_DeInit
*/
SpmiBus_ResultType SpmiBus_Init()
{
    SpmiBus_ResultType result;

    SPMI_LOG_INIT();
    
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_Init func Entry");

    /* Initialize the driver if this is the first call */
    result = SpmiBus_InitializeIfNeeded();

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_Init func Exit, Status: %d", result);

    return result;
}


/**
@brief De-initialize the driver

This function de-initializes the Spmi Bus driver.

@return SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_Init
*/
SpmiBus_ResultType SpmiBus_DeInit()
{
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_DeInit func Entry");

    /* DeInitialize the driver if it is initialized */
    if(gSpmiBusDeviceInfo.bInitialized)
    {

        /* Deinitialize the drivers OS layer */
        SpmiBus_OsDeInit(&gSpmiBusDeviceInfo);

        gSpmiBusDeviceInfo.bInitialized = FALSE;
    }
    else
    {
        result = SPMI_BUS_SUCCESS;
        SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_DeInit: Bus in not initialized");
    }

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_DeInit func Exit, Status: %d", result);

    return result;
}


/**
@brief Reads from a SPMI slave device

This function reads data from SPMI Slave device. The register address
is in long(16 bit) format

@param[in] uSlaveId Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs
to be sent on the SPMI bus

@param[in] uRegisterAddress Register Address on the SPMI
slave device from which read is to be initiated. 16 LSB bit of this
parameter are used as Register address

@param[in] pucData Pointer to data array. Data read from the
SPMI bus will be filled in this array

@param[in] uDataLen Number of bytes to read

@param[out] puTotalBytesRead Pointer to an uint32 which is
used to return the total number of bytes read from SPMI
device

@return SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_WriteLong
*/
SpmiBus_ResultType SpmiBus_ReadLong(uint32 uSlaveId,
                                    SpmiBus_AccessPriorityType eAccessPriority,
                                    uint32 uRegisterAddress,
                                    uint8 *pucData,
                                    uint32 uDataLen,
                                    uint32 *puTotalBytesRead)
{

    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadLong func Entry");

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_ReadLong func Entry");

    SPMI_LOG(INFO,
        "SpmiBus: SpmiBus_ReadLong: Parameters: uSlaveId: 0x%x, eAccessPriority: %d, uRegisterAddress: 0x%x, pucData: 0x%x, uDataLen: %d, puTotalBytesRead 0x%x",
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen,
        puTotalBytesRead);

    SpmiBus_DebugTransStart(SPMI_READ, uSlaveId, uRegisterAddress, uDataLen);


    /* Initialize the driver if this is the first call */
    if(SPMI_BUS_SUCCESS != (result = SpmiBus_InitializeIfNeeded()))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiBus: SpmiBus_ReadLong : ***SPMI_FATAL_ERROR*** Spmi Bus Init failed, Status: %d",
            result);
        goto ERROR;
    }

    /* Validate the parameters */
    if((SPMI_BUS_MAX_NUM_SLAVES <= uSlaveId) ||
        (SPMI_BUS_ACCESS_PRIORITY_COUNT <= eAccessPriority) ||
        (SPMI_BUS_MAX_REGISTER_SPACE_LONG <= uRegisterAddress) ||
        (NULL == pucData) ||
        (0 == uDataLen))
    {
        result = SPMI_BUS_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_ReadLong : ***SPMI_ERROR*** Invalid Parameters: result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, uRegisterAddress: 0x%lx, pucData: %p, uDataLen: %ld",
            result,
            uSlaveId,
            eAccessPriority,
            uRegisterAddress,
            pucData,
            uDataLen);
        goto ERROR;
    }

    result = SpmiBus_ReadLongInternal(uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen,
        puTotalBytesRead);

    if(SPMI_BUS_SUCCESS != result)
    {
        result = result; /* Avoid compiler warining/error when logs are removed */
        SPMI_LOG(ERROR, "SpmiBus: SpmiBus_ReadLong: ***SPMI_ERROR*** Read failed with result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, uRegisterAddress: 0x%lx, pucData: %p, uDataLen: %ld",
            result,
            uSlaveId,
            eAccessPriority,
            uRegisterAddress,
            pucData,
            uDataLen);
    }

ERROR:
    SpmiBus_DebugTransFinish(SPMI_READ, uSlaveId, uRegisterAddress, uDataLen, result);
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_ReadLong func Exit, Status: %d", result);

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadLong func Exit");
    return result;
}


/**
@brief Writes to a SPMI slave device

This function writes data to SPMI Slave device. The register address
is in long(16 bit) format

@param[in] uSlaveId Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs
to be sent on the SPMI bus

@param[in] uRegisterAddress Register Address on the SPMI
slave device to which write is to be initiated. 16 LSB bit of this
parameter are used as Register address

@param[in] pucData Pointer to data array containing data to
be written on the bus.

@param[in] uDataLen Number of bytes to write

@return SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_ReadLong
*/
SpmiBus_ResultType SpmiBus_WriteLong(uint32 uSlaveId,
                                     SpmiBus_AccessPriorityType eAccessPriority,
                                     uint32 uRegisterAddress,
                                     uint8 *pucData,
                                     uint32 uDataLen)
{

    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;

    SPMI_PROFILE("SpmiBus: SpmiBus_WriteLong func Entry");

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_WriteLong func Entry");

    SPMI_LOG(INFO,
        "SpmiBus: SpmiBus_WriteLong: Parameters: uSlaveId: 0x%x, eAccessPriority: %d, uRegisterAddress: 0x%x, pucData: 0x%x, uDataLen: %d",
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen);

    SpmiBus_DebugTransStart(SPMI_WRITE, uSlaveId, uRegisterAddress, uDataLen);

    /* Initialize the driver if this is the first call */
    if(SPMI_BUS_SUCCESS != (result = SpmiBus_InitializeIfNeeded()))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiBus: SpmiBus_WriteLong : ***SPMI_FATAL_ERROR*** Spmi Bus Init failed, Status: %d",
            result);
        goto ERROR;
    }

    /* Validate the parameters */
    if((SPMI_BUS_MAX_NUM_SLAVES <= uSlaveId) ||
        (SPMI_BUS_ACCESS_PRIORITY_COUNT <= eAccessPriority) ||
        (SPMI_BUS_MAX_REGISTER_SPACE_LONG <= uRegisterAddress) ||
        (NULL == pucData) ||
        (0 == uDataLen))
    {
        result = SPMI_BUS_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_WriteLong : ***SPMI_ERROR*** Invalid Parameters: result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, uRegisterAddress: 0x%lx, pucData: %p, uDataLen: %ld",
            result,
            uSlaveId,
            eAccessPriority,
            uRegisterAddress,
            pucData,
            uDataLen);
        goto ERROR;
    }

    result = SpmiBus_WriteLongInternal( uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen);

    if(SPMI_BUS_SUCCESS != result)
    {
        result = result; /* Avoid compiler warining/error when logs are removed */
        SPMI_LOG(ERROR, "SpmiBus: SpmiBus_WriteLong: ***SPMI_ERROR*** Write failed with result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, uRegisterAddress: 0x%lx, pucData: %p, uDataLen: %ld",
            result,
            uSlaveId,
            eAccessPriority,
            uRegisterAddress,
            pucData,
            uDataLen);
    }

ERROR:

    SpmiBus_DebugTransFinish(SPMI_WRITE, uSlaveId, uRegisterAddress, uDataLen, result);
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_WriteLong func Exit, Status: %d", result);

    SPMI_PROFILE("SpmiBus: SpmiBus_WriteLong func Exit");

    return result;
}

/**
@brief Reads Modify Writes a byte from SPMI slave device

This function reads a byte from SPMI slave device, modifies it
and writes it back to SPMI Slave device. The register address is
in long(16 bit) format

@param[in] uSlaveId Slave Id of the device

@param[in] Priority with which the command needs to be sent on the SPMI bus

@param[in] uRegisterAddress Register Address on the SPMI
slave device to which write is to be initiated. 16 LSB bit of this
parameter are used as Register address

@param[in] uData Data byte to be read modify write

@param[in] uMask Mask of the bits which needs to be modified

@param[out] pucDataWritten Pointer to an uint8 which is used
to return the value of the byte written on the bus after after
read modify write

@return SPMI_BUS_SUCCESS on success, error code on error

@see None
*/
SpmiBus_ResultType SpmiBus_ReadModifyWriteLongByte(uint32 uSlaveId,
                                                   SpmiBus_AccessPriorityType eAccessPriority,
                                                   uint32 uRegisterAddress,
                                                   uint32 uData,
                                                   uint32 uMask,
                                                   uint8 *pucDataWritten)
{
    uint32 uTotalBytesRead;
    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;
    uint8 ucReadData;
    uint8 ucWriteData;

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadModifyWriteLongByte func Entry");

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_ReadModifyWriteLongByte func Entry");

    SPMI_LOG(INFO,
        "SpmiBus: SpmiBus_ReadModifyWriteLongByte: Parameters: uSlaveId: 0x%x, eAccessPriority: %d, uRegisterAddress: 0x%x, uData: 0x%x, pucDataWritten: 0x%x",
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        uData,
        pucDataWritten);

    SpmiBus_DebugTransStart(SPMI_READ_MODIFY_WRITE, uSlaveId, uRegisterAddress, 1);

    /* Validate the parameters */
    if((SPMI_BUS_MAX_NUM_SLAVES <= uSlaveId) ||
        (SPMI_BUS_ACCESS_PRIORITY_COUNT <= eAccessPriority) ||
        (SPMI_BUS_MAX_REGISTER_SPACE_LONG <= uRegisterAddress) ||
        (0xFF < (uint8)uData) ||
        (0xFF < (uint8)uMask))
    {
        result = SPMI_BUS_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_ReadModifyWriteLongByte : ***SPMI_ERROR*** Invalid Parameters: result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, uRegisterAddress: 0x%lx, uData: 0x%lx, uMask: 0x%lx",
            result,
            uSlaveId,
            eAccessPriority,
            uRegisterAddress,
            uData,
            uMask);
        goto ERROR;
    }

    /* Initialize the driver if this is the first call */
    if(SPMI_BUS_SUCCESS != (result = SpmiBus_InitializeIfNeeded()))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiBus: SpmiBus_ReadModifyWriteLongByte : ***SPMI_FATAL_ERROR*** Spmi Bus Init failed, Status: %d",
            result);
        goto ERROR;
    }

    if(SPMI_BUS_SUCCESS != (result = SpmiBus_ReadLongInternal(uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        &ucReadData,
        1,
        &uTotalBytesRead)))
    {
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_ReadModifyWriteLongByte: ***SPMI_ERROR*** Spmi read long failed, Status: %d",
            result);
        goto ERROR;
    }

    SPMI_LOG(VERBOSE,
        "SpmiBus: SpmiBus_ReadModifyWriteLongByte: Spmi read long internal successful");

    ucWriteData = ucReadData & ~uMask; /* Clear all the bits represented by the mask */
    ucWriteData = ucWriteData | ((uint8)uData & (uint8)uMask); /* Update the write data */


    if(SPMI_BUS_SUCCESS != (result = SpmiBus_WriteLongInternal(uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        &ucWriteData,
        1)))
    {
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_ReadModifyWriteLongByte: ***SPMI_ERROR*** Spmi write long failed, Status: %d",
            result);
        goto ERROR;
    }

    SPMI_LOG(VERBOSE,
        "SpmiBus: SpmiBus_ReadModifyWriteLongByte: Spmi write long internal successful");

    if(pucDataWritten)
    {
        *pucDataWritten = ucWriteData;
    }

ERROR:

    SpmiBus_DebugTransFinish(SPMI_READ_MODIFY_WRITE, uSlaveId, uRegisterAddress, 1, result);
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_ReadModifyWriteLongByte func Exit, Status: %d", result);

    SPMI_PROFILE("SpmiBus: SpmiBus_ReadModifyWriteLongByte func Exit");

    return result;
}


/**
@brief Send a command to SPMI slave device

This function sends a command to SPMI slave device

@param[in] uSlaveId Slave Id of the device

@param[in] Priority with which the command needs to be sent on the SPMI bus

@param[in] eSpmiCommand Command type

@return SPMI_BUS_SUCCESS on success, error code on error

@see None

*/
SpmiBus_ResultType SpmiBus_Command(uint32 uSlaveId,
                                   SpmiBus_AccessPriorityType eAccessPriority,
                                   SpmiBus_CommandType eSpmiCommand)
{

    SpmiBus_ResultType result = SPMI_BUS_SUCCESS;
    HAL_PmicArb_SpmiCommandType halCommand = HAL_PMIC_ARB_SPMI_COMMAND_RESET;

    SPMI_PROFILE("SpmiBus: SpmiBus_Command func Entry");

    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_Command func Entry");

    SPMI_LOG(INFO,
        "SpmiBus: SpmiBus_Command: Parameters: uSlaveId: 0x%x, eAccessPriority: %d, eSpmiCommand: %d",
        uSlaveId,
        eAccessPriority,
        eSpmiCommand);

    SpmiBus_DebugTransStart(SPMI_COMMAND, uSlaveId, (uint32)eSpmiCommand, 1);

    /* Initialize the driver if this is the first call */
    if(SPMI_BUS_SUCCESS != (result = SpmiBus_InitializeIfNeeded()))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiBus: SpmiBus_Command : ***SPMI_FATAL_ERROR*** Spmi Bus Init failed, Status: %d",
            result);
        goto ERROR;
    }

    /* Validate the parameters */
    if((SPMI_BUS_MAX_NUM_SLAVES <= uSlaveId) ||
        (SPMI_BUS_ACCESS_PRIORITY_COUNT <= eAccessPriority) ||
        (SPMI_BUS_COMMAND_COUNT <= eSpmiCommand))
    {
        result = SPMI_BUS_FAILURE_INVALID_PARAMETER;
        SPMI_LOG(ERROR,
            "SpmiBus: SpmiBus_Command : ***SPMI_ERROR*** Invalid Parameters: result: %d, uSlaveId: 0x%lx, eAccessPriority: %d, eSpmiCommand: %d",
            result,
            uSlaveId,
            eAccessPriority,
            eSpmiCommand);
        goto ERROR;
    }

    do
    {
        switch (eSpmiCommand)
        {
        case SPMI_BUS_COMMAND_RESET:
            halCommand = HAL_PMIC_ARB_SPMI_COMMAND_RESET;
            break;
        case SPMI_BUS_COMMAND_SLEEP:
            halCommand = HAL_PMIC_ARB_SPMI_COMMAND_SLEEP;
            break;
        case SPMI_BUS_COMMAND_SHUTDOWN:
            halCommand = HAL_PMIC_ARB_SPMI_COMMAND_SHUTDOWN;
            break;
        case SPMI_BUS_COMMAND_WAKEUP:
            halCommand = HAL_PMIC_ARB_SPMI_COMMAND_WAKEUP;
            break;
        default:
            result = SPMI_BUS_FAILURE_INVALID_PARAMETER;
            break;
        }

        if(SPMI_BUS_SUCCESS != result)
        {
            SPMI_LOG(ERROR,
                "SpmiBus: SpmiBus_Command : ***SPMI_ERROR*** Result: %d, Invalid Command: %d",
                result,
                eSpmiCommand);
            goto ERROR;
        }

        HAL_PmicArb_SpmiCommand( gSpmiBusDeviceInfo.swConfig.halHandle,
            gSpmiBusDeviceInfo.chipConfig.uThisOwnerNumber,
            uSlaveId,
            halCommand,
            (HAL_PmicArb_SpmiAccessPrioirtyType) eAccessPriority,
            0,
            0);

        if(SPMI_BUS_SUCCESS != (result = SpmiBus_WaitForStatusDone()))
        {
            SPMI_LOG(ERROR,
                "SpmiBus: SpmiBus_Command : ***SPMI_ERROR*** Spmi wait for status done failed, Status: %d",
                result);
            goto ERROR;
        }

        SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_Command: Spmi command successful");

    }while(ZERO);

ERROR:

    SpmiBus_DebugTransFinish(SPMI_COMMAND, uSlaveId, (uint32)eSpmiCommand, 1, result);
    SPMI_LOG(VERBOSE, "SpmiBus: SpmiBus_Command func Exit, Status: %d", result);

    SPMI_PROFILE("SpmiBus: SpmiBus_Command func Exit");

    return result;
}


