#ifndef __SPMI_BUS_H__
#define __SPMI_BUS_H__
/*=========================================================================*/
/**
@file:  SpmiBus.h

@brief: This module provides the interface to the Spmi Bus driver software.
It mainly provides APIs to do I/O on SPMI Bus via SPMI controller.		
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiBus/SpmiBus.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
09/04/12   PS      Added ReadModifyWrite api support
07/05/12   UR      Moved Spmi_ComDefs.h from SpmiBus_Defs.h to this file
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2011 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "Spmi_ComDefs.h"
#include "SpmiBus_Defs.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/
/**
@brief Initialize the driver 

This function initializes the Spmi Bus driver. It is mandatory to call this 
function before using any other APIs of this driver

@return  SPMI_BUS_SUCCESS on success, error code on error

@see PmicArbCfg_DeInit
*/
SpmiBus_ResultType SpmiBus_Init(void);


/**
@brief De-initialize the driver 

This function de-initializes the Spmi Bus driver.

@return  SPMI_BUS_SUCCESS on success, error code on error

@see PmicArbCfg_Init
*/
SpmiBus_ResultType SpmiBus_DeInit(void);


/**
@brief Reads from a SPMI slave device 

This function reads data from SPMI Slave device. The register address 
is in long(16 bit) format

@param[in] uSlaveId  Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs to be sent on
the SPMI bus

@param[in] uRegisterAddress  Register Address on the SPMI slave device from 
which read is to be initiated. 16 LSB bit of this parameter are used as 
Register address

@param[in] pucData  Pointer to data array. Data read from the SPMI bus will
be filled in this array

@param[in] uDataLen  Number of bytes to read

@param[out] puTotalBytesRead Pointer to an uint32 which is used to return the 
total number of bytes read from SPMI device

@return  SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_WriteLong()
*/
SpmiBus_ResultType  SpmiBus_ReadLong(
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress, 
    uint8 *pucData,
    uint32 uDataLen, 
    uint32 *puTotalBytesRead);


/**
@brief Writes to a SPMI slave device 

This function writes data to SPMI Slave device. The register address 
is in long(16 bit) format

@param[in] uSlaveId  Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs to be sent on 
the SPMI bus

@param[in] uRegisterAddress  Register Address on the SPMI slave device from 
which write is to be initiated. 16 LSB bit of this parameter are used as 
Register address

@param[in] pucData  Pointer to data array containing data to 
be written on the bus.

@param[in] uDataLen  Number of bytes to write 

@return  SPMI_BUS_SUCCESS on success, error code on error

@see SpmiBus_ReadLong()
*/
SpmiBus_ResultType  SpmiBus_WriteLong(
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    uint8 *pucData, 
    uint32 uDatalen);


/**
@brief Reads Modify Writes a byte from SPMI slave device

This function reads a byte from SPMI slave device, modifies it 
and writes it back to SPMI Slave device. The register address is 
in long(16 bit) format 

@param[in] uSlaveId  Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs 
to be sent on the SPMI bus

@param[in] uRegisterAddress  Register Address on the SPMI 
slave device to which write is to be initiated. 16 LSB bit of this
parameter are used as Register address

@param[in] uWriteData  Data byte to be read modify write 

@param[in] uMask  Mask of the bits which needs to be modified

@param[out] pucDataWritten Pointer to an uint8 which is used 
to return the value of the byte written on the bus after after 
read modify write

@return  SPMI_BUS_SUCCESS on success, error code on error

@see None
*/
SpmiBus_ResultType  SpmiBus_ReadModifyWriteLongByte(
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    uint32 uWriteData, 
    uint32 uMask,
	uint8 *pucDataWritten);


/**
@brief Send a command to SPMI slave device 

This function sends a command to SPMI slave device

@param[in] uSlaveId  Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs to be sent 
on the SPMI bus

@param[in] eSpmiCommand  Command type 

@return  SPMI_BUS_SUCCESS on success, error code on error

@see None
*/
SpmiBus_ResultType  SpmiBus_Command(
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    SpmiBus_CommandType eSpmiCommand); 


#endif /* __SPMI_BUS_H__ */
