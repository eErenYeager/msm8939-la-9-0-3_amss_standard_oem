/*=========================================================================*/
/**
@file SpmiBus_Os.c

This module provides the OS interface to the SPMI Bus driver.
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/driver/SpmiBus/SpmiBus_Os.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
05/16/13   PS      Moved debug entries to OS layer
04/12/13   PS      Replace IsClkEnabled to IsClkOn api 
02/28/13   PS      Added Timeout, profiling and diagnostic mechanism
12/03/12   PS      Added support for reading physical address of the core
10/24/12   PS      Added Logging capability
08/20/12   PS      Removed clk management. Clk should be accessed from RPM only 
08/17/12   PS      Changed DALDevice.h to DalDevice.h
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "SpmiBus_Internal.h"
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DDIHWIO.h"
#include "DDIClock.h"
#include "SpmiLogs.h"
#include "SpmiProfile.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
#define SPMI_BUS_DEBUG_MAX_TRANS 10

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
static DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
static DalDeviceHandle *hHWIO = NULL;
static DalDeviceHandle *hClk = NULL;
static ClockIdType spmiSerialClockId = 0;
static ClockIdType spmiAhbClockId = 0;
static bool32 isSerialClkDiagnosticSupported = FALSE;
static bool32 isAhbClkDiagnosticSupported = FALSE;
static SpmiBus_DebugTransactionType debugTrans[SPMI_BUS_DEBUG_MAX_TRANS]; /* Store last few transaction details */

/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/
static SpmiBus_ResultType SpmiBus_ChipInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    DALSYSPropertyVar PropVar;
    SpmiBus_ResultType spmiResult = SPMI_BUS_SUCCESS;

    do
    {
        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "this_owner_number", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            pDeviceInfo->chipConfig.uThisOwnerNumber = (uint32)PropVar.Val.dwVal;
            SPMI_LOG(INFO,
                "SpmiBus: SpmiBus_ChipInit : Read this_owner_number: %ld",
                pDeviceInfo->chipConfig.uThisOwnerNumber);
        }
        else
        {
            spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiBus: SpmiBus_ChipInit : ***SPMI_FATAL_ERROR*** Unable to read this_owner_number from properties, Result: %d",
                spmiResult);
            break;
        }
    }while(0);
    return spmiResult;
}

static void SpmiBus_ChipDeInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    pDeviceInfo->chipConfig.uThisOwnerNumber = 0;
}

static SpmiBus_ResultType SpmiBus_SwInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    DALResult result;
    DALSYSPropertyVar PropVar;
    char *pszCoreBaseName;
    char *pszCoreClkName;
    uint32 uLoggingLevel;
    SpmiBus_ResultType spmiResult = SPMI_BUS_SUCCESS;

    do {

        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "logging_level", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            uLoggingLevel = (uint32)PropVar.Val.dwVal;
            SpmiLogs_SetLogLevel(uLoggingLevel);

            SPMI_LOG(INFO,
                "SpmiBus: SpmiBus_SwInit : Read logging_level: %ld",
                uLoggingLevel);
        }

        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "core_base_name", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            pszCoreBaseName = PropVar.Val.pszVal;
        }
        else
        {
            spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiBus: SpmiBus_SwInit : ***SPMI_FATAL_ERROR*** Unable to read core_base_name from properties, Result: %d",
                spmiResult);
            break;
        }

        /* Determine the HWIO base for the Spmi registers */
        result = DAL_HWIODeviceAttach(DALDEVICEID_HWIO, &hHWIO);
        if((DAL_SUCCESS == result) && (hHWIO))
        {
            result = DalDevice_Open(hHWIO, DAL_OPEN_SHARED);
            if(DAL_SUCCESS != result)
            {
                spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
                SPMI_LOG(FATAL_ERROR,
                    "SpmiBus: SpmiBus_SwInit : ***SPMI_FATAL_ERROR*** DalDevice_Open on HWIO failed with status %d",
                    result);
                break;
            }

            result = DalHWIO_MapRegion(hHWIO, pszCoreBaseName,
                (uint8**)&(pDeviceInfo->swConfig.puPmicArbSpmiVirtualAddress));
            if(DAL_SUCCESS != result)
            {
                spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
                SPMI_LOG(FATAL_ERROR,
                    "SpmiBus: SpmiBus_SwInit : ***SPMI_FATAL_ERROR*** DalHWIO_MapRegion on HWIO failed with status %d",
                    result);
                break;
            }
            result = DalDevice_Close(hHWIO);
            if(DAL_SUCCESS != result)
            {
                spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
                SPMI_LOG(WARNING,
                    "SpmiBus: SpmiBus_SwInit : ***SPMI_WARNING*** DalDevice_Close on HWIO failed with status %d",
                    result);
                break;
            }
        }
        else /* On some image DALHWIO is not present and thus we read physical base address from properties. */
        {
            if(DAL_SUCCESS == DALSYS_GetPropertyValue
                (hProp, //Prop handle
                "core_base_physical_address", //Prop Name String
                0, //Not interested in Prop Id
                &PropVar)) //Output
            {
                pDeviceInfo->swConfig.puPmicArbSpmiVirtualAddress = (uint64 *)PropVar.Val.dwVal;
            }
            else
            {
                spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
                SPMI_LOG(FATAL_ERROR,
                    "SpmiBus: SpmiBus_SwInit : ***SPMI_FATAL_ERROR*** Unable to read core_base_physical_address from properties, Result: %d",
                    spmiResult);
                break;
            }
        }
        if(0 == pDeviceInfo->swConfig.puPmicArbSpmiVirtualAddress)
        {
            spmiResult = SPMI_BUS_FAILURE_INIT_FAILED;
            SPMI_LOG(FATAL_ERROR,
                "SpmiBus: SpmiBus_SwInit : ***SPMI_FATAL_ERROR*** Mapped Virtual Address is NULL, Result: %d",
                spmiResult);
            break;
        }

        SPMI_LOG(INFO,
            "SpmiBus: SpmiBus_SwInit: Mapped Virtual Address is at: 0x%x",
            pDeviceInfo->swConfig.puPmicArbSpmiVirtualAddress);

        if(DAL_SUCCESS == DALSYS_GetPropertyValue
            (hProp, //Prop handle
            "polling_mode", //Prop Name String
            0, //Not interested in Prop Id
            &PropVar)) //Output
        {
            pDeviceInfo->swConfig.bPollingMode = (uint32)PropVar.Val.dwVal;
        }
        else
        {
            pDeviceInfo->swConfig.bPollingMode = 1; /* By default, set polling mode true */
        }
        SPMI_LOG(INFO,
            "SpmiBus: SpmiBus_SwInit: Polling mode: %d",
            pDeviceInfo->swConfig.bPollingMode);

        /* If DAL CLK driver is available, we will open a handle to it */
        result = DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &hClk);
        if((DAL_SUCCESS == result) && (hClk))
        {
            if(DAL_SUCCESS == DALSYS_GetPropertyValue
                (hProp, //Prop handle
                "spmi_ser_clk", //Prop Name String
                0, //Not interested in Prop Id
                &PropVar)) //Output
            {
                pszCoreClkName = PropVar.Val.pszVal;

                result = DalClock_GetClockId(hClk, pszCoreClkName, &spmiSerialClockId);
                if(DAL_SUCCESS == result)
                {
                    isSerialClkDiagnosticSupported = TRUE;
                }
            }

            if(DAL_SUCCESS == DALSYS_GetPropertyValue
                (hProp, //Prop handle
                "spmi_ahb_clk", //Prop Name String
                0, //Not interested in Prop Id
                &PropVar)) //Output
            {
                pszCoreClkName = PropVar.Val.pszVal;

                result = DalClock_GetClockId(hClk, pszCoreClkName, &spmiAhbClockId);
                if(DAL_SUCCESS == result)
                {
                    isAhbClkDiagnosticSupported = TRUE;
                }
            }

        }

        SPMI_LOG(INFO,
            "SpmiBus: SpmiBus_SwInit: Diagnostic status Serial: %d, AHB: %d",
            isSerialClkDiagnosticSupported, 
            isAhbClkDiagnosticSupported);

        /* Now init the debug trans pointers */
		pDeviceInfo->pDebugTrans = &debugTrans[0];
        pDeviceInfo->uNumDebugTransEntries = SPMI_BUS_DEBUG_MAX_TRANS;

        /* Now init the Spmi Profile driver */
        SpmiProfile_Init();

    }while(0);

    return spmiResult;
}

static void SpmiBus_SwDeInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    if(hHWIO)
    {
        DalDevice_Open(hHWIO, DAL_OPEN_SHARED);
        DalHWIO_UnMapRegion(hHWIO,
            (uint8*) pDeviceInfo->swConfig.puPmicArbSpmiVirtualAddress);
        DalDevice_Close(hHWIO);
        DAL_DeviceDetach(hHWIO);
        hHWIO = 0;
    }

    if(hClk)
    {
        DAL_DeviceDetach(hClk);
        hClk = 0;
    }

    /* Now Deinit the Spmi Profile driver */
    SpmiProfile_Deinit();

    return;
}

/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/
SpmiBus_ResultType SpmiBus_OsInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    SpmiBus_ResultType spmiResult;

    /*
    This function shall fill in following fields of the DeviceInfo strcuture
    1. chipConfig -> uThisOwnerNumber
    2. swConfig -> puSpmiBusVirtualAddress
    3. swConfig -> bPollingMode
    4. Set the logging level using SpmiLogs_SetLogLevel
    5. Fill in debug transaction buffer and size 
       pDeviceInfo->pDebugTrans
       pDeviceInfo->uNumDebugTransEntries 
   */

    if( DAL_SUCCESS != DALSYS_GetDALPropertyHandleStr("DALDEVICEID_SPMI_DEVICE",
        hProp))
    {
        SPMI_LOG(FATAL_ERROR,
            "SpmiBus: SpmiBus_OsInit : ***SPMI_FATAL_ERROR*** Unable to get property handle, Result: %d",
            SPMI_BUS_FAILURE_INIT_FAILED);
        return SPMI_BUS_FAILURE_INIT_FAILED;
    }

    /* Call SW init function */
    if (SPMI_BUS_SUCCESS != (spmiResult = SpmiBus_SwInit(pDeviceInfo)))
    {
        return spmiResult;
    }

    /* Call Chip Init function */
    if (SPMI_BUS_SUCCESS != (spmiResult = SpmiBus_ChipInit(pDeviceInfo)))
    {
        return spmiResult;
    }

    return SPMI_BUS_SUCCESS;
}

void SpmiBus_OsDeInit(SpmiBus_DeviceInfoType *pDeviceInfo)
{
    /* Call Chip De-init function */
    SpmiBus_ChipDeInit(pDeviceInfo);

    /* Call SW De-init function */
    SpmiBus_SwDeInit(pDeviceInfo);

    return;
}

void SpmiBus_OsBusyWait(uint32 uUsec)
{
    DALSYS_BusyWait(uUsec);

    return;
}

SpmiBus_ClkDiagnosticType SpmiBus_OsGetSerialClkState(void)
{
    if(isSerialClkDiagnosticSupported)
    {
        if(DalClock_IsClockOn(hClk, spmiSerialClockId))
        {
            return SPMI_CLK_IS_ON;
        }
        else
        {
            return SPMI_CLK_IS_OFF;
        }
    }
    else
    {
        return SPMI_CLK_DIAGNOSTIC_NOT_SUPPORTED;
    }
}

SpmiBus_ClkDiagnosticType SpmiBus_OsGetAhbClkState(void)
{
    if(isAhbClkDiagnosticSupported)
    {
        if(DalClock_IsClockOn(hClk, spmiAhbClockId))
        {
            return SPMI_CLK_IS_ON;
        }
        else
        {
            return SPMI_CLK_IS_OFF;
        }
    }
    else
    {
        return SPMI_CLK_DIAGNOSTIC_NOT_SUPPORTED;
    }
}

