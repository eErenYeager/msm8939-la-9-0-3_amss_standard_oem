﻿#ifndef __HAL_SPMI_HWIO_VERIFY_H__
#define __HAL_SPMI_HWIO_VERIFY_H__
/*=========================================================================*/
/**
@file: HalSpmiHwioVerify.h

@brief: This module does the compile time verifications of constants values 
        defined in HalSpmi.h against the actual HWIO
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/common/HalSpmiHwioVerify.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2011 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "PmicArbHWIO.h"
#include "HalSpmi.h"

/** SPMI Protocol IRQ status type */
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_DATA_ADDR_FRAME_PARITY_ERROR_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNSUPPORTED_COMMAND_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_BUS_REQUEST_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_MASTER_ARBITRATION_WIN_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNEXPECTED_SSC_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_PERIPH_IRQ_LOST_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_PERIH_IRQ_LOST_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_BMSK)
#error SPMI_HAL_ERROR
#endif
#if (__HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_MASK != HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_BMSK)
#error SPMI_HAL_ERROR
#endif


#endif /* __HAL_SPMI_HWIO_VERIFY_H__ */
