/*=========================================================================*/
/**
@file: HalPmicArb.c

@brief: This module provides the Hardware Abstraction Layer software to the 
Pmic Arbiter HW core. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/common/HalPmicArb.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
08/15/12   PS      Fixed bug in INI2 and OUTI2 macros 
07/13/12   PS      Fixed Warnings
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "HalPmicArb.h"
#include "HalSpmiInternal.h"
#include "HalPmicArbHwioVerify.h"
/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/
/**
* Initalize the Pmic Arb HAL. 
*  
* @param[in] hHandle - This PmicArb hHandle. 
*
* @param[out] ppszSbVersion - pointer to PmicArb version string.
*
* @return None
*
*/ 
void HAL_PmicArb_Init(HAL_PmicArb_HandleType hHandle, char **ppszSbVersion)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    ppszSbVersion = ppszSbVersion; /* Unreferenced Parameter */
}


/**
* Reset the PMIC Arbiter.  
*  
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
*/
void HAL_PmicArb_Reset(HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
}


/**
* Get the PMIC Arbiter HW version. 
* 
* @param[in] hHandle - This PmicArb hHandle.
* 
* @param[out] puMajor - Pmic Arb major version.
*
* @param[out] puMinor - Pmic Arb minor version.
*
* @param[out] puStep - Pmic Arb step version.
*
* @return None
* 
* @see None
* 
* @attention None
*/ 
void HAL_PmicArb_GetVersion(
    HAL_PmicArb_HandleType hHandle, 
    uint32 *puMajor, 
    uint32 *puMinor, 
    uint32 *puStep)
{ 
    uint32 uHwVersion =  SPMI_HWIO_IN(hHandle, PMIC_ARB_HW_VERSION);

    *puMajor = (uHwVersion & HWIO_PMIC_ARB_HW_VERSION_MAJOR_BMSK) >> 
        HWIO_PMIC_ARB_HW_VERSION_MAJOR_SHFT;
    *puMinor = (uHwVersion & HWIO_PMIC_ARB_HW_VERSION_MINOR_BMSK) >> 
        HWIO_PMIC_ARB_HW_VERSION_MINOR_SHFT;
    *puStep  = (uHwVersion & HWIO_PMIC_ARB_HW_VERSION_STEP_BMSK) >> 
        HWIO_PMIC_ARB_HW_VERSION_STEP_SHFT;
}

/**
* Enable dynamic clk gating for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_DynamicClkGatingDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_DynamicClkGatingEnable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_BMSK,
        0x1<<HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_SHFT);
}

/**
* Disable dynamic clk gating for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_DynamicClkGatingEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_DynamicClkGatingDisable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_BMSK, 
        0x0<<HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_SHFT);
}


/**
* Check if dynamic clk gating is enabled for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_DynamicClkGatingEnable(), 
*      HAL_PmicArb_DynamicClkGatingDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_DynamicClkGatingIsEnabled(HAL_PmicArb_HandleType hHandle)
{
    return ((
        SPMI_HWIO_INM(hHandle, PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_BMSK)) >> 
        HWIO_PMIC_ARB_MISC_CONFIG_CLK_GATE_EN_SHFT);
}

/**
* Enable transaction done interrupts globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_GlobalDoneIntDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_GlobalDoneIntEnable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_BMSK, 
        0x1<<HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_SHFT);
}


/**
* Disable transaction done interrupts globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_GlobalDoneIntEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_GlobalDoneIntDisable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_BMSK, 
        0x0<<HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_SHFT);
}


/**
* Check if transaction done interrupts is enabled globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_GlobalDoneIntDisable(), HAL_PmicArb_GlobalDoneIntEnable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_GlobalDoneIntIsEnabled(HAL_PmicArb_HandleType hHandle)
{
    return ((SPMI_HWIO_INM(hHandle, 
        PMIC_ARB_MISC_CONFIG, 
        HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_BMSK)) >> 
        HWIO_PMIC_ARB_MISC_CONFIG_DONE_IRQ_EN_SHFT);
}


/**
* Enable all PVC Intf globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcIntfEnable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_PVC_INTF_CTL, 
        HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_BMSK, 
        0x1<<HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_SHFT);
}


/**
* Disable all PVC Intf globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcIntfDisable(HAL_PmicArb_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_PVC_INTF_CTL, 
        HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_BMSK, 
        0x0<<HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_SHFT);
}

/**
* Check if all PVC Intf are enabled globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_PvcIntfEnable(), HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_PvcIntfIsEnabled(HAL_PmicArb_HandleType hHandle)
{
    return ((SPMI_HWIO_INM(hHandle, 
        PMIC_ARB_PVC_INTF_CTL, 
        HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_BMSK)) >> 
        HWIO_PMIC_ARB_PVC_INTF_CTL_PVC_INTF_EN_SHFT);
}

/**
* Get the PVC Intf status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return Bitmask of HAL_PmicArb_PvcIntfStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_PvcIntfStatusFieldMaskType HAL_PmicArb_PvcIntfGetStatus(
    HAL_PmicArb_HandleType hHandle)
{
    return (HAL_PmicArb_PvcIntfStatusFieldMaskType) 
        SPMI_HWIO_IN(hHandle, PMIC_ARB_PVC_INTF_STATUS);
}


/**
* Set the port priorities.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @param[in] uPriority - Priority value. Priority 0 is the highest priority, 
*            priority 1 is the next highest, and so on
*
* @return None
* 
* @see HAL_PmicArb_PortGetPriority()
* 
* @attention None
*/
void HAL_PmicArb_PortSetPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort, 
    uint32 uPriority)
{
    SPMI_HWIO_OUTI(hHandle, PMIC_ARB_PRIORITIESn, uPriority, ePort);
}

/**
* Get the port priority.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return Priority value. Priority 0 is the highest priority, priority 1
*         is the next highest, and so on. -1 return value indicates that
*         the port number was not found in the priority table
* 
* @see HAL_PmicArb_PortSetPriority()
* 
* @attention None
*/
int32 HAL_PmicArb_PortGetPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    uint32 uIndex;
    for(uIndex=0; uIndex<HAL_PmicArb_PortGetNumPortPriorities(hHandle); uIndex++)
    {
        if(	SPMI_HWIO_INI(hHandle, PMIC_ARB_PRIORITIESn, uIndex) == ePort)
        {
            return uIndex;
        }
    }
    return -1;
}

/**
* Enable PVC port. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return None
* 
* @see HAL_PmicArb_PvcPortDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortEnable(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    SPMI_HWIO_OUTMI(hHandle, 
        PMIC_ARB_PVC_PORTn_CTL, 
        ePort,
        HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_BMSK, 
        0x1<<HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_SHFT);
}


/**
* Disable PVC port. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return None
* 
* @see HAL_PmicArb_PvcPortEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortDisable(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    SPMI_HWIO_OUTMI(hHandle, 
        PMIC_ARB_PVC_PORTn_CTL, 
        ePort,
        HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_BMSK, 
        0x0<<HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_SHFT);
}

/**
* Check if a PVC port is enabled. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_PvcPortEnable(),HAL_PmicArb_PvcPortDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_PvcPortIsEnabled(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    return ((SPMI_HWIO_INMI(hHandle, 
        PMIC_ARB_PVC_PORTn_CTL, 
        ePort, 
        HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_BMSK)) >> 
        HWIO_PMIC_ARB_PVC_PORTn_CTL_PVC_PORT_EN_SHFT);
}


/**
* Set the PVC port's Spmi command access priority. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @param[in] eAccessPriority - Access priority value
*
* @return None
* 
* @see HAL_PmicArb_PvcPortGetSpmiAccessPriority()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortSetSpmiAccessPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort, 
    HAL_PmicArb_SpmiAccessPrioirtyType eAccessPriority)

{
    SPMI_HWIO_OUTMI(hHandle, 
        PMIC_ARB_PVC_PORTn_CTL, 
        ePort,
        HWIO_PMIC_ARB_PVC_PORTn_CTL_SPMI_PRIORITY_BMSK, 
        eAccessPriority<<HWIO_PMIC_ARB_PVC_PORTn_CTL_SPMI_PRIORITY_SHFT);
}


/**
* Get the PVC port spmi command access priority. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return Access priority value
* 
* @see HAL_PmicArb_PvcPortSetSpmiAccessPriority()
* 
* @attention None
*/ 
HAL_PmicArb_SpmiAccessPrioirtyType HAL_PmicArb_PvcPortGetSpmiAccessPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    return ((HAL_PmicArb_SpmiAccessPrioirtyType) (SPMI_HWIO_INMI(hHandle, 
        PMIC_ARB_PVC_PORTn_CTL, 
        ePort,
        HWIO_PMIC_ARB_PVC_PORTn_CTL_SPMI_PRIORITY_BMSK) >> 
        HWIO_PMIC_ARB_PVC_PORTn_CTL_SPMI_PRIORITY_SHFT));
}


/**
* Get the PVC Port status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @return Bitmask of HAL_PmicArb_PvcPortStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_PvcPortStatusFieldMaskType HAL_PmicArb_PvcPortGetStatus(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort)
{
    return (HAL_PmicArb_PvcPortStatusFieldMaskType) SPMI_HWIO_INI(hHandle, 
        PMIC_ARB_PVC_PORTn_STATUS, 
        ePort);
}


/**
* Set the PVC port allowed access address at an index.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @param[in] uIndex - Address index.
*
* @param[in] uAddr - Access address. 
*            [SPMI Address<4Bits> , Register Address<16bits>] 
*
* @return None
* 
* @see HAL_PmicArb_PvcPortGetAllowedAccessAddress
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortSetAllowedAccessAddress(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort, 
    uint32 uIndex, 
    uint32 uAddr)
{
    SPMI_HWIO_OUTI2(hHandle, PMIC_ARB_PVCn_ADDRm, ePort, uIndex, uAddr);
}

/**
* Get the PVC port allowed access address from an index.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] ePort - Port index.
*
* @param[in] uIndex - Address index.
*
* @return Access address. [SPMI Address<4Bits> , Register Address<16bits>] 
* 
* @see HAL_PmicArb_PvcPortSetAllowedAccessAddress
* 
* @attention None
*/ 
uint32 HAL_PmicArb_PvcPortGetAllowedAccessAddress(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType ePort, uint32 uIndex)
{
    return SPMI_HWIO_INI2(hHandle, PMIC_ARB_PVCn_ADDRm, ePort, uIndex);
}

/**
* Sets up and starts a SPMI command
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[in] uSlaveId - SPMI Slave id
*
* @param[in] eCommand - Command type
*
* @param[in] eCommandPriority - Prioirty with which the 
*            command is issued on the SPMI bus
*
* @param[in] uRegisterAddress - Register address. Number of bits of this 
*            register address is based on command type. Some commands may
*            not use the register address and in that case, this parameter 
*            is ignored
*
* @param[in] uByteCount - Number of bytes to read/write. Some non read/write
*            type command may ignore this parameter
*
* @return None
* 
* @see None
* 
* @attention - For Write type commands, caller needs to queue up the data 
*              first by calling HAL_PmicArb_SpmiWriteOutDataBuffer()
*/
void HAL_PmicArb_SpmiCommand(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum, 
    uint32 uSlaveId, 
    HAL_PmicArb_SpmiCommandType eCommand,
    HAL_PmicArb_SpmiAccessPrioirtyType eCommandPriority, 
    uint32 uRegisterAddress,
    uint32 uByteCount)
{
    SPMI_HWIO_OUTI(hHandle, PMIC_ARB_CHNLn_CMD, 
        uChannelNum, 
        ((uSlaveId << HWIO_PMIC_ARB_CHNLn_CMD_SID_SHFT) |
        (eCommand << HWIO_PMIC_ARB_CHNLn_CMD_OPCODE_SHFT) |
        (eCommandPriority << HWIO_PMIC_ARB_CHNLn_CMD_PRIORITY_SHFT) |
        (uRegisterAddress << HWIO_PMIC_ARB_CHNLn_CMD_ADDRESS_SHFT) |
        ((uByteCount - 1) << HWIO_PMIC_ARB_CHNLn_CMD_BYTE_CNT_SHFT)));
}

/**
* Write to Out data buffer for write type commands
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[in] pucData - Pointer to the write data array 
*
* @param[in] uByteCount - Number of bytes in the data array
*
* @return None
* 
* @see None
* 
* @attention - HAL_PmicArb_SpmiCommand() needs to be called subsequently to 
*              start the write command
*/
void HAL_PmicArb_SpmiWriteOutDataBuffer(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum, 
    uint8 *pucDataBuffer, 
    uint32 uByteCount)
{
    uint32 uWriteData[2] = {0,0};
    uint32 i;


    for(i=0; i<uByteCount; i++)
    {
        uWriteData[i/4] = 
            uWriteData[i/4] | 
            (((uint32) pucDataBuffer[i]) << ((i*8)%32));
    }

    if(sizeof(uint32) < uByteCount)
    {
        SPMI_HWIO_OUTI(hHandle, PMIC_ARB_CHNLn_WDATA1, uChannelNum, uWriteData[1]);
    }
    SPMI_HWIO_OUTI(hHandle, PMIC_ARB_CHNLn_WDATA0, uChannelNum, uWriteData[0]);
}


/**
* Reads from the In Data Buffer after read type command
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[out] pucData - Pointer to the in read data array 
*
* @param[in] uByteCount - Number of bytes in the data array
*
* @return None
* 
* @see None
* 
* @attention - For reads, HAL_PmicArb_SpmiCommand() needs 
*              to be called prior to this API
*/
void HAL_PmicArb_SpmiReadInDataBuffer(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum, 
    uint8 *pucDataBuffer, 
    uint32 uByteCount)
{
    uint32 uReadData[2];
    uint32 i;

    uReadData[0] = SPMI_HWIO_INI(hHandle, PMIC_ARB_CHNLn_RDATA0, uChannelNum);


    if(sizeof(uint32) < uByteCount)
    {
        uReadData[1] = SPMI_HWIO_INI(hHandle, PMIC_ARB_CHNLn_RDATA1, uChannelNum);
    }

    for(i=0; i<uByteCount; i++)
    {
        pucDataBuffer[i] = (uint8)(uReadData[i/4] >> ((i*8)%32));
    }
}


/**
* Enable the transaction done interrupt
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return None
* 
* @see HAL_PmicArb_ChannelTransDoneIntDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_ChannelTransDoneIntEnable(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum)
{
    SPMI_HWIO_OUTMI(hHandle, 
        PMIC_ARB_CHNLn_CONFIG, 
        uChannelNum, 
        HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_BMSK, 
        1<<HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_SHFT);
}

/**
* Disable the transaction done interrupt
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return None
* 
* @see HAL_PmicArb_ChannelTransDoneIntEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_ChannelTransDoneIntDisable(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum)
{
    SPMI_HWIO_OUTMI(hHandle, 
        PMIC_ARB_CHNLn_CONFIG, 
        uChannelNum, 
        HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_BMSK, 
        0<<HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_SHFT);
}
/**
* Check if  the transaction done interrupt is enabled
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_ChannelTransDoneIntEnable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_ChannelTransDoneIntIsEnabled(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum)
{
    return SPMI_HWIO_INMI(hHandle, 
        PMIC_ARB_CHNLn_CONFIG, 
        uChannelNum, 
        HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_BMSK) >> 
        HWIO_PMIC_ARB_CHNLn_CONFIG_IRQ_EN_SHFT;
}


/**
* Get the channel status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return Bitmask of HAL_PmicArb_ChannelStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_ChannelStatusFieldMaskType HAL_PmicArb_ChannelGetStatus(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum)
{
    return (HAL_PmicArb_ChannelStatusFieldMaskType) SPMI_HWIO_INI(hHandle, 
        PMIC_ARB_CHNLn_STATUS,
        uChannelNum);
}

/**
* Get total number of PVC ports supported by the controller
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPvcPorts
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PvcPortGetNumPorts(HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_PVC_PORTn_CTL_MAXn + 1;
}

/**
* Get total number of port priorities supported by the controller
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPortPriorities
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PortGetNumPortPriorities(HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_PRIORITIESn_MAXn + 1;
}

/**
* Get total number of Address Indexes supported per PVC port
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumAddrIndexes
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PvcPortGetNumAddrIndexes(HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_PVCn_ADDRm_MAXm + 1;
}


/**
* Get total number of supported owners 
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
* 
* @see None
* 
* @attention None
*/
uint32  HAL_PmicArb_ChannelGetNumOwners(HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_CHNLn_CMD_MAXn + 1;
}

/**
* Get max number of read write bytes per command supported in a channel 
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
* 
* @see None
* 
* @attention None
*/
uint32  HAL_PmicArb_ChannelGetMaxAllowedReadWriteBytes(
    HAL_PmicArb_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return (HWIO_PMIC_ARB_CHNLn_CMD_BYTE_CNT_BMSK >> 
        HWIO_PMIC_ARB_CHNLn_CMD_BYTE_CNT_SHFT) + 1;
}
