/*=========================================================================*/
/**
@file: HalSpmi.c

@brief: This module provides the Hardware Abstraction Layer interface to the
SPMI controller. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/common/HalSpmi.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
08/09/13   mq      Added support for different hw core versions
07/25/13   mq      Added API to query protocol status
04/25/13   PS      Added support for SPMI core version 1.1.0
12/03/12   PS      Fixed a cast bug in HAL_Spmi_CfgGetApidFromPpidUsingRadixMap
09/28/12   PS      Added Enable/Disable of Force Master Write On Error APIs
09/25/12   PS      Added Enable/Disable MWB APIs
08/28/12   PS      Added PPID to APID conversion API
08/14/12   PS      Fixed a bug with INI2 macro usage
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "HalSpmi.h"
#include "HalSpmiInternal.h"
#include "HalSpmiHwioVerify.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
#define SPMI_MAX_RADIX_TREE_STEPS 12

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Global Data Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Static Function Declarations and Definitions
* ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
* Externalized Function Definitions
* ----------------------------------------------------------------------*/

/**
* Initalize the Spmi HAL.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @param[out] ppszSbVersion - pointer to Spmi version string.
*
* @return None
*/
void HAL_Spmi_Init(HAL_Spmi_HandleType hHandle, char **ppszSbVersion)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    ppszSbVersion = ppszSbVersion; /* Unreferenced Parameter */
}

/**
* Reset the Spmi Controller.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @return None
*/
void HAL_Spmi_Reset(HAL_Spmi_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
}

/**
* Get the SPMI HW version.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @param[out] puMajor - Pmic Arb major version.
*
* @param[out] puMinor - Pmic Arb minor version.
*
* @param[out] puStep - Pmic Arb step version.
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_GetVersion(
    HAL_Spmi_HandleType hHandle,
    uint32 *puMajor,
    uint32 *puMinor,
    uint32 *puStep)
{
    uint32 uHwVersion = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_HW_VERSION);

    *puMajor = (uHwVersion & HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MAJOR_BMSK) >>
        HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MAJOR_SHFT;
    *puMinor = (uHwVersion & HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MINOR_BMSK) >>
        HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MINOR_SHFT;
    *puStep = (uHwVersion & HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_STEP_BMSK) >>
        HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_STEP_SHFT;
}

/**
* Get the SPMI protocol status.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @return The status flags
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_GetProtocolStatus(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS);
}

/**
* Set the owner of a Peripheral.
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number.
*
* @param[in] uOwner - Owner number.
*
* @return None
*
* @see HAL_Spmi_CfgGetPeripheralOwner()
*
* @attention None
*/
void HAL_Spmi_CfgSetPeripheralOwner(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph,
    uint32 uOwner)
{
    SPMI_HWIO_OUTMI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG,
        uPeriph,
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_BMSK,
        uOwner<<HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_SHFT);
}


/**
* Get the owner of a Peripheral.
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number.
*
* @return Owner number.
*
* @see HAL_Spmi_CfgSetPeripheralOwner()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetPeripheralOwner(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph)
{
    return SPMI_HWIO_INMI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG,
        uPeriph,
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_BMSK) >>
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_SHFT;
}

/**
* Set the owner of a Peripheral and also the reverse lookup entry 
* of APID to PPID mapping
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @param[in] uPpid - Physical Peripheral number.
*
* @param[in] uOwner - Owner number.
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_CfgSetPeripheralOwnerAndApid2PpidEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uApid,
    uint32 uPpid,
    uint32 uOwner)
{
    #ifdef HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK
    uint32 uValue =  SPMI_HWIO_INI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG, 
        uApid);

    uValue = uValue & 
        ~HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_BMSK & 
        ~HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK;

    uValue = uValue | 
        ((uPpid << HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_SHFT )& 
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK) |
        ((uOwner << HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_SHFT )& 
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_BMSK);

    SPMI_HWIO_OUTI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG,
        uApid,        
        uValue);
    #endif
}

/**
* Set the reverse lookup table entry of APID to PPID mapping. 
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @param[in] uPpid - Physical Peripheral number.
*
* @return None
*
* @see HAL_Spmi_CfgGetApid2PpidEntry()
*
* @attention None
*/
void HAL_Spmi_CfgSetApid2PpidEntry(
                                   HAL_Spmi_HandleType hHandle,
                                   uint32 uApid,
                                   uint32 uPpid)
{
    #ifdef HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK
    SPMI_HWIO_OUTMI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG,
        uApid,
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK,
        uPpid<<HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_SHFT);
    #endif
}

/**
* Get the reverse lookup table entry of APID to PPID mapping.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @return Physical Peripheral number, -1 on error.
*
* @see HAL_Spmi_CfgSetApid2PpidEntry()
*
* @attention None
*/
int32 HAL_Spmi_CfgGetApid2PpidEntry(
                                     HAL_Spmi_HandleType hHandle,
                                     uint32 uApid)
{
    #ifdef HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK
    return SPMI_HWIO_INMI(hHandle,
        PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG,
        uApid,
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_BMSK) >>
        HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_APID2PPID_SHFT;
    #else
    return -1;
    #endif
}

/**
* Enable the compare APID2PPID LUT logic.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @see HAL_Spmi_CfgApid2PpidCmprDisable()
*
* @attention None
*/
void HAL_Spmi_CfgApid2PpidCmprEnable(
                                     HAL_Spmi_HandleType hHandle)
{
    #ifdef HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_CMPR_EN_REG,        
        HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK,
        0x1<<HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_SHFT);
    #endif
}

/**
* Disable the compare APID2PPID LUT logic.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @see HAL_Spmi_CfgApid2PpidCmprEnable()
*
* @attention None
*/
void HAL_Spmi_CfgApid2PpidCmprDisable(
                                      HAL_Spmi_HandleType hHandle)
{
    #ifdef HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_CMPR_EN_REG,        
        HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_BMSK,
        0x0<<HWIO_PMIC_ARB_SPMI_CMPR_EN_REG_CMPR_ENABLE_SHFT);
    #endif
}

/**
* Set PPID(Physical Peripheral Id) to APID (Application Peripheral Id) mapping
* table entry at an index
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMappingTableIndex - Index into mapping table
*
* @param[in] uMappingEntry - Mapping table entry at index uMappingTableIndex
*
* @return None
*
* @see HAL_Spmi_CfgGetPpidToApidMappingTableEntry()
*
* @attention None
*/
void HAL_Spmi_CfgSetPpidToApidMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uMappingTableIndex,
    uint32 uMappingEntry)
{
    SPMI_HWIO_OUTI(hHandle,
        PMIC_ARB_SPMI_MAPPING_TABLE_REGk,
        uMappingTableIndex,
        uMappingEntry);
}

/**
* Get PPID(Physical Peripheral Id) to APID (Application Peripheral Id) mapping
* table entry at an index
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMappingTableIndex - Index into mapping table
*
* @return Mapping table entry at index uMappingTableIndex
*
* @see HAL_Spmi_CfgSetPpidToApidMappingTableEntry()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetPpidToApidMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uMappingTableIndex)
{
    return SPMI_HWIO_INI(hHandle,PMIC_ARB_SPMI_MAPPING_TABLE_REGk, uMappingTableIndex);
}


/**
* Get APID(Application Peripheral Id) from given PPID (Physical
* Peripheral Id) using the Radix Map provided
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uRadixMap - Pointer to Radix Map Table
*
* @param[in] uPpid - Physical peripheral id
*
* @return Valid APID or -1 on error
*
* @see None
*
* @attention None
*/
int32 HAL_Spmi_CfgGetApidFromPpidUsingRadixMap(
    HAL_Spmi_HandleType hHandle,
    uint32 *uRadixMap,
    uint32 uPpid)
{

    uint32 uSteps = 0;
    uint32 uEntryIndex = 0;
    uint32 uEntryValue;
    uint32 uBitIndex;
    uint32 uBitValue;
    int32 uBranchOrResult;
    uint32 uBranchOrResultFlag;

    hHandle = hHandle; /* Unreferenced Parameter */

    /* The longest possible radix tree (e.g. right hanging thread) requires
    exactly 12 nodes to be visited (each iteration visits one node) */
    while (uSteps < SPMI_MAX_RADIX_TREE_STEPS)
    {
        ++uSteps;

        uEntryValue = uRadixMap[uEntryIndex];

        /* Determine the action in current entry */
        uBitIndex = (uEntryValue & HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_BMSK) >>
            HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_SHFT;
        uBitValue = (uPpid & (1 << uBitIndex));

        if(uBitValue)
        {
            uBranchOrResult = ((uEntryValue &
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_BMSK) >>
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_SHFT);
            uBranchOrResultFlag = ((uEntryValue &
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_BMSK) >>
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_SHFT);
        }
        else
        {
            uBranchOrResult = ((uEntryValue &
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_BMSK) >>
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_SHFT);
            uBranchOrResultFlag = ((uEntryValue &
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_BMSK) >>
                HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_SHFT);
        }

        if(uBranchOrResultFlag) /* This is Branch */
        {
            uEntryIndex = uBranchOrResult;
        }
        else /* This is result */
        {
            return uBranchOrResult;
        }
    }

    /* If reaches here, we were not able to find the APID in the Map table */
    return -1;
}

/**
* Create mapping table entry
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uBitIndex - Bit Index
*
* @param[in] uBranchResult0Flag - Branch or Result Flag for bit value 0 
*
* @param[in] uBranchResult0 - Branch or Result target for bit value 0
*
* @param[in] uBranchResult1Flag - Branch or Result Flag for bit value 1 
*
* @param[in] uBranchResult1 - Branch or Result target for bit value 1
*
* @return Created mapping table entry
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgCreateMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uBitIndex,
    uint32 uBranchResult0Flag,
    uint32 uBranchResult0,
    uint32 uBranchResult1Flag,
    uint32 uBranchResult1)
{

    uint32 uEntry = ((uBitIndex <<  HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_SHFT) & 
        HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_BMSK) | 
        ((uBranchResult0Flag <<  HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_SHFT) & 
        HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_BMSK)|
        ((uBranchResult0 <<  HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_SHFT) & 
        HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_BMSK)|
        ((uBranchResult1Flag <<  HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_SHFT) & 
        HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_BMSK)|
        ((uBranchResult1 <<  HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_SHFT) & 
        HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_BMSK);

    return uEntry;
}

/**
* Set SPMI Master Id
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMid - Master Id [2 bits]
*
* @return None
*
* @see HAL_Spmi_CfgGetMasterId()
*
* @attention None
*/
void HAL_Spmi_CfgSetMasterId(HAL_Spmi_HandleType hHandle, uint32 uMasterId)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_MID_REG,HWIO_PMIC_ARB_SPMI_MID_REG_MID_BMSK,
        uMasterId<<HWIO_PMIC_ARB_SPMI_MID_REG_MID_SHFT);
}


/**
* Get SPMI Master Id
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Master Id [2 bits]
*
* @see HAL_Spmi_CfgSetMasterId()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetMasterId(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_INM(hHandle,PMIC_ARB_SPMI_MID_REG,HWIO_PMIC_ARB_SPMI_MID_REG_MID_BMSK) >>
        HWIO_PMIC_ARB_SPMI_MID_REG_MID_SHFT;
}

/**
* Enable the SPMI Bus Arbitration process
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgBusArbitrationProcessDisable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationProcessEnable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_CFG_REG,HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_BMSK,
        0x1<<HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_SHFT);
}

/**
* Disable the SPMI Bus Arbitration process
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgBusArbitrationProcessEnable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationProcessDisable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_CFG_REG,HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_BMSK,
        0x0<<HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_SHFT);
}


/**
* Check if SPMI Bus Arbitration process is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgBusArbitrationProcessEnable(),
* HAL_Spmi_CfgBusArbitrationProcessDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsBusArbitrationProcessEnabled(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_INM(hHandle,
        PMIC_ARB_SPMI_CFG_REG,
        HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_BMSK) >>
        HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_SHFT;
}

/**
* Enable the SPMI Bus Arbitration Bypass
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgArbiterBypassDisable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationBypassEnable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_CFG_REG,HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_BMSK,
        0x1<<HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_SHFT);
}


/**
* Disable the SPMI Bus Arbitration Bypass
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgArbiterBypassEnable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationBypassDisable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_CFG_REG,HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_BMSK,
        0x0<<HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_SHFT);
}


/**
* Check if SPMI Bus Arbitration is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgArbiterBypassEnable(),
* HAL_Spmi_CfgBusArbitrationBypassDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsBusArbitrationBypassEnabled(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_INM(hHandle,
        PMIC_ARB_SPMI_CFG_REG,
        HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_BMSK) >>
        HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_SHFT;
}

/**
* Enable the Force Master Write On Error
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorDisable()
*
* @attention None
*/
void HAL_Spmi_CfgForceMasterWriteOnErrorEnable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_CFG_REG,HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_BMSK,
        0x1<<HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_SHFT);
}


/**
* Disable the Force Master Write On Error
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorEnable()
*
* @attention None
*/
void HAL_Spmi_CfgForceMasterWriteOnErrorDisable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_CFG_REG,
        HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_BMSK,
        0x0<<HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_SHFT);
}


/**
* Check if Force Master Write On Error is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorEnable(),
* HAL_Spmi_CfgForceMasterWriteOnErrorDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsForceMasterWriteOnErrorEnabled(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_INM(hHandle,
        PMIC_ARB_SPMI_CFG_REG,
        HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_BMSK) >>
        HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_SHFT;
}


/**
* Enable the SPMI Security features
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgSecurityDisable()
*
* @attention None
*/
void HAL_Spmi_CfgSecurityEnable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_SEC_DISABLE_REG,
        HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK,
        0x0<<HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_SHFT);
}


/**
* Disable the SPMI Security features
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgSecurityEnable()
*
* @attention None
*/
void HAL_Spmi_CfgSecurityDisable(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_SEC_DISABLE_REG,
        HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK,
        0x1<<HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_SHFT);
}

/**
* Check if SPMI Security features are enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgSecurityEnable(),HAL_Spmi_CfgSecurityDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsSecurityEnabled(HAL_Spmi_HandleType hHandle)
{
    return ((SPMI_HWIO_INM(hHandle,
        PMIC_ARB_SPMI_SEC_DISABLE_REG,
        HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK) >>
        HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_SHFT) ? FALSE:TRUE);
}

/**
* Enable CGC Ctrl for SPMI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCEnMask - Bitmask of CGC to set.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgEnableCGCCtrl(HAL_Spmi_HandleType hHandle,
                               uint32 uCGCEnMask)
{
    uint32 uCGCEnValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_CGC_CTRL);
    uCGCEnValue = uCGCEnValue | uCGCEnMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_CGC_CTRL, uCGCEnValue);
}

/**
* Disable CGC Ctrl for SPMI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCClrMask - Bitmask of CGC to clear.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgDisableCGCCtrl(HAL_Spmi_HandleType hHandle,
                                uint32 uCGCClrMask)
{
    uint32 uCGCClrValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_CGC_CTRL);
    uCGCClrValue = uCGCClrValue & ~uCGCClrMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_CGC_CTRL, uCGCClrValue);
}

/**
* Get the SPMI Protocol IRQ status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Bitmask of HAL_Spmi_CfgProtocolIrqStatusType enumeration
*
* @see None
*
* @attention None
*/
HAL_Spmi_CfgProtocolIrqStatusType HAL_Spmi_CfgGetProtocolIrqStatus(
    HAL_Spmi_HandleType hHandle)
{
    return (HAL_Spmi_CfgProtocolIrqStatusType)SPMI_HWIO_IN(hHandle,
        PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS);
}



/**
* Enable the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to enable
*
* @return None
*
* @see HAL_Spmi_CfgProtocolIrqDisable()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqEnable(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq)
{
    SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET, uProtocolIrq);
}

/**
* Disable the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to disable
*
* @return None
*
* @see HAL_Spmi_CfgProtocolIrqEnable()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqDisable(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq)
{
    SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR, uProtocolIrq);
}

/**
* Get all the SPMI Protocol IRQs which are enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return BitMask of all the IRQs which are enabled
*
* @see HAL_Spmi_CfgProtocolIrqEnable()
* HAL_Spmi_CfgProtocolIrqDisable()
*
* @attention None
*/
uint32 HAL_Spmi_CfgProtocolIrqGetEnabled(
    HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_IN(hHandle,PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE);
}


/**
* Clears the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to clear
*
* @return None
*
* @see HAL_Spmi_CfgGetProtocolIrqStatus()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqClear(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq)
{
    SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR, uProtocolIrq);
}


/**
* Get all the Peripheral accumulated Interrupt status for an owner
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uOwner - Owner number
*
* @param[out] puIntrStatus - Pointer to an array of 8 words which will hold
* the the accumulated IntrStatus
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_PicGetPeriphAccumulatedIntrStatus(
    HAL_Spmi_HandleType hHandle,
    uint32 uOwner,
    uint32 *puIntrStatus)
{
    uint32 uIndex;
    for(uIndex = 0; uIndex<HAL_Spmi_CfgGetNumPeriphs(hHandle)/32;uIndex++)
    {
        puIntrStatus[uIndex]= SPMI_HWIO_INI2(
            hHandle,
            PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn,
            uOwner,
            uIndex);
    }

}

/**
* Check if a peripheral accumulated Interrupt is set for an
* owner
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uOwner - Owner number
*
* @param[in] uPeriph - Peripheral number
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see None
*
* @attention None
*/
bool32 HAL_Spmi_PicIsPeriphAccumulatedIntrSet(
    HAL_Spmi_HandleType hHandle,
    uint32 uOwner,
    uint32 uPeriph)
{
    return ((SPMI_HWIO_INI2(hHandle,
        PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn,
        uOwner,
        uPeriph/32) &
        (0x1<<(uPeriph%32)))?TRUE:FALSE);
}

/**
* Enable a Peripheral accumulated Interrupt
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return None
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrDisable()
*
* @attention None
*/
void HAL_Spmi_PicPeriphAccumulatedIntrEnable(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph)

{
    SPMI_HWIO_OUTI(hHandle,PMIC_ARB_SPMI_PIC_ACC_ENABLEn, uPeriph, 1);
}


/**
* Disable a Peripheral accumulated Interrupt
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return None
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrEnable()
*
* @attention None
*/
void HAL_Spmi_PicPeriphAccumulatedIntrDisable(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph)
{
    SPMI_HWIO_OUTI(hHandle,PMIC_ARB_SPMI_PIC_ACC_ENABLEn, uPeriph, 0);
}


/**
* Check if a Peripheral accumulated Interrupt is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrEnable(),
* HAL_Spmi_PicPeriphAccumulatedIntrDisable()
*
* @attention None
*/
bool32 HAL_Spmi_PicIsPeriphAccumulatedIntrEnabled(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph)
{
    return SPMI_HWIO_INI(hHandle,PMIC_ARB_SPMI_PIC_ACC_ENABLEn, uPeriph);
}


/**
* Get the peripheral extended Intr status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return Bitmask indicating status of Peripheral extended status register
*
* @see HAL_Spmi_PicPeriphIntrClear()
*
* @attention None
*/
uint32 HAL_Spmi_PicGetPeriphIntrStatus(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph)
{
    return SPMI_HWIO_INI(hHandle,PMIC_ARB_SPMI_PIC_IRQ_STATUSn, uPeriph);
}


/**
* Clear the peripheral extended Intr status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @param[in] uPeriphIntrClearMask - Bitmask to clear the Peripheral Intr status
* bits
*
* @return None
*
* @see HAL_Spmi_PicPeriphIntrClear()
*
* @attention None
*/
void HAL_Spmi_PicPeriphIntrClear(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph,
    uint32 uPeriphIntrClearMask)
{
    SPMI_HWIO_OUTI(hHandle,PMIC_ARB_SPMI_PIC_IRQ_CLEARn, uPeriph, uPeriphIntrClearMask);
}


/**
* Configure the GENI configuration
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] pGeniCfg - Index of GENI_CFG register
*
* @param[in] uGeniCfgVal - Configuration value
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfg(HAL_Spmi_HandleType hHandle,
    uint32 uGeniCfgIndex,
    uint32 uGeniCfgVal)
{
    switch(uGeniCfgIndex)
    {
    case 0:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG0, uGeniCfgVal);
        break;
    case 1:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG1, uGeniCfgVal);
        break;
    case 2:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG2, uGeniCfgVal);
        break;
    case 3:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG3, uGeniCfgVal);
        break;
    case 4:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG4, uGeniCfgVal);
        break;
    case 5:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG5, uGeniCfgVal);
        break;
    case 6:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG6, uGeniCfgVal);
        break;
    case 7:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG7, uGeniCfgVal);
        break;
    case 8:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG8, uGeniCfgVal);
        break;
    case 9:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG9, uGeniCfgVal);
        break;
    case 10:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG10, uGeniCfgVal);
        break;
    case 11:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG11, uGeniCfgVal);
        break;
    case 12:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG12, uGeniCfgVal);
        break;
    case 13:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG13, uGeniCfgVal);
        break;
    case 14:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG14, uGeniCfgVal);
        break;
    case 15:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG15, uGeniCfgVal);
        break;
    case 16:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG16, uGeniCfgVal);
        break;
    case 17:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG17, uGeniCfgVal);
        break;
    case 18:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG18, uGeniCfgVal);
        break;
    
    #ifdef HWIO_PMIC_ARB_SPMI_GENI_CFG_REG30_ADDR
    case 19:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG19, uGeniCfgVal);
        break;
    case 20:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG20, uGeniCfgVal);
        break;
    case 21:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG21, uGeniCfgVal);
        break;
    case 22:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG22, uGeniCfgVal);
        break;
    case 23:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG23, uGeniCfgVal);
        break;
    case 24:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG24, uGeniCfgVal);
        break;
    case 25:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG25, uGeniCfgVal);
        break;
    case 26:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG26, uGeniCfgVal);
        break;
    case 27:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG27, uGeniCfgVal);
        break;
    case 28:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG28, uGeniCfgVal);
        break;
    case 29:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG29, uGeniCfgVal);
        break;
    case 30:
        SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_CFG_REG30, uGeniCfgVal);
        break;
    #endif
    }
}

/**
* Configure the GENI RAM
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uGeniRamStartIndex - Index of GENI_RAM register
to start configuring
*
* @param[in] uGeniCfgVal - Ram Configuration value array
*
* @param[in] uNumWords - Number of values to program in RAM
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgRam(HAL_Spmi_HandleType hHandle,
    uint32 uGeniRamStartIndex,
    const uint32 *uGeniRamVal,
    uint32 uNumWords)
{
    uint32 i=0;
    for(i=0; i<uNumWords; i++)
    {
        SPMI_HWIO_OUTI(hHandle,
            PMIC_ARB_SPMI_GENI_CFG_RAMn,
            uGeniRamStartIndex + i,
            uGeniRamVal[i]);
    }
}

/**
* Set FORCE_DEFAULT for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetForceDefault(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG, 0x1);
}

/**
* Clear FORCE_DEFAULT for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgClearForceDefault(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUT(hHandle,PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG, 0x0);
}

/**
* Set value for SER_CLK_SEL for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSerialClkSelect - Value for SER_CLK_SEL.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetSerialClkSelect(HAL_Spmi_HandleType hHandle,
    uint32 uSerialClkSelect)
{
    SPMI_HWIO_OUTM(hHandle,PMIC_ARB_SPMI_GENI_CLK_CTRL,
        HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_BMSK,
        uSerialClkSelect<<HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_SHFT);
}

/**
* Get value for SER_CLK_SEL for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Value for SER_CLK_SEL
*
* @see None()
*
* @attention None
*/
uint32 HAL_Spmi_GeniCfgGetSerialClkSelect(HAL_Spmi_HandleType hHandle)
{
    return SPMI_HWIO_INM(hHandle, PMIC_ARB_SPMI_GENI_CLK_CTRL,
        HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_BMSK) >>
        HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_SHFT;
}

/**
* Enable Output Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSOEnMask - Bitmask of SOEn to enable.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgEnableOutputCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uSOEnMask)
{
    uint32 uSOEnValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_GENI_OUTPUT_CTRL);
    uSOEnValue = uSOEnValue | uSOEnMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_GENI_OUTPUT_CTRL, uSOEnValue);
}

/**
* Disable Output Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSOEnMask - Bitmask of SOEn to disable.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgDisbleOutputCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uSOEnMask)
{
    uint32 uSOEnValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_GENI_OUTPUT_CTRL);
    uSOEnValue = uSOEnValue & ~uSOEnMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_GENI_OUTPUT_CTRL, uSOEnValue);
}

/**
* Enable CGC Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCEnMask - Bitmask of CGC to set.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgEnableCGCCtrl(HAL_Spmi_HandleType hHandle,
                                   uint32 uCGCEnMask)
{
    uint32 uCGCEnValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_GENI_CGC_CTRL);
    uCGCEnValue = uCGCEnValue | uCGCEnMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_GENI_CGC_CTRL, uCGCEnValue);
}

/**
* Disable CGC Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCClrMask - Bitmask of CGC to clear.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgDisableCGCCtrl(HAL_Spmi_HandleType hHandle,
                                    uint32 uCGCClrMask)
{
    uint32 uCGCClrValue = SPMI_HWIO_IN(hHandle, PMIC_ARB_SPMI_GENI_CGC_CTRL);
    uCGCClrValue = uCGCClrValue & ~uCGCClrMask;

    SPMI_HWIO_OUT(hHandle, PMIC_ARB_SPMI_GENI_CGC_CTRL, uCGCClrValue);
}

/**
* Program the ProgROMCtrl 
* Note: This is only avaiable 1.1.0 onwards
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProgRomCtrlVal - Value to program
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetProgRomCtrl(HAL_Spmi_HandleType hHandle,
                                    uint32 uProgRomCtrlVal)
{
    #ifdef HWIO_PMIC_ARB_SPMI_GENI_PROG_ROM_CTRL_REG_ADDR
    SPMI_HWIO_OUT(hHandle, 
        PMIC_ARB_SPMI_GENI_PROG_ROM_CTRL_REG, 
        uProgRomCtrlVal);
    #endif
}


/**
* Configure Arbiter Ctrl values
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uArbiterCtlrValue - Value to be written to Arbiter
* Ctrl field of SPMI_CFG register.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgSetArbiterCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uArbiterCtlrValue)
{
    SPMI_HWIO_OUTM(hHandle,
        PMIC_ARB_SPMI_CFG_REG,
        HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_CTRL_BMSK,
        uArbiterCtlrValue<<HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_CTRL_SHFT);
}

/**
* Enable MWB
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
* 
* @see None()
* 
* @attention HAL_Spmi_CfgDisableMWB
*/ 
void HAL_Spmi_CfgEnableMWB(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_SPMI_MWB_ENABLE_REG, 
        HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_BMSK, 
        1<<HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_SHFT);
}

/**
* Disable MWB
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
* 
* @see None()
* 
* @attention HAL_Spmi_CfgEnableMWB
*/ 
void HAL_Spmi_CfgDisableMWB(HAL_Spmi_HandleType hHandle)
{
    SPMI_HWIO_OUTM(hHandle, 
        PMIC_ARB_SPMI_MWB_ENABLE_REG, 
        HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_BMSK, 
        0<<HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_SHFT);
}

/**
* Get total number of owners supported by the controller
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumOwners(HAL_Spmi_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_MAXm + 1;
}

/**
* Get total number of peripherals supported by the controller
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPeriphs
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumPeriphs(HAL_Spmi_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_MAXm + 1;
}

/**
* Get total number of mapping table entries
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumMappingTableEntries
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumMappingTableEntries(HAL_Spmi_HandleType hHandle)
{
    hHandle = hHandle; /* Unreferenced Parameter */
    return HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_MAXk + 1;
}

/**
* Clears registers that don't adhere to their correct POR value of zero.
*
* @param[in] hHandle - This SPMI hHandle.
*/
void HAL_Spmi_ClearSpmiMemory(HAL_Spmi_HandleType hHandle)
{
    uint32 i;
    
    for(i = 0; i <= HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_MAXn; i++) {
        SPMI_HWIO_OUTI(hHandle, PMIC_ARB_SPMI_PIC_ACC_ENABLEn, i, 0);
    }
    
    for(i = 0; i <= HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_MAXm; i++) {
        SPMI_HWIO_OUTI(hHandle, PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG, i, 0);
    }
}

