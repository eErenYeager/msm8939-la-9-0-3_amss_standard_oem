#ifndef HAL_SPMI_INTERNAL_H
#define HAL_SPMI_INTERNAL_H
/*
============================================================================

FILE:          HaLSpmiInternal.h

DESCRIPTION:   HAL SPMI Internal header file.


===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================	

============================================================================
EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/common/HalSpmiInternal.h#1 $

when        who  what, where, why
----------  ---  -----------------------------------------------------------
2012-02-24  ps   Initial version.
============================================================================
*/

/* ----------------------------------------------------------------------- 
** Includes
** ----------------------------------------------------------------------- */ 

#include "HALcomdef.h"  /* uint32*/
#include "HALhwio.h"    /* __inpdw() , __outpdw() macros */
#include "PmicArbHWIO.h"   /* HWIO registers address , uMask and shift */

/* ----------------------------------------------------------------------- 
** Constants  SB
** ----------------------------------------------------------------------- */ 
#define SPMI_ADDR(hwiosym)        HWIO_##hwiosym##_ADDR
#define SPMI_ADDRn(hwiosym,index) HWIO_##hwiosym##_ADDR(index)
#define SPMI_ADDRnm(hwiosym,n,m)  HWIO_##hwiosym##_ADDR(n,m)
#define SPMI_BSMSK(hwiosym,field) HWIO_##hwiosym##_##field##_BMSK
#define SPMI_SHFT(hwiosym,field)  HWIO_##hwiosym##_##field##_SHFT
#define SPMI_MAXn(hwiosym)        HWIO_##hwiosym##_MAXn

#define SPMI_HWIO_IN(base, hwiosym)                      __inpdw(base + SPMI_ADDR(hwiosym))                          
#define SPMI_HWIO_OUT(base, hwiosym, val)                __outpdw(base + SPMI_ADDR(hwiosym), (val))         
#define SPMI_HWIO_INI(base, hwiosym, index)              __inpdw(base + SPMI_ADDRn(hwiosym,index))
#define SPMI_HWIO_OUTI(base, hwiosym, index, val)        __outpdw(base + SPMI_ADDRn(hwiosym,index), (val))
#define SPMI_HWIO_OUTI2(base, hwiosym ,n, m, val)        __outpdw(base + SPMI_ADDRnm(hwiosym,n,m), (val))  
#define SPMI_HWIO_INI2(base, hwiosym ,n, m)              __inpdw(base + SPMI_ADDRnm(hwiosym,n,m))  

#define SPMI_HWIO_INM(base, hwiosym, mask)               ( SPMI_HWIO_IN((base), hwiosym) & (mask))
#define SPMI_HWIO_OUTM(base, hwiosym, mask, val)         SPMI_HWIO_OUT((base), hwiosym, ((SPMI_HWIO_IN((base), hwiosym) & (~(mask))) | ((val) & (mask))))
#define SPMI_HWIO_INMI(base, hwiosym, index, mask)       ( SPMI_HWIO_INI(base, hwiosym, index) & (mask))
#define SPMI_HWIO_OUTMI(base, hwiosym, index, mask, val) SPMI_HWIO_OUTI(base, hwiosym, index, ((SPMI_HWIO_INI(base, hwiosym, index) & (~(mask))) | ((val) & (mask))))

#define SPMI_HWIO_INF(base, hwiosym, field)               ( (SPMI_HWIO_IN((base), hwiosym) & SPMI_BSMSK(hwiosym,field))>>SPMI_SHFT(hwiosym,field))
#define SPMI_HWIO_OUTF(base, hwiosym, field, val)         SPMI_HWIO_OUT((base), hwiosym, ((SPMI_HWIO_IN((base), hwiosym) & (~(SPMI_BSMSK(hwiosym,field)))) | ((val<<SPMI_SHFT(hwiosym,field)) & (SPMI_BSMSK(hwiosym,field)))))
#define SPMI_HWIO_INFI(base, hwiosym, index, field)       ( (SPMI_HWIO_INI((base), hwiosym , index) & SPMI_BSMSK(hwiosym,field))>>SPMI_SHFT(hwiosym,field))
#define SPMI_HWIO_OUTFI(base, hwiosym, index, field, val) SPMI_HWIO_OUTI((base), hwiosym, index, ((SPMI_HWIO_INI((base), hwiosym,index) & (~(SPMI_BSMSK(hwiosym,field)))) | ((val<<SPMI_SHFT(hwiosym,field)) & (SPMI_BSMSK(hwiosym,field)))))


#endif  /* HAL_SPMI_INTERNAL_H */
