﻿#ifndef __HAL_PMIC_ARB_HWIO_VERIFY_H__
#define __HAL_PMIC_ARB_HWIO_VERIFY_H__
/*=========================================================================*/
/**
@file: HalPmicArbHwioVerify.h

@brief: This module does the compile time verifications of constants values 
defined in HalPmicArb.h against the actual HWIO
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/common/HalPmicArbHwioVerify.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/
#include "PmicArbHWIO.h"
#include "HalPmicArb.h"

/** PVC interface status type */
#if (__HAL_PMIC_ARB_PVC_INTF_STATUS_BUSY_MASK != HWIO_PMIC_ARB_PVC_INTF_STATUS_PVC_INTF_BUSY_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif

/** SPMI access priority type */
#if 0
#if (__HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_LOW != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_HIGH != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#endif

/** PVC port status type */
#if (__HAL_PMIC_ARB_PVC_PORT_STATUS_BUSY_MASK != HWIO_PMIC_ARB_PVC_PORTn_STATUS_PVC_PORT_BUSY_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_PVC_PORT_STATUS_FAILURE_MASK != HWIO_PMIC_ARB_PVC_PORTn_STATUS_PVC_PORT_FAILURE_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif

/** Channel status type */
#if (__HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK != HWIO_PMIC_ARB_CHNLn_STATUS_DONE_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_CHANNEL_STATUS_FAILURE_MASK != HWIO_PMIC_ARB_CHNLn_STATUS_FAILURE_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_CHANNEL_STATUS_DENIED_MASK != HWIO_PMIC_ARB_CHNLn_STATUS_DENIED_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK != HWIO_PMIC_ARB_CHNLn_STATUS_DROPPED_BMSK)
#error PMIC_ARB_HAL_ERROR
#endif

/** SPMI command type */
#if 0
#if (__HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG_DELAYED != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_READ != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE_LONG != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_WRITE != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_ZERO_WRITE != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_MASTER_READ != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_MASTER_WRITE != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_RESET != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_SLEEP != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_SHUTDOWN != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#if (__HAL_PMIC_ARB_SPMI_COMMAND_WAKEUP != FVALS)
#error PMIC_ARB_HAL_ERROR
#endif
#endif

#endif /* __HAL_PMIC_ARB_HWIO_VERIFY_H__ */
