#ifndef __HAL_PMIC_ARB_H__
#define __HAL_PMIC_ARB_H__
/*=========================================================================*/
/**
@file: HalPmicArb.h

@brief: This module provides the Hardware Abstraction Layer interface to the
Pmic Arbiter HW core. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/inc/HalPmicArb.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "HALhwio.h"
#include "HALcomdef.h"      /* uint32 and other basic types */

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/** HAL Pmic Arb version strings */
#define HAL_PMIC_ARB_VERSION_STRING      "HAL_PMIC_ARB_VERSION_1.0"

/** HAL Pmic Arb Invalid handle */
#define HAL_PMIC_ARB_HANDLE_INVALID         ((HAL_PmicArb_HandleType)0)

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/** The Pmic Arbiter Handle holds the virtual base address of
*  the PMIC Arbiter */
typedef unsigned long HAL_PmicArb_HandleType;

/** PVC interface status type */
#define __HAL_PMIC_ARB_PVC_INTF_STATUS_BUSY_MASK 0x1

typedef enum
{
    HAL_PMIC_ARB_PVC_INTF_STATUS_BUSY_MASK = __HAL_PMIC_ARB_PVC_INTF_STATUS_BUSY_MASK,
    HAL_ENUM_32BITS(PMIC_ARB_PVC_INTF_STATUS)
}HAL_PmicArb_PvcIntfStatusFieldMaskType;


/** SPMI access priority type */
#define __HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_LOW 0x0
#define __HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_HIGH 0x1

typedef enum
{
    HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_LOW = __HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_LOW,
    HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_HIGH = __HAL_PMIC_ARB_SPMI_ACCESS_PRIORITY_HIGH,

    HAL_ENUM_32BITS(PMIC_ARB_SPMI_ACCESS_PRIORITY)
}HAL_PmicArb_SpmiAccessPrioirtyType;   

/** PVC port status type */
#define __HAL_PMIC_ARB_PVC_PORT_STATUS_BUSY_MASK  0x1
#define __HAL_PMIC_ARB_PVC_PORT_STATUS_FAILURE_MASK 0x2

typedef enum
{
    HAL_PMIC_ARB_PVC_PORT_STATUS_BUSY_MASK = __HAL_PMIC_ARB_PVC_PORT_STATUS_BUSY_MASK,
    HAL_PMIC_ARB_PVC_PORT_STATUS_FAILURE_MASK = __HAL_PMIC_ARB_PVC_PORT_STATUS_FAILURE_MASK,

    HAL_ENUM_32BITS(HAL_PMIC_ARB_PVC_PORT_STATUS)
}HAL_PmicArb_PvcPortStatusFieldMaskType;

/** Port Type */
typedef uint32 HAL_PmicArb_PortType;

/** Channel status type */
#define __HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK     0x1
#define __HAL_PMIC_ARB_CHANNEL_STATUS_FAILURE_MASK  0x2
#define __HAL_PMIC_ARB_CHANNEL_STATUS_DENIED_MASK   0x4
#define __HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK  0x8

typedef enum
{
    HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK = __HAL_PMIC_ARB_CHANNEL_STATUS_DONE_MASK,
    HAL_PMIC_ARB_CHANNEL_STATUS_FAILURE_MASK = __HAL_PMIC_ARB_CHANNEL_STATUS_FAILURE_MASK,
    HAL_PMIC_ARB_CHANNEL_STATUS_DENIED_MASK =__HAL_PMIC_ARB_CHANNEL_STATUS_DENIED_MASK,
    HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK = __HAL_PMIC_ARB_CHANNEL_STATUS_DROPPED_MASK,

    HAL_ENUM_32BITS(HAL_PMIC_ARB_CHANNEL_STATUS)
}HAL_PmicArb_ChannelStatusFieldMaskType;


/* SPMI command type */
#define __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG 0x1
#define __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG_DELAYED 0x2
#define __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ 0x9
#define __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_READ 0xB
#define __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE_LONG 0x0
#define __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE  0x8
#define __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_WRITE 0xA
#define __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_ZERO_WRITE 0xC
#define __HAL_PMIC_ARB_SPMI_COMMAND_MASTER_READ  0xF
#define __HAL_PMIC_ARB_SPMI_COMMAND_MASTER_WRITE  0xE
#define __HAL_PMIC_ARB_SPMI_COMMAND_RESET 0x4
#define __HAL_PMIC_ARB_SPMI_COMMAND_SLEEP  0X5
#define __HAL_PMIC_ARB_SPMI_COMMAND_SHUTDOWN  0x6
#define __HAL_PMIC_ARB_SPMI_COMMAND_WAKEUP  0x7

typedef enum
{

    HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG = __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG,
    HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG_DELAYED = __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ_LONG_DELAYED,	
    HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ = __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_READ,
    HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_READ = __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_READ,
    HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE_LONG = __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE_LONG,
    HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE = __HAL_PMIC_ARB_SPMI_COMMAND_EXTENDED_REGISTER_WRITE,
    HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_WRITE = __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_WRITE,
    HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_ZERO_WRITE = __HAL_PMIC_ARB_SPMI_COMMAND_REGISTER_ZERO_WRITE,
    HAL_PMIC_ARB_SPMI_COMMAND_MASTER_READ = __HAL_PMIC_ARB_SPMI_COMMAND_MASTER_READ,
    HAL_PMIC_ARB_SPMI_COMMAND_MASTER_WRITE = __HAL_PMIC_ARB_SPMI_COMMAND_MASTER_WRITE,
    HAL_PMIC_ARB_SPMI_COMMAND_RESET = __HAL_PMIC_ARB_SPMI_COMMAND_RESET,
    HAL_PMIC_ARB_SPMI_COMMAND_SLEEP = __HAL_PMIC_ARB_SPMI_COMMAND_SLEEP,
    HAL_PMIC_ARB_SPMI_COMMAND_SHUTDOWN = __HAL_PMIC_ARB_SPMI_COMMAND_SHUTDOWN,
    HAL_PMIC_ARB_SPMI_COMMAND_WAKEUP = __HAL_PMIC_ARB_SPMI_COMMAND_WAKEUP,

    HAL_ENUM_32BITS(HAL_PMIC_ARB_SPMI_COMMAND)
}HAL_PmicArb_SpmiCommandType;


/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/

/** @name  Mandatory APIs */
/** @{ */

/**
* Initalize the Pmic Arb HAL. 
*  
* @param[in] hHandle - This PmicArb hHandle. 
*
* @param[out] ppszSbVersion - pointer to PmicArb version string.
*
* @return None
*
*/ 
void HAL_PmicArb_Init(HAL_PmicArb_HandleType hHandle, char **ppszSbVersion);


/**
* Reset the PMIC Arbiter.  
*  
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
*/
void HAL_PmicArb_Reset(HAL_PmicArb_HandleType hHandle);


/** @name  Configuration APIs */
/** @{ */

/**
* Get the PMIC Arbiter HW version. 
* 
* @param[in] hHandle - This PmicArb hHandle.
* 
* @param[out] puMajor - Pmic Arb major version.
*
* @param[out] puMinor - Pmic Arb minor version.
*
* @param[out] puStep - Pmic Arb step version.
*
* @return None
* 
* @see None
* 
* @attention None
*/ 
void HAL_PmicArb_GetVersion(
                            HAL_PmicArb_HandleType hHandle, 
                            uint32 *puMajor, 
                            uint32 *puMinor, 
                            uint32 *puStep);

/**
* Enable dynamic clk gating for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_DynamicClkGatingDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_DynamicClkGatingEnable(HAL_PmicArb_HandleType hHandle);

/**
* Disable dynamic clk gating for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_DynamicClkGatingEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_DynamicClkGatingDisable(HAL_PmicArb_HandleType hHandle);


/**
* Check if dynamic clk gating is enabled for the PMIC Arbiter. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_DynamicClkGatingEnable(), 
*      HAL_PmicArb_DynamicClkGatingDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_DynamicClkGatingIsEnabled(HAL_PmicArb_HandleType hHandle);

/**
* Enable transaction done interrupts globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_GlobalDoneIntDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_GlobalDoneIntEnable(HAL_PmicArb_HandleType hHandle);


/**
* Disable transaction done interrupts globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_GlobalDoneIntEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_GlobalDoneIntDisable(HAL_PmicArb_HandleType hHandle);


/**
* Check if transaction done interrupts is enabled globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_GlobalDoneIntDisable(), HAL_PmicArb_GlobalDoneIntEnable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_GlobalDoneIntIsEnabled(HAL_PmicArb_HandleType hHandle);


/**
* Enable all PVC Intf globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcIntfEnable(HAL_PmicArb_HandleType hHandle);

/**
* Disable all PVC Intf globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return None
* 
* @see HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcIntfDisable(HAL_PmicArb_HandleType hHandle);

/**
* Check if all PVC Intf are enabled globally. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_PvcIntfEnable(), HAL_PmicArb_PvcIntfDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_PvcIntfIsEnabled(HAL_PmicArb_HandleType hHandle);

/**
* Get the PVC Intf status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @return Bitmask of HAL_PmicArb_PvcIntfStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_PvcIntfStatusFieldMaskType HAL_PmicArb_PvcIntfGetStatus(
    HAL_PmicArb_HandleType hHandle);


/**
* Set the port priorities.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @param[in] uPriority - Priority value. Priority 0 is the highest priority, 
*            priority 1 is the next highest, and so on
*
* @return None
* 
* @see HAL_PmicArb_PortGetPriority()
* 
* @attention None
*/
void HAL_PmicArb_PortSetPriority(
                                 HAL_PmicArb_HandleType hHandle, 
                                 HAL_PmicArb_PortType portId, 
                                 uint32 uPriority);

/**
* Get the port priority.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return Priority value. Priority 0 is the highest priority, priority 1
*         is the next highest, and so on. -1 return value indicates that
*         the port number was not found in the priority table
* 
* @see HAL_PmicArb_PortSetPriority()
* 
* @attention None
*/
int32 HAL_PmicArb_PortGetPriority(
                                  HAL_PmicArb_HandleType hHandle, 
                                  HAL_PmicArb_PortType portId);

/**
* Enable PVC port. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return None
* 
* @see HAL_PmicArb_PvcPortDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortEnable(
                               HAL_PmicArb_HandleType hHandle, 
                               HAL_PmicArb_PortType portId);


/**
* Disable PVC port. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return None
* 
* @see HAL_PmicArb_PvcPortEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortDisable(
                                HAL_PmicArb_HandleType hHandle, 
                                HAL_PmicArb_PortType portId);

/**
* Check if a PVC port is enabled. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_PvcPortEnable(),HAL_PmicArb_PvcPortDisable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_PvcPortIsEnabled(
                                    HAL_PmicArb_HandleType hHandle, 
                                    HAL_PmicArb_PortType portId);


/**
* Set the PVC port's Spmi command access priority. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @param[in] eAccessPriority - Access priority value
*
* @return None
* 
* @see HAL_PmicArb_PvcPortGetSpmiAccessPriority()
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortSetSpmiAccessPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType portId, 
    HAL_PmicArb_SpmiAccessPrioirtyType eAccessPriority);


/**
* Get the PVC port spmi command access priority. 
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return Access priority value
* 
* @see HAL_PmicArb_PvcPortSetSpmiAccessPriority()
* 
* @attention None
*/ 
HAL_PmicArb_SpmiAccessPrioirtyType HAL_PmicArb_PvcPortGetSpmiAccessPriority(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType portId);


/**
* Get the PVC Port status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @return Bitmask of HAL_PmicArb_PvcPortStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_PvcPortStatusFieldMaskType HAL_PmicArb_PvcPortGetStatus(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType portId);


/**
* Set the PVC port allowed access address at an index.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @param[in] uIndex - Address index.
*
* @param[in] uAddr - Access address. 
*            [SPMI Address<4Bits> , Register Address<16bits>] 
*
* @return None
* 
* @see HAL_PmicArb_PvcPortGetAllowedAccessAddress
* 
* @attention None
*/ 
void HAL_PmicArb_PvcPortSetAllowedAccessAddress(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType portId, 
    uint32 uIndex, 
    uint32 uAddr);

/**
* Get the PVC port allowed access address from an index.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] portId - Port index.
*
* @param[in] uIndex - Address index.
*
* @return Access address. [SPMI Address<4Bits> , Register Address<16bits>] 
* 
* @see HAL_PmicArb_PvcPortSetAllowedAccessAddress
* 
* @attention None
*/ 
uint32 HAL_PmicArb_PvcPortGetAllowedAccessAddress(
    HAL_PmicArb_HandleType hHandle, 
    HAL_PmicArb_PortType portId, 
    uint32 uIndex);

/** @} */
/** @name Channel APIs */
/** @{ */

/**
* Sets up and starts a SPMI command
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[in] uSlaveId - SPMI Slave id
*
* @param[in] eCommand - Command type
*
* @param[in] eCommandPriority - Prioirty with which the 
*            command is issued on the SPMI bus
*
* @param[in] uRegisterAddress - Register address. Number of bits of this 
*            register address is based on command type. Some commands may
*            not use the register address and in that case, this parameter 
*            is ignored
*
* @param[in] uByteCount - Number of bytes to read/write. Some non read/write
*            type command may ignore this parameter
*
* @return None
* 
* @see None
* 
* @attention - For Write type commands, caller needs to queue up the data 
*              first by calling HAL_PmicArb_SpmiWriteOutDataBuffer()
*/
void HAL_PmicArb_SpmiCommand(
                             HAL_PmicArb_HandleType hHandle, 
                             uint32 uChannelNum, 
                             uint32 uSlaveId, 
                             HAL_PmicArb_SpmiCommandType eCommand,
                             HAL_PmicArb_SpmiAccessPrioirtyType eCommandPriority, 
                             uint32 uRegisterAddress,
                             uint32 uByteCount);

/**
* Write to Out data buffer for write type commands
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[in] pucData - Pointer to the write data array 
*
* @param[in] uByteCount - Number of bytes in the data array
*
* @return None
* 
* @see None
* 
* @attention - HAL_PmicArb_SpmiCommand() needs to be called subsequently to 
*              start the write command
*/
void HAL_PmicArb_SpmiWriteOutDataBuffer(
                                        HAL_PmicArb_HandleType hHandle, 
                                        uint32 uChannelNum, 
                                        uint8 *pucDataBuffer, 
                                        uint32 uByteCount);


/**
* Reads from the In Data Buffer after read type command
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @param[out] pucData - Pointer to the in read data array 
*
* @param[in] uByteCount - Number of bytes in the data array
*
* @return None
* 
* @see None
* 
* @attention - For reads, HAL_PmicArb_SpmiCommand() needs 
*              to be called prior to this API
*/
void HAL_PmicArb_SpmiReadInDataBuffer(
                                      HAL_PmicArb_HandleType hHandle, 
                                      uint32 uChannelNum, 
                                      uint8 *pucDataBuffer, 
                                      uint32 uByteCount);

/**
* Enable the transaction done interrupt
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return None
* 
* @see HAL_PmicArb_ChannelTransDoneIntDisable()
* 
* @attention None
*/ 
void HAL_PmicArb_ChannelTransDoneIntEnable(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum);

/**
* Disable the transaction done interrupt
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return None
* 
* @see HAL_PmicArb_ChannelTransDoneIntEnable()
* 
* @attention None
*/ 
void HAL_PmicArb_ChannelTransDoneIntDisable(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum);

/**
* Check if  the transaction done interrupt is enabled
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return TRUE, if enabled, FALSE otherwise
* 
* @see HAL_PmicArb_ChannelTransDoneIntEnable()
* 
* @attention None
*/ 
bool32 HAL_PmicArb_ChannelTransDoneIntIsEnabled(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum);


/**
* Get the channel status.
* 
* @param[in] hHandle - This PmicArb hHandle.
*
* @param[in] uChannelNum - Channel number.
*
* @return Bitmask of HAL_PmicArb_ChannelStatusFieldMaskType enumeration
* 
* @see None
* 
* @attention None
*/ 
HAL_PmicArb_ChannelStatusFieldMaskType HAL_PmicArb_ChannelGetStatus(
    HAL_PmicArb_HandleType hHandle, 
    uint32 uChannelNum);


/** @} */
/** @name Core information APIs */
/** @{ */

/**
* Get total number of PVC ports supported by the controller
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPvcPorts
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PvcPortGetNumPorts(HAL_PmicArb_HandleType hHandle);


/**
* Get total number of port priorities supported by the controller
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPortPriorities
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PortGetNumPortPriorities(HAL_PmicArb_HandleType hHandle);


/**
* Get total number of Address Indexes supported per PVC port
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumAddrIndexes
* 
* @see None
* 
* @attention None
*/
uint32 HAL_PmicArb_PvcPortGetNumAddrIndexes(HAL_PmicArb_HandleType hHandle);


/**
* Get total number of supported owners 
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
* 
* @see None
* 
* @attention None
*/
uint32  HAL_PmicArb_ChannelGetNumOwners(HAL_PmicArb_HandleType hHandle);


/**
* Get max number of read write bytes per command supported in a channel 
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
* 
* @see None
* 
* @attention None
*/
uint32  HAL_PmicArb_ChannelGetMaxAllowedReadWriteBytes(
    HAL_PmicArb_HandleType hHandle);

/** @} */

#endif /* __HAL_PMIC_ARB_H__ */
