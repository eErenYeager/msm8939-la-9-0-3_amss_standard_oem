#ifndef __HAL_SPMI_H_
#define __HAL_SPMI_H_
/*=========================================================================*/
/**
@file: HalSpmi.h

@brief: This module provides the Hardware Abstraction Layer interface to the
SPMI controller. 
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/src/hal/inc/HalSpmi.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
08/09/13   mq      Added support for different hw core versions
07/25/13   mq      Added API to query protocol status
04/25/13   PS      Added support for SPMI core version 1.1.0
09/25/12   PS      Added Enable/Disable MWB APIs
08/28/12   PS      Added PPID to APID conversion API
09/19/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "HALhwio.h"
#include "HALcomdef.h" /* uint32 and other basic types */

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/** HAL Spmi version strings */
#define HAL_SPMI_VERSION_STRING "HAL_SPMI_VERSION_1.0"

/** HAL Spmi Invalid handle */
#define HAL_SPMI_HANDLE_INVALID ((HAL_Spmi_HandleType)0)

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
/** The SPMI Handle holds the virtual base address of the SPMI Controller */
typedef unsigned long HAL_Spmi_HandleType;

/** SPMI Protocol IRQ status type */
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_MASK 0x1
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_MASK 0x2
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_MASK 0x4
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_MASK 0x8
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_MASK 0x10
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_MASK 0x20
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_MASK 0x40
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_MASK 0x80
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_MASK 0x100
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_PERIPH_IRQ_LOST_MASK 0x200
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_MASK 0x400
#define __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_MASK 0x800

typedef enum
{
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_PERIPH_IRQ_LOST_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_PERIPH_IRQ_LOST_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_MASK,
    HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_MASK = __HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_MASK,
    HAL_ENUM_32BITS(HAL_SPMI_CFG_PROTOCOL_IRQ_STATUS)
}HAL_Spmi_CfgProtocolIrqStatusType;

/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/

/** @name Mandatory APIs */
/** @{ */

/**
* Initalize the Spmi HAL.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @param[out] ppszSbVersion - pointer to Spmi version string.
*
* @return None
*/
void HAL_Spmi_Init(HAL_Spmi_HandleType hHandle, char **ppszSbVersion);


/**
* Reset the Spmi Controller.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @return None
*/
void HAL_Spmi_Reset(HAL_Spmi_HandleType hHandle);

/**
* Get the SPMI HW version.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @param[out] puMajor - Pmic Arb major version.
*
* @param[out] puMinor - Pmic Arb minor version.
*
* @param[out] puStep - Pmic Arb step version.
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_GetVersion(
    HAL_Spmi_HandleType hHandle,
    uint32 *puMajor,
    uint32 *puMinor,
    uint32 *puStep);

/**
* Get the SPMI protocol status.
*
* @param[in] hHandle - This Spmi hHandle.
*
* @return The status flags
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_GetProtocolStatus(HAL_Spmi_HandleType hHandle);


/** @name Configuration APIs */
/** @{ */

/**
* Set the owner of a Peripheral.
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number.
*
* @param[in] uOwner - Owner number.
*
* @return None
*
* @see HAL_Spmi_CfgGetPeripheralOwner()
*
* @attention None
*/
void HAL_Spmi_CfgSetPeripheralOwner(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph,
    uint32 uOwner);


/**
* Get the owner of a Peripheral.
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number.
*
* @return Owner number.
*
* @see HAL_Spmi_CfgSetPeripheralOwner()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetPeripheralOwner(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);

/**
* Set the owner of a Peripheral and also the reverse lookup entry 
* of APID to PPID mapping
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @param[in] uPpid - Physical Peripheral number.
*
* @param[in] uOwner - Owner number.
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_CfgSetPeripheralOwnerAndApid2PpidEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uApid,
	uint32 uPpid,
    uint32 uOwner);

/**
* Set the reverse lookup table entry of APID to PPID mapping. 
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @param[in] uPpid - Physical Peripheral number.
*
* @return None
*
* @see HAL_Spmi_CfgGetApid2PpidEntry()
*
* @attention None
*/
void HAL_Spmi_CfgSetApid2PpidEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uApid,
	uint32 uPpid);

/**
* Get the reverse lookup table entry of APID to PPID mapping.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uApid - Logical Peripheral number.
*
* @return Physical Peripheral number, -1 on error.
*
* @see HAL_Spmi_CfgSetApid2PpidEntry()
*
* @attention None
*/
int32 HAL_Spmi_CfgGetApid2PpidEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);

/**
* Enable the compare APID2PPID LUT logic.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @see HAL_Spmi_CfgApid2PpidCmprDisable()
*
* @attention None
*/
void HAL_Spmi_CfgApid2PpidCmprEnable(
HAL_Spmi_HandleType hHandle);

/**
* Disable the compare APID2PPID LUT logic.
* Note: Only available in version 1.1.0 and beyond
*
* @param[in] hHandle - This SPMI hHandle.
*
* @see HAL_Spmi_CfgApid2PpidCmprEnable()
*
* @attention None
*/
void HAL_Spmi_CfgApid2PpidCmprDisable(
HAL_Spmi_HandleType hHandle);

/**
* Set PPID(Physical Peripheral Id) to APID (Application Peripheral Id) mapping
* table entry at an index
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMappingTableIndex - Index into mapping table
*
* @param[in] uMappingEntry - Mapping table entry at index uMappingTableIndex
*
* @return None
*
* @see HAL_Spmi_CfgGetPpidToApidMappingTableEntry()
*
* @attention None
*/
void HAL_Spmi_CfgSetPpidToApidMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uMappingTableIndex,
    uint32 uMappingEntry);

/**
* Get PPID(Physical Peripheral Id) to APID (Application Peripheral Id) mapping
* table entry at an index
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMappingTableIndex - Index into mapping table
*
* @return Mapping table entry at index uMappingTableIndex
*
* @see HAL_Spmi_CfgSetPpidToApidMappingTableEntry()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetPpidToApidMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uMappingTableIndex);


/**
* Get APID(Application Peripheral Id) from given PPID (Physical
* Peripheral Id) using the Radix Map provided
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uRadixMap - Pointer to Radix Map Table
*
* @param[in] uPpid - Physical peripheral id
*
* @return Valid APID or -1 on error
*
* @see None
*
* @attention None
*/
int32 HAL_Spmi_CfgGetApidFromPpidUsingRadixMap(
    HAL_Spmi_HandleType hHandle,
    uint32 *uRadixMap,
    uint32 uPpid);

/**
* Create mapping table entry
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uBitIndex - Bit Index
*
* @param[in] uBranchResult0Flag - Branch or Result Flag for bit value 0 
*
* @param[in] uBranchResult0 - Branch or Result target for bit value 0
*
* @param[in] uBranchResult1Flag - Branch or Result Flag for bit value 1 
*
* @param[in] uBranchResult1 - Branch or Result target for bit value 1
*
* @return Created mapping table entry
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgCreateMappingTableEntry(
    HAL_Spmi_HandleType hHandle,
    uint32 uBitIndex,
    uint32 uBranchResult0Flag,
    uint32 uBranchResult0,
    uint32 uBranchResult1Flag,
    uint32 uBranchResult1);

/**
* Set SPMI Master Id
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uMid - Master Id [2 bits]
*
* @return None
*
* @see HAL_Spmi_CfgGetMasterId()
*
* @attention None
*/
void HAL_Spmi_CfgSetMasterId(HAL_Spmi_HandleType hHandle, uint32 uMasterId);


/**
* Enable the SPMI Bus Arbitration process
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgBusArbitrationProcessDisable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationProcessEnable(HAL_Spmi_HandleType hHandle);


/**
* Disable the SPMI Bus Arbitration process
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgBusArbitrationProcessEnable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationProcessDisable(HAL_Spmi_HandleType hHandle);

/**
* Check if SPMI Bus Arbitration process is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgBusArbitrationProcessEnable(),
* HAL_Spmi_CfgBusArbitrationProcessDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsBusArbitrationProcessEnabled(HAL_Spmi_HandleType hHandle);


/**
* Get SPMI Master Id
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Master Id [2 bits]
*
* @see HAL_Spmi_CfgSetMasterId()
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetMasterId(HAL_Spmi_HandleType hHandle);


/**
* Enable the SPMI Bus Arbitration Bypass
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgArbiterBypassDisable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationBypassEnable(HAL_Spmi_HandleType hHandle);


/**
* Disable the SPMI Bus Arbitration Bypass
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgArbiterBypassEnable()
*
* @attention None
*/
void HAL_Spmi_CfgBusArbitrationBypassDisable(HAL_Spmi_HandleType hHandle);


/**
* Check if SPMI Bus Arbitration is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgArbiterBypassEnable(),
* HAL_Spmi_CfgBusArbitrationBypassDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsBusArbitrationBypassEnabled(HAL_Spmi_HandleType hHandle);


/**
* Enable the Force Master Write On Error
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorDisable()
*
* @attention None
*/
void HAL_Spmi_CfgForceMasterWriteOnErrorEnable(HAL_Spmi_HandleType hHandle);


/**
* Disable the Force Master Write On Error
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorEnable()
*
* @attention None
*/
void HAL_Spmi_CfgForceMasterWriteOnErrorDisable(HAL_Spmi_HandleType hHandle);


/**
* Check if Force Master Write On Error is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgForceMasterWriteOnErrorEnable(),
* HAL_Spmi_CfgForceMasterWriteOnErrorDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsForceMasterWriteOnErrorEnabled(HAL_Spmi_HandleType hHandle);


/**
* Enable the SPMI Security features
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgSecurityDisable()
*
* @attention None
*/
void HAL_Spmi_CfgSecurityEnable(HAL_Spmi_HandleType hHandle);


/**
* Disable the SPMI Security features
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see HAL_Spmi_CfgSecurityEnable()
*
* @attention None
*/
void HAL_Spmi_CfgSecurityDisable(HAL_Spmi_HandleType hHandle);


/**
* Check if SPMI Security features are enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_CfgSecurityEnable(),HAL_Spmi_CfgSecurityDisable()
*
* @attention None
*/
bool32 HAL_Spmi_CfgIsSecurityEnabled(HAL_Spmi_HandleType hHandle);

/**
* Enable CGC Ctrl for SPMI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCEnMask - Bitmask of CGC to set.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgEnableCGCCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uCGCEnMask);

/**
* Disable CGC Ctrl for SPMI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCClrMask - Bitmask of CGC to clear.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgDisableCGCCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uCGCClrMask);

/**
* Get the SPMI Protocol IRQ status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Bitmask of HAL_Spmi_CfgProtocolIrqStatusType enumeration
*
* @see None
*
* @attention None
*/
HAL_Spmi_CfgProtocolIrqStatusType HAL_Spmi_CfgGetProtocolIrqStatus(
    HAL_Spmi_HandleType hHandle);


/**
* Enable the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to enable
*
* @return None
*
* @see HAL_Spmi_CfgProtocolIrqDisable()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqEnable(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq);

/**
* Disable the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to disable
*
* @return None
*
* @see HAL_Spmi_CfgProtocolIrqEnable()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqDisable(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq);

/**
* Get all the SPMI Protocol IRQs which are enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return BitMask of all the IRQs which are enabled
*
* @see HAL_Spmi_CfgProtocolIrqEnable()
* HAL_Spmi_CfgProtocolIrqDisable()
*
* @attention None
*/
uint32 HAL_Spmi_CfgProtocolIrqGetEnabled(
    HAL_Spmi_HandleType hHandle);

/**
* Clears the SPMI Protocol IRQs
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProtocolIrq - Bitmask of IRQs to clear
*
* @return None
*
* @see HAL_Spmi_CfgGetProtocolIrqStatus()
*
* @attention None
*/
void HAL_Spmi_CfgProtocolIrqClear(
    HAL_Spmi_HandleType hHandle,
    HAL_Spmi_CfgProtocolIrqStatusType uProtocolIrq);

/** @} */
/** @name Peripheral interrupt controller APIs */
/** @{ */

/**
* Get all the Peripheral accumulated Interrupt status for an owner
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uOwner - Owner number
*
* @param[out] puIntrStatus - Pointer to an array of 8 words which will hold
* the the accumulated IntrStatus
*
* @return None
*
* @see None
*
* @attention None
*/
void HAL_Spmi_PicGetPeriphAccumulatedIntrStatus(
    HAL_Spmi_HandleType hHandle,
    uint32 uOwner,
    uint32 *puIntrStatus);

/**
* Check if a peripheral accumulated Interrupt is set for an
* owner
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uOwner - Owner number
*
* @param[in] uPeriph - Peripheral number
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see None
*
* @attention None
*/
bool32 HAL_Spmi_PicIsPeriphAccumulatedIntrSet(
    HAL_Spmi_HandleType hHandle,
    uint32 uOwner,
    uint32 uPeriph);

/**
* Enable a Peripheral accumulated Interrupt
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return None
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrDisable()
*
* @attention None
*/
void HAL_Spmi_PicPeriphAccumulatedIntrEnable(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);


/**
* Disable a Peripheral accumulated Interrupt
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return None
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrEnable()
*
* @attention None
*/
void HAL_Spmi_PicPeriphAccumulatedIntrDisable(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);


/**
* Check if a Peripheral accumulated Interrupt is enabled
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return TRUE, if enabled, FALSE otherwise
*
* @see HAL_Spmi_PicPeriphAccumulatedIntrEnable(),
* HAL_Spmi_PicPeriphAccumulatedIntrDisable()
*
* @attention None
*/
bool32 HAL_Spmi_PicIsPeriphAccumulatedIntrEnabled(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);


/**
* Get the peripheral extended Intr status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @return Bitmask indicating status of Peripheral extended status register
*
* @see HAL_Spmi_PicPeriphIntrClear()
*
* @attention None
*/
uint32 HAL_Spmi_PicGetPeriphIntrStatus(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph);


/**
* Clear the peripheral extended Intr status
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uPeriph - Peripheral number
*
* @param[in] uPeriphIntrClearMask - Bitmask to clear the Peripheral Intr status
* bits
*
* @return None
*
* @see HAL_Spmi_PicPeriphIntrClear()
*
* @attention None
*/
void HAL_Spmi_PicPeriphIntrClear(
    HAL_Spmi_HandleType hHandle,
    uint32 uPeriph,
    uint32 uPeriphIntrClearMask);


/** @} */
/** @name GENI Configuration APIs */
/** @{ */



/**
* Configure the GENI configuration
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] pGeniCfg - Index og GENI_CFG register
*
* @param[in] uGeniCfgVal - Configuration value
*
* @return None
*
* @see None
*
* @attention None
*/

void HAL_Spmi_GeniCfg(HAL_Spmi_HandleType hHandle,
    uint32 uGeniCfgIndex,
    uint32 uGeniCfgVal);

/**
* Configure the GENI RAM
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uGeniRamStartIndex - Index of GENI_RAM register
to start configuring
*
* @param[in] uGeniCfgVal - Ram Configuration value array
*
* @param[in] uNumWords - Number of values to program in RAM
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgRam(HAL_Spmi_HandleType hHandle,
    uint32 uGeniRamStartIndex,
    const uint32 *uGeniRamVal,
    uint32 uNumWords);

/**
* Set FORCE_DEFAULT for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetForceDefault(HAL_Spmi_HandleType hHandle);


/**
* Clear FORCE_DEFAULT for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgClearForceDefault(HAL_Spmi_HandleType hHandle);


/**
* Set value for SER_CLK_SEL for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSerialClkSelect - Value for SER_CLK_SEL.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetSerialClkSelect(HAL_Spmi_HandleType hHandle,
    uint32 uSerialClkSelect);


/**
* Get value for SER_CLK_SEL for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return Value for SER_CLK_SEL
*
* @see None()
*
* @attention None
*/
uint32 HAL_Spmi_GeniCfgGetSerialClkSelect(HAL_Spmi_HandleType hHandle);

/**
* Enable Output Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSOEnMask - Bitmask of SOEn to enable.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgEnableOutputCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uSOEnMask);

/**
* Disable Output Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uSOEnMask - Bitmask of SOEn to disable.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgDisbleOutputCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uSOEnMask);

/**
* Enable CGC Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCEnMask - Bitmask of CGC to set.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgEnableCGCCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uCGCEnMask);

/**
* Disable CGC Ctrl for GENI
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uCGCClrMask - Bitmask of CGC to clear.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgDisableCGCCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uCGCClrMask);

/**
* Program the ProgROMCtrl 
* Note: This is only avaiable 1.1.0 onwards
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uProgRomCtrlVal - Value to program
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_GeniCfgSetProgRomCtrl(HAL_Spmi_HandleType hHandle,
                                    uint32 uProgRomCtrlVal);

/**
* Configure Arbiter Ctrl value
*
* @param[in] hHandle - This SPMI hHandle.
*
* @param[in] uArbiterCtlrValue - Value to be written to Arbiter
* Ctrl field of SPMI_CFG register.
*
* @return None
*
* @see None()
*
* @attention None
*/
void HAL_Spmi_CfgSetArbiterCtrl(HAL_Spmi_HandleType hHandle,
    uint32 uArbiterCtlrValue);


/**
* Enable MWB
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
* 
* @see None()
* 
* @attention HAL_Spmi_CfgDisableMWB
*/ 
void HAL_Spmi_CfgEnableMWB(HAL_Spmi_HandleType hHandle);


/**
* Disable MWB
* 
* @param[in] hHandle - This SPMI hHandle.
*
* @return None
* 
* @see None()
* 
* @attention HAL_Spmi_CfgEnableMWB
*/ 
void HAL_Spmi_CfgDisableMWB(HAL_Spmi_HandleType hHandle);


/** @} */
/** @name Controller information API */
/** @{ */

/**
* Get total number of owners supported by the controller
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumOwners
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumOwners(HAL_Spmi_HandleType hHandle);


/**
* Get total number of peripherals supported by the controller
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumPeriphs
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumPeriphs(HAL_Spmi_HandleType hHandle);

/**
* Get total number of mapping table entries
*
* @param[in] hHandle - This SPMI hHandle.
*
* @return NumMappingTableEntries
*
* @see None
*
* @attention None
*/
uint32 HAL_Spmi_CfgGetNumMappingTableEntries(HAL_Spmi_HandleType hHandle);

/**
* Clears registers that don't adhere to their correct POR value of zero.
*
* @param[in] hHandle - This SPMI hHandle.
*/
void HAL_Spmi_ClearSpmiMemory(HAL_Spmi_HandleType hHandle);

/** @} */
#endif /* __HAL_SPMI_H_ */
