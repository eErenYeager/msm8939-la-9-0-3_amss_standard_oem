#===============================================================================
#
# SPMI Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2013 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/13/13   mq      Added support for 8994
# 10/09/13   mq      Added support for 9900
# 08/05/13   mq      Added standalone shutdown library for tz only
# 08/09/13   mq      Added support for different hw core versions
# 07/11/13   PS      Added back 8x62
# 07/03/13   mq      Added support for 8092
# 06/30/13   PS      Removed TZ specific configuration files
# 06/14/13   PS      Changed 8x62 to 8962
# 05/24/13   PS      Added support for 8926
# 05/06/13   PS      Added support for 8084, 8x62, 9x35
# 04/05/13   PS      Added HOSTDL image
# 03/19/13   PS      Added Profile directory
# 02/22/13   PS      Updated for SBL1 dev cfg conversion
# 12/03/12   PS      Added TZ config file support
# 11/28/12   PS      Added Pack functionality
# 11/09/12   PS      Added logging capability
# 08/17/12   PS      Added apps image
# 02/09/12   PS      Initial release
#===============================================================================
Import('env')

#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
env = env.Clone()

# Additional defines
env.Append(CPPDEFINES = ["FEATURE_LIBRARY_ONLY"])   

pmicArbCoreVersion = '2.0.2'  # only version currently used in Badger
picCoreVersion = '' # not currently versioned in ipcat
spmiCoreVersion = None
geniCoreVersion = None

if env['MSM_ID'] in ['8974']:
    env.Replace(SPMI_MSM_ID = '8974')
    geniCoreVersion = '1.0.0'
    spmiCoreVersion = '1.0.2'
elif env['MSM_ID'] in ['9x25']:
    env.Replace(SPMI_MSM_ID = '9x25')
    geniCoreVersion = '1.0.0'
    spmiCoreVersion = '1.0.2'
elif env['MSM_ID'] in ['9x35']:
    env.Replace(SPMI_MSM_ID = '9x35')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.1.0'
elif env['MSM_ID'] in ['8x26', '8926']:
    env.Replace(SPMI_MSM_ID = '8x26')
    geniCoreVersion = '1.0.0'
    spmiCoreVersion = '1.0.2'
elif env['MSM_ID'] in ['8x10']:
    env.Replace(SPMI_MSM_ID = '8x10')
    geniCoreVersion = '1.0.0'
    spmiCoreVersion = '1.0.2'
elif env['MSM_ID'] in ['8084']:
    env.Replace(SPMI_MSM_ID = '8084')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.1.0'
elif env['MSM_ID'] in ['8962', '8x62']:
    env.Replace(SPMI_MSM_ID = '8962')
    geniCoreVersion = '1.0.0'
    spmiCoreVersion = '1.0.2'
elif env['MSM_ID'] in ['9x35']:
    env.Replace(SPMI_MSM_ID = '9x35')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.1.0'
elif env['MSM_ID'] in ['8092']:
    env.Replace(SPMI_MSM_ID = '8092')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.1.0'
elif env['MSM_ID'] in ['9900']:
    env.Replace(SPMI_MSM_ID = '9900')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.1.0'
elif env['MSM_ID'] in ['8994']:
    env.Replace(SPMI_MSM_ID = '8994')
    geniCoreVersion = '2.0.0'
    spmiCoreVersion = '1.2.0'    
else:
   Return();
      
SRCPATH = "../src"
CFGPATH = "../config"
CBSP_APIS = []

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 
IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE', 
          'RPM_IMAGE', 
          'TZOS_IMAGE', 
          'SPMI_BOOT_DRIVER', 'EMMCBLD_IMAGE', 'EHOSTDL_IMAGE', 'HOSTDL_IMAGE',
          'WCN_IMAGE', 'CBSP_WCN_IMAGE', 'CORE_WCN',
          'APPS_IMAGE', 'CBSP_APPS_IMAGE']

if env.has_key('EMMCBLD_IMAGE'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_NONE"])
elif env.has_key('EHOSTDL_IMAGE') or env.has_key('HOSTDL_IMAGE'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_NONE"])
elif env.has_key('SPMI_BOOT_DRIVER'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_NONE"])
elif env.has_key('MODEM_PROC'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_MAX"])
elif env.has_key('TZOS_IMAGE'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_NONE"])
elif env.has_key('APPS_PROC'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_MAX"])
elif env.has_key('RPM_IMAGE'):
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_NONE"])    
else:
    env.Append(CPPDEFINES = ["SPMI_LOG_COMPILE_LEVEL=SPMI_LOG_LEVEL_ERROR"])

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
env.PublishPrivateApi('BUSES_SPMI', [
   '../hw',
   '../hw/arbiter/' + pmicArbCoreVersion,
   '../hw/pic/' + picCoreVersion,
   '../hw/spmi/' + spmiCoreVersion,
   '../hw/geni/' + geniCoreVersion,
   '../src/hal/inc',
   '../config/${SPMI_MSM_ID}',
   '../src/tzos/${SPMI_MSM_ID}',
   '../src/tzos',
   '../src/logs',
   '../src/driver/SpmiBus', #needed for SBL1, emmcbld and hostdl
   '../src/driver/SpmiCfg', #needed for SBL1, emmcbld and hostdl
   '../src/driver/SpmiIntrCtlr',
   '../src/profile',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS += [
   'BUSES',
   'HAL',
   'DAL',
   'SYSTEMDRIVERS',
   'SERVICES',
   'POWER',
   'DEBUGTRACE',
   'SECUREMSM',  #For tzos_log   
   'BOOT', # for boot_log
]

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
SPMI_SOURCES_RAW = env.GlobFiles(SRCPATH + '/*/*.c', posix=True)
SPMI_SOURCES_RAW += env.GlobFiles(SRCPATH + '/*/*/*.c', posix=True)
if env.has_key('EMMCBLD_IMAGE') or env.has_key('EHOSTDL_IMAGE') or env.has_key('HOSTDL_IMAGE'):
    SPMI_SOURCES_RAW += env.GlobFiles(SRCPATH + '/*/*/no_os/*.c', posix=True)
elif env.has_key('SPMI_BOOT_DRIVER'):
    SPMI_SOURCES_RAW += env.GlobFiles(SRCPATH + '/*/*/dal_os/*.c', posix=True)

SPMI_SOURCES = [path.replace(SRCPATH, '${BUILDPATH}') for path in SPMI_SOURCES_RAW]

if env.has_key('TZOS_IMAGE'):    
    SPMI_HALT_SOURCES_RAW = [SRCPATH + '/driver/SpmiCfg/SpmiCfgHalt.c']
    SPMI_HALT_SOURCES = [path.replace(SRCPATH, '${BUILDPATH}') for path in SPMI_HALT_SOURCES_RAW]

#-------------------------------------------------------------------------------
# XML files
#-------------------------------------------------------------------------------
if 'USES_DEVCFG' in env:
    # where multiple dev cfg is supported
    if env.has_key('MODEM_PROC') or env.has_key('CORE_RPM') or env.has_key('RPM_IMAGE'):  
        SPMI_CONFIG_FILE_XML_8084 = env.GlobFiles(CFGPATH + '/8084/*8084*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8092 = env.GlobFiles(CFGPATH + '/8092/*8092*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8962 = env.GlobFiles(CFGPATH + '/8962/*8962*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8974 = env.GlobFiles(CFGPATH + '/8974/*8974*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8994 = env.GlobFiles(CFGPATH + '/8994/*8994*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8x10 = env.GlobFiles(CFGPATH + '/8x10/*8x10*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_8x26 = env.GlobFiles(CFGPATH + '/8x26/*8x26*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_9x25 = env.GlobFiles(CFGPATH + '/9x25/*9x25*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_9x35 = env.GlobFiles(CFGPATH + '/9x35/*9x35*.xml', posix=True)
        SPMI_CONFIG_FILE_XML_9900 = env.GlobFiles(CFGPATH + '/9900/*9900*.xml', posix=True)
        
        env.AddDevCfgInfo(['DAL_DEVCFG_IMG'], {
            '9900_xml'    : SPMI_CONFIG_FILE_XML_9900,
            
            '8084_xml'    : SPMI_CONFIG_FILE_XML_8084,
            
            '8092_xml'    : SPMI_CONFIG_FILE_XML_8092,
                        
            '8962_xml'    : SPMI_CONFIG_FILE_XML_8962,
            '8x62_xml'    : SPMI_CONFIG_FILE_XML_8962,
            
            '8974_xml'    : SPMI_CONFIG_FILE_XML_8974,
            '8974_pro_xml': SPMI_CONFIG_FILE_XML_8974,
            
            '8994_xml'    : SPMI_CONFIG_FILE_XML_8994,
                                    
            '8610_xml'    : SPMI_CONFIG_FILE_XML_8x10,
            '8x10_xml'    : SPMI_CONFIG_FILE_XML_8x10,
                        
            '8926_xml'    : SPMI_CONFIG_FILE_XML_8x26,
            '8626_xml'    : SPMI_CONFIG_FILE_XML_8x26,
            '8x26_xml'    : SPMI_CONFIG_FILE_XML_8x26,
                        
            '9625_xml'    : SPMI_CONFIG_FILE_XML_9x25,
            '9x25_xml'    : SPMI_CONFIG_FILE_XML_9x25,
            
            '9635_xml'    : SPMI_CONFIG_FILE_XML_9x35,
            '9x35_xml'    : SPMI_CONFIG_FILE_XML_9x35,
        })
    else:
        SPMI_CONFIG_FILE_XML = env.GlobFiles(CFGPATH + '/${SPMI_MSM_ID}/*${SPMI_MSM_ID}*.xml', posix=True)
        SPMI_CONFIG_FILE_XML = ', '.join(SPMI_CONFIG_FILE_XML) #convert list to string

        if SPMI_CONFIG_FILE_XML: #try including XML only if it is present in the build 
            env.AddDevCfgInfo(['DAL_DEVCFG_IMG'], {
                'devcfg_xml'    : SPMI_CONFIG_FILE_XML
            })
     
#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddLibrary(IMAGES, '${BUILDPATH}/Spmi', SPMI_SOURCES)
if env.has_key('TZOS_IMAGE'): 
    env.AddLibrary(IMAGES, '${BUILDPATH}/SpmiHalt', SPMI_HALT_SOURCES)

#-------------------------------------------------------------------------------
# Remove unused target files during the clean process
#-------------------------------------------------------------------------------
Full_File_List=env.FindFiles(['*'], '../config/', posix=True)
Full_File_List+=env.FindFiles(['*'], '../scripts/', posix=True)
Using_File_List=env.FindFiles(['*'], '../config/${SPMI_MSM_ID}', posix=True)
Using_File_List+=env.FindFiles(['pmic_arb_spmi_cfg_tzos.xml'], '../config/', posix=True)
Using_File_List+=env.FindFiles(['*'], '../scripts/${SPMI_MSM_ID}', posix=True)
Removed_File_List=list(set(Full_File_List).difference(Using_File_List))
env.CleanPack(IMAGES, Removed_File_List)
