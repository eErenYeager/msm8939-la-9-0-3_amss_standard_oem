#ifndef _PMIC_ARB_HWIO_
#define _PMIC_ARB_HWIO_

#define PMIC_ARB_BASE 0x00000000

#include "PmicArbCoreHWIO.h"
#include "PmicArbPicHWIO.h"
#include "SpmiHWIO.h"
#include "SpmiGeniHWIO.h"

#endif
