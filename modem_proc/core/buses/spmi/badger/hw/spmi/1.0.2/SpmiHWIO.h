#ifndef __SPMIHWIO_H__
#define __SPMIHWIO_H__
/*
===========================================================================
*/
/**
  @file SpmiCfgHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    PMIC_ARB_SPMI_CFG

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/hw/spmi/1.0.2/SpmiHWIO.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: PMIC_ARB_SPMI_CFG
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_SPMI_CFG_REG_BASE                                                             (PMIC_ARB_BASE      + 0x0000a700)

#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR(m)                                    (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000000 + 0x4 * (m))
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_RMSK                                              0x7
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_MAXm                                              255
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_INI(m)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR(m), HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_RMSK)
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_INMI(m,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR(m), mask)
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_OUTI(m,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR(m),val)
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_OUTMI(m,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_ADDR(m),mask,val,HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_INI(m))
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_BMSK                                 0x7
#define HWIO_PMIC_ARB_SPMI_PERIPHm_2OWNER_TABLE_REG_PERIPH2OWNER_SHFT                                 0x0

#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_ADDR(k)                                          (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000400 + 0x4 * (k))
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_RMSK                                               0x3fffff
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_MAXk                                                    254
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_INI(k)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_ADDR(k), HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_RMSK)
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_INMI(k,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_ADDR(k), mask)
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_OUTI(k,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_ADDR(k),val)
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_OUTMI(k,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_ADDR(k),mask,val,HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_INI(k))
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_BMSK                                     0x3c0000
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BIT_INDEX_SHFT                                         0x12
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_BMSK                           0x20000
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_FLAG_SHFT                              0x11
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_BMSK                                0x1fe00
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_0_SHFT                                    0x9
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_BMSK                             0x100
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_FLAG_SHFT                               0x8
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_BMSK                                   0xff
#define HWIO_PMIC_ARB_SPMI_MAPPING_TABLE_REGk_BRANCH_RESULT_1_SHFT                                    0x0

#define HWIO_PMIC_ARB_SPMI_MID_REG_ADDR                                                        (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000800)
#define HWIO_PMIC_ARB_SPMI_MID_REG_RMSK                                                               0x3
#define HWIO_PMIC_ARB_SPMI_MID_REG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MID_REG_ADDR, HWIO_PMIC_ARB_SPMI_MID_REG_RMSK)
#define HWIO_PMIC_ARB_SPMI_MID_REG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MID_REG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_MID_REG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_MID_REG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_MID_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_MID_REG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_MID_REG_IN)
#define HWIO_PMIC_ARB_SPMI_MID_REG_MID_BMSK                                                           0x3
#define HWIO_PMIC_ARB_SPMI_MID_REG_MID_SHFT                                                           0x0

#define HWIO_PMIC_ARB_SPMI_CFG_REG_ADDR                                                        (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000804)
#define HWIO_PMIC_ARB_SPMI_CFG_REG_RMSK                                                           0x1ffff
#define HWIO_PMIC_ARB_SPMI_CFG_REG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CFG_REG_ADDR, HWIO_PMIC_ARB_SPMI_CFG_REG_RMSK)
#define HWIO_PMIC_ARB_SPMI_CFG_REG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CFG_REG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_CFG_REG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_CFG_REG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_CFG_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_CFG_REG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_CFG_REG_IN)
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_CTRL_BMSK                                              0x1ffc0
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_CTRL_SHFT                                                  0x6
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_ARB_AFTER_MASTER_TO_BMSK                                    0x20
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_ARB_AFTER_MASTER_TO_SHFT                                     0x5
#define HWIO_PMIC_ARB_SPMI_CFG_REG_BUS_IDLE_CONN_MODE_BMSK                                           0x10
#define HWIO_PMIC_ARB_SPMI_CFG_REG_BUS_IDLE_CONN_MODE_SHFT                                            0x4
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_BMSK                                   0x8
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MASTER_WRITE_ON_ERROR_SHFT                                   0x3
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MPM_CLK_REQ_BMSK                                             0x4
#define HWIO_PMIC_ARB_SPMI_CFG_REG_FORCE_MPM_CLK_REQ_SHFT                                             0x2
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_BMSK                                                0x2
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_BYPASS_SHFT                                                0x1
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_BMSK                                                0x1
#define HWIO_PMIC_ARB_SPMI_CFG_REG_ARBITER_ENABLE_SHFT                                                0x0

#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_ADDR                                                (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000808)
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_RMSK                                                       0x1
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_ADDR, HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_RMSK)
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_IN)
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_BMSK                                      0x1
#define HWIO_PMIC_ARB_SPMI_SEC_DISABLE_REG_DISABLE_SECURITY_SHFT                                      0x0

#define HWIO_PMIC_ARB_SPMI_HW_VERSION_ADDR                                                     (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x0000080c)
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_RMSK                                                     0xffffffff
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_HW_VERSION_ADDR, HWIO_PMIC_ARB_SPMI_HW_VERSION_RMSK)
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_HW_VERSION_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MAJOR_BMSK                                    0xf0000000
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MAJOR_SHFT                                          0x1c
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MINOR_BMSK                                     0xfff0000
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_MINOR_SHFT                                          0x10
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_STEP_BMSK                                         0xffff
#define HWIO_PMIC_ARB_SPMI_HW_VERSION_HW_VERSION_STEP_SHFT                                            0x0

#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_ADDR                                                       (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000810)
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_RMSK                                                             0x7f
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CGC_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_CGC_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CGC_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_CGC_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_CGC_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_CGC_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_MAP_LOGIC_CLK_CGC_ON_BMSK                                        0x40
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_MAP_LOGIC_CLK_CGC_ON_SHFT                                         0x6
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_RPU_CLK_CGC_ON_BMSK                                              0x20
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_RPU_CLK_CGC_ON_SHFT                                               0x5
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_MWB_CLK_CGC_ON_BMSK                                              0x10
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_MWB_CLK_CGC_ON_SHFT                                               0x4
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_PIC_CLK_CGC_ON_BMSK                                               0x8
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_PIC_CLK_CGC_ON_SHFT                                               0x3
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_PAC_CLK_CGC_ON_BMSK                                               0x4
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_PAC_CLK_CGC_ON_SHFT                                               0x2
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_CFG_AHB_BRIDGE_WR_CLK_CGC_ON_BMSK                                 0x2
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_CFG_AHB_BRIDGE_WR_CLK_CGC_ON_SHFT                                 0x1
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_CFG_AHB_BRIDGE_CLK_CGC_ON_BMSK                                    0x1
#define HWIO_PMIC_ARB_SPMI_CGC_CTRL_CFG_AHB_BRIDGE_CLK_CGC_ON_SHFT                                    0x0

#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_ADDR                                                 (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000814)
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_RMSK                                                        0x1
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_ADDR, HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_RMSK)
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_IN)
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_BMSK                                             0x1
#define HWIO_PMIC_ARB_SPMI_MWB_ENABLE_REG_MWB_ENABLE_SHFT                                             0x0

#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ADDR                                            (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000820)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_RMSK                                                 0xfff
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ADDR, HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_RMSK)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_BMSK                            0x800
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_DISCONNECTED_SHFT                              0xb
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_BMSK                               0x400
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_ARBITER_CONNECTED_SHFT                                 0xa
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_PERIH_IRQ_LOST_BMSK                                  0x200
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_PERIH_IRQ_LOST_SHFT                                    0x9
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_BMSK                                  0x100
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_UNEXPECTED_SSC_SHFT                                    0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK                  0x80
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_NO_RESPONSE_DATA_FRAME_DETECTED_SHFT                   0x7
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK                   0x40
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_NO_RESPONSE_CMD_FRAME_DETECTED_SHFT                    0x6
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_BMSK                     0x20
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_FALSE_MASTER_ARBITRATION_WIN_SHFT                      0x5
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_BMSK                                0x10
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_FALSE_BUS_REQUEST_SHFT                                 0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_BMSK                               0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_UNSUPPORTED_COMMAND_SHFT                               0x3
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_BMSK                      0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_DATA_ADDR_FRAME_PARITY_ERROR_SHFT                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_SLAVE_CMD_FRAME_PARITY_ERROR_SHFT                      0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_BMSK                     0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_STATUS_MASTER_CMD_FRAME_PARITY_ERROR_SHFT                     0x0

#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ADDR                                            (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000824)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_RMSK                                                 0xfff
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ADDR, HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_RMSK)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ADDR,m,v,HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_IN)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ARBITER_DISCONNECTED_BMSK                            0x800
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ARBITER_DISCONNECTED_SHFT                              0xb
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ARBITER_CONNECTED_BMSK                               0x400
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_ARBITER_CONNECTED_SHFT                                 0xa
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_PERIH_IRQ_LOST_BMSK                                  0x200
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_PERIH_IRQ_LOST_SHFT                                    0x9
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_UNEXPECTED_SSC_BMSK                                  0x100
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_UNEXPECTED_SSC_SHFT                                    0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK                  0x80
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_NO_RESPONSE_DATA_FRAME_DETECTED_SHFT                   0x7
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK                   0x40
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_NO_RESPONSE_CMD_FRAME_DETECTED_SHFT                    0x6
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_FALSE_MASTER_ARBITRATION_WIN_BMSK                     0x20
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_FALSE_MASTER_ARBITRATION_WIN_SHFT                      0x5
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_FALSE_BUS_REQUEST_BMSK                                0x10
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_FALSE_BUS_REQUEST_SHFT                                 0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_UNSUPPORTED_COMMAND_BMSK                               0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_UNSUPPORTED_COMMAND_SHFT                               0x3
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_DATA_ADDR_FRAME_PARITY_ERROR_BMSK                      0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_DATA_ADDR_FRAME_PARITY_ERROR_SHFT                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_SLAVE_CMD_FRAME_PARITY_ERROR_SHFT                      0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_MASTER_CMD_FRAME_PARITY_ERROR_BMSK                     0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_ENABLE_MASTER_CMD_FRAME_PARITY_ERROR_SHFT                     0x0

#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ADDR                                             (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000828)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_RMSK                                                  0xfff
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ARBITER_DISCONNECTED_BMSK                             0x800
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ARBITER_DISCONNECTED_SHFT                               0xb
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ARBITER_CONNECTED_BMSK                                0x400
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_ARBITER_CONNECTED_SHFT                                  0xa
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_PERIH_IRQ_LOST_BMSK                                   0x200
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_PERIH_IRQ_LOST_SHFT                                     0x9
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_UNEXPECTED_SSC_BMSK                                   0x100
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_UNEXPECTED_SSC_SHFT                                     0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK                   0x80
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_NO_RESPONSE_DATA_FRAME_DETECTED_SHFT                    0x7
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK                    0x40
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_NO_RESPONSE_CMD_FRAME_DETECTED_SHFT                     0x6
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_FALSE_MASTER_ARBITRATION_WIN_BMSK                      0x20
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_FALSE_MASTER_ARBITRATION_WIN_SHFT                       0x5
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_FALSE_BUS_REQUEST_BMSK                                 0x10
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_FALSE_BUS_REQUEST_SHFT                                  0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_UNSUPPORTED_COMMAND_BMSK                                0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_UNSUPPORTED_COMMAND_SHFT                                0x3
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_DATA_ADDR_FRAME_PARITY_ERROR_BMSK                       0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_DATA_ADDR_FRAME_PARITY_ERROR_SHFT                       0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK                       0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_SLAVE_CMD_FRAME_PARITY_ERROR_SHFT                       0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_MASTER_CMD_FRAME_PARITY_ERROR_BMSK                      0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_CLEAR_MASTER_CMD_FRAME_PARITY_ERROR_SHFT                      0x0

#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ADDR                                            (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x0000082c)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_RMSK                                                 0xfff
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ARBITER_DISCONNECTED_BMSK                            0x800
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ARBITER_DISCONNECTED_SHFT                              0xb
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ARBITER_CONNECTED_BMSK                               0x400
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_ARBITER_CONNECTED_SHFT                                 0xa
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_PERIH_IRQ_LOST_BMSK                                  0x200
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_PERIH_IRQ_LOST_SHFT                                    0x9
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_UNEXPECTED_SSC_BMSK                                  0x100
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_UNEXPECTED_SSC_SHFT                                    0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK                  0x80
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_NO_RESPONSE_DATA_FRAME_DETECTED_SHFT                   0x7
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK                   0x40
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_NO_RESPONSE_CMD_FRAME_DETECTED_SHFT                    0x6
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_FALSE_MASTER_ARBITRATION_WIN_BMSK                     0x20
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_FALSE_MASTER_ARBITRATION_WIN_SHFT                      0x5
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_FALSE_BUS_REQUEST_BMSK                                0x10
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_FALSE_BUS_REQUEST_SHFT                                 0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_UNSUPPORTED_COMMAND_BMSK                               0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_UNSUPPORTED_COMMAND_SHFT                               0x3
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_DATA_ADDR_FRAME_PARITY_ERROR_BMSK                      0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_DATA_ADDR_FRAME_PARITY_ERROR_SHFT                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK                      0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_SLAVE_CMD_FRAME_PARITY_ERROR_SHFT                      0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_MASTER_CMD_FRAME_PARITY_ERROR_BMSK                     0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_SET_MASTER_CMD_FRAME_PARITY_ERROR_SHFT                     0x0

#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ADDR                                          (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000830)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_RMSK                                               0xfff
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ARBITER_DISCONNECTED_BMSK                          0x800
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ARBITER_DISCONNECTED_SHFT                            0xb
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ARBITER_CONNECTED_BMSK                             0x400
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_ARBITER_CONNECTED_SHFT                               0xa
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_PERIH_IRQ_LOST_BMSK                                0x200
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_PERIH_IRQ_LOST_SHFT                                  0x9
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNEXPECTED_SSC_BMSK                                0x100
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNEXPECTED_SSC_SHFT                                  0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_DATA_FRAME_DETECTED_BMSK                0x80
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_DATA_FRAME_DETECTED_SHFT                 0x7
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_CMD_FRAME_DETECTED_BMSK                 0x40
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_NO_RESPONSE_CMD_FRAME_DETECTED_SHFT                  0x6
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_MASTER_ARBITRATION_WIN_BMSK                   0x20
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_MASTER_ARBITRATION_WIN_SHFT                    0x5
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_BUS_REQUEST_BMSK                              0x10
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_FALSE_BUS_REQUEST_SHFT                               0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNSUPPORTED_COMMAND_BMSK                             0x8
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_UNSUPPORTED_COMMAND_SHFT                             0x3
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_DATA_ADDR_FRAME_PARITY_ERROR_BMSK                    0x4
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_DATA_ADDR_FRAME_PARITY_ERROR_SHFT                    0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_SLAVE_CMD_FRAME_PARITY_ERROR_BMSK                    0x2
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_SLAVE_CMD_FRAME_PARITY_ERROR_SHFT                    0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_MASTER_CMD_FRAME_PARITY_ERROR_BMSK                   0x1
#define HWIO_PMIC_ARB_SPMI_PROTOCOL_IRQ_EN_CLEAR_MASTER_CMD_FRAME_PARITY_ERROR_SHFT                   0x0

#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_ADDR                                                       (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000840)
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_RMSK                                                          0xfff11
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CHAR_CFG_ADDR, HWIO_PMIC_ARB_SPMI_CHAR_CFG_RMSK)
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CHAR_CFG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_CHAR_CFG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_CHAR_CFG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_CHAR_CFG_IN)
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_CHAR_MODE_BMSK                                                0xff000
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_CHAR_MODE_SHFT                                                    0xc
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_CHAR_STATUS_BMSK                                                0xf00
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_CHAR_STATUS_SHFT                                                  0x8
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_DIRECTION_BMSK                                                   0x10
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_DIRECTION_SHFT                                                    0x4
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_ENABLE_BMSK                                                       0x1
#define HWIO_PMIC_ARB_SPMI_CHAR_CFG_ENABLE_SHFT                                                       0x0

#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_ADDR(n)                                                 (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000844 + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_RMSK                                                        0xffff
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_MAXn                                                             1
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_INI(n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_ADDR(n), HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_RMSK)
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_INMI(n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_ADDR(n), mask)
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_OUTI(n,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_ADDR(n),val)
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_ADDR(n),mask,val,HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_INI(n))
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_DIN_ACTUAL_BMSK                                             0xff00
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_DIN_ACTUAL_SHFT                                                0x8
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_DOUT_DATA_DIN_EXP_BMSK                                        0xff
#define HWIO_PMIC_ARB_SPMI_CHAR_DATA_n_DOUT_DATA_DIN_EXP_SHFT                                         0x0

#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_ADDR                                                  (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x0000084c)
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_RMSK                                                       0x3ff
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_ARB_SEL_BMSK                                      0x3c0
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_ARB_SEL_SHFT                                        0x6
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_INT_SEL_BMSK                                       0x3c
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_INT_SEL_SHFT                                        0x2
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_SEL_BMSK                                            0x3
#define HWIO_PMIC_ARB_SPMI_TEST_BUS_CTRL_TEST_BUS_SEL_SHFT                                            0x0

#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_ADDR                                              (PMIC_ARB_SPMI_CFG_REG_BASE      + 0x00000850)
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_RMSK                                                     0x7
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_HW_SW_EVENTS_SEL_BMSK                                    0x7
#define HWIO_PMIC_ARB_SPMI_HW_SW_EVENTS_CTRL_HW_SW_EVENTS_SEL_SHFT                                    0x0


#endif /* __SPMIHWIO_H__ */
