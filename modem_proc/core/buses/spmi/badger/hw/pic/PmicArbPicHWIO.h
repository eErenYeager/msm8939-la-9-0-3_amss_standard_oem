#ifndef __PMICARBPICHWIO_H__
#define __PMICARBPICHWIO_H__
/*
===========================================================================
*/
/**
  @file PmicArbSpmiPicHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    PMIC_ARB_SPMI_PIC

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/hw/pic/PmicArbPicHWIO.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: PMIC_ARB_SPMI_PIC
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_SPMI_PIC_REG_BASE                                             (PMIC_ARB_BASE      + 0x0000b000)

#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_ADDR(m,n)                    (PMIC_ARB_SPMI_PIC_REG_BASE      + 0x00000000 + 0x20 * (m) + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_RMSK                         0xffffffff
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_MAXm                                  5
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_MAXn                                  7
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_INI2(m,n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_ADDR(m,n), HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_RMSK)
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_INMI2(m,n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_ADDR(m,n), mask)
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_INT_ACC_STATUS_BMSK          0xffffffff
#define HWIO_PMIC_ARB_SPMI_PIC_OWNERm_ACC_STATUSn_INT_ACC_STATUS_SHFT                 0x0

#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_ADDR(n)                             (PMIC_ARB_SPMI_PIC_REG_BASE      + 0x00000200 + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_RMSK                                       0x1
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_MAXn                                       255
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_INI(n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_ADDR(n), HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_RMSK)
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_INMI(n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_ADDR(n), mask)
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_OUTI(n,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_ADDR(n),val)
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_ADDR(n),mask,val,HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_INI(n))
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_INT_ACC_ENABLE_BMSK                        0x1
#define HWIO_PMIC_ARB_SPMI_PIC_ACC_ENABLEn_INT_ACC_ENABLE_SHFT                        0x0

#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_ADDR(n)                             (PMIC_ARB_SPMI_PIC_REG_BASE      + 0x00000600 + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_RMSK                                      0xff
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_MAXn                                       255
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_INI(n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_ADDR(n), HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_RMSK)
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_INMI(n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_ADDR(n), mask)
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_INT_STATUS_BMSK                           0xff
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_STATUSn_INT_STATUS_SHFT                            0x0

#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_ADDR(n)                              (PMIC_ARB_SPMI_PIC_REG_BASE      + 0x00000a00 + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_RMSK                                       0xff
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_MAXn                                        255
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_OUTI(n,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_ADDR(n),val)
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_INT_CLEAR_BMSK                             0xff
#define HWIO_PMIC_ARB_SPMI_PIC_IRQ_CLEARn_INT_CLEAR_SHFT                              0x0


#endif /* __PMICARBPICHWIO_H__ */
