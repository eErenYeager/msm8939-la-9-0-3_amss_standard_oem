#ifndef __SPMIGENIHWIO_H__
#define __SPMIGENIHWIO_H__
/*
===========================================================================
*/
/**
  @file SpmiGeniHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    PMIC_ARB_SPMI_SPMI_GENI_CFG

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/buses/spmi/badger/hw/geni/1.0.0/SpmiGeniHWIO.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: PMIC_ARB_SPMI_SPMI_GENI_CFG
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE                                        (PMIC_ARB_BASE      + 0x0000a000)

#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000000)
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_RMSK                                              0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_BMSK                                  0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CLK_CTRL_SER_CLK_SEL_SHFT                                  0x0

#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_ADDR                                     (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000004)
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_RMSK                                     0xffffffff
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_ADDR, HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_MAJOR_BMSK                               0xf0000000
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_MAJOR_SHFT                                     0x1c
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_MINOR_BMSK                                0xfff0000
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_MINOR_SHFT                                     0x10
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_STEP_BMSK                                    0xffff
#define HWIO_PMIC_ARB_SPMI_GENI_HW_VERSION_STEP_SHFT                                       0x0

#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_ADDR                                    (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000008)
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_RMSK                                    0xffffffff
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_ADDR, HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_MAJOR_REVISION_BMSK                     0xff000000
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_MAJOR_REVISION_SHFT                           0x18
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_VERSION_BMSK                              0xffffff
#define HWIO_PMIC_ARB_SPMI_GENI_FW_REVISION_VERSION_SHFT                                   0x0

#define HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_ADDR                              (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000000c)
#define HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_RMSK                                     0x1
#define HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_FORCE_DEFAULT_BMSK                       0x1
#define HWIO_PMIC_ARB_SPMI_GENI_FORCE_DEFAULT_REG_FORCE_DEFAULT_SHFT                       0x0

#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_ADDR                                    (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000010)
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_RMSK                                           0x7
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE2_EN_BMSK                                   0x4
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE2_EN_SHFT                                   0x2
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE1_EN_BMSK                                   0x2
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE1_EN_SHFT                                   0x1
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE0_EN_BMSK                                   0x1
#define HWIO_PMIC_ARB_SPMI_GENI_OUTPUT_CTRL_SOE0_EN_SHFT                                   0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000014)
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_RMSK                                            0x30f
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_PROG_RAM_SCLK_OFF_BMSK                          0x200
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_PROG_RAM_SCLK_OFF_SHFT                            0x9
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_PROG_RAM_HCLK_OFF_BMSK                          0x100
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_PROG_RAM_HCLK_OFF_SHFT                            0x8
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_SCLK_CGC_ON_BMSK                                  0x8
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_SCLK_CGC_ON_SHFT                                  0x3
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_DATA_AHB_CLK_CGC_ON_BMSK                          0x4
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_DATA_AHB_CLK_CGC_ON_SHFT                          0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_CFG_AHB_WR_CLK_CGC_ON_BMSK                        0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_CFG_AHB_WR_CLK_CGC_ON_SHFT                        0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_CFG_AHB_CLK_CGC_ON_BMSK                           0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CGC_CTRL_CFG_AHB_CLK_CGC_ON_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000018)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_RMSK                                          0xfff11
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_CHAR_MODE_BMSK                                0xff000
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_CHAR_MODE_SHFT                                    0xc
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_CHAR_STATUS_BMSK                                0xf00
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_CHAR_STATUS_SHFT                                  0x8
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_DIRECTION_BMSK                                   0x10
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_DIRECTION_SHFT                                    0x4
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ENABLE_BMSK                                       0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_CFG_ENABLE_SHFT                                       0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_ADDR(n)                                 (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000001c + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_RMSK                                        0xffff
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_MAXn                                             1
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_INI(n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_ADDR(n), HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_INMI(n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_ADDR(n), mask)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_OUTI(n,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_ADDR(n),val)
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_ADDR(n),mask,val,HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_INI(n))
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_DIN_ACTUAL_BMSK                             0xff00
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_DIN_ACTUAL_SHFT                                0x8
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_DOUT_DATA_DIN_EXP_BMSK                        0xff
#define HWIO_PMIC_ARB_SPMI_GENI_CHAR_DATA_n_DOUT_DATA_DIN_EXP_SHFT                         0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_ADDR                                           (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000024)
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_RMSK                                                  0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_GENI_DISABLE_BMSK                                     0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CTRL_GENI_DISABLE_SHFT                                     0x0

#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_ADDR                                         (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000028)
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_RMSK                                               0xff
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_STATUS_ADDR, HWIO_PMIC_ARB_SPMI_GENI_STATUS_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_STATUS_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_GENI_CMD_FSM_STATE_BMSK                            0xf0
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_GENI_CMD_FSM_STATE_SHFT                             0x4
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_NOT_USED_BITS_BMSK                                  0xe
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_NOT_USED_BITS_SHFT                                  0x1
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_GENI_CMD_ACTIVE_BMSK                                0x1
#define HWIO_PMIC_ARB_SPMI_GENI_STATUS_GENI_CMD_ACTIVE_SHFT                                0x0

#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_ADDR                                  (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000002c)
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_RMSK                                         0xf
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_ADDR, HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_TEST_BUS_SEL_BMSK                            0xf
#define HWIO_PMIC_ARB_SPMI_GENI_TEST_BUS_CTRL_TEST_BUS_SEL_SHFT                            0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000100)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_PRIM_CONTEXT_VEC_1_BMSK                     0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_PRIM_CONTEXT_VEC_1_SHFT                           0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_PRIM_CONTEXT_VEC_0_BMSK                        0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG0_PRIM_CONTEXT_VEC_0_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000104)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_PRIM_CONTEXT_VEC_3_BMSK                     0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_PRIM_CONTEXT_VEC_3_SHFT                           0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_PRIM_CONTEXT_VEC_2_BMSK                        0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG1_PRIM_CONTEXT_VEC_2_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000108)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_PRIM_CONTEXT_VEC_5_BMSK                     0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_PRIM_CONTEXT_VEC_5_SHFT                           0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_PRIM_CONTEXT_VEC_4_BMSK                        0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG2_PRIM_CONTEXT_VEC_4_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000010c)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_PRIM_CONTEXT_VEC_7_BMSK                     0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_PRIM_CONTEXT_VEC_7_SHFT                           0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_PRIM_CONTEXT_VEC_6_BMSK                        0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG3_PRIM_CONTEXT_VEC_6_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000110)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_PRIM_CONTEXT_VEC_9_BMSK                     0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_PRIM_CONTEXT_VEC_9_SHFT                           0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_PRIM_CONTEXT_VEC_8_BMSK                        0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG4_PRIM_CONTEXT_VEC_8_SHFT                           0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000114)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_PRIM_CONTEXT_VEC_11_BMSK                    0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_PRIM_CONTEXT_VEC_11_SHFT                          0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_PRIM_CONTEXT_VEC_10_BMSK                       0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG5_PRIM_CONTEXT_VEC_10_SHFT                          0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000118)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_PRIM_CONTEXT_VEC_13_BMSK                    0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_PRIM_CONTEXT_VEC_13_SHFT                          0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_PRIM_CONTEXT_VEC_12_BMSK                       0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG6_PRIM_CONTEXT_VEC_12_SHFT                          0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000011c)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_PRIM_CONTEXT_VEC_15_BMSK                    0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_PRIM_CONTEXT_VEC_15_SHFT                          0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_PRIM_CONTEXT_VEC_14_BMSK                       0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG7_PRIM_CONTEXT_VEC_14_SHFT                          0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000120)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_PRIM_CONTEXT_VEC_17_BMSK                    0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_PRIM_CONTEXT_VEC_17_SHFT                          0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_PRIM_CONTEXT_VEC_16_BMSK                       0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG8_PRIM_CONTEXT_VEC_16_SHFT                          0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_ADDR                                       (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000124)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_RMSK                                        0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_PRIM_CONTEXT_VEC_19_BMSK                    0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_PRIM_CONTEXT_VEC_19_SHFT                          0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_PRIM_CONTEXT_VEC_18_BMSK                       0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG9_PRIM_CONTEXT_VEC_18_SHFT                          0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000128)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_RMSK                                       0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_PRIM_CONTEXT_VEC_21_BMSK                   0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_PRIM_CONTEXT_VEC_21_SHFT                         0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_PRIM_CONTEXT_VEC_20_BMSK                      0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG10_PRIM_CONTEXT_VEC_20_SHFT                         0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000012c)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_RMSK                                       0xfffffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_PRIM_CONTEXT_VEC_23_BMSK                   0xfffc000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_PRIM_CONTEXT_VEC_23_SHFT                         0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_PRIM_CONTEXT_VEC_22_BMSK                      0x3fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG11_PRIM_CONTEXT_VEC_22_SHFT                         0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000130)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_RMSK                                          0x7fff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_COND_COMP_IN_VEC_BMSK                         0x7f80
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_COND_COMP_IN_VEC_SHFT                            0x7
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_DATA_CNT_MODE_BMSK                              0x40
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_DATA_CNT_MODE_SHFT                               0x6
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_DATA_CNT_EN_BITS_BMSK                           0x3c
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_DATA_CNT_EN_BITS_SHFT                            0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_EXT_ARB_EN_BMSK                                  0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_EXT_ARB_EN_SHFT                                  0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_PROG_RAM_SEC_WORD_EN_BMSK                        0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG12_PROG_RAM_SEC_WORD_EN_SHFT                        0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000134)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_RMSK                                         0x3ffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_TX_PARAM_TABLE_VEC_1_BMSK                    0x3fe00
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_TX_PARAM_TABLE_VEC_1_SHFT                        0x9
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_TX_PARAM_TABLE_VEC_0_BMSK                      0x1ff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG13_TX_PARAM_TABLE_VEC_0_SHFT                        0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000138)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_RMSK                                         0x3ffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_TX_PARAM_TABLE_VEC_3_BMSK                    0x3fe00
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_TX_PARAM_TABLE_VEC_3_SHFT                        0x9
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_TX_PARAM_TABLE_VEC_2_BMSK                      0x1ff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG14_TX_PARAM_TABLE_VEC_2_SHFT                        0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x0000013c)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_RMSK                                         0xfffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_TX_DATA_TABLE_VEC_1_BMSK                     0xffc00
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_TX_DATA_TABLE_VEC_1_SHFT                         0xa
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_TX_DATA_TABLE_VEC_0_BMSK                       0x3ff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG15_TX_DATA_TABLE_VEC_0_SHFT                         0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000140)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_RMSK                                         0xfffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_TX_DATA_TABLE_VEC_3_BMSK                     0xffc00
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_TX_DATA_TABLE_VEC_3_SHFT                         0xa
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_TX_DATA_TABLE_VEC_2_BMSK                       0x3ff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG16_TX_DATA_TABLE_VEC_2_SHFT                         0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000144)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_RMSK                                        0x3fffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_RX_SIN_SEL_BMSK                             0x380000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_RX_SIN_SEL_SHFT                                 0x13
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_SOE_EDGE_SEL_BMSK                         0x40000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_SOE_EDGE_SEL_SHFT                            0x12
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_SOUT_EDGE_SEL_BMSK                        0x20000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_SOUT_EDGE_SEL_SHFT                           0x11
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_PRIM_SOE_VALUE_BMSK               0x10000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_PRIM_SOE_VALUE_SHFT                  0x10
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_PRIM_SOUT_VALUE_BMSK               0x8000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_PRIM_SOUT_VALUE_SHFT                  0xf
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_SOE_VALUE_BMSK                     0x4000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_SOE_VALUE_SHFT                        0xe
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_SOUT_VALUE_BMSK                    0x2000
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_DEFAULT_SOUT_VALUE_SHFT                       0xd
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_CONST1_EFF_SIZE_BMSK                       0x1c00
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_CONST1_EFF_SIZE_SHFT                          0xa
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_CONST1_REG_BMSK                             0x3fc
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_CONST1_REG_SHFT                               0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_PAR_MODE_BMSK                                 0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_PAR_MODE_SHFT                                 0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_PAR_CALC_EN_BMSK                              0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG17_TX_PAR_CALC_EN_SHFT                              0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_ADDR                                      (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000148)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_RMSK                                           0xfff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IN          \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_ADDR, HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_INM(m)      \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_ADDR, m)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_OUT(v)      \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_ADDR,v)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_ADDR,m,v,HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IN)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_DRIVE_DEFAULT_ON_START_EN_BMSK                 0x800
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_DRIVE_DEFAULT_ON_START_EN_SHFT                   0xb
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_EXT_SECURITY_EN_BMSK                           0x400
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_EXT_SECURITY_EN_SHFT                             0xa
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_DATA_CNT_INIT_VALUE_INCR_BMSK                  0x200
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_DATA_CNT_INIT_VALUE_INCR_SHFT                    0x9
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_TX_THRESHOLD_BMSK                              0x180
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_TX_THRESHOLD_SHFT                                0x7
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_CONST_EFF_SIZE_BMSK                         0x40
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_CONST_EFF_SIZE_SHFT                          0x6
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_CONST_REG_BMSK                              0x30
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_CONST_REG_SHFT                               0x4
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_PRIM_SOE_VALUE_BMSK                  0x8
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_PRIM_SOE_VALUE_SHFT                  0x3
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_PRIM_SOUT_VALUE_BMSK                 0x4
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_PRIM_SOUT_VALUE_SHFT                 0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_SOE_VALUE_BMSK                       0x2
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_SOE_VALUE_SHFT                       0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_SOUT_VALUE_BMSK                      0x1
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_REG18_IO2_DEFAULT_SOUT_VALUE_SHFT                      0x0

#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_ADDR(n)                                    (PMIC_ARB_SPMI_SPMI_GENI_CFG_REG_BASE      + 0x00000200 + 0x4 * (n))
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_RMSK                                         0x1fffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_MAXn                                              255
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_INI(n)        \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_ADDR(n), HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_RMSK)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_INMI(n,mask)    \
        in_dword_masked(HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_ADDR(n), mask)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_OUTI(n,val)    \
        out_dword(HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_ADDR(n),val)
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_ADDR(n),mask,val,HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_INI(n))
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_TBD_BMSK                                     0x1fffff
#define HWIO_PMIC_ARB_SPMI_GENI_CFG_RAMn_TBD_SHFT                                          0x0


#endif /* __SPMIGENIHWIO_H__ */
