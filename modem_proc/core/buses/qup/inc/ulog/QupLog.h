#ifndef _QUPLOG_H_
#define _QUPLOG_H_
/*
===========================================================================

FILE:   QupLog.h

DESCRIPTION:
    This file contains the API for the QUP Log services 


===========================================================================

        Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/qup/inc/ulog/QupLog.h#1 $

When     Who    What, where, why
-------- ---    -----------------------------------------------------------
04/17/14   MS     Created


===========================================================================
        Copyright c 2012-2014 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Proprietary/GTDR

===========================================================================
*/

#include "QupTypes.h"
#include "stdarg.h"
#include "ULogFront.h"

typedef void* QUPLOG_DEV_IMP_HANDLE;



struct QupLogSvcType
{
   QUPLOG_DEV_IMP_HANDLE (*pfnGetReadHandle)(uint32 uDevAddr);
   QUPLOG_DEV_IMP_HANDLE (*pfnGetWriteHandle)(uint32 uDevAddr);
};

typedef struct QupLogSvcType QupLogSvcType;

extern QupLogSvcType _gQupLog;


#define QUPLOG_LOG_WRITE(address, args...) \
    if(_gQupLog.pfnGetWriteHandle(address) != NULL)\
    {\
       ULogFront_RealTimePrintf((ULogHandle) _gQupLog.pfnGetWriteHandle(address), ##args);\
    }


#define QUPLOG_LOG_READ(address, args...) \
    if(_gQupLog.pfnGetReadHandle(address) != NULL)\
    {\
       ULogFront_RealTimePrintf((ULogHandle) _gQupLog.pfnGetReadHandle(address), ##args);\
    }


#endif /*_QUPLOG_H_*/

