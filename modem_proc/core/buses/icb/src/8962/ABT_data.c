/*==============================================================================

FILE:      ABT_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.mpss/3.7.24/buses/icb/src/8962/ABT_data.c#1 $ 
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
$Change: 7351156 $ 

When        Who    What, where, why
----------  ---    ----------------------------------------------------------- 
2013/04/16  pm     Added interrupt priority
2012/10/04  av     Support for disabling ABT 
2012/05/31  av     Created
 
        Copyright (c) 2013 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "ABTimeout.h"
#include "ABTimeout_HWIO.h"

/*============================================================================
                      TARGET AND PLATFORM SPECIFIC DATA
============================================================================*/

/* Base address for devices */
#define ABT_MSSCFG_BASE_ADDR  MSS_CONF_BUS_TIMEOUT_REG_BASE

/* Bit Mask for ABT Slaves */
#define ABT_MSSCFG_BMSK   HWIO_TCSR_TIMEOUT_INTR_STATUS_MSS_CONFIG_TIMEOUT_SLAVE_IRQ_SHFT

/* ABT Slave CLK Name */
#define ABT_MSSCFG_CLK   "clk_bus_slave_timeout"

/* Timeout Interrupt Register Address */
#define ABT_TIMEOUT_INTR_MODEM_ENABLE  HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR
#define ABT_TIMEOUT_INTR_STATUS        HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR
#define ABT_TIMEOUT_SLAVE_GLB_EN       HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR

/* TCSR Summary Interrupt Vectors */
#define ABT_TCSR_MODEM_INTR_VECTOR     238

/* Modem Interrupt Priority (Note:  This varies with the processor, as DAL    */
/*                                  passes this through to the underlying     */
/*                                  kernel/OS                                 */
#define ABT_MODEM_INTR_PRIORITY     1


/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/*---------------------------------------------------------------------------*/
/*          Properties data for device ID  = "/dev/ABTimeout"                */
/*---------------------------------------------------------------------------*/

/* ABT Configuration Data*/
ABT_slave_info_type ABT_cfgdata[] = 
{ 
//ABT_SLAVE_INFO(  name, sl_en, int_en, to_val)
  ABT_SLAVE_INFO(MSSCFG,  TRUE,   TRUE,   0xFF),//  MODEM_CONFIG
};

/* ABT Platform Data type */
ABT_platform_info_type ABT_platform_info =
{
    "MSS",                                 /* Image name */
    (void*)ABT_TIMEOUT_INTR_MODEM_ENABLE,  /* INTR Enable address */
    (void*)ABT_TIMEOUT_INTR_STATUS,        /* INTR Status Register address */
    (void*)ABT_TIMEOUT_SLAVE_GLB_EN,       /* ABT Slave global en address */
    ABT_TCSR_MODEM_INTR_VECTOR,            /* ABT TCSR Summary interrupt vector */
    ABT_MODEM_INTR_PRIORITY,               /* ABT Modem Interrupt priority */
};


/* ABT Configuration Property Data*/
ABT_propdata_type ABT_propdata = 
{
    /* Length of the config  data array */
    sizeof(ABT_cfgdata)/sizeof(ABT_slave_info_type), 
    /* Pointer to config data array */ 
    ABT_cfgdata,
    /* Pointer to platform info data */ 
    &ABT_platform_info                                    
};

