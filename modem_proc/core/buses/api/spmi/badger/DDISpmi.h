#ifndef __DDISPMI_H__
#define __DDISPMI_H__
/*=========================================================================*/
/**
@file:  DDISpmi.h

@brief: This module provides the interface to the Spmi DAL driver software. 
It provides APIs to do I/O on SPMI Bus via SPMI controller as well 
as interface the interrupt controller within SPMI component
*/ 
/*=========================================================================

Edit History

$Header: //components/rel/core.mpss/3.7.24/buses/api/spmi/badger/DDISpmi.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
06/19/13   MQ      Added PPID to APID api
09/04/12   PS      Added ReadModifyWrite api support
08/28/12   PS      Added support to specify intrId as PPID. And support to
                   register for individual Extended Intr bits within a
                   peripheral
09/14/11   PS      Initial revision.

===========================================================================
             Copyright (c) 2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================	
*/ 
/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include "DalDevice.h"
#include "SpmiBus_Defs.h"
#include "SpmiIntrCtlr_Defs.h"

/*-------------------------------------------------------------------------
* Preprocessor Definitions and Constants
* ----------------------------------------------------------------------*/
/* Need to define the interface version format is (major,minor) */
#define DALSPMI_INTERFACE_VERSION DALINTERFACE_VERSION(1,0)

/*-------------------------------------------------------------------------
* Type Declarations
* ----------------------------------------------------------------------*/
typedef struct DalSpmi DalSpmi;
struct DalSpmi
{
    struct DalDevice DalDevice;
    DALResult (*SpmiReadLong)(DalDeviceHandle * _h, uint32 uSlaveId, SpmiBus_AccessPriorityType eAccessPriority, uint32 uRegisterAddress, unsigned char* pucData, uint32 uDataLen, uint32* puTotalBytesRead);
    DALResult (*SpmiWriteLong)(DalDeviceHandle * _h, uint32 uSlaveId, SpmiBus_AccessPriorityType eAccessPriority, uint32 uRegisterAddress, unsigned char* pucData, uint32 uDataLen);
    DALResult (*SpmiReadModifyWriteLongByte)(DalDeviceHandle * _h, uint32 uSlaveId, SpmiBus_AccessPriorityType eAccessPriority, uint32 uRegisterAddress, uint32 uData, uint32 uMask, uint8 *pucDataWritten);
    DALResult (*SpmiCommand)(DalDeviceHandle * _h, uint32 uSlaveId, SpmiBus_AccessPriorityType eAccessPriority, SpmiBus_CommandType eSpmiCommand);
    DALResult (*SpmiRegisterIsr)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 uExtendedIntrMask, const SpmiIsr isr, const SpmiIsrCtxt ctxt);
    DALResult (*SpmiUnregisterIsr)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 uExtendedIntrMask);
    DALResult (*SpmiInterruptEnable)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID);
    DALResult (*SpmiInterruptDisable)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID);
    DALResult (*SpmiIsInterruptEnabled)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 * bState);
    DALResult (*SpmiInterruptDone)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 uExtendedIntrMask);
    DALResult (*SpmiExtendedInterruptStatus)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 * puIntrStatusMask);
    DALResult (*SpmiExtendedInterruptClear)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID, uint32 uIntrClearMask);
    DALResult (*SpmiGetLogicalIdFromPpid)(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphId, uint32* pLogicalId);
};

typedef struct DalSpmiHandle DalSpmiHandle;
struct DalSpmiHandle
{
    uint32 dwDalHandleId;
    const DalSpmi * pVtbl;
    void * pClientCtxt;
};

/*-------------------------------------------------------------------------
* Function Declarations and Documentation
* ----------------------------------------------------------------------*/
#define DAL_SpmiDeviceAttach(DevId,hDalDevice)\
    DAL_StringDeviceAttachEx(NULL,DevId,DALSPMI_INTERFACE_VERSION,hDalDevice)

/**
@brief Reads from a SPMI slave device

This function reads data from SPMI Slave device. The register address
is in long(16 bit) format

@param[in] _h Dal Device Handle

@param[in] uSlaveId Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs to be sent
on the SPMI bus

@param[in] uRegisterAddress Register Address on the SPMI slave device to
which read is to be initiated. 16 LSB bit of this parameter are used as
Register address

@param[in] pucData Pointer to data array. Data read from the SPMI bus will
be filled in this array

@param[in] uDataLen Number of bytes to read

@param[out] puTotalBytesRead Pointer to an uint32 which is used to return
the total number of bytes read from SPMI device

@return SPMI_BUS_SUCCESS on success, error code from SpmiBus_ResultType enum
on error

@see DalSpmi_WriteLong()
*/
static __inline DALResult
    DalSpmi_ReadLong(DalDeviceHandle * _h,
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    unsigned char* pucData,
    uint32 uDataLen,
    uint32* puTotalBytesRead)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiReadLong(_h,
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen,
        puTotalBytesRead);
}


/**
@brief Writes to a SPMI slave device

This function writes data to SPMI Slave device. The register address
is in long(16 bit) format

@param[in] _h Dal Device Handle

@param[in] uSlaveId Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs to be
sent on the SPMI bus

@param[in] uRegisterAddress Register Address on the SPMI slave device to
which write is to be initiated. 16 LSB bit of this parameter are used as
Register address

@param[in] pucData Pointer to data array containing data to be written
on the bus.

@param[in] uDataLen Number of bytes to write

@return SPMI_BUS_SUCCESS on success, error code from SpmiBus_ResultType enum
on error

@see DalSpmi_ReadLong()
*/
static __inline DALResult
    DalSpmi_WriteLong(DalDeviceHandle * _h,
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    unsigned char* pucData,
    uint32 uDataLen)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiWriteLong(_h,
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        pucData,
        uDataLen);
}

/**
@brief Reads Modify Writes a byte from SPMI slave device

This function reads a byte from SPMI slave device, modifies it 
and writes it back to SPMI Slave device. The register address is 
in long(16 bit) format 

@param[in] _h Dal Device Handle

@param[in] uSlaveId  Slave Id of the device

@param[in] eAccessPriority Priority with which the command needs
to be sent on the SPMI bus

@param[in] uRegisterAddress  Register address on the SPMI 
slave device to which write is to be initiated. 16 LSB bit of this
parameter are used as Register address

@param[in] uData  Data byte to be read modify write 

@param[in] uMask  Mask of the bits which needs to be modified 

@param[out] pucDataWritten Pointer to an uint8 which is used 
to return the value of the byte written on the bus after after 
read modify write

@return  SPMI_BUS_SUCCESS on success, error code on error

@see None
*/
static __inline DALResult
    DalSpmi_ReadModifyWriteLongByte(DalDeviceHandle * _h,
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    uint32 uRegisterAddress,
    uint32 uData,
    uint32 uMask, 
    uint8 *pucDataWritten)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiReadModifyWriteLongByte(_h,
        uSlaveId,
        eAccessPriority,
        uRegisterAddress,
        uData,
        uMask, 
        pucDataWritten);
}


/**
@brief Send a command to SPMI slave device

This function sends a command to SPMI slave device

@param[in] _h Dal Device Handle

@param[in] uSlaveId Slave Id of the device

@param[in] eAccessPriority Priority with which the command
needs to be sent on the SPMI bus

@param[in] eSpmiCommand Command type

@return SPMI_BUS_SUCCESS on success, error code from SpmiBus_ResultType enum
on error

@see None
*/
static __inline DALResult
    DalSpmi_Command(DalDeviceHandle * _h,
    uint32 uSlaveId,
    SpmiBus_AccessPriorityType eAccessPriority,
    SpmiBus_CommandType eSpmiCommand)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiCommand(_h,
        uSlaveId,
        eAccessPriority,
        eSpmiCommand);
}


/**
@brief Regsiters an ISR

This function regsiters an ISR with SPMI Interrupt Controller
driver.

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means register for Intr bit 0 of this peripheral
0x3 means register for Intr bit 0 and bit 1 of this peripheral
0x5 means register for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are registering for all the interrupts
within this peripheral

@param[in] ctxt ISR context which will be copied as a
parameter on isr invocation

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see DalSpmi_UnregisterIsr()
*/
static __inline DALResult
    DalSpmi_RegisterIsr(DalDeviceHandle * _h,
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask,
    const SpmiIsr isr,
    const SpmiIsrCtxt ctxt)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiRegisterIsr(_h, 
        periphID, 
        uExtendedIntrMask, 
        isr, 
        ctxt);
}

/**
@brief Unregsiters an ISR

This function unregsiters an previoulsy registered ISR with
SPMI Interrupt Controller driver.

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits
Example
0x1 means unregister for Intr bit 0 of this peripheral
0x3 means unregister for Intr bit 0 and bit 1 of this peripheral
0x5 means unregister for Intr bit 0 and bit 2 of this peripheral
and so on. Every bit represents Intr id within a peripheral.
Mask of 0xFF means you are unregistering for all the interrupts
within this peripheral

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see DalSpmi_RegisterIsr()
*/
static __inline DALResult
    DalSpmi_UnregisterIsr(DalDeviceHandle * _h, 
    SpmiPhyPeriphIdType periphID, 
    uint32 uExtendedIntrMask)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiUnregisterIsr(_h, 
        periphID, 
        uExtendedIntrMask);
}

/**
@brief Enables an Interrupt

This function enables an Interrupt on SPMI Interrupt
Controller

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see DalSpmi_InterruptDisable()
*/
static __inline DALResult
    DalSpmi_InterruptEnable(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiInterruptEnable(_h, periphID);
}

/**
@brief Disables an Interrupt

This function disables an Interrupt on SPMI Interrupt Controller

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see DalSpmi_InterruptEnable()
*/
static __inline DALResult
    DalSpmi_InterruptDisable(DalDeviceHandle * _h, SpmiPhyPeriphIdType periphID)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiInterruptDisable(_h, periphID);
}

/**
@brief Is Interrupt Enabled

This function returns whether or not an Interrupt is enabled
on SPMI Interrupt Controller

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] bState Pointer to a variable which will hold the
return value of TRUE or FALSE

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see DalSpmi_InterruptEnable(),
DalSpmi_InterruptDisable()
*/
static __inline DALResult
    DalSpmi_IsInterruptEnabled(DalDeviceHandle * _h,
    SpmiPhyPeriphIdType periphID,
    uint32 *bState)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiIsInterruptEnabled(_h, periphID, bState);
}

/**
@brief Interrupt Done

This function needs to be called by the client when Interrupt
processing is done for this interrupt id.

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uExtendedIntrMask Mask of extended interrupt bits

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None
*/
static __inline DALResult
    DalSpmi_InterruptDone(DalDeviceHandle * _h, 
    SpmiPhyPeriphIdType periphID,
    uint32 uExtendedIntrMask)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiInterruptDone(_h, 
        periphID, 
        uExtendedIntrMask);
}

/**
@brief Get the Extended Interrupt Status

This function returns the extended Interrupt Status associated with
the periphID

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[out] puIntrStatusMask Pointer to the uint32 which will be
populated by this function reflecting the status of the extended
interrupts. Each set bit in this parameter, represents that
extended Interrupts is set.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on
error

@see None
*/
static __inline DALResult
    DalSpmi_ExtendedInterruptStatus(DalDeviceHandle * _h,
    SpmiPhyPeriphIdType periphID,
    uint32 * puIntrStatusMask)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiExtendedInterruptStatus(
        _h,
        periphID,
        puIntrStatusMask);
}

/**
@brief Clear the Extended Interrupt Status

This function clears the extended Interrupt Status associated
with the periphID

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] uIntrClearMask This is a mask of bits which
represents Interrupts to be cleared.

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None
*/
static __inline DALResult
    DalSpmi_ExtendedInterruptClear(DalDeviceHandle * _h,
    SpmiPhyPeriphIdType periphID,
    uint32 uIntrClearMask)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiExtendedInterruptClear(
        _h,
        periphID,
        uIntrClearMask);
}

/**
@brief Get the Logical(APID) from a physical peripheral Id (PPID)

This function returns the logical ID of a peripheral from the given
PPID

@param[in] _h Dal Device Handle

@param[in] periphID Physical Peripheral address. 12 bits (4 bit
SID + 8 bit upper peripheral address)

@param[in] pLogicalId Pointer to the uint32 which will be
populated by this function. The value will be the corresponding APID

@return SPMI_INTR_CTLR_SUCCESS on success, error code on error

@see None
*/
static __inline DALResult DalSpmi_GetLogicalIdFromPpid(DalDeviceHandle*  _h,
                                                       SpmiPhyPeriphIdType periphID,
                                                       uint32* pLogicalId)
{
    return ((DalSpmiHandle *)_h)->pVtbl->SpmiGetLogicalIdFromPpid(
        _h,
        periphID,
        pLogicalId);
}

#endif /* __DDISPMI_H__ */
