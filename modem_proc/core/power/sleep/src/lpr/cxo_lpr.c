/*============================================================================
  FILE:         cxo_lpr.c

  OVERVIEW:     This file provides the LPR definition for the CXO clock 
                low-power modes.

  DEPENDENCIES: None

                Copyright (c) 2012-2013 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.7.24/power/sleep/src/lpr/cxo_lpr.c#1 $
$DateTime: 2015/01/27 06:04:57 $
==============================================================================*/
#include "rpm.h"
#include "rpmclient.h"
#include "kvp.h"
#include "mpmint.h"

/*===========================================================================
 *              INTERNAL VARIABLES AND MACROS
 *==========================================================================*/
static kvp_t* p_cxo_dis_kvp;
static kvp_t* p_cxo_en_kvp;

static void cxo_enab_set_1(void)
{
  kvp_reset(p_cxo_en_kvp);
  rpm_post_request(RPM_SLEEP_SET, RPM_CLOCK_0_REQ /* 'clk0' is for 'sources' */, /*cxo == source */ 0, p_cxo_en_kvp);
}

void cxo_shutdown_init( void )
{
  const uint32 cxo_dis_req = 0;
  const uint32 cxo_en_req = 1;

  p_cxo_dis_kvp = kvp_create(4*3);
  p_cxo_en_kvp = kvp_create(4*3);

  kvp_put(p_cxo_dis_kvp, 0x62616e45 /* 'Enab' request */, sizeof(cxo_dis_req), (void *)&cxo_dis_req);
  kvp_put(p_cxo_en_kvp, 0x62616e45 /* 'Enab' request */, sizeof(cxo_en_req), (void *)&cxo_en_req);

  /* Clock Driver's Remote CXO node sets Sleep Set Enab kvp to 0. Override it here during init */
  cxo_enab_set_1();
  return;
}

void cxo_shutdown_enter( uint64 wakeup_tick )
{
  kvp_reset(p_cxo_dis_kvp);
  rpm_post_request(RPM_SLEEP_SET, RPM_CLOCK_0_REQ /* 'clk0' is for 'sources' */, /*cxo == source */ 0, p_cxo_dis_kvp);
  return;
}

void cxo_shutdown_exit( void )
{
  cxo_enab_set_1();
  mpmint_trigger_interrupts();
  return;
}

