/**********************************************************************
 * Qdsp_classic.c
 *
 * This module implements the qdsp_qdsp_classic algorithm for
 * Dynamic Clock and Voltage Scaling(DCVS).
 *
 * EXTERNALIZED FUNCTIONS
 *
 * Copyright (C) 2009 - 2014 by Qualcomm Technologies, Inc.
 * All Rights Reserved.
 *
 **********************************************************************/
/*=======================================================================
                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/dcvs/src/common/algorithms/qdsp_classic/qdsp_classic.c#1 $
$DateTime: 2015/01/27 06:04:57 $

when       who     what, where, why
--------   ----    --------------------------------------------------- 
05/16/13   nj      Replaced the value to a macro for high busy MIPS requirement. 
04/11/13   sg      Updated algo to account for cycles wasted on L2 misses.
12/07/12   nj      Minor updates to ULog print messages
11/28/12   nj      Fixed a ULog printing bug
10/01/12   nj      Fixed ULog printing format for 64 bits
09/06/12   nj      Detecting PMU available and restarting sampling window 
09/26/12   nj      Fixed a bug in calculating OneThreadBusy percentage 
09/15/12   nj      Added code to deal with concurrent PMU usage
09/10/12   nj      INTLOCK/INTFREE sections when reading PMU 
09/10/12   nj      Corrected ULOG printing for 64 bit 
08/30/12   nj      Added New Scheme based Profiling on top of existing scheme 
                   Added ClkChgCB
08/29/12   nj      Fixed bug in 'old code' where timer is restarted from ClkChgCB 
                   w/o the updated timer interval value (always SAMPLE PERIOD was used) 
08/23/12   nj      Added PMU Profiling code 
03/29/12   nj      Added header macros that include target specific include files 
03/23/12   nj      Changed include files to be compatible with 8974
09/24/11   sg      Declare extern requestStateVector.
09/19/11   ss      Removing the notifications into the algorithm when the state of the 
                   clock changes. 
09/15/11   ss      Restarting the timers so that DCVS fires every sample period interval
                   taking into account the clock changes.
09/12/11   ss      Correcting a bug in identifying dedicated and non dedicated threads.
                   Adding additional logging for vector requests.
04/26/11   ss      Fixing Q6 compiler warnings.
04/14/11   ss      Fixing the system profiling functionality.
02/28/11   bjs     Initial check in.
========================================================================*/
#include "BuildFeatures.h"
#ifndef DEFEATURE_DCVS_ALGO_QDSP_CLASSIC

#include "dcvs_core.h"
#include "dcvs_os.h"
#include "dcvs_utils.h"
#include "dcvs_algorithm.h"
#include "classic.h"
#include "dcvs_npa.h"
#include "dcvs_nv.h"
#include "ULogFront.h"
#include "npa.h"
#include "CoreVerify.h"
#include "npa_resource.h"
#include "max_stub.h"
#include "blast.h"
#include "msg.h"
#include "CoreTime.h"
#include "CLM.h"

#include DCVS_OS_HEADER1_H
#include DCVS_OS_HEADER2_H


typedef enum
{
  DCVS_PARAMS_ALG_QDSP_CLASSIC_SAMPLE_PERIOD = 0,
  DCVS_PARAMS_ALG_QDSP_CLASSIC_HIGH_THRESHOLD_BUSY_PCT,
} DCVS_PARAMS_ALG_QDSP_CLASSIC;

static const DCVS_NV_PARAM_ENTRY dcvsParamsAlgDefault[] =
{
  {"SamplePeriod", DCVS_NV_PARAM_UINT32, {(void*)50}},
  {"HighThresholdBusyPct", DCVS_NV_PARAM_UINT32, {(void *)90}},
  DCVS_NV_PARAM_LIST_END
};

#define DCVS_ALGO_QDSP_CLASSIC_HIGH_WATERMARK_SAFETY (10)

/* Used for identifing a truly busy use-case to skip the L2 cache cycles reduction */ 
#define CPU_MAXED_OUT_PCT 97

/* Macro for multiplication by 10 .
   Multiply the number by 8 and 2 and add the sum . */
#define MUL_TEN(x) \
  ((x<<3)+(x<<1))

/* Macro for PMU Event config register value */
// 03=MPPS; a0=ANY_DU_STALLS; 9f==DU_STALL_CYCLES 77=L2_IU_MISS; 
#define PMU_EVT_CONFIG_VAL 0x03a09f77

/* Macro for PMU config register value */
#define PMU_CONFIG_VAL 7


/* Structure to maintain the low and high thresholds for each clock level in OAL. */
typedef struct
{
  /* The low threshold for a clock level in terms of busy percentage */
  uint32 lowThresholdBusyPct;

}DCVS_ALGO_QDSP_CLASSIC_CLK_THRESHOLD;


/* Static variable to store the context handle */
static DCVS_CONTEXT* localCoreContext = NULL;

/* Default DCVS Algorithm specific members */
typedef struct
{
  /* Members specific to the default DCVS algorithm */
  ULogHandle logHandle;

  /* mips calculated by the OAL portion of the algorithm. */
  uint32 mips;

  /* Structure which stores the clock plan for the particular target */
  DCVS_TARGET_CLOCK_PLAN_INFO_TYPE* clockPlan;

  /* Handle to CLM client*/
  uint32  clmHandle;  

  /* The high threshold busy percentage from the NV .*/
  uint32 highThresholdBusyPct;

  /* Structure to maintain the low and high thresholds for each clock level in OAL. */
  DCVS_ALGO_QDSP_CLASSIC_CLK_THRESHOLD* thresholdTable;

  /* The current index for the clock plan as calculated by this DCVS algorithm/NPA Requests */
  uint32 clockPlanCurrIndex;

  /* Index for the clock plan determined by DCVS algorithm output ONLY*/
  uint32 clockPlanDCVSTimerIndex;
  
  /* Stores the total time elapsed in milli-second units*/
  uint32 totalTimeElapsed;

  /*CPU utilization percentage based on the clock rate when thewindow ended 
    Range: 0..100 */
  uint32 utilPctAtCurrClk;

  /* # of pcycles that were  used to execute instructions since last update. */
  uint64 utilizedPcycles; 

  /*The sample period parameter to ClmRegister*/
  uint32   samplePeriodMs;

}DCVS_ALGO_QDSP_CLASSIC;


/**
 * Determines the clock level to use based on the current utilization stats
 * @param busyPcycles 
 * @param context 
 * @return uint32 : clk level recommended
 */
static inline uint32 DCVSFindClockLevelForMeasuredUtil(
   uint64                   busyPcycles,
   DCVS_CONTEXT           * context
)
{
  uint32 nearestLevel;

  DCVS_ALGO_QDSP_CLASSIC * algoDataContext =
    (DCVS_ALGO_QDSP_CLASSIC *)context->algoDataContext;

  uint64 pcyclesScaledToFullSecond =
    1000 * busyPcycles / (uint64)algoDataContext->totalTimeElapsed;

  ULogFront_RealTimePrintf(
     algoDataContext->logHandle, 4,
     "QDSPCLASSIC (RawBusyPcycles: 0x%x) (ScaledPcycles: 0x%x%x) (elapsed: %d)",
     (uint32)busyPcycles,
     DCVS_ULOG64_DATA(pcyclesScaledToFullSecond),
     algoDataContext->totalTimeElapsed);

  // find the lowest clock level that supports the utilized level
  // nearestLevel will be at MAX if we fall thru the loop
  for (nearestLevel = 0;
       nearestLevel < algoDataContext->clockPlan->numLevels-1; nearestLevel++)
  {
    if ((algoDataContext->clockPlan->clockPlanTable[nearestLevel].cpuFreq * 1000) >=
        pcyclesScaledToFullSecond)
    {
      break;
    }
  }

  if (nearestLevel < algoDataContext->clockPlan->numLevels-1)
  {
     //if loop level is greater than the current index, check high threshold
    if (nearestLevel >= algoDataContext->clockPlanCurrIndex)
    {
      uint32 nearestClk90pctCycles =
        // cpuFreqKHz * 1000 * highThresholdBusyPct / 100
        algoDataContext->clockPlan->clockPlanTable[nearestLevel].cpuFreq *
         algoDataContext->highThresholdBusyPct * 10;

      // Jump to the next level if we've exceeded the high threshold.
      if (pcyclesScaledToFullSecond >= nearestClk90pctCycles)
      {
        nearestLevel++;
      }
      // else nearestLevel is good
    }
    else
    {
      uint32 lowThresCyclesForCurrLevel =
        // cpuFreqKHz * 1000 * lowThresholdBusyPct / 100
        algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanCurrIndex].cpuFreq *
        algoDataContext->thresholdTable[algoDataContext->clockPlanCurrIndex].lowThresholdBusyPct * 10;

      if (pcyclesScaledToFullSecond > lowThresCyclesForCurrLevel)
      {
        nearestLevel = algoDataContext->clockPlanCurrIndex;
      }
      // else go with nearestLevel
    }
  }
  // else we'll use MAX clk
  return nearestLevel;
}

/**
  @brief DCVSRunQdspClassic

  This function is used to run the actual qdsp_classic 
  algorithm. 
   
  This function is run every time the DCVSTimer expires 
  (ie:every SamplePeriod). We calculate the Budget PCycles that
  were available in the last sample period. then get the
  profiling data by reading the PMU Registers that tell us how
  for how many cycles did atleast one HW Thread run for. Based 
  on this, we calculate the busy percentage of the time where
  atleast one HW thread was running. Then compare this % with 
  high and low thresholds � if greater than high threshold, step 
  up the clock index (Performance Level) by 1, if lower than low 
  threshold, calculate the new clock index (Performance Level) 
  that atleast meets the clock frequency based on the busy 
  percentage. Restart the DCVSTimer with the SamplePeriod time 
  Return the mips based on the above caclulated clock plan index 

  @param context: The DCVS data context.

  @return : The mips calculated by the algorithm.

*/

static uint32 DCVSRunQdspClassic(DCVS_CONTEXT *context)
{
  // This should run every time the timer is fired.  It is run from an
  // asynchronous client, so already has the lock.
  // Never allow this to drop below activeFloor

  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC *algoDataContext = context->algoDataContext;

  /* Variable to hold the clock plan index before the algorithm is executed to check if the index
  changed . */
  uint32 clockPlanOldIndex = algoDataContext->clockPlanCurrIndex;

  DCVSOSCriticalSectionEnter(context);

  uint64 busyPcycles = algoDataContext->utilizedPcycles;

  algoDataContext->clockPlanCurrIndex = DCVSFindClockLevelForMeasuredUtil( busyPcycles, context );
  algoDataContext->clockPlanDCVSTimerIndex = algoDataContext->clockPlanCurrIndex;
  if(algoDataContext->clockPlanDCVSTimerIndex >= algoDataContext->clockPlan->numLevels)
  {
    algoDataContext->clockPlanDCVSTimerIndex = algoDataContext->clockPlan->numLevels - 1;
  }

  /* Check to see if the clock rate has changed or the active floor has changed . */
  if ((context->resDataContext->activeFloor > 0) || (clockPlanOldIndex != algoDataContext->clockPlanCurrIndex))
  {
    uint32 i;
    /* We make sure that the algorithm mips is greater than the active floor. Start from the current index
       because we are sure to be above the active floor till now. */
    for (i = algoDataContext->clockPlanCurrIndex; i < algoDataContext->clockPlan->numLevels; i++)
    {
      if (algoDataContext->clockPlan->clockPlanTable[i].reqMips >= context->resDataContext->activeFloor)
      {
        break;
      }
    }
    if (i >= algoDataContext->clockPlan->numLevels)
    {
      i = algoDataContext->clockPlan->numLevels - 1;
    }
    algoDataContext->clockPlanCurrIndex = i;
  }

  DCVSOSCriticalSectionExit(context);

  // Return current mips.
  return (algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanCurrIndex].reqMips);

}

/**
  @brief DCVSImpulseQdspClassic

  This function is used to return the max mips of the target in response to the impulse client 
  request.

  @param  : DCVS data context.

  @return : The max mips supported by the target.

*/
static uint32 DCVSImpulseQdspClassic(DCVS_CONTEXT* context)
{
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = (DCVS_ALGO_QDSP_CLASSIC*)context->algoDataContext;

  algoDataContext->clockPlanCurrIndex = algoDataContext->clockPlan->numLevels - 1;

  // Return current mips.
  return (algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanCurrIndex].reqMips);
}

/**
  @brief DCVSFloorQdspClassic

  This function is used to reset the members of the qdsp_classic algorithm.

  @param  : DCVS data context.

  @return : The max mips supported by the target.

*/
static uint32 DCVSFloorQdspClassic (DCVS_CONTEXT* context, uint32 floor)
{
/* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = context->algoDataContext;

  uint32 result = algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanDCVSTimerIndex].reqMips;
  // This will be run from a required client request, so it already
  // has the lock.
  //

  if (algoDataContext->mips < floor)
  {
    result = floor;
  }
  else
  {
    // Result should be max of DCVS floor and DCVS algorithm recommended MIPS.
    if(result < floor)
    {
      result = floor;
    }
    else
    {	
      //update algoDataContext->clockPlanCurrIndex to clockPlanDCVSTimerIndex because it's DCVS algo output 
      // deciding the mips as floor is less
      algoDataContext->clockPlanCurrIndex = algoDataContext->clockPlanDCVSTimerIndex;
    }
	
  }

  return (result);
}

/**
  @brief DCVSAlgoQdspClassicReset

  This function is used to reset the members of the qdsp_classic algorithm.

  @param  : DCVS data context.

  @return : The max mips supported by the target.

*/
static uint32 DCVSAlgoQdspClassicReset(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = (DCVS_ALGO_QDSP_CLASSIC*)coreContext->algoDataContext;

  /* Return max mips. */
  algoDataContext->clockPlanCurrIndex = algoDataContext->clockPlan->numLevels - 1;
  algoDataContext->clockPlanDCVSTimerIndex = algoDataContext->clockPlan->numLevels - 1;
  return (algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanCurrIndex].reqMips);
}

/**
  @brief DCVSAlgoQdspClassicThresholdTableSetup

  This function is used to calculate the low and high threshold
  values in terms of busy percentage for each clock level.

  @param  context: The DCVS data context.

  @return : None

*/
static void DCVSAlgoQdspClassicThresholdTableSetup(DCVS_CONTEXT* context)
{
  uint32 currClkMips, prevClkMips, highWaterMarkIdle, lowWaterMarkBusy;
  int32 i;
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = context->algoDataContext;

  /* Allocate the memory for threshold table information for the resources */
  if (DALSYS_Malloc(sizeof(DCVS_ALGO_QDSP_CLASSIC_CLK_THRESHOLD) * (algoDataContext->clockPlan->numLevels),
                    (void **)&algoDataContext->thresholdTable)
      != DAL_SUCCESS)
  {
    /* Error fatal */
    CORE_VERIFY(NULL);
  }

  /* Precalculate the low/high thresholds for the busy percentage for different clock levels
     for the algorithm */
  for (i = algoDataContext->clockPlan->numLevels - 1; i >= 0; i--)
  {
    /* Calculate the HIGH threshold for the clock level
       High threshold busy pct is fixed according to the NV input. */

    /* Calculate the LOW threshold for the clock level
       High threshold is the current clock level is input from the NV . */

    if (i == 0)
    {
      algoDataContext->thresholdTable[i].lowThresholdBusyPct = 0;
    } 
    else
    {
      currClkMips = algoDataContext->clockPlan->clockPlanTable[i].reqMips;
      prevClkMips = algoDataContext->clockPlan->clockPlanTable[i - 1].reqMips;
      /* Calculate the high watermark percent for idle for this clock level */
      highWaterMarkIdle = (((currClkMips - prevClkMips) * 100) / currClkMips) + DCVS_ALGO_QDSP_CLASSIC_HIGH_WATERMARK_SAFETY;
      /* The low watermark percentage for busy for this clock level */
      lowWaterMarkBusy = 100 - highWaterMarkIdle;
      algoDataContext->thresholdTable[i].lowThresholdBusyPct = lowWaterMarkBusy;
    }
  }
}


/**
  @brief DCVSAlgoQdspClassicInitialize

  This function is used to initialize the qdsp_classic DCVS algorithm specific members .

  @param  context: DCVS data context.

  @return : Initial mips of the system

*/
static uint32 DCVSAlgoQdspClassicInitialize(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  
  CORE_VERIFY(coreContext);
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext;

  /* Allocate the memory for the algorithm data context. */
  if (DALSYS_Malloc(sizeof(DCVS_ALGO_QDSP_CLASSIC),
                    (void **)&coreContext->algoDataContext)
      != DAL_SUCCESS)
  {
    /* Error fatal */
    CORE_VERIFY(NULL);
  }

  algoDataContext = (DCVS_ALGO_QDSP_CLASSIC*)coreContext->algoDataContext;

  algoDataContext->logHandle = DCVSCoreLogHandleGet(coreContext);

  CORE_VERIFY(algoDataContext->logHandle);

  /* Initalize local variable to point to core context handle */
  localCoreContext = coreContext;

  if (!localCoreContext) 
  {
    CORE_VERIFY(NULL);
  }

  /* Get the current clock plan of the target */
  algoDataContext->clockPlan = DCVSCoreClockPlanGet(coreContext);

  // Start at the max
  algoDataContext->clockPlanCurrIndex = algoDataContext->clockPlan->numLevels - 1;
  algoDataContext->clockPlanDCVSTimerIndex = algoDataContext->clockPlan->numLevels - 1;

  /* Set up the threshold table for each clock level. */
  DCVSAlgoQdspClassicThresholdTableSetup(coreContext);

  /* Set the inital mips to max */
  algoDataContext->mips = DCVSAlgoQdspClassicReset(coreContext);

  return (algoDataContext->mips);
}


/**
  @brief DCVSAlgoQdspClassicBuildParams

  This function is used to get the parameters for the qdsp_classic DCVS algorithm.

  @param  : DCVS data context.

  @return : None

*/
static void DCVSAlgoQdspClassicBuildParams(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = coreContext->algoDataContext;

  /* Read the timer period */
  DcvsNvParamUint32Read(0,
                        "Default",
                        &dcvsParamsAlgDefault[DCVS_PARAMS_ALG_QDSP_CLASSIC_SAMPLE_PERIOD],
                        &algoDataContext->samplePeriodMs);
  DcvsNvParamUint32Read(0,
                        "Default",
                        &dcvsParamsAlgDefault[DCVS_PARAMS_ALG_QDSP_CLASSIC_HIGH_THRESHOLD_BUSY_PCT],
                        &algoDataContext->highThresholdBusyPct);

} /* DCVSAlgoQdspClassicBuildParams */


static void DCVSAlgoQdspClassicCLMClient_callback( CLM_LoadInfoExtendedStructType *clmInfo,
                                                   void *clientData )
{

  DCVS_CONTEXT  *context = (DCVS_CONTEXT *)clientData ;
  DCVS_ALGO_QDSP_CLASSIC * algoDataContext = context->algoDataContext;

  algoDataContext->utilizedPcycles = clmInfo->utilizedPcycles;
  algoDataContext->totalTimeElapsed = clmInfo->elapsedTimeUs / 1000; 

  if( clmInfo->updateReason == CLM_UPDATE_REASON_PERIODIC )
  {
    ULogFront_RealTimePrintf( context->coreDataContext->logHandle, 4, "CLM_UPDATE "
                              "(UtilAtCurr: %d) "
                              "(UtilAtMax:  %d) "
                              "(UpdatedAfter: %d) "
                              "(ClkKhz:  %d) "
                         	    ,clmInfo->utilPctAtCurrClk
                              ,clmInfo->utilPctAtMaxClk
                              ,clmInfo->elapsedTimeUs
                              ,clmInfo->currentClkKhz );

    /* The DCVS sampling timer has fired calculate the new mips of the QDSP classic algorithm */
    DCVSIssueInternalRequest( context );
  }

  /* The DCVS timer is reduced to 5ms when coming out of PC. This is to ensure
     that we will run in a standby case where PC is occuring frequently */
  if ( clmInfo->measurementPeriod == CLM_PC_EXIT_REDUCED_PERIOD ) 
{
    CLM_SwitchClientMeasurementPeriod( algoDataContext->clmHandle, algoDataContext->samplePeriodMs * 1000 );
  }
}


/**
  @brief DCVSAlgoQdspClassicEnable

  This function is used to set the state of the qdsp_classic algorithm specific members
  when enabling the algorithm.

  @param context: DCVS data context.

  @return : The mips of the system after enabling the algorithm.

*/
static uint32 DCVSAlgoQdspClassicEnable(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = coreContext->algoDataContext;

  CORE_VERIFY( algoDataContext->clmHandle =
               CLM_RegisterPeriodicClient( "DCVS", CLM_CLIENT_EXTENDED_CPUUTIL,
                                           (algoDataContext->samplePeriodMs) * 1000,
                                           (CLM_ATTRIBUTE_RESET_MEAS_ON_WAKEUP
                                            | CLM_ATTRIBUTE_REDUCED_PERIOD_ON_PC_EXIT), 
                                           (CLM_CallbackFuncPtr)DCVSAlgoQdspClassicCLMClient_callback, coreContext) );

  algoDataContext->mips = 
    algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlan->numLevels - 1].reqMips;
  return (algoDataContext->mips);
}


/**
  @brief DCVSAlgoQdspClassicDisable

  This function is used to reset the state of the qdsp_qdsp_classic algorithm specific members
  when disabling the algorithm.

  @param context: DCVS data context.

  @return : The mips of the system after disabling the algorithm.

*/
static uint32 DCVSAlgoQdspClassicDisable(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = coreContext->algoDataContext;
 
  /* Return max mips */
  return (algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlan->numLevels - 1].reqMips);
}

/**
  @brief DCVSAlgoQdspClassicDecideMips

  This function is used to incorporate external requests by the clients into the
  algorithm. The supported requests are impulse, floor etc.

  @param  context: DCVS data context.

  @return : Mips required

*/
static uint32 DCVSAlgoQdspClassicDecideMips(void* context)
{
  DCVS_CONTEXT* coreContext = (DCVS_CONTEXT*)context;
  /* Obtain the client context from the resource. */
  DCVS_RES_DATA* resDataContext = coreContext->resDataContext;

  /* Obtain the algorithm data context. */
  DCVS_ALGO_QDSP_CLASSIC* algoDataContext = (DCVS_ALGO_QDSP_CLASSIC*)coreContext->algoDataContext;

  if (resDataContext->algoMipsChangedOrTimerFired)
  {
    // Clear the timer fired flag here.
    resDataContext->algoMipsChangedOrTimerFired = FALSE;
    algoDataContext->mips = DCVSRunQdspClassic(context);
  }
  /* If there is an impulse request, send it to the OAL component. */
  else if (resDataContext->activeImpulse)
  {
    // Clear the impulse request flag here.
    resDataContext->activeImpulse = FALSE;
    algoDataContext->mips = DCVSImpulseQdspClassic(context);
  }
  // If it is not a timer, or an impulse, then the floor is being adjusted.
  else
  {
    algoDataContext->mips = DCVSFloorQdspClassic(context, resDataContext->activeFloor);
  }

  /* Logging for qdsp_classic algorithm */
  ULogFront_RealTimePrintf(algoDataContext->logHandle,
                           3,
                           "QDSPCLASSIC (UtilLevel: %d)(Floor: %d)(PerfLevel: %d)",
                           algoDataContext->clockPlan->clockPlanTable[algoDataContext->clockPlanCurrIndex].reqMips,
                           resDataContext->activeFloor, 
                           algoDataContext->mips);

  return algoDataContext->mips;
} /* DCVSAlgoQdspClassicDecideMips */


DCVS_CORE_ALGO qdspClassicAlgorithm =
{
  "qdsp_classic",
  dcvsParamsAlgDefault,
  DCVSAlgoQdspClassicInitialize,
  DCVSAlgoQdspClassicBuildParams,
  DCVSAlgoQdspClassicDecideMips,
  DCVSAlgoQdspClassicReset,
  DCVSAlgoQdspClassicEnable,
  DCVSAlgoQdspClassicDisable
};
#endif //DEFEATURE_DCVS_ALGO_QDSP_CLASSIC

