#ifndef DCVS_ALG_FIXED_H
#define DCVS_ALG_FIXED_H

/********************************************************************
* fixed.h
*
* This module has the data for DCVS fixed algorithm.
*
* EXTERNALIZED FUNCTIONS
*
* Copyright (C) 2009 - 2010 by Qualcomm Technologies, Inc.
* All Rights Reserved.
*
**********************************************************************/
/*=======================================================================
                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/dcvs/src/common/algorithms/fixed/fixed.h#1 $ 
$DateTime: 2015/01/27 06:04:57 $

when       who     what, where, why
--------   ----    ---------------------------------------------------
11/24/09   ss      Initial check in.
01/20/10   bjs     Cleanup
========================================================================*/

// NV Item Definitions for this algorithm
//
// Type = 2
// Param 0 = Desired MIPS
// Param 1-9 = N/A
//

#include "dcvs_oal_if.h"

// Add and fill in if algorithm specific parameters are required.
// OAL <- Kernel Write
// OAL -> Kernel Read
// OAL Write -> Kernel
//
//typedef enum 
//{
//} DCVS_ALG_ID_FIXED;

#endif // DCVS_ALG_FIXED_H
                          
