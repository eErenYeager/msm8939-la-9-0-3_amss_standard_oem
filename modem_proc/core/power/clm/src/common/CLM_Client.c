/*=============================================================================
FILE:         CLM_Client.c

DESCRIPTION:  Implementation of CLM Client layer of the CPU Load Monitor. It 
implements the APIs defined in CLM.h
 
 
 
=========================================================================== 
Copyright � 2013-2014 QUALCOMM Technologies, Incorporated. All Rights Reserved. 
QUALCOMM Confidential and Proprietary. 
 
===========================================================================

                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/clm/src/common/CLM_Client.c#1 $

=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "DALSys.h"
#include "CLM.h"
#include "CLM_Engine.h"
#include "CoreVerify.h"
#include "CoreMutex.h"
#include "ULogFront.h"

/*----------------------------------------------------------------------------
 * Data Definitions
 * -------------------------------------------------------------------------*/

/* ULog Handle */
ULogHandle clmUlogHandle = NULL;

/*----------------------------------------------------------------------------
 * Internal Function Definitions
 * -------------------------------------------------------------------------*/

/** 
 *   <!-- CLM_UpdateUtilFn --> 
 * 
 *   @brief Creates a local payload struct that contains the
 *   measurement information from the engine layer which is then
 *   passed in as an argument to the external client's callback
 *   function
 * 
 *   @param client : pointer to the Client specific data
 *
 *   @param clmEngineCalc : Struct contains engine calculations
 *
 *   @param updateReason : enum values from
 *                        CLM_UpdateReasonEnumType
 * 
 *   @return None 
 */
static void CLM_UpdateUtilFn( CLM_Client * clmClientData,
                              CLM_EngineCalculation * clmEngineCalc,
                              CLM_UpdateReasonEnumType updateReason )
{
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 i;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();
#endif

  if ( clmClientData->clientType == CLM_CLIENT_BASIC_CPUUTIL )
  {
    //create a local payload struct
    CLM_LoadInfoBasicStructType * loadInfo = clmClientData->loadInfo;


    loadInfo->updateReason = updateReason;
    loadInfo->currentClkKhz = clmEngineCalc->currentClkKhz;

    loadInfo->utilPctAtCurrClk = clmClientData->busyPctCurrClk;
    loadInfo->utilPctAtMaxClk = clmClientData->busyPctMaxClk;
    loadInfo->elapsedTimeUs = clmClientData->elapsedTimeUs;

    ULOG_RT_PRINTF_4( clmUlogHandle, "\tClient "
                      "(Name: %s)"
                      "(UtilAtCurrClk: %d)"
                      "(UtilAtMaxClk: %d)"
                      "(UpdateReason: %d)",
                      clmClientData->clientName,
                      loadInfo->utilPctAtCurrClk,
                      loadInfo->utilPctAtMaxClk,
                      loadInfo->updateReason );

    clmClientData->clmCBFn( (void *)loadInfo, clmClientData->clientData );

  }
  else if ( clmClientData->clientType == CLM_CLIENT_EXTENDED_CPUUTIL )
  {
    //create a local payload struct
    CLM_LoadInfoExtendedStructType * loadInfo = clmClientData->loadInfo;

    loadInfo->updateReason = updateReason;
    loadInfo->currentClkKhz = clmEngineCalc->currentClkKhz;
    loadInfo->utilPctAtCurrClk = clmClientData->busyPctCurrClk;
    loadInfo->utilPctAtMaxClk = clmClientData->busyPctMaxClk;
    loadInfo->elapsedTimeUs = clmClientData->elapsedTimeUs;
    loadInfo->availablePcycles = clmClientData->budgetPcycles;
    loadInfo->utilizedPcycles = clmClientData->utilizedPcycles;
    loadInfo->measurementPeriod = clmClientData->measurementPeriodUs;

#if DCVS_NEW_ALGO_INPUT_ACTIVE
    uint32 * pcyclesPerThreadPtr = clmClientData->pcyclesPerThread;
    for ( i = 0; i < maxThreads; i++ )
    {
      loadInfo->pcyclesPerThread[i] = *pcyclesPerThreadPtr;
      pcyclesPerThreadPtr++;
    }

#else
    loadInfo->l2DUstallCnt = clmClientData->l2DUstallCnt;
    loadInfo->l2DUstallPcyclesCnt = clmClientData->l2DUstallPcyclesCnt;
    loadInfo->l2IUmissCnt = clmClientData->l2IUmissCnt;
    loadInfo->pps = clmClientData->pps;
#endif
    ULOG_RT_PRINTF_6( clmUlogHandle, "\tClient "
                      "(Name: %s)"
                      "(UtilAtCurrClk: %d)"
                      "(UtilAtMaxClk: %d)"
                      "(UpdateReason: %d) "
                      "(CurrFreq: %d) "
                      "(MaxFreq: %d)",
                      clmClientData->clientName,
                      loadInfo->utilPctAtCurrClk,
                      loadInfo->utilPctAtMaxClk,
                      loadInfo->updateReason,
                      clmEngineCalc->currentClkKhz,
                      clmEngineCalc->maxClkKhz );
    clmClientData->clmCBFn( (void *)loadInfo, clmClientData->clientData );

  }
  else if ( clmClientData->clientType == CLM_CLIENT_PMU_INFO )
  {
    CLM_LoadInfoPMUInfoStructType * loadInfo = clmClientData->loadInfo;

    loadInfo->elapsedTimeUs = clmClientData->elapsedTimeUs;
    loadInfo->pmuIsDisabled = clmEngineCalc->pmuDisabled;
    loadInfo->updateReason = updateReason;

    //if PMU is disabled, clear data as it is invalid
    if ( clmEngineCalc->pmuDisabled )
    {

#if DCVS_NEW_ALGO_INPUT_ACTIVE
      for ( i = 0; i < maxThreads; i++ )
      {
        loadInfo->pcyclesPerThread[i] = 0;
      }
#else
      loadInfo->l2DUstallCnt = 0;
      loadInfo->l2DUstallPcyclesCnt = 0;
      loadInfo->l2IUmissCnt = 0;
      loadInfo->pps = 0;
#endif

    }
    else
    {
#if DCVS_NEW_ALGO_INPUT_ACTIVE
      uint32 * pcyclesPerThreadPtr = clmClientData->pcyclesPerThread;
      for ( i = 0; i < maxThreads; i++ )
      {
        loadInfo->pcyclesPerThread[i] = *pcyclesPerThreadPtr;
        pcyclesPerThreadPtr++;
      }
#else
      loadInfo->l2DUstallCnt = clmClientData->l2DUstallCnt;
      loadInfo->l2DUstallPcyclesCnt = clmClientData->l2DUstallPcyclesCnt;
      loadInfo->l2IUmissCnt = clmClientData->l2IUmissCnt;
      loadInfo->pps = clmClientData->pps;
#endif
    }

#if DCVS_NEW_ALGO_INPUT_ACTIVE
    ULOG_RT_PRINTF_6( clmUlogHandle, "\tClient "
                      "(Name: %s)"
                      "(PMU Disabled: %d)"
                      "(Pcycles Thread 0: %d)"
                      "(Pcycles Thread 1: %d)"
                      "(Pcycles Thread 2: %d) "
                      "(Pcycles Thread 3: %d) ",
                      clmClientData->clientName,
                      loadInfo->pmuIsDisabled,
                      loadInfo->pcyclesPerThread[0],
                      loadInfo->pcyclesPerThread[1],
                      loadInfo->pcyclesPerThread[2],
                      loadInfo->pcyclesPerThread[3] );
#else
    ULOG_RT_PRINTF_6( clmUlogHandle, "\tClient "
                      "(Name: %s)"
                      "(PMU Disabled: %d)"
                      "(L2 DU Stall Cnt: %d)"
                      "(L2 DU Stall Pcycles Cnt: %d)"
                      "(L2 IU Miss Count: %d) "
                      "(PPS: %d) ",
                      clmClientData->clientName,
                      loadInfo->pmuIsDisabled,
                      loadInfo->l2DUstallCnt,
                      loadInfo->l2DUstallPcyclesCnt,
                      loadInfo->l2IUmissCnt,
                      loadInfo->pps );
#endif

    clmClientData->clmCBFn( (void *)loadInfo, clmClientData->clientData );
  }
}

/** 
 *   <!-- CLM_CheckBusyHighThreshold --> 
 *  
 *   @brief Checks if utilization of ALL HW threads for a given
 *          window falls above the client specified high
 *          threshold
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return TRUE if CPU Utilization of ALL HW Threads are above
 *           the high threshold
 */
static uint32 CLM_CheckBusyHighThreshold( CLM_Client * client,
                                          CLM_EngineCalculation * engineCalc )
{

  uint64 busyPyclesThread;
  uint32 busyPctThread;
  uint32 numThreadsBusyHigh = 0;
  uint32 i, maxThreads = CLM_EngineGetMaxHWThreads();
  uint64 * idleCyclesPtr = client->idleCycles;
  for ( i = 0; i < maxThreads; i++ )
  {

    if ( client->budgetPcycles / maxThreads > *idleCyclesPtr )
    {
      busyPyclesThread = ((client->budgetPcycles / maxThreads) - *idleCyclesPtr);
    }
    else
    {
      busyPyclesThread = 0;
    }

    busyPctThread = (busyPyclesThread * maxThreads * 100) /
                    client->budgetPcycles;

    //Calculate this at max clock
    busyPctThread = (uint64)(busyPctThread * engineCalc->currentClkKhz)
                    / (uint64)engineCalc->maxClkKhz;

    if ( busyPctThread > client->highThresholdPercentage )
    {
      numThreadsBusyHigh++;
    }
    idleCyclesPtr++;
  }

  // Only if all threads meet the minimum condition
  if ( numThreadsBusyHigh >= client->numThreadsForHighTrigger )
  {
    return TRUE;
  }

  return FALSE;

}

/** 
 *    <!-- CLM_CheckBusyLowThreshold -->
 * 
 *    @brief Checks if utilization of ALL HW threads for a given
 *           window falls below the client specified low
 *           threshold
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return TRUE if CPU Utilization of atleast 1 HW Threads is below
 *           the low threshold 
 */
uint32 CLM_CheckBusyLowThreshold( CLM_Client * client,
                                  CLM_EngineCalculation * engineCalc )
{

  uint64 busyPyclesThread;
  uint32 busyPctThread;
  uint32 i;
  uint32 numThreadsBusyLow = 0;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();
  uint64 * idleCyclesPtr = client->idleCycles;
  for ( i = 0; i < maxThreads; i++ )
  {

    if ( client->budgetPcycles / maxThreads > *idleCyclesPtr )
    {
      busyPyclesThread = ((client->budgetPcycles / maxThreads) - *idleCyclesPtr);
    }
    else
    {
      busyPyclesThread = 0;
    }

    busyPctThread = (busyPyclesThread * maxThreads * 100) /
                    client->budgetPcycles;

    //Calculate this at max clock
    busyPctThread = (uint64)(busyPctThread * engineCalc->currentClkKhz)
                    / (uint64)engineCalc->maxClkKhz;

    if ( busyPctThread <= client->lowThresholdPercentage )
    {
      numThreadsBusyLow++;
    }
    idleCyclesPtr++;
  }

  if ( numThreadsBusyLow >= client->numThreadsForLowTrigger )
  {
    return TRUE;
  }

  return FALSE;

}

/** 
 *   <!--  CLM_AdjustForParallelism -->
 *  
 *   @brief Calculates the utilized PCycles by accounting for
 *          parallelism in a multi-HW thread scenario
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return None
 */
static void CLM_AdjustForParallelism( CLM_Client * client,
                                      CLM_EngineCalculation * clmEngineCalc )
{
  uint64 newUtilizedPcycles = client->utilizedPcycles;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 i;
  uint32 * pcyclesPerThread = client->pcyclesPerThread;
#if DCVS_EXTRA_LOGS_ENABLED
  ULOG_RT_PRINTF_2( clmUlogHandle, "AdjustForParalleism Stage 1 (Utilized Pcycles : %d %d)", ULOG64_LOWWORD( newUtilizedPcycles ), ULOG64_HIGHWORD( newUtilizedPcycles ) );
#endif
  for ( i = 3; i > 0; i-- )
  {
    newUtilizedPcycles -= (uint64)((i - 1) * (*pcyclesPerThread)) / 3;

#if DCVS_EXTRA_LOGS_ENABLED
    ULOG_RT_PRINTF_4( clmUlogHandle, "AdjustForParalleism Stage %d (Utilized Pcycles : %d %d) (PMU Factor : %d)"
                      , (5 - i), ULOG64_LOWWORD( newUtilizedPcycles ), ULOG64_HIGHWORD( newUtilizedPcycles ), *pcyclesPerThread );
#endif

    pcyclesPerThread++;

  }
#else
  // rough approximation from data stalls.
  uint64 l2IUstallPcycles = client->l2IUmissCnt
                            * (client->l2DUstallPcyclesCnt / client->l2DUstallCnt);

  uint32 l2cacheMissPcycles = client->l2DUstallPcyclesCnt + l2IUstallPcycles;

  if ( client->utilizedPcycles > l2cacheMissPcycles )
  {
    client->utilizedPcycles -= l2cacheMissPcycles;
  }
  else
  {
    client->utilizedPcycles = 0;
  }

#endif

  ULOG_RT_PRINTF_4( clmUlogHandle, "\t(Utilized Pcycles before Adjustment : %d %d)"
                    " (Utilized Pcycles after Adjustment : %d %d)",
                    ULOG64_LOWWORD( client->utilizedPcycles ),
                    ULOG64_HIGHWORD( client->utilizedPcycles ),
                    ULOG64_LOWWORD( newUtilizedPcycles ),
                    ULOG64_HIGHWORD( newUtilizedPcycles ) );

  client->utilizedPcycles = newUtilizedPcycles;

}

/** 
 *   <!-- CLM_CalculateUtilizations -->
 *  
 *   @brief Calculates the utilization at current and max clock
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return None
 */
static void CLM_CalculateUtilizations( CLM_Client * client,
                                       CLM_EngineCalculation * clmEngineCalc )
{

  // At current clock
  client->busyPctCurrClk = (uint64)(client->utilizedPcycles *
                                    100) / (uint64)client->budgetPcycles;
                                    
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  client->busyPctSingleThread = (uint64)(client->pcyclesPerThread[0] *
                                         100) / (uint64)client->budgetPcycles;
#endif

  // At max clock
  client->busyPctMaxClk = (uint64)(client->busyPctCurrClk * clmEngineCalc->currentClkKhz)
                          / (uint64)(clmEngineCalc->maxClkKhz);
                          
#if DCVS_EXTRA_LOGS_ENABLED && DCVS_NEW_ALGO_INPUT_ACTIVE
  ULOG_RT_PRINTF_3( clmUlogHandle, "\t Client Data: (utilizedPcycles : %d) (budgetPcycles : %d) (pcyclesPerThread : %d)"
                    , client->utilizedPcycles, client->budgetPcycles, client->pcyclesPerThread[0] );
  ULOG_RT_PRINTF_3( clmUlogHandle, "\t(CurrClkBusyPct : %d) (MaxClkBusyPct : %d) (SingleThread : %d)"
                    , client->busyPctCurrClk, client->busyPctMaxClk, client->busyPctSingleThread );
#endif

}


/** 
 *   <!--  CLM_ClientResetMeasurements -->
 *  
 *   @brief Resets the client measurement data for the next
 *          window
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @return None
 */
static void CLM_ClientResetMeasurements( CLM_Client * client )
{

  int i;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();
  uint64 * idleCyclesPtr = client->idleCycles;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 * pcyclesPerThreadPtr = client->pcyclesPerThread;
#endif

#if DCVS_EXTRA_LOGS_ENABLED
    ULOG_RT_PRINTF_1( clmUlogHandle, "CLM_ClientResetMeasurements for Client %s ", client->clientName );
#endif

  client->budgetPcycles = 0;
  client->utilizedPcycles = 0;
  client->elapsedTimeUs = 0;
  client->clientPendingTime = client->measurementPeriodUs;

  for ( i = 0; i < maxThreads; i++ )
  {
    *idleCyclesPtr = 0;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
    *pcyclesPerThreadPtr = 0;
    pcyclesPerThreadPtr++;
#endif
    idleCyclesPtr++;
  }

#if !DCVS_NEW_ALGO_INPUT_ACTIVE
  client->l2DUstallCnt = 0;
  client->l2DUstallPcyclesCnt = 0;
  client->l2IUmissCnt = 0;
  client->pps = 0;
#endif

}


/** 
 *   <!-- CLM_UpdateStats -->
 *  
 *   @brief Updates the client statistics based on Engine
 *          measurements whenever a callback from engine is
 *          triggered
 *
 *   @param client : pointer to the Client specific data
 * 
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return None 
 */
void CLM_UpdateStats( CLM_Client * client,
                      CLM_EngineCalculation * clmEngineCalc )

{
  int i;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();

  uint64 * clientIdleCyclesPtr,*engineIdleCyclesPtr;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 * clientPcyclesPerThreadPtr,*enginePcyclesPerThreadPtr;
#endif

#if DCVS_EXTRA_LOGS_ENABLED
    ULOG_RT_PRINTF_1( clmUlogHandle, "CLM_UpdateStats - client %s", client->clientName );
#endif

  client->budgetPcycles += clmEngineCalc->sumBudgetCycles;
  client->utilizedPcycles += clmEngineCalc->utilizedCycles;
  client->elapsedTimeUs += clmEngineCalc->timeElapsed;

  clientIdleCyclesPtr = client->idleCycles;
  engineIdleCyclesPtr = clmEngineCalc->idleCycles;

#if DCVS_NEW_ALGO_INPUT_ACTIVE
  clientPcyclesPerThreadPtr = client->pcyclesPerThread;
  enginePcyclesPerThreadPtr = clmEngineCalc->pcyclesPerThread;
#endif

  for ( i = 0; i < maxThreads; i++ )
  {
    *clientIdleCyclesPtr = *engineIdleCyclesPtr;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
    *clientPcyclesPerThreadPtr += *enginePcyclesPerThreadPtr;
    clientPcyclesPerThreadPtr++;
    enginePcyclesPerThreadPtr++;
#endif
    clientIdleCyclesPtr++;
    engineIdleCyclesPtr++;
  }

#if !DCVS_NEW_ALGO_INPUT_ACTIVE
  client->l2DUstallCnt += clmEngineCalc->l2DUstallCnt;
  client->l2DUstallPcyclesCnt += clmEngineCalc->l2DUstallPcyclesCnt;
  client->l2IUmissCnt += clmEngineCalc->l2IUmissCnt;
  client->pps += clmEngineCalc->pps;
#endif
}


/** 
 *   <!-- CLM_UpdatePeriodicClient -->
 *  
 *   @brief Update clients that need periodic measurement data
 *   Clients will be updated only when their window size has been exceeded
 *
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 *
 *   @return None
 */
static void CLM_UpdatePeriodicClient( CLM_Client * client,
                                      CLM_EngineCalculation * clmEngineCalc )
{

  if ( client->elapsedTimeUs >= client->measurementPeriodUs - CLM_TIMER_TOLERANCE ) //account for +/- 100 uS from Timer Subsystem
  {
    if ( client->elapsedTimeUs >= client->measurementPeriodUs + CLM_TIMER_TOLERANCE + 400 )
    {
      //sleep occured
      if ( client->attributes & CLM_ATTRIBUTE_REDUCED_PERIOD_ON_PC_EXIT && client->measurementPeriodUs != CLM_PC_EXIT_REDUCED_PERIOD )
      {
        uint32 deferredTime = client->elapsedTimeUs - client->measurementPeriodUs;
        ULOG_RT_PRINTF_1( clmUlogHandle, "CLIENT (Timer Was Deferred by : %d)", deferredTime);
        client->measurementPeriodUs = CLM_PC_EXIT_REDUCED_PERIOD;
        client->clientPendingTime = client->measurementPeriodUs; 
        return;
      }
      
    }
#if DCVS_EXTRA_LOGS_ENABLED
    ULOG_RT_PRINTF_1( clmUlogHandle, "QuRT Pcycles : %d", client->utilizedPcycles );
#endif
    CLM_CalculateUtilizations( client, clmEngineCalc );

#if DCVS_NEW_ALGO_INPUT_ACTIVE
    if ( (client->busyPctCurrClk <= CPU_MAXED_OUT_PCT || client->busyPctSingleThread <= SINGLE_THREAD_MAXED_OUT_PCT) && !clmEngineCalc->clkGated )
    {
#else
    if ( client->busyPctCurrClk <= CPU_MAXED_OUT_PCT && !clmEngineCalc->clkGated )
    {
#endif

      CLM_AdjustForParallelism( client, clmEngineCalc );
      CLM_CalculateUtilizations( client, clmEngineCalc );

    }
#if DCVS_EXTRA_LOGS_ENABLED
    else
    {
      ULOG_RT_PRINTF_2( clmUlogHandle, "CPU UTILIZATION NOT BEING ADJUSTED. busyPctCurrClk = %d, clkGated = %d ",
	                                   client->busyPctCurrClk,clmEngineCalc->clkGated );
    }
#endif
    CLM_UpdateUtilFn( client, clmEngineCalc, CLM_UPDATE_REASON_PERIODIC );

    // Reset data for new measurement window
    CLM_ClientResetMeasurements( client );
  }

}

/** 
 *   <!--  CLM_UpdateThresholdClient -->
 *  
 *   @brief Update clients that need notifications when the CPU
 *   Utilization exceeds/falls below threshold
 * 
 *   @param client : pointer to the Client specific data
 *   
 *   @param clmEngineCalc : Struct contains engine calculations
 *
 *   @return None
 */
static void CLM_UpdateThresholdClient( CLM_Client * client,
                                       CLM_EngineCalculation * clmEngineCalc )
{
  if ( client->elapsedTimeUs >= client->measurementPeriodUs - CLM_TIMER_TOLERANCE )
  {
    // Client Callbacks called only when clock gating is enabled

    // In Case Util Pct Is Above High Threshold
    if ( CLM_CheckBusyHighThreshold( client, clmEngineCalc ) )

    {
      CLM_CalculateUtilizations( client, clmEngineCalc );

      // Inform client only when state transitions from low or intermediate to high
      if ( client->lastKnownThreshold == CLM_LOW_THRESHOLD_LEVEL || client->lastKnownThreshold == CLM_UNKNOWN_THRESHOLD_LEVEL )
      {
        client->lastKnownThreshold = CLM_HIGH_THRESHOLD_LEVEL;
        CLM_UpdateUtilFn( client, clmEngineCalc, CLM_UPDATE_REASON_HIGH_THRESHOLD );
      }
    }

    // In Case Util Pct Is Under Low Threshold
    else if ( CLM_CheckBusyLowThreshold( client, clmEngineCalc ) )
    {

      CLM_CalculateUtilizations( client, clmEngineCalc );

      // Inform client only when state transitions from high or intermediate to low
      if ( client->lastKnownThreshold == CLM_HIGH_THRESHOLD_LEVEL || client->lastKnownThreshold == CLM_UNKNOWN_THRESHOLD_LEVEL )
      {
        client->lastKnownThreshold = CLM_LOW_THRESHOLD_LEVEL;
        CLM_UpdateUtilFn( client, clmEngineCalc, CLM_UPDATE_REASON_LOW_THRESHOLD );
      }

    }

    CLM_ClientResetMeasurements( client );

  }

}


/** 
 *   <!--  CLM_ClientUpdate --> 
 *  
 *   @brief Callback function called by CLM_Engine whenever
 *          Engine Timer expires. This function will update all
 *          the client data with the measurement statistics that
 *          the engine gathered. Notifications to registered
 *          clients will be sent via their callback functions
 *          whenever their notification conditions are true
 *
 *   @param clmEngineCalc : Struct contains engine calculations
 * 
 *   @return None
 */

void CLM_ClientUpdate( CLM_EngineCalculation * clmEngineCalc )
{
  CLM_Client * client = CLM_EngineGetClients();

  CORE_VERIFY_PTR( client );

  // Walk the list to update each client's members
  while ( client )
  {
    CLM_UpdateStats( client, clmEngineCalc );

    if ( clmEngineCalc->timerExpired )
    {
      if ( !client->lowThresholdPercentage && !client->highThresholdPercentage )
      {
        CLM_UpdatePeriodicClient( client, clmEngineCalc );
      }
      else
      {
        CLM_UpdateThresholdClient( client, clmEngineCalc );
      }
    }
    client = client->next;
  }

}


/**
 *   <!-- CLM_InitUlog -->
 * 
 *   @brief Initialize logging system for CLM
 *  
 *   @return None
 */
inline static void CLM_InitUlog( void )
{

  const char logName[] = "ClmLog";

  // Create a Ulog handle
  CORE_DAL_VERIFY(
                   ULogFront_RealTimeInit(
                                           &clmUlogHandle,
                                           logName,
                                           CLM_ULOG_DEFAULT_BUFFER_SIZE_BYTES,
                                           ULOG_MEMORY_LOCAL,
                                           ULOG_LOCK_OS ) );

  // Header allows the visualization tool to apply the appropriate parser.
  ULogCore_HeaderSet( clmUlogHandle,
                      "Content-Type: text/tagged-log-1.0; title=CLM" );
}

/**
 *   <!-- CLM_Initialize -->
 * 
 *   @brief Initialize CLM. This function will be invoked by
 *          RC_Init
 *  
 *   @return None
 */
void CLM_Initialize( void )
{

  CLM_InitUlog();

  /* Create an instance of the CLM Engine */
  CLM_EngineCreate( CLM_ClientUpdate );

  ULOG_RT_PRINTF_0( clmUlogHandle, "Engine Created" );

}

/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/

/**
 *  <!-- CLM_RegisterPeriodicClient -->
 * 
 *  @brief Register periodic client with CLM. This function also invokes
 * 
 *  @param clientName : String containing client name
 *  @param measPeriodUs : Measurement period of client in usec
 *  @param callbackPtr : Client provided callback to be invoked when meas window elapses/threshold crossed 
 *  @param clientType : Client output type (basic, extended, PMU)
 *  @param client : Client specific data
 * 
 *  @return CLM_HandleType 
 */
CLM_HandleType CLM_RegisterPeriodicClient(
                                           const char * clientName,
                                           CLM_ClientOutputType clientType,
                                           uint32 measPeriodUs,
                                           uint32 attributes,
                                           CLM_CallbackFuncPtr callbackPtr,
                                           void * clientData
                                           )
{
  CLM_Client * newClmClient = NULL;
  uint32 loadInfoSize = 0;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();

  //verify parameters
  CORE_VERIFY( clientName );
  CORE_VERIFY( strlen( clientName ) <= CLM_MAX_NAME_LENGTH );
  CORE_VERIFY_PTR( callbackPtr );
  CORE_VERIFY( (clientType == CLM_CLIENT_BASIC_CPUUTIL) ||
               (clientType == CLM_CLIENT_EXTENDED_CPUUTIL) ||
               (clientType == CLM_CLIENT_PMU_INFO) );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( sizeof(CLM_Client),
                                             (void
                                              **)&newClmClient ) );

  /* Initialize the allocated memory. */
  memset( newClmClient, 0, sizeof(CLM_Client) );

  //Determine proper size for loadInfo struct
  if ( clientType == CLM_CLIENT_BASIC_CPUUTIL )
  {
    loadInfoSize = sizeof(CLM_LoadInfoBasicStructType);
  }
  else if ( clientType == CLM_CLIENT_EXTENDED_CPUUTIL )
  {
    loadInfoSize = sizeof(CLM_LoadInfoExtendedStructType);
  }
  else if ( clientType == CLM_CLIENT_PMU_INFO )
  {
    loadInfoSize = sizeof(CLM_LoadInfoPMUInfoStructType);
  }

  /* Allocate and initialize memory for variable size of load info, and
     idleCycles / busyCyclesforNThreadsActive pointers (size of ptrs varies
     with the number of HW threads for each target */
  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( loadInfoSize,
                                             (void
                                              **)&newClmClient->loadInfo ) );
  memset( newClmClient->loadInfo, 0, loadInfoSize );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxThreads * sizeof(uint64),
                                             (void
                                              **)&newClmClient->idleCycles ) );
  memset( newClmClient->idleCycles, 0, maxThreads * sizeof(uint64) );

#if DCVS_NEW_ALGO_INPUT_ACTIVE
  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxThreads * sizeof(uint32),
                                             (void
                                              **)&newClmClient->pcyclesPerThread ) );
  memset( newClmClient->pcyclesPerThread, 0, maxThreads * sizeof(uint32) );
#endif

  // Copy over the client data
  newClmClient->numThreadsForHighTrigger = 3;
  newClmClient->numThreadsForLowTrigger = 3;
  newClmClient->clientName = clientName;
  newClmClient->measurementPeriodUs = measPeriodUs;
  newClmClient->lowThresholdPercentage = 0;
  newClmClient->highThresholdPercentage = 0;
  newClmClient->clmCBFn = callbackPtr;
  newClmClient->clientType = clientType;
  newClmClient->attributes = attributes;
  newClmClient->clientData = clientData;

  // initialize threshold level to unknown
  newClmClient->lastKnownThreshold = CLM_UNKNOWN_THRESHOLD_LEVEL;

  ULOG_RT_PRINTF_0( clmUlogHandle, "Before Engine Register" );

  CLM_EngineRegisterClient( newClmClient, newClmClient->measurementPeriodUs );

  ULogFront_RealTimePrintf( clmUlogHandle, 1, "Client %s has registered", newClmClient->clientName );

  return (CLM_HandleType)newClmClient;

}

/**
 *  <!-- CLM_RegisterThresholdClient -->
 * 
 *  @brief Register client with CLM. This function also invokes
 * 
 *  @param clientName : String containing client name
 *  @param params : Client provided configuration data
 *  @param callbackPtr : Client provided callback to be invoked when meas window elapses/threshold crossed 
 *  @param clientType : Client output type (basic, extended, PMU)
 *  @param client : Client specific data
 * 
 *  @return CLM_HandleType 
 */
CLM_HandleType CLM_RegisterThresholdClient(
                                            const char * clientName,
                                            CLM_ClientOutputType clientType,
                                            uint32 measPeriodUs,
                                            CLM_ThresholdClientRegistrationStruct * params,
                                            uint32 attributes,
                                            CLM_CallbackFuncPtr callbackPtr,
                                            void * clientData
                                            )
{
  CLM_Client * newClmClient = NULL;
  uint32 loadInfoSize = 0;
  uint32 maxThreads = CLM_EngineGetMaxHWThreads();

  CORE_VERIFY( clientName );
  CORE_VERIFY( strlen( clientName ) <= CLM_MAX_NAME_LENGTH );
  CORE_VERIFY_PTR( params );
  CORE_VERIFY( measPeriodUs );
  CORE_VERIFY( params->numThreadsForHighTrigger <= maxThreads );
  CORE_VERIFY( params->numThreadsForLowTrigger <= maxThreads );
  CORE_VERIFY_PTR( callbackPtr );
  CORE_VERIFY( (clientType == CLM_CLIENT_BASIC_CPUUTIL) ||
               (clientType == CLM_CLIENT_EXTENDED_CPUUTIL) ||
               (clientType == CLM_CLIENT_PMU_INFO) );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( sizeof(CLM_Client),
                                             (void
                                              **)&newClmClient ) );

  /* Initialize the allocated memory. */
  memset( newClmClient, 0, sizeof(CLM_Client) );

  //Determine proper size for loadInfo struct
  if ( clientType == CLM_CLIENT_BASIC_CPUUTIL )
  {
    loadInfoSize = sizeof(CLM_LoadInfoBasicStructType);
  }
  else if ( clientType == CLM_CLIENT_EXTENDED_CPUUTIL )
  {
    loadInfoSize = sizeof(CLM_LoadInfoExtendedStructType);
  }
  else if ( clientType == CLM_CLIENT_PMU_INFO )
  {
    loadInfoSize = sizeof(CLM_LoadInfoPMUInfoStructType);
  }

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( loadInfoSize,
                                             (void
                                              **)&newClmClient->loadInfo ) );

  /* Initialize the allocated memory. */
  memset( newClmClient->loadInfo, 0, loadInfoSize );

  // Copy over the client data
  newClmClient->numThreadsForHighTrigger = params->numThreadsForHighTrigger;
  newClmClient->numThreadsForLowTrigger = params->numThreadsForLowTrigger;
  newClmClient->clientName = clientName;
  newClmClient->measurementPeriodUs = measPeriodUs;
  newClmClient->lowThresholdPercentage = params->lowThresholdPercentage;
  newClmClient->highThresholdPercentage = params->highThresholdPercentage;
  newClmClient->attributes = attributes;
  newClmClient->clmCBFn = callbackPtr;
  newClmClient->clientType = clientType;
  newClmClient->clientData = clientData;

  // initialize threshold level to unknown
  newClmClient->lastKnownThreshold = CLM_UNKNOWN_THRESHOLD_LEVEL;

  CLM_EngineRegisterClient( newClmClient, newClmClient->measurementPeriodUs );

  ULOG_RT_PRINTF_1( clmUlogHandle, "Client %s has registered", newClmClient->clientName );

  return (CLM_HandleType)newClmClient;

}

/**
 *  <!-- CLM_UnregisterClient -->
 * 
 *   @brief This function is used to remove a client from CLM
 * 
 *   @param handle : Client handle of the client to be removed
 *  
 *   @return None
 */
void CLM_UnregisterClient( CLM_HandleType handle )
{
  CLM_EngineUnregisterClient( handle );
}


/**
 *  <!-- CLM_GetLastLPRExitTime -->
 * 
 *   @brief This function is used to obtain the time of the last
 *          CLM LPR exit
 * 
 *  
 *   @return uint64 : the time of the last CLM LPR exit
 */
uint64 CLM_GetLastLPRExitTime( void )
{
  return ( CLM_EngineGetLastLPRExitTime() );
}

/**
 *  <!-- CLM_SwitchClientMeasurementPeriod -->
 * 
 *   @brief This function switches the measurement period of the
 *          client to new measurement period passed in by the
 *          user
 *  
 *   @param client : Client handle of the client that will
 *                 receive the new measurement period
 *  
 *   @param measurementPeriodUs : The new measurement period
 *  
 *   @return None
 */
void CLM_SwitchClientMeasurementPeriod( CLM_HandleType client, uint32 measurementPeriodUs )
{
  CORE_VERIFY( measurementPeriodUs );
  CLM_Client * clmClient = (CLM_Client *)client;
  clmClient->measurementPeriodUs = measurementPeriodUs;
}


