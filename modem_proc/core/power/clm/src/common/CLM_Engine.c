/*=========================================================================== 

DESCRIPTION:  Implementation of CLM Engine layer that monitors the CPU
Utilization by way of measuring the Utilized and Budget PCycles using the PMU
and QURT APIs. The measurement is done on a regular interval using a timer
interrupt. The Engine layer interfaces with the CLM Client layer to send out
measurement notifications. The Engine Layer can be considered as the
back-end while the Client Layer is the front end
 
=========================================================================== 
Copyright � 2013-2014 QUALCOMM Technologies, Incorporated. All Rights Reserved. 
QUALCOMM Confidential and Proprietary. 
===========================================================================  
 
                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/clm/src/common/CLM_Engine.c#1 $

=============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "CLM_Engine.h"
#include "ULogFront.h"
#include "npa.h"
#include "CoreVerify.h"
#include "ClockDefs.h"
#include "qurt.h"
#include "DALSys.h"
#include "DALStdErr.h"
#include "qurt_consts.h"
#include "qurt_event.h"
#include "sleep_lpr.h"
#include "ULogFront.h"
#include "msmhwiobase.h"
#include "HALhwio.h"
#include "CoreTime.h"
#include "npa_resource.h"
#include CLM_HWIO_H

static uint32 bClmClkChangeEventRegistered = 0;
/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/



#if DCVS_NEW_ALGO_INPUT_ACTIVE

/* Macro for PMU Event config register value */
// 03=PPS; a0=ANU_DU_STALL; 9f=ANY_DU_STALL_CYCLES; 77=L2_IU_MISS;
#define PMU_EVT_CONFIG_VAL 0x3e3d3c3b

#else

/* Macro for PMU Event config register value */
// 03=PPS; a0=ANU_DU_STALL; 9f=ANY_DU_STALL_CYCLES; 77=L2_IU_MISS;
#define PMU_EVT_CONFIG_VAL 0x03a09f77

#endif

/* Macro for PMU Event config register value */
// 3e=THREE_THREAD_RUNNING_CYCLES,3d=THREE_THREAD_RUNNING_CYCLES,
// 3c=TWO_THREAD_RUNNING_CYCLES, 3b=ONE_THREAD_RUNNING_CYCLES
#define PMU_EVT_CONFIG_VAL_CLK_GATING 0x3e3d3c3b

/* Macro for PMU config register value */
#define PMU_CONFIG_VAL 7

/* Macro for reading the IO mapped mem with a supplied mask*/
#define CLM_CGC_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_CGC_OVERRIDE_ADDR, m)

/* Macros for turning on Debug Messages */
//#define ENGINE_CLK_CHG_MSG
#define ENGINE_TIMER_CB_MSG

/*----------------------------------------------------------------------------
 * Type Definitions
 * -------------------------------------------------------------------------*/
/* Structure type to maintain the context for the data for CLM Engine */
struct
{
  /* Indicates whether or not clk resource is ready */
  uint32 initDone;

  /* The number of registered clients */
  uint32 numClients;

  /* Pointer to the lock  */
  CoreMutexType * clmMutexHandle;

  // head of the linked list for Engine Clients
  CLM_Client * clientList;

  /* Pointer to struct that contacts CLM Engine calculation stats */
  CLM_EngineCalculation engineCalc;

  /* Data structure/handle used for CLM timer */
  timer_ptr_type timer;

  /* Ptr to CLM timer group*/
  timer_group_ptr clmTimerGroup;

  /* Query Handle for "/clk/cpu" resource */
  npa_query_handle queryHandle;

  /* Value for number of HW threads for this target */
  uint32 maxHWThreads;

  // timestamp to record when the CLM Timer was started
  uint64 sysTickStart;

  /* Indicate if timer is started */
  uint32 timerStarted;

  /* Callback event for state change of clock resource */
  DALSYSEventHandle clockChangeEvent;

  /* CLM_Client update callback function */
  CLM_ClientCB clmClientCb;

}CLM_engineContext;
/*----------------------------------------------------------------------------
 * Data Declarations (external and forward)
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Data Definitions
 * -------------------------------------------------------------------------*/

/*Static variable to store the current status of PMU usage
  TRUE==not available for CLM use; FALSE==available for CLM use.*/
static uint32 isPMUBusy = FALSE;


/*----------------------------------------------------------------------------
 * Internal Function Definitions
 * -------------------------------------------------------------------------*/


/** 
 * <!--  CLM_EngineIsClockGated -->
 *  
 * @brief Checks if the clock core gating has been disabled by
 *        the scripts
 *
 * @param None
 *
 * @return None
 */
uint32 CLM_EngineIsClockGated( void )
{

  CLM_engineContext.engineCalc.clkGated =

                                          CLM_CGC_OVERRIDE_INM( HWIO_MSS_QDSP6SS_CGC_OVERRIDE_CORE_CLK_EN_BMSK
                                                                | HWIO_MSS_QDSP6SS_CGC_OVERRIDE_CORE_RCLK_EN_BMSK );

  if ( CLM_engineContext.engineCalc.clkGated )
  {
    CLM_engineContext.engineCalc.clkGated = TRUE;
  }
  else
  {
    CLM_engineContext.engineCalc.clkGated = FALSE;
  }
  return CLM_engineContext.engineCalc.clkGated;
}

/** 
 * <!--  CLM_EnginePMUInitializeUnderClkGating -->
 *  
 * @brief Initialize the PMU for Clock Gating Disabled
 *
 * @param None
 *
 * @return None
 */
static void CLM_EnginePMUInitializeUnderClkGating( void )
{
  int i;
  qurt_pmu_set( QURT_PMUEVTCFG, PMU_EVT_CONFIG_VAL_CLK_GATING ); // Set Event Types
  qurt_pmu_set( QURT_PMUCFG, PMU_CONFIG_VAL );        // Thread Bit Mask

  qurt_pmu_set( QURT_PMUCNT0, 0 );
  qurt_pmu_set( QURT_PMUCNT1, 0 );
  qurt_pmu_set( QURT_PMUCNT2, 0 );
  qurt_pmu_set( QURT_PMUCNT3, 0 );

  CLM_engineContext.engineCalc.prevOneThreadCycleCnt = 0;
  CLM_engineContext.engineCalc.prevTwoThreadCycleCnt = 0;
  CLM_engineContext.engineCalc.prevThreeThreadCycleCnt = 0;

#if DCVS_NEW_ALGO_INPUT_ACTIVE
  for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ ) 
  {
    CLM_engineContext.engineCalc.prevPcyclesPerThread[i] = 0;
  }
#endif

  ULogFront_RealTimePrintf( clmUlogHandle, 1, "PMU Configured "
                            "to 0x%x ",
                            PMU_EVT_CONFIG_VAL_CLK_GATING );

  CLM_engineContext.engineCalc.pmuUnderClkGatingInitialized = TRUE;

}


/** 
 * <!--  CLM_EnginePMUInitialize -->
 *  
 * @brief Initialize the PMU for normal use
 *
 * @param None
 *
 * @return None
 */

static void CLM_EnginePMUInitialize( void )
{
  qurt_pmu_set( QURT_PMUEVTCFG, PMU_EVT_CONFIG_VAL ); // Set Event Types
  qurt_pmu_set( QURT_PMUCFG, PMU_CONFIG_VAL );        // Thread Bit Mask
}


/** 
 * <!--  checkPMUEnableBit -->
 *  
 * @brief Function to check if PMU is enabled
   Place holder for now
 *
 * @param None
 *
 * @return TRUE if PMU is enabled
 */
static inline uint32 checkPMUEnableBit( void )
{
  return TRUE;
}

/** 
 *  <!-- DcvsQdspPMUbusy -->
 *  
 * @brief Called by the QURT code to indicate to CLM if the PMU
 *        is currently free/busy. Updates the global variable
 *        isPMUBusy which is used by the Engine Measure
 *        function&
 * @param isBusy: TRUE when PMU is being used
 *
 * @return None 
 *  
 */

void DcvsQdspPMUbusy( int isBusy )
{
  // PMU not used when timer is not started
  if ( !CLM_engineContext.timerStarted )
  {
    return;
  }

  CLM_MUTEX_ACQUIRE();

  if ( isBusy )
  {
    // PMU is not being used by someone other than CLM
    if ( qurt_pmu_get( QURT_PMUEVTCFG ) != PMU_EVT_CONFIG_VAL )
    {
      isPMUBusy = TRUE;
    }
    else
    {
      isPMUBusy = FALSE;
    }
  }
  else
  {
    // PMU just got free - hence we need to initalize again
    isPMUBusy = FALSE;
    CLM_EnginePMUInitialize();
  }

  CLM_MUTEX_RELEASE();
}


/** 
 *  <!-- CLM_EngineHasPMUConfigChanged -->
 *  
 *  @brief This function checks if the PMU is either disabled or
 *  if it's Register Configuration bits have changed or if
 *  somebody other than CLM is using the PMU Registers
 *
 *  @param none
 *
 *  @return : TRUE if PMU config has changed
 *           FALSE otherwise
 *
 */
static inline uint32 CLM_EngineHasPMUConfigChanged( void )
{
  uint32 evtCFG = qurt_pmu_get( QURT_PMUEVTCFG );

  if ( !checkPMUEnableBit() || isPMUBusy )
  {
    CLM_engineContext.engineCalc.pmuDisabled = TRUE;
    return TRUE;
  }
  if ( evtCFG != PMU_EVT_CONFIG_VAL )
  {
    if ( CLM_engineContext.engineCalc.clkGated && evtCFG ==
            PMU_EVT_CONFIG_VAL_CLK_GATING )
    {
      CLM_engineContext.engineCalc.pmuDisabled = FALSE;
      return FALSE;
    }
    CLM_engineContext.engineCalc.pmuDisabled = TRUE;
    return TRUE;
  }
  CLM_engineContext.engineCalc.pmuDisabled = FALSE;
  return FALSE;
}

/**
 *  @brief CLM_EngineCurrClockStateQuery
 * 
 *   This function is used to read the current state of the
 *   clock.
 *
 *  @param : None
 *
 *  @return : The current mips of the clock or NULL for invalid
 *         state .
 */
static uint32 CLM_EngineCurrClockStateQuery( void )
{
  npa_query_type queryResult;

  /* Query the current state of clock resource. */
  CORE_VERIFY( NPA_QUERY_SUCCESS == npa_query( CLM_engineContext.queryHandle,
                                               NPA_QUERY_CURRENT_STATE, &queryResult ) );

  return ( queryResult.data.state );
}


/** 
 *  <!--  CLM_EngineStartTimer -->
 * 
 *  @brief Starts the CLM Engine Timer
 *
 *  @param timeUs value of timer period
 *
 *  @return None
 */
static void CLM_EngineStartTimer( uint32 timeUs )
{
  /* Set Timer delay */
  timer_set_64( CLM_engineContext.timer, timeUs,
                0, T_USEC );

  CLM_engineContext.timerStarted = TRUE;

}

/** 
 *  <!-- CLM_EngineStopTimer -->
 *  
 *  @brief Stops the CLM Engine Timer
 *
 *  @param timerElapsedTime : value of micro-seconds that have
 *                           elapsed since the last instance of
 *                           timer was started. If timer is
 *                           started for the first time, this
 *                           value is 0
 *
 *  @return None
 */
static void CLM_EngineStopTimer( void )
{
  /* Clear the timer */
  timer_clr( CLM_engineContext.timer, T_USEC );

}

/** 
 *  <!-- CLM_EngineFindNextTimerVal -->
 *  
 *  @brief Finds the next timer expiry period from the
 *          registered clients' window sizes
 *
 *  @param currentTime : current time in micro-seconds
 *  
 *  @param resetTolerance : Determines the threshold for
 *  					  reseting a clients period. 
 *
 * @return Next timer expiry period
 */
static uint32 CLM_EngineFindNextTimerVal( int elapsedTime )
{
  int minVal = 0x7FFFFFFF;
  CLM_Client * engineClient = CLM_engineContext.clientList;
  CORE_VERIFY_PTR( engineClient );

  // Walk the list of registered client data
  while ( engineClient ) //do while
  {
    // first subtract current time
    engineClient->clientPendingTime -= elapsedTime;

    // reset the timer period for this client if the clients measurment period has expired
    if ( engineClient->clientPendingTime <= CLM_TIMER_TOLERANCE ) //account for +/- 100 uS inaccuracy from Timer Subsystem
    {
      engineClient->clientPendingTime = engineClient->measurementPeriodUs;
    }

    minVal = MIN( minVal, engineClient->clientPendingTime );
    engineClient = engineClient->next;
  }

  return minVal;
}


/** 
 *  <!-- CLM_EngineClkChg_callback -->
 *  
 *  @brief Callback function trigged whenever the state of the&
 *         clock changes
 *
 *  @param user_data The optional user data passed in this
 *                 callback.
 *  @param cb_event_type  NPA event type
 *  @param cb_data NPA event data structure passed to this
 *                callback.
 *  @param size size of the NPA event data structure.
 *
 *  @return None
 */
void* CLM_EngineClkChg_callback( void * user_data, uint32 cb_event_type, void
                                 * cb_data, uint32 size )
{
  uint64 currentSysTick;
  uint32 deltaSysTick;
  uint32 prevClkKhz;

  if ( NULL == CLM_engineContext.clientList )
  {
    ULOG_RT_PRINTF_0( clmUlogHandle, "Clk CPU change event has to be unregistered as no registered clients for CLM" );
    return ( NULL );
  }

  CLM_MUTEX_ACQUIRE();

  CLM_EngineStopTimer();

  npa_event_data * clkData = (npa_event_data *)cb_data;

  // Update the prev and curr frequencies
  prevClkKhz = CLM_engineContext.engineCalc.currentClkKhz;
  CLM_engineContext.engineCalc.currentClkKhz = clkData->state;

  CORE_VERIFY( CLM_engineContext.engineCalc.currentClkKhz );

  /* Update Budget Cycles */
  currentSysTick = qurt_sysclock_get_hw_ticks();
  deltaSysTick = currentSysTick - CLM_engineContext.sysTickStart;

  uint64 timerElapsedTimeUs = ((uint64)deltaSysTick * 1000000) / (uint64)TICKS_PER_SEC;

  CLM_engineContext.engineCalc.sumBudgetCycles += (uint64)((uint64)deltaSysTick *
                                                           (uint64)prevClkKhz * 1000) / (uint64)TICKS_PER_SEC;

  uint32 nextTimerVal = CLM_EngineFindNextTimerVal( timerElapsedTimeUs );

  CLM_engineContext.engineCalc.currentTimerPeriod = nextTimerVal;

  CLM_engineContext.engineCalc.timeElapsed += timerElapsedTimeUs;

  CLM_EngineStartTimer( nextTimerVal );

  // Next sub-window starts...
  CLM_engineContext.sysTickStart = qurt_sysclock_get_hw_ticks();

  ULogFront_RealTimePrintf( clmUlogHandle, 9,
                            "Engine CLK CHG"
                            "(CurrClockKhz: %d)"
                            "(Budget: 0x%x%x)"
                            "(SysTickStart: 0x%x%x)"
                            "(TimerElapsedTime: %d%d) "
                            "(TotalTimeElapsed: %d%d)",
                            CLM_engineContext.engineCalc.currentClkKhz,
                            ULOG64_DATA( CLM_engineContext.engineCalc.sumBudgetCycles ),
                            ULOG64_DATA( CLM_engineContext.sysTickStart ),
                            ULOG64_DATA( timerElapsedTimeUs ),
                            ULOG64_DATA( CLM_engineContext.engineCalc.timeElapsed ) );

  CLM_MUTEX_RELEASE();

  return ( NULL );
}

/** 
 *  <!--  CLM_EngineRegisterClkChgEvent -->
 *  
 *  @brief Creates and registers a clock change callback event
 *
 *  @param None
 *
 *  @return None
 */
static void CLM_EngineRegisterClkChgEvent( void )
{
  /* Create a callback event for state change of /clk/cpu resource */
  CORE_DAL_VERIFY( DALSYS_EventCreate( DALSYS_EVENT_ATTR_CALLBACK_EVENT,
                                       &CLM_engineContext.clockChangeEvent,
                                       NULL ) );

  CORE_DAL_VERIFY( DALSYS_SetupCallbackEvent( CLM_engineContext.clockChangeEvent,
                                              CLM_EngineClkChg_callback, NULL ) );

  CORE_VERIFY( npa_create_change_event( "/clk/cpu",
                                        "ClockChangeEvent",
                                        CLM_engineContext.clockChangeEvent ) );
}


/** 
 *  <!--  CLM_EngineCaculatePMUBasedBusyPcycles -->
 *  
 *  @brief Finds the Busy PCycles on all threads
 *
 *  @param None
 *
 *  @return : Busy Pcycles
 */
static uint64 CLM_EngineCaculatePMUBasedBusyPcycles( void )
{

  /* Counts for Thread Busy PCycles */
  uint64 oneThreadCycles = 0;
  uint64 twoThreadCycles = 0;
  uint64 threeThreadCycles = 0;

  /* Counts for Thread Busy PCycles Deltas */
  uint64 oneThreadCyclesDel = 0;
  uint64 twoThreadCyclesDel = 0;
  uint64 threeThreadCyclesDel = 0;

  /* 
  Time to get some Qurt Profiling data out... 
  We are reading 3 registers and this operation needs to be atomic, hence the lock 
  */
  INTLOCK();
  oneThreadCycles = qurt_pmu_get( QURT_PMUCNT0 );
  twoThreadCycles = qurt_pmu_get( QURT_PMUCNT1 );
  threeThreadCycles = qurt_pmu_get( QURT_PMUCNT2 );
  INTFREE();

  if ( oneThreadCycles < CLM_engineContext.engineCalc.prevOneThreadCycleCnt )
  {
    //wrap-around has occured ...
    oneThreadCyclesDel = (0xFFFFFFFF -
                          CLM_engineContext.engineCalc.prevOneThreadCycleCnt) + oneThreadCycles + 1;
  }
  else
  {
    oneThreadCyclesDel = oneThreadCycles - CLM_engineContext.engineCalc.prevOneThreadCycleCnt;
  }

  if ( twoThreadCycles < CLM_engineContext.engineCalc.prevTwoThreadCycleCnt )
  {
    //wrap-around has occured ...
    twoThreadCyclesDel = (0xFFFFFFFF -
                          CLM_engineContext.engineCalc.prevTwoThreadCycleCnt) + twoThreadCycles + 1;
  }
  else
  {
    twoThreadCyclesDel = twoThreadCycles -
                         CLM_engineContext.engineCalc.prevTwoThreadCycleCnt;
  }

  if ( threeThreadCycles < CLM_engineContext.engineCalc.prevThreeThreadCycleCnt )
  {
    //wrap-around has occured ...
    threeThreadCyclesDel = (0xFFFFFFFF -
                            CLM_engineContext.engineCalc.prevThreeThreadCycleCnt) + threeThreadCycles + 1;
  }
  else
  {
    threeThreadCyclesDel = threeThreadCycles -
                           CLM_engineContext.engineCalc.prevThreeThreadCycleCnt;
  }
#if 0
  ULogFront_RealTimePrintf(clmUlogHandle,
                           6,
                           "PMU_CALC (1-Th: 0x%x%x) "
                           "(2-Th: 0x%x%x) "
                           "(3-Th: 0x%x%x) ",
                           ULOG64_DATA(oneThreadCycles),
                           ULOG64_DATA(twoThreadCycles),
                           ULOG64_DATA(threeThreadCycles));
#endif

  /* We're interested to find the number of cycles when atleast 1 HW Thread was running, this
     should give us the minimum CPU clock requirement during the profiling interval */
  uint64 sumActiveThreadCycles = (oneThreadCyclesDel +
                                  twoThreadCyclesDel +
                                  threeThreadCyclesDel);

#if 0
  ULogFront_RealTimePrintf(clmUlogHandle,
                           8,
                           "PMU_CALC (Del-1-Th: 0x%x%x) "
                           "(Del-2-Th: 0x%x%x) "
                           "(Del-3-Th: 0x%x%x) "
                           "(Sum: 0x%x%x)",
                           ULOG64_DATA(oneThreadCyclesDel),
                           ULOG64_DATA(twoThreadCyclesDel),
                           ULOG64_DATA(threeThreadCyclesDel),
                           ULOG64_DATA(sumActiveThreadCycles));
#endif

  CLM_engineContext.engineCalc.prevOneThreadCycleCnt = oneThreadCycles;
  CLM_engineContext.engineCalc.prevTwoThreadCycleCnt = twoThreadCycles;
  CLM_engineContext.engineCalc.prevThreeThreadCycleCnt = threeThreadCycles;

#if 0

  ULogFront_RealTimePrintf(clmUlogHandle,
                           6,
                           "PMU_CALC (Prev-1-Th: 0x%x%x) "
                           "(Prev-2-Th: 0x%x%x) "
                           "(Prev-3-Th: 0x%x%x) ",
                           ULOG64_DATA(CLM_engineContext.engineCalc.prevOneThreadCycleCnt),
                           ULOG64_DATA(CLM_engineContext.engineCalc.prevTwoThreadCycleCnt),
                           ULOG64_DATA(CLM_engineContext.engineCalc.prevThreeThreadCycleCnt));
#endif


  return sumActiveThreadCycles;

}

/** 
 * <!-- CLM_EngineMeasure --> 
 *  
 *  @brief Measurement function that profiles the system and
 *         collects PMU data
 *
 *  @param None
 *
 *  @return None
 */
static void CLM_EngineMeasure( void )
{
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 currPcyclesPerThread[CLM_engineContext.maxHWThreads];
#else
  uint32 currL2IUmissCnt = 0;
  uint32 currL2DUstallPcyclesCnt = 0;
  uint32 currL2DUstallCnt = 0;
  uint64 currPps = 0;
#endif
  uint64 currentSysTick;
  uint64 currUtilizedCycles = 0;
  uint32 deltaSysTick;
  uint32 i;
  uint64 idleCycles[CLM_engineContext.maxHWThreads];

  /* Budget Cycles */
  currentSysTick = qurt_sysclock_get_hw_ticks();
  deltaSysTick = currentSysTick - CLM_engineContext.sysTickStart;

  uint64 timerElapsedTimeUs = ((uint64)deltaSysTick * 1000000)
                              / (uint64)TICKS_PER_SEC;

  CLM_engineContext.engineCalc.timeElapsed += timerElapsedTimeUs;

  CLM_engineContext.engineCalc.currentTimerPeriod = CLM_EngineFindNextTimerVal( timerElapsedTimeUs );

  ULogFront_RealTimePrintf( clmUlogHandle, 6, "Engine Begin Timer Callback"
                            "(CurrentSysTick: 0x%x%x)"
                            "(DeltaSysTick: 0x%x%x) "
                            "(TotalTimeElapsed: %d) "
                            "(ClockKhz: %d)"
                            , ULOG64_DATA( currentSysTick )
                            , ULOG64_DATA( deltaSysTick )
                            , ULOG64_LOWWORD( CLM_engineContext.engineCalc.timeElapsed )
                            , CLM_engineContext.engineCalc.currentClkKhz );


  CLM_engineContext.engineCalc.sumBudgetCycles += (uint64)((uint64)deltaSysTick *
                                                           ((uint64)CLM_engineContext.engineCalc.currentClkKhz * 1000))
                                                  / (uint64)TICKS_PER_SEC;


  /* reset engine pcyclesPerThread that would be calculated fresh in this cycle - 
     This is to make sure we wont propagate previous window pcycles data to cleent
	 when clock gating override scenario comes up where this data persists if we 
     don't reset */
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
  {
    CLM_engineContext.engineCalc.pcyclesPerThread[i] = 0;
  }
#endif
  
  if ( !CLM_EngineHasPMUConfigChanged() )
  {
    //pmu configuration has not changed
    if ( CLM_EngineIsClockGated() )
    {
      //clock is gated,
      // Do this only once... at the start of detect of clock gating
      if ( !CLM_engineContext.engineCalc.pmuUnderClkGatingInitialized )
      {
        // Configure the PMU for getting Thread Busy Cycles
        CLM_EnginePMUInitializeUnderClkGating();
      }
      CLM_engineContext.engineCalc.utilizedCycles =
                                                    CLM_EngineCaculatePMUBasedBusyPcycles();

      CLM_engineContext.engineCalc.prevUtilizedCycles =
                                                        CLM_engineContext.engineCalc.utilizedCycles;

    }
    else
    {
      if ( CLM_engineContext.engineCalc.pmuUnderClkGatingInitialized )
      {
        /* we enter this path when Q6 clock overriding bit changed from 1 to zero so fresh of
           measurements */
        
        /* prevUtilizedCycles calculated when CLM_CGC_OVERRIDE_INM needs to be updatd here when 
           override is removed. */
        CLM_engineContext.engineCalc.prevUtilizedCycles =  qurt_get_core_pcycles();
		
	    // restore back the PMU con-fig for getting L2 info
        CLM_EnginePMUInitialize();
        qurt_pmu_set( QURT_PMUCNT0, 0 );
        qurt_pmu_set( QURT_PMUCNT1, 0 );
        qurt_pmu_set( QURT_PMUCNT2, 0 );
        qurt_pmu_set( QURT_PMUCNT3, 0 );

#if DCVS_NEW_ALGO_INPUT_ACTIVE		
		// clear out old data
        uint32 * prevBusyPcyclesOnNThreads = CLM_engineContext.engineCalc.prevPcyclesPerThread;
        for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
        {
          prevBusyPcyclesOnNThreads[i] = 0;
        }
#endif		
        
        CLM_engineContext.engineCalc.pmuUnderClkGatingInitialized = FALSE;

      }

#if DCVS_NEW_ALGO_INPUT_ACTIVE

      INTLOCK();
      for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
      {
        currPcyclesPerThread[i] = qurt_pmu_get( i );
      }
	  
      if ( CLM_engineContext.maxHWThreads == 4 )
      {
        ULOG_RT_PRINTF_4( clmUlogHandle, "\t RAW PMU Data -> (0x3b : %d) (0x3c : %d) (0x3d : %d) (0x3e : %d) ",
                          currPcyclesPerThread[0], currPcyclesPerThread[1], currPcyclesPerThread[2], currPcyclesPerThread[3] );
      }
      else
      {
        ULOG_RT_PRINTF_3( clmUlogHandle, "\t Raw PMU Data -> (0x3b : %d) (0x3c : %d) (0x3d : %d)",
                          currPcyclesPerThread[0], currPcyclesPerThread[1], currPcyclesPerThread[2] );
      }
	  
      INTFREE();

      uint32 * prevBusyPcyclesOnNThreads = CLM_engineContext.engineCalc.prevPcyclesPerThread;
      uint32 * busyPcyclesOnNThreads = CLM_engineContext.engineCalc.pcyclesPerThread;
      for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
      {
        if ( currPcyclesPerThread[i] < *prevBusyPcyclesOnNThreads )
        {
          //wrap-around has occured ...
          *busyPcyclesOnNThreads = (0xFFFFFFFF - *prevBusyPcyclesOnNThreads)
                                   + currPcyclesPerThread[i] + 1;
        }
        else
        {
          *busyPcyclesOnNThreads = currPcyclesPerThread[i] - *prevBusyPcyclesOnNThreads;
        }
        *prevBusyPcyclesOnNThreads = currPcyclesPerThread[i];
        currPcyclesPerThread[i] = *busyPcyclesOnNThreads;
        prevBusyPcyclesOnNThreads++;
        busyPcyclesOnNThreads++;
      }

#if DCVS_EXTRA_LOGS_ENABLED
      if ( CLM_engineContext.maxHWThreads == 4 )
      {
        uint32 sum = currPcyclesPerThread[0] + currPcyclesPerThread[1] + currPcyclesPerThread[2] + currPcyclesPerThread[3];
        ULOG_RT_PRINTF_5( clmUlogHandle, "\tRaw PMU Data (0x3b : %d) (0x3c : %d) (0x3d : %d) (0x3e : %d) (Sum : %d)",
                          currPcyclesPerThread[0], currPcyclesPerThread[1], currPcyclesPerThread[2], currPcyclesPerThread[3], sum );
      }
      else
      {
        uint32 sum = currPcyclesPerThread[0] + currPcyclesPerThread[1] + currPcyclesPerThread[2];
        ULOG_RT_PRINTF_4( clmUlogHandle, "\tRaw PMU Data (0x3b : %d) (0x3c : %d) (0x3d : %d) (Sum : %d)",
                          currPcyclesPerThread[0], currPcyclesPerThread[1], currPcyclesPerThread[2], sum );
      }
#endif

#else

      /* L2 Miss Stats - PMU Read */
      INTLOCK();
      //03=PPS; a0=ANU_DU_STALL; 9f=ANY_DU_STALL_CYCLES; 77=L2_IU_MISS; 0x03a09f77
      currL2IUmissCnt = qurt_pmu_get( QURT_PMUCNT0 );  //0x77
      currL2DUstallPcyclesCnt = qurt_pmu_get( QURT_PMUCNT1 ); //0x9F
      currL2DUstallCnt = qurt_pmu_get( QURT_PMUCNT2 );  //0xA0
      currPps = qurt_pmu_get( QURT_PMUCNT3 );  //0x03
      INTFREE();

      CLM_engineContext.engineCalc.l2IUmissCnt = currL2IUmissCnt - CLM_engineContext.engineCalc.prevL2IUmissCnt;
      CLM_engineContext.engineCalc.l2DUstallCnt = currL2DUstallCnt - CLM_engineContext.engineCalc.prevL2DUstallCnt;
      CLM_engineContext.engineCalc.l2DUstallPcyclesCnt = currL2DUstallPcyclesCnt -
                                                         CLM_engineContext.engineCalc.prevL2DUstallPcyclesCnt;
      CLM_engineContext.engineCalc.pps = currPps - CLM_engineContext.engineCalc.prevPps;

      CLM_engineContext.engineCalc.prevL2IUmissCnt = currL2IUmissCnt;
      CLM_engineContext.engineCalc.prevL2DUstallCnt = currL2DUstallCnt;
      CLM_engineContext.engineCalc.prevL2DUstallPcyclesCnt = currL2DUstallPcyclesCnt;
      CLM_engineContext.engineCalc.prevPps = currPps;

#endif

      /* Idle Cycles  */
      qurt_profile_get_idle_pcycles( idleCycles );
      uint64 * idleCyclesPtr = CLM_engineContext.engineCalc.idleCycles;
      uint64 * prevIdleCyclesPtr = CLM_engineContext.engineCalc.prevIdleCycles;
      for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
      {
        *idleCyclesPtr = idleCycles[i] - *prevIdleCyclesPtr;
        *prevIdleCyclesPtr = idleCycles[i];
        idleCyclesPtr++;
        prevIdleCyclesPtr++;
      }

      /* Utilized Cycles */
      currUtilizedCycles = qurt_get_core_pcycles();

      // utilized cycles from the QURT API (exact value)
      CLM_engineContext.engineCalc.utilizedCycles += (currUtilizedCycles -
                                                      CLM_engineContext.engineCalc.prevUtilizedCycles);

      CLM_engineContext.engineCalc.prevUtilizedCycles = currUtilizedCycles;
    }
  }
  else
  {
    if ( CLM_EngineIsClockGated() )
    {
      //clk gated and PMU unavailable
      // utilized cycles == budget (CPU Utilization will be 100%)
      CLM_engineContext.engineCalc.utilizedCycles = CLM_engineContext.engineCalc.sumBudgetCycles;
      CLM_engineContext.engineCalc.prevUtilizedCycles = CLM_engineContext.engineCalc.utilizedCycles;

    }
    else
    {
      /* Idle Cycles  */
      qurt_profile_get_idle_pcycles( idleCycles );
      uint64 * idleCyclesPtr = CLM_engineContext.engineCalc.idleCycles;
      uint64 * prevIdleCyclesPtr = CLM_engineContext.engineCalc.prevIdleCycles;
      for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
      {
        *idleCyclesPtr = idleCycles[i] - *prevIdleCyclesPtr;
        *prevIdleCyclesPtr = idleCycles[i];
        idleCyclesPtr++;
        prevIdleCyclesPtr++;
      }

      /* Utilized Cycles */
      currUtilizedCycles = qurt_get_core_pcycles();

      // utilized cycles from the QURT API
      CLM_engineContext.engineCalc.utilizedCycles += (currUtilizedCycles -
                                                      CLM_engineContext.engineCalc.prevUtilizedCycles);

      CLM_engineContext.engineCalc.prevUtilizedCycles = currUtilizedCycles;
    }
  }

  ULogFront_RealTimePrintf( clmUlogHandle, 6, "\tEngine "
                            "(ClockGated: %d) "
                            "(PMU Unavailable: %d) "
                            "(Busy Cycles: 0x%x%x) "
                            "(Budget Cycles: 0x%x%x) ",
                            //"(L2DUstallPcycleCnt: 0x%x) "
                            //"(l2DUstallCnt: 0x%x) "
                            //"(pps: 0x%x%x)",
                            CLM_engineContext.engineCalc.clkGated,
                            CLM_engineContext.engineCalc.pmuDisabled,
                            ULOG64_DATA( CLM_engineContext.engineCalc.utilizedCycles ),
                            ULOG64_DATA( CLM_engineContext.engineCalc.sumBudgetCycles ) );
  // CLM_engineContext.engineCalc.l2DUstallPcyclesCnt,
  // CLM_engineContext.engineCalc.l2DUstallCnt,
  // ULOG64_DATA(CLM_engineContext.engineCalc.pps));


}

/** 
 *  <!--  CLM_EngineInitPMUStats -->
 *  
 *  @brief Initializes the PMU context variables before the first
 *         measurement begins
 *
 *  @param None
 *
 *  @return None
 */
static void CLM_EngineInitPMUStats( void )
{
  uint32 i;

  INTLOCK();

  /* Get Core PCycles */
  CLM_engineContext.engineCalc.prevUtilizedCycles = qurt_get_core_pcycles();

  uint64 idleCycles[CLM_engineContext.maxHWThreads];
  uint64 * prevIdleCyclesPtr = CLM_engineContext.engineCalc.prevIdleCycles;
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 * prevPcyclesPerThreadPtr = CLM_engineContext.engineCalc.prevPcyclesPerThread;
#endif
  /* Current Idle Cycles */
  qurt_profile_get_idle_pcycles( idleCycles );

  for ( i = 0; i < CLM_engineContext.maxHWThreads; i++ )
  {
    *prevIdleCyclesPtr = idleCycles[i];
#if DCVS_NEW_ALGO_INPUT_ACTIVE
    *prevPcyclesPerThreadPtr = qurt_pmu_get( i );
    prevPcyclesPerThreadPtr++;
#endif
    prevIdleCyclesPtr++;
  }

#if !DCVS_NEW_ALGO_INPUT_ACTIVE
  CLM_engineContext.engineCalc.prevL2IUmissCnt = qurt_pmu_get( QURT_PMUCNT0 );
  CLM_engineContext.engineCalc.prevL2DUstallPcyclesCnt = qurt_pmu_get( QURT_PMUCNT1 );
  CLM_engineContext.engineCalc.prevL2DUstallCnt = qurt_pmu_get( QURT_PMUCNT2 );
  CLM_engineContext.engineCalc.prevPps = qurt_pmu_get( QURT_PMUCNT3 );
#endif
  INTFREE();

  ULogFront_RealTimePrintf( clmUlogHandle, 2, "Engine PMU Statistics Initialized "
                            "(prevUtilizedCycles: 0x%x%x)",
                            ULOG64_DATA( CLM_engineContext.engineCalc.prevUtilizedCycles ) );

}

/** 
 *  <!--  CLM_EngineGetMaxFreqKhz -->
 *  
 *  @brief Returns the max frequency supported
 *
 *  @param None
 *
 *  @return Max clock frequency in KHz
 */
static uint32 CLM_EngineGetMaxFreqKhz( void )
{
  npa_query_type queryResult;

  /* Error fatal if the clock plan data cannot be fetched. */
  CORE_VERIFY( !(npa_query( CLM_engineContext.queryHandle, CLOCK_NPA_QUERY_NUM_PERF_LEVELS, &queryResult )) );

  /* Fetch the number of levels from the from clkrgm */
  uint32 numLevels = queryResult.data.value;

  /* Error fatal if the clock plan data cannot be fetched. */
  CORE_VERIFY( !npa_query( CLM_engineContext.queryHandle,
                           CLOCK_NPA_QUERY_PERF_LEVEL_KHZ + numLevels - 1,
                           &queryResult ) );

  /* The value returned is in KHz. */
  return queryResult.data.value;
}



/** 
 *   <!--  CLM_EnteringLowPowerMode -->
 *  
 *   @brief Entering power collapse mode. Pause the timer since we
 *   want to exclude idle time in power collapse modes.
 *
 *   @param wakeupTick Not Used
 *
 *   @return None
 */
void CLM_EnteringLowPowerMode( uint64_t wakeupTick )
{
  //CLM_EnginePauseTimer( TRUE );
}

/** 
 *   <!--  CLM_ExitingLowPowerMode -->
 *  
 *   @brief Leaving power collapse mode. Resume our timer.
 *
 *   @param None
 *
 *   @return None
 */
void CLM_ExitingLowPowerMode( void )
{
  CLM_engineContext.engineCalc.lastLPRexitTS = qurt_sysclock_get_hw_ticks(); 
}

/** 
 *  <!--  CLM_InitLowPowerConfig_callback -->
 *  
 *  @brief Initializes the LPM Config for CLM
 *
 *  @param None
 *
 *  @return None
 */
static void CLM_InitLowPowerConfig_callback( void * clmClientContextPtr, unsigned int event_type, void * data, unsigned int data_size )
{
  npa_client_handle npaClientHandle;

  CORE_VERIFY_PTR( npaClientHandle = npa_create_sync_client( SLEEP_LPR_NODE_NAME,
                                                             "CLM", NPA_CLIENT_REQUIRED ) );
  sleep_define_lpr_str( "CLM", npaClientHandle );

  npa_issue_required_request( npaClientHandle, SLEEP_ALL_LPRMS );

}

/** 
 *  
 *  <!--  CLM_EngineRegisterClient -->
 *  
 *  @brief Registers an instance of CLM_Client with the Engine
 *
 *  @param CLM_Client handle
 *
 *  @param Client Window Size
 * 
 *  @return Handle to the CLM_EngineClientInfo struct
 */
void CLM_EngineRegisterClient( CLM_Client * newClient, uint32 measurementPeriodUs )
{

  CLM_Client * client;
  CORE_VERIFY_PTR( newClient );

  CLM_MUTEX_ACQUIRE();

  //Increment the number of registered clients/add to list
  CLM_engineContext.numClients++;

  if ( CLM_engineContext.clientList == NULL )
  {
    CLM_engineContext.clientList = newClient;
  }
  else
  {
    client = CLM_engineContext.clientList;

    while ( client->next != NULL )
    {
      client = client->next;
    }
    client->next = newClient;
  }

  if ( CLM_engineContext.timerStarted )
  {
    // new client has come in, we need to align this client's window
    // with next client's window

    // add the remaining time to the current period
    newClient->clientPendingTime = measurementPeriodUs
                                   + timer_get( CLM_engineContext.timer, T_USEC );
  }
  else
  {
    newClient->clientPendingTime = measurementPeriodUs;
    if ( CLM_engineContext.initDone )
    {
      CLM_EngineInitPMUStats();

      /* timer is being started now, hence prev elapsed time is 0 */
      CLM_engineContext.engineCalc.currentTimerPeriod = CLM_EngineFindNextTimerVal( 0 );

      /* Start the timer */
      CLM_EngineStartTimer( CLM_engineContext.engineCalc.currentTimerPeriod );
      ULOG_RT_PRINTF_1( clmUlogHandle, "Timer Started for %d us", CLM_engineContext.engineCalc.currentTimerPeriod );
      if ( 0 == bClmClkChangeEventRegistered )
      {
        CLM_EngineRegisterClkChgEvent();
        bClmClkChangeEventRegistered = 1;
      }
    }
  }

  CLM_MUTEX_RELEASE();
}

/** 
 *  <!--  CLM_EngineUnregisterClient -->
 * 
 *  @brief Unregisters the instance of CLM_Client from the
 *          Engine
 *
 *  @param engineClient : Handle to the CLM_EngineClientInfo
 *               struct previously registered
 *
 *  @return None
 */
void CLM_EngineUnregisterClient( CLM_HandleType engineClient )
{

  int clientFound = FALSE;
  CORE_VERIFY( engineClient );

  CLM_MUTEX_ACQUIRE();

  CLM_Client * client = CLM_engineContext.clientList;

  if ( client == NULL )
  {
    // list is empty
  }
  else if ( client == (CLM_Client *)engineClient )
  {
    // head element is the handle
    CLM_engineContext.clientList = client->next;

    // Free up the memory
    CORE_VERIFY( DAL_SUCCESS == DALSYS_Free( (void *)engineClient ) );

    clientFound = TRUE;
  }
  else
  {
    while ( client->next != NULL )
    {
      if ( client->next == (CLM_Client *)engineClient )
      {
        // found matching element !
        client->next = client->next->next;

        // Free up the memory
        CORE_VERIFY( DAL_SUCCESS == DALSYS_Free( (void *)engineClient ) );

        clientFound = TRUE;
      }
      client = client->next;
    }
  }

  if ( !clientFound )
  {
    // element not in list !
    CORE_VERIFY( NULL );
  }
  else
  {
    ULogFront_RealTimePrintf( clmUlogHandle, 0, "Engine ( Client Unregistered )" );
    CLM_engineContext.numClients--;
    if ( CLM_engineContext.numClients == 0 )
    {
      // Stop the active timer
      timer_clr( CLM_engineContext.timer, T_USEC );
      CLM_engineContext.timerStarted = 0;
    }
  }

  CLM_MUTEX_RELEASE();

}


/** 
 *  <!--  CLM_EngineTimer_callback -->
 * 
 *  @brief Callback function that gets triggered upon expiration of
 *        CLM Engine Timer
 *
 *  @param data : Timer related data passed in callback function
 *
 *  @return None
*/
void CLM_EngineTimer_callback( timer_cb_data_type data )
{
  CLM_MUTEX_ACQUIRE();

  CLM_EngineMeasure(); //need to keep a temp copy of the engineCalc determining if multiple clients have expired in the same callback and then update the actual struct accordingly

  CLM_engineContext.engineCalc.timerExpired = 1;
  CLM_engineContext.clmClientCb( &CLM_engineContext.engineCalc );
  CLM_engineContext.engineCalc.currentTimerPeriod = CLM_EngineFindNextTimerVal( 0 );
  
  ULogFront_RealTimePrintf( clmUlogHandle, 1,
                            "Engine End Timer Callback"
                            "(NextTimerStartedFor: %d)",
                            CLM_engineContext.engineCalc.currentTimerPeriod );

  // reset the timer with the next timer value
  CLM_EngineStartTimer( CLM_engineContext.engineCalc.currentTimerPeriod );

  // mark the start of next timer window
  CLM_engineContext.sysTickStart = qurt_sysclock_get_hw_ticks();

  // Reset and prepare for next window
  CLM_engineContext.engineCalc.sumBudgetCycles = 0;
  CLM_engineContext.engineCalc.timeElapsed = 0;
  CLM_engineContext.engineCalc.utilizedCycles = 0;

  CLM_MUTEX_RELEASE();

}


/** 
 *  
 *  <!--  CLM_EngineInitialization_callback -->
 *  
 *  @brief Callback function triggered upon availability of
 *        "/clk/cpu", "/sleep/lpr" resources
 *
 *  @param clmClientContextPtr The CLM Client Context
 *
 *  @param event_type The type of event
 *
 *  @param data The name of the node being created
 *
 *  @param data_size The length of the node name
 *
 *  @return None
 */
void CLM_EngineMemoryAllocateAndInitialize( void )
{
  uint32 maxHWThreads;

  maxHWThreads = CLM_engineContext.maxHWThreads;

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxHWThreads * sizeof(uint64),
                                             (void **)
                                             &CLM_engineContext.engineCalc.idleCycles ) );
  memset( CLM_engineContext.engineCalc.idleCycles, 0, maxHWThreads * sizeof(uint64) );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxHWThreads * sizeof(uint64),
                                             (void **)
                                             &CLM_engineContext.engineCalc.prevIdleCycles ) );
  memset( CLM_engineContext.engineCalc.prevIdleCycles, 0, maxHWThreads * sizeof(uint64) );

#if DCVS_NEW_ALGO_INPUT_ACTIVE
  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxHWThreads * sizeof(uint32),
                                             (void **)
                                             &CLM_engineContext.engineCalc.
                                             prevPcyclesPerThread ) );
  memset( CLM_engineContext.engineCalc.prevPcyclesPerThread, 0, maxHWThreads * sizeof(uint32) );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( maxHWThreads * sizeof(uint32),
                                             (void **)
                                             &CLM_engineContext.engineCalc.pcyclesPerThread ) );
  memset( CLM_engineContext.engineCalc.pcyclesPerThread, 0, maxHWThreads * sizeof(uint32) );
#endif
}


/** 
 *  
 *  <!--  CLM_EngineInitialization_callback -->
 *  
 *  @brief Callback function triggered upon availability of
 *        "/clk/cpu", "/sleep/lpr" resources
 *
 *  @param clmClientContextPtr The CLM Client Context
 *
 *  @param event_type The type of event
 *
 *  @param data The name of the node being created
 *
 *  @param data_size The length of the node name
 *
 *  @return None
 */
void CLM_EngineInitialization_callback( void * clmClientContextPtr, unsigned int event_type, void * data, unsigned int data_size )
{
  qurt_sysenv_max_hthreads_t maxHWThreads;
  CLM_MUTEX_ACQUIRE();

  CLM_EnginePMUInitialize();

  ULOG_RT_PRINTF_0( clmUlogHandle, "PMU Initialized" );

  //get max/current clk value in kHz
  CORE_VERIFY_PTR( CLM_engineContext.queryHandle =
                                                   npa_create_query_handle( "/clk/cpu" ) );

  CLM_engineContext.engineCalc.maxClkKhz = CLM_EngineGetMaxFreqKhz();

  CLM_engineContext.engineCalc.currentClkKhz = CLM_EngineCurrClockStateQuery();

  //read in max num HW threads
  qurt_sysenv_get_max_hw_threads( &maxHWThreads );
  CLM_engineContext.maxHWThreads = maxHWThreads.max_hthreads;
  ULOG_RT_PRINTF_1( clmUlogHandle, "Number of HW Threads %d ", CLM_engineContext.maxHWThreads );

  CLM_EngineMemoryAllocateAndInitialize();

  // allocate memory for timer
  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( sizeof(timer_type),
                                             (void
                                              **)&CLM_engineContext.timer ) );

  memset( CLM_engineContext.timer, 0, sizeof(timer_type) );

  CORE_VERIFY( DAL_SUCCESS == DALSYS_Malloc( sizeof(timer_group_type),
                                             (void
                                              **)&CLM_engineContext.clmTimerGroup ) );

  memset( CLM_engineContext.clmTimerGroup, 0, sizeof(timer_group_type) );

  // create timer for the CLM Engine
  timer_def( CLM_engineContext.timer, CLM_engineContext.clmTimerGroup,
             NULL, 0, CLM_EngineTimer_callback, 0 );

  //ensure the timer is deferrable since we are working with sleep
  timer_group_set_deferrable( CLM_engineContext.clmTimerGroup, TRUE );

  ULOG_RT_PRINTF_0( clmUlogHandle, "Timer Created" );

  CLM_engineContext.initDone = TRUE;

  CLM_MUTEX_RELEASE();

  npa_define_marker( "/clm/enabled" );

}

/** 
 *  <!--  CLM_EngineCreate -->
 *  
 *  @brief The engine create function that creates and
 *        initializes a singleton engine
 *
 *  @param cb : CLM_Client Callback function that will be called
 *           to send out measurement updates
 *
 *  @return None
 */
void CLM_EngineCreate( CLM_ClientCB cb )
{
  static const char * CLMdependencies[] =
  {
    "/clk/cpu",
    "/init/timer"
  };

  static const char * SleepDependencies[] =
  {
    SLEEP_LPR_NODE_NAME
  };

  CLM_engineContext.clmClientCb = cb;

  CORE_VERIFY_PTR( CLM_engineContext.clmMutexHandle =
                                                      Core_MutexCreate( CORE_MUTEXATTR_DEFAULT ) );

  /* Register and wait for the external system dependencies to be available */
  npa_resources_available_cb( NPA_ARRAY( CLMdependencies ), CLM_EngineInitialization_callback, NULL );

  /* Config LPM for CLM when sleep lpr node is available */
  npa_resources_available_cb( NPA_ARRAY( SleepDependencies ), CLM_InitLowPowerConfig_callback, NULL );
}


/**
 *  
 * <!-- CLM_EngineGetClients -->
 *  
 * @brief Returns a pointer to the beginning of the client list
 *
 *
 * @return CLM_HandleType : Pointer to the list head
 */
CLM_Client* CLM_EngineGetClients( void )
{
  return ( CLM_engineContext.clientList );
}

/**
 *  
 * <!-- CLM_EngineGetMaxHWThreads -->
 *  
 * @brief Returns the number of HW threads in the system
 *
 * @return uint32 : Number of HW threads 
 */
uint32 CLM_EngineGetMaxHWThreads( void )
{
  return ( CLM_engineContext.maxHWThreads );
}


/**
 *  
 * <!-- CLM_EngineGetLastLPRExitTime -->
 *  
 * @brief Returns the time of the last LPR exit 
 *
 * @return uint64 : the time of the last LPR exit umber of HW 
 *         threads
 */
uint64 CLM_EngineGetLastLPRExitTime( void )
{
  return ( CLM_engineContext.engineCalc.lastLPRexitTS );
}
