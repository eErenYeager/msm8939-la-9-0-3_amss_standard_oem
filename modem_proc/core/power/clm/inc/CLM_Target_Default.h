#ifndef _CLM_TARGET_DEFAULT_H_
#define _CLM_TARGET_DEFAULT_H_

/*===========================================================================

FILE:         CLM_Target_Default.h

DESCRIPTION:  Defines the default target macros for the CPU Load Monitor 
              (CLM) module

===========================================================================
             Copyright � 2013-2014 QUALCOMM Technologies, Incorporated.
                 All Rights Reserved.
                 QUALCOMM Confidential and Proprietary.
===========================================================================

                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/clm/inc/CLM_Target_Default.h#1 $

=============================================================================*/
#define TICKS_PER_SEC 19200000
#define MAX_CLIENTS 7
#define CPU_MAXED_OUT_PCT 95
#define SINGLE_THREAD_MAXED_OUT_PCT 90
#define CLM_ULOG_DEFAULT_BUFFER_SIZE_BYTES 65536

#endif // _CLM_TARGET_DEFAULT_H_
