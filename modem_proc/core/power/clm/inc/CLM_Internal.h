#ifndef _CLM_INTERNAL_H_
#define _CLM_INTERNAL_H_

/*===========================================================================

FILE:         CLM_Internal.h

DESCRIPTION:  Defines the Internal macros, declarations for the 
          CPU Load Monitor (CLM) module

===========================================================================
         Copyright � 2013 QUALCOMM Technologies, Incorporated.
         All Rights Reserved.
         QUALCOMM Confidential and Proprietary.
===========================================================================

            Edit History
$Header: //components/rel/core.mpss/3.7.24/power/clm/inc/CLM_Internal.h#1 $

=============================================================================*/
#include "DALSysTypes.h"
#include "DALSys.h"
#include "CoreMutex.h"
#include "ULog.h"

#define CLM_LOW_THRESHOLD_LEVEL 0
#define CLM_HIGH_THRESHOLD_LEVEL 1
#define CLM_UNKNOWN_THRESHOLD_LEVEL 2 

#define CLM_VAR_NO_EXTENDED_CPUUTIL 7
#define CLM_VAR_NO_CPUUTIL  3

#define CLM_TIMER_TOLERANCE 100

/* CLM Mutexes to protect critical sections of the code  */
#define CLM_MUTEX_ACQUIRE() { Core_MutexLock(CLM_engineContext.clmMutexHandle);

#define CLM_MUTEX_RELEASE()  Core_MutexUnlock(CLM_engineContext.clmMutexHandle); }

#endif // _CLM_INTERNAL_H_
