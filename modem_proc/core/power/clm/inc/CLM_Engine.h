/*===========================================================================

FILE:         CLM_Engine.h

DESCRIPTION:  Defines the API for the CPU Load Monitor (CLM) Engine. The CLM 
              Engine provides clients with periodic updates regarding CPU
              Utilization Measurements

===========================================================================
             Copyright � 2013-2014 QUALCOMM Technologies, Incorporated.
                 All Rights Reserved.
                 QUALCOMM Confidential and Proprietary.
===========================================================================

                        Edit History
$Header: //components/rel/core.mpss/3.7.24/power/clm/inc/CLM_Engine.h#1 $

=============================================================================*/
#ifndef _CLM_ENGINE_H_
#define _CLM_ENGINE_H_

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "CLM.h"
#include "CLM_Internal.h"
#include "ULog.h"
#include "timer.h"
#include "DALStdDef.h"
#include "npa.h"
#include CLM_TARGET_H 

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

typedef struct CLM_Client
{
  //client input
  uint32 highThresholdPercentage;
  uint32 lowThresholdPercentage;
  uint32 numThreadsForHighTrigger;
  uint32 numThreadsForLowTrigger;
  uint32 measurementPeriodUs;
  CLM_ClientOutputType clientType;
  uint32 attributes;
  const char * clientName;
  CLM_CallbackFuncPtr clmCBFn;
  void * clientData;

  //basic
  uint64 elapsedTimeUs;
  uint32 busyPctCurrClk;
  uint32 busyPctMaxClk;
  //extended
  uint64 budgetPcycles;
  uint64 utilizedPcycles;
  uint64 * idleCycles; //[CLM_MAX_HW_THREADS];
  //pmu
#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 busyPctSingleThread;
  uint32 * pcyclesPerThread; //[CLM_MAX_HW_THREADS];
#else
  uint64 l2IUmissCnt;
  uint64 l2DUstallPcyclesCnt;
  uint64 l2DUstallCnt;
  uint64 pps;
#endif

  //engine fields
  int32 clientPendingTime;
  char lastKnownThreshold; // 1 == High. 0 == Low
  void * loadInfo; //points to client-specific data structure passed into client callback
  struct CLM_Client * next;


}CLM_Client; 


typedef struct
{
  uint32 currentClkKhz;
  uint32 maxClkKhz;
  uint64 sumBudgetCycles;
  uint64 prevUtilizedCycles;
  uint64 utilizedCycles;
  uint64 * idleCycles;
  uint64 * prevIdleCycles;

#if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 * prevPcyclesPerThread;
  uint32 * pcyclesPerThread;
#else
  uint32 l2IUmissCnt;
  uint32 l2DUstallPcyclesCnt;
  uint32 l2DUstallCnt;
  uint64 pps;

  uint32 prevL2IUmissCnt;
  uint32 prevL2DUstallPcyclesCnt;
  uint32 prevL2DUstallCnt;
  uint64 prevPps;
#endif

  uint32 timerExpired;

  uint32 prevOneThreadCycleCnt;
  uint32 prevTwoThreadCycleCnt;
  uint32 prevThreeThreadCycleCnt;

  /* time in usec */
  uint64 timeElapsed;

  /* period in us that the timer is set for */
  uint32 currentTimerPeriod;

  /* Flag to indicate if PMU is disabled for CLM usage */
  uint32 pmuDisabled;

  /* Flag to indiacate if clock gating is disabled */
  uint32 clkGated;

  /* Flag to indicate if PMU was initialize under clk gating */
  uint32 pmuUnderClkGatingInitialized;

  /* Indicates the time of the last PC exit */
  uint64 lastLPRexitTS;

}CLM_EngineCalculation;

typedef void (*CLM_ClientCB)( CLM_EngineCalculation * );


/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/* ULog Handle */
extern ULogHandle	clmUlogHandle;

/**
 *  
 * <!-- CLM_EngineRegisterClient -->
 *  
 * @brief CLM Clients needing CPU load information must register
 *      using this API.
 *  
 *
 * @param handle : pointer to the CLM Client Handle (that is 
 *                              unique to every external client
 *                              that registered with the CLM
 * 
 * @param measurementPeriodUs: window size for client meas 
 *
 * @return void 
 */
void CLM_EngineRegisterClient( CLM_Client * clmClient, uint32 measurementPeriodUs );


/**
 *  
 * <!--  CLM_EngineUnregisterClient -->
 *  
 * If a CLM client no longer needs the services of the CLM 
 * Engine, it should un-register using this API. 
 * 
 * @param handle : Handle that was provided
 *               by a prior CLMEngine_RegisterClient() call.
 */
void CLM_EngineUnregisterClient( CLM_HandleType clientContext );

/**
 *  
 * <!-- CLM_EngineCreate -->
 *  
 * Creates an instance of CLM Engine
 * allocate memory for the client structure 
 *  
 * The CLM Engine module uses the Performance Monitor Unit (PMU) for its
 * measurements - However CLM may not be the only module using the PMU and
 * incase some other module is actively using the PMU, we need to notified to
 * handle this appropriately. This notification is done by the QURT framework
 * by calling the function DcvsQdspPMUbusy (Defined in CLM_Engine.c) 
 *
 * @param Callback Function Pointer
 *
 * @return void
 */
void CLM_EngineCreate( CLM_ClientCB cb );


/**
 *  
 * <!-- CLM_EngineGetClients -->
 *  
 * @brief Returns a pointer to the beginning of the client list
 *
 *
 * @return Pointer to the list head
 */
CLM_Client* CLM_EngineGetClients( void );


/**
 *  
 * <!-- CLM_EngineGetMaxHWThreads -->
 *  
 * @brief Returns the number of HW threads in the system
 *
 * @return uint32 : Number of HW threads 
 */
uint32 CLM_EngineGetMaxHWThreads( void );


/**
 *  
 * <!-- CLM_EngineGetLastLPRExitTime -->
 *  
 * @brief Returns the time of the last LPR exit 
 *
 * @return uint64 : the time of the last LPR exit umber of HW 
 *         threads
 */
uint64 CLM_EngineGetLastLPRExitTime(void); 


/**
 *  
 * <!-- CLM_UpdateStats -->
 *  
 * @brief Updates the client data structures with information from a timer expiry,
 *        PC enter/exit, or a clock change event
 *
 * @param client : The client to update 
 *  
 * @param clmEngineCalc : pointer to the structure containing 
 *                      the latest information from the CLM
 *                      engine
 *
 * @return none
 */
void CLM_UpdateStats( CLM_Client * client, CLM_EngineCalculation * clmEngineCalc );

#endif // _CLM_Engine_H_

