/*============================================================================
@file cpr_disabled.c

CPR driver stub that interfaces with the system when the CPR HW
is not used. The driver will still continue to correct clkrgm's
default voltage with the offset values blown into the efuses.

Copyright � 2013 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/src/common/cpr_disabled.c#1 $
============================================================================*/
#include "DALStdDef.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DDIClock.h"
#include "CoreVerify.h"
#include "cpr_internal.h"

/* Contains all the CPR context data. */
extern CprContextType cprContext;


/**
 * <-- CprAcquireSystemResourcesInDisabledMode -->
 *
 * @brief Acquire resources necessary to correct the default clkrgm voltage
 * with the efused offsets when CPR has been DISABLED in the efuses.
 */
inline static void CprAcquireSystemResourcesInDisabledMode( void )
{
  // We need a DAL handle to be able to correct the default voltage used by
  // clkrgm with initVoltOffsetSteps since this is applicable whether or not
  // CPR is enabled.
  CORE_DAL_VERIFY(DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &cprContext.refClkHandle));
}


/**
 * <-- CprDisabledModePreSwitchCallback -->
 *
 * @brief Stub - nothing to do here.
 */
static void CprDisabledModePreSwitchCallback( void )
{
}


/**
 * <-- CprDisabledModeSetVoltageMode -->
 *
 * @brief  Correct the default clkrgm voltage with the efused offsets obtained
 * by characterization in the manufacturing processes.
 * 
 * @param newMode : Voltage mode clkrgm is switching to.
 * @param newVoltage : Return value in microVolts
 */
static void CprDisabledModeSetVoltageMode( uint32   newMode,
                                           uint32 * newVoltage )
{
  CORE_VERIFY(newMode < CPR_VMODE_COUNT);

  *newVoltage = cprContext.measurements[newMode].currentVoltage;

  ULogFront_RealTimePrintf(cprContext.ulogHandle,
                           2,
                           "CprDisabled (Vmode: %u) (Voltage: %u)",
                           newMode,
                           *newVoltage);
}


/**
 * <-- CprDisabledModePostClockSwitchCallback -->
 *
 * @brief Callback that is invoked by clkrgm after it has completed switching
 * to a new voltage/reference clock rate/changed the power source for the CPU. 
 * 
 * @param qdsp6NotUsingMssRail : Set to TRUE if CPU in the modem subsystem 
 *           is using an LDO instead of directly using the MSS rail. 
 * @param refClkRateKhz : CPR reference clock rate in kHz
 * @param currRailVoltage : MSS rail voltage in microVolts
 */
static void CprDisabledModePostClockSwitchCallback( boolean qdsp6NotUsingMssRail,
                                                    uint32  refClkRateKhz,
                                                    uint32  currRailVoltage )
{
  // Nothing to do here
}


/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
/**
 * <-- CprInitDisabledMode -->
 *
 * @brief Setup the driver to handle voltage requirements in disabled mode.
 *
 * Initialize the driver so that CPR HW is not used. The driver will still
 * continue to correct clkrgm's default voltage with the offset
 * values blown into the efuses.
 */
void CprInitDisabledMode( void )
{
  // Even though CPR is disabled, we should still apply the 
  // initVoltOffsetSteps for each voltage mode. This could be handled in
  // clkrgm as well, if they read the same fuses, but we'll do it here
  // so that we can localize all the related info in the CPR driver.

  CprAcquireSystemResourcesInDisabledMode();

  // We'll just be correcting the default clkrgm voltage with a fixed
  // offset, but clkrgm doesn't need to know that!
  DalClock_RegisterCPRCallbacks(cprContext.refClkHandle,
                                CprDisabledModePreSwitchCallback,
                                CprDisabledModePostClockSwitchCallback,
                                CprDisabledModeSetVoltageMode);
}

