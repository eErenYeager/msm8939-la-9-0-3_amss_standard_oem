/*============================================================================
@file cpr_enabled.c

CPR driver implementation. Will manage the CPR HW if the user and efuse
config allow the use of CPR.

Copyright � 2013-2014 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/src/common/cpr_enabled.c#1 $
============================================================================*/
#include "DALStdDef.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "CoreVerify.h"
#include "DDIClock.h"
#include "DDIInterruptController.h"
#include "HAL_cpr.h"
#include "cpr_internal.h"
#include "ClockDefs.h"
#include "npa.h"

/* Contains all the CPR context data. */
extern CprContextType cprContext;

static DalDeviceHandle *interruptHandle = NULL;

/**
 * <!-- CprEnable -->
 *
 * @brief Enable/disable CPR measurements and associated interrupts.
 * 
 * @param enable: TRUE to enable; FALSE otherwise.
 */
static void CprEnable( boolean enable )
{
  HalCprConfigInterrupts(enable, cprContext.upIntEnabled, cprContext.downIntEnabled);
  HalCprEnable(enable);
}


/**
 * <!-- CprSwitchVoltage -->
 *
 * @brief Tell the clock driver to switch to the new voltage at the specified
 * voltage mode.
 * 
 * @param newVoltage: voltage in microVolts
 * 
 * @return boolean: TRUE if successful; FALSE otherwise.
 */
static boolean CprSwitchVoltage( uint32 newVoltage )
{
  DALResult retval;	
  boolean   cpuNotUsingMssRail = FALSE;

  retval = DalClock_SetVoltage(cprContext.refClkHandle,
                               CLOCK_VREG_VDD_MSS,
                               newVoltage,
                               &cpuNotUsingMssRail);

  if (retval == DAL_SUCCESS)
  {
    cprContext.measurements[cprContext.currentVmode].currentVoltage = newVoltage;

    // Disable the sensors on the CPU if clkrgm used the LDO to power the CPU
    // at a lower voltage - CPR HW needs all the sensors to be at the
    // same voltage.
    HalCprBypassCpuBlock(cpuNotUsingMssRail);

    return TRUE;
  }
  return FALSE;
}


/**
 * <!-- CprPreSwitchConfig -->
 *
 * @brief Prepare the HW for a voltage/reference clock switch by disabling & 
 * clearing interrupts. 
 */
static void CprPreSwitchConfig( void )
{
  cprContext.switchingVmode = TRUE;

  // Disable the HW in preparation for the switch.
  CprEnable(FALSE);

  // Clear any pending interrupts
  HalCprClearAllInterrupts();
}


/**
 * <!-- CprEnabledModePreSwitchCallback -->
 *
 * @brief Callback which will be invoked by clkrgm prior to switching to a 
 * different voltage mode. 
 *
 * The CPR driver uses this callback to disable the CPR block in preparation for
 * the mode switching. clkrgm is required to invoke CprEnabledModeSetVoltageMode() 
 * after it finishes switching to the newMode.
 */
static void CprEnabledModePreSwitchCallback( void )
{
  CPR_MUTEX_ACQUIRE();

  CprPreSwitchConfig();

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (SwitchingMode: 1) (Enabled: 0)");

  CPR_MUTEX_RELEASE();
}


/**
 * <!-- CprEnabledModeSetVoltageMode -->
 *
 * @brief Callback that is invoked by clkrgm whenever it switches to another 
 * voltage mode to query CPR for the recommended voltage to use for that mode.
 *
 * Clkrgm should follow this call with a call to 
 * CprEnabledModePostSwitchCallback().
 * 
 * @param newMode : New voltage mode
 * @param newVoltage : Return value in microVolts
 */
static void CprEnabledModeSetVoltageMode( uint32   newMode,
                                          uint32 * newVoltage)
{
  CORE_VERIFY(newMode < CPR_VMODE_COUNT);

  CPR_MUTEX_ACQUIRE();

  // Use the last recommendation from the HW.
  *newVoltage = cprContext.measurements[newMode].currentVoltage;

  if (!cprContext.switchingVmode)
  {
    if ((cprContext.currentVmode < CPR_VMODE_COUNT) &&
        (newMode != cprContext.currentVmode))
    {
      ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (ERROR: SetVoltage called before PreSwitch)");

      // Disable the HW as a safety measure
      CprPreSwitchConfig();
    }
  }
  cprContext.currentVmode = newMode;

  // Cap at the ceiling and disable/enable up ints
  if (*newVoltage >= cprContext.targetCfg->vmodeCfg[newMode].voltageCeiling)
  {
    *newVoltage = cprContext.targetCfg->vmodeCfg[newMode].voltageCeiling;
    cprContext.upIntEnabled = FALSE;
  } 
  else  
  {
    cprContext.upIntEnabled = TRUE;
  }

  // Cap at the floor and disable/enable down ints
  if (*newVoltage <= cprContext.targetCfg->vmodeCfg[newMode].voltageFloor)
  {
    *newVoltage = cprContext.targetCfg->vmodeCfg[newMode].voltageFloor;
    cprContext.downIntEnabled = FALSE;
  } else {
    cprContext.downIntEnabled = TRUE;
  }

  cprContext.measurements[newMode].currentVoltage = *newVoltage;

  ULogFront_RealTimePrintf(cprContext.ulogHandle,
                           2,
                           "CPR (Vmode: %u) (Voltage: %u)",
                           newMode,
                           *newVoltage);

  CPR_MUTEX_RELEASE();
}


/**
 * <!-- CprEnableDownRecommendation -->
 *
 * @brief Change the cprContext downIntEnabled flag and call the
 * Hal configuration function.
 *
 * @param enable : turn on or off the down enable. 
 */
void CprEnableDownRecommendation( boolean enable ){
  cprContext.downIntEnabled  = enable;
  HalCprEnableDownRecommendation(enable);
}


/**
 * <!-- CprEnableUpRecommendation -->
 *
 * @brief Change the cprContext upIntEnabled flag and call the
 * Hal configuration function.
 *
 * @param enable : turn on or off the up enable. 
 */
void CprEnableUpRecommendation( boolean enable ){
  cprContext.upIntEnabled  = enable;
  HalCprEnableUpRecommendation(enable);
}


/**
 * <!-- CprEnabledModePostSwitchCallback -->
 *
 * @brief Callback that is invoked by clkrgm after it has completed switching
 * to a new voltage/reference clock rate/changed the power source for the CPU. 
 * 
 * @param cpuNotUsingMssRail : Set to TRUE if CPU in the modem subsystem 
 *           is using an LDO instead of directly using the MSS rail. 
 * @param refClkRateKhz : CPR reference clock rate in kHz
 * @param currRailVoltage : MSS rail voltage in microVolts
 */
static void CprEnabledModePostSwitchCallback( boolean cpuNotUsingMssRail,
                                              uint32  refClkRateKhz,
                                              uint32  currRailVoltage )
{

  if (!(cprContext.enableFlags_ClosedLoopEnabled) || (cprContext.enableFlags_DisabledByFuse))
  {
    return;
  }

  CORE_VERIFY( CPR_IS_VALID_REFERENCE_CLOCK_RATE(refClkRateKhz) );

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 1, "CprEnabledModePostSwitchCallback(): refClkRateKhz: %u", refClkRateKhz);

  CPR_MUTEX_ACQUIRE();

  // At init, currentVmode is set to CPR_VMODE_INVALID 
  if (cprContext.currentVmode >= CPR_VMODE_COUNT)
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (ERROR: Post called before setting initial Voltage mode)");

    // Save the update anyways 
    cprContext.cpuNotUsingMssRail = cpuNotUsingMssRail;
    cprContext.currentRefClkRate = refClkRateKhz;

    DALSYS_SyncLeave(cprContext.mutexHandle);
    return;
  }

  if (!cprContext.switchingVmode &&
      ((cprContext.cpuNotUsingMssRail == cpuNotUsingMssRail) &&
       (cprContext.currentRefClkRate == refClkRateKhz) &&
       (cprContext.measurements[cprContext.currentVmode].currentVoltage == currRailVoltage)))
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (ERROR: Post called but params have not changed)");
    DALSYS_SyncLeave(cprContext.mutexHandle);
    return;
  }

  if (!cprContext.switchingVmode)
  {
    if(cprContext.clearedAfterFirstUse)
    {
      /* The first time PostSwitch is called the CPR driver is enabled.
         For future transitions we will always expect PreSwitch to be called before PostSwitch. */
      cprContext.clearedAfterFirstUse = FALSE; 
      ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR PostSwitch enabling CPR for 1st use.");
    } else {
      ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (ERROR: Post called before PreSwitch)");
    }
    // Try to recover as best as we can 
    CprPreSwitchConfig();
  }

  /* The first time PostSwitch is called the CPR driver is enabled.
     For future transitions we will always expect PreSwitch to be called before PostSwitch. */
  cprContext.clearedAfterFirstUse = FALSE;
  

  cprContext.cpuNotUsingMssRail = cpuNotUsingMssRail;
  cprContext.currentRefClkRate = refClkRateKhz;

  /* Reconfigure the CPR HW with the new settings */
  HalCprSetupVoltageMode(cprContext.currentVmode, refClkRateKhz, cprContext.targetCfg->delayBeforeNextMeas);

  HalCprBypassCpuBlock(cpuNotUsingMssRail);

  if (currRailVoltage <= cprContext.targetCfg->vmodeCfg[cprContext.currentVmode].voltageFloor)
  {
    /* Disable the down interrupts if we're already at the floor voltage. */
    CprEnableDownRecommendation(FALSE);
  } else {
    CprEnableDownRecommendation(TRUE);
 }

  if (currRailVoltage >= cprContext.targetCfg->vmodeCfg[cprContext.currentVmode].voltageCeiling)
  {
    /* Disable the up interrupts if we're already at the ceiling voltage. */
    CprEnableUpRecommendation(FALSE);
  } else {
    CprEnableUpRecommendation(TRUE);
  }

  if (currRailVoltage != cprContext.measurements[cprContext.currentVmode].currentVoltage)
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 3,
                             "ERROR (RailVolt: %lu) (Vmode: %lu) (Voltage: %lu)",
                             currRailVoltage, cprContext.currentVmode,
                             cprContext.measurements[cprContext.currentVmode].currentVoltage);

    cprContext.measurements[cprContext.currentVmode].currentVoltage = currRailVoltage;

    // Log the voltage so the USee tool can plot the voltage accurately.
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 1, "CPR (Voltage: %lu)", currRailVoltage);
  }

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 2,
                           "CPR (SwitchingMode: 0) (Enabled: 1) (RefClkRate: %lu) (CPUnotUsingRail: %lu)",
                           refClkRateKhz, cpuNotUsingMssRail);

  cprContext.switchingVmode = FALSE;
  CprEnable(TRUE);

  CPR_MUTEX_RELEASE();
}


/**
 * <!-- CprGetOffsetRecommendation -->
 *
 * @brief Get the new offset recommended by the HW.
 * 
 * @return int32: offset in microVolts
 */
static int32 CprGetOffsetRecommendation(void)
{
  // Poll until the HW is not busy - this shouldn't really be necessary since
  // the interrupt already implies that a result is ready. 
  if (HalCprIsBusy())
  {
      ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR (ERROR: CPR is busy following ISR)");
      while(HalCprIsBusy());
  }

  while(HalCprIsBusy());

  return (HalCprGetOffsetRecommendation() * cprContext.targetCfg->voltageStepSize);
}


/**
 * <!-- CprIsr -->
 *
 * @brief ISR for CPR interrupts. Makes corrections to the voltage at the current
 * operating voltage mode based on the HW's recommendation.
 * 
 * @param unused
 */
static void CprIsr( uint32 unused )
{
  boolean           ack = TRUE;
  int32             offset;
  uint32            currVmode;
  uint32            newVoltage;
  CprVmodeCfgType * vmodeCfg;

  if (!(cprContext.enableFlags_ClosedLoopEnabled) || (cprContext.enableFlags_DisabledByFuse))
  {
    return;
  }

  CPR_MUTEX_ACQUIRE();

  if (cprContext.switchingVmode ||
      (cprContext.currentVmode >= CPR_VMODE_COUNT) ||
      (!cprContext.currentRefClkRate))
  {
    // This may happen if the interrupt fired just as we were executing the
    // PreSwitch callback.
    HalCprClearAllInterrupts();
    HalCprStartNextMeasurement(FALSE);
    DALSYS_SyncLeave(cprContext.mutexHandle);
    return;
  }

  HalCprLogInterruptStatus();

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 1,
                           "CPR reference clock: %u",
                           cprContext.currentRefClkRate);

  HalCprLogCprRegisters (cprContext.ulogHandle);
  
  currVmode = cprContext.currentVmode;
  vmodeCfg = &cprContext.targetCfg->vmodeCfg[currVmode];

  offset = CprGetOffsetRecommendation();

  if (offset < (int32)(-cprContext.targetCfg->voltageStepSize))
  {
    // Limit the downward changes to lowering one step at a time. 
    offset = (-cprContext.targetCfg->voltageStepSize);
  }

  if (offset > (int32)(cprContext.targetCfg->voltageStepSize))
  {
    // Limit the upward changes to increasing one step at a time. 
    offset = cprContext.targetCfg->voltageStepSize;
  }

  // Calculate what the new voltage would be with the change. 
  newVoltage = cprContext.measurements[currVmode].currentVoltage + offset;

  if (newVoltage == vmodeCfg->voltageFloor)    				// --- At the floor
  {
    // Disable down interrupts if we're at the floor voltage.
    CprEnableDownRecommendation(FALSE);

    // Allow up interrupts in case we previously disabled them
    CprEnableUpRecommendation(TRUE);

    ULOG_RT_PRINTF_2(cprContext.ulogHandle, "Arrived at %d floor for mode %d", newVoltage, currVmode);
  }
  else if (newVoltage == vmodeCfg->voltageCeiling)          // --- At the ceiling
  {
    // Disable up interrupts if we're at the ceiling.
    CprEnableUpRecommendation(FALSE);

    // Allow down interrupts in case we previously disabled them
    CprEnableDownRecommendation(TRUE);

    ULOG_RT_PRINTF_2(cprContext.ulogHandle, "Arrived at %d ceiling for mode %d", newVoltage, currVmode);
  }
  else if (newVoltage < vmodeCfg->voltageFloor)             // --- Below the floor
  {
    
    CprEnableDownRecommendation(FALSE);

    // Allow up interrupts in case we previously disabled them
    CprEnableUpRecommendation(TRUE);

    ULOG_RT_PRINTF_3(cprContext.ulogHandle, "Calculated %d voltage for mode %d.  Voltage will be limited to %d",
                     newVoltage, currVmode, vmodeCfg->voltageFloor);
					 
    // Limit ourselves to the floor
    newVoltage = vmodeCfg->voltageFloor;

    offset = newVoltage - cprContext.measurements[currVmode].currentVoltage;
  }
  else if (newVoltage > vmodeCfg->voltageCeiling)           // --- Above the ceiling
  {

    ULOG_RT_PRINTF_3(cprContext.ulogHandle, "Calculated %d voltage for mode %d.  Voltage will be limited to %d",
                     newVoltage, currVmode, vmodeCfg->voltageCeiling);

					 
    // Cap off the voltage at the ceiling
    newVoltage = vmodeCfg->voltageCeiling;

    // Disable up interrupts
    CprEnableUpRecommendation(FALSE);
    // Allow down interrupts in case we previously disabled them
    CprEnableDownRecommendation(TRUE);

  }
  else                                                 // --- Between the ceiling and floor.
  {
    // Allow UP/DOWN interrupts in case we previously disabled them
    CprEnableDownRecommendation(TRUE);
    CprEnableUpRecommendation(TRUE);
  }


  // If there's ever a case where the offset was 0 and the newVoltage is the same as the currentVoltage we should nack.
  if (newVoltage == cprContext.measurements[currVmode].currentVoltage){
    ULOG_RT_PRINTF_2(cprContext.ulogHandle,
                     "CPR No voltage change (CurMode: %d) (Voltage: %u)",
                     currVmode, cprContext.measurements[currVmode].currentVoltage);
    ack = FALSE;
  } else {
    //we are requesting a voltage move so we intend to ack.
    ack = TRUE;
  }
  

  // Always inform clkrgm so that it can tell us if the CPU sensors need to be
  // disabled (due to LDO usage).
  if (CprSwitchVoltage(newVoltage) == FALSE)
  {
    //if the voltage change was not done we should not ack. 
    ack = FALSE;
  }

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 4,
                           "CPR (ISR: 0) (Voltage: %u) (Offset: %d) (Ack: %d) (Vmode: %lu)",
                           cprContext.measurements[currVmode].currentVoltage,
                           offset, ack, currVmode);

  HalCprClearAllInterrupts();
  HalCprStartNextMeasurement(ack);

  CPR_MUTEX_RELEASE();
}


/**
 * <!-- CprEnabledModeAcquireSystemResources -->
 *
 * @brief Acquire clocks, mutex etc. for the CPR driver.
 *
 * CPR Hardware initialization should NOT be done here - that should be in
 * HalCprInit().
 */
inline static void CprEnabledModeAcquireSystemResources( void )
{
  ClockIdType clockId;

  // Enable the rbcpr bus clock.
  CORE_DAL_VERIFY( DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &cprContext.busClkHandle));

  DalClock_GetClockId(cprContext.busClkHandle, "clk_bus_rbcpr", &clockId);
  DalClock_EnableClock(cprContext.busClkHandle, clockId);

  // Enable the rbcpr reference clock.
  CORE_DAL_VERIFY(DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &cprContext.refClkHandle));

  DalClock_GetClockId(cprContext.refClkHandle, "clk_bus_rbcpr_ref", &clockId);
  DalClock_EnableClock(cprContext.refClkHandle, clockId);

  // Take the clock out of reset state.
  DalClock_ResetClock(cprContext.refClkHandle, clockId, CLOCK_RESET_DEASSERT);

  // Create a mutex
  CORE_DAL_VERIFY( DALSYS_SyncCreate( DALSYS_SYNC_ATTR_RESOURCE_INTERRUPT,
                                      &cprContext.mutexHandle,
                                      &cprContext.syncObj));

  // Get an interrupt controller handle
  CORE_DAL_VERIFY(DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER, &interruptHandle));

  // Register the RBCPR interrupt.
  CORE_DAL_VERIFY(DalInterruptController_RegisterISR(interruptHandle, 
                                           cprContext.targetCfg->irq,
                                           (DALIRQ)CprIsr, 
                                           (DALISRCtx)0, 
                                           DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER));
}


/**
 * <!-- CprGoToLowPowerMode -->
 *
 * @brief Enable/disable CPR HW when entering/exiting low power modes.
 * 
 * @param inLowPowerMode: TRUE if entering low power mode; FALSE otherwise.
 */
void CprGoToLowPowerMode( boolean inLowPowerMode )
{
  // If the CPR driver hasn't begun being used don't change the enable state.
  if (cprContext.clearedAfterFirstUse==FALSE){
    //disable or re-enable CPR
    CprEnable(!inLowPowerMode);
  }
}


/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
/**
 * <!-- CprInitEnabledMode -->
 *
 * @brief Initialize the driver so that CPR HW can be used to manager the voltages.
 */
void CprInitEnabledMode( void )
{
  CprEnabledModeAcquireSystemResources();

  //begin with ints enabled. 
  cprContext.downIntEnabled = TRUE;
  cprContext.upIntEnabled = TRUE;

  HalCprInit(cprContext.halTargetCfg, cprContext.ulogHandle);

  // Set this "clearedAfterFirstUse" flag so that the first time the postSwitch
  // function is used to turn on CPR on we don't mind that preswitch hasn't been
  // called yet it and log an error message. 
  cprContext.clearedAfterFirstUse = TRUE;  

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "CPR Ready - Registering CPR Switch Mode Callbacks");
  
  // Tell the clock driver that we're ready to operate.
  DalClock_RegisterCPRCallbacks(cprContext.busClkHandle,
                                CprEnabledModePreSwitchCallback,
                                CprEnabledModePostSwitchCallback,
                                CprEnabledModeSetVoltageMode);

  cprContext.lpFunc = CprGoToLowPowerMode;
}

/**
 * <!-- CprDeRegisterRBCPRInterrupt -->
 *
 * @brief De-register the RBCPR interrupt when Q6 goes to power collapse, so that no
 *        pending CPR interrupt wakes up Q6 from power collapse.
 */
void CprDeRegisterRBCPRInterrupt (void)
{
  if( DalInterruptController_Unregister(
            interruptHandle,
            cprContext.targetCfg->irq ) != DAL_SUCCESS )
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 1, "CprDeRegisterRBCPRInterrupt: INT %d disablement unsuccessful", cprContext.targetCfg->irq);
  }
}

/**
 * <!-- CprRegisterRBCPRInterrupt -->
 *
 * @brief Register the RBCPR interrupt when Q6 comes out of power collapse.
 */
void CprRegisterRBCPRInterrupt (void)
{
  CORE_DAL_VERIFY(DalInterruptController_RegisterISR(interruptHandle, 
                                           cprContext.targetCfg->irq,
                                           (DALIRQ)CprIsr, 
                                           (DALISRCtx)0, 
                                           DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER));
  
}

