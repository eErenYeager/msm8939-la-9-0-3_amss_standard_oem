/*============================================================================
@file cpr.c

Implements the Rapid Bridge Core Power Reduction driver.

Copyright � 2013, 2014 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/src/common/cpr.c#1 $
============================================================================*/
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "cpr_internal.h"
#include "ClockDefs.h"
#include "DALDeviceId.h"
#include "DDIClock.h"
#include "npa.h"
#include "sleep_lpr.h"
#include "cpr_starting_voltages.h"

/** Contains all the CPR context data. */
CprContextType cprContext;


/**
 * <!-- convertToIndex -->
 *
 * @brief Converts the configuration flags into an index to be used in 
 * the starting_voltage_truth_table
 * 
 * @param lsb : Bit 0
 * @param b1 : Bit 1
 * @param b2 : Bit 2
 * @param msb : Bit 3
 *
 * @return The value of the combined bits. 
 */
inline static uint32 convertToIndex( boolean msb, boolean b2, boolean b1, boolean lsb )
{
  uint32 return_val = 0;

  if (lsb){
    return_val |= 1;
  }

  if (b1){
    return_val |= 2;
  }

  if (b2){
    return_val |= 4;
  }

  if (msb){
    return_val |= 8;
  }

  return return_val;
}


/**
 * <!-- CprInitStateData -->
 *
 * @brief Set initial values for the cprContext data structure before 
 * beginning to use the HW.
 */
inline static void CprInitStateData( void )
{
  CprVModeEnumType vmode;

  /* Determine whether to use the open loop or ceiling voltages as starting.
   * Turn the 4 enableFlags into an index into the truth table to get the 
   * correct option. 
   */
  char starting_voltage_config = 
             starting_voltage_truth_table[convertToIndex(cprContext.enableFlags_TTtiming,
                                                         cprContext.enableFlags_DisabledByFuse,
                                                         cprContext.enableFlags_OpenLoopEnabled,
                                                         cprContext.enableFlags_ClosedLoopEnabled)];

  if (starting_voltage_config == OPEN_LOOP_STARTING_VOLTAGES)
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "Using open loop starting voltages.");
  }    
  else if (starting_voltage_config == CEILING_STARTING_VOLTAGES)
  {
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 0, "Using ceiling starting voltages");
  }

  /* Until we get a callback from clkrgm, we won't know what voltage mode
     clkrgm is currently using. */
  cprContext.currentVmode = CPR_VMODE_INVALID;

  /* Correct each mode's starting voltage with the offset specified in the
     fuse target config values. This offset is typically derived from
     HW characterization. */
  for (vmode=0; vmode<CPR_VMODE_COUNT; vmode++)
  {
    int pmicSteps =0;
    if (starting_voltage_config == OPEN_LOOP_STARTING_VOLTAGES)
    {
      //ULogFront_RealTimePrintf(cprContext.ulogHandle, 2, "Voltage steps in Vmode %u = %d", vmode, cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps);
	  
      /* Only allow a decrease from the ceiling voltage. Clip positive increases to 0. */
      if(cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps > 0){
        ULogFront_RealTimePrintf(cprContext.ulogHandle, 
                                 2,
                                 "Positive initial voltage steps in Vmode %u [%d]. Clipping to 0.",
                                 vmode,
                                 cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps);

        /* Clip the positive increase to 0. */
        cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps = 0;
      }
    } 
    else if (starting_voltage_config == CEILING_STARTING_VOLTAGES)
    {
      // Setting initVoltOffsetSteps will cause the ceiling voltages to be used.
      cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps = 0;
    }
    else
    {
      // Setting initVoltOffsetSteps will cause the ceiling voltages to be used.
      cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps = 0;
    }

    /* If CPR is being run in disabled or open-loop this will be the only adjustment applied.
     * If CPR is running in closed loop this will be a starting point.
     */
    cprContext.measurements[vmode].currentVoltage =
              cprContext.targetCfg->vmodeCfg[vmode].voltageCeiling +
              (cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps * 10000);

    ULogFront_RealTimePrintf(cprContext.ulogHandle, 3,
                             "Calculated initial currentVoltage for Vmode %u = %u"
     			     "initVoltOffsetSteps = %d",
                             vmode, cprContext.measurements[vmode].currentVoltage,
			     cprContext.targetCfg->vmodeCfg[vmode].initVoltOffsetSteps);		  
    
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 2,
                             "Calculated initial currentVoltage for Vmode %u = %u",
                             vmode, cprContext.measurements[vmode].currentVoltage);
	  			  
    //clip positive voltages above the ceiling
    if (cprContext.measurements[vmode].currentVoltage >= cprContext.targetCfg->vmodeCfg[vmode].voltageCeiling)
    {
      cprContext.measurements[vmode].currentVoltage = cprContext.targetCfg->vmodeCfg[vmode].voltageCeiling;
    } 
    else //round to the nearest safe pmic step.
    {
      //round up to the nearest PMIC
      pmicSteps = cprContext.measurements[vmode].currentVoltage / cprContext.targetCfg->voltageStepSize;
  
      if ((pmicSteps * cprContext.targetCfg->voltageStepSize) < cprContext.measurements[vmode].currentVoltage)
      {
        pmicSteps += 1;  //add one more PMIC step to make sure the voltage is ABOVE the fused value not below it. 
      }
      cprContext.measurements[vmode].currentVoltage = pmicSteps * cprContext.targetCfg->voltageStepSize;
    
      if (cprContext.measurements[vmode].currentVoltage > cprContext.targetCfg->vmodeCfg[vmode].voltageCeiling)
      {
        cprContext.measurements[vmode].currentVoltage = cprContext.targetCfg->vmodeCfg[vmode].voltageCeiling;
      } 
   }		  
   ULogFront_RealTimePrintf(cprContext.ulogHandle, 3,
                             "Calculated final currentVoltage for Vmode %u = %u "
      			     "pmicSteps = %d",
                             vmode, cprContext.measurements[vmode].currentVoltage, pmicSteps);
  }

 /* If there are any final target specific changes needed to the initial
  * voltages they'll be made here.
  */
  CprGetTargetStartingVoltageAdjustments(&cprContext, starting_voltage_config);
}


/**
 * <!-- CprInitUlog -->
 *
 * @brief Create and initialize a "CprLog" ULog for CPR.
 */
inline static void CprInitUlog( void )
{
  const char logName[] = "CprLog";

  /* Create a Ulog handle */
  CORE_DAL_VERIFY( ULogFront_RealTimeInit(&cprContext.ulogHandle,
                                          logName,
                                          CPR_ULOG_DEFAULT_BUFFER_SIZE_BYTES,
                                          ULOG_MEMORY_LOCAL,
                                          ULOG_LOCK_OS));

  /* Header allows the visualization tool to apply the appropriate parser. */
  ULogCore_HeaderSet(cprContext.ulogHandle, "Content-Type: text/tagged-log-1.0; title=CPR running history");
}


/**
 * <!-- CprPopulateVoltageLimitValues -->
 *
 * @brief Query the clock driver for the voltage ranges for each voltage mode
 * and populate our internal table with that data.
 */
static void CprPopulateVoltageLimitValues( void )
{
  npa_query_handle          clkCpuQueryNpaHandle;
  npa_query_type            queryResult;
  CprVModeEnumType          vmode;
  ClockVRegCornerDataType * clkrgmVoltageTbl;

  CORE_VERIFY_PTR(clkCpuQueryNpaHandle = npa_create_query_handle("/vdd/mss"));

  CORE_VERIFY(NPA_QUERY_SUCCESS ==
              npa_query(clkCpuQueryNpaHandle,
                        CLOCK_NPA_QUERY_VOLTAGE_NUM_ENTRIES,
                        &queryResult));

  /* Number of levels in the clock driver must match our expectation. */
  CORE_VERIFY(queryResult.data.value == CPR_VMODE_COUNT);

  CORE_VERIFY(NPA_QUERY_SUCCESS == npa_query(clkCpuQueryNpaHandle,
                                             CLOCK_NPA_QUERY_VOLTAGE_TABLE,
                                             &queryResult));

  clkrgmVoltageTbl = (ClockVRegCornerDataType *)queryResult.data.value;

  CORE_VERIFY_PTR(clkrgmVoltageTbl);

  /* Populate our voltage levels from the clkrgm table */
  for (vmode=0; vmode<CPR_VMODE_COUNT; vmode++)
  {
    CprVmodeCfgType * vmodeCfg = &cprContext.targetCfg->vmodeCfg[vmode];

    vmodeCfg->voltageCeiling = clkrgmVoltageTbl[vmode].nUVMax;
    vmodeCfg->voltageFloor = clkrgmVoltageTbl[vmode].nUVMin;
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 
                             3,
                             "Vmode %u (Floor Voltage: %u) (Ceiling Voltage: %u)",
                             vmode,
                             vmodeCfg->voltageFloor,
                             vmodeCfg->voltageCeiling);
  }

  npa_destroy_query_handle(clkCpuQueryNpaHandle);
}


/**
 * <!-- CprPopulateVoltageLimitValuesBasedonOpenLoop -->
 *
 * @brief Populate the CPR ceiling values based on open loop
 * voltage rather than ceiling voltage maintained by clock driver.
 */
static void CprPopulateVoltageLimitValuesBasedonOpenLoop( void )
{
  CprVModeEnumType          vmode;
	
  /* Populate CPR ceiling voltage based on open loop voltage */
  for (vmode=0; vmode<CPR_VMODE_COUNT; vmode++)
  {
    CprVmodeCfgType * vmodeCfg = &cprContext.targetCfg->vmodeCfg[vmode];
    vmodeCfg->voltageCeiling = cprContext.measurements[vmode].currentVoltage;
	  
    ULogFront_RealTimePrintf(cprContext.ulogHandle, 
                           2,
                           "Vmode %u (Final Ceiling voltage: %u)",
                           vmode,
                           vmodeCfg->voltageCeiling);
   }
}

/**
 * <!-- CprInitContinueCallback -->
 *
 * @brief Continue initialization after our dependencies have become available.
 * 
 * @param context : Unused
 * @param event_type : Unused
 * @param data : Unused
 * @param data_size : Unused
 */
static void CprInitContinueCallback( void         * context,
                                     unsigned int   event_type,
                                     void         * data,
                                     unsigned int   data_size )
{
  /* Get the chip specific HAL, appropriate CPR target quotients and read efuses.*/
  CprGetTargetConfiguration(&cprContext);

  /* Resize the "CprLog" ULog buffer to a larger log size if CPR is active. */
  if ((cprContext.enableFlags_ClosedLoopEnabled) && !(cprContext.enableFlags_DisabledByFuse))
  {
    if (cprContext.targetCfg->ulogBufSize > CPR_ULOG_DEFAULT_BUFFER_SIZE_BYTES)
    {
      CORE_DAL_VERIFY(ULogCore_Allocate(cprContext.ulogHandle, cprContext.targetCfg->ulogBufSize));
    }
  }

  /* Log the settings used */
  ULogFront_RealTimePrintf(cprContext.ulogHandle, 2,
                      "MSS CPR Driver Version %d using %s",
                      CPR_DRIVER_VERSION, cprContext.halTargetCfg->staticCfg.configVersionText);

  ULogFront_RealTimePrintf(cprContext.ulogHandle, 4,
                      "CPR  (OpenLoopEnabled: %d) (ClosedLoopEnabled: %d) (DisabledByHW: %d) (PmicVersion: %d)",
                      cprContext.enableFlags_OpenLoopEnabled, cprContext.enableFlags_ClosedLoopEnabled, cprContext.enableFlags_DisabledByFuse, cprContext.detectedPMIC);

  /* Fill in the Ceiling and Floor values of cprContext.targetCfg->vmodeCfg[] 
     entries based on clock driver voltage ranges.*/
  CprPopulateVoltageLimitValues();

  /* Register the Sleep "mss_rbcpr" LPR */
  CprInitLowPowerMgr();

  /* Calculate the cprContext.measurements[vmode].currentVoltage based on 
     fuse, target configuration and clock driver voltage information. 
     If the system is running closed-loop CPR these will be a starting points. 
   */
  CprInitStateData();

  CprPopulateVoltageLimitValuesBasedonOpenLoop();

  if (!(cprContext.enableFlags_ClosedLoopEnabled) || (cprContext.enableFlags_DisabledByFuse))
  {
    /* Register the disabled mode functions with DalClock_RegisterCPRCallbacks */
    CprInitDisabledMode();
  }
  else
  {
    /* Setup the mutex, clocks, interrupts, register the enabled mode functions
       with DalClock_RegisterCPRCallbacks, and configure the CPR hardware registers*/
    CprInitEnabledMode();
  }
}


/**
 * <!-- CprWaitForDependencies -->
 *
 * @brief Ask NPA to call CprInitContinueCB once clocks and sleep external
 * dependencies become available
 */
inline static void CprWaitForDependencies( void )
{
  static const char *dependencies[] =
  {
    "/clk/cpu",
    "/clk/mss/config_bus",
    "/vdd/mss",
    SLEEP_LPR_NODE_NAME
  };

  // Wait for clkrgm so that it can be queried for the voltage ranges of each
  // voltage mode. Also wait for sleep so that the CPR LPRM can be registered.
  npa_resources_available_cb(NPA_ARRAY(dependencies), CprInitContinueCallback, NULL);
}


/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
/**
 * <!-- CprInit -->
 *
 * @brief Initialize the CPR driver including the "CprLog" ULog.
 *
 * If CPR is disallowed by the efuse settings, the driver will continue to
 * correct clkrgm's default voltage with the offset values blown into the efuses.
 */
 #define RBCPR_REF_CLOCK    "clk_bus_rbcpr_ref" 
 static DalDeviceHandle  *pRBCPRClkHandle;
 static ClockIdType      nClockID_rbcpr;
 static DALResult        setClkDividerResult = DAL_SUCCESS;
 
 
void CprInit( void )
{
  static boolean initDone = FALSE;

  if (initDone)
  {
    return;
  }
    
  /* There is no separate clock for MSS CPR. It is derived from MSS config bus clock.
   * HW team has recommended that CPR clock be set by applying Div4 to the
   * MSS config bus clock
   */
    
  if (DAL_SUCCESS == DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &pRBCPRClkHandle))
  {
    if (DAL_SUCCESS == DalClock_GetClockId (pRBCPRClkHandle, RBCPR_REF_CLOCK, &nClockID_rbcpr))
    {
      setClkDividerResult = DalClock_SetClockDivider (pRBCPRClkHandle, nClockID_rbcpr, 4);  
    }
  }
  
  initDone = TRUE;

  HalCprClearRBCPRBitInMssClampIO();

  CprInitUlog();

 /* Most initialization occurs in the CprInitContinueCallback which is setup here to 
    be called by NPA when the needed clock and sleep resources are available */
  CprWaitForDependencies();
}

