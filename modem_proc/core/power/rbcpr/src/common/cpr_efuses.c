/*============================================================================
@file cpr_efuses.c

Gathers config data from Efuses.

Copyright � 2013-2014 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/src/common/cpr_efuses.c#1 $
============================================================================*/
#include "HALhwio.h"
#include "HALcpr_hwio.h"
#include "cpr_internal.h"
#include "cpr_efuse.h"
#include "CoreVerify.h"

extern CprContextType cprContext;

/**
 * <!-- CprEfuseGetDataFromFuses -->
 *
 * @brief Reads CPR configuration data blown into efuses.
 * 
 * @param fuseDb : Pointer to the database of efuse registers and fields from
 * which CPR data can be read.
 * @param vmodeCfg : Pointer to an array of structs in which the efused data
 * will be stored. The array size must match CPR_VMODE_COUNT.
 * 
 * @return TRUE if efuses have indicated that CPR should be disabled; 
 * FALSE otherwise.
 */
static boolean CprEfuseGetDataFromFuses( CprEfuseDatabaseType * fuseDb,
                                         CprVmodeCfgType      * vmodeCfg )
{
  int vmode;

  // Get efuse data for each voltage mode.
  for (vmode=0; vmode<CPR_VMODE_COUNT; vmode++)
  {
    uint32 targetVsteps0, targetVsteps1;
    int8 targetVsteps, initVoltOffsetSteps;
    CprEfuseVModeCfgType * efuseVmode = &fuseDb->vmodeCfg[vmode];

    targetVsteps0 = CPR_EFUSE_IN(efuseVmode->targetVsteps0);
    targetVsteps1 = CPR_EFUSE_IN(efuseVmode->targetVsteps1);
    initVoltOffsetSteps = (int8) ((targetVsteps1 << efuseVmode->shiftLeftBy)
                                	|  targetVsteps0);

    ULogFront_RealTimePrintf(cprContext.ulogHandle,
                           4,
                           "CprEfuseGetDataFromFuses: (Vmode: %u) (targetVsteps0: %u)"
                           "(targetVsteps1: %u) (initVoltOffsetSteps: %d)",
                           vmode,
                           targetVsteps0,
                           targetVsteps1,
                           initVoltOffsetSteps);									

    // Treat as negative number if MSB is set in the 5-bit field and make it
    // a valid signed int8 value.
    if (initVoltOffsetSteps & 0x10)
    {
      initVoltOffsetSteps = -(initVoltOffsetSteps & 0xf);
    }
    vmodeCfg[vmode].initVoltOffsetSteps = initVoltOffsetSteps;
	
    ULogFront_RealTimePrintf(cprContext.ulogHandle,
                           1,
                           "(Final voltage steps: %d)",
                           vmodeCfg[vmode].initVoltOffsetSteps);
  }

  return FALSE;
}


/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
/**
 * <!-- CprEfuseGetData -->
 *
 * @brief Reads CPR configuration data blown into efuses correcting for the
 * redundancy select bit.
 * 
 * @param fuseDb : Pointer to the database of efuse registers and fields from
 * which CPR data can be read.
 * @param vmodeCfg : Pointer to an array of structs in which the efused data
 * will be stored. The array size must match CPR_VMODE_COUNT.
 * 
 * @return TRUE if efuses have indicated that CPR should be disabled; 
 * FALSE otherwise.
 */
boolean CprEfuseGetData( CprEfuseMasterDatabaseType * fuseDb,
                         CprVmodeCfgType            * vmodeCfg )
{
  if (CPR_EFUSE_IN(fuseDb->redundancySel) == 0x1)
  {
    return CprEfuseGetDataFromFuses(&fuseDb->redundantDb, vmodeCfg);
  }
  else
  {
    return CprEfuseGetDataFromFuses(&fuseDb->primaryDb, vmodeCfg);
  }
}

