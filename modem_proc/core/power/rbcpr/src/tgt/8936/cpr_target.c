/*============================================================================
@file cpr_target.c

Target-specific CPR configuration data for 9x35.

Copyright � 2013-2015 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/src/tgt/8936/cpr_target.c#4 $
=============================================================================*/
#include "HALhwio.h"
#include "HAL_cpr.h"
#include "HALcpr_hwio.h"
#include "cpr_efuse.h"
#include "cpr_internal.h"
#include "CoreVerify.h"
#include "pm_version.h"
#include "DDIChipInfo.h"

/* Distinguish among different chip versions*/
DalChipInfoVersionType  chip_ver;

/** Database of fuse locations and fields we want to read from. */
static CprEfuseMasterDatabaseType cprEfuseMasterDb =
{
  .redundancySel = { HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0xe0000000, 29 },
  .primaryDb = 
  {
    .vmodeCfg =
    {
      // CPR_VMODE_RETENTION - not used by MSS rail (and hence not used by CPR). Only added to align with clkrgm enums
      [CPR_VMODE_RETENTION] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29},
        2,
      },
      [CPR_VMODE_LOW_MINUS] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29},
        2,
      },
      [CPR_VMODE_LOW] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29},
        2,
      },
      [CPR_VMODE_NOMINAL] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x70000,  16},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x300000, 20},
        3,
      },
      [CPR_VMODE_TURBO] =
      {
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
        0,
      },
      [CPR_VMODE_SUPER_TURBO] =
      {
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
        0,        
      }
    }
  },
  .redundantDb =
  {
    .vmodeCfg =
    {
      // CPR_VMODE_RETENTION - not used by MSS rail (and hence not used by CPR). Only added to align with clkrgm enums
      [CPR_VMODE_RETENTION] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29},
	2,
      },
      [CPR_VMODE_LOW_MINUS] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25}, //Lower bits
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29}, //Upper bits
	2,
      },
      [CPR_VMODE_LOW] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, 0x6000000,  25},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xe0000000, 29},
        2,        
      },
      [CPR_VMODE_NOMINAL] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x70000,  16}, //Lower bits
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x300000, 20}, //Upper bits
	3,
      },
      [CPR_VMODE_TURBO] =
      {
        .targetVsteps0 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xf800, 11},  //Lower bits
        .targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xf800, 11},  //Upper bits	
	0,
      },
      [CPR_VMODE_SUPER_TURBO] =
      {
        .targetVsteps0 = {HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
	.targetVsteps1 = {HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0xf800, 11},
        0,        
      }
    }
  }
};


/** Target specific config. */
static CprTargetCfgType cprTargetCfg =
{
  .irq                     = 93,       // From MSS HPG document
  .delayBeforeNextMeas     = 5,        // millisecs
  .ulogBufSize             = 65536,    // 64K Bytes (Switch to 65536 if more debug info needed)
  .voltageStepSize         = 12500,    // microVolts
};

/**********TSMC static margins for Shere 3.0*********/
/*  TURBO   - 37.5                                  */
/*  NOMINAL - 25.0                                  */
/*  SVS     - 25.0 + 25 = 50 mV                                   */
/****************************************************/

/** Hardware version specific config needed by the HAL. */
static HalCprTargetConfigType halCprTargetCfgTSMC_3_0 =
{
  .staticCfg =
  {
    .configVersionText = "8939 Initial Values 2014",

    .sensorMask[0] = 0x18,
    .sensorMask[1] = 0,

    .speedpushed_ROs = 0xC,     //mask R2 and R3 in their GCNT values due to speed pushed sensors.

    .sensorBypass[0] = 0,
    .sensorBypass[1] = 0,

    .CTL__DN_THRESHOLD = 3,
    .CTL__UP_THRESHOLD = 2,

    .SW_VLEVEL__SW_VLEVEL = 0x20,

    .TIMER_ADJUST__CONSECUTIVE_DN = 0x2,

    .sensorsOnCpuBlock[0] = 0x3000,
    .sensorsOnCpuBlock[1] = 0,
  },
  .vmodeCfg =
  {
    [CPR_VMODE_RETENTION] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 335,
      .oscCfg[1].target      = 347,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 289,
      .oscCfg[5].target      = 311,
      .oscCfg[6].target      = 461,
      .oscCfg[7].target      = 479,
    },
    [CPR_VMODE_LOW_MINUS] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 335,
      .oscCfg[1].target      = 347,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 289,
      .oscCfg[5].target      = 311,
      .oscCfg[6].target      = 461,
      .oscCfg[7].target      = 479,
    },
    [CPR_VMODE_LOW] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 335,
      .oscCfg[1].target      = 347,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 289,
      .oscCfg[5].target      = 311,
      .oscCfg[6].target      = 461,
      .oscCfg[7].target      = 479,
    },
    [CPR_VMODE_NOMINAL] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 496,
      .oscCfg[1].target      = 506,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 437,
      .oscCfg[5].target      = 458,
      .oscCfg[6].target      = 630,
      .oscCfg[7].target      = 640,
    },
#if 0
    [CPR_VMODE_TURBO_MINUS] =
#endif
    [CPR_VMODE_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 621,
      .oscCfg[1].target      = 622,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 539,
      .oscCfg[5].target      = 554,
      .oscCfg[6].target      = 757,
      .oscCfg[7].target      = 750,
    },
    [CPR_VMODE_SUPER_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 621,
      .oscCfg[1].target      = 622,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 539,
      .oscCfg[5].target      = 554,
      .oscCfg[6].target      = 757,
      .oscCfg[7].target      = 750,
    },
  }
};

/************ TSMC static margins************/
/*  TURBO   - 37.5                          */
/*  NOMINAL - 50.0                          */
/*  SVS     - 50.0                          */
/********************************************/

/** Hardware version specific config needed by the HAL. */
static HalCprTargetConfigType halCprTargetCfgTSMC =
{
  .staticCfg =
  {
    .configVersionText = "8939 Initial Values 2014",

    .sensorMask[0] = 0x18,
    .sensorMask[1] = 0,

    .speedpushed_ROs = 0xC,     //mask R2 and R3 in their GCNT values due to speed pushed sensors.

    .sensorBypass[0] = 0,
    .sensorBypass[1] = 0,

    .CTL__DN_THRESHOLD = 3,
    .CTL__UP_THRESHOLD = 2,

    .SW_VLEVEL__SW_VLEVEL = 0x20,

    .TIMER_ADJUST__CONSECUTIVE_DN = 0x2,

    .sensorsOnCpuBlock[0] = 0x3000,
    .sensorsOnCpuBlock[1] = 0,
  },
  .vmodeCfg =
  {
    [CPR_VMODE_RETENTION] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 364,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 329,
      .oscCfg[5].target      = 343,
      .oscCfg[6].target      = 549,
      .oscCfg[7].target      = 521,
    },
    [CPR_VMODE_LOW_MINUS] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 364,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 329,
      .oscCfg[5].target      = 343,
      .oscCfg[6].target      = 549,
      .oscCfg[7].target      = 521,
    },
    [CPR_VMODE_LOW] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 364,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 329,
      .oscCfg[5].target      = 343,
      .oscCfg[6].target      = 549,
      .oscCfg[7].target      = 521,
    },
    [CPR_VMODE_NOMINAL] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 592,
      .oscCfg[1].target      = 587,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 541,
      .oscCfg[5].target      = 547,
      .oscCfg[6].target      = 801,
      .oscCfg[7].target      = 757,
    },
#if 0
    [CPR_VMODE_TURBO_MINUS] =
#endif
    [CPR_VMODE_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 678,
      .oscCfg[1].target      = 667,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 622,
      .oscCfg[5].target      = 622,
      .oscCfg[6].target      = 891,
      .oscCfg[7].target      = 841,
    },
    [CPR_VMODE_SUPER_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 678,
      .oscCfg[1].target      = 667,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 622,
      .oscCfg[5].target      = 622,
      .oscCfg[6].target      = 891,
      .oscCfg[7].target      = 841,
    },
  }
};


/**********GF static margins for Shere 3.0 ***********/
/*  TURBO   - 50.0                                   */
/*  NOMINAL - 75.0                                   */
/*  SVS     - 62.5                                   */
/****************************************************/

static HalCprTargetConfigType halCprTargetCfgGF_3_0 =
{
  .staticCfg =
  {
    .configVersionText = "8939 Initial Values 2014",

    .sensorMask[0] = 0x18,
    .sensorMask[1] = 0,

    .speedpushed_ROs = 0xC,     //mask R2 and R3 in their GCNT values due to speed pushed sensors.

    .sensorBypass[0] = 0,
    .sensorBypass[1] = 0,

    .CTL__DN_THRESHOLD = 3,
    .CTL__UP_THRESHOLD = 2,

    .SW_VLEVEL__SW_VLEVEL = 0x20,

    .TIMER_ADJUST__CONSECUTIVE_DN = 0x2,

    .sensorsOnCpuBlock[0] = 0x3000,
    .sensorsOnCpuBlock[1] = 0,
  },
  .vmodeCfg =
  {
    [CPR_VMODE_RETENTION] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_LOW_MINUS] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_LOW] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_NOMINAL] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 515,
      .oscCfg[1].target      = 562,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 522,
      .oscCfg[5].target      = 475,
      .oscCfg[6].target      = 723,
      .oscCfg[7].target      = 670,
    },
#if 0
    [CPR_VMODE_TURBO_MINUS] =
#endif
    [CPR_VMODE_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 568,
      .oscCfg[1].target      = 614,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 568,
      .oscCfg[5].target      = 525,
      .oscCfg[6].target      = 777,
      .oscCfg[7].target      = 725,
    },
    [CPR_VMODE_SUPER_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 568,
      .oscCfg[1].target      = 614,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 568,
      .oscCfg[5].target      = 525,
      .oscCfg[6].target      = 777,
      .oscCfg[7].target      = 725,
    },
  }
};

/************ GF static margins ***********/
/*  TURBO   - 50.0                        */
/*  NOMINAL - 75.0                        */
/*  SVS     - 62.5                        */
/******************************************/

static HalCprTargetConfigType halCprTargetCfgGF =
{
  .staticCfg =
  {
    .configVersionText = "8939 Initial Values 2014",

    .sensorMask[0] = 0x18,
    .sensorMask[1] = 0,

    .speedpushed_ROs = 0xC,     //mask R2 and R3 in their GCNT values due to speed pushed sensors.

    .sensorBypass[0] = 0,
    .sensorBypass[1] = 0,

    .CTL__DN_THRESHOLD = 3,
    .CTL__UP_THRESHOLD = 2,

    .SW_VLEVEL__SW_VLEVEL = 0x20,

    .TIMER_ADJUST__CONSECUTIVE_DN = 0x2,

    .sensorsOnCpuBlock[0] = 0x3000,
    .sensorsOnCpuBlock[1] = 0,
  },
  .vmodeCfg =
  {
    [CPR_VMODE_RETENTION] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_LOW_MINUS] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_LOW] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 324,
      .oscCfg[1].target      = 370,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 346,
      .oscCfg[5].target      = 301,
      .oscCfg[6].target      = 513,
      .oscCfg[7].target      = 467,
    },
    [CPR_VMODE_NOMINAL] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 515,
      .oscCfg[1].target      = 562,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 522,
      .oscCfg[5].target      = 475,
      .oscCfg[6].target      = 723,
      .oscCfg[7].target      = 670,
    },
#if 0
    [CPR_VMODE_TURBO_MINUS] =
#endif
    [CPR_VMODE_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 568,
      .oscCfg[1].target      = 614,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 568,
      .oscCfg[5].target      = 525,
      .oscCfg[6].target      = 777,
      .oscCfg[7].target      = 725,
    },
    [CPR_VMODE_SUPER_TURBO] =
    {
      .stepQuotient          = 26,
      .idleClocks            = 15,
      .dn_threshold          = 3,
      .up_threshold          = 2,
      .oscCfg[0].target      = 568,
      .oscCfg[1].target      = 614,
      .oscCfg[2].target      = 0,
      .oscCfg[3].target      = 0,
      .oscCfg[4].target      = 568,
      .oscCfg[5].target      = 525,
      .oscCfg[6].target      = 777,
      .oscCfg[7].target      = 725,
    },
  }
};
/*----------------------------------------------------------------------------
 * Public interfaces
 * -------------------------------------------------------------------------*/
 /**
 * <!-- CprGetTargetStartingVoltageAdjustments -->
 *
 * @brief Some targets (like 8974) require special checking and initial voltage adjustments.
 *
 * The cprContext.measurements[vmode].currentVoltage will be adjusted as needed.
 *
 * @param cprContext : Contains the initial calculated voltages as well as other
 * needed pointers and data. 
 * @param starting_voltage_config : Indicates if the system starting with open loop or ceiling voltages.
 */
void CprGetTargetStartingVoltageAdjustments( CprContextType *cprContext, char starting_voltage_config )
{
   // Nothing special needed here for this target.
}


/**
 * <!-- CprGetTargetConfiguration -->
 *
 * @brief Get target-specific configuration info, including reading efuses etc.
 *
 * @param cprContext: cprContext->targetCfg and cprContext->halTargetCfg will
 * will be set to non-NULL pointers.
 */
void CprGetTargetConfiguration( CprContextType *cprContext )
{
  uint32                pmic_version;
  
  
  cprContext->partValue = HalCPRGetPartInfo ();
  cprContext->PartInfo = (cprContext->partValue &0x700) >> 8;
  
  // Use the SS timing starting voltages. 
  cprContext->enableFlags_TTtiming = FALSE;

  // Enable reading the fuses to get the open loop voltages (Temporarily disabled pending further validation)
  cprContext->enableFlags_OpenLoopEnabled = FALSE;

  // Some items in cprTargetCfg should be read in from efuses. (per chip CPR enable, initVoltOffsetSteps) 
  cprContext->enableFlags_DisabledByFuse = CprEfuseGetData(&cprEfuseMasterDb, &cprTargetCfg.vmodeCfg[0]);

  // Disable CPR pending validation and enable decisions.
  cprContext->enableFlags_ClosedLoopEnabled = TRUE;


  cprContext->targetCfg = &cprTargetCfg;
  pmic_version = pm_get_pmic_model(0);       //get the (0 primary) pmic info	
  cprContext->detectedPMIC = pmic_version;   //store the PMIC for debug and logging later 
  chip_ver = DalChipInfo_ChipVersion();
  if (chip_ver == DALCHIPINFO_VERSION(3,0))
  {
    // 8939 3.0
    if (1 == cprContext->PartInfo)
    {
      cprContext->halTargetCfg = &halCprTargetCfgGF_3_0;
    }
    else
    {
      cprContext->halTargetCfg = &halCprTargetCfgTSMC_3_0;
    }
  }
  else
  {
    if (1 == cprContext->PartInfo)
    {
      cprContext->halTargetCfg = &halCprTargetCfgGF;
    }
    else
    {
      cprContext->halTargetCfg = &halCprTargetCfgTSMC;
    }
  }
}

