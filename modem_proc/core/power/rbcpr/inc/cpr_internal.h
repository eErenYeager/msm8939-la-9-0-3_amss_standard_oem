#ifndef CPR_INTERNAL_H
#define CPR_INTERNAL_H
/*===========================================================================
cpr_internal.h

Internal data types. Should NOT be included by the HAL layer.

Copyright � 2013-2014 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/inc/cpr_internal.h#1 $

=============================================================================*/
#include "HAL_cpr.h"
#include "npa.h"
#include "ULogFront.h"
#include "ULog.h"
#include "rex.h"

/** Increment this when driver functionality changes. It's logged to help
    assist debug only.  */
                           //2 - older versions where the version isn't logged.  
                           //3 - added version logging.  Speedpush masking impovements. 
                           //4 - driver now allows custom up/down thresholds in each mode
#define CPR_DRIVER_VERSION 4 

/** Default Ulog buffer size can be overridden with the target config */
#define CPR_ULOG_DEFAULT_BUFFER_SIZE_BYTES 4096

// Must be used in pairs - will allow compiler to catch incomplete pairs.
#define CPR_MUTEX_ACQUIRE() { DALSYS_SyncEnter(cprContext.mutexHandle);
#define CPR_MUTEX_RELEASE()   DALSYS_SyncLeave(cprContext.mutexHandle); }

/* Definitions for the starting voltages truth table. */
#define CEILING_STARTING_VOLTAGES       1
#define OPEN_LOOP_STARTING_VOLTAGES     2
#define CLOSED_LOOP_STARTING_VOLTAGES   2 /* Closed loop start at open loop voltages */

/**
 * Dynamic CPR measurement data per voltage mode
 */
typedef struct
{
  uint32   currentVoltage; // microVolts

} CprMeasurementInfoType;


/**
 * Configuration info for a given voltage mode.
 */
typedef struct
{
  uint32   voltageCeiling;
  uint32   voltageFloor;

  // Number of PMIC Vdd step sizes by which the default operating voltage
  // should be corrected
  int8     initVoltOffsetSteps;

} CprVmodeCfgType;


/**
 * Target specific configuration info (things that are not needed/usable
 * directly at the HAL layer).
 */
typedef struct
{
  // CPR IRQ
  uint32           irq;

  // Milliseconds by which we want to delay the next measurement
  uint32           delayBeforeNextMeas;

  // Ulog buffer size
  uint32           ulogBufSize;

  // PMIC voltage step size in microVolts
  uint32           voltageStepSize;

  // Characterization data for each voltage mode. (ceiling, floor, intial offset...)
  CprVmodeCfgType  vmodeCfg[CPR_VMODE_COUNT];

} CprTargetCfgType;


/**
 * Container for ALL the CPR driver data.
 */
typedef struct
{
  //
  // There are 4 main "enableFlags" to configure the driver.
  //

  // Turns on all the closed loop ISR features.
  boolean                    enableFlags_ClosedLoopEnabled;

  // Starting voltages for each mode will either be set by the open loop fuses
  // or to the ceiling voltages.
  boolean                    enableFlags_OpenLoopEnabled;

  // Set to TRUE if eFuse has been blown to disable the use of CPR.
  boolean                    enableFlags_DisabledByFuse;

  // Set to TRUE if the part has TT timing.  This makes the starting
  // voltages more restrictive than SS timing. ie the starting 
  // voltages are always open loop, never ceiling
  boolean                    enableFlags_TTtiming;



  //set to TRUE if the down interrupt in the current mode has been disabled. 
  boolean downIntEnabled;

  //set to TRUE if the up interrupt in the current mode has been disabled. 
  boolean upIntEnabled;


  // Set to TRUE if clkrgm is in the process of switching voltage modes.
  boolean                    switchingVmode;

  // Set to TRUE at init.  Cleared after the first time the PostSwitch is called to enable CPR.
  boolean                    clearedAfterFirstUse;

  // Voltage mode we're currently at
  CprVModeEnumType           currentVmode;

  // Mode specific measurements based on CPR HW. (current voltage)
  CprMeasurementInfoType     measurements[CPR_VMODE_COUNT];

  // CPR version specific HW config that is independent of which target we're on.
  HalCprTargetConfigType   * halTargetCfg;

  // Target specific data that is independent of the CPR HW version.
  CprTargetCfgType         * targetCfg;

  // Mutex handle
  DALSYSSyncHandle           mutexHandle;

  // AHB bus clock handle - this clock is used for register access from the Q6
  DalDeviceHandle          * busClkHandle;

  // Reference clock used by the CPR HW - all measurements and config are
  // based on this clock.
  DalDeviceHandle          * refClkHandle;

  // Ulog handle
  ULogHandle                 ulogHandle;

  //detected PMIC for selecting the right quotients and step size on some targets.
  uint32                     detectedPMIC;
  
  // NPA Event handle for receiving notification when the reference clock
  // frequency changes.
  npa_event_handle           refClkChgNpaHandle;

  // Current reference clock rate in kHz
  uint32                     currentRefClkRate;

  // LDO is in use for QDSP6 if TRUE - we need to disable the sensor on the
  // QDSP6 if TRUE;
  boolean                    cpuNotUsingMssRail;

  // Handler for low power modes
  void (*lpFunc)(boolean enteringLowPowerMode);

  // Mutex object
  DALSYSSyncObj syncObj;

  uint32                partValue;
  uint32                PartInfo; 

} CprContextType;


/**
 * <!-- CprGetTargetConfiguration -->
 * 
 * @brief Get target-specific configuration info, including reading efuses etc.
 *
 * This is NOT the place to do any HW initialization.
 * 
 * @param cprContext: cprContext->targetCfg and cprContext->halTargetCfg will
 * will be set to non-NULL pointers.
 */
void CprGetTargetConfiguration( CprContextType *cprContext );


 /**
 * <!-- CprGetTargetStartingVoltageAdjustments -->
 *
 * @brief Some targets (like 8974) require special checking and initial voltage adjustments.
 *
 * The cprContext.measurements[vmode].currentVoltage will be adjusted as needed.
 *
 * @param cprContext : Contains the initial calculated voltages as well as other
 * needed pointers and data. 
 * @param starting_voltage_config : Indicates if the system starting with open loop or ceiling voltages.
 */
void CprGetTargetStartingVoltageAdjustments( CprContextType *cprContext, char starting_voltage_config );


/**
 * <!-- CprInitEnabledMode -->
 *
 * @brief Initialize the driver so that CPR HW can be used to manager the voltages.
 */
void CprInitEnabledMode( void );


/**
 * <-- CprInitDisabledMode -->
 *
 * @brief Setup the driver to handle voltage requirements in disabled mode.
 *
 * Initialize the driver so that CPR HW is not used. The driver will still
 * continue to correct clkrgm's default voltage with the offset
 * values blown into the efuses.
 */
void CprInitDisabledMode( void );


/**
 * <!-- CprInitLowPowerMgr -->
 * 
 * @brief Register an LPRM with Sleep so that we can disable CPR during 
 * power collapse. 
 */
void  CprInitLowPowerMgr( void );

/**
 * <!-- CprDeRegisterRBCPRInterrupt -->
 *
 * @brief De-register the RBCPR interrupt when Q6 goes to power collapse, so that no
 *        pending CPR interrupt wakes up Q6 from power collapse.
 */
void CprDeRegisterRBCPRInterrupt (void);


/**
 * <!-- CprRegisterRBCPRInterrupt -->
 *
 * @brief Register the RBCPR interrupt when Q6 comes out of power collapse.
 */
void CprRegisterRBCPRInterrupt (void);

#endif // CPR_INTERNAL_H

