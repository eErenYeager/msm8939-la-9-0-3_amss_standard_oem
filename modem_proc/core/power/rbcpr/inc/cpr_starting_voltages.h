/*============================================================================
@file cpr_starting_voltages.c

Implements the starting voltage truth table as defined Sept 2013

Copyright � 2013 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Confidential and Proprietary.

$Header: //components/rel/core.mpss/3.7.24/power/rbcpr/inc/cpr_starting_voltages.h#1 $
============================================================================*/


/*
The starting voltages have a large truth table. 

TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable    Use
0                    1                       n/a                     n/a	                 SS ceiling voltage
0                    1                       n/a                     n/a                   SS ceiling voltage
0                    0                       0	                     0	                   SS ceiling voltage
0                    0                       1                       0                     open loop
0                    0                       0                       1                     close loop
0                    0                       1                       1	                   close loop
1                    1                       n/a                     n/a                   open loop
1                    1                       n/a                     n/a	                 open loop
1                    0                       0                       0                     open loop
1                    0                       1                       0                     open loop
1                    0                       0                       1                     close loop
1                    0                       1                       1                     close loop
*/


/* 
  The index starting_voltage_truth_table is a combination of bits for:
  TT part / disabled by fuse / open loop enabled / closed loop enabled
*/

static char starting_voltage_truth_table[16] = 
{
  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    0                    0                       0	                     0	                   SS ceiling voltage 
   */
  /* 0 */
  CEILING_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    0                    0                       0                       1                     close loop 
   */
  /* 1 */
  CLOSED_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    0                    0                       1                       0                     open loop    
   */
  /* 2 */
  OPEN_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    0                    0                       1                       1	                   close loop    
   */
  /* 3 */
  CLOSED_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    0                    1                       n/a                     n/a                   SS ceiling voltage    
   */
  /* 4 */
  CEILING_STARTING_VOLTAGES,
  /* 5 */
  CEILING_STARTING_VOLTAGES,
  /* 6 */
  CEILING_STARTING_VOLTAGES,
  /* 7 */
  CEILING_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    1                    0                       0                       0                     open loop    
   */
  /* 8 */
  OPEN_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    1                    0                       0                       1                     close loop    
   */
  /* 9 */
  CLOSED_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    1                    0                       1                       0                     open loop    
   */
  /* 10 */
  OPEN_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    1                    0                       1                       1                     close loop    
   */
  /* 11 */
  CLOSED_LOOP_STARTING_VOLTAGES,


  /*
    TT timing closure   CPR disable fuse      SW open loop enable      SW closed loop enable   Use
    1                    1                       n/a                     n/a                   open loop    
   */
  /* 12 */
  OPEN_LOOP_STARTING_VOLTAGES,
  /* 13 */
  OPEN_LOOP_STARTING_VOLTAGES,
  /* 14 */
  OPEN_LOOP_STARTING_VOLTAGES,
  /* 15 */
  OPEN_LOOP_STARTING_VOLTAGES,
};




