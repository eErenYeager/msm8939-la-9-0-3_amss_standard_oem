/*===========================================================================
  FILE:         mpmint.c
  
  OVERVIEW:     This file contains the necessary functions to interface
                the MPM IC hardware registers.

                Copyright (c) 2005 - 2013 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

===========================================================================*/
/*=============================================================================
$Header: //components/rel/core.mpss/3.7.24/power/mpm/src/mpmint.c#1 $
$DateTime: 2015/01/27 06:04:57 $ 
$Author: mplp4svc $
=============================================================================*/


#include "mpmint.h"
#include "mpminti.h"
#include "HALmpmint.h"
#include "CoreVerify.h"
#include "CoreIntrinsics.h"

/*============================================================================
 *                      INTERNAL FUNCTION DECLARATIONS
 *===========================================================================*/

/**
 * @brief Converts detection type and polarity to equivalent HAL trigger type.
 *
 * @param detect: Detection type for the interrupt or gpio (edge/level).
 * @param polarity: Polarity of the interrupt or gpio (positive/negative).
 *
 * @return HAL trigger type equivalent for input detection and polarity values.
 */
static HAL_mpmint_TriggerType mpmint_trans_int_type
(
  mpmint_detect_type   detect, 
  mpmint_polarity_type polarity
)
{
  HAL_mpmint_TriggerType hal_trigger = HAL_MPMINT_TRIGGER_INVALID;

  if ( ( detect   == MPMINT_LEVEL_DETECT ) && 
       ( polarity == MPMINT_ACTIVE_LOW ) )
  {
    hal_trigger = HAL_MPMINT_TRIGGER_LOW;
  }
  else if ( ( detect   == MPMINT_LEVEL_DETECT ) && 
            ( polarity == MPMINT_ACTIVE_HIGH ) ) 
  {
    hal_trigger = HAL_MPMINT_TRIGGER_HIGH;
  }
  else if ( ( detect   == MPMINT_EDGE_DETECT ) && 
            ( polarity == MPMINT_ACTIVE_LOW ) ) 
  {
    hal_trigger = HAL_MPMINT_TRIGGER_FALLING;
  }
  else if ( ( detect   == MPMINT_EDGE_DETECT ) && 
            ( polarity == MPMINT_ACTIVE_HIGH ) ) 
  {
    hal_trigger = HAL_MPMINT_TRIGGER_RISING;
  }
  else if ( ( detect   == MPMINT_EDGE_DETECT ) && 
            ( polarity == MPMINT_ACTIVE_DUAL ) )
  {
    hal_trigger = HAL_MPMINT_TRIGGER_DUAL;
  }
  else 
  {
    mpmint_log_printf(2, "Invalid detect and/or polarity: %d %d", 
                      (int)detect, (int)polarity);
    CORE_VERIFY(0);
  }

  return hal_trigger;

}

/**
 * @brief Determines if the input mpmint_isr_type value corresponds to a
 *        valid wakeup interrupt line for a this target.
 *
 * It is a valid wake up if the table entry at that index is mapped to MPM 
 * and it is of interrupt type.
 *
 * @param in_id        Enum value to check if it is valid wakeup interrupt line 
 *                     on mpm.
 * @param[out] out_id  Index in to mpm mapping table that corresponds to input
 *                     if input is valid wakeup interrupt (returns TRUE) else 
 *                     untouched (returns FALSE)
 *
 * @return TRUE if the input value is a valid wakeup interrupt else FALSE.
 */
static bool32 mpmint_valid_wakeup_interrupt
(
  mpmint_isr_type in_id,
  uint32 *out_id
)
{
  bool32 ret_val = FALSE;
  uint32 idx;
  uint16 irq;

  if( ( TRUE == mpmint_get_isr_tbl_index(in_id, &idx) ) &&
      ( MPMINT_NULL_IRQ != mpmint_isr_tbl[idx].local_irq ) &&
      ( TRUE == HAL_mpmint_IsWakeupIRQ(mpmint_isr_tbl[idx].hal_irq, &irq) ) )
  {
    CORE_VERIFY_PTR(out_id);
    *out_id = idx;
    ret_val = TRUE;
  }

  return ret_val;
}

/*============================================================================
 *                      EXTERNAL FUNCTION DECLARATIONS
 *===========================================================================*/

/*
 * mpmint_get_isr_tbl_index
 */
bool32 mpmint_get_isr_tbl_index
( 
  mpmint_isr_type in_id, 
  uint32 *out_id 
)
{
  boolean ret_val = FALSE;

  if( in_id < mpmint_isr_tbl_size &&
      MPMINT_IMT_UNMAPPED_HAL_IRQ != mpmint_isr_tbl[in_id].hal_irq )
  {
    /* Only mapped interrupts/gpios are considered. */
    *out_id = (uint32)in_id;
    ret_val = TRUE;
  }

  return ret_val;
}

/**
 * @brief A function that loops through all the latched MPM interrupts and
 *        performs an associated soft-trigger operation so that it looks like
 *        the interrupt was caught in the normal mechanism, rather than by a
 *        secondary interrupt controller during deep sleep.
 *
 */
void mpmint_trigger_interrupts( void )
{
  uint32 num_masks, status, temp, phys_num, hal_num;
  uint32 gpio_num, i;
  uint16 gic_irq_id;   /* Interrupt id at master's gic level */

  HAL_mpmint_GetNumberMasks(&num_masks);

  for(i = 0; i < num_masks; i++)
  {
    HAL_mpmint_GetPending(i, &status);

    temp = status;
    while(temp)
    {
      /* Get the next interrupt's physical id and remove it for the future */
      phys_num = (31 - CoreCountLeadingZeros32(temp)) + (32 * i);
      temp &= ~(1 << (31 - CoreCountLeadingZeros32(temp)));

      /* try to figure out which interrupt it is */
      if(!HAL_mpmint_GetEnumNumber(phys_num, &hal_num))
      {
        continue;
      }

      /* If it's a gpio, perform soft-trigger into that driver */
      if( HAL_mpmint_IsGpio(hal_num, &gpio_num) )
      {
        mpmint_log_printf( 4, "Interrupt soft trigger "
                           "(type: \"GPIO\") (num: %u) (bit: %u) "
                           "(pending mask num: %u) (pending mask: 0x%x)",
                           gpio_num, phys_num, i, status );
        mpmint_retrigger_gpio( gpio_num );
      }
      /* Else check if it was a wakeup irq */
      else if( HAL_mpmint_IsWakeupIRQ(hal_num, &gic_irq_id) )
      {
        mpmint_log_printf( 4, "Interrupt soft trigger "
                           "(type: \"IRQ\") (num: %u) (bit: %u) "
                           "(pending mask num: %u) (pending mask: 0x%x)",
                           gic_irq_id, phys_num, i, status );

        mpmint_retrigger_interrupt( (uint32)gic_irq_id );
      }
      /* Not a GPIO or wakeup irq - stray signal? */

    }    /* end of while(temp) */
  }

  /* Clearing all interrupts for next cycle */
  HAL_mpmint_All_Clear();
}

/*
 * mpmint_get_num_mapped_interrupts
 */
uint32 mpmint_get_num_mapped_interrupts()
{
  return mpmint_num_mapped_irqs;
}

/*
 * mpmint_map_interrupts_with_master
 */
uint32 mpmint_map_interrupts_with_master
(
  mpmint_config_info_type *intrs,
  uint32 intrs_count
)
{
  uint32 mpm_num, irqs_mapped = 0;
  uint32 gpio_num;
  mpmint_irq_data_type *table_entry = NULL;    /* Mapping table iterator */

  if( intrs_count != mpmint_num_mapped_irqs )
  {
    /* Calling function does not expect same number of mapped interrupts as 
     * present in mpmint table. Log a message and return 0 indicating that
     * no interrupts were mapped. It is up to caller to handle that. */
    mpmint_log_printf( 2, "WARNING: Interrupt mapping count mismatch "
                       "(expected: %u) (requested: %u)", 
                       mpmint_num_mapped_irqs, intrs_count );
    return 0;
  }

  for( mpm_num = 0; mpm_num < mpmint_isr_tbl_size; mpm_num++ )
  {
    table_entry = &(mpmint_isr_tbl[mpm_num]);

    /* Third condition is somewhat redundant but just for sanity */
    if( MPMINT_IMT_UNMAPPED_HAL_IRQ != table_entry->hal_irq &&  /* Mapped IRQ */
        MPMINT_NULL_IRQ  != table_entry->local_irq &&           /* Wakeup IRQ */
        !HAL_mpmint_IsGpio( table_entry->hal_irq, &gpio_num ) )
    {
      intrs[irqs_mapped].mpm_hw_int_id = table_entry->hal_irq;
      intrs[irqs_mapped].master_hw_int_id = table_entry->local_irq;

      irqs_mapped++;
    }
  }

  mpmint_log_printf( 1, "Mapped mpm interrupts with master (IRQs Mapped: %u)",
                     irqs_mapped );

  return irqs_mapped;
}

/*
 * mpmint_config_int
 */
void mpmint_config_int
(
  mpmint_isr_type      int_num,     
  mpmint_detect_type   detection,
  mpmint_polarity_type polarity
)
{
  uint32 isr_tbl_idx; /* Index into mpmint_isr_tbl[] */

  if( TRUE == mpmint_valid_wakeup_interrupt(int_num, &isr_tbl_idx) )
  {
    mpmint_log_printf( 4, "Configuring interrupt (Local IRQ Id: %u) "
                       "(MPM ID: %u) (Detection: %d) (Polarity: %d)", 
                       mpmint_isr_tbl[isr_tbl_idx].local_irq, 
                       mpmint_isr_tbl[isr_tbl_idx].hal_irq, 
                       (int)detection, (int)polarity);
 
    VMPM_MSG_RAM_LOCK();
    HAL_mpmint_SetTrigger(mpmint_isr_tbl[isr_tbl_idx].hal_irq, 
                          mpmint_trans_int_type(detection, polarity));
    VMPM_MSG_RAM_UNLOCK();
  }
  else
  {
    mpmint_log_printf( 1, "WARNING: Invalid IRQ config request (IRQ id: %u)",
                       (uint32)int_num );
  }
}

/*
 * mpmint_config_wakeup
 */
void mpmint_config_wakeup
(
  mpmint_isr_type       int_num,
  mpmint_processor_type processor
)
{
  uint32 isr_tbl_idx; /* Index into mpmint_isr_tbl[] */

  if( TRUE == mpmint_valid_wakeup_interrupt(int_num, &isr_tbl_idx) )
  {
    mpmint_log_printf( 2, "Enabling interrupt (Local IRQ Id: %u) (MPM Id: %u)",
                       mpmint_isr_tbl[isr_tbl_idx].local_irq,
                       mpmint_isr_tbl[isr_tbl_idx].hal_irq );

    VMPM_MSG_RAM_LOCK();
    HAL_mpmint_Enable(mpmint_isr_tbl[isr_tbl_idx].hal_irq);
    VMPM_MSG_RAM_UNLOCK();
  }
  else
  {
    mpmint_log_printf( 1, "WARNING: Invalid IRQ enable request (IRQ id: %u)",
                       (uint32)int_num );
  }
}

/*
 * mpmint_disable_wakeup
 */
void mpmint_disable_wakeup(mpmint_isr_type int_num)
{
  uint32 isr_tbl_idx; /* Index into mpmint_isr_tbl[] */

  if( TRUE == mpmint_valid_wakeup_interrupt(int_num, &isr_tbl_idx) )
  {
    mpmint_log_printf( 2, "Disabling interrupt (Local IRQ Id: %u) (MPM Id: %u)",
                       mpmint_isr_tbl[isr_tbl_idx].local_irq,
                       mpmint_isr_tbl[isr_tbl_idx].hal_irq );

    VMPM_MSG_RAM_LOCK();
    HAL_mpmint_Disable(mpmint_isr_tbl[isr_tbl_idx].hal_irq);
    VMPM_MSG_RAM_UNLOCK();
  }
  else
  {
    mpmint_log_printf( 1, "WARNING: Invalid IRQ disable request (IRQ id: %u)",
                       (uint32)int_num );
  }
}

/*
 * mpmint_config_gpio_wakeup
 */
void mpmint_config_gpio_wakeup
(
  uint32                which_gpio,
  mpmint_detect_type    detection,
  mpmint_polarity_type  polarity,
  mpmint_processor_type processor
)
{
  uint32 int_num;         

  if(HAL_mpmint_IsGpioSupported(which_gpio, &int_num))
  {
    mpmint_log_printf( 4, "Enabling GPIO (Local id: %u) (MPM HAL id: %u) "
                       "(Detection: %d) (Polarity: %d)", which_gpio, int_num,
                       (int)detection, (int)polarity);

    VMPM_MSG_RAM_LOCK();
    HAL_mpmint_SetTrigger(int_num, mpmint_trans_int_type(detection, polarity));
    HAL_mpmint_Enable(int_num);
    VMPM_MSG_RAM_UNLOCK();
  }
  else
  {
    mpmint_log_printf( 1, "WARNING: Invalid GPIO config request (GPIO id: %u)",
                       which_gpio );
  }
}

/*
 * mpmint_disable_gpio_wakeup
 */
void mpmint_disable_gpio_wakeup
(
  uint32 which_gpio
)
{
  uint32 int_num;         

  if(HAL_mpmint_IsGpioSupported(which_gpio, &int_num))
  {
    mpmint_log_printf( 2, "Disabling GPIO (Local id: %u) (MPM HAL id: %u)",
                       which_gpio, int_num);

    VMPM_MSG_RAM_LOCK();
    HAL_mpmint_Disable(int_num);
    VMPM_MSG_RAM_UNLOCK();
  }
  else
  {
    mpmint_log_printf( 1, "WARNING: Invalid GPIO disable request (GPIO id: %u)",
                       which_gpio );
  }
}

/*
 * mpmint_setup_interrupts
 */
void mpmint_setup_interrupts
(
  mpmint_config_info_type *intrs,
  uint32 intrs_count
)
{
  uint32 i;
  uint16 gic_irq;
  bool32 supported_irq = FALSE;

  if( (intrs_count != mpmint_num_mapped_irqs) )
  {
    /* Input array does not contain same number of interrupts than are 
     * actually mapped for this master */
    mpmint_log_printf( 2, "ERROR: Interrupt setup count mismatch "
                       "(expected: %u) (requested: %u)",
                       mpmint_num_mapped_irqs, intrs_count);
    CORE_VERIFY(0);
  }

  if( 0 == mpmint_num_mapped_irqs )
  {
    /* Only GPIOs are mapped to mpm for this target. No need to
     * configure anything. Simply return */
    mpmint_log_printf( 0, "WARNING: No interrupt mapped to MPM" );
    return ;
  }

  /* Checking the validity of input */
  for( i = 0; i < intrs_count; i++ )
  {
    supported_irq = HAL_mpmint_IsWakeupIRQ(intrs[i].mpm_hw_int_id, &gic_irq);
    if( (FALSE == supported_irq) || (gic_irq != intrs[i].master_hw_int_id) )
    {
      /* Either the mpm_id or the master hw id is not consistent with what
       * information vMPM driver has. It should not usually happen as clients
       * are not supposed to disturb mpmint_config_info_type objects. */

      mpmint_log_printf(3, "ERROR: Possibly corrupted mpm_config_info_type obj"
                        " (index: %u) (mpm id: %d) (master id: %u)", i, 
                        intrs[i].mpm_hw_int_id, intrs[i].master_hw_int_id);
      CORE_VERIFY(0);
    }
  }

  VMPM_MSG_RAM_LOCK();
  HAL_mpmint_setup_interrupts((HAL_mpmint_ConfigInfoType *)intrs, intrs_count);
  VMPM_MSG_RAM_UNLOCK();

}

