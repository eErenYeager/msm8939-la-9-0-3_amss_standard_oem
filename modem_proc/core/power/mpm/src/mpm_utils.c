/*===========================================================================
   FILE          mpm_utils.c
 
   DESCRIPTION   Contains implementation of public MPM utility functions.

                 Copyright � 2011-2013 QUALCOMM Technologies Incorporated.
                 All Rights Reserved.
                 QUALCOMM Confidential and Proprietary.

   $Header: //components/rel/core.mpss/3.7.24/power/mpm/src/mpm_utils.c#1 $

=============================================================================*/

/*============================================================================
                           INCLUDE FILES FOR MODULE
============================================================================*/

#include "mpm_utils.h"
#include "mpmhw_hwio.h"
#include "mpminti.h"
#include "HALmpmintInternal.h"

/*===========================================================================
 *                        GLOBAL FUNCTION DEFINITIONS
 * =========================================================================*/

/**
 * Reads time in sclk read from mpm registers.
 *
 * @Note:
 * This API is more for legacy code support. On B family targets, clients 
 * should use QTimer base time instead of sclk based.
 */
uint32 mpm_get_timetick( void )
{
  uint32 curr_tick;
  uint32 last_tick;
  static int mpm_hw_reg_mapped = 0;
  static uint32 mpm_timetick_reg_addr = 0;

  if(!mpm_hw_reg_mapped)
  {
    /* Map physical address of actual MPM hw registe base to some 
     * virtual address so we can access mpm hardware registers. */
    mpm_map_hw_reg_base();
    mpm_timetick_reg_addr = HWIO_MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL_ADDR;
    mpm_hw_reg_mapped = 1;
  }

  /* Reading the counter value once may not return an accurate value if the
   * counter is in the processing of counting to the next value, and several
   * bits are changing.  Instead, the counter is repeatedly read until a
   * consistant value is read. */
  curr_tick = inpdw(mpm_timetick_reg_addr);
  do 
  {
    last_tick = curr_tick;
    curr_tick = inpdw(mpm_timetick_reg_addr);
  } while( curr_tick != last_tick );

  return curr_tick;

}

/**
 * mpm_set_wakeup_timetick
 */
void mpm_set_wakeup_timetick(uint64_t wakeup_tick)
{
  uint32 nReg;
  nReg = registerLayout.WakeupReg;

  /* Capturing lower 32 bits */
  outpdw(nReg, (uint32)(wakeup_tick & UINT32_MAX));

  nReg += sizeof(uint32);

  /* Caputuring upper 32 bits */
  outpdw(nReg, (uint32)((wakeup_tick >> 32) & UINT32_MAX));

  return;
}
