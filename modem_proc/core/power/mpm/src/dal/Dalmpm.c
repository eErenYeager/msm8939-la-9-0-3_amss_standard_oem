/*==============================================================================

FILE:      Dalmpm.c

DESCRIPTION: This file implements a sample DeviceDriver.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

        Copyright (c) 2013 QUALCOMM Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "Dalmpm.h"
#include "mpmint.h"
/*------------------------------------------------------------------------------
Following functions are for DALDriver specific functionality
------------------------------------------------------------------------------*/
DALResult mpm_DriverInit(mpmDrvCtxt *pCtxt)
{
  return DAL_SUCCESS;
}

DALResult mpm_DriverDeInit(mpmDrvCtxt *pCtxt)
{
  return DAL_SUCCESS;
}

/*------------------------------------------------------------------------------
Following functions are declared in DalDevice Interface. 
------------------------------------------------------------------------------*/
DALResult mpm_DeviceInit(mpmClientCtxt *pCtxt)
{
  return DAL_SUCCESS;
}

DALResult mpm_DeviceDeInit(mpmClientCtxt *pCtxt)
{
  return DAL_SUCCESS;
}

DALResult mpm_PowerEvent(mpmClientCtxt *pCtxt,
                         DalPowerCmd    PowerCmd, 
                         DalPowerDomain PowerDomain)
{
  return DAL_SUCCESS;
}

DALResult mpm_Open(mpmClientCtxt *pCtxt, uint32 dwaccessMode )
{
  return DAL_SUCCESS;
}

DALResult mpm_Close(mpmClientCtxt *pCtxt)
{
  return DAL_SUCCESS;
}

DALResult mpm_Info(mpmClientCtxt *pCtxt,
                   DalDeviceInfo *pdeviceInfo,
                   uint32         dwSize)
{
  return DAL_SUCCESS;
}

/*------------------------------------------------------------------------------
Following functions are extended in Dalmpm Interface. 
------------------------------------------------------------------------------*/

DALResult mpm_ConfigInt( mpmClientCtxt      * pCtxt,
                         uint32               int_num,
                         mpmint_detect_type   detection,
                         mpmint_polarity_type polarity) 
{
  mpmint_config_int(int_num, detection, polarity);
  return DAL_SUCCESS;
}

DALResult mpm_ConfigWakeup( mpmClientCtxt * pCtxt, uint32  int_num) 
{

  mpmint_config_wakeup(int_num, MPMINT_MODEM);
  return DAL_SUCCESS;
}

DALResult mpm_ConfigGpioWakeup( mpmClientCtxt       *pCtxt,
                                uint32               which_gpio,
                                mpmint_detect_type   detection,
                                mpmint_polarity_type polarity) 
{

  mpmint_config_gpio_wakeup(which_gpio, detection, polarity, MPMINT_MODEM);
  return DAL_SUCCESS;
}

DALResult mpm_DisableWakeup( mpmClientCtxt * pCtxt, uint32 int_num) 
{
  mpmint_disable_wakeup(int_num);
  return DAL_SUCCESS;
}

DALResult mpm_DisableGpioWakeup( mpmClientCtxt * pCtxt, uint32 which_gpio) 
{
  mpmint_disable_gpio_wakeup(which_gpio);
  return DAL_SUCCESS;
}

DALResult mpm_GetNumMappedInterrupts( mpmClientCtxt * pCtxt, uint32 *pnum) 
{
  if(NULL == pnum)
  {
    return DAL_ERROR;
  }
   
  *pnum = mpmint_get_num_mapped_interrupts();
  return DAL_SUCCESS;
}

DALResult mpm_MapInterrupts( mpmClientCtxt           *pCtxt,
                             mpmint_config_info_type *intrs,
                             uint32                   intrs_count,
                             uint32                  *pintrs_mapped) 
{
  *pintrs_mapped = mpmint_map_interrupts_with_master(intrs, intrs_count);
  return DAL_SUCCESS;
}

DALResult mpm_SetupInterrupts( mpmClientCtxt           *pCtxt,
                               mpmint_config_info_type *intrs,
                               uint32                   intrs_count) 
{
  mpmint_setup_interrupts(intrs, intrs_count);
  return DAL_SUCCESS;
}

