/*==============================================================================

FILE:      DalmpmFwk.c

DESCRIPTION: This file implements a mpm DeviceDriver.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

       Copyright (c) 2013 QUALCOMM Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/

#include "DALFramework.h"
#include "DALSys.h"
#include "DDImpm.h"
#include "Dalmpm.h"
#include "DALSys.h"
#include "string.h" //for memset warning


DALResult mpm_Dalmpm_Attach(const char *, DALDEVICEID,DalDeviceHandle **);
  
static uint32 mpm_Dalmpm_AddRef(DalmpmHandle* h)
{
  return DALFW_AddRef((DALClientCtxt *)(h->pClientCtxt));
}

/*------------------------------------------------------------------------------
Following functions are defined in DalDevice DAL Interface.
------------------------------------------------------------------------------*/

static uint32 mpm_Dalmpm_Detach(DalDeviceHandle* h)
{
  uint32 dwref = DALFW_Release((DALClientCtxt *)(h->pClientCtxt));
  return dwref;
}

static DALResult mpm_Dalmpm_Init(DalDeviceHandle *h)
{  
  mpmClientCtxt *pClientCtxt = (mpmClientCtxt *)(h->pClientCtxt);
  DALSYS_GetDALPropertyHandle(pClientCtxt->pmpmDevCtxt->DevId,
                              pClientCtxt->pmpmDevCtxt->hProp);
  return mpm_DeviceInit(h->pClientCtxt);
}

static DALResult mpm_Dalmpm_DeInit(DalDeviceHandle *h)
{
  return mpm_DeviceDeInit(h->pClientCtxt);
}

static DALResult mpm_Dalmpm_PowerEvent(DalDeviceHandle *h, DalPowerCmd PowerCmd, 
                                       DalPowerDomain PowerDomain )
{
  return mpm_PowerEvent(h->pClientCtxt,PowerCmd,PowerDomain);
}

static DALResult mpm_Dalmpm_Open(DalDeviceHandle* h, uint32 mode)
{
  return mpm_Open(h->pClientCtxt,mode);
}

static DALResult mpm_Dalmpm_Close(DalDeviceHandle* h)
{
  return mpm_Close(h->pClientCtxt);
}

static DALResult mpm_Dalmpm_Info(DalDeviceHandle *h,
                                 DalDeviceInfo   *info,
                                 uint32           infoSize)
{
  info->Version = DALMPM_INTERFACE_VERSION;
  return mpm_Info(h->pClientCtxt,info,infoSize);
}

static DALResult mpm_Dalmpm_SysRequest(DalDeviceHandle *h,
                                       DalSysReq        ReqIdx,
                                       const void      *SrcBuf,
                                       uint32           SrcBufLen, 
                                       void            *DestBuf,
                                       uint32           DestBufLen,
                                       uint32          *DestBufLenReq)
{
  return DAL_ERROR;
}

/*------------------------------------------------------------------------------
Following functions are extended in Dalmpm Interface. 
------------------------------------------------------------------------------*/
static DALResult mpm_Dalmpm_ConfigInt( DalDeviceHandle      *h,
                                       uint32               int_num,
                                       mpmint_detect_type   detection,
                                       mpmint_polarity_type polarity) 
{
  return mpm_ConfigInt(((DalmpmHandle *)h)->pClientCtxt, int_num, detection, polarity);
}

static DALResult mpm_Dalmpm_ConfigWakeup( DalDeviceHandle *h, uint32  int_num) 
{
  return mpm_ConfigWakeup(((DalmpmHandle *)h)->pClientCtxt, int_num);
}

static DALResult mpm_Dalmpm_ConfigGpioWakeup( DalDeviceHandle      *h,
                                              uint32               which_gpio,
                                              mpmint_detect_type   detection,
                                              mpmint_polarity_type polarity) 
{
  return mpm_ConfigGpioWakeup(((DalmpmHandle *)h)->pClientCtxt, which_gpio, detection, polarity);
}

static DALResult mpm_Dalmpm_DisableWakeup( DalDeviceHandle *h, uint32 int_num) 
{
  return mpm_DisableWakeup(((DalmpmHandle *)h)->pClientCtxt, int_num);
}

static DALResult mpm_Dalmpm_DisableGpioWakeup( DalDeviceHandle *h, uint32 which_gpio) 
{
  return mpm_DisableGpioWakeup(((DalmpmHandle *)h)->pClientCtxt, which_gpio);
}

static DALResult mpm_Dalmpm_GetNumMappedInterrupts( DalDeviceHandle *h, uint32 *pnum) 
{
  return mpm_GetNumMappedInterrupts(((DalmpmHandle *)h)->pClientCtxt, pnum);
}

static DALResult mpm_Dalmpm_MapInterrupts( DalDeviceHandle         *h,
                                           mpmint_config_info_type *intrs,
                                           uint32                   intrs_count,
                                           uint32                  *intrs_mapped) 
{
  return mpm_MapInterrupts(((DalmpmHandle *)h)->pClientCtxt, intrs, intrs_count, intrs_mapped);
}

static DALResult mpm_Dalmpm_SetupInterrupts( DalDeviceHandle         *h,
                                             mpmint_config_info_type *intrs,
                                             uint32                   intrs_count) 
{
  return mpm_SetupInterrupts(((DalmpmHandle *)h)->pClientCtxt, intrs, intrs_count);
}

static void 
mpm_InitInterface(mpmClientCtxt* pclientCtxt)
{
  static const Dalmpm vtbl = {
    {
      mpm_Dalmpm_Attach,
      mpm_Dalmpm_Detach,
      mpm_Dalmpm_Init,
      mpm_Dalmpm_DeInit,
      mpm_Dalmpm_Open,
      mpm_Dalmpm_Close,
      mpm_Dalmpm_Info,
      mpm_Dalmpm_PowerEvent,
      mpm_Dalmpm_SysRequest
    } ,
    mpm_Dalmpm_ConfigInt,
    mpm_Dalmpm_ConfigWakeup,
    mpm_Dalmpm_ConfigGpioWakeup,
    mpm_Dalmpm_DisableWakeup,
    mpm_Dalmpm_DisableGpioWakeup,
    mpm_Dalmpm_GetNumMappedInterrupts,
    mpm_Dalmpm_MapInterrupts,
    mpm_Dalmpm_SetupInterrupts
  };  

  /*--------------------------------------------------------------------------
  Depending upon client type setup the vtables (entry points)
  --------------------------------------------------------------------------*/
  pclientCtxt->DalmpmHandle.dwDalHandleId = DALDEVICE_INTERFACE_HANDLE_ID;
  pclientCtxt->DalmpmHandle.pVtbl  = &vtbl;
  pclientCtxt->DalmpmHandle.pClientCtxt = pclientCtxt;

}

DALResult mpm_Dalmpm_Attach(const char *pszArg, DALDEVICEID DeviceId, 
                            DalDeviceHandle **phDalDevice)
{               
  DALResult nErr;
  static mpmDrvCtxt drvCtxt = {{mpm_DriverInit,
                                mpm_DriverDeInit
                                },1,
                                sizeof(mpmDevCtxt)};
  static mpmClientCtxt clientCtxt;

  mpmClientCtxt *pclientCtxt = &clientCtxt;
  *phDalDevice = NULL;

  nErr = DALFW_AttachToStringDevice(pszArg,(DALDrvCtxt *)&drvCtxt,
                                    (DALClientCtxt *)pclientCtxt);

  if (DAL_SUCCESS == nErr)
  {
    mpm_InitInterface(pclientCtxt);
    mpm_Dalmpm_AddRef(&(pclientCtxt->DalmpmHandle));
    *phDalDevice = (DalDeviceHandle *)&(pclientCtxt->DalmpmHandle);
  }
  return nErr;
}

