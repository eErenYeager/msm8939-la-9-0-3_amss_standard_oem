/*
===========================================================================
  
  FILE:         HALmpmint.c
  
  DESCRIPTION:  This is the generic hardware abstraction layer implementation 
                for the MPM interrupt controller.
  
  DEPENDENCIES: NONE
  
                Copyright � 2008-2013 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <string.h>
#include <HALcomdef.h>
#include "HALmpmint.h"
#include "HALmpmintInternal.h"

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */

/*
 * Identifier string for this HAL.
 */
#define HAL_MPMINT_VERSION_STRING "HAL_MPMINT_V2"

/*
 * Macro to determine what offset should be used while configuring given
 * interrupt number. Validity of the input should be verified prior to using
 * value returned by this macro.
 */
#define MPMINT_2_REG_OFFSET(nMPM) ((sizeof(uint32)*(nMPM/(sizeof(uint32)*8))))

/*
 * Macros to create bit masks, set and clear bits in a mask.
 */
#define MAKE_MASK(bit_pos)  (1 << ((bit_pos) % 32))
#define BIT_SET32(val, bit_pos)  (val |  (MAKE_MASK(bit_pos)))
#define BIT_CLR32(val, bit_pos)  (val & ~(MAKE_MASK(bit_pos)))

/* -------------------------------------------------------------------------
**                         VARIABLE DECLARATIONS
** ------------------------------------------------------------------------- */

/**
 * Register layout object for the master. This will store information 
 * regarding interrupt/gpio configuration (detection, polarity, status) and
 * will be used by RPM to write to actual MPM driver.
 */
HAL_mpmint_PlatformType registerLayout;

/* -----------------------------------------------------------------------
**                         INTERNAL FUNCTIONS
** ----------------------------------------------------------------------- */

/** 
 * Configures the detection and polarity of a given interrupt from the input
 * trigger type. 
 *
 * @note It must be noted that it does not notify RPM about these
 *       changes so it is up to the caller to perform this task.
 *
 * @param nMPM: Interrupt id (at MPM HW) for which we want to change this
 *              configuration.
 * @param eTrigger: New trigger settings for detection and polarity
 *                  Any valid enum value of type HAL_mpmint_TriggerType
 *                  can be a possible input.
 *
 * @Note
 * From MPM HDD, three bits are used as follows for interrupt detection and 
 * polarity. 
 *   
 * 0 00    Level Sensitive Active Low
 * 0 01    Falling Edge Sensitive
 * 0 10    Rising Edge Sensitive
 * 0 11    Dual Edge Sensitive
 * 1 00    Level Sensitive Active High
 * 1 01    Rising Edge Sensitive
 * 1 10    Falling Edge Sensitive
 * 1 11    Dual Edge Sensitive
 */
static void HAL_mpmint_ConfigDetectionAndPolarity
(
  uint32 nMPM,
  HAL_mpmint_TriggerType eTrigger
)
{
  uint32 nReg;
  uint32 nVal;
  uint32 nIndex;

  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    /* Unsupported mpm irq/gpio id */
    return;
  }

  /* Offset of nMPM within two 4-byte registers (0 or 4) */
  nIndex = MPMINT_2_REG_OFFSET(nMPM);

  /* Configure detection1 bit */
  nReg = registerLayout.nDetectReg + nIndex;
  nVal = inpdw(nReg);
  nVal = HAL_TT_EXTRACT_DET1_BIT(eTrigger) ? 
         BIT_SET32(nVal, nMPM) : BIT_CLR32(nVal, nMPM);
  outpdw(nReg, nVal);

  /* Configure detection3 bit */
  nReg = registerLayout.nDetect3Reg + nIndex;
  nVal = inpdw(nReg);
  nVal = HAL_TT_EXTRACT_DET3_BIT(eTrigger) ? 
         BIT_SET32(nVal, nMPM) : BIT_CLR32(nVal, nMPM);
  outpdw(nReg, nVal);

  /* Configure the polarity */
  nReg = registerLayout.nPolarityReg + nIndex;
  nVal = inpdw(nReg);
  nVal = HAL_TT_EXTRACT_POL_BIT(eTrigger) ? 
         BIT_SET32(nVal, nMPM) : BIT_CLR32(nVal, nMPM);
  outpdw(nReg, nVal);

  /* Update the shadow copy in the HAL table */
  aInterruptTable[nMPM].eTrigger = eTrigger;
  
}


/**
 * Sets the status of a given input interrupt. 
 *
 * @note It simply writes in shared memory but does not notirfy RPM. It's 
 *       left up to caller.
 *
 *       Also, it simply sets status bit for a given interrupt so if some
 *       pre/post processing is needed, it has to be done by call (e.g.
 *       clearing the interrupt before enabling it).
 *
 * @param nMPM:    Interrupt id (at MPM HW) for which we want to change this
 *                 configuration.
 * @param eStatus: Status to which above interrupt will be set to.
 *                 Possible Inputs:
 *                 - HAL_MPMINT_STATUS_ENABLE
 *                 - HAL_MPMINT_STATUS_DISABLE
 */
static void HAL_mpmint_SetStatus
(
  uint32 nMPM,
  HAL_mpmint_StatusType eStatus
)
{
  uint32 nReg;
  uint32 nVal;
  uint32 nIndex;

  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    /* Unsupported mpm irq/gpio id */
    return;
  }

  /* Offset of nMPM within two 4-byte registers (0 or 4) */
  nIndex = MPMINT_2_REG_OFFSET(nMPM); 

  nReg = registerLayout.nEnableReg + nIndex;
  nVal = inpdw(nReg);
  nVal = (HAL_MPMINT_STATUS_ENABLE == eStatus) ? 
         BIT_SET32(nVal, nMPM) : BIT_CLR32(nVal, nMPM);
  outpdw(nReg, nVal);  
}

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */


/* ===========================================================================
**  HAL_mpmint_Init
**
** ======================================================================== */

void HAL_mpmint_Init
(
  char **ppszVersion,
  uint8 *master_msg_ram_base_ptr,
  bool32 init_vmpm_regs
)
{
  uint32 nMPM;

  /* Initializing Target specific HAL data */
  HAL_mpmint_InitTargetData();

  /* Preparing mpm driver for sending IPC interrupt to let rpm know about the 
   * modified interrupt/gpio configurations. */
  HAL_mpmint_SetupIPC();

  /* Initializing the registerLayout of shared memory */
  HAL_mpmint_InitializeRegisterLayout(master_msg_ram_base_ptr);

  if( TRUE == init_vmpm_regs )
  {
    /* Populate MPM interrupt controller with initial guesses from BSP data. */
    for (nMPM = 0; nMPM < HAL_MPMINT_NUM; nMPM++)
    {
      HAL_mpmint_SetTrigger( nMPM, aInterruptTable[nMPM].eTrigger );
    }

    /* Disable and clear all interrupts */
    HAL_mpmint_All_Disable();
    HAL_mpmint_All_Clear();
  }

  /* Fill in return value */
  if (ppszVersion != NULL)
  {
    *ppszVersion = (char*)HAL_MPMINT_VERSION_STRING;
  }

} /* END HAL_mpmint_Init */


/* ===========================================================================
**  HAL_mpmint_Reset
**
** ======================================================================== */

void HAL_mpmint_Reset (void)
{
  HAL_mpmint_All_Disable();
  HAL_mpmint_All_Clear();

} /* END HAL_mpmint_Reset */


/* ===========================================================================
**  HAL_mpmint_Save
**
** ======================================================================== */

void HAL_mpmint_Save (void)
{
  /* MPM interrupt controller is always on. */
} /* END HAL_mpmint_Save */


/* ===========================================================================
**  HAL_mpmint_Restore
**
** ======================================================================== */

void HAL_mpmint_Restore (void)
{
  /* MPM interrupt controller is always on. */
} /* END HAL_mpmint_Restore */


/* ===========================================================================
**  HAL_mpmint_Enable
**
** ======================================================================== */

void HAL_mpmint_Enable
(
  uint32 nMPM
)
{
  /* Enable the interrupt in the MPM register */
  HAL_mpmint_SetStatus( nMPM, HAL_MPMINT_STATUS_ENABLE );

  vmpm_send_interrupt();

} /* END HAL_mpmint_Enable */


/* ===========================================================================
**  HAL_mpmint_Disable
**
** ======================================================================== */

void HAL_mpmint_Disable
(
  uint32 nMPM
)
{
  /* Disable the interrupt in MPM */
  HAL_mpmint_SetStatus( nMPM, HAL_MPMINT_STATUS_DISABLE );

  vmpm_send_interrupt();

} /* END HAL_mpmint_Disable */


/* ===========================================================================
**  HAL_mpmint_Clear
**
** ======================================================================== */

void HAL_mpmint_Clear
(
  uint32 nMPM
)
{
  uint32 nReg;
  uint32 nVal;
  uint32 nIndex;

  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    /* Unsupported mpm irq/gpio id */
    return;
  }

  /* Offset of nMPM within two 4-byte registers (0 or 4) */
  nIndex = MPMINT_2_REG_OFFSET(nMPM); 

  nReg = registerLayout.nStatusReg + nIndex;
  nVal = inpdw(nReg);
  nVal = BIT_CLR32(nVal, nMPM);
  outpdw(nReg, nVal); 
  
  /* No need to notify the RPM, since it has nothing to do in response to
   * a clear. */
   
} /* END HAL_mpmint_Clear */


/* ===========================================================================
**  HAL_mpmint_All_Enable
**
** ======================================================================== */

void HAL_mpmint_All_Enable( void )
{
  uint32 nNumRegs, nReg, nMask, regIndex;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  /* We could have used memset as used earlier but somehow it results into
   * symbol loading problem. */
  for( regIndex = 0; regIndex < nNumRegs; regIndex++ )
  {
    nReg  = registerLayout.nEnableReg + (sizeof(uint32) * regIndex);
    nMask = 0xFFFFFFFF;

    outpdw(nReg, nMask);               /* Enable the interrupts */
  }

  vmpm_send_interrupt();

} /* END HAL_mpmint_All_Enable */


/* ===========================================================================
**  HAL_mpmint_All_Disable
**
** ======================================================================== */

void HAL_mpmint_All_Disable( void )
{
  uint32 nNumRegs, nReg, nMask, regIndex;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  /* We could have used memset as used earlier but somehow it results into
   * symbol loading problem. */
  for( regIndex = 0; regIndex < nNumRegs; regIndex++ )
  {
    nReg  = registerLayout.nEnableReg + (sizeof(uint32) * regIndex);
    nMask = 0x0;
    outpdw(nReg, nMask);
  }

  vmpm_send_interrupt();

} /* END HAL_mpmint_Disable */


/* ===========================================================================
**  HAL_mpmint_All_Clear
**
** ======================================================================== */

void HAL_mpmint_All_Clear( void )
{
  uint32 nNumRegs, nReg, nMask, regIndex;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  for( regIndex = 0; regIndex < nNumRegs; regIndex++ )
  {
    nReg  = registerLayout.nStatusReg + (sizeof(uint32) * regIndex);
    nMask = 0x0;
    outpdw(nReg, nMask);
  }

  /* No need to notify the RPM, since it has nothing to do in response to
   * a clear. */

} /* END HAL_mpmint_All_Clear */


/* ===========================================================================
**  HAL_mpmint_GetNumberMasks
**
** ======================================================================== */

void HAL_mpmint_GetNumberMasks
(
  uint32 *pnNumber
)
{
  *pnNumber = ((HAL_MPMINT_PHYS_INTS + 31) / 32);

} /* END HAL_mpmint_GetNumberMasks */


/* ===========================================================================
**  HAL_mpmint_Mask_Enable
**
** ======================================================================== */

void HAL_mpmint_Mask_Enable
(
  uint32 nMaskIndex,
  uint32 nMask
)
{
  uint32 nReg, nVal, nNumRegs;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  if(nMaskIndex >= nNumRegs)
    return;

  /* Enable the interrupts in the MPM register */
  nReg = registerLayout.nEnableReg + (sizeof(uint32) * nMaskIndex);
  nVal = inpdw(nReg);
  nVal |= nMask;
  outpdw(nReg, nVal);

  vmpm_send_interrupt();

} /* END HAL_mpmint_Mask_Enable */


/* ===========================================================================
**  HAL_mpmint_Mask_Disable
**
** ======================================================================== */

void HAL_mpmint_Mask_Disable
(
  uint32 nMaskIndex,
  uint32 nMask
)
{
  uint32 nReg, nVal, nNumRegs;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  if(nMaskIndex >= nNumRegs)
    return;

  /* Disable the interrupts in the MPM register */
  nReg = registerLayout.nEnableReg + (sizeof(uint32) * nMaskIndex);
  nVal = inpdw(nReg);
  nVal &= ~nMask;
  outpdw(nReg, nVal);

  vmpm_send_interrupt();

} /* END HAL_mpmint_Mask_Disable */


/* ===========================================================================
**  HAL_mpmint_Mask_Clear
**
** ======================================================================== */

void HAL_mpmint_Mask_Clear
(
  uint32 nMaskIndex,
  uint32 nMask
)
{
  uint32 nReg, nVal, nNumRegs;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  if(nMaskIndex >= nNumRegs)
    return;

  /* Clear the interrupts in the MPM register */
  nReg = registerLayout.nStatusReg + (sizeof(uint32) * nMaskIndex);
  nVal = inpdw(nReg);
  nVal &= ~nMask;
  outpdw(nReg, nVal);

  /* No need to notify the RPM, since it has nothing to do in response to
   * a clear. */

} /* END HAL_mpmint_Mask_Clear */


/* ===========================================================================
**  HAL_mpmint_GetPending
**
** ======================================================================== */

void HAL_mpmint_GetPending
(
  uint32 nMaskIndex,
  uint32 *pnMask
)
{
  uint32 nNumRegs, nReg, nEnable;

  HAL_mpmint_GetNumberMasks(&nNumRegs);

  if((nMaskIndex >= nNumRegs) || (!pnMask))
    return;

  nReg = registerLayout.nStatusReg + (sizeof(uint32) * nMaskIndex);
  nEnable = registerLayout.nEnableReg + (sizeof(uint32) * nMaskIndex);
  *pnMask = ( inpdw(nReg) & inpdw(nEnable) );

} /* HAL_mpmint_GetPending */


/* ===========================================================================
**  HAL_mpmint_GetTrigger
**
** ======================================================================== */

void HAL_mpmint_GetTrigger
(
  uint32                  nMPM,
  HAL_mpmint_TriggerType  *peTrigger
)
{
  uint32 nIndex, nMask, nReg;
  uint32 nDetect1Val, nDetect3Val, nPolarityVal;

  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    /* Unsupported mpm irq/gpio id */
    return;
  }

  /* Offset of nMPM within two 4-byte registers (0 or 4) */
  nIndex = MPMINT_2_REG_OFFSET(nMPM);

  nMask = MAKE_MASK(nMPM);

  nReg = registerLayout.nDetectReg + nIndex;
  nDetect1Val = (inpdw(nReg) & nMask) ? 1 : 0;

  nReg = registerLayout.nDetect3Reg + nIndex;
  nDetect3Val = (inpdw(nReg) & nMask) ? 1 : 0;

  nReg = registerLayout.nPolarityReg + nIndex;
  nPolarityVal = (inpdw(nReg) & nMask) ? 1 : 0;

  /* HAL_mpmint_TriggerType is enum'ed to out of POLARITY | DETECT3 | DETECT1 */
  *peTrigger = (HAL_mpmint_TriggerType)((nPolarityVal << 0x2) | 
                                        (nDetect3Val << 0x1)  | 
                                        (nDetect1Val << 0) );

  /* Update the shadow in case it was incorrect */
  aInterruptTable[nMPM].eTrigger = *peTrigger;

} /* HAL_mpmint_GetTrigger */


/* ===========================================================================
**  HAL_mpmint_SetTrigger
**
** ======================================================================== */

void HAL_mpmint_SetTrigger
(
  uint32                   nMPM,
  HAL_mpmint_TriggerType  eTrigger
)
{
  /* Configuring to input detection and polarity. */
  HAL_mpmint_ConfigDetectionAndPolarity(nMPM, eTrigger);

  /* Commit the change. */
  vmpm_send_interrupt();

} /* END HAL_mpmint_SetTrigger */


/* ===========================================================================
**  HAL_mpmint_MapWakeupIRQ
**
** ======================================================================== */

void HAL_mpmint_MapWakeupIRQ
(
  uint32 nMPM,
  uint16 nGicIrq
)
{
  if( HAL_mpmint_IsSupported(nMPM) && 
      (HAL_MPMINT_INVALID_GPIO == aInterruptTable[nMPM].gpio) )
  {
    /* Fill in gic_irq field only if index is valid for the HAL
     * table and entry at that index is not marked as GPIO. */
    aInterruptTable[nMPM].gic_irq = nGicIrq;
  }
}


/* ===========================================================================
**  HAL_mpmint_GetNumber
**
** ======================================================================== */

void HAL_mpmint_GetNumber
(
  uint32 *pnNumber
)
{
  /* MPM interrupt controller supports HAL_MPMINT_NUM interrupts */
  if( NULL != pnNumber )
  {
    *pnNumber = HAL_MPMINT_NUM;
  }

} /* END HAL_mpmint_GetNumber */


/* ===========================================================================
**  HAL_mpmint_GetNumberPhysical
**
** ======================================================================== */

void HAL_mpmint_GetNumberPhysical
(
  uint32 *pnNumber
)
{
  /* the correct number is defined by the platform */
  if( NULL != pnNumber )
  {
    *pnNumber = HAL_MPMINT_PHYS_INTS;
  }

} /* HAL_mpmint_GetNumberPhysical */


/* ===========================================================================
**  HAL_mpmint_GetPhysNumber
**
** ======================================================================== */

bool32 HAL_mpmint_GetPhysNumber
(
  uint32 nMPM,
  uint32 *pnPhys
)
{
  if( !HAL_mpmint_IsSupported(nMPM) || (NULL == pnPhys) )
  {
    return FALSE;
  }
  else
  {
    /* current HAL uses a 1:1 mapping between phys and enum */
    *pnPhys = nMPM;
    return TRUE;
  }

} /* HAL_mpmint_GetPhysNumber */


/* ===========================================================================
**  HAL_mpmint_GetEnumNumber
**
** ======================================================================== */

bool32 HAL_mpmint_GetEnumNumber
(
  uint32 nPhys,
  uint32 *pnMPM
)
{
  if( !HAL_mpmint_IsSupported(nPhys) || (NULL == pnMPM) )
  {
    return FALSE;
  }
  else
  {
    /* current HAL uses a 1:1 mapping between phys and enum */
    *pnMPM = nPhys;
    return TRUE;
  }

} /* HAL_mpmint_GetEnumNumber */


/* ===========================================================================
**  HAL_mpmint_IsSupported
**
** ======================================================================== */

bool32 HAL_mpmint_IsSupported
(
  uint32 nMPM
)
{
  return ( nMPM < HAL_MPMINT_NUM );
} /* END HAL_mpmint_IsSupported */


/* ===========================================================================
**  HAL_mpmint_IsGpioSupported
**
** ======================================================================== */

bool32 HAL_mpmint_IsGpioSupported
(
  uint32 nGPIO,
  uint32 *pnMPM
)
{
  uint32 nMPM;

  /* Ensure GPIO number is valid */
  if ( !(nGPIO < HAL_MPMINT_INVALID_GPIO)  )
  {
    return FALSE;
  }

  /* Check to see if this GPIO is supported as a wakeup, and if so, 
   * return the MPM interrupt that it corresponds to. */ 
  for ( nMPM = 0; nMPM < HAL_MPMINT_NUM; nMPM++ )
  {
    if ( aInterruptTable[nMPM].gpio == nGPIO )
    {
      break;
    }
  }

  if ( (nMPM < HAL_MPMINT_NUM) && (NULL != pnMPM) )
  {
    *pnMPM = nMPM;
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}


/* ===========================================================================
**  HAL_mpmint_IsGpio
**
** ======================================================================== */

bool32 HAL_mpmint_IsGpio
(
  uint32 nMPM,
  uint32 *pnGPIO
)
{
  bool32 retval = FALSE;

  if( !HAL_mpmint_IsSupported(nMPM) || (NULL == pnGPIO) )
  {
    return retval;
  }

  if ( aInterruptTable[nMPM].gpio != HAL_MPMINT_INVALID_GPIO )
  {
    *pnGPIO = aInterruptTable[nMPM].gpio;
    retval = TRUE;
  }

  return retval;
} /* HAL_mpmint_IsGpio */


/* ========================================================================
** HAL_mpmint_IsWakeupIRQ
**
** ========================================================================*/

bool32 HAL_mpmint_IsWakeupIRQ
( 
  uint32 nMPM, 
  uint16 *pnIRQ
)
{
  bool32 bIsWakeupIRQ = FALSE;

  if( !HAL_mpmint_IsSupported(nMPM) || (NULL == pnIRQ) )
  {
    /* Invalid input */
    return bIsWakeupIRQ;
  }

  if( (HAL_MPMINT_INVALID_GPIO == aInterruptTable[nMPM].gpio) &&
      (HAL_MPMINT_GIC_IRQ_NONE != aInterruptTable[nMPM].gic_irq) )
  {
    /* Given entry in HAL table is not a GPIO and has a valid GIC irq id */
    *pnIRQ = aInterruptTable[nMPM].gic_irq;
    bIsWakeupIRQ = TRUE;
  }

  return bIsWakeupIRQ;
}


/* ===========================================================================
**  HAL_mpmint_IsPending
**
** ======================================================================== */

bool32 HAL_mpmint_IsPending
(
  uint32 nMPM
)
{
  uint32 nIndex, nMask;
  uint32 nStatusReg, nEnableReg;
  bool32 bPending = FALSE;

  if(!HAL_mpmint_IsSupported(nMPM))
  {
    return bPending;
  }

  /* Offset of nMPM within two 4-byte registers (0 or 4) */
  nIndex     = MPMINT_2_REG_OFFSET(nMPM);

  nMask      = MAKE_MASK(nMPM);
  nStatusReg = registerLayout.nStatusReg + nIndex;
  nEnableReg = registerLayout.nEnableReg + nIndex;

  if (inpdw(nStatusReg) & inpdw(nEnableReg) & nMask)
  {
    bPending = TRUE;
  }

  /* Return if we are set */
  return bPending;

} /* END HAL_mpmint_IsPending */


/* ===========================================================================
**  HAL_mpmint_IsEnabled
**
** ======================================================================== */

bool32 HAL_mpmint_IsEnabled
(
  uint32 nMPM
)
{
  uint32 nIndex, nMask, nReg;
  bool32 bEnabled = FALSE;

  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    return bEnabled;
  }

  /* Determine position of nMPM within 64 bit register length */
  nIndex = MPMINT_2_REG_OFFSET(nMPM);

  nMask = MAKE_MASK(nMPM);
  nReg  = registerLayout.nEnableReg + nIndex;

  if (inpdw(nReg) & nMask)
  {
    bEnabled = TRUE;
  }

  /* Return if we are set */
  return bEnabled;

} /* END HAL_mpmint_IsEnabled */


/* ===========================================================================
**  HAL_mpmint_IsClearBefore
**
** ======================================================================== */

bool32 HAL_mpmint_IsClearBefore
(
  uint32 nMPM
)
{   
  if( !HAL_mpmint_IsSupported(nMPM) )
  {
    return FALSE;
  }

  /* Return TRUE if this interrupt clear before.  If not, then it is
   * assumed to be clear after. */
  if ( aInterruptTable[nMPM].eTrigger == HAL_MPMINT_TRIGGER_LOW ||
       aInterruptTable[nMPM].eTrigger == HAL_MPMINT_TRIGGER_HIGH )
  {
    return FALSE;
  }
  else
  {
    /* All edge detect interrupts are also CLEAR BEFORE */
    return TRUE;
  }
}


/* ========================================================================
**  HAL_mpmint_setup_interrupts
**
** ========================================================================*/

void HAL_mpmint_setup_interrupts  
( 
  HAL_mpmint_ConfigInfoType *intrs,
  uint32 intrs_count 
)
{
  uint32 iter, nMPM;
  HAL_mpmint_TriggerType eTrigger;
  HAL_mpmint_StatusType  eStatus;

  for( iter = 0; iter < intrs_count; iter++ )
  {
    nMPM     = intrs[iter].mpm_hw_int_id;
    eTrigger = intrs[iter].trigger;
    eStatus  = intrs[iter].status;

    /* Setting up the detection and polarity */
    HAL_mpmint_ConfigDetectionAndPolarity(nMPM, eTrigger);

    /* Updating interrupt status */
    HAL_mpmint_SetStatus(nMPM, eStatus);

  }

  /* Notifying RPM about this update */
  vmpm_send_interrupt();

}

/* ========================================================================
**  HAL_mpmint_InitializeRegisterLayout
**
**  This function must be called with appropriate address passed to it 
**  during early initialization (i.e. before configuring any interrupt/gpio).
** ========================================================================*/

void HAL_mpmint_InitializeRegisterLayout
(
  uint8 *msg_ram_base_ptr
)
{
  uint32 vmpm_base_addr = (uint32)msg_ram_base_ptr + 0x0;

  /* Offset is of 0x8 because each config register is a set of two 
   * 4-byte registers */
  registerLayout.WakeupReg     = vmpm_base_addr + 0x8 * 0;
  registerLayout.nEnableReg    = vmpm_base_addr + 0x8 * 1;
  registerLayout.nDetectReg    = vmpm_base_addr + 0x8 * 2;
  registerLayout.nDetect3Reg   = vmpm_base_addr + 0x8 * 3;
  registerLayout.nPolarityReg  = vmpm_base_addr + 0x8 * 4;
  registerLayout.nStatusReg    = vmpm_base_addr + 0x8 * 5;
  registerLayout.nEnableRegPad = 0;
  registerLayout.nClearReg     = 0;
}

