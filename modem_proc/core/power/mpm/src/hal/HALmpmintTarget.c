/*===========================================================================
  FILE:         HALmpmint_target_data.c
  
  OVERVIEW:     This file contains the function that obtains target specific
                data for mpm HAL level and assigns them to various structures/
                variables so that HAL level functions can use them.

  DEPENDENCY:   DevCfg support. If not available, we may need other 
                implementation of function that initializes target specific
                data.

                Copyright (c) 2012-2013 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
===========================================================================*/

#include "CoreVerify.h"
#include "DalDevice.h"            /* Header files for DevCfg queries */
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "HALmpmint.h"            /* MPM HAL level files */
#include "HALmpmintInternal.h"

/*==========================================================================
 *                      GLOBAL VARIABLES DECLARATIONS
 *==========================================================================*/

/* Pointer to HAL level mpm interrupt configuration table. */
HAL_mpmint_PlatformIntType *aInterruptTable = NULL;

/* Size of the above table */
uint32 HAL_MPMINT_NUM = 0;

/*==========================================================================
 *                      GLOBAL FUNCTIONS
 *==========================================================================*/

/* 
 * Obtains target specific data and use them to intialize various variables.
 */
void HAL_mpmint_InitTargetData( void )
{
  DALSYS_PROPERTY_HANDLE_DECLARE(hMpmHalDevCfg);
  DALSYSPropertyVar uProp;
  uint32 iter = 0;
  HAL_mpmint_PlatformIntType *pTableEle;

  /* Obtaining a DAL handle for our driver */ 
  CORE_DAL_VERIFY( 
    DALSYS_GetDALPropertyHandleStr("/dev/core/power/mpm", hMpmHalDevCfg)
  );

  /* Querying HAL interrupt configuration table */
  CORE_DAL_VERIFY( 
    DALSYS_GetPropertyValue( hMpmHalDevCfg, "mpmint_hal_cfg_table", 0, &uProp )
  );
  aInterruptTable = (HAL_mpmint_PlatformIntType *)uProp.Val.pStruct;

  /* Calculating size of the above table */
  for( iter = 0; ; iter++ )
  {
    pTableEle = &(aInterruptTable[iter]);
    if( HAL_MPMINT_TRIGGER_INVALID == pTableEle->eTrigger &&
        HAL_MPMINT_INVALID_GPIO == pTableEle->gpio &&
        HAL_MPMINT_GIC_IRQ_NONE == pTableEle->gic_irq )
    {
      /* We have reached end of table marked by special values */
      break;
    }
  }
  HAL_MPMINT_NUM = iter;

  return ;
}
