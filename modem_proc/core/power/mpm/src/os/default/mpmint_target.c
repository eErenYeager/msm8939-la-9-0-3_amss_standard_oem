/*===========================================================================
  FILE:         mpmint_target_data.c
  
  OVERVIEW:     This file contains the function that obtains target specific
                data for mpm driver and assigns them to various structures/
                variables so that common driver can use them.

  DEPENDENCY:   DevCfg support. If not available, we may need other 
                implementation of function that initializes target specific
                data.

                Copyright (c) 2012-2014 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
===========================================================================*/

#include "CoreVerify.h"
#include "DalDevice.h"            /* Header files for DevCfg queries */
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DDIHWIO.h"              /* HWIO mapping */
#include "mpmint.h"               /* MPM driver header files */
#include "mpminti.h"
#include "msmhwiobase.h"
#include "mpm_src_util.h"

/*==========================================================================
 *                      STATIC VARIABLES DECLARATIONS
 *==========================================================================*/
/* After initialization, this variable will be contain the Virtual Address
 * in message ram for master's mpm configuration registers base.
 * It is this variable that is updated or accessed by the mutator & accessor 
 * functions. */ 
static uint32 s_mpm_cfg_base_addr_va = 0;
/*==========================================================================
 *                      GLOBAL VARIABLES DECLARATIONS
 *==========================================================================*/

/* After initialization, this variable will contain the base address (virtual
 * or physical) of mpm hw registers in memory. */
uint8* mpm_hw_base_addr = NULL;

/* Pointer to mpm<->master interrupt mapping table */
mpmint_irq_data_type *mpmint_isr_tbl = NULL;

/* Size of the above table */
uint32 mpmint_isr_tbl_size = 0;

/* Virtual Address in message ram for master's mpm configuration registers base. */
uint32 mpmint_irq_cfg_reg_base_addr = 0;

/* RPM wakeup interrupt id */
uint32 mpmint_rpm_wakeup_irq = 0;

/* RPM wakeup interrupt flag */
uint32 mpmint_rpm_wakeup_irq_flags = 0;

/*==========================================================================
 *                      STATIC FUNCTIONS
 *==========================================================================*/
static void mpm_find_proc_va_from_pa( uint32 proc_start_pa );
static void mpm_set_proc_mpm_base_va( uint32 proc_mpm_base_va );

/* Mutator function for static variable s_mpm_cfg_base_addr_va */
static void mpm_set_proc_mpm_base_va( uint32 proc_mpm_base_va )
{
   s_mpm_cfg_base_addr_va =  proc_mpm_base_va;
}

/* Accessor function for static variable s_mpm_cfg_base_addr_va */
uint32 mpm_get_proc_mpm_base_va( void )
{
   return s_mpm_cfg_base_addr_va;
}
/*==========================================================================
 *                      GLOBAL FUNCTIONS
 *==========================================================================*/

/*
 * Obtains target specific data and use them to intialize various variables.
 */
void mpmint_target_data_init( void )
{
  DALSYS_PROPERTY_HANDLE_DECLARE(mpm_devcfg_handle);
  DALSYSPropertyVar prop;
  uint32 iter = 0;
  /* Physical Address in message RAM for processor's MPM config register base */
  uint32 mpm_proc_region_start_pa = 0x0;

  /* Obtaining a DAL handle for our driver */
  CORE_DAL_VERIFY( DALSYS_GetDALPropertyHandleStr( "/dev/core/power/mpm", 
                                                   mpm_devcfg_handle ) );

  /* mpm interrupt mapping table */
  CORE_DAL_VERIFY( DALSYS_GetPropertyValue( mpm_devcfg_handle, 
                                            "mpmint_isr_mapping_table", 
                                            0, &prop ) );
  mpmint_isr_tbl = (mpmint_irq_data_type *)prop.Val.pStruct;

  /* Calculate actual table size. It can be at most MPMINT_NUM_INTS */
  for(iter = 0; iter < MPMINT_NUM_INTS ; iter++)
  {
    if( MPMINT_IMT_EOS_HAL_IRQ == mpmint_isr_tbl[iter].hal_irq &&
        MPMINT_IMT_EOS_LOCAL_IRQ == mpmint_isr_tbl[iter].local_irq )
    {
      /* We have reached end of mapping table marked by special values */
      break;
    }
  }
  mpmint_isr_tbl_size = iter;

  /* Get the Physical Address (PA) of the processors MPM region in RPM RAM */
  CORE_DAL_VERIFY( DALSYS_GetPropertyValue( mpm_devcfg_handle, 
                                            "mpmint_proc_region_start_pa", 
                                            0, &prop ) );
  mpm_proc_region_start_pa = (uint32)prop.Val.dwVal;

  /* Determine the processors MPM RAM Virtual Address (VA) from PA */
  mpm_find_proc_va_from_pa( mpm_proc_region_start_pa );

  mpmint_irq_cfg_reg_base_addr = mpm_get_proc_mpm_base_va();

  /* Getting rpm wakeup interrupt id */
  CORE_DAL_VERIFY( DALSYS_GetPropertyValue( mpm_devcfg_handle, 
                                            "mpmint_rpm_wakeup_irq", 
                                            0, &prop ) );
  mpmint_rpm_wakeup_irq = (uint32)prop.Val.dwVal;

  /* Getting rpm wakeup interrupt trigger type */
  CORE_DAL_VERIFY( DALSYS_GetPropertyValue( mpm_devcfg_handle, 
                                            "mpmint_rpm_wakeup_irq_flags", 
                                            0, &prop ) );
  mpmint_rpm_wakeup_irq_flags = (uint32)prop.Val.dwVal;
}

/**
 * Maps the MPM HW to memory and sets the global variable mpm_hw_base_addr to
 * contain that virtual address.
 */
void mpm_map_hw_reg_base( void )
{
  DalDeviceHandle *mpm_hw_dal_handle;

  CORE_DAL_VERIFY( DAL_DeviceAttach( DALDEVICEID_HWIO, &mpm_hw_dal_handle ) );
  CORE_DAL_VERIFY( DalHWIO_MapRegion( mpm_hw_dal_handle, 
                                      "MPM2_MPM", &mpm_hw_base_addr ) );
}

/**
 * Calculate the Processor's MPM RAM Virtual Address from the 
 * corresponding Physical Address.  
 */
static void mpm_find_proc_va_from_pa( uint32 mpm_proc_region_start_pa )
{
  uint32 RPM_RAM_start_pa = RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS;
  uint32 RPM_RAM_end_va = 0;
  uint32 RPM_RAM_base_size = RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE;
  uint32 rpm_proc_mpm_pa_offset;
  DalDeviceHandle *mpm_rpm_ram_dal_handle;
  uint32 RPM_RAM_end_pa = UINT32_MAX;

  /* After initialization, this variable will contain the Virtual Address 
   * corresponding to the RPM's shared RAM Base Physical Address */
  uint32 rpm_ram_base_va = 0x0;

  /* Calculate the RPM RAM Physical Addr upper boundary */
  if ( (RPM_RAM_start_pa + RPM_RAM_base_size) < RPM_RAM_start_pa )
  {
    CORE_VERIFY(0);
  }
  else
  {
    RPM_RAM_end_pa = RPM_RAM_start_pa + RPM_RAM_base_size;
  }

  /* Calculate the physical address offset of master MPM Region
   * relative to the RPM RAM start address */
  rpm_proc_mpm_pa_offset = 
               safe_uint32_subtract( mpm_proc_region_start_pa,
                                     RPM_RAM_start_pa );
  /* Determine the RPM RAM Base adddress virtual address */
  CORE_DAL_VERIFY( DAL_DeviceAttach( DALDEVICEID_HWIO, &mpm_rpm_ram_dal_handle ) );
  CORE_DAL_VERIFY( DalHWIO_MapRegion( mpm_rpm_ram_dal_handle, 
                                      "RPM_SS_MSG_RAM_START_ADDRESS", 
                                      (uint8 **) &rpm_ram_base_va ) );

  /* Add Processor's address offset to the RPM RAM base address
   * to get the processor's MPM virtual addr (VA) region start */
  uint32 proc_mpm_base_va = safe_uint32_addition( rpm_proc_mpm_pa_offset, 
                                                  rpm_ram_base_va);

  /* Calculate the RPM RAM Virtual Addr upper boundary */
  RPM_RAM_end_va = safe_uint32_addition( rpm_ram_base_va,
                                         RPM_RAM_base_size);

  /* Perform bounds check on result.  If processor's virtual address
   * is outside RPM RAM virtual address range,
   * log an error and halt processor */ 
  if ( proc_mpm_base_va <= RPM_RAM_end_va ) 
  {
    mpm_set_proc_mpm_base_va( proc_mpm_base_va );
    mpmint_log_printf( 1,
                      "MPM_SUCCESS: RAM Address within expected addr range: %x",
                      proc_mpm_base_va );
  }
  else
  {
    mpmint_log_printf( 1,
                      "MPM_FAIL: RAM Address out of range error: %x",
                      proc_mpm_base_va );
    CORE_VERIFY(0);
  }
}
