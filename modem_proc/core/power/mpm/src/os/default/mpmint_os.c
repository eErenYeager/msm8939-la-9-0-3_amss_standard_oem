/*==========================================================================
 
  FILE:       mpmint_internal.c

  OVERVIEW:   This file implements default OS specific functions of MPM driver
              and is expected to be used within MPM driver only.

              Copyright (c) 2011-2013 QUALCOMM Technologies Incorporated
              All Rights Reserved.
              Qualcomm confidential and Proprietary
===========================================================================*/
/*==========================================================================
$Header: //components/rel/core.mpss/3.7.24/power/mpm/src/os/default/mpmint_os.c#1 $
$DateTime: 2015/01/27 06:04:57 $
$Author: mplp4svc $
===========================================================================*/
#include "mpmint.h"                     /* MPM related files */
#include "mpminti.h"
#include "mpmint_target.h"
#include "DDIInterruptController.h"     /* IC and GPIO related files */
#include "DDITlmm.h"
#include "DDIGPIOInt.h"
#include "DalDevice.h"
#include "DALDeviceId.h"
#include "DALStdErr.h"                  /* General DAL files */
#include "DALSys.h"

#include "CoreVerify.h"


/*=========================================================================
 *                   INTERNAL DEFINITIONS & DECLARATIONS
 *========================================================================*/

/**
 * Handle for DAL based Interrupt Controller. This will be used for mapping,
 * registering and triggering interrupts.
 */
static DalDeviceHandle *mpm_dal_ic_handle = NULL;

/**
 * Handle for DAL based GPIO controller. It will be used for mapping and 
 * triggering GPIOs.
 */
static DalDeviceHandle *mpm_dal_gpio_handle = NULL;

/**
 * ISR for rpm to master mpm wakeup interrupt. This function is not doing
 * anything as of now as we trigger interrupts and gpios occured during sleep
 * while coming out of xo shutdown
 */
static void* rpm_to_master_isr( DALISRCtx isr_context )
{
  return NULL;
}

/*=========================================================================
 *                   GLOBAL DEFINITIONS & DECLARATIONS
 *========================================================================*/

/**
 * Number of interrupts master wants MPM to monitor during deep sleep.
 */
uint32 mpmint_num_mapped_irqs = 0;

/**
 * DAL lock handle for the shared message RAM area. This lock must be
 * acquired whenever Message RAM is being modified.
 */
DALSYSSyncHandle mpm_shared_msg_ram_lock;

/*
 * mpmint_init
 */
void mpmint_init( void )
{
  uint32 mpm_num;
  uint32 gpio_num;

  /* Initializing MPM logging */
  mpmint_log_init();

  /* Obtain target specific data */
  mpmint_target_data_init();

  /* Get the MPM interrupt controller hardware in a good state. */
  HAL_mpmint_Init(NULL, (uint8 *)mpmint_irq_cfg_reg_base_addr, TRUE);

  /* Creating a lock for Shared Message RAM */
  CORE_DAL_VERIFY( DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE_INTERRUPT,
                                     &mpm_shared_msg_ram_lock, NULL) );

  /* Obtaining DAL handle for Interrupt controller */
  CORE_LOG_DAL_VERIFY(
    DAL_DeviceAttach( DALDEVICEID_INTERRUPTCONTROLLER, &mpm_dal_ic_handle ),
    mpmint_log_printf( 0, "ERROR: Attaching to Interrupt Controller failed" ) 
  );

  /* Obtaining DAL handle for GPIO controller */
  CORE_LOG_DAL_VERIFY(
    DAL_DeviceAttach( DALDEVICEID_GPIOINT, &mpm_dal_gpio_handle ),
    mpmint_log_printf( 0, "ERROR: Attaching to GPIO Controller failed" ) 
  );

  /* Inform the local interrupt controller and GPIO controller of the wakeup 
   * interrupts. Those subsystems will let us know when any of these 
   * interrupts gets enabled, disabled or reconfigured. Also, update the
   * HAL level interrupt config table to have mapping of wakeup irqs. */
  for( mpm_num = 0; mpm_num < mpmint_isr_tbl_size; mpm_num++ )
  {
    if( MPMINT_IMT_UNMAPPED_HAL_IRQ == mpmint_isr_tbl[mpm_num].hal_irq )
    {
      /* Unmapped interrupt/gpio. Skip this table entry */
      continue;
    }

    if( HAL_mpmint_IsGpio( mpmint_isr_tbl[mpm_num].hal_irq, &gpio_num ) )
    {
      /* Inform about the GPIO wakeup. */
      if( DAL_SUCCESS != 
          GPIOInt_MapMPMInterrupt( mpm_dal_gpio_handle, gpio_num, mpm_num ) )
      {
        mpmint_log_printf( 4, "ERROR: MPM GPIO mapping failed (Handle: 0x%x) "
                           "(Index: %d) (GPIO: %u) (MPM ID: %u)", 
                           mpm_dal_gpio_handle, mpm_num, gpio_num, 
                           mpmint_isr_tbl[mpm_num].hal_irq );
        CORE_VERIFY(0);
      }
    }
    else if( MPMINT_NULL_IRQ != mpmint_isr_tbl[mpm_num].local_irq )
    {
      /* Inform about the IRQ wakeup. */
      if( DAL_SUCCESS != 
          DalInterruptController_MapWakeupInterrupt( mpm_dal_ic_handle, 
             (DALInterruptID)mpmint_isr_tbl[mpm_num].local_irq, mpm_num ) )
      {
        mpmint_log_printf( 4, "ERROR: MPM interrupt mapping failed "
                           "(Handle: 0x%x) (Index: %d) (IRQ: %u) (MPM ID: %u)",
                           mpm_dal_ic_handle, mpm_num, 
                           mpmint_isr_tbl[mpm_num].local_irq, 
                           mpmint_isr_tbl[mpm_num].hal_irq );
        CORE_VERIFY(0);
      }

      /* Updating HAL config table to speed up reverse look up */
      HAL_mpmint_MapWakeupIRQ( mpmint_isr_tbl[mpm_num].hal_irq,
                               mpmint_isr_tbl[mpm_num].local_irq );

      mpmint_num_mapped_irqs++;
    }
  }

  /* Register with TRAMP for the wakeup interrupt from the RPM. */
  CORE_LOG_DAL_VERIFY( 
    DalInterruptController_RegisterISR( mpm_dal_ic_handle, 
                                        mpmint_rpm_wakeup_irq, 
                                        rpm_to_master_isr, 
                                        (DALISRCtx)0, 
                                        mpmint_rpm_wakeup_irq_flags ),
    mpmint_log_printf( 0, "ERROR: RPM wakeup interrupt registration failed" ) 
  );
}

/*
 * mpmint_retrigger_gpio
 */
void mpmint_retrigger_gpio( uint32 gpio_num )
{
  /* Triggering GPIO on the master */
  if( DAL_SUCCESS != GPIOInt_TriggerInterrupt( mpm_dal_gpio_handle, gpio_num ) )
  {
    mpmint_log_printf( 1, "ERROR: MPM GPIO trigger failed (GPIO: %u)", 
                       gpio_num );
    CORE_VERIFY(0);
  }

  return;
}

/*
 * mpmint_retrigger_interrupt
 */
void mpmint_retrigger_interrupt( uint32 irq_num )
{
  /* Triggering interrupt on the master */
  if( DAL_SUCCESS != DalInterruptController_InterruptTrigger(  
                         mpm_dal_ic_handle, (DALInterruptID)irq_num ) )
  {
    mpmint_log_printf( 1, "ERROR: MPM interrupt trigger failed (IRQ: %u)", 
                       irq_num );
    CORE_VERIFY(0);
  }

  return;
}
