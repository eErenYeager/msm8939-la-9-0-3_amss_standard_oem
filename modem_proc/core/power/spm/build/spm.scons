#===============================================================================
#
# SPM Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2013 by QUALCOMM Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/power/spm/rel/1h10/build/SConscript#7 $
#  $DateTime: 2011/08/30 17:22:20 $
#  $Author: tulmer $
#  $Change: 1908500 $
#===============================================================================
Import('env')
env = env.Clone()

#===== CCFLAGS - add the flags to the C compiler command line.
# Note: If SPM_BASE_NAME changes from one chip to other, it may break binary
#       compatibility. If that happens, switch to variable instead of macro.
env.Append(CPPDEFINES = ['-DSPM_HWIO_PREFIX=QDSP6SS_SAW2',
                         '-DSPM_BASE_NAME=MSS_TOP'
                         ])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/power/spm"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------
env.RequireExternalApi([
  'CS',
   'MODEM_PMIC',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'DEBUGTOOLS',
   'HAL',
   'POWER',
   'SERVICES',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.PublishPrivateApi('POWER_SPM', [
   "${BUILD_ROOT}/core/power/spm/inc"
])

env.PublishPrivateApi('POWER_HAL_SPM', [
   "${INC_ROOT}/core/power/spm/src/hal",
])

if env['MSM_ID'] in ['8916']:
  env.PublishPrivateApi('POWER_HAL_SPM', [
     "${INC_ROOT}/core/power/spm/src/hal/hwio/8916", 
  ])
elif env['MSM_ID'] in ['8926']:
    env.PublishPrivateApi('POWER_HAL_SPM', [
     "${INC_ROOT}/core/power/spm/src/hal/hwio/8926", 
  ])
elif env['MSM_ID'] in ['8936']:
    env.PublishPrivateApi('POWER_HAL_SPM', [
     "${INC_ROOT}/core/power/spm/src/hal/hwio/8936", 
  ])

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------


SPM_SOURCES =  [
   '${BUILDPATH}/src/spm.c'
]

SPM_HAL_SOURCES =  [
   '${BUILDPATH}/src/hal/BSPspm.c',
   '${BUILDPATH}/src/hal/HALspmPlatform.c',
   '${BUILDPATH}/src/hal/HALspmmmap.c',
   '${BUILDPATH}/config/asic/8916/SPMDevCfgData.c',
]

#------------------------------------------------------------------------------
# Adding device config data
#------------------------------------------------------------------------------
if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   # Select appropriate xml config file based on target. May not change on
   # single core targets
   env.AddDevCfgInfo(
      DEVCFG_IMG,
      {
         '8916_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/8916/SPMDevCfgData.c'],
         '8936_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/8936/SPMDevCfgData.c'],
         '8974_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/8974/SPMDevCfgData.c'],
         '8962_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/89xx/SPMDevCfgData.c'],
         '8926_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/89xx/SPMDevCfgData.c'],
         '9625_xml' : ['${BUILD_ROOT}/core/power/spm/config/SPMDevCfg.xml',
                       '${BUILD_ROOT}/core/power/spm/config/asic/9x25/SPMDevCfgData.c'],					   
      }
   )

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(['MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
                    'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE'],
                    '${BUILDPATH}/spm',
                    SPM_SOURCES + SPM_HAL_SOURCES)
