/*============================================================================
  FILE:         system_db_modem.c
 
  OVERVIEW:     Implement calls to RPM side system_DB 
 
  DEPENDENCIES: None
 
                Copyright (c) 2012 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

$Header: //components/rel/core.mpss/3.7.24/power/system_db/src/system_db_modem.c#1 $
$Date: 2015/01/27 $
============================================================================*/

#include "system_db.h"
#include "system_db_modem.h"
#include "smd_lite.h"
#include "rpm.h"
#include "rpm_wire.h"
#include "DALSys.h"
#include "DALStdDef.h"

#define MSG__KEY 0x2367736d

void RPM_system_db_init( void )
{
  // placeholder in case we need an init function at a later date.
}

void RPM_system_db_read( SYSTEMDB_EVENT_TYPE	event_type,
                         uint32 start_addr, 
                         unsigned int num, 
                         uint32 command_id,
                         SYSTEMDB_ADDR_TYPE addr_type )
{
  struct system_db_read_msg_data
  {
    uint32 type;        /* 16 MSB = command_id
                         * next 1b = read/write
                         * next 4b = address type
                         * last 11b = event type
                         */
    SYSTEMDB_READ read;
  } read_data;
  //Need an RPMMessageHeader b/c it is checked on the RPM
  RPMMessageHeader header;
  smdl_iovec_type header_dat, system_db_read_msg;
  //set the data;
  memset(&header, 0, sizeof(RPMMessageHeader));
  header.msg_id = MSG__KEY;
  header.data_len = sizeof(struct system_db_read_msg_data);
  memset(&read_data, 0, sizeof(struct system_db_read_msg_data));
  SET_OP_TYPE_COMMAND_ID( read_data.type, command_id );
  SET_OP_TYPE_RW( read_data.type, SYSTEMDB_OP_READ );
  SET_OP_TYPE_ADDR_TYPE( read_data.type, addr_type );
  SET_OP_TYPE_EVENT_TYPE( read_data.type, event_type );
  read_data.read.addr = start_addr;
  read_data.read.num = num;
  //set up the smdl data to have the correct size and data ptrs
  system_db_read_msg.next = NULL;
  system_db_read_msg.length = sizeof(struct system_db_read_msg_data);
  system_db_read_msg.buffer = &read_data;
  header_dat.next = &system_db_read_msg;
  header_dat.length = sizeof(header);
  header_dat.buffer = &header;
  //send the message
  rpmclient_put( RPM_SYSTEMDB_SERVICE, &header_dat );
}


void RPM_system_db_write( SYSTEMDB_EVENT_TYPE	event_type,
                          uint32 start_addr, 
                          unsigned int num, 
                          uint32 data, 
                          uint32 mask, 
                          uint32 command_id,
                          SYSTEMDB_ADDR_TYPE addr_type )
{
  struct system_db_write_msg_data
  {
    uint32 type;        /* 16 MSB = command_id
                         * next 1b = read/write
                         * next 4b = address type
                         * last 11b = event type
                         */
    SYSTEMDB_WRITE write;
  } write_data;
  //Need an RPMMessageHeader b/c it is checked on the RPM
  RPMMessageHeader header;
  smdl_iovec_type header_dat, system_db_write_msg;
  //set the data;
  memset(&header, 0, sizeof(RPMMessageHeader));
  header.msg_id = MSG__KEY;
  header.data_len = sizeof(struct system_db_write_msg_data);
  memset(&write_data, 0, sizeof(struct system_db_write_msg_data));
  SET_OP_TYPE_COMMAND_ID( write_data.type, command_id );
  SET_OP_TYPE_RW( write_data.type, SYSTEMDB_OP_WRITE );
  SET_OP_TYPE_ADDR_TYPE( write_data.type, addr_type );
  SET_OP_TYPE_EVENT_TYPE( write_data.type, event_type );
  write_data.write.addr = start_addr;
  write_data.write.num = num;
  write_data.write.data = data;
  write_data.write.mask = mask;
  //set up the smdl data to have the correct size and data ptrs
  system_db_write_msg.next = NULL;
  system_db_write_msg.length = sizeof(struct system_db_write_msg_data);
  system_db_write_msg.buffer = &write_data;
  header_dat.next = &system_db_write_msg;
  header_dat.length = sizeof(header);
  header_dat.buffer = &header;
  //send the message
  rpmclient_put( RPM_SYSTEMDB_SERVICE, &header_dat );
}

/* Clears all operations from the system DB queue */
void RPM_system_db_clearall( void )
{
  // rpmclient_put data struct
  struct system_db_clearall_msg_data
  {
    uint32 type;        /* Will fill with 0xFFFFFFFF sentinel val */
  } clearall_data;
  //Need an RPMMessageHeader b/c it is checked on the RPM
  RPMMessageHeader header;
  smdl_iovec_type header_dat, system_db_clearall_msg;
  //set up the smdl data to have the correct size and data ptrs
  memset(&header, 0, sizeof(RPMMessageHeader));
  header.msg_id = MSG__KEY;
  header.data_len = sizeof(struct system_db_clearall_msg_data);
  memset(&clearall_data, 0, sizeof(struct system_db_clearall_msg_data));
  //set type to sentinel value
  clearall_data.type = 0xFFFFFFFF;
  //set up the smdl data to have the correct size and data ptrs
  system_db_clearall_msg.next = NULL;
  system_db_clearall_msg.length = sizeof(struct system_db_clearall_msg_data);
  system_db_clearall_msg.buffer = &clearall_data;
  header_dat.next = &system_db_clearall_msg;
  header_dat.length = sizeof(header);
  header_dat.buffer = &header;
  rpmclient_put( RPM_SYSTEMDB_SERVICE, &header_dat );
}
