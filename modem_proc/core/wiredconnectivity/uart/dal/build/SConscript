#===============================================================================
#
# DAL UART Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2014,2015 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.7.24/wiredconnectivity/uart/dal/build/SConscript#1 $
#  $DateTime: 2015/01/27 06:04:57 $
#  $Author: mplp4svc $
#  $Change: 7351156 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================

Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------

SRCPATH = "${BUILD_ROOT}/core/wiredconnectivity/uart/dal/src"

if not env.PathExists( SRCPATH ):
   SRCPATH = "${BUILD_ROOT}/drivers/uart"

if not env.PathExists( SRCPATH ):
   SRCPATH = "${BUILD_ROOT}/drivers/sio"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------

env.RequireExternalApi([
   'MODEM_PMIC',
   'MODEM_RF',
   'MODEM_SERVICES',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_APIS = [
   'DAL',
   'HAL',
   'BUSES',
   'HWENGINES',
   'SYSTEMDRIVERS',
   'SERVICES',
   'KERNEL',
   'WIREDCONNECTIVITY',
   'POWER',
]

env.RequirePublicApi( CBSP_APIS )
env.RequireRestrictedApi( CBSP_APIS )

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

DAL_UART_SOURCES = [
   '${BUILDPATH}/DalUart.c',
   '${BUILDPATH}/DalUartDma.c',
   '${BUILDPATH}/DalUartFwk.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

IMAGES = []

if env['MSM_ID'] in ['6615','6695']:
   IMAGES = ['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE']
elif env['MSM_ID'] in ['7x30','8660','8x60']:
   IMAGES = ['MODEM_IMAGE', 'CBSP_MODEM_IMAGE', 'APPS_IMAGE', 'CBSP_APPS_IMAGE']
else:
   #  Do this for all targets moving forward (8960,9x15,8974,etc.).  We don't want to add
   #  a line for each new target.
   IMAGES = ['MODEM_IMAGE', 'CBSP_MODEM_IMAGE', 'APPS_IMAGE', 'CBSP_APPS_IMAGE', 'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']

env.AddLibrary(IMAGES,'${BUILDPATH}/DALuart',DAL_UART_SOURCES)

if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG,
   {
            '8974_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8974.xml'],
            '8626_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8x26.xml'],
            '8926_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8926.xml'],
            '8962_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8962.xml'],
            '8610_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8x10.xml'],
            '9625_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_9x25.xml'],
            '8916_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8916.xml'],
            '8936_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8936.xml'],			
            '8929_xml' : ['${BUILD_ROOT}/core/wiredconnectivity/uart/config/uart_8929.xml'],
   })
