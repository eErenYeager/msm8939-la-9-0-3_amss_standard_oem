#===============================================================================
#
# UART driver target abstraction layer
#
# This build script is responsible for picking the correct target abstraction
# layer (TAL) implementation for a particular build, based on the requirements
# of each individual product line.
#
# Copyright (c) 2012-2013 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/bsp/config/scmm/main/latest/buses/ebi2/dal/build/SConscript#5 $
#
#===============================================================================

Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------

SRCPATH = "${BUILD_ROOT}/core/wiredconnectivity/uart/tal/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_APIS = [
   'DAL',
   'HAL',
   'BUSES',
   'HWENGINES',
   'SYSTEMDRIVERS',
   'SERVICES',
   'KERNEL',   
   'POWER',   
]

env.RequirePublicApi( CBSP_APIS )
env.RequireRestrictedApi( CBSP_APIS )

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

UART_TAL_SOURCES = [
   '${BUILDPATH}/tal_config.c',
   '${BUILDPATH}/tal_clock.c',
   '${BUILDPATH}/tal_tlmm.c',
   '${BUILDPATH}/tal_dma_bam.c',
]

if (env['PROC'] == 'qdsp6'):
   UART_TAL_SOURCES += [
      '${BUILDPATH}/qurt/tal_interrupt_qurt.c',
   ]
else:
   UART_TAL_SOURCES += [
      '${BUILDPATH}/tal_interrupt.c',
   ]


#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

IMAGES = ['MODEM_IMAGE', 'CBSP_MODEM_IMAGE', 'APPS_IMAGE', 'CBSP_APPS_IMAGE', 'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE']

env.AddLibrary( IMAGES,'${BUILDPATH}/uart_tal',UART_TAL_SOURCES)
