/*==================================================================================================

FILE: tal_dma_bam.c

DESCRIPTION: This module defines the stub implementation of the DMA APIs for
             the Target Abstraction Layer.  Note that the TAL DMA APIs are
             intended to be used with device->memory (RX) or memory->device
             (TX) transfers.  They do not support device->device or
             memory->memory transfers.

                           Copyright (c) 2012-2014 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header: //components/rel/core.mpss/3.7.24/wiredconnectivity/uart/tal/src/tal_dma_bam.c#1 $

==================================================================================================*/
/*==================================================================================================
                                            DESCRIPTION
====================================================================================================

GLOBAL FUNCTIONS:
   tal_dma_close
   tal_dma_open
   tal_dma_rx_cancel
   tal_dma_rx_transfer
   tal_dma_tx_cancel
   tal_dma_tx_transfer

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#if ( defined(_WIN32) && !defined(_WIN32_WCE) )
#include "dal.h"
#include "tal_dma_dmov.tmh"
#else
#include "DALDeviceId.h"
#include "DALFramework.h"
#endif

#include "tal.h"

/*==================================================================================================
                                          GLOBAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================

FUNCTION: tal_dma_close

DESCRIPTION:

==================================================================================================*/
void tal_dma_close(TAL_DMA_HANDLE handle)
{
}

/*==================================================================================================

FUNCTION: tal_dma_open

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_dma_open(TAL_DMA_HANDLE *phandle, TAL_DMA_CONFIG *config)
{
   return TAL_ERROR;
}

/*==================================================================================================

FUNCTION: tal_dma_rx_cancel

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_dma_rx_cancel(TAL_DMA_HANDLE handle)
{
   return TAL_ERROR;
}

/*==================================================================================================

FUNCTION: tal_dma_rx_transfer

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_dma_rx_transfer(TAL_DMA_HANDLE handle, TAL_DMA_DESCRIPTOR *descriptor)
{
   return TAL_ERROR;
}

/*==================================================================================================

FUNCTION: tal_dma_tx_cancel

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_dma_tx_cancel(TAL_DMA_HANDLE handle)
{
   return TAL_ERROR;
}

/*==================================================================================================

FUNCTION: tal_dma_tx_transfer

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_dma_tx_transfer(TAL_DMA_HANDLE handle, TAL_DMA_DESCRIPTOR *descriptor)
{
   return TAL_ERROR;
}
