/*==================================================================================================

FILE: tal_config.c

DESCRIPTION: This module defines the "stock" implementation of the 'config' APIs for the
             Target Abstraction Layer. 

                              Copyright (c) 2012, 2014,2015 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header:

==================================================================================================*/
/*==================================================================================================
                                            DESCRIPTION
====================================================================================================

GLOBAL FUNCTIONS:
   tal_config_close
   tal_config_open

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#if ( defined(_WIN32) && !defined(_WIN32_WCE) )
#include "dal.h"
#else
#include "comdef.h"
#include "DALDeviceId.h"
#include "DDIUart.h"
#include "DALSys.h"
#include "DDIPlatformInfo.h"
#endif

#include "tal.h"

/*==================================================================================================
                                             CONSTANTS
==================================================================================================*/

#define MIN_PAGE_SIZE         (4*1024)
#define UART_INT_SEL_MEM_SIZE MIN_PAGE_SIZE
#define UART_INT_SEL_MAX      3   // maximum number of UART int sel registers

/*==================================================================================================
                                              TYPEDEFS
==================================================================================================*/

typedef struct tal_config_context
{
   DALDEVICEID          client_id;

   // (from XML props) physical addresses of the UART int_sel registers 
   uint32               *uart_int_sel_physical;  

   // (from XML props) number of entries in previous array
   uint32               uart_int_sel_num; 

   // contains the virtual address mapping for UART int sel registers
   DALSYSMemHandle      uart_int_sel_mem_handle[UART_INT_SEL_MAX]; 

} TAL_CONFIG_CONTEXT;

typedef enum
{
   PROCESSOR_INVALID,
   PROCESSOR_LPASS,
   PROCESSOR_PRONTO,
   PROCESSOR_MODEM,
   PROCESSOR_APPS

}  UartProcessorType;

/*==================================================================================================
                                     LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

static boolean            configure_irq(TAL_CONFIG_HANDLE handle, boolean configure);
static UartProcessorType  get_processor(void);
static void               get_property_dword_seq(DALDEVICEID id, 
                                                 DALSYSPropertyHandle handle,
                                                 const char *property_name, 
                                                 uint32 **property_value, 
                                                 uint32 *num_entries );

/*==================================================================================================
                                          LOCAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: configure_irq

DESCRIPTION:
   This function routes the UART IRQ to the appropriate processor.
   It returns TRUE if successful, or FALSE otherwise

==================================================================================================*/
static boolean configure_irq(TAL_CONFIG_HANDLE handle, boolean configure)
{
   DALSYSMemInfo          meminfo;
   volatile uint32       *uart_int_sel;
   uint8                 *ptr;
   uint32                 val;
   UartProcessorType      processor;
   uint32                 blsp;
   uint32                 uart;
   uint32                 mask;
   DALSYSMemHandle       *mem_handle;
   uint32                 offset;
   DALDEVICEID            id = handle->client_id;
   int                    index;
   DalChipInfoFamilyType  chip_family;


   DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "+configure_irq()");

   //  Check if we need to route the UART IRQs.

   if (0 == handle->uart_int_sel_num)
   {
      DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "-configure_irq()");
      return TRUE;
   }
   
   //  Route the interrupt.  We only have to do this on Badger-family chips.
   chip_family = DalChipInfo_ChipFamily();

   DalChipInfoVersionType chip_version;

   DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "Routing UART IRQ");

   //  Figure out which processor we're running on.

   processor = get_processor();

   chip_version = DalChipInfo_ChipVersion();

   //  HW bug:  the modem IRQs were not routed on v1 at all.  At the last minute,
   //  they hijacked the pronto IRQs instead.  This will be fixed in v2.

   if (chip_family == DALCHIPINFO_FAMILY_MSM8974 &&
       chip_version < DALCHIPINFO_VERSION(2,0))
   {
      if (PROCESSOR_PRONTO == processor)
      {
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "Pronto not supported on this HW");
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "-configure_irq()");
         return FALSE;
      }
      else if (PROCESSOR_MODEM == processor)
      {
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "Using Pronto IRQ lines for modem");
         processor = PROCESSOR_PRONTO;
      }
   }

   //  Get a pointer to the register.  This is harder than it looks.  The 
   //  DALSYS malloced region will be on a MIN_PAGE_SIZE boundary (which may
   //  be OS/target/platform specific).  The actual physical address in the 
   //  meminfo will be adjusted downward.  The virtual address must be adjusted 
   //  by this same size;

   index = processor - 1;
   mem_handle = &handle->uart_int_sel_mem_handle[index]; 
   DALSYS_MemInfo(*mem_handle, &meminfo);

   ptr = (uint8*) meminfo.VirtualAddr;
   offset = handle->uart_int_sel_physical[index] - meminfo.PhysicalAddr;
   ptr += offset;
   uart_int_sel = (uint32*) ptr;

   //  Figure out which bit to twiddle in the UART_INT_SEL register
   //  to turn on/off the IRQ for this device.

   switch (id)
   {
      case DALDEVICEID_UARTBAM_DEVICE_1:  blsp = 0; uart = 0; break;
      case DALDEVICEID_UARTBAM_DEVICE_2:  blsp = 0; uart = 1; break;
      case DALDEVICEID_UARTBAM_DEVICE_3:  blsp = 0; uart = 2; break;
      case DALDEVICEID_UARTBAM_DEVICE_4:  blsp = 0; uart = 3; break;
      case DALDEVICEID_UARTBAM_DEVICE_5:  blsp = 0; uart = 4; break;
      case DALDEVICEID_UARTBAM_DEVICE_6:  blsp = 0; uart = 5; break;

      case DALDEVICEID_UARTBAM_DEVICE_7:  blsp = 1; uart = 0; break;
      case DALDEVICEID_UARTBAM_DEVICE_8:  blsp = 1; uart = 1; break;
      case DALDEVICEID_UARTBAM_DEVICE_9:  blsp = 1; uart = 2; break;
      case DALDEVICEID_UARTBAM_DEVICE_10: blsp = 1; uart = 3; break;
      case DALDEVICEID_UARTBAM_DEVICE_11: blsp = 1; uart = 4; break;
      case DALDEVICEID_UARTBAM_DEVICE_12: blsp = 1; uart = 5; break;

      case DALDEVICEID_UARTCXM:
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO,
            "-configure_irq(): Mux not necessary for CXM");
         return TRUE;

      default:
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO,
            "Device 0x%x not recogonized", id);
         return FALSE;
   }

   mask = ( 1 << ((6 * blsp) + uart));

   //  Twiddle the bit.

   val = *uart_int_sel;

   if (configure)
   {
      DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "Writing 0x%x to 0x%08x",
         mask, meminfo.PhysicalAddr);
      val |= mask;
   }
   else
   {
      DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "Clearing 0x%x from 0x%08x",
         mask, meminfo.PhysicalAddr);
      val &= ~mask;
   }

   *uart_int_sel = val;

   DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "-configure_irq()");

   return TRUE;
}

/*==================================================================================================

FUNCTION: get_processor

DESCRIPTION:
   Returns the processor that we're running on.

==================================================================================================*/

static UartProcessorType get_processor(void)
{
   //  The build system has the knowledge of what processor has been built.
   //  It sets UART_PROCESSOR and passes it in via the preprocessor command line.
   //  It would be nice if there was some DAL functionality (like DalPlatformInfo)
   //  that could provide this information.

   return UART_PROCESSOR;
}

/*==================================================================================================

FUNCTION: get_property_dword_seq

DESCRIPTION:
   Retrieve a dword (32-bit integer) sequence property.

==================================================================================================*/
static void get_property_dword_seq(DALDEVICEID id, 
                                   DALSYSPropertyHandle property_handle,
                                   const char *property_name, 
                                   uint32 **property_value, 
                                   uint32 *num_entries )
{
   DALSYSPropertyVar property_variable;
   DALResult result;
   int i;


   result = DALSYS_GetPropertyValue(property_handle, property_name, 0, &property_variable);

   if (result == DAL_SUCCESS)
   {
      *property_value = property_variable.Val.pdwVal;
      *num_entries    = property_variable.dwLen;
   }
   else
   {
      *property_value = NULL;
      *num_entries    = 0;
   }

   if (*num_entries > 0)
   {
      DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "get_property_dword_seq: %s", property_name);
      for (i = 0; i < *num_entries; i++)
      {
         DALSYS_LogEvent(id, DALSYS_LOGEVENT_INFO, "  [%d] = 0x%08X", i, (*property_value)[i]);
      }
   }
}

/*==================================================================================================
                                          GLOBAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: tal_config_close

DESCRIPTION:

==================================================================================================*/
void tal_config_close(TAL_CONFIG_HANDLE handle)
{
   int i;

   configure_irq(handle, FALSE);

   for (i = 0; i < handle->uart_int_sel_num; i++)
   {
      if (handle->uart_int_sel_mem_handle[i])
      {
         DALSYS_DestroyObject(handle->uart_int_sel_mem_handle[i]);
      }
   }
   DALSYS_Free(handle);
}

/*==================================================================================================

FUNCTION: tal_config_open

DESCRIPTION:

==================================================================================================*/
TAL_RESULT tal_config_open(TAL_CONFIG_HANDLE *phandle, uint32 client_id)
{
   TAL_CONFIG_CONTEXT *tal_config_ctxt = NULL;
   DALSYS_PROPERTY_HANDLE_DECLARE(property_handle);
   DALResult result;
   uint32 *uart_int_sel_physical;
   uint32 uart_int_sel_num = 0;
   DALSYSMemHandle uart_int_sel_mem_handle[UART_INT_SEL_MAX];
   int i;

   for (i = 0;i<UART_INT_SEL_MAX;i++)
   {
      uart_int_sel_mem_handle[i] = 0;
   }

   result = DALSYS_Malloc(sizeof(TAL_CONFIG_CONTEXT), (void **)(&tal_config_ctxt));
   if (result != DAL_SUCCESS) 
   { 
      goto error;
   }

   result = DALSYS_GetDALPropertyHandle(client_id, property_handle);
   if (result != DAL_SUCCESS) { goto error; }

   get_property_dword_seq(client_id, property_handle, "UartIntSelBase", 
                          &uart_int_sel_physical, 
                          &uart_int_sel_num );

   if (uart_int_sel_num > UART_INT_SEL_MAX ) { goto error; }

   //  Map in UART int sel registers.

   for (i = 0; i < uart_int_sel_num; i++)
   {
      result = DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_HWIO,
                                     DALSYS_MEM_ADDR_NOT_SPECIFIED, 
                                     uart_int_sel_physical[i],
                                     UART_INT_SEL_MEM_SIZE, 
                                     &uart_int_sel_mem_handle[i], 
                                     NULL);

      if (result != DAL_SUCCESS) { goto error; }
   }

   tal_config_ctxt->client_id   = client_id;
   tal_config_ctxt->uart_int_sel_num = uart_int_sel_num;
   tal_config_ctxt->uart_int_sel_physical = uart_int_sel_physical;
   for (i = 0; i < uart_int_sel_num; i++)
   {
      tal_config_ctxt->uart_int_sel_mem_handle[i] = uart_int_sel_mem_handle[i];
   }

   if (configure_irq(tal_config_ctxt, TRUE) != TRUE)
   {
      goto error;
   }

   *phandle = tal_config_ctxt;
   return TAL_SUCCESS;

error:
   for (i = 0; i < uart_int_sel_num; i++)
   {
      if (uart_int_sel_mem_handle[i])
      {
         DALSYS_DestroyObject(uart_int_sel_mem_handle[i]);
      }
   }
   if (tal_config_ctxt) { DALSYS_Free(tal_config_ctxt); }
   *phandle = NULL;
   return TAL_ERROR;
}

