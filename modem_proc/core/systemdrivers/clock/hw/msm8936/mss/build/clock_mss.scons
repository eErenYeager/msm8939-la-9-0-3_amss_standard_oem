#===============================================================================
#
# CLOCK DRIVER HW (CHIPSET) LIBRARY
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2012-2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/build/clock_mss.scons#1 $
#  $DateTime: 2015/01/27 06:04:57 $
#  $Author: mplp4svc $
#  $Change: 7351156 $
#
#===============================================================================

import os
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Add API folders
#-------------------------------------------------------------------------------

CLOCK_BUILD_ROOT = os.getcwd();

#-------------------------------------------------------------------------------
# Define paths
#-------------------------------------------------------------------------------

SRCPATH = "../"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Define any features or compiler flags
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'DAL',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'POWER',
   'KERNEL',
   'DEBUGTRACE'
]
 	 
env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Define sources
#-------------------------------------------------------------------------------

CLOCK_HW_SOURCES = [
   '${BUILDPATH}/src/ClockMSS.c',
   '${BUILDPATH}/src/ClockMSSConfigBus.c',
   '${BUILDPATH}/src/ClockMSSDCVS.c',
   '${BUILDPATH}/src/ClockMSSPLL.c',
   '${BUILDPATH}/src/ClockMSSVDD.c',
   '${BUILDPATH}/src/ClockMSSXO.c',
]

#-------------------------------------------------------------------------------
# Add libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(
   ['CORE_MODEM'],
   '${BUILDPATH}/ClockHW',
   CLOCK_HW_SOURCES)

#-------------------------------------------------------------------------------
# HWIO
#-------------------------------------------------------------------------------

if env.has_key('HWIO_IMAGE'):

  env.AddHWIOFile('HWIO', [
    {
      'filename': '${INC_ROOT}/core/systemdrivers/clock/hw/${CHIPSET}/mss/src/ClockMSSHWIO.h',
      'modules': [
        'MSS_PERPH',
        'MSS_QDSP6SS_PUB',
        'SECURITY_CONTROL_CORE'],
      'filter-include': [
        'MSS_TCSR_ACC_SEL',
        'MSS_QDSP6SS_STRAP_ACC',
        'QFPROM_CORR_CALIB_ROW1_MSB'],
      'output-offsets': True,
      'header':
        '#include <HALhwio.h>'
        '\n\n'
        '/*\n'
        ' * HWIO base definitions\n'
        ' */\n'
        'extern uint32                      HAL_clk_nHWIOBaseMSS;\n' +
        '#define MSS_TOP_BASE               HAL_clk_nHWIOBaseMSS\n\n' +
        'extern uint32                      HAL_clk_nHWIOBaseSecurity;\n' +
        '#define SECURITY_CONTROL_BASE      HAL_clk_nHWIOBaseSecurity\n',
    }
  ])

#-------------------------------------------------------------------------------
# Register initialization function and dependencies
#-------------------------------------------------------------------------------

if 'USES_RCINIT' in env:
  RCINIT_IMG = ['CORE_MODEM']
  env.AddRCInitFunc(              # Code Fragment in TMC: NO
    RCINIT_IMG,                   # 
    {
      'sequence_group' : 'RCINIT_GROUP_1',                       # required
      'init_name'      : 'clk_regime_init_nv',                   # required
      'init_function'  : 'clk_regime_init_nv',                   # required
      'dependencies'   : ['efs']
    })

#-------------------------------------------------------------------------------
# SWEvent processing
#-------------------------------------------------------------------------------

if 'USES_QDSS_SWE' in env:
  env.Append(CPPDEFINES = ["CLOCK_TRACER_SWEVT"])

if 'QDSS_TRACER_SWE' in env:
  env.Append(CPPPATH = ['${BUILD_ROOT}/core/systemdrivers/clock/build/${BUILDPATH}/src'])

