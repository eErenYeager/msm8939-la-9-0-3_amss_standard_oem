#ifndef __CLOCKMSSHWIO_H__
#define __CLOCKMSSHWIO_H__
/*
===========================================================================
*/
/**
  @file ClockMSSHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MSS_PERPH
    MSS_QDSP6SS_PUB
    SECURITY_CONTROL_CORE

  'Include' filters applied: MSS_TCSR_ACC_SEL MSS_QDSP6SS_STRAP_ACC QFPROM_CORR_CALIB_ROW1_MSB 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/src/ClockMSSHWIO.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

#include <HALhwio.h>

/*
 * HWIO base definitions
 */
extern uint32                      HAL_clk_nHWIOBaseMSS;
#define MSS_TOP_BASE               HAL_clk_nHWIOBaseMSS

extern uint32                      HAL_clk_nHWIOBaseSecurity;
#define SECURITY_CONTROL_BASE      HAL_clk_nHWIOBaseSecurity


/*----------------------------------------------------------------------------
 * MODULE: MSS_QDSP6SS_PUB
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6SS_PUB_REG_BASE                                      (MSS_TOP_BASE      + 0x00080000)
#define MSS_QDSP6SS_PUB_REG_BASE_OFFS                                 0x00080000

#define HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR                               (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000110)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OFFS                               (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000110)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_RMSK                               0xffffffff
#define HWIO_MSS_QDSP6SS_STRAP_ACC_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR, HWIO_MSS_QDSP6SS_STRAP_ACC_RMSK)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR, m)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR,v)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR,m,v,HWIO_MSS_QDSP6SS_STRAP_ACC_IN)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_DATA_BMSK                          0xffffffff
#define HWIO_MSS_QDSP6SS_STRAP_ACC_DATA_SHFT                                 0x0

/*----------------------------------------------------------------------------
 * MODULE: MSS_PERPH
 *--------------------------------------------------------------------------*/

#define MSS_PERPH_REG_BASE                                                  (MSS_TOP_BASE      + 0x00180000)
#define MSS_PERPH_REG_BASE_OFFS                                             0x00180000

#define HWIO_MSS_TCSR_ACC_SEL_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_OFFS                                          (MSS_PERPH_REG_BASE_OFFS + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_RMSK                                                 0x3
#define HWIO_MSS_TCSR_ACC_SEL_IN          \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, HWIO_MSS_TCSR_ACC_SEL_RMSK)
#define HWIO_MSS_TCSR_ACC_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, m)
#define HWIO_MSS_TCSR_ACC_SEL_OUT(v)      \
        out_dword(HWIO_MSS_TCSR_ACC_SEL_ADDR,v)
#define HWIO_MSS_TCSR_ACC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_TCSR_ACC_SEL_ADDR,m,v,HWIO_MSS_TCSR_ACC_SEL_IN)
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_BMSK                                     0x3
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_SHFT                                     0x0

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                         (SECURITY_CONTROL_BASE      + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_OFFS                                                    0x00000000

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_OFFS                                                   (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_SVS_QUOT_VMIN_8_0_BMSK                            0xff800000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_SVS_QUOT_VMIN_8_0_SHFT                                  0x17
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_SVS_TARGET_BMSK                                     0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_SVS_TARGET_SHFT                                         0x12
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_NOMINAL_QUOT_VMIN_BMSK                               0x3ffc0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_NOMINAL_QUOT_VMIN_SHFT                                   0x6
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_NOMINAL_TARGET_BMSK                                     0x3e
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_NOMINAL_TARGET_SHFT                                      0x1
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TURBO_QUOT_VMIN_11_BMSK                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TURBO_QUOT_VMIN_11_SHFT                                  0x0


#endif /* __CLOCKMSSHWIO_H__ */
