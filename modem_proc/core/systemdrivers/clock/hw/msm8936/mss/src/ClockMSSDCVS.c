/*
===========================================================================
*/
/**
  @file ClockMSSDCVS.c 
  
  DCVS NPA node definitions for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/src/ClockMSSDCVS.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/17/11   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DDIClock.h"
#include "ClockDriver.h"
#include "ClockMSS.h"
#include "ClockAVS.h"
#include "ClockSWEVT.h"
#include "ClockMSSHWIO.h"
#include "DALDeviceId.h"

#include <DALSys.h>
#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/


/*
 * Enumeration of local CPU resources.
 */
enum
{
  CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU,
  CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU_TEST,
  CLOCK_MSS_LOCAL_RESOURCE_TOTAL
};


/*
 * Clock_NPALocalResourcesType
 *
 * Structure containing the NPA node and resource data for local nodes.
 *
 *  resources  - CPU resources data - /clk/cpu and /clk/cpu.test
 *  node       - CPU node data
 */
typedef struct
{
  npa_resource_definition  resources[CLOCK_MSS_LOCAL_RESOURCE_TOTAL];
  npa_node_definition       node;
} Clock_NPAResourcesType;


/*=========================================================================
      Prototypes
==========================================================================*/

static npa_resource_state Clock_NPACPUDriverFunc
(
  npa_resource        *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState
);


static void Clock_ClkCPUReadyCallback 
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
);


static npa_query_status Clock_NPACPUResourceQuery
( 
  npa_resource    *resource, 
  unsigned int     id,
  npa_query_type  *result
);


/*=========================================================================
      Data
==========================================================================*/

/*
 * NPA remote resources used on MSS
 */
static npa_remote_resource_definition Clock_aNPARemoteResources[] =
{
  {
    "/clk/qdss",
    "clk0\x01\x00\x00\x00",
    "/protocol/rpm/rpm",
    &npa_max_plugin,
    npa_remote_resource_local_aggregation_driver_fcn, 
    "STATE",
    NPA_MAX_STATE
  },
  {
    "/clk/pcnoc",
    "clk1\x00\x00\x00\x00",
    "/protocol/rpm/rpm",
    &npa_max_plugin,
    npa_remote_resource_local_aggregation_driver_fcn, 
    "KHz",
    NPA_MAX_STATE
  },
  {
    "/clk/snoc",
    "clk1\x01\x00\x00\x00",
    "/protocol/rpm/rpm",
    &npa_max_plugin,
    npa_remote_resource_local_aggregation_driver_fcn, 
    "KHz",
    NPA_MAX_STATE
  },
  {
    "/clk/bimc",
    "clk2\x00\x00\x00\x00",
    "/protocol/rpm/rpm",
    &npa_max_plugin,
    npa_remote_resource_local_aggregation_driver_fcn, 
    "KHz",
    NPA_MAX_STATE
  },
  {
    "/clk/dcvs.ena",
    "clk0\x02\x00\x00\x00",
    "/protocol/rpm/rpm",
    &npa_binary_plugin,
    npa_remote_resource_local_aggregation_driver_fcn,
    "Enable",
    NPA_MAX_STATE
  }
};


/*
 * Define the local clock resources.
 */
static Clock_NPAResourcesType Clock_NPALocalResources =
{
  /*
   * resources
   */
  {
    /*
     * CPU
   */
  {
      "/clk/cpu",
    "KHz",
    0,  /* Max, fill in later */
    &npa_max_plugin, 
    NPA_RESOURCE_DEFAULT,
      NULL,
    Clock_NPACPUResourceQuery
  },

  /*
     * CPU Test
     */
    {
      "/clk/cpu.test",
      "KHz",
      0,  /* Max, fill in later */
      &npa_max_plugin,
      NPA_RESOURCE_SINGLE_CLIENT,
      NULL,
      Clock_NPACPUResourceQuery
    }
  },

  /*
   * node
   */
  { 
    "/node/clk/cpu",           /* name       */
    Clock_NPACPUDriverFunc,   /* driver_fcn */
    NPA_NODE_DEFAULT,         /* attributes */
    NULL,                     /* data */
    NPA_EMPTY_ARRAY,
     SIZEOF_ARRAY(Clock_NPALocalResources.resources),
     Clock_NPALocalResources.resources
  }
};


/*=========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_InitDCVS
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitDCVS
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32              i, nPL, nConfig;
  npa_resource_state  nInitialStates[CLOCK_MSS_LOCAL_RESOURCE_TOTAL];
  ClockMSSCtxtType   *pMSSCtxt;

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Make sure we have a valid BSP version.                                */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig == NULL)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* We must disable scaling until:                                        */
  /*  - EFS init has complete.                                             */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->QDSP6Ctxt.nDisableDCS |= CLOCK_FLAG_WAITING_FOR_EFS_INIT;

  /*-----------------------------------------------------------------------*/
  /* Create our remote nodes.                                              */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < SIZEOF_ARRAY(Clock_aNPARemoteResources); i++)
  {
    npa_remote_define_resource(
      &Clock_aNPARemoteResources[i], (npa_resource_state)0, NULL);
  }

  /*-----------------------------------------------------------------------*/
  /* Init the NPA CPU resource/node                                        */
  /* NOTE: Our inital request is the current configuration.                */
  /*-----------------------------------------------------------------------*/
  
  Clock_NPALocalResources.node.data = (npa_user_data)pDrvCtxt;

  nPL     = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig->nMaxPerfLevel;
  nConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig->anPerfLevel[nPL];

  for (i = 0; i < Clock_NPALocalResources.node.resource_count; i++)
  {
    Clock_NPALocalResources.resources[i].max =
    pMSSCtxt->pBSPConfig->pQDSP6Config[nConfig].Mux.nFreqHz / 1000;
  }

  nInitialStates[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU] =
    pMSSCtxt->QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz / 1000;
  nInitialStates[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU_TEST] = 0;

  npa_define_node_cb(
    &Clock_NPALocalResources.node,
    nInitialStates,
    Clock_ClkCPUReadyCallback,
    pMSSCtxt);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitDCVS */


/* =========================================================================
**  Function : Clock_EnableDCVS
** =========================================================================*/
/*
  See DDIClock.h
*/ 

DALResult Clock_EnableDCVS
(
  ClockDrvCtxt *pDrvCtxt
) 
{
  return DAL_SUCCESS;

} /* END Clock_EnableDCVS */


/* =========================================================================
**  Function : Clock_SetQDSP6Voltage
** =========================================================================*/
/**
  Set QDSP6 voltage.
 
  This function sets QDSP6 voltage based on input parameters.
 
  @param pDrvCtxt        [in] -- Pointer to the driver context.
  @param  eVRegMSSCorner [in] -- New voltage.

  @return
  DAL_SUCCESS -- Request was successful.
  DAL_ERROR -- Request failed.

  @dependencies
  None.
*/ 

static DALResult Clock_SetQDSP6Voltage
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eVRegMSSCorner
)
{
  DALResult            eResult = DAL_SUCCESS;
  ClockMSSCtxtType    *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockVDDMSSCtxtType *pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;

  /*-----------------------------------------------------------------------*/
  /* Update the /vdd/mss voltage corner.                                   */
  /*-----------------------------------------------------------------------*/

  if (!pVDDMSSCtxt->nDisableMSSVDD)
  {
    eResult = Clock_SetVDDMSSCorner(pDrvCtxt, eVRegMSSCorner, TRUE);
  }

  return eResult;
}


/* =========================================================================
**  Function : Clock_SetQDSP6ConfigGFMux
** =========================================================================*/
/**
  Set QDSP6 configuration.

  This function sets QDSP6 configuration based on input parameters.
  This configures the Q6 MUX and GFMUX.

  @param *pDrvCtxt [in] -- Pointer to driver context.
  @param  pConfig  [in] -- New configuration.

  @return
  DAL_SUCCESS -- Request was successful.
  DAL_ERROR -- Request failed.

  @dependencies
  The clock mutex must be locked.
*/

static DALResult Clock_SetQDSP6ConfigGFMux
(
  ClockDrvCtxt          *pDrvCtxt,
  ClockQDSP6ConfigType  *pConfig
)
{
  DALResult            eResult;
  ClockMSSCtxtType    *pMSSCtxt;
  ClockNodeType       *pClock;
  ClockSourceNodeType *pSource;

  /*-----------------------------------------------------------------------*/
  /* Get QDSP6 clock.                                                      */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  pClock = pMSSCtxt->QDSP6Ctxt.pQDSP6Clock;

  /*-----------------------------------------------------------------------*/
  /* Save domain source                                                    */
  /*-----------------------------------------------------------------------*/

  pSource = pClock->pDomain->pSource;

  /*-----------------------------------------------------------------------*/
  /* Put an extra vote to keep current source ON - we do this since        */
  /* Clock_SetClockConfig could turn OFF current source (if unused), but   */
  /* we want to keep it ON until we switch to GFMUX in HAL_clk_ConfigClock */
  /* function.                                                             */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_EnableSourceInternal(pDrvCtxt, pSource, TRUE);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Switch QDSP6 core clock.                                              */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetClockConfig(pDrvCtxt, pClock->pDomain, &pConfig->Mux);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Set QDSP6 core configuration.                                         */
  /*-----------------------------------------------------------------------*/

  HAL_clk_ConfigClock(pClock->HALHandle, pConfig->CoreConfig);

  /*-----------------------------------------------------------------------*/
  /* Releasing our vote for old source.                                    */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_DisableSourceInternal(pDrvCtxt, pSource, TRUE, FALSE);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Update state.                                                         */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->QDSP6Ctxt.pQDSP6Config = pConfig;

  return DAL_SUCCESS;

} /* END Clock_SetQDSP6ConfigGFMux */

/* =========================================================================
**  Function : Clock_SetQDSP6Config
** =========================================================================*/
/**
  Set QDSP6 configuration.
 
  This function sets QDSP6 configuration based on input parameters.
  The requested configuration is applied with an intermediate configuration
  if it requires the current source to be reconfigured to satisfy the
  requested configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.
  @param  pConfig  [in] -- New configuration.

  @return
  None.

  @dependencies
  None.
*/ 

void Clock_SetQDSP6Config
(
  ClockDrvCtxt          *pDrvCtxt,
  ClockQDSP6ConfigType  *pConfig
)
{
  DALResult             eResult;
  ClockQDSP6ConfigType *pPreviousConfig, *pTempConfig;
  ClockMSSCtxtType     *pMSSCtxt;
  ClockMuxConfigType   *pMux, *pPreviousMux;
  uint32                nPLLSwitchPerfLevel;


  /*-----------------------------------------------------------------------*/
  /* Get config data.                                                      */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  pPreviousConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6Config;

  /*-----------------------------------------------------------------------*/
  /* Short-circuit if the configuration is already active.                 */
  /*-----------------------------------------------------------------------*/

  if (pConfig == pPreviousConfig)
  {
    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Pre-frequency voltage update                                          */
  /*-----------------------------------------------------------------------*/

  if (pConfig->eVRegMSSCorner > pPreviousConfig->eVRegMSSCorner)
  {
    eResult = Clock_SetQDSP6Voltage(pDrvCtxt, pConfig->eVRegMSSCorner);
    if (eResult != DAL_SUCCESS)
      {
        DALSYS_LogEvent (
          DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to set QDSP6 voltage.");

      DALCLOCK_FREE(pDrvCtxt);

      return;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-frequency STRAP_ACC update                                        */
  /*-----------------------------------------------------------------------*/

  if (pConfig->Mux.nFreqHz > pPreviousConfig->Mux.nFreqHz)
  {
    HWIO_OUT(MSS_QDSP6SS_STRAP_ACC, pConfig->nStrapACCVal);
  }

  /*-----------------------------------------------------------------------*/
  /* If we are switching between levels using the same source A but the    */
  /* configurations of that source A are different then we must switch to  */
  /* a level using a different source B temporarily while we reconfigure   */
  /* source A for the new level.                                           */
  /* The intermediate performanace level must require a voltage <= the MAX */
  /* of our current performance level voltage requirement or the new       */
  /* performance level voltage requirement.                                */
  /*-----------------------------------------------------------------------*/

  pMux = &pConfig->Mux;
  pPreviousMux = &pPreviousConfig->Mux;
  if ( (pPreviousMux->HALConfig.eSource == pMux->HALConfig.eSource) &&
       (pPreviousMux->pSourceFreqConfig != pMux->pSourceFreqConfig) )
  {
    nPLLSwitchPerfLevel =
      pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig->nPLLSwitchPerfLevel;

    pTempConfig = &pMSSCtxt->pBSPConfig->pQDSP6Config[nPLLSwitchPerfLevel];

    eResult = Clock_SetQDSP6ConfigGFMux(pDrvCtxt, pTempConfig);
    if (eResult != DAL_SUCCESS)
    {
      DALCLOCK_FREE(pDrvCtxt);

      return;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Switch to the requested configuration.                                */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetQDSP6ConfigGFMux(pDrvCtxt, pConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Post-frequency STRAP_ACC update                                       */
  /*-----------------------------------------------------------------------*/

  if (pConfig->Mux.nFreqHz < pPreviousConfig->Mux.nFreqHz)
  {
    HWIO_OUT(MSS_QDSP6SS_STRAP_ACC, pConfig->nStrapACCVal);
  }

  /*-----------------------------------------------------------------------*/
  /* Post-frequency voltage update                                         */
  /*-----------------------------------------------------------------------*/

  if (pConfig->eVRegMSSCorner < pPreviousConfig->eVRegMSSCorner)
  {
    eResult = Clock_SetQDSP6Voltage(pDrvCtxt, pConfig->eVRegMSSCorner);
    if (eResult != DAL_SUCCESS)
      {
        DALSYS_LogEvent (
          DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to set QDSP6 voltage.");

      DALCLOCK_FREE(pDrvCtxt);

      return;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);  

} /* END Clock_SetQDSP6Config */


/* =========================================================================
**  Function : Clock_NPACPUDriverFunc
** =========================================================================*/
/**
  NPA node driver function for the CPU node.
 
  This function handles minimum frequency requests for the CPU NPA node.
 
  @param *pResource [in] -- Pointer to resource whose state changed.
  @param hClient    [in] -- Client handle that triggered the change.
  @param nState     [in] -- New request state.

  @return
  npa_resource_state -- The new active state of the resource.

  @dependencies
  None.
*/ 

static npa_resource_state Clock_NPACPUDriverFunc
(
  npa_resource        *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState 
)
{
  uint32                    nPL, nMinPL, nMaxPL;
  uint32                    nConfig;
  uint32                    nResultFreqKHz, nMinFreqHz;
  npa_resource             *pResourceCPU;
  npa_resource             *pResourceCPUTest;
  ClockDrvCtxt             *pDrvCtxt;
  ClockMSSCtxtType         *pMSSCtxt;
  ClockVDDMSSCtxtType      *pVDDMSSCtxt;
  ClockQDSP6ConfigType     *pQDSP6Config;
  ClockQDSP6PerfConfigType *pQDSP6PerfConfig;

  pDrvCtxt = (ClockDrvCtxt *)pResource->node->data;
  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  pQDSP6Config = pMSSCtxt->pBSPConfig->pQDSP6Config;
  pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;
  nMinFreqHz = (uint32)nState * 1000;

  pResourceCPU =
    Clock_NPALocalResources.resources[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU].handle; 
   
  pResourceCPUTest =
    Clock_NPALocalResources.resources[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU_TEST].handle;

  /*-----------------------------------------------------------------------*/
  /* Check if dynamic switching is disabled or if no BSP data for this HW. */
  /*-----------------------------------------------------------------------*/

  if ((pMSSCtxt->QDSP6Ctxt.nDisableDCS && pResource == pResourceCPU) ||
      (pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig == NULL)                  )
  {
    if (pResource == pResourceCPU)
    {
      nResultFreqKHz = pMSSCtxt->QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz / 1000;
      return (npa_resource_state)nResultFreqKHz;
    }
    else
    {
      return pResource->active_state;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* If the resource is /clk/cpu.test and request is zero, enable dynamic  */
  /* clock switching and set the CPU to /clk/cpu's request state. If the   */
  /* request non-zero, disable dynamic clock switching and CPR.            */
  /*-----------------------------------------------------------------------*/

  if (pResource == pResourceCPUTest)
  {
    /*
     * If the request state on /clk/cpu.test is zero, that means either the
     * resource is being initialized or the client completed its request.
     * In either case we clear the relevant bit in the disable flag and
     * update /clk/cpu to its current request state.
     */
    if (nState == 0)
  {
      DALCLOCK_LOCK(pDrvCtxt);
      pMSSCtxt->QDSP6Ctxt.nDisableDCS &= ~CLOCK_FLAG_DISABLED_BY_CPU_TEST;
      DALCLOCK_FREE(pDrvCtxt);

      DALCLOCK_LOCK(pVDDMSSCtxt);
      pMSSCtxt->CPRCtxt.nDisableCPR &= ~CLOCK_FLAG_DISABLED_BY_CPU_TEST;
      DALCLOCK_FREE(pVDDMSSCtxt);

      npa_assign_resource_state(
        pResourceCPU,
        Clock_NPACPUDriverFunc(
          pResourceCPU,
          NULL,
          pResourceCPU->request_state));

      return nState;
  }
    else
    {
      DALCLOCK_LOCK(pDrvCtxt);
      pMSSCtxt->QDSP6Ctxt.nDisableDCS |= CLOCK_FLAG_DISABLED_BY_CPU_TEST;
      DALCLOCK_FREE(pDrvCtxt);
      
      DALCLOCK_LOCK(pVDDMSSCtxt);
      pMSSCtxt->CPRCtxt.nDisableCPR |= CLOCK_FLAG_DISABLED_BY_CPU_TEST;
      DALCLOCK_FREE(pVDDMSSCtxt);
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Find minimum performance level.                                       */
  /*-----------------------------------------------------------------------*/

  pQDSP6PerfConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig;

  nMinPL = pQDSP6PerfConfig->nMinPerfLevel;
  nMaxPL = pQDSP6PerfConfig->nMaxPerfLevel;

  if (nState == NPA_MAX_STATE)
  {
    nPL = nMaxPL;
  }
  else
  {
    for (nPL = nMinPL; nPL < nMaxPL; nPL++)
    {
      nConfig = pQDSP6PerfConfig->anPerfLevel[nPL];

      if (pQDSP6Config[nConfig].Mux.nFreqHz >= nMinFreqHz)
      {
        break;
      }
    }
  }

  nConfig = pQDSP6PerfConfig->anPerfLevel[nPL];

  /*-----------------------------------------------------------------------*/
  /* Switch the CPU clock.                                                 */
  /*-----------------------------------------------------------------------*/

  Clock_SetQDSP6Config(pDrvCtxt, &pQDSP6Config[nConfig]);

  /*-----------------------------------------------------------------------*/
  /* Log clock frequency.                                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(
    CLOCK_EVENT_CLOCK_FREQ,
    2,
    HAL_clk_GetTestClockId(pMSSCtxt->QDSP6Ctxt.pQDSP6Clock->HALHandle),
    pQDSP6Config[nConfig].Mux.nFreqHz / 1000);

  /*-----------------------------------------------------------------------*/
  /* Return new state of this resource.                                    */
  /*-----------------------------------------------------------------------*/

  nResultFreqKHz = pMSSCtxt->QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz / 1000;

  return (npa_resource_state)nResultFreqKHz;

} /* END Clock_NPACPUDriverFunc */


/* =========================================================================
**  Function : Clock_ClkCPUReadyCallback
** =========================================================================*/
/**
  Callback when a /clk/cpu node is created.

  This function is called by the NPA framework when the given /clk/cpu node
  is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext     [in] -- Context passed in npa_define_node_cb
  @param nEventType    [in] -- Zero.
  @param *pNodeName    [in] -- Name of the node being created.
  @param nNodeNameSize [in] -- Length of the name.

  @return
  None.

  @dependencies
  None.
*/ 

static void Clock_ClkCPUReadyCallback 
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
)
{
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pContext;

  /*
   * Create an impulse client for triggering re-aggregation on the CPU resource.
   */
  pMSSCtxt->QDSP6Ctxt.hNPAClkCPUImpulse = 
    npa_create_sync_client(
      Clock_NPALocalResources.resources[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU].name,
      CLOCK_NPA_CLIENT_NAME_CLK_CPU_IMPULSE,
      NPA_CLIENT_IMPULSE);

  if (pMSSCtxt->QDSP6Ctxt.hNPAClkCPUImpulse == NULL)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to create NPA sync client %s->%s.",
      CLOCK_NPA_CLIENT_NAME_CLK_CPU_IMPULSE,
      CLOCK_NPA_NODE_NAME_CPU);
  }

} /* END Clock_ClkCPUReadyCallback */


/* =========================================================================
**  Function : Clock_NPACPUResourceQuery
** =========================================================================*/
/**
  NPA CPU resource query function.
 
  This function is called by NPA query nodes to get a list of supported CPU
  frequencies, or the CPU's vote on its voltage rail.
 
  @param *resource  [in]  -- Pointer to the resource in question
  @param id         [in]  -- ID of the query.
  @param *result    [out] -- Pointer to the data to be filled by this function.

  @return
  npa_query_status - NPA_QUERY_SUCCESS, if query supported.
                   - NPA_QUERY_UNSUPPORTED_QUERY_ID, if query not supported.

  @dependencies
  None.
*/ 

static npa_query_status Clock_NPACPUResourceQuery
( 
  npa_resource    *pResource,
  unsigned int     id,
  npa_query_type  *result
)
{

  uint32                    nMinPL, nMaxPL, nConfig,  nIndex;
  uint32                    nNumPerfLevels;
  npa_query_status          nStatus  = NPA_QUERY_UNSUPPORTED_QUERY_ID;
  ClockDrvCtxt             *pDrvCtxt =
                              (ClockDrvCtxt *)pResource->node->data;
  ClockMSSCtxtType         *pMSSCtxt =
                              (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockQDSP6ConfigType     *pQDSP6Config =
                              pMSSCtxt->pBSPConfig->pQDSP6Config;
  ClockQDSP6PerfConfigType *pQDSP6PerfConfig = NULL;
  DALResult                 eResult;



  /*-----------------------------------------------------------------------*/
  /* Return an error if no valid BSP data exists for this HW version.      */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig == NULL)
  {
    return NPA_QUERY_NO_VALUE;
  }

  /*-----------------------------------------------------------------------*/
  /* Get a handle to QDSP6 performance data                                */
  /*-----------------------------------------------------------------------*/

  pQDSP6PerfConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig;


    /*
   * Get min, max and number of performance levels of QDSP6
     */
    nMinPL = pQDSP6PerfConfig->nMinPerfLevel;
    nMaxPL = pQDSP6PerfConfig->nMaxPerfLevel;
    nNumPerfLevels = nMaxPL - nMinPL + 1;

  /*-----------------------------------------------------------------------*/
  /* If this is a performance level indexing request then return the       */
  /* frequency corresponding to this performance level.                    */
  /*-----------------------------------------------------------------------*/

  if((id >= CLOCK_NPA_QUERY_PERF_LEVEL_KHZ) &&
     (id < CLOCK_NPA_QUERY_NUM_PERF_LEVELS))
    {
    nIndex = ((uint32)id - (uint32)CLOCK_NPA_QUERY_PERF_LEVEL_KHZ);
    if(nIndex < nNumPerfLevels)
    {
      nConfig = pQDSP6PerfConfig->anPerfLevel[nMinPL + nIndex];
      result->type = NPA_QUERY_TYPE_VALUE;
      result->data.value = pQDSP6Config[nConfig].Mux.nFreqHz/1000;
      nStatus = NPA_QUERY_SUCCESS;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Returns the number of performance levels.                             */
  /*-----------------------------------------------------------------------*/

  else if(id == CLOCK_NPA_QUERY_NUM_PERF_LEVELS)
    {
        result->type = NPA_QUERY_TYPE_VALUE;
        result->data.value = nNumPerfLevels;
        nStatus = NPA_QUERY_SUCCESS;
  }
    
  /*-----------------------------------------------------------------------*/
  /* If this is a max frequency at corner voltage indexing request then    */
  /* return the frequency corresponding to this corner voltage.            */
  /*-----------------------------------------------------------------------*/
    
  else if((id >= CLOCK_NPA_QUERY_CPU_MAX_KHZ_AT_CORNER) &&
          (id < CLOCK_NPA_QUERY_MIN_FREQ_KHZ))
  {
    nIndex = ((uint32)id - (uint32)CLOCK_NPA_QUERY_CPU_MAX_KHZ_AT_CORNER);
    if(nIndex < CLOCK_VREG_MSS_NUM_CORNERS)
    {
      eResult = Clock_FindCPUMaxConfigAtVoltage(pDrvCtxt, &pQDSP6Config, nIndex);
      if (eResult == DAL_SUCCESS)
      {
        result->type = NPA_QUERY_TYPE_VALUE;
        result->data.value = pQDSP6Config->Mux.nFreqHz / 1000;
        nStatus = NPA_QUERY_SUCCESS;
      }
    }
  }
    
  /*-----------------------------------------------------------------------*/
  /* Return the minimum frequency in KHz.                                  */
  /*-----------------------------------------------------------------------*/

  else if (id == CLOCK_NPA_QUERY_MIN_FREQ_KHZ)
  {
    nConfig = pQDSP6PerfConfig->anPerfLevel[nMinPL];
        result->type = NPA_QUERY_TYPE_VALUE;
    result->data.value = pQDSP6Config[nConfig].Mux.nFreqHz / 1000;
        nStatus = NPA_QUERY_SUCCESS;
  }
  else if (id == CLOCK_NPA_QUERY_CPU_RAIL_VOLTAGE_MV)
  {
        result->type = NPA_QUERY_TYPE_VALUE;
        result->data.value = Clock_MapVDDMSSCornerToVoltage(
          pDrvCtxt,
          pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU) / 1000;
        nStatus = NPA_QUERY_SUCCESS;
  }

  else if (id == CLOCK_NPA_QUERY_CPU_ELDO_VOLTAGE_MV)
  {
        result->type = NPA_QUERY_TYPE_VALUE;
        result->data.value = pMSSCtxt->QDSP6Ctxt.nLDOVoltageUV / 1000;
        nStatus = NPA_QUERY_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Return the CPU's corner vote on the voltage rail.                     */
  /*-----------------------------------------------------------------------*/

  else if (id == CLOCK_NPA_QUERY_CPU_RAIL_VOLTAGE_CORNER)
  {
    result->type = NPA_QUERY_TYPE_VALUE;
    result->data.value =
    pMSSCtxt->QDSP6Ctxt.pQDSP6Config->eVRegMSSCorner;
    nStatus = NPA_QUERY_SUCCESS;
      }
  else if ( id == CLOCK_NPA_QUERY_NUM_CORNERS)
  {
    result->type = NPA_QUERY_TYPE_VALUE;
    result->data.value = CLOCK_VREG_NUM_LEVELS ;
    nStatus = NPA_QUERY_SUCCESS;
  }
  return nStatus;

} /* END Clock_NPACPUResourceQuery */


/* =========================================================================
**  Function : Clock_NPATriggerCPUStateUpdate
** =========================================================================*/
/**
  Trigger function to update modem CPU state.
 
  This function updates modem CPU state (frequency) and then
  synchronizes them with NPA framework.
 
  @param *pDrvCtxt [in] -- Pointer to the clock driver context.

  @return
  None.

  @dependencies
  None.
*/ 

void Clock_NPATriggerCPUStateUpdate
(
  ClockDrvCtxt *pDrvCtxt
)
{
  npa_resource *pResource =
    Clock_NPALocalResources.resources[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU].handle;

  /*-----------------------------------------------------------------------*/
  /* Make sure the node has been created before we attempt to update.      */
  /*-----------------------------------------------------------------------*/

  if (!pResource)
  {
    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pResource);

  /*-----------------------------------------------------------------------*/
  /* Trigger CPU state update.                                             */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(
    pResource,
    Clock_NPACPUDriverFunc(pResource, NULL, pResource->request_state));

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pResource);

} /* END Clock_NPATriggerCPUStateUpdate */


/* =========================================================================
**  Function : Clock_NPACPUResourceOverwriteActiveState
** =========================================================================*/
/**
  This Function overwrites the CPU resource's active state. It should ONLY
  be invoked during the Sleep sequence when in Single Threaded Mode (STM).

  We invoke this function after directly configuring the CPU without
  going through the NPA. This results in the CPU resource's active state
  being different from the requested state. Upon wake-up, Sleep must make
  an NPA Impulse request which will re-aggregate the CPU requests and
  trigger our driver function.

  @param nState [in] -- Active state of the CPU resource.

  @return
  DAL_SUCCESS -- Resource active state is set. \n
  DAL_ERROR  --  Resource handle is NULL.

  @dependencies
  None.
*/
DALResult Clock_NPACPUResourceOverwriteActiveState
(
  npa_resource_state nState
)
{
  npa_resource *pResource =
    Clock_NPALocalResources.resources[CLOCK_MSS_LOCAL_RESOURCE_CLK_CPU].handle;

  /*-----------------------------------------------------------------------*/
  /* Make sure the node has been created before we attempt to update.      */
  /*-----------------------------------------------------------------------*/

  if (pResource == NULL)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Overwrite the resource's active state.                                */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(pResource, nState);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* Clock_NPACPUResourceOverwriteActiveState */

