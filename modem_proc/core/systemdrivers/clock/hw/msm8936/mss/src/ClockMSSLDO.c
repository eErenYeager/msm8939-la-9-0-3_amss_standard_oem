/*
===========================================================================
*/
/**
  @file ClockMSSLDO.c

  The file contains the resource definitions for LDO voltage scaling on
  the MPSS.
*/
/*
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/src/ClockMSSLDO.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockDriver.h"
#include "ClockMSS.h"
#include "ClockMSSBSP.h"
#include "ClockMSSHWIO.h"
#include "ClockSWEVT.h"
#include "ULog.h"


/*=========================================================================
      Function Prototypes
==========================================================================*/


/*=========================================================================
      Global Variables
==========================================================================*/


/*=========================================================================
      Macros
==========================================================================*/

#define CLOCK_LDO_CTL1_ENABLE_CONFIG                                          \
  0x20

/*
 * Min/Max voltage values for the LDO operation range.
 * These voltages need to be converted to LDO reg format when programmed.
 */
#define CLOCK_LDO_VREF_MIN_UV   465 * 1000
#define CLOCK_LDO_VREF_MAX_UV  1100 * 1000

/*
 * LDO voltage delta.
 */
#define CLOCK_LDO_DELTA(val1, val2)                                            \
  ((val1)>(val2)?(val1)-(val2):(val2)-(val1))

/*
 * LDO range checker.
 */
#define CLOCK_LDO_IN_RANGE(x)                                                  \
  ( ((x) >=CLOCK_LDO_VREF_MIN_UV) && ((x) <= CLOCK_LDO_VREF_MAX_UV) )

/*
 * LDO uV to LDO_REF register format.
 */
#define CLOCK_LDO_UV_MAP_TO_HW(x)                                              \
  ( ( ( (x) - CLOCK_LDO_VREF_MIN_UV ) / 1000 ) / 5 ) + 1

/*
 * Set wait time to the equivalent of 5 us per 10 mV.
 * This value is expected to change when data becomes available.
 */
#define CLOCK_LDO_SETTLE_DELAY(start, end)                                     \
  DALSYS_BusyWait(5 * ( ( ( CLOCK_LDO_DELTA(start, end) + (10000 - 1) ) / 10000) ) )

#define QFPROM_RAW_SW_CALIB_ROW0_MSB_QDSS0_LDO_ENABLE_BMSK         (1 << 27)

/*=========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_DetectLDOEFuse
** =========================================================================*/
/**
  Read LDO Trimmed Info

  This function will read an efuse field to detect whether the LDO has
  been trimmed and is safe to enable the LDO output.

  @return
  None.

  @dependencies
  None.
*/
static void Clock_DetectLDOEFuse
(
  ClockDrvCtxt  *pDrvCtxt
)
{
  uint32            nLDOEFuseRegVal;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Read EFuse to determine whether LDO output should be prevented.       */
  /*-----------------------------------------------------------------------*/

  nLDOEFuseRegVal = HWIO_IN(QFPROM_RAW_SW_CALIB_ROW0_MSB);

  /*-----------------------------------------------------------------------*/
  /* Mask out the enable bit                                               */
  /*-----------------------------------------------------------------------*/

  if ((nLDOEFuseRegVal &
       QFPROM_RAW_SW_CALIB_ROW0_MSB_QDSS0_LDO_ENABLE_BMSK) != 0)
  {
    pMSSCtxt->QDSP6Ctxt.nDisableLDO = FALSE;
  }
  else
  {
    pMSSCtxt->QDSP6Ctxt.nDisableLDO = CLOCK_FLAG_DISABLED_LDO_BY_EFUSE;
  }

} /* Clock_DetectLDOEFuse */


/* =========================================================================
**  Function : Clock_SwitchToBHS
** =========================================================================*/
/**
  Switches to using BHS mode.

  This function is used to switch to BHS mode.  If already in BHS mode,
  it will simply return.

  @return
  None.

  @dependencies
  None.
*/
static void Clock_SwitchToBHS(void)
{
  uint32 nQ6PowerCtl = HWIO_IN(MSS_QDSP6SS_PWR_CTL);

  /*-----------------------------------------------------------------------*/
  /* Only switch to BHS if we aren't already in BHS mode.                  */
  /*-----------------------------------------------------------------------*/

  if ((nQ6PowerCtl & HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON)) == 0)
  {
    /*
     * Turn on BHS from LDO mode.
     */
    nQ6PowerCtl |= HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nQ6PowerCtl);

    /*
     * Wait 10 uS for the LDO to turn on.
     */
    DALSYS_BusyWait(1);

    /*
     * Put LDO in bypass.
     */
    nQ6PowerCtl |= HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_BYP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nQ6PowerCtl);

    /*
     * Turn off LDO and BG_UP.
     */
    nQ6PowerCtl &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_BG_PU);
    nQ6PowerCtl &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_PWR_UP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nQ6PowerCtl);

    /*
     * Log the LDO disable event to QDSS.
     */
    Clock_SWEvent(CLOCK_EVENT_LDO, 1, 0);
  }

} /* Clock_SwitchToBHS */


/* =========================================================================
**  Function : Clock_SwitchToLDO
** =========================================================================*/
/**
  Switches to using LDO mode.

  This function is used to switch to LDO mode.  If already in LDO mode,
  it will simply return.

  @return
  None.

  @dependencies
  None.
*/
static void Clock_SwitchToLDO(void)
{
  uint32 nQ6PowerCtl = HWIO_IN(MSS_QDSP6SS_PWR_CTL);
  uint32 nLDOCTL1Val;

  /*-----------------------------------------------------------------------*/
  /* Only switch to LDO if in BHS mode.                                    */
  /*-----------------------------------------------------------------------*/

  if(nQ6PowerCtl & HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON))
  {
    /*
     * The mode bit inside LDO_CTL1 needs to be set during the switch.
     */
    nLDOCTL1Val = HWIO_INF(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);
    HWIO_OUTF(
      MSS_QDSP6SS_LDO_CFG0,
      LDO_CTL1,
      nLDOCTL1Val | CLOCK_LDO_CTL1_ENABLE_CONFIG);

    /*
     * Turn on LDO by setting LDO_BG_UP and LDO_PWR_UP.
     * NOTE: The HPG recommends to leave LDO_BG_UP asserted to allow fast
     *       switching between LDO and BHS
     *       (estimated 150 uA leakage in BHS mode).
     */
    nQ6PowerCtl |= HWIO_MSS_QDSP6SS_PWR_CTL_LDO_BG_PU_BMSK;
    nQ6PowerCtl |= HWIO_MSS_QDSP6SS_PWR_CTL_LDO_PWR_UP_BMSK; 

    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nQ6PowerCtl);

    DALSYS_BusyWait(5);

    /*
     * Turn off BHS and LDO Bypass.
     */
    nQ6PowerCtl &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON);
    nQ6PowerCtl &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_BYP);

    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nQ6PowerCtl);

    /*
     * Clear the mode bit after switching to LDO mode from BHS.
     */
    HWIO_OUTF(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1, nLDOCTL1Val);

    /*
     * Log the LDO enable event to QDSS.
     */
    Clock_SWEvent(CLOCK_EVENT_LDO, 1, 1);
  }

} /* Clock_SwitchToLDO */


/* =========================================================================
**  Function : Clock_InitLDOVoltage
** =========================================================================*/
/*
  See ClockMSS.h
*/
DALResult Clock_InitLDOVoltage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32 nLDOVoltageData;
  ClockLDODataType *pLDOData = NULL;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Read the EFuse to determine whether LDO output should be allowed.     */
  /*-----------------------------------------------------------------------*/

  Clock_DetectLDOEFuse(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Get the LDO BSP data.                                                 */
  /*-----------------------------------------------------------------------*/

  pLDOData = pMSSCtxt->pBSPConfig->pLDOData;

  /*-----------------------------------------------------------------------*/
  /* Ensure we're in BHS mode before setting up the LDO config.            */
  /*-----------------------------------------------------------------------*/

  Clock_SwitchToBHS();

  /*-----------------------------------------------------------------------*/
  /* Program the LDO_CFG registers per recommendation from HW.             */
  /*-----------------------------------------------------------------------*/

  HWIO_OUT(MSS_QDSP6SS_LDO_CFG0, pLDOData->nLDOCFG0);
  HWIO_OUT(MSS_QDSP6SS_LDO_CFG1, pLDOData->nLDOCFG1);
  HWIO_OUT(MSS_QDSP6SS_LDO_CFG2, pLDOData->nLDOCFG2);

  /*-----------------------------------------------------------------------*/
  /* Program the retention voltage based on the BSP.                       */
  /*-----------------------------------------------------------------------*/

  if (!CLOCK_LDO_IN_RANGE(pLDOData->nRetentionVoltageUV))
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "BSP LDO rentetion voltage is out of supported range - disabling LDO.");

    pMSSCtxt->QDSP6Ctxt.nDisableLDO = TRUE;

    return DAL_ERROR;
  }

  nLDOVoltageData = CLOCK_LDO_UV_MAP_TO_HW(pLDOData->nRetentionVoltageUV);
  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, VREF_RET, nLDOVoltageData);

  /*-----------------------------------------------------------------------*/
  /* Program the operating voltage based on the BSP.                       */
  /*-----------------------------------------------------------------------*/

  if (!CLOCK_LDO_IN_RANGE(pLDOData->nOperatingVoltageUV))
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "BSP LDO operating voltage is out of supported range - disabling LDO.");

    pMSSCtxt->QDSP6Ctxt.nDisableLDO = TRUE;

    return DAL_ERROR;
  }

  nLDOVoltageData = CLOCK_LDO_UV_MAP_TO_HW(pLDOData->nOperatingVoltageUV);
  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, VREF_LDO, nLDOVoltageData);

  return DAL_SUCCESS;

} /* Clock_InitLDOVoltage */


/* =========================================================================
**  Function : Clock_SetLDOVoltage
** =========================================================================*/
/*
  See ClockMSS.h
*/
DALResult Clock_SetLDOVoltage
(
  ClockDrvCtxt  *pDrvCtxt,
  uint32         nCPUVoltageUV
)
{
  ClockMSSCtxtType *pMSSCtxt;
  uint32            nLDOVoltageData;

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Switch to LDO mode if a non-zero voltage was provided.                */
  /*-----------------------------------------------------------------------*/

  if (nCPUVoltageUV != 0)
  {
    /*
     * Sanity check to verify that the cpu voltage is supported.
     */
    if (!CLOCK_LDO_IN_RANGE(nCPUVoltageUV))
    {
      return DAL_ERROR;
    }

    /*
     * Program the new operating voltage data to the LDO VREF register.
     */
    nLDOVoltageData = CLOCK_LDO_UV_MAP_TO_HW(nCPUVoltageUV);
    HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, VREF_LDO, nLDOVoltageData);

    /*
     * Switch the Q6 over to use the LDO.
     */
    Clock_SwitchToLDO();

    /*
     * Wait for voltage to settle.
     */
    CLOCK_LDO_SETTLE_DELAY(nCPUVoltageUV, pMSSCtxt->QDSP6Ctxt.nLDOVoltageUV);
    
    /*
     * Update the context.
     */
    pMSSCtxt->QDSP6Ctxt.nLDOVoltageUV = nCPUVoltageUV;

    /*
     * Log the LDO voltage via QDSS.
     */
    Clock_SWEvent(CLOCK_EVENT_LDO_VOLTAGE, 1, nCPUVoltageUV);

    /*
     * Log LDO change to ULOG.
     */
    if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
    {
      ULOG_RT_PRINTF_1(
        pDrvCtxt->hClockLog,
        "LDO (enabled, output: %lu)",
        nCPUVoltageUV);
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Switch to BHS if we are disabling the LDO.                            */
  /*-----------------------------------------------------------------------*/

  else
  {
    Clock_SwitchToBHS();

    /*
     * Update the context.
     */
    pMSSCtxt->QDSP6Ctxt.nLDOVoltageUV = 0;

    /*
     * Log the LDO voltage via QDSS.
     */
    Clock_SWEvent(CLOCK_EVENT_LDO_VOLTAGE, 1, nCPUVoltageUV);

    /*
     * Log LDO change.
     */
    if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
    {
      ULOG_RT_PRINTF_0(pDrvCtxt->hClockLog,"LDO (disabled)");
    }
  }

  return DAL_SUCCESS;

} /* Clock_SetLDOVoltage */

