/*
===========================================================================
*/
/**
  @file ClockMSSPLL.c 
  
  Modem PLL NPA node definitions for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/src/ClockMSSPLL.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/17/12   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockDriver.h"
#include "ClockMSS.h"
#include "DALDeviceId.h"

#include <DALSys.h>
#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>


/*=========================================================================
      Macros
==========================================================================*/

/*
 * PMIC PLL SR2 client and resource names
 */
#define CLOCK_PMIC_NPA_GROUP_ID_PLL_SR2            "/pmic/client/pll_sr2"
#define CLOCK_PMIC_RESOURCE_PLL_SR2_CLIENT         "/clkregim/pll_sr2"
#define CLOCK_PMIC_NPA_MODE_ID_GENERIC_ACTIVE      1


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * Enumeration of modem PLLs.
 */
enum
{
  CLOCK_MODEM_PLL0,
  CLOCK_MODEM_PLL1,
  CLOCK_MODEM_PLL2,
  CLOCK_MODEM_PLL_TOTAL
};


/*
 * Clock_NPAModemPLLResourcesType
 *
 * Structure containing the NPA node and resource data for the modem PLLs.
 * 
 * MPLL0Resource - Modem PLL0 resource data - /pll/modem/0
 * MPLL0Node     - Modem PLL0 node data
 * MPLL1Resource - Modem PLL1 resource data - /pll/modem/1
 * MPLL1Node     - Modem PLL1 node data
 * MPLL2Resource - Modem PLL2 resource data - /pll/modem/2
 * MPLL2Node     - Modem PLL2 node data
 */
typedef struct
{
  npa_resource_definition   MPLL0Resource;
  npa_node_definition       MPLL0Node;
  npa_resource_definition   MPLL1Resource;
  npa_node_definition       MPLL1Node;
  npa_resource_definition   MPLL2Resource;
  npa_node_definition       MPLL2Node;
} Clock_NPAModemPLLResourcesType;


/*=========================================================================
      Prototypes
==========================================================================*/

static npa_resource_state Clock_NPAMPLLDriverFunc
(
  struct npa_resource *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState 
);


static void Clock_PLLSR2ReadyCallback
(
  void *pContext
);


/*=========================================================================
      Data
==========================================================================*/

/*
 * Define the local clock resources.
 */
static Clock_NPAModemPLLResourcesType Clock_NPAMPLLResources =
{
  /*
   * MPLL0Resource
   */
  {
    "/pll/modem/0", 
    "on/off",
    1,
    &npa_max_plugin, 
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)CLOCK_MODEM_PLL0
  },

  /*
   * MPLL0Node
   */
  { 
    "/node/pll/modem/0",       /* name */
    Clock_NPAMPLLDriverFunc,   /* driver_fcn */
    NPA_NODE_DEFAULT,          /* attributes */
    NULL,                      /* data */
    NPA_EMPTY_ARRAY,
    1, &Clock_NPAMPLLResources.MPLL0Resource
  },

  /*
   * MPLL1Resource
   */
  {
    "/pll/modem/1", 
    "on/off",
    1,
    &npa_max_plugin, 
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)CLOCK_MODEM_PLL1
  },

  /*
   * MPLL1Node
   */
  { 
    "/node/pll/modem/1",       /* name */
    Clock_NPAMPLLDriverFunc,   /* driver_fcn */
    NPA_NODE_DEFAULT,          /* attributes */
    NULL,                      /* data */
    NPA_EMPTY_ARRAY,
    1, &Clock_NPAMPLLResources.MPLL1Resource
  },


  /*
   * MPLL2Resource
   */
  {
    "/pll/modem/2", 
    "on/off",
    1,
    &npa_max_plugin, 
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)CLOCK_MODEM_PLL2
  },

  /*
   * MPLL2Node
   */
  { 
    "/node/pll/modem/2",       /* name */
    Clock_NPAMPLLDriverFunc,   /* driver_fcn */
    NPA_NODE_DEFAULT,          /* attributes */
    NULL,                      /* data */
    NPA_EMPTY_ARRAY,
    1, &Clock_NPAMPLLResources.MPLL2Resource
  }
};


/*=========================================================================
      Functions
==========================================================================*/

/* =========================================================================
**  Function : Clock_InitPLL
** =========================================================================*/
/*
  See ClockMSS.h
*/ 

DALResult Clock_InitPLL
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32              nIndex, nMPLL;
  HAL_clk_SourceType  nSource;
  npa_resource_state  nInitialRequest;
  DALResult           eRes;
  ClockMSSCtxtType   *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

#if 0
  /*-----------------------------------------------------------------------*/
  /* Commenting this code to remove MPSS clkdriver vote as LD07 is         */
  /* always ON and this change has good impact on timelines                */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Register a callback for the PMIC_NPA_GROUP_ID_PLL_SR2 node            */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_RegisterResourceCallback(pDrvCtxt,
                                        CLOCK_PMIC_NPA_GROUP_ID_PLL_SR2,
                                        Clock_PLLSR2ReadyCallback,
                                        pDrvCtxt);

  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }
#endif

  /*-----------------------------------------------------------------------*/
  /* Init MPLL0 source and NPA node.                                       */
  /*-----------------------------------------------------------------------*/

  nMPLL = (uint32) Clock_NPAMPLLResources.MPLL0Resource.data;
  nSource = pMSSCtxt->pBSPConfig->pModemPLLConfig[nMPLL].nSource;
  nIndex = pDrvCtxt->anSourceIndex[nSource];

  if((nIndex != 0xFF) && 
     (pDrvCtxt->aSources[nIndex].nReferenceCount == 0) &&
     (pDrvCtxt->aSources[nIndex].nReferenceCountSuppressible == 0))
  {
    eRes = Clock_InitSource(pDrvCtxt, nSource, NULL);

    if (eRes != DAL_SUCCESS) 
    {
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to initialize MPLL0 source.");
      return eRes;
    }
  }

  Clock_NPAMPLLResources.MPLL0Node.data = (npa_user_data)pDrvCtxt;

  nInitialRequest = 0;

  npa_define_node(&Clock_NPAMPLLResources.MPLL0Node, &nInitialRequest, NULL);

  /*-----------------------------------------------------------------------*/
  /* Init MPLL1 source and NPA node.                                       */
  /*-----------------------------------------------------------------------*/

  nMPLL = (uint32) Clock_NPAMPLLResources.MPLL1Resource.data;
  nSource = pMSSCtxt->pBSPConfig->pModemPLLConfig[nMPLL].nSource;
  nIndex = pDrvCtxt->anSourceIndex[nSource];

  if((nIndex != 0xFF) && 
     (pDrvCtxt->aSources[nIndex].nReferenceCount == 0) &&
     (pDrvCtxt->aSources[nIndex].nReferenceCountSuppressible == 0))
  {
    eRes = Clock_InitSource(pDrvCtxt, nSource, NULL);

    if (eRes != DAL_SUCCESS) 
    {
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to initialize MPLL1 source.");
      return eRes;
    }
  }

  Clock_NPAMPLLResources.MPLL1Node.data = (npa_user_data)pDrvCtxt;

  nInitialRequest = 0;

  npa_define_node(&Clock_NPAMPLLResources.MPLL1Node, &nInitialRequest, NULL);

  /*-----------------------------------------------------------------------*/
  /* Init MPLL2 source and NPA node.                                       */
  /*-----------------------------------------------------------------------*/

  nMPLL = (uint32) Clock_NPAMPLLResources.MPLL2Resource.data;
  nSource = pMSSCtxt->pBSPConfig->pModemPLLConfig[nMPLL].nSource;
  nIndex = pDrvCtxt->anSourceIndex[nSource];

  if((nIndex != 0xFF) && 
     (pDrvCtxt->aSources[nIndex].nReferenceCount == 0) &&
     (pDrvCtxt->aSources[nIndex].nReferenceCountSuppressible == 0))
  {
    eRes = Clock_InitSource(pDrvCtxt, nSource, NULL);

    if (eRes != DAL_SUCCESS) 
    {
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to initialize MPLL2 source.");
      return eRes;
    }
  }

  Clock_NPAMPLLResources.MPLL2Node.data = (npa_user_data)pDrvCtxt;

  nInitialRequest = 0;

  npa_define_node(&Clock_NPAMPLLResources.MPLL2Node, &nInitialRequest, NULL);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitPLL */


/* =========================================================================
**  Function : Clock_NPAMPLLDriverFunc
** =========================================================================*/
/**
  Handle state changes on the modem PLLs.
 
  This function handles state changes on the modem PLL nodes.
 
  @param pResource [in] -- The NPA resource being requested.
  @param hClient [in]   -- Pointer to the client making the request.
  @param nState [in]    -- New state of the resource.

  @return
  New state of the resource.

  @dependencies
  None.
*/ 

static npa_resource_state Clock_NPAMPLLDriverFunc
(
  struct npa_resource *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState 
)
{
  uint32              nIndex, nMPLL;
  HAL_clk_SourceType  nSource;
  ClockDrvCtxt       *pDrvCtxt = (ClockDrvCtxt *)pResource->node->data;
  ClockMSSCtxtType   *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  nMPLL = (uint32) pResource->definition->data;
  nSource = pMSSCtxt->pBSPConfig->pModemPLLConfig[nMPLL].nSource;
  nIndex = pDrvCtxt->anSourceIndex[nSource];

  /*-----------------------------------------------------------------------*/
  /* Ignore the first/initial request                                      */
  /*-----------------------------------------------------------------------*/

  if ((hClient->type == NPA_CLIENT_INITIALIZE) || (nIndex == 0xFF))
  {
    return nState;
  }

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);  

  /*-----------------------------------------------------------------------*/
  /* "state" is binary on/off                                              */
  /*-----------------------------------------------------------------------*/

  if (nState == 1)
  {
    Clock_EnableSourceInternal(pDrvCtxt, &pDrvCtxt->aSources[nIndex], FALSE);
  }
  else if (nState == 0)
  {
    Clock_DisableSourceInternal(
      pDrvCtxt,
      &pDrvCtxt->aSources[nIndex],
      FALSE,
      FALSE);
  }

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);  

  /*-----------------------------------------------------------------------*/
  /* Note:This state is NOT indicative of what current modem PLL state is. */
  /*-----------------------------------------------------------------------*/

  return nState;  

} /* END Clock_NPAMPLLDriverFunc */


/* =========================================================================
**  Function : Clock_PLLSR2ReadyCallback
** =========================================================================*/
/**
  Callback after /pmic/client/pll_sr2 node is created.

  This function is called by the NPA framework after the /pmic/client/pll_sr2
  node is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext [in] -- Context pointer, the driver context in this case.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_PLLSR2ReadyCallback
(
  void *pContext
)
{
  npa_client_handle hNPAPMICPLLSR2;

  /*-----------------------------------------------------------------------*/
  /* Get a handle to PMIC_NPA_GROUP_ID_PLL_SR2 node                        */
  /*-----------------------------------------------------------------------*/

  hNPAPMICPLLSR2 = npa_create_sync_client(
                     CLOCK_PMIC_NPA_GROUP_ID_PLL_SR2,
                     CLOCK_PMIC_RESOURCE_PLL_SR2_CLIENT,
                     NPA_CLIENT_SUPPRESSIBLE);

  if (hNPAPMICPLLSR2 == NULL)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to create NPA sync client %s.",
      CLOCK_PMIC_RESOURCE_PLL_SR2_CLIENT);

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Vote for SR2 PLL active state                                         */
  /*-----------------------------------------------------------------------*/

  npa_issue_scalar_request(hNPAPMICPLLSR2, CLOCK_PMIC_NPA_MODE_ID_GENERIC_ACTIVE);

}
