#ifndef CLOCKMSS_H
#define CLOCKMSS_H
/*
===========================================================================
*/
/**
  @file ClockMSS.h 
  
  Internal header file for the clock device driver on the MSS image.
*/
/*  
  ====================================================================

  Copyright (c) 2011-12 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/src/ClockMSS.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  10/19/11   vs      Created.

  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DDIClock.h"
#include "ClockMSSBSP.h"


/*=========================================================================
      Macro Definitions
==========================================================================*/


/*
 * Parameter for /core/cpu/vdd NPA node
 */
#define CLOCK_POWER_COLLAPSE_PREVENT              2

/*
 * Resource scaling disable flags
 */
#define CLOCK_FLAG_DISABLED_BY_EFS                (0x1 << 0)
#define CLOCK_FLAG_WAITING_FOR_EFS_INIT           (0x1 << 1)
#define CLOCK_FLAG_DISABLED_BY_CPU_TEST           (0x1 << 4)
#define CLOCK_FLAG_WAITING_FOR_CPR_REGISTRATION   (0x1 << 5)

/*
 * NPA Node Names
 */
#define CLOCK_NPA_NODE_NAME_CRYPTO                "/clk/mss/crypto"
#define CLOCK_NPA_NODE_NAME_CONFIG_BUS            "/clk/mss/config_bus"
#define CLOCK_NPA_NODE_NAME_VDD_MSS               "/vdd/mss"
#define CLOCK_NPA_NODE_NAME_VDD_MSS_THERMAL       "/vdd/thermal"
#define CLOCK_NPA_NODE_NAME_CPU                   "/clk/cpu"
#define CLOCK_NPA_NODE_NAME_PCNOC                 "/clk/pcnoc"
#define CLOCK_NPA_NODE_NAME_CXO                   "/xo/cxo"
#define NPA_NODE_NAME_CORE_CPU_VDD                "/core/cpu/vdd"
#define CLOCK_NPA_CLIENT_NAME_CLK_CPU_IMPULSE     "/clk/cpu/impulse"


/*
 * MAX definition
 */
#ifndef MAX
   #define  MAX( x, y ) ( ((x) > (y)) ? (x) : (y) )
#endif

#ifndef MIN
  #define   MIN( x, y ) ( ((x) < (y)) ? (x) : (y) )
#endif


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * QDSP6 clock driver context.
 */
typedef struct
{
  ClockNodeType            *pQDSP6Clock;
  ClockQDSP6ConfigType     *pQDSP6Config;
  ClockQDSP6PerfConfigType *pQDSP6PerfConfig;
  npa_client_handle         hNPAClkCPUImpulse;
  uint32                    nDisableDCS;
  uint32                    nDisableLDO;
  uint32                    nLDOVoltageUV;
} ClockQDSP6CtxtType;


/*
 * MSS Config Bus context.
 */
typedef struct
{
  ClockNodeType            *pConfigBusClock;
  ClockNodeType            *pCryptoClock;
  ClockConfigBusConfigType *pConfigBusConfig;
} ClockConfigBusCtxtType;


/*
 * VDDMSS Context.
 */
typedef struct
{
  DALSYSSyncHandle       hLock;
  DALSYSSyncObj          LockObj;
  npa_client_handle      hNPACoreCpuVdd;
  uint32                 nDisableMSSVDD;
  ClockVRegMSSCornerType eVRegMSSCornerNonCPU;
  ClockVRegMSSCornerType eVRegMSSCornerCPU;
  ClockVRegMSSCornerType eVRegMSSCornerInit;
  ClockVRegMSSCornerType eVRegMSSCornerRail;
  ClockVRegMSSCornerType eVRegMSSCornerFloor;
  ClockVRegMSSCornerType eVRegMSSCornerCeiling;
  uint32                 nRawVoltageUV;
  boolean                bPreventPC;
} ClockVDDMSSCtxtType;


/*
 * CPR Context.
 */
typedef struct
{
  CPR_PreSwitchFuncPtr   fpPreSwitch;
  CPR_PostSwitchFuncPtr  fpPostSwitch;
  CPR_GetVoltageFuncPtr  fpGetVoltage;
  uint32                 nDisableCPR;
} ClockCPRCtxtType;


/**
 * MSS clock driver context.
 */
typedef struct
{
  ClockQDSP6CtxtType     QDSP6Ctxt;
  ClockConfigBusCtxtType ConfigBusCtxt;
  ClockVDDMSSCtxtType    VDDMSSCtxt;
  ClockCPRCtxtType       CPRCtxt;
  ClockMSSBSPConfigType *pBSPConfig;
} ClockMSSCtxtType;


/*=========================================================================
      Function Definitions
==========================================================================*/

/* =========================================================================
**  Function : Clock_InitPLL
** =========================================================================*/
/**
  Initialize the modem PLL management subsystem.
 
  This function initializes NPA nodes and resources to control modem PLLs.
 
  @param *pDrvCtxt [in] -- The driver context.

  @return
  DAL_SUCCESS

  @dependencies
  None.
*/ 

DALResult Clock_InitPLL
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_InitXO
** =========================================================================*/
/**
  Initialize the XO management subsystem.
 
  This function initializes the XO LPR nodes required for communicating 
  resource requests to the RPM.
 
  @param *pDrvCtxt [in] -- The driver context.

  @return
  DAL_SUCCESS

  @dependencies
  None.
*/ 


DALResult Clock_InitXO
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_InitMSSVDD
** =========================================================================*/
/**
  Initialize /vdd/mss NPA node.
 
  This function initializes /vdd/mss NPA node.
 
  @param *pDrvCtxt [in] -- The driver context.

  @return
  DAL_SUCCESS

  @dependencies
  None.
*/ 

DALResult Clock_InitMSSVDD
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_InitConfigBus
** =========================================================================*/
/**
  Initialize /clk/mss/config_bus NPA node.

  This function initializes /clk/mss/config_bus NPA node.

  @param *pDrvCtxt [in] -- The driver context.

  @return
  DAL_SUCCESS

  @dependencies
  None.
*/

DALResult Clock_InitConfigBus
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_InitCrypto
** =========================================================================*/
/**
  Initialize /clk/mss/crypto NPA node.

  This function initializes /clk/mss/crypto NPA node.

  @param *pDrvCtxt [in] -- The driver context.

  @return
  DAL_SUCCESS

  @dependencies
  None.
*/

DALResult Clock_InitCrypto
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_SetQDSP6Config
** =========================================================================*/
/**
  Set QDSP6 configuration.
 
  This function sets QDSP6 configuration based on input parameters.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.
  @param  pConfig  [in] -- New configuration.

  @return
  None.

  @dependencies
  None.
*/ 

void Clock_SetQDSP6Config
(
  ClockDrvCtxt          *pDrvCtxt,
  ClockQDSP6ConfigType  *pConfig
);


/* =========================================================================
**  Function : Clock_NPATriggerCPUStateUpdate
** =========================================================================*/
/**
  Trigger function to update modem CPU state.
 
  This function updates modem CPU state (frequency) and then synchronizes
  it with NPA framework.
 
  @param *pDrvCtxt [in] -- Pointer to the clock driver context.

  @return
  None.

  @dependencies
  None.
*/

void Clock_NPATriggerCPUStateUpdate
( 
  ClockDrvCtxt *pDrvCtxt 
);


/* =========================================================================
**  Function : Clock_SetVDDMSSCorner
** =========================================================================*/
/**
  Updates the /vdd/mss voltage.

  This function updates the voltage on the /vdd/mss rail.

  @param pDrvCtxt [in] -- Pointer to the driver context.
  @param eVRegMSSCorner [in] -- New /vdd/mss corner.
  @param bCPURequest    [in] -- CPU vs. Non-CPU request being made.

  @return
  DAL_SUCCESS -- The voltage corner was successfully applied.
  DAL_ERROR -- The voltage corner was not applied.

  @dependencies
  None.
*/

DALResult Clock_SetVDDMSSCorner
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eVRegMSSCorner,
  boolean                 bCPURequest
);


/* =========================================================================
**  Function : Clock_MapVDDMSSCornerToVoltage
** =========================================================================*/
/**
  Maps an MSS rail corner to a raw voltage.

  This function takes in an MSS voltage rail corner and returns a raw voltage.

  @param pDrvCtxt       [in] -- Pointer to the driver context.
  @param eVRegMSScorner [in] -- Corner for /vdd/mss.

  @return
  Raw voltage in uV.

  @dependencies
  None.
*/

uint32 Clock_MapVDDMSSCornerToVoltage
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eVRegMSSCorner
);


/* =========================================================================
**  Function : Clock_InitLDOVoltage
** =========================================================================*/
/**
  Initializes the LDO voltage control.

  This function does required work to initialize the image-specific LDO
  voltage scaling functionality.

  @param pDrvCtxt [in] -- Pointer to the driver context.

  @return
  DAL_SUCCESS -- Successfully initialized the voltage functionality.
  DAL_ERROR  -- A problem occurred trying to initialize the driver.

  @dependencies
  None.
*/
DALResult Clock_InitLDOVoltage
(
  ClockDrvCtxt *pDrvCtxt
);


/* =========================================================================
**  Function : Clock_SetLDOVoltage
** =========================================================================*/
/**
  Sets a voltage for a current performance level.

  This function will program the LDO to obtain the desired voltage level.

  @param pDrvCtxt       [in] -- Pointer to the driver context.
  @param nCPUVoltageUV  [in] -- The CPU voltage requirement in uV.

  @return
  DAL_SUCCESS -- Successfully set the new voltage.
  DAL_ERROR  -- Unable to set the desired voltage to HW.

  @dependencies
  None.
*/
DALResult Clock_SetLDOVoltage
(
  ClockDrvCtxt  *pDrvCtxt,
  uint32         nCPUVoltageUV
);


/* =========================================================================
**  Function : Clock_NPACPUResourceOverwriteActiveState
** =========================================================================*/
/**
  This Function overwrites the CPU resource's active state. It should ONLY
  be invoked during the Sleep sequence when in Single Threaded Mode (STM).

  We invoke this function after directly configuring the CPU without
  going through the NPA. This results in the CPU resource's active state
  being different from the requested state. Upon wake-up, Sleep must make
  an NPA Impulse request which will re-aggregate the CPU requests and
  trigger our driver function.

  @param nState [in] -- Active state of the CPU resource.

  @return
  DAL_SUCCESS -- Resource active state is set. \n
  DAL_ERROR  --  Resource handle is NULL.

  @dependencies
  None.
*/
DALResult Clock_NPACPUResourceOverwriteActiveState
(
  npa_resource_state nState
);

/* =========================================================================
**  Function : Clock_FindCPUMaxConfigAtVoltage
** =========================================================================*/
/**
  Finds the maximum CPU config at the specified voltage level.

  @param *pDrvCtxt[in] -- Pointer to driver context.
  @param **pConfig[in] -- Pointer to CPU config pointer.
  @param nCorner[in]   -- Corner id from pmapp_npa.h

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/

DALResult Clock_FindCPUMaxConfigAtVoltage
(
  ClockDrvCtxt         *pDrvCtxt,
  ClockQDSP6ConfigType **pConfig,
  uint32                nCorner
);

#endif /* !CLOCKMSS_H */

