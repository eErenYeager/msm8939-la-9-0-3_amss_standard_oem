#ifndef __CLOCKMSSBSP_H__
#define __CLOCKMSSBSP_H__
/*
===========================================================================
*/
/**
  @file ClockMSSBSP.h 
  
  Header description for the MSS clock driver BSP format.
*/
/*  
  ====================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mss/inc/ClockMSSBSP.h#2 $
  $DateTime: 2015/05/19 03:55:37 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  10/19/11   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBSP.h"


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * ClockVRegMSSCornerType
 *
 * List of voltage corners that a client can request on MSS VDD rail.
 * Note that the enumeration values map to pmic NPA node request values.
 *
 *  OFF          - DNC voltage corner
 *  LOW_MINUS    - Low minus voltage corner
 *  LOW          - Low voltage corner
 *  LOW_PLUS     - Low plus voltage corner
 *  NOMINAL      - Nominal voltage corner
 *  NOMINAL_PLUS - Nominal plus voltage corner
 *  TURBO        - Turbo voltage corner.
 */
typedef enum
{
  CLOCK_VREG_MSS_CORNER_OFF          = 0,
  CLOCK_VREG_MSS_CORNER_RETENTION    = 1,
  CLOCK_VREG_MSS_CORNER_LOW_MINUS    = 2,
  CLOCK_VREG_MSS_CORNER_LOW          = 3,
  CLOCK_VREG_MSS_CORNER_NOMINAL      = 4,
  CLOCK_VREG_MSS_CORNER_NOMINAL_PLUS = 5,
  CLOCK_VREG_MSS_CORNER_TURBO        = 6,
  CLOCK_VREG_MSS_CORNER_MAX          = CLOCK_VREG_MSS_CORNER_TURBO,
  CLOCK_VREG_MSS_NUM_CORNERS         = 7,
  CLOCK_BSP_ENUM_32BITS(VREG_MSS_CORNER)
} ClockVRegMSSCornerType;


/*
 * ClockQDSP6ConfigType
 *
 * Configuration parameters for a performance level of a QDSP6.
 * 
 * CoreConfig     - QDSP6 core configuration
 * Mux            - General mux configuration
 * eVRegMSSCorner - MSS voltage corner for this performance level
 * nStrapACCVal   - 4 byte value for STRAP_ACC register at this level.
 */
typedef struct
{
  HAL_clk_ClockConfigType CoreConfig;
  ClockMuxConfigType      Mux;
  ClockVRegMSSCornerType  eVRegMSSCorner;
  uint32                  nStrapACCVal;
} ClockQDSP6ConfigType;


/*
 * ClockQDSP6PerfConfigType
 *
 * Actual format for the data stored in the BSP for the Q6.
 *
 *  nMinPerfLevel       - Minimum performance level
 *  nMaxPerfLevel       - Maximum performance level
 *  HWVersion           - Version of the chip HW this configuration is for.
 *  nPLLSwitchPerfLevel - Performance level for reconfiguring an active PLL
 *  anPerfLevel         - Array of performance level indices
 */
typedef struct
{
  uint32              nMinPerfLevel;
  uint32              nMaxPerfLevel;
  ClockHWVersionType  HWVersion;
  uint32              nPLLSwitchPerfLevel;
  uint32             *anPerfLevel;
} ClockQDSP6PerfConfigType;


/*
 * ClockModemPLLConfigType
 *
 * Modem PLL to actual PLL mapping
 *
 *  nSource     - Source enum
 */
typedef struct
{
  HAL_clk_SourceType nSource;
} ClockModemPLLConfigType;


/*
 * ClockConfigBusConfigType
 *
 * Parameters for an MSS config bus configuration.
 *
 * Mux            - General mux configuration
 * eVRegMSSCorner - MSS voltage corner for this configuration
 */
typedef struct
{
  ClockMuxConfigType     Mux;
  ClockVRegMSSCornerType eVRegMSSCorner;
} ClockConfigBusConfigType;


/*
 * ClockConfigBusPerfConfigType
 *
 * Actual format for the data stored in the BSP for the MSS Config Bus.
 *
 *  nMinPerfLevel - Minimum performance level
 *  nMaxPerfLevel - Maximum performance level
 *  anPerfLevel   - Array of performance level indices
 */
typedef struct
{
  uint32  nMinPerfLevel;
  uint32  nMaxPerfLevel;
  uint32 *anPerfLevel;
} ClockConfigBusPerfConfigType;


/*
 * ClockLDODataType
 *
 * BSP data for LDO programming.
 * NOTE: These values are defined in the QDSP6v5SS HPG.
 *
 * nLDOCFG0            - Value for QDSP6SS_LDO_CFG0.
 * nLDOCFG1            - Value for QDSP6SS_LDO_CFG1.
 * nLDOCFG2            - Value for QDSP6SS_LDO_CFG2.
 * nRetentionVoltageUV - The retention voltage in mV.
 * nOperatingVoltageUV - The operating voltage in mV.
 */
typedef struct
{
  uint32                 nLDOCFG0;
  uint32                 nLDOCFG1;
  uint32                 nLDOCFG2;
  uint32                 nRetentionVoltageUV;
  uint32                 nOperatingVoltageUV;
  uint32                 nHeadroomVoltageUV;
  ClockVRegMSSCornerType VRegMSSCornerMin;
  ClockVRegMSSCornerType VRegMSSCornerMax;
} ClockLDODataType;


/*
 * ClockVRegCornerInfoType
 *
 *  ClockVRegCorner  - Indicates corner voltage mappings
 *  HWVersion        - Version of the chip HW this configuration is for
 */
typedef struct
{
  ClockVRegCornerDataType     *pClockVRegCorner; 
  ClockHWVersionType           HWVersion;
} ClockVRegCornerInfoType;


/*
 * ClockMSSBSPConfigType
 *
 * Modem BSP data.
 *
 *  pVRegCornerData                 - Array of corner voltage mappings.
 *  nTotalVRegCornerData            - Total number of corner tables defined.
 *  pLDOData                        - LDO Configuration Data.
 *  pQDSP6Config                    - Array of Q6 configurations
 *  pQDSP6PerfConfig                - Array of Q6 perf configurations
 *  pModemPLLConfig                 - Array of MPLL to actual PLL mappings
 *  pConfigBusConfig                - Array of Config Bus configurations
 *  pConfigBusPerfConfig            - Array of Config Bus perf levels
 *  nTotalQDSP6PerfLevelConfigs     - Number of Q6 perf levels
 *  nTotalConfigBusPerfLevelConfigs - Number of Config Bus perf levels
 *  eVDDMSSVRegInitLevel            - Initial VDD/MSS VReg corner
 *  eVDDMSSVRegCeiling              - Max supported corner for VDD/MSS rail
 *  eVDDMSSVRegFloor                - Min supported corner for VDD/MSS rail
 */ 
typedef struct
{
  ClockVRegCornerInfoType      *pVRegCornerData;
  uint32                        nTotalVRegCornerData;
  ClockLDODataType             *pLDOData;
  ClockQDSP6ConfigType         *pQDSP6Config;
  ClockQDSP6PerfConfigType     *pQDSP6PerfConfig;
  ClockModemPLLConfigType      *pModemPLLConfig;
  ClockConfigBusConfigType     *pConfigBusConfig;
  ClockConfigBusPerfConfigType *pConfigBusPerfConfig;
  uint32                        nTotalQDSP6PerfLevelConfigs;
  uint32                        nTotalConfigBusPerfLevelConfigs;
  ClockVRegMSSCornerType        eVDDMSSVRegInitLevel;
  ClockVRegMSSCornerType        eVDDMSSVRegCeiling;
  ClockVRegMSSCornerType        eVDDMSSVRegFloor;
} ClockMSSBSPConfigType;

/*=========================================================================
      Extern Definitions
==========================================================================*/


#endif  /* __CLOCKMSSBSP_H__ */ 

