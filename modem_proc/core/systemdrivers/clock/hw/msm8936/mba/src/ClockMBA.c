/*
===========================================================================
*/
/**
  @file ClockMBA.c

  Implementation file for the MBA's MSS clock API.
*/
/*
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8936/mba/src/ClockMBA.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/


#include <HALhwio.h>
#include "ClockMBAHWIO.h"


/*=========================================================================
      Defines
==========================================================================*/


/*
 * RCGR update timeout. In practice this should be very short (less than 1us),
 * but it depends on the clock frequency being fed into the RCG. Choosing
 * a very conservative value.
 */
#define CLOCK_MBA_UPDATE_TIMEOUT_US 1000


/*
 * Common clock branch fields/masks (*_CBCR register)
 */
#define HAL_CLK_BRANCH_CTRL_REG_CLK_OFF_FMSK                           0x80000000
#define HAL_CLK_BRANCH_CTRL_REG_CLK_OFF_SHFT                           31
#define HAL_CLK_BRANCH_CTRL_REG_CLK_DIV_FMSK                           0x01FF0000
#define HAL_CLK_BRANCH_CTRL_REG_CLK_DIV_SHFT                           16
#define HAL_CLK_BRANCH_CTRL_REG_CLK_FORCE_MEM_CORE_ON_FMSK             0x00004000
#define HAL_CLK_BRANCH_CTRL_REG_CLK_FORCE_MEM_PERIPH_ON_FMSK           0x00002000
#define HAL_CLK_BRANCH_CTRL_REG_CLK_FORCE_MEM_PERIPH_OFF_FMSK          0x00001000
#define HAL_CLK_BRANCH_CTRL_REG_CLK_ENABLE_FMSK                        0x00000001
#define HAL_CLK_BRANCH_CTRL_REG_CLK_ENABLE_SHFT                        0


#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x)               (sizeof(x) / sizeof(x[0]))
#endif


/*
 * HAL_CLK_FMSK
 *
 * Define a register mask data structure given the register name
 * and field.
 */
#define HAL_CLK_FMSK(io, field)   \
  {                               \
    HWIO_ADDR(io),                \
    HWIO_FMSK(io, field)          \
  }


#define WAIT_1_MICROSECOND                                    \
  do {                                                          \
       /* Set QTimer match value in cycles */                      \
       HWIO_OUT(QDSP6SS_QTMR_V1_CNTP_TVAL_0, 19);                  \
       /* Start QTimer */                                          \
       HWIO_OUTF(QDSP6SS_QTMR_V1_CNTP_CTL_0, EN, 0x1);             \
       /* Loop until the interrupt status bit goes high */         \
       while(HWIO_INF(QDSP6SS_QTMR_V1_CNTP_CTL_0,ISTAT) == 0x0);   \
       /* Disable QTimer upon finishing. */                        \
       HWIO_OUTF(QDSP6SS_QTMR_V1_CNTP_CTL_0,EN, 0x0);              \
     } while(0)


/*
 * HAL_clk_RegisterMaskType
 *
 * Contains a register address and mask, used for setting and clearing
 * a given set of bits.
 *
 * nAddr - Address of the register (32-bit).
 * nMask - 32-bit mask.
 */
typedef struct
{
  uint32 nAddr;
  uint32 nMask;
} HAL_clk_RegisterMaskType;


/*
 * HAL_clk_ClockRegistersType
 *
 * nBranchAddr  - Clock branch address/offset.
 * mHWVotable   - Register/mask for HW votable configuration setup.
 */
typedef struct
{
  uint32                    nBranchAddr;
  HAL_clk_RegisterMaskType  mHWVotable;
} HAL_clk_ClockRegistersType;


/*=========================================================================
      External and forward references
==========================================================================*/


extern void mba_modem_wait_microseconds(uint32 microseconds);


/*=========================================================================
      Data
==========================================================================*/


static const HAL_clk_ClockRegistersType aClockRegisters[] =
{
  /*
   * Clock Name: gcc_prng_ahb_clk
   */
  {
    .nBranchAddr = HWIO_ADDR(GCC_PRNG_AHB_CBCR),
    .mHWVotable  = HAL_CLK_FMSK(PROC_CLK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA)
  },

  /*
   * Clock Name: gcc_dehr_clk
   */
  {
    .nBranchAddr = HWIO_ADDR(GCC_DEHR_CBCR),
    .mHWVotable  = {0, 0}
  },

  /*
   * Clock Name: clk_nav_snoc_axi
   */
  {
    .nBranchAddr = HWIO_ADDR(MSS_NAV_SNOC_AXI_CBCR),
    .mHWVotable  = {0, 0}
  },

  /*
   * Clock Name: clk_bus_nav_ce_bridge
   */
  {
    .nBranchAddr = HWIO_ADDR(MSS_BUS_NAV_CE_BRIDGE_CBCR),
    .mHWVotable  = {0, 0}
  },

  /*
   * Clock Name: clk_bus_nc_hm_bridge_cx
   */
  {
    .nBranchAddr = HWIO_ADDR(MSS_BUS_NC_HM_BRIDGE_CX_CBCR),
    .mHWVotable  = {0, 0}
  },
  
  /*
   * Clock Name: clk_bus_crypto
   */
  {
    .nBranchAddr = HWIO_ADDR(MSS_BUS_CRYPTO_CBCR),
    .mHWVotable  = {0, 0}
  },

};


/*=========================================================================
      Functions
==========================================================================*/


/* ===========================================================================
**  ClockMBA_EnableClock
**
** ======================================================================== */
/**
  Enable a clock using the specified register and mask.

  @param
  None

  @return
  TRUE  -- Successfully enabled clock.
  FALSE -- Failed to enable clock.
  None

  @dependencies
  None
*/

static boolean ClockMBA_EnableClock
(
  const HAL_clk_ClockRegistersType *pRegisters
)
{
  uint32 nAddr, nVal, nMask;
  uint32 nTimeout = 0;

  /*
   * Sanity check - All clocks must have a CBCR
   */
  if (pRegisters->nBranchAddr == 0)
  {
    return FALSE;
  }

  /*
   * Check for HW votable registers
   */
  nAddr = pRegisters->mHWVotable.nAddr;
  if(nAddr)
  {
    nMask = pRegisters->mHWVotable.nMask;
  }
  else
  {
    nAddr = pRegisters->nBranchAddr;
    nMask = HAL_CLK_BRANCH_CTRL_REG_CLK_ENABLE_FMSK;
  }

  /*
   * Sanity check
   */
  if(nAddr == 0)
  {
    return FALSE;
  }

  /*
   * Enable clock
   */
  nVal  = inpdw(nAddr) | nMask;
  outpdw(nAddr, nVal);

  /*
   * Wait for the clock to turn on
   */
  nAddr = pRegisters->nBranchAddr;
  while (inpdw(nAddr) & HAL_CLK_BRANCH_CTRL_REG_CLK_OFF_FMSK)
  {
    /*
     * Check for timeout
     */
    if (nTimeout++ >= CLOCK_MBA_UPDATE_TIMEOUT_US)
    {
      return FALSE;
    }

    /*
     * Add enable delay
     */
    mba_modem_wait_microseconds(1);
  }

  return TRUE;

} /* ClockMBA_EnableClock */


/* ===========================================================================
**  ClockMBA_DisableClock
**
** ======================================================================== */
/**
  Disable a clock using the specified register and mask.

  @param
  None

  @return
  None

  @dependencies
  None
*/

static void ClockMBA_DisableClock
(
  const HAL_clk_ClockRegistersType *pRegisters
)
{
  uint32 nAddr, nVal, nMask;

  /*
   * Check for HW votable registers
   */
  nAddr = pRegisters->mHWVotable.nAddr;
  if(nAddr)
  {
    nMask = pRegisters->mHWVotable.nMask;
  }
  else
  {
    nAddr = pRegisters->nBranchAddr;
    nMask = HAL_CLK_BRANCH_CTRL_REG_CLK_ENABLE_FMSK;
  }

  /*
   * Sanity check
   */
  if(nAddr == 0)
  {
    return;
  }

  /*
   * Disable clock
   */
  nVal  = inpdw(nAddr);
  nVal &= ~nMask;
  outpdw(nAddr, nVal);

} /* ClockMBA_DisableClock */


/* =========================================================================
**  Function : ClockMBA_Init
** =========================================================================*/
/**
  Initialize the MBA clock driver.

  @param
  None

  @return
  TRUE -- Init was successful.
  FALSE -- Init failed.

  @dependencies
  None
*/

boolean ClockMBA_Init(void)
{
  uint32 i, nTimeout = 0;

  /* 
   * PBL has Q6/BUS @ 384/72
   * MBA needs to Bump up Q6/BUS to 576/144
   */

  /* 
   * Program  MSS_BUS_CFG_RCGR
   *  MSS HPG 5.5.2 step 1, table 5-11.
   * SRC_SEL = MPLL1_OUT_MAIN
   * SRC_DIV = 576 Div4
   */
  HWIO_OUTF(MSS_BUS_CFG_RCGR, SRC_SEL, 1);
  HWIO_OUTF(MSS_BUS_CFG_RCGR, SRC_DIV, 0x07);

  /* Initiate  MSS_BUS_CMD_RCGR update */ 
  HWIO_OUTF(MSS_BUS_CMD_RCGR, UPDATE, 1);

  while(inpdw(HWIO_MSS_BUS_CMD_RCGR_ADDR) & HWIO_FMSK(MSS_BUS_CMD_RCGR, UPDATE))
  {
    if (nTimeout++ >= 1000)
    {
      return FALSE;
    }
    WAIT_1_MICROSECOND;
  }

  /* Now we are at 384/144*/

  /* Program MSS_Q6_CFG_RCGR
   * Q6 @ 576 MHz sourced from MPLL1_OUT_MAIN
   * 0x1 corresponds to MPLL1_OUT_MAIN
   */
  HWIO_OUTF(MSS_Q6_CFG_RCGR, SRC_SEL, 0x1);
  HWIO_OUTF(MSS_Q6_CFG_RCGR, SRC_DIV, 1);

  /* Initiate MSS_Q6_CMD_RCGR update */ 
  HWIO_OUTF(MSS_Q6_CMD_RCGR, UPDATE, 1);
  
  nTimeout = 0;

  while(inpdw(HWIO_ADDR(MSS_Q6_CMD_RCGR)) & HWIO_FMSK(MSS_Q6_CMD_RCGR, UPDATE))
  {
    if (nTimeout++ >= 1000)
    {
      return FALSE;
    }
    WAIT_1_MICROSECOND;
  }

  /* Now we are at 576/144*/

 /*-----------------------------------------------------------------------*/
  /*  Enable clocks.                                                       */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < ARRAY_SIZE(aClockRegisters); i++)
  {
    if (ClockMBA_EnableClock(&aClockRegisters[i]) != TRUE)
    {
      return FALSE;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Init complete.                                                        */
  /*-----------------------------------------------------------------------*/

  return TRUE;

} /* END ClockMBA_Init */


/* =========================================================================
**  Function : ClockMBA_DeInit
** =========================================================================*/
/**
  Deinitialize the MBA clock driver.

  @param
  None

  @return
  TRUE -- Deinit was successful.
  FALSE -- Deinit failed.

  @dependencies
  None
*/

boolean ClockMBA_DeInit(void)
{
  /*-----------------------------------------------------------------------*/
  /*  Disable clocks (reverse order of init).                              */
  /*-----------------------------------------------------------------------*/

  ClockMBA_DisableClock(&aClockRegisters[5]);
  ClockMBA_DisableClock(&aClockRegisters[4]);
  ClockMBA_DisableClock(&aClockRegisters[3]);
  ClockMBA_DisableClock(&aClockRegisters[2]);
  ClockMBA_DisableClock(&aClockRegisters[1]);
  ClockMBA_DisableClock(&aClockRegisters[0]);

  /*-----------------------------------------------------------------------*/
  /* Deinit complete.                                                      */
  /*-----------------------------------------------------------------------*/

  return TRUE;

} /* END ClockMBA_DeInit */

