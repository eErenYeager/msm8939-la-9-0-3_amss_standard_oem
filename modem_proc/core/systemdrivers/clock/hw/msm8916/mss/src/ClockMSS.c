
/*
===========================================================================
*/
/**
  @file ClockMSS.c 
  
  Main entry point for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2011-12 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8916/mss/src/ClockMSS.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  10/19/11   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "ClockDriver.h"
#include "ClockMSS.h"
#include "ClockAVS.h"
#include "ClockSWEVT.h"
#include <CoreIni.h>


/*=========================================================================
      Macros
==========================================================================*/

/*
 * Clock configuration names in EFS .ini file
 */
#define CLOCK_EFS_INI_FILENAME              "/nv/item_files/clock/clock_mss.ini"
#define CLOCK_EFS_GLOBAL_CONFIG_SECTION     "global"
#define CLOCK_EFS_GLOBAL_DVS_FLAG           "dvs"
#define CLOCK_EFS_QDSP6_CONFIG_SECTION      "QDSP6"
#define CLOCK_EFS_QDSP6_DCS_FLAG            "dcs"

/*
 * Clock ID's
 */
#define CLOCK_ID_CPU                        "clk_q6"
#define CLOCK_ID_MSS_CONFIG_BUS             "clk_mss_config_bus"
#define CLOCK_ID_CRYPTO                     "clk_bus_crypto"


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Extern Definitions
==========================================================================*/


/*=========================================================================
      Data
==========================================================================*/


static ClockMSSCtxtType Clock_MSSCtxt;

 
/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_FindCPUMaxConfigAtVoltage
** =========================================================================*/
/**
  Finds the maximum CPU config at the specified voltage level.

  @param *pDrvCtxt[in] -- Pointer to driver context.
  @param **pConfig[in] -- Pointer to CPU config pointer.
  @param nCorner[in]   -- Corner id from pmapp_npa.h

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/

DALResult Clock_FindCPUMaxConfigAtVoltage
(
  ClockDrvCtxt         *pDrvCtxt,
  ClockQDSP6ConfigType **pConfig,
  uint32                nCorner
)
{
  ClockMSSCtxtType     *pImageCtxt;
  ClockQDSP6ConfigType  *pCPUConfig, *pTempConfig;
  uint32                nMinPL, nMaxPL, nPL, nCfg;
  

   pImageCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  /*-----------------------------------------------------------------------*/
  /* Validate argument.                                                    */
  /*-----------------------------------------------------------------------*/

  if (pConfig == NULL)
  {
    return DAL_ERROR;
  }
  
  /*-----------------------------------------------------------------------*/
  /* Find the highest frequency configuration.                             */
  /*-----------------------------------------------------------------------*/

  nMinPL = pImageCtxt->QDSP6Ctxt.pQDSP6PerfConfig->nMinPerfLevel;
  nMaxPL = pImageCtxt->QDSP6Ctxt.pQDSP6PerfConfig->nMaxPerfLevel;
  pCPUConfig = pImageCtxt->pBSPConfig->pQDSP6Config;
  pTempConfig = NULL;
  
  for (nPL = nMinPL; nPL <= nMaxPL; nPL++)
  {
    nCfg = pImageCtxt->QDSP6Ctxt.pQDSP6PerfConfig->anPerfLevel[nPL];

    if (pCPUConfig[nCfg].Mux.eVRegLevel > nCorner)
    {
      break;
    }
    else
    {
      pTempConfig = &pCPUConfig[nCfg];
    }
  }
  
  /*-----------------------------------------------------------------------*/
  /* Validate config was found.                                            */
  /*-----------------------------------------------------------------------*/

  if (pTempConfig == NULL)
  {
    return DAL_ERROR;
  }
  
  /*-----------------------------------------------------------------------*/
  /* Store config for caller.                                              */
  /*-----------------------------------------------------------------------*/

  *pConfig = pTempConfig;

  return DAL_SUCCESS;

} /* END Clock_FindCPUMaxConfigAtVoltage */


/* =========================================================================
**  Function : Clock_DetectPVSData
** =========================================================================*/
/**
  Detects efuse settings and keeps a handle to the appropriate PVS data.
 
  This function is invoked at driver initialization to detect efuse
  settings to distinguish b/w slow, nominal and fast part.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if detection fails, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_DetectPVSData
(
  ClockDrvCtxt *pDrvCtxt
)
{

  /*-----------------------------------------------------------------------*/
  /* Nothing needed here - VRegCornerData is part of BSP Config.           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;
}


/* =========================================================================
**  Function : Clock_DetectBSPVersion
** =========================================================================*/
/**
  Detects which BSP configuration to use for the current HW.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if a valid configuration was not found, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_DetectBSPVersion
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockMSSCtxtType         *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockQDSP6PerfConfigType *pQDSP6PerfConfig;

  /*-----------------------------------------------------------------------*/
  /* Detect which BSP data to use for this HW version.                     */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig = NULL;
  pQDSP6PerfConfig = pMSSCtxt->pBSPConfig->pQDSP6PerfConfig;
  while(pQDSP6PerfConfig->anPerfLevel != NULL)
  {
    if (Clock_IsBSPSupported(&pQDSP6PerfConfig->HWVersion) == TRUE)
    {
      pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig = pQDSP6PerfConfig;
      break;
    }
    else
    {
      /*
       * Continue searching through the next item in the array
       */
      pQDSP6PerfConfig++;
    }
  }

  if (pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig == NULL)
  {
    return DAL_ERROR;
  }

  return DAL_SUCCESS;
}

/* =========================================================================
**  Function : Clock_DetectQDSP6Config
** =========================================================================*/
/**
  Detects current configuration of QDSP6 clock
 
  This function is invoked at driver initialization to detect the current
  QDSP6 configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_DetectQDSP6Config
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32                      nCfg, nDiv2x, nSrcIdx, nPL;
  HAL_clk_ClockMuxConfigType  ActiveMuxCfg;
  HAL_clk_PLLConfigType       ActivePLLCfg;
  ClockNodeType              *pClock;
  ClockQDSP6PerfConfigType   *pPerfConfig;
  ClockMSSCtxtType           *pMSSCtxt;
  ClockSourceNodeType        *pSourceDetected;
  ClockSourceFreqConfigType  *pSourceFreqConfig;
  HAL_clk_SourceType          eSourceDetected;
  ClockQDSP6ConfigType       *pPerfLevelConfig, *pPerfLevelConfigMatch;

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Make sure we have a valid BSP version.                                */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig == NULL)
  {
    return DAL_ERROR;
  }

  pPerfConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig;

  /*-----------------------------------------------------------------------*/
  /* Get proper clock and configuration data                               */
  /*-----------------------------------------------------------------------*/

  pClock = pMSSCtxt->QDSP6Ctxt.pQDSP6Clock;

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);  

  /*-----------------------------------------------------------------------*/
  /* Get the current hardware configuration.                               */
  /*-----------------------------------------------------------------------*/

  HAL_clk_DetectClockMuxConfig(pClock->pDomain->HALHandle, &ActiveMuxCfg);
  HAL_clk_DetectPLLConfig(ActiveMuxCfg.eSource, &ActivePLLCfg);

  /*-----------------------------------------------------------------------*/
  /* If we got back divider of 0, set to 2 for comparison.                 */
  /*-----------------------------------------------------------------------*/

  if (ActiveMuxCfg.nDiv2x == 0)
  {
    ActiveMuxCfg.nDiv2x = 2;
  }
  
  /*-----------------------------------------------------------------------*/
  /* Validate we have BSP data for the detected source.                    */
  /*-----------------------------------------------------------------------*/

  eSourceDetected = ActiveMuxCfg.eSource;
  nSrcIdx = pDrvCtxt->anSourceIndex[eSourceDetected];
  if (nSrcIdx == 0xFF)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return DAL_ERROR;
  }

  pSourceDetected = &pDrvCtxt->aSources[nSrcIdx];

  if (pSourceDetected == NULL ||
      pSourceDetected->pBSPConfig == NULL ||
      pSourceDetected->pBSPConfig->pSourceFreqConfig == NULL)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Go through config array to find a matching configuration.             */
  /*-----------------------------------------------------------------------*/

  pPerfLevelConfigMatch = NULL;
  for (nPL = 0;
       nPL <= pPerfConfig->nMaxPerfLevel;
       nPL++)
  {
    nCfg = pPerfConfig->anPerfLevel[nPL];
    pPerfLevelConfig = &pMSSCtxt->pBSPConfig->pQDSP6Config[nCfg];

    nDiv2x = pPerfLevelConfig->Mux.HALConfig.nDiv2x;
    if (nDiv2x == 0)
    {
        nDiv2x = 2;
    }

    /*
     * Check for matching source and divider.
     */
    if (pPerfLevelConfig->Mux.HALConfig.eSource == eSourceDetected &&
        nDiv2x == ActiveMuxCfg.nDiv2x)
    {
      /*
       * Check if this BSP has a source freq config for us to validate.
       */
      if (pPerfLevelConfig->Mux.pSourceFreqConfig == NULL)
      {
        pPerfLevelConfigMatch = pPerfLevelConfig;
        break;
      }

      /*
       * Check if the BSP source freq config matches the detected PLL config.
       */
      else
      {
        pSourceFreqConfig = pPerfLevelConfig->Mux.pSourceFreqConfig;
        if (pSourceFreqConfig != NULL &&
            (pSourceFreqConfig->HALConfig.nPreDiv  == ActivePLLCfg.nPreDiv &&
             pSourceFreqConfig->HALConfig.nPostDiv == ActivePLLCfg.nPostDiv &&
             pSourceFreqConfig->HALConfig.nL       == ActivePLLCfg.nL &&
             pSourceFreqConfig->HALConfig.nM       == ActivePLLCfg.nM &&
             pSourceFreqConfig->HALConfig.nN       == ActivePLLCfg.nN))
        {
          pPerfLevelConfigMatch = pPerfLevelConfig;
          break;
        }
      }
    }
  }

  if (pPerfLevelConfigMatch == NULL)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the clock node with the configuration.                         */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->QDSP6Ctxt.pQDSP6Config = pPerfLevelConfigMatch;
  pClock->pDomain->pActiveMuxConfig = &pPerfLevelConfigMatch->Mux;
  pClock->pDomain->pSource = pSourceDetected;

  /*-----------------------------------------------------------------------*/
  /* We assume the CX voltage requirement for the current CPU frequency is */
  /* is already met: pCfg->Mux.eVRegLevel was satisfied at boot.           */
  /* We update the driver context's initial vreg corner level if the       */
  /* current CX corner level is greater than the default from the BSP.     */
  /* The initial vreg level allows us to check if we're attempting to      */
  /* request a voltage corner above the current level prior to             */
  /* establishing connection with RPM.                                     */
  /*-----------------------------------------------------------------------*/

  if (pPerfLevelConfigMatch->Mux.eVRegLevel >
      pDrvCtxt->VRegConfig.eInitLevel)
  {
     pDrvCtxt->VRegConfig.eMinLevel =
     pDrvCtxt->VRegConfig.eInitLevel = pPerfLevelConfigMatch->Mux.eVRegLevel;
  }

  if (pPerfLevelConfigMatch->Mux.eVRegLevel >
      pDrvCtxt->VRegConfigSuppressible.eInitLevel)
  {
    pDrvCtxt->VRegConfigSuppressible.eInitLevel = pPerfLevelConfigMatch->Mux.eVRegLevel;
  }

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);  

  return DAL_SUCCESS;

} /* END Clock_DetectQDSP6Config */


/* =========================================================================
**  Function : Clock_DetectMSSConfigBusConfig
** =========================================================================*/
/**
  Detects current configuration of the MSS config bus clock

  This function is invoked at driver initialization to detect the current
  MSS config bus configuration.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_DetectMSSConfigBusConfig
(
  ClockDrvCtxt *pDrvCtxt
)
{
  uint32                        nCfg, nDiv2x, nSrcIdx, nPL;
  HAL_clk_ClockMuxConfigType    ActiveMuxCfg;
  ClockNodeType                *pClock;
  ClockConfigBusPerfConfigType *pPerfConfig;
  ClockMSSCtxtType             *pMSSCtxt;
  ClockSourceNodeType          *pSourceDetected;
  HAL_clk_SourceType            eSourceDetected;
  ClockConfigBusConfigType     *pPerfLevelConfig, *pPerfLevelConfigMatch;

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Make sure we have a valid BSP version.                                */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->pBSPConfig->pConfigBusPerfConfig == NULL)
  {
    return DAL_ERROR;
  }

  pPerfConfig = pMSSCtxt->pBSPConfig->pConfigBusPerfConfig;

  /*-----------------------------------------------------------------------*/
  /* Get proper clock and configuration data                               */
  /*-----------------------------------------------------------------------*/

  pClock = Clock_MSSCtxt.ConfigBusCtxt.pConfigBusClock;

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Get the current hardware configuration.                               */
  /*-----------------------------------------------------------------------*/

  HAL_clk_DetectClockMuxConfig(pClock->pDomain->HALHandle, &ActiveMuxCfg);

  /*
   * If we got back divider of 0, set to 2 for comparison.
   */
  if (ActiveMuxCfg.nDiv2x == 0)
  {
    ActiveMuxCfg.nDiv2x = 2;
  }

  /*-----------------------------------------------------------------------*/
  /* Validate we have BSP data for the detected source.                    */
  /*-----------------------------------------------------------------------*/

  eSourceDetected = ActiveMuxCfg.eSource;
  nSrcIdx = pDrvCtxt->anSourceIndex[eSourceDetected];
  if (nSrcIdx == 0xFF)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return DAL_ERROR;
  }

  pSourceDetected = &pDrvCtxt->aSources[nSrcIdx];

  if (pSourceDetected == NULL ||
      pSourceDetected->pBSPConfig == NULL ||
      pSourceDetected->pBSPConfig->pSourceFreqConfig == NULL)
  {
    DALCLOCK_FREE(pDrvCtxt);

    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Go through config array to find a matching configuration.             */
  /*-----------------------------------------------------------------------*/

  pPerfLevelConfigMatch = NULL;
  for (nPL = pPerfConfig->nMinPerfLevel;
       nPL <= pPerfConfig->nMaxPerfLevel;
       nPL++)
  {
    nCfg = pPerfConfig->anPerfLevel[nPL];
    pPerfLevelConfig = &pMSSCtxt->pBSPConfig->pConfigBusConfig[nCfg];

    nDiv2x = pPerfLevelConfig->Mux.HALConfig.nDiv2x;
    if (nDiv2x == 0)
    {
        nDiv2x = 2;
    }

    if (pPerfLevelConfig->Mux.HALConfig.eSource == eSourceDetected &&
        nDiv2x == ActiveMuxCfg.nDiv2x)
    {
      pPerfLevelConfigMatch = pPerfLevelConfig;
      break;
    }
  }

  if (pPerfLevelConfigMatch == NULL)
  {
    DALCLOCK_FREE(pDrvCtxt);
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the clock node with the matched configuration.                 */
  /*-----------------------------------------------------------------------*/
  pMSSCtxt->ConfigBusCtxt.pConfigBusConfig = pPerfLevelConfigMatch;
  pClock->pDomain->pActiveMuxConfig = &pPerfLevelConfigMatch->Mux;
  pClock->pDomain->pSource = pSourceDetected;

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);

  return DAL_SUCCESS;

} /* END Clock_DetectMSSConfigBusConfig */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult              eRes;
  ClockPropertyValueType PropVal;

  /*-----------------------------------------------------------------------*/
  /* Get the CPU BSP Configuration.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_GetPropertyValue("ClockMSSBSP", &PropVal);
  if (eRes != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  Clock_MSSCtxt.pBSPConfig = (ClockMSSBSPConfigType *)PropVal;

  /*-----------------------------------------------------------------------*/
  /* Assign the image context.                                             */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt->pImageCtxt = &Clock_MSSCtxt;

  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  /*-----------------------------------------------------------------------*/
  /* Initialize the /vdd/mss mutex resource.                               */
  /*-----------------------------------------------------------------------*/

  eRes =
    DALSYS_SyncCreate(
      DALSYS_SYNC_ATTR_RESOURCE,
      &Clock_MSSCtxt.VDDMSSCtxt.hLock,
      &Clock_MSSCtxt.VDDMSSCtxt.LockObj);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "DALSYS_SyncCreate failed on /vdd/mss mutex resource.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Detect the BSP version to use.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectBSPVersion(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Failed to detect the BSP version.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Get QDSP6 core/cpu clock ID.                                          */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_CPU,
      (ClockIdType *)&Clock_MSSCtxt.QDSP6Ctxt.pQDSP6Clock);

  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get QDSP6 core clock ID.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Get initial VDD/MSS voltage level from BSP.                           */
  /*-----------------------------------------------------------------------*/

  if ((Clock_MSSCtxt.pBSPConfig->eVDDMSSVRegInitLevel ==
        CLOCK_VREG_MSS_CORNER_OFF)                    ||
      (Clock_MSSCtxt.pBSPConfig->eVDDMSSVRegInitLevel >=
        CLOCK_VREG_MSS_NUM_CORNERS))
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "Invalid initial VDD/MSS corner value in BSP. Setting to NOMINAL.");

    Clock_MSSCtxt.VDDMSSCtxt.eVRegMSSCornerInit =
      CLOCK_VREG_MSS_CORNER_NOMINAL;
  }
  else
  {
    Clock_MSSCtxt.VDDMSSCtxt.eVRegMSSCornerInit =
      Clock_MSSCtxt.pBSPConfig->eVDDMSSVRegInitLevel;
  }

  /*-----------------------------------------------------------------------*/
  /* Detect Q6 efuse settings and update voltage data depending on slow,   */
  /* nominal or fast part.                                                 */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectPVSData(pDrvCtxt);

  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_WARNING,
      "Unable to detect QDSP6 efuse settings - assuming SLOW part.");
  }

  /*-----------------------------------------------------------------------*/
  /* Detect initial QDSP6 core clock frequency configuration.              */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectQDSP6Config(pDrvCtxt);

  if (eRes != DAL_SUCCESS) 
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect QDSP6 core clock configuration.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Ensure that the QDSP6 core clock/domain/source reference counts are 1.*/
  /*-----------------------------------------------------------------------*/
 
  Clock_EnableClock(pDrvCtxt, (ClockIdType)Clock_MSSCtxt.QDSP6Ctxt.pQDSP6Clock);

  /*-----------------------------------------------------------------------*/
  /* Initialize the modem PLL resources.                                   */
  /*-----------------------------------------------------------------------*/

  Clock_InitPLL(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* As of now for 8916, we are configuring GPLL1 on modem. Later we will  */
  /* to boot once we start working on 8936.                                */
  /*-----------------------------------------------------------------------*/
  Clock_InitSource(pDrvCtxt, HAL_CLK_SOURCE_GPLL1, NULL);

  /*-----------------------------------------------------------------------*/
  /* Initialize AVS.                                                       */
  /*-----------------------------------------------------------------------*/

  if(Clock_InitAVS(pDrvCtxt, NULL) != DAL_SUCCESS)
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the DCVS module.                                           */
  /*-----------------------------------------------------------------------*/

  Clock_InitDCVS(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize /vdd/mss NPA node.                                         */
  /*-----------------------------------------------------------------------*/

#if 0  /* There is no vdd/mss on 8916. */
  Clock_InitMSSVDD(pDrvCtxt);
#endif

  /*-----------------------------------------------------------------------*/
  /* Initialize the XO module.                                             */
  /*-----------------------------------------------------------------------*/
   
  Clock_InitXO(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Get MSS Config Bus clock ID.                                          */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_MSS_CONFIG_BUS,
      (ClockIdType *)&Clock_MSSCtxt.ConfigBusCtxt.pConfigBusClock);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get MSS config bus clock ID.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Detect initial MSS config bus clock frequency configuration.          */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectMSSConfigBusConfig(pDrvCtxt);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect MSS config bus clock configuration.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Ensure that MSS config bus clock/domain/source reference counts are 1.*/
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(
    pDrvCtxt, (ClockIdType)Clock_MSSCtxt.ConfigBusCtxt.pConfigBusClock);

  /*-----------------------------------------------------------------------*/
  /* Get Crypto clock ID.                                                  */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_CRYPTO,
      (ClockIdType *)&Clock_MSSCtxt.ConfigBusCtxt.pCryptoClock);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to get Crypto clock ID.");
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize MSS Config Bus module.                                     */
  /*-----------------------------------------------------------------------*/

  Clock_InitConfigBus(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_SetCPUMaxFrequencyAtCurrentVoltage
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_SetCPUMaxFrequencyAtCurrentVoltage
(
  ClockDrvCtxt *pDrvCtxt,
  boolean       bRequired,
  uint32       *pnResultFreqHz
)
{
  DALResult              eRes;
  uint32                 nMinPL, nMaxPL, nPL, nCfg;
  ClockVRegLevelType    eVRegLevel;
  ClockVRegConfigType  *pVRegConfig = &pDrvCtxt->VRegConfig;
  ClockVRegConfigType  *pVRegConfigSuppressible = &pDrvCtxt->VRegConfigSuppressible;
  ClockQDSP6ConfigType  *pQDSP6Config = Clock_MSSCtxt.pBSPConfig->pQDSP6Config;
  ClockQDSP6ConfigType *pNewQDSP6Config = pQDSP6Config;

  if (bRequired == TRUE)
  {
    /*
     * Find the highest frequency configuration supported by the clock driver's
     * current vote (suppressible and non-suppressible) on the power rail.
     */
    nMinPL = Clock_MSSCtxt.QDSP6Ctxt.pQDSP6PerfConfig->nMinPerfLevel;
    nMaxPL = Clock_MSSCtxt.QDSP6Ctxt.pQDSP6PerfConfig->nMaxPerfLevel;

    if (pVRegConfig->eMinLevel >= pVRegConfigSuppressible->eMinLevel)
    {
      eVRegLevel = pVRegConfig->eMinLevel;
    }
    else
    {
      eVRegLevel = pVRegConfigSuppressible->eMinLevel;
    }

    for (nPL = nMinPL; nPL <= nMaxPL; nPL++)
    {
      nCfg = Clock_MSSCtxt.pBSPConfig->pQDSP6PerfConfig->anPerfLevel[nPL];

      if (pQDSP6Config[nCfg].Mux.eVRegLevel > eVRegLevel)
      {
        break;
      }
      else
      {
        pNewQDSP6Config = &pQDSP6Config[nCfg];
      }
    }

    /*
     * Configure the Q6 to the desired performance level if the current
     * frequency is lower.
     */
    if (Clock_MSSCtxt.QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz <
          pNewQDSP6Config->Mux.nFreqHz)
    {
      Clock_SetQDSP6Config(pDrvCtxt, pNewQDSP6Config);

      /*
       * Overwrite the active state (in KHz) of the NPA node.
       */
      eRes = Clock_NPACPUResourceOverwriteActiveState(
               (npa_resource_state)(pNewQDSP6Config->Mux.nFreqHz / 1000));

      if (eRes != DAL_SUCCESS)
      {
        if (pnResultFreqHz != NULL)
        {
          *pnResultFreqHz = Clock_MSSCtxt.QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz;
        }

        return eRes;
      }
    }

    if (pnResultFreqHz != NULL)
    {
      *pnResultFreqHz = Clock_MSSCtxt.QDSP6Ctxt.pQDSP6Config->Mux.nFreqHz;
    }
  }
  else
  {
    /*
     * The max CPU at current voltage level is no longer required.  We can 
     * request that NPA re-aggregate the CPU requests. 
     */
    if (Clock_MSSCtxt.QDSP6Ctxt.hNPAClkCPUImpulse != NULL)
    {
      npa_issue_impulse_request(Clock_MSSCtxt.QDSP6Ctxt.hNPAClkCPUImpulse);

      if (pnResultFreqHz != NULL)
      {
        *pnResultFreqHz = 0;
      }
    }
  }

  return DAL_SUCCESS;

} /* END Clock_SetCPUMaxFrequencyAtCurrentVoltage */


/* =========================================================================
**  Function : Clock_ProcessorSleep
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorSleep
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
)
{
  return DAL_SUCCESS;

} /* END Clock_ProcessorSleep */


/* =========================================================================
**  Function : Clock_ProcessorRestore
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorRestore
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
) 
{
  return DAL_SUCCESS;
  
} /* END Clock_ProcessorRestore */


/* =========================================================================
**  Function : Clock_LoadNV
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_LoadNV
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockVDDMSSCtxtType *pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;
  CoreConfigHandle hConfig = NULL;
  boolean bEnableDCSQ6  = TRUE;
  boolean bEnableMSSVDD = TRUE; 

  /*-----------------------------------------------------------------------*/
  /* Read clock configuration file.                                        */
  /*-----------------------------------------------------------------------*/

  hConfig = CoreIni_ConfigCreate(CLOCK_EFS_INI_FILENAME);

  if(hConfig != NULL)
  { 
    /*
     * Read global DVS (dynamic voltage scaling) flag for /mss/vdd
     */
    CoreConfig_ReadBool( hConfig, 
                         CLOCK_EFS_GLOBAL_CONFIG_SECTION, 
                         CLOCK_EFS_GLOBAL_DVS_FLAG, 
                         &bEnableMSSVDD );

    /*
     * Read QDSP6 DCS (dynamic clock/freq scaling) flag
     */
    CoreConfig_ReadBool( hConfig, 
                         CLOCK_EFS_QDSP6_CONFIG_SECTION, 
                         CLOCK_EFS_QDSP6_DCS_FLAG, 
                         &bEnableDCSQ6 );

    /*
     * TODO: Any other {key,value} combinations to read from EFS                    
     * should go here. 
     * ...                                                                 
     * ...                                                                       
     * ...                                                                          
     */


    /*
     * Destory the handle.
     */
    CoreIni_ConfigDestroy( hConfig );
  }

  /*-----------------------------------------------------------------------*/
  /* Update the /vdd/mss DVS flag.                                         */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /* NOTE: We use the /vdd/mss lock to synchronize with the consumers of   */
  /*       this resources.                                                 */
  /* NOTE: We disable voltage scaling if clock scaling is disabled.        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pVDDMSSCtxt);

  pMSSCtxt->VDDMSSCtxt.nDisableMSSVDD &= ~CLOCK_FLAG_WAITING_FOR_EFS_INIT;

  if(!bEnableMSSVDD || !bEnableDCSQ6)
  {
    pMSSCtxt->VDDMSSCtxt.nDisableMSSVDD |= CLOCK_FLAG_DISABLED_BY_EFS;
  }

  /*-----------------------------------------------------------------------*/
  /* Free using the /vdd/mss lock - see note above.                        */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pVDDMSSCtxt);

  /*-----------------------------------------------------------------------*/
  /* Update the Q6 CPU DCS flag.                                           */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /* NOTE: We use the main clock driver lock to synchronize with the       */
  /*       consumers/readers of this resource.                             */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pDrvCtxt);

  pMSSCtxt->QDSP6Ctxt.nDisableDCS &= ~CLOCK_FLAG_WAITING_FOR_EFS_INIT;

  if(!bEnableDCSQ6)
  {
    pMSSCtxt->QDSP6Ctxt.nDisableDCS |= CLOCK_FLAG_DISABLED_BY_EFS;
  }

  /*-----------------------------------------------------------------------*/
  /* Free using main clock driver lock - see note above.                   */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pDrvCtxt);  

  /*-----------------------------------------------------------------------*/
  /* Trigger QDSP6 CPU state update.                                       */
  /*-----------------------------------------------------------------------*/

  if (!pMSSCtxt->QDSP6Ctxt.nDisableDCS)
  {
    Clock_NPATriggerCPUStateUpdate(pDrvCtxt);
  }

  return DAL_SUCCESS;

} /* END Clock_LoadNV */


/* =========================================================================
**  Function : Clock_ImageBIST
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult Clock_ImageBIST
(
  ClockDrvCtxt  *pDrvCtxt,
  boolean       *bBISTPassed,
  uint32        *nFailedTests
)
{

  /*
   * Nothing to do yet.
   */
  return DAL_SUCCESS;

} /* END Clock_ImageBIST */

