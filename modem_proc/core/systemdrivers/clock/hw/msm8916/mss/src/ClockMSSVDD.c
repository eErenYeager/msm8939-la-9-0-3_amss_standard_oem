/*
===========================================================================
*/
/**
  @file ClockMSSVDD.c 
  
  /vdd/mss NPA node definitions for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8916/mss/src/ClockMSSVDD.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/17/12   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockDriver.h"
#include "ClockMSS.h"
#include "DALDeviceId.h"
#include "ClockAVS.h"
#include "ClockMSSHWIO.h"
#include "ClockSWEVT.h"

#include <DALSys.h>
#include <npa.h>
#include <npa_resource.h>
#include <npa_remote.h>
#include <npa_remote_resource.h>


/*=========================================================================
      Macros
==========================================================================*/


/*
 * NPA Dependency handles
 */
#define NPA_DEPENDENCY_PMIC_VDD_MEM()                   \
  NPA_DEPENDENCY(Clock_NPAMSSVDDNonCPUResources.resource.handle, 0)


/*=========================================================================
      Type Definitions
==========================================================================*/


/*
 * Clock_NPAMSSVDDNonCPUResourcesType
 *
 * Structure containing the NPA node and resource data for /vdd/mss.
 * 
 * resource       - resource data   - /vdd/mss
 * node           - node data       - /node/vdd/mss
 * aDependency[0] - dependency data - /pmic/client/vdd_mem_uvol
 */
typedef struct
{
  npa_resource_definition   resource;
  npa_node_definition       node;
  npa_node_dependency       aDependency[1];
} Clock_NPAMSSVDDNonCPUResourcesType;


/*=========================================================================
      Prototypes
==========================================================================*/


static npa_resource_state Clock_NPAMSSVDDNonCPUDriverFunc
(
  struct npa_resource *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState
);


static void Clock_CallbackNodeAvailableMSSVDDCoreCpuVdd
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
);


static void Clock_CallbackNodeAvailableMSSVDDNonCPU
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
);


static npa_query_status Clock_NPAMSSVDDResourceQuery
(
  npa_resource   *pResource,
  unsigned int    nID,
  npa_query_type *pResult
);


static void Clock_MXReadyCallback
(
  void *pContext
);


/*=========================================================================
      Data
==========================================================================*/

/*
 * Define resources for the /vdd/mss node.
 */
static Clock_NPAMSSVDDNonCPUResourcesType Clock_NPAMSSVDDNonCPUResources =
{
  /*
   * resource
   */
  {
    CLOCK_NPA_NODE_NAME_VDD_MSS,
    "Vreg Level",
    CLOCK_VREG_MSS_CORNER_MAX,
    &npa_max_plugin, 
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)NULL,
    Clock_NPAMSSVDDResourceQuery
  },

  /*
   * node
   */
  { 
    "/node" CLOCK_NPA_NODE_NAME_VDD_MSS,       /* name       */
    Clock_NPAMSSVDDNonCPUDriverFunc,           /* driver_fcn */
    NPA_NODE_DEFAULT,                          /* attributes */
    NULL,                                      /* data       */
    NPA_ARRAY(Clock_NPAMSSVDDNonCPUResources.aDependency),
    1, &Clock_NPAMSSVDDNonCPUResources.resource
  },
  /*
   * aDependency
   */
  {
    {
      NPA_NODE_NAME_PMIC_VDD_MX,
      NPA_CLIENT_REQUIRED
    }
  }
};


/*=========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_MapCornerToTableIndex
** =========================================================================*/
/**
  Retrieves the data for a particular corner.

  @param pDrvCtxt    [in] -- Pointer to the driver context.
  @param eMSScorner  [in] -- Corner for /vdd/mss.
  @param pCornerData [in] -- Pointer to the corner data.

  @return
  None.

  @dependencies
  None.
*/

uint32 Clock_MapCornerToTableIndex
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType eMSSCorner
)
{
  uint32 nCornerDataIndex;

  /*-----------------------------------------------------------------------*/
  /* Cap the corner to the max supported.                                  */
  /*-----------------------------------------------------------------------*/

  if (eMSSCorner > CLOCK_VREG_MSS_CORNER_MAX)
  {
    nCornerDataIndex = (uint32)CLOCK_VREG_MSS_CORNER_MAX;
  }
  else
  {
    nCornerDataIndex = (uint32)eMSSCorner;
  }

  /*-----------------------------------------------------------------------*/
  /* Subtract 1 since the voltage table doesn't contain OFF values.        */
  /*-----------------------------------------------------------------------*/

  if (nCornerDataIndex > 0)
  {
    nCornerDataIndex--;
  }

  return nCornerDataIndex;

} /* END of Clock_MapCornerToTableIndex */


/* =========================================================================
**  Function : Clock_GetCornerData
** =========================================================================*/
/**
  Retrieves the data for a particular corner.

  @param pDrvCtxt    [in] -- Pointer to the driver context.
  @param eMSScorner  [in] -- Corner for /vdd/mss.
  @param pCornerData [in] -- Pointer to the corner data.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_GetCornerData
(
  ClockDrvCtxt                    *pDrvCtxt,
  ClockVRegMSSCornerType           eMSSCorner,
  const ClockVRegCornerDataType  **pCornerData
)
{
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  const ClockVRegCornerDataType *pCornerDataFirst;
  uint32 nCornerDataIndex;

  /*-----------------------------------------------------------------------*/
  /* Validate the arguments.                                               */
  /*-----------------------------------------------------------------------*/

  if (pCornerData == NULL)
  {
    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the first corner voltage data.                                    */
  /*-----------------------------------------------------------------------*/

  pCornerDataFirst = pMSSCtxt->pBSPConfig->pVRegCornerData;
  if (pCornerDataFirst == NULL)
  {
    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Map the corner to a table index.                                      */
  /*-----------------------------------------------------------------------*/

  nCornerDataIndex = Clock_MapCornerToTableIndex(pDrvCtxt, eMSSCorner);

  /*-----------------------------------------------------------------------*/
  /* Update the client's pointer.                                          */
  /*-----------------------------------------------------------------------*/

  *pCornerData = &pCornerDataFirst[nCornerDataIndex];

  return;

} /* END of Clock_GetCornerData */


/* =========================================================================
**  Function : Clock_VoltageRangeCheck
** =========================================================================*/
/**
  Range checking function to validate a voltage is usable.

  @param pDrvCtxt    [in] -- Pointer to the driver context.
  @param eMSScorner  [in] -- Corner for /vdd/mss.
  @param nVoltageUV  [in] -- Voltage to check.

  @return
  boolean.

  @dependencies
  None.
*/

static boolean Clock_VoltageRangeCheck
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eVRegMSSCorner,
  uint32                  nVoltageUV
)
{
  boolean bIsInRange = FALSE;
  const ClockVRegCornerDataType *pCornerData = NULL;

  /*-----------------------------------------------------------------------*/
  /* Get the corner data.                                                  */
  /*-----------------------------------------------------------------------*/

  Clock_GetCornerData(pDrvCtxt, eVRegMSSCorner, &pCornerData);
  if (pCornerData != NULL)
  {
    if (nVoltageUV >= pCornerData->nUVMin &&
        nVoltageUV <= pCornerData->nUVMax)
    {
      bIsInRange = TRUE;
    }
  }

  return bIsInRange;
}


/* =========================================================================
**  Function : Clock_MapVDDMSSCornerToVoltage
** =========================================================================*/
/**
  Maps an MSS rail corner to a raw voltage.

  This function takes in an MSS voltage rail corner and returns a raw voltage.

  @param pDrvCtxt   [in] -- Pointer to the driver context.
  @param eMSScorner [in] -- Corner for /vdd/mss.

  @return
  Raw voltage in uV.

  @dependencies
  None.
*/

uint32 Clock_MapVDDMSSCornerToVoltage
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eMSSCorner
)
{
  const ClockVRegCornerDataType *pCornerData = NULL;
  uint32 nVoltageUV = 0;

  /*-----------------------------------------------------------------------*/
  /* Get the corner data.                                                  */
  /*-----------------------------------------------------------------------*/

  Clock_GetCornerData(pDrvCtxt, eMSSCorner, &pCornerData);
  if (pCornerData != NULL)
  {
    nVoltageUV = pCornerData->nUVMax;
  }

  return nVoltageUV;

} /* END of Clock_MapVDDMSSCornerToVoltage */


/* =========================================================================
**  Function : Clock_MapCornerToAccMemSel
** =========================================================================*/
/**
  Maps a MSS voltage rail corner to an ACC memory select setting.

  This function maps VDD_MSS corners to ACC settings.

  @param eVRegMSSCorner [in] -- /vdd/mss corner to map.

  @return
  ACC memory select setting.

  @dependencies
  None.
*/

static uint32 Clock_MapCornerToAccMemSel
(
  ClockVRegMSSCornerType eVRegMSSCorner
)
{
  switch(eVRegMSSCorner)
  {
    case CLOCK_VREG_MSS_CORNER_TURBO:
    case CLOCK_VREG_MSS_CORNER_NOMINAL_PLUS:   return 0x3;
    case CLOCK_VREG_MSS_CORNER_NOMINAL:        return 0x1;
    case CLOCK_VREG_MSS_CORNER_LOW_PLUS:
    case CLOCK_VREG_MSS_CORNER_LOW:
    case CLOCK_VREG_MSS_CORNER_LOW_MINUS:      return 0x0;

    default:
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Invalid /vdd/mss corner passed to mapping function.");

      /*
       * This return value is necessary for the compiler even with ERR_FATAL.
       */
      return 0;
  }

} /* END of CLOCK_MapCornerToAccMemSel */


/* =========================================================================
**  Function : Clock_MapAccMemSelToCorner
** =========================================================================*/
/**
  Maps an MSS ACC memory select setting to a voltage rail corner.

  This function maps ACC settings to VDD_MSS corners.

  @param nACCSetting [in] -- /vdd/mss ACC setting to map.

  @return
  /vdd/mss voltage rail corner.

  @dependencies
  None.
*/

static ClockVRegMSSCornerType Clock_MapAccMemSelToCorner
(
  uint32 nACCSetting
)
{
  switch(nACCSetting)
  {
    case 0x3: return CLOCK_VREG_MSS_CORNER_TURBO;
    case 0x1: return CLOCK_VREG_MSS_CORNER_NOMINAL;
    case 0x0: return CLOCK_VREG_MSS_CORNER_LOW;

    default:
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Invalid /vdd/mss ACC setting passed to mapping function.");

      /*
       * This return value is necessary for the compiler even with ERR_FATAL.
       */
      return 0;
  }

} /* END of Clock_MapAccMemSelToCorner */


/* =========================================================================
**  Function : Clock_SetACCSetting
** =========================================================================*/
/**
  Updates the ACC setting for /vdd/mss.

  This function updates the ACC setting for the /vdd/mss rail.

  @param eVRegMSSCorner [in] -- New /vdd/mss corner.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_SetACCSetting
(
  ClockVRegMSSCornerType eVRegMSSCorner
)
{
  ClockVRegMSSCornerType eVRegMSSCornerNew;
  ClockVRegMSSCornerType eVRegMSSCornerACC;

  /*-----------------------------------------------------------------------*/
  /* Map the current ACC setting to a /vdd/mss voltage rail corner.        */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCornerACC =
    Clock_MapAccMemSelToCorner(HWIO_INF(MSS_TCSR_ACC_SEL, ACC_MEM_SEL));

  /*-----------------------------------------------------------------------*/
  /* Normalize all turbo requests by placing a ceiling on high requests.   */
  /* NOTE: The Turbo and Super-Turbo corners are treated as the same from  */
  /*       the perspective of ACC settings.                                */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCornerNew = MIN(eVRegMSSCorner, CLOCK_VREG_MSS_CORNER_TURBO);

  /*-----------------------------------------------------------------------*/
  /* ACC settings from high to low.                                        */
  /*                                                                       */
  /* NOTE: Incremental scaling of ACC settings is required for multi       */
  /*       corner switches due to grey coding of this HW API.              */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerACC > eVRegMSSCornerNew)
  {
    /*
     * Transition [Super Turbo or Turbo -> Nominal]
     * Set ACC to nominal.
     */
    if (eVRegMSSCornerACC > CLOCK_VREG_MSS_CORNER_NOMINAL)
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        Clock_MapCornerToAccMemSel(CLOCK_VREG_MSS_CORNER_NOMINAL));
    }

    /*
     * Transition [Nominal -> Low]
     * Set ACC to low.
     */
    if (eVRegMSSCornerNew < CLOCK_VREG_MSS_CORNER_NOMINAL)
    {
       HWIO_OUTF(
         MSS_TCSR_ACC_SEL,
         ACC_MEM_SEL,
         Clock_MapCornerToAccMemSel(CLOCK_VREG_MSS_CORNER_LOW));
    }
  }

  /*-----------------------------------------------------------------------*/
  /* ACC settings from low to high.                                        */
  /*                                                                       */
  /* NOTE: Incremental scaling of ACC settings is required for multi       */
  /*       corner switches due to grey coding of this HW API.              */
  /*-----------------------------------------------------------------------*/

  else if (eVRegMSSCornerACC < eVRegMSSCornerNew)
  {
    /*
     * Transition [Low -> Nominal]
     * Set ACC to nominal.
     */
    if (eVRegMSSCornerACC < CLOCK_VREG_MSS_CORNER_NOMINAL)
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        Clock_MapCornerToAccMemSel(CLOCK_VREG_MSS_CORNER_NOMINAL));
    }

    /*
     * Transition [Nominal -> Turbo or Super Turbo]
     * Set ACC to high.
     */
    if (eVRegMSSCornerNew > CLOCK_VREG_MSS_CORNER_NOMINAL)
    {
      HWIO_OUTF(
        MSS_TCSR_ACC_SEL,
        ACC_MEM_SEL,
        Clock_MapCornerToAccMemSel(CLOCK_VREG_MSS_CORNER_TURBO));
    }
  }

}


/* =========================================================================
**  Function : Clock_InitMSSVDD
** =========================================================================*/
/*
  See ClockMSS.h
*/ 

DALResult Clock_InitMSSVDD
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult            eRes;
  npa_resource_state   nInitialRequest;
  ClockMSSCtxtType    *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* We must disable scaling until:                                        */
  /*  - The /vdd/mss node is created.                                      */
  /*  - EFS init has complete.                                             */
  /*  - TEMPORARY: disable voltage scaling on /vdd/mss.                    */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.nDisableMSSVDD = CLOCK_FLAG_WAITING_FOR_EFS_INIT;

  /*-----------------------------------------------------------------------*/
  /* We must disable CPR related activities until CPR registers its        */
  /* callbacks with us.                                                    */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->CPRCtxt.nDisableCPR = CLOCK_FLAG_WAITING_FOR_CPR_REGISTRATION;

  /*-----------------------------------------------------------------------*/
  /* Init the ceiling/floor based on the BSP min/max.                      */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerFloor =
    pMSSCtxt->pBSPConfig->eVDDMSSVRegFloor;

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCeiling =
    pMSSCtxt->pBSPConfig->eVDDMSSVRegCeiling;

  /*-----------------------------------------------------------------------*/
  /* Register a callback for the MX node                                   */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_RegisterResourceCallback(
      pDrvCtxt,
      NPA_NODE_NAME_PMIC_VDD_MX,
      Clock_MXReadyCallback,
      pDrvCtxt);

  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Create callback event for notification of /core/cpu/vdd availability. */
  /*-----------------------------------------------------------------------*/

  npa_resource_available_cb(
    NPA_NODE_NAME_CORE_CPU_VDD,
    (npa_callback)Clock_CallbackNodeAvailableMSSVDDCoreCpuVdd,
    pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Init the CPU corner request to the current CPU PL requirement.        */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU =
    pMSSCtxt->QDSP6Ctxt.pQDSP6Config->eVRegMSSCorner;

  /*-----------------------------------------------------------------------*/
  /* Init /vdd/mss NPA resource.                                           */
  /* NOTE: Init the non-CPU corner request to off.                         */
  /*-----------------------------------------------------------------------*/

  Clock_NPAMSSVDDNonCPUResources.node.data = (npa_user_data)pDrvCtxt;

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU =
    CLOCK_VREG_MSS_CORNER_OFF;

  nInitialRequest =
    (npa_resource_state)pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU;

  npa_define_node_cb(
    &Clock_NPAMSSVDDNonCPUResources.node,
    &nInitialRequest,
    Clock_CallbackNodeAvailableMSSVDDNonCPU,
    &Clock_NPAMSSVDDNonCPUResources.node);

  /*-----------------------------------------------------------------------*/
  /* Set the initial MSS rail raw voltage to the CPU voltage requirement.  */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.nRawVoltageUV =
    Clock_MapVDDMSSCornerToVoltage(
      pDrvCtxt,
      pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU);

  /*-----------------------------------------------------------------------*/
  /* Init the rail corner.                                                 */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail =
    MAX(
      pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU,
      pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitMSSVDD */


/* =========================================================================
**  Function : Clock_SetVDDMSSVoltage
** =========================================================================*/
/**
  Make a voltage regulator request on the MSS rail.

  This function is used internally to make a raw voltage regulator request.

  @param *pDrvCtxt     [in] -- Handle to the DAL driver context.
  @param nRawVoltageUV [in] -- Raw voltage requirement in microvolts.

  @return
  DAL_SUCCESS -- The voltage was successfully applied.
  DAL_ERROR -- The voltage was not applied.

  @dependencies
  The clock voltage mutex (/vdd/mss) must be locked prior to calling this function.
  The latest cpu and non-cpu voltage requirements are stored in the context.
*/

static DALResult Clock_SetVDDMSSVoltage
(
  ClockDrvCtxt *pDrvCtxt,
  uint32        nRawVoltageUV
)
{
  DALResult eResult;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  uint32 nPreviousRawVoltageUV = pMSSCtxt->VDDMSSCtxt.nRawVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Short-circuit if:                                                     */
  /*  - there is no voltage change required.                               */
  /*-----------------------------------------------------------------------*/

  if (nRawVoltageUV == nPreviousRawVoltageUV)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Validate that the /vdd/mx dependency has been satisfied.              */
  /*-----------------------------------------------------------------------*/

  if (!NPA_DEPENDENCY_PMIC_VDD_MEM())
  {
    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-voltage update: Update MX first if we're increasing the rail.     */
  /* The reason being is that MX >= MSS.                                   */
  /*-----------------------------------------------------------------------*/

  if (nRawVoltageUV > nPreviousRawVoltageUV)
  {
    npa_issue_scalar_request(NPA_DEPENDENCY_PMIC_VDD_MEM(), nRawVoltageUV);
  }

  /*-----------------------------------------------------------------------*/
  /* Configure AVS to update /vdd/mss.                                     */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVoltageLevel(HAL_AVS_CORE_QDSPSW, nRawVoltageUV);

  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the state of /vdd/mss.                                         */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.nRawVoltageUV = nRawVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Post-voltage update: Update MX last if we're decreasing the rail.     */
  /* The reason being is that MX >= MSS.                                   */
  /*-----------------------------------------------------------------------*/

  if (nRawVoltageUV < nPreviousRawVoltageUV)
  {
    npa_issue_scalar_request(NPA_DEPENDENCY_PMIC_VDD_MEM(), nRawVoltageUV);
  }

  /*-----------------------------------------------------------------------*/
  /* Log the change.                                                       */
  /*-----------------------------------------------------------------------*/

  if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
  {
    ULOG_RT_PRINTF_1 (
      pDrvCtxt->hClockLog,
      "Rail[/vdd/mss] = %lu (uV)",
      nRawVoltageUV);
  }

  /*
   * Log /vdd/mss voltage change event.
   */
  Clock_SWEvent(CLOCK_EVENT_MSS_VOLTAGE, 1, nRawVoltageUV);

  return DAL_SUCCESS;

} /* END of Clock_SetVDDMSSVoltage */


/* =========================================================================
**  Function : Clock_SetVDDMSSCorner
** =========================================================================*/
/*
  See ClockMSS.h
*/

DALResult Clock_SetVDDMSSCorner
(
  ClockDrvCtxt           *pDrvCtxt,
  ClockVRegMSSCornerType  eVRegMSSCornerRequest,
  boolean                 bCPURequest
)
{
  DALResult              eResult;
  uint32                 nRawVoltageUV;
  boolean                bIsInRange;
  ClockMSSCtxtType      *pMSSCtxt;
  ClockVDDMSSCtxtType   *pVDDMSSCtxt;
  ClockVRegMSSCornerType eVRegMSSCornerMax;
  ClockVRegMSSCornerType eVRegMSSCornerPrevious;
  
  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;

  /*-----------------------------------------------------------------------*/
  /* Note:                                                                 */
  /*   PMIC defaults to NOMINAL at boot-up.                                */
  /*                                                                       */
  /* Note:                                                                 */
  /*   Once the PMIC NPA node is available we get callback                 */
  /*   notification that allows us to vote immediately.                    */
  /*                                                                       */
  /* Note:                                                                 */
  /*   We ERROR_FATAL if the request is greater than the VDD/MSS context's */
  /*   initial vreg level prior to us being able to communicate the        */
  /*   resulting MX requirement over to the RPM.                           */
  /*-----------------------------------------------------------------------*/

  if ((!NPA_DEPENDENCY_PMIC_VDD_MEM()) &&
      (eVRegMSSCornerRequest > pVDDMSSCtxt->eVRegMSSCornerInit))
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Clock_SetVDDMSSCorner received [%d] prior to communication with RPM",
      eVRegMSSCornerRequest);

    return DAL_ERROR;
  }

  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/


  /*-----------------------------------------------------------------------*/
  /* Thread safety - using the /vdd/mss lock.                              */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pVDDMSSCtxt);

  /*-----------------------------------------------------------------------*/
  /* Log the request.                                                      */
  /*-----------------------------------------------------------------------*/

  if (bCPURequest)
  {
    if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
    {
      ULOG_RT_PRINTF_2 (
        pDrvCtxt->hClockLog,
        "Rail[/vdd/mss] request[CPU]  (Prev[%lu], New[%lu])",
        pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU, eVRegMSSCornerRequest);
    }


    pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU = eVRegMSSCornerRequest;
  }
  else
  {
    if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
    {
      ULOG_RT_PRINTF_2 (
        pDrvCtxt->hClockLog,
        "Rail[/vdd/mss] request [Non-CPU] (Prev[%lu], New[%lu])",
        pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU, eVRegMSSCornerRequest);
    }

    pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU = eVRegMSSCornerRequest;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the new max /vdd/mss corner request.                              */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCornerMax =
    MAX(
      pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU,
      pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCPU);

  /*-----------------------------------------------------------------------*/
  /* Bound the voltage corner to the current ceiling/floor.                */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerMax < pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerFloor)
  {
    eVRegMSSCornerMax = pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerFloor;
  }
  else if (eVRegMSSCornerMax > pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCeiling)
  {
    eVRegMSSCornerMax = pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerCeiling;
  }

  /*-----------------------------------------------------------------------*/
  /* Short-circuit if the max corner is already applied.                   */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerMax == pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail)
  {
    DALCLOCK_FREE(pVDDMSSCtxt);

    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Keep track of the previous corner.                                    */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCornerPrevious = pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail;

  /*-----------------------------------------------------------------------*/
  /* Log the change.                                                       */
  /*-----------------------------------------------------------------------*/

  if (CLOCK_GLOBAL_FLAG_IS_SET(LOG_VOLTAGE_CHANGE))
  {
    ULOG_RT_PRINTF_2 (
      pDrvCtxt->hClockLog,
      "Rail[/vdd/mss] (Prev[%lu], New[%lu])",
      eVRegMSSCornerPrevious, eVRegMSSCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-voltage update - Invoke the CPR callback.                         */
  /*-----------------------------------------------------------------------*/

  if (!pMSSCtxt->CPRCtxt.nDisableCPR)
  {
    CPR_PreSwitchFuncPtr pfPreSwitch = pMSSCtxt->CPRCtxt.fpPreSwitch;

    pfPreSwitch();
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-voltage update.                                                   */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerPrevious > eVRegMSSCornerMax)
  {
    /*
     * ACC settings from high to low.
     */
    Clock_SetACCSetting(eVRegMSSCornerMax);
  }

  if (eVRegMSSCornerMax > eVRegMSSCornerPrevious)
  {
    /*
     * Set the PMIC power mode to PWM (high power).
     */
    if ((eVRegMSSCornerPrevious <= CLOCK_VREG_MSS_CORNER_NOMINAL) &&
        (eVRegMSSCornerMax > CLOCK_VREG_MSS_CORNER_NOMINAL))
    {
      eResult = Clock_SetPowerMode(HAL_AVS_CORE_QDSPSW, HAL_AVS_POWER_MODE_PWM);
      if (eResult != DAL_SUCCESS)
      {
        DALCLOCK_FREE(pVDDMSSCtxt);

        return eResult;
      }
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Map corner to raw voltage.                                            */
  /*-----------------------------------------------------------------------*/

  nRawVoltageUV = 0;
  if (!pMSSCtxt->CPRCtxt.nDisableCPR)
  {
    CPR_GetVoltageFuncPtr pfGetVoltage = pMSSCtxt->CPRCtxt.fpGetVoltage;
    uint32 nCornerTableIndex;

    /*
     * Map the corner to a table index.
     */
    nCornerTableIndex =
      Clock_MapCornerToTableIndex(pDrvCtxt, eVRegMSSCornerMax);

    /*
     * Use the CPR recommended voltage if available.
     */
    pfGetVoltage(nCornerTableIndex, &nRawVoltageUV);
  }

  /*-----------------------------------------------------------------------*/
  /* Use a fail-safe voltage if CPR recommendation isn't valid.            */
  /*-----------------------------------------------------------------------*/

  bIsInRange =
    Clock_VoltageRangeCheck(pDrvCtxt, eVRegMSSCornerMax, nRawVoltageUV);

  if (bIsInRange != TRUE)
  {
    nRawVoltageUV =
      Clock_MapVDDMSSCornerToVoltage(pDrvCtxt, eVRegMSSCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Set the voltage rail.                                                 */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVDDMSSVoltage(pDrvCtxt, nRawVoltageUV);

  if (eResult != DAL_SUCCESS)
  {
    DALCLOCK_FREE(pVDDMSSCtxt);

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Post-voltage update.                                                  */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerPrevious < eVRegMSSCornerMax)
  {
    /*
     * ACC settings from low to high.
     */
    Clock_SetACCSetting(eVRegMSSCornerMax);
  }

  if (eVRegMSSCornerMax < eVRegMSSCornerPrevious)
  {
    /*
     * Set the PMIC power mode to auto (low power).
     */
    if ((eVRegMSSCornerPrevious > CLOCK_VREG_MSS_CORNER_NOMINAL) &&
        (eVRegMSSCornerMax <= CLOCK_VREG_MSS_CORNER_NOMINAL))
    {
      eResult = Clock_SetPowerMode(HAL_AVS_CORE_QDSPSW, HAL_AVS_POWER_MODE_AUTO);
      if (eResult != DAL_SUCCESS)
      {
        DALCLOCK_FREE(pVDDMSSCtxt);

        return eResult;
      }
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Post-voltage update - Invoke the CPR callback.                        */
  /*-----------------------------------------------------------------------*/

  if (!pMSSCtxt->CPRCtxt.nDisableCPR)
  {
    CPR_PostSwitchFuncPtr pfPostSwitch = pMSSCtxt->CPRCtxt.fpPostSwitch;
    uint32 nRefClkKHz;

    /*
     * Get the CPR clock frequency.
     */
    nRefClkKHz = pMSSCtxt->ConfigBusCtxt.pConfigBusConfig->Mux.nFreqHz / 1000;

    /*
     * We pass the new corner index to CPR.
     */
    pfPostSwitch(FALSE, nRefClkKHz, nRawVoltageUV);
  }

  /*-----------------------------------------------------------------------*/
  /* Update the context for the rail.                                      */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail = eVRegMSSCornerMax;

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pVDDMSSCtxt);

  return DAL_SUCCESS;

} /* END of Clock_SetVDDMSSCorner */


/* =========================================================================
**  Function : Clock_NPAMSSVDDNonCPUDriverFunc
** =========================================================================*/
/**
  Handle state changes on /vdd/mss NPA node.
 
  This function handles state changes on /vdd/mss NPA node.
 
  @param pResource [in] -- The NPA resource being requested.
  @param hClient [in]   -- Pointer to the client making the request.
  @param nState [in]    -- New state of the resource.

  @return
  New state of the resource.

  @dependencies
  None.
*/ 

static npa_resource_state Clock_NPAMSSVDDNonCPUDriverFunc
(
  struct npa_resource *pResource,
  npa_client_handle    hClient,
  npa_resource_state   nState 
)
{
  DALResult eResult;
  ClockDrvCtxt *pDrvCtxt = (ClockDrvCtxt *)pResource->node->data;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockVRegMSSCornerType eVRegMSSCornerNewNonCPU;

  /*
   * Cap the NPA request at the highest /vdd/mss voltage corner.
   */
  eVRegMSSCornerNewNonCPU =
    (ClockVRegMSSCornerType) MIN(nState, CLOCK_VREG_MSS_CORNER_MAX);

  /*-----------------------------------------------------------------------*/
  /* Prevent PC if non-CPU clients are using /vdd/mss.                     */
  /*-----------------------------------------------------------------------*/

  if (eVRegMSSCornerNewNonCPU != CLOCK_VREG_MSS_CORNER_OFF)
  {
  if (pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd != NULL)
  {
      npa_issue_scalar_request(
        pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd,
        CLOCK_POWER_COLLAPSE_PREVENT);
    }

    /*
     * Set the power collapse prevention flag.
     */
    pMSSCtxt->VDDMSSCtxt.bPreventPC = TRUE;
  }

  /*-----------------------------------------------------------------------*/
  /* Remove request to prevent PC if no clients are using /vdd/mss.        */
  /*-----------------------------------------------------------------------*/

    else
    {
    if (pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd != NULL)
    {
      npa_complete_request(pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd);
    }

    /*
     * Clear the power collapse prevention flag.
     */
    pMSSCtxt->VDDMSSCtxt.bPreventPC = FALSE;
  }

  /*-----------------------------------------------------------------------*/
  /* Update the /vdd/mss voltage corner.                                   */
  /*-----------------------------------------------------------------------*/

  if (!pMSSCtxt->VDDMSSCtxt.nDisableMSSVDD)
  {
    eResult = Clock_SetVDDMSSCorner(pDrvCtxt, eVRegMSSCornerNewNonCPU, FALSE);

    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent (
        DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
        "Unable to set /vdd/mss voltage from non-CPU request.");
    }
  }

  return pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerNonCPU;

} /* END Clock_NPAMSSVDDNonCPUDriverFunc */


/* =========================================================================
**  Function : Clock_CallbackNodeAvailableMSSVDDCoreCpuVdd
** =========================================================================*/
/**
  Callback after /core/cpu/vdd node is created.

  This function is called by the NPA framework after the /core/cpu/vdd node
  is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext     [in] -- Context passed in npa_define_node_cb
  @param nEventType    [in] -- Zero.
  @param *pNodeName    [in] -- Name of the node being created.
  @param nNodeNameSize [in] -- Length of the name.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_CallbackNodeAvailableMSSVDDCoreCpuVdd
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
)
{
  ClockDrvCtxt *pDrvCtxt = (ClockDrvCtxt *)pContext;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockVDDMSSCtxtType *pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;

  /*-----------------------------------------------------------------------*/
  /* Thread safety - using the /vdd/mss lock.                              */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pVDDMSSCtxt);

  /*-----------------------------------------------------------------------*/
  /* The framework is up, we create our handle to vote against PC.         */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd =
    npa_create_sync_client(
      NPA_NODE_NAME_CORE_CPU_VDD,
      CLOCK_NPA_NODE_NAME_VDD_MSS,
      NPA_CLIENT_REQUIRED);

  if (pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd == NULL)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to create NPA sync client %s->%s.",
      CLOCK_NPA_NODE_NAME_VDD_MSS,
      NPA_NODE_NAME_CORE_CPU_VDD);
  }

  /*-----------------------------------------------------------------------*/
  /* Prevent PC if any non-CPU clients are using /vdd/mss.                 */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->VDDMSSCtxt.bPreventPC != FALSE)
  {
    npa_issue_scalar_request(
      pMSSCtxt->VDDMSSCtxt.hNPACoreCpuVdd,
      CLOCK_POWER_COLLAPSE_PREVENT);
  }

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pVDDMSSCtxt);

} /* END Clock_CallbackNodeAvailableMSSVDDCoreCpuVdd */


/* =========================================================================
**  Function : Clock_CallbackNodeAvailableMSSVDDNonCPU
** =========================================================================*/
/**
  Callback after /vdd/mss node is created.

  This function is called by the NPA framework after the /vdd/mss node
  is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext     [in] -- Context passed in npa_define_node_cb
  @param nEventType    [in] -- Zero.
  @param *pNodeName    [in] -- Name of the node being created.
  @param nNodeNameSize [in] -- Length of the name.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_CallbackNodeAvailableMSSVDDNonCPU
(
  void        *pContext,
  unsigned int nEventType,
  void        *pNodeName,
  unsigned int nNodeNameSize
)
{
  uint32                  nMaxPL, nMaxConfig;
  DALResult               eResult;
  ClockVRegMSSCornerType  eVRegMSSCorner;
  npa_node_definition    *pNode;
  ClockDrvCtxt           *pDrvCtxt;
  ClockMSSCtxtType       *pMSSCtxt;

  pNode = (npa_node_definition *)pContext;
  pDrvCtxt = (ClockDrvCtxt *)pNode->data;
  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get the voltage required to scale the CPU to max performance.         */
  /*-----------------------------------------------------------------------*/

  nMaxPL = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig->nMaxPerfLevel;
  nMaxConfig = pMSSCtxt->QDSP6Ctxt.pQDSP6PerfConfig->anPerfLevel[nMaxPL];

  eVRegMSSCorner =
    pMSSCtxt->pBSPConfig->pQDSP6Config[nMaxConfig].eVRegMSSCorner;

  /*-----------------------------------------------------------------------*/
  /* Update the /vdd/mss voltage corner.                                   */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVDDMSSCorner(pDrvCtxt, eVRegMSSCorner, TRUE);

  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to set /vdd/mss voltage from CPU request.");

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Run QDSP6 at max performance level - if DCVS is enabled then we'll    */
  /* go to lower performance level later.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SetQDSP6Config(
    pDrvCtxt,
    &pMSSCtxt->pBSPConfig->pQDSP6Config[nMaxConfig]);

} /* END Clock_CallbackNodeAvailableMSSVDDNonCPU */


/* =========================================================================
**  Function : Clock_SetVoltage
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_SetVoltage
(
  ClockDrvCtxt *pDrvCtxt,
  ClockVRegIdType eVRegId,
  uint32 nVoltageUV,
  boolean *pbIsLDOEnabled
)
{
  DALResult eRes;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  ClockVDDMSSCtxtType *pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;
  boolean bIsInRange;
  ClockVRegMSSCornerType eVRegMSSCorner;

  /*-----------------------------------------------------------------------*/
  /* Validate arguments.                                                   */
  /*-----------------------------------------------------------------------*/

  if (pbIsLDOEnabled == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* This image only supports CRP adjustments on /vdd/mss.                 */
  /*-----------------------------------------------------------------------*/

  if (eVRegId != CLOCK_VREG_VDD_MSS)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* Check if voltage scaling or CPR is disabled.                          */
  /*-----------------------------------------------------------------------*/

  if (pMSSCtxt->VDDMSSCtxt.nDisableMSSVDD ||
      pMSSCtxt->CPRCtxt.nDisableCPR)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Thread safety.                                                        */
  /* NOTE: CPR API call to adjust the voltage should not block!            */
  /*-----------------------------------------------------------------------*/

  eRes = DALCLOCK_TRYLOCK(pVDDMSSCtxt);
  if (eRes != DAL_SUCCESS)
  {
    return DAL_ERROR_BUSY_RESOURCE;
  }

  /*-----------------------------------------------------------------------*/
  /* Validate the voltage is usable within the current rail corner.        */
  /*-----------------------------------------------------------------------*/

  eVRegMSSCorner = pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail;
  bIsInRange = Clock_VoltageRangeCheck(pDrvCtxt, eVRegMSSCorner, nVoltageUV);
  if (bIsInRange != TRUE)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "CPR requested voltage adjustment[%lu] outside of current corner[%lu] range.",
      nVoltageUV, eVRegMSSCorner);

    DALCLOCK_FREE(pVDDMSSCtxt);

    return DAL_ERROR_OUT_OF_RANGE_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Adjust the vdd mss voltage.                                           */
  /* NOTE: We use the CPU npa dependency resource handle for CPR requests. */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_SetVDDMSSVoltage(pDrvCtxt, nVoltageUV);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to adjust /vdd/mss voltage from CPR.");
    eRes = DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Set the LDO indicator flag.                                           */
  /*-----------------------------------------------------------------------*/

  *pbIsLDOEnabled = FALSE;

  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pVDDMSSCtxt);

  return eRes;

} /* END Clock_SetVoltage */


/* =========================================================================
**  Function : Clock_RegisterCPRCallbacks
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_RegisterCPRCallbacks
(
  ClockDrvCtxt           *pDrvCtxt,
  CPR_PreSwitchFuncPtr    fpPreSwitch,
  CPR_PostSwitchFuncPtr   fpPostSwitch,
  CPR_GetVoltageFuncPtr   fpGetVoltage
)
{
  DALResult               eResult;
  ClockMSSCtxtType       *pMSSCtxt;
  ClockVDDMSSCtxtType    *pVDDMSSCtxt;
  uint32                  nRefClkKHz;
  uint32                  nCornerTableIndex;
  uint32                  nRawVoltageUV;
  ClockVRegMSSCornerType  eVRegMSSCorner;
  boolean                 bIsInRange, bIsLDOEnabled;

  pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;
  
  /*-----------------------------------------------------------------------*/
  /* Validate parameters.                                                  */
  /*-----------------------------------------------------------------------*/

  if (fpPreSwitch == NULL || fpPostSwitch == NULL || fpGetVoltage == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Thread safety - using the /vdd/mss lock.                              */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_LOCK(pVDDMSSCtxt);

  /*-----------------------------------------------------------------------*/
  /* Install CPR callback function.                                        */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->CPRCtxt.fpPreSwitch  = fpPreSwitch;
  pMSSCtxt->CPRCtxt.fpPostSwitch = fpPostSwitch;
  pMSSCtxt->CPRCtxt.fpGetVoltage = fpGetVoltage;
  
  /*-----------------------------------------------------------------------*/
  /* Enable CPR.                                                           */
  /*-----------------------------------------------------------------------*/

  pMSSCtxt->CPRCtxt.nDisableCPR &= ~CLOCK_FLAG_WAITING_FOR_CPR_REGISTRATION;

  /*-----------------------------------------------------------------------*/
  /* Get CPR recommendation for the current voltage level.                 */
  /*-----------------------------------------------------------------------*/

  nRawVoltageUV = 0;
  eVRegMSSCorner = pMSSCtxt->VDDMSSCtxt.eVRegMSSCornerRail;

  nCornerTableIndex =
    Clock_MapCornerToTableIndex(pDrvCtxt, eVRegMSSCorner);

  fpGetVoltage(nCornerTableIndex, &nRawVoltageUV);

  /*-----------------------------------------------------------------------*/
  /* Use a fail-safe voltage if CPR recommendation isn't valid.            */
  /*-----------------------------------------------------------------------*/

  bIsInRange =
    Clock_VoltageRangeCheck(pDrvCtxt, eVRegMSSCorner, nRawVoltageUV);

  if (bIsInRange != TRUE)
  {
    nRawVoltageUV =
      Clock_MapVDDMSSCornerToVoltage(pDrvCtxt, eVRegMSSCorner);
  }

  /*-----------------------------------------------------------------------*/
  /* Set the voltage rail.                                                 */
  /*-----------------------------------------------------------------------*/

  eResult = Clock_SetVDDMSSVoltage(pDrvCtxt, nRawVoltageUV);

  if (eResult != DAL_SUCCESS)
  {
    DALCLOCK_FREE(pVDDMSSCtxt);

    return eResult;
  }
  
  /*-----------------------------------------------------------------------*/
  /* Let CPR know we applied the recommendation.                           */
  /*-----------------------------------------------------------------------*/

  nRefClkKHz = pMSSCtxt->ConfigBusCtxt.pConfigBusConfig->Mux.nFreqHz / 1000;
  bIsLDOEnabled = FALSE;
  fpPostSwitch(bIsLDOEnabled, nRefClkKHz, nRawVoltageUV);
  
  /*-----------------------------------------------------------------------*/
  /* Free.                                                                 */
  /*-----------------------------------------------------------------------*/

  DALCLOCK_FREE(pVDDMSSCtxt);

  return DAL_SUCCESS;

} /* END Clock_RegisterCPRCallbacks */


/* =========================================================================
**  Function : Clock_NPAMSSVDDResourceQuery
** =========================================================================*/
/**
  NPA /vdd/mss resource query function.

  This function is called to get the following /vdd/mss information:
    -- Number of corners.
    -- Voltage table for each corner.
    -- Current corner (id / table index).

  @param *pResource  [in]  -- Pointer to the resource in question
  @param  nID        [in]  -- ID of the query.
  @param *pResult    [out] -- Pointer to the data to be filled by this function.

  @return
  npa_query_status - NPA_QUERY_SUCCESS, if query supported.
                   - NPA_QUERY_UNSUPPORTED_QUERY_ID, if query not supported.

  @dependencies
  None.
*/

static npa_query_status Clock_NPAMSSVDDResourceQuery
(
  npa_resource   *pResource,
  unsigned int    nID,
  npa_query_type *pResult
)
{
  ClockDrvCtxt *pDrvCtxt = (ClockDrvCtxt *)pResource->node->data;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;
  npa_query_status nStatus = NPA_QUERY_SUCCESS;

  /*-----------------------------------------------------------------------*/
  /* Validate parameters.                                                  */
  /*-----------------------------------------------------------------------*/

  if (!pResource || !pResult)
  {
    return NPA_QUERY_NULL_POINTER;
  }

  switch(nID)
  {
    case CLOCK_NPA_QUERY_VOLTAGE_TABLE:
    {
      /*
       * Store the corner voltage data for this part.
       */
      pResult->data.reference = (void *)pMSSCtxt->pBSPConfig->pVRegCornerData;
      pResult->type = NPA_QUERY_TYPE_REFERENCE;

      break;
    }

    case CLOCK_NPA_QUERY_VOLTAGE_NUM_ENTRIES:
    {
      /*
       * Store the number of entries in the corner voltage table.
       */
      pResult->data.value = CLOCK_VREG_MSS_CORNER_MAX;
      pResult->type = NPA_QUERY_TYPE_VALUE;

      break;
    }

    case CLOCK_NPA_QUERY_VOLTAGE_CURRENT_ENTRY:
    {
      ClockVDDMSSCtxtType *pVDDMSSCtxt = &pMSSCtxt->VDDMSSCtxt;
      ClockVRegMSSCornerType eMaxCorner;
      uint32 nCornerIndex;

      /*
       * Thread safety - using the /vdd/mss lock.
       */
      DALCLOCK_LOCK(pVDDMSSCtxt);

      /*
       * The current corner is the max request between CPU vs. non-CPU.
       */
      eMaxCorner =
        MAX(pVDDMSSCtxt->eVRegMSSCornerCPU, pVDDMSSCtxt->eVRegMSSCornerNonCPU);

      /*
       * Subtract 1 because the voltage table doesn't include an OFF row.
       */
      if (eMaxCorner == CLOCK_VREG_MSS_CORNER_OFF)
      {
        nCornerIndex = 0;
      }
      else
      {
        nCornerIndex = eMaxCorner - 1;
      }

      /*
       * Free.
       */
      DALCLOCK_FREE(pVDDMSSCtxt);

      /*
       * Store current corner request.
       */
      pResult->data.value = nCornerIndex;
      pResult->type = NPA_QUERY_TYPE_VALUE;

      break;
    }

    default:
    {
      nStatus = NPA_QUERY_UNSUPPORTED_QUERY_ID;
    }
  }

  return nStatus;

} /* END Clock_NPACPUResourceQuery */


/* =========================================================================
**  Function : Clock_MXReadyCallback
** =========================================================================*/
/**
  Callback after /pmic/client/vdd_mem_uvol node is created.

  This function is called by the NPA framework after the /vdd/mss node
  is created.  The creation is delayed until all dependencies are also
  created.

  @param *pContext [in] -- Context pointer, the driver context in this case.

  @return
  None.

  @dependencies
  None.
*/

static void Clock_MXReadyCallback
(
  void *pContext
)
{
  ClockDrvCtxt     *pDrvCtxt = (ClockDrvCtxt *)pContext;
  ClockMSSCtxtType *pMSSCtxt = (ClockMSSCtxtType *)pDrvCtxt->pImageCtxt;

  npa_issue_scalar_request(
    NPA_DEPENDENCY_PMIC_VDD_MEM(),
    pMSSCtxt->VDDMSSCtxt.nRawVoltageUV);
}

