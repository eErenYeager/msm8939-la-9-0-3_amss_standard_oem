#ifndef __CLOCKMSSHWIO_H__
#define __CLOCKMSSHWIO_H__
/*
===========================================================================
*/
/**
  @file ClockMSSHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MSS_PERPH

  'Include' filters applied: MSS_TCSR_ACC_SEL 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/hw/msm8916/mss/src/ClockMSSHWIO.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

#include <HALhwio.h>

/*
 * HWIO base definitions
 */
extern uint32                      HAL_clk_nHWIOBaseMSS;
#define MSS_TOP_BASE               HAL_clk_nHWIOBaseMSS
#define MSS_TOP_BASE_PHYS          0x04000000
#define MSS_TOP_BASE_SIZE          0x400000

/*----------------------------------------------------------------------------
 * MODULE: MSS_PERPH
 *--------------------------------------------------------------------------*/

#define MSS_PERPH_REG_BASE                                                  (MSS_TOP_BASE      + 0x00180000)
#define MSS_PERPH_REG_BASE_OFFS                                             0x00180000

#define HWIO_MSS_TCSR_ACC_SEL_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_OFFS                                          (MSS_PERPH_REG_BASE_OFFS + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_RMSK                                                 0x3
#define HWIO_MSS_TCSR_ACC_SEL_IN          \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, HWIO_MSS_TCSR_ACC_SEL_RMSK)
#define HWIO_MSS_TCSR_ACC_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, m)
#define HWIO_MSS_TCSR_ACC_SEL_OUT(v)      \
        out_dword(HWIO_MSS_TCSR_ACC_SEL_ADDR,v)
#define HWIO_MSS_TCSR_ACC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_TCSR_ACC_SEL_ADDR,m,v,HWIO_MSS_TCSR_ACC_SEL_IN)
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_BMSK                                     0x3
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_SHFT                                     0x0


#endif /* __CLOCKMSSHWIO_H__ */
