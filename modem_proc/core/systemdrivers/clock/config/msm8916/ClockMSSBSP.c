/*
===========================================================================
*/
/**
  @file ClockMSSBSP.c 
  
  BSP data for the MSS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2011-12 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/clock/config/msm8916/ClockMSSBSP.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  09/23/13   stu     Branched from 8x62 code base.
  10/19/11   vs      Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockMSSBSP.h"


/*=========================================================================
      Macros
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * Enumeration of QDSP6 performance levels.
 */
enum
{
  CLOCK_QDSP6_PERF_LEVEL_0,
  CLOCK_QDSP6_PERF_LEVEL_1,
  CLOCK_QDSP6_PERF_LEVEL_2,
  CLOCK_QDSP6_PERF_LEVEL_3,
  CLOCK_QDSP6_PERF_LEVEL_4,
  CLOCK_QDSP6_PERF_LEVEL_5,
  CLOCK_QDSP6_PERF_LEVEL_6,
  CLOCK_QDSP6_PERF_LEVEL_7,
  CLOCK_QDSP6_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of QDSP6 configurations.
 */
enum
{
  CLOCK_QDSP6_CONFIG_XO,
  CLOCK_QDSP6_CONFIG_115P20MHz_MPLL1_OUT_EARLY_DIV5_DIV2,
  CLOCK_QDSP6_CONFIG_144MHz_MPLL1_DIV4,
  CLOCK_QDSP6_CONFIG_230P40MHz_MPLL1_OUT_EARLY_DIV5,
  CLOCK_QDSP6_CONFIG_288MHz_MPLL1_DIV2,
  CLOCK_QDSP6_CONFIG_384MHz_MPLL1_OUT_EARLY_DIV3,
  CLOCK_QDSP6_CONFIG_576MHz_MPLL1,
  CLOCK_QDSP6_CONFIG_691P20MHz_MPLL2,
  CLOCK_QDSP6_CONFIG_TOTAL
};


/*
 * Enumeration of MSS Config Bus performance levels.
 */
enum
{
  CLOCK_CONFIG_BUS_PERF_LEVEL_0,
  CLOCK_CONFIG_BUS_PERF_LEVEL_1,
  CLOCK_CONFIG_BUS_PERF_LEVEL_2,
  CLOCK_CONFIG_BUS_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of MSS Config Bus configurations.
 */
enum
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz_MPLL1_DIV8,
  CLOCK_CONFIG_BUS_CONFIG_144MHz_MPLL1_DIV4,
  CLOCK_CONFIG_BUS_CONFIG_TOTAL
};


/*=========================================================================
      Data
==========================================================================*/

/*
 * QDSP6 performance levels.
 */
static uint32 Clock_QDSP6PerfLevels[] =
{
  CLOCK_QDSP6_CONFIG_XO,
  CLOCK_QDSP6_CONFIG_115P20MHz_MPLL1_OUT_EARLY_DIV5_DIV2,
  CLOCK_QDSP6_CONFIG_144MHz_MPLL1_DIV4,
  CLOCK_QDSP6_CONFIG_230P40MHz_MPLL1_OUT_EARLY_DIV5,
  CLOCK_QDSP6_CONFIG_288MHz_MPLL1_DIV2,
  CLOCK_QDSP6_CONFIG_384MHz_MPLL1_OUT_EARLY_DIV3,
  CLOCK_QDSP6_CONFIG_576MHz_MPLL1,
  CLOCK_QDSP6_CONFIG_691P20MHz_MPLL2,
};


/*
 * Config Bus performance levels
 */
static uint32 Clock_ConfigBusPerfLevels [] =
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz_MPLL1_DIV8,
  CLOCK_CONFIG_BUS_CONFIG_144MHz_MPLL1_DIV4,
};


/*
 * Mux configuration for different QDSP6 frequencies.
 */
static ClockQDSP6ConfigType Clock_QDSP6Config[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 19200 * 1000, { HAL_CLK_SOURCE_XO, 2 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 115200 * 1000, { HAL_CLK_SOURCE_MPLL1_OUT_EARLY_DIV5, 4 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 144000 * 1000, { HAL_CLK_SOURCE_MPLL1, 8 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 230400 * 1000, { HAL_CLK_SOURCE_MPLL1_OUT_EARLY_DIV5, 2 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /*  .Mux       */   { 288000 * 1000, { HAL_CLK_SOURCE_MPLL1, 4 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig        */ HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 384000 * 1000, { HAL_CLK_SOURCE_MPLL1_OUT_EARLY_DIV3, 2 }, CLOCK_VREG_LEVEL_LOW }
  },

  {
    /* .CoreConfig         */  HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    /* .Mux        */   { 576000 * 1000, { HAL_CLK_SOURCE_MPLL1, 2 }, CLOCK_VREG_LEVEL_NOMINAL }
  },

  {
    /* .CoreConfig         */  HAL_CLK_CONFIG_Q6SS_CORE_PLL_MAIN,
    /* .Mux        */   { 691200 * 1000, { HAL_CLK_SOURCE_MPLL2, 2 }, CLOCK_VREG_LEVEL_HIGH }
  }
};


/*
 * Performance level configuration data for the QDSP6.
 */
static ClockQDSP6PerfConfigType Clock_QDSP6PerfConfig[] =
{
  {
    /*
     * min level
     */
    CLOCK_QDSP6_PERF_LEVEL_1,

    /*
     * max level
     */
    CLOCK_QDSP6_PERF_LEVEL_7,

    /*
     * performance configuration for all versions
     */
    { {0, 0}, {0xFF, 0xFF} },

    /*
     * PLL Switching Index - not used
     */
    CLOCK_QDSP6_CONFIG_576MHz_MPLL1,

    /*
     * Define default performance level mappings.
     */
    Clock_QDSP6PerfLevels
  },
  { 0, 0 } /* last entry */
};


/*
 * Modem PLL to actual PLL configuration/mapping
 */
static ClockModemPLLConfigType Clock_ModemPLLConfig [] =
{
  { /* CLOCK_MODEM_PLL0 */   HAL_CLK_SOURCE_MPLL0 },
  { /* CLOCK_MODEM_PLL1 */   HAL_CLK_SOURCE_MPLL1 },
  { /* CLOCK_MODEM_PLL2 */   HAL_CLK_SOURCE_MPLL2 }
};


/*
 * Mux configuration for different MSS Config Bus frequencies.
 *
 * NOTE:
 *  The HW running off the MSS config bus is powered by /vdd/cx except for
 *  crypto which is powered by /vdd/mss.  The below configurations contain
 *  voltage requirements for both power domains to support all HW running
 *  off the MSS config bus.
 */
static ClockConfigBusConfigType Clock_ConfigBusConfig[] =
{
  /*  NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    /* .Mux */   { 19200 * 1000, { HAL_CLK_SOURCE_XO, 2 }, CLOCK_VREG_LEVEL_LOW }
  },
  {
    /* .Mux */   { 72000 * 1000, { HAL_CLK_SOURCE_MPLL1, 16 }, CLOCK_VREG_LEVEL_LOW }
  },
  {
    /* .Mux */   { 144000 * 1000, { HAL_CLK_SOURCE_MPLL1, 8 }, CLOCK_VREG_LEVEL_NOMINAL }
  }
};

/*
 * Performance level configuration data for the MSS Config Bus.
 */
static ClockConfigBusPerfConfigType Clock_ConfigBusPerfConfig =
{
  /*
   * min level
   */
  CLOCK_CONFIG_BUS_PERF_LEVEL_0,

  /*
   * max level
   */
  CLOCK_CONFIG_BUS_PERF_LEVEL_2,

  /*
   * Define default performance level mappings.
   */
  Clock_ConfigBusPerfLevels
};


/*
 * Clock LDO Configuration Data. - Not used for Bear
 * Can we remove this ??
 */
static ClockLDODataType ClockLDODataConfig =
{
  /* nLDOCFG0            */ 0x01004000,
  /* nLDOCFG1            */ 0x00000050,
  /* nLDOCFG2            */ 0x00000048,
  /* nRetentionVoltageUV */ 500000,
  /* nOperatingVoltageUV */ 775000,
  /* nHeadroomVoltageUV  */ 75000,
  /* VRegMSSCornerMin    */ CLOCK_VREG_MSS_CORNER_LOW_MINUS,
  /* VRegMSSCornerMax    */ CLOCK_VREG_MSS_CORNER_LOW,
};


/*
 * Corner voltage data. Not used for Bear.
 * Can we remove this ?? 
 */
static ClockVRegCornerDataType ClockVRegCornerData[] =
{
  {.nUVMin = 725000, .nUVMax = 725000  }, /* LOW_MINUS    */
  {.nUVMin = 762500, .nUVMax =  812500 }, /* LOW          */
  {.nUVMin = 762500, .nUVMax =  862500 }, /* LOW_PLUS     */
  {.nUVMin = 787500, .nUVMax =  900000 }, /* NOMINAL      */
  {.nUVMin = 850000, .nUVMax =  987500 }, /* NOMINAL_PLUS */
  {.nUVMin = 925000, .nUVMax = 1050000 }, /* TURBO        */
  { 0, 0 }
};


/*
 * MSS BSP data
 */
const ClockMSSBSPConfigType ClockMSSBSPConfig =
{
  /*
   * ClockVRegCornerData
   */
  ClockVRegCornerData,

  /*
   * Clock LDO Data
   */
  &ClockLDODataConfig,

  /*
   * QDSP6 config
   */
  Clock_QDSP6Config,

  /*
   * QDSP6 performance config
   */
  Clock_QDSP6PerfConfig,

  /*
   * Modem PLL
   */
  Clock_ModemPLLConfig,

  /*
   *  Bus Config
   */
  Clock_ConfigBusConfig,

  /*
   * Config Bus performanance config
   */
  &Clock_ConfigBusPerfConfig,

  /*
   * Total number of Q6 configurations
   */
  sizeof(Clock_QDSP6Config)/sizeof(ClockQDSP6ConfigType),

  /*
   * Total number of Config Bus configurations
   */
  sizeof(Clock_ConfigBusConfig)/sizeof(ClockConfigBusConfigType),

  /*
   * Initial VDD/MSS VReg corner
   */
  CLOCK_VREG_MSS_CORNER_NOMINAL,

  /*
   * Ceiling VDD/MSS VReg corner
   */
  CLOCK_VREG_MSS_CORNER_TURBO,

  /*
   * Floor VDD/MSS VReg corner
   */
  CLOCK_VREG_MSS_CORNER_LOW,

};
