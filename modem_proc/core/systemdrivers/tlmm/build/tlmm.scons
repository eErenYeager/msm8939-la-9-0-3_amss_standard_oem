#===============================================================================
#
# TLMM LIBRARY
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/tlmm/build/tlmm.scons#1 $
#  $DateTime: 2015/01/27 06:04:57 $
#  $Author: mplp4svc $
#  $Change: 7351156 $
#
#===============================================================================

import os

Import('env')
env = env.Clone()

#------------------------------------------------------------------------------
# Add API folders
#------------------------------------------------------------------------------

env.PublishPrivateApi("SYSTEMDRIVERS_TLMM", [
   "${INC_ROOT}/core/systemdrivers/tlmm/inc",
   "${INC_ROOT}/core/api/systemdrivers/hwio/${CHIPSET}",
   "${INC_ROOT}/core/systemdrivers/tlmm/config/${CHIPSET}"
])

SRCPATH = "../"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
if env['MSM_ID'] in ['8926']:
  env.Append(CPPPATH = [
     "${INC_ROOT}/core/systemdrivers/tlmm/src",
     "${INC_ROOT}/core/systemdrivers/tlmm/config",
     "${INC_ROOT}/core/systemdrivers/tlmm/hw/v1"
  ])
else:
  env.Append(CPPPATH = [
     "${INC_ROOT}/core/systemdrivers/tlmm/src",
     "${INC_ROOT}/core/systemdrivers/tlmm/config",
     "${INC_ROOT}/core/systemdrivers/tlmm/hw/v2"
  ])


#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'DAL',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

if env['MSM_ID'] in ['8926']:
  HAL_TLMM_V1_SOURCES =  [
     '${BUILDPATH}/hw/v1/HALtlmm.c'
  ]
else:
  HAL_TLMM_V2_SOURCES =  [
     '${BUILDPATH}/hw/v2/HALtlmm.c'
  ]

TLMM_COMMON_SOURCES =  [
   '${BUILDPATH}/src/DALTLMMFwk.c',
   '${BUILDPATH}/src/DALTLMMInfo.c',
   '${BUILDPATH}/src/DALTLMM.c',
   '${BUILDPATH}/src/DALTLMMState.c'
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

HAL_TLMM_IMAGES =  ['CORE_MODEM']

env.AddLibrary(HAL_TLMM_IMAGES,'${BUILDPATH}/DALtlmm',TLMM_COMMON_SOURCES)
if env['MSM_ID'] in ['8926']:
  env.AddLibrary(HAL_TLMM_IMAGES,'${BUILDPATH}/HALtlmmV1',HAL_TLMM_V1_SOURCES)
else:
  env.AddLibrary(HAL_TLMM_IMAGES,'${BUILDPATH}/HALtlmmV2',HAL_TLMM_V2_SOURCES)


#---------------------------------------------------------------------------
# Invoke document generation SConscript
#---------------------------------------------------------------------------

if os.path.exists(env['BUILD_ROOT'] + '/core/api/systemdrivers/docsrc/tlmm/SConscript') :
  env.SConscript(
    '${BUILD_ROOT}/core/api/systemdrivers/docsrc/tlmm/SConscript',
    exports='env')


#---------------------------------------------------------------------------
# Add DAL XML file
#---------------------------------------------------------------------------

if os.path.exists(env['BUILD_ROOT'] + '/core/systemdrivers/tlmm/config/' + env['CHIPSET']) :
 if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
    '8974_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8974/TLMMChipset.xml'],
    '8674_PLATFORM_MTP_MSM_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8974/PlatformIO_MTP.xml'],
    '8674_PLATFORM_CDP_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8974/PlatformIO_CDP.xml'],
    '8626_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x26/TLMMChipset.xml'],
    '8626_PLATFORM_MTP_MSM_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x26/PlatformIO_MTP.xml'],
    '8626_PLATFORM_CDP_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x26/PlatformIO_CDP.xml'],
    '8626_PLATFORM_QRD_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x26/PlatformIO_QRD.xml'],
    '8610_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x10/TLMMChipset.xml'],
    '9625_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/mdm9x25/TLMMChipset.xml'],
    '8926_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8926/TLMMChipset.xml'],
    '8926_PLATFORM_MTP_MSM_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8926/PlatformIO_MTP.xml'],
    '8926_PLATFORM_CDP_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8926/PlatformIO_CDP.xml'],
    '8926_PLATFORM_QRD_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8926/PlatformIO_QRD.xml'],
    '8962_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8x62/TLMMChipset.xml'],
    '8916_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8916/TLMMChipset.xml'],
    '8936_xml' : ['${BUILD_ROOT}/core/systemdrivers/tlmm/config/msm8936/TLMMChipset.xml']
   })
  


