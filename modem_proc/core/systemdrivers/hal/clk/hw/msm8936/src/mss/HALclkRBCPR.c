/*
==============================================================================

FILE:         HALclkRBCPR.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   RBCPR clocks.

   List of clock domains:
   -HAL_clk_mMSSRBCPRClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/hal/clk/hw/msm8936/src/mss/HALclkRBCPR.c#1 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
01/17/2013            Auto-generated.
10/02/2012    frv     Manually merged in support for dividing the RBCPR ref clock.

==============================================================================
            Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/

static void HAL_clk_RBCPRConfigDivider(HAL_clk_ClockDescType *pmClockDesc, uint32  nDiv);
static void HAL_clk_RBCPRDetectDivider(HAL_clk_ClockDescType *pmClockDesc, uint32 *nDiv);

/* ============================================================================
**    Externs
** ==========================================================================*/


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_clk_mRBCPRClockControl
 *
 * Functions for controlling RBCPR clock functions.
 */
HAL_clk_ClockControlType HAL_clk_mRBCPRClockControl =
{
  /* .Enable           = */ HAL_clk_GenericEnable,
  /* .Disable          = */ HAL_clk_GenericDisable,
  /* .IsEnabled        = */ HAL_clk_GenericIsEnabled,
  /* .IsOn             = */ HAL_clk_GenericIsOn,
  /* .Reset            = */ HAL_clk_GenericReset,
  /* .IsReset          = */ HAL_clk_GenericIsReset,
  /* .Config           = */ NULL,
  /* .DetectConfig     = */ NULL,
  /* .ConfigDivider    = */ HAL_clk_RBCPRConfigDivider,
  /* .DetectDivider    = */ HAL_clk_RBCPRDetectDivider
};


/*                           
 *  HAL_clk_mRBCPRClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mRBCPRClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_bus_rbcpr_ref",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BUS_RBCPR_REF_CBCR), HWIO_OFFS(MSS_RBCPR_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mRBCPRClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BUS_RBCPR_REF
  },
};


/*
 * HAL_clk_mMSSRBCPRClkDomain
 *
 * RBCPR clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSRBCPRClkDomain =
{
  /* .nCGRAddr             = */ 0,
  /* .pmClocks             = */ HAL_clk_mRBCPRClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mRBCPRClkDomainClks)/sizeof(HAL_clk_mRBCPRClkDomainClks[0]),
  /* .pmControl            = */ NULL,
  /* .pmNextClockDomain    = */ NULL
};



/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_RBCPRConfigDivider
**
** ======================================================================== */

void HAL_clk_RBCPRConfigDivider
(
  HAL_clk_ClockDescType *pmClockDesc,
  uint32                nDiv
)
{
  if (nDiv <= 1)
  {
    nDiv = 0;
  }
  else
  {
    nDiv--;
  }

  HWIO_OUTF(MSS_BUS_RBCPR_REF_DIV_MISC, SRC_DIV, nDiv);

} /* END HAL_clk_RBCPRConfigDivider */


/* ===========================================================================
**  HAL_clk_RBCPRDetectDivider
**
** ======================================================================== */

void HAL_clk_RBCPRDetectDivider
(
  HAL_clk_ClockDescType *pmClockDesc,
  uint32                *pnDiv
)
{

  *pnDiv = HWIO_INF(MSS_BUS_RBCPR_REF_DIV_MISC, SRC_DIV) + 1;

} /* END HAL_clk_RBCPRDetectDivider */
