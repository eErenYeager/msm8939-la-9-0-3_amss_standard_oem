/*
==============================================================================

FILE:         HALclkSLIMBUSCORE.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   SLIMBUS CORE clocks.

   List of clock domains:
   -HAL_clk_mGCCULTAUDIOSLIMBUSCOREClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/hal/clk/hw/msm8936/src/gcc/HALclkSLIMBUSCORE.c#1 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
03/05/2014            Auto-generated.

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mLPASSClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mULTAUDIOSLIMBUSCOREClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mULTAUDIOSLIMBUSCOREClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_ultaudio_slimbus_core_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_ULTAUDIO_SLIMBUS_CORE_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_ULTAUDIO_SLIMBUS_CORE_CLK
  },
};


/*
 * HAL_clk_mGCCULTAUDIOSLIMBUSCOREClkDomain
 *
 * ULTAUDIO SLIMBUS CORE clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCULTAUDIOSLIMBUSCOREClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_ULTAUDIO_SLIMBUS_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mULTAUDIOSLIMBUSCOREClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mULTAUDIOSLIMBUSCOREClkDomainClks)/sizeof(HAL_clk_mULTAUDIOSLIMBUSCOREClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mLPASSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


