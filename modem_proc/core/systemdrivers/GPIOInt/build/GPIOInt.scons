#===============================================================================
#
# GPIO INTERRUPT CONTROLLER Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/GPIOInt/build/GPIOInt.scons#1 $
#  $DateTime: 2015/01/27 06:04:57 $
#  $Author: mplp4svc $
#  
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/systemdrivers/GPIOInt/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# External depends within CoreBSP
#-------------------------------------------------------------------------------
env.RequireExternalApi([
  'CS_INC',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
env.Append(CPPPATH = [
   "${INC_ROOT}/core/systemdrivers/GPIOInt/inc",
   "${INC_ROOT}/core/systemdrivers/GPIOInt/src",
   "${INC_ROOT}/core/systemdrivers/GPIOInt/config",
   "${INC_ROOT}/core/api/systemdrivers",
])

env.PublishPrivateApi('SYSTEMDRIVERS_GPIOINT', [
   "${INC_ROOT}/core/systemdrivers/hal/gpioint/inc",
])


CBSP_API = [
  'HAL',
  'DAL',
  'DEBUGTOOLS',
  'MPROC',
  'POWER',
  'SERVICES',
  'SYSTEMDRIVERS_8660',
  'SYSTEMDRIVERS',
  'KERNEL',
]
 
env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

# turned off until we have mpm support.
env.Append(CCFLAGS = " -DGPIOINT_USE_MPM")
env.Append(CCFLAGS = " -DGPIOINT_USE_NPA")


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
GPIO_INTERRUPT_SOURCES = [
   '${BUILDPATH}/GPIOIntFwk.c',
   '${BUILDPATH}/GPIOIntInfo.c',
   '${BUILDPATH}/GPIOInt.c',
]

GPIO_INTERRUPT_CFG_SOURCES = [
   '${BUILD_ROOT}/core/systemdrivers/GPIOInt/config/GPIOInt_ConfigData.c',
]

#-------------------------------------------------------------------------------
# RCInit Dependencies
#-------------------------------------------------------------------------------

if 'USES_RCINIT' in env:
  RCINIT_IMG = ['CORE_MODEM', 'CORE_QDSP6_SW']
  env.AddRCInitFunc(       # Code Fragment in TMC: NO
  RCINIT_IMG,              # define TMC_RCINIT_INIT_GPIOINT_INIT
  {
    'sequence_group'             : 'RCINIT_GROUP_0',       # required
    'init_name'                  : 'GPIOInt',              # required
    'init_function'              : 'GPIOInt_Init',         # required
    'dependencies'               : ['dalsys','npa']
  })

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(['MODEM_IMAGE','CBSP_MODEM_IMAGE','APPS_IMAGE','CBSP_APPS_IMAGE','WCN_IMAGE','CBSP_WCN_IMAGE', 'CORE_SPS','CBSP_IMAGE_PPSS', 'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
   'SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE'],'${BUILDPATH}/GPIOInt',GPIO_INTERRUPT_SOURCES)

if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG, 
   {
      'soc_xml' : ['${BUILD_ROOT}/core/systemdrivers/GPIOInt/config/GPIOInt.xml',
                   '${BUILD_ROOT}/core/systemdrivers/GPIOInt/config/GPIOInt_ConfigData.c']      
   })

 