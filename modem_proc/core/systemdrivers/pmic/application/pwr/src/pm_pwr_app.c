/*! \file pm_pwr_app.c
*  \n
*  \brief Pwr Application APIs implementation.
*  \n  
*  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                            Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/application/pwr/src/pm_pwr_app.c#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/20/14   rk      Provide PMIC API in MPSS to set voltage for Vdd_MSS Rail (CR - 668036)
10/07/13   rh      File created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "hw_module_type.h"
#include "pm_pbs_client.h"
#include "pmapp_pwr.h"
#include "pm_target_information.h"
#include "pm_resources_and_types.h"
#include "pm_version.h"
#include "pm_npa.h"

#include "pm_ldo_driver.h"
#include "pm_smps_driver.h"

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

pm_err_flag_type pmapp_pwr_set_vdd_mss(uint32 voltage)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pwr_resource_info_type* mss_rail = NULL;
    pm_pbs_client_type* mss_pbs_level_trigger_sequence = NULL;
    uint8 model = pm_get_pmic_model(0);
    
    mss_rail = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_MSS);
    
    if(mss_rail == NULL)
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }
    
    switch(mss_rail->resource_type)
    {
        // Apply supplied voltage to MSS rail. Note that in target config where we get the MSS rail info
        // from the regulator indices are 1-indexed while driver code expects 0-indexed resource ids.
        // Subtract resource_index by 1 to account for this.
        case RPM_SMPS_A_REQ:
            err_flag = pm_smps_volt_level(0, mss_rail->resource_index - 1, voltage);
            break;
        case RPM_SMPS_B_REQ:
            err_flag = pm_smps_volt_level(1, mss_rail->resource_index - 1, voltage);
            break;
        case RPM_LDO_A_REQ:
            err_flag = pm_ldo_volt_level(0, mss_rail->resource_index - 1, voltage);
            break;
        case RPM_LDO_B_REQ:
            err_flag = pm_ldo_volt_level(1, mss_rail->resource_index - 1, voltage);
            break;
        default:
            err_flag = PM_HW_MODULE_INVALID;
            break;
    }
    
    //PMD9635 needs PBS client from config to be triggered after setting MSS rail
    if(err_flag == PM_ERR_FLAG__SUCCESS && model == PMIC_IS_PMD9635)
    {        
        mss_pbs_level_trigger_sequence = (pm_pbs_client_type*)mss_rail->data1;
        
        if(mss_pbs_level_trigger_sequence == NULL)
        {
            return PM_ERR_FLAG__INVALID_POINTER;
        }  
        
        err_flag = pm_pbs_client_sw_trigger(0, *mss_pbs_level_trigger_sequence);
    }
        
    return err_flag;
}