/*! \file
 *  \n
 *  \brief  pm_pwron_driver.cpp
 *  \details  
 *  \n &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/pwron/src/pm_pwron_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/19/12   hs      Removed the IRQ handle for WDOG status since it will be
                   saved in one of the registers.
12/13/11   wra     pwron module V6 Initial Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pwron_driver.h"
#include "pm_target_information.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the PWRON data */
static pm_pwron_data_type pm_pwron_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_pwron_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
	pm_pwron_data_type *pwron_ptr = NULL;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
	const char* prop_id_arr[] = {PM_PROP_PWRONA_NUM, PM_PROP_PWRONB_NUM};
	
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
	
	pwron_ptr = &pm_pwron_data_arr[pmic_index];
		
    if (pwron_ptr->periph_exists == FALSE)
    {
        pwron_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        pwron_ptr->comm_ptr = comm_ptr;
		
		/* PWRON Register Info - Obtaining Data through dal config */
        pwron_ptr->pwron_register = (pwron_register_ds*)pm_target_information_get_common_info(PM_PROP_PWRON_REG);
		
        CORE_VERIFY_PTR(pwron_ptr->pwron_register);
		
        pwron_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(pwron_ptr->num_of_peripherals != 0);
    }
}

pm_pwron_data_type* pm_pwron_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_pwron_data_arr[pmic_index];
    }

    return NULL;
}