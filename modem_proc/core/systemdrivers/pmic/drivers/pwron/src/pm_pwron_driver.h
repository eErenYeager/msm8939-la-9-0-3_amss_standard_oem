#ifndef PM_PWRON_DRIVER__H
#define PM_PWRON_DRIVER__H

/*! \file
*  \n
*  \brief  pm_pwron_driver.h
*  \details  This file contains functions prototypes and variable/type/constant
*  declarations for supporting PWRON pin services for the Qualcomm
*  PMIC chip set.
*  \n &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/pwron/src/pm_pwron_driver.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/10/12   hs      Changed the type for internalResourceIndex from int to uint8.
03/19/12   hs      Removed the IRQ handle for WDOG status since it will be
                   saved in one of the registers.
12/15/11   tdh	   Updated for V6 Badger
12/13/11   wra     pwron module V6 Initial Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_err_flags.h"
#include "icomm.h"


/*===========================================================================

                     TYPE DEFINITIONS 

===========================================================================*/
typedef struct pwron_common_ds
{
    const int*           pm_pon_reset_delay_table;
    unsigned char        pm_pon_reset_delay_table_size;
    const int*           pm_pon_debounce_timer_table;
    unsigned char        pm_pon_debounce_timer_table_size;
    const uint32*        pm_pon_delay_table;
    unsigned char        pm_pon_delay_table_size;
}pwron_common_ds;

typedef struct pwron_register_ds
{
    pm_register_address_type    base_address;
    pm_register_address_type    peripheral_offset;
    pm_register_address_type    status;
    pm_register_address_type    kpdpwr_n_reset_s2_ctl;
    pm_register_address_type    resin_n_reset_s2_ctl;
    pm_register_address_type    resin_and_kpdpwr_reset_s2_ctl;
    pm_register_address_type    pmic_wd_reset_s2_ctl;
    pm_register_address_type    overtemp_reset_ctl;
    pm_register_address_type    ps_hold_reset_ctl;
    pm_register_address_type    sw_reset_s2_ctl;
    pm_register_address_type    pullup_ctl;
    pm_register_address_type    debounce_ctl;
    pm_register_address_type    pon_trigger_en;
    pm_register_address_type    perph_reset_ctl1;
}pwron_register_ds;  

typedef struct pm_pwron_data_type
{
	boolean      periph_exists;
	pmiC_IComm   *comm_ptr;
	
    pwron_register_ds* pwron_register;
    boolean m_wdog_reset_status;
    uint8 num_of_peripherals;
	
}pm_pwron_data_type;

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
void pm_pwron_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

pm_pwron_data_type* pm_pwron_get_data(uint8 pmic_index);
#endif //PM_PWRON_DRIVER__H
