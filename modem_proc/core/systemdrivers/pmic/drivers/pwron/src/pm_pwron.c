/*! \file
*  
*  \brief  pm_pwron.c ----File contains the implementation of the public APIs for PWRON resource type.
*  \details Implementation file for PWRON resourece type.
*  Each of the APIs checks for access and then if necessary directs
*  the call to Driver implementation or to RPC function for the master processor.
*  
*    PMIC code generation Version: 2.0.0.19
*    This file contains code for Target specific settings and modes.
*  
*  &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/pwron/src/pm_pwron.c#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/10/12   hs      Changed the type for internalResourceIndex from int to uint8.

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pwron.h"
#include "pm_pwron_driver.h"

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_pwron_power_key_setup(pm_pwr_key_delay_type  delay, pm_switch_cmd_type     pull_up_en, uint8  perph_index)
{
    pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type regVal = 0;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {
        if (delay >= PM_NUM_OF_PWR_KEY_DELAY)
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        } 
        else
        {
            if (pull_up_en >= PM_INVALID_CMD) 
            {          
                errFlag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
            }
            else
            { 
                /* Get the corresponding Power key hysteresis bit settings */
                regVal = (pm_register_data_type)(delay);

                // TDH: Need to figure out which to set...
                //errFlag = comm_ptr->WriteByte(pwronCommon->base_address + (pm_register_address_type)(perph_index*0x0100) + pwron_ptr->pwron_register->debounce_ctl, regVal);
                
                /* Add the Power Key Enable/Disable bit setting. */
                regVal = (pm_register_data_type)(pull_up_en << 1); // TDH: This isn't right, need to figure out what to do...
                //regVal |= pull_up_en << 7;

                reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->pullup_ctl;

                /* Write the Power key settings into the PMIC */
                pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg, 0x02, regVal, 0);

            }
        }
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_set_pwr_key_pullup(pm_switch_cmd_type  pull_up_en, uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);
	
	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else if(pull_up_en >= PM_INVALID_CMD)
    {
        errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    else
    {
        reg =  pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->pullup_ctl;

        if(PM_ON_CMD == pull_up_en)
        {
            /*errFlag = comm_ptr->WriteByteMask(me, me->pwron_register[perph_index].pm_pon_cntrl1_r_w,
                                           pwronMaskTable.pm_pon_cntrl1_r_w__kpdpwr_pu_mask,
                                           0xFF);*/
            // TODO: Verify reg val...
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg,  0x02, 0xFF, 0);
            //pm_pon_ps_hold_reset_ctl_r_w__reset_en_mask
        }
        else
        {
            /*errFlag = comm_ptr->WriteByteMask(me, me->pwron_register[perph_index].pm_pon_cntrl1_r_w,
                                           pwronMaskTable.pm_pon_cntrl1_r_w__kpdpwr_pu_mask,
                                           0);*/
            // TODO: Verify regVal...
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x02, 0, 0);
        }
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_get_power_on_status(uint32*  pwr_on_status, uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type reg_data  = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {
        if ( NULL != pwr_on_status)
        {
          //  errFlag = comm_ptr->ReadByte( me->pwron_register[perph_index].pm_pon_cntrl3_r_w, &reg_data );
        //	reg_data = reg_data & pwronMaskTable.pm_pon_cntrl3_r__pon_trigger_mask;

            switch (reg_data)
            {
            case 0:     //000 � No trigger received
                *pwr_on_status = 0;
                break;
            case 1:     //001 � Triggered from CBL
                *pwr_on_status = PM_PWR_ON_EVENT_CABLE;
                break;
            case 2:     //010 � Triggered from KPD
                *pwr_on_status = PM_PWR_ON_EVENT_KEYPAD;
                break;
            case 3:     //011 � Triggered from CHG
                *pwr_on_status = PM_PWR_ON_EVENT_USB_CHG;  //can't tell if it's wall or USB charger
                break;
            case 4:     //100 � Triggered from SMPL
                *pwr_on_status = PM_PWR_ON_EVENT_SMPL;
                break;
            case 5:     //101 � Triggered from RTC
                *pwr_on_status = PM_PWR_ON_EVENT_RTC;
                break;
            case 6:     //110 � Triggered by hard reset
                *pwr_on_status = 0x100;
                break;
            case 7:     //111 � Triggered by general-purpose trigger
                *pwr_on_status = 0x200;
                break;
            default:
                errFlag = PM_ERR_FLAG__SBI_OPT_ERR;
                break;
            }
        }
        else
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_clear_power_on_status(uint8  perph_index)
{
    (void)perph_index; // suppresses warning C4100: unreferenced formal parameter
    
    /* in PM8058, the power on status register is read only,
     * so there is nothing to clear */
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
}

pm_err_flag_type pm_pwron_watchdog_reset_detect_switch(pm_switch_cmd_type  OnOff, uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {
        if (OnOff >= PM_INVALID_CMD) 
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
        else
        {
            reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->ps_hold_reset_ctl;

            if (OnOff == PM_ON_CMD)
            {   /* Set register value to enable this feature. */

                errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x80, 0xFF, 0);
                // TDH: Need to set the reason bit also!...
                
                //errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_pon_cntrl1_r_w, 
                //                                pwronMaskTable.pm_pon_cntrl1_r_w__watchdog_en_mask, 0xFF);
            }
            else
            {   /* Set register value to disable this feature. */

                /* Write the new setting into the PMIC. */
                errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x80, 0x00, 0);

                //errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_pon_cntrl1_r_w, 
                //                                pwronMaskTable.pm_pon_cntrl1_r_w__watchdog_en_mask, 0x00);
            }
        }
    }
    return errFlag;
}

pm_err_flag_type pm_pwron_cbl_pwr_on_switch(pm_cbl_pwr_on_switch_type  cmd, uint8  perph_index)
{
    (void)cmd; // suppresses warning C4100: unreferenced formal parameter
    (void)perph_index; // suppresses warning C4100: unreferenced formal parameter
    
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    // on KOTO, CBLPWR_EN is an input to the PON module that is tied high. 
    // So CBLPWR_EN = 1.
}

pm_err_flag_type pm_pwron_cbl_pwr_on_pullup_switch(pm_switch_cmd_type       OnOff, pm_cbl_pwr_on_pin_type   cable, uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type data      = 0;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {   
        if (OnOff >= PM_INVALID_CMD)
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
        else
        {
            if (OnOff == PM_ON_CMD) 
            {
                /* ENABLE CABLE POWER ON */
                data = 0xFF;
            }

            if (cable < PM_CBL_PWR_ON_PIN__INVALID)
            {
                reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->pullup_ctl;

                if (cable == PM_CBL_PWR_ON_PIN__0)
                {
                    errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg, 0x04, data, 0);  
                    //errFlag = comm_ptr->WriteByteMask(me, me->pwron_register[perph_index].pm_pon_cntrl1_r_w,
                    //                               pwronMaskTable.pm_pon_cntrl1_r_w__cbl0pwr_pu_mask,
                    //                               data);
                }
                else if (cable == PM_CBL_PWR_ON_PIN__1)
                {
                    errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg, 0x08, data, 0);  
                    //errFlag = comm_ptr->WriteByteMask(me, me->pwron_register[perph_index].pm_pon_cntrl1_r_w,
                    //                               pwronMaskTable.pm_pon_cntrl1_r_w__cbl1pwr_pu_mask,
                    //                               data);
                }
            }
            else
            {
                /* Parameter 2 is out of the valid range. */ 
                errFlag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
            } 
        }
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_start_up_abort_timer_switch(pm_switch_cmd_type  OnOff, uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type data      = 0;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {
        if (OnOff < PM_INVALID_CMD) 
        {
            if (OnOff == PM_ON_CMD)
            {   
                /* Set register value to enable the power On fail 
                  detect timer. */
                data = 0x01;
            }
            else
            {   
                /* Set register value to disable the power On fail 
                  detect timer. */
                data = 0x00;
            }

           /* Select Bank 1. */
            //data |= (1<<4);

            reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->perph_reset_ctl1;
        
            /* Write the power On fail detect timer setting into the PMIC. */
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg, 0x01, data, 0);
            
            //errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_gp_test1_w, 
            //                                    pwronMaskTable.pm_gp_test1_w__bank_sel_mask |
            //                                    pwronMaskTable.pm_gp_test1_w__bank1_dat_mask,
            //                                    data);
        }
        else
        {   /* Invalid input parameter */ 
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_pwron_soft_reset(uint8  perph_index)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  ( perph_index >= pwron_ptr->num_of_peripherals )
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
    }
    else 
    {
        /* Reset the PMIC registers */
        errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  0x00, // register_address         
																	0xFF, // mask
																	0x55,  // data
																	0      // priority
																	);
    }

    return errFlag;
}

pm_err_flag_type pm_pwron_pwron_hard_reset_enable(boolean  enable, uint8  perph_index)
{
    pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type data = 0;
    pm_register_address_type reg = 0;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    if  (perph_index >= pwron_ptr->num_of_peripherals)
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED;
    }
    else 
    {
        data = (enable == TRUE ? 0x01 : 0x00);

        reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->ps_hold_reset_ctl;

        if (enable == TRUE)
        {
            /* Enable hard reset feature */
            //TDH: Which bit to use?
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x80, data, 0);
            /*errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_pon_cntrl5_r_w, 
                                            pwronMaskTable.pm_pon_cntrl5_r_w__hard_reset_en_mask,
                                            0xFF);*/

            //TODO
            /* set the hardware reset enable bit */ 
            //TDH: Which bit to use?
            //errFlag = comm_ptr->WriteByteMask(me,  pwronCommon->base_address + (pm_register_address_type)(perph_index*0x0100) 
            //    + me->pwron_register->ps_hold_reset_ctl, pwronMaskTable.pm_ps_hold_reset_ctl_r_w__reset_en_mask, data);
            /*errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_pon_cntrl4_r_w, 
                                            pwronMaskTable.pm_pon_cntrl4_r_w__reset_en_mask, 0xFF);*/
        }
        else
        {
            /* clear the hardware reset enable bit */
            //TDH: Which bit to use?
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x80, data, 0);
            /*errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_pon_cntrl4_r_w, 
                                            pwronMaskTable.pm_pon_cntrl4_r_w__reset_en_mask, 0);*/
        }
    }
    return errFlag;
}

pm_err_flag_type pm_pwron_pwron_signal_reset_with_action(pm_power_reset_signal  signal, pm_power_reset_action_type  action, uint8  perph_index)
{
    pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
	(void)signal;
    pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

    if  (perph_index >= pwron_ptr->num_of_peripherals)
    {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED;
    }
    else
    {
        // TODO: action handlers...
        switch(action)
        {
            case PM_PWR_ON_RST_ACTION_TYPE__COMPLETE_FULL_SHUTDOWN:
                break;

            case PM_PWR_ON_RST_ACTION_TYPE__FULL_SHUTDOWN:
                break;

            case PM_PWR_ON_RST_ACTION_TYPE__HARD_RESET:
                break;

            case PM_PWR_ON_RST_ACTION_TYPE__POWER_OFF_NORMAL:
                break;

            case PM_PWR_ON_RST_ACTION_TYPE__SOFT_RESET:
                break;

            case PM_PWR_ON_RST_ACTION_TYPE__WARM_RESET:
                break;

            default:
                break;
        }

    }
    return errFlag;
}

pm_err_flag_type pm_pwron_get_wdog_status(boolean  *dog_status, uint8  perph_index)
{
   pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
   pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

   if  ( perph_index >= pwron_ptr->num_of_peripherals)
   {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED ;
   }
   else if(NULL == dog_status)
   {
        errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE ;
   }
   else
   {
       *dog_status = pwron_ptr->m_wdog_reset_status;
   }

    return errFlag;
}

pm_err_flag_type pm_pwron_pwron_stay_on_enable(boolean  enable, uint8  perph_index)
{
   pm_err_flag_type errFlag = PM_ERR_FLAG__SUCCESS;
   pm_register_address_type reg = 0;
   pm_pwron_data_type *pwron_ptr = pm_pwron_get_data(0);

	if ((pwron_ptr == NULL) || (pwron_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
   if  (perph_index >= pwron_ptr->num_of_peripherals)
   {   
        errFlag = PM_ERR_FLAG__INVALID_PWRON_INDEXED;
   }
   else
   {
       reg = pwron_ptr->pwron_register->base_address + (pm_register_address_type)(pwron_ptr->pwron_register->peripheral_offset*perph_index) + pwron_ptr->pwron_register->ps_hold_reset_ctl;

        /* enable or disable stay on bit -- note this register is write-only 
        and thus we need to make a shadow copy of this register still*/
        if (enable == TRUE)
        {
            /* Enable stay-on bit */
            // TDH: Find correct register and bits...
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr,  reg, 0x80, 0x93, 0);
            /*errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_gp_test1_w, 
                                            pwronMaskTable.pm_gp_test1_w__mask,
                                            0x93);*/

        }
        else
        {
            /* clear stay-on bit */
            // TDH: Find correct register and bits...
            errFlag = pwron_ptr->comm_ptr->WriteByteMask(pwron_ptr->comm_ptr, reg, 0x80, 0x91, 0);
            /*errFlag = comm_ptr->WriteByteMask(me,  me->pwron_register[perph_index].pm_gp_test1_w, 
                                            pwronMaskTable.pm_gp_test1_w__mask,
                                            0x91);*/

        }
   }
   return errFlag;
}

/*===========================================================================

                        DEPRECATED FUNCTIONS

===========================================================================*/
pm_err_flag_type pm_dev_pwr_key_delay_set(unsigned pmic_chip, uint32 microseconds)
{
	return pm_pwr_key_delay_set(microseconds);
}
pm_err_flag_type pm_dev_pwr_key_pullup_set(unsigned pmic_chip, pm_switch_cmd_type pull_up_en)
{
	return pm_pwr_key_pullup_set(pull_up_en);
}
pm_err_flag_type pm_dev_wdog_status_get(unsigned pmic_chip, boolean *dog_status)
{
	return pm_wdog_status_get(dog_status);
}
pm_err_flag_type pm_dev_get_power_on_status(unsigned pmic_chip, uint32* pwr_on_status)
{
	return pm_get_power_on_status(pwr_on_status);
}
pm_err_flag_type pm_dev_clear_power_on_status(unsigned pmic_chip)
{
	return pm_clear_power_on_status();
}
pm_err_flag_type pm_dev_watchdog_reset_detect_switch(unsigned pmic_chip, pm_switch_cmd_type OnOff)
{
	return pm_watchdog_reset_detect_switch(OnOff);
}
pm_err_flag_type pm_dev_cbl_pwr_on_switch(unsigned pmic_chip, pm_cbl_pwr_on_switch_type cmd)
{
	return pm_cbl_pwr_on_switch(cmd);
}
pm_err_flag_type pm_dev_cbl_pwr_on_pull_up_switch(unsigned pmic_chip, pm_switch_cmd_type OnOff, pm_cbl_pwr_on_pin_type cable)
{
	return pm_cbl_pwr_on_pull_up_switch(OnOff, cable);
}
pm_err_flag_type pm_dev_pwron_soft_reset(unsigned pmic_chip)
{
	return pm_pwron_soft_reset();
}
pm_err_flag_type pm_dev_pwron_hard_reset_enable(unsigned pmic_chip, boolean enable)
{
	return pm_pwron_hard_reset_enable(enable);
}
pm_err_flag_type pm_dev_pwron_hard_reset_delay_timer_set(unsigned pmic_chip, int delay_timer_ms)
{
	return pm_pwron_hard_reset_delay_timer_set(delay_timer_ms);
}
pm_err_flag_type pm_dev_pwron_hard_reset_debounce_timer_set(unsigned pmic_chip, int debounce_timer_ms)
{
	return pm_pwron_hard_reset_debounce_timer_set(debounce_timer_ms);
}
pm_err_flag_type pm_dev_pwron_stay_on_enable(unsigned pmic_chip, boolean enable)
{
	return pm_pwron_stay_on_enable(enable);
}
