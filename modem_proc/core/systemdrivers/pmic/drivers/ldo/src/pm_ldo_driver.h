#ifndef PM_LDO_DRIVER__H
#define PM_LDO_DRIVER__H

/*! \file pm_ldo.h
 *  \n
 *  \brief   
 *  \details  
 *  \n &copy; Copyright 2012-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/ldo/src/pm_ldo_driver.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_pwr_alg.h"

/*===========================================================================

                     STRUCTURE TYPE AND ENUM

===========================================================================*/

#define PM_MAX_NUM_LDO       32

/*===========================================================================

                     LDO TYPES AND STRUCTURES 

===========================================================================*/
typedef struct
{
    boolean           periph_exists;
    pmiC_IComm        *comm_ptr;
    pm_pwr_data_type  pm_pwr_data;
    uint8             periph_subtype[PM_MAX_NUM_LDO];
} pm_ldo_data_type;

/*===========================================================================

                     FUNCTION DECLARATION 

===========================================================================*/
void pm_ldo_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

pm_ldo_data_type* pm_ldo_get_data(uint8 pmic_index);

pm_err_flag_type pm_ldo_volt_level(uint8 pmic_chip, uint8 smps_peripheral_index, pm_volt_level_type volt_level);

#endif /* PM_LDO_DRIVER__H */
