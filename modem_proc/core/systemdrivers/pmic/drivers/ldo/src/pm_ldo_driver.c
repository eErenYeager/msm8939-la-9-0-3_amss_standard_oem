/*! \file pm_ldo_driver.c
*  \n
*  \brief  
*  \n 
*  \details  
*  \n &copy; Copyright 2012-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/ldo/src/pm_ldo_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/26/13   rk      removing pin configuration functionality.
12/06/12   hw      Rearchitecturing module driver to peripheral driver
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_ldo_driver.h"
#include "pm_target_information.h"
#include "pm_malloc.h"

/*===========================================================================

                        STATIC VARIABLES

===========================================================================*/

/* Static global variable to store the LDO driver data */
static pm_ldo_data_type pm_ldo_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                     LOCAL FUNCTION PROTOTYPE

===========================================================================*/

static pm_pwr_volt_info_type* pm_ldo_get_volt_setting_info(peripheral_info_type *peripheral_info);

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS

===========================================================================*/

void pm_ldo_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_ldo_data_type *ldo_ptr = NULL;
    uint32 ldo_index = 0;
    pm_register_address_type base_address = 0;
    pm_register_address_type periph_offset = 0;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg = 0x0;
    pm_register_data_type data = 0;
    const char* prop_id_arr[] = {PM_PROP_LDOA_NUM, PM_PROP_LDOB_NUM};

    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    ldo_ptr = &pm_ldo_data_arr[pmic_index];

    if (ldo_ptr->periph_exists == FALSE)
    {
        ldo_ptr->periph_exists = TRUE;

        /* Assign Comm ptr */
        ldo_ptr->comm_ptr = comm_ptr;

        /* LDO Register Info - Obtaining Data through dal config */
        ldo_ptr->pm_pwr_data.pwr_reg_table = (pm_pwr_register_info_type*)pm_target_information_get_common_info(PM_PROP_LDO_REG);

        CORE_VERIFY_PTR(ldo_ptr->pm_pwr_data.pwr_reg_table);

        /* LDO Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));

        ldo_ptr->pm_pwr_data.num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(ldo_ptr->pm_pwr_data.num_of_peripherals != 0);

        /* LDO pwr rail specific info pointer malloc to save all the peripheral's base address, Type, Range and Vset */
        pm_malloc(sizeof(pm_pwr_specific_info_type)*(ldo_ptr->pm_pwr_data.num_of_peripherals), (void**)&(ldo_ptr->pm_pwr_data.pwr_specific_info));

        CORE_VERIFY_PTR(ldo_ptr->pm_pwr_data.pwr_specific_info);

        /* Save first LDO peripheral's base address */
        ldo_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address = peripheral_info->base_address;

        DALSYS_memset(ldo_ptr->periph_subtype, 0, sizeof(ldo_ptr->periph_subtype));
    }

    if (ldo_ptr->periph_exists == TRUE)
    {
        base_address = ldo_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address;
        periph_offset = ldo_ptr->pm_pwr_data.pwr_reg_table->peripheral_offset;

        /* Peripheral base_address should be >= first peripheral's base addr */
        CORE_VERIFY(peripheral_info->base_address >= base_address);

        /* Calculate LDO peripheral index */
        ldo_index = ((peripheral_info->base_address - base_address)/periph_offset);

        /* Peripheral Index should be less than max number of peripherals */
        CORE_VERIFY(ldo_index < PM_MAX_NUM_LDO);

        /* Save LDO's Peripheral Subtype value */
        ldo_ptr->periph_subtype[ldo_index] = peripheral_info->peripheral_subtype;

        /* Peripheral Index should be less than number of peripherals */
        CORE_VERIFY(ldo_index < (ldo_ptr->pm_pwr_data.num_of_peripherals));

        /* Save LDO's Peripheral Type value */
        ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].periph_type = peripheral_info->peripheral_type;

        /* Save each LDO peripheral's base address */
        ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].periph_base_address = peripheral_info->base_address;

        if (PM_HW_MODULE_ULT_LDO == ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].periph_type)
        {
            ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].pwr_range = 0;
        }
        else
        {
            /* Read current LDO's range value and save it */
            reg = (pm_register_address_type)(peripheral_info->base_address + ldo_ptr->pm_pwr_data.pwr_reg_table->VOLTAGE_CTRL1);
            err_flag = ldo_ptr->comm_ptr->ReadByte(ldo_ptr->comm_ptr, reg, &data, 0);

            if (PM_ERR_FLAG__SUCCESS != err_flag)
            {
                ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].pwr_range = PM_VOLT_INVALID_RANGE;
            }
            else
            {
                ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].pwr_range = data;
            }
        }
        /* LDO Vset Info - Obtaining Data through dal config */
        ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].pwr_vset = pm_ldo_get_volt_setting_info(peripheral_info);

        CORE_VERIFY_PTR(ldo_ptr->pm_pwr_data.pwr_specific_info[ldo_index].pwr_vset);
    }
}

pm_pwr_volt_info_type* pm_ldo_get_volt_setting_info(peripheral_info_type *peripheral_info)
{
    pm_pwr_volt_info_type *volt_info = NULL;

    switch ((pm_hw_module_type)peripheral_info->peripheral_type)
    {
        case PM_HW_MODULE_LDO:
        {
            switch(peripheral_info->peripheral_subtype)
            {
                case PM_HW_MODULE_LDO_N50:
                case PM_HW_MODULE_LDO_N150:
                case PM_HW_MODULE_LDO_N300:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_NMOS_VOLT);
                    break;
                case PM_HW_MODULE_LDO_N600:
                case PM_HW_MODULE_LDO_N600_STEPPER:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_N600_VOLT);
                    break;
                case PM_HW_MODULE_LDO_N1200:
                case PM_HW_MODULE_LDO_N1200_STEPPER:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_N1200_VOLT);
                    break;
                case PM_HW_MODULE_LDO_P50:
                case PM_HW_MODULE_LDO_P150:
                case PM_HW_MODULE_LDO_P300:
                case PM_HW_MODULE_LDO_P600:
                case PM_HW_MODULE_LDO_P1200:
                case PM_HW_MODULE_LDO_LV_P50:
                case PM_HW_MODULE_LDO_LV_P150:
                case PM_HW_MODULE_LDO_LV_P300:
                case PM_HW_MODULE_LDO_LV_P600:
                case PM_HW_MODULE_LDO_LV_P1200:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_PMOS_VOLT);
                    break;
                case PM_HW_MODULE_LDO_USB_LDO:
                case PM_HW_MODULE_LDO_LN_LDO:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_LN_VOLT);
                    break;
                default:
                    volt_info = NULL;
                    break;
            }
        }
        break;
		
        case PM_HW_MODULE_ULT_LDO:
        {
            switch(peripheral_info->peripheral_subtype)
            {
                case PM_HW_MODULE_LDO_N300_STEPPER:
                case PM_HW_MODULE_LDO_N600_STEPPER:
                case PM_HW_MODULE_LDO_N900_STEPPER:
                case PM_HW_MODULE_LDO_N1200_STEPPER:
                {
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_NMOS_VOLT);
                }
                break;

                case PM_HW_MODULE_LDO_P50:
                case PM_HW_MODULE_LDO_P150:
                case PM_HW_MODULE_LDO_P300:
                case PM_HW_MODULE_LDO_P450:
                case PM_HW_MODULE_LDO_P600:
                case PM_HW_MODULE_LDO_P1200:
                case PM_HW_MODULE_LDO_LV_P50:
                case PM_HW_MODULE_LDO_LV_P150:
                case PM_HW_MODULE_LDO_LV_P300:
                    volt_info = (pm_pwr_volt_info_type*)pm_target_information_get_common_info(PM_PROP_PMOS_VOLT);
                break;

                default:
                {
                    // we should not reach this point
                }
                break;
            } // end submodule switch
        }
        break;
        
		default:
            volt_info = NULL;
        break;
    }

    return volt_info;
}

pm_ldo_data_type* pm_ldo_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_ldo_data_arr[pmic_index];
    }

    return NULL;
}
