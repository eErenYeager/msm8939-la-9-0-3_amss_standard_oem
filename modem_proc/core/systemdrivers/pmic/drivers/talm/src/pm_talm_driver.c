/*! \file
 *  \n
 *  \brief  pm_talm_driver.c
 *  \details  
 *  \n &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/talm/src/pm_talm_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/01/13   rh      Moved to pm_talm_driver.c
02/22/11   wra     Added the delete before the target setting initialization
02/03/11   hs      Added initialization of the module.
12/20/10   wra     Initial Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_talm_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the talm data */
static pm_talm_data_type pm_talm_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_talm_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
	pm_talm_data_type *talm_ptr = NULL;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
	const char* prop_id_arr[] = {PM_PROP_TALMA_NUM, PM_PROP_TALMB_NUM};
	
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
	
	talm_ptr = &pm_talm_data_arr[pmic_index];
		
    if (talm_ptr->periph_exists == FALSE)
    {
        talm_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        talm_ptr->comm_ptr = comm_ptr;
		
		/* talm Register Info - Obtaining Data through dal config */
        talm_ptr->talm_register = (pm_talm_register_info_type*)pm_target_information_get_common_info(PM_PROP_TALM_REG);
		
        CORE_VERIFY_PTR(talm_ptr->talm_register);
		
        talm_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(talm_ptr->num_of_peripherals != 0);
    }
}

pm_talm_data_type* pm_talm_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_talm_data_arr[pmic_index];
    }

    return NULL;
}
