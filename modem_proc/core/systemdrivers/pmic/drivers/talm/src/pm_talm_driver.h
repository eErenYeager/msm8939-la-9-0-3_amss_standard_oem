#ifndef PM_TALM_DRIVER__H
#define PM_TALM_DRIVER__H

/*! \file
*  \n
*  \brief  pm_talm_driver.h 
*  \details  This file contains functions prototypes and variable/type/constant
*  declarations for supporting TALM pin services for the Qualcomm
*  PMIC chip set.
*  \n &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/talm/src/pm_talm_driver.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/10/12   hs       Changed the type for internalResourceIndex from int to uint8.
12/13/11   ekwon   created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_target_information.h"
#include "icomm.h"


/*===========================================================================

                     TYPE DEFINITIONS 

===========================================================================*/
typedef struct pm_talm_register_info_type
{
    pm_register_address_type    base_address;  
    pm_register_address_type    peripheral_offset;
    pm_register_address_type    status1;                 //0x08
    pm_register_address_type    shutdown_ctl1;           //0x42
}pm_talm_register_info_type;

typedef struct pm_talm_data_type
{
	boolean      periph_exists;
	pmiC_IComm   *comm_ptr;
	
    pm_talm_register_info_type* talm_register;
    uint8	num_of_peripherals;
}pm_talm_data_type;

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
void pm_talm_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

pm_talm_data_type* pm_talm_get_data(uint8 pmic_index);
#endif //PM_TALM_DRIVER__H
