/*! \file
*  
*  \brief  pm_talm ----File contains public APIs for TALM resource type.
*  \details Implementation file for TALM resourece type.
*  Each of the APIs checks for access and then if necessary directs
*  the call to Driver implementation or to RPC function for the master processor.
*  
*    PMIC code generation Version: 2.0.0.19
*    This file contains code for Target specific settings and modes.
*  
*  &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/talm/src/pm_talm.c#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/10/12   hs       Changed the type for internalResourceIndex from int to uint8.

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_itemp.h"
#include "pm_talm_driver.h"

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_itemp_get_stage(pm_item_stage_type  *itemp_stage)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type reg_data = 0 ;
    pm_register_address_type reg = 0 ;
	pm_talm_data_type *talm_ptr = pm_talm_get_data(0);
	
	if ((talm_ptr == NULL) || (talm_ptr->periph_exists == FALSE))
    {
        errFlag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else 
    {
        if ( NULL != itemp_stage)
        {
            reg = talm_ptr->talm_register->base_address + talm_ptr->talm_register->shutdown_ctl1;

            errFlag = talm_ptr->comm_ptr->ReadByte(talm_ptr->comm_ptr, reg, &reg_data, 0 );

            *itemp_stage = (pm_item_stage_type)( 0x03 & reg_data);
        }
        else
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
    }

    /* Let the user know if we were successful or not */
    return errFlag;
}

pm_err_flag_type pm_itemp_stage_override(pm_switch_cmd_type  oride_cmd, pm_item_oride_type  oride_stage)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg = 0 ;
	pm_talm_data_type *talm_ptr = pm_talm_get_data(0);
	
	if ((talm_ptr == NULL) || (talm_ptr->periph_exists == FALSE))
    {
        errFlag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else 
    {
        if ( oride_cmd >= PM_INVALID_CMD)
        {
            errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
        }
        else if(oride_stage >= PM_ITEMP_ORIDE_OUT_OF_RANGE)
        {
            errFlag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
        }
        else
        {
            reg = talm_ptr->talm_register->base_address + talm_ptr->talm_register->shutdown_ctl1;
            if( PM_ON_CMD == oride_cmd)
            {
                errFlag = talm_ptr->comm_ptr->WriteByteMask(talm_ptr->comm_ptr, reg, 0x40, 0xFF, 0);
            }
            else
            {
                errFlag = talm_ptr->comm_ptr->WriteByteMask(talm_ptr->comm_ptr, reg, 0x40, 0, 0);
            }
        }
    }

    /* Let the user know if we were successful or not */
    return errFlag;            
}

pm_err_flag_type pm_itemp_thresh_cntrl(pm_itemp_threshold_type   thresh_value)
{
    pm_err_flag_type      errFlag   = PM_ERR_FLAG__SUCCESS;
    pm_register_data_type reg_data = 0;
    pm_register_address_type reg = 0 ;
	pm_talm_data_type *talm_ptr = pm_talm_get_data(0);
	
	if ((talm_ptr == NULL) || (talm_ptr->periph_exists == FALSE))
    {
        errFlag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if(thresh_value >= PM_INVALID_INPUT)
    {
        errFlag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE ;
    }
    else
    {
        switch(thresh_value)
        {
            case PM_TEMP_THRESH_CTRL0:
                reg_data = 0;
                break;
            case PM_TEMP_THRESH_CTRL1:
                reg_data = 1;
                break;
            case PM_TEMP_THRESH_CTRL2:
                reg_data = 2;
                break;
            case PM_TEMP_THRESH_CTRL3:
                reg_data = 3;
                break;
            default:
                break;
        }

        reg = (pm_register_address_type)(talm_ptr->talm_register->base_address + talm_ptr->talm_register->shutdown_ctl1);
        errFlag = talm_ptr->comm_ptr->WriteByteMask(talm_ptr->comm_ptr, reg, 0x03, reg_data, 0);
    }

    /* Let the user know if we were successful or not */
    return errFlag;            
}

/*===========================================================================

                        DEPRECATED FUNCTIONS

===========================================================================*/
pm_err_flag_type pm_dev_itemp_get_stage(unsigned pmic_chip, pm_item_stage_type *itemp_stage )
{
	return pm_itemp_get_stage(itemp_stage);
}
pm_err_flag_type pm_dev_itemp_stage_override(unsigned pmic_chip, pm_switch_cmd_type oride_cmd, pm_item_oride_type oride_stage)
{
	return pm_itemp_stage_override(oride_cmd, oride_stage);
}
pm_err_flag_type pm_dev_itemp_thresh_cntrl(unsigned pmic_chip, pm_itemp_threshold_type  thresh_value )
{
	return pm_itemp_thresh_cntrl(thresh_value);
}

