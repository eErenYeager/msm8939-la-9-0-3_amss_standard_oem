/*! \file pm_clk_driver.c
 *  \n
 *  \brief  
 *  \n  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/clk/src/pm_clk_driver.c#1 $
 
when        who     what, where, why
--------    ---     ----------------------------------------------------------
12/10/13   rh      File created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_clk_driver.h"
#include "pm_version.h"
#include "pm_target_information.h"
#include "DALSys.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the CLK driver data */
static pm_clk_data_type pm_clk_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                     LOCAL FUNCTION PROTOTYPE

===========================================================================*/
static void pm_clk_periph_map_init(pm_clk_data_type *clk_ptr);

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS 

===========================================================================*/

void pm_clk_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_clk_data_type *clk_ptr = NULL;
    uint32 clk_index = 0;
    pm_register_address_type base_address = 0;
    pm_register_address_type periph_offset = 0;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);

    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    clk_ptr = &pm_clk_data_arr[pmic_index];
    
    if (clk_ptr->periph_exists == FALSE)
    {
        clk_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        clk_ptr->comm_ptr = comm_ptr;

        /* Clock Register Info - Obtaining Data through dal config */
        clk_ptr->clk_reg_table = (pm_clk_register_info_type*)pm_target_information_get_common_info(PM_PROP_CLK_REG);

        CORE_VERIFY_PTR(clk_ptr->clk_reg_table);

        clk_ptr->clk_sleep_reg_table = (pm_clk_sleep_register_info_type*)pm_target_information_get_common_info(PM_PROP_CLK_SLEEP_REG);

        CORE_VERIFY_PTR(clk_ptr->clk_sleep_reg_table);

        /* Clock Peripheral Map Info - Obtaining Data through dal config */
        clk_ptr->clk_common = (pm_clk_type*)pm_target_information_get_common_info(PM_PROP_CLK_COMMON);

        CORE_VERIFY_PTR(clk_ptr->clk_common);

        /* This is kept for design consistency with other drivers. This is not really
           relied upon in clk case and will be auto-detected in the future */
        clk_ptr->num_of_peripherals = (uint32)PM_MAX_NUM_CLKS;

        CORE_VERIFY(clk_ptr->num_of_peripherals <= PM_MAX_NUM_CLKS);

        /* Link a periph index to the device config data through a mapping table */
        pm_clk_periph_map_init(clk_ptr);

        DALSYS_memset(clk_ptr->periph_subtype, 0, sizeof(clk_ptr->periph_subtype));
    }
    
    if (clk_ptr->periph_exists == TRUE)
    {
        base_address = clk_ptr->clk_reg_table->base_address;
        periph_offset = clk_ptr->clk_reg_table->peripheral_offset;

        /* Peripheral base_address should be >= first peripheral's base addr */
        CORE_VERIFY(peripheral_info->base_address >= base_address);

        /* Calculate CLK peripheral index */
        clk_index = ((peripheral_info->base_address - base_address)/periph_offset);

        /* Peripheral Index should be less than max number of peripherals */
        CORE_VERIFY(clk_index < PM_MAX_NUM_CLKS);

        /* Save Clock's Peripheral Subtype value */
        clk_ptr->periph_subtype[clk_index] = peripheral_info->peripheral_subtype;
    }
}

void pm_clk_periph_map_init(pm_clk_data_type *clk_ptr)
{
   uint8 i = 0;
   uint8 periph = 0;

   /* Initialize clk_periph mapping data */
   DALSYS_memset(clk_ptr->periph_map, PM_CLK_INVALID_DATA, sizeof(clk_ptr->periph_map));

   /* Go through device config table that contains the supported periph
      data and create a mapping table to be used when PMIC API is called.
      This mapping table is created to link between API level peripheral
      enum and it's periph index in the structure. 
      The goal is to allow the order of the API level peripheral enum to
      change without affecting the periph index used by the register. */
   for(i = 0; i < PM_MAX_NUM_CLKS; i++)
   {
      /* Local var periph created to satisfy KW */
      periph = clk_ptr->clk_common[i];

      if (periph < PM_ALL_CLKS)
      {
         clk_ptr->periph_map[periph] = i;
      }
   }
}

pm_clk_data_type* pm_clk_get_data(uint8 pmic_index)
{
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    return &pm_clk_data_arr[pmic_index];
}
