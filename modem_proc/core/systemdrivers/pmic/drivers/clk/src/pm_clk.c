/*! \file pm_clk.c
*  \n 
*  \brief Implementation file for CLK resource type.
*  \n  
*  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/clk/src/pm_clk.c#1 $
 
when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/10/13   rh      File created
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_clk_buffer.h"
#include "pm_clk_driver.h"

/*===========================================================================

                     API IMPLEMENTATION 

===========================================================================*/

//pm_err_flag_type pm_clk_drv_strength(uint8 pmic_chip, pm_clk_type periph, pm_clk_drv_strength_type drive_strength)
pm_err_flag_type
pm_dev_clk_buff_set_output_drive_strength(unsigned pmic_chip,
                                          int periph,
                                          pm_clk_buff_output_drive_strength_type drive_strength)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_clk_data_type *clk_ptr = pm_clk_get_data(pmic_chip);
    pm_register_address_type reg = 0;
    uint32 periph_index = 0;

    if ((clk_ptr == NULL) || (clk_ptr->periph_exists == FALSE) || (periph > PM_SLEEP_CLK1) || 
        (drive_strength >= PM_CLK_DRV_STRENGTH_INVALID))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    periph_index = clk_ptr->periph_map[periph];

    if((periph_index == PM_CLK_INVALID_DATA) || (periph_index >= PM_MAX_NUM_CLKS) ||
       (clk_ptr->periph_subtype[periph_index] == 0))
    {
        return PM_ERR_FLAG__INVALID_CLK_INDEXED;
    }

    if (clk_ptr->periph_subtype[periph_index] == PM_HW_MODULE_CLOCK_XO_CORE)
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    reg = clk_ptr->clk_reg_table->base_address + (pm_register_address_type)(clk_ptr->clk_reg_table->peripheral_offset*periph_index) + 
          clk_ptr->clk_reg_table->DRV_CTL1;

    /* Set strength */
    err_flag = clk_ptr->comm_ptr->WriteByteMask(clk_ptr->comm_ptr, reg, 0x03, (pm_register_data_type)(drive_strength), 0);  // 1:0
    
    /* Let the user know if we were successful or not */
    return err_flag;
}

//pm_err_flag_type pm_dev_clk_buff_set_output_drive_strength(unsigned pmic_chip, 
//                                                           pm_clk_type periph, 
//                                                           pm_clk_drv_strength_type drive_strength)
pm_err_flag_type
pm_clk_buff_set_output_drive_strength(int resourceIndex,
                                      pm_clk_buff_output_drive_strength_type drive_strength)
{
	return pm_dev_clk_buff_set_output_drive_strength(0, resourceIndex, drive_strength);
}