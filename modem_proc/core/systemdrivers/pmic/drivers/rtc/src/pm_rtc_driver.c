/*! \file
 *  \n
 *  \brief  pm_rtc_driver.c
 *  \details  
 *  \n &copy; Copyright 2010-2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/rtc/src/pm_rtc_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/18/11   wra     mega clocks module V6 Initial Creation
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_rtc_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the RTC data */
static pm_rtc_data_type pm_rtc_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_rtc_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
	pm_rtc_data_type *rtc_ptr = NULL;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
	const char* prop_id_arr[] = {PM_PROP_RTCA_NUM, PM_PROP_RTCB_NUM};
	
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
	
	rtc_ptr = &pm_rtc_data_arr[pmic_index];
		
    if (rtc_ptr->periph_exists == FALSE)
    {
        rtc_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        rtc_ptr->comm_ptr = comm_ptr;
		
		/* RTC Register Info - Obtaining Data through dal config */
        rtc_ptr->megartc_register = (pm_rtc_register_info_type*)pm_target_information_get_common_info(PM_PROP_RTC_REG);
		
        CORE_VERIFY_PTR(rtc_ptr->megartc_register);
		
        rtc_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(rtc_ptr->num_of_peripherals != 0);
    }
}

pm_rtc_data_type* pm_rtc_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_rtc_data_arr[pmic_index];
    }

    return NULL;
}