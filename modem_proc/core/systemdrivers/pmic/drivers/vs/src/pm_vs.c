/*! \file
*  
*  \brief  pm_vs_api.c
*  
*  &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/drivers/vs/src/pm_vs.c#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_vs_driver.h"
#include "pm_resource_manager.h"
#include "pm_resources_and_types.h"

/*===========================================================================

                     API IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_vs_sw_mode_status(uint8 pmic_chip, uint8 perph_index, pm_sw_mode_type* sw_mode) 
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_vs_data_type *vs_ptr = pm_vs_get_data(pmic_chip);

    if ((vs_ptr == NULL) || (vs_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    { 
        err_flag = pm_pwr_sw_mode_status_alg(&(vs_ptr->pm_pwr_data), vs_ptr->comm_ptr, perph_index, sw_mode);
    }
    return err_flag;
}

pm_err_flag_type pm_vs_pin_ctrled_status(uint8 pmic_chip, uint8 perph_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_vs_data_type *vs_ptr = pm_vs_get_data(pmic_chip);
	
    if ((vs_ptr == NULL) || (vs_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {  
        err_flag = pm_pwr_pin_ctrl_status_alg(&(vs_ptr->pm_pwr_data), vs_ptr->comm_ptr, perph_index, on_off);
    }
    return err_flag;
}

pm_err_flag_type pm_vs_sw_enable_status(uint8 pmic_chip, uint8 perph_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_vs_data_type *vs_ptr = pm_vs_get_data(pmic_chip);
	
    if ((vs_ptr == NULL) || (vs_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    { 
        err_flag = pm_pwr_sw_enable_status_alg(&(vs_ptr->pm_pwr_data), vs_ptr->comm_ptr, perph_index, on_off);
    }
    return err_flag;
}
