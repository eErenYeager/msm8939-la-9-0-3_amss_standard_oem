#ifndef __PM_DIAG_PWR_RAILS_H__
#define __PM_DIAG_PWR_RAILS_H__
/*
===========================================================================
*/
/**
  @file pm_diag_pwr_rails.h

  Internal header file for the PMIC Power Rails DIAG functionality.
*/
/*
  ====================================================================

  Copyright (c) 2012 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/utils/diag/pm_diag_pwr_rails.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when          who        what, where, why
  ------------------------------------------------------------------ 
08/26/13      rk         removing unused header files.

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_smps.h"
#include "pm_ldo.h"

/*=========================================================================
      Macro Definitions
==========================================================================*/

/*
 * # of ANSI characters(bytes) allocated for a Power rail name
 */
#define PM_DIAG_PWR_RAILS_BUF_NAME_LEN      16

/*
 * PMIC Power Rails Driver Subsystem Command ID
 */
#define PM_DIAG_PWR_RAILS_LOG_CMD_ID        10


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * This structure is used to describe the Diag log packet 
 * sent back to the test tool. This structure is appended 
 * to Diag response packet structure in memory prior to 
 * submitting to Diag.
 */
typedef PACK(struct)
{
  uint64       timestamp;                                   /* Power rail log time stamp */
  uint32       volt_level;                                  /* Power rail Voltage Level */
  char         pmic_name[PM_DIAG_PWR_RAILS_BUF_NAME_LEN];   /* PMIC Model name to which the Power Rail belongs */
  uint8        pmic_model_type;                             /* PMIC Model type to which the Power Rail belongs */
  uint8        periph_type;                                 /* Power Rail type (SMPS,BOOST,LDO,VS) */
  uint8        periph_index;                                /* Power Rail peripheral index */
  uint8        enable_status;                               /* Power rail Enable Status */
  uint8        sw_status;                                   /* Power rail SW Enable Status */
  uint8        pin_ctrl_status;                             /* Power rail Pin Ctrl Status */
  uint8        mode_type;                                   /* Power rail Mode Type (LPM,NPM,AUTO,BYPASS) */
} pwr_rails_diag_log_item_type;


/*=========================================================================
      Function Definitions
==========================================================================*/

/* =========================================================================
**  Function : pm_diag_pwr_rails_init
** =========================================================================*/
/**
  Initialization function for the PMIC Power Rails Diag functionality.

  @param
  None.

  @return
  None.

  @dependencies
  None.
*/
void pm_diag_pwr_rails_init (void);

#endif /* !__PM_DIAG_PWR_RAILS_H__ */

