/*! \file pm_processor.c
*  
*  \brief This file contains processor specific initialization functions.
*         This file contains code for Target specific settings and modes.
*  
*  &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/framework/src/pm_processor.c#1 $


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/18/11   wra     Fixed kloc work warnings  

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_processor.h"
#include "pm_resource_manager.h"
#include "pm_target_information.h"
#include "comm_manager.h"
#include "comm_type.h"
#include "hw_module_type.h"
#include "CoreVerify.h"
#include "pm_malloc.h"
#include "spmi_1_0.h"
#include "DDISpmi.h" // DAL_SPMI

#ifdef FEATURE_PMIC_DIAG_SERVICES
#include "pmapp_diag.h"
#include "pm_diag_pwr_rails.h"
#endif

static uint32 number_of_pmics = 0;

/*! \brief Internal function. This function initialized the Comm.
 *  \param[in] None.
 *
 *  \return None.
 *
 *  <b>Dependencies</b>
 *  \li pmiC_CommManager_CreateSingleton() .
 */
static void pm_processor_initialize_comms(void);

void pm_processor_init()
{
    number_of_pmics = pm_target_information_get_count_info(PM_PROP_PMIC_NUM);
	
    /* Num of PMICs cannot be 0 */
    CORE_VERIFY(number_of_pmics != 0);
    
    pmiC_CommManager_CreateSingleton(number_of_pmics);

    // Initialize Comms. These may already be initialized if this is an off-target test.
    if(!pmiC_CommManager_GetSingleton()->IsInitialized)
{
        pm_processor_initialize_comms();
}
}

void pm_processor_initialize_comms(void)
{
    DalDeviceHandle *dalHandle = NULL;
    pmiC_CommManager *commManager = pmiC_CommManager_GetSingleton();

    static pmiC_DeviceInfo deviceInfo0;
    static pmiC_CommInfo CommInfo0;
    static pmiC_CommInfo CommInfo1;
    
    static pmiC_DeviceInfo deviceInfo1;
    static pmiC_CommInfo CommInfo2;
    static pmiC_CommInfo CommInfo3;

    if ( DAL_StringDeviceAttach( "DALDEVICEID_SPMI_DEVICE" , &dalHandle ) == DAL_SUCCESS )
    {    
        if ( DalDevice_Open ( dalHandle , DAL_OPEN_SHARED ) == DAL_SUCCESS )
        {
            /* Each PMIC device has 2 slave ids */

            /* Primary PMIC Comm Initialization */
            pmiC_DeviceInfo_Init(&deviceInfo0, QcPmicDevice, PmDeviceIndex1);

            /* Primary PMIC Comm Initialization for slave id index 0 */
            pmiC_CommInfo_Init(&CommInfo0, &deviceInfo0, SPMI_1_0_TYPE, 0, dalHandle);
            pmiC_CommManager_AddCommInterface(commManager, &CommInfo0);

            /* Primary PMIC Comm Initialization for slave id index 1 */
            pmiC_CommInfo_Init(&CommInfo1, &deviceInfo0, SPMI_1_0_TYPE, 1, dalHandle);
            pmiC_CommManager_AddCommInterface(commManager, &CommInfo1);

            if(number_of_pmics == 2)
            {
                /* Secondary PMIC Comm Initialization */
                pmiC_DeviceInfo_Init(&deviceInfo1, QcPmicDevice, PmDeviceIndex2);

                /* Secondary PMIC Comm Initialization for slave id index 0 */
                pmiC_CommInfo_Init(&CommInfo2, &deviceInfo1, SPMI_1_0_TYPE, 0, dalHandle);
                pmiC_CommManager_AddCommInterface(commManager, &CommInfo2);

                /* Secondary PMIC Comm Initialization for slave id index 1 */
                pmiC_CommInfo_Init(&CommInfo3, &deviceInfo1, SPMI_1_0_TYPE, 1, dalHandle);
                pmiC_CommManager_AddCommInterface(commManager, &CommInfo3);
            }
            commManager->IsInitialized = TRUE;
		}
	}
	else
	{
		commManager->IsInitialized = FALSE;
	}
}

pm_err_flag_type pm_processor_create_comm_channel(pmiC_CommInfo*  commInfo, pmiC_IComm**  pCurrentComm, pmiC_IComm**  pNewComm)
{
    pm_err_flag_type errPowerup = PM_ERR_FLAG__SUCCESS;
    CommType commType = commInfo->mCommType;     
    
    switch(commType)
    {
    case SPMI_1_0_TYPE:
        {
            pm_malloc(sizeof(pmiC_SPMI_1_0), (void**)pNewComm);
            pmiC_SPMI_1_0_Init( (pmiC_SPMI_1_0*)*pNewComm, pCurrentComm, commInfo->mBusIndex);
            break;
       }               
       default :
       {
          errPowerup = PM_ERR_FLAG__COMM_TYPE_NOT_RECOGNIZED;
          break;
       }
    }/* switch */
    
    return errPowerup;
}

pm_err_flag_type pm_processor_resolve_peripheral_version(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;

    switch(comm_ptr->mpCommInfo->mDeviceInfo->mDeviceType)
    {
    case QcPmicDevice:
       {
           err = pm_target_information_read_peripheral_rev(comm_ptr, peripheral_info);
           break;
       }
    default:
       {
           err = PM_ERR_FLAG__DEV_MISMATCH;
       }
    }

    return err;            
}
