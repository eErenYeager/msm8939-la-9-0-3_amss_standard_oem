#ifndef PM_DAL_PROP_IDS_H
#define PM_DAL_PROP_IDS_H
/*! \file
 *  
 *  \brief  pm_dal_prop_id.h ----This file contains all the available PMIC DAL device config property values.
 *  \details This file contains all the available PMIC DAL device config property values.
 *  
 *  &copy; Copyright 2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/framework/inc/pm_dal_prop_ids.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/25/13   hs      Created.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

//PMIC HW peripheral specific dal properties -- starting from 1
#define PM_PROP_CLK_REG                        "PM_PROP_CLK_REG"
#define PM_PROP_SMPS_REG                       "PM_PROP_SMPS_REG"
#define PM_PROP_LDO_REG                        "PM_PROP_LDO_REG"
#define PM_PROP_VS_REG                         "PM_PROP_VS_REG"
#define PM_PROP_BOOST_REG                      "PM_PROP_BOOST_REG"
#define PM_PROP_FTS_VOLT                       "PM_PROP_FTS_VOLT"
#define PM_PROP_HFS_VOLT                       "PM_PROP_HFS_VOLT"
#define PM_PROP_BOOST_VOLT                     "PM_PROP_BOOST_VOLT"
#define PM_PROP_NMOS_VOLT                      "PM_PROP_NMOS_VOLT"
#define PM_PROP_N600_VOLT                      "PM_PROP_N600_VOLT"
#define PM_PROP_N1200_VOLT                     "PM_PROP_N1200_VOLT"
#define PM_PROP_PMOS_VOLT                      "PM_PROP_PMOS_VOLT"
#define PM_PROP_LN_VOLT                        "PM_PROP_LN_VOLT"
#define PM_PROP_FTS_RANGE                      "PM_PROP_FTS_RANGE"
#define PM_PROP_HFS_RANGE                      "PM_PROP_HFS_RANGE"
#define PM_PROP_BOOST_RANGE                    "PM_PROP_BOOST_RANGE"
#define PM_PROP_NMOS_RANGE                     "PM_PROP_NMOS_RANGE"
#define PM_PROP_N600_RANGE                     "PM_PROP_N600_RANGE"
#define PM_PROP_N1200_RANGE                    "PM_PROP_N1200_RANGE"
#define PM_PROP_PMOS_RANGE                     "PM_PROP_PMOS_RANGE"
#define PM_PROP_LN_RANGE                       "PM_PROP_LN_RANGE"
#define PM_PROP_LDO_SETTLING_TIME              "PM_PROP_LDO_SETTLING_TIME"
#define PM_PROP_SMPS_SETTLING_TIME             "PM_PROP_SMPS_SETTLING_TIME"
#define PM_PROP_VS_SETTLING_TIME               "PM_PROP_VS_SETTLING_TIME"
#define PM_PROP_BOOST_SETTLING_TIME            "PM_PROP_BOOST_SETTLING_TIME"
#define PM_PROP_CLK_SLEEP_REG                  "PM_PROP_CLK_SLEEP_REG"
#define PM_PROP_CLK_XO_REG                     "PM_PROP_CLK_XO_REG"
#define PM_PROP_CLK_COMMON                     "PM_PROP_CLK_COMMON"
#define PM_PROP_PWRON_REG			           "PM_PROP_PWRON_REG"
#define PM_PROP_GPIO_REG			           "PM_PROP_GPIO_REG"
#define PM_PROP_MPP_REG			          	   "PM_PROP_MPP_REG"
#define PM_PROP_RTC_REG			          	   "PM_PROP_RTC_REG"
#define PM_PROP_TALM_REG			           "PM_PROP_TALM_REG"
#define PM_PROP_SMPS_ILIMIT_LUT			       "PM_PROP_SMPS_ILIMIT_LUT"
#define PM_PROP_BOOST_BYP_SETTLING_TIME        "PM_PROP_BOOST_BYP_SETTLING_TIME"
#define PM_PROP_FTS2p5_VOLT                    "PM_PROP_FTS2p5_VOLT"
#define PM_PROP_FTS2p5_RANGE                   "PM_PROP_FTS2p5_RANGE"
#define PM_PROP_ULT_BUCK_RANGE_1               "PM_PROP_ULT_BUCK_RANGE_1"
#define PM_PROP_ULT_BUCK_VOLT_1                "PM_PROP_ULT_BUCK_VOLT_1"
#define PM_PROP_ULT_BUCK_RANGE_2               "PM_PROP_ULT_BUCK_RANGE_2"
#define PM_PROP_ULT_BUCK_VOLT_2                "PM_PROP_ULT_BUCK_VOLT_2"
#define PM_PROP_CLK_LDO_RANGE                  "PM_PROP_CLK_LDO_RANGE"
#define PM_PROP_CLK_LDO_VOLT                   "PM_PROP_CLK_LDO_VOLT"


//Target/power grid specific dal properties -- starting from 101
#define PM_PROP_PMIC_NUM                     "PM_PROP_PMIC_NUM"
#define PM_PROP_SMPSA_NUM                    "PM_PROP_SMPSA_NUM"
#define PM_PROP_SMPSB_NUM                    "PM_PROP_SMPSB_NUM"
#define PM_PROP_CLKA_NUM                     "PM_PROP_CLKA_NUM"
#define PM_PROP_CLKB_NUM                     "PM_PROP_CLKB_NUM"
#define PM_PROP_LDOA_NUM                     "PM_PROP_LDOA_NUM"
#define PM_PROP_LDOB_NUM                     "PM_PROP_LDOB_NUM"
#define PM_PROP_VSA_NUM                      "PM_PROP_VSA_NUM"
#define PM_PROP_VSB_NUM                      "PM_PROP_VSB_NUM"
#define PM_PROP_BOOSTA_NUM                   "PM_PROP_BOOSTA_NUM"
#define PM_PROP_BOOSTB_NUM                   "PM_PROP_BOOSTB_NUM"
#define PM_PROP_GPIOA_NUM                    "PM_PROP_GPIOA_NUM"
#define PM_PROP_GPIOB_NUM                    "PM_PROP_GPIOB_NUM"
#define PM_PROP_MPPA_NUM                     "PM_PROP_MPPA_NUM"
#define PM_PROP_MPPB_NUM                     "PM_PROP_MPPB_NUM"
#define PM_PROP_PWRONA_NUM                   "PM_PROP_PWRONA_NUM"
#define PM_PROP_PWRONB_NUM                   "PM_PROP_PWRONB_NUM"
#define PM_PROP_RTCA_NUM                     "PM_PROP_RTCA_NUM"
#define PM_PROP_RTCB_NUM                     "PM_PROP_RTCB_NUM"
#define PM_PROP_TALMA_NUM                    "PM_PROP_TALMA_NUM"
#define PM_PROP_TALMB_NUM                    "PM_PROP_TALMB_NUM"
#define PM_PROP_MEGAXOA_NUM                  "PM_PROP_MEGAXOA_NUM"
#define PM_PROP_PAM_NODE_RSRCS               "PM_PROP_PAM_NODE_RSRCS"
#define PM_PROP_PAM_NODE_NUM                 "PM_PROP_PAM_NODE_NUM"
#define PM_PROP_REMOTE_LDO                   "PM_PROP_REMOTE_LDO"
#define PM_PROP_REMOTE_CLK                   "PM_PROP_REMOTE_CLK"
#define PM_PROP_REMOTE_SMPS                  "PM_PROP_REMOTE_SMPS"
#define PM_PROP_REMOTE_VS                    "PM_PROP_REMOTE_VS"
#define PM_PROP_XO_SPECIFIC                  "PM_PROP_XO_SPECIFIC"
#define PM_PROP_UICC_SPECIFIC                "PM_PROP_UICC_SPECIFIC"
#define PM_PROP_MPP_SPECIFIC                 "PM_PROP_MPP_SPECIFIC"
#define PM_PROP_MX                           "PM_PROP_MX"
#define PM_PROP_CX                           "PM_PROP_CX"
#define PM_PROP_MSS                 	     "PM_PROP_MSS"
#define PM_PROP_GFX                          "PM_PROP_GFX"

//dummy dal properties to get rid of compiler warnings/errors -- staring from 201
#define PM_PROP_PMIC_DUMMY_1                 "PM_PROP_PMIC_DUMMY_1"
#define PM_PROP_PMIC_DUMMY_2                 "PM_PROP_PMIC_DUMMY_2"

//MPM SPMI command dal properties -- starting from 301
#define PM_PROP_MPM_ACTIVE_CMDS               "PM_PROP_MPM_ACTIVE_CMDS"
#define PM_PROP_MPM_SLEEP_CMDS                "PM_PROP_MPM_SLEEP_CMDS"
#define PM_PROP_MPM_CMD_INDICES               "PM_PROP_MPM_CMD_INDICES"

#endif // PM_DAL_PROP_IDS_H
