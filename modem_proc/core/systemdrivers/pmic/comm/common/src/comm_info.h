#ifndef COMMINFO_H
#define COMMINFO_H

/*! \file comm_info.h
 *  \n
 *  \brief Comm Info related structure type and function prototypes
 *  \n 
 *  \n &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/comm/common/src/comm_info.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------        
03/20/12   hs      Added a member vailable for DAL SPMI handle.      
08/19/11   jtn     Refactor spelling error
03/15/11   APU     Removing unused include file.
12/08/10   wra     RPM memory reduction. 
                   Removing unused Variables and methods
========================================================================== */

#include "comm_type.h"
#include "device_info.h" 
#include "DalDevice.h" //DalDeviceHandle

/*! \class CommInfo CommInfo.h 
*  \brief This is the class that defines the information that is needed
*   to create and establish communications IComm objects 
*  \details This class is used by the ResourceManagers to communicate
*   the with the CommManager class. The CommManager class uses the information
*   to setup IComm objects that will then be used by the system PmicResource classes
*   to communicate with HW
*  \note The instances of this class would be generated and destroyed, at runtime,
*  during the creation for application resources in the ResourceManager
*
*  <b>Handles the following</b>
*  \n None
*
*  <b>Listens for the following events</b>
*  \n None
*
*  <b>Generates the following events</b>
*  \n None
 */
typedef struct pmiC_CommInfo_STRUCT
{
   
   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   pmiC_DeviceInfo *mDeviceInfo;
   uint8 mBusIndex;
   CommType mCommType;
   DalDeviceHandle* mDalHandle;
   boolean mDeviceInitialized;
}pmiC_CommInfo;

/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
// This constructor will be used primarily by driver tests. When the DeviceInfo
// class is passed into this constructor it should have already been created and initialized.
void pmiC_CommInfo_Init(pmiC_CommInfo *me, pmiC_DeviceInfo *deviceInfo, CommType commType, uint8 slaveIdIndex, DalDeviceHandle *dalHandle);
void pmiC_CommInfo_InitCopy(pmiC_CommInfo *me, const pmiC_CommInfo *rhs);

#endif // COMMINFO_H
