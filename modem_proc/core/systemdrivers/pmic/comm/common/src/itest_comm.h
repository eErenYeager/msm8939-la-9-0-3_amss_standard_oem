#ifndef ITESTCOMM_H
#define ITESTCOMM_H

/*! \file
 *  \n
 *  \brief  ITestComm.h 
 *  \details  
 *  \n &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/comm/common/src/itest_comm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
========================================================================== */

/*! \file 
 *  \n
 *  \brief  ITestComm.h ---- 
 *  \details This header file contains class declarations of
 *    Version 1.0 of the ITestComm HW communications interface
 *  \n
 *  \n &copy; 2009 Qualcomm Technologies Incorporated, All Rights Reserved
 */

#include "icomm.h"

// Function Pointers for Virtual Methods
typedef pm_err_flag_type (*pmiC_func_ITestComm_ReadByte)(void *me, pm_register_address_type addr, pm_register_data_type* data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_ITestComm_WriteByte)(void *me, pm_register_address_type addr, pm_register_data_type data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_ITestComm_WriteByteMask)(void *me, pm_register_address_type addr, pm_register_mask_type mask, pm_register_data_type data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_ITestComm_BankedReadModifyWrite)(void *me, uint8 bank_number, pm_register_address_type address,
                                                                      pm_register_mask_type mask, pm_register_data_type data);
typedef pm_err_flag_type (*pmiC_func_ITestComm_ReadAdc)(void *me, unsigned  short externalResourceIndex, float*  data);

typedef struct pmiC_ITestComm_STRUCT
{
    pmiC_IComm super; // Inheritance from ICommstructure. This must be the first item in structure
    // Function Pointers for Virtual Methods
    pmiC_func_ITestComm_ReadByte ReadByte;
    pmiC_func_ITestComm_WriteByte WriteByte;
    pmiC_func_ITestComm_WriteByteMask WriteByteMask;
    pmiC_func_ITestComm_BankedReadModifyWrite BankedReadModifyWrite;
    pmiC_func_ITestComm_ReadAdc ReadAdc;
   
}pmiC_ITestComm;
/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_ITestComm_Init(pmiC_ITestComm *me, pmiC_IComm  **innerComm, CommType  type);


#endif // ITESTCOMM_H
