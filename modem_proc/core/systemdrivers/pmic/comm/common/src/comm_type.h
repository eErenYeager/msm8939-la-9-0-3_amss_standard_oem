#ifndef COMMTYPE_H
#define COMMTYPE_H

/*! \file
*  \n
*  \brief  CommType.h 
*  \details  
*  \n &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/comm/common/src/comm_type.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/7/10    wra     Added Feature definition to optimize RPM memory
========================================================================== */

enum CommMode
{
    OFF_TARGET,
    ON_TARGET,
    DEBUG_IT,
    INVALID_COMM_MODE 
} ;

typedef enum CommTypeEnum
{
    FIRST_INVALID_COMM_INTERFACE = -1,
    SPMI_1_0_TYPE,
    TRANSCOMM_TYPE,
    SSBI_2_0_TYPE_LITE = 0 ,
    SSBI_2_0_TYPE,
#if defined(PMIC_OFFTARGET_TESTING)
    F3_1_0_TYPE,
    SHADOW_REGISTER_ONLY_TYPE,
    BAT_BOARD_BASE_TYPE,
#endif
    INVALID_COMM_INTERFACE
}CommType;

typedef enum BusIndex
{
    BusIndex0,
    BusIndex1,
    BusIndex2, // external charger, don't use
    BusIndex3, // external charger, don't use
    BusIndex4,
    BusIndex5,
    InvalidSsbiIndex
}BusIndex;

#endif // COMMTYPE_H
