#ifndef SPMI_1_0_H
#define SPMI_1_0_H

/*! \file
 *  \n
 *  \brief  SPMI_1_0.h 
 *  \details  
 *  \n &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/comm/spmi/src/spmi_1_0.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
========================================================================== */

/*! \file 
*  \n
*  \brief  SPMI_1_0.h ---- 
*  \details This header file contains class declarations of
*    Version 2.0 of the SPMI 2.0 HW communictions interface
*  \n
*  \n &copy; 2009 Qualcomm Technologies Incorporated, All Rights Reserved
*/

#include "icomm.h"

typedef struct pmiC_SPMI_1_0_STRUCT
{
    pmiC_IComm super; // Inheritance from ICommstructure. This must be the first item in structure

   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   uint32 mPeripheralSlaveID;
}pmiC_SPMI_1_0;

/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_SPMI_1_0_Init(pmiC_SPMI_1_0 *me, pmiC_IComm  **pInnerComm, uint32  uSlaveId);

#endif // SPMI_1_0_H


