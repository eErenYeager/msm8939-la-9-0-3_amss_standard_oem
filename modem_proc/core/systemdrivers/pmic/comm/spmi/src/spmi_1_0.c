/*! \file 
*  \n
*  \brief  SPMI_1_0.cpp ---- SPMI Version 2.0 RELATED DEFINITION
*  \n
*  \n This header file contains class implementation of the SPMI Version 
*  \n 2.0 messaging interface
*  \n
*  \n &copy; Copyright 2009-2013 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/systemdrivers/pmic/comm/spmi/src/spmi_1_0.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/9/2009  wra     File created

========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "spmi_1_0.h"  
#include "DDISpmi.h"

static pm_err_flag_type pmiC_SPMI_1_0_ReadByte(void *me, 
                                                 pm_register_address_type  addr, 
                                                 pm_register_data_type*  data, 
                                                 uint8  priority);
static pm_err_flag_type pmiC_SPMI_1_0_WriteByte(void *me, 
                                                  pm_register_address_type  addr, 
                                                  pm_register_data_type  data, 
                                                  uint8  priority);
static pm_err_flag_type pmiC_SPMI_1_0_WriteByteMask(void *me, 
                                                      pm_register_address_type  addr, 
                                                      pm_register_mask_type  mask, 
                                                      pm_register_data_type  data, 
                                                      uint8  priority);
static pm_err_flag_type pmiC_SPMI_1_0_BankedReadModifyWrite(void *me, 
                                                              uint8  bank_number, 
                                                              pm_register_address_type  address, 
                                                              pm_register_mask_type  mask, 
                                                              pm_register_data_type  data);
static pm_err_flag_type pmiC_SPMI_1_0_WriteByteArray(void *me, 
                                                       pm_register_address_type  address, 
                                                       unsigned  int numBytes, 
                                                       pm_register_data_type*  data,
                                                       uint8  priority);
static pm_err_flag_type pmiC_SPMI_1_0_ReadByteArray(void *me, 
                                                      pm_register_address_type  address, 
                                                      unsigned  int numBytes, 
                                                      pm_register_data_type*  data, 
                                                      uint8  priority);
static pm_err_flag_type pmiC_SPMI_1_0_ReadBytesAtomic(void *me, 
                                                        pm_register_address_type*  addressBuf, 
                                                        unsigned  int numBytes, 
                                                        pm_register_data_type*  dataBuf, 
                                                        uint8  priority);
/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_SPMI_1_0_Init(pmiC_SPMI_1_0 *me, pmiC_IComm  **pInnerComm, uint32  uSlaveId)
{
    pmiC_IComm_Init((pmiC_IComm*)me,  pInnerComm, SPMI_1_0_TYPE);

    ((pmiC_IComm *)me)->ReadByte = pmiC_SPMI_1_0_ReadByte;
    ((pmiC_IComm *)me)->WriteByte = pmiC_SPMI_1_0_WriteByte;
    ((pmiC_IComm *)me)->WriteByteMask = pmiC_SPMI_1_0_WriteByteMask;
    ((pmiC_IComm *)me)->BankedReadModifyWrite = pmiC_SPMI_1_0_BankedReadModifyWrite;
    ((pmiC_IComm *)me)->WriteByteArray = pmiC_SPMI_1_0_WriteByteArray;
    ((pmiC_IComm *)me)->ReadByteArray = pmiC_SPMI_1_0_ReadByteArray;
    ((pmiC_IComm *)me)->ReadBytesAtomic = pmiC_SPMI_1_0_ReadBytesAtomic;

    me->mPeripheralSlaveID = uSlaveId;
   
}

/*************************************
NOTE: VIRTUAL METHOD IMPLEMENTATION
**************************************/
pm_err_flag_type pmiC_SPMI_1_0_ReadByte(void *me, 
                                          pm_register_address_type  addr, 
                                          pm_register_data_type*  data, 
                                          uint8  priority)
{
    pm_err_flag_type   err = PM_ERR_FLAG__SUCCESS ;
    DALResult          spmi_result = DAL_SUCCESS; 
    /* Read the data from the SPMI */
    uint32             dataReadLength = 0;

    /* check for out-of-bounds index */
    if ( addr > PM_MAX_REGS)
    { 
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
        spmi_result = DalSpmi_ReadLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                        ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                        (SpmiBus_AccessPriorityType)priority, addr, data, 1, &dataReadLength);
 
        /* TODO should we update the shadow register? */
        //pm_image[addr] = *data ;

        if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
      }
    return err ;
}

pm_err_flag_type pmiC_SPMI_1_0_WriteByte(void *me, 
                                           pm_register_address_type  addr, 
                                           pm_register_data_type  data, 
                                           uint8  priority)
{
    pm_err_flag_type    err = PM_ERR_FLAG__SUCCESS ;
    DALResult           spmi_result = DAL_SUCCESS;

    /* check for out-of-bounds index */
    if ( addr > PM_MAX_REGS)
    { 
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
        spmi_result =  DalSpmi_WriteLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                          ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                          (SpmiBus_AccessPriorityType)priority, addr, &data, 1);

        if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
    }

    return err ; 
}

pm_err_flag_type pmiC_SPMI_1_0_WriteByteMask(void *me, 
                                               pm_register_address_type  addr, 
                                               pm_register_mask_type  mask, 
                                               pm_register_data_type  value, 
                                               uint8  priority)
{
    DALResult             spmi_result = DAL_SUCCESS ;
    pm_err_flag_type      err = PM_ERR_FLAG__SUCCESS ;
    uint32                dataReadLength = 1;
    pm_register_data_type data = 0 ;


    /* check for out-of-bounds index */
    if ( addr > PM_MAX_REGS )
    {
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
         /* Read the data from the SPMI and leave all the bits other than the mask to the previous state */
        spmi_result = DalSpmi_ReadLong(((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                       ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                       (SpmiBus_AccessPriorityType)priority, addr, &data, 1, &dataReadLength);
        
        if ( spmi_result == DAL_SUCCESS )
        {
            data &= (~mask) ; 
            data |= (value & mask) ;
            spmi_result = DalSpmi_WriteLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                             ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                             (SpmiBus_AccessPriorityType)priority, addr, &data, dataReadLength);

            if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
        }
    }
    return err ;
}

pm_err_flag_type pmiC_SPMI_1_0_BankedReadModifyWrite(void *me, 
                                                       uint8  bank_number, 
                                                       pm_register_address_type  address, 
                                                       pm_register_mask_type  mask, 
                                                       pm_register_data_type  data)
{
    (void) me;
    (void)bank_number;
    (void)address;
    (void)mask;
    (void)data;
   
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED ;
}

pm_err_flag_type pmiC_SPMI_1_0_WriteByteArray(void *me, 
                                                pm_register_address_type  address, 
                                                unsigned  int numBytes, 
                                                pm_register_data_type*  data, 
                                                uint8  priority)
{
    pm_err_flag_type    err = PM_ERR_FLAG__SUCCESS ; 
    DALResult           spmi_result = DAL_SUCCESS;

    /* check for out-of-bounds index */
    if ( address > PM_MAX_REGS)
    { 
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
        spmi_result =  DalSpmi_WriteLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                          ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                          (SpmiBus_AccessPriorityType)priority, address, data, numBytes);

        if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
    }

    return err;
}

pm_err_flag_type pmiC_SPMI_1_0_ReadByteArray(void *me, 
                                               pm_register_address_type  address,
                                               unsigned  int numBytes, 
                                               pm_register_data_type*  data, 
                                               uint8  priority)
{
    pm_err_flag_type    err = PM_ERR_FLAG__SUCCESS ; 
    DALResult           spmi_result = DAL_SUCCESS;
    /* Read the data from the SPMI */
    uint32              dataReadLength = 0;

    /* check for out-of-bounds index */
    if ( address > PM_MAX_REGS)
    { 
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
        spmi_result =  DalSpmi_ReadLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                         ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                         (SpmiBus_AccessPriorityType)priority, 
                                         address, 
                                         data, 
                                         numBytes,
                                         &dataReadLength);

        if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
    }

    return err;
}

pm_err_flag_type pmiC_SPMI_1_0_ReadBytesAtomic(void *me, 
                                                 pm_register_address_type*  addressBuf, 
                                                 unsigned  int numBytes, 
                                                 pm_register_data_type*  dataBuf, 
                                                 uint8  priority)
{
    pm_err_flag_type    err = PM_ERR_FLAG__SUCCESS ; 
    DALResult           spmi_result = DAL_SUCCESS;
    /* Read the data from the SPMI */
    uint32              dataReadLength = 0;

    /* check for out-of-bounds index */
    if ( addressBuf[0] > PM_MAX_REGS)
    { 
        err = PM_ERR_FLAG__SPMI_OPT_ERR ;
    }
    else
    {
        spmi_result =  DalSpmi_ReadLong( ((pmiC_IComm *)me)->mpCommInfo->mDalHandle, 
                                         ((pmiC_IComm *)me)->mpCommInfo->mBusIndex, 
                                         (SpmiBus_AccessPriorityType)priority, 
                                         addressBuf[0], 
                                         dataBuf, 
                                         numBytes,
                                         &dataReadLength);

        if ( spmi_result != DAL_SUCCESS ) { err = PM_ERR_FLAG__SPMI_OPT_ERR ; }
    }

    return err;
}






/*===========================================================================

VARIABLE DEFINITIONS

===========================================================================*/
/* Array used by software to buffer the PMIC registers  */
//pm_register_data_type pm_image[ PM_MAX_REGS ];


