#===============================================================================
#
# QFPROM Driver , Diag Libs
#
# GENERAL DESCRIPTION
#    QFPROM HAL build script
#
# Copyright 2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.7.24/boot/qfprom/build/core_2_0.sconscript#1 $
#  $DateTime: 2015/01/27 06:04:57 $
#  $Author: mplp4svc $
#  $Change: 7351156 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 12/17/12   kedara  Added src's to support qfprom_drivers in mpss.
# 07/10/12   dh      Add qfprom_fuse_region.c
# 11/08/11   plc     Initial Revision.
#===============================================================================
Import('env')
env = env.Clone()
env.LoadSoftwareUnits(level=1)

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/boot/qfprom/"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends outside CoreBSP
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'BOOT',
   'HAL',
   'DAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'MPROC',
   'POWER',
   'DEBUGTOOLS',
   'STORAGE',
   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)
env.RequirePublicApi(['RFA'],area='modem')

#-------------------------------------------------------------------------------
# Source Code and LIBs
#-------------------------------------------------------------------------------

# Note: qfprom_fuse_region.c is image/target specific. On porting driver to
# different images/targets, this src may need to be updated.

QFPROM_SOURCES = [
   '${BUILDPATH}/hw/core_2_0/qfprom_target.c',
   '${BUILDPATH}/hw/core_2_0/qfprom_fuse_region.c',
   '${BUILDPATH}/src/qfprom.c',
 ]

QFPROM_MPSS_SOURCES = [
   '${BUILDPATH}/hw/core_2_0/qfprom_mpss.c',
 ]

QFPROM_TZ_SOURCES = [
   '${BUILDPATH}/hw/core_2_0/qfprom_tz.c',
]

QFPROM_HAL_SOURCES = [
   '${BUILDPATH}/hw/core_2_0/HALqfprom.c'
]

QFPROM_TEST_SOURCES = [
   '${BUILDPATH}/src/qfprom_test.c',
 ]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddLibrary(['CORE_QDSP6_SW'], '${BUILDPATH}/qfprom', QFPROM_SOURCES + QFPROM_HAL_SOURCES + QFPROM_TEST_SOURCES + QFPROM_MPSS_SOURCES)

env.AddLibrary(['TZOS_IMAGE'], '${BUILDPATH}/qfprom_tz', QFPROM_SOURCES + QFPROM_HAL_SOURCES + QFPROM_TZ_SOURCES)
