#ifndef __PRNG_CLKHWIOREG_H__
#define __PRNG_CLKHWIOREG_H__
/*
===========================================================================
*/
/**
  @file prng_clkhwioreg.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    GCC_CLK_CTL_REG
    MSS_PERPH
    MSS_QDSP6SS_PUB

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.7.24/securemsm/cryptodrivers/prng/chipset/msm8x26/inc/prng_clkhwioreg.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: GCC_CLK_CTL_REG
 *--------------------------------------------------------------------------*/

#define GCC_CLK_CTL_REG_REG_BASE                                                                      (CLK_CTL_BASE      + 0x00000000)

#define HWIO_GCC_GPLL0_MODE_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00000000)
#define HWIO_GCC_GPLL0_MODE_RMSK                                                                        0x3fff0f
#define HWIO_GCC_GPLL0_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_MODE_ADDR, HWIO_GCC_GPLL0_MODE_RMSK)
#define HWIO_GCC_GPLL0_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_MODE_ADDR, m)
#define HWIO_GCC_GPLL0_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_MODE_ADDR,v)
#define HWIO_GCC_GPLL0_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_MODE_ADDR,m,v,HWIO_GCC_GPLL0_MODE_IN)
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_RESET_BMSK                                                     0x200000
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_RESET_SHFT                                                         0x15
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_ENA_BMSK                                                       0x100000
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_ENA_SHFT                                                           0x14
#define HWIO_GCC_GPLL0_MODE_PLL_BIAS_COUNT_BMSK                                                          0xfc000
#define HWIO_GCC_GPLL0_MODE_PLL_BIAS_COUNT_SHFT                                                              0xe
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_COUNT_BMSK                                                           0x3f00
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_COUNT_SHFT                                                              0x8
#define HWIO_GCC_GPLL0_MODE_PLL_PLLTEST_BMSK                                                                 0x8
#define HWIO_GCC_GPLL0_MODE_PLL_PLLTEST_SHFT                                                                 0x3
#define HWIO_GCC_GPLL0_MODE_PLL_RESET_N_BMSK                                                                 0x4
#define HWIO_GCC_GPLL0_MODE_PLL_RESET_N_SHFT                                                                 0x2
#define HWIO_GCC_GPLL0_MODE_PLL_BYPASSNL_BMSK                                                                0x2
#define HWIO_GCC_GPLL0_MODE_PLL_BYPASSNL_SHFT                                                                0x1
#define HWIO_GCC_GPLL0_MODE_PLL_OUTCTRL_BMSK                                                                 0x1
#define HWIO_GCC_GPLL0_MODE_PLL_OUTCTRL_SHFT                                                                 0x0

#define HWIO_GCC_GPLL0_L_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000004)
#define HWIO_GCC_GPLL0_L_VAL_RMSK                                                                           0xff
#define HWIO_GCC_GPLL0_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_L_VAL_ADDR, HWIO_GCC_GPLL0_L_VAL_RMSK)
#define HWIO_GCC_GPLL0_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL0_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL0_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_L_VAL_ADDR,m,v,HWIO_GCC_GPLL0_L_VAL_IN)
#define HWIO_GCC_GPLL0_L_VAL_PLL_L_BMSK                                                                     0xff
#define HWIO_GCC_GPLL0_L_VAL_PLL_L_SHFT                                                                      0x0

#define HWIO_GCC_GPLL0_M_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000008)
#define HWIO_GCC_GPLL0_M_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL0_M_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_M_VAL_ADDR, HWIO_GCC_GPLL0_M_VAL_RMSK)
#define HWIO_GCC_GPLL0_M_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_M_VAL_ADDR, m)
#define HWIO_GCC_GPLL0_M_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_M_VAL_ADDR,v)
#define HWIO_GCC_GPLL0_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_M_VAL_ADDR,m,v,HWIO_GCC_GPLL0_M_VAL_IN)
#define HWIO_GCC_GPLL0_M_VAL_PLL_M_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL0_M_VAL_PLL_M_SHFT                                                                      0x0

#define HWIO_GCC_GPLL0_N_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0000000c)
#define HWIO_GCC_GPLL0_N_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL0_N_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_N_VAL_ADDR, HWIO_GCC_GPLL0_N_VAL_RMSK)
#define HWIO_GCC_GPLL0_N_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_N_VAL_ADDR, m)
#define HWIO_GCC_GPLL0_N_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_N_VAL_ADDR,v)
#define HWIO_GCC_GPLL0_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_N_VAL_ADDR,m,v,HWIO_GCC_GPLL0_N_VAL_IN)
#define HWIO_GCC_GPLL0_N_VAL_PLL_N_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL0_N_VAL_PLL_N_SHFT                                                                      0x0

#define HWIO_GCC_GPLL0_USER_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000010)
#define HWIO_GCC_GPLL0_USER_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL0_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_ADDR, HWIO_GCC_GPLL0_USER_CTL_RMSK)
#define HWIO_GCC_GPLL0_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL0_USER_CTL_IN)
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS31_25_BMSK                                                0xfe000000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS31_25_SHFT                                                      0x19
#define HWIO_GCC_GPLL0_USER_CTL_MN_EN_BMSK                                                             0x1000000
#define HWIO_GCC_GPLL0_USER_CTL_MN_EN_SHFT                                                                  0x18
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS23_21_BMSK                                                  0xe00000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS23_21_SHFT                                                      0x15
#define HWIO_GCC_GPLL0_USER_CTL_VCO_SEL_BMSK                                                            0x100000
#define HWIO_GCC_GPLL0_USER_CTL_VCO_SEL_SHFT                                                                0x14
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS19_13_BMSK                                                   0xfe000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS19_13_SHFT                                                       0xd
#define HWIO_GCC_GPLL0_USER_CTL_PRE_DIV_RATIO_BMSK                                                        0x1000
#define HWIO_GCC_GPLL0_USER_CTL_PRE_DIV_RATIO_SHFT                                                           0xc
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS11_10_BMSK                                                     0xc00
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS11_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL0_USER_CTL_POST_DIV_RATIO_BMSK                                                        0x300
#define HWIO_GCC_GPLL0_USER_CTL_POST_DIV_RATIO_SHFT                                                          0x8
#define HWIO_GCC_GPLL0_USER_CTL_OUTPUT_INV_BMSK                                                             0x80
#define HWIO_GCC_GPLL0_USER_CTL_OUTPUT_INV_SHFT                                                              0x7
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS6_5_BMSK                                                        0x60
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS6_5_SHFT                                                         0x5
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_TEST_BMSK                                                         0x10
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_TEST_SHFT                                                          0x4
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                         0x8
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                         0x3
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_BIST_BMSK                                                          0x4
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_BIST_SHFT                                                          0x2
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX_BMSK                                                           0x2
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX_SHFT                                                           0x1
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                          0x1
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                          0x0

#define HWIO_GCC_GPLL0_CONFIG_CTL_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000014)
#define HWIO_GCC_GPLL0_CONFIG_CTL_RMSK                                                                0xffffffff
#define HWIO_GCC_GPLL0_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR, HWIO_GCC_GPLL0_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL0_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL0_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BITS31_30_BMSK                                              0xc0000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BITS31_30_SHFT                                                    0x1e
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_PFD_UP_BMSK                                                   0x20000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_PFD_UP_SHFT                                                         0x1d
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_PFD_DOWN_BMSK                                                 0x10000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_PFD_DOWN_SHFT                                                       0x1c
#define HWIO_GCC_GPLL0_CONFIG_CTL_NMOSC_FREQ_CTRL_BMSK                                                 0xc000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_NMOSC_FREQ_CTRL_SHFT                                                      0x1a
#define HWIO_GCC_GPLL0_CONFIG_CTL_PFD_DZSEL_BMSK                                                       0x3000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_PFD_DZSEL_SHFT                                                            0x18
#define HWIO_GCC_GPLL0_CONFIG_CTL_NMOSC_EN_BMSK                                                         0x800000
#define HWIO_GCC_GPLL0_CONFIG_CTL_NMOSC_EN_SHFT                                                             0x17
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BIT22_BMSK                                                    0x400000
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BIT22_SHFT                                                        0x16
#define HWIO_GCC_GPLL0_CONFIG_CTL_ICP_DIV_BMSK                                                          0x300000
#define HWIO_GCC_GPLL0_CONFIG_CTL_ICP_DIV_SHFT                                                              0x14
#define HWIO_GCC_GPLL0_CONFIG_CTL_IREG_DIV_BMSK                                                          0xc0000
#define HWIO_GCC_GPLL0_CONFIG_CTL_IREG_DIV_SHFT                                                             0x12
#define HWIO_GCC_GPLL0_CONFIG_CTL_CUSEL_BMSK                                                             0x30000
#define HWIO_GCC_GPLL0_CONFIG_CTL_CUSEL_SHFT                                                                0x10
#define HWIO_GCC_GPLL0_CONFIG_CTL_REF_MODE_BMSK                                                           0x8000
#define HWIO_GCC_GPLL0_CONFIG_CTL_REF_MODE_SHFT                                                              0xf
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BIT14_BMSK                                                      0x4000
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BIT14_SHFT                                                         0xe
#define HWIO_GCC_GPLL0_CONFIG_CTL_CFG_LOCKDET_BMSK                                                        0x3000
#define HWIO_GCC_GPLL0_CONFIG_CTL_CFG_LOCKDET_SHFT                                                           0xc
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_ISEED_BMSK                                                         0x800
#define HWIO_GCC_GPLL0_CONFIG_CTL_FORCE_ISEED_SHFT                                                           0xb
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BITS10_0_BMSK                                                    0x7ff
#define HWIO_GCC_GPLL0_CONFIG_CTL_RESERVE_BITS10_0_SHFT                                                      0x0

#define HWIO_GCC_GPLL0_TEST_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000018)
#define HWIO_GCC_GPLL0_TEST_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL0_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_ADDR, HWIO_GCC_GPLL0_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL0_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL0_TEST_CTL_IN)
#define HWIO_GCC_GPLL0_TEST_CTL_RESERVE_BITS31_15_BMSK                                                0xffff8000
#define HWIO_GCC_GPLL0_TEST_CTL_RESERVE_BITS31_15_SHFT                                                       0xf
#define HWIO_GCC_GPLL0_TEST_CTL_PLLOUT_LV_TEST_SEL_BMSK                                                   0x4000
#define HWIO_GCC_GPLL0_TEST_CTL_PLLOUT_LV_TEST_SEL_SHFT                                                      0xe
#define HWIO_GCC_GPLL0_TEST_CTL_RESERVE_BITS13_10_BMSK                                                    0x3c00
#define HWIO_GCC_GPLL0_TEST_CTL_RESERVE_BITS13_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL0_TEST_CTL_ICP_TST_EN_BMSK                                                            0x200
#define HWIO_GCC_GPLL0_TEST_CTL_ICP_TST_EN_SHFT                                                              0x9
#define HWIO_GCC_GPLL0_TEST_CTL_ICP_EXT_SEL_BMSK                                                           0x100
#define HWIO_GCC_GPLL0_TEST_CTL_ICP_EXT_SEL_SHFT                                                             0x8
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_SEL_BMSK                                                              0x80
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_SEL_SHFT                                                               0x7
#define HWIO_GCC_GPLL0_TEST_CTL_BYP_TESTAMP_BMSK                                                            0x40
#define HWIO_GCC_GPLL0_TEST_CTL_BYP_TESTAMP_SHFT                                                             0x6
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_SEL_BMSK                                                             0x30
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_SEL_SHFT                                                              0x4
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_SEL_BMSK                                                              0xc
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_SEL_SHFT                                                              0x2
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_EN_BMSK                                                               0x2
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_EN_SHFT                                                               0x1
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_EN_BMSK                                                               0x1
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_EN_SHFT                                                               0x0

#define HWIO_GCC_GPLL0_STATUS_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000001c)
#define HWIO_GCC_GPLL0_STATUS_RMSK                                                                       0x3ffff
#define HWIO_GCC_GPLL0_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_STATUS_ADDR, HWIO_GCC_GPLL0_STATUS_RMSK)
#define HWIO_GCC_GPLL0_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_STATUS_ADDR, m)
#define HWIO_GCC_GPLL0_STATUS_PLL_ACTIVE_FLAG_BMSK                                                       0x20000
#define HWIO_GCC_GPLL0_STATUS_PLL_ACTIVE_FLAG_SHFT                                                          0x11
#define HWIO_GCC_GPLL0_STATUS_PLL_LOCK_DET_BMSK                                                          0x10000
#define HWIO_GCC_GPLL0_STATUS_PLL_LOCK_DET_SHFT                                                             0x10
#define HWIO_GCC_GPLL0_STATUS_PLL_D_BMSK                                                                  0xffff
#define HWIO_GCC_GPLL0_STATUS_PLL_D_SHFT                                                                     0x0

#define HWIO_GCC_GPLL1_MODE_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00000040)
#define HWIO_GCC_GPLL1_MODE_RMSK                                                                        0x3fff0f
#define HWIO_GCC_GPLL1_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_MODE_ADDR, HWIO_GCC_GPLL1_MODE_RMSK)
#define HWIO_GCC_GPLL1_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_MODE_ADDR, m)
#define HWIO_GCC_GPLL1_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_MODE_ADDR,v)
#define HWIO_GCC_GPLL1_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_MODE_ADDR,m,v,HWIO_GCC_GPLL1_MODE_IN)
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_RESET_BMSK                                                     0x200000
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_RESET_SHFT                                                         0x15
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_ENA_BMSK                                                       0x100000
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_ENA_SHFT                                                           0x14
#define HWIO_GCC_GPLL1_MODE_PLL_BIAS_COUNT_BMSK                                                          0xfc000
#define HWIO_GCC_GPLL1_MODE_PLL_BIAS_COUNT_SHFT                                                              0xe
#define HWIO_GCC_GPLL1_MODE_PLL_LOCK_COUNT_BMSK                                                           0x3f00
#define HWIO_GCC_GPLL1_MODE_PLL_LOCK_COUNT_SHFT                                                              0x8
#define HWIO_GCC_GPLL1_MODE_PLL_PLLTEST_BMSK                                                                 0x8
#define HWIO_GCC_GPLL1_MODE_PLL_PLLTEST_SHFT                                                                 0x3
#define HWIO_GCC_GPLL1_MODE_PLL_RESET_N_BMSK                                                                 0x4
#define HWIO_GCC_GPLL1_MODE_PLL_RESET_N_SHFT                                                                 0x2
#define HWIO_GCC_GPLL1_MODE_PLL_BYPASSNL_BMSK                                                                0x2
#define HWIO_GCC_GPLL1_MODE_PLL_BYPASSNL_SHFT                                                                0x1
#define HWIO_GCC_GPLL1_MODE_PLL_OUTCTRL_BMSK                                                                 0x1
#define HWIO_GCC_GPLL1_MODE_PLL_OUTCTRL_SHFT                                                                 0x0

#define HWIO_GCC_GPLL1_L_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000044)
#define HWIO_GCC_GPLL1_L_VAL_RMSK                                                                           0xff
#define HWIO_GCC_GPLL1_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_L_VAL_ADDR, HWIO_GCC_GPLL1_L_VAL_RMSK)
#define HWIO_GCC_GPLL1_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL1_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL1_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_L_VAL_ADDR,m,v,HWIO_GCC_GPLL1_L_VAL_IN)
#define HWIO_GCC_GPLL1_L_VAL_PLL_L_BMSK                                                                     0xff
#define HWIO_GCC_GPLL1_L_VAL_PLL_L_SHFT                                                                      0x0

#define HWIO_GCC_GPLL1_M_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000048)
#define HWIO_GCC_GPLL1_M_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL1_M_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_M_VAL_ADDR, HWIO_GCC_GPLL1_M_VAL_RMSK)
#define HWIO_GCC_GPLL1_M_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_M_VAL_ADDR, m)
#define HWIO_GCC_GPLL1_M_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_M_VAL_ADDR,v)
#define HWIO_GCC_GPLL1_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_M_VAL_ADDR,m,v,HWIO_GCC_GPLL1_M_VAL_IN)
#define HWIO_GCC_GPLL1_M_VAL_PLL_M_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL1_M_VAL_PLL_M_SHFT                                                                      0x0

#define HWIO_GCC_GPLL1_N_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0000004c)
#define HWIO_GCC_GPLL1_N_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL1_N_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_N_VAL_ADDR, HWIO_GCC_GPLL1_N_VAL_RMSK)
#define HWIO_GCC_GPLL1_N_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_N_VAL_ADDR, m)
#define HWIO_GCC_GPLL1_N_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_N_VAL_ADDR,v)
#define HWIO_GCC_GPLL1_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_N_VAL_ADDR,m,v,HWIO_GCC_GPLL1_N_VAL_IN)
#define HWIO_GCC_GPLL1_N_VAL_PLL_N_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL1_N_VAL_PLL_N_SHFT                                                                      0x0

#define HWIO_GCC_GPLL1_USER_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000050)
#define HWIO_GCC_GPLL1_USER_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL1_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_USER_CTL_ADDR, HWIO_GCC_GPLL1_USER_CTL_RMSK)
#define HWIO_GCC_GPLL1_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL1_USER_CTL_IN)
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS31_25_BMSK                                                0xfe000000
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS31_25_SHFT                                                      0x19
#define HWIO_GCC_GPLL1_USER_CTL_MN_EN_BMSK                                                             0x1000000
#define HWIO_GCC_GPLL1_USER_CTL_MN_EN_SHFT                                                                  0x18
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS23_21_BMSK                                                  0xe00000
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS23_21_SHFT                                                      0x15
#define HWIO_GCC_GPLL1_USER_CTL_VCO_SEL_BMSK                                                            0x100000
#define HWIO_GCC_GPLL1_USER_CTL_VCO_SEL_SHFT                                                                0x14
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS19_13_BMSK                                                   0xfe000
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS19_13_SHFT                                                       0xd
#define HWIO_GCC_GPLL1_USER_CTL_PRE_DIV_RATIO_BMSK                                                        0x1000
#define HWIO_GCC_GPLL1_USER_CTL_PRE_DIV_RATIO_SHFT                                                           0xc
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS11_10_BMSK                                                     0xc00
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS11_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL1_USER_CTL_POST_DIV_RATIO_BMSK                                                        0x300
#define HWIO_GCC_GPLL1_USER_CTL_POST_DIV_RATIO_SHFT                                                          0x8
#define HWIO_GCC_GPLL1_USER_CTL_OUTPUT_INV_BMSK                                                             0x80
#define HWIO_GCC_GPLL1_USER_CTL_OUTPUT_INV_SHFT                                                              0x7
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS6_5_BMSK                                                        0x60
#define HWIO_GCC_GPLL1_USER_CTL_RESERVE_BITS6_5_SHFT                                                         0x5
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_TEST_BMSK                                                         0x10
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_TEST_SHFT                                                          0x4
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                         0x8
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                         0x3
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_BIST_BMSK                                                          0x4
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_BIST_SHFT                                                          0x2
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX_BMSK                                                           0x2
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX_SHFT                                                           0x1
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                          0x1
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                          0x0

#define HWIO_GCC_GPLL1_CONFIG_CTL_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000054)
#define HWIO_GCC_GPLL1_CONFIG_CTL_RMSK                                                                0xffffffff
#define HWIO_GCC_GPLL1_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR, HWIO_GCC_GPLL1_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL1_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL1_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BITS31_30_BMSK                                              0xc0000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BITS31_30_SHFT                                                    0x1e
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_PFD_UP_BMSK                                                   0x20000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_PFD_UP_SHFT                                                         0x1d
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_PFD_DOWN_BMSK                                                 0x10000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_PFD_DOWN_SHFT                                                       0x1c
#define HWIO_GCC_GPLL1_CONFIG_CTL_NMOSC_FREQ_CTRL_BMSK                                                 0xc000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_NMOSC_FREQ_CTRL_SHFT                                                      0x1a
#define HWIO_GCC_GPLL1_CONFIG_CTL_PFD_DZSEL_BMSK                                                       0x3000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_PFD_DZSEL_SHFT                                                            0x18
#define HWIO_GCC_GPLL1_CONFIG_CTL_NMOSC_EN_BMSK                                                         0x800000
#define HWIO_GCC_GPLL1_CONFIG_CTL_NMOSC_EN_SHFT                                                             0x17
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BIT22_BMSK                                                    0x400000
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BIT22_SHFT                                                        0x16
#define HWIO_GCC_GPLL1_CONFIG_CTL_ICP_DIV_BMSK                                                          0x300000
#define HWIO_GCC_GPLL1_CONFIG_CTL_ICP_DIV_SHFT                                                              0x14
#define HWIO_GCC_GPLL1_CONFIG_CTL_IREG_DIV_BMSK                                                          0xc0000
#define HWIO_GCC_GPLL1_CONFIG_CTL_IREG_DIV_SHFT                                                             0x12
#define HWIO_GCC_GPLL1_CONFIG_CTL_CUSEL_BMSK                                                             0x30000
#define HWIO_GCC_GPLL1_CONFIG_CTL_CUSEL_SHFT                                                                0x10
#define HWIO_GCC_GPLL1_CONFIG_CTL_REF_MODE_BMSK                                                           0x8000
#define HWIO_GCC_GPLL1_CONFIG_CTL_REF_MODE_SHFT                                                              0xf
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BIT14_BMSK                                                      0x4000
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BIT14_SHFT                                                         0xe
#define HWIO_GCC_GPLL1_CONFIG_CTL_CFG_LOCKDET_BMSK                                                        0x3000
#define HWIO_GCC_GPLL1_CONFIG_CTL_CFG_LOCKDET_SHFT                                                           0xc
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_ISEED_BMSK                                                         0x800
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_ISEED_SHFT                                                           0xb
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BITS10_0_BMSK                                                    0x7ff
#define HWIO_GCC_GPLL1_CONFIG_CTL_RESERVE_BITS10_0_SHFT                                                      0x0

#define HWIO_GCC_GPLL1_TEST_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000058)
#define HWIO_GCC_GPLL1_TEST_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL1_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_TEST_CTL_ADDR, HWIO_GCC_GPLL1_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL1_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL1_TEST_CTL_IN)
#define HWIO_GCC_GPLL1_TEST_CTL_RESERVE_BITS31_15_BMSK                                                0xffff8000
#define HWIO_GCC_GPLL1_TEST_CTL_RESERVE_BITS31_15_SHFT                                                       0xf
#define HWIO_GCC_GPLL1_TEST_CTL_PLLOUT_LV_TEST_SEL_BMSK                                                   0x4000
#define HWIO_GCC_GPLL1_TEST_CTL_PLLOUT_LV_TEST_SEL_SHFT                                                      0xe
#define HWIO_GCC_GPLL1_TEST_CTL_RESERVE_BITS13_10_BMSK                                                    0x3c00
#define HWIO_GCC_GPLL1_TEST_CTL_RESERVE_BITS13_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL1_TEST_CTL_ICP_TST_EN_BMSK                                                            0x200
#define HWIO_GCC_GPLL1_TEST_CTL_ICP_TST_EN_SHFT                                                              0x9
#define HWIO_GCC_GPLL1_TEST_CTL_ICP_EXT_SEL_BMSK                                                           0x100
#define HWIO_GCC_GPLL1_TEST_CTL_ICP_EXT_SEL_SHFT                                                             0x8
#define HWIO_GCC_GPLL1_TEST_CTL_DTEST_SEL_BMSK                                                              0x80
#define HWIO_GCC_GPLL1_TEST_CTL_DTEST_SEL_SHFT                                                               0x7
#define HWIO_GCC_GPLL1_TEST_CTL_BYP_TESTAMP_BMSK                                                            0x40
#define HWIO_GCC_GPLL1_TEST_CTL_BYP_TESTAMP_SHFT                                                             0x6
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_SEL_BMSK                                                             0x30
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_SEL_SHFT                                                              0x4
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_SEL_BMSK                                                              0xc
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_SEL_SHFT                                                              0x2
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_EN_BMSK                                                               0x2
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_EN_SHFT                                                               0x1
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_EN_BMSK                                                               0x1
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_EN_SHFT                                                               0x0

#define HWIO_GCC_GPLL1_STATUS_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000005c)
#define HWIO_GCC_GPLL1_STATUS_RMSK                                                                       0x3ffff
#define HWIO_GCC_GPLL1_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_STATUS_ADDR, HWIO_GCC_GPLL1_STATUS_RMSK)
#define HWIO_GCC_GPLL1_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_STATUS_ADDR, m)
#define HWIO_GCC_GPLL1_STATUS_PLL_ACTIVE_FLAG_BMSK                                                       0x20000
#define HWIO_GCC_GPLL1_STATUS_PLL_ACTIVE_FLAG_SHFT                                                          0x11
#define HWIO_GCC_GPLL1_STATUS_PLL_LOCK_DET_BMSK                                                          0x10000
#define HWIO_GCC_GPLL1_STATUS_PLL_LOCK_DET_SHFT                                                             0x10
#define HWIO_GCC_GPLL1_STATUS_PLL_D_BMSK                                                                  0xffff
#define HWIO_GCC_GPLL1_STATUS_PLL_D_SHFT                                                                     0x0

#define HWIO_GCC_GPLL2_MODE_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00000080)
#define HWIO_GCC_GPLL2_MODE_RMSK                                                                        0x3fff0f
#define HWIO_GCC_GPLL2_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_MODE_ADDR, HWIO_GCC_GPLL2_MODE_RMSK)
#define HWIO_GCC_GPLL2_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_MODE_ADDR, m)
#define HWIO_GCC_GPLL2_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_MODE_ADDR,v)
#define HWIO_GCC_GPLL2_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_MODE_ADDR,m,v,HWIO_GCC_GPLL2_MODE_IN)
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_RESET_BMSK                                                     0x200000
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_RESET_SHFT                                                         0x15
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_ENA_BMSK                                                       0x100000
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_ENA_SHFT                                                           0x14
#define HWIO_GCC_GPLL2_MODE_PLL_BIAS_COUNT_BMSK                                                          0xfc000
#define HWIO_GCC_GPLL2_MODE_PLL_BIAS_COUNT_SHFT                                                              0xe
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_COUNT_BMSK                                                           0x3f00
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_COUNT_SHFT                                                              0x8
#define HWIO_GCC_GPLL2_MODE_PLL_PLLTEST_BMSK                                                                 0x8
#define HWIO_GCC_GPLL2_MODE_PLL_PLLTEST_SHFT                                                                 0x3
#define HWIO_GCC_GPLL2_MODE_PLL_RESET_N_BMSK                                                                 0x4
#define HWIO_GCC_GPLL2_MODE_PLL_RESET_N_SHFT                                                                 0x2
#define HWIO_GCC_GPLL2_MODE_PLL_BYPASSNL_BMSK                                                                0x2
#define HWIO_GCC_GPLL2_MODE_PLL_BYPASSNL_SHFT                                                                0x1
#define HWIO_GCC_GPLL2_MODE_PLL_OUTCTRL_BMSK                                                                 0x1
#define HWIO_GCC_GPLL2_MODE_PLL_OUTCTRL_SHFT                                                                 0x0

#define HWIO_GCC_GPLL2_L_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000084)
#define HWIO_GCC_GPLL2_L_VAL_RMSK                                                                           0xff
#define HWIO_GCC_GPLL2_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_L_VAL_ADDR, HWIO_GCC_GPLL2_L_VAL_RMSK)
#define HWIO_GCC_GPLL2_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL2_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL2_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_L_VAL_ADDR,m,v,HWIO_GCC_GPLL2_L_VAL_IN)
#define HWIO_GCC_GPLL2_L_VAL_PLL_L_BMSK                                                                     0xff
#define HWIO_GCC_GPLL2_L_VAL_PLL_L_SHFT                                                                      0x0

#define HWIO_GCC_GPLL2_M_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000088)
#define HWIO_GCC_GPLL2_M_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL2_M_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_M_VAL_ADDR, HWIO_GCC_GPLL2_M_VAL_RMSK)
#define HWIO_GCC_GPLL2_M_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_M_VAL_ADDR, m)
#define HWIO_GCC_GPLL2_M_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_M_VAL_ADDR,v)
#define HWIO_GCC_GPLL2_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_M_VAL_ADDR,m,v,HWIO_GCC_GPLL2_M_VAL_IN)
#define HWIO_GCC_GPLL2_M_VAL_PLL_M_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL2_M_VAL_PLL_M_SHFT                                                                      0x0

#define HWIO_GCC_GPLL2_N_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0000008c)
#define HWIO_GCC_GPLL2_N_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL2_N_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_N_VAL_ADDR, HWIO_GCC_GPLL2_N_VAL_RMSK)
#define HWIO_GCC_GPLL2_N_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_N_VAL_ADDR, m)
#define HWIO_GCC_GPLL2_N_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_N_VAL_ADDR,v)
#define HWIO_GCC_GPLL2_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_N_VAL_ADDR,m,v,HWIO_GCC_GPLL2_N_VAL_IN)
#define HWIO_GCC_GPLL2_N_VAL_PLL_N_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL2_N_VAL_PLL_N_SHFT                                                                      0x0

#define HWIO_GCC_GPLL2_USER_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000090)
#define HWIO_GCC_GPLL2_USER_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL2_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_ADDR, HWIO_GCC_GPLL2_USER_CTL_RMSK)
#define HWIO_GCC_GPLL2_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL2_USER_CTL_IN)
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS31_25_BMSK                                                0xfe000000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS31_25_SHFT                                                      0x19
#define HWIO_GCC_GPLL2_USER_CTL_MN_EN_BMSK                                                             0x1000000
#define HWIO_GCC_GPLL2_USER_CTL_MN_EN_SHFT                                                                  0x18
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS23_21_BMSK                                                  0xe00000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS23_21_SHFT                                                      0x15
#define HWIO_GCC_GPLL2_USER_CTL_VCO_SEL_BMSK                                                            0x100000
#define HWIO_GCC_GPLL2_USER_CTL_VCO_SEL_SHFT                                                                0x14
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS19_13_BMSK                                                   0xfe000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS19_13_SHFT                                                       0xd
#define HWIO_GCC_GPLL2_USER_CTL_PRE_DIV_RATIO_BMSK                                                        0x1000
#define HWIO_GCC_GPLL2_USER_CTL_PRE_DIV_RATIO_SHFT                                                           0xc
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS11_10_BMSK                                                     0xc00
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS11_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL2_USER_CTL_POST_DIV_RATIO_BMSK                                                        0x300
#define HWIO_GCC_GPLL2_USER_CTL_POST_DIV_RATIO_SHFT                                                          0x8
#define HWIO_GCC_GPLL2_USER_CTL_OUTPUT_INV_BMSK                                                             0x80
#define HWIO_GCC_GPLL2_USER_CTL_OUTPUT_INV_SHFT                                                              0x7
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS6_5_BMSK                                                        0x60
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS6_5_SHFT                                                         0x5
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_TEST_BMSK                                                         0x10
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_TEST_SHFT                                                          0x4
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                         0x8
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                         0x3
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_BIST_BMSK                                                          0x4
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_BIST_SHFT                                                          0x2
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX_BMSK                                                           0x2
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX_SHFT                                                           0x1
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                          0x1
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                          0x0

#define HWIO_GCC_GPLL2_CONFIG_CTL_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000094)
#define HWIO_GCC_GPLL2_CONFIG_CTL_RMSK                                                                0xffffffff
#define HWIO_GCC_GPLL2_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR, HWIO_GCC_GPLL2_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL2_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL2_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BITS31_30_BMSK                                              0xc0000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BITS31_30_SHFT                                                    0x1e
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_PFD_UP_BMSK                                                   0x20000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_PFD_UP_SHFT                                                         0x1d
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_PFD_DOWN_BMSK                                                 0x10000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_PFD_DOWN_SHFT                                                       0x1c
#define HWIO_GCC_GPLL2_CONFIG_CTL_NMOSC_FREQ_CTRL_BMSK                                                 0xc000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_NMOSC_FREQ_CTRL_SHFT                                                      0x1a
#define HWIO_GCC_GPLL2_CONFIG_CTL_PFD_DZSEL_BMSK                                                       0x3000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_PFD_DZSEL_SHFT                                                            0x18
#define HWIO_GCC_GPLL2_CONFIG_CTL_NMOSC_EN_BMSK                                                         0x800000
#define HWIO_GCC_GPLL2_CONFIG_CTL_NMOSC_EN_SHFT                                                             0x17
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BIT22_BMSK                                                    0x400000
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BIT22_SHFT                                                        0x16
#define HWIO_GCC_GPLL2_CONFIG_CTL_ICP_DIV_BMSK                                                          0x300000
#define HWIO_GCC_GPLL2_CONFIG_CTL_ICP_DIV_SHFT                                                              0x14
#define HWIO_GCC_GPLL2_CONFIG_CTL_IREG_DIV_BMSK                                                          0xc0000
#define HWIO_GCC_GPLL2_CONFIG_CTL_IREG_DIV_SHFT                                                             0x12
#define HWIO_GCC_GPLL2_CONFIG_CTL_CUSEL_BMSK                                                             0x30000
#define HWIO_GCC_GPLL2_CONFIG_CTL_CUSEL_SHFT                                                                0x10
#define HWIO_GCC_GPLL2_CONFIG_CTL_REF_MODE_BMSK                                                           0x8000
#define HWIO_GCC_GPLL2_CONFIG_CTL_REF_MODE_SHFT                                                              0xf
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BIT14_BMSK                                                      0x4000
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BIT14_SHFT                                                         0xe
#define HWIO_GCC_GPLL2_CONFIG_CTL_CFG_LOCKDET_BMSK                                                        0x3000
#define HWIO_GCC_GPLL2_CONFIG_CTL_CFG_LOCKDET_SHFT                                                           0xc
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_ISEED_BMSK                                                         0x800
#define HWIO_GCC_GPLL2_CONFIG_CTL_FORCE_ISEED_SHFT                                                           0xb
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BITS10_0_BMSK                                                    0x7ff
#define HWIO_GCC_GPLL2_CONFIG_CTL_RESERVE_BITS10_0_SHFT                                                      0x0

#define HWIO_GCC_GPLL2_TEST_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000098)
#define HWIO_GCC_GPLL2_TEST_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL2_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_ADDR, HWIO_GCC_GPLL2_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL2_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL2_TEST_CTL_IN)
#define HWIO_GCC_GPLL2_TEST_CTL_RESERVE_BITS31_15_BMSK                                                0xffff8000
#define HWIO_GCC_GPLL2_TEST_CTL_RESERVE_BITS31_15_SHFT                                                       0xf
#define HWIO_GCC_GPLL2_TEST_CTL_PLLOUT_LV_TEST_SEL_BMSK                                                   0x4000
#define HWIO_GCC_GPLL2_TEST_CTL_PLLOUT_LV_TEST_SEL_SHFT                                                      0xe
#define HWIO_GCC_GPLL2_TEST_CTL_RESERVE_BITS13_10_BMSK                                                    0x3c00
#define HWIO_GCC_GPLL2_TEST_CTL_RESERVE_BITS13_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL2_TEST_CTL_ICP_TST_EN_BMSK                                                            0x200
#define HWIO_GCC_GPLL2_TEST_CTL_ICP_TST_EN_SHFT                                                              0x9
#define HWIO_GCC_GPLL2_TEST_CTL_ICP_EXT_SEL_BMSK                                                           0x100
#define HWIO_GCC_GPLL2_TEST_CTL_ICP_EXT_SEL_SHFT                                                             0x8
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_SEL_BMSK                                                              0x80
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_SEL_SHFT                                                               0x7
#define HWIO_GCC_GPLL2_TEST_CTL_BYP_TESTAMP_BMSK                                                            0x40
#define HWIO_GCC_GPLL2_TEST_CTL_BYP_TESTAMP_SHFT                                                             0x6
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_SEL_BMSK                                                             0x30
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_SEL_SHFT                                                              0x4
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_SEL_BMSK                                                              0xc
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_SEL_SHFT                                                              0x2
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_EN_BMSK                                                               0x2
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_EN_SHFT                                                               0x1
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_EN_BMSK                                                               0x1
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_EN_SHFT                                                               0x0

#define HWIO_GCC_GPLL2_STATUS_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000009c)
#define HWIO_GCC_GPLL2_STATUS_RMSK                                                                       0x3ffff
#define HWIO_GCC_GPLL2_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_STATUS_ADDR, HWIO_GCC_GPLL2_STATUS_RMSK)
#define HWIO_GCC_GPLL2_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_STATUS_ADDR, m)
#define HWIO_GCC_GPLL2_STATUS_PLL_ACTIVE_FLAG_BMSK                                                       0x20000
#define HWIO_GCC_GPLL2_STATUS_PLL_ACTIVE_FLAG_SHFT                                                          0x11
#define HWIO_GCC_GPLL2_STATUS_PLL_LOCK_DET_BMSK                                                          0x10000
#define HWIO_GCC_GPLL2_STATUS_PLL_LOCK_DET_SHFT                                                             0x10
#define HWIO_GCC_GPLL2_STATUS_PLL_D_BMSK                                                                  0xffff
#define HWIO_GCC_GPLL2_STATUS_PLL_D_SHFT                                                                     0x0

#define HWIO_GCC_GPLL3_MODE_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000000c0)
#define HWIO_GCC_GPLL3_MODE_RMSK                                                                        0x3fff0f
#define HWIO_GCC_GPLL3_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_MODE_ADDR, HWIO_GCC_GPLL3_MODE_RMSK)
#define HWIO_GCC_GPLL3_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_MODE_ADDR, m)
#define HWIO_GCC_GPLL3_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_MODE_ADDR,v)
#define HWIO_GCC_GPLL3_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_MODE_ADDR,m,v,HWIO_GCC_GPLL3_MODE_IN)
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_RESET_BMSK                                                     0x200000
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_RESET_SHFT                                                         0x15
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_ENA_BMSK                                                       0x100000
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_ENA_SHFT                                                           0x14
#define HWIO_GCC_GPLL3_MODE_PLL_BIAS_COUNT_BMSK                                                          0xfc000
#define HWIO_GCC_GPLL3_MODE_PLL_BIAS_COUNT_SHFT                                                              0xe
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_COUNT_BMSK                                                           0x3f00
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_COUNT_SHFT                                                              0x8
#define HWIO_GCC_GPLL3_MODE_PLL_PLLTEST_BMSK                                                                 0x8
#define HWIO_GCC_GPLL3_MODE_PLL_PLLTEST_SHFT                                                                 0x3
#define HWIO_GCC_GPLL3_MODE_PLL_RESET_N_BMSK                                                                 0x4
#define HWIO_GCC_GPLL3_MODE_PLL_RESET_N_SHFT                                                                 0x2
#define HWIO_GCC_GPLL3_MODE_PLL_BYPASSNL_BMSK                                                                0x2
#define HWIO_GCC_GPLL3_MODE_PLL_BYPASSNL_SHFT                                                                0x1
#define HWIO_GCC_GPLL3_MODE_PLL_OUTCTRL_BMSK                                                                 0x1
#define HWIO_GCC_GPLL3_MODE_PLL_OUTCTRL_SHFT                                                                 0x0

#define HWIO_GCC_GPLL3_L_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x000000c4)
#define HWIO_GCC_GPLL3_L_VAL_RMSK                                                                           0xff
#define HWIO_GCC_GPLL3_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_L_VAL_ADDR, HWIO_GCC_GPLL3_L_VAL_RMSK)
#define HWIO_GCC_GPLL3_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL3_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL3_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_L_VAL_ADDR,m,v,HWIO_GCC_GPLL3_L_VAL_IN)
#define HWIO_GCC_GPLL3_L_VAL_PLL_L_BMSK                                                                     0xff
#define HWIO_GCC_GPLL3_L_VAL_PLL_L_SHFT                                                                      0x0

#define HWIO_GCC_GPLL3_M_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x000000c8)
#define HWIO_GCC_GPLL3_M_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL3_M_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_M_VAL_ADDR, HWIO_GCC_GPLL3_M_VAL_RMSK)
#define HWIO_GCC_GPLL3_M_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_M_VAL_ADDR, m)
#define HWIO_GCC_GPLL3_M_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_M_VAL_ADDR,v)
#define HWIO_GCC_GPLL3_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_M_VAL_ADDR,m,v,HWIO_GCC_GPLL3_M_VAL_IN)
#define HWIO_GCC_GPLL3_M_VAL_PLL_M_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL3_M_VAL_PLL_M_SHFT                                                                      0x0

#define HWIO_GCC_GPLL3_N_VAL_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x000000cc)
#define HWIO_GCC_GPLL3_N_VAL_RMSK                                                                        0x7ffff
#define HWIO_GCC_GPLL3_N_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_N_VAL_ADDR, HWIO_GCC_GPLL3_N_VAL_RMSK)
#define HWIO_GCC_GPLL3_N_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_N_VAL_ADDR, m)
#define HWIO_GCC_GPLL3_N_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_N_VAL_ADDR,v)
#define HWIO_GCC_GPLL3_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_N_VAL_ADDR,m,v,HWIO_GCC_GPLL3_N_VAL_IN)
#define HWIO_GCC_GPLL3_N_VAL_PLL_N_BMSK                                                                  0x7ffff
#define HWIO_GCC_GPLL3_N_VAL_PLL_N_SHFT                                                                      0x0

#define HWIO_GCC_GPLL3_USER_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000000d0)
#define HWIO_GCC_GPLL3_USER_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL3_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_ADDR, HWIO_GCC_GPLL3_USER_CTL_RMSK)
#define HWIO_GCC_GPLL3_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL3_USER_CTL_IN)
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS31_25_BMSK                                                0xfe000000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS31_25_SHFT                                                      0x19
#define HWIO_GCC_GPLL3_USER_CTL_MN_EN_BMSK                                                             0x1000000
#define HWIO_GCC_GPLL3_USER_CTL_MN_EN_SHFT                                                                  0x18
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS23_21_BMSK                                                  0xe00000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS23_21_SHFT                                                      0x15
#define HWIO_GCC_GPLL3_USER_CTL_VCO_SEL_BMSK                                                            0x100000
#define HWIO_GCC_GPLL3_USER_CTL_VCO_SEL_SHFT                                                                0x14
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS19_13_BMSK                                                   0xfe000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS19_13_SHFT                                                       0xd
#define HWIO_GCC_GPLL3_USER_CTL_PRE_DIV_RATIO_BMSK                                                        0x1000
#define HWIO_GCC_GPLL3_USER_CTL_PRE_DIV_RATIO_SHFT                                                           0xc
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS11_10_BMSK                                                     0xc00
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS11_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL3_USER_CTL_POST_DIV_RATIO_BMSK                                                        0x300
#define HWIO_GCC_GPLL3_USER_CTL_POST_DIV_RATIO_SHFT                                                          0x8
#define HWIO_GCC_GPLL3_USER_CTL_OUTPUT_INV_BMSK                                                             0x80
#define HWIO_GCC_GPLL3_USER_CTL_OUTPUT_INV_SHFT                                                              0x7
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS6_5_BMSK                                                        0x60
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS6_5_SHFT                                                         0x5
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_TEST_BMSK                                                         0x10
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_TEST_SHFT                                                          0x4
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                         0x8
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                         0x3
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_BIST_BMSK                                                          0x4
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_BIST_SHFT                                                          0x2
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX_BMSK                                                           0x2
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX_SHFT                                                           0x1
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                          0x1
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                          0x0

#define HWIO_GCC_GPLL3_CONFIG_CTL_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x000000d4)
#define HWIO_GCC_GPLL3_CONFIG_CTL_RMSK                                                                0xffffffff
#define HWIO_GCC_GPLL3_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR, HWIO_GCC_GPLL3_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL3_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL3_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BITS31_30_BMSK                                              0xc0000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BITS31_30_SHFT                                                    0x1e
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_PFD_UP_BMSK                                                   0x20000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_PFD_UP_SHFT                                                         0x1d
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_PFD_DOWN_BMSK                                                 0x10000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_PFD_DOWN_SHFT                                                       0x1c
#define HWIO_GCC_GPLL3_CONFIG_CTL_NMOSC_FREQ_CTRL_BMSK                                                 0xc000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_NMOSC_FREQ_CTRL_SHFT                                                      0x1a
#define HWIO_GCC_GPLL3_CONFIG_CTL_PFD_DZSEL_BMSK                                                       0x3000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_PFD_DZSEL_SHFT                                                            0x18
#define HWIO_GCC_GPLL3_CONFIG_CTL_NMOSC_EN_BMSK                                                         0x800000
#define HWIO_GCC_GPLL3_CONFIG_CTL_NMOSC_EN_SHFT                                                             0x17
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BIT22_BMSK                                                    0x400000
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BIT22_SHFT                                                        0x16
#define HWIO_GCC_GPLL3_CONFIG_CTL_ICP_DIV_BMSK                                                          0x300000
#define HWIO_GCC_GPLL3_CONFIG_CTL_ICP_DIV_SHFT                                                              0x14
#define HWIO_GCC_GPLL3_CONFIG_CTL_IREG_DIV_BMSK                                                          0xc0000
#define HWIO_GCC_GPLL3_CONFIG_CTL_IREG_DIV_SHFT                                                             0x12
#define HWIO_GCC_GPLL3_CONFIG_CTL_CUSEL_BMSK                                                             0x30000
#define HWIO_GCC_GPLL3_CONFIG_CTL_CUSEL_SHFT                                                                0x10
#define HWIO_GCC_GPLL3_CONFIG_CTL_REF_MODE_BMSK                                                           0x8000
#define HWIO_GCC_GPLL3_CONFIG_CTL_REF_MODE_SHFT                                                              0xf
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BIT14_BMSK                                                      0x4000
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BIT14_SHFT                                                         0xe
#define HWIO_GCC_GPLL3_CONFIG_CTL_CFG_LOCKDET_BMSK                                                        0x3000
#define HWIO_GCC_GPLL3_CONFIG_CTL_CFG_LOCKDET_SHFT                                                           0xc
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_ISEED_BMSK                                                         0x800
#define HWIO_GCC_GPLL3_CONFIG_CTL_FORCE_ISEED_SHFT                                                           0xb
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BITS10_0_BMSK                                                    0x7ff
#define HWIO_GCC_GPLL3_CONFIG_CTL_RESERVE_BITS10_0_SHFT                                                      0x0

#define HWIO_GCC_GPLL3_TEST_CTL_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000000d8)
#define HWIO_GCC_GPLL3_TEST_CTL_RMSK                                                                  0xffffffff
#define HWIO_GCC_GPLL3_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_ADDR, HWIO_GCC_GPLL3_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL3_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL3_TEST_CTL_IN)
#define HWIO_GCC_GPLL3_TEST_CTL_RESERVE_BITS31_15_BMSK                                                0xffff8000
#define HWIO_GCC_GPLL3_TEST_CTL_RESERVE_BITS31_15_SHFT                                                       0xf
#define HWIO_GCC_GPLL3_TEST_CTL_PLLOUT_LV_TEST_SEL_BMSK                                                   0x4000
#define HWIO_GCC_GPLL3_TEST_CTL_PLLOUT_LV_TEST_SEL_SHFT                                                      0xe
#define HWIO_GCC_GPLL3_TEST_CTL_RESERVE_BITS13_10_BMSK                                                    0x3c00
#define HWIO_GCC_GPLL3_TEST_CTL_RESERVE_BITS13_10_SHFT                                                       0xa
#define HWIO_GCC_GPLL3_TEST_CTL_ICP_TST_EN_BMSK                                                            0x200
#define HWIO_GCC_GPLL3_TEST_CTL_ICP_TST_EN_SHFT                                                              0x9
#define HWIO_GCC_GPLL3_TEST_CTL_ICP_EXT_SEL_BMSK                                                           0x100
#define HWIO_GCC_GPLL3_TEST_CTL_ICP_EXT_SEL_SHFT                                                             0x8
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_SEL_BMSK                                                              0x80
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_SEL_SHFT                                                               0x7
#define HWIO_GCC_GPLL3_TEST_CTL_BYP_TESTAMP_BMSK                                                            0x40
#define HWIO_GCC_GPLL3_TEST_CTL_BYP_TESTAMP_SHFT                                                             0x6
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_SEL_BMSK                                                             0x30
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_SEL_SHFT                                                              0x4
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_SEL_BMSK                                                              0xc
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_SEL_SHFT                                                              0x2
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_EN_BMSK                                                               0x2
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_EN_SHFT                                                               0x1
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_EN_BMSK                                                               0x1
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_EN_SHFT                                                               0x0

#define HWIO_GCC_GPLL3_STATUS_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000000dc)
#define HWIO_GCC_GPLL3_STATUS_RMSK                                                                       0x3ffff
#define HWIO_GCC_GPLL3_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_STATUS_ADDR, HWIO_GCC_GPLL3_STATUS_RMSK)
#define HWIO_GCC_GPLL3_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_STATUS_ADDR, m)
#define HWIO_GCC_GPLL3_STATUS_PLL_ACTIVE_FLAG_BMSK                                                       0x20000
#define HWIO_GCC_GPLL3_STATUS_PLL_ACTIVE_FLAG_SHFT                                                          0x11
#define HWIO_GCC_GPLL3_STATUS_PLL_LOCK_DET_BMSK                                                          0x10000
#define HWIO_GCC_GPLL3_STATUS_PLL_LOCK_DET_SHFT                                                             0x10
#define HWIO_GCC_GPLL3_STATUS_PLL_D_BMSK                                                                  0xffff
#define HWIO_GCC_GPLL3_STATUS_PLL_D_SHFT                                                                     0x0

#define HWIO_GCC_SYSTEM_NOC_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000100)
#define HWIO_GCC_SYSTEM_NOC_BCR_RMSK                                                                         0x1
#define HWIO_GCC_SYSTEM_NOC_BCR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BCR_ADDR, HWIO_GCC_SYSTEM_NOC_BCR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BCR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_BCR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_BCR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_BCR_IN)
#define HWIO_GCC_SYSTEM_NOC_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_SYSTEM_NOC_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000120)
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_RMSK                                                             0x80000013
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ADDR, HWIO_GCC_SYSTEM_NOC_CMD_RCGR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_CMD_RCGR_IN)
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_SYSTEM_NOC_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000124)
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_RMSK                                                                  0x71f
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_CFG_RCGR_ADDR, HWIO_GCC_SYSTEM_NOC_CFG_RCGR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_CFG_RCGR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_CFG_RCGR_IN)
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_SYSTEM_NOC_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000138)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_RMSK                                                             0x30001
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_IN          \
        in_dword_masked(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR, HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_RMSK)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR, m)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR,v)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR,m,v,HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_IN)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_DIVIDE_BMSK                                                      0x30000
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_DIVIDE_SHFT                                                         0x10
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000150)
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_RMSK                                                             0x80000013
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_CMD_RCGR_ADDR, HWIO_GCC_CONFIG_NOC_CMD_RCGR_RMSK)
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CONFIG_NOC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CONFIG_NOC_CMD_RCGR_ADDR,m,v,HWIO_GCC_CONFIG_NOC_CMD_RCGR_IN)
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_CONFIG_NOC_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000154)
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_RMSK                                                                  0x71f
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_CFG_RCGR_ADDR, HWIO_GCC_CONFIG_NOC_CFG_RCGR_RMSK)
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CONFIG_NOC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CONFIG_NOC_CFG_RCGR_ADDR,m,v,HWIO_GCC_CONFIG_NOC_CFG_RCGR_IN)
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_CONFIG_NOC_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000190)
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_RMSK                                                             0x80000013
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CMD_RCGR_ADDR, HWIO_GCC_PERIPH_NOC_CMD_RCGR_RMSK)
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_CMD_RCGR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_CMD_RCGR_IN)
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_PERIPH_NOC_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000194)
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_RMSK                                                                  0x71f
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CFG_RCGR_ADDR, HWIO_GCC_PERIPH_NOC_CFG_RCGR_RMSK)
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_CFG_RCGR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_CFG_RCGR_IN)
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_PERIPH_NOC_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000104)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_SYS_NOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000010c)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000110)
#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_RMSK                                                           0x80000000
#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_ADDR, HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SYS_NOC_KPSS_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f

#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000114)
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_CNOC_AHB_CBCR_ADDR, HWIO_GCC_SNOC_CNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_CNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_CNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_CNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_CNOC_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SNOC_CNOC_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000118)
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_PNOC_AHB_CBCR_ADDR, HWIO_GCC_SNOC_PNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_PNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_PNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_PNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_PNOC_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SNOC_PNOC_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_SYS_NOC_AT_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0000011c)
#define HWIO_GCC_SYS_NOC_AT_CBCR_RMSK                                                                 0x80000001
#define HWIO_GCC_SYS_NOC_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR, HWIO_GCC_SYS_NOC_AT_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_AT_CBCR_IN)
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_CONFIG_NOC_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000140)
#define HWIO_GCC_CONFIG_NOC_BCR_RMSK                                                                         0x1
#define HWIO_GCC_CONFIG_NOC_BCR_IN          \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_BCR_ADDR, HWIO_GCC_CONFIG_NOC_BCR_RMSK)
#define HWIO_GCC_CONFIG_NOC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CONFIG_NOC_BCR_ADDR, m)
#define HWIO_GCC_CONFIG_NOC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CONFIG_NOC_BCR_ADDR,v)
#define HWIO_GCC_CONFIG_NOC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CONFIG_NOC_BCR_ADDR,m,v,HWIO_GCC_CONFIG_NOC_BCR_IN)
#define HWIO_GCC_CONFIG_NOC_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_CONFIG_NOC_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_CFG_NOC_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000144)
#define HWIO_GCC_CFG_NOC_AHB_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_CFG_NOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CFG_NOC_AHB_CBCR_ADDR, HWIO_GCC_CFG_NOC_AHB_CBCR_RMSK)
#define HWIO_GCC_CFG_NOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CFG_NOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CFG_NOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CFG_NOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CFG_NOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CFG_NOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_CFG_NOC_AHB_CBCR_IN)
#define HWIO_GCC_CFG_NOC_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_CFG_NOC_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_CFG_NOC_AHB_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_CFG_NOC_AHB_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000148)
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR, HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_RMSK)
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR, m)
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR,v)
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_ADDR,m,v,HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_IN)
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_CFG_NOC_DDR_CFG_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0000014c)
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_ADDR, HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_RMSK)
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_ADDR,m,v,HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_IN)
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_CFG_NOC_RPM_AHB_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_PERIPH_NOC_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000180)
#define HWIO_GCC_PERIPH_NOC_BCR_RMSK                                                                         0x1
#define HWIO_GCC_PERIPH_NOC_BCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_BCR_ADDR, HWIO_GCC_PERIPH_NOC_BCR_RMSK)
#define HWIO_GCC_PERIPH_NOC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_BCR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_BCR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_BCR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_BCR_IN)
#define HWIO_GCC_PERIPH_NOC_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_PERIPH_NOC_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000184)
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_AHB_CBCR_ADDR, HWIO_GCC_PERIPH_NOC_AHB_CBCR_RMSK)
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_AHB_CBCR_IN)
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_PERIPH_NOC_AHB_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00000188)
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_RMSK                                                         0x80000001
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_ADDR, HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_IN)
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_PERIPH_NOC_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_PERIPH_NOC_AT_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000018c)
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_AT_CBCR_ADDR, HWIO_GCC_PERIPH_NOC_AT_CBCR_RMSK)
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_AT_CBCR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_AT_CBCR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_AT_CBCR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_AT_CBCR_IN)
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_PERIPH_NOC_AT_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000001c0)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR, HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_RMSK)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR, m)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR,v)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR,m,v,HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_IN)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_IMEM_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000200)
#define HWIO_GCC_IMEM_BCR_RMSK                                                                               0x1
#define HWIO_GCC_IMEM_BCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_BCR_ADDR, HWIO_GCC_IMEM_BCR_RMSK)
#define HWIO_GCC_IMEM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_BCR_ADDR, m)
#define HWIO_GCC_IMEM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_BCR_ADDR,v)
#define HWIO_GCC_IMEM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_BCR_ADDR,m,v,HWIO_GCC_IMEM_BCR_IN)
#define HWIO_GCC_IMEM_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_IMEM_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_IMEM_AXI_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000204)
#define HWIO_GCC_IMEM_AXI_CBCR_RMSK                                                                   0xf000fff0
#define HWIO_GCC_IMEM_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_AXI_CBCR_ADDR, HWIO_GCC_IMEM_AXI_CBCR_RMSK)
#define HWIO_GCC_IMEM_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_AXI_CBCR_ADDR, m)
#define HWIO_GCC_IMEM_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_AXI_CBCR_ADDR,v)
#define HWIO_GCC_IMEM_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_AXI_CBCR_ADDR,m,v,HWIO_GCC_IMEM_AXI_CBCR_IN)
#define HWIO_GCC_IMEM_AXI_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_IMEM_AXI_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_IMEM_AXI_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_IMEM_AXI_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_IMEM_AXI_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_IMEM_AXI_CBCR_SLEEP_SHFT                                                                    0x4

#define HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000208)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR, HWIO_GCC_IMEM_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_IMEM_CFG_AHB_CBCR_IN)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_MMSS_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000240)
#define HWIO_GCC_MMSS_BCR_RMSK                                                                               0x1
#define HWIO_GCC_MMSS_BCR_IN          \
        in_dword_masked(HWIO_GCC_MMSS_BCR_ADDR, HWIO_GCC_MMSS_BCR_RMSK)
#define HWIO_GCC_MMSS_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MMSS_BCR_ADDR, m)
#define HWIO_GCC_MMSS_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MMSS_BCR_ADDR,v)
#define HWIO_GCC_MMSS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MMSS_BCR_ADDR,m,v,HWIO_GCC_MMSS_BCR_IN)
#define HWIO_GCC_MMSS_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_MMSS_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000244)
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_RMSK                                                          0xf0008000
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_ADDR, HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_RMSK)
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_ADDR,m,v,HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_IN)
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                 0x70000000
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                       0x1c
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                         0x8000
#define HWIO_GCC_OCMEM_SYS_NOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                            0xf

#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000024c)
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_ADDR, HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_IN)
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_MMSS_NOC_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_MMSS_NOC_AT_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000250)
#define HWIO_GCC_MMSS_NOC_AT_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_MMSS_NOC_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MMSS_NOC_AT_CBCR_ADDR, HWIO_GCC_MMSS_NOC_AT_CBCR_RMSK)
#define HWIO_GCC_MMSS_NOC_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MMSS_NOC_AT_CBCR_ADDR, m)
#define HWIO_GCC_MMSS_NOC_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MMSS_NOC_AT_CBCR_ADDR,v)
#define HWIO_GCC_MMSS_NOC_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MMSS_NOC_AT_CBCR_ADDR,m,v,HWIO_GCC_MMSS_NOC_AT_CBCR_IN)
#define HWIO_GCC_MMSS_NOC_AT_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_MMSS_NOC_AT_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_MMSS_NOC_AT_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_MMSS_NOC_AT_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000280)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_RMSK                                                                0xf0008001
#define HWIO_GCC_MSS_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR, HWIO_GCC_MSS_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_MSS_CFG_AHB_CBCR_IN)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000284)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_RMSK                                                            0x80000003
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR, HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_RMSK)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR,m,v,HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_IN)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_HW_CTL_BMSK                                                            0x2
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_HW_CTL_SHFT                                                            0x1
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x000002c0)
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_RMSK                                                         0xf0008001
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_ADDR, HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_RMSK)
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_IN)
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                0x70000000
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                      0x1c
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                        0x8000
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                           0xf
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_QDSS_RBCPR_XPU_AHB_CBCR_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_QDSS_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000300)
#define HWIO_GCC_QDSS_BCR_RMSK                                                                               0x1
#define HWIO_GCC_QDSS_BCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_BCR_ADDR, HWIO_GCC_QDSS_BCR_RMSK)
#define HWIO_GCC_QDSS_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_BCR_ADDR, m)
#define HWIO_GCC_QDSS_BCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_BCR_ADDR,v)
#define HWIO_GCC_QDSS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_BCR_ADDR,m,v,HWIO_GCC_QDSS_BCR_IN)
#define HWIO_GCC_QDSS_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_QDSS_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000304)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR, HWIO_GCC_QDSS_DAP_AHB_CBCR_RMSK)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_DAP_AHB_CBCR_IN)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000308)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR, HWIO_GCC_QDSS_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_CFG_AHB_CBCR_IN)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000340)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_RMSK                                                                0x80000013
#define HWIO_GCC_QDSS_AT_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR, HWIO_GCC_QDSS_AT_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_AT_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_AT_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_QDSS_AT_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_QDSS_AT_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_QDSS_AT_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000344)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_RMSK                                                                     0x71f
#define HWIO_GCC_QDSS_AT_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR, HWIO_GCC_QDSS_AT_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_AT_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_QDSS_AT_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000030c)
#define HWIO_GCC_QDSS_AT_CBCR_RMSK                                                                    0x80007ff1
#define HWIO_GCC_QDSS_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CBCR_ADDR, HWIO_GCC_QDSS_AT_CBCR_RMSK)
#define HWIO_GCC_QDSS_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CBCR_ADDR,m,v,HWIO_GCC_QDSS_AT_CBCR_IN)
#define HWIO_GCC_QDSS_AT_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_QDSS_AT_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_CORE_ON_BMSK                                                      0x4000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_CORE_ON_SHFT                                                         0xe
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                    0x2000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                       0xd
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                   0x1000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                      0xc
#define HWIO_GCC_QDSS_AT_CBCR_WAKEUP_BMSK                                                                  0xf00
#define HWIO_GCC_QDSS_AT_CBCR_WAKEUP_SHFT                                                                    0x8
#define HWIO_GCC_QDSS_AT_CBCR_SLEEP_BMSK                                                                    0xf0
#define HWIO_GCC_QDSS_AT_CBCR_SLEEP_SHFT                                                                     0x4
#define HWIO_GCC_QDSS_AT_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_QDSS_AT_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000310)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_QDSS_ETR_USB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR, HWIO_GCC_QDSS_ETR_USB_CBCR_RMSK)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_ETR_USB_CBCR_IN)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000358)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_QDSS_STM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR, HWIO_GCC_QDSS_STM_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_STM_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_QDSS_STM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_QDSS_STM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_QDSS_STM_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_QDSS_STM_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000035c)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_QDSS_STM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR, HWIO_GCC_QDSS_STM_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_STM_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_QDSS_STM_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000314)
#define HWIO_GCC_QDSS_STM_CBCR_RMSK                                                                   0xf0008001
#define HWIO_GCC_QDSS_STM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CBCR_ADDR, HWIO_GCC_QDSS_STM_CBCR_RMSK)
#define HWIO_GCC_QDSS_STM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CBCR_ADDR,m,v,HWIO_GCC_QDSS_STM_CBCR_IN)
#define HWIO_GCC_QDSS_STM_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_QDSS_STM_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf
#define HWIO_GCC_QDSS_STM_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_QDSS_STM_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000380)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_RMSK                                                        0x80000013
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                               0x10
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                0x4
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_EN_SHFT                                                       0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_UPDATE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_UPDATE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000384)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_RMSK                                                             0x71f
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_SEL_BMSK                                                     0x700
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_SEL_SHFT                                                       0x8
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_DIV_BMSK                                                      0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_DIV_SHFT                                                       0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000318)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CBCR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CBCR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000398)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_RMSK                                                             0x80000013
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR, HWIO_GCC_QDSS_TSCTR_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0000039c)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_RMSK                                                                  0x71f
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR, HWIO_GCC_QDSS_TSCTR_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0000031c)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000320)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_DAP_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000324)
#define HWIO_GCC_QDSS_DAP_CBCR_RMSK                                                                   0x80000001
#define HWIO_GCC_QDSS_DAP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_DAP_CBCR_ADDR, HWIO_GCC_QDSS_DAP_CBCR_RMSK)
#define HWIO_GCC_QDSS_DAP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_DAP_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_DAP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_DAP_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_DAP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_DAP_CBCR_ADDR,m,v,HWIO_GCC_QDSS_DAP_CBCR_IN)
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000328)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0000032c)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000330)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x000010d0)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_RMSK                                                              0x80000002
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR, HWIO_GCC_GCC_SLEEP_CMD_RCGR_RMSK)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR,m,v,HWIO_GCC_GCC_SLEEP_CMD_RCGR_IN)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_OFF_SHFT                                                           0x1f
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_EN_BMSK                                                             0x2
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_EN_SHFT                                                             0x1

#define HWIO_GCC_USB_HS_HSIC_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000400)
#define HWIO_GCC_USB_HS_HSIC_BCR_RMSK                                                                        0x1
#define HWIO_GCC_USB_HS_HSIC_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_HSIC_BCR_ADDR, HWIO_GCC_USB_HS_HSIC_BCR_RMSK)
#define HWIO_GCC_USB_HS_HSIC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_HSIC_BCR_ADDR, m)
#define HWIO_GCC_USB_HS_HSIC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_HSIC_BCR_ADDR,v)
#define HWIO_GCC_USB_HS_HSIC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_HSIC_BCR_ADDR,m,v,HWIO_GCC_USB_HS_HSIC_BCR_IN)
#define HWIO_GCC_USB_HS_HSIC_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_USB_HS_HSIC_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_USB_HS_HSIC_GDSCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000404)
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RMSK                                                               0xf8ffffff
#define HWIO_GCC_USB_HS_HSIC_GDSCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_HSIC_GDSCR_ADDR, HWIO_GCC_USB_HS_HSIC_GDSCR_RMSK)
#define HWIO_GCC_USB_HS_HSIC_GDSCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_HSIC_GDSCR_ADDR, m)
#define HWIO_GCC_USB_HS_HSIC_GDSCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_HSIC_GDSCR_ADDR,v)
#define HWIO_GCC_USB_HS_HSIC_GDSCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_HSIC_GDSCR_ADDR,m,v,HWIO_GCC_USB_HS_HSIC_GDSCR_IN)
#define HWIO_GCC_USB_HS_HSIC_GDSCR_PWR_ON_BMSK                                                        0x80000000
#define HWIO_GCC_USB_HS_HSIC_GDSCR_PWR_ON_SHFT                                                              0x1f
#define HWIO_GCC_USB_HS_HSIC_GDSCR_GDSC_STATE_BMSK                                                    0x78000000
#define HWIO_GCC_USB_HS_HSIC_GDSCR_GDSC_STATE_SHFT                                                          0x1b
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_REST_WAIT_BMSK                                                    0xf00000
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_REST_WAIT_SHFT                                                        0x14
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_FEW_WAIT_BMSK                                                      0xf0000
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_FEW_WAIT_SHFT                                                         0x10
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLK_DIS_WAIT_BMSK                                                      0xf000
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLK_DIS_WAIT_SHFT                                                         0xc
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RETAIN_FF_ENABLE_BMSK                                                   0x800
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RETAIN_FF_ENABLE_SHFT                                                     0xb
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RESTORE_BMSK                                                            0x400
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RESTORE_SHFT                                                              0xa
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SAVE_BMSK                                                               0x200
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SAVE_SHFT                                                                 0x9
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RETAIN_BMSK                                                             0x100
#define HWIO_GCC_USB_HS_HSIC_GDSCR_RETAIN_SHFT                                                               0x8
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_REST_BMSK                                                             0x80
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_REST_SHFT                                                              0x7
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_FEW_BMSK                                                              0x40
#define HWIO_GCC_USB_HS_HSIC_GDSCR_EN_FEW_SHFT                                                               0x6
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLAMP_IO_BMSK                                                            0x20
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLAMP_IO_SHFT                                                             0x5
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLK_DISABLE_BMSK                                                         0x10
#define HWIO_GCC_USB_HS_HSIC_GDSCR_CLK_DISABLE_SHFT                                                          0x4
#define HWIO_GCC_USB_HS_HSIC_GDSCR_PD_ARES_BMSK                                                              0x8
#define HWIO_GCC_USB_HS_HSIC_GDSCR_PD_ARES_SHFT                                                              0x3
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SW_OVERRIDE_BMSK                                                          0x4
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SW_OVERRIDE_SHFT                                                          0x2
#define HWIO_GCC_USB_HS_HSIC_GDSCR_HW_CONTROL_BMSK                                                           0x2
#define HWIO_GCC_USB_HS_HSIC_GDSCR_HW_CONTROL_SHFT                                                           0x1
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SW_COLLAPSE_BMSK                                                          0x1
#define HWIO_GCC_USB_HS_HSIC_GDSCR_SW_COLLAPSE_SHFT                                                          0x0

#define HWIO_GCC_USB_HSIC_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000408)
#define HWIO_GCC_USB_HSIC_AHB_CBCR_RMSK                                                               0xf000fff1
#define HWIO_GCC_USB_HSIC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_AHB_CBCR_ADDR, HWIO_GCC_USB_HSIC_AHB_CBCR_RMSK)
#define HWIO_GCC_USB_HSIC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_USB_HSIC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_USB_HSIC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_AHB_CBCR_ADDR,m,v,HWIO_GCC_USB_HSIC_AHB_CBCR_IN)
#define HWIO_GCC_USB_HSIC_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_USB_HSIC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_USB_HSIC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_USB_HSIC_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_USB_HSIC_AHB_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_USB_HSIC_AHB_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_USB_HSIC_AHB_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_USB_HSIC_AHB_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_USB_HSIC_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_USB_HSIC_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000041c)
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_RMSK                                                        0x80000013
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ADDR, HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_IN)
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                               0x10
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                0x4
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_ROOT_EN_SHFT                                                       0x1
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_UPDATE_BMSK                                                        0x1
#define HWIO_GCC_USB_HSIC_SYSTEM_CMD_RCGR_UPDATE_SHFT                                                        0x0

#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000420)
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_RMSK                                                             0x71f
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_ADDR, HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_IN)
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_SRC_SEL_BMSK                                                     0x700
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_SRC_SEL_SHFT                                                       0x8
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_SRC_DIV_BMSK                                                      0x1f
#define HWIO_GCC_USB_HSIC_SYSTEM_CFG_RCGR_SRC_DIV_SHFT                                                       0x0

#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0000040c)
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_RMSK                                                            0x80007ff1
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CBCR_ADDR, HWIO_GCC_USB_HSIC_SYSTEM_CBCR_RMSK)
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_SYSTEM_CBCR_ADDR, m)
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_SYSTEM_CBCR_ADDR,v)
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_SYSTEM_CBCR_ADDR,m,v,HWIO_GCC_USB_HSIC_SYSTEM_CBCR_IN)
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_CORE_ON_BMSK                                              0x4000
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_CORE_ON_SHFT                                                 0xe
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                            0x2000
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                               0xd
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                           0x1000
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                              0xc
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_WAKEUP_BMSK                                                          0xf00
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_WAKEUP_SHFT                                                            0x8
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_SLEEP_BMSK                                                            0xf0
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_SLEEP_SHFT                                                             0x4
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_USB_HSIC_SYSTEM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_USB_HSIC_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000440)
#define HWIO_GCC_USB_HSIC_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_USB_HSIC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_CMD_RCGR_ADDR, HWIO_GCC_USB_HSIC_CMD_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_CMD_RCGR_IN)
#define HWIO_GCC_USB_HSIC_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_USB_HSIC_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_USB_HSIC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_USB_HSIC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_USB_HSIC_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_USB_HSIC_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_USB_HSIC_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_USB_HSIC_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_USB_HSIC_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000444)
#define HWIO_GCC_USB_HSIC_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_USB_HSIC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_CFG_RCGR_ADDR, HWIO_GCC_USB_HSIC_CFG_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_CFG_RCGR_IN)
#define HWIO_GCC_USB_HSIC_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_USB_HSIC_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_USB_HSIC_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_USB_HSIC_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_USB_HSIC_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000410)
#define HWIO_GCC_USB_HSIC_CBCR_RMSK                                                                   0x80000001
#define HWIO_GCC_USB_HSIC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_CBCR_ADDR, HWIO_GCC_USB_HSIC_CBCR_RMSK)
#define HWIO_GCC_USB_HSIC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_CBCR_ADDR, m)
#define HWIO_GCC_USB_HSIC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_CBCR_ADDR,v)
#define HWIO_GCC_USB_HSIC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_CBCR_ADDR,m,v,HWIO_GCC_USB_HSIC_CBCR_IN)
#define HWIO_GCC_USB_HSIC_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_USB_HSIC_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_USB_HSIC_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_USB_HSIC_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000458)
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_RMSK                                                        0x80000013
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ADDR, HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_IN)
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                               0x10
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                0x4
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_ROOT_EN_SHFT                                                       0x1
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_UPDATE_BMSK                                                        0x1
#define HWIO_GCC_USB_HSIC_IO_CAL_CMD_RCGR_UPDATE_SHFT                                                        0x0

#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000045c)
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_RMSK                                                              0x1f
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_ADDR, HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_RMSK)
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_IN)
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_SRC_DIV_BMSK                                                      0x1f
#define HWIO_GCC_USB_HSIC_IO_CAL_CFG_RCGR_SRC_DIV_SHFT                                                       0x0

#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000414)
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CBCR_ADDR, HWIO_GCC_USB_HSIC_IO_CAL_CBCR_RMSK)
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_CBCR_ADDR, m)
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_IO_CAL_CBCR_ADDR,v)
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_IO_CAL_CBCR_ADDR,m,v,HWIO_GCC_USB_HSIC_IO_CAL_CBCR_IN)
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_USB_HSIC_IO_CAL_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00000418)
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_ADDR, HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_RMSK)
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_IN)
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_USB_HSIC_IO_CAL_SLEEP_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_USB_HS_BCR_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00000480)
#define HWIO_GCC_USB_HS_BCR_RMSK                                                                             0x1
#define HWIO_GCC_USB_HS_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_BCR_ADDR, HWIO_GCC_USB_HS_BCR_RMSK)
#define HWIO_GCC_USB_HS_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_BCR_ADDR, m)
#define HWIO_GCC_USB_HS_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_BCR_ADDR,v)
#define HWIO_GCC_USB_HS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_BCR_ADDR,m,v,HWIO_GCC_USB_HS_BCR_IN)
#define HWIO_GCC_USB_HS_BCR_BLK_ARES_BMSK                                                                    0x1
#define HWIO_GCC_USB_HS_BCR_BLK_ARES_SHFT                                                                    0x0

#define HWIO_GCC_USB_HS_SYSTEM_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000484)
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_RMSK                                                              0x80007ff1
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CBCR_ADDR, HWIO_GCC_USB_HS_SYSTEM_CBCR_RMSK)
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CBCR_ADDR, m)
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_SYSTEM_CBCR_ADDR,v)
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_SYSTEM_CBCR_ADDR,m,v,HWIO_GCC_USB_HS_SYSTEM_CBCR_IN)
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_CORE_ON_BMSK                                                0x4000
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_CORE_ON_SHFT                                                   0xe
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                              0x2000
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                 0xd
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                             0x1000
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                0xc
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_WAKEUP_BMSK                                                            0xf00
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_WAKEUP_SHFT                                                              0x8
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_SLEEP_BMSK                                                              0xf0
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_SLEEP_SHFT                                                               0x4
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_USB_HS_SYSTEM_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_USB_HS_AHB_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000488)
#define HWIO_GCC_USB_HS_AHB_CBCR_RMSK                                                                 0xf000fff1
#define HWIO_GCC_USB_HS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_AHB_CBCR_ADDR, HWIO_GCC_USB_HS_AHB_CBCR_RMSK)
#define HWIO_GCC_USB_HS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_USB_HS_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_AHB_CBCR_ADDR,v)
#define HWIO_GCC_USB_HS_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_AHB_CBCR_ADDR,m,v,HWIO_GCC_USB_HS_AHB_CBCR_IN)
#define HWIO_GCC_USB_HS_AHB_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_USB_HS_AHB_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_USB_HS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                        0x70000000
#define HWIO_GCC_USB_HS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                              0x1c
#define HWIO_GCC_USB_HS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                0x8000
#define HWIO_GCC_USB_HS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                   0xf
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                   0x4000
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                      0xe
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                 0x2000
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                    0xd
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                0x1000
#define HWIO_GCC_USB_HS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                   0xc
#define HWIO_GCC_USB_HS_AHB_CBCR_WAKEUP_BMSK                                                               0xf00
#define HWIO_GCC_USB_HS_AHB_CBCR_WAKEUP_SHFT                                                                 0x8
#define HWIO_GCC_USB_HS_AHB_CBCR_SLEEP_BMSK                                                                 0xf0
#define HWIO_GCC_USB_HS_AHB_CBCR_SLEEP_SHFT                                                                  0x4
#define HWIO_GCC_USB_HS_AHB_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_USB_HS_AHB_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0000048c)
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_RMSK                                                   0x80000001
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_ADDR, HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_RMSK)
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_ADDR, m)
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_ADDR,v)
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_ADDR,m,v,HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_IN)
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_GCC_USB_HS_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_SHFT                                               0x0

#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000490)
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_RMSK                                                          0x80000013
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ADDR, HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_RMSK)
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_IN)
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ROOT_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ROOT_OFF_SHFT                                                       0x1f
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                 0x10
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                  0x4
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ROOT_EN_BMSK                                                         0x2
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_ROOT_EN_SHFT                                                         0x1
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_UPDATE_BMSK                                                          0x1
#define HWIO_GCC_USB_HS_SYSTEM_CMD_RCGR_UPDATE_SHFT                                                          0x0

#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000494)
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_RMSK                                                               0x71f
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_ADDR, HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_RMSK)
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_IN)
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_SRC_SEL_BMSK                                                       0x700
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_SRC_SEL_SHFT                                                         0x8
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_SRC_DIV_BMSK                                                        0x1f
#define HWIO_GCC_USB_HS_SYSTEM_CFG_RCGR_SRC_DIV_SHFT                                                         0x0

#define HWIO_GCC_USB2A_PHY_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x000004a8)
#define HWIO_GCC_USB2A_PHY_BCR_RMSK                                                                          0x1
#define HWIO_GCC_USB2A_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB2A_PHY_BCR_ADDR, HWIO_GCC_USB2A_PHY_BCR_RMSK)
#define HWIO_GCC_USB2A_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB2A_PHY_BCR_ADDR, m)
#define HWIO_GCC_USB2A_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB2A_PHY_BCR_ADDR,v)
#define HWIO_GCC_USB2A_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB2A_PHY_BCR_ADDR,m,v,HWIO_GCC_USB2A_PHY_BCR_IN)
#define HWIO_GCC_USB2A_PHY_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_USB2A_PHY_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x000004ac)
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB2A_PHY_SLEEP_CBCR_ADDR, HWIO_GCC_USB2A_PHY_SLEEP_CBCR_RMSK)
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB2A_PHY_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB2A_PHY_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB2A_PHY_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_USB2A_PHY_SLEEP_CBCR_IN)
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_USB2A_PHY_SLEEP_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_SDCC1_BCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x000004c0)
#define HWIO_GCC_SDCC1_BCR_RMSK                                                                              0x1
#define HWIO_GCC_SDCC1_BCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_BCR_ADDR, HWIO_GCC_SDCC1_BCR_RMSK)
#define HWIO_GCC_SDCC1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_BCR_ADDR, m)
#define HWIO_GCC_SDCC1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_BCR_ADDR,v)
#define HWIO_GCC_SDCC1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_BCR_ADDR,m,v,HWIO_GCC_SDCC1_BCR_IN)
#define HWIO_GCC_SDCC1_BCR_BLK_ARES_BMSK                                                                     0x1
#define HWIO_GCC_SDCC1_BCR_BLK_ARES_SHFT                                                                     0x0

#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x000004d0)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_RMSK                                                             0x800000f3
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR, HWIO_GCC_SDCC1_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CMD_RCGR_IN)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_D_BMSK                                                           0x80
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_D_SHFT                                                            0x7
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_M_BMSK                                                           0x40
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_M_SHFT                                                            0x6
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_N_BMSK                                                           0x20
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_N_SHFT                                                            0x5
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x000004d4)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_RMSK                                                                 0x371f
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR, HWIO_GCC_SDCC1_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CFG_RCGR_IN)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_MODE_BMSK                                                            0x3000
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_MODE_SHFT                                                               0xc
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_SDCC1_APPS_M_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000004d8)
#define HWIO_GCC_SDCC1_APPS_M_RMSK                                                                          0xff
#define HWIO_GCC_SDCC1_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_M_ADDR, HWIO_GCC_SDCC1_APPS_M_RMSK)
#define HWIO_GCC_SDCC1_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_M_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_M_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_M_ADDR,m,v,HWIO_GCC_SDCC1_APPS_M_IN)
#define HWIO_GCC_SDCC1_APPS_M_M_BMSK                                                                        0xff
#define HWIO_GCC_SDCC1_APPS_M_M_SHFT                                                                         0x0

#define HWIO_GCC_SDCC1_APPS_N_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000004dc)
#define HWIO_GCC_SDCC1_APPS_N_RMSK                                                                          0xff
#define HWIO_GCC_SDCC1_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_N_ADDR, HWIO_GCC_SDCC1_APPS_N_RMSK)
#define HWIO_GCC_SDCC1_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_N_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_N_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_N_ADDR,m,v,HWIO_GCC_SDCC1_APPS_N_IN)
#define HWIO_GCC_SDCC1_APPS_N_N_BMSK                                                                        0xff
#define HWIO_GCC_SDCC1_APPS_N_N_SHFT                                                                         0x0

#define HWIO_GCC_SDCC1_APPS_D_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000004e0)
#define HWIO_GCC_SDCC1_APPS_D_RMSK                                                                          0xff
#define HWIO_GCC_SDCC1_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_D_ADDR, HWIO_GCC_SDCC1_APPS_D_RMSK)
#define HWIO_GCC_SDCC1_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_D_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_D_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_D_ADDR,m,v,HWIO_GCC_SDCC1_APPS_D_IN)
#define HWIO_GCC_SDCC1_APPS_D_D_BMSK                                                                        0xff
#define HWIO_GCC_SDCC1_APPS_D_D_SHFT                                                                         0x0

#define HWIO_GCC_SDCC1_APPS_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x000004c4)
#define HWIO_GCC_SDCC1_APPS_CBCR_RMSK                                                                 0x80007ff1
#define HWIO_GCC_SDCC1_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CBCR_ADDR, HWIO_GCC_SDCC1_APPS_CBCR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CBCR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CBCR_IN)
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_CORE_ON_BMSK                                                   0x4000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_CORE_ON_SHFT                                                      0xe
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                 0x2000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                    0xd
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                0x1000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                   0xc
#define HWIO_GCC_SDCC1_APPS_CBCR_WAKEUP_BMSK                                                               0xf00
#define HWIO_GCC_SDCC1_APPS_CBCR_WAKEUP_SHFT                                                                 0x8
#define HWIO_GCC_SDCC1_APPS_CBCR_SLEEP_BMSK                                                                 0xf0
#define HWIO_GCC_SDCC1_APPS_CBCR_SLEEP_SHFT                                                                  0x4
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_SDCC1_AHB_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000004c8)
#define HWIO_GCC_SDCC1_AHB_CBCR_RMSK                                                                  0xf000fff1
#define HWIO_GCC_SDCC1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_AHB_CBCR_ADDR, HWIO_GCC_SDCC1_AHB_CBCR_RMSK)
#define HWIO_GCC_SDCC1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SDCC1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SDCC1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_AHB_CBCR_ADDR,m,v,HWIO_GCC_SDCC1_AHB_CBCR_IN)
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                         0x70000000
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                               0x1c
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                 0x8000
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                    0xf
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                    0x4000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                       0xe
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                  0x2000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                     0xd
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                 0x1000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                    0xc
#define HWIO_GCC_SDCC1_AHB_CBCR_WAKEUP_BMSK                                                                0xf00
#define HWIO_GCC_SDCC1_AHB_CBCR_WAKEUP_SHFT                                                                  0x8
#define HWIO_GCC_SDCC1_AHB_CBCR_SLEEP_BMSK                                                                  0xf0
#define HWIO_GCC_SDCC1_AHB_CBCR_SLEEP_SHFT                                                                   0x4
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000004cc)
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_ADDR, HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_RMSK)
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_ADDR,m,v,HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_IN)
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_SDCC1_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_SDCC2_BCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000500)
#define HWIO_GCC_SDCC2_BCR_RMSK                                                                              0x1
#define HWIO_GCC_SDCC2_BCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_BCR_ADDR, HWIO_GCC_SDCC2_BCR_RMSK)
#define HWIO_GCC_SDCC2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_BCR_ADDR, m)
#define HWIO_GCC_SDCC2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_BCR_ADDR,v)
#define HWIO_GCC_SDCC2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_BCR_ADDR,m,v,HWIO_GCC_SDCC2_BCR_IN)
#define HWIO_GCC_SDCC2_BCR_BLK_ARES_BMSK                                                                     0x1
#define HWIO_GCC_SDCC2_BCR_BLK_ARES_SHFT                                                                     0x0

#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000510)
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_RMSK                                                             0x800000f3
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CMD_RCGR_ADDR, HWIO_GCC_SDCC2_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_SDCC2_APPS_CMD_RCGR_IN)
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_D_BMSK                                                           0x80
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_D_SHFT                                                            0x7
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_M_BMSK                                                           0x40
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_M_SHFT                                                            0x6
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_N_BMSK                                                           0x20
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_N_SHFT                                                            0x5
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_SDCC2_APPS_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000514)
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_RMSK                                                                 0x371f
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CFG_RCGR_ADDR, HWIO_GCC_SDCC2_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_SDCC2_APPS_CFG_RCGR_IN)
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_MODE_BMSK                                                            0x3000
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_MODE_SHFT                                                               0xc
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_SDCC2_APPS_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_SDCC2_APPS_M_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000518)
#define HWIO_GCC_SDCC2_APPS_M_RMSK                                                                          0xff
#define HWIO_GCC_SDCC2_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_M_ADDR, HWIO_GCC_SDCC2_APPS_M_RMSK)
#define HWIO_GCC_SDCC2_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_M_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_M_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_M_ADDR,m,v,HWIO_GCC_SDCC2_APPS_M_IN)
#define HWIO_GCC_SDCC2_APPS_M_M_BMSK                                                                        0xff
#define HWIO_GCC_SDCC2_APPS_M_M_SHFT                                                                         0x0

#define HWIO_GCC_SDCC2_APPS_N_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000051c)
#define HWIO_GCC_SDCC2_APPS_N_RMSK                                                                          0xff
#define HWIO_GCC_SDCC2_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_N_ADDR, HWIO_GCC_SDCC2_APPS_N_RMSK)
#define HWIO_GCC_SDCC2_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_N_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_N_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_N_ADDR,m,v,HWIO_GCC_SDCC2_APPS_N_IN)
#define HWIO_GCC_SDCC2_APPS_N_N_BMSK                                                                        0xff
#define HWIO_GCC_SDCC2_APPS_N_N_SHFT                                                                         0x0

#define HWIO_GCC_SDCC2_APPS_D_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000520)
#define HWIO_GCC_SDCC2_APPS_D_RMSK                                                                          0xff
#define HWIO_GCC_SDCC2_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_D_ADDR, HWIO_GCC_SDCC2_APPS_D_RMSK)
#define HWIO_GCC_SDCC2_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_D_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_D_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_D_ADDR,m,v,HWIO_GCC_SDCC2_APPS_D_IN)
#define HWIO_GCC_SDCC2_APPS_D_D_BMSK                                                                        0xff
#define HWIO_GCC_SDCC2_APPS_D_D_SHFT                                                                         0x0

#define HWIO_GCC_SDCC2_APPS_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000504)
#define HWIO_GCC_SDCC2_APPS_CBCR_RMSK                                                                 0x80007ff1
#define HWIO_GCC_SDCC2_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CBCR_ADDR, HWIO_GCC_SDCC2_APPS_CBCR_RMSK)
#define HWIO_GCC_SDCC2_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_APPS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC2_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_APPS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC2_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_APPS_CBCR_ADDR,m,v,HWIO_GCC_SDCC2_APPS_CBCR_IN)
#define HWIO_GCC_SDCC2_APPS_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_SDCC2_APPS_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_CORE_ON_BMSK                                                   0x4000
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_CORE_ON_SHFT                                                      0xe
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                 0x2000
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                    0xd
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                0x1000
#define HWIO_GCC_SDCC2_APPS_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                   0xc
#define HWIO_GCC_SDCC2_APPS_CBCR_WAKEUP_BMSK                                                               0xf00
#define HWIO_GCC_SDCC2_APPS_CBCR_WAKEUP_SHFT                                                                 0x8
#define HWIO_GCC_SDCC2_APPS_CBCR_SLEEP_BMSK                                                                 0xf0
#define HWIO_GCC_SDCC2_APPS_CBCR_SLEEP_SHFT                                                                  0x4
#define HWIO_GCC_SDCC2_APPS_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_SDCC2_APPS_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_SDCC2_AHB_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000508)
#define HWIO_GCC_SDCC2_AHB_CBCR_RMSK                                                                  0xf000fff1
#define HWIO_GCC_SDCC2_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_AHB_CBCR_ADDR, HWIO_GCC_SDCC2_AHB_CBCR_RMSK)
#define HWIO_GCC_SDCC2_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SDCC2_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SDCC2_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_AHB_CBCR_ADDR,m,v,HWIO_GCC_SDCC2_AHB_CBCR_IN)
#define HWIO_GCC_SDCC2_AHB_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_SDCC2_AHB_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_SDCC2_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                         0x70000000
#define HWIO_GCC_SDCC2_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                               0x1c
#define HWIO_GCC_SDCC2_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                 0x8000
#define HWIO_GCC_SDCC2_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                    0xf
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                    0x4000
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                       0xe
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                  0x2000
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                     0xd
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                 0x1000
#define HWIO_GCC_SDCC2_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                    0xc
#define HWIO_GCC_SDCC2_AHB_CBCR_WAKEUP_BMSK                                                                0xf00
#define HWIO_GCC_SDCC2_AHB_CBCR_WAKEUP_SHFT                                                                  0x8
#define HWIO_GCC_SDCC2_AHB_CBCR_SLEEP_BMSK                                                                  0xf0
#define HWIO_GCC_SDCC2_AHB_CBCR_SLEEP_SHFT                                                                   0x4
#define HWIO_GCC_SDCC2_AHB_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_SDCC2_AHB_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000050c)
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_ADDR, HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_RMSK)
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_ADDR,m,v,HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_IN)
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_SDCC2_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_SDCC3_BCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000540)
#define HWIO_GCC_SDCC3_BCR_RMSK                                                                              0x1
#define HWIO_GCC_SDCC3_BCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_BCR_ADDR, HWIO_GCC_SDCC3_BCR_RMSK)
#define HWIO_GCC_SDCC3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_BCR_ADDR, m)
#define HWIO_GCC_SDCC3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_BCR_ADDR,v)
#define HWIO_GCC_SDCC3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_BCR_ADDR,m,v,HWIO_GCC_SDCC3_BCR_IN)
#define HWIO_GCC_SDCC3_BCR_BLK_ARES_BMSK                                                                     0x1
#define HWIO_GCC_SDCC3_BCR_BLK_ARES_SHFT                                                                     0x0

#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000550)
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_RMSK                                                             0x800000f3
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CMD_RCGR_ADDR, HWIO_GCC_SDCC3_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_SDCC3_APPS_CMD_RCGR_IN)
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_D_BMSK                                                           0x80
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_D_SHFT                                                            0x7
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_M_BMSK                                                           0x40
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_M_SHFT                                                            0x6
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_N_BMSK                                                           0x20
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_N_SHFT                                                            0x5
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_SDCC3_APPS_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000554)
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_RMSK                                                                 0x371f
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CFG_RCGR_ADDR, HWIO_GCC_SDCC3_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_SDCC3_APPS_CFG_RCGR_IN)
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_MODE_BMSK                                                            0x3000
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_MODE_SHFT                                                               0xc
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_SDCC3_APPS_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_SDCC3_APPS_M_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000558)
#define HWIO_GCC_SDCC3_APPS_M_RMSK                                                                          0xff
#define HWIO_GCC_SDCC3_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_M_ADDR, HWIO_GCC_SDCC3_APPS_M_RMSK)
#define HWIO_GCC_SDCC3_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_M_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_M_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_M_ADDR,m,v,HWIO_GCC_SDCC3_APPS_M_IN)
#define HWIO_GCC_SDCC3_APPS_M_M_BMSK                                                                        0xff
#define HWIO_GCC_SDCC3_APPS_M_M_SHFT                                                                         0x0

#define HWIO_GCC_SDCC3_APPS_N_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000055c)
#define HWIO_GCC_SDCC3_APPS_N_RMSK                                                                          0xff
#define HWIO_GCC_SDCC3_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_N_ADDR, HWIO_GCC_SDCC3_APPS_N_RMSK)
#define HWIO_GCC_SDCC3_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_N_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_N_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_N_ADDR,m,v,HWIO_GCC_SDCC3_APPS_N_IN)
#define HWIO_GCC_SDCC3_APPS_N_N_BMSK                                                                        0xff
#define HWIO_GCC_SDCC3_APPS_N_N_SHFT                                                                         0x0

#define HWIO_GCC_SDCC3_APPS_D_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000560)
#define HWIO_GCC_SDCC3_APPS_D_RMSK                                                                          0xff
#define HWIO_GCC_SDCC3_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_D_ADDR, HWIO_GCC_SDCC3_APPS_D_RMSK)
#define HWIO_GCC_SDCC3_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_D_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_D_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_D_ADDR,m,v,HWIO_GCC_SDCC3_APPS_D_IN)
#define HWIO_GCC_SDCC3_APPS_D_D_BMSK                                                                        0xff
#define HWIO_GCC_SDCC3_APPS_D_D_SHFT                                                                         0x0

#define HWIO_GCC_SDCC3_APPS_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000544)
#define HWIO_GCC_SDCC3_APPS_CBCR_RMSK                                                                 0x80007ff1
#define HWIO_GCC_SDCC3_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CBCR_ADDR, HWIO_GCC_SDCC3_APPS_CBCR_RMSK)
#define HWIO_GCC_SDCC3_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_APPS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC3_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_APPS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC3_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_APPS_CBCR_ADDR,m,v,HWIO_GCC_SDCC3_APPS_CBCR_IN)
#define HWIO_GCC_SDCC3_APPS_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_SDCC3_APPS_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_CORE_ON_BMSK                                                   0x4000
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_CORE_ON_SHFT                                                      0xe
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                 0x2000
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                    0xd
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                0x1000
#define HWIO_GCC_SDCC3_APPS_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                   0xc
#define HWIO_GCC_SDCC3_APPS_CBCR_WAKEUP_BMSK                                                               0xf00
#define HWIO_GCC_SDCC3_APPS_CBCR_WAKEUP_SHFT                                                                 0x8
#define HWIO_GCC_SDCC3_APPS_CBCR_SLEEP_BMSK                                                                 0xf0
#define HWIO_GCC_SDCC3_APPS_CBCR_SLEEP_SHFT                                                                  0x4
#define HWIO_GCC_SDCC3_APPS_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_SDCC3_APPS_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_SDCC3_AHB_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000548)
#define HWIO_GCC_SDCC3_AHB_CBCR_RMSK                                                                  0xf000fff1
#define HWIO_GCC_SDCC3_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_AHB_CBCR_ADDR, HWIO_GCC_SDCC3_AHB_CBCR_RMSK)
#define HWIO_GCC_SDCC3_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SDCC3_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SDCC3_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_AHB_CBCR_ADDR,m,v,HWIO_GCC_SDCC3_AHB_CBCR_IN)
#define HWIO_GCC_SDCC3_AHB_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_SDCC3_AHB_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_SDCC3_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                         0x70000000
#define HWIO_GCC_SDCC3_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                               0x1c
#define HWIO_GCC_SDCC3_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                 0x8000
#define HWIO_GCC_SDCC3_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                    0xf
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                    0x4000
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                       0xe
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                  0x2000
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                     0xd
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                 0x1000
#define HWIO_GCC_SDCC3_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                    0xc
#define HWIO_GCC_SDCC3_AHB_CBCR_WAKEUP_BMSK                                                                0xf00
#define HWIO_GCC_SDCC3_AHB_CBCR_WAKEUP_SHFT                                                                  0x8
#define HWIO_GCC_SDCC3_AHB_CBCR_SLEEP_BMSK                                                                  0xf0
#define HWIO_GCC_SDCC3_AHB_CBCR_SLEEP_SHFT                                                                   0x4
#define HWIO_GCC_SDCC3_AHB_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_SDCC3_AHB_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000054c)
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_ADDR, HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_RMSK)
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_ADDR,m,v,HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_IN)
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_SDCC3_INACTIVITY_TIMERS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_BCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x000005c0)
#define HWIO_GCC_BLSP1_BCR_RMSK                                                                              0x1
#define HWIO_GCC_BLSP1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_BCR_ADDR, HWIO_GCC_BLSP1_BCR_RMSK)
#define HWIO_GCC_BLSP1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_BCR_IN)
#define HWIO_GCC_BLSP1_BCR_BLK_ARES_BMSK                                                                     0x1
#define HWIO_GCC_BLSP1_BCR_BLK_ARES_SHFT                                                                     0x0

#define HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x000005c8)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_RMSK                                                                0x80000000
#define HWIO_GCC_BLSP1_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR, HWIO_GCC_BLSP1_SLEEP_CBCR_RMSK)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_BLSP1_SLEEP_CBCR_CLK_OFF_SHFT                                                              0x1f

#define HWIO_GCC_BLSP1_AHB_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000005c4)
#define HWIO_GCC_BLSP1_AHB_CBCR_RMSK                                                                  0xf000fff0
#define HWIO_GCC_BLSP1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_AHB_CBCR_ADDR, HWIO_GCC_BLSP1_AHB_CBCR_RMSK)
#define HWIO_GCC_BLSP1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_AHB_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_AHB_CBCR_IN)
#define HWIO_GCC_BLSP1_AHB_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_BLSP1_AHB_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                         0x70000000
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                               0x1c
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                 0x8000
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                    0xf
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                    0x4000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                       0xe
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                  0x2000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                     0xd
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                 0x1000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                    0xc
#define HWIO_GCC_BLSP1_AHB_CBCR_WAKEUP_BMSK                                                                0xf00
#define HWIO_GCC_BLSP1_AHB_CBCR_WAKEUP_SHFT                                                                  0x8
#define HWIO_GCC_BLSP1_AHB_CBCR_SLEEP_BMSK                                                                  0xf0
#define HWIO_GCC_BLSP1_AHB_CBCR_SLEEP_SHFT                                                                   0x4

#define HWIO_GCC_BLSP1_QUP1_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000640)
#define HWIO_GCC_BLSP1_QUP1_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_BCR_ADDR, HWIO_GCC_BLSP1_QUP1_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_BCR_IN)
#define HWIO_GCC_BLSP1_QUP1_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP1_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000644)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000648)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000064c)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000650)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000654)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000658)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000065c)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART1_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000680)
#define HWIO_GCC_BLSP1_UART1_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_BCR_ADDR, HWIO_GCC_BLSP1_UART1_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_BCR_IN)
#define HWIO_GCC_BLSP1_UART1_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART1_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000684)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000688)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART1_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000068c)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000690)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART1_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000694)
#define HWIO_GCC_BLSP1_UART1_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR, HWIO_GCC_BLSP1_UART1_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART1_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000698)
#define HWIO_GCC_BLSP1_UART1_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR, HWIO_GCC_BLSP1_UART1_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART1_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000069c)
#define HWIO_GCC_BLSP1_UART1_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR, HWIO_GCC_BLSP1_UART1_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_QUP2_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000006c0)
#define HWIO_GCC_BLSP1_QUP2_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP2_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_BCR_ADDR, HWIO_GCC_BLSP1_QUP2_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_BCR_IN)
#define HWIO_GCC_BLSP1_QUP2_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP2_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000006c4)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000006c8)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000006cc)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000006d0)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000006d4)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000006d8)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000006dc)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART2_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000700)
#define HWIO_GCC_BLSP1_UART2_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART2_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_BCR_ADDR, HWIO_GCC_BLSP1_UART2_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_BCR_IN)
#define HWIO_GCC_BLSP1_UART2_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART2_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000704)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000708)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART2_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000070c)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000710)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART2_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000714)
#define HWIO_GCC_BLSP1_UART2_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR, HWIO_GCC_BLSP1_UART2_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART2_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000718)
#define HWIO_GCC_BLSP1_UART2_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR, HWIO_GCC_BLSP1_UART2_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART2_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000071c)
#define HWIO_GCC_BLSP1_UART2_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR, HWIO_GCC_BLSP1_UART2_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_QUP3_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000740)
#define HWIO_GCC_BLSP1_QUP3_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP3_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_BCR_ADDR, HWIO_GCC_BLSP1_QUP3_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_BCR_IN)
#define HWIO_GCC_BLSP1_QUP3_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP3_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000744)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000748)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000074c)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000750)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000754)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000758)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000075c)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART3_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000780)
#define HWIO_GCC_BLSP1_UART3_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART3_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_BCR_ADDR, HWIO_GCC_BLSP1_UART3_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_BCR_IN)
#define HWIO_GCC_BLSP1_UART3_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART3_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000784)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000788)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART3_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000078c)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000790)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART3_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000794)
#define HWIO_GCC_BLSP1_UART3_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR, HWIO_GCC_BLSP1_UART3_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART3_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000798)
#define HWIO_GCC_BLSP1_UART3_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR, HWIO_GCC_BLSP1_UART3_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART3_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000079c)
#define HWIO_GCC_BLSP1_UART3_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR, HWIO_GCC_BLSP1_UART3_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_QUP4_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000007c0)
#define HWIO_GCC_BLSP1_QUP4_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP4_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_BCR_ADDR, HWIO_GCC_BLSP1_QUP4_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_BCR_IN)
#define HWIO_GCC_BLSP1_QUP4_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP4_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000007c4)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000007c8)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000007cc)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000007d0)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000007d4)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000007d8)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000007dc)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART4_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000800)
#define HWIO_GCC_BLSP1_UART4_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART4_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_BCR_ADDR, HWIO_GCC_BLSP1_UART4_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_BCR_IN)
#define HWIO_GCC_BLSP1_UART4_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART4_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000804)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000808)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART4_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000080c)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000810)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART4_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000814)
#define HWIO_GCC_BLSP1_UART4_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR, HWIO_GCC_BLSP1_UART4_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART4_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000818)
#define HWIO_GCC_BLSP1_UART4_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR, HWIO_GCC_BLSP1_UART4_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART4_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000081c)
#define HWIO_GCC_BLSP1_UART4_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR, HWIO_GCC_BLSP1_UART4_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_QUP5_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000840)
#define HWIO_GCC_BLSP1_QUP5_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP5_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_BCR_ADDR, HWIO_GCC_BLSP1_QUP5_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP5_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_BCR_IN)
#define HWIO_GCC_BLSP1_QUP5_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP5_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000844)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000848)
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP5_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000084c)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000850)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000854)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000858)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000085c)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP5_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART5_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000880)
#define HWIO_GCC_BLSP1_UART5_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART5_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_BCR_ADDR, HWIO_GCC_BLSP1_UART5_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART5_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART5_BCR_IN)
#define HWIO_GCC_BLSP1_UART5_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART5_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000884)
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART5_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART5_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000888)
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART5_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART5_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART5_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000088c)
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART5_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000890)
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART5_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART5_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000894)
#define HWIO_GCC_BLSP1_UART5_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_M_ADDR, HWIO_GCC_BLSP1_UART5_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART5_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000898)
#define HWIO_GCC_BLSP1_UART5_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_N_ADDR, HWIO_GCC_BLSP1_UART5_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART5_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000089c)
#define HWIO_GCC_BLSP1_UART5_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_D_ADDR, HWIO_GCC_BLSP1_UART5_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART5_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART5_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART5_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART5_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART5_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART5_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART5_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART5_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART5_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_QUP6_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x000008c0)
#define HWIO_GCC_BLSP1_QUP6_BCR_RMSK                                                                         0x1
#define HWIO_GCC_BLSP1_QUP6_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_BCR_ADDR, HWIO_GCC_BLSP1_QUP6_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP6_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_BCR_IN)
#define HWIO_GCC_BLSP1_QUP6_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_BLSP1_QUP6_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000008c4)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x000008c8)
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP6_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000008cc)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_RMSK                                                    0x800000f3
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                                  0x80
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                                   0x7
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                                  0x40
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                                   0x6
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                                  0x20
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                                   0x5
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000008d0)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_RMSK                                                        0x371f
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_MODE_BMSK                                                   0x3000
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_MODE_SHFT                                                      0xc
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000008d4)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_M_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_M_M_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000008d8)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_N_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_N_N_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000008dc)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_RMSK                                                                 0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_D_BMSK                                                               0xff
#define HWIO_GCC_BLSP1_QUP6_SPI_APPS_D_D_SHFT                                                                0x0

#define HWIO_GCC_BLSP1_UART6_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000900)
#define HWIO_GCC_BLSP1_UART6_BCR_RMSK                                                                        0x1
#define HWIO_GCC_BLSP1_UART6_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_BCR_ADDR, HWIO_GCC_BLSP1_UART6_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART6_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART6_BCR_IN)
#define HWIO_GCC_BLSP1_UART6_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_BLSP1_UART6_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00000904)
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART6_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART6_APPS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00000908)
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART6_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART6_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BLSP1_UART6_SIM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000090c)
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_BLSP1_UART6_APPS_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000910)
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_BLSP1_UART6_APPS_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_BLSP1_UART6_APPS_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000914)
#define HWIO_GCC_BLSP1_UART6_APPS_M_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_M_ADDR, HWIO_GCC_BLSP1_UART6_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_M_M_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_M_M_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART6_APPS_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000918)
#define HWIO_GCC_BLSP1_UART6_APPS_N_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_N_ADDR, HWIO_GCC_BLSP1_UART6_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_N_N_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_N_N_SHFT                                                                   0x0

#define HWIO_GCC_BLSP1_UART6_APPS_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000091c)
#define HWIO_GCC_BLSP1_UART6_APPS_D_RMSK                                                                  0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_D_ADDR, HWIO_GCC_BLSP1_UART6_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART6_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART6_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART6_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART6_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART6_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART6_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART6_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART6_APPS_D_D_BMSK                                                                0xffff
#define HWIO_GCC_BLSP1_UART6_APPS_D_D_SHFT                                                                   0x0

#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000600)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_RMSK                                                          0x80000013
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR, HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_IN)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_OFF_SHFT                                                       0x1f
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                 0x10
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                  0x4
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_EN_BMSK                                                         0x2
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_EN_SHFT                                                         0x1
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_UPDATE_BMSK                                                          0x1
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_UPDATE_SHFT                                                          0x0

#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00000604)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_RMSK                                                                0x1f
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR, HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_IN)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_SRC_DIV_BMSK                                                        0x1f
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_SRC_DIV_SHFT                                                         0x0

#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000c80)
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_RMSK                                                             0xf0008001
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_XPU_AHB_CBCR_ADDR, HWIO_GCC_PERIPH_XPU_AHB_CBCR_RMSK)
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_XPU_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_XPU_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_XPU_AHB_CBCR_ADDR,m,v,HWIO_GCC_PERIPH_XPU_AHB_CBCR_IN)
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                    0x70000000
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                          0x1c
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                            0x8000
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                               0xf
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_PERIPH_XPU_AHB_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_PDM_BCR_ADDR                                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00000cc0)
#define HWIO_GCC_PDM_BCR_RMSK                                                                                0x1
#define HWIO_GCC_PDM_BCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_BCR_ADDR, HWIO_GCC_PDM_BCR_RMSK)
#define HWIO_GCC_PDM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_BCR_ADDR, m)
#define HWIO_GCC_PDM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_BCR_ADDR,v)
#define HWIO_GCC_PDM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_BCR_ADDR,m,v,HWIO_GCC_PDM_BCR_IN)
#define HWIO_GCC_PDM_BCR_BLK_ARES_BMSK                                                                       0x1
#define HWIO_GCC_PDM_BCR_BLK_ARES_SHFT                                                                       0x0

#define HWIO_GCC_PDM_AHB_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000cc4)
#define HWIO_GCC_PDM_AHB_CBCR_RMSK                                                                    0xf0008001
#define HWIO_GCC_PDM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_AHB_CBCR_ADDR, HWIO_GCC_PDM_AHB_CBCR_RMSK)
#define HWIO_GCC_PDM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PDM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PDM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_AHB_CBCR_ADDR,m,v,HWIO_GCC_PDM_AHB_CBCR_IN)
#define HWIO_GCC_PDM_AHB_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_PDM_AHB_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                           0x70000000
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                 0x1c
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                   0x8000
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                      0xf
#define HWIO_GCC_PDM_AHB_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_PDM_AHB_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_PDM_XO4_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000cc8)
#define HWIO_GCC_PDM_XO4_CBCR_RMSK                                                                    0x80030001
#define HWIO_GCC_PDM_XO4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_XO4_CBCR_ADDR, HWIO_GCC_PDM_XO4_CBCR_RMSK)
#define HWIO_GCC_PDM_XO4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_XO4_CBCR_ADDR, m)
#define HWIO_GCC_PDM_XO4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_XO4_CBCR_ADDR,v)
#define HWIO_GCC_PDM_XO4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_XO4_CBCR_ADDR,m,v,HWIO_GCC_PDM_XO4_CBCR_IN)
#define HWIO_GCC_PDM_XO4_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_PDM_XO4_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_PDM_XO4_CBCR_CLK_DIV_BMSK                                                               0x30000
#define HWIO_GCC_PDM_XO4_CBCR_CLK_DIV_SHFT                                                                  0x10
#define HWIO_GCC_PDM_XO4_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_PDM_XO4_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_PDM2_CBCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000ccc)
#define HWIO_GCC_PDM2_CBCR_RMSK                                                                       0x80000001
#define HWIO_GCC_PDM2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CBCR_ADDR, HWIO_GCC_PDM2_CBCR_RMSK)
#define HWIO_GCC_PDM2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CBCR_ADDR, m)
#define HWIO_GCC_PDM2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CBCR_ADDR,v)
#define HWIO_GCC_PDM2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CBCR_ADDR,m,v,HWIO_GCC_PDM2_CBCR_IN)
#define HWIO_GCC_PDM2_CBCR_CLK_OFF_BMSK                                                               0x80000000
#define HWIO_GCC_PDM2_CBCR_CLK_OFF_SHFT                                                                     0x1f
#define HWIO_GCC_PDM2_CBCR_CLK_ENABLE_BMSK                                                                   0x1
#define HWIO_GCC_PDM2_CBCR_CLK_ENABLE_SHFT                                                                   0x0

#define HWIO_GCC_PDM2_CMD_RCGR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000cd0)
#define HWIO_GCC_PDM2_CMD_RCGR_RMSK                                                                   0x80000013
#define HWIO_GCC_PDM2_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CMD_RCGR_ADDR, HWIO_GCC_PDM2_CMD_RCGR_RMSK)
#define HWIO_GCC_PDM2_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PDM2_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PDM2_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CMD_RCGR_ADDR,m,v,HWIO_GCC_PDM2_CMD_RCGR_IN)
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_OFF_SHFT                                                                0x1f
#define HWIO_GCC_PDM2_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                          0x10
#define HWIO_GCC_PDM2_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                           0x4
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_EN_BMSK                                                                  0x2
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_EN_SHFT                                                                  0x1
#define HWIO_GCC_PDM2_CMD_RCGR_UPDATE_BMSK                                                                   0x1
#define HWIO_GCC_PDM2_CMD_RCGR_UPDATE_SHFT                                                                   0x0

#define HWIO_GCC_PDM2_CFG_RCGR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000cd4)
#define HWIO_GCC_PDM2_CFG_RCGR_RMSK                                                                        0x71f
#define HWIO_GCC_PDM2_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CFG_RCGR_ADDR, HWIO_GCC_PDM2_CFG_RCGR_RMSK)
#define HWIO_GCC_PDM2_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PDM2_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PDM2_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CFG_RCGR_ADDR,m,v,HWIO_GCC_PDM2_CFG_RCGR_IN)
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_SEL_BMSK                                                                0x700
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_SEL_SHFT                                                                  0x8
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_DIV_BMSK                                                                 0x1f
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_DIV_SHFT                                                                  0x0

#define HWIO_GCC_PRNG_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000d00)
#define HWIO_GCC_PRNG_BCR_RMSK                                                                               0x1
#define HWIO_GCC_PRNG_BCR_IN          \
        in_dword_masked(HWIO_GCC_PRNG_BCR_ADDR, HWIO_GCC_PRNG_BCR_RMSK)
#define HWIO_GCC_PRNG_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PRNG_BCR_ADDR, m)
#define HWIO_GCC_PRNG_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PRNG_BCR_ADDR,v)
#define HWIO_GCC_PRNG_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PRNG_BCR_ADDR,m,v,HWIO_GCC_PRNG_BCR_IN)
#define HWIO_GCC_PRNG_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_PRNG_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_PRNG_AHB_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000d04)
#define HWIO_GCC_PRNG_AHB_CBCR_RMSK                                                                   0xf0008000
#define HWIO_GCC_PRNG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PRNG_AHB_CBCR_ADDR, HWIO_GCC_PRNG_AHB_CBCR_RMSK)
#define HWIO_GCC_PRNG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PRNG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PRNG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PRNG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PRNG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PRNG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PRNG_AHB_CBCR_IN)
#define HWIO_GCC_PRNG_AHB_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_PRNG_AHB_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf

#define HWIO_GCC_BAM_DMA_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000d40)
#define HWIO_GCC_BAM_DMA_BCR_RMSK                                                                            0x1
#define HWIO_GCC_BAM_DMA_BCR_IN          \
        in_dword_masked(HWIO_GCC_BAM_DMA_BCR_ADDR, HWIO_GCC_BAM_DMA_BCR_RMSK)
#define HWIO_GCC_BAM_DMA_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BAM_DMA_BCR_ADDR, m)
#define HWIO_GCC_BAM_DMA_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BAM_DMA_BCR_ADDR,v)
#define HWIO_GCC_BAM_DMA_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BAM_DMA_BCR_ADDR,m,v,HWIO_GCC_BAM_DMA_BCR_IN)
#define HWIO_GCC_BAM_DMA_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_BAM_DMA_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_BAM_DMA_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000d44)
#define HWIO_GCC_BAM_DMA_AHB_CBCR_RMSK                                                                0xf000fff0
#define HWIO_GCC_BAM_DMA_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BAM_DMA_AHB_CBCR_ADDR, HWIO_GCC_BAM_DMA_AHB_CBCR_RMSK)
#define HWIO_GCC_BAM_DMA_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BAM_DMA_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BAM_DMA_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BAM_DMA_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BAM_DMA_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BAM_DMA_AHB_CBCR_ADDR,m,v,HWIO_GCC_BAM_DMA_AHB_CBCR_IN)
#define HWIO_GCC_BAM_DMA_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_BAM_DMA_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_BAM_DMA_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                  0x4000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                     0xe
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                0x2000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                   0xd
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                               0x1000
#define HWIO_GCC_BAM_DMA_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                  0xc
#define HWIO_GCC_BAM_DMA_AHB_CBCR_WAKEUP_BMSK                                                              0xf00
#define HWIO_GCC_BAM_DMA_AHB_CBCR_WAKEUP_SHFT                                                                0x8
#define HWIO_GCC_BAM_DMA_AHB_CBCR_SLEEP_BMSK                                                                0xf0
#define HWIO_GCC_BAM_DMA_AHB_CBCR_SLEEP_SHFT                                                                 0x4

#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000d48)
#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_RMSK                                                  0x80000000
#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_ADDR, HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_RMSK)
#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_ADDR, m)
#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_CLK_OFF_BMSK                                          0x80000000
#define HWIO_GCC_BAM_DMA_INACTIVITY_TIMERS_CBCR_CLK_OFF_SHFT                                                0x1f

#define HWIO_GCC_TCSR_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000dc0)
#define HWIO_GCC_TCSR_BCR_RMSK                                                                               0x1
#define HWIO_GCC_TCSR_BCR_IN          \
        in_dword_masked(HWIO_GCC_TCSR_BCR_ADDR, HWIO_GCC_TCSR_BCR_RMSK)
#define HWIO_GCC_TCSR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TCSR_BCR_ADDR, m)
#define HWIO_GCC_TCSR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_TCSR_BCR_ADDR,v)
#define HWIO_GCC_TCSR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TCSR_BCR_ADDR,m,v,HWIO_GCC_TCSR_BCR_IN)
#define HWIO_GCC_TCSR_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_TCSR_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_TCSR_AHB_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000dc4)
#define HWIO_GCC_TCSR_AHB_CBCR_RMSK                                                                   0xf0008001
#define HWIO_GCC_TCSR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TCSR_AHB_CBCR_ADDR, HWIO_GCC_TCSR_AHB_CBCR_RMSK)
#define HWIO_GCC_TCSR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TCSR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_TCSR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_TCSR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_TCSR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TCSR_AHB_CBCR_ADDR,m,v,HWIO_GCC_TCSR_AHB_CBCR_IN)
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_BOOT_ROM_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e00)
#define HWIO_GCC_BOOT_ROM_BCR_RMSK                                                                           0x1
#define HWIO_GCC_BOOT_ROM_BCR_IN          \
        in_dword_masked(HWIO_GCC_BOOT_ROM_BCR_ADDR, HWIO_GCC_BOOT_ROM_BCR_RMSK)
#define HWIO_GCC_BOOT_ROM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BOOT_ROM_BCR_ADDR, m)
#define HWIO_GCC_BOOT_ROM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BOOT_ROM_BCR_ADDR,v)
#define HWIO_GCC_BOOT_ROM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BOOT_ROM_BCR_ADDR,m,v,HWIO_GCC_BOOT_ROM_BCR_IN)
#define HWIO_GCC_BOOT_ROM_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_BOOT_ROM_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e04)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_RMSK                                                               0xf000fff0
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR, HWIO_GCC_BOOT_ROM_AHB_CBCR_RMSK)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR,m,v,HWIO_GCC_BOOT_ROM_AHB_CBCR_IN)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_SLEEP_SHFT                                                                0x4

#define HWIO_GCC_MSG_RAM_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e40)
#define HWIO_GCC_MSG_RAM_BCR_RMSK                                                                            0x1
#define HWIO_GCC_MSG_RAM_BCR_IN          \
        in_dword_masked(HWIO_GCC_MSG_RAM_BCR_ADDR, HWIO_GCC_MSG_RAM_BCR_RMSK)
#define HWIO_GCC_MSG_RAM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSG_RAM_BCR_ADDR, m)
#define HWIO_GCC_MSG_RAM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MSG_RAM_BCR_ADDR,v)
#define HWIO_GCC_MSG_RAM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSG_RAM_BCR_ADDR,m,v,HWIO_GCC_MSG_RAM_BCR_IN)
#define HWIO_GCC_MSG_RAM_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_MSG_RAM_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e44)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_RMSK                                                                0xf000fff0
#define HWIO_GCC_MSG_RAM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR, HWIO_GCC_MSG_RAM_AHB_CBCR_RMSK)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR,m,v,HWIO_GCC_MSG_RAM_AHB_CBCR_IN)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                  0x4000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                     0xe
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                0x2000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                   0xd
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                               0x1000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                  0xc
#define HWIO_GCC_MSG_RAM_AHB_CBCR_WAKEUP_BMSK                                                              0xf00
#define HWIO_GCC_MSG_RAM_AHB_CBCR_WAKEUP_SHFT                                                                0x8
#define HWIO_GCC_MSG_RAM_AHB_CBCR_SLEEP_BMSK                                                                0xf0
#define HWIO_GCC_MSG_RAM_AHB_CBCR_SLEEP_SHFT                                                                 0x4

#define HWIO_GCC_TLMM_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e80)
#define HWIO_GCC_TLMM_BCR_RMSK                                                                               0x1
#define HWIO_GCC_TLMM_BCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_BCR_ADDR, HWIO_GCC_TLMM_BCR_RMSK)
#define HWIO_GCC_TLMM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_BCR_ADDR, m)
#define HWIO_GCC_TLMM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_TLMM_BCR_ADDR,v)
#define HWIO_GCC_TLMM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TLMM_BCR_ADDR,m,v,HWIO_GCC_TLMM_BCR_IN)
#define HWIO_GCC_TLMM_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_TLMM_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_TLMM_AHB_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e84)
#define HWIO_GCC_TLMM_AHB_CBCR_RMSK                                                                   0xf0008000
#define HWIO_GCC_TLMM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_AHB_CBCR_ADDR, HWIO_GCC_TLMM_AHB_CBCR_RMSK)
#define HWIO_GCC_TLMM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_TLMM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_TLMM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_TLMM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TLMM_AHB_CBCR_ADDR,m,v,HWIO_GCC_TLMM_AHB_CBCR_IN)
#define HWIO_GCC_TLMM_AHB_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_TLMM_AHB_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf

#define HWIO_GCC_TLMM_CBCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00000e88)
#define HWIO_GCC_TLMM_CBCR_RMSK                                                                       0x80000000
#define HWIO_GCC_TLMM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_CBCR_ADDR, HWIO_GCC_TLMM_CBCR_RMSK)
#define HWIO_GCC_TLMM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_CBCR_ADDR, m)
#define HWIO_GCC_TLMM_CBCR_CLK_OFF_BMSK                                                               0x80000000
#define HWIO_GCC_TLMM_CBCR_CLK_OFF_SHFT                                                                     0x1f

#define HWIO_GCC_MPM_BCR_ADDR                                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00000ec0)
#define HWIO_GCC_MPM_BCR_RMSK                                                                                0x1
#define HWIO_GCC_MPM_BCR_IN          \
        in_dword_masked(HWIO_GCC_MPM_BCR_ADDR, HWIO_GCC_MPM_BCR_RMSK)
#define HWIO_GCC_MPM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_BCR_ADDR, m)
#define HWIO_GCC_MPM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MPM_BCR_ADDR,v)
#define HWIO_GCC_MPM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_BCR_ADDR,m,v,HWIO_GCC_MPM_BCR_IN)
#define HWIO_GCC_MPM_BCR_BLK_ARES_BMSK                                                                       0x1
#define HWIO_GCC_MPM_BCR_BLK_ARES_SHFT                                                                       0x0

#define HWIO_GCC_MPM_MISC_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000ec4)
#define HWIO_GCC_MPM_MISC_RMSK                                                                               0x7
#define HWIO_GCC_MPM_MISC_IN          \
        in_dword_masked(HWIO_GCC_MPM_MISC_ADDR, HWIO_GCC_MPM_MISC_RMSK)
#define HWIO_GCC_MPM_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_MISC_ADDR, m)
#define HWIO_GCC_MPM_MISC_OUT(v)      \
        out_dword(HWIO_GCC_MPM_MISC_ADDR,v)
#define HWIO_GCC_MPM_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_MISC_ADDR,m,v,HWIO_GCC_MPM_MISC_IN)
#define HWIO_GCC_MPM_MISC_MPM_NON_AHB_RESET_BMSK                                                             0x4
#define HWIO_GCC_MPM_MISC_MPM_NON_AHB_RESET_SHFT                                                             0x2
#define HWIO_GCC_MPM_MISC_MPM_AHB_RESET_BMSK                                                                 0x2
#define HWIO_GCC_MPM_MISC_MPM_AHB_RESET_SHFT                                                                 0x1
#define HWIO_GCC_MPM_MISC_MPM_REF_CLK_EN_BMSK                                                                0x1
#define HWIO_GCC_MPM_MISC_MPM_REF_CLK_EN_SHFT                                                                0x0

#define HWIO_GCC_MPM_AHB_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000ec8)
#define HWIO_GCC_MPM_AHB_CBCR_RMSK                                                                    0xf0008000
#define HWIO_GCC_MPM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MPM_AHB_CBCR_ADDR, HWIO_GCC_MPM_AHB_CBCR_RMSK)
#define HWIO_GCC_MPM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MPM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MPM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MPM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_AHB_CBCR_ADDR,m,v,HWIO_GCC_MPM_AHB_CBCR_IN)
#define HWIO_GCC_MPM_AHB_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_MPM_AHB_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                           0x70000000
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                 0x1c
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                   0x8000
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                      0xf

#define HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f00)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_RMSK                                                              0x80000000
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR, HWIO_GCC_RPM_PROC_HCLK_CBCR_RMSK)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR, m)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_CLK_OFF_SHFT                                                            0x1f

#define HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f04)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_RMSK                                                                0xf000fff0
#define HWIO_GCC_RPM_BUS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR, HWIO_GCC_RPM_BUS_AHB_CBCR_RMSK)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR,v)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR,m,v,HWIO_GCC_RPM_BUS_AHB_CBCR_IN)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                  0x4000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                     0xe
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                0x2000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                   0xd
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                               0x1000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                  0xc
#define HWIO_GCC_RPM_BUS_AHB_CBCR_WAKEUP_BMSK                                                              0xf00
#define HWIO_GCC_RPM_BUS_AHB_CBCR_WAKEUP_SHFT                                                                0x8
#define HWIO_GCC_RPM_BUS_AHB_CBCR_SLEEP_BMSK                                                                0xf0
#define HWIO_GCC_RPM_BUS_AHB_CBCR_SLEEP_SHFT                                                                 0x4

#define HWIO_GCC_RPM_SLEEP_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f08)
#define HWIO_GCC_RPM_SLEEP_CBCR_RMSK                                                                  0x80000001
#define HWIO_GCC_RPM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_SLEEP_CBCR_ADDR, HWIO_GCC_RPM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_RPM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_RPM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_RPM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_RPM_SLEEP_CBCR_IN)
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_RPM_TIMER_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f0c)
#define HWIO_GCC_RPM_TIMER_CBCR_RMSK                                                                  0x80000003
#define HWIO_GCC_RPM_TIMER_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_TIMER_CBCR_ADDR, HWIO_GCC_RPM_TIMER_CBCR_RMSK)
#define HWIO_GCC_RPM_TIMER_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_TIMER_CBCR_ADDR, m)
#define HWIO_GCC_RPM_TIMER_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_TIMER_CBCR_ADDR,v)
#define HWIO_GCC_RPM_TIMER_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_TIMER_CBCR_ADDR,m,v,HWIO_GCC_RPM_TIMER_CBCR_IN)
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_RPM_TIMER_CBCR_HW_CTL_BMSK                                                                  0x2
#define HWIO_GCC_RPM_TIMER_CBCR_HW_CTL_SHFT                                                                  0x1
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_RPM_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f10)
#define HWIO_GCC_RPM_CMD_RCGR_RMSK                                                                    0x80000013
#define HWIO_GCC_RPM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RPM_CMD_RCGR_ADDR, HWIO_GCC_RPM_CMD_RCGR_RMSK)
#define HWIO_GCC_RPM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_RPM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_RPM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CMD_RCGR_ADDR,m,v,HWIO_GCC_RPM_CMD_RCGR_IN)
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_RPM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_RPM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_RPM_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_RPM_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_RPM_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f14)
#define HWIO_GCC_RPM_CFG_RCGR_RMSK                                                                         0x71f
#define HWIO_GCC_RPM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RPM_CFG_RCGR_ADDR, HWIO_GCC_RPM_CFG_RCGR_RMSK)
#define HWIO_GCC_RPM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_RPM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_RPM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CFG_RCGR_ADDR,m,v,HWIO_GCC_RPM_CFG_RCGR_IN)
#define HWIO_GCC_RPM_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_RPM_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_RPM_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_RPM_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_RPM_MISC_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f24)
#define HWIO_GCC_RPM_MISC_RMSK                                                                              0xf1
#define HWIO_GCC_RPM_MISC_IN          \
        in_dword_masked(HWIO_GCC_RPM_MISC_ADDR, HWIO_GCC_RPM_MISC_RMSK)
#define HWIO_GCC_RPM_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_MISC_ADDR, m)
#define HWIO_GCC_RPM_MISC_OUT(v)      \
        out_dword(HWIO_GCC_RPM_MISC_ADDR,v)
#define HWIO_GCC_RPM_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_MISC_ADDR,m,v,HWIO_GCC_RPM_MISC_IN)
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIV_BMSK                                                       0xf0
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIV_SHFT                                                        0x4
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIS_BMSK                                                        0x1
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIS_SHFT                                                        0x0

#define HWIO_GCC_SEC_CTRL_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f40)
#define HWIO_GCC_SEC_CTRL_BCR_RMSK                                                                           0x1
#define HWIO_GCC_SEC_CTRL_BCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BCR_ADDR, HWIO_GCC_SEC_CTRL_BCR_RMSK)
#define HWIO_GCC_SEC_CTRL_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_BCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_BCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_BCR_IN)
#define HWIO_GCC_SEC_CTRL_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_SEC_CTRL_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_ACC_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f80)
#define HWIO_GCC_ACC_CMD_RCGR_RMSK                                                                    0x80000013
#define HWIO_GCC_ACC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ACC_CMD_RCGR_ADDR, HWIO_GCC_ACC_CMD_RCGR_RMSK)
#define HWIO_GCC_ACC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ACC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ACC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ACC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_CMD_RCGR_ADDR,m,v,HWIO_GCC_ACC_CMD_RCGR_IN)
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_ACC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_ACC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_ACC_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_ACC_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_ACC_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f84)
#define HWIO_GCC_ACC_CFG_RCGR_RMSK                                                                         0x71f
#define HWIO_GCC_ACC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ACC_CFG_RCGR_ADDR, HWIO_GCC_ACC_CFG_RCGR_RMSK)
#define HWIO_GCC_ACC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ACC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ACC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ACC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_CFG_RCGR_ADDR,m,v,HWIO_GCC_ACC_CFG_RCGR_IN)
#define HWIO_GCC_ACC_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_ACC_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_ACC_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_ACC_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_ACC_MISC_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f94)
#define HWIO_GCC_ACC_MISC_RMSK                                                                               0x1
#define HWIO_GCC_ACC_MISC_IN          \
        in_dword_masked(HWIO_GCC_ACC_MISC_ADDR, HWIO_GCC_ACC_MISC_RMSK)
#define HWIO_GCC_ACC_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_MISC_ADDR, m)
#define HWIO_GCC_ACC_MISC_OUT(v)      \
        out_dword(HWIO_GCC_ACC_MISC_ADDR,v)
#define HWIO_GCC_ACC_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_MISC_ADDR,m,v,HWIO_GCC_ACC_MISC_IN)
#define HWIO_GCC_ACC_MISC_JTAG_ACC_SRC_SEL_EN_BMSK                                                           0x1
#define HWIO_GCC_ACC_MISC_JTAG_ACC_SRC_SEL_EN_SHFT                                                           0x0

#define HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f44)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_RMSK                                                               0x80007ff1
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR, HWIO_GCC_SEC_CTRL_ACC_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_ACC_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f48)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR, HWIO_GCC_SEC_CTRL_AHB_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_AHB_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SEC_CTRL_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f4c)
#define HWIO_GCC_SEC_CTRL_CBCR_RMSK                                                                   0x80007ff1
#define HWIO_GCC_SEC_CTRL_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CBCR_ADDR, HWIO_GCC_SEC_CTRL_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_SEC_CTRL_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_SEC_CTRL_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_SEC_CTRL_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_SEC_CTRL_CBCR_SLEEP_SHFT                                                                    0x4
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f50)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR, HWIO_GCC_SEC_CTRL_SENSE_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_SENSE_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f54)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR, HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f98)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR, HWIO_GCC_SEC_CTRL_CMD_RCGR_RMSK)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CMD_RCGR_IN)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000f9c)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR, HWIO_GCC_SEC_CTRL_CFG_RCGR_RMSK)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CFG_RCGR_IN)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_SPMI_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fc0)
#define HWIO_GCC_SPMI_BCR_RMSK                                                                               0x1
#define HWIO_GCC_SPMI_BCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_BCR_ADDR, HWIO_GCC_SPMI_BCR_RMSK)
#define HWIO_GCC_SPMI_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_BCR_ADDR, m)
#define HWIO_GCC_SPMI_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_BCR_ADDR,v)
#define HWIO_GCC_SPMI_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_BCR_ADDR,m,v,HWIO_GCC_SPMI_BCR_IN)
#define HWIO_GCC_SPMI_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_SPMI_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fd0)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_SPMI_SER_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR, HWIO_GCC_SPMI_SER_CMD_RCGR_RMSK)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR,m,v,HWIO_GCC_SPMI_SER_CMD_RCGR_IN)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SPMI_SER_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_SPMI_SER_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_SPMI_SER_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_SPMI_SER_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fd4)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_SPMI_SER_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR, HWIO_GCC_SPMI_SER_CFG_RCGR_RMSK)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR,m,v,HWIO_GCC_SPMI_SER_CFG_RCGR_IN)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_SPMI_SER_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fc4)
#define HWIO_GCC_SPMI_SER_CBCR_RMSK                                                                   0x80007ff1
#define HWIO_GCC_SPMI_SER_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CBCR_ADDR, HWIO_GCC_SPMI_SER_CBCR_RMSK)
#define HWIO_GCC_SPMI_SER_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CBCR_ADDR,m,v,HWIO_GCC_SPMI_SER_CBCR_IN)
#define HWIO_GCC_SPMI_SER_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_SPMI_SER_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_SPMI_SER_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_SPMI_SER_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_SPMI_SER_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_SPMI_SER_CBCR_SLEEP_SHFT                                                                    0x4
#define HWIO_GCC_SPMI_SER_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_SPMI_SER_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fc8)
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_RMSK                                                              0xf0008000
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_CNOC_AHB_CBCR_ADDR, HWIO_GCC_SPMI_CNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_CNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_CNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_CNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPMI_CNOC_AHB_CBCR_IN)
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                     0x70000000
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                           0x1c
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                             0x8000
#define HWIO_GCC_SPMI_CNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                0xf

#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fe8)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR, HWIO_GCC_SPMI_AHB_CMD_RCGR_RMSK)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CMD_RCGR_IN)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fec)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR, HWIO_GCC_SPMI_AHB_CFG_RCGR_RMSK)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CFG_RCGR_IN)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_SPMI_AHB_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00000fcc)
#define HWIO_GCC_SPMI_AHB_CBCR_RMSK                                                                   0x80007ff1
#define HWIO_GCC_SPMI_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CBCR_ADDR, HWIO_GCC_SPMI_AHB_CBCR_RMSK)
#define HWIO_GCC_SPMI_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CBCR_IN)
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_SPMI_AHB_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_SPMI_AHB_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_SPMI_AHB_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_SPMI_AHB_CBCR_SLEEP_SHFT                                                                    0x4
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_SPDM_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001000)
#define HWIO_GCC_SPDM_BCR_RMSK                                                                               0x1
#define HWIO_GCC_SPDM_BCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_BCR_ADDR, HWIO_GCC_SPDM_BCR_RMSK)
#define HWIO_GCC_SPDM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_BCR_ADDR, m)
#define HWIO_GCC_SPDM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_BCR_ADDR,v)
#define HWIO_GCC_SPDM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_BCR_ADDR,m,v,HWIO_GCC_SPDM_BCR_IN)
#define HWIO_GCC_SPDM_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_SPDM_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001004)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR, HWIO_GCC_SPDM_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPDM_CFG_AHB_CBCR_IN)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001008)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR, HWIO_GCC_SPDM_MSTR_AHB_CBCR_RMSK)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPDM_MSTR_AHB_CBCR_IN)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_SPDM_FF_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000100c)
#define HWIO_GCC_SPDM_FF_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_SPDM_FF_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_FF_CBCR_ADDR, HWIO_GCC_SPDM_FF_CBCR_RMSK)
#define HWIO_GCC_SPDM_FF_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_FF_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_FF_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_FF_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_FF_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_FF_CBCR_ADDR,m,v,HWIO_GCC_SPDM_FF_CBCR_IN)
#define HWIO_GCC_SPDM_FF_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_SPDM_FF_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_SPDM_FF_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_SPDM_FF_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001010)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR, HWIO_GCC_SPDM_BIMC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_BIMC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001014)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR, HWIO_GCC_SPDM_SNOC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_SNOC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001018)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR, HWIO_GCC_SPDM_DEBUG_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_DEBUG_CY_CBCR_IN)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_SPDM_PNOC_CY_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000101c)
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_PNOC_CY_CBCR_ADDR, HWIO_GCC_SPDM_PNOC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_PNOC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_PNOC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_PNOC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_PNOC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPDM_PNOC_CY_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00001020)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_SPDM_RPM_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR, HWIO_GCC_SPDM_RPM_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_RPM_CY_CBCR_IN)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_CE1_BCR_ADDR                                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001040)
#define HWIO_GCC_CE1_BCR_RMSK                                                                                0x1
#define HWIO_GCC_CE1_BCR_IN          \
        in_dword_masked(HWIO_GCC_CE1_BCR_ADDR, HWIO_GCC_CE1_BCR_RMSK)
#define HWIO_GCC_CE1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_BCR_ADDR, m)
#define HWIO_GCC_CE1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CE1_BCR_ADDR,v)
#define HWIO_GCC_CE1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CE1_BCR_ADDR,m,v,HWIO_GCC_CE1_BCR_IN)
#define HWIO_GCC_CE1_BCR_BLK_ARES_BMSK                                                                       0x1
#define HWIO_GCC_CE1_BCR_BLK_ARES_SHFT                                                                       0x0

#define HWIO_GCC_CE1_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001050)
#define HWIO_GCC_CE1_CMD_RCGR_RMSK                                                                    0x80000013
#define HWIO_GCC_CE1_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CE1_CMD_RCGR_ADDR, HWIO_GCC_CE1_CMD_RCGR_RMSK)
#define HWIO_GCC_CE1_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_CMD_RCGR_ADDR, m)
#define HWIO_GCC_CE1_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CE1_CMD_RCGR_ADDR,v)
#define HWIO_GCC_CE1_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CE1_CMD_RCGR_ADDR,m,v,HWIO_GCC_CE1_CMD_RCGR_IN)
#define HWIO_GCC_CE1_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_CE1_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_CE1_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_CE1_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_CE1_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_CE1_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_CE1_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_CE1_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_CE1_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001054)
#define HWIO_GCC_CE1_CFG_RCGR_RMSK                                                                         0x71f
#define HWIO_GCC_CE1_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CE1_CFG_RCGR_ADDR, HWIO_GCC_CE1_CFG_RCGR_RMSK)
#define HWIO_GCC_CE1_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_CFG_RCGR_ADDR, m)
#define HWIO_GCC_CE1_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CE1_CFG_RCGR_ADDR,v)
#define HWIO_GCC_CE1_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CE1_CFG_RCGR_ADDR,m,v,HWIO_GCC_CE1_CFG_RCGR_IN)
#define HWIO_GCC_CE1_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_CE1_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_CE1_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_CE1_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_CE1_CBCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001044)
#define HWIO_GCC_CE1_CBCR_RMSK                                                                        0x80007ff0
#define HWIO_GCC_CE1_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CE1_CBCR_ADDR, HWIO_GCC_CE1_CBCR_RMSK)
#define HWIO_GCC_CE1_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_CBCR_ADDR, m)
#define HWIO_GCC_CE1_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CE1_CBCR_ADDR,v)
#define HWIO_GCC_CE1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CE1_CBCR_ADDR,m,v,HWIO_GCC_CE1_CBCR_IN)
#define HWIO_GCC_CE1_CBCR_CLK_OFF_BMSK                                                                0x80000000
#define HWIO_GCC_CE1_CBCR_CLK_OFF_SHFT                                                                      0x1f
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_CORE_ON_BMSK                                                          0x4000
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_CORE_ON_SHFT                                                             0xe
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                        0x2000
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                           0xd
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                       0x1000
#define HWIO_GCC_CE1_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                          0xc
#define HWIO_GCC_CE1_CBCR_WAKEUP_BMSK                                                                      0xf00
#define HWIO_GCC_CE1_CBCR_WAKEUP_SHFT                                                                        0x8
#define HWIO_GCC_CE1_CBCR_SLEEP_BMSK                                                                        0xf0
#define HWIO_GCC_CE1_CBCR_SLEEP_SHFT                                                                         0x4

#define HWIO_GCC_CE1_AXI_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001048)
#define HWIO_GCC_CE1_AXI_CBCR_RMSK                                                                    0x80000000
#define HWIO_GCC_CE1_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CE1_AXI_CBCR_ADDR, HWIO_GCC_CE1_AXI_CBCR_RMSK)
#define HWIO_GCC_CE1_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_AXI_CBCR_ADDR, m)
#define HWIO_GCC_CE1_AXI_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_CE1_AXI_CBCR_CLK_OFF_SHFT                                                                  0x1f

#define HWIO_GCC_CE1_AHB_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000104c)
#define HWIO_GCC_CE1_AHB_CBCR_RMSK                                                                    0xf0008000
#define HWIO_GCC_CE1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CE1_AHB_CBCR_ADDR, HWIO_GCC_CE1_AHB_CBCR_RMSK)
#define HWIO_GCC_CE1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CE1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CE1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CE1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CE1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CE1_AHB_CBCR_ADDR,m,v,HWIO_GCC_CE1_AHB_CBCR_IN)
#define HWIO_GCC_CE1_AHB_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_CE1_AHB_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_CE1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                           0x70000000
#define HWIO_GCC_CE1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                 0x1c
#define HWIO_GCC_CE1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                   0x8000
#define HWIO_GCC_CE1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                      0xf

#define HWIO_GCC_GCC_AHB_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000010c0)
#define HWIO_GCC_GCC_AHB_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_GCC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_AHB_CBCR_ADDR, HWIO_GCC_GCC_AHB_CBCR_RMSK)
#define HWIO_GCC_GCC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_GCC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_GCC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_AHB_CBCR_ADDR,m,v,HWIO_GCC_GCC_AHB_CBCR_IN)
#define HWIO_GCC_GCC_AHB_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_GCC_AHB_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_GCC_AHB_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_GCC_AHB_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_GCC_XO_CMD_RCGR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x000010e8)
#define HWIO_GCC_GCC_XO_CMD_RCGR_RMSK                                                                 0x80000002
#define HWIO_GCC_GCC_XO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR, HWIO_GCC_GCC_XO_CMD_RCGR_RMSK)
#define HWIO_GCC_GCC_XO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GCC_XO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GCC_XO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR,m,v,HWIO_GCC_GCC_XO_CMD_RCGR_IN)
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_OFF_SHFT                                                              0x1f
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_EN_BMSK                                                                0x2
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_EN_SHFT                                                                0x1

#define HWIO_GCC_GCC_XO_CBCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x000010c4)
#define HWIO_GCC_GCC_XO_CBCR_RMSK                                                                     0x80000001
#define HWIO_GCC_GCC_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_CBCR_ADDR, HWIO_GCC_GCC_XO_CBCR_RMSK)
#define HWIO_GCC_GCC_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_CBCR_ADDR, m)
#define HWIO_GCC_GCC_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_CBCR_ADDR,v)
#define HWIO_GCC_GCC_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_CBCR_ADDR,m,v,HWIO_GCC_GCC_XO_CBCR_IN)
#define HWIO_GCC_GCC_XO_CBCR_CLK_OFF_BMSK                                                             0x80000000
#define HWIO_GCC_GCC_XO_CBCR_CLK_OFF_SHFT                                                                   0x1f
#define HWIO_GCC_GCC_XO_CBCR_CLK_ENABLE_BMSK                                                                 0x1
#define HWIO_GCC_GCC_XO_CBCR_CLK_ENABLE_SHFT                                                                 0x0

#define HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x000010c8)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_GCC_XO_DIV4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR, HWIO_GCC_GCC_XO_DIV4_CBCR_RMSK)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR, m)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR,v)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR,m,v,HWIO_GCC_GCC_XO_DIV4_CBCR_IN)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x000010cc)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, HWIO_GCC_GCC_IM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_GCC_IM_SLEEP_CBCR_IN)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_BIMC_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001100)
#define HWIO_GCC_BIMC_BCR_RMSK                                                                               0x1
#define HWIO_GCC_BIMC_BCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_BCR_ADDR, HWIO_GCC_BIMC_BCR_RMSK)
#define HWIO_GCC_BIMC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_BCR_ADDR, m)
#define HWIO_GCC_BIMC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_BCR_ADDR,v)
#define HWIO_GCC_BIMC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_BCR_ADDR,m,v,HWIO_GCC_BIMC_BCR_IN)
#define HWIO_GCC_BIMC_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_BIMC_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_BIMC_GDSCR_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001104)
#define HWIO_GCC_BIMC_GDSCR_RMSK                                                                      0xf8ffffff
#define HWIO_GCC_BIMC_GDSCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_GDSCR_ADDR, HWIO_GCC_BIMC_GDSCR_RMSK)
#define HWIO_GCC_BIMC_GDSCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_GDSCR_ADDR, m)
#define HWIO_GCC_BIMC_GDSCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_GDSCR_ADDR,v)
#define HWIO_GCC_BIMC_GDSCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_GDSCR_ADDR,m,v,HWIO_GCC_BIMC_GDSCR_IN)
#define HWIO_GCC_BIMC_GDSCR_PWR_ON_BMSK                                                               0x80000000
#define HWIO_GCC_BIMC_GDSCR_PWR_ON_SHFT                                                                     0x1f
#define HWIO_GCC_BIMC_GDSCR_GDSC_STATE_BMSK                                                           0x78000000
#define HWIO_GCC_BIMC_GDSCR_GDSC_STATE_SHFT                                                                 0x1b
#define HWIO_GCC_BIMC_GDSCR_EN_REST_WAIT_BMSK                                                           0xf00000
#define HWIO_GCC_BIMC_GDSCR_EN_REST_WAIT_SHFT                                                               0x14
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_WAIT_BMSK                                                             0xf0000
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_WAIT_SHFT                                                                0x10
#define HWIO_GCC_BIMC_GDSCR_CLK_DIS_WAIT_BMSK                                                             0xf000
#define HWIO_GCC_BIMC_GDSCR_CLK_DIS_WAIT_SHFT                                                                0xc
#define HWIO_GCC_BIMC_GDSCR_RETAIN_FF_ENABLE_BMSK                                                          0x800
#define HWIO_GCC_BIMC_GDSCR_RETAIN_FF_ENABLE_SHFT                                                            0xb
#define HWIO_GCC_BIMC_GDSCR_RESTORE_BMSK                                                                   0x400
#define HWIO_GCC_BIMC_GDSCR_RESTORE_SHFT                                                                     0xa
#define HWIO_GCC_BIMC_GDSCR_SAVE_BMSK                                                                      0x200
#define HWIO_GCC_BIMC_GDSCR_SAVE_SHFT                                                                        0x9
#define HWIO_GCC_BIMC_GDSCR_RETAIN_BMSK                                                                    0x100
#define HWIO_GCC_BIMC_GDSCR_RETAIN_SHFT                                                                      0x8
#define HWIO_GCC_BIMC_GDSCR_EN_REST_BMSK                                                                    0x80
#define HWIO_GCC_BIMC_GDSCR_EN_REST_SHFT                                                                     0x7
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_BMSK                                                                     0x40
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_SHFT                                                                      0x6
#define HWIO_GCC_BIMC_GDSCR_CLAMP_IO_BMSK                                                                   0x20
#define HWIO_GCC_BIMC_GDSCR_CLAMP_IO_SHFT                                                                    0x5
#define HWIO_GCC_BIMC_GDSCR_CLK_DISABLE_BMSK                                                                0x10
#define HWIO_GCC_BIMC_GDSCR_CLK_DISABLE_SHFT                                                                 0x4
#define HWIO_GCC_BIMC_GDSCR_PD_ARES_BMSK                                                                     0x8
#define HWIO_GCC_BIMC_GDSCR_PD_ARES_SHFT                                                                     0x3
#define HWIO_GCC_BIMC_GDSCR_SW_OVERRIDE_BMSK                                                                 0x4
#define HWIO_GCC_BIMC_GDSCR_SW_OVERRIDE_SHFT                                                                 0x2
#define HWIO_GCC_BIMC_GDSCR_HW_CONTROL_BMSK                                                                  0x2
#define HWIO_GCC_BIMC_GDSCR_HW_CONTROL_SHFT                                                                  0x1
#define HWIO_GCC_BIMC_GDSCR_SW_COLLAPSE_BMSK                                                                 0x1
#define HWIO_GCC_BIMC_GDSCR_SW_COLLAPSE_SHFT                                                                 0x0

#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00001140)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_RMSK                                                            0x80000002
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR, HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_EN_BMSK                                                           0x2
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_EN_SHFT                                                           0x1

#define HWIO_GCC_BIMC_XO_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001108)
#define HWIO_GCC_BIMC_XO_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_BIMC_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_XO_CBCR_ADDR, HWIO_GCC_BIMC_XO_CBCR_RMSK)
#define HWIO_GCC_BIMC_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_XO_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_XO_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_XO_CBCR_ADDR,m,v,HWIO_GCC_BIMC_XO_CBCR_IN)
#define HWIO_GCC_BIMC_XO_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_BIMC_XO_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_BIMC_XO_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_BIMC_XO_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000110c)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, HWIO_GCC_BIMC_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_BIMC_CFG_AHB_CBCR_IN)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_BIMC_SLEEP_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00001110)
#define HWIO_GCC_BIMC_SLEEP_CBCR_RMSK                                                                 0x80000001
#define HWIO_GCC_BIMC_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, HWIO_GCC_BIMC_SLEEP_CBCR_RMSK)
#define HWIO_GCC_BIMC_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_BIMC_SLEEP_CBCR_IN)
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00001114)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_RMSK                                                            0xf0008001
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_RMSK)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR,m,v,HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_IN)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001158)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR, HWIO_GCC_BIMC_DDR_CMD_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_CMD_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000115c)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR, HWIO_GCC_BIMC_DDR_CFG_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_CFG_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_BIMC_MISC_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000116c)
#define HWIO_GCC_BIMC_MISC_RMSK                                                                         0x7f0f3f
#define HWIO_GCC_BIMC_MISC_IN          \
        in_dword_masked(HWIO_GCC_BIMC_MISC_ADDR, HWIO_GCC_BIMC_MISC_RMSK)
#define HWIO_GCC_BIMC_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_MISC_ADDR, m)
#define HWIO_GCC_BIMC_MISC_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_MISC_ADDR,v)
#define HWIO_GCC_BIMC_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_MISC_ADDR,m,v,HWIO_GCC_BIMC_MISC_IN)
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_SEL_STATUS_BMSK                                               0x400000
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_SEL_STATUS_SHFT                                                   0x16
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_FRQSW_FSM_STATUS_BMSK                                               0x3f0000
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_FRQSW_FSM_STATUS_SHFT                                                   0x10
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_CLK_GATE_DIS_BMSK                                                      0x800
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_CLK_GATE_DIS_SHFT                                                        0xb
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_LEGACY_MODE_EN_BMSK                                                    0x400
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_LEGACY_MODE_EN_SHFT                                                      0xa
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_DIS_DDR_UPDATE_BMSK                                                    0x200
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_DIS_DDR_UPDATE_SHFT                                                      0x9
#define HWIO_GCC_BIMC_MISC_BIMC_FRQSW_FSM_DIS_BMSK                                                         0x100
#define HWIO_GCC_BIMC_MISC_BIMC_FRQSW_FSM_DIS_SHFT                                                           0x8
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_CLK_2X_MODE_BMSK                                                  0x20
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_CLK_2X_MODE_SHFT                                                   0x5
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_BYPASS_BMSK                                                       0x10
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_JCPLL_BYPASS_SHFT                                                        0x4
#define HWIO_GCC_BIMC_MISC_JCPLL_CLK_POST_DIV_BMSK                                                           0xf
#define HWIO_GCC_BIMC_MISC_JCPLL_CLK_POST_DIV_SHFT                                                           0x0

#define HWIO_GCC_BIMC_CBCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001118)
#define HWIO_GCC_BIMC_CBCR_RMSK                                                                       0x80000001
#define HWIO_GCC_BIMC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_CBCR_ADDR, HWIO_GCC_BIMC_CBCR_RMSK)
#define HWIO_GCC_BIMC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_CBCR_ADDR,m,v,HWIO_GCC_BIMC_CBCR_IN)
#define HWIO_GCC_BIMC_CBCR_CLK_OFF_BMSK                                                               0x80000000
#define HWIO_GCC_BIMC_CBCR_CLK_OFF_SHFT                                                                     0x1f
#define HWIO_GCC_BIMC_CBCR_CLK_ENABLE_BMSK                                                                   0x1
#define HWIO_GCC_BIMC_CBCR_CLK_ENABLE_SHFT                                                                   0x0

#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000111c)
#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_RMSK                                                              0x80000000
#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_KPSS_AXI_CBCR_ADDR, HWIO_GCC_BIMC_KPSS_AXI_CBCR_RMSK)
#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_KPSS_AXI_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_BIMC_KPSS_AXI_CBCR_CLK_OFF_SHFT                                                            0x1f

#define HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00001180)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_RMSK                                                                0xf0008001
#define HWIO_GCC_DDR_DIM_CFG_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, HWIO_GCC_DDR_DIM_CFG_CBCR_RMSK)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, m)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR,v)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR,m,v,HWIO_GCC_DDR_DIM_CFG_CBCR_IN)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000118c)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, HWIO_GCC_DDR_DIM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_DDR_DIM_SLEEP_CBCR_IN)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_LPASS_Q6_AXI_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x000011c0)
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_RMSK                                                               0x80000003
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_LPASS_Q6_AXI_CBCR_ADDR, HWIO_GCC_LPASS_Q6_AXI_CBCR_RMSK)
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_LPASS_Q6_AXI_CBCR_ADDR, m)
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_LPASS_Q6_AXI_CBCR_ADDR,v)
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_LPASS_Q6_AXI_CBCR_ADDR,m,v,HWIO_GCC_LPASS_Q6_AXI_CBCR_IN)
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_HW_CTL_BMSK                                                               0x2
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_HW_CTL_SHFT                                                               0x1
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_LPASS_Q6_AXI_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_KPSS_AHB_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000120c)
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CMD_RCGR_ADDR, HWIO_GCC_KPSS_AHB_CMD_RCGR_RMSK)
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CMD_RCGR_ADDR, m)
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_KPSS_AHB_CMD_RCGR_ADDR,v)
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_KPSS_AHB_CMD_RCGR_ADDR,m,v,HWIO_GCC_KPSS_AHB_CMD_RCGR_IN)
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_KPSS_AHB_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_KPSS_AHB_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001210)
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CFG_RCGR_ADDR, HWIO_GCC_KPSS_AHB_CFG_RCGR_RMSK)
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CFG_RCGR_ADDR, m)
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_KPSS_AHB_CFG_RCGR_ADDR,v)
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_KPSS_AHB_CFG_RCGR_ADDR,m,v,HWIO_GCC_KPSS_AHB_CFG_RCGR_IN)
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_KPSS_AHB_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_KPSS_AHB_MISC_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00001220)
#define HWIO_GCC_KPSS_AHB_MISC_RMSK                                                                         0xf1
#define HWIO_GCC_KPSS_AHB_MISC_IN          \
        in_dword_masked(HWIO_GCC_KPSS_AHB_MISC_ADDR, HWIO_GCC_KPSS_AHB_MISC_RMSK)
#define HWIO_GCC_KPSS_AHB_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_AHB_MISC_ADDR, m)
#define HWIO_GCC_KPSS_AHB_MISC_OUT(v)      \
        out_dword(HWIO_GCC_KPSS_AHB_MISC_ADDR,v)
#define HWIO_GCC_KPSS_AHB_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_KPSS_AHB_MISC_ADDR,m,v,HWIO_GCC_KPSS_AHB_MISC_IN)
#define HWIO_GCC_KPSS_AHB_MISC_KPSS_AHB_CLK_AUTO_SCALE_DIV_BMSK                                             0xf0
#define HWIO_GCC_KPSS_AHB_MISC_KPSS_AHB_CLK_AUTO_SCALE_DIV_SHFT                                              0x4
#define HWIO_GCC_KPSS_AHB_MISC_KPSS_AHB_CLK_AUTO_SCALE_DIS_BMSK                                              0x1
#define HWIO_GCC_KPSS_AHB_MISC_KPSS_AHB_CLK_AUTO_SCALE_DIS_SHFT                                              0x0

#define HWIO_GCC_KPSS_AHB_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00001204)
#define HWIO_GCC_KPSS_AHB_CBCR_RMSK                                                                   0xf0008000
#define HWIO_GCC_KPSS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CBCR_ADDR, HWIO_GCC_KPSS_AHB_CBCR_RMSK)
#define HWIO_GCC_KPSS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_KPSS_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_KPSS_AHB_CBCR_ADDR,v)
#define HWIO_GCC_KPSS_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_KPSS_AHB_CBCR_ADDR,m,v,HWIO_GCC_KPSS_AHB_CBCR_IN)
#define HWIO_GCC_KPSS_AHB_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_KPSS_AHB_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_KPSS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_KPSS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_KPSS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_KPSS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf

#define HWIO_GCC_KPSS_AXI_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00001208)
#define HWIO_GCC_KPSS_AXI_CBCR_RMSK                                                                   0x80000000
#define HWIO_GCC_KPSS_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_KPSS_AXI_CBCR_ADDR, HWIO_GCC_KPSS_AXI_CBCR_RMSK)
#define HWIO_GCC_KPSS_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_AXI_CBCR_ADDR, m)
#define HWIO_GCC_KPSS_AXI_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_KPSS_AXI_CBCR_CLK_OFF_SHFT                                                                 0x1f

#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001240)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_RMSK                                                                  0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001244)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001280)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_RMSK                                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001284)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001288)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_RMSK                                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0000128c)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001290)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_RMSK                                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001294)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001298)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_RMSK                                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0000129c)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012a0)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_RMSK                                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012a4)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,m,v,HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_IN)
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012c0)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012c4)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012c8)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012cc)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012d0)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012d4)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012d8)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012dc)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012e0)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012e4)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012e8)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012ec)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000012f0)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_RMSK                                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_BLK_ARES_BMSK                                                         0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_BCR_BLK_ARES_SHFT                                                         0x0

#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x000012f4)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_RMSK                                                      0x80000001
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR, HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_RMSK)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR,m,v,HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_IN)
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_CNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_DEHR_BCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001300)
#define HWIO_GCC_DEHR_BCR_RMSK                                                                               0x1
#define HWIO_GCC_DEHR_BCR_IN          \
        in_dword_masked(HWIO_GCC_DEHR_BCR_ADDR, HWIO_GCC_DEHR_BCR_RMSK)
#define HWIO_GCC_DEHR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DEHR_BCR_ADDR, m)
#define HWIO_GCC_DEHR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_DEHR_BCR_ADDR,v)
#define HWIO_GCC_DEHR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DEHR_BCR_ADDR,m,v,HWIO_GCC_DEHR_BCR_IN)
#define HWIO_GCC_DEHR_BCR_BLK_ARES_BMSK                                                                      0x1
#define HWIO_GCC_DEHR_BCR_BLK_ARES_SHFT                                                                      0x0

#define HWIO_GCC_DEHR_CBCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001304)
#define HWIO_GCC_DEHR_CBCR_RMSK                                                                       0xf000fff1
#define HWIO_GCC_DEHR_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DEHR_CBCR_ADDR, HWIO_GCC_DEHR_CBCR_RMSK)
#define HWIO_GCC_DEHR_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DEHR_CBCR_ADDR, m)
#define HWIO_GCC_DEHR_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DEHR_CBCR_ADDR,v)
#define HWIO_GCC_DEHR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DEHR_CBCR_ADDR,m,v,HWIO_GCC_DEHR_CBCR_IN)
#define HWIO_GCC_DEHR_CBCR_CLK_OFF_BMSK                                                               0x80000000
#define HWIO_GCC_DEHR_CBCR_CLK_OFF_SHFT                                                                     0x1f
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                              0x70000000
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                    0x1c
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                      0x8000
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                         0xf
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_CORE_ON_BMSK                                                         0x4000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_CORE_ON_SHFT                                                            0xe
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                       0x2000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                          0xd
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                      0x1000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                         0xc
#define HWIO_GCC_DEHR_CBCR_WAKEUP_BMSK                                                                     0xf00
#define HWIO_GCC_DEHR_CBCR_WAKEUP_SHFT                                                                       0x8
#define HWIO_GCC_DEHR_CBCR_SLEEP_BMSK                                                                       0xf0
#define HWIO_GCC_DEHR_CBCR_SLEEP_SHFT                                                                        0x4
#define HWIO_GCC_DEHR_CBCR_CLK_ENABLE_BMSK                                                                   0x1
#define HWIO_GCC_DEHR_CBCR_CLK_ENABLE_SHFT                                                                   0x0

#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00001340)
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_RMSK                                                     0xf0008001
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_ADDR, HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_IN)
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_CLK_OFF_BMSK                                             0x80000000
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_CLK_OFF_SHFT                                                   0x1f
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                            0x70000000
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                  0x1c
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                    0x8000
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                       0xf
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                 0x1
#define HWIO_GCC_PERIPH_NOC_MPU_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                 0x0

#define HWIO_GCC_RBCPR_BCR_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001380)
#define HWIO_GCC_RBCPR_BCR_RMSK                                                                              0x1
#define HWIO_GCC_RBCPR_BCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_BCR_ADDR, HWIO_GCC_RBCPR_BCR_RMSK)
#define HWIO_GCC_RBCPR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_BCR_ADDR, m)
#define HWIO_GCC_RBCPR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_BCR_ADDR,v)
#define HWIO_GCC_RBCPR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_BCR_ADDR,m,v,HWIO_GCC_RBCPR_BCR_IN)
#define HWIO_GCC_RBCPR_BCR_BLK_ARES_BMSK                                                                     0x1
#define HWIO_GCC_RBCPR_BCR_BLK_ARES_SHFT                                                                     0x0

#define HWIO_GCC_RBCPR_CBCR_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001384)
#define HWIO_GCC_RBCPR_CBCR_RMSK                                                                      0x80000001
#define HWIO_GCC_RBCPR_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CBCR_ADDR, HWIO_GCC_RBCPR_CBCR_RMSK)
#define HWIO_GCC_RBCPR_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_CBCR_IN)
#define HWIO_GCC_RBCPR_CBCR_CLK_OFF_BMSK                                                              0x80000000
#define HWIO_GCC_RBCPR_CBCR_CLK_OFF_SHFT                                                                    0x1f
#define HWIO_GCC_RBCPR_CBCR_CLK_ENABLE_BMSK                                                                  0x1
#define HWIO_GCC_RBCPR_CBCR_CLK_ENABLE_SHFT                                                                  0x0

#define HWIO_GCC_RBCPR_AHB_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00001388)
#define HWIO_GCC_RBCPR_AHB_CBCR_RMSK                                                                  0xf0008001
#define HWIO_GCC_RBCPR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_AHB_CBCR_ADDR, HWIO_GCC_RBCPR_AHB_CBCR_RMSK)
#define HWIO_GCC_RBCPR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_AHB_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_AHB_CBCR_IN)
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                         0x70000000
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                               0x1c
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                 0x8000
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                    0xf
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_RBCPR_CMD_RCGR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0000138c)
#define HWIO_GCC_RBCPR_CMD_RCGR_RMSK                                                                  0x80000013
#define HWIO_GCC_RBCPR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CMD_RCGR_ADDR, HWIO_GCC_RBCPR_CMD_RCGR_RMSK)
#define HWIO_GCC_RBCPR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CMD_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_CMD_RCGR_IN)
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_OFF_SHFT                                                               0x1f
#define HWIO_GCC_RBCPR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                         0x10
#define HWIO_GCC_RBCPR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                          0x4
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_EN_BMSK                                                                 0x2
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_EN_SHFT                                                                 0x1
#define HWIO_GCC_RBCPR_CMD_RCGR_UPDATE_BMSK                                                                  0x1
#define HWIO_GCC_RBCPR_CMD_RCGR_UPDATE_SHFT                                                                  0x0

#define HWIO_GCC_RBCPR_CFG_RCGR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00001390)
#define HWIO_GCC_RBCPR_CFG_RCGR_RMSK                                                                       0x71f
#define HWIO_GCC_RBCPR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CFG_RCGR_ADDR, HWIO_GCC_RBCPR_CFG_RCGR_RMSK)
#define HWIO_GCC_RBCPR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CFG_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_CFG_RCGR_IN)
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_SEL_BMSK                                                               0x700
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_SEL_SHFT                                                                 0x8
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_DIV_BMSK                                                                0x1f
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_DIV_SHFT                                                                 0x0

#define HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001440)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_RMSK                                                                      0xf
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR, HWIO_GCC_RPM_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL3_BMSK                                                                0x8
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL3_SHFT                                                                0x3
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL2_BMSK                                                                0x4
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL2_SHFT                                                                0x2
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL1_BMSK                                                                0x2
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL1_SHFT                                                                0x1
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL0_BMSK                                                                0x1
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL0_SHFT                                                                0x0

#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001444)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_RMSK                                                        0xfffffff
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                                 0x8000000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                      0x1b
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                                 0x4000000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                      0x1a
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                                0x2000000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                     0x19
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                       0x1000000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                            0x18
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                                0x800000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                    0x17
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                                   0x400000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                       0x16
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                        0x200000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                            0x15
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                        0x100000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                            0x14
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                          0x80000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                             0x13
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                                0x40000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                                   0x12
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                        0x20000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                           0x11
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                      0x10000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                         0x10
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                         0x8000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                            0xf
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                       0x4000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                          0xe
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                          0x2000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                             0xd
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                       0x1000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                          0xc
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                          0x800
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                            0xb
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                       0x400
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                         0xa
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                        0x200
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                          0x9
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                           0x100
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                             0x8
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                                0x80
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                                 0x7
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                       0x40
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                        0x6
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                                 0x20
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                                  0x5
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                             0x10
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                              0x4
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                              0x8
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                              0x3
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                                  0x4
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                                  0x2
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                              0x2
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                              0x1
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                              0x1
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                              0x0

#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001448)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_RMSK                                                         0xfffffff
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                            0x8000000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                                 0x1b
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                            0x4000000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                                 0x1a
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                           0x2000000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                                0x19
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                                  0x1000000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                       0x18
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                           0x800000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                               0x17
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                              0x400000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                  0x16
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                                   0x200000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                       0x15
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                                   0x100000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                       0x14
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                     0x80000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                        0x13
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                           0x40000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                              0x12
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                   0x20000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                      0x11
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                                 0x10000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                    0x10
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                                    0x8000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                       0xf
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                                  0x4000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                                     0xe
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                     0x2000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                        0xd
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                                  0x1000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                                     0xc
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK                     0x800
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                       0xb
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                                  0x400
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                    0xa
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                                   0x200
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                     0x9
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                      0x100
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                        0x8
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                           0x80
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                            0x7
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                                  0x40
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                                   0x6
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                            0x20
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                             0x5
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                        0x10
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                         0x4
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                         0x8
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                         0x3
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                             0x4
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                             0x2
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                         0x2
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                         0x1
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                         0x1
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                         0x0

#define HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001480)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_RMSK                                                                     0xf
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR, HWIO_GCC_APCS_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL3_BMSK                                                               0x8
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL3_SHFT                                                               0x3
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL2_BMSK                                                               0x4
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL2_SHFT                                                               0x2
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL1_BMSK                                                               0x2
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL1_SHFT                                                               0x1
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL0_BMSK                                                               0x1
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL0_SHFT                                                               0x0

#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001484)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_RMSK                                                       0xfffffff
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                                0x8000000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                     0x1b
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                                0x4000000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                     0x1a
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                               0x2000000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                    0x19
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                      0x1000000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                           0x18
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                               0x800000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                   0x17
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                                  0x400000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                      0x16
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                       0x200000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                           0x15
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                       0x100000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                           0x14
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                         0x80000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                            0x13
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                               0x40000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                                  0x12
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                       0x20000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                          0x11
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                     0x10000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                        0x10
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                        0x8000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                           0xf
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                      0x4000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                         0xe
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                         0x2000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                            0xd
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                      0x1000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                         0xc
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                         0x800
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                           0xb
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                      0x400
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                        0xa
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                       0x200
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                         0x9
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                          0x100
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                            0x8
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                               0x80
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                                0x7
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                      0x40
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                       0x6
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                                0x20
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                                 0x5
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                            0x10
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                             0x4
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                             0x8
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                             0x3
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                                 0x4
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                                 0x2
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                             0x2
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                             0x1
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                             0x1
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                             0x0

#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001488)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_RMSK                                                        0xfffffff
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                           0x8000000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                                0x1b
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                           0x4000000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                                0x1a
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                          0x2000000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                               0x19
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                                 0x1000000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                      0x18
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                          0x800000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                              0x17
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                             0x400000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                 0x16
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                                  0x200000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                      0x15
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                                  0x100000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                      0x14
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                    0x80000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                       0x13
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                          0x40000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                             0x12
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                  0x20000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                     0x11
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                                0x10000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                   0x10
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                                   0x8000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                      0xf
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                                 0x4000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                                    0xe
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                    0x2000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                       0xd
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                                 0x1000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                                    0xc
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK                    0x800
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                      0xb
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                                 0x400
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                   0xa
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                                  0x200
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                    0x9
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                     0x100
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                       0x8
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                          0x80
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                           0x7
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                                 0x40
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                                  0x6
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                           0x20
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                            0x5
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                       0x10
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                        0x4
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                        0x8
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                        0x3
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                            0x4
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                            0x2
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                        0x2
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                        0x1
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                        0x1
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                        0x0

#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x000014c0)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_RMSK                                                                  0xf
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL3_BMSK                                                            0x8
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL3_SHFT                                                            0x3
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL2_BMSK                                                            0x4
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL2_SHFT                                                            0x2
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL1_BMSK                                                            0x2
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL1_SHFT                                                            0x1
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL0_BMSK                                                            0x1
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL0_SHFT                                                            0x0

#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x000014c4)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_RMSK                                                    0xfffffff
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                             0x8000000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                  0x1b
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                             0x4000000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                  0x1a
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                            0x2000000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                 0x19
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                   0x1000000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                        0x18
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                            0x800000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                0x17
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                               0x400000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                   0x16
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                    0x200000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                        0x15
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                    0x100000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                        0x14
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                      0x80000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                         0x13
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                            0x40000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                               0x12
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                    0x20000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                       0x11
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                  0x10000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                     0x10
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                     0x8000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                        0xf
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                   0x4000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                      0xe
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                      0x2000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                         0xd
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                   0x1000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                      0xc
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                      0x800
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                        0xb
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                   0x400
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                     0xa
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                    0x200
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                      0x9
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                       0x100
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                         0x8
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                            0x80
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                             0x7
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                   0x40
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                    0x6
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                             0x20
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                              0x5
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                         0x10
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                          0x4
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                          0x8
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                          0x3
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                              0x4
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                              0x2
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                          0x2
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                          0x1
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                          0x1
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                          0x0

#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000014c8)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_RMSK                                                     0xfffffff
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                        0x8000000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                             0x1b
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                        0x4000000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                             0x1a
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                       0x2000000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                            0x19
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                              0x1000000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                   0x18
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                       0x800000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                           0x17
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                          0x400000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                              0x16
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                               0x200000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                   0x15
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                               0x100000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                   0x14
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                 0x80000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                    0x13
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                       0x40000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                          0x12
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                               0x20000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                  0x11
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                             0x10000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                0x10
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                                0x8000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                   0xf
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                              0x4000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                                 0xe
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                 0x2000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                    0xd
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                              0x1000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                                 0xc
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK                 0x800
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                   0xb
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                              0x400
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                0xa
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                               0x200
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                 0x9
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                  0x100
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                    0x8
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                       0x80
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                        0x7
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                              0x40
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                               0x6
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                        0x20
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                         0x5
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                    0x10
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                     0x4
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                     0x8
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                     0x3
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                         0x4
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                         0x2
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                     0x2
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                     0x1
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                     0x1
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                     0x0

#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00001500)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_RMSK                                                                   0xf
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL3_BMSK                                                             0x8
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL3_SHFT                                                             0x3
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL2_BMSK                                                             0x4
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL2_SHFT                                                             0x2
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL1_BMSK                                                             0x2
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL1_SHFT                                                             0x1
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL0_BMSK                                                             0x1
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL0_SHFT                                                             0x0

#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001504)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_RMSK                                                     0xfffffff
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                              0x8000000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                   0x1b
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                              0x4000000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                   0x1a
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                             0x2000000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                  0x19
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                    0x1000000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                         0x18
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                             0x800000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                 0x17
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                                0x400000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                    0x16
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                     0x200000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                         0x15
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                     0x100000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                         0x14
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                       0x80000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                          0x13
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                             0x40000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                                0x12
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                     0x20000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                        0x11
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                   0x10000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                      0x10
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                      0x8000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                         0xf
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                    0x4000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                       0xe
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                       0x2000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                          0xd
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                    0x1000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                       0xc
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                       0x800
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                         0xb
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                    0x400
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                      0xa
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                     0x200
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                       0x9
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                        0x100
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                          0x8
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                             0x80
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                              0x7
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                    0x40
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                     0x6
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                              0x20
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                               0x5
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                          0x10
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                           0x4
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                           0x8
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                           0x3
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                               0x4
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                               0x2
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                           0x2
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                           0x1
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                           0x1
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                           0x0

#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00001508)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_RMSK                                                      0xfffffff
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                         0x8000000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                              0x1b
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                         0x4000000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                              0x1a
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                        0x2000000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                             0x19
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                               0x1000000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                    0x18
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                        0x800000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                            0x17
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                           0x400000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                               0x16
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                                0x200000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                    0x15
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                                0x100000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                    0x14
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                  0x80000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                     0x13
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                        0x40000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                           0x12
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                0x20000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                   0x11
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                              0x10000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                 0x10
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                                 0x8000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                    0xf
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                               0x4000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                                  0xe
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                  0x2000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                     0xd
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                               0x1000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                                  0xc
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK                  0x800
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                    0xb
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                               0x400
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                 0xa
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                                0x200
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                  0x9
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                   0x100
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                     0x8
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                        0x80
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                         0x7
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                               0x40
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                                0x6
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                         0x20
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                          0x5
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                     0x10
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                      0x4
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                      0x8
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                      0x3
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                          0x4
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                          0x2
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                      0x2
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                      0x1
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                      0x1
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                      0x0

#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001540)
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_RMSK                                                                0xf
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_ADDR, HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL3_BMSK                                                          0x8
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL3_SHFT                                                          0x3
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL2_BMSK                                                          0x4
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL2_SHFT                                                          0x2
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL1_BMSK                                                          0x2
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL1_SHFT                                                          0x1
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL0_BMSK                                                          0x1
#define HWIO_GCC_LPASS_DSP_GPLL_ENA_VOTE_GPLL0_SHFT                                                          0x0

#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00001544)
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_RMSK                                                  0xfffffff
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                           0x8000000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                0x1b
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                           0x4000000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                0x1a
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                          0x2000000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                               0x19
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                 0x1000000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                      0x18
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                          0x800000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                              0x17
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                             0x400000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                 0x16
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                  0x200000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                      0x15
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                  0x100000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                      0x14
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                    0x80000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                       0x13
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                          0x40000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                             0x12
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                  0x20000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                     0x11
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                0x10000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                   0x10
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                   0x8000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                      0xf
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                 0x4000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                    0xe
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                    0x2000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                       0xd
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                 0x1000
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                    0xc
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                    0x800
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                      0xb
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                 0x400
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                   0xa
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                  0x200
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                    0x9
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                     0x100
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                       0x8
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                          0x80
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                           0x7
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                 0x40
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                  0x6
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                           0x20
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                            0x5
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                       0x10
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                        0x4
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                        0x8
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                        0x3
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                            0x4
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                            0x2
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                        0x2
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                        0x1
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                        0x1
#define HWIO_GCC_LPASS_DSP_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                        0x0

#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00001548)
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_RMSK                                                   0xfffffff
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                      0x8000000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                           0x1b
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                      0x4000000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                           0x1a
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                     0x2000000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                          0x19
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                            0x1000000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                 0x18
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                     0x800000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                         0x17
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                        0x400000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                            0x16
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                             0x200000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                 0x15
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                             0x100000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                 0x14
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                               0x80000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                  0x13
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                     0x40000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                        0x12
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                             0x20000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                0x11
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                           0x10000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                              0x10
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                              0x8000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                 0xf
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                            0x4000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                               0xe
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                               0x2000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                  0xd
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                            0x1000
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                               0xc
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK               0x800
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                 0xb
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                            0x400
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                              0xa
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                             0x200
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                               0x9
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                0x100
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                  0x8
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                     0x80
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                      0x7
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                            0x40
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                             0x6
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                      0x20
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                       0x5
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                  0x10
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                   0x4
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                   0x8
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                   0x3
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                       0x4
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                       0x2
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                   0x2
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                   0x1
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                   0x1
#define HWIO_GCC_LPASS_DSP_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                   0x0

#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001580)
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_RMSK                                                                     0xf
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_WCSS_GPLL_ENA_VOTE_ADDR, HWIO_GCC_WCSS_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_WCSS_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_WCSS_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_WCSS_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_WCSS_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL3_BMSK                                                               0x8
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL3_SHFT                                                               0x3
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL2_BMSK                                                               0x4
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL2_SHFT                                                               0x2
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL1_BMSK                                                               0x2
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL1_SHFT                                                               0x1
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL0_BMSK                                                               0x1
#define HWIO_GCC_WCSS_GPLL_ENA_VOTE_GPLL0_SHFT                                                               0x0

#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001584)
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_RMSK                                                       0xfffffff
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                                0x8000000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                     0x1b
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                                0x4000000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                     0x1a
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                               0x2000000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                    0x19
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                      0x1000000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                           0x18
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                               0x800000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                   0x17
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                                  0x400000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                      0x16
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                       0x200000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                           0x15
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                       0x100000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                           0x14
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                         0x80000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                            0x13
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                               0x40000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                                  0x12
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                       0x20000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                          0x11
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                     0x10000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                        0x10
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                        0x8000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                           0xf
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                      0x4000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                         0xe
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                         0x2000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                            0xd
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                      0x1000
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                         0xc
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                         0x800
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                           0xb
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                      0x400
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                        0xa
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                       0x200
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                         0x9
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                          0x100
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                            0x8
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                               0x80
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                                0x7
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                      0x40
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                       0x6
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                                0x20
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                                 0x5
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                            0x10
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                             0x4
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                             0x8
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                             0x3
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                                 0x4
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                                 0x2
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                             0x2
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                             0x1
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                             0x1
#define HWIO_GCC_WCSS_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                             0x0

#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001588)
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_RMSK                                                        0xfffffff
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_BMSK                           0x8000000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_WCSS_GPLL1_CLK_SRC_SLEEP_ENA_SHFT                                0x1b
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                           0x4000000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MMSS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                                0x1a
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_BMSK                          0x2000000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_LPASS_GPLL0_CLK_SRC_SLEEP_ENA_SHFT                               0x19
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                                 0x1000000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                      0x18
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_BMSK                          0x800000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_SLEEP_ENA_SHFT                              0x17
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_BMSK                             0x400000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BIMC_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                 0x16
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_BMSK                                  0x200000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_KPSS_AHB_CLK_SLEEP_ENA_SHFT                                      0x15
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_BMSK                                  0x100000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_KPSS_AXI_CLK_SLEEP_ENA_SHFT                                      0x14
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                    0x80000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                       0x13
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_BMSK                          0x40000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_SLEEP_ENA_SHFT                             0x12
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                  0x20000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                     0x11
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                                0x10000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                   0x10
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_BMSK                                   0x8000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP2_AHB_CLK_SLEEP_ENA_SHFT                                      0xf
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_BMSK                                 0x4000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BLSP2_SLEEP_CLK_SLEEP_ENA_SHFT                                    0xe
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                    0x2000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                       0xd
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_BMSK                                 0x1000
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_AHB_CLK_SLEEP_ENA_SHFT                                    0xc
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_BMSK                    0x800
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_SLEEP_ENA_SHFT                      0xb
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                                 0x400
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                   0xa
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                                  0x200
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                    0x9
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                     0x100
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                       0x8
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                          0x80
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                           0x7
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_BMSK                                 0x40
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_SPMI_CNOC_AHB_CLK_SLEEP_ENA_SHFT                                  0x6
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_BMSK                                           0x20
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_CLK_SLEEP_ENA_SHFT                                            0x5
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_BMSK                                       0x10
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_AXI_CLK_SLEEP_ENA_SHFT                                        0x4
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_BMSK                                        0x8
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE1_AHB_CLK_SLEEP_ENA_SHFT                                        0x3
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_BMSK                                            0x4
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_CLK_SLEEP_ENA_SHFT                                            0x2
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_BMSK                                        0x2
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_AXI_CLK_SLEEP_ENA_SHFT                                        0x1
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_BMSK                                        0x1
#define HWIO_GCC_WCSS_CLOCK_SLEEP_ENA_VOTE_CE2_AHB_CLK_SLEEP_ENA_SHFT                                        0x0

#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x000015c0)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_RMSK                                                                    0xf
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR, HWIO_GCC_SPARE_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_SPARE_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL3_BMSK                                                              0x8
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL3_SHFT                                                              0x3
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL2_BMSK                                                              0x4
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL2_SHFT                                                              0x2
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL1_BMSK                                                              0x2
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL1_SHFT                                                              0x1
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL0_BMSK                                                              0x1
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL0_SHFT                                                              0x0

#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x000015c4)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_RMSK                                                      0xfffffff
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_BMSK                               0x8000000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_WCSS_GPLL1_CLK_SRC_ENA_SHFT                                    0x1b
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_BMSK                               0x4000000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MMSS_GPLL0_CLK_SRC_ENA_SHFT                                    0x1a
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_BMSK                              0x2000000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_LPASS_GPLL0_CLK_SRC_ENA_SHFT                                   0x19
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                     0x1000000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                          0x18
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_BMSK                              0x800000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_KPSS_AHB_CLK_ENA_SHFT                                  0x17
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_BMSK                                 0x400000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BIMC_KPSS_AXI_CLK_ENA_SHFT                                     0x16
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_BMSK                                      0x200000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_KPSS_AHB_CLK_ENA_SHFT                                          0x15
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_BMSK                                      0x100000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_KPSS_AXI_CLK_ENA_SHFT                                          0x14
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                        0x80000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                           0x13
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_BMSK                              0x40000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OCMEM_SYS_NOC_AXI_CLK_ENA_SHFT                                 0x12
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                      0x20000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                         0x11
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                    0x10000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                       0x10
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_BMSK                                       0x8000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP2_AHB_CLK_ENA_SHFT                                          0xf
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_BMSK                                     0x4000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP2_SLEEP_CLK_ENA_SHFT                                        0xe
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                        0x2000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                           0xd
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_BMSK                                     0x1000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_AHB_CLK_ENA_SHFT                                        0xc
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_BMSK                        0x800
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BAM_DMA_INACTIVITY_TIMERS_CLK_ENA_SHFT                          0xb
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                     0x400
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                       0xa
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                      0x200
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                        0x9
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                         0x100
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                           0x8
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                              0x80
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                               0x7
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_BMSK                                     0x40
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SPMI_CNOC_AHB_CLK_ENA_SHFT                                      0x6
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_BMSK                                               0x20
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_CLK_ENA_SHFT                                                0x5
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_BMSK                                           0x10
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_AXI_CLK_ENA_SHFT                                            0x4
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_BMSK                                            0x8
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE1_AHB_CLK_ENA_SHFT                                            0x3
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_BMSK                                                0x4
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_CLK_ENA_SHFT                                                0x2
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_BMSK                                            0x2
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_AXI_CLK_ENA_SHFT                                            0x1
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_BMSK                                            0x1
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CE2_AHB_CLK_ENA_SHFT                                            0x0

#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001600)
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT0_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001604)
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT1_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001608)
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT2_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0000160c)
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT3_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001610)
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT4_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001614)
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT5_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001618)
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT6_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0000161c)
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT7_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001620)
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT8_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001624)
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_RMSK                                                         0x80000003
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_EVENT_DISABLE_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_EVENT_DISABLE_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_EVENT_MUX_SEL_BMSK                                                  0x3
#define HWIO_GCC_QDSS_STM_EVENT9_CTL_REG_EVENT_MUX_SEL_SHFT                                                  0x0

#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001628)
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT10_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000162c)
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT11_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001630)
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT12_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001634)
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT13_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001638)
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT14_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000163c)
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT15_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001640)
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT16_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001644)
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT17_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001648)
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT18_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000164c)
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT19_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001650)
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT20_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001654)
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT21_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001658)
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT22_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000165c)
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT23_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001660)
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT24_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001664)
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT25_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001668)
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT26_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000166c)
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT27_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001670)
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT28_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001674)
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT29_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001678)
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT30_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0000167c)
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_RMSK                                                        0x80000003
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_ADDR, HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_RMSK)
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_ADDR, m)
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_ADDR,v)
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_ADDR,m,v,HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_IN)
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_EVENT_DISABLE_BMSK                                          0x80000000
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_EVENT_DISABLE_SHFT                                                0x1f
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_EVENT_MUX_SEL_BMSK                                                 0x3
#define HWIO_GCC_QDSS_STM_EVENT31_CTL_REG_EVENT_MUX_SEL_SHFT                                                 0x0

#define HWIO_GCC_MSS_RESTART_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00001680)
#define HWIO_GCC_MSS_RESTART_RMSK                                                                            0x1
#define HWIO_GCC_MSS_RESTART_IN          \
        in_dword_masked(HWIO_GCC_MSS_RESTART_ADDR, HWIO_GCC_MSS_RESTART_RMSK)
#define HWIO_GCC_MSS_RESTART_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_RESTART_ADDR, m)
#define HWIO_GCC_MSS_RESTART_OUT(v)      \
        out_dword(HWIO_GCC_MSS_RESTART_ADDR,v)
#define HWIO_GCC_MSS_RESTART_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_RESTART_ADDR,m,v,HWIO_GCC_MSS_RESTART_IN)
#define HWIO_GCC_MSS_RESTART_MSS_RESTART_BMSK                                                                0x1
#define HWIO_GCC_MSS_RESTART_MSS_RESTART_SHFT                                                                0x0

#define HWIO_GCC_LPASS_RESTART_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x000016c0)
#define HWIO_GCC_LPASS_RESTART_RMSK                                                                          0x1
#define HWIO_GCC_LPASS_RESTART_IN          \
        in_dword_masked(HWIO_GCC_LPASS_RESTART_ADDR, HWIO_GCC_LPASS_RESTART_RMSK)
#define HWIO_GCC_LPASS_RESTART_INM(m)      \
        in_dword_masked(HWIO_GCC_LPASS_RESTART_ADDR, m)
#define HWIO_GCC_LPASS_RESTART_OUT(v)      \
        out_dword(HWIO_GCC_LPASS_RESTART_ADDR,v)
#define HWIO_GCC_LPASS_RESTART_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_LPASS_RESTART_ADDR,m,v,HWIO_GCC_LPASS_RESTART_IN)
#define HWIO_GCC_LPASS_RESTART_LPASS_RESTART_BMSK                                                            0x1
#define HWIO_GCC_LPASS_RESTART_LPASS_RESTART_SHFT                                                            0x0

#define HWIO_GCC_WCSS_RESTART_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001700)
#define HWIO_GCC_WCSS_RESTART_RMSK                                                                           0x1
#define HWIO_GCC_WCSS_RESTART_IN          \
        in_dword_masked(HWIO_GCC_WCSS_RESTART_ADDR, HWIO_GCC_WCSS_RESTART_RMSK)
#define HWIO_GCC_WCSS_RESTART_INM(m)      \
        in_dword_masked(HWIO_GCC_WCSS_RESTART_ADDR, m)
#define HWIO_GCC_WCSS_RESTART_OUT(v)      \
        out_dword(HWIO_GCC_WCSS_RESTART_ADDR,v)
#define HWIO_GCC_WCSS_RESTART_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_WCSS_RESTART_ADDR,m,v,HWIO_GCC_WCSS_RESTART_IN)
#define HWIO_GCC_WCSS_RESTART_WCSS_RESTART_BMSK                                                              0x1
#define HWIO_GCC_WCSS_RESTART_WCSS_RESTART_SHFT                                                              0x0

#define HWIO_GCC_VENUS_RESTART_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00001740)
#define HWIO_GCC_VENUS_RESTART_RMSK                                                                          0x1
#define HWIO_GCC_VENUS_RESTART_IN          \
        in_dword_masked(HWIO_GCC_VENUS_RESTART_ADDR, HWIO_GCC_VENUS_RESTART_RMSK)
#define HWIO_GCC_VENUS_RESTART_INM(m)      \
        in_dword_masked(HWIO_GCC_VENUS_RESTART_ADDR, m)
#define HWIO_GCC_VENUS_RESTART_OUT(v)      \
        out_dword(HWIO_GCC_VENUS_RESTART_ADDR,v)
#define HWIO_GCC_VENUS_RESTART_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_VENUS_RESTART_ADDR,m,v,HWIO_GCC_VENUS_RESTART_IN)
#define HWIO_GCC_VENUS_RESTART_VENUS_RESTART_BMSK                                                            0x1
#define HWIO_GCC_VENUS_RESTART_VENUS_RESTART_SHFT                                                            0x0

#define HWIO_GCC_WDOG_DEBUG_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001780)
#define HWIO_GCC_WDOG_DEBUG_RMSK                                                                         0x3ffff
#define HWIO_GCC_WDOG_DEBUG_IN          \
        in_dword_masked(HWIO_GCC_WDOG_DEBUG_ADDR, HWIO_GCC_WDOG_DEBUG_RMSK)
#define HWIO_GCC_WDOG_DEBUG_INM(m)      \
        in_dword_masked(HWIO_GCC_WDOG_DEBUG_ADDR, m)
#define HWIO_GCC_WDOG_DEBUG_OUT(v)      \
        out_dword(HWIO_GCC_WDOG_DEBUG_ADDR,v)
#define HWIO_GCC_WDOG_DEBUG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_WDOG_DEBUG_ADDR,m,v,HWIO_GCC_WDOG_DEBUG_IN)
#define HWIO_GCC_WDOG_DEBUG_WDOG_DEBUG_EN_BMSK                                                           0x20000
#define HWIO_GCC_WDOG_DEBUG_WDOG_DEBUG_EN_SHFT                                                              0x11
#define HWIO_GCC_WDOG_DEBUG_PROC_HALT_EN_BMSK                                                            0x10000
#define HWIO_GCC_WDOG_DEBUG_PROC_HALT_EN_SHFT                                                               0x10
#define HWIO_GCC_WDOG_DEBUG_DEBUG_TIMER_VAL_BMSK                                                          0xffff
#define HWIO_GCC_WDOG_DEBUG_DEBUG_TIMER_VAL_SHFT                                                             0x0

#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001784)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_RMSK                                                               0xffff
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_IN          \
        in_dword_masked(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR, HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_RMSK)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_INM(m)      \
        in_dword_masked(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR, m)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_OUT(v)      \
        out_dword(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR,v)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR,m,v,HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_IN)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_FLUSH_ETR_DEBUG_TIMER_VAL_BMSK                                     0xffff
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_FLUSH_ETR_DEBUG_TIMER_VAL_SHFT                                        0x0

#define HWIO_GCC_RESET_STATUS_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x000017c0)
#define HWIO_GCC_RESET_STATUS_RMSK                                                                           0xf
#define HWIO_GCC_RESET_STATUS_IN          \
        in_dword_masked(HWIO_GCC_RESET_STATUS_ADDR, HWIO_GCC_RESET_STATUS_RMSK)
#define HWIO_GCC_RESET_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_RESET_STATUS_ADDR, m)
#define HWIO_GCC_RESET_STATUS_OUT(v)      \
        out_dword(HWIO_GCC_RESET_STATUS_ADDR,v)
#define HWIO_GCC_RESET_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RESET_STATUS_ADDR,m,v,HWIO_GCC_RESET_STATUS_IN)
#define HWIO_GCC_RESET_STATUS_TSENSE_RESET_STATUS_BMSK                                                       0x8
#define HWIO_GCC_RESET_STATUS_TSENSE_RESET_STATUS_SHFT                                                       0x3
#define HWIO_GCC_RESET_STATUS_SRST_STATUS_BMSK                                                               0x4
#define HWIO_GCC_RESET_STATUS_SRST_STATUS_SHFT                                                               0x2
#define HWIO_GCC_RESET_STATUS_WDOG_RESET_STATUS_BMSK                                                         0x3
#define HWIO_GCC_RESET_STATUS_WDOG_RESET_STATUS_SHFT                                                         0x0

#define HWIO_GCC_SW_SRST_ADDR                                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00001800)
#define HWIO_GCC_SW_SRST_RMSK                                                                                0x1
#define HWIO_GCC_SW_SRST_IN          \
        in_dword_masked(HWIO_GCC_SW_SRST_ADDR, HWIO_GCC_SW_SRST_RMSK)
#define HWIO_GCC_SW_SRST_INM(m)      \
        in_dword_masked(HWIO_GCC_SW_SRST_ADDR, m)
#define HWIO_GCC_SW_SRST_OUT(v)      \
        out_dword(HWIO_GCC_SW_SRST_ADDR,v)
#define HWIO_GCC_SW_SRST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SW_SRST_ADDR,m,v,HWIO_GCC_SW_SRST_IN)
#define HWIO_GCC_SW_SRST_SW_SRST_BMSK                                                                        0x1
#define HWIO_GCC_SW_SRST_SW_SRST_SHFT                                                                        0x0

#define HWIO_GCC_PROC_HALT_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00001840)
#define HWIO_GCC_PROC_HALT_RMSK                                                                              0x1
#define HWIO_GCC_PROC_HALT_IN          \
        in_dword_masked(HWIO_GCC_PROC_HALT_ADDR, HWIO_GCC_PROC_HALT_RMSK)
#define HWIO_GCC_PROC_HALT_INM(m)      \
        in_dword_masked(HWIO_GCC_PROC_HALT_ADDR, m)
#define HWIO_GCC_PROC_HALT_OUT(v)      \
        out_dword(HWIO_GCC_PROC_HALT_ADDR,v)
#define HWIO_GCC_PROC_HALT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PROC_HALT_ADDR,m,v,HWIO_GCC_PROC_HALT_IN)
#define HWIO_GCC_PROC_HALT_PROC_HALT_BMSK                                                                    0x1
#define HWIO_GCC_PROC_HALT_PROC_HALT_SHFT                                                                    0x0

#define HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00001880)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_RMSK                                                                  0x1f1ff
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_IN          \
        in_dword_masked(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR, HWIO_GCC_GCC_DEBUG_CLK_CTL_RMSK)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR, m)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR,v)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR,m,v,HWIO_GCC_GCC_DEBUG_CLK_CTL_IN)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_CLK_ENABLE_BMSK                                                       0x10000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_CLK_ENABLE_SHFT                                                          0x10
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_POST_DIV_BMSK                                                          0xf000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_POST_DIV_SHFT                                                             0xc
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_MUX_SEL_BMSK                                                            0x1ff
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_MUX_SEL_SHFT                                                              0x0

#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001884)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_RMSK                                                             0x1fffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_IN          \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR, HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_RMSK)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR, m)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_OUT(v)      \
        out_dword(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR,v)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR,m,v,HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_IN)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_CNT_EN_BMSK                                                      0x100000
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_CNT_EN_SHFT                                                          0x14
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_XO_DIV4_TERM_CNT_BMSK                                             0xfffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_XO_DIV4_TERM_CNT_SHFT                                                 0x0

#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001888)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_RMSK                                                         0x3ffffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_IN          \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR, HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_RMSK)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR, m)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_XO_DIV4_CNT_DONE_BMSK                                        0x2000000
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_XO_DIV4_CNT_DONE_SHFT                                             0x19
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_MEASURE_CNT_BMSK                                             0x1ffffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_MEASURE_CNT_SHFT                                                   0x0

#define HWIO_GCC_PLLTEST_PAD_CFG_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0000188c)
#define HWIO_GCC_PLLTEST_PAD_CFG_RMSK                                                                    0xf3f1f
#define HWIO_GCC_PLLTEST_PAD_CFG_IN          \
        in_dword_masked(HWIO_GCC_PLLTEST_PAD_CFG_ADDR, HWIO_GCC_PLLTEST_PAD_CFG_RMSK)
#define HWIO_GCC_PLLTEST_PAD_CFG_INM(m)      \
        in_dword_masked(HWIO_GCC_PLLTEST_PAD_CFG_ADDR, m)
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT(v)      \
        out_dword(HWIO_GCC_PLLTEST_PAD_CFG_ADDR,v)
#define HWIO_GCC_PLLTEST_PAD_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PLLTEST_PAD_CFG_ADDR,m,v,HWIO_GCC_PLLTEST_PAD_CFG_IN)
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_N_BMSK                                                        0xc0000
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_N_SHFT                                                           0x12
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_P_BMSK                                                        0x30000
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_P_SHFT                                                           0x10
#define HWIO_GCC_PLLTEST_PAD_CFG_RT_EN_BMSK                                                               0x2000
#define HWIO_GCC_PLLTEST_PAD_CFG_RT_EN_SHFT                                                                  0xd
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_OE_BMSK                                                             0x1000
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_OE_SHFT                                                                0xc
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_N_BMSK                                                            0x800
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_N_SHFT                                                              0xb
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_P_BMSK                                                            0x400
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_P_SHFT                                                              0xa
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_DRIVE_BMSK                                                           0x300
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_DRIVE_SHFT                                                             0x8
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT_SEL_BMSK                                                               0x1f
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT_SEL_SHFT                                                                0x0

#define HWIO_GCC_JITTER_PROBE_CFG_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00001890)
#define HWIO_GCC_JITTER_PROBE_CFG_RMSK                                                                     0x1ff
#define HWIO_GCC_JITTER_PROBE_CFG_IN          \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_CFG_ADDR, HWIO_GCC_JITTER_PROBE_CFG_RMSK)
#define HWIO_GCC_JITTER_PROBE_CFG_INM(m)      \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_CFG_ADDR, m)
#define HWIO_GCC_JITTER_PROBE_CFG_OUT(v)      \
        out_dword(HWIO_GCC_JITTER_PROBE_CFG_ADDR,v)
#define HWIO_GCC_JITTER_PROBE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_JITTER_PROBE_CFG_ADDR,m,v,HWIO_GCC_JITTER_PROBE_CFG_IN)
#define HWIO_GCC_JITTER_PROBE_CFG_JITTER_PROBE_EN_BMSK                                                     0x100
#define HWIO_GCC_JITTER_PROBE_CFG_JITTER_PROBE_EN_SHFT                                                       0x8
#define HWIO_GCC_JITTER_PROBE_CFG_INIT_COUNTER_BMSK                                                         0xff
#define HWIO_GCC_JITTER_PROBE_CFG_INIT_COUNTER_SHFT                                                          0x0

#define HWIO_GCC_JITTER_PROBE_VAL_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00001894)
#define HWIO_GCC_JITTER_PROBE_VAL_RMSK                                                                      0xff
#define HWIO_GCC_JITTER_PROBE_VAL_IN          \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_VAL_ADDR, HWIO_GCC_JITTER_PROBE_VAL_RMSK)
#define HWIO_GCC_JITTER_PROBE_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_VAL_ADDR, m)
#define HWIO_GCC_JITTER_PROBE_VAL_COUNT_VALUE_BMSK                                                          0xff
#define HWIO_GCC_JITTER_PROBE_VAL_COUNT_VALUE_SHFT                                                           0x0

#define HWIO_GCC_GP1_CBCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001900)
#define HWIO_GCC_GP1_CBCR_RMSK                                                                        0x80000001
#define HWIO_GCC_GP1_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CBCR_ADDR, HWIO_GCC_GP1_CBCR_RMSK)
#define HWIO_GCC_GP1_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CBCR_ADDR, m)
#define HWIO_GCC_GP1_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CBCR_ADDR,v)
#define HWIO_GCC_GP1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CBCR_ADDR,m,v,HWIO_GCC_GP1_CBCR_IN)
#define HWIO_GCC_GP1_CBCR_CLK_OFF_BMSK                                                                0x80000000
#define HWIO_GCC_GP1_CBCR_CLK_OFF_SHFT                                                                      0x1f
#define HWIO_GCC_GP1_CBCR_CLK_ENABLE_BMSK                                                                    0x1
#define HWIO_GCC_GP1_CBCR_CLK_ENABLE_SHFT                                                                    0x0

#define HWIO_GCC_GP1_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001904)
#define HWIO_GCC_GP1_CMD_RCGR_RMSK                                                                    0x800000f3
#define HWIO_GCC_GP1_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CMD_RCGR_ADDR, HWIO_GCC_GP1_CMD_RCGR_RMSK)
#define HWIO_GCC_GP1_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP1_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP1_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP1_CMD_RCGR_IN)
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_D_BMSK                                                                  0x80
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_D_SHFT                                                                   0x7
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_M_BMSK                                                                  0x40
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_M_SHFT                                                                   0x6
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_N_BMSK                                                                  0x20
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_N_SHFT                                                                   0x5
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_GP1_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_GP1_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_GP1_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001908)
#define HWIO_GCC_GP1_CFG_RCGR_RMSK                                                                        0x371f
#define HWIO_GCC_GP1_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CFG_RCGR_ADDR, HWIO_GCC_GP1_CFG_RCGR_RMSK)
#define HWIO_GCC_GP1_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP1_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP1_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP1_CFG_RCGR_IN)
#define HWIO_GCC_GP1_CFG_RCGR_MODE_BMSK                                                                   0x3000
#define HWIO_GCC_GP1_CFG_RCGR_MODE_SHFT                                                                      0xc
#define HWIO_GCC_GP1_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_GP1_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_GP1_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_GP1_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_GP1_M_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000190c)
#define HWIO_GCC_GP1_M_RMSK                                                                                 0xff
#define HWIO_GCC_GP1_M_IN          \
        in_dword_masked(HWIO_GCC_GP1_M_ADDR, HWIO_GCC_GP1_M_RMSK)
#define HWIO_GCC_GP1_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_M_ADDR, m)
#define HWIO_GCC_GP1_M_OUT(v)      \
        out_dword(HWIO_GCC_GP1_M_ADDR,v)
#define HWIO_GCC_GP1_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_M_ADDR,m,v,HWIO_GCC_GP1_M_IN)
#define HWIO_GCC_GP1_M_M_BMSK                                                                               0xff
#define HWIO_GCC_GP1_M_M_SHFT                                                                                0x0

#define HWIO_GCC_GP1_N_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001910)
#define HWIO_GCC_GP1_N_RMSK                                                                                 0xff
#define HWIO_GCC_GP1_N_IN          \
        in_dword_masked(HWIO_GCC_GP1_N_ADDR, HWIO_GCC_GP1_N_RMSK)
#define HWIO_GCC_GP1_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_N_ADDR, m)
#define HWIO_GCC_GP1_N_OUT(v)      \
        out_dword(HWIO_GCC_GP1_N_ADDR,v)
#define HWIO_GCC_GP1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_N_ADDR,m,v,HWIO_GCC_GP1_N_IN)
#define HWIO_GCC_GP1_N_N_BMSK                                                                               0xff
#define HWIO_GCC_GP1_N_N_SHFT                                                                                0x0

#define HWIO_GCC_GP1_D_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001914)
#define HWIO_GCC_GP1_D_RMSK                                                                                 0xff
#define HWIO_GCC_GP1_D_IN          \
        in_dword_masked(HWIO_GCC_GP1_D_ADDR, HWIO_GCC_GP1_D_RMSK)
#define HWIO_GCC_GP1_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_D_ADDR, m)
#define HWIO_GCC_GP1_D_OUT(v)      \
        out_dword(HWIO_GCC_GP1_D_ADDR,v)
#define HWIO_GCC_GP1_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_D_ADDR,m,v,HWIO_GCC_GP1_D_IN)
#define HWIO_GCC_GP1_D_D_BMSK                                                                               0xff
#define HWIO_GCC_GP1_D_D_SHFT                                                                                0x0

#define HWIO_GCC_GP2_CBCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001940)
#define HWIO_GCC_GP2_CBCR_RMSK                                                                        0x80000001
#define HWIO_GCC_GP2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CBCR_ADDR, HWIO_GCC_GP2_CBCR_RMSK)
#define HWIO_GCC_GP2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CBCR_ADDR, m)
#define HWIO_GCC_GP2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CBCR_ADDR,v)
#define HWIO_GCC_GP2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CBCR_ADDR,m,v,HWIO_GCC_GP2_CBCR_IN)
#define HWIO_GCC_GP2_CBCR_CLK_OFF_BMSK                                                                0x80000000
#define HWIO_GCC_GP2_CBCR_CLK_OFF_SHFT                                                                      0x1f
#define HWIO_GCC_GP2_CBCR_CLK_ENABLE_BMSK                                                                    0x1
#define HWIO_GCC_GP2_CBCR_CLK_ENABLE_SHFT                                                                    0x0

#define HWIO_GCC_GP2_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001944)
#define HWIO_GCC_GP2_CMD_RCGR_RMSK                                                                    0x800000f3
#define HWIO_GCC_GP2_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CMD_RCGR_ADDR, HWIO_GCC_GP2_CMD_RCGR_RMSK)
#define HWIO_GCC_GP2_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP2_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP2_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP2_CMD_RCGR_IN)
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_D_BMSK                                                                  0x80
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_D_SHFT                                                                   0x7
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_M_BMSK                                                                  0x40
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_M_SHFT                                                                   0x6
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_N_BMSK                                                                  0x20
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_N_SHFT                                                                   0x5
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_GP2_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_GP2_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_GP2_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001948)
#define HWIO_GCC_GP2_CFG_RCGR_RMSK                                                                        0x371f
#define HWIO_GCC_GP2_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CFG_RCGR_ADDR, HWIO_GCC_GP2_CFG_RCGR_RMSK)
#define HWIO_GCC_GP2_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP2_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP2_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP2_CFG_RCGR_IN)
#define HWIO_GCC_GP2_CFG_RCGR_MODE_BMSK                                                                   0x3000
#define HWIO_GCC_GP2_CFG_RCGR_MODE_SHFT                                                                      0xc
#define HWIO_GCC_GP2_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_GP2_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_GP2_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_GP2_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_GP2_M_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000194c)
#define HWIO_GCC_GP2_M_RMSK                                                                                 0xff
#define HWIO_GCC_GP2_M_IN          \
        in_dword_masked(HWIO_GCC_GP2_M_ADDR, HWIO_GCC_GP2_M_RMSK)
#define HWIO_GCC_GP2_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_M_ADDR, m)
#define HWIO_GCC_GP2_M_OUT(v)      \
        out_dword(HWIO_GCC_GP2_M_ADDR,v)
#define HWIO_GCC_GP2_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_M_ADDR,m,v,HWIO_GCC_GP2_M_IN)
#define HWIO_GCC_GP2_M_M_BMSK                                                                               0xff
#define HWIO_GCC_GP2_M_M_SHFT                                                                                0x0

#define HWIO_GCC_GP2_N_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001950)
#define HWIO_GCC_GP2_N_RMSK                                                                                 0xff
#define HWIO_GCC_GP2_N_IN          \
        in_dword_masked(HWIO_GCC_GP2_N_ADDR, HWIO_GCC_GP2_N_RMSK)
#define HWIO_GCC_GP2_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_N_ADDR, m)
#define HWIO_GCC_GP2_N_OUT(v)      \
        out_dword(HWIO_GCC_GP2_N_ADDR,v)
#define HWIO_GCC_GP2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_N_ADDR,m,v,HWIO_GCC_GP2_N_IN)
#define HWIO_GCC_GP2_N_N_BMSK                                                                               0xff
#define HWIO_GCC_GP2_N_N_SHFT                                                                                0x0

#define HWIO_GCC_GP2_D_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001954)
#define HWIO_GCC_GP2_D_RMSK                                                                                 0xff
#define HWIO_GCC_GP2_D_IN          \
        in_dword_masked(HWIO_GCC_GP2_D_ADDR, HWIO_GCC_GP2_D_RMSK)
#define HWIO_GCC_GP2_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_D_ADDR, m)
#define HWIO_GCC_GP2_D_OUT(v)      \
        out_dword(HWIO_GCC_GP2_D_ADDR,v)
#define HWIO_GCC_GP2_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_D_ADDR,m,v,HWIO_GCC_GP2_D_IN)
#define HWIO_GCC_GP2_D_D_BMSK                                                                               0xff
#define HWIO_GCC_GP2_D_D_SHFT                                                                                0x0

#define HWIO_GCC_GP3_CBCR_ADDR                                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00001980)
#define HWIO_GCC_GP3_CBCR_RMSK                                                                        0x80000001
#define HWIO_GCC_GP3_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CBCR_ADDR, HWIO_GCC_GP3_CBCR_RMSK)
#define HWIO_GCC_GP3_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CBCR_ADDR, m)
#define HWIO_GCC_GP3_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CBCR_ADDR,v)
#define HWIO_GCC_GP3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CBCR_ADDR,m,v,HWIO_GCC_GP3_CBCR_IN)
#define HWIO_GCC_GP3_CBCR_CLK_OFF_BMSK                                                                0x80000000
#define HWIO_GCC_GP3_CBCR_CLK_OFF_SHFT                                                                      0x1f
#define HWIO_GCC_GP3_CBCR_CLK_ENABLE_BMSK                                                                    0x1
#define HWIO_GCC_GP3_CBCR_CLK_ENABLE_SHFT                                                                    0x0

#define HWIO_GCC_GP3_CMD_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001984)
#define HWIO_GCC_GP3_CMD_RCGR_RMSK                                                                    0x800000f3
#define HWIO_GCC_GP3_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CMD_RCGR_ADDR, HWIO_GCC_GP3_CMD_RCGR_RMSK)
#define HWIO_GCC_GP3_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP3_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP3_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP3_CMD_RCGR_IN)
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_D_BMSK                                                                  0x80
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_D_SHFT                                                                   0x7
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_M_BMSK                                                                  0x40
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_M_SHFT                                                                   0x6
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_N_BMSK                                                                  0x20
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_N_SHFT                                                                   0x5
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                           0x10
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                            0x4
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_EN_BMSK                                                                   0x2
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_EN_SHFT                                                                   0x1
#define HWIO_GCC_GP3_CMD_RCGR_UPDATE_BMSK                                                                    0x1
#define HWIO_GCC_GP3_CMD_RCGR_UPDATE_SHFT                                                                    0x0

#define HWIO_GCC_GP3_CFG_RCGR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00001988)
#define HWIO_GCC_GP3_CFG_RCGR_RMSK                                                                        0x371f
#define HWIO_GCC_GP3_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CFG_RCGR_ADDR, HWIO_GCC_GP3_CFG_RCGR_RMSK)
#define HWIO_GCC_GP3_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP3_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP3_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP3_CFG_RCGR_IN)
#define HWIO_GCC_GP3_CFG_RCGR_MODE_BMSK                                                                   0x3000
#define HWIO_GCC_GP3_CFG_RCGR_MODE_SHFT                                                                      0xc
#define HWIO_GCC_GP3_CFG_RCGR_SRC_SEL_BMSK                                                                 0x700
#define HWIO_GCC_GP3_CFG_RCGR_SRC_SEL_SHFT                                                                   0x8
#define HWIO_GCC_GP3_CFG_RCGR_SRC_DIV_BMSK                                                                  0x1f
#define HWIO_GCC_GP3_CFG_RCGR_SRC_DIV_SHFT                                                                   0x0

#define HWIO_GCC_GP3_M_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000198c)
#define HWIO_GCC_GP3_M_RMSK                                                                                 0xff
#define HWIO_GCC_GP3_M_IN          \
        in_dword_masked(HWIO_GCC_GP3_M_ADDR, HWIO_GCC_GP3_M_RMSK)
#define HWIO_GCC_GP3_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_M_ADDR, m)
#define HWIO_GCC_GP3_M_OUT(v)      \
        out_dword(HWIO_GCC_GP3_M_ADDR,v)
#define HWIO_GCC_GP3_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_M_ADDR,m,v,HWIO_GCC_GP3_M_IN)
#define HWIO_GCC_GP3_M_M_BMSK                                                                               0xff
#define HWIO_GCC_GP3_M_M_SHFT                                                                                0x0

#define HWIO_GCC_GP3_N_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001990)
#define HWIO_GCC_GP3_N_RMSK                                                                                 0xff
#define HWIO_GCC_GP3_N_IN          \
        in_dword_masked(HWIO_GCC_GP3_N_ADDR, HWIO_GCC_GP3_N_RMSK)
#define HWIO_GCC_GP3_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_N_ADDR, m)
#define HWIO_GCC_GP3_N_OUT(v)      \
        out_dword(HWIO_GCC_GP3_N_ADDR,v)
#define HWIO_GCC_GP3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_N_ADDR,m,v,HWIO_GCC_GP3_N_IN)
#define HWIO_GCC_GP3_N_N_BMSK                                                                               0xff
#define HWIO_GCC_GP3_N_N_SHFT                                                                                0x0

#define HWIO_GCC_GP3_D_ADDR                                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00001994)
#define HWIO_GCC_GP3_D_RMSK                                                                                 0xff
#define HWIO_GCC_GP3_D_IN          \
        in_dword_masked(HWIO_GCC_GP3_D_ADDR, HWIO_GCC_GP3_D_RMSK)
#define HWIO_GCC_GP3_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_D_ADDR, m)
#define HWIO_GCC_GP3_D_OUT(v)      \
        out_dword(HWIO_GCC_GP3_D_ADDR,v)
#define HWIO_GCC_GP3_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_D_ADDR,m,v,HWIO_GCC_GP3_D_IN)
#define HWIO_GCC_GP3_D_D_BMSK                                                                               0xff
#define HWIO_GCC_GP3_D_D_SHFT                                                                                0x0

#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x000019c0)
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_RMSK                                                                    0x1
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_GCC_KPSS_BOOT_CLOCK_CTL_ADDR, HWIO_GCC_KPSS_BOOT_CLOCK_CTL_RMSK)
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_KPSS_BOOT_CLOCK_CTL_ADDR, m)
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_KPSS_BOOT_CLOCK_CTL_ADDR,v)
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_KPSS_BOOT_CLOCK_CTL_ADDR,m,v,HWIO_GCC_KPSS_BOOT_CLOCK_CTL_IN)
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_KPSS_BOOT_CLOCK_CTL_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001a00)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_RMSK                                                                     0x1
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR, HWIO_GCC_USB_BOOT_CLOCK_CTL_RMSK)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR, m)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR,v)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR,m,v,HWIO_GCC_USB_BOOT_CLOCK_CTL_IN)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001fc0)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_RMSK                                                                     0x1
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_IN          \
        in_dword_masked(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR, HWIO_GCC_RAW_SLEEP_CLK_CTRL_RMSK)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR, m)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR,v)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR,m,v,HWIO_GCC_RAW_SLEEP_CLK_CTRL_IN)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_GATING_DISABLE_BMSK                                                      0x1
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_GATING_DISABLE_SHFT                                                      0x0

/*----------------------------------------------------------------------------
 * MODULE: MSS_PERPH
 *--------------------------------------------------------------------------*/

#define MSS_PERPH_REG_BASE                                                (MSS_TOP_BASE      + 0x00180000)

#define HWIO_MSS_ENABLE_ADDR                                              (MSS_PERPH_REG_BASE      + 0x00000000)
#define HWIO_MSS_ENABLE_RMSK                                                   0x1ff
#define HWIO_MSS_ENABLE_IN          \
        in_dword_masked(HWIO_MSS_ENABLE_ADDR, HWIO_MSS_ENABLE_RMSK)
#define HWIO_MSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_MSS_ENABLE_ADDR, m)
#define HWIO_MSS_ENABLE_OUT(v)      \
        out_dword(HWIO_MSS_ENABLE_ADDR,v)
#define HWIO_MSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ENABLE_ADDR,m,v,HWIO_MSS_ENABLE_IN)
#define HWIO_MSS_ENABLE_RESERVE_BIT_8_BMSK                                     0x100
#define HWIO_MSS_ENABLE_RESERVE_BIT_8_SHFT                                       0x8
#define HWIO_MSS_ENABLE_COXM_BMSK                                               0x80
#define HWIO_MSS_ENABLE_COXM_SHFT                                                0x7
#define HWIO_MSS_ENABLE_RESERVE_BIT_6_BMSK                                      0x40
#define HWIO_MSS_ENABLE_RESERVE_BIT_6_SHFT                                       0x6
#define HWIO_MSS_ENABLE_CRYPTO5_BMSK                                            0x20
#define HWIO_MSS_ENABLE_CRYPTO5_SHFT                                             0x5
#define HWIO_MSS_ENABLE_UIM1_BMSK                                               0x10
#define HWIO_MSS_ENABLE_UIM1_SHFT                                                0x4
#define HWIO_MSS_ENABLE_UIM0_BMSK                                                0x8
#define HWIO_MSS_ENABLE_UIM0_SHFT                                                0x3
#define HWIO_MSS_ENABLE_NAV_BMSK                                                 0x4
#define HWIO_MSS_ENABLE_NAV_SHFT                                                 0x2
#define HWIO_MSS_ENABLE_Q6_BMSK                                                  0x2
#define HWIO_MSS_ENABLE_Q6_SHFT                                                  0x1
#define HWIO_MSS_ENABLE_MODEM_BMSK                                               0x1
#define HWIO_MSS_ENABLE_MODEM_SHFT                                               0x0

#define HWIO_MSS_CLAMP_MEM_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00000004)
#define HWIO_MSS_CLAMP_MEM_RMSK                                                  0x7
#define HWIO_MSS_CLAMP_MEM_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, HWIO_MSS_CLAMP_MEM_RMSK)
#define HWIO_MSS_CLAMP_MEM_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, m)
#define HWIO_MSS_CLAMP_MEM_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_MEM_ADDR,v)
#define HWIO_MSS_CLAMP_MEM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_MEM_ADDR,m,v,HWIO_MSS_CLAMP_MEM_IN)
#define HWIO_MSS_CLAMP_MEM_SPARE_BMSK                                            0x4
#define HWIO_MSS_CLAMP_MEM_SPARE_SHFT                                            0x2
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_BMSK                                      0x2
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_SHFT                                      0x1
#define HWIO_MSS_CLAMP_MEM_HM_CLAMP_BMSK                                         0x1
#define HWIO_MSS_CLAMP_MEM_HM_CLAMP_SHFT                                         0x0

#define HWIO_MSS_CLAMP_IO_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00000008)
#define HWIO_MSS_CLAMP_IO_RMSK                                                  0xff
#define HWIO_MSS_CLAMP_IO_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, HWIO_MSS_CLAMP_IO_RMSK)
#define HWIO_MSS_CLAMP_IO_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, m)
#define HWIO_MSS_CLAMP_IO_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_IO_ADDR,v)
#define HWIO_MSS_CLAMP_IO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_IO_ADDR,m,v,HWIO_MSS_CLAMP_IO_IN)
#define HWIO_MSS_CLAMP_IO_SPARE_7_BMSK                                          0x80
#define HWIO_MSS_CLAMP_IO_SPARE_7_SHFT                                           0x7
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_BMSK                                      0x40
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_SHFT                                       0x6
#define HWIO_MSS_CLAMP_IO_CPR_BYP_MUX_SEL_Q6_BMSK                               0x20
#define HWIO_MSS_CLAMP_IO_CPR_BYP_MUX_SEL_Q6_SHFT                                0x5
#define HWIO_MSS_CLAMP_IO_BBRX_ADC_BMSK                                         0x10
#define HWIO_MSS_CLAMP_IO_BBRX_ADC_SHFT                                          0x4
#define HWIO_MSS_CLAMP_IO_GNSS_DAC_BMSK                                          0x8
#define HWIO_MSS_CLAMP_IO_GNSS_DAC_SHFT                                          0x3
#define HWIO_MSS_CLAMP_IO_COM_COMP_BMSK                                          0x4
#define HWIO_MSS_CLAMP_IO_COM_COMP_SHFT                                          0x2
#define HWIO_MSS_CLAMP_IO_NC_HM_BMSK                                             0x2
#define HWIO_MSS_CLAMP_IO_NC_HM_SHFT                                             0x1
#define HWIO_MSS_CLAMP_IO_MODEM_BMSK                                             0x1
#define HWIO_MSS_CLAMP_IO_MODEM_SHFT                                             0x0

#define HWIO_MSS_BUS_AHB2AHB_CFG_ADDR                                     (MSS_PERPH_REG_BASE      + 0x0000000c)
#define HWIO_MSS_BUS_AHB2AHB_CFG_RMSK                                            0x3
#define HWIO_MSS_BUS_AHB2AHB_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR, HWIO_MSS_BUS_AHB2AHB_CFG_RMSK)
#define HWIO_MSS_BUS_AHB2AHB_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR, m)
#define HWIO_MSS_BUS_AHB2AHB_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR,v)
#define HWIO_MSS_BUS_AHB2AHB_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR,m,v,HWIO_MSS_BUS_AHB2AHB_CFG_IN)
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_NAV_BMSK                        0x2
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_NAV_SHFT                        0x1
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_BMSK                            0x1
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_SHFT                            0x0

#define HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00000010)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_RMSK                                           0xf
#define HWIO_MSS_BUS_MAXI2AXI_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR, HWIO_MSS_BUS_MAXI2AXI_CFG_RMSK)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR, m)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR,v)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR,m,v,HWIO_MSS_BUS_MAXI2AXI_CFG_IN)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_NAV_AREQPRIORITY_BMSK                    0xc
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_NAV_AREQPRIORITY_SHFT                    0x2
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_CRYPTO_AREQPRIORITY_BMSK                 0x3
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_CRYPTO_AREQPRIORITY_SHFT                 0x0

#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR                                 (MSS_PERPH_REG_BASE      + 0x00000014)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_RMSK                                       0xff
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_IN          \
        in_dword_masked(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR, HWIO_MSS_CUSTOM_MEM_ARRSTBYN_RMSK)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_INM(m)      \
        in_dword_masked(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR, m)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_OUT(v)      \
        out_dword(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR,v)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR,m,v,HWIO_MSS_CUSTOM_MEM_ARRSTBYN_IN)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_CTL_BMSK                                   0xff
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_CTL_SHFT                                    0x0

#define HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR                                  (MSS_PERPH_REG_BASE      + 0x00000018)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_RMSK                                         0x3
#define HWIO_MSS_ANALOG_IP_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR, HWIO_MSS_ANALOG_IP_TEST_CTL_RMSK)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR, m)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR,v)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR,m,v,HWIO_MSS_ANALOG_IP_TEST_CTL_IN)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_IQDATA_EN_BMSK                      0x2
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_IQDATA_EN_SHFT                      0x1
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_Y1Y2_EN_BMSK                        0x1
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_Y1Y2_EN_SHFT                        0x0

#define HWIO_MSS_ATB_ID_ADDR                                              (MSS_PERPH_REG_BASE      + 0x0000001c)
#define HWIO_MSS_ATB_ID_RMSK                                                    0x7f
#define HWIO_MSS_ATB_ID_IN          \
        in_dword_masked(HWIO_MSS_ATB_ID_ADDR, HWIO_MSS_ATB_ID_RMSK)
#define HWIO_MSS_ATB_ID_INM(m)      \
        in_dword_masked(HWIO_MSS_ATB_ID_ADDR, m)
#define HWIO_MSS_ATB_ID_OUT(v)      \
        out_dword(HWIO_MSS_ATB_ID_ADDR,v)
#define HWIO_MSS_ATB_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ATB_ID_ADDR,m,v,HWIO_MSS_ATB_ID_IN)
#define HWIO_MSS_ATB_ID_ATB_ID_BMSK                                             0x7f
#define HWIO_MSS_ATB_ID_ATB_ID_SHFT                                              0x0

#define HWIO_MSS_DBG_BUS_CTL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00000020)
#define HWIO_MSS_DBG_BUS_CTL_RMSK                                            0x7ffff
#define HWIO_MSS_DBG_BUS_CTL_IN          \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, HWIO_MSS_DBG_BUS_CTL_RMSK)
#define HWIO_MSS_DBG_BUS_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, m)
#define HWIO_MSS_DBG_BUS_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DBG_BUS_CTL_ADDR,v)
#define HWIO_MSS_DBG_BUS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DBG_BUS_CTL_ADDR,m,v,HWIO_MSS_DBG_BUS_CTL_IN)
#define HWIO_MSS_DBG_BUS_CTL_BRIC_AXI2AXI_NAV_SEL_BMSK                       0x70000
#define HWIO_MSS_DBG_BUS_CTL_BRIC_AXI2AXI_NAV_SEL_SHFT                          0x10
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_BMSK                             0xe000
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_SHFT                                0xd
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_NAV_CONFIG_SEL_BMSK                      0x1800
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_NAV_CONFIG_SEL_SHFT                         0xb
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_SEL_BMSK                                  0x600
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_SEL_SHFT                                    0x9
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_TOP_SEL_BMSK                          0x1e0
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_TOP_SEL_SHFT                            0x5
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_NC_HM_SEL_BMSK                         0x1c
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_NC_HM_SEL_SHFT                          0x2
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_GPIO_ATB_SEL_BMSK                           0x3
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_GPIO_ATB_SEL_SHFT                           0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000024)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK                                      0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,m,v,HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_BMSK                                   0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_SHFT                                   0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR                           (MSS_PERPH_REG_BASE      + 0x00000028)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK                                  0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_BMSK                           0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_SHFT                           0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR                              (MSS_PERPH_REG_BASE      + 0x0000002c)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_RMSK                                     0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_BMSK                                 0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_SHFT                                 0x0

#define HWIO_MSS_BUS_CTL_CFG_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00000030)
#define HWIO_MSS_BUS_CTL_CFG_RMSK                                                0x1
#define HWIO_MSS_BUS_CTL_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_CTL_CFG_ADDR, HWIO_MSS_BUS_CTL_CFG_RMSK)
#define HWIO_MSS_BUS_CTL_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CTL_CFG_ADDR, m)
#define HWIO_MSS_BUS_CTL_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CTL_CFG_ADDR,v)
#define HWIO_MSS_BUS_CTL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CTL_CFG_ADDR,m,v,HWIO_MSS_BUS_CTL_CFG_IN)
#define HWIO_MSS_BUS_CTL_CFG_Q6_FORCE_UNBUFFERED_BMSK                            0x1
#define HWIO_MSS_BUS_CTL_CFG_Q6_FORCE_UNBUFFERED_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW0_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000034)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, HWIO_MSS_RELAY_MSG_SHADOW0_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW0_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW0_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW1_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000038)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, HWIO_MSS_RELAY_MSG_SHADOW1_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW1_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW1_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW2_ADDR                                   (MSS_PERPH_REG_BASE      + 0x0000003c)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, HWIO_MSS_RELAY_MSG_SHADOW2_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW2_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW2_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW3_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000040)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, HWIO_MSS_RELAY_MSG_SHADOW3_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW3_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW3_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW4_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000044)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, HWIO_MSS_RELAY_MSG_SHADOW4_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW4_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW4_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW5_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000048)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, HWIO_MSS_RELAY_MSG_SHADOW5_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW5_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW5_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW6_ADDR                                   (MSS_PERPH_REG_BASE      + 0x0000004c)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, HWIO_MSS_RELAY_MSG_SHADOW6_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW6_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW6_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW7_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000050)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, HWIO_MSS_RELAY_MSG_SHADOW7_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW7_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW7_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW8_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000054)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, HWIO_MSS_RELAY_MSG_SHADOW8_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW8_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW8_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_SHFT                        0x0

#define HWIO_MSS_MSA_ADDR                                                 (MSS_PERPH_REG_BASE      + 0x00000058)
#define HWIO_MSS_MSA_RMSK                                                        0x3
#define HWIO_MSS_MSA_IN          \
        in_dword_masked(HWIO_MSS_MSA_ADDR, HWIO_MSS_MSA_RMSK)
#define HWIO_MSS_MSA_INM(m)      \
        in_dword_masked(HWIO_MSS_MSA_ADDR, m)
#define HWIO_MSS_MSA_OUT(v)      \
        out_dword(HWIO_MSS_MSA_ADDR,v)
#define HWIO_MSS_MSA_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MSA_ADDR,m,v,HWIO_MSS_MSA_IN)
#define HWIO_MSS_MSA_MBA_OK_BMSK                                                 0x2
#define HWIO_MSS_MSA_MBA_OK_SHFT                                                 0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_BMSK                                            0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_SHFT                                            0x0

#define HWIO_MSS_HW_VERSION_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000005c)
#define HWIO_MSS_HW_VERSION_RMSK                                          0xffffffff
#define HWIO_MSS_HW_VERSION_IN          \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, HWIO_MSS_HW_VERSION_RMSK)
#define HWIO_MSS_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, m)
#define HWIO_MSS_HW_VERSION_MAJOR_BMSK                                    0xf0000000
#define HWIO_MSS_HW_VERSION_MAJOR_SHFT                                          0x1c
#define HWIO_MSS_HW_VERSION_MINOR_BMSK                                     0xfff0000
#define HWIO_MSS_HW_VERSION_MINOR_SHFT                                          0x10
#define HWIO_MSS_HW_VERSION_STEP_BMSK                                         0xffff
#define HWIO_MSS_HW_VERSION_STEP_SHFT                                            0x0

#define HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000060)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_RMSK                                    0x302030f
#define HWIO_MSS_DIME_MEM_SLP_CNTL_IN          \
        in_dword_masked(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR, HWIO_MSS_DIME_MEM_SLP_CNTL_RMSK)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR, m)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_OUT(v)      \
        out_dword(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR,v)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR,m,v,HWIO_MSS_DIME_MEM_SLP_CNTL_IN)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_SPM_BMSK                        0x2000000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_SPM_SHFT                             0x19
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_NOW_BMSK                        0x1000000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_NOW_SHFT                             0x18
#define HWIO_MSS_DIME_MEM_SLP_CNTL_ALL_NR_SLP_NRET_N_BMSK                    0x20000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_ALL_NR_SLP_NRET_N_SHFT                       0x11
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_NRET_N_BMSK                         0x200
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_NRET_N_SHFT                           0x9
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_RET_N_BMSK                          0x100
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_RET_N_SHFT                            0x8
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_NRET_N_BMSK                          0x8
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_NRET_N_SHFT                          0x3
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_RET_N_BMSK                           0x4
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_RET_N_SHFT                           0x2
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_NRET_N_BMSK                          0x2
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_NRET_N_SHFT                          0x1
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_RET_N_BMSK                           0x1
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_RET_N_SHFT                           0x0

#define HWIO_MSS_CLOCK_SPDM_MON_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00000064)
#define HWIO_MSS_CLOCK_SPDM_MON_RMSK                                             0x3
#define HWIO_MSS_CLOCK_SPDM_MON_IN          \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, HWIO_MSS_CLOCK_SPDM_MON_RMSK)
#define HWIO_MSS_CLOCK_SPDM_MON_INM(m)      \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, m)
#define HWIO_MSS_CLOCK_SPDM_MON_OUT(v)      \
        out_dword(HWIO_MSS_CLOCK_SPDM_MON_ADDR,v)
#define HWIO_MSS_CLOCK_SPDM_MON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLOCK_SPDM_MON_ADDR,m,v,HWIO_MSS_CLOCK_SPDM_MON_IN)
#define HWIO_MSS_CLOCK_SPDM_MON_Q6_MON_CLKEN_BMSK                                0x2
#define HWIO_MSS_CLOCK_SPDM_MON_Q6_MON_CLKEN_SHFT                                0x1
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_BMSK                               0x1
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_SHFT                               0x0

#define HWIO_MSS_BBRX0_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00000068)
#define HWIO_MSS_BBRX0_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX0_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_MUX_SEL_ADDR, HWIO_MSS_BBRX0_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX0_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX0_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX0_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX0_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX0_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX0_MUX_SEL_IN)
#define HWIO_MSS_BBRX0_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX0_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX0_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX0_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_BBRX1_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x0000006c)
#define HWIO_MSS_BBRX1_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX1_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_MUX_SEL_ADDR, HWIO_MSS_BBRX1_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX1_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX1_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX1_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX1_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX1_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX1_MUX_SEL_IN)
#define HWIO_MSS_BBRX1_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX1_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX1_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX1_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_DEBUG_CLOCK_CTL_ADDR                                     (MSS_PERPH_REG_BASE      + 0x00000078)
#define HWIO_MSS_DEBUG_CLOCK_CTL_RMSK                                           0x3f
#define HWIO_MSS_DEBUG_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, HWIO_MSS_DEBUG_CLOCK_CTL_RMSK)
#define HWIO_MSS_DEBUG_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CLOCK_CTL_IN)
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_MUX_SEL_BMSK                               0x20
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_MUX_SEL_SHFT                                0x5
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_BMSK                        0x10
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_SHFT                         0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_BMSK                         0x8
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_SHFT                         0x3
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_BMSK                         0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_SHFT                         0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_BMSK                         0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_SHFT                         0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_BMSK                         0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_SHFT                         0x0

#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR                              (MSS_PERPH_REG_BASE      + 0x0000007c)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_RMSK                                     0x3
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR, HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_IN)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_PLLTEST_BMSK                             0x2
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_PLLTEST_SHFT                             0x1
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_EXT_CLOCK_MUX_SEL_BMSK                   0x1
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_EXT_CLOCK_MUX_SEL_SHFT                   0x0

#define HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR                                 (MSS_PERPH_REG_BASE      + 0x00000084)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_RMSK                                       0x7f
#define HWIO_MSS_GNSSADC_BIST_CONFIG_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR, HWIO_MSS_GNSSADC_BIST_CONFIG_RMSK)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_OUT(v)      \
        out_dword(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR,v)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR,m,v,HWIO_MSS_GNSSADC_BIST_CONFIG_IN)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_EXT_CLK_SEL_BMSK                   0x40
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_EXT_CLK_SEL_SHFT                    0x6
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_BIST_EN_BMSK                       0x20
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_BIST_EN_SHFT                        0x5
#define HWIO_MSS_GNSSADC_BIST_CONFIG_HITSIDEAL_BMSK                             0x1f
#define HWIO_MSS_GNSSADC_BIST_CONFIG_HITSIDEAL_SHFT                              0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000088)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_RMSK                                0xfffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_1_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MAX_BMSK              0xff80000
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MAX_SHFT                   0x13
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MIN_BMSK                0x7fc00
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MIN_SHFT                    0xa
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_POS_BMSK                  0x3e0
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_POS_SHFT                    0x5
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_NEG_BMSK                   0x1f
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_NEG_SHFT                    0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR                               (MSS_PERPH_REG_BASE      + 0x0000008c)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_RMSK                               0xffffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_2_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_DNL_Q_POS_BMSK             0xf8000000
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_DNL_Q_POS_SHFT                   0x1b
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_GAIN_I_BMSK                 0x7fc0000
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_GAIN_I_SHFT                      0x12
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MAX_BMSK                0x3fe00
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MAX_SHFT                    0x9
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MIN_BMSK                  0x1ff
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MIN_SHFT                    0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000090)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_RMSK                               0xffffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_3_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_DNL_Q_NEG_BMSK             0xf8000000
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_DNL_Q_NEG_SHFT                   0x1b
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_I_BMSK               0x7fc0000
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_I_SHFT                    0x12
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_GAIN_Q_BMSK                   0x3fe00
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_GAIN_Q_SHFT                       0x9
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_Q_BMSK                   0x1ff
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_Q_SHFT                     0x0

#define HWIO_MSS_BBRX_CTL_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00000094)
#define HWIO_MSS_BBRX_CTL_RMSK                                                   0x3
#define HWIO_MSS_BBRX_CTL_IN          \
        in_dword_masked(HWIO_MSS_BBRX_CTL_ADDR, HWIO_MSS_BBRX_CTL_RMSK)
#define HWIO_MSS_BBRX_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX_CTL_ADDR, m)
#define HWIO_MSS_BBRX_CTL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX_CTL_ADDR,v)
#define HWIO_MSS_BBRX_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX_CTL_ADDR,m,v,HWIO_MSS_BBRX_CTL_IN)
#define HWIO_MSS_BBRX_CTL_BBRX_HS_TEST_MUX_CTL_BMSK                              0x3
#define HWIO_MSS_BBRX_CTL_BBRX_HS_TEST_MUX_CTL_SHFT                              0x0

#define HWIO_MSS_DEBUG_CTL_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00000098)
#define HWIO_MSS_DEBUG_CTL_RMSK                                                  0x7
#define HWIO_MSS_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, HWIO_MSS_DEBUG_CTL_RMSK)
#define HWIO_MSS_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CTL_IN)
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_BMSK                            0x4
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_SHFT                            0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_BMSK                              0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_SHFT                              0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_BMSK                             0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_SHFT                             0x0

#define HWIO_MSS_MPLL1_MODE_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001020)
#define HWIO_MSS_MPLL1_MODE_RMSK                                            0x3fff3f
#define HWIO_MSS_MPLL1_MODE_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MODE_ADDR, HWIO_MSS_MPLL1_MODE_RMSK)
#define HWIO_MSS_MPLL1_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MODE_ADDR, m)
#define HWIO_MSS_MPLL1_MODE_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_MODE_ADDR,v)
#define HWIO_MSS_MPLL1_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_MODE_ADDR,m,v,HWIO_MSS_MPLL1_MODE_IN)
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_RESET_BMSK                         0x200000
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_RESET_SHFT                             0x15
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_ENA_BMSK                           0x100000
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_ENA_SHFT                               0x14
#define HWIO_MSS_MPLL1_MODE_PLL_BIAS_COUNT_BMSK                              0xfc000
#define HWIO_MSS_MPLL1_MODE_PLL_BIAS_COUNT_SHFT                                  0xe
#define HWIO_MSS_MPLL1_MODE_PLL_LOCK_COUNT_BMSK                               0x3f00
#define HWIO_MSS_MPLL1_MODE_PLL_LOCK_COUNT_SHFT                                  0x8
#define HWIO_MSS_MPLL1_MODE_PLL_REF_XO_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL1_MODE_PLL_REF_XO_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL1_MODE_PLL_PLLTEST_BMSK                                     0x8
#define HWIO_MSS_MPLL1_MODE_PLL_PLLTEST_SHFT                                     0x3
#define HWIO_MSS_MPLL1_MODE_PLL_RESET_N_BMSK                                     0x4
#define HWIO_MSS_MPLL1_MODE_PLL_RESET_N_SHFT                                     0x2
#define HWIO_MSS_MPLL1_MODE_PLL_BYPASSNL_BMSK                                    0x2
#define HWIO_MSS_MPLL1_MODE_PLL_BYPASSNL_SHFT                                    0x1
#define HWIO_MSS_MPLL1_MODE_PLL_OUTCTRL_BMSK                                     0x1
#define HWIO_MSS_MPLL1_MODE_PLL_OUTCTRL_SHFT                                     0x0

#define HWIO_MSS_MPLL1_L_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001024)
#define HWIO_MSS_MPLL1_L_VAL_RMSK                                               0x7f
#define HWIO_MSS_MPLL1_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_L_VAL_ADDR, HWIO_MSS_MPLL1_L_VAL_RMSK)
#define HWIO_MSS_MPLL1_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_L_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_L_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_L_VAL_ADDR,m,v,HWIO_MSS_MPLL1_L_VAL_IN)
#define HWIO_MSS_MPLL1_L_VAL_PLL_L_BMSK                                         0x7f
#define HWIO_MSS_MPLL1_L_VAL_PLL_L_SHFT                                          0x0

#define HWIO_MSS_MPLL1_M_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001028)
#define HWIO_MSS_MPLL1_M_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL1_M_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_M_VAL_ADDR, HWIO_MSS_MPLL1_M_VAL_RMSK)
#define HWIO_MSS_MPLL1_M_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_M_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_M_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_M_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_M_VAL_ADDR,m,v,HWIO_MSS_MPLL1_M_VAL_IN)
#define HWIO_MSS_MPLL1_M_VAL_PLL_M_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL1_M_VAL_PLL_M_SHFT                                          0x0

#define HWIO_MSS_MPLL1_N_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x0000102c)
#define HWIO_MSS_MPLL1_N_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL1_N_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_N_VAL_ADDR, HWIO_MSS_MPLL1_N_VAL_RMSK)
#define HWIO_MSS_MPLL1_N_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_N_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_N_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_N_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_N_VAL_ADDR,m,v,HWIO_MSS_MPLL1_N_VAL_IN)
#define HWIO_MSS_MPLL1_N_VAL_PLL_N_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL1_N_VAL_PLL_N_SHFT                                          0x0

#define HWIO_MSS_MPLL1_USER_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001030)
#define HWIO_MSS_MPLL1_USER_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL1_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_USER_CTL_ADDR, HWIO_MSS_MPLL1_USER_CTL_RMSK)
#define HWIO_MSS_MPLL1_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_USER_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_USER_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_USER_CTL_ADDR,m,v,HWIO_MSS_MPLL1_USER_CTL_IN)
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_31_30_BMSK                   0xc0000000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_31_30_SHFT                         0x1e
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_29_28_BMSK                   0x30000000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_29_28_SHFT                         0x1c
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_27_25_BMSK                    0xe000000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_27_25_SHFT                         0x19
#define HWIO_MSS_MPLL1_USER_CTL_MN_EN_BMSK                                 0x1000000
#define HWIO_MSS_MPLL1_USER_CTL_MN_EN_SHFT                                      0x18
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_23_13_BMSK                     0xffe000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_23_13_SHFT                          0xd
#define HWIO_MSS_MPLL1_USER_CTL_PREDIV2_EN_BMSK                               0x1000
#define HWIO_MSS_MPLL1_USER_CTL_PREDIV2_EN_SHFT                                  0xc
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_11_10_BMSK                        0xc00
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_11_10_SHFT                          0xa
#define HWIO_MSS_MPLL1_USER_CTL_POSTDIV_CTL_BMSK                               0x300
#define HWIO_MSS_MPLL1_USER_CTL_POSTDIV_CTL_SHFT                                 0x8
#define HWIO_MSS_MPLL1_USER_CTL_INV_OUTPUT_BMSK                                 0x80
#define HWIO_MSS_MPLL1_USER_CTL_INV_OUTPUT_SHFT                                  0x7
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_6_5_BMSK                            0x60
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_6_5_SHFT                             0x5
#define HWIO_MSS_MPLL1_USER_CTL_LVTEST_EN_BMSK                                  0x10
#define HWIO_MSS_MPLL1_USER_CTL_LVTEST_EN_SHFT                                   0x4
#define HWIO_MSS_MPLL1_USER_CTL_LVEARLY_EN_BMSK                                  0x8
#define HWIO_MSS_MPLL1_USER_CTL_LVEARLY_EN_SHFT                                  0x3
#define HWIO_MSS_MPLL1_USER_CTL_LVBIST_EN_BMSK                                   0x4
#define HWIO_MSS_MPLL1_USER_CTL_LVBIST_EN_SHFT                                   0x2
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX_EN_BMSK                                    0x2
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX_EN_SHFT                                    0x1
#define HWIO_MSS_MPLL1_USER_CTL_LVMAIN_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL1_USER_CTL_LVMAIN_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL1_CONFIG_CTL_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00001034)
#define HWIO_MSS_MPLL1_CONFIG_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_MPLL1_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR, HWIO_MSS_MPLL1_CONFIG_CTL_RMSK)
#define HWIO_MSS_MPLL1_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR,m,v,HWIO_MSS_MPLL1_CONFIG_CTL_IN)
#define HWIO_MSS_MPLL1_CONFIG_CTL_SR2_PLL_FIELDS_BMSK                     0xffffffff
#define HWIO_MSS_MPLL1_CONFIG_CTL_SR2_PLL_FIELDS_SHFT                            0x0

#define HWIO_MSS_MPLL1_TEST_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001038)
#define HWIO_MSS_MPLL1_TEST_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL1_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_TEST_CTL_ADDR, HWIO_MSS_MPLL1_TEST_CTL_RMSK)
#define HWIO_MSS_MPLL1_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_TEST_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_TEST_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_TEST_CTL_ADDR,m,v,HWIO_MSS_MPLL1_TEST_CTL_IN)
#define HWIO_MSS_MPLL1_TEST_CTL_RESERVE_31_10_BMSK                        0xfffffc00
#define HWIO_MSS_MPLL1_TEST_CTL_RESERVE_31_10_SHFT                               0xa
#define HWIO_MSS_MPLL1_TEST_CTL_IEXT_SEL_BMSK                                  0x200
#define HWIO_MSS_MPLL1_TEST_CTL_IEXT_SEL_SHFT                                    0x9
#define HWIO_MSS_MPLL1_TEST_CTL_DTEST_SEL_BMSK                                 0x180
#define HWIO_MSS_MPLL1_TEST_CTL_DTEST_SEL_SHFT                                   0x7
#define HWIO_MSS_MPLL1_TEST_CTL_BYP_TESTAMP_BMSK                                0x40
#define HWIO_MSS_MPLL1_TEST_CTL_BYP_TESTAMP_SHFT                                 0x6
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_SEL_BMSK                                  0xc
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_SEL_SHFT                                  0x2
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_EN_BMSK                                   0x2
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_EN_SHFT                                   0x1
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL1_STATUS_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000103c)
#define HWIO_MSS_MPLL1_STATUS_RMSK                                           0x3ffff
#define HWIO_MSS_MPLL1_STATUS_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_STATUS_ADDR, HWIO_MSS_MPLL1_STATUS_RMSK)
#define HWIO_MSS_MPLL1_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_STATUS_ADDR, m)
#define HWIO_MSS_MPLL1_STATUS_PLL_ACTIVE_FLAG_BMSK                           0x20000
#define HWIO_MSS_MPLL1_STATUS_PLL_ACTIVE_FLAG_SHFT                              0x11
#define HWIO_MSS_MPLL1_STATUS_PLL_LOCK_DET_BMSK                              0x10000
#define HWIO_MSS_MPLL1_STATUS_PLL_LOCK_DET_SHFT                                 0x10
#define HWIO_MSS_MPLL1_STATUS_PLL_D_BMSK                                      0xffff
#define HWIO_MSS_MPLL1_STATUS_PLL_D_SHFT                                         0x0

#define HWIO_MSS_MPLL2_MODE_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001040)
#define HWIO_MSS_MPLL2_MODE_RMSK                                            0x3fff3f
#define HWIO_MSS_MPLL2_MODE_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_MODE_ADDR, HWIO_MSS_MPLL2_MODE_RMSK)
#define HWIO_MSS_MPLL2_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_MODE_ADDR, m)
#define HWIO_MSS_MPLL2_MODE_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_MODE_ADDR,v)
#define HWIO_MSS_MPLL2_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_MODE_ADDR,m,v,HWIO_MSS_MPLL2_MODE_IN)
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_RESET_BMSK                         0x200000
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_RESET_SHFT                             0x15
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_ENA_BMSK                           0x100000
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_ENA_SHFT                               0x14
#define HWIO_MSS_MPLL2_MODE_PLL_BIAS_COUNT_BMSK                              0xfc000
#define HWIO_MSS_MPLL2_MODE_PLL_BIAS_COUNT_SHFT                                  0xe
#define HWIO_MSS_MPLL2_MODE_PLL_LOCK_COUNT_BMSK                               0x3f00
#define HWIO_MSS_MPLL2_MODE_PLL_LOCK_COUNT_SHFT                                  0x8
#define HWIO_MSS_MPLL2_MODE_PLL_REF_XO_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL2_MODE_PLL_REF_XO_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL2_MODE_PLL_PLLTEST_BMSK                                     0x8
#define HWIO_MSS_MPLL2_MODE_PLL_PLLTEST_SHFT                                     0x3
#define HWIO_MSS_MPLL2_MODE_PLL_RESET_N_BMSK                                     0x4
#define HWIO_MSS_MPLL2_MODE_PLL_RESET_N_SHFT                                     0x2
#define HWIO_MSS_MPLL2_MODE_PLL_BYPASSNL_BMSK                                    0x2
#define HWIO_MSS_MPLL2_MODE_PLL_BYPASSNL_SHFT                                    0x1
#define HWIO_MSS_MPLL2_MODE_PLL_OUTCTRL_BMSK                                     0x1
#define HWIO_MSS_MPLL2_MODE_PLL_OUTCTRL_SHFT                                     0x0

#define HWIO_MSS_MPLL2_L_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001044)
#define HWIO_MSS_MPLL2_L_VAL_RMSK                                               0xff
#define HWIO_MSS_MPLL2_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_L_VAL_ADDR, HWIO_MSS_MPLL2_L_VAL_RMSK)
#define HWIO_MSS_MPLL2_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_L_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_L_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_L_VAL_ADDR,m,v,HWIO_MSS_MPLL2_L_VAL_IN)
#define HWIO_MSS_MPLL2_L_VAL_PLL_L_BMSK                                         0xff
#define HWIO_MSS_MPLL2_L_VAL_PLL_L_SHFT                                          0x0

#define HWIO_MSS_MPLL2_M_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001048)
#define HWIO_MSS_MPLL2_M_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL2_M_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_M_VAL_ADDR, HWIO_MSS_MPLL2_M_VAL_RMSK)
#define HWIO_MSS_MPLL2_M_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_M_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_M_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_M_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_M_VAL_ADDR,m,v,HWIO_MSS_MPLL2_M_VAL_IN)
#define HWIO_MSS_MPLL2_M_VAL_PLL_M_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL2_M_VAL_PLL_M_SHFT                                          0x0

#define HWIO_MSS_MPLL2_N_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x0000104c)
#define HWIO_MSS_MPLL2_N_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL2_N_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_N_VAL_ADDR, HWIO_MSS_MPLL2_N_VAL_RMSK)
#define HWIO_MSS_MPLL2_N_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_N_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_N_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_N_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_N_VAL_ADDR,m,v,HWIO_MSS_MPLL2_N_VAL_IN)
#define HWIO_MSS_MPLL2_N_VAL_PLL_N_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL2_N_VAL_PLL_N_SHFT                                          0x0

#define HWIO_MSS_MPLL2_USER_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001050)
#define HWIO_MSS_MPLL2_USER_CTL_RMSK                                      0xfffff3ff
#define HWIO_MSS_MPLL2_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_USER_CTL_ADDR, HWIO_MSS_MPLL2_USER_CTL_RMSK)
#define HWIO_MSS_MPLL2_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_USER_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_USER_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_USER_CTL_ADDR,m,v,HWIO_MSS_MPLL2_USER_CTL_IN)
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_31_25_BMSK                        0xfe000000
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_31_25_SHFT                              0x19
#define HWIO_MSS_MPLL2_USER_CTL_MN_EN_BMSK                                 0x1000000
#define HWIO_MSS_MPLL2_USER_CTL_MN_EN_SHFT                                      0x18
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_23_21_BMSK                          0xe00000
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_23_21_SHFT                              0x15
#define HWIO_MSS_MPLL2_USER_CTL_VCO_SEL_BMSK                                0x100000
#define HWIO_MSS_MPLL2_USER_CTL_VCO_SEL_SHFT                                    0x14
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_19_13_BMSK                           0xfe000
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_19_13_SHFT                               0xd
#define HWIO_MSS_MPLL2_USER_CTL_PREDIV2_EN_BMSK                               0x1000
#define HWIO_MSS_MPLL2_USER_CTL_PREDIV2_EN_SHFT                                  0xc
#define HWIO_MSS_MPLL2_USER_CTL_POSTDIV_CTL_BMSK                               0x300
#define HWIO_MSS_MPLL2_USER_CTL_POSTDIV_CTL_SHFT                                 0x8
#define HWIO_MSS_MPLL2_USER_CTL_OUTPUT_INV_BMSK                                 0x80
#define HWIO_MSS_MPLL2_USER_CTL_OUTPUT_INV_SHFT                                  0x7
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_BIT_6_BMSK                              0x40
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_BIT_6_SHFT                               0x6
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_BIT_5_BMSK                              0x20
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_BIT_5_SHFT                               0x5
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_TEST_BMSK                             0x10
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_TEST_SHFT                              0x4
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_EARLY_BMSK                             0x8
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_EARLY_SHFT                             0x3
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_BIST_BMSK                              0x4
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_BIST_SHFT                              0x2
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX_BMSK                               0x2
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX_SHFT                               0x1
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_MAIN_BMSK                              0x1
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_MAIN_SHFT                              0x0

#define HWIO_MSS_MPLL2_CONFIG_CTL_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00001054)
#define HWIO_MSS_MPLL2_CONFIG_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_MPLL2_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR, HWIO_MSS_MPLL2_CONFIG_CTL_RMSK)
#define HWIO_MSS_MPLL2_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR,m,v,HWIO_MSS_MPLL2_CONFIG_CTL_IN)
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BITS_31_30_BMSK                 0xc0000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BITS_31_30_SHFT                       0x1e
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_PFD_UP_BMSK                       0x20000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_PFD_UP_SHFT                             0x1d
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_PFD_DOWN_BMSK                     0x10000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_PFD_DOWN_SHFT                           0x1c
#define HWIO_MSS_MPLL2_CONFIG_CTL_NMOSC_FREQ_CTRL_BMSK                     0xc000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_NMOSC_FREQ_CTRL_SHFT                          0x1a
#define HWIO_MSS_MPLL2_CONFIG_CTL_PFD_DZSEL_BMSK                           0x3000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_PFD_DZSEL_SHFT                                0x18
#define HWIO_MSS_MPLL2_CONFIG_CTL_NMOSC_EN_BMSK                             0x800000
#define HWIO_MSS_MPLL2_CONFIG_CTL_NMOSC_EN_SHFT                                 0x17
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BIT_22_BMSK                       0x400000
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BIT_22_SHFT                           0x16
#define HWIO_MSS_MPLL2_CONFIG_CTL_ICP_DIV_BMSK                              0x300000
#define HWIO_MSS_MPLL2_CONFIG_CTL_ICP_DIV_SHFT                                  0x14
#define HWIO_MSS_MPLL2_CONFIG_CTL_IREG_DIV_BMSK                              0xc0000
#define HWIO_MSS_MPLL2_CONFIG_CTL_IREG_DIV_SHFT                                 0x12
#define HWIO_MSS_MPLL2_CONFIG_CTL_CUSEL_BMSK                                 0x30000
#define HWIO_MSS_MPLL2_CONFIG_CTL_CUSEL_SHFT                                    0x10
#define HWIO_MSS_MPLL2_CONFIG_CTL_REF_MODE_BMSK                               0x8000
#define HWIO_MSS_MPLL2_CONFIG_CTL_REF_MODE_SHFT                                  0xf
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BIT_14_BMSK                         0x4000
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BIT_14_SHFT                            0xe
#define HWIO_MSS_MPLL2_CONFIG_CTL_CFG_LOCKDET_BMSK                            0x3000
#define HWIO_MSS_MPLL2_CONFIG_CTL_CFG_LOCKDET_SHFT                               0xc
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_ISEED_BMSK                             0x800
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_ISEED_SHFT                               0xb
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BITS_10_0_BMSK                       0x7ff
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_BITS_10_0_SHFT                         0x0

#define HWIO_MSS_MPLL2_TEST_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001058)
#define HWIO_MSS_MPLL2_TEST_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL2_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_TEST_CTL_ADDR, HWIO_MSS_MPLL2_TEST_CTL_RMSK)
#define HWIO_MSS_MPLL2_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_TEST_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_TEST_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_TEST_CTL_ADDR,m,v,HWIO_MSS_MPLL2_TEST_CTL_IN)
#define HWIO_MSS_MPLL2_TEST_CTL_RESERVE_31_21_BMSK                        0xffe00000
#define HWIO_MSS_MPLL2_TEST_CTL_RESERVE_31_21_SHFT                              0x15
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_CFG_BMSK                               0x1c0000
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_CFG_SHFT                                   0x12
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_EN_BMSK                                 0x20000
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_EN_SHFT                                    0x11
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_FREQ_CTRL_BMSK                         0x18000
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_FREQ_CTRL_SHFT                             0xf
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_EN_BMSK                                 0x4000
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_EN_SHFT                                    0xe
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_UP_BMSK                             0x2000
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_UP_SHFT                                0xd
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_DOWN_BMSK                           0x1000
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_DOWN_SHFT                              0xc
#define HWIO_MSS_MPLL2_TEST_CTL_TEST_OUT_SEL_BMSK                              0x800
#define HWIO_MSS_MPLL2_TEST_CTL_TEST_OUT_SEL_SHFT                                0xb
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_TST_EN_BMSK                                0x400
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_TST_EN_SHFT                                  0xa
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_EXT_SEL_BMSK                               0x200
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_EXT_SEL_SHFT                                 0x9
#define HWIO_MSS_MPLL2_TEST_CTL_DTEST_SEL_BMSK                                 0x180
#define HWIO_MSS_MPLL2_TEST_CTL_DTEST_SEL_SHFT                                   0x7
#define HWIO_MSS_MPLL2_TEST_CTL_BYP_TESTAMP_BMSK                                0x40
#define HWIO_MSS_MPLL2_TEST_CTL_BYP_TESTAMP_SHFT                                 0x6
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_SEL_BMSK                                  0xc
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_SEL_SHFT                                  0x2
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_EN_BMSK                                   0x2
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_EN_SHFT                                   0x1
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL2_STATUS_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000105c)
#define HWIO_MSS_MPLL2_STATUS_RMSK                                           0x3ffff
#define HWIO_MSS_MPLL2_STATUS_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_STATUS_ADDR, HWIO_MSS_MPLL2_STATUS_RMSK)
#define HWIO_MSS_MPLL2_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_STATUS_ADDR, m)
#define HWIO_MSS_MPLL2_STATUS_PLL_ACTIVE_FLAG_BMSK                           0x20000
#define HWIO_MSS_MPLL2_STATUS_PLL_ACTIVE_FLAG_SHFT                              0x11
#define HWIO_MSS_MPLL2_STATUS_PLL_LOCK_DET_BMSK                              0x10000
#define HWIO_MSS_MPLL2_STATUS_PLL_LOCK_DET_SHFT                                 0x10
#define HWIO_MSS_MPLL2_STATUS_PLL_D_BMSK                                      0xffff
#define HWIO_MSS_MPLL2_STATUS_PLL_D_SHFT                                         0x0

#define HWIO_MSS_UIM0_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001060)
#define HWIO_MSS_UIM0_BCR_RMSK                                                   0x1
#define HWIO_MSS_UIM0_BCR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_BCR_ADDR, HWIO_MSS_UIM0_BCR_RMSK)
#define HWIO_MSS_UIM0_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_BCR_ADDR, m)
#define HWIO_MSS_UIM0_BCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_BCR_ADDR,v)
#define HWIO_MSS_UIM0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_BCR_ADDR,m,v,HWIO_MSS_UIM0_BCR_IN)
#define HWIO_MSS_UIM0_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_UIM0_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_UIM1_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001064)
#define HWIO_MSS_UIM1_BCR_RMSK                                                   0x1
#define HWIO_MSS_UIM1_BCR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_BCR_ADDR, HWIO_MSS_UIM1_BCR_RMSK)
#define HWIO_MSS_UIM1_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_BCR_ADDR, m)
#define HWIO_MSS_UIM1_BCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_BCR_ADDR,v)
#define HWIO_MSS_UIM1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_BCR_ADDR,m,v,HWIO_MSS_UIM1_BCR_IN)
#define HWIO_MSS_UIM1_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_UIM1_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_Q6SS_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001068)
#define HWIO_MSS_Q6SS_BCR_RMSK                                                   0x1
#define HWIO_MSS_Q6SS_BCR_IN          \
        in_dword_masked(HWIO_MSS_Q6SS_BCR_ADDR, HWIO_MSS_Q6SS_BCR_RMSK)
#define HWIO_MSS_Q6SS_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6SS_BCR_ADDR, m)
#define HWIO_MSS_Q6SS_BCR_OUT(v)      \
        out_dword(HWIO_MSS_Q6SS_BCR_ADDR,v)
#define HWIO_MSS_Q6SS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6SS_BCR_ADDR,m,v,HWIO_MSS_Q6SS_BCR_IN)
#define HWIO_MSS_Q6SS_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_Q6SS_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_NC_HM_BCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x0000106c)
#define HWIO_MSS_NC_HM_BCR_RMSK                                                  0x1
#define HWIO_MSS_NC_HM_BCR_IN          \
        in_dword_masked(HWIO_MSS_NC_HM_BCR_ADDR, HWIO_MSS_NC_HM_BCR_RMSK)
#define HWIO_MSS_NC_HM_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_NC_HM_BCR_ADDR, m)
#define HWIO_MSS_NC_HM_BCR_OUT(v)      \
        out_dword(HWIO_MSS_NC_HM_BCR_ADDR,v)
#define HWIO_MSS_NC_HM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NC_HM_BCR_ADDR,m,v,HWIO_MSS_NC_HM_BCR_IN)
#define HWIO_MSS_NC_HM_BCR_BLK_ARES_BMSK                                         0x1
#define HWIO_MSS_NC_HM_BCR_BLK_ARES_SHFT                                         0x0

#define HWIO_MSS_COXM_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001074)
#define HWIO_MSS_COXM_BCR_RMSK                                                   0x1
#define HWIO_MSS_COXM_BCR_IN          \
        in_dword_masked(HWIO_MSS_COXM_BCR_ADDR, HWIO_MSS_COXM_BCR_RMSK)
#define HWIO_MSS_COXM_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_COXM_BCR_ADDR, m)
#define HWIO_MSS_COXM_BCR_OUT(v)      \
        out_dword(HWIO_MSS_COXM_BCR_ADDR,v)
#define HWIO_MSS_COXM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_COXM_BCR_ADDR,m,v,HWIO_MSS_COXM_BCR_IN)
#define HWIO_MSS_COXM_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_COXM_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_UIM0_CBCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00001078)
#define HWIO_MSS_UIM0_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_CBCR_ADDR, HWIO_MSS_UIM0_CBCR_RMSK)
#define HWIO_MSS_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_CBCR_ADDR,m,v,HWIO_MSS_UIM0_CBCR_IN)
#define HWIO_MSS_UIM0_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_UIM0_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_UIM0_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_UIM0_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_UIM1_CBCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x0000107c)
#define HWIO_MSS_UIM1_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_CBCR_ADDR, HWIO_MSS_UIM1_CBCR_RMSK)
#define HWIO_MSS_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_CBCR_ADDR,m,v,HWIO_MSS_UIM1_CBCR_IN)
#define HWIO_MSS_UIM1_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_UIM1_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_UIM1_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_UIM1_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_XO_UIM0_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001080)
#define HWIO_MSS_XO_UIM0_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_XO_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_UIM0_CBCR_ADDR, HWIO_MSS_XO_UIM0_CBCR_RMSK)
#define HWIO_MSS_XO_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_XO_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_XO_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_UIM0_CBCR_ADDR,m,v,HWIO_MSS_XO_UIM0_CBCR_IN)
#define HWIO_MSS_XO_UIM0_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_XO_UIM0_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_XO_UIM0_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_XO_UIM0_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_XO_UIM1_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001084)
#define HWIO_MSS_XO_UIM1_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_XO_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_UIM1_CBCR_ADDR, HWIO_MSS_XO_UIM1_CBCR_RMSK)
#define HWIO_MSS_XO_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_XO_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_XO_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_UIM1_CBCR_ADDR,m,v,HWIO_MSS_XO_UIM1_CBCR_IN)
#define HWIO_MSS_XO_UIM1_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_XO_UIM1_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_XO_UIM1_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_XO_UIM1_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_XO_MODEM_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00001088)
#define HWIO_MSS_XO_MODEM_CBCR_RMSK                                       0x80000000
#define HWIO_MSS_XO_MODEM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_MODEM_CBCR_ADDR, HWIO_MSS_XO_MODEM_CBCR_RMSK)
#define HWIO_MSS_XO_MODEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_MODEM_CBCR_ADDR, m)
#define HWIO_MSS_XO_MODEM_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_XO_MODEM_CBCR_CLKOFF_SHFT                                      0x1f

#define HWIO_MSS_XO_Q6_CBCR_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000108c)
#define HWIO_MSS_XO_Q6_CBCR_RMSK                                          0x80000001
#define HWIO_MSS_XO_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_Q6_CBCR_ADDR, HWIO_MSS_XO_Q6_CBCR_RMSK)
#define HWIO_MSS_XO_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_Q6_CBCR_ADDR, m)
#define HWIO_MSS_XO_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_Q6_CBCR_ADDR,v)
#define HWIO_MSS_XO_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_Q6_CBCR_ADDR,m,v,HWIO_MSS_XO_Q6_CBCR_IN)
#define HWIO_MSS_XO_Q6_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_XO_Q6_CBCR_CLKOFF_SHFT                                         0x1f
#define HWIO_MSS_XO_Q6_CBCR_CLKEN_BMSK                                           0x1
#define HWIO_MSS_XO_Q6_CBCR_CLKEN_SHFT                                           0x0

#define HWIO_MSS_BUS_UIM0_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00001090)
#define HWIO_MSS_BUS_UIM0_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, HWIO_MSS_BUS_UIM0_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM0_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM0_CBCR_IN)
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_UIM1_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00001094)
#define HWIO_MSS_BUS_UIM1_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, HWIO_MSS_BUS_UIM1_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM1_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM1_CBCR_IN)
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_CSR_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001098)
#define HWIO_MSS_BUS_CSR_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_CSR_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CSR_CBCR_ADDR, HWIO_MSS_BUS_CSR_CBCR_RMSK)
#define HWIO_MSS_BUS_CSR_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CSR_CBCR_ADDR, m)
#define HWIO_MSS_BUS_CSR_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CSR_CBCR_ADDR,v)
#define HWIO_MSS_BUS_CSR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CSR_CBCR_ADDR,m,v,HWIO_MSS_BUS_CSR_CBCR_IN)
#define HWIO_MSS_BUS_CSR_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_CSR_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_CSR_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_CSR_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_BRIDGE_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x0000109c)
#define HWIO_MSS_BUS_BRIDGE_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_BUS_BRIDGE_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR, HWIO_MSS_BUS_BRIDGE_CBCR_RMSK)
#define HWIO_MSS_BUS_BRIDGE_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR, m)
#define HWIO_MSS_BUS_BRIDGE_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR,v)
#define HWIO_MSS_BUS_BRIDGE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR,m,v,HWIO_MSS_BUS_BRIDGE_CBCR_IN)
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_BUS_MODEM_CBCR_ADDR                                      (MSS_PERPH_REG_BASE      + 0x000010a0)
#define HWIO_MSS_BUS_MODEM_CBCR_RMSK                                      0x80000000
#define HWIO_MSS_BUS_MODEM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_MODEM_CBCR_ADDR, HWIO_MSS_BUS_MODEM_CBCR_RMSK)
#define HWIO_MSS_BUS_MODEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MODEM_CBCR_ADDR, m)
#define HWIO_MSS_BUS_MODEM_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_BUS_MODEM_CBCR_CLKOFF_SHFT                                     0x1f

#define HWIO_MSS_BUS_Q6_CBCR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x000010a4)
#define HWIO_MSS_BUS_Q6_CBCR_RMSK                                         0x80000001
#define HWIO_MSS_BUS_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_Q6_CBCR_ADDR, HWIO_MSS_BUS_Q6_CBCR_RMSK)
#define HWIO_MSS_BUS_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_Q6_CBCR_ADDR, m)
#define HWIO_MSS_BUS_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_Q6_CBCR_ADDR,v)
#define HWIO_MSS_BUS_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_Q6_CBCR_ADDR,m,v,HWIO_MSS_BUS_Q6_CBCR_IN)
#define HWIO_MSS_BUS_Q6_CBCR_CLKOFF_BMSK                                  0x80000000
#define HWIO_MSS_BUS_Q6_CBCR_CLKOFF_SHFT                                        0x1f
#define HWIO_MSS_BUS_Q6_CBCR_CLKEN_BMSK                                          0x1
#define HWIO_MSS_BUS_Q6_CBCR_CLKEN_SHFT                                          0x0

#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010a8)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_RMSK                               0x80000001
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR, HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_RMSK)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR,m,v,HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_IN)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_BUS_CRYPTO_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x000010ac)
#define HWIO_MSS_BUS_CRYPTO_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_BUS_CRYPTO_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, HWIO_MSS_BUS_CRYPTO_CBCR_RMSK)
#define HWIO_MSS_BUS_CRYPTO_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, m)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,v)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,m,v,HWIO_MSS_BUS_CRYPTO_CBCR_IN)
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_BUS_NAV_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x000010b0)
#define HWIO_MSS_BUS_NAV_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_NAV_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, HWIO_MSS_BUS_NAV_CBCR_RMSK)
#define HWIO_MSS_BUS_NAV_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NAV_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NAV_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NAV_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NAV_CBCR_ADDR,m,v,HWIO_MSS_BUS_NAV_CBCR_IN)
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR                            (MSS_PERPH_REG_BASE      + 0x000010b4)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_RMSK                            0x80000001
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR, HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_RMSK)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR,m,v,HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_IN)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKOFF_BMSK                     0x80000000
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKOFF_SHFT                           0x1f
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKEN_BMSK                             0x1
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKEN_SHFT                             0x0

#define HWIO_MSS_BUS_ATB_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x000010b8)
#define HWIO_MSS_BUS_ATB_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_ATB_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_ATB_CBCR_ADDR, HWIO_MSS_BUS_ATB_CBCR_RMSK)
#define HWIO_MSS_BUS_ATB_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_ATB_CBCR_ADDR, m)
#define HWIO_MSS_BUS_ATB_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_ATB_CBCR_ADDR,v)
#define HWIO_MSS_BUS_ATB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_ATB_CBCR_ADDR,m,v,HWIO_MSS_BUS_ATB_CBCR_IN)
#define HWIO_MSS_BUS_ATB_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_ATB_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_ATB_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_ATB_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_COXM_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010bc)
#define HWIO_MSS_BUS_COXM_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, HWIO_MSS_BUS_COXM_CBCR_RMSK)
#define HWIO_MSS_BUS_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BUS_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BUS_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_COXM_CBCR_ADDR,m,v,HWIO_MSS_BUS_COXM_CBCR_IN)
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR                              (MSS_PERPH_REG_BASE      + 0x000010c4)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK                              0x80000001
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, m)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,v)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,m,v,HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_BMSK                       0x80000000
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_SHFT                             0x1f
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_BMSK                               0x1
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_SHFT                               0x0

#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR                              (MSS_PERPH_REG_BASE      + 0x000010cc)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_RMSK                              0x80000000
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR, HWIO_MSS_BUS_COMBODAC_COMP_CBCR_RMSK)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR, m)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_CLKOFF_BMSK                       0x80000000
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_CLKOFF_SHFT                             0x1f

#define HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR                                  (MSS_PERPH_REG_BASE      + 0x000010d4)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_RMSK                                  0x80000000
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR, HWIO_MSS_MODEM_CFG_AHB_CBCR_RMSK)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_CLKOFF_BMSK                           0x80000000
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_CLKOFF_SHFT                                 0x1f

#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR                                 (MSS_PERPH_REG_BASE      + 0x000010d8)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_RMSK                                 0x80000000
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR, HWIO_MSS_MODEM_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_CLKOFF_BMSK                          0x80000000
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_CLKOFF_SHFT                                0x1f

#define HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x000010dc)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_RMSK                                   0x80000001
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR, HWIO_MSS_NAV_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR,v)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR,m,v,HWIO_MSS_NAV_SNOC_AXI_CBCR_IN)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKOFF_BMSK                            0x80000000
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKOFF_SHFT                                  0x1f
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKEN_BMSK                                    0x1
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKEN_SHFT                                    0x0

#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR                                (MSS_PERPH_REG_BASE      + 0x000010e0)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_RMSK                                0x80000001
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR, HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR,v)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR,m,v,HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_IN)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKOFF_BMSK                         0x80000000
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKOFF_SHFT                               0x1f
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKEN_BMSK                                 0x1
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKEN_SHFT                                 0x0

#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010e4)
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_RMSK                               0x80000001
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_ADDR, HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_RMSK)
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_ADDR, m)
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_ADDR,v)
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_ADDR,m,v,HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_IN)
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_MPLL2_MAIN_MODEM_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_MPLL1_MAIN_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x000010e8)
#define HWIO_MSS_MPLL1_MAIN_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_MPLL1_MAIN_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR, HWIO_MSS_MPLL1_MAIN_CBCR_RMSK)
#define HWIO_MSS_MPLL1_MAIN_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_MAIN_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_MAIN_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_MAIN_CBCR_IN)
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR                                 (MSS_PERPH_REG_BASE      + 0x000010ec)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_RMSK                                 0x80000000
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR, HWIO_MSS_MPLL1_MAIN_BUS_CBCR_RMSK)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_CLKOFF_BMSK                          0x80000000
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_CLKOFF_SHFT                                0x1f

#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010f0)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_RMSK                               0x80000001
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR, HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_RMSK)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_IN)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010f4)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_RMSK                               0x80000001
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR, HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_RMSK)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_IN)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_BIT_COXM_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010f8)
#define HWIO_MSS_BIT_COXM_CBCR_RMSK                                       0x80000001
#define HWIO_MSS_BIT_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, HWIO_MSS_BIT_COXM_CBCR_RMSK)
#define HWIO_MSS_BIT_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BIT_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BIT_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_CBCR_ADDR,m,v,HWIO_MSS_BIT_COXM_CBCR_IN)
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_SLEEP_Q6_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010fc)
#define HWIO_MSS_SLEEP_Q6_CBCR_RMSK                                       0x80000001
#define HWIO_MSS_SLEEP_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_SLEEP_Q6_CBCR_ADDR, HWIO_MSS_SLEEP_Q6_CBCR_RMSK)
#define HWIO_MSS_SLEEP_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_SLEEP_Q6_CBCR_ADDR, m)
#define HWIO_MSS_SLEEP_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_SLEEP_Q6_CBCR_ADDR,v)
#define HWIO_MSS_SLEEP_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_SLEEP_Q6_CBCR_ADDR,m,v,HWIO_MSS_SLEEP_Q6_CBCR_IN)
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR                           (MSS_PERPH_REG_BASE      + 0x00001100)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_RMSK                           0x80000001
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR, HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_RMSK)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_IN)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKOFF_BMSK                    0x80000000
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKOFF_SHFT                          0x1f
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKEN_BMSK                            0x1
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKEN_SHFT                            0x0

#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR                           (MSS_PERPH_REG_BASE      + 0x00001104)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_RMSK                           0x80000001
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR, HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_RMSK)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_IN)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKOFF_BMSK                    0x80000000
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKOFF_SHFT                          0x1f
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKEN_BMSK                            0x1
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKEN_SHFT                            0x0

#define HWIO_MSS_BUS_CMD_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001108)
#define HWIO_MSS_BUS_CMD_RCGR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, HWIO_MSS_BUS_CMD_RCGR_RMSK)
#define HWIO_MSS_BUS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CMD_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CMD_RCGR_ADDR,m,v,HWIO_MSS_BUS_CMD_RCGR_IN)
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_BMSK                               0x80000000
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_SHFT                                     0x1f
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_BMSK                                        0x1
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_SHFT                                        0x0

#define HWIO_MSS_BUS_CFG_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000110c)
#define HWIO_MSS_BUS_CFG_RCGR_RMSK                                             0x71f
#define HWIO_MSS_BUS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, HWIO_MSS_BUS_CFG_RCGR_RMSK)
#define HWIO_MSS_BUS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CFG_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CFG_RCGR_ADDR,m,v,HWIO_MSS_BUS_CFG_RCGR_IN)
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_BMSK                                     0x700
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_SHFT                                       0x8
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_BMSK                                      0x1f
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_SHFT                                       0x0

#define HWIO_MSS_Q6_CMD_RCGR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001110)
#define HWIO_MSS_Q6_CMD_RCGR_RMSK                                         0x80000003
#define HWIO_MSS_Q6_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_Q6_CMD_RCGR_ADDR, HWIO_MSS_Q6_CMD_RCGR_RMSK)
#define HWIO_MSS_Q6_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6_CMD_RCGR_ADDR, m)
#define HWIO_MSS_Q6_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_Q6_CMD_RCGR_ADDR,v)
#define HWIO_MSS_Q6_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6_CMD_RCGR_ADDR,m,v,HWIO_MSS_Q6_CMD_RCGR_IN)
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_OFF_BMSK                                0x80000000
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_OFF_SHFT                                      0x1f
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_EN_BMSK                                        0x2
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_EN_SHFT                                        0x1
#define HWIO_MSS_Q6_CMD_RCGR_UPDATE_BMSK                                         0x1
#define HWIO_MSS_Q6_CMD_RCGR_UPDATE_SHFT                                         0x0

#define HWIO_MSS_Q6_CFG_RCGR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001114)
#define HWIO_MSS_Q6_CFG_RCGR_RMSK                                              0x71f
#define HWIO_MSS_Q6_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_Q6_CFG_RCGR_ADDR, HWIO_MSS_Q6_CFG_RCGR_RMSK)
#define HWIO_MSS_Q6_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6_CFG_RCGR_ADDR, m)
#define HWIO_MSS_Q6_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_Q6_CFG_RCGR_ADDR,v)
#define HWIO_MSS_Q6_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6_CFG_RCGR_ADDR,m,v,HWIO_MSS_Q6_CFG_RCGR_IN)
#define HWIO_MSS_Q6_CFG_RCGR_SRC_SEL_BMSK                                      0x700
#define HWIO_MSS_Q6_CFG_RCGR_SRC_SEL_SHFT                                        0x8
#define HWIO_MSS_Q6_CFG_RCGR_SRC_DIV_BMSK                                       0x1f
#define HWIO_MSS_Q6_CFG_RCGR_SRC_DIV_SHFT                                        0x0

#define HWIO_MSS_UIM_CMD_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001118)
#define HWIO_MSS_UIM_CMD_RCGR_RMSK                                        0x80000003
#define HWIO_MSS_UIM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, HWIO_MSS_UIM_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM_CMD_RCGR_IN)
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_BMSK                               0x80000000
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_SHFT                                     0x1f
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_BMSK                                       0x2
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_SHFT                                       0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_BMSK                                        0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_SHFT                                        0x0

#define HWIO_MSS_UIM_CFG_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000111c)
#define HWIO_MSS_UIM_CFG_RCGR_RMSK                                              0x1f
#define HWIO_MSS_UIM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, HWIO_MSS_UIM_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM_CFG_RCGR_IN)
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_BMSK                                      0x1f
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_SHFT                                       0x0

#define HWIO_MSS_BBRX0_CBCR_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001120)
#define HWIO_MSS_BBRX0_CBCR_RMSK                                          0x80000000
#define HWIO_MSS_BBRX0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_CBCR_ADDR, HWIO_MSS_BBRX0_CBCR_RMSK)
#define HWIO_MSS_BBRX0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_CBCR_ADDR, m)
#define HWIO_MSS_BBRX0_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_BBRX0_CBCR_CLKOFF_SHFT                                         0x1f

#define HWIO_MSS_BBRX1_CBCR_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001124)
#define HWIO_MSS_BBRX1_CBCR_RMSK                                          0x80000000
#define HWIO_MSS_BBRX1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_CBCR_ADDR, HWIO_MSS_BBRX1_CBCR_RMSK)
#define HWIO_MSS_BBRX1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_CBCR_ADDR, m)
#define HWIO_MSS_BBRX1_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_BBRX1_CBCR_CLKOFF_SHFT                                         0x1f

#define HWIO_MSS_RESERVE_01_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001130)
#define HWIO_MSS_RESERVE_01_RMSK                                          0xffffffff
#define HWIO_MSS_RESERVE_01_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, HWIO_MSS_RESERVE_01_RMSK)
#define HWIO_MSS_RESERVE_01_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, m)
#define HWIO_MSS_RESERVE_01_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_01_ADDR,v)
#define HWIO_MSS_RESERVE_01_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_01_ADDR,m,v,HWIO_MSS_RESERVE_01_IN)
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_BMSK                           0xffffffff
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_SHFT                                  0x0

#define HWIO_MSS_RESERVE_02_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001134)
#define HWIO_MSS_RESERVE_02_RMSK                                          0xffffffff
#define HWIO_MSS_RESERVE_02_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, HWIO_MSS_RESERVE_02_RMSK)
#define HWIO_MSS_RESERVE_02_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, m)
#define HWIO_MSS_RESERVE_02_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_02_ADDR,v)
#define HWIO_MSS_RESERVE_02_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_02_ADDR,m,v,HWIO_MSS_RESERVE_02_IN)
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_BMSK                           0xffffffff
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_SHFT                                  0x0

#define HWIO_MSS_BBRX0_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001138)
#define HWIO_MSS_BBRX0_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX0_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_MISC_ADDR, HWIO_MSS_BBRX0_MISC_RMSK)
#define HWIO_MSS_BBRX0_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_MISC_ADDR, m)
#define HWIO_MSS_BBRX0_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX0_MISC_ADDR,v)
#define HWIO_MSS_BBRX0_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX0_MISC_ADDR,m,v,HWIO_MSS_BBRX0_MISC_IN)
#define HWIO_MSS_BBRX0_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX0_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BBRX1_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000113c)
#define HWIO_MSS_BBRX1_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX1_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_MISC_ADDR, HWIO_MSS_BBRX1_MISC_RMSK)
#define HWIO_MSS_BBRX1_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_MISC_ADDR, m)
#define HWIO_MSS_BBRX1_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX1_MISC_ADDR,v)
#define HWIO_MSS_BBRX1_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX1_MISC_ADDR,m,v,HWIO_MSS_BBRX1_MISC_IN)
#define HWIO_MSS_BBRX1_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX1_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BIT_COXM_DIV_MISC_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001148)
#define HWIO_MSS_BIT_COXM_DIV_MISC_RMSK                                          0xf
#define HWIO_MSS_BIT_COXM_DIV_MISC_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR, HWIO_MSS_BIT_COXM_DIV_MISC_RMSK)
#define HWIO_MSS_BIT_COXM_DIV_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR, m)
#define HWIO_MSS_BIT_COXM_DIV_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR,v)
#define HWIO_MSS_BIT_COXM_DIV_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR,m,v,HWIO_MSS_BIT_COXM_DIV_MISC_IN)
#define HWIO_MSS_BIT_COXM_DIV_MISC_SRC_DIV_BMSK                                  0xf
#define HWIO_MSS_BIT_COXM_DIV_MISC_SRC_DIV_SHFT                                  0x0

#define HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001150)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK                                   0x80000003
#define HWIO_MSS_UIM0_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_BMSK                          0x80000000
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_SHFT                                0x1f
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_BMSK                                  0x2
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_SHFT                                  0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_BMSK                                   0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_SHFT                                   0x0

#define HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001154)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK                                       0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_BMSK                                  0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_SHFT                                     0xc

#define HWIO_MSS_UIM0_MND_M_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001158)
#define HWIO_MSS_UIM0_MND_M_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, HWIO_MSS_UIM0_MND_M_RMSK)
#define HWIO_MSS_UIM0_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, m)
#define HWIO_MSS_UIM0_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_M_ADDR,v)
#define HWIO_MSS_UIM0_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_M_ADDR,m,v,HWIO_MSS_UIM0_MND_M_IN)
#define HWIO_MSS_UIM0_MND_M_M_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_M_M_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM0_MND_N_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000115c)
#define HWIO_MSS_UIM0_MND_N_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, HWIO_MSS_UIM0_MND_N_RMSK)
#define HWIO_MSS_UIM0_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, m)
#define HWIO_MSS_UIM0_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_N_ADDR,v)
#define HWIO_MSS_UIM0_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_N_ADDR,m,v,HWIO_MSS_UIM0_MND_N_IN)
#define HWIO_MSS_UIM0_MND_N_N_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_N_N_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM0_MND_D_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001160)
#define HWIO_MSS_UIM0_MND_D_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, HWIO_MSS_UIM0_MND_D_RMSK)
#define HWIO_MSS_UIM0_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, m)
#define HWIO_MSS_UIM0_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_D_ADDR,v)
#define HWIO_MSS_UIM0_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_D_ADDR,m,v,HWIO_MSS_UIM0_MND_D_IN)
#define HWIO_MSS_UIM0_MND_D_D_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_D_D_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001164)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK                                   0x80000003
#define HWIO_MSS_UIM1_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_BMSK                          0x80000000
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_SHFT                                0x1f
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_BMSK                                  0x2
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_SHFT                                  0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_BMSK                                   0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_SHFT                                   0x0

#define HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001168)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK                                       0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_BMSK                                  0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_SHFT                                     0xc

#define HWIO_MSS_UIM1_MND_M_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000116c)
#define HWIO_MSS_UIM1_MND_M_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, HWIO_MSS_UIM1_MND_M_RMSK)
#define HWIO_MSS_UIM1_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, m)
#define HWIO_MSS_UIM1_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_M_ADDR,v)
#define HWIO_MSS_UIM1_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_M_ADDR,m,v,HWIO_MSS_UIM1_MND_M_IN)
#define HWIO_MSS_UIM1_MND_M_M_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_M_M_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_N_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001170)
#define HWIO_MSS_UIM1_MND_N_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, HWIO_MSS_UIM1_MND_N_RMSK)
#define HWIO_MSS_UIM1_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, m)
#define HWIO_MSS_UIM1_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_N_ADDR,v)
#define HWIO_MSS_UIM1_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_N_ADDR,m,v,HWIO_MSS_UIM1_MND_N_IN)
#define HWIO_MSS_UIM1_MND_N_N_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_N_N_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_D_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001174)
#define HWIO_MSS_UIM1_MND_D_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, HWIO_MSS_UIM1_MND_D_RMSK)
#define HWIO_MSS_UIM1_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, m)
#define HWIO_MSS_UIM1_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_D_ADDR,v)
#define HWIO_MSS_UIM1_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_D_ADDR,m,v,HWIO_MSS_UIM1_MND_D_IN)
#define HWIO_MSS_UIM1_MND_D_D_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_D_D_VALUE_SHFT                                         0x0

#define HWIO_MSS_TCSR_ACC_SEL_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_RMSK                                               0x3
#define HWIO_MSS_TCSR_ACC_SEL_IN          \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, HWIO_MSS_TCSR_ACC_SEL_RMSK)
#define HWIO_MSS_TCSR_ACC_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, m)
#define HWIO_MSS_TCSR_ACC_SEL_OUT(v)      \
        out_dword(HWIO_MSS_TCSR_ACC_SEL_ADDR,v)
#define HWIO_MSS_TCSR_ACC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_TCSR_ACC_SEL_ADDR,m,v,HWIO_MSS_TCSR_ACC_SEL_IN)
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_BMSK                                   0x3
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_SHFT                                   0x0



#endif /* __PRNG_CLKHWIOREG_H__ */

