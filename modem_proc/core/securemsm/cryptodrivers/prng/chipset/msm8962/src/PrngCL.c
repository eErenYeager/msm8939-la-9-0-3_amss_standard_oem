/**
@file PrngCL.c 
@brief PRNG Engine source file 
*/

/*===========================================================================

                     P R N G E n g i n e D r i v e r

DESCRIPTION
  This file contains declarations and definitions for the
  interface between PRNG engine api and the PRNG hardware

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/core.mpss/3.7.24/securemsm/cryptodrivers/prng/chipset/msm8962/src/PrngCL.c#1 $
 $DateTime: 2015/01/27 06:04:57 $
 $Author: mplp4svc $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------------
2014-03-14   jk     Replace memcpy with memscpy
2013-12-03   nk     Increased random number placeholder
2012-09-19   amen   Added PRNG lite api's, dont use mutex or dal apis, direct register config
2011-09-08   nk     Added the implementation of de-init routine. 
2010-06-25   yk     Initial version
============================================================================*/


#include "comdef.h"
#include <stringl.h>

#include "PrngCL.h"
#include "PrngEL.h"
#include "PrngCL_target.h"

/**Enable this flag to allow run of standalone builds (without TZ) */
uint32 g_prng_config_enable = 0;

#define BUSYWAIT_XO_FREQUENCY_IN_KHZ 19200

/**
 * @brief This function initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_getdata
 *
 */
PrngCL_Result_Type PrngCL_init(void)
{
	PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;
   /* Enable clock for random number generator */
   stat = (PrngCL_Result_Type)PrngEL_ClkEnable();

   PrngELMemoryBarrier();

   if(g_prng_config_enable) // TZ will configure PRNG
   {
   HWIO_OUT(SEC_PRNG_LFSR_CFG,
        (0x5                                                                                 | //Set RING_OSC0_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR0_EN)) | //Enable LFSR0_EN
        0x50                                                                                 | //Set RING_OSC1_CFG to 101, which is feedback point 1 
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR1_EN)) | //Enable LFSR1_EN
        0x500                                                                               | //Set RING_OSC2_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR2_EN)) | //Enable LFSR2_EN
        0x5000                                                                             | //Set RING_OSC3_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR3_EN))    //Enable LFSR3_EN
        ));
     PrngELMemoryBarrier();

     HWIO_OUT(SEC_PRNG_CONFIG,
          ((1 << HWIO_SHFT(SEC_PRNG_CONFIG, SEC_PRNG_EN)) ));

     PrngELMemoryBarrier();
   }
   return stat;
}
/**
 * @brief  This function returns the contents of the PRNG_DATA_OUT register.
 *
 * @param random_ptr [in]pointer to random number
 * @param random_len [in]length of random number
 *
 * @return PrngCL_Resut_Type
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_getdata(uint8*  random_ptr,  uint16  random_len)
{
  PrngCL_Result_Type ret_val = PRNGCL_ERROR_NONE;
  uint32 tmp_iv;
  uint32 i;
  uint32 tmp_random_len = random_len;
  const uint16 unit_random_len = 4;
   
  if(!random_ptr || !random_len){
      return PRNGCL_ERROR_INVALID_PARAM; 
   }

  if(random_len%unit_random_len !=0){
  	tmp_random_len += (unit_random_len - random_len%unit_random_len);
  	}
  
  /* Generate random numbers. Unit length of PRNG is 4 Bytes. 
  *  Hence, multiple PRNG random numbers are generated if random_len > 4 */
  for (i=0; i<(tmp_random_len/unit_random_len); i++){

      /* check PRNG_STATUS */
      while(1)
        {
             uint32 prng_status = HWIO_IN(SEC_PRNG_STATUS);

             if(prng_status &  SEC_HWIO_PRNG_STATUS_DATA_AVAIL_BMSK)	{
                break;
             }
        }


       /* Get the randum number from PRNG_DATA_OUT and check if it is not 0 */
       while(1)
           {
                tmp_iv = HWIO_IN(SEC_PRNG_DATA_OUT);

                if (tmp_iv != 0) break;
  	     }
	   
       /* Generate mutiple of 4 bytes random numbers. Then, if necessary, adjust total random
        *  number to random_len size */
       if( i < (tmp_random_len/unit_random_len -1) ){
           memscpy(random_ptr, unit_random_len, &tmp_iv, unit_random_len);
           random_ptr += unit_random_len;
          }
	   
       else {
           if ( (random_len%unit_random_len) !=0){
	        memscpy(random_ptr, random_len%unit_random_len, &tmp_iv, random_len%unit_random_len);
           	}
           else {
               memscpy(random_ptr, unit_random_len, &tmp_iv, unit_random_len);
		}
           }
        }

  return ret_val;
}

/**
 * @brief This function de-initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_deinit(void)
{
	PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;

   /* Disable clock for random number generator */
   stat = (PrngCL_Result_Type) PrngEL_ClkDisable();
   return stat;
}



/**
 * @brief    This function initializes PRNG Engine. Uses direct register write/reads  
 *
 * @param None
 *
 * @return PrngCL_Resut_Type
 *
 * @see PrngCL_lite_init
 *
 */
PrngCL_Result_Type PrngCL_lite_init(void)
{
	PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;

  HWIO_OUTF(GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 1);

  while (HWIO_INF(GCC_PRNG_AHB_CBCR, CLK_OFF))
  {
  }

  if(g_prng_config_enable) // TZ will configure PRNG
  {
   HWIO_OUT(SEC_PRNG_LFSR_CFG,
        (0x5                                                                                 | //Set RING_OSC0_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR0_EN)) | //Enable LFSR0_EN
        0x50                                                                                 | //Set RING_OSC1_CFG to 101, which is feedback point 1 
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR1_EN)) | //Enable LFSR1_EN
        0x500                                                                               | //Set RING_OSC2_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR2_EN)) | //Enable LFSR2_EN
        0x5000                                                                             | //Set RING_OSC3_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR3_EN))    //Enable LFSR3_EN
        ));

   PrngELMemoryBarrier();

   HWIO_OUT(SEC_PRNG_CONFIG,
        ((1 << HWIO_SHFT(SEC_PRNG_CONFIG, SEC_PRNG_EN)) ));

   PrngELMemoryBarrier();
  }

  return stat;
}



/**
 * @brief This function de-initializes PRNG Engine. Uses direct register write/reads
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_lite_deinit
 *
 */
PrngCL_Result_Type PrngCL_lite_deinit(void)
{
  PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;
  uint32 start_count = 0;
  uint32 delay_count = 0;
  uint32 pause_time_us=100;

  HWIO_OUTF(GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 0);

  start_count = in_dword(QDSP6SS_QTMR_F1_1_REG_BASE);
  /*
   * Perform the delay and handle potential overflows of the timer.
   * Setting timer to 100microsec, done as multiple calls to api can
   * cause timer to not turn on, if a continous toggle of the vote 
   * register happens
   */

  delay_count = (pause_time_us * BUSYWAIT_XO_FREQUENCY_IN_KHZ)/1000;
  while ((in_dword(QDSP6SS_QTMR_F1_1_REG_BASE) - start_count) < delay_count);
 
  return stat;
}

