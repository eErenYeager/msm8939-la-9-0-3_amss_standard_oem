#ifndef MBA_UTILS_H
#define MBA_UTILS_H

/**
@file mba_utils.h
@brief MBA Utility Api's
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/mba/inc/mba_utils.h#2 $
  $DateTime: 2015/05/06 02:42:31 $
  $Author: pwbldsvc $

when       who      what, where, why
--------   ---      ------------------------------------
12/20/11   mm       Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include <IxErrno.h> 
#include "qurt.h"
#include "mba_error_handler.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define FOUR_KB 0x1000
#define SIXTEEN_KB 0x4000
#define SIXTY_FOUR_KB 0x10000
#define TWO_FIFTY_SIX_KB 0x40000
#define ONE_MB  0x100000
#define FOUR_MB  0x400000
#define SIXTEEN_MB 0x1000000
#define PAGE_SIZES 7
#define SIXTY_FOUR_MB (0x4000000)
#define ONE_TWENTY_EIGHT_MB (0x8000000)
#define TWO_FIFTY_SIX_MB (0x10000000)
#define MBA_MAX_UINT32 (0xFFFFFFFF)

/* Making a TLB entry dynamically */
int mba_qurt_map_memory_range(uint32 mba_region_paddr, uint32 mba_region_size, 
  qurt_mem_cache_mode_t cache_attribs, unsigned int *entry, uint8 remap, uint32 *vadd_out_param);
							  
/* Make multiple entries */
int mba_map_memory_range(uint32 mba_region_paddr, uint32 mba_region_size, 
  qurt_mem_cache_mode_t cache_attribs, unsigned int *entry, uint8 remap, uint32 *vadd_out_param);

int mba_check_memory_in_bank0(uint32 start_addr, uint32 size);

#endif /* MBA_UTILS_H */
  