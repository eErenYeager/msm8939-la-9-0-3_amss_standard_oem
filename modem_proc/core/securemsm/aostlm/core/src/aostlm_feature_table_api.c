

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   Feature tables for 8916

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_feature_table_api.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/14   eh      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_feature_table_api.h"
#include "comdef.h"
#include "msg.h"

/*===========================================================================

                     Public Methods

===========================================================================*/

/**
 * @brief Fetches the number of features in the table for a 
 *        particular processor id
 *  
 * @param proc - The processor performing the query 
 *  
 * @return the number of features in the table for proc
 */
const uint16 aostlm_ft_get_num_features(aostlm_proc_id_t proc)
{
    uint16 size = 0;
    if ( proc >= 0 && proc < AOSTLM_NUM_PROC_IDS)
    {
        size =  aostlm_feature_table[proc].num_features;
    }
    else
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "  Fetch num features:  Proc %d out of range", proc );
    }
    return size;

}

/**
 * @brief Fetches the feature ids managed by proc's AOSTLM 
 *        client
 *  
 * @param proc - The processor performing the query 
 *  
 * @return a list of the feature ids managed by proc
 */
const aostlm_feature_list_t aostlm_ft_get_features(aostlm_proc_id_t proc )
{

    aostlm_feature_list_t ret_tbl = NULL;

    if ( proc >= 0 && proc < AOSTLM_NUM_PROC_IDS)
    {
        ret_tbl = aostlm_feature_table[proc].subsystem_features;
    }
    else
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "  Fetch features:  Proc %d out of range", proc );
    }

    return ret_tbl;
}
