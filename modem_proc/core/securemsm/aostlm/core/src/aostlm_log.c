
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_log.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/14   eh      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "aostlm_common.h"
#include "log.h"
#include "log_codes.h"
#include "aostlm_log.h"
#include "msg.h"

/*  DEFINES */
#define LOG_1X_BASE_C        ((uint16) 0x1000)
#define LOG_AOSTLM_REPORT_C  ((0x872) + LOG_1X_BASE_C) 

/* log report struct */
typedef PACKED struct
{
    log_hdr_type    hdr;
    aostlm_report_t report;
} log_aostlm_report_t;


/**
 * @brief Allows clients to submit aostlm logs to the log 
 *        service
 *
 * @param report - the Log to submit
 *  
 * @return none
 *
 */
void aostlm_submit_log(aostlm_report_t report)
{
    log_aostlm_report_t* log_ptr = NULL;

    // DJC TODO:  Temporary hijacking of 0x182B instead of using LOG_AOSTLM_REPORT_C
    //            Will use AOSTLM_REPORT_C when MDSP is rebuilt with new log_codes.h
    log_ptr = (log_aostlm_report_t*)log_alloc(
//        0x182B,sizeof(log_aostlm_report_t));
        LOG_AOSTLM_REPORT_C,sizeof(log_aostlm_report_t));
    if ( log_ptr != NULL )
    {
        log_ptr->report = report;
        log_commit(log_ptr);
    }
}
