/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
Prepares aostlm data to be delivered to the QMI channel.  

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_chan_prep.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/6/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_chan_prep.h"
#include "secapi.h"
#include "msg.h"

static boolean aostlm_chan_init = FALSE;
/**
 * @brief Initializes the channel preparation module.  Must 
 *        be called before data can be packed or unpacked
 *  
 * @return none
 *
 */
void aostlm_cp_init(void)
{
    // initialize the secure channel
    secapi_secure_channel_init();
    aostlm_chan_init = TRUE;
}
/**
 * @brief Packs a message for delivery to IPC
 *  
 * @param input_msg_ptr[in]: the input payload to be packed
 * @param input_msg_len[in]: length of the input message 
 * @param output_msg_ptr[out]:  The packed message
 * @param output_msg_len_ptr[out]:  Length of the packed message
 *  
 * @details Caller is responsible for allocation and maintenance 
 *          of all buffers.  Caller must allocate enough space
 *          for output message
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_cp_pack(const uint8* input_msg_ptr, 
                       const uint32 input_msg_len,
                       uint8* output_msg_ptr, 
                       uint32* output_msg_len_ptr )
{
    boolean retval = TRUE;

    if ( !aostlm_chan_init || !input_msg_ptr || !output_msg_ptr || !output_msg_len_ptr)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Cannot pack message.  Uninitialized or bad param" ); 
        retval = FALSE;
    }
    else
    {

#ifdef AOSTLM_SECURE_OFF

    memscpy(output_msg_ptr, *output_msg_len_ptr, input_msg_ptr, input_msg_len );
    *output_msg_len_ptr = input_msg_len;

#else
        retval = (secapi_secure_message(SC_SSID_TZ,     SC_CID_AOSTLM, 
                                        input_msg_ptr,  input_msg_len, 
                                        output_msg_ptr, output_msg_len_ptr)) == E_SUCCESS;
#endif
    }


    return retval;
}

/**
 * @brief unpacks a message received from IPC
 *  
 * @param input_msg_ptr[in]: the input payload to be unpacked
 * @param input_msg_len[in]: length of the input message 
 * @param output_msg_ptr[out]:  The unpacked message
 * @param output_msg_len_ptr[out]:  Length of the unpacked 
 *                          message
 *  
 * @details Caller is responsible for allocation and maintenance 
 *          of all buffers.  Caller must allocate enough space
 *          for output message
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_cp_unpack(const uint8* input_msg_ptr, 
                         const uint32 input_msg_len,
                         uint8* output_msg_ptr, 
                         uint32* output_msg_len_ptr )
{
    boolean retval = TRUE;

    if ( !aostlm_chan_init || !input_msg_ptr || !output_msg_ptr || !output_msg_len_ptr)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Cannot unpack message.  Uninitialized or bad param" ); 
        retval = FALSE;
    }
    else
    {
#ifdef AOSTLM_SECURE_OFF

    memscpy(output_msg_ptr, *output_msg_len_ptr, input_msg_ptr, input_msg_len );
    *output_msg_len_ptr = input_msg_len;

#else
        retval = (secapi_authenticate_decrypt_message(SC_SSID_TZ,     SC_CID_AOSTLM, 
                                                      input_msg_ptr,  input_msg_len,
                                                      output_msg_ptr, output_msg_len_ptr )) == E_SUCCESS;
#endif
    }
    return retval;
}
