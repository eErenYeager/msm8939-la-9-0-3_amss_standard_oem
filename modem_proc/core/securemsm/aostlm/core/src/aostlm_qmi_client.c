/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM QMI Client provides QMI message transport services to the
   AOSTLM Client. It connects with the AOSTLM QMI Server on the APSS
   and delivers a synchronous message, which is then delievred to the
   AOSTLM Trust Zone App. When the synchronous call returns,
   the AOSTLM Client can access the response from the AOSTLM Trust
   Zone App.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 
Qualcomm Technologies Proprietary and Confidential.  
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_qmi_client.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who         what, where, why
--------   ---         ----------------------------------------------------------
06/02/2014   sundarr   Initial version (8974). 
07/15/2014   sundarr   Updated based on QMI IDL file review (8916). 
07/18/2014   sundarr   Addressed code review comments. 
10/14/2014   sundarr   Released qmi notifier (stability issue). 
10/29/2014   sundarr   Cleared signal (CR  747605).
===========================================================================*/

#include <string.h>
#include <stringl.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include "comdef.h"
#include "qurt.h"
#include "msg.h"
#include "qmi_client.h"
#include "qmi_cci_target_ext.h"   /* OS tailored */
#include "aostlm_access_v01.h"    /* QMI IDL generated header file */
#include "aostlm_qmi_client.h"
#include "aostlm_common.h"

/*=========================================================================== 
 
            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

/* OS Signal that is set when AOSTLM QMI Server comes up */
#define AOSTLM_QMI_CLNT_WAIT_SIG      (0x00010000)   

/* Synchronous messsage timeout in microseconds */
#define AOSTLM_QMI_CLIENT_SEND_SYNC_TIMEOUT_MICRO_SECS (1000000)

/* The code under this flag should be stripped  for commercial release. */
#define FEATURE_AOSTLM_DEBUG 0

typedef struct 
{
    qmi_client_type  qmi_client;
    qmi_service_info qmi_service_info;
} aostlm_qmi_client_t;


/* QMI Client related Data */
static aostlm_qmi_client_t  aostlm_qmi_data;
static qmi_client_os_params os_params;


/*=============================================================================
                     INTERNAL FUNCTION DECLARATIONS
=============================================================================*/
static int aostlm_qmi_client_release(const aostlm_qmi_client_t *handle);
static void aostlm_qmi_ind_cb( qmi_client_type user_handle,
                               unsigned int    msg_id,
                               void	           *ind_buf,
                               unsigned int    ind_buf_len,
                               void            *ind_cb_data ) ;

/*=============================================================================
                     FUNCTION DEFINITIONS
=============================================================================*/


/**
 * @brief Callback function for the QMI framework for letting 
 *        the QMI Client know about Indications received from
 *        the QMI Server. This callback function is registered
 *        at initialization when qmi_client_init() is called.
 * 
 * @details Callback function invoked by the QMI framework when 
 *          an Indication message is received from the QMI
 *          server. As of now, no server initiated QMI
 *          Indications have been defined for the AOSTLM
 *          service. If needed, they must be defined in the QMI
 *          IDL file. This is a place holder function.
 *  
 * @param [in] user_handle: Handle used by the QMI framework.
 * @param [in] msg_id: Message ID. 
 * @param [in] ind_buf: Pointer to the indication buffer. 
 * @param [in] ind_buf_len: Length of the indication buffer.   
 * @param [in] ind_cb_data: User data.  
 *
 * @return None. 
 *  
 */
static void aostlm_qmi_ind_cb
(

    qmi_client_type user_handle,
    unsigned int    msg_id,
    void	        *ind_buf,
    unsigned int    ind_buf_len,
    void            *ind_cb_data
)
{

    MSG_HIGH("aostlm_qmi_ind_cb: No indications have been defined for aostlm.\n",0,0,0);   
    return;

}


/**
 * @brief Sends the encrypted message from the AOSTLM Client to 
 *        the AOSTLM Server and gets the response.
 *  
 * @details  This function is called by the AOSTLM Client to 
 *           send an encrypted QMI messsage to the AOSTLM  Trust
 *           Zone App using the QMI QCCI, QCSI framework. This
 *           is a synchronous message. When the function returns
 *           the response buffer contains the response received
 *           from the AOSTLM Trust Zone App. If there is no
 *           response from the AOSTLM Server within 1 second,
 *           this function returns an error.
 *  
 *           The AOSTLM Server Proxy in APSS should have
 *           completed the following: 1) AOSTLM Server
 *           Initializatiuon in the Trust Zone 2) Reading and
 *           sending of all the license files to the AOSTLM
 *           Linces Server 3) On successful parsing and
 *           validation of the license files by the License
 *           Server in the Trust Zone, started the AOSTLM QMI
 *           Server.
 *
 * @param [in]reqData: encrypted Data sent to the AOSTLM Server.
 * @param [in]reqLen: length of the sent encrypted data. 
 * @param [out]rspData: pointer to encrypted Data received from 
 *                      the AOSTLM Server
 * @param [out]rspLen: length of the received encrypted data.
 *
 * @return 0  if success. 
 * @return -1 if failure. 
 *
 * @note Blocks calling thread until a reposne or an error is 
 *       received.
 */
int aostlm_get_synchronous_server_response( const uint8  *reqData, 
                                            const uint16 reqLen,
                                            uint8  *rspData,
                                            uint16 *rspLen )
{


    int                               result = -1;
    int                               rel_result;
    qmi_idl_service_object_type       aostlm_service_object;
    aostlm_access_client_req_msg_v01  *aostlm_req_ptr = 0;
    aostlm_access_client_resp_msg_v01 *aostlm_resp_ptr = 0;
    int i = 0;

 
    if ((!reqData) || (!reqLen) || (reqLen > AOSTLM_MAX_QMI_MSG_LENGTH_V01) || (!rspData))
    {
        MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response : Invalid reqData: %d or reqLen: %d\n", reqData, reqLen ); 
        return result;
    }

    aostlm_req_ptr = (aostlm_access_client_req_msg_v01*)AOSTLM_MEM_ALLOC(sizeof(aostlm_access_client_req_msg_v01));
    if (!aostlm_req_ptr)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: failed to allocate memory for request message. \n"); 
        return result;
    }

    aostlm_resp_ptr = (aostlm_access_client_resp_msg_v01*)AOSTLM_MEM_ALLOC(sizeof(aostlm_access_client_resp_msg_v01));
    if (!aostlm_resp_ptr)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: failed to allocate memory for request message. \n"); 

        /* Release the memory allocated. */
        AOSTLM_MEM_FREE(aostlm_req_ptr);

        return result;
    }
    
    /* Initialize the AOSTLM Client. */
    aostlm_service_object = aostlm_get_service_object_v01(); 
    if (NULL == aostlm_service_object)
    {
         MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: Invalid AOSTLM QMI Service Object. \n"); 
         return result;
    }
    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: os_params = 0x%x, tcb = 0x%x, sig = %d\n", &os_params, os_params.tcb, os_params.sig); 
    os_params.tcb = rex_self();
    os_params.sig = (rex_sigs_type)AOSTLM_QMI_CLNT_WAIT_SIG;
    os_params.timer_sig = 0x20000000;
    os_params.timed_out = 0;  
    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: os_params = 0x%x, tcb = 0x%x, sig = %d\n", &os_params, os_params.tcb, os_params.sig);

    result = qmi_client_init( &aostlm_qmi_data.qmi_service_info, aostlm_service_object, aostlm_qmi_ind_cb, NULL, &os_params, &aostlm_qmi_data.qmi_client);
    if ( result != QMI_NO_ERR) 
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response : qmi_client_init() failed with result:%d\n", result ); 

        /* Release the allocated memory. */
        AOSTLM_MEM_FREE(aostlm_req_ptr);
        AOSTLM_MEM_FREE(aostlm_resp_ptr);
        return result;
    }
    MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response:  qmi_client_init() succeeded. Client handle address = 0x%x, handle = 0x%x\n", &aostlm_qmi_data.qmi_client, aostlm_qmi_data.qmi_client ); 

    /*
     * Initialize the aostlm_feature_access_req_msg_v01.
     */
    memset(aostlm_req_ptr, 0, sizeof(aostlm_access_client_req_msg_v01));
    aostlm_req_ptr->length = reqLen;
    memscpy(aostlm_req_ptr->blob, aostlm_req_ptr->length, reqData, reqLen);

    /*
     * Initialize the aostlm_feature_access_resp_msg_v01.
     */ 
    memset(aostlm_resp_ptr, 0, sizeof(aostlm_access_client_resp_msg_v01));

    /*
     * Send the request to the AOSTLM Server.
     */

    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: sending synchronous QMI message\n");
    result  = qmi_client_send_msg_sync(
                      aostlm_qmi_data.qmi_client,
                      AOSTLM_ACCESS_CLIENT_REQ_V01,
                      aostlm_req_ptr, sizeof(aostlm_access_client_req_msg_v01),
                      aostlm_resp_ptr, sizeof(aostlm_access_client_resp_msg_v01),
                      AOSTLM_QMI_CLIENT_SEND_SYNC_TIMEOUT_MICRO_SECS );


    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Received synchronous QMI Message\n");
    if ( result != QMI_NO_ERR ) 
    {
        /* Error sending the QMI message.
         * Allocated memory is released at the end of the function.
         */
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response : send failed with result: %d\n", result);  
    }
    else
    {   
        /* Successfully sent and received synchronous QMI message.
         * Check if the length and blob are valid.
         */
        if ( ( aostlm_resp_ptr->length_valid == TRUE ) &&
             ( aostlm_resp_ptr->blob_valid == TRUE ) )
        {

            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response : Response length and blob are valid. Blob length: %d\n", aostlm_resp_ptr->length ); 
            *rspLen = aostlm_resp_ptr->length;
            memscpy(rspData, *rspLen, aostlm_resp_ptr->blob, aostlm_resp_ptr->length);

#ifdef FEATURE_AOSTLM_DEBUG
{
            aostlm_access_client_resp_msg_v01 *temp_resp_ptr = 0;
            temp_resp_ptr = aostlm_resp_ptr;
            for ( i = 0; i < 10; i++)
            {
                MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response : Blob[%d] = %d\n", i, aostlm_resp_ptr->blob[i]); 
            }
}
#endif
        }
        else
        {
           /* The AOSTLM Trust Zone APP returned an invalid message length. */
           *rspLen = 0; 
        }       
    } /* end of else. Send Succeeded. */

    rel_result = aostlm_qmi_client_release( &aostlm_qmi_data );
    if ( rel_result != QMI_NO_ERR ) 
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm server response: QMI Client Release failed with result: %d\n", rel_result);   
    }

     /* Free the allocated memory */
     AOSTLM_MEM_FREE(aostlm_req_ptr);
     AOSTLM_MEM_FREE(aostlm_resp_ptr);

    return result;    
}


/**
 * @brief Discovers the AOSTLM QMI Server. 
 *  
 * @Details Only after the AOSTLM QMI Server is discovered, a 
 *          QMI message could be sent to the AOSTLM QMI Server.
 *
 * @param None.
 * 
 * @return 0  if success. 
 * @return error code if failure.
 * 
 */
int aostlm_discover_qmi_server(void)
{


    qmi_client_type              notifier;
    qmi_idl_service_object_type  aostlm_service_object;
    unsigned int                 num_services = 0;
    unsigned int                 num_entries = 0;
    int                          result = 0;

    /* Verify that the QMI service object is not NULL */
    aostlm_service_object = aostlm_get_service_object_v01(); 
    if (!aostlm_service_object)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: Getting AOSTLM QMI serivce object failed.\n");    
        return -1;
    }
    /* Set the OS Parameters needed for QCCI. This enables QCCI to indicate
       AOSTLM thread when the AOSTLM QMI Service becomes available. */
    memset(&os_params,0,sizeof(qmi_cci_os_signal_type));

    os_params.tcb = rex_self();
    os_params.sig = (rex_sigs_type)AOSTLM_QMI_CLNT_WAIT_SIG;
    os_params.timer_sig = 0x20000000;
    os_params.timed_out = 0;

    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: os_params = 0x%x, tcb = 0x%x, sig = %d\n", &os_params, os_params.tcb, os_params.sig);   

    /* Sign up to get notified when the  AOSTLM QMI Service becomes available. */
    result = qmi_client_notifier_init(aostlm_service_object, &os_params, &notifier);
    if (result != QMI_NO_ERR) 
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: aostlm qmi_client_notifier_init failed with result: %d\n", result);     
        result = qmi_client_release(notifier); 
        if (result != QMI_NO_ERR)
        {
            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: qmi_client_release() #1 result: %d\n", result); 
        }
        return -1; 
    }
    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: aostlm qmi_client_notifier_init succeeded.\n");  
        
    /* Check if the AOSTLM service is up, if not wait on a signal */
    result = qmi_client_get_service_list( aostlm_service_object, NULL, NULL, &num_services);

    MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: qmi_client_get_service_list() result: %d "
                                             "num_services = %d.\n", result, num_services);    

    if ( result != QMI_NO_ERR )
    {
        // wait for server to come up
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: wait for server to come up.\n");  
        QMI_CCI_OS_SIGNAL_WAIT( &os_params, 5000 );
        QMI_CCI_OS_SIGNAL_CLEAR( &os_params );
        if ( os_params.timed_out )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: server never came up #1"); 
            result = qmi_client_release(notifier);
            if (result != QMI_NO_ERR)
            {
                MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: qmi_client_release() #2 result: %d\n", result); 
            }
 
            return -1;

        }

        result = qmi_client_get_service_list( aostlm_service_object, NULL, NULL, &num_services);
        if ( result != QMI_NO_ERR )
        {            
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: server never came up #2"); 
            result = qmi_client_release(notifier); 
            if (result != QMI_NO_ERR)
            {
                MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: qmi_client_release() #3 result: %d\n", result); 
            }
            return -1;
        }
    }
 
    num_entries = num_services;

    /* The server has come up, store the information in info variable */
    result = qmi_client_get_service_list( aostlm_service_object, &aostlm_qmi_data.qmi_service_info, &num_entries, &num_services);
    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: qmi_client_get_service_list() returned %d "
                                         "num_entries = %d num_services = %d.\n", result, num_entries, num_services); 

    /* Sinec this thread is getting terminated, release the Notifier.
       If the notifier is not released,in a race condition mproc tries
       to set a signal to the terminated thread and it causes a crash. */
    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: Before notifier release. s_params = 0x%x, tcb = 0x%x, sig = %d\n", &os_params, os_params.tcb, os_params.sig);  
    result = qmi_client_release(notifier);
    if ( result != QMI_NO_ERR )
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: Releasing the notifier failed with QMI error code: %d", result);  
        return -1;
    } 
    MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_discover: After notifier release. s_params = 0x%x, tcb = 0x%x, sig = %d\n", &os_params, os_params.tcb, os_params.sig);  

    return 0;
}


/**
 * @brief Releases the AOSTLM QMI Client's connection with the 
 *        AOSTLM QMI Server.
 *
 * @param [in]client_data: pointer to AOSTLM QMI Client data.
 * 
 * @return 0  if success. 
 * @return error code if failure.
 * 
 */
static int aostlm_qmi_client_release( const aostlm_qmi_client_t *client_data )
{

    int result = 0;

    result = qmi_client_release(client_data->qmi_client);
    if (result != QMI_NO_ERR)
    {
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm: qmi_client_release() failed with result: %d", result);   
    }

    return result;

}

