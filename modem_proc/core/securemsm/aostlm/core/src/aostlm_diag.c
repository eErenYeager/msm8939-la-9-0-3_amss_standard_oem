/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M
 
General Description
    Diagnostic packet processing routines for AOSTLM operations.
    The Diag subsystem's send_data capability is used by AOSTLM
    to trigger test cases for MPSS AOSTLM Client.
 

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 
Qualcomm Technologies Proprietary and Confidential.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                           Edit History

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_diag.c#1 $

when           who          what, where, why
--------       --------     ----------------------------------------------------------
04/16/2014     sundarr      Created. 
06/04/2014     sundarr      Modified based on new AOSTLM architetcure. 
07/21/2014     sundarr      Addressed code review comments.  
===========================================================================*/

#ifdef __cplusplus
  extern "C" {
#endif

#include "aostlm_diag.h"
#ifdef AOSTLM_DIAG

#include <stringl/stringl.h>
#include "comdef.h"
#include "aostlm_qmi_client.h"
#include "aostlm_audiomm_wrapper.h"
#include "msg.h"
#include "aostlm_api.h"
#include "qurt.h"


/*=============================================================================
                     INTERNAL FUNCTION DECLARATIONS
=============================================================================*/

PACK(void *) aostlm_diagpkt_hdlr ( PACK(void *) req_pkt, uint16 pkt_len );


/*===========================================================================
              DATA DEFINITIONS
===========================================================================*/

 
/* 
 * For any send_data received by the AOSTLM module, the function
 * aostlm_diagpkt_hdlr() will be called.
 */
static const diagpkt_user_table_entry_type aostlm_diagpkt_tbl[] =
{
  {AOSTLM_CMD_CODE_LO, AOSTLM_CMD_CODE_HI, aostlm_diagpkt_hdlr}
};


/*=============================================================================

                         FUNCTION DEFINITIONS
=============================================================================*/


/**
 * @brief Callback function for the Diag framework for handling send_data
 *        based Unit Test cases.
 * 
 * @details  This function is the high level callback function registered
 *           with the DIAG frame work. For any Diag packet sent from 
 *           QXDM using send_data 75 105 X X, this function will be called.
 * @param [in] req_pkt: The data packet containing send_data's data
 * @param [in] pkt_len: Length of the received data
 *
 * @return The response packet.
 *  
 */

PACK(void *) aostlm_diagpkt_hdlr 
(
  PACK(void *) req_pkt,
  uint16 pkt_len
)
{
  aostlm_diag_test_req_type *req_ptr = (aostlm_diag_test_req_type *) req_pkt;
  uint8 aostlm_sub_comp = req_ptr->sub_comp_id;
  uint8 test_num = req_ptr->test_num;
  uint16 i = 0;


  MSG_4(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: CmdCode: %d, SubSysId: %d, SubComp: %d Test no: %d.\n", req_ptr->cmd_code, req_ptr->subsys_id, aostlm_sub_comp, test_num );

  /*
  The response message just reflects back the request message.
  This is done just to look at the QXDM command window and
  confirm that the command has been successfully sent and received by AOSTM.
  */
  
  const int rsp_len = sizeof( aostlm_diag_test_rsp_type );
  PACK(void *) rsp_ptr = NULL;

  rsp_ptr = (aostlm_diag_test_rsp_type *)diagpkt_subsys_alloc
                                         ( DIAG_SUBSYS_AOSTLM_TEST,
                                           req_ptr->sub_comp_id,
                                           rsp_len );

  if (rsp_ptr != NULL)
  {
    memscpy ((void *) rsp_ptr, sizeof(aostlm_diag_test_rsp_type), (void *) req_pkt, pkt_len);
  }
  else
  {
      MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: DiagPkt allocation for response failed.\n");
	  return (rsp_ptr);
  }
    
  switch (aostlm_sub_comp)
  {
      case AOSTLM_ACCESS_CLIENT:
      switch ( test_num )
      {
          // Request Access Test
          case 1:
          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_CLIENT: Access Request Test.\n");
          MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_CLIENT: num params = %d.\n", req_ptr->num_params);

          if ( req_ptr->num_params != 1)
          {
              MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Invalid number of parameters for Access Request Test");
          }
          else
          {
              aostlm_feature_id_t feature_id = req_ptr->param_1;
              aostlm_access_t grant = aostlm_req_access(feature_id);

              switch ( grant )
              {
              case AOSTLM_GRANT_COMM:
                  MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d GRANT COMMERCIAL", feature_id);
                  break;
              case AOSTLM_GRANT_EVAL:
                  MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d GRANT EVAL", feature_id);
                  break;
              case AOSTLM_DENY:
                  MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d DENIED", feature_id);
                  break;
              case AOSTLM_INVALID_RSP:
              default:
                  MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Invalid response for feature %d", feature_id);
                  break;

              }
          }
          break;
		  
          // Fetch Metadata Test
          case 2:
          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_CLIENT: Request Metadata.\n");

          aostlm_meta_data_t meta_data;
          meta_data.data_ptr = qurt_malloc(2048);

          if ( req_ptr->num_params != 1)
          {
              MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Invalid number of parameters for Metadata request Test");
          }
          else
          {
              aostlm_feature_id_t feature_id = req_ptr->param_1;
              MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Requesting Metadata for %d", feature_id);
              if ( aostlm_req_metadata(feature_id, &meta_data))
              {
                    MSG_1( MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Retrieved %d bytes of MetaData", meta_data.size);

                    MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  1st 30 bytes:");
                    for ( i = 0; i < 10 && i < meta_data.size; i++ )
                    {
                        MSG_3(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   %0x %0x %0x", meta_data.data_ptr[i*3],
                              meta_data.data_ptr[(i*3)+1],
                              meta_data.data_ptr[(i*3)+2] );
                    }
              }
              else
              {
                  MSG_1( MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Error retrieving meta data for feature %d", feature_id);
              }
          }
          qurt_free(meta_data.data_ptr);
          break;
		  
          // Query Status test		  
          case 3:
              MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM ACCESS CLIENT: Querying status");
              switch ( aostlm_status() )
              {
              case AOSTLM_STOPPED:
                  MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "   CLIENT STATUS = STOPPED");
                  break;
              case AOSTLM_CONNECTING:
                  MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "   CLIENT STATUS = CONNECTING");
                  break;
              case AOSTLM_CONNECTED:
                  MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "   CLIENT STATUS = CONNECTED");
                  break;
              case AOSTLM_CONNECT_FAIL:
                  MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "   CLIENT STATUS = CONNECT_FAIL");
                  break;
              default:
                  MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "   CLIENT STATUS = INVALID");
                  break;
              }
              break;

          case 4:

              MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM ACCESS CLIENT: Initializing wrapper for license");
              if ( req_ptr->num_params != 1 )
              {
                  MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Invalid number of parameters for license initialization test");
              }
              else
              {
                  aostlm_license_id_t feature_id = req_ptr->param_1;
                  MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Initializing license %d", feature_id);

                  aostlm_init_result_t result = aostlm_audiomm_init_license(feature_id);
                  switch ( result )
                  {
                      case AOSTLM_AUDIO_SUCCESS:
                          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: INITIALIZATION RESULT:  SUCCESS");
                          break;

                      case AOSTLM_AUDIO_NO_LICENSE:
                          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: INITIALIZATION RESULT:  NO LICENSE");
                          break;

                      case AOSTLM_AUDIO_METADATA_FAIL:
                          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: INITIALIZATION RESULT:  METADATA FAIL");
                          break;

                      default:
                          MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: INITIALIZATION RESULT:  INVALID REULT");
                  }
              }
              break;

          case 5:
              MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM ACCESS CLIENT: requesting access for module in license");
              if ( req_ptr->num_params != 2 )
              {
                  MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Invalid number of parameters for license initialization test");
              }
              else
              {
                  uint8* arg_ptr = &(req_ptr->param_1);
                  aostlm_license_id_t feature_id = *arg_ptr;
                  aostlm_module_id_t  module_id  = *(arg_ptr + 1);
                  MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: checking license %d for module %d", feature_id, module_id);

                  aostlm_access_t access = aostlm_audiomm_request_access( feature_id, module_id );
                  switch ( access )
                  {
                      case AOSTLM_GRANT_COMM:
                          MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d module id %d GRANT COMMERCIAL", 
                                feature_id, module_id);
                          break;
                      case AOSTLM_GRANT_EVAL:
                          MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d module id %d GRANT EVAL", 
                                feature_id, module_id);
                          break;
                      case AOSTLM_DENY:
                          MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Feature %d module id %d DENIED", 
                                feature_id, module_id);
                          break;
                      case AOSTLM_INVALID_RSP:
                      default:
                          MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag:  Invalid response for feature %d", 
                                feature_id, module_id);
                          break;

                  }
              }
              break;
          case 6:
              MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM ACCESS CLIENT: Requesting Audio Wrapper Dump");
              aostlm_audiomm_dump_licenses();
              break;
          default:
              MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_CLIENT: Unsupported test received: %d.\n", test_num);
              break;
		  
      }
	  break;
	  
	  case AOSTLM_ACCESS_QMI_CLIENT:
      switch ( test_num )
      {
          case 1:
              MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_QMI_CLIENT: Test 1 executed.\n");
          break;
		  
          case 2:
          {
              MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_QMI_CLIENT: Test 2 executed.\n");
          }
          break;
		  
		  default:
		      MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: AOSTLM_ACCESS_QMI_CLIENT: Unsupported test received: %d.\n", test_num);
		  break;
		  
      }
	  break;
	  


  default :
       MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Unsupported AOSTLM sub component: %d.\n", aostlm_sub_comp);
     break;
  }
 
  // I believe the following is needed to keep KW/Lint happy.
  (void)pkt_len; 
  return (rsp_ptr);

} /* aostlm_diag_test */

#endif // AOSTLM_DIAG

/**
 * @brief Registers the AOSTLM's diag packet function dispatch table with 
 *        the Diag subsystem
 * 
 * @return None. 
 *  
 */

void aostlm_diagpkt_init (void)
{
#ifdef AOSTLM_DEBUG
    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "aostlm_diag: Registering the Diag Test Table.\n");

    /* \core\api\services\diag.h defines all the various Processor enums.
       If this send_data functionality need to be implemented in another
       processor, replace DIAG_MODEM_PROC below with the appropriate processor.
       For example, to implement this capability in LPASS/ADSP replace
       DIAG_MODEM_PROC below with DIAG_QDSP6_PROC. Other changes would be 
       needed for LPASS/ADSP. For example, the whole range of test cases are
       handled at the MPSS now. Refer the aostlm_diagpkt_tbl[] defined above.
       For LPASS/ADSP implementation, the range of test cases need to be
       split between MPSS and LPASS/ADSP. Look at similar examples in code. */

    /* Register the AOSTLM Subsystem and the Diag Packet table with Diag. */
    DIAGPKT_DISPATCH_TABLE_REGISTER_PROC ( DIAG_MODEM_PROC, DIAG_SUBSYS_AOSTLM_TEST,  
                                           aostlm_diagpkt_tbl);
#endif
}
