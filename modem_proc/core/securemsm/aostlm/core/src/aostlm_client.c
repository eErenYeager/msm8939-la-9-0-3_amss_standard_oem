
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_client.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/30/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "qurt.h"
#include "qurt_mutex.h"
#include "msg.h"
#include "comdef.h"
#include "modem_mem.h"
#include "aostlm_client.h"
#include "list.h"
#include "pthread.h"

#include "aostlm_qmi_client.h"
#include "aostlm_api.h"
#include "aostlm_feature_table_api.h"
#include "aostlm_trans_mgr.h"
#include "aostlm_diag.h"
#include "aostlm_log.h"
#include "time_svc.h"

#include "aostlm_audiomm_wrapper.h"

/*===========================================================================
Local types and data
===========================================================================*/

#define INIT_THREAD_STACKSIZE 8192

// License Cache data
typedef struct 
{
    list_link_type        link;
    aostlm_license_data_t data;
} aostlm_license_cache_node_type;

// Connected Callback Data
typedef struct
{
    list_link_type      link;
    aostlm_connected_cb cb;
} aostlm_callback_list_node_type;


/*==============================================================================
  Variables
==============================================================================*/

static aostlm_client_state_t  aostlm_state = AOSTLM_STOPPED;  // System State
static list_type    aosltm_license_list;                      // Cached local licenses
static list_type    aostlm_cb_list;                           // Registered callbacks
static list_type    aostlm_true_connect_list;                 // the callbacks for truly connected components
static qurt_mutex_t aostlm_access_mutex;                      // protects access
static qurt_mutex_t aostlm_connect_mutex;                     // protects connect condvar
static qurt_cond_t  aostlm_connect_cond;                      // Set when connected
static pthread_t    aostlm_init_thread;                       // Thread that processes init

/*==============================================================================
                     INTERNAL FUNCTION DECLARATIONS
==============================================================================*/
static int aostlm_client_feature_comp_func(void* item_ptr, void* compare_val);
static boolean aostlm_client_initialize_list(void);
static void aostlm_block_until_connected(void);

/*
 * @brief Initialization thread function.  
 *  
 * @details This function is run in a thread created by aostlm_client_startup
 *          and performs the startup heavy lifting.  Initializing the transfer manager
 *          may take a while as we must wait for the AOSTLM server to be up and running.
 *          We cannot block the rcinit thread that starts the component, so that work
 *          is done here.  the connect_cond condvar is used to allow other thread to block
 *          until initialization is complete.
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
static void* aostlm_init(void* unused )
{
    aostlm_callback_list_node_type* node = NULL;
    boolean success = FALSE;

    // Start the transfer manager (May take several seconds)
    if( !aostlm_trans_mgr_startup() )
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "failed to start transfer manager" );
        success = FALSE;
    }
    else
    {
        // now that we're connected, fetch the client list and populate the local cache.
        aostlm_client_initialize_list();

        #ifdef AOSTLM_DEBUG
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "------------------------------------------------------------" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License Cache is now populated.  Tests may now be run" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, " " );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Access Request test:  send_data 75 107 0 1 1 <feature_id>" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Metadata test:        send_data 75 107 0 2 1 <feature_id>" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Query status:         send_data 75 107 0 3 0 " );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Initialize Wrapper:   send_data 75 107 0 4 1 <feature_id>" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Access Module Request:send_data 75 107 0 5 2 <feature_id> <module id>" );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Dump Wrapper List:    send_data 75 107 0 6 0 " );
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "------------------------------------------------------------" );

        #endif
        // Send a log packet to inform the world that we're now connected
        aostlm_report_t report;
        report.log_type = LOG_INITIALIZED;
        aostlm_submit_log(report);
        success = TRUE;
    }
    // lock the connect mutex
    qurt_mutex_lock(&aostlm_connect_mutex);

    // change state to indicate that we are connected

    aostlm_state = success ? AOSTLM_CONNECTED : AOSTLM_CONNECT_FAIL;
    // unblock any component waiting for this state change
    qurt_cond_broadcast(&aostlm_connect_cond);
    qurt_mutex_unlock(&aostlm_connect_mutex);

    // notify all registered waiting components
    int size = list_size(&aostlm_cb_list);
    while ( list_size(&aostlm_cb_list) > 0 )
    {        
        node = list_pop_front(&aostlm_cb_list);
        if ( node != NULL )
        {
            (*(node->cb))(); 
            AOSTLM_MEM_FREE( node );
        }
    }
    
    while ( !success )
    {
        static int num_retries = 0;
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOST:  Failed to connect, trying again" );
        success = aostlm_trans_mgr_startup();
        if ( success )
        {
            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOST:  Retry #%d was successful", num_retries++); 
        }
        else
        {
            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOST:  Retry #%d was unsuccessful", num_retries++); 
        }
    }

    aostlm_state = AOSTLM_CONNECTED;


    // notify components that registered for True connection data
    while ( list_size(&aostlm_true_connect_list) > 0 )
    {
        node = list_pop_front( &aostlm_true_connect_list );
        if ( node != NULL )
        {
            (*(node->cb))(); 
            AOSTLM_MEM_FREE( node );
        }
    }

    return NULL;

}

/*==============================================================================
                    PUBLIC FUNCTION DECLARATIONS FOR MODULE
===============================================================================*/

/**
 * @brief Starts the AOSTLM Client.  Enables QMI and populates license cache
 *  
 * @details This operation is called automatically by rcinit
 *  
 * @return none
 */
void aostlm_client_startup(void)
{
    // Initialization thread
    pthread_attr_t init_attr;
    struct sched_param init_param;    
    // Register with diag service
    aostlm_diagpkt_init();

    if ( aostlm_state == AOSTLM_STOPPED )
    {
        // Initialize our mutexes
        qurt_mutex_init(&aostlm_access_mutex);     
        qurt_mutex_init(&aostlm_connect_mutex);
        qurt_cond_init(&aostlm_connect_cond);
        list_init(&aosltm_license_list);
        list_init(&aostlm_cb_list);
        list_init(&aostlm_true_connect_list);

        // This operation will modify the connecting state.  Lock connectivity mutex
        qurt_mutex_lock(&aostlm_connect_mutex);
        aostlm_state = AOSTLM_CONNECTING;                    
                        
        // Create and start our initialization thread, so as not to block the
        // initialization context that called us

        pthread_attr_init(&init_attr);
        pthread_attr_setschedparam(&init_attr, &init_param);
        pthread_attr_setstacksize(&init_attr,INIT_THREAD_STACKSIZE);
        pthread_create(&aostlm_init_thread, &init_attr, aostlm_init, NULL);

        qurt_mutex_unlock(&aostlm_connect_mutex);
    }
}

/**
 * @brief Stops the AOSTLM client.  Clears the license cache and 
 *        disconnects from QMI
 * 
 * @return none
 */
void aostlm_client_shutdown(void)
{
    if ( aostlm_state != AOSTLM_STOPPED )
    {
        // disconnect from QMI
        qurt_mutex_lock(&aostlm_access_mutex);

        // clear cache
        list_destroy(&aosltm_license_list);
        list_destroy(&aostlm_cb_list);

        aostlm_state = AOSTLM_STOPPED;

        qurt_mutex_unlock(&aostlm_access_mutex);
    }
}

/**
 * @brief requests access for a particular feature 
 *
 * @param [in]feature_id: the identifier of the feature 
 *        requested
 *
 * @return AOSTLM_GRANT_COMM if a commercial license is 
 *            present,
 *         AOSTLM_GRANT_EVAL if an evaluation license is
 *            present
 *         AOSTLM_DENIED if no valid license is present.
 *
 * @note Blocks calling thread until access is granted or denied
 */
aostlm_access_t aostlm_req_access(aostlm_feature_id_t feature_id)
{
    aostlm_access_t grant_deny = AOSTLM_DENY;
    aostlm_license_cache_node_type* node_ptr = NULL;
    aostlm_license_t license_type = AOSTLM_INVALID_TYPE;
    aostlm_report_t report;

    aostlm_block_until_connected();

    if ( aostlm_state != AOSTLM_CONNECT_FAIL )
    {
        #ifdef AOSTLM_DEBUG
            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License request for feature id %d", feature_id );
        #endif
        // Ensure 1 at a time access
        qurt_mutex_lock(&aostlm_access_mutex);



        // look for the feature in our cache
        node_ptr = list_linear_search(&aosltm_license_list, 
                                      aostlm_client_feature_comp_func, 
                                      &feature_id);

        // check to see if we found it
        if ( node_ptr != NULL )
        {
            license_type = node_ptr->data.license_type;
        }
        
        report.feature_id = feature_id;
        report.license_type = license_type;     

        // Return a value based on the license type
        switch ( license_type )
        {
            // if commercial, grant
            case AOSTLM_LICENSE_COMM:
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Comercial License Found" ); 
                grant_deny = AOSTLM_GRANT_COMM;
                break;
            // if no license, deny
            // NOTE:  no license is different than license not found.  No License 
            //        is what you get when the subsystem queried for a feature
            //        during license config and the server said "No, I don't have that"
            //        License not found is what you get when you request a feature we 
            //        never asked to populate our cache.  Shouldn't happen but could
            case AOSTLM_NO_LICENSE: 
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "No License for feature" ); 
                grant_deny = AOSTLM_DENY;
                break;

            // if eval or not found, forward request to License Server
            case AOSTLM_LICENSE_EVAL:
            default:            
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "License was Eval or unknown, asking remote" ); 

                if( !aostlm_trans_mgr_license_req(feature_id, &grant_deny) )
                {
                    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to retrieve license data from server");
                    grant_deny = AOSTLM_DENY;
                }

                switch(grant_deny)
                {
                    case AOSTLM_GRANT_COMM:
                        license_type = AOSTLM_LICENSE_COMM;
                        break;
                    case AOSTLM_GRANT_EVAL:
                        license_type = AOSTLM_LICENSE_EVAL;
                        break;
                    case AOSTLM_DENY:
                    default:
                        license_type = AOSTLM_NO_LICENSE;
                        break;
                }

                // add to our cache
                node_ptr = modem_mem_alloc(sizeof(aostlm_license_cache_node_type), 
                                           MODEM_MEM_CLIENT_QMI_CRIT);

                if ( node_ptr != NULL)
                {
                    node_ptr->data.feature_id = feature_id;
                    node_ptr->data.license_type = license_type;
                    list_push_back(&aosltm_license_list, &(node_ptr->link));
                }
                break;
        }
         
        // build and send our log
        report.log_type = (grant_deny == AOSTLM_GRANT_COMM || grant_deny == AOSTLM_GRANT_EVAL) ? 
                 LOG_GRANT : LOG_DENY;
        aostlm_submit_log(report);

        qurt_mutex_unlock(&aostlm_access_mutex);

        #ifdef AOSTLM_DEBUG
            switch ( grant_deny )
            {
                case AOSTLM_GRANT_COMM:
                    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License GRANTED COMMERCIAL"); 
                    break;
                case AOSTLM_GRANT_EVAL:
                    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License GRANTED EVAL"); 
                    break;
                case AOSTLM_DENY:
                    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License DENIED"); 
                    break;
                default:
                    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM License unknown"); 
                    break;
            }    
        #endif
    }
    else  // connection failed
    {
        grant_deny = AOSTLM_GRANT_COMM;
    }


    return grant_deny;
    
}

/**
 * @brief Fetches metadata, if any, for a particular feature
 * 
 * * @details Caller allocates an unpopulated aostlm_meta_data_t 
 *          struct.  Operation will populate with a pointer to
 *          meta data.  Caller assumes ownership of metadata
 *          buffer, and must free when complete.
 * 
 * @param [in] feature_id: the identifier of the feature 
 *        requested
 * @param [in/out] metadata: pointer to metadata.   
 *
 * @return none
 */
boolean aostlm_req_metadata(aostlm_feature_id_t feature_id,
                            aostlm_meta_data_t* meta_ptr )
{

    aostlm_license_cache_node_type* node_ptr = NULL;
    boolean retval = FALSE;
    // Ensure this feature is actually in our list
    node_ptr = list_linear_search(&aosltm_license_list, 
                                  aostlm_client_feature_comp_func, 
                                  &feature_id);

    if ( node_ptr != NULL)
    {
        aostlm_block_until_connected();

        if ( aostlm_state != AOSTLM_CONNECT_FAIL)
        {
            // Prevent Multi-access
            qurt_mutex_lock(&aostlm_access_mutex);

            // fetch the meta data
            retval = (aostlm_trans_mgr_metadata_req(feature_id, meta_ptr)); 
            qurt_mutex_unlock(&aostlm_access_mutex);
        }
    }
    return retval;
}

/**
 * @brief Fetches the AOSTLM Status
 *
 * @return AOSTLM client status (STOPPED, CONNECTING, or 
 *         CONNECTED)
 */
aostlm_client_state_t aostlm_status(void)
{
    return aostlm_state;
}

/**
 * @brief Allows feature to register a function to be called 
 *        when the AOSTLM Client is connected to the server and
 *        may begin processing requests.
 *  
 * @details When a feature wishes to request access, but does 
 *          not want to block until the server is connected, it
 *          may register a callback to be called upon
 *          connection.  If the system is already connected, the
 *          callback will be called immediately from the thread
 *          that called aostlm_register_connected_callback.  The
 *          implementation of the callback should be light
 *          weight and return quickly so as not to hold up
 *          notification of other registered components.  
 *  
 * @return status structure with information regarding aostlm 
 *         connectivity
 *
 */
void aostlm_register_connected_callback(aostlm_connected_cb cb)
{
    aostlm_callback_list_node_type* node_ptr = NULL;
    // we're checking connection
    qurt_mutex_lock(&aostlm_connect_mutex);
    if ( aostlm_state == AOSTLM_CONNECTED || aostlm_state == AOSTLM_CONNECT_FAIL)
    {
        cb();
    }
    else
    {        
        node_ptr = modem_mem_alloc(
            sizeof(aostlm_callback_list_node_type), 
            MODEM_MEM_CLIENT_QMI_CRIT);
        
        if ( node_ptr != NULL )
        {
            node_ptr->cb = cb; 
            list_push_back(&aostlm_cb_list, &(node_ptr->link));
        }
    }
    qurt_mutex_unlock(&aostlm_connect_mutex);
}

void aostlm_register_true_connection_callback(aostlm_connected_cb cb)
{
    aostlm_callback_list_node_type* node_ptr = NULL;
    // we're checking connection
    qurt_mutex_lock(&aostlm_connect_mutex);
    if ( aostlm_state == AOSTLM_CONNECTED)
    {
        cb();
    }
    else
    {        
        node_ptr = modem_mem_alloc(
            sizeof(aostlm_callback_list_node_type), 
            MODEM_MEM_CLIENT_QMI_CRIT);
        
        if ( node_ptr != NULL )
        {
            node_ptr->cb = cb; 
            list_push_back(&aostlm_true_connect_list, &(node_ptr->link));
        }
    }
    qurt_mutex_unlock(&aostlm_connect_mutex);
}

/*==============================================================================
                    PRIVATE HELPER FUNCTION DEFINITIONS
==============================================================================*/

/**
 * @brief Searches through our list looking for a matching 
 *        feature id
 *
 * @param [in] item_ptr: the current node in our list 
 * @param [in] compare_val: the value we are comparing against 
 *
 * @return 0 if we found what we're looking for, nonzero if we 
 *         haven't
 */
static int aostlm_client_feature_comp_func(  void* item_ptr, void* compare_val_ptr )
{
    // make sure both pointers aren't null
    int retval = -1;
    if ( item_ptr != NULL && compare_val_ptr != NULL )
    {
    
        // cast compare_val to a feature id
        retval =  (((aostlm_license_cache_node_type*)item_ptr)->data.feature_id == 
                  *((aostlm_feature_id_t*)compare_val_ptr));
    }
    return retval;
}

/**
 * @brief Populates the license cache by requesting information 
 *        for each feature in our feature list
 *
 * @return TRUE if successful, FALSE otherwise
 */
static boolean aostlm_client_initialize_list(void)
{
    boolean retval = TRUE;
    uint16 num_features = 0;
    const aostlm_feature_id_t* feature_list = NULL;
    aostlm_license_cache_node_type* node_ptr = NULL;
    aostlm_license_data_t license_buf[AOSTLM_MAX_LICENSES];
    uint16 licenses_reported = 0;
    int i = 0;
    // get the features supported by this processor
    num_features = aostlm_ft_get_num_features(AOSTLM_PROC_ID);
    feature_list = aostlm_ft_get_features(AOSTLM_PROC_ID);
    // Ask the license manager for the licenses to the fetched features
    if( aostlm_trans_mgr_license_cfg(
            num_features, feature_list, 
            &licenses_reported, license_buf) )
    {
        for ( i = 0; i < licenses_reported; i++)
        {            
            node_ptr = modem_mem_alloc(
                sizeof(aostlm_license_cache_node_type), 
                MODEM_MEM_CLIENT_QMI_CRIT);


            if ( node_ptr != NULL)
            {

                node_ptr->data = license_buf[i];


                list_push_back(&aosltm_license_list, &(node_ptr->link));
            }
            else
            {
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Error allocating space for licenses" );
                break;

            }
        }
    }
    else
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to retrive licenses" );
        retval = FALSE;
    }
    return retval;
}

/**
 * @brief If we are not yet connected, blocks until the 
 *        connected condvar is set
 */
static void aostlm_block_until_connected(void)
{
    // If we haven't been started yet, block until we are
    qurt_mutex_lock(&aostlm_connect_mutex);

    if ( aostlm_state == AOSTLM_STOPPED || aostlm_state == AOSTLM_CONNECTING )
    {
        qurt_cond_wait(&aostlm_connect_cond, &aostlm_connect_mutex);
    }
    qurt_mutex_unlock(&aostlm_connect_mutex);
}
