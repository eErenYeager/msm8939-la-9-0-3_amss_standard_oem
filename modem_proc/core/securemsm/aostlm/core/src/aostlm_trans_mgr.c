
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM Transaction Manager is responsible for taking AOSTLM requests and
   pushing them to the License Manager via IPC.  It ensures the AOSTLM request
   messages are protected before delivery to QMI.


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_trans_mgr.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/03/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include <stringl/stringl.h>
#include "aostlm_trans_mgr.h"
#include "aostlm_chan_prep.h"
#include "aostlm_api.h"
#include "aostlm_common.h"
#include "aostlm_qmi_client.h"
#include "msg.h"
#include "modem_mem.h"
#include "comdef.h"
#include "qurt.h"
#include "rex.h"
#include "qmi_client.h"


/*===========================================================================
Local types and data
===========================================================================*/

/*==============================================================================
  Variables
==============================================================================*/
#define AOSTLM_SEND_DATA (1)

#define AOSTLM_SEND_COMPLETE_SIG (1)

#define AOSTLM_QMI_PIPE_DEPTH (5)

// Need lots of room to allocate send/receive messages across QMI
#define AOSTLM_WORKER_THREAD_STACK_SIZE 20480

//LA2.0 COMPILE ERROR FIX
//static uint16 aostlm_cur_pipe_entry = 0;

// State
static boolean aostlm_tm_initialized = FALSE;

// command processing pipe
static qurt_pipe_t* aostlm_qmi_pipe_ptr;

typedef struct aostlm_send_qmi_data_s
{
    uint8* send_buf_ptr;
    uint16 send_buf_len;
    uint8* recv_buf_ptr;
    uint16 recv_buf_len;
    boolean success;
} aostlm_send_qmi_data_t;

typedef union
{
    unsigned long long raw;
    aostlm_send_qmi_data_t* send_qmi_ptr;
} aostlm_send_qmi_cmd_t;

//LA2.0 COMPILE ERROR FIX
//static aostlm_send_qmi_data_t aostlm_pipe_buf[AOSTLM_QMI_PIPE_DEPTH];

// Processing thread
static pthread_t aostlm_qmi_thread;

// signal when thread is done
static qurt_signal_t aostlm_sync_signal;

/*==============================================================================
                     INTERNAL FUNCTION DECLARATIONS
==============================================================================*/
boolean aostlm_send_msg(uint8* out_msg,  uint16 out_len,
                        uint8* resp_msg, uint16 allocated_resp_len, 
                        uint16* actual_resp_len );

static void* qmi_calling_thread(void* unused);

/*==============================================================================
                     EXTERNAL FUNCTION DECLARATIONS
==============================================================================*/

/**
 * @brief Starts the transaction manager.  
 * 
 * @details Initializes QMI client
 * 
 * @return TRUE when successful, FALSE otherwise
 */
boolean aostlm_trans_mgr_startup(void)
{
    static boolean local_initialized = FALSE;
    boolean retval = TRUE;
    pthread_attr_t attr;
    struct sched_param param;

    // Only initialize our local variables if this is the first time this operation has been called
    if ( !local_initialized )
    {
        // Initialize the signal we use to sync with the worker thread
        qurt_signal_init(&aostlm_sync_signal);

        // Initialize the channel
        aostlm_cp_init();

        // Create our pipe
        qurt_pipe_attr_t qpattr;
        qurt_pipe_attr_set_elements( &qpattr, AOSTLM_QMI_PIPE_DEPTH);
        qurt_pipe_create(&aostlm_qmi_pipe_ptr, &qpattr);

        // Create a thread to wait on the pipe
        pthread_attr_init(&attr);
        pthread_attr_setschedparam(&attr, &param);
        pthread_attr_setstacksize(&attr, AOSTLM_WORKER_THREAD_STACK_SIZE);
        pthread_create(&aostlm_qmi_thread, &attr, qmi_calling_thread, NULL);
        local_initialized = TRUE;
    }

    // start QMI
    retval = (aostlm_discover_qmi_server() == 0);
    if ( !retval )
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to discover QMI server");
    }
    aostlm_tm_initialized = TRUE; 
    return retval;

}

/**
 * @brief Requests the license configuration from the License 
 *        Manager
 *  
 * @param num_features[in]: The number of elements in 
 *                     feature_ids
 * @param feature_ids_ptr[in]: We want to know the license 
 *                         information for these features
 * @param licenses_found_ptr[out]: The number of elements in 
 *                       licenses
 * @param licenses_ptr[out]: License information for each 
 *                         feature requested
 * 
 * @return TRUE when successful, FALSE otherwise
 */ 
boolean aostlm_trans_mgr_license_cfg(
    const uint16  num_features,       
    const aostlm_feature_id_t*  feature_ids_ptr,
    uint16* licenses_found_ptr, 
    aostlm_license_data_t* licenses_ptr)
{
    int i = 0;
    boolean success = TRUE;
    uint16 resp_size = 0;
    aostlm_license_config_request_msg_t config_req_msg;
    aostlm_license_config_response_msg_t response_msg;

    if( !feature_ids_ptr || !licenses_found_ptr || !licenses_ptr || !aostlm_tm_initialized || 
        num_features < 0 || num_features > AOSTLM_MAX_LICENSES)
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM TxMgr: ERROR - INVALID PARAMS");
        success = FALSE;
    }
    else
    {
        // build config request message        
        config_req_msg.msg_id = AOSTLM_CLIENT_CMD_LICENSE_CONFIG_REQUEST;
        config_req_msg.num_features = num_features;

        // Copy licenses into the message to send
        for ( i = 0; i < num_features; i++ )
        {
            config_req_msg.feature_ids[i] = feature_ids_ptr[i];
        }

        // send the message to QMI
        if( !aostlm_send_msg(
                (uint8*)(&config_req_msg), sizeof(aostlm_license_config_request_msg_t),
                (uint8*)&response_msg,     sizeof(aostlm_license_config_response_msg_t), 
                &resp_size) )

        {
            // FAIL
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to deliver aostlm_message");
            success = FALSE;
        }

        if ( resp_size == 0  )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Return Message failed");
            success = FALSE;
        }

        if ( success )
        {
            // Copy result to output parameters
            *licenses_found_ptr = response_msg.num_licenses;
            for ( i = 0; i < response_msg.num_licenses; i++ )
            {
               licenses_ptr[i].feature_id   = response_msg.licenses[i].feature_id;
               licenses_ptr[i].license_type = response_msg.licenses[i].license_type;
            }
        }
    }
    return success;
}

/**
 * @brief Requests access to a particular feature
 *  
 * @param feature_id[in]: the feature requesting permission to 
 *                  run
 * @param license_grant_ptr[out]: indicates whether the license 
 *                         grants or denies access
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_trans_mgr_license_req(
    const aostlm_feature_id_t feature_id,
    aostlm_access_t*     license_grant_ptr)
{    
    boolean success = TRUE;
    aostlm_license_access_request_msg_t access_req_msg;
    uint16 resp_size = 0;
    aostlm_license_access_response_msg_t response_msg;


    MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "License requested for feature %d", feature_id);

    if( !license_grant_ptr || !aostlm_tm_initialized )
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM TxMgr: ERROR - INVALID PARAMS");
        success = FALSE;
    }
    else
    {
        // build our outgoing message
        access_req_msg.msg_id = AOSTLM_CLIENT_CMD_ACCESS_REQUEST;
        access_req_msg.feature_id = feature_id;


        // send
        if( !aostlm_send_msg(
                (uint8*)&access_req_msg,  sizeof(aostlm_license_access_request_msg_t),
                (uint8*)&response_msg,    sizeof(aostlm_license_access_response_msg_t), 
                &resp_size))
        {
            // FAIL
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to deliver aostlm_message");
            success = FALSE;
        }

        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "response size = %d", resp_size);

        if ( resp_size == 0 )
        {
            // FAIL
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Response was empty");
            success = FALSE;
        }

        if ( success )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM TxMgr: Sent message successfully");

            MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Response was %d", response_msg.grant);

            // Copy result to output parameters
            *license_grant_ptr = response_msg.grant;
        }
    }
    return success;
}

/**
 * @brief Requests metadata for a particular feature
 *  
 * @param feature_id[in]: the feature requesting metadata
 * @param license_grant[out]: the requested metadata.  Null if 
 *          there is none 
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_trans_mgr_metadata_req(
    const aostlm_feature_id_t feature_id,
    aostlm_meta_data_t* meta_ptr )
{
    
    boolean success = TRUE;

    aostlm_license_metadata_request_msg_t meta_req_msg;
    aostlm_license_metadata_response_msg_t response_msg;

    aostlm_license_metadata_request_msg_t* meta_req_msg_ptr;
    aostlm_license_metadata_response_msg_t* response_msg_ptr;
    uint16 resp_size = sizeof(aostlm_license_metadata_response_msg_t);

    meta_req_msg_ptr = &meta_req_msg;
    response_msg_ptr = &response_msg;


    if ( !meta_ptr || !aostlm_tm_initialized )
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM TxMgr: ERROR - INVALID PARAMS");
        success = FALSE;
    }
    else
    {
        // build our outgoing message
        meta_req_msg_ptr->msg_id = AOSTLM_CLIENT_CMD_META_DATA_REQUEST;
        meta_req_msg_ptr->feature_id = feature_id;

        if( !aostlm_send_msg(
                (uint8*)(meta_req_msg_ptr),   sizeof(aostlm_license_metadata_request_msg_t),
                (uint8*)(response_msg_ptr),   sizeof(aostlm_license_metadata_response_msg_t),
                &resp_size) )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to deliver aostlm_message");
            success = FALSE;
        }

        if ( resp_size == 0  )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Return message empty");
            success = FALSE;
        }

        if ( success )
        {
            // Copy result to output parameters
            meta_ptr->size = response_msg_ptr->size;
            memscpy(meta_ptr->data_ptr, MAX_META_SIZE, 
                    response_msg_ptr->meta_data, response_msg_ptr->size );
        }
    }

    return success;
}

/*==============================================================================
                     INTERNAL FUNCTION DEFINITIONS
==============================================================================*/


/**
 * @brief Packages the message, delivers it to QMI, and sets up 
 *        the response.
 *  
 * @param out_msg[in]: buffer to the message to send
 * @param out_len[in]: length of the out_msg 
 * @param resp_msg[out]:  The response message 
 * @param resp_len[in]:  Length of the response message 
 * @param actual_resp_len_ptr[out]:  How much repsonse we 
 *                           ACTUALLY got
 *  
 * @details Caller is responsible for allocation and maintenance 
 *          of all buffers.  Caller must allocate enough space
 *          for response message
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_send_msg(uint8* out_msg_ptr,  uint16 out_len,
                        uint8* resp_msg_ptr, uint16 allocated_resp_len, 
                        uint16* actual_resp_len_ptr )
{

    // set the current pipe entry
    aostlm_send_qmi_data_t qmi_buffers;

    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "sending message");

    boolean retval = FALSE;
    if ( out_msg_ptr != NULL &&
         resp_msg_ptr != NULL && 
         actual_resp_len_ptr != NULL )
    {
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Setting qmi buffers");
        qmi_buffers.send_buf_ptr = out_msg_ptr;
        qmi_buffers.send_buf_len = out_len;
        qmi_buffers.recv_buf_ptr = resp_msg_ptr;
        qmi_buffers.recv_buf_len = allocated_resp_len;
        qmi_buffers.success = FALSE;

        aostlm_send_qmi_cmd_t cmd;
        cmd.send_qmi_ptr = &qmi_buffers;

        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Process the message");
        // tell the thread to process the message
        qurt_pipe_send(aostlm_qmi_pipe_ptr, cmd.raw);

        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "wait for thread to return");
        // wait for thread to complete the work
        qurt_signal_wait(&aostlm_sync_signal, AOSTLM_SEND_COMPLETE_SIG, QURT_SIGNAL_ATTR_WAIT_ANY);

        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "thread returned, len = %d", qmi_buffers.recv_buf_len);

        qurt_signal_clear(&aostlm_sync_signal, AOSTLM_SEND_COMPLETE_SIG);

        // set output
        *actual_resp_len_ptr = qmi_buffers.recv_buf_len;

        retval = qmi_buffers.success;
    }
    return retval;
}

/**
 * @brief function that runs the QMI Processing thread 
 *  
 * @details This operation is needed to ensure that a suitable 
 *          thread context is used when pushing data to/from
 *          QMI.  It performs the heavy lifting involved in
 *          prepping and sending data
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
static void* qmi_calling_thread(void* unused)
{
    // Buffer into which we will place packed data
    static uint8 send_buf[AOSTLM_MAX_MSG_SIZE];
    // Buffer we receive data from QMI in packed format
    static uint8 recv_buf[AOSTLM_MAX_MSG_SIZE];
    // Buffer we will unpack QMI packed data into
    static uint8 unpacked_recv_buf[AOSTLM_MAX_MSG_SIZE];
    aostlm_send_qmi_cmd_t cmd;

    while (TRUE)
    {
        // reset the buffer sizes
        uint32 send_buf_size = AOSTLM_MAX_MSG_SIZE;
        uint16 recv_buf_size = AOSTLM_MAX_MSG_SIZE;
        uint32 unpack_buf_size = AOSTLM_MAX_MSG_SIZE;    

        cmd.raw = qurt_pipe_receive(aostlm_qmi_pipe_ptr);

        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "got buffer to send");
        // remove this
        if ( cmd.send_qmi_ptr == NULL )
        {
            MSG_1( MSG_SSID_FWS, MSG_LEGACY_HIGH, "Couldn't get buffer, got %p", cmd.send_qmi_ptr);
        }

        cmd.send_qmi_ptr->success = TRUE;

        do
        {

            // Ensure the buffers are good
            if ( !cmd.send_qmi_ptr->send_buf_ptr || 
                 !cmd.send_qmi_ptr->recv_buf_ptr )
            {
                MSG( MSG_SSID_FWS, MSG_LEGACY_HIGH, "Message data was null");
                cmd.send_qmi_ptr->success = FALSE;
                break;
            }

            // clear our reusable buffers
            memset( send_buf, 0, AOSTLM_MAX_MSG_SIZE );
            memset( recv_buf, 0, AOSTLM_MAX_MSG_SIZE );
            memset( unpacked_recv_buf, 0, AOSTLM_MAX_MSG_SIZE);

            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "packing");
            // Pack the data
            if( !aostlm_cp_pack(cmd.send_qmi_ptr->send_buf_ptr, 
                                cmd.send_qmi_ptr->send_buf_len,
                                send_buf, &send_buf_size ) )
            {
                // FAIL
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to pack AOSTLM data");        
                cmd.send_qmi_ptr->success = FALSE;
                break;
            }

            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "send to QMI");
            // Push to QMI
            if( aostlm_get_synchronous_server_response(send_buf, send_buf_size, 
                                                       recv_buf, &recv_buf_size) != QMI_NO_ERR)
            {
                // FAIL
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to deliver AOSTLM data to QMI");
                cmd.send_qmi_ptr->success = FALSE;
                break;
            }

            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "unpacking");
            // unpack response    
            if ( !aostlm_cp_unpack(recv_buf, recv_buf_size,
                                   unpacked_recv_buf, &unpack_buf_size))
            {
                // FAIL
                MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "Failed to unpack AOSTLM data");
                cmd.send_qmi_ptr->success = FALSE;
                break;
            }

            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "copy");
            // Copy unpacked data into provided recv buffer
            memscpy(cmd.send_qmi_ptr->recv_buf_ptr, cmd.send_qmi_ptr->recv_buf_len, 
                    unpacked_recv_buf, unpack_buf_size);

        }
        while(FALSE);
        MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "signal");
        // Let the caller know we're done
        qurt_signal_set(&aostlm_sync_signal, AOSTLM_SEND_COMPLETE_SIG);

    }

    return NULL;
}

