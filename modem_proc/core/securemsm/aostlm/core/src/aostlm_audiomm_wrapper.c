

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_audiomm_wrapper.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/30/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_common.h"
#include "aostlm_audiomm_wrapper.h"
#include "qurt.h"
#include "qurt_mutex.h"
#include "msg.h"
#include "comdef.h"
#include "modem_mem.h"
#include "aostlm_api.h"
#include "list.h"


/*===========================================================================
Local types and data
===========================================================================*/

//Long network to host translation
#define aostlm_ntohl(x)                                                      \
  (((((uint32)(x) & 0x000000FFU) << 24) |                                \
  (((uint32)(x) & 0x0000FF00U) <<  8) |                                  \
  (((uint32)(x) & 0x00FF0000U) >>  8) |                                  \
  (((uint32)(x) & 0xFF000000U) >> 24)))

// Short network to host translation
#define aostlm_ntohs(x)                                                      \
  (((((uint16)(x) & 0x00FF) << 8) | (((uint16)(x) & 0xFF00) >> 8)))

typedef struct
{
    list_link_type      link;
    aostlm_license_id_t license_id;
} aostlm_audiomm_pended_license_node_t;


// License Cache data
typedef struct 
{
    list_link_type        link;
    aostlm_license_id_t   license_id;
    aostlm_access_t       access;
    list_type             module_list;
} aostlm_audiomm_license_node_t;

// module enabled
typedef enum
{
    AOSTLM_MODULE_DISABLED = 0x0000,
    AOSTLM_MODULE_ENABLED  = 0x0001,
    AOSTLM_MODULE_INVALID  = 0x7FFF
} aostlm_module_access_t;

// Connected Callback Data
typedef struct
{
    list_link_type         link;
    aostlm_module_id_t     module_id;
    aostlm_module_access_t module_access;
} aostlm_audiomm_module_node_t;

/*==============================================================================
  Variables
==============================================================================*/

static list_type    aostlm_audiomm_license_list;              // Cached local licenses
static list_type    aostlm_audiomm_pended_license_list;       // Licenses not yet ready to be fetched                                                              
static qurt_mutex_t aostlm_audiomm_access_mutex;              // protects access
static boolean      aostlm_audiomm_initialized = FALSE;
/*==============================================================================
                     INTERNAL FUNCTION DECLARATIONS
==============================================================================*/
static int aostlm_audiomm_lic_comp_func(void* item_ptr, void* compare_val_ptr);
static int aostlm_audiomm_mod_comp_func(void* item_ptr, void* compare_val_ptr);
static void aostlm_audiomm_true_connect_cb(void);
/**
 * @brief Initializes the wrapper for a particular license id.  MUST be
 *        called before a module requests access.
 * 
 * @details Operation requests access for the license, and if granted,
 *          fetches the appropriate metadata.  Metadata is then parsed
 *          and used to determine which modules are enabled/disabled
 * 
 * @param [in] license_id: The identifier of the license to initialize
 *         
 * @return AOSTLM_AUDIO_SUCCESS if the operation succeeded
 *         AOSTLM_AUDIO_NO_LICENSE if access was denied
 *         AOSTLM_AUDIO_METADATA_FAIL if metadata failed to parse
 *
 *
 */
aostlm_init_result_t aostlm_audiomm_init_license(const aostlm_license_id_t license_id)
{
    aostlm_access_t grant;
    aostlm_meta_data_t metadata;
    aostlm_audiomm_license_node_t* license_node_ptr = NULL;
    aostlm_audiomm_module_node_t*  module_node_ptr  = NULL;
    aostlm_init_result_t result = AOSTLM_AUDIO_SUCCESS;
    uint32 num_modules = 0;
    uint16 data_left = 0;
    boolean parse_fail = FALSE;
    uint16* meta_ptr = NULL;
    aostlm_audiomm_pended_license_node_t* pended_license_node_ptr = NULL;
    int i;

    static const uint16 entry_size = (2 * sizeof( uint16));

    if ( !aostlm_audiomm_initialized )
    {
        // init our list and mutexes
        qurt_mutex_init(&aostlm_audiomm_access_mutex);     
        list_init(&aostlm_audiomm_license_list);
        list_init(&aostlm_audiomm_pended_license_list);
        aostlm_audiomm_initialized = TRUE;    
    }

    qurt_mutex_lock(&aostlm_audiomm_access_mutex);

    // ask aostlm_client if the license is granted or not.
    do
    {
        if ( aostlm_status() == AOSTLM_CONNECT_FAIL )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM Wrapper: Failed to connect, granting all & Pending metadata fetch/parse");

            // add this to our list of metadata to fetch
            pended_license_node_ptr = modem_mem_alloc(sizeof(aostlm_audiomm_pended_license_node_t),
                                                      MODEM_MEM_CLIENT_QMI_CRIT);
            if ( pended_license_node_ptr != NULL)
            {
                pended_license_node_ptr->license_id = license_id;
                list_push_back(&aostlm_audiomm_pended_license_list,
                               &(pended_license_node_ptr->link));
            }

            aostlm_register_true_connection_callback(aostlm_audiomm_true_connect_cb);


            break;
        }
        // check to see if we've added this element before
        license_node_ptr = list_linear_search(&aostlm_audiomm_license_list, 
                                              aostlm_audiomm_lic_comp_func,
                                              (void *)&license_id );

        if ( license_node_ptr != NULL )
        {
            // license has already been initialized.  Not an error, but we're done.
            result = license_node_ptr->access == AOSTLM_DENY ? 
                AOSTLM_AUDIO_NO_LICENSE : AOSTLM_AUDIO_SUCCESS;
            break;
        }

        // Still there?  Means this is a new request.  
        license_node_ptr = modem_mem_alloc(sizeof(aostlm_audiomm_license_node_t), 
                                           MODEM_MEM_CLIENT_QMI_CRIT);
        
        license_node_ptr->license_id = license_id;
        list_init(&(license_node_ptr->module_list));
        list_push_back(&aostlm_audiomm_license_list, &(license_node_ptr->link));

        // Find out if the we are licensed for this feature (This will consume an evaluation license)
        grant = aostlm_req_access(license_id);

        license_node_ptr->access = grant;

        if ( grant == AOSTLM_DENY )
        {
            result = AOSTLM_AUDIO_NO_LICENSE;
            // we aren't licensed for this feature.  All done.
            break;
        }
        // We are licensed for this feature.  Get the metadata to find out
        // what modules are enabled.

        metadata.data_ptr = modem_mem_alloc(2048, 
                                            MODEM_MEM_CLIENT_QMI_CRIT);

        if ( !aostlm_req_metadata(license_id, &metadata))
        {
            // our attempt to get metadata failed.
            result = AOSTLM_AUDIO_METADATA_FAIL;
            break;
        }

        // the metadata should ALWAYS at least be big enough to hold our 'num modules' param
        data_left = metadata.size;
        if ( data_left < sizeof( uint32))
        {
            result = AOSTLM_AUDIO_METADATA_FAIL;
            break;
        }
        // metadata is good.  Parse it.  May need to translate it to local endianness
        num_modules = aostlm_ntohl(*((uint32*)metadata.data_ptr));
        data_left -= sizeof(uint32);
        meta_ptr = (uint16*)(metadata.data_ptr + sizeof(uint32));

        for (i = 0; i < num_modules && !parse_fail; i++)
        {
            // make sure the metadata contains enough information for this next entry
            if ( data_left < entry_size)
            {
                parse_fail = TRUE;
            }
            else
            {
                // allocate a module node
                module_node_ptr = modem_mem_alloc(sizeof(module_node_ptr), 
                                                  MODEM_MEM_CLIENT_QMI_CRIT);

                module_node_ptr->module_id = aostlm_ntohs(*meta_ptr);
                meta_ptr++;
                module_node_ptr->module_access = aostlm_ntohs(*meta_ptr);
                meta_ptr++;
                data_left -= entry_size;
                list_push_back(&(license_node_ptr->module_list), &(module_node_ptr->link));
            }
        }
        if ( !parse_fail)
        {
            result = AOSTLM_AUDIO_SUCCESS;
        }
    }
    while(FALSE);
    qurt_mutex_unlock(&aostlm_audiomm_access_mutex);
    aostlm_audiomm_dump_licenses();
    return result;
}

/**
 * @brief allows a module to request access to operate.  
 *
 * @param [in] license_id: The identifier of the license the 
 *      module is managed by
 * 
 * @param [in] module_id:  Identifier of the module requesting 
 *        access
 *  
 * @return  AOSTLM_GRANT_COMM if the module is granted 
 *          commercial access
 *  
 *          AOSTLM_GRANT_EVAL if the module is granted
 *          evaluation license f
 *  
 *          AOSTLM_DENY if access to the module is denied
 * 
 */
aostlm_access_t aostlm_audiomm_request_access(const aostlm_license_id_t license_id,
                                              const aostlm_module_id_t  module_id)
{
    // find the license
    aostlm_audiomm_license_node_t* license_node_ptr = NULL;
    aostlm_audiomm_module_node_t*  module_node_ptr  = NULL;
    aostlm_access_t access = AOSTLM_GRANT_COMM;

    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM Wrapper: License requested");
    MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "    aostlm License %d, module %d", license_id, module_id ); 

    do
    {

        if ( aostlm_status() == AOSTLM_CONNECT_FAIL )
        {
            MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM Wrapper: Failed to connect, granting request %d.%d", license_id, module_id);
            break;
        }

        license_node_ptr = list_linear_search(&aostlm_audiomm_license_list, 
                                              aostlm_audiomm_lic_comp_func,
                                              (void *)&license_id );

        if ( license_node_ptr == NULL )
        {
            access = AOSTLM_DENY;
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   AOSTLM: MODULE DISABLED");
            break;
        }

        if ( license_node_ptr->access == AOSTLM_DENY )
        {
            access = AOSTLM_DENY;
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   AOSTLM: MODULE DISABLED");
            break;
        }

        // find the module for that license
        module_node_ptr = list_linear_search(&(license_node_ptr->module_list),
                                             aostlm_audiomm_mod_comp_func,
                                             (void *)&module_id );

        if ( module_node_ptr == NULL)
        {
            access = AOSTLM_DENY;
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   AOSTLM: MODULE DISABLED");
            break;
        }

        access = module_node_ptr->module_access == AOSTLM_MODULE_ENABLED ? 
                license_node_ptr->access : AOSTLM_DENY;

        if ( module_node_ptr->module_access == AOSTLM_MODULE_ENABLED )
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   AOSTLM: MODULE ENABLED");
        }
        else
        {
            MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   AOSTLM: MODULE DISABLED");
        }

    }
    while(FALSE);

    return access;
}

/**
 * @brief Looks for a particular license id.  
 *
 * @param [in] item_ptr: The item we are checking
 * 
 * @param [in] compare_val_ptr: The field we want item to match
 *  
 * @return  0 if the node matches the license ID we are looking 
 *          for, nonzero otherwise
 * 
 */
static int aostlm_audiomm_lic_comp_func(void* item_ptr, void* compare_val_ptr)
{
    int retval = -1;
    if ( item_ptr != NULL && compare_val_ptr != NULL )
    {
        retval = (((aostlm_audiomm_license_node_t*)(item_ptr))->license_id ==
                 *((aostlm_license_id_t*)compare_val_ptr));
    }
    return retval;
}

/**
 * @brief Looks for a particular module id.  
 *
 * @param [in] item_ptr: The item we are checking
 * 
 * @param [in] compare_val_ptr: The field we want item to match
 *  
 * @return  0 if the node matches the module ID we are looking 
 *          for, nonzero otherwise
 * 
 */
static int aostlm_audiomm_mod_comp_func(void* item_ptr, void* compare_val_ptr)
{
    int retval = -1;
    if ( item_ptr != NULL && compare_val_ptr != NULL )
    {
        retval = (((aostlm_audiomm_module_node_t*)(item_ptr))->module_id ==
                 *((aostlm_module_id_t*)compare_val_ptr));
    }
    return retval;
}

/**
 * @brief Prints out the audiomm wrapper information.  
 *
 */
void aostlm_audiomm_dump_licenses(void)
{
    aostlm_audiomm_license_node_t* list_node = NULL;
    aostlm_audiomm_module_node_t*  module_node = NULL;
    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "DUMPING LICENSE LIST" );
    list_node = list_peek_front( &aostlm_audiomm_license_list );

    while ( list_node != NULL )
    {
        MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "   License %d, access = %d", list_node->license_id, list_node->access );
        module_node = list_peek_front(&(list_node->module_list));
        while (module_node != NULL)
        {
            MSG_2(MSG_SSID_FWS, MSG_LEGACY_HIGH, "       Module %d, enabled = %d", module_node->module_id, module_node->module_access );
            module_node = list_peek_next(&(list_node->module_list), &(module_node->link));
        }
        list_node = list_peek_next(&aostlm_audiomm_license_list, &(list_node->link));
    }

}

void aostlm_audiomm_true_connect_cb(void)
{
    aostlm_audiomm_pended_license_node_t* node;
    MSG(MSG_SSID_FWS, MSG_LEGACY_HIGH, "WRAPPER TOLD WE ARE TRULY CONNECTED" );
    // fetch appropriate metadata

    while ( list_size(&aostlm_audiomm_pended_license_list) > 0 )
    {
        node = list_pop_front( &aostlm_audiomm_pended_license_list );
        if ( node != NULL )
        {
            aostlm_audiomm_init_license(node->license_id);
            AOSTLM_MEM_FREE( node );
        }
    }
}


