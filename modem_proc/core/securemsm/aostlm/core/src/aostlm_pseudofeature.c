/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_pseudofeature.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/30/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "msg.h"
#include "comdef.h"

#include "aostlm_api.h"
#include "pthread.h"
/*===========================================================================
Local types and data
===========================================================================*/
static pthread_t wait_thread;

/**
 * @brief Thread that issues commands to aostlm client
 *  
 * @param unused - not used
 *  
 * @return none
 */

static void* thread_run(void* unused)
{

    int i = 0;
    static int val = 0;

    while ( TRUE )
    {
        qurt_timer_sleep(1000000);
        MSG_1(MSG_SSID_FWS, MSG_LEGACY_HIGH, "AOSTLM_PSEUDOFEATURE tick %u", ++val);

        if ( val == 30 )
        {
            for ( int j = 1; j <= 5; j++)
            {
                MSG_1( MSG_SSID_FWS, MSG_LEGACY_HIGH, "Requesting access to feature id = %d", j);
                aostlm_access_t grant = aostlm_req_access(j);
                MSG_1( MSG_SSID_FWS, MSG_LEGACY_HIGH, "     Grant/deny = %d", grant );
            }
        }
    }
}
/*==============================================================================
                    PUBLIC FUNCTION DECLARATIONS FOR MODULE
==============================================================================*/

/**
 * @brief Starts the pseudo feature
 * 
 * @return none
 */
void aostlm_pf_startup(void)
{

    pthread_attr_t attr;
    struct sched_param param;
    pthread_attr_init(&attr);
    pthread_attr_setschedparam(&attr, &param);
    pthread_attr_setstacksize(&attr,10240);
    pthread_create(&wait_thread, &attr, thread_run, NULL);
}
