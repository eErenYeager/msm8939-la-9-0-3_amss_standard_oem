
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   Feature tables for 8916

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_feature_table.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/14   eh      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_feature_table.h"
#include "aostlm_api.h"
#include "aostlm_common.h"
#include "comdef.h"

/*===========================================================================

                     FEATURE TABLES

===========================================================================*/

/* Features supported by APPS processor*/
static const aostlm_feature_id_t APSS_FEATURE_TABLE[] =
{
    AOSTLM_APPS_FEATURE_PLACEHOLDER_1,
    AOSTLM_APPS_FEATURE_PLACEHOLDER_2,

    // ADD NEW APPS FEATURE IDS HERE

};
#define NUM_APSS_FEATURES sizeof(APSS_FEATURE_TABLE)/sizeof(aostlm_feature_id_t)

/* Features supported by ADSP processor */
static const aostlm_feature_id_t ADSP_FEATURE_TABLE[] =
{
    AOSTLM_ADSP_FEATURE_PLACEHOLDER_1,
    AOSTLM_ADSP_FEATURE_PLACEHOLDER_2,

    // ADD NEW ADSP FEATURE IDS HERE

};
#define NUM_ADSP_FEATURES sizeof(ADSP_FEATURE_TABLE)/sizeof(aostlm_feature_id_t)

/* Features supported by MPSS processor */
static const aostlm_feature_id_t MPSS_FEATURE_TABLE[] =
{
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_1,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_2,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_3,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_4,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_5,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_6,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_7,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_8,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_9,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_10,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_11,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_12,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_13,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_14,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_15,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_16,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_17,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_18,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_19,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_20,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_21,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_22,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_23,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_24,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_25,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_26,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_27,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_28,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_29,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_30,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_44,
    AOSTLM_MPSS_FEATURE_PLACEHOLDER_45,
    // ADD NEW MPSS FEATURES HERE

};
#define NUM_MPSS_FEATURES sizeof(MPSS_FEATURE_TABLE)/sizeof(aostlm_feature_id_t)

/* Trust Zone */
static const aostlm_feature_id_t TZ_FEATURE_TABLE[] =
{
    AOSTLM_TZ_FEATURE_PLACEHOLDER_1,
    // add new features here
};


#define NUM_TZ_FEATURES sizeof(TZ_FEATURE_TABLE)/sizeof(aostlm_feature_id_t)

/* Master list of all potential feature ids */
const aostlm_feature_list_data_t aostlm_feature_table[] =
{
    {NUM_APSS_FEATURES, APSS_FEATURE_TABLE},
    {NUM_ADSP_FEATURES, ADSP_FEATURE_TABLE},
    {NUM_MPSS_FEATURES, MPSS_FEATURE_TABLE},
    {NUM_TZ_FEATURES,   TZ_FEATURE_TABLE},
    // ADD NEW PROCESSORS HERE
};
