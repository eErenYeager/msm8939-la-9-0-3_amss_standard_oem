/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
API Stub

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/src/aostlm_api_stub.c#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/28/14   djc      initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include aostlm_api.h

aostlm_access_t aostlm_req_access(aostlm_feature_id_t feature_id)
{
    return AOSTLM_GRANT_COMM;
}
