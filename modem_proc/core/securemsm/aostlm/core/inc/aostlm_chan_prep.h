#ifndef AOSTLM_CHANNEL_PREP_H
#define AOSTLM_CHANNEL_PREP_H

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM Channel Prep component is responsible for packing and unpacking
   data delivered to / received from the channel.  This header should not change
   from platform to platform, but the implementation (c file) may.  In remote
   clients the implementation will use secure_message.  In local clients the
   implementation will handle authentication

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_chan_prep.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/03/2014 djc     initial rev
===========================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

/**
 * @brief Initializes the channel preparation module.  Must 
 *        be called before data can be packed or unpacked
 *  
 * @return none
 *
 */
void aostlm_cp_init(void);

/**
 * @brief Packs a message for delivery to IPC
 *  
 * @param input_msg_ptr[in]: the input payload to be packed
 * @param input_msg_len[in]: length of the input message 
 * @param output_msg_ptr[out]:  The packed message
 * @param output_msg_len_ptr[out]:  Length of the packed message
 *  
 * @details Caller is responsible for allocation and maintenance 
 *          of all buffers.  Caller must allocate enough space
 *          for output message
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_cp_pack(const uint8*  input_msg_ptr, 
                       const uint32  input_msg_len,
                       uint8*  output_msg_ptr, 
                       uint32* output_msg_len_ptr );

/**
 * @brief unpacks a message received from IPC
 *  
 * @param input_msg_ptr[in]: the input payload to be unpacked
 * @param input_msg_len[in]: length of the input message 
 * @param output_msg_ptr[out]:  The unpacked message
 * @param output_msg_len_ptr[out]:  Length of the unpacked 
 *                          message
 *  
 * @details Caller is responsible for allocation and maintenance 
 *          of all buffers.  Caller must allocate enough space
 *          for output message
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean aostlm_cp_unpack(const uint8* input_msg_ptr, 
                         const uint32 input_msg_len,
                         uint8* output_msg_ptr, 
                         uint32* output_msg_len_ptr );

#endif
