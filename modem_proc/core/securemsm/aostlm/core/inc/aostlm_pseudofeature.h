#ifndef AOSTLM_PSEUDOFEATURE_H
#define AOSTLM_PSEUDOFEATURE_H

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM pseudofeature pretends to be a feature, sending various access requests
   the aostlm client

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_pseudofeature.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/2014   eh      initial version 
04/24/2014   sr      added diagpkt handling and command queue capabilities.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/**
 * @brief Starts the AOSTLM Client.  Enables QMI and populates license cache
 * 
 * @return none
 */
void aostlm_pf_startup(void);

#endif
