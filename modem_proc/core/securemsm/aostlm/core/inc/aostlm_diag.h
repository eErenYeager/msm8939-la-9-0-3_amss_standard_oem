#ifndef AOSTLM_DIAG_H
#define AOSTLM_DIAG_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M
 
General Description
    Diagnostic packet processing routines for AOSTLM operations.
    The Diag subsystem's send_data capability is used by AOSTLM
    to trigger test cases for MPSS AOSTLM Client.
 

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 
Qualcomm Technologies Proprietary and Confidential.  
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                           Edit History

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_diag.h#1 $

when           who          what, where, why
--------       --------     ---------------------------------------------------
04/16/2014     sundarr      Created. 
06/04/2014     sundarr      Modified based on new AOSTLM architetcure. 
07/15/2014     sundarr      Cleaned up before the code review. 
07/21/2014     sundarr      Addressed code review comments. 
=============================================================================*/

#ifdef __cplusplus
  extern "C" {
#endif


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "diagcmd.h"
#include "diagpkt.h"


/*=============================================================================

                         DATA DEFINITIONS
=============================================================================*/
/* 
The send_data command is used from the QXDM command window to send a buffer of
bytes to a specific sybsystem. send_data  sends a a minimum of a 4 bytes. 
The first two bytes are used by the diag module as shown below. 
The next two bytes are within our control and we can use it in any way 
we want. AOSTLM utilizes these 4 bytes to trigger unit testing of individual 
sub components under AOSTLM. Please note, optional additional bytes could be 
defined and used as needed. 
 
Any Diag packet sent from QXDM using send_data 75 105 <x>  <x>, 
the meaning of the send_date bytes as used by AOSTLM 
is described by this table below. 
 
------------------------------------------------------------------------------
| Byte # |             Meaning                   |            Value          |
------------------------------------------------------------------------------
|    0   | Command Type - diagcmd.h              | DIAG_SUBSYS_CMD_F   75    |
------------------------------------------------------------------------------
|    1   | Sub System ID - diagcmd.h             | DIAG_SUBSYS_AOSTLM = 105  |
------------------------------------------------------------------------------
|    2   | AOSTLM SUB COMPONENTS - aostlm_diag.h | Access Client = 0         |   
|        |                                       | Access QMI Client = 1     |
------------------------------------------------------------------------------
|    3   | Test case numbers for sub components  |   1 .. n                  |
------------------------------------------------------------------------------              
   
*/

/* AOSTLM Sub Components */
typedef enum {
     AOSTLM_ACCESS_CLIENT = 0,
	 AOSTLM_ACCESS_QMI_CLIENT = 1,
	 AOSTLM_SUB_COMPONENT_MAX = 2     
} aost_sub_component_type;	 


/* The range of command codes accepted by AOSTLM */
#define AOSTLM_CMD_CODE_LO (0x0000)
#define AOSTLM_CMD_CODE_HI (0xFFFF)


typedef PACK(struct)
{

    uint8  cmd_code;    // DIAG_SUBSYS_CMD_F = 75 defined in diagcmd.h
	uint8  subsys_id;   // DIAG_SUBSYSS_AOST = 105 defined in diagcmd.h 
	uint8  sub_comp_id; // AOSTLM Sub Components defined in aostdiag.h
    uint8  test_num;    // Test case number specific to AOSTLM Sub component
    uint8  num_params;  // Number of additional parameters for the test case
    uint8  param_1;     // Additional parameter for the test case
	
}aostlm_diag_test_req_type;


typedef PACK(struct)
{

    uint8  cmd_code;    // DIAG_SUBSYS_CMD_F = 75 defined in diagcmd.h
	uint8  subsys_id;   // DIAG_SUBSYSS_AOST = 105 defined in diagcmd.h 
	uint8  sub_comp_id; // AOSTLM Sub Components defined in aostdiag.h
    uint8  test_num;    // Test case number specific to each AOSTLM Sub component
    uint8  num_params;  // Number of additional parameters for the test case
    uint8  param_1;     // Additional parameter for the test case
	
}aostlm_diag_test_rsp_type;


/*=============================================================================

                         PUBLIC FUNCTION DECLARATIONS
=============================================================================*/

/**
 * @brief Registers the AOSTLM module with the Diag framework
 * 
 * @return None.
 */
void aostlm_diagpkt_init( void );


#ifdef __cplusplus
}
#endif


#endif /* AOSTLM_DIAG */