
#ifndef AOSTLM_FEATURE_TABLE_API_H
#define AOSTLM_FEATURE_TABLE_API_H
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
Defines the features serviced by AOSTLM


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_feature_table_api.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/22/2014  djc     initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_common.h"
#include "aostlm_api.h"
#include "comdef.h"

/* Individual Processor IDs */
typedef enum
{
    AOSTLM_PROC_ID_APPS = 0,
    AOSTLM_PROC_ID_ADSP = 1,
    AOSTLM_PROC_ID_MDSP = 2,
    AOSTLM_PROC_ID_TZ   = 3,

    // Insert new proc ids here

    AOSTLM_NUM_PROC_IDS
}aostlm_proc_id_t;

/* to be defined in a processor specific feature table */
extern const aostlm_feature_list_data_t aostlm_feature_table[];

/**
 * @brief Fetches the number of feature in the table for a 
 *        particular processor id
 *  
 * @param proc - The processor performing the query 
 *  
 * @return the number of features in the table for proc
 */
const uint16 aostlm_ft_get_num_features(aostlm_proc_id_t proc);

/**
 * @brief Fetches the feature ids managed by proc's AOSTLM 
 *        client
 *  
 * @param proc - The processor performing the query 
 *  
 * @return a list of the feature ids managed by proc
 */
const aostlm_feature_list_t aostlm_ft_get_features(aostlm_proc_id_t proc );

#endif /* AOSTLM_FEATURE_TABLE_API_H */
