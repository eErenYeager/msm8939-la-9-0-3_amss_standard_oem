#ifndef AOSTLM_TRANSACTION_MANAGER_H
#define AOSTLM_TRANSACTION_MANAGER_H

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM Transaction Manager is responsible for taking AOSTLM requests and
   pushing them to the License Manager via IPC.  It ensures the AOSTLM request
   messages are protected before delivery to QMI.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_trans_mgr.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/03/2014 djc     initial rev
===========================================================================*/



/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "aostlm_api.h"
#include "aostlm_common.h"


#define MAX_META_SIZE (3*1024)

/* command ids to be sent across IPC */
typedef enum aostlm_client_cmd_e
{
  AOSTLM_CLIENT_CMD_INVALID                         = 0x0000,    /* Invalid Command */
  AOSTLM_CLIENT_CMD_LICENSE_CONFIG_REQUEST,                      /* License Configuration Request */
  AOSTLM_CLIENT_CMD_ACCESS_REQUEST,                              /* Feature Access Request */
  AOSTLM_CLIENT_CMD_META_DATA_REQUEST,                           /* Meta Data Request */
  AOSTLM_CLIENT_CMD_UNKNOWN                         = 0x7FFF     /* Unknown command */
} aostlm_client_cmd_t;

/* License Config Request Structure */
typedef struct aostlm_license_config_request_msg_s
{
  aostlm_client_cmd_t msg_id;                          /* AOSTLM Client License Config Request Command ID */
  uint16              num_features;                    /* Number of Licenses Requested */
  aostlm_feature_id_t feature_ids[AOSTLM_MAX_LICENSES];   /* Array of Feature IDs of Licenses Requested */
} aostlm_license_config_request_msg_t;

/* License Config Response Structure */
typedef struct aostlm_license_config_response_msg_s
{
  uint16                num_licenses;                    /* Number of Licenses being Returned */
  aostlm_license_data_t licenses[AOSTLM_MAX_LICENSES];   /* Array of License Config Data Structs */
} aostlm_license_config_response_msg_t;

/* License Access Request Structure */
typedef struct aostlm_license_access_request_msg_s
{
  aostlm_client_cmd_t msg_id;         /* AOSTLM Client License Access Request Command ID */
  aostlm_feature_id_t feature_id;     /* Feature ID of License Access Requested */
} aostlm_license_access_request_msg_t;

/* License Access Response Structure */
typedef struct aostlm_license_access_response_msg_s
{
  aostlm_access_t grant;    /* Response to License Access Request */
} aostlm_license_access_response_msg_t;

/* License Metadata Request Structure */
typedef struct aostlm_license_metadata_request_msg_s
{
  aostlm_client_cmd_t msg_id;       /* AOSTLM Client License Metadata Request Command ID */
  aostlm_feature_id_t feature_id;   /* Feature ID of License Metadata Requested */
} aostlm_license_metadata_request_msg_t;

/* License Metadata Response Structure */
typedef struct aostlm_license_metadata_response_msg_s
{
  uint32 size;                       /* Size of Metadata retrieved */
  uint8  meta_data[MAX_META_SIZE];   /* Metadata */
} aostlm_license_metadata_response_msg_t;


/**
 * @brief Starts the transaction manager.  
 * 
 * @details Initializes QMI client
 * 
 * @return TRUE when successful, false otherwise
 *
 */
boolean aostlm_trans_mgr_startup(void);

/**
 * @brief Requests the license configuration from the License 
 *        Manager
 *  
 * @param num_features[in]: The number of elements in 
 *                     feature_ids
 * @param feature_ids_ptr[in]: We want to know the license 
 *                         information for these features
 * @param licenses_found_ptr[out]: The number of elements in 
 *                       licenses
 * @param licenses_ptr[out]: License information for each 
 *                         feature requested
 * 
 * @return TRUE when successful, FALSE otherwise
 */ 
boolean aostlm_trans_mgr_license_cfg(
    const uint16  num_features,       
    const aostlm_feature_id_t*   feature_ids_ptr,
    uint16* licenses_found_ptr, 
    aostlm_license_data_t* licenses_ptr);

/**
 * @brief Requests access to a particular feature
 *  
 * @param feature_id[in]: the feature requesting permission to 
 *                  run
 * @param license_grant_ptr[out]: indicates whether the license 
 *                         grants or denies access
 * 
 * @return TRUE when successful, FALSE otherwise
 *
 */
boolean  aostlm_trans_mgr_license_req(
    const aostlm_feature_id_t feature_id,
    aostlm_access_t*     license_grant_ptr);

/**
 * @brief Requests metadata for a particular feature
 *  
 * @param feature_id[in]: the feature requesting metadata
 * @param license_grant[out]: the requested metadata.  Null if 
 *          there is none 
 * 
 * @return TRUE when successful, false otherwise
 *
 */
boolean aostlm_trans_mgr_metadata_req(
    const aostlm_feature_id_t feature_id,
    aostlm_meta_data_t* meta_ptr );

#endif
