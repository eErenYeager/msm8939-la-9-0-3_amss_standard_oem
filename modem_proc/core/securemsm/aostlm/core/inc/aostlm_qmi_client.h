#ifndef AOSTLM_QMI_CLIENT_H
#define AOSTLM_QMI_CLIENT_H

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM QMI Client provides QMI message transport services to the
   AOSTLM Client. It connects with the AOSTLM QMI Server on the APSS
   and delivers a synchronous message, which is then delivered to the
   AOSTLM Trust Zone App. When the synchronous call returns,
   the AOSTLM Client can access the response from the AOSTLM Trust
   Zone App.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 
Qualcomm Technologies Proprietary and Confidential.  
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_qmi_client.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when         who         what, where, why
--------     -------     ------------------------------------------------------
06/02/2014   sundarr     Initial version (8974). 
07/15/2014   sundarr     Cleaned up before code review (8916). 
07/18/2014   sundarr     Addressed code review comments. 
=============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"

/*=============================================================================

                         PUBLIC FUNCTION DECLARATIONS
=============================================================================*/


/**
 * @brief Waits for the AOSTLM QMI Server to come up.
 * 
 * @return 0 if success; error code if failure.
 */
int aostlm_discover_qmi_server(void);


/**
 * @brief Sends a synchronous QMI message to the AOSTLM QMI 
 *        Server and gets the response.
 * 
 * @return 0 if success; error code if failure.
 */
int aostlm_get_synchronous_server_response( const uint8  *reqData, 
                                            const uint16 reqLen,
                                            uint8  *rspData,
                                            uint16 *rspLen );


#ifdef __cplusplus
}
#endif


#endif /* AOSTLM_QMI_CLIENT_H */
