#ifndef AOSTLM_FEATURE_TABLE_H
#define AOSTLM_FEATURE_TABLE_H
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
Defines the features serviced by AOSTLM


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_feature_table.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/22/2014  djc     initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/*===========================================================================
    USAGE:
       This file is used to keep track of all possible features for each
       processor.  Each AOSTLM client will query the license table with its
       own processor ID to determine which features it is responsible for
       managing.  
 
       When adding a feature, define its unique ID here and add it to the
       appropriate table in the .c
===========================================================================*/

// All possible feature types

// APPS features
#define AOSTLM_APPS_FEATURE_PLACEHOLDER_1          ( 79)
#define AOSTLM_APPS_FEATURE_PLACEHOLDER_2          ( 80)

// ADSP features
#define AOSTLM_ADSP_FEATURE_PLACEHOLDER_1          ( 81)
#define AOSTLM_ADSP_FEATURE_PLACEHOLDER_2          ( 82)

// MDSP features
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_1          (1)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_2          (2)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_3          (3)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_4          (4)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_5          (5)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_6          (6)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_7          (7)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_8          (8)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_9          (9)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_10         (10)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_11         (11)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_12         (12)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_13         (13)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_14         (14)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_15         (15)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_16         (16)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_17         (17)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_18         (18)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_19         (19)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_20         (20)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_21         (21)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_22         (22)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_23         (23)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_24         (24)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_25         (25)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_26         (26)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_27         (27)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_28         (28)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_29         (29)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_30         (30)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_44         (44)
#define AOSTLM_MPSS_FEATURE_PLACEHOLDER_45         (45)

// TZ Features
#define AOSTLM_TZ_FEATURE_PLACEHOLDER_1            (88)

#endif /* AOSTLM_FEATURE_TABLE_H */
