
#ifndef AOSTLM_LOG_H
#define AOSTLM_LOG_H
/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
Processes AOSTLM Log packets


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_log.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/22/2014  djc     initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "aostlm_common.h"

/*  Individual log type codes */
typedef enum
{
    LOG_GRANT = 0,
    LOG_DENY,
    LOG_INITIALIZED,
    LOG_INITIALIZE_FAILUE,
    LOG_SERVER_CONNECTED,
    LOG_UNKNOWN_FAIL
} aost_log_code_t;

/* Log Report Struct */
typedef struct aostlm_report_s
{
  aostlm_feature_id_t feature_id;
  aostlm_license_t    license_type;  
  uint16              eval_cnt;
  aost_log_code_t     log_type;
} __attribute__ ((packed)) aostlm_report_t;

/**
 * @brief Allows clients to submit aostlm logs to the log 
 *        service
 *
 * @param report - the Log to submit
 *  
 * @return none
 *
 */
void aostlm_submit_log(aostlm_report_t report);


#endif /* AOSTLM_LOG_H */
