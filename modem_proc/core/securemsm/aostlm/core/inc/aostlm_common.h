#ifndef AOSTLM_COMMON_H
#define AOSTLM_COMMON_H

/*===========================================================================

               A S W   S E C U R I T Y   S E R V I C E S

                    A O S T L M   S U B S Y S T E M

GENERAL DESCRIPTION
   The AOSTLM client allows features to request validation of AOST licenses
   for features.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.7.24/securemsm/aostlm/core/inc/aostlm_common.h#1 $
  $DateTime: 2015/01/27 06:04:57 $
  $Author: mplp4svc $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
03/27/2014   eh      initial version 
07/21/2014   sr      Added AOSTLM_MEM_ALLOC and AOSTLM_MEM_FREE 
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "aostlm_api.h"
#include "modem_mem.h"

// Maximum number of licenses that can be passed at a time
#define AOSTLM_MAX_LICENSES 128
#define AOSTLM_MAX_MSG_SIZE 3072

/* What type of license do we have */
typedef enum 
{
  AOSTLM_LICENSE_COMM   = 0,    /* Valid Commercial License */
  AOSTLM_LICENSE_EVAL,          /* Valid Evaluation License */
  AOSTLM_NO_LICENSE,            /* Invalid License or No License */
  AOSTLM_INVALID_TYPE   = 0xF   /* Reserved - Not a Valid License Type */
} aostlm_license_t;

/* A feature id / License type pair */
typedef struct aostlm_license_data_s
{
    aostlm_feature_id_t feature_id;
    aostlm_license_t    license_type;
}aostlm_license_data_t;

/* list of license data */
typedef aostlm_license_data_t* aostlm_license_list_t;

/* list of feature ids */
//LA2.0 COMPILE ERROR FIX
typedef const aostlm_feature_id_t*   aostlm_feature_list_t;

/* A feature list structure, with num features indicated */
typedef struct aostlm_feature_list_data_s
{
    uint32                num_features;
    aostlm_feature_list_t subsystem_features;
}aostlm_feature_list_data_t;


/* modem_mem_alloc() and modem_mem_free() are used only in MPSS. */
#define AOSTLM_MEM_ALLOC( size ) modem_mem_alloc( size, MODEM_MEM_CLIENT_QMI_CRIT )
#define AOSTLM_MEM_FREE( ptr ) modem_mem_free( ptr, MODEM_MEM_CLIENT_QMI_CRIT ) 

#endif
