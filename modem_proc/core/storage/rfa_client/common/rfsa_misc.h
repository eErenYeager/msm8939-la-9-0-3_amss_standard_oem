#ifndef __RFSA_MISC_H__
#define __RFSA_MISC_H__
/******************************************************************************
 * rfsa_misc.h
 *
 * Platform common header files
 *
 * Copyright (c) 2013
 * Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 *****************************************************************************/
/*=============================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.7.24/storage/rfa_client/common/rfsa_misc.h#1 $
  $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when         who     what, where, why
----------   ---     ---------------------------------------------------------- 
2012-03-22   rh      Initial checkin
=============================================================================*/


#include <stdint.h>
#include <string.h>

#include "rfsa_lock.h"

#endif /* __RFSA_MISC_H__ */

