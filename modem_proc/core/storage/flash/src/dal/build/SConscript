#===============================================================================
#
# FLASH DAL Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2013 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/pkg/bootloaders/rel/1.2/boot_images/core/storage/flash/src/dal/build/SConscript#4 $
#  $DateTime: 2011/12/19 12:05:02 $
#  $Author: coresvc $
#  $Change: 2106076 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 05/03/13   eo      Add flash ops mechanism to enable/disable QPIC clocks
# 04/08/13   eo      Add DAL Device Config dependency for MBA
# 03/13/13   eo      Disable shared memory dependency
# 01/18/13   eo      Add run time NAND/SPI NOR driver support
# 10/15/12   sb      Add support to get the NAND parti info
# 09/05/12   sv      BAM based NAND driver support
# 08/21/12   sb      Add SPI NOR tools support for 9x25
# 07/24/12   as      Create environment key for enabling flash stats w/ build cmd
# 01/03/12   sv      Enable Flash profile support
# 07/01/11   eo      Add flash_boot_ops_setup.c for 9x15
# 06/22/11   eo      Add NAND_BOOT_DRIVER for 9x15
# 05/27/11   sv      Optimize scons script in including libraries
# 05/20/11   bb/sv   Flash driver Optimization
# 03/09/11   sv      Change flash_sleep_wrapper.c to flash_ops_setup.c 
# 10/27/10   eo      Set Flash Stats Framework to enabled as default. 
# 10/26/10   bn      Added NOR I2C support
# 10/20/10   sb      Fixed compilation errors on 9K. 
# 10/08/10   jz/sv   Cleanup & NOR tools support. 
# 10/08/10   eo      Add formalized partition support
# 09/21/10   sv      Disable watchdog for RUMI builds
# 09/03/10   nr      Added support for Flash profiling framework
# 08/19/10   eo      Remove uses flags check against 'yes' value condition. 
# 08/12/10   nr      Replaced nand_log.h with flash_log.h
# 07/28/10   eo      Add sleep handling source files. 
# 07/13/10   bb      Added support for 6615
# 05/16/10   eo      Updated OneNAND libs to include more images. 
# 05/11/10   eo      Added SPI NOR support.
# 04/27/10   sv      Disable Onenand compilation for 9K
# 04/22/10   eo      Added sflashc predef optimizations support.
# 02/23/10   jz      Fixed to use the correct build flag for babylon
# 01/19/10   jz      Added build flag to conditionally build flash_babylon_wrapper.c
# 01/04/10   jz      Added building of flash_babylon_wrapper.c
# 10/13/09   jz      Cleaned up include paths in scons build 
# 10/01/09   eo      Add DM support to MDM9k target
# 09/08/09   eo      Update MDM9K SCONS scripts to new SCONS framework
# 08/20/09   jz      Added XMEMC based OneNAND, also updated for single image
# 08/12/09   mh      Branched and updated to support SCMM target
#===============================================================================
Import('env')
env = env.Clone()

# Enable one of the following flash profiling methods. By default stats 
# framework will not be enabled in all the builds
# Flash Stats is enabled by appending the following to the build command:
# USES_FLAGS=USES_FLASH_STATS_FWK
if env.has_key('USES_FLASH_STATS_FWK'):
  FLASH_PROFILE_SFWK_ENABLE = True
else:
  FLASH_PROFILE_SFWK_ENABLE = False

FLASH_PROFILE_BABYLON_ENABLE = False
FLASH_PROFILE_LA_ENABLE = False

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/storage/flash/src/dal"

if not env.SubstPathExists(SRCPATH):
   SRCPATH = "${BUILD_ROOT}/drivers/flash/src/dal"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
# BOOT: needed for miparti.h header inclusion
# HWENGINES: needed for BAM header files 
# SERVICES: needed for identifiers like "boolean" defs etc...
# MPROC: needed when building AMSS
# SYSTEMDRIVERS: needed for hwio headers
CBSP_API = [
   'BOOT',
   'BUSES',
   'DAL',   
   'DEBUGTOOLS',
   'HAL',   
   'HWENGINES',
   'MPROC',
   'POWER',
   'SERVICES',
   'STORAGE',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',   
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)
env.RequireProtectedApi(['FLASH_EXTDRV'])
env.RequireProtectedApi(['STORAGE_FLASH'])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
FLASH_DAL_FWK_SOURCES = [
   '${BUILDPATH}/flash_dal.c',
   '${BUILDPATH}/flash_dal_fwk.c',
   '${BUILDPATH}/flash_dal_info.c',
   '${BUILDPATH}/flash_properties.c',
   '${BUILDPATH}/flash_dal_util.c',
]

FLASH_DAL_CONFIG_SOURCES = [
   '${BUILDPATH}/flash_dal_config.c',
]

FLASH_DAL_NAND_SOURCES_REDUCED = [
   '${BUILDPATH}/flash_nand_core.c',
   '${BUILDPATH}/flash_nand_init.c',
   '${BUILDPATH}/flash_onfi.c',
   '${BUILDPATH}/flash_decode_nandids.c',
   '${BUILDPATH}/flash_nand_entry_reduced.c',
   '${BUILDPATH}/flash_nand_partition.c',
   '${BUILDPATH}/flash_nand_config.c',
   '${BUILDPATH}/flash_otp_config.c',
   '${BUILDPATH}/flash_nand_get_info.c',
   '${BUILDPATH}/flash_dal_nand_reduced.c',
]

FLASH_DAL_NAND_SOURCES = [
   '${BUILDPATH}/flash_nand_core.c',
   '${BUILDPATH}/flash_nand_init.c',
   '${BUILDPATH}/flash_onfi.c',
   '${BUILDPATH}/flash_decode_nandids.c',
   '${BUILDPATH}/flash_nand_entry.c',
   '${BUILDPATH}/flash_nand_partition.c',
   '${BUILDPATH}/flash_nand_config.c',
   '${BUILDPATH}/flash_otp_config.c',
   '${BUILDPATH}/flash_dal_nand_default.c',
]

FLASH_DAL_NOR_SPI_SOURCES = [
   '${BUILDPATH}/flash_nor_entry.c',
   '${BUILDPATH}/flash_nor_spi_init.c',
   '${BUILDPATH}/flash_nor_spi_core.c',
   '${BUILDPATH}/flash_nor_partition_tbl.c',
   '${BUILDPATH}/flash_nor_partition.c',
   '${BUILDPATH}/flash_nor_spi_config.c'
]

FLASH_DAL_SPI_INT_SOURCES = [
   '${BUILDPATH}/flash_spi_wrapper_int.c',
   '${BUILDPATH}/flash_ops_setup.c',
   '${BUILDPATH}/flash_clocks.c',
]

FLASH_DAL_SPI_POLL_SOURCES = [
   '${BUILDPATH}/flash_spi_wrapper_poll.c',
]

FLASH_DAL_NOR_I2C_SOURCES = [
   '${BUILDPATH}/flash_nor_entry_reduced.c',
   '${BUILDPATH}/flash_nor_i2c_init.c',
   '${BUILDPATH}/flash_nor_i2c_core.c',
   '${BUILDPATH}/flash_nor_i2c_config.c',
]

#-------------------------------------------------------------------------------
# Allow Flash Profiling enablement on modem and apps images only.
#-------------------------------------------------------------------------------
if env.has_key('BUILD_TOOL_CHAIN') or env.has_key('BUILD_BOOT_CHAIN') :
   FLASH_PROFILE_SFWK_ENABLE = False
   FLASH_PROFILE_BABYLON_ENABLE = False
   FLASH_PROFILE_LA_ENABLE = False

if FLASH_PROFILE_SFWK_ENABLE :
   env.Append(CPPDEFINES = ["FLASH_PROFILE_STATFWK_ENABLED", 
      "FLASH_PROFILING_ENABLED"])
   FLASH_DAL_FWK_SOURCES += [
      '${BUILDPATH}/flash_profile_sfwk.c',
      '${BUILDPATH}/flash_profile_common.c',
   ]

if FLASH_PROFILE_LA_ENABLE :
   env.Append(CPPDEFINES = ["FLASH_PROFILE_LA_ENABLED", 
      "FLASH_PROFILING_ENABLED"])
   FLASH_DAL_FWK_SOURCES += [
      '${BUILDPATH}/flash_profile_la.c',
      '${BUILDPATH}/flash_profile_common.c',
   ]

#------------------------------------------------------------------------------
script_fname = '${MSM_ID}.sconscript';
if env.PathExists(script_fname):
   env.LoadSConscripts(script_fname);

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
target_images = ['HOSTDL_IMAGE',
   'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
   'APPS_IMAGE', 'CBSP_APPS_IMAGE',
   'EHOSTDL_IMAGE', 'QDSP6_SW_IMAGE',
   'CBSP_QDSP6_SW_IMAGE', 'APPSBL_BOOT_IMAGE',
   'OSBL_BOOT_IMAGE', 'NAND_BOOT_DRIVER',
   'NAND_TOOLS_IMAGE', 'NOR_TOOLS_IMAGE',
   'I2C_BOOT_DRIVER', 'SBL2_BOOT_IMAGE',
   'NAND_MBA_DRIVER',
   ]

# DAL FWK Library - Include if any flash feature is enabled
if env.has_key('ENABLE_NAND_DCPU_REDUCED') or \
env.has_key('ENABLE_NAND_DCPU') or env.has_key('ENABLE_NAND_BAM') or \
env.has_key('ENABLE_NAND_BAM_REDUCED') or env.has_key('ENABLE_NOR_SPI_INT') or \
env.has_key('ENABLE_NOR_SPI_POLL') or env.has_key('ENABLE_NOR_I2C'):
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash', FLASH_DAL_FWK_SOURCES)

   #Add flash props to DAL config
   if 'USES_DEVCFG' in env :
      DEVCFG_IMG = ['DAL_DEVCFG_IMG', 'DEVCFG_MBA_CORE_SW']
      Flash_XML_path = '${BUILD_ROOT}/core/storage/flash/src/config/flash_props.xml'
      env.AddDevCfgInfo(DEVCFG_IMG, 
      {
            'soc_xml' : [Flash_XML_path]
      })

# Libraries for NAND
if env.has_key('ENABLE_NAND_DCPU_REDUCED') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_nand_dcpu_reduced',
   FLASH_DAL_NAND_SOURCES_REDUCED)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NAND"])
elif env.has_key('ENABLE_NAND_DCPU') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_nand_dcpu', 
   FLASH_DAL_NAND_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NAND"])
elif env.has_key('ENABLE_NAND_BAM_REDUCED') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_nand_dma_reduced', 
   FLASH_DAL_NAND_SOURCES_REDUCED)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NAND"])
   env.Append(CPPDEFINES = ["FLASH_ENABLE_DMA"])
elif env.has_key('ENABLE_NAND_BAM') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_nand_dma', 
   FLASH_DAL_NAND_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NAND"])
   env.Append(CPPDEFINES = ["FLASH_ENABLE_DMA"])

# Remove SMEM dependency
if env.has_key('DISABLE_SMEM_IN_FLASH') :
   env.Append(CPPDEFINES = ["FLASH_DISABLE_SMEM"])

# Libraries for SPI NOR
if env.has_key('ENABLE_NOR_SPI_INT') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_spi_nor', 
   FLASH_DAL_NOR_SPI_SOURCES)
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_spi_int', 
   FLASH_DAL_SPI_INT_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NOR_SPI"])
elif env.has_key('ENABLE_NOR_SPI_POLL') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_spi_nor', 
   FLASH_DAL_NOR_SPI_SOURCES)
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_spi_poll', 
   FLASH_DAL_SPI_POLL_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NOR_SPI"])
   
   # Define watchdog reset register for given target 
   if env['MSM_ID'] in ['9x25']:
     env.Append(CPPDEFINES = ["FLASH_WATCHDOG_RESET_ADDR=0xFC180040"])  

# Libraries for combined NAND and SPI NOR driver support
if env.has_key('ENABLE_NAND_BAM') and env.has_key('ENABLE_NOR_SPI_INT') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_dal_config',
   FLASH_DAL_CONFIG_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_DAL_CONFIG"])
   
# Libraries for I2C NOR
if env.has_key('ENABLE_NOR_I2C') :
   env.AddLibrary(target_images, '${BUILDPATH}/DALflash_i2c_nor',
   FLASH_DAL_NOR_I2C_SOURCES)
   env.Append(CPPDEFINES = ["FLASH_ENABLE_NOR_I2C"])
