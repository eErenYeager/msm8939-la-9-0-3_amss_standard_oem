#===============================================================================
#
# 9x25 Target Specific Scons file for Flash DAL
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2012-2013 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/pkg/bootloaders/rel/1.2/boot_images/core/storage/flash/src/dal/build/9x15.sconscript#2 $
#  $DateTime: 2011/08/04 22:04:12 $
#  $Author: coresvc $
#  $Change: 1868724 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 03/13/13   eo      Disable shared memory dependency
# 01/18/13   eo      Add run time NAND/SPI NOR driver support
# 10/15/12   sb      Add support for MBA
# 09/05/12   sv      BAM based NAND driver support
# 06/20/12   sv      Initial Revision
#===============================================================================

Import('env')

nand_dcpu_reduced_images = [
   ]
   
nand_dcpu_images = [
   'HOSTDL_IMAGE', 'EHOSTDL_IMAGE',
   ]

nand_bam_reduced_images = [
   'NAND_BOOT_DRIVER', 'NAND_MBA_DRIVER',
   ]

nand_bam_images = [
   'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
   'APPS_IMAGE', 'CBSP_APPS_IMAGE',
   'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
   'NAND_TOOLS_IMAGE',
   ]

nor_spi_int_images = [
   'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
   ]

nor_spi_poll_images = [
   'NOR_TOOLS_IMAGE',
   ]

nor_i2c_images = [
   'I2C_BOOT_DRIVER',
   'NOR_TOOLS_IMAGE',
   ]

dictionary_keys = set(env.Dictionary())


# Find the intersection
nand_dcpu_images_result_keys = set(nand_dcpu_images) & dictionary_keys
nand_dcpu_reduced_images_result_keys = set(nand_dcpu_reduced_images) & dictionary_keys
nand_bam_images_result_keys = set(nand_bam_images) & dictionary_keys
nand_bam_reduced_images_result_keys = set(nand_bam_reduced_images) & dictionary_keys
nor_spi_int_images_result_keys = set(nor_spi_int_images) & dictionary_keys
nor_spi_poll_images_result_keys = set(nor_spi_poll_images) & dictionary_keys
nor_i2c_images_result_keys = set(nor_i2c_images) & dictionary_keys


#Enable the required flags for this image
if len(nor_spi_int_images_result_keys) != 0 :
   env.Replace(ENABLE_NOR_SPI_INT = 'yes')

if len(nand_dcpu_reduced_images_result_keys) != 0 :
   env.Replace(ENABLE_NAND_DCPU_REDUCED = 'yes')
elif len(nand_dcpu_images_result_keys) != 0 :
   env.Replace(ENABLE_NAND_DCPU = 'yes')
elif len(nand_bam_reduced_images_result_keys) != 0 :
   env.Replace(ENABLE_NAND_BAM_REDUCED = 'yes')
elif len(nand_bam_images_result_keys) != 0 :
   env.Replace(ENABLE_NAND_BAM = 'yes')

#Disable SMEM dependency in flash driver
if env.IsTargetEnable(['NAND_MBA_DRIVER']) :
   env.Replace(DISABLE_SMEM_IN_FLASH = 'yes')

if len(nor_spi_poll_images_result_keys) != 0 :
   env.Replace(ENABLE_NOR_SPI_POLL = 'yes')

if len(nor_i2c_images_result_keys) != 0 :
   env.Replace(ENABLE_NOR_I2C = 'yes')
