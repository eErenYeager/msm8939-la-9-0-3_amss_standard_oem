/*=======================================================================================

                           Remote Storage Client Interface

DESCRIPTION
   This file contains implementation of the QMI based Remote Storage Client.

 Copyright (c) 2012-14 by Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
=========================================================================================*/
/*=======================================================================================
                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/storage/remotefs/rmts_api.c#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

YYYY-MM-DD       who     what, where, why
--------------   ---     ----------------------------------------------------------
2014-04-23       pa      Fix banned function usage memmove
2013-12-24       sb      Klocwork fixes
2013-02-21       bb      Klocwork Fixes 
2013-01-04       ak      update read, get buffer, write for fusion cases (because of 
                         efs & hsic deadlock issue
2013-01-10       ak      Add multiclient/ SFP support
2012-11-07       ak      Initial version based on rmts_api.h for standalone
=========================================================================================*/
/** \mainpage Remote Storage Client 
 *
 *   This header file contains the Remote Storage Client (RSC) API implimentation. RSC provides a
 *  handle to its clients to access data on storage devices controlled by a remote processor.
 */

// Just for DALTF, DELETE

#include "remotefs_comm_api.h"
#include "msg.h"
//

#include <string.h>
#include <stdlib.h>
#include "qmi_cci_target_ext.h"
#include "qmi_client.h"
#include "mba_ac.h"
#include "remote_storage_v01.h"
#include "rmts_api.h"
//#include "remotefs_priv.h" 

// SCL_GOLDEN_EFS_RAM_BASE 0x400000
// add critical sections for SFP
#define MAX_NUM_CLIENTS 3
#define RMTS_OP_COMPLETE_SIG 0x80000000
#define RMTS_OP_TIMEOUT_SIG  0x20000000


struct rmts_info
{
   boolean                       client_init_done;
                                                /**< Client Init done flag   */
   qmi_idl_service_object_type   service_object;
                                                /**< Object of the server to connect */
   qmi_cci_os_signal_type        os_params;     /**< Signal on which to wait on */
   qmi_client_type               notifier;      /**< client notifier */
   uint32                        num_services;  /**< Number of Services */
   uint32                        num_entries;   /**< Number of entries in service info */
   qmi_service_info              service_info[MAX_NUM_CLIENTS]; 
                                                /**< service info */
   boolean                       crit_sec_initialized;
                                                /**< Indicates critical section initialized or not */
   rex_crit_sect_type            crit_sect; 
                                                /**< Critical section to protect the data structures */
   rex_crit_sect_type            write_crit_sect; /**< Critical section to protect writes */
};

//contains remotefs_handle_type
struct client_info
{   
    uint32 id;
	uint32 handle_fs1;
	uint32 handle_fs2;
	uint32 handle_fsg;
	uint32 handle_fsc;
	boolean                       is_fusion_guid;    /**< if guids are for fussion, so we fail the reads */
   uint64                         shared_ram_addr;    /**< Shared RAM address */
   uint32                         shared_ram_size;    /**< Shared RAM size */
   boolean                        alloc_done;         /**< Flag indicates alloc done */
   qmi_cci_os_signal_type         os_params;          /**< Signal on which to wait on */
   qmi_client_type                client;             /**< client object returned by server */
   rex_tcb_type                   *caller_tcb;        /**< TCB of calling task > */
   rex_sigs_type                  caller_sig;         /**< Signal to wait on > */

   qmi_cci_os_signal_type         write_os_params;          /**< Signal on which to wait on */
   qmi_client_type                write_client;             /**< client object returned by server */

   rex_tcb_type                   *write_caller_tcb;        /**< TCB of calling task > */
   rex_sigs_type                  write_caller_sig;         /**< Signal to wait on > */
};

static struct rmts_guid partition_FS1 = {0xEBBEADAF, 0x22C9, 0xE33B, 
                            {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}};

static struct rmts_guid partition_FS2 = {0x0A288B1F, 0x22C9, 0xE33B, 
                            {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}}; 
                                
static struct rmts_guid partition_FSG = {0x638FF8E2, 0x22C9, 0xE33B, 
                            {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}}; 

static struct rmts_guid partition_FSC = {0x57B90A16, 0x22C9, 0xE33B, 
                            {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}}; 

// add guids for fussion
static struct rmts_guid fusion_partition_FS1 =
{0x2290BE64, 0x22C9, 0xE33B, {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}};

static struct rmts_guid fusion_partition_FS2 =
{0x346C26D1, 0x22C9, 0xE33B, {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}};

static struct rmts_guid fusion_partition_FSG =
{0xBF64FB9C, 0x22C9, 0xE33B, {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}};

static struct rmts_guid fusion_partition_FSC =
{0x5CB43A64, 0x22C9, 0xE33B, {0x8F, 0x5D, 0x0E, 0x81, 0x68, 0x6A, 0x68, 0xCB}};

static boolean rmts_rc_init_done = FALSE;
static struct client_info client_data[MAX_NUM_CLIENTS]; 
static struct rmts_info rmts_init_info;
#ifdef SCL_SHARED_EFS_RAM_BASE   
static boolean rmts_fusion_init_sequence_done = FALSE;
#endif 


void rmts_rc_init(void)
{
   struct rmts_info *prmts_init_info = &rmts_init_info;
   
   memset(prmts_init_info, 0, sizeof(struct rmts_info));
   
   if (!prmts_init_info->crit_sec_initialized)
   {
      rex_init_crit_sect(&prmts_init_info->crit_sect);
	  rex_init_crit_sect(&prmts_init_info->write_crit_sect);
      prmts_init_info->crit_sec_initialized = TRUE;
   }
   
   rmts_rc_init_done = TRUE; 
   return;
}

enum rmts_status rmts_init 
(
   struct rmts_info *rmts_init_data,
   rex_tcb_type *tcb,
   rex_sigs_type sigs,
   rex_sigs_type timer_sig
)
{
   qmi_client_error_type rc;

   rex_enter_crit_sect(&rmts_init_data->crit_sect);

   if (rmts_init_data->client_init_done)
   {
      goto pass;
   }
   
   /* Verify that service_object did not return NULL */
   rmts_init_data->service_object = rmtfs_get_service_object_v01();
   if (!rmts_init_data->service_object)
   {
      goto fail;
   }

   rmts_init_data->os_params.sig = sigs;
   rmts_init_data->os_params.timed_out = 0;
   rmts_init_data->os_params.tcb = tcb;


   rc = qmi_client_notifier_init(rmts_init_data->service_object,
                                 &rmts_init_data->os_params,
                                 &rmts_init_data->notifier);
   if(rc != QMI_NO_ERR)
   {
      goto fail;
   }
   
   /* Check if the service is up, if not wait on a signal */
   while(1)
   {
      rc = qmi_client_get_service_list(rmts_init_data->service_object,
                                       NULL, NULL,
                                       (unsigned int *)&rmts_init_data->num_services);

      if(rc == QMI_NO_ERR)
      {
         break;
      }
      /* wait for server to come up */
      QMI_CCI_OS_SIGNAL_WAIT(&rmts_init_data->os_params, 0); //these macros give warning inside them, qmi teams
   };

   QMI_CCI_OS_SIGNAL_CLEAR(&rmts_init_data->os_params);
   
   rmts_init_data->num_entries = MAX_NUM_CLIENTS;
   /* The server has come up, store the information in info variable */
   rc = qmi_client_get_service_list(rmts_init_data->service_object,
                                    rmts_init_data->service_info, 
                                    (unsigned int *)&rmts_init_data->num_entries,
                                    (unsigned int *)&rmts_init_data->num_services);

   if(rc != QMI_NO_ERR)
   {
      goto fail;
   }
   
   rmts_init_data->client_init_done = TRUE;
   
pass:

   rex_leave_crit_sect(&rmts_init_data->crit_sect);
   return RMTS_STATUS_SUCCESS;

fail:
   rex_leave_crit_sect(&rmts_init_data->crit_sect);
   return RMTS_STATUS_ERROR_SERVER_DOWN;
}
  

/** \details
 * Clients of Remote Storage call rmts_get_handle to reserve a handle for future transactions. The client
 * identifies itself with input parameter client_id. A client may hold only one open handle at a time. The
 * same handle can be used for multiple read and write operations. A valid handle is merely a local token -
 * it does not indicate the condition of the remote server.
 *
 * @param[in] client_id
 *    A string identifier for the client. e.g "EFS"
 *
 * @param[out] client_handle
 *    If rmts_get_handle() succeeds, a valid handle for future transactions.
 *
 * @return enum rmts_status
 *     Success or error code. Valid error codes for this function are RMTS_STATUS_ERROR_PARAM,
 *     RMTS_STATUS_ERROR_SYSTEM
 */
enum rmts_status rmts_get_handle (char *client_id, void **client_handle)
{
    if ( 0 == strcmp("EFS" , client_id))
    {   
        client_data[1].id = 1;
        *client_handle = (void *)client_data[1].id;
		MSG_HIGH ("rmts: EFS Client recognized assigned handle",0,0,0);
        goto pass;
    }

	if ( 0 == strcmp("SFP" , client_id))
    {   
        client_data[2].id = 2;
        *client_handle = (void *)client_data[2].id;
		MSG_HIGH ("rmts: SFP Client recognized assigned handle",0,0,0);
        goto pass;
    }
    else 
    {   
		MSG_HIGH ("rmts: No Client recognized",0,0,0);
        goto fail;
    }

pass:
        return RMTS_STATUS_SUCCESS;

fail:
        *client_handle = (void *)NULL;
        return RMTS_STATUS_ERROR_PARAM;
}


static void remotefs_indicator(void)
{
 // delete this function at some point 
	int a = 0;
	a=a;
	return;
}

enum rmts_status open_partitions_on_server( struct rmts_info *prmts_init_info, struct client_info *pclient_data, rex_tcb_type *tcb, rex_sigs_type sigs, rex_sigs_type timer_sig, const char *path, uint32 *ret_fs_handle)
{
   unsigned int path_length;
   rmtfs_open_req_msg_v01  rmtfs_req;
   rmtfs_open_resp_msg_v01 rmtfs_resp;
   qmi_client_error_type rc;

   pclient_data->caller_tcb = tcb;
   pclient_data->caller_sig = sigs;
  
   pclient_data->os_params.sig = sigs;
   pclient_data->os_params.timer_sig = timer_sig;
   pclient_data->os_params.timed_out = 0; //no timeout
   pclient_data->os_params.tcb = tcb;

   rc = qmi_client_init(&prmts_init_info->service_info[0],
                        prmts_init_info->service_object,
                        (qmi_client_ind_cb) remotefs_indicator,
                        NULL, 
                        &pclient_data->os_params,
                        &pclient_data->client);


   MSG_HIGH ("QMI First time prmts_init_info SERVICE INFO[0] = %x",prmts_init_info->service_info[0].info[0],0,0);
   MSG_HIGH ("QMI First time prmts_init_info SERVICE INFO[1] = %x",prmts_init_info->service_info[0].info[1],0,0);
   MSG_HIGH ("QMI First time prmts_init_info SERVICE INFO[2] = %x",prmts_init_info->service_info[0].info[2],0,0);
   MSG_HIGH ("QMI First time prmts_init_info->service_object = %x",prmts_init_info->service_object,0,0);
   MSG_HIGH ("QMI First time pclient_data->client = %d",pclient_data->client,0,0);
   MSG_HIGH ("QMI First time pclient_data->os_params.sig = %d",pclient_data->os_params.sig,0,0);
   MSG_HIGH ("QMI First time pclient_data->os_params.timer_sig = %d",pclient_data->os_params.timer_sig,0,0);
   MSG_HIGH ("QMI First time pclient_data->os_params.tcb = %d",pclient_data->os_params.tcb,0,0);
   MSG_HIGH ("QMI First time CLINET init rmts:rc = %d",rc,0,0);

   if(rc != QMI_NO_ERR)
   {
      goto fail;
   }
   
	memset(&rmtfs_req, 0, sizeof(rmtfs_open_req_msg_v01));
	memset(&rmtfs_resp, 0, sizeof(rmtfs_open_resp_msg_v01));

	path_length = strlen (path);
   
	if ( path_length > RMTFS_MAX_FILE_PATH_V01)
	{
		MSG_HIGH ("rmts: Bad path failure",0,0,0);
		goto fail;
	}
   
   memsmove (rmtfs_req.path, path_length, path, path_length);
   rmtfs_req.path[path_length] = '\0';
   
   rc = qmi_client_send_msg_sync(pclient_data->client,
                                 QMI_RMTFS_OPEN_REQ_V01,
                                 (void *)&rmtfs_req,
                                 sizeof(rmtfs_open_req_msg_v01),
                                 (void *)&rmtfs_resp,
                                 sizeof(rmtfs_open_resp_msg_v01),
                                 0);
                                 
   if (rc != QMI_NO_ERR)
   {
      (void)qmi_client_release(pclient_data->client);
      goto fail;
   }
   
   if (rmtfs_resp.resp.result != QMI_RESULT_SUCCESS_V01)
   {
      (void)qmi_client_release(pclient_data->client);
      goto fail;
   }
   
   if (rmtfs_resp.caller_id_valid)
   {
      //pclient_data->handle_fs1 = rmtfs_resp.caller_id;
	  *ret_fs_handle = rmtfs_resp.caller_id;
	  goto pass;
   }
   else
   {
      goto fail;
   }

pass:
   return RMTS_STATUS_SUCCESS ;

fail:
   *ret_fs_handle = -1;
   return RMTS_STATUS_ERROR_SERVER_DOWN;

//serverssr:

}

enum rmts_status get_buffer_from_server(struct client_info *pclient_data, uint32 size)
{
   qmi_client_error_type rc;
   qmi_client_type client;
   rmtfs_alloc_buff_req_msg_v01  rmtfs_req;
   rmtfs_alloc_buff_resp_msg_v01 rmtfs_resp;
   
   if (pclient_data->alloc_done)
   {
      goto pass;
   }

   memset(&rmtfs_req, 0, sizeof(rmtfs_alloc_buff_req_msg_v01));
   memset(&rmtfs_resp, 0, sizeof(rmtfs_alloc_buff_resp_msg_v01));
   
   rmtfs_req.caller_id = pclient_data->handle_fs1;
   rmtfs_req.buff_size = size;
   

   client = pclient_data->client;

   rc = qmi_client_send_msg_sync(client,
                                 QMI_RMTFS_ALLOC_BUFF_REQ_V01,
                                 (void *)&rmtfs_req,
                                 sizeof(rmtfs_alloc_buff_req_msg_v01),
                                 (void *)&rmtfs_resp,
                                 sizeof(rmtfs_alloc_buff_resp_msg_v01),
                                 0);
                                 
   if (rc != QMI_NO_ERR)
   {
     goto fail;
   }
   
   if (rmtfs_resp.resp.result != QMI_RESULT_SUCCESS_V01)
   {
     goto fail;
   }
   
   if (!rmtfs_resp.buff_address_valid)
   {
      goto fail;
   }
   
   pclient_data->shared_ram_size = size;
   pclient_data->shared_ram_addr = rmtfs_resp.buff_address;

   /* Protect the shared memory using XPU */
   if (E_SUCCESS == mba_xpu_lock_region(pclient_data->shared_ram_addr,
     (pclient_data->shared_ram_addr+pclient_data->shared_ram_size-1),
     MBA_XPU_PARTITION_MODEM_EFS))
   {

   }
   else
   {
     pclient_data->shared_ram_addr = NULL;
     pclient_data->shared_ram_size = 0;
     
     goto fail;
   }

pass:
   pclient_data->alloc_done = TRUE;
   return RMTS_STATUS_SUCCESS;

fail:
   pclient_data->alloc_done = FALSE;
   return RMTS_STATUS_ERROR_NOMEM;

//serverssr:

}

enum rmts_status get_server_status(struct rmts_info *prmts_init_info)
{
   enum rmts_status status = RMTS_STATUS_ERROR_SERVER_DOWN;
   uint32 num_services = 0;
   qmi_client_error_type rc;

   if (prmts_init_info->client_init_done)
   {
      rc = qmi_client_get_service_list(prmts_init_info->service_object,
                                       NULL, NULL,
                                       (unsigned int *)&num_services);
      if(rc == QMI_NO_ERR)
      {
         status = RMTS_STATUS_SUCCESS;
      }
   }

   return status;
}

/** \details
 * rmts_get_buffer is used by the client to determine the shared RAM address where data to be synced is held.
 * Depending on the target architecture, this memory may be allocated either by the remote processor or by
 * the local processor. If the memory is assigned locally, the return is immediate. If the memory is assigned
 * remotely, the function call will block until the remote system has finished handling the request.
 *
 * @param[in] client_handle
 *    client_handle returned by rmts_get_handle
 *
 * @param[in] size
 *    size of the shared memory - the client specifies this.
 *
 * @param[out] buff
 *    Pointer to the shared memory location if allocation was succesful.
 * @param[in] os_params
 *   OS parameter required by the QMI infrastructure to preserve context
 *
 * @return enum rmts_status
 *     Success or error code. Valid error codes for this function are RMTS_STATUS_ERROR_PARAM,
 *     RMTS_STATUS_ERROR_NOT_SUPPORTED, RMTS_STATUS_ERROR_TIMEOUT, RMTS_STATUS_ERROR_QMI,
 *     RMTS_STATUS_ERROR_NOMEM and RMTS_STATUS_ERROR_SYSTEM. In all error situations buff will be
 *     explicitly set to NULL.
 *     If ERROR_NOT_SUPPORTED is returned, the client may use other information (static or build time
 *     data) to determine the memory to use for remote storage.
 */
enum rmts_status rmts_get_buffer (void *client_handle, uint8 **buff, uint32 size, rmts_os_params_t *os_params)
{
	enum rmts_status status = RMTS_STATUS_ERROR_NOMEM;
	uint32 server_fs_handle = -1;


	struct client_info *pclient_data = NULL;
	struct rmts_info *prmts_init_info = &rmts_init_info;

	if (client_data[1].id == (uint32) client_handle)
	{
       pclient_data = &client_data[1];
	   MSG_HIGH ("rmts: EFS Client needs buffer",0,0,0);
	}
	else if (client_data[2].id == (uint32) client_handle)
	{
       pclient_data = &client_data[2];
	   MSG_HIGH ("rmts: SFP Client needs buffer",0,0,0);

	   #ifdef SCL_GOLDEN_EFS_RAM_BASE

          MSG_HIGH ("rmts: Fusion SFP handling of rmts_get_buffer API",0,0,0);
          //9x25 and 9x15 SCL_GOLDEN_EFS_RAM_BASE 0x400000
          *buff = (uint8 *)SCL_GOLDEN_EFS_RAM_BASE ;

          return RMTS_STATUS_SUCCESS;

      #endif

	}
	else
	{
		MSG_HIGH ("rmts: unknown Client needs buffer",0,0,0);
		goto fail;
	}


   if (pclient_data->alloc_done) // not setting it for fusion., so stays false
   {
      goto pass;
   }

   // Special handling of Fusion case 9x25 and 9x15 untill we finish discussion with HSIC/USB
   #ifdef SCL_SHARED_EFS_RAM_BASE

      MSG_HIGH ("rmts: Fusion handling of rmts_get_buffer API",0,0,0);
      //9x25 and 9x15 SCL_SHARED_EFS_RAM_BASE 0x200000, SCL_SHARED_EFS_RAM_SIZE 768KB
      *buff = (uint8 *)SCL_SHARED_EFS_RAM_BASE ;

      return RMTS_STATUS_SUCCESS;

   #endif

	// initialize with qmi, but incase of fusion, it happens in write
	status = rmts_init (prmts_init_info, rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG);
	if (RMTS_STATUS_SUCCESS != status)
	{
		goto fail;
	}
    
	// see if server is up
	if (RMTS_STATUS_ERROR_SERVER_DOWN == get_server_status(prmts_init_info))
	{
		goto fail; 
	}

	if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fs1", &server_fs_handle))
	{
		pclient_data->handle_fs1 = server_fs_handle;
		server_fs_handle = -1;

	}
	else
	{
		goto fail;
	}

	if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fs2", &server_fs_handle))
	{
		pclient_data->handle_fs2 = server_fs_handle;
		server_fs_handle = -1;

	}
	else
	{
		goto fail;
	}

	if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fsg", &server_fs_handle))
	{
		pclient_data->handle_fsg = server_fs_handle;
		server_fs_handle = -1;

	}
	else
	{
		goto fail;
	}
	
	if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fsc", &server_fs_handle))
	{
		pclient_data->handle_fsc = server_fs_handle;
		server_fs_handle = -1;

	}
	else
	{
		goto fail;
	}
 
	if (RMTS_STATUS_SUCCESS == get_buffer_from_server(pclient_data, size))
	{

	}
	else
	{
		goto fail;
	}


pass:
   *buff = (uint8 *)(unsigned long)pclient_data->shared_ram_addr;
   return RMTS_STATUS_SUCCESS;

fail:
	*buff = (uint8 *)NULL;
	return RMTS_STATUS_ERROR_NOMEM;

}


uint32 get_partition_handle_for_guid(struct client_info *pclient_data, struct rmts_guid *guid)
{
	uint32 handle_for_guid = -1;

	if ( 0 == memcmp(guid, &partition_FS1, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fs1;
		pclient_data->is_fusion_guid = FALSE;
		goto done;
	}

	if ( 0 == memcmp(guid, &partition_FS2, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fs2;
		pclient_data->is_fusion_guid = FALSE;
		goto done;
	}

	if ( 0 == memcmp(guid, &partition_FSG, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fsg;
		pclient_data->is_fusion_guid = FALSE;
		goto done;
	}

	if ( 0 == memcmp(guid, &partition_FSC, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fsc;
		pclient_data->is_fusion_guid = FALSE;
		goto done;
	}

	if ( 0 == memcmp(guid, &fusion_partition_FS1, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fs1;
		pclient_data->is_fusion_guid = TRUE;
		goto done;
	}

	if ( 0 == memcmp(guid, &fusion_partition_FS2, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fs2;
		pclient_data->is_fusion_guid = TRUE;
		goto done;
	}

	if ( 0 == memcmp(guid, &fusion_partition_FSG, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fsg;
		pclient_data->is_fusion_guid = TRUE;
		goto done;
	}

	if ( 0 == memcmp(guid, &fusion_partition_FSC, sizeof(struct rmts_guid)) )
	{
		handle_for_guid = pclient_data->handle_fsc;
		pclient_data->is_fusion_guid = TRUE;
		goto done;
	}
	

done:
    return handle_for_guid; 

}

/** \details
 * rmts_read_iovec synchronously reads data from eMMC sectors specified in the iovec into the
 * memory region specified by the iovec.
 *
 * @param[in] client_handle
 *    client_handle returned by rmts_get_handle
 *
 * @param[in] partition_id
 *    Partition identifier (16 byte GUID). If GUIDs are not used, the partititon name.
 *
 * @param[in] iovec
 *    An iovec array describing source and destination locations.
 *
 * @param[in] size
 *    Size of the iovec array
 *
 * @param[in] os_params
 *   OS parameter required by the QMI infrastructure to preserve context
 *
 * @return enum rmts_status
 *     Success or error code as described above. Valid errors for this operation are
 * RMTS_STATUS_ERROR_PARAM, RMTS_STATUS_ERROR_SERVER_DOWN, RMTS_STATUS_ERROR_NOT_SUPPORTED,
 * RMTS_STATUS_ERROR_TIMEOUT, RMTS_STATUS_ERROR_QMI, RMTS_STATUS_ERROR_TRANSPORT and
 * RMTS_STATUS_ERROR_SYSTEM
 */
enum rmts_status rmts_read_iovec (void *client_handle,
                                  struct rmts_guid *guid,
                                  rmts_iovec_t *iovec,
                                  uint32 size,
                                  rmts_os_params_t *os_params)
{
	enum rmts_status status = RMTS_STATUS_ERROR_NOMEM;
	qmi_client_error_type rc;
	uint32 server_fs_handle = -1;
    uint32 i = 0;
    qmi_client_type client;
    rmtfs_rw_iovec_req_msg_v01  *rmtfs_req = NULL;
    rmtfs_rw_iovec_resp_msg_v01 *rmtfs_resp = NULL;
	unsigned int time_out;

	struct client_info *pclient_data = NULL;
	struct rmts_info *prmts_init_info = &rmts_init_info;

	time_out = (unsigned int) os_params->timeout_in_ms;

	if (client_data[1].id == (uint32) client_handle)
	{
       pclient_data = &client_data[1];
	   MSG_HIGH ("rmts: EFS Client read",0,0,0);
	}
	else if (client_data[2].id == (uint32) client_handle)
	{
       pclient_data = &client_data[2];
	   MSG_HIGH ("rmts: SFP Client read",0,0,0);
       // we can fail now too, but adding another check
	   #ifdef SCL_GOLDEN_EFS_RAM_BASE

          MSG_HIGH ("rmts: Fusion SFP does not support rmts_read_iovec",0,0,0);
          return RMTS_STATUS_ERROR_NOT_SUPPORTED;

      #endif

	}
	else
	{
		MSG_HIGH ("rmts: unknown Client read",0,0,0);
	    status = RMTS_STATUS_ERROR_PARAM;
        goto done;
	}

    // Special handling of Read for Fusion case 9x25 and 9x15 untill we finish discussion with HSIC/USB
   #ifdef SCL_SHARED_EFS_RAM_BASE

      MSG_HIGH ("rmts: Fusion does not support rmts_read_iovec",0,0,0);

      return RMTS_STATUS_ERROR_NOT_SUPPORTED;

   #endif

	// see if server is down
	if ( RMTS_STATUS_ERROR_SERVER_DOWN == get_server_status(prmts_init_info) )
	{
		status = RMTS_STATUS_ERROR_SERVER_DOWN;
		goto done;
	}

	server_fs_handle = get_partition_handle_for_guid(pclient_data, guid);
	if (-1 == server_fs_handle)
	{ 
		status = RMTS_STATUS_ERROR_PARAM;
	    goto done;
	}

	//see if fussion

	if ( TRUE == pclient_data->is_fusion_guid)
	{
		MSG_HIGH ("rmts: ERROR Fusion GUID used for standalone",0,0,0);
		status = RMTS_STATUS_ERROR_NOT_SUPPORTED;
		goto done;
	}
	
   rmtfs_req = (rmtfs_rw_iovec_req_msg_v01 *)
                  malloc(sizeof(rmtfs_rw_iovec_req_msg_v01));
   if (rmtfs_req == NULL)
   {
      status = RMTS_STATUS_ERROR_SYSTEM;
	  goto done;
   }

   rmtfs_resp = (rmtfs_rw_iovec_resp_msg_v01 *)
                  malloc(sizeof(rmtfs_rw_iovec_resp_msg_v01));
   if (rmtfs_resp == NULL)
   {
      status = RMTS_STATUS_ERROR_SYSTEM;
	  goto done;
   }

   memset(rmtfs_resp, 0, sizeof(rmtfs_rw_iovec_resp_msg_v01));
   memset(rmtfs_req, 0, sizeof(rmtfs_rw_iovec_req_msg_v01));

   rmtfs_req->caller_id = server_fs_handle;
   rmtfs_req->direction = RMTFS_DIRECTION_READ_V01;
   rmtfs_req->iovec_struct_len = size;
   
   for(i=0; i < size; i++)
   {
      rmtfs_req->iovec_struct[i].sector_addr = iovec[i].sector_addr;
      rmtfs_req->iovec_struct[i].data_phy_addr_offset = iovec[i].data_phy_addr;
      rmtfs_req->iovec_struct[i].num_sector = iovec[i].num_sector;
   }
   

   client = pclient_data->client;

   rc = qmi_client_send_msg_sync(client,
                                 QMI_RMTFS_RW_IOVEC_REQ_V01,
                                 (void *)rmtfs_req,
                                 sizeof(rmtfs_rw_iovec_req_msg_v01),
                                 (void *)rmtfs_resp,
                                 sizeof(rmtfs_rw_iovec_resp_msg_v01),
                                 time_out);
   
   if (rc != QMI_NO_ERR)
   {
      if (rc == QMI_SERVICE_ERR)
	  {
          status = RMTS_STATUS_ERROR_QMI; //RMTS_STATUS_ERROR_SERVER_DOWN;
	  }
      if (QMI_TIMEOUT_ERR == rc )
	  {
		  status = RMTS_STATUS_ERROR_TIMEOUT;
	  }
	  else
	  {
         status = RMTS_STATUS_ERROR_QMI;
	  }
   }
   else
   {
	  if (rmtfs_resp->resp.result == QMI_RESULT_SUCCESS_V01)
      {
         status = RMTS_STATUS_SUCCESS;
      }
	  else
	  {
	     status = RMTS_STATUS_ERROR_TRANSPORT;
	  }
   }

done:
   if ( NULL != rmtfs_req )
   free(rmtfs_req);
   if ( NULL != rmtfs_resp )
   free(rmtfs_resp);

   return status;

}

/** \details
 * rmts_write_iovec synchronously writes data into the eMMC sectors specified in the iovec
 * from the memory specified by the iovec.
 *
 * @param[in] client_handle
 *    client_handle returned by rmts_get_handle
 *
 * @param[in] partition_id
 *    Partition identifier (16 byte GUID). If GUIDs are not used, the partititon name.
 *
 * @param[in] iovec
 *    An iovec array describing source and destination locations.
 *
 * @param[in] size
 *    Size of the iovec array
 *
 * @param[in] os_params
 *   OS parameter required by the QMI infrastructure to preserve context
 *
 * @return enum rmts_status
 *     Success or error code as described above. Valid errors for this operation are
 * RMTS_STATUS_ERROR_PARAM, RMTS_STATUS_ERROR_SERVER_DOWN, RMTS_STATUS_ERROR_WRITE_PROTECTED,
 * RMTS_STATUS_ERROR_TIMEOUT, RMTS_STATUS_ERROR_QMI, RMTS_STATUS_ERROR_TRANSPORT and
 * RMTS_STATUS_ERROR_SYSTEM
 */
enum rmts_status rmts_write_iovec (void *client_handle,
                                   struct rmts_guid *guid,
                                   rmts_iovec_t *iovec,
                                   uint32 size,
                                   rmts_os_params_t *os_params)
{
	enum rmts_status status = RMTS_STATUS_ERROR_NOMEM;
	qmi_client_error_type rc;
	uint32 server_fs_handle = -1;
    uint32 i = 0;
    qmi_client_type new_write_client;
    rmtfs_rw_iovec_req_msg_v01  *rmtfs_req = NULL;
    rmtfs_rw_iovec_resp_msg_v01 *rmtfs_resp = NULL;
	unsigned int time_out;

	struct client_info *pclient_data = NULL;
	struct rmts_info *prmts_init_info = &rmts_init_info;

	if (client_data[1].id == (uint32) client_handle)
	{
       pclient_data = &client_data[1];
	   MSG_HIGH ("rmts: EFS Client write",0,0,0);
	}
	else if (client_data[2].id == (uint32) client_handle)
	{
       pclient_data = &client_data[2];
	   MSG_HIGH ("rmts: SFP Client write",0,0,0);
	}
	else
	{
	   MSG_HIGH ("rmts: unknown Client write",0,0,0);
       return RMTS_STATUS_ERROR_PARAM;
	}

    //Get critical section here and release in done
	rex_enter_crit_sect(&prmts_init_info->write_crit_sect);
	
	time_out = (unsigned int) os_params->timeout_in_ms;

	//*************************** Start special initialization of fusion**

	// Special handling of Fusion case 8x25 and 8x15 untill we finish discussion with HSIC/USB
   #ifdef SCL_SHARED_EFS_RAM_BASE   

      if (FALSE == rmts_fusion_init_sequence_done)
      {

      MSG_HIGH ("rmts: Fusion handling of write",0,0,0);
      MSG_HIGH ("rmts: Fusion rmts_init",0,0,0);
	   // initialize with qmi
	   status = rmts_init (prmts_init_info, rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG);
	   if (RMTS_STATUS_SUCCESS != status)
	   {
	   	   MSG_HIGH ("rmts: Fusion ERROR rmts_init",0,0,0);
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done;
	   }
    
	   // see if server is up
	   if (RMTS_STATUS_ERROR_SERVER_DOWN == get_server_status(prmts_init_info))
	   {
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done; 
	   }

	   if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fs1", &server_fs_handle))
	   {
		   client_data[1].handle_fs1 = server_fs_handle;
		   server_fs_handle = -1;
		   MSG_HIGH ("rmts: Fusion partition fs1 opened",0,0,0);

	   }
	   else
	   {
		   MSG_HIGH ("rmts: Fusion ERROR fs1 opening",0,0,0);
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done;
	   }

	   if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fs2", &server_fs_handle))
	   {
		   client_data[1].handle_fs2 = server_fs_handle;
		   server_fs_handle = -1;
		   MSG_HIGH ("rmts: Fusion partition fs2 opened",0,0,0);

	   }
	   else
	   {
		   MSG_HIGH ("rmts: Fusion ERROR fs2 opening",0,0,0);
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done;
	   }

	   if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fsg", &server_fs_handle))
	   {
		   client_data[1].handle_fsg = server_fs_handle;
		   client_data[2].handle_fsg = server_fs_handle;
		   server_fs_handle = -1;
		   MSG_HIGH ("rmts: Fusion partition fsg opened",0,0,0);

	   }
	   else
	   {   
		   MSG_HIGH ("rmts: Fusion ERROR fsg opening",0,0,0);
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done;
	   }
	
	   if (RMTS_STATUS_SUCCESS == open_partitions_on_server( prmts_init_info, pclient_data,rex_self(), RMTS_OP_COMPLETE_SIG, RMTS_OP_TIMEOUT_SIG, "/boot/modem_fsc", &server_fs_handle))
	   {
		   client_data[1].handle_fsc = server_fs_handle;
		   server_fs_handle = -1;
		   MSG_HIGH ("rmts: Fusion partition fsc opened",0,0,0);

	   }
	   else
	   {
		   MSG_HIGH ("rmts: Fusion ERROR fsc opening",0,0,0);
		   status = RMTS_STATUS_ERROR_SERVER_DOWN;
		   goto done;
	   }

	rmts_fusion_init_sequence_done = TRUE;

   }

	#endif

	//*************************** end special initialization of fusion ******

	// see if server is down
	if ( RMTS_STATUS_ERROR_SERVER_DOWN == get_server_status(prmts_init_info) )
	{
		status = RMTS_STATUS_ERROR_SERVER_DOWN;
		goto done;
	}
	
	MSG_HIGH ("rmts: write iovec server is up",0,0,0);

	server_fs_handle = get_partition_handle_for_guid(pclient_data, guid);
	if (-1 == server_fs_handle)
	{ 
		status = RMTS_STATUS_ERROR_PARAM;
	    goto done;
	}
	
	MSG_HIGH ("rmts: write iovec partition found",0,0,0);
	
   rmtfs_req = (rmtfs_rw_iovec_req_msg_v01 *)
                  malloc(sizeof(rmtfs_rw_iovec_req_msg_v01));
   if (rmtfs_req == NULL)
   {
      status = RMTS_STATUS_ERROR_SYSTEM;
	  goto done;
   }

   rmtfs_resp = (rmtfs_rw_iovec_resp_msg_v01 *)
                  malloc(sizeof(rmtfs_rw_iovec_resp_msg_v01));
   if (rmtfs_resp == NULL)
   {
      status = RMTS_STATUS_ERROR_SYSTEM;
	  goto done;
   }

   memset(rmtfs_resp, 0, sizeof(rmtfs_rw_iovec_resp_msg_v01));
   memset(rmtfs_req, 0, sizeof(rmtfs_rw_iovec_req_msg_v01));

   rmtfs_req->caller_id = server_fs_handle;
   rmtfs_req->direction = RMTFS_DIRECTION_WRITE_V01;
   rmtfs_req->iovec_struct_len = size;
   
   MSG_HIGH ("rmts: write populating iovec",0,0,0);
   for(i=0; i < size; i++)
   {
      rmtfs_req->iovec_struct[i].sector_addr = iovec[i].sector_addr;
      rmtfs_req->iovec_struct[i].data_phy_addr_offset = iovec[i].data_phy_addr;
      rmtfs_req->iovec_struct[i].num_sector = iovec[i].num_sector;
	  MSG_HIGH ("rmts: sector_addr= %d buffer = %d num sec = %d",rmtfs_req->iovec_struct[i].sector_addr,rmtfs_req->iovec_struct[i].data_phy_addr_offset,rmtfs_req->iovec_struct[i].num_sector);

   }
   

   pclient_data->write_caller_tcb = rex_self();
   pclient_data->write_caller_sig = os_params->sig;
  
   pclient_data->write_os_params.sig = os_params->sig;
   pclient_data->write_os_params.timer_sig = RMTS_OP_TIMEOUT_SIG;
   pclient_data->write_os_params.timed_out = 0; //no timeout
   pclient_data->write_os_params.tcb = rex_self();

   rc = qmi_client_init(&prmts_init_info->service_info[0],
                        prmts_init_info->service_object,
                        (qmi_client_ind_cb) remotefs_indicator,
                        NULL, 
                        &pclient_data->write_os_params,
                        &pclient_data->write_client);
   MSG_HIGH ("QMI WRITE prmts_init_info SERVICE INFO[0] = %x",prmts_init_info->service_info[0].info[0],0,0); 
   MSG_HIGH ("QMI WRITE prmts_init_info SERVICE INFO[1] = %x",prmts_init_info->service_info[0].info[1],0,0); 
   MSG_HIGH ("QMI WRITE prmts_init_info SERVICE INFO[2] = %x",prmts_init_info->service_info[0].info[2],0,0); 
   MSG_HIGH ("QMI WRITE prmts_init_info->service_object = %x",prmts_init_info->service_object,0,0);
   MSG_HIGH ("QMI WRITE pclient_data->write_client = %d",pclient_data->write_client,0,0);
   MSG_HIGH ("QMI WRITE pclient_data->write_os_params.sig = %d",pclient_data->write_os_params.sig,0,0);
   MSG_HIGH ("QMI WRITE pclient_data->write_os_params.timer_sig = %d",pclient_data->write_os_params.timer_sig,0,0);
   MSG_HIGH ("QMI WRITE pclient_data->write_os_params.tcb = %d",pclient_data->write_os_params.tcb,0,0);
   MSG_HIGH ("QMI WRITE CLINET init rmts:rc = %d",rc,0,0);
   

   if(rc != QMI_NO_ERR)
   {
      //print
	  status = RMTS_STATUS_ERROR_QMI;
	  MSG_HIGH ("QMI CLINET init failed rmts:rc = %d",rc,0,0);
	  MSG_HIGH ("QMI CLINET init failed in write BAD",0,0,0);
	  goto done;
   }

   ////////////////////////
   new_write_client = pclient_data->write_client;

   MSG_HIGH ("rmts: start qmi",0,0,0);
   rc = qmi_client_send_msg_sync(new_write_client,
                                 QMI_RMTFS_RW_IOVEC_REQ_V01,
                                 (void *)rmtfs_req,
                                 sizeof(rmtfs_rw_iovec_req_msg_v01),
                                 (void *)rmtfs_resp,
                                 sizeof(rmtfs_rw_iovec_resp_msg_v01),
                                 time_out);
    MSG_HIGH ("rmts:rc = %d end qmi",rc,0,0);								 
   
   if (rc != QMI_NO_ERR)
   {
      MSG_HIGH ("rc != QMI_NO_ERR",0,0,0);
      if (rc == QMI_SERVICE_ERR)
	  {
		  MSG_HIGH ("rc == QMI_SERVICE_ERR",0,0,0);
          status = RMTS_STATUS_ERROR_QMI; //RMTS_STATUS_ERROR_SERVER_DOWN;
	  }
      if (QMI_TIMEOUT_ERR == rc )
	  {
	      MSG_HIGH ("QMI_TIMEOUT_ERR == rc",0,0,0);
		  status = RMTS_STATUS_ERROR_TIMEOUT;
	  }
	  else
	  {
	     MSG_HIGH ("status = RMTS_STATUS_ERROR_QMI",0,0,0);
         status = RMTS_STATUS_ERROR_QMI;
	  }
   }
   else
   {
	  if (rmtfs_resp->resp.result == QMI_RESULT_SUCCESS_V01)
      {
	     MSG_HIGH ("rmtfs_resp->resp.result = QMI_RESULT_SUCCESS_V01",0,0,0);
         status = RMTS_STATUS_SUCCESS;
      }
	  else
	  {
	    MSG_HIGH ("rmtfs_resp->resp.result != QMI_RESULT_SUCCESS_V01",0,0,0);
	     status = RMTS_STATUS_ERROR_TRANSPORT;
	  }
   }
   
   MSG_HIGH ("rmts_before_done",0,0,0);

done:
   if ( NULL != rmtfs_req )
   free(rmtfs_req);
   if ( NULL != rmtfs_resp )
   free(rmtfs_resp);

   (void)qmi_client_release(pclient_data->write_client);

   MSG_HIGH ("Write done ",0,0,0);
   //release critical section here
   rex_leave_crit_sect(&prmts_init_info->write_crit_sect);
   return status;
}

/** \details
 * rmts_close deregisters the handle assigned in rmts_get_handle. This is used to close the
 * remote storage session opened by the client
 *
 * @param[in] client_handle
 *    client_handle returned by rmts_get_handle
 *
 * @return enum rmts_status
 *     Status - RMTS_STATUS_SUCCESS or RMTS_STATUS_ERROR_PARAM
 */
enum rmts_status rmts_close (uint32 *client_handle)
{
        return RMTS_STATUS_SUCCESS;
}



/***** FOr DALTF to compile *****/
//** will be deleted once DALTF gets updated by AST**/

void *remotefs_register_open
(
   const char *path,
   rex_tcb_type *tcb,
   rex_sigs_type sigs
)
{
	return NULL;
}

REMOTEFS_SERVER_STATUS remotefs_get_server_status(void)
{
	return 0;
}

void remotefs_get_shared_ram_addr
(
   void *handle,
   uint32** rmts_ram_addr,
   uint32* rmts_ram_size
)
{
   return; 
}

remotefs_status_type remotefs_read_iovec
(
   void *handle, 
   remotefs_iovec_desc_type *iovec_struct, 
   uint32 ent_cnt)
{
	return 0;
}

remotefs_status_type remotefs_write_iovec
(
   void *handle, 
   remotefs_iovec_desc_type *iovec_struct, 
   uint32 ent_cnt)
{
	return 0;
}
