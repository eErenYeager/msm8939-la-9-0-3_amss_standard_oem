#ifndef MSMHWIO_H
#define MSMHWIO_H
/*
===========================================================================
*/
/**
  @file msmhwio.h 

  Deprecated public interface include file for accessing the HWIO macro
  definitions.

  The msmhwio.h file is the legacy public API interface to the HW I/O (HWIO)
  register access definitions.  See HALhwio.h and DDIHWIO.h for the new
  interfaces.
*/
/*  
  ====================================================================

  Copyright (c) 2010 Qualcomm Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/msmhwio.h#1 $ $DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $
  ====================================================================
*/ 

/*=========================================================================
      Include Files
==========================================================================*/

/*
 * Include main HWIO macros.
 */
#include "HALhwio.h"


#endif /* MSMHWIO_H */

