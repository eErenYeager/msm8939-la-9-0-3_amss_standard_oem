#ifndef PM_CLK_SLEEP_H
#define PM_CLK_SLEEP_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_clk_sleep.h CLOCK SLEEP Functionality related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *        for Clock sleep functionality.
 */
/* ======================================================================= */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_clk_sleep.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/29/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/13/13   hw      Rearchitecting clk module driver to peripheral driver
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "pm_clk_buffer.h"

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

#define PM_CLK_SLEEP PM_SLEEP_CLK1

/*===========================================================================

                   SMPL FUNCTION PROTOTYPE

===========================================================================*/
/**
 * Returns the ON/OFF (enable/disable) status of the sleep clock 
 * (external XTAL oscillator).
 *
 * @datatypes
 * #pm_clk_type \n
 * #pm_on_off_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] clk_type Selects which clock type to use.
 * @param [in] on_off Returns the clock's ON/OFF status.
 *                    - PM_ON -- Clock is ON
 *                    - PM_OFF -- Clock is OFF @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS.
 *
 * @newpage 
 */
pm_err_flag_type pm_clk_sleep_src_status(uint8 pmic_device_index,
                                         pm_clk_type clk_type,
                                         pm_on_off_type* on_off);
/** @} */ /* end_addtogroup pm_clk_sleep */

#endif    /* PM_CLK_SLEEP_H */

