#ifndef PM_LDO_H
#define PM_LDO_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_ldo.h PMIC LDO related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC voltage
 *        regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_ldo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
10/21/13   rk      Support for Vunicore 8926 core compilation
10/03/13   rk      Adding one LDO
08/26/13   rk      remove pin control support and not supported LDOs.
12/06/12   hw      Rearchitecturing module driver to peripheral driver
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/** @addtogroup pm_ldo
@{ */
/**
 * @anonenum{LDO Peripheral Index}
 * Contains all LDO regulators that are required.
 */
enum
{
  PM_LDO_1,   /**< LDO 1.  */
  PM_LDO_2,   /**< LDO 2.  */
  PM_LDO_3,   /**< LDO 3.  */
  PM_LDO_4,   /**< LDO 4.  */
  PM_LDO_5,   /**< LDO 5.  */
  PM_LDO_6,   /**< LDO 6.  */
  PM_LDO_7,   /**< LDO 7.  */
  PM_LDO_8,   /**< LDO 8.  */
  PM_LDO_9,   /**< LDO 9.  */
  PM_LDO_10,  /**< LDO 10.  */
  PM_LDO_11,  /**< LDO 11.  */
  PM_LDO_12,  /**< LDO 12.  */
  PM_LDO_13,  /**< LDO 13.  */
  PM_LDO_14,  /**< LDO 14.  */
  PM_LDO_15,  /**< LDO 15.  */
  PM_LDO_16,  /**< LDO 16.  */
  PM_LDO_17,  /**< LDO 17.  */
  PM_LDO_18,  /**< LDO 18.  */
  PM_LDO_19,
  PM_LDO_20,
  PM_LDO_21,
  PM_LDO_22,
  PM_LDO_23,
  PM_LDO_24,
  PM_LDO_25,
  PM_LDO_26,
  PM_LDO_27,
  PM_LDO_28,
  PM_LDO_INVALID  /**< Invalid value.  */
};


/*===========================================================================

                    LDO DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Returns the mode status (LPM, NPM, auto, bypass) of the selected power rail. 
 *
 * @note1hang A regulator's mode changes dynamically.
 *
 * @datatypes
 * #pm_sw_mode_type
 *
 * @param [in] pmic_chip  Primary -- 0. Secondary -- 1.
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for the first LDO peripheral).
 * @param [out] sw_mode Variable with the mode status to return to the caller.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.  @newpage
 *
 */
pm_err_flag_type pm_ldo_sw_mode_status (uint8 pmic_chip,
                                        uint8 ldo_peripheral_index,
                                        pm_sw_mode_type* sw_mode);

/**
 * Returns the pin controlled status or hardware
 * enable status (ON/OFF) of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param [in] pmic_chip Primary -- 0. Secondary -- 1.
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for the first LDO peripheral).
 * @param [out] on_off Variable with the pin control status to return to the caller.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. @newpage
 *
 */
pm_err_flag_type pm_ldo_pin_ctrled_status (uint8 pmic_chip,
                                           uint8 ldo_peripheral_index,
                                           pm_on_off_type* on_off);

/**
 * Returns the voltage level (in microvolts) of the selected power rail.
 *
 * @param [in] pmic_chip Primary -- 0. Secondary -- 1.
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for the first LDO peripheral).
 * @param [out] volt_level Variable with the LDO Voltage level status to return to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. @newpage
 *
 */
pm_err_flag_type pm_ldo_volt_level_status (uint8 pmic_chip,
                                           uint8 ldo_peripheral_index,
                                           pm_volt_level_type* volt_level);

/**
 * Returns the software enable status (ON/OFF) of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param [in] pmic_chip Primary -- 0. Secondary -- 1. 
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for the first LDO peripheral).
 * @param [out] on_off Variable with the software status to return to the caller.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. 
 * @newpage
 *
 */
pm_err_flag_type pm_ldo_sw_enable_status (uint8 pmic_chip,
                                          uint8 ldo_peripheral_index,
                                          pm_on_off_type* on_off);

/**
 * Returns the local soft reset status of the selected power rail.
 *
 * @param [in] pmic_chip Primary -- 0. Secondary -- 1. 
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for first LDO peripheral).
 * @param [out] status Variable with the software status to return to the caller.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS --
 * Sucess.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. @newpage
 *
 */
pm_err_flag_type pm_ldo_soft_reset_status (uint8 pmic_chip,
                                           uint8 ldo_peripheral_index,
                                           boolean* status);

/**
 * Brings the selected power rail out of local soft reset.
 *
 * @param [in] pmic_chip Primary -- 0. Secondary -- 1. 
 * @param [in] ldo_peripheral_index LDO peripheral index. Starts from 0 (for the first LDO peripheral).
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC. @newpage
 *
 */
pm_err_flag_type pm_ldo_soft_reset_exit (uint8 pmic_chip,
                                          uint8 ldo_peripheral_index);

/** @} */ /* end_addtogroup pm_ldo */

#endif    /* PM_LDO_H */

