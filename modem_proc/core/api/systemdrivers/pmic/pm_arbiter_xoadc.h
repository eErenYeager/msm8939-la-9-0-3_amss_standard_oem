#ifndef PM_ARBITER_XOADC_H
#define PM_ARBITER_XOADC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_arbiter_xoadc.h PMIC-ADC ARBITER Module related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *        to support Qualcomm PMIC ARB_ADC (ADC Arbiter) module.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_arbiter_xoadc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/03/11   hs      Initial version.
========================================================================== */
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/


/** @addtogroup pm_arbiter_xoadc
@{ */
/**
 * @enum pm_adc_mode_type
 * @brief ADC mode.
 */
typedef enum
{
   PM_ADC_MODE__MODULE_OFF,  /*!< ADC module is OFF.  */
   PM_ADC_MODE__MODULE_ON    /*!< ADC module is ON.  */
}pm_adc_mode_type;

/**
 * @enum pm_adc_conv_req_cmd_type
 * @brief Commands used to set or clear the conversion request in the
 *        ADC module.
 */
typedef enum
{
   PM_ADC_CONVERSION_REQ_CMD__OFF,  /*!< Stop a conversion.  */
   PM_ADC_CONVERSION_REQ_CMD__ON    /*!< Request a conversion.  */
}pm_adc_conv_req_cmd_type;
/**
 * @enum pm_adc_cmd_type
 * @brief Commands used to enable or disable certain functionalities in
 *        the ADC module.
 */
typedef enum
{
   PM_ADC_CMD__DISABLE,  /*!< Disable the functionality.  */
   PM_ADC_CMD__ENABLE    /*!< Enable the functionality.  */
}pm_adc_cmd_type;

/**
 * @enum pm_adc_conversion_status_type
 * @brief Conversion status.
 */
typedef enum
{
    PM_ADC_CONVERSION_STATUS__COMPLETE,  /*!< Conversion is complete.  */
    PM_ADC_CONVERSION_STATUS__PENDING    /*!< Waiting for ADC to complete 
                                              a conversion for another 
                                              process.  */
} pm_adc_conversion_status_type;

/**
 * @enum pm_adc_conv_trig_type
 * @brief Commands used to set the conversion trigger condition that
 *        starts the ADC holdoff timer.
 */
typedef enum
{
   PM_ADC_CONV_TRIG__FE,   /*!< Falling edge.  */
   PM_ADC_CONV_TRIG__RE    /*!< Rising edge.   */
}pm_adc_conv_trig_type;

/**
 * @enum pm_adc_mux_type
 * @brief Pre-MUX selection for ADC.
 */
typedef enum
{
   PM_ADC_AMUX_MAIN,     /*!< ADC AMUX main.  */
   PM_ADC_PREMUX_TO_CH6, /*!< ADC pre-MUX to ch6.  */
   PM_ADC_PREMUX_TO_CH7, /*!< ADC pre-MUX to ch7.  */
   PM_ADC_RSV_DISABLED   /*!< ADC RSV is disabled. */
} pm_adc_mux_type;

/**
 * @enum pm_adc_xoadc_input_select_type
 * @brief XOADC input.
 */
typedef enum
{
   PM_ADC_XOADC_INPUT_XO_IN_XOADC_GND,      /*!< Input XO in XOADC GND. */
   PM_ADC_XOADC_INPUT_PMIC_IN_XOADC_GND,    /*!< Input PMIC in XOADC GND. */
   PM_ADC_XOADC_INPUT_PMIC_IN_BMS_CSP,      /*!< Input PMIC in BMS CSP. */
   PM_ADC_XOADC_INPUT_RESERVED,             /*!< Input is reserved. */
   PM_ADC_XOADC_INPUT_XOADC_GND_XOADC_GND,  /*!< Input XOADC GND XOADC GND. */
   PM_ADC_XOADC_INPUT_XOADC_VDD_XOADC_GND,  /*!< Input XOADC VDD XOADC GND. */
   PM_ADC_XOADC_INPUT_VREFN_VREFN,          /*!< Input VREFN VREFN. */
   PM_ADC_XOADC_INPUT_VREFP_VREFN           /*!< Input VREFP VREFN.  */
} pm_adc_xoadc_input_select_type;

/**
 * @enum pm_adc_xoadc_decimation_ratio_type
 * @brief XOADC decimation ratio.
 */
typedef enum
{
  PM_ADC_XOADC_DECIMATION_RATIO_512,    /*!< Ratio is 512. */
  PM_ADC_XOADC_DECIMATION_RATIO_1K,     /*!< Ratio is  1 K. */
  PM_ADC_XOADC_DECIMATION_RATIO_2K,     /*!< Ratio is  2 K. */
  PM_ADC_XOADC_DECIMATION_RATIO_4K,     /*!< Ratio is  4 K. @newpage */
} pm_adc_xoadc_decimation_ratio_type;

/**
 * @enum pm_adc_xoadc_conversion_rate_type
 * @brief XOADC conversion rate.
 */
typedef enum
{
  PM_ADC_XOADC_CONVERSION_RATE_TCXO_DIV_8,  /*!< Conversion is at TCXO/8. */
  PM_ADC_XOADC_CONVERSION_RATE_TCXO_DIV_4,  /*!< Conversion is at TCXO/4. */
  PM_ADC_XOADC_CONVERSION_RATE_TCXO_DIV_2,  /*!< Conversion is at TCXO/2. */
  PM_ADC_XOADC_CONVERSION_RATE_TCXO         /*!< Conversion is at TCXO frequency. */
} pm_adc_xoadc_conversion_rate_type;

/**
 * @struct pm_adc_conv_seq_status_type
 * @brief Used to get the XOADC conversion sequencer status.
 */
typedef struct
{
   boolean   timeout;           /*!< Timeout flag.  */
   boolean   fifo_not_empty;    /*!< FIFO not empty flag.   */
}pm_adc_conv_seq_status_type;

/*===========================================================================

                 ARBITER-ADC DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Enables/disables the ADC Arbiter.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_adc_mode_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] mode Enables or disablesthe ADC Arbiter.
 *                  - PM_ADC_MODE__MODULE_ON -- 
 *                    Enable the ADC Arbiter
 *                  - PM_ADC_MODE__MODULE_OFF -- 
 *                    Disable the ADC Arbiter @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_adc_set_mode(int externalResourceIndex,
                                 pm_adc_mode_type mode);

/**
 * Sets the ADC conversion request.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_adc_conv_req_cmd_type
 *
 * @param [in] externalResourceIndex External ADC ID.
 * @param [in] cmd Sets the conversion request.
 *                 - PM_ADC_CONVERSION_REQ_CMD__OFF -- 
 *                   Clear the ADC conversion request
 *                 - PM_ADC_CONVERSION_REQ_CMD__ON -- 
 *                   Set the ADC conversion request @tablebulletend 
 *
 * @detdesc
 * This function sets the ADC conversion request. After the request
 * is sent, the request is stored in the conversion request queue in
 * the PMIC hardware. The request is cleared when the ADC conversion is 
 * completed.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_conversion_request(int externalResourceIndex,
                                               pm_adc_conv_req_cmd_type cmd);

/**
 * Gets the ADC conversion status based on the status
 * of the conversion status strobe and the end-of-conversion status flag.
 *
 * @datatypes
 * #pm_adc_conversion_status_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [out] status Status of the conversion request.
 *                     - PM_ADC_CONVERSION_STATUS__COMPLETE -- 
 *                       Conversion is complete
 *                     - PM_ARB_ADC_CONVERSION_STATUS__PENDING -- 
 *                       Waiting for ADC to complete another process' conversion 
 *                       request or an interval timer to expire @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid pointer was passed in.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_get_conversion_status(int externalResourceIndex,
                                  pm_adc_conversion_status_type *status);

/**
 * Enables/disable the conversion sequencer.
 *
 * @note 1. The default setting in the PMIC hardware is device specific.
 * @note 2. This function is available only to the modem processor.
 *
 * @datatypes
 * #pm_adc_cmd_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] cmd Enables or disables the conversion sequencer.
 *                - PM_ARB_ADC_CMD__ENABLE -- 
 *                  Enable the conversion sequencer
 *                - PM_ARB_ADC_CMD__DISABLE -- 
 *                  Disable the conversion sequencer @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_conv_sequencer(int externalResourceIndex,
                                           pm_adc_cmd_type cmd);

/**
 * Sets the conversion trigger condition that starts the ADC holdoff timer.
 *
 * @note 1. The default setting in the PMIC hardware is device specific.
 * @note 2. This function is available only to the modem processor.
 * 
 *  @datatypes
 *  #pm_adc_conv_trig_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] trig Sets the conversion trigger condition.
 *                  - PM_ADC_CONV_TRIG__FE -- Failing edge
 *                  - PM_ADC_CONV_TRIG__RE -- Rising edge @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_conv_trig_condition(int externalResourceIndex,
                                                pm_adc_conv_trig_type trig);

/**
 * Sets the holdoff time, which is the delay from
 * conversion trigger transition to ADC enable.
 *
 * @note 1. The default setting in the PMIC hardware is device specific.
 * @note 2. This function is available only to the modem processor.
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] time_us Holdoff time in microseconds. \n
 *                     @note1 If the value of parameter time_us is out of the 
 *                        valid range that the PMIC hardware can take, the value 
 *                        is limited to the boundary and the PMIC hardware 
 *                        is set to that value; at the same time,
 *                        PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to 
 *                        indicate that there was an out-of-bounds time value 
 *                        parameter input.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_holdoff_time(int externalResourceIndex,
                                         uint32 time_us);

/**
 * Sets the timeout time. The timeout time is the delay from the SBI conversion 
 * request to triggering the conversion sequencer holdoff timer.
 *
 * @note 1. The default setting in the PMIC hardware is device specific.
 * @note 2. This function is available only to the modem processor.
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] time_ms Timeout time in milliseconds. \n
 *                     @note1 If the value of parameter time_ms is out of the 
 *                        valid range that the PMIC hardware can take, the value 
 *                        is limited to the boundary and the PMIC hardware 
 *                        is set to that value; at the same time,
 *                        PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to 
 *                        indicate that there was an out-of-bounds time value 
 *                        parameter input.
 *
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_timeout_time(int externalResourceIndex,
                                         uint32 time_ms);

/**
 * Gets the status of conversion sequencer, including the SBI conversion 
 * request timeout flag and status of FIFO.
 *
 * @note This function is available only to the modem processor.
 *
 * @datatypes
 * #pm_adc_conv_seq_status_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [out] status Status of the conversion request.
 *                     - PM_ARB_ADC_CONVERSION_STATUS__INVALID -- 
 *                       Invalid status
 *                     - PM_ARB_ADC_CONVERSION_STATUS__COMPLETE -- 
 *                       Conversion is complete
 *                     - PM_ARB_ADC_CONVERSION_STATUS__OCCURRING -- 
 *                       Conversion is in progress
 *                     - PM_ARB_ADC_CONVERSION_STATUS__WAITING -- 
 *                       Waiting for ADC to complete another process' conversion 
 *                       request or interval timer to expire. @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid pointer was passed in.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_get_conv_sequencer_status(int externalResourceIndex,
                                        pm_adc_conv_seq_status_type *status);

/**
 * Sets up the AMUX, such as channel and pre-MUX selection.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 *
 * @datatypes
 * #pm_adc_mux_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] channel Analog channel.
 * @param [in] mux MUX.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE-- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE -- 
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_amux(int externalResourceIndex,
                                 uint8 channel,
                                 pm_adc_mux_type mux);

/**
 * Gets the prescaler value for the specific channel.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External Battery Temperature Management 
                                     (BTM) ID.
 * @param [in] channel Analog channel.
 * @param [out] numerator Numerator of the prescalar.
 * @param [out] denominator Denominator of the prescalar.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE-- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid input pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_get_resource_prescalar(int externalResourceIndex,
                                               uint8 channel,
                                               uint8 *numerator,
                                               uint8 *denominator);

/**
 * Sets the XOADC input that is used for ARB_ADC.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_adc_xoadc_input_select_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] input Selects the ADC input.
 *                   - PM_ARB_ADC_ADC_INPUT_PMIC -- PMIC_IN
 *                   - PM_ARB_ADC_ADC_INPUT_XO -- XO_IN
 *                   - PM_ARB_ADC_ADC_INPUT_VREFP -- VREFP @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE-- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_xoadc_input(int externalResourceIndex,
                                        pm_adc_xoadc_input_select_type input);

/**
 * Sets the XOADC decimation ratio that is used for ARB_ADC.
 *
 * @note The default setting in the PMIC hardware is PM_ARB_ADC_CMD__DISABLE.
 *
 * @datatypes
 * #pm_adc_xoadc_decimation_ratio_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] ratio Selects the ADC decimation ratio.
 *                   - PM_ARB_ADC_ADC_DECIMATION_RATIO_512 -- 512
 *                   - PM_ARB_ADC_ADC_DECIMATION_RATIO_1K -- 1 K
 *                   - PM_ARB_ADC_ADC_DECIMATION_RATIO_2K -- 2 K
 *                   - PM_ARB_ADC_ADC_DECIMATION_RATIO_4K -- 4 K @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE-- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_xoadc_decimation_ratio(int externalResourceIndex,
                                    pm_adc_xoadc_decimation_ratio_type ratio);

/**
 * Sets the XOADC clock rate that is used for ARB_ADC.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 *
 * @datatypes
 * #pm_adc_xoadc_conversion_rate_type
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [in] rate Selects the ADC clock rate.
 *                  - PM_ARB_ADC_ADC_CONVERSION_RATE_TCXO_DIV_8 -- TCXO/8
 *                  - PM_ARB_ADC_ADC_CONVERSION_RATE_TCXO_DIV_4 -- TCXO/4
 *                  - PM_ARB_ADC_ADC_CONVERSION_RATE_TCXO_DIV_2 -- TCXO/2
 *                  - PM_ARB_ADC_ADC_CONVERSION_RATE_TCXO -- TCXO/1 @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE-- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_set_xoadc_conversion_rate(int externalResourceIndex,
                                    pm_adc_xoadc_conversion_rate_type rate);

/**
 * Gets the XOADC conversion data.
 *
 * @param [in] externalResourceIndex External ARB_ADC ID.
 * @param [out] data Stores the ADC conversion result.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the ARB_ADC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED -- 
 * The internal resource ID for the ARB_ADC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid input pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 *
 * @newpage 
 */
pm_err_flag_type pm_adc_get_xoadc_data(int externalResourceIndex,
                                       uint32* data);
/** @} */ /* end_addtogroup pm_arbiter_xoadc */

#endif /* PM_ARBITER_XOADC_H */

