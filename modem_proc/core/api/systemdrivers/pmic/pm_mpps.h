#ifndef PM_MPPS_H
#define PM_MPPS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_mpps.h PMIC MPPs related declaration.
 *
 * @brief This header file contains enums and API definitions for MPPS driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_mpps.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/04/14   rk      Make MPP Enable API public (CR - 689496)
04/02/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
10/21/13   rk      Support for Vunicore 8926 core compilation
10/03/13   rk      Reducing the MPP number to 4
07/05/12   hs      Updated the interface.
03/02/12   hs      Removed deprecated APIs.
05/03/11   dy      Rename digital logic level enumerations
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/26/10   wra     Changed documentation from @example to Examples so Deoxygen
                    can parse the file
07/11/10   jtn     Aded APIs to get MPP status, corrected pm_mpp_dlogic_inout_pup_type
                   to reflect PMIC HW capabilities
07/02/10   wra     Changed pm_mpp_which_type to int
03/15/10   fpe     CMI Merge - Made the interface common between 7x30 and SCMM
03/05/10   vk      Added API pm_mpp_config_dtest_output()
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/19/09   vk      Removed init API
07/06/09   jtn     Added enums for PM8028 digital logic levels
01/26/09   Vk      Introduced following APIs
                   pm_config_secure_mpp_config_digital_input()
                   pm_secure_mpp_config_digital_input()
12/17/08   APU     Introduced the APIs:
                   1. pm_secure_mpp_config_i_sink ()
                   2. pm_make_secure__mpp_config_i_sink ()
08/01/08   jtn     Move all proc comm functions to pm_pcil.c
06/27/08   jtn     Merge changes from QSC1100 branch, consolidated
                   definition for max. number of MPPs
06/24/08   vk      Provided API that maintains the list of MPPs that have shunt
                   capacitors to ground. In the MPP configuration API for
                   anaolog output this list is used.
03/01/08   jtn     Corrected number of MPPs for Han
02/27/08   jtn     Added support for Kip PMIC
01/07/08   jtn     Merged from QSC6270 branch
(begin QSC6270 changes)
10/11/07   jnoblet Added MPPs for Han
(end QSC6270 changes)
11/13/07   jtn     Added pm_config_secure_mpp_config_digital_output and
                   pm_secure_mpp_config_digital_output to support RPC access
                   to MPP digital outputs
06/18/07   cng     Added an input parameter to pm_mpp_config_analog_output
                   to allow configurable analog output voltage
05/18/07   hs      Restored the third parameter in pm_mpp_config_digital_input()
09/08/06   cng     Removed API pm_mpp_config_d_test and enum pm_mpp_dtest_in_dbus_type
06/22/06   hs      Removed the third parameter in pm_mpp_config_digital_input()
06/08/06   hs      Added pm_mpp_config_atest API support
11/09/05   cng     Added PM6620 support
10/24/05   cng     Added Panoramix support; Removed MPP config type enum;
                   Corrected number of MPPs for PM6650
07/25/05   cng     PM7500 support
07/01/05   cng     Added MPP config type enum
03/15/05   cng     For the analog input function, added the functionality to
                   route the MPP input to one of the 3 analog buses
01/28/04   rmd     Added initial support for multiple PMIC models/tiers.
11/03/03   rmd     Updated the settings for the BIDIRECTIONAL the pull up
                   resistor.
09/08/03   rmd     enum "pm_mpp_aout_switch_type" was in the wrong order.
08/11/03   rmd     Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"
#include "pm_lib_cmd.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/** @addtogroup pm_mpps
@{ */
/**
 * @brief Which MPP.
 */
typedef int pm_mpp_which_type;

/**
 * @anonenum{pm_mpp_which_type}@xreflabel{pm_mpp_which_type} 
 * @brief MPP to be configured.
 */
enum
{
  PM_MPP_1,  /**< MPP 1.  */
  PM_MPP_2,  /**< MPP 2.  */
  PM_MPP_3,  /**< MPP 3.  */
  PM_MPP_4,  /**< MPP 4.  */
  PM_MPP_5,
  PM_MPP_6,
  PM_MPP_7,
  PM_MPP_8,
  PM_MPP_9,
  PM_MPP_10,
  PM_MPP_11,
  PM_MPP_12,
  PM_MPP_13,
  PM_MPP_14,
  PM_MPP_15,
  PM_MPP_16,
  PM_MPP_INVALID  /**< Invalid.  */
};


/**
 * @enum pm_mpp_dlogic_lvl_type
 * @brief Digital logic levels. Configures the output logic level.
 */
typedef enum
{
  PM_MPP__DLOGIC__LVL_VIO_0 = 0,  /**< Voltage I/O level 0.  */
  PM_MPP__DLOGIC__LVL_VIO_1,      /**< Voltage I/O level 1.  */
  PM_MPP__DLOGIC__LVL_VIO_2,      /**< Voltage I/O level 2.  */
  PM_MPP__DLOGIC__LVL_VIO_3,      /**< Voltage I/O level 3.  */
  PM_MPP__DLOGIC__LVL_VIO_4,      /**< Voltage I/O level 4.  */
  PM_MPP__DLOGIC__LVL_VIO_5,      /**< Voltage I/O level 5.  */
  PM_MPP__DLOGIC__LVL_VIO_6,      /**< Voltage I/O level 6.  */
  PM_MPP__DLOGIC__LVL_VIO_7,      /**< Voltage I/O level 7.  */
  PM_MPP__DLOGIC__LVL_INVALID = 8,  /*!< Invalid. */
} pm_mpp_dlogic_lvl_type;

/**
 * @enum pm_mpp_dlogic_in_dbus_type
 * @brief Data bus configuration for the digital input.
 */
typedef enum
{
  PM_MPP__DLOGIC_IN__DBUS1,   /**< Data bus 1.  */
  PM_MPP__DLOGIC_IN__DBUS2,   /**< Data bus 2.  */
  PM_MPP__DLOGIC_IN__DBUS3,   /**< Data bus 3.  */
  PM_MPP__DLOGIC_IN__DBUS4,   /**< Data bus 4.  */
  PM_MPP__DLOGIC_IN__DBUS_INVALID  /**< Invalid.  @newpage */
}pm_mpp_dlogic_in_dbus_type;

/**
 * @enum pm_mpp_dlogic_out_ctrl_type
 * @brief Configures the output logic.
 */
 /* TODO - hshen: this enum seems obsolete for BADGER
 */
typedef enum
{
  PM_MPP__DLOGIC_OUT__CTRL_LOW,    /*!< MPP output: logic low.  */
  PM_MPP__DLOGIC_OUT__CTRL_HIGH,   /*!< MPP output: logic high. */
  PM_MPP__DLOGIC_OUT__CTRL_MPP,    /*!< MPP output: corresponding
                                                    MPP input.   */
  PM_MPP__DLOGIC_OUT__CTRL_NOT_MPP,/*!< MPP output: corresponding
                                                    inverted MPP input. */
  PM_MPP__DLOGIC_OUT__CTRL_INVALID /*!< Invalid. */
}pm_mpp_dlogic_out_ctrl_type;

/**
 * @enum pm_mpp_dlogic_inout_pup_type
 * @brief Configures the pull up resistor. Mode is bidirectional.
 */
typedef enum
{
  PM_MPP__DLOGIC_INOUT__PUP_1K,     /*!< Pull up 1 kOhm.*/
  PM_MPP__DLOGIC_INOUT__PUP_10K,    /*!< Pull up 10 kOhm.*/
  PM_MPP__DLOGIC_INOUT__PUP_30K,    /*!< Pull up 30 kOhm.*/
  PM_MPP__DLOGIC_INOUT__PUP_OPEN,   /*!< Pull up Open. */
  PM_MPP__DLOGIC_INOUT__PUP_INVALID /*!< Invalid. */
}pm_mpp_dlogic_inout_pup_type;

/**
 * @enum pm_mpp_dlogic_out_dbus_type
 * @brief Data bus configuration for the digital test output.
 */
typedef enum
{
  PM_MPP__DLOGIC_OUT__DBUS1, /**< Data bus 1.*/
  PM_MPP__DLOGIC_OUT__DBUS2, /**< Data bus 2.*/
  PM_MPP__DLOGIC_OUT__DBUS3, /**< Data bus 3.*/
  PM_MPP__DLOGIC_OUT__DBUS4, /**< Data bus 4.*/
  PM_MPP__DLOGIC_OUT__DBUS_INVALID /**< Invalid.*/
}pm_mpp_dlogic_out_dbus_type;

/**
 * @enum pm_mpp_ain_ch_type
 * @brief Analog input channel.
 */
typedef enum
{
  PM_MPP__AIN__CH_AMUX5,  /**< AMUX5.*/
  PM_MPP__AIN__CH_AMUX6,  /**< AMUX6.*/
  PM_MPP__AIN__CH_AMUX7,  /**< AMUX7.*/
  PM_MPP__AIN__CH_AMUX8,  /**< AMUX8.*/
  PM_MPP__AIN__CH_ABUS1,  /**< ABUS1.*/
  PM_MPP__AIN__CH_ABUS2,  /**< ABUS2.*/
  PM_MPP__AIN__CH_ABUS3,  /**< ABUS3.*/
  PM_MPP__AIN__CH_ABUS4,  /**< ABUS4.*/
  PM_MPP__AIN__CH_INVALID  /**< Invalid.*/
} pm_mpp_ain_ch_type;

/**
 * @enum pm_mpp_aout_level_type
 * @brief Analog output voltage reference level.
 */
typedef enum
{
  PM_MPP__AOUT__LEVEL_VREF_1p25_Volts,    /**< 1.25 V.*/
  PM_MPP__AOUT__LEVEL_VREF_0p625_Volts,   /**< 0.625 V.*/
  PM_MPP__AOUT__LEVEL_VREF_0p3125_Volts,  /**< 0.3125 V.*/
  PM_MPP__AOUT__LEVEL_INVALID  /**< Invalid.*/
}pm_mpp_aout_level_type;

/**
 * @enum pm_mpp_aout_switch_type
 * @brief Analog output switch configuration.
 */
typedef enum
{
  PM_MPP__AOUT__SWITCH_OFF,   /**< Switch is OFF. */
  PM_MPP__AOUT__SWITCH_ON,    /**< Switch is ON. */
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_HIGH,  /**< Switch is on if MPP is high. */
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_LOW,   /**< Switch is on if MPP is low. */
  PM_MPP__AOUT__SWITCH_INVALID   /**< Invalid. */
}pm_mpp_aout_switch_type;

/**
 * @enum pm_mpp_i_sink_level_type
 * @brief Current sink level.
 */
typedef enum
{
  PM_MPP__I_SINK__LEVEL_5mA,   /**< 5 mA. */
  PM_MPP__I_SINK__LEVEL_10mA,  /**< 10 mA. */
  PM_MPP__I_SINK__LEVEL_15mA,  /**< 15 mA. */
  PM_MPP__I_SINK__LEVEL_20mA,  /**< 20 mA. */
  PM_MPP__I_SINK__LEVEL_25mA,  /**< 25 mA. */
  PM_MPP__I_SINK__LEVEL_30mA,  /**< 30 mA. */
  PM_MPP__I_SINK__LEVEL_35mA,  /**< 35 mA. */
  PM_MPP__I_SINK__LEVEL_40mA,  /**< 40 mA. */
  PM_MPP__I_SINK__LEVEL_INVALID  /**< Invalid. */
}pm_mpp_i_sink_level_type;

/**
 * @enum pm_mpp_i_sink_switch_type
 * @brief Current sink switch configuration.
 */
typedef enum
{
  PM_MPP__I_SINK__SWITCH_DIS,              /**< Current sink switch is disabled. */
  PM_MPP__I_SINK__SWITCH_ENA,              /**< Current sink switch is enabled. */
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_HIGH,  /**< Current sink switch enabled if the MPP is high. */
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_LOW,   /**< Current sink switch enabled if the MPP is low. */
  PM_MPP__I_SINK__SWITCH_INVALID           /**< Invalid. */ 
}pm_mpp_i_sink_switch_type;

/**
 * @enum pm_mpp_ch_atest_type
 * @brief A-test configuration.
 */
typedef enum
{
  PM_MPP__CH_ATEST1, /**< A-test 1. */
  PM_MPP__CH_ATEST2, /**< A-test 2. */
  PM_MPP__CH_ATEST3, /**< A-test 3. */
  PM_MPP__CH_ATEST4, /**< A-test 4. */
  PM_MPP__CH_ATEST_INVALID /**< Invalid. */
}pm_mpp_ch_atest_type;

/**
 * @struct pm_mpp_digital_input_status_type
 * @brief Structure used for returning digital input configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;            /*!< Digital logic level. */
    pm_mpp_dlogic_in_dbus_type mpp_dlogic_in_dbus;    /*!< Digital logic level in the data bus. */
} pm_mpp_digital_input_status_type;

/**
 * @struct pm_mpp_digital_output_status_type
 * @brief Structure used for returning digital output configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;              /*!< Digital logic level. */
    pm_mpp_dlogic_out_ctrl_type mpp_dlogic_out_ctrl;    /*!< Digital logic output control. */
} pm_mpp_digital_output_status_type;

/**
 * @struct pm_mpp_bidirectional_status_type
 * @brief Structure used for returning bidirectional configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;                /*!< Digital logic level. */
    pm_mpp_dlogic_inout_pup_type mpp_dlogic_inout_pup;    /*!< Digital logic input/output pull up. @newpagetable */
}  pm_mpp_bidirectional_status_type;

/**
 * @struct pm_mpp_analog_input_status_type
 * @brief Structure used for returning analog input configuration.
 */
typedef struct
{
    pm_mpp_ain_ch_type mpp_ain_ch;    /*!< Analog input channel. */
}  pm_mpp_analog_input_status_type;

/**
 * @struct pm_mpp_analog_output_status_type
 * @brief Structure used for returning analog output configuration.
 */
typedef struct
{
    pm_mpp_aout_level_type mpp_aout_level;      /*!< Analog output level. */
    pm_mpp_aout_switch_type mpp_aout_switch;    /*!< Analog output switch. */
}  pm_mpp_analog_output_status_type;

/**
 * @struct pm_mpp_current_sink_status_type
 * @brief Structure used for returning current sink configuration.
 */
typedef struct
{
    pm_mpp_i_sink_level_type  mpp_i_sink_level;     /*!< Current sink level. */
    pm_mpp_i_sink_switch_type mpp_i_sink_switch;    /*!< Current sink switch. */
}  pm_mpp_current_sink_status_type;

/**
 * @struct pm_mpp_dtest_output_status_type
 * @brief Structure used for returning D-test output configuration.
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;               /*!< Digital logic level. */
    pm_mpp_dlogic_out_dbus_type  mpp_dlogic_out_dbus;    /*!< Digital logic out data bus. */
} pm_mpp_dtest_output_status_type;

/**
 * @enum pm_drv_mpp_config_type
 * @brief Available MPP mode.
 */
typedef enum
{
    PM_MPP_DIG_IN,           /*!< 0: Digital input. */
    PM_MPP_DIG_OUT,          /*!< 1: Digital output. */
    PM_MPP_DIG_IN_DIG_OUT,   /*!< 2: Digital input and digital output. */
    PM_MPP_BI_DIR,           /*!< 3: Bidirectional logic. */
    PM_MPP_ANALOG_IN,        /*!< 4: Analog input. */ 
    PM_MPP_ANALOG_OUT,       /*!< 5: Analog output. */
    PM_MPP_I_SINK,           /*!< 6: Current sink. */
    PM_MPP_TYPE_INVALID      /*!< Invalid. */ 
} pm_drv_mpp_config_type;

/**
 * @struct pm_mpp_status_type
 * @brief Structure type used for returning MPP status.
 *		  The structure that has valid data depends on
 *		  the MPP mode (input, output, etc.) returned in mpp_config.
 */
typedef struct
{
    pm_drv_mpp_config_type mpp_config;    /*!< Returns the MPP's mode (input, output, etc.). */
    union
    {
        pm_mpp_digital_input_status_type mpp_digital_input_status;   /**< If ( _DISC_ == 0 ) s_pm_mpp_status_type.mpp_config_settings.mpp_digital_input_status. */
        pm_mpp_digital_output_status_type mpp_digital_output_status; /**< If ( _DISC_ == 1 ) s_pm_mpp_status_type.mpp_config_settings.mpp_digital_output_status. */
        pm_mpp_bidirectional_status_type mpp_bidirectional_status;   /**< If ( _DISC_ == 2 ) s_pm_mpp_status_type.mpp_config_settings.mpp_bidirectional_status.*/
        pm_mpp_analog_input_status_type mpp_analog_input_status;     /**< If ( _DISC_ == 3 ) s_pm_mpp_status_type.mpp_config_settings.mpp_analog_input_status. */
        pm_mpp_analog_output_status_type mpp_analog_output_status;   /**< If ( _DISC_ == 4 ) s_pm_mpp_status_type.mpp_config_settings.mpp_analog_output_status. */
        pm_mpp_current_sink_status_type mpp_current_sink_status;     /**< If ( _DISC_ == 5 ) s_pm_mpp_status_type.mpp_config_settings.mpp_current_sink_status. */
        pm_mpp_dtest_output_status_type mpp_dtest_output_status;     /**< If ( _DISC_ == 6 ) s_pm_mpp_status_type.mpp_config_settings.mpp_dtest_output_status. @newpagetable */
    } mpp_config_settings;                                           /**< Union of structures used to return MPP status. */
} pm_mpp_status_type;


/*===========================================================================

                    MPP DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Returns the status of one of the PMIC MPPs.
 * 
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_status_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP. 
 * @param [out] mpp_status Pointer to the MPP status structure.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
 /* Note: Do not use pm_mpp_status_get(), which is deprecated. Use pm_dev_mpp_status_get() instead. 
 */
pm_err_flag_type pm_dev_mpp_status_get(unsigned pmic_device_index,
                                       pm_mpp_which_type mpp,
                                       pm_mpp_status_type *mpp_status);
pm_err_flag_type pm_mpp_status_get(pm_mpp_which_type mpp,
                                   pm_mpp_status_type *mpp_status);

/**
 * Configures the selected MPP to be a digital input pin.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 * 
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_dlogic_lvl_type \n
 * #pm_mpp_dlogic_in_dbus_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] level Logic level reference to use with this MPP.
 * @param [in] dbus Data line the MPP is to drive. Only one MPP can be assigned
 *                  per data bus.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE --
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE --
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 * 
 * @newpage
 */  
 /* Note: Do not use pm_mpp_config_digital_input(), which is deprecated. 
            Use pm_dev_mpp_config_digital_input() instead. */
extern pm_err_flag_type
pm_dev_mpp_config_digital_input( unsigned pmic_device_index,
                                 pm_mpp_which_type mpp,
                                 pm_mpp_dlogic_lvl_type level,
                                 pm_mpp_dlogic_in_dbus_type  dbus);
extern pm_err_flag_type
pm_mpp_config_digital_input( pm_mpp_which_type mpp,
                             pm_mpp_dlogic_lvl_type level,
                             pm_mpp_dlogic_in_dbus_type dbus);

/**
 * Configures the selected MPP to be a digital output pin.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC. 
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_dlogic_lvl_type \n
 * #pm_mpp_dlogic_out_ctrl_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] level Logic level reference to use with this MPP.
 * @param [in] output_ctrl Output setting of the selected MPP.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE --
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE --
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 * 
 * @newpage
 */
/* Note: Do not use pm_mpp_config_digital_output(), which is deprecated. 
            Use pm_dev_mpp_config_digital_output() instead.  */
 extern pm_err_flag_type
 pm_dev_mpp_config_digital_output( unsigned pmic_device_index,
                                   pm_mpp_which_type mpp,
                                   pm_mpp_dlogic_lvl_type level,
                                   pm_mpp_dlogic_out_ctrl_type output_ctrl);
extern pm_err_flag_type
pm_mpp_config_digital_output( pm_mpp_which_type mpp,
                              pm_mpp_dlogic_lvl_type level,
                              pm_mpp_dlogic_out_ctrl_type output_ctrl);

/**
 * Configures the selected MPP to be a digital input/output pin.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC. 
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_dlogic_lvl_type \n
 * #pm_mpp_dlogic_inout_pup_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] level Logic level reference to use with this MPP.
 * @param [in] pull_up Pull-up resistor setting of the selected MPP.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE --
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage
 */
/* Note: Do not use pm_mpp_config_digital_inout(), which is deprecated. Use pm_dev_mpp_config_digital_inout() instead. 
 */
 extern pm_err_flag_type
 pm_dev_mpp_config_digital_inout(unsigned pmic_device_index,
                                 pm_mpp_which_type mpp,
                                 pm_mpp_dlogic_lvl_type level,
                                 pm_mpp_dlogic_inout_pup_type pull_up);
extern pm_err_flag_type
pm_mpp_config_digital_inout(pm_mpp_which_type mpp,
                            pm_mpp_dlogic_lvl_type level,
                            pm_mpp_dlogic_inout_pup_type pull_up);

/**
 * Configures the selected MPP to be a digital test output pin.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 * 
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_dlogic_lvl_type \n
 * #pm_mpp_dlogic_out_dbus_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] level Logic level reference to use with this MPP.
 * @param [in] dbus Data line the MPP is to drive.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par 
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE --
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR--
 * The SBI driver failed to communicate with the PMIC.
 * 
 * @newpage
 */  
/* Note: Do not use pm_mpp_config_dtest_output(), which is deprecated. Use pm_dev_mpp_config_dtest_output() instead. */
pm_err_flag_type
pm_dev_mpp_config_dtest_output(unsigned pmic_device_index,
                               pm_mpp_which_type mpp,
                               pm_mpp_dlogic_lvl_type level,
                               pm_mpp_dlogic_out_dbus_type  dbus);
pm_err_flag_type
pm_mpp_config_dtest_output(pm_mpp_which_type mpp,
                           pm_mpp_dlogic_lvl_type level,
                           pm_mpp_dlogic_out_dbus_type  dbus);

/**
 * Configures the selected MPP as an analog input.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC. 
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_ain_ch_type 
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] ch_select Analog MUX or analog bus to which the selected MPP is to be
 *                       routed.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE -- 
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR -- 
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage 
 */
/* Note: Do not use pm_mpp_config_analog_input(), which is deprecated. Use pm_dev_mpp_config_analog_input() instead. */
extern pm_err_flag_type
pm_dev_mpp_config_analog_input(unsigned pmic_device_index,
                               pm_mpp_which_type mpp,
                               pm_mpp_ain_ch_type ch_select);
extern pm_err_flag_type
pm_mpp_config_analog_input(pm_mpp_which_type mpp,
                           pm_mpp_ain_ch_type ch_select);

/**
 * Configures the selected MPP as an analog output.
 * 
 * @note1hang Interrupts are disabled while communicating with the PMIC. 
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_aout_level_type \n
 * #pm_mpp_aout_switch_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] level Specifies the analog output reference voltage level.
 * @param [in] OnOff Enable/disable the MPP output.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par 
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE --
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage
 */
/* Note: Do not use pm_mpp_config_analog_output(), which is deprecated. 
            Use pm_dev_mpp_config_analog_output() instead.  */
extern pm_err_flag_type
pm_dev_mpp_config_analog_output(unsigned pmic_device_index,
                                pm_mpp_which_type        mpp,
                                pm_mpp_aout_level_type   level,
                                pm_mpp_aout_switch_type  OnOff);
extern pm_err_flag_type
pm_mpp_config_analog_output(pm_mpp_which_type        mpp,
                            pm_mpp_aout_level_type   level,
                            pm_mpp_aout_switch_type  OnOff);

/**
 * Configures the selected MPP as a current sink.
 * 
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_i_sink_level_type \n
 * #pm_mpp_i_sink_switch_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] mpp Which MPP.
 * @param [in] level Amount of current to allow the MPP to sink.
 * @param [in] OnOff Enable/disable the current sink.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par 
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par 
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage
 */
/* Note: Do not use pm_mpp_config_i_sink(), which is deprecated. Use pm_dev_mpp_config_i_sink() instead. 
 */
extern pm_err_flag_type
pm_dev_mpp_config_i_sink(unsigned pmic_device_index,
                         pm_mpp_which_type         mpp,
                         pm_mpp_i_sink_level_type  level,
                         pm_mpp_i_sink_switch_type OnOff);
extern pm_err_flag_type
pm_mpp_config_i_sink(pm_mpp_which_type mpp,
                     pm_mpp_i_sink_level_type  level,
                     pm_mpp_i_sink_switch_type OnOff);

/**
 * Configures the selected MPP as A-test.
 * 
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type} \n
 * #pm_mpp_ch_atest_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [in] atest_select A-test to which the selected MPP is to be routed.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par 
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par 
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 * 
 * @newpage
 */
/* Note: Do not use pm_mpp_config_atest(), which is deprecated. Use pm_dev_mpp_config_atest() instead. */
extern pm_err_flag_type
pm_dev_mpp_config_atest(unsigned pmic_device_index,
                        pm_mpp_which_type mpp,
                        pm_mpp_ch_atest_type atest_select);
extern pm_err_flag_type
pm_mpp_config_atest(pm_mpp_which_type mpp,
                    pm_mpp_ch_atest_type atest_select);

/**
 * Creates an array of MPPs that have shunt capacitors to ground.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type}
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] mpp Which MPP.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE -- 
 * Input parameter one is out of range.
 * @par 
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter twp is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE -- 
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage 
 */
/* Note: Do not use pm_mpp_set_list_mpp_with_shunt_cap(), which is deprecated. 
            Use pm_dev_mpp_set_list_mpp_with_shunt_cap() instead.  */
pm_err_flag_type
pm_dev_mpp_set_list_mpp_with_shunt_cap(unsigned pmic_device_index,
                                       pm_mpp_which_type mpp);
pm_err_flag_type pm_mpp_set_list_mpp_with_shunt_cap(pm_mpp_which_type mpp);

/**
 * Returns the 32-bit status as to whether the corresponding MPP 
 * has a shunt capacitor to ground.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * @xnameref{pm_mpp_which_type}
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] mpp Which MPP.
 * @param [out] shuntList List of the shunt capacitors.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__DBUS_IS_BUSY_MODE --
 * The data bus is busy.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * The SBI driver failed to communicate with the PMIC.
 *
 * @newpage
 */ 
/* Note: Do not use pm_get_mpp_with_shunt_cap_list_status_for_device(),
 *       which is deprecated. Use pm_dev_get_mpp_with_shunt_cap_list_status_for_device() instead. */
pm_err_flag_type
pm_dev_get_mpp_with_shunt_cap_list_status_for_device(unsigned pmic_device_index,
                                                     pm_mpp_which_type mpp,
                                                     uint32* shuntList);
pm_err_flag_type
pm_get_mpp_with_shunt_cap_list_status_for_device(pm_mpp_which_type mpp,
                                                 uint32* shuntList);

/*===========================================================================
FUNCTION pm_mpp_enable                           EXTERNAL FUNCTION
===========================================================================*/

/**
    Enable/disable MPP.

@param[in] pmic_chip Select the device index. Device index starts with zero.
@param[in] perph_index MPP to configure. See #pm_mpp_which_type.
@param[in] enable TRUE: turn on MPP, FALSE: turn off MPP.
     
@return 
  SUCCESS or Error -- See #pm_err_flag_type.
        
@dependencies
  The following functions must have been called: \n
  pm_init()

@sideeffects
  Interrupts are disabled while communicating with the PMIC.
*/
extern pm_err_flag_type pm_mpp_enable(uint8 pmic_chip, pm_mpp_which_type  perph_index, boolean enable);


/** @} */ /* end_addtogroup pm_mpps */

#endif     /* PM_MPPS_H */
