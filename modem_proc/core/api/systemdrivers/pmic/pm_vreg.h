#ifndef PM_VREG_H
#define PM_VREG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_vreg.h PMIC VOLTAGE Regulator related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC voltage
 *        regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_vreg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
04/11/12   hs      Removed unsupported type defs.
                   Updated pm_pwr_buck_switching_freq_type.
04/04/12   hs      Removed the buck switch size alg.
03/30/12   hs      Removed the dirver size alg.
04/10/11   wra     Removed the API to set the driver size.
08/24/11   hw/dy   Replace deprecated APIs
04/17/11   wra     Added A2 output buffer to pm_vreg_smps_mode_type
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
02/07/11   hw      Added enum pm_vreg_smps_inductor_ilim_type
01/24/11   hw      Adding pm_vreg_mode_status() API support
07/26/10   wra     Changed documentation from @example to Examples so Deoxygen 
                   can parse the file
07/21/10   vk      Added pm_vreg_smps_set_stepper_control_delay()
                   pm_vreg_smps_is_stepping_done() API
07/09/10   jtn     Add APIs to return VREG status
07/02/10   wra     Changed pm_vreg_id_type to int
02/18/10   jtn     Added pm_lp_force_lpm_control() API
12/02/09   jtn     CMI cleanup
08/04/09   jtn     Fix Lint critical and high warnings.
07/21/09   jtn     Added required APIs for SCMM. Changed name for USB 3.3V VREG
07/02/09   jtn     Add PM_VREG_MSMC1_ID to list of VREGs
06/30/09   jtn     Updated file documentation
05/11/09   jtn     Removed some legacy VREG IDs
03/19/09   jtn     New file, replacement for pm66XXvregs.h
=============================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_lib_cmd.h"
#include "pm_external_resources.h"
#include "comdef.h"

#include "pm_smps.h"

/**
 * @brief If VREG is deprecated
 */ 
#ifdef __RVCT__
#define PM_VREG_DEPRECATED __attribute__((__deprecated__))
#else
#define PM_VREG_DEPRECATED 
#endif


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_vreg 
@{ */
/**
 * VREG identifier used to call the VREG functions.
 */
typedef int pm_vreg_id_type;

/**
 * VREG level in millivolts.
 */
typedef unsigned pm_vreg_level_type_microvolts ;

/**
 * VREG mask type.
 */ 
typedef uint64 pm_vreg_mask_type;

/**
 * VREG masked type.
 */
typedef uint64 pm_vreg_masked_type;

/**
 * @enum pm_pwr_sw_mode_type
 * @brief Regulator power mode.
 */
typedef enum 
{
    PM_PWR_SW_MODE_PFM = 0,    /**< Low power mode. */	
    PM_PWR_SW_MODE_BYPASS = 1, /**< Bypass mode bit 5. */
    PM_PWR_SW_MODE_AUTO = 2,   /**< Auto mode bit 6. */
    PM_PWR_SW_MODE_NPM = 4,    /**< Normal mode bit 7. */
    PM_PWR_SW_MODE_INVALID     /**< Invalid mode. */
}pm_pwr_sw_mode_type;

/**
 * Buck VREG Sawtooth current compensation.
 */
#define PM_VREG_BUCK_MAX_SAWTOOTH_I_COMP  0x07
/**
 * Buck VREG GM compensation.
 */
#define PM_VREG_BUCK_MAX_GM_COMP          0x0f
/**
 * Buck VREG Resistor compensation.
 */
#define PM_VREG_BUCK_MAX_RES_COMP         0x0f

/**
 * @brief Buck VREGs compensation setting.
 */
typedef struct
{
   uint8 sawtooth_set; /**< Set Sawtooth. */
   uint8 gm_set;       /**< Set GM. */
   uint8 resistor_set; /**< Set Resistor. */
} pm_vreg_buck_cfg_cp_type;

/**
 * Used for selecting the SMPS stepper delay.
 */
typedef float pm_vreg_smps_stepper_delay_type;

/**
 * Used for selecting the SMPS step size.
 */
typedef float pm_vreg_smps_step_size_type;


/*===========================================================================

                 VREG CONTROL FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Returns the mode status (LPM, NPM, auto, bypass) of the selected power rail. 
 *
 * @note1hang A regulator's mode changes dynamically.
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1..
 * @param [in] resourceIndex Voltage regulator index.
 * @param [in] voltLevel Voltage level to set for the regulator.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
*/
/* Note: Do not use pm_vreg_set_level(), which is deprecated. 
 * Use pm_dev_pwr_volt_level() instead.
*/
pm_err_flag_type pm_dev_pwr_volt_level(unsigned pmic_device_index,
									   unsigned resourceIndex,
									   pm_vreg_level_type_microvolts voltLevel);
pm_err_flag_type  pm_vreg_set_level(pm_vreg_id_type    vreg,
									pm_vreg_level_type_microvolts voltage);
									
/**
 * Gets the regulator voltage status.
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] resourceIndex Voltage regulator index.
 * @param [out] voltLevel Variable with the regulator voltage status to return 
 *                        to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_get_level(), which is deprecated. 
 * Use pm_dev_pwr_volt_level_status() instead.
*/
pm_err_flag_type pm_dev_pwr_volt_level_status(unsigned pmic_device_index,
											  unsigned resourceIndex,
											  pm_vreg_level_type_microvolts *voltLevel);
pm_err_flag_type  pm_vreg_get_level(pm_vreg_id_type    vreg,
									pm_vreg_level_type_microvolts *vreg_level);

/**
 * Selects RC or TCXO as the SMPS clock source.
 *
 * @datatypes
 * #pm_clk_src_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] externalResourceIndex Buck voltage regulator index.
 * @param [in] clkSource Reference clock for the SMPS clock.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_pwr_buck_clk_source(), which is deprecated. 
 * Use pm_dev_pwr_buck_clk_source() instead.
*/
pm_err_flag_type pm_dev_pwr_buck_clk_source(unsigned pmic_device_index,
											uint8 externalResourceIndex,
											pm_clk_src_type clkSource );
pm_err_flag_type pm_pwr_buck_clk_source(uint8 externalResourceIndex, 
										pm_clk_src_type clkSource );

/**
 * Switches the buck regulator frequency.
 *
 * @datatypes
 * #pm_smps_switching_freq_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] externalResourceIndex Buck voltage regulator index.
 * @param [in] switchingFreq SMPS switching frequency.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_pwr_buck_switching_freq(), which is deprecated. 
 * Use pm_dev_pwr_buck_switching_freq() instead.
*/
pm_err_flag_type
pm_dev_pwr_buck_switching_freq(unsigned pmic_device_index,
								uint8 externalResourceIndex,
								pm_smps_switching_freq_type switchingFreq);
pm_err_flag_type
pm_pwr_buck_switching_freq(uint8 externalResourceIndex,
							pm_smps_switching_freq_type switchingFreq);
 
/**
 * Allows the user to configure the buck VREGs compensation parameters. 
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_vreg_buck_cfg_cp_type
 *
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] vreg_id Which buck voltage regulator to be configured.
 * @param [in] cfg User's buck VREGs compensation parameters.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_buck_config_comp(), which is deprecated. 
 * Use pm_dev_vreg_buck_config_comp() instead.
*/
pm_err_flag_type pm_dev_vreg_buck_config_comp(unsigned pmic_device_index,
											  pm_vreg_id_type vreg_id,
											  const pm_vreg_buck_cfg_cp_type *cfg);
pm_err_flag_type pm_vreg_buck_config_comp(pm_vreg_id_type vreg_id,
                                          const pm_vreg_buck_cfg_cp_type *cfg);
										  
/**
 * Enables/disables the VREG pull down.
 *
 * @datatypes
 * #pm_switch_cmd_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] externalResourceIndex External resource index; starts from 0.
 * @param [out] enable Commands to enable or disable the VREG pull down.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_pwr_pull_down(), which is deprecated. 
 * Use pm_dev_pwr_pull_down() instead.
*/
pm_err_flag_type pm_dev_pwr_pull_down(unsigned pmic_device_index,
									  uint8 externalResourceIndex, 
									  pm_switch_cmd_type enable);
pm_err_flag_type pm_pwr_pull_down(uint8 externalResourceIndex,
								  pm_switch_cmd_type enable);
 
/**
 * Returns the VREG On/Off state.
 * 
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 *
 * @detdesc
 * This function returns the real-time status of the voltage regulators.
 * The return value is a bit-mapped variable where each bit represents
 * the On (1) or Off (0) state of the regulators enumerated in
 * pm_vreg_mask_type. 
 * @par
 * vreg_rt_status is initialized during a PMIC initialization through a call 
 * to pm_vreg_status_initialize().
 * @par
 * The bits are set or cleared in pm_vreg_control.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_status_get(), which is deprecated. 
 * Use pm_dev_vreg_status_get() instead.
*/
pm_vreg_mask_type pm_dev_vreg_status_get(unsigned pmic_device_index);
pm_vreg_mask_type pm_vreg_status_get(void);

/**
 * Enables or disables the SMPS pulse skipping.
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] vreg_id SMPS to be controlled.
 * @param [out] enable Whether to enable the SMPS pulse skipping. 
 *                     - TRUE -- Enable pulse skipping
 *                     - FALSE -- Disable pulse skipping @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_smps_pulse_skipping_enable(), which is deprecated. 
 * Use pm_dev_vreg_smps_pulse_skipping_enable() instead.
*/
pm_err_flag_type
pm_dev_vreg_smps_pulse_skipping_enable(unsigned pmic_device_index,
									   pm_vreg_id_type vreg_id,
									   boolean enable);
pm_err_flag_type
pm_vreg_smps_pulse_skipping_enable( pm_vreg_id_type vreg_id,
									boolean enable);

/**
 * Gets the SMPS voltage stepping done status.
 *
 * @datatypes
 * #pm_pwr_sw_mode_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] resourceIndex External resource ID.
 * @param [out] mode Variable with the SMPS voltage stepping done status to 
 *                   return to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_mode_status(), which is deprecated. 
 * Use pm_dev_pwr_mode_status() instead.
*/
pm_err_flag_type pm_dev_pwr_mode_status(unsigned pmic_device_index,
										uint8 resourceIndex,
										pm_pwr_sw_mode_type* mode); 
pm_err_flag_type pm_vreg_mode_status(pm_vreg_id_type vreg,
									 pm_pwr_sw_mode_type*  mode_status);

/**
 * Allows the client to change the current limit mode.
 *
 * @datatypes
 * #pm_smps_ilim_mode_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] ilim_mode Current limit mode.
 * @param [in] externalResourceIndex External resource ID.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_smps_inductor_ilim(), which is deprecated. 
 * Use pm_dev_vreg_smps_inductor_ilim() instead.
*/ 
pm_err_flag_type pm_dev_vreg_smps_inductor_ilim(unsigned pmic_device_index,
												pm_smps_ilim_mode_type ilim_mode,
												pm_vreg_id_type externalResourceIndex);
pm_err_flag_type pm_vreg_smps_inductor_ilim(pm_smps_ilim_mode_type ilim_mode,
											pm_vreg_id_type externalResourceIndex);
											
/**
 * Allows the client to read the set current limit mode.
 *
 * @datatypes
 * #pm_smps_ilim_mode_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [in] externalResourceIndex External resource ID.
 * @param [out] ilim_mode Variable with the current limit mode to return to 
                          the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --  SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_vreg_smps_inductor_ilim_status(), which is deprecated. 
 * Use pm_dev_vreg_smps_inductor_ilim_status() instead.
*/
pm_err_flag_type
pm_dev_vreg_smps_inductor_ilim_status(unsigned pmic_device_index,
									  pm_smps_ilim_mode_type* ilim_mode,
									  pm_vreg_id_type externalResourceIndex) ;
pm_err_flag_type
pm_vreg_smps_inductor_ilim_status(pm_smps_ilim_mode_type* ilim_mode,
								  pm_vreg_id_type externalResourceIndex);

/** @} */ /* end_addtogroup pm_vreg */

#endif   /* PM_VREG_H */

