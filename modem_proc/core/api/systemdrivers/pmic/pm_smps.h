#ifndef PM_SMPS_H
#define PM_SMPS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_smps.h PMIC SMPS related declaration.
 *
 * @brief This header file contains enums and API definitions for the Switched
 *		  Mode Power Supply (SMPS) PMIC voltage regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_smps.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/20/14   rk      Provide PMIC API in MPSS to set voltage for Vdd_MSS Rail (CR - 668036)
04/02/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/13   mr      (Tech Pubs) Edited/added Doxygen comments and markup (CR-522045)
08/26/13   rk      remove pin control support and not supported SMPS.
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/** @addtogroup pm_smps 
@{ */
/**
 * @anonenum{SMPS Peripheral Index}
 * Contains all the required SMPS regulators.
 */
enum
{
  PM_SMPS_1,   /*!< SMPS 1. */
  PM_SMPS_2,   /*!< SMPS 2. */
  PM_SMPS_3,   /*!< SMPS 3. */
  PM_SMPS_4,   /*!< SMPS 4. */
  PM_SMPS_INVALID /*!< Invalid value. */
};

/**
 * @enum pm_smps_ilim_mode_type
 * @brief SMPS mode.
 */
typedef enum
{
    PM_ILIM_SMPS_PWM_MODE,  /*!< Pulse-width modulation. */ 
    PM_ILIM_SMPS_AUTO_MODE, /*!< Auto mode; for the SMPS mode
                                 transition between PFM and PWM. */
    PM_ILIM_SMPS_MODE_INVALID /*!< Invalid mode. */
}pm_smps_ilim_mode_type;

/**
 * @enum pm_clk_src_type
 * @brief Clock source.
 */
typedef enum 
{
   PM_CLK_TCXO,          /**< TXCO. */
   PM_CLK_RC,            /**< RC. */
   PM_CLK_SOURCE_INVALID /**< Invalid source. */
}pm_clk_src_type;

/**
 * @enum pm_smps_switching_freq_type
 * @brief SMPS switching frequency. This enumeration assumes an input clock
 *        frequency of 19.2 MHz and is 5 bits long.
 *
 *        Clock frequency = (input clock freq) / ((CLK_PREDIV+1)(CLK_DIV + 1)).
 */
typedef enum 
{
    PM_CLK_19p2_MHz     = 0,   /*!< Clock frequency = 19.2 MHz. */
    PM_CLK_9p6_MHz      = 1,   /*!< Clock frequency = 9.6 MHz. */
    PM_CLK_6p4_MHz      = 2,   /*!< Clock frequency = 6.4 MHz. */
    PM_CLK_4p8_MHz      = 3,   /*!< Clock frequency = 4.8 MHz. */
    PM_CLK_3p84_MHz     = 4,   /*!< Clock frequency = 3.84 MHz. */
    PM_CLK_3p2_MHz      = 5,   /*!< Clock frequency = 3.2 MHz. */
    PM_CLK_2p74_MHz     = 6,   /*!< Clock frequency = 2.74 MHz. */
    PM_CLK_2p4_MHz      = 7,   /*!< Clock frequency = 2.4 MHz. */
    PM_CLK_2p13_MHz     = 8,   /*!< Clock frequency = 2.13 MHz. */
    PM_CLK_1p92_MHz     = 9,   /*!< Clock frequency = 1.92 MHz. */
    PM_CLK_1p75_MHz     = 10,  /*!< Clock frequency = 1.75 MHz. */
    PM_CLK_1p6_MHz      = 11,  /*!< Clock frequency = 1.6 MHz. */
    PM_CLK_1p48_MHz     = 12,  /*!< Clock frequency = 1.48 MHz. */
    PM_CLK_1p37_MHz     = 13,  /*!< Clock frequency = 1.37 MHz. */
    PM_CLK_1p28_MHz     = 14,  /*!< Clock frequency = 1.28 MHz. */
    PM_CLK_1p2_MHz      = 15,  /*!< Clock frequency = 1.2 MHz. */
    PM_CLK_1p13_MHz     = 16,  /*!< Clock frequency = 1.13 MHz. */
    PM_CLK_1p07_MHz     = 17,  /*!< Clock frequency = 1.07 MHz. */
    PM_CLK_1p01_MHz     = 18,  /*!< Clock frequency = 1.01 MHz. */
    PM_CLK_960_KHz      = 19,  /*!< Clock frequency = 960 kHz. */
    PM_CLK_914_KHz      = 20,  /*!< Clock frequency = 914 kHz. */
    PM_CLK_873_KHz      = 21,  /*!< Clock frequency = 873 kHz. */
    PM_CLK_835_KHz      = 22,  /*!< Clock frequency = 835 kHz. */
    PM_CLK_800_KHz      = 23,  /*!< Clock frequency = 800 kHz. */
    PM_CLK_768_KHz      = 24,  /*!< Clock frequency = 768 kHz. */
    PM_CLK_738_KHz      = 25,  /*!< Clock frequency = 738 kHz. */
    PM_CLK_711_KHz      = 26,  /*!< Clock frequency = 711 kHz. */
    PM_CLK_686_KHz      = 27,  /*!< Clock frequency = 686 kHz. */
    PM_CLK_662_KHz      = 28,  /*!< Clock frequency = 662 kHz. */
    PM_CLK_640_KHz      = 29,  /*!< Clock frequency = 640 kHz. */
    PM_CLK_619_KHz      = 30,  /*!< Clock frequency = 619 kHz. */
    PM_CLK_600_KHz      = 31,  /*!< Clock frequency = 600 kHz. */
    PM_SWITCHING_FREQ_INVALID, /*!< Invalid value. */
	PM_SWITCHING_FREQ_FREQ_NONE /*!< No frequency. */
}pm_smps_switching_freq_type;

/**
 * @enum pm_quiet_mode_type
 * @brief Quiet mode.
 */
typedef enum
{
    PM_QUIET_MODE__DISABLE,      /*!< Disabled (default). */
    PM_QUIET_MODE__QUIET,        /*!< Enabled. */
    PM_QUIET_MODE__SUPER_QUIET,  /*!< Super Quiet mode is enabled. */
    PM_QUIET_MODE__INVALID       /*!< Invalid mode. */
}pm_quiet_mode_type;


/*===========================================================================

                    SMPS DRIVER API PROTOTYPE

===========================================================================*/
/**
 * Returns the mode status (LPM, NPM, auto, bypass) of the selected power 
 * rail. 
 * 
 * @note1hang A regulator's mode changes dynamically.
 *
 * @datatypes
 * #pm_sw_mode_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param [out] sw_mode Variable with the mode status to return to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_sw_mode_status(uint8 pmic_chip,
										uint8 smps_peripheral_index,
										pm_sw_mode_type* sw_mode);

/** 
 * Returns the pin controlled status or the hardware enable status (ON/OFF) 
 * of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param[in]  pmic_chip Primary: 0. Secondary: 1.
 * @param[in]  smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param[out] on_off Variable with the pin control status to return to the 
                      caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not supported under error 
 *                                       conditions such as invalid parameter 
 *                                       entered.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_pin_ctrled_status(uint8 pmic_chip,
											uint8 smps_peripheral_index,
											pm_on_off_type* on_off);
											
/**
 * Returns the voltage level (in microvolts) of the selected power rail.
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param [out] volt_level Variable with voltage level status in microvolts 
 *                         (uint32) to return to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_volt_level_status (uint8 pmic_chip,
											uint8 smps_peripheral_index,
											pm_volt_level_type* volt_level);
											

/**
 * @name pm_smps_volt_level
 *
 * @brief This function sets the voltage level for the selected power rail.
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1
 * @param [in] smps_peripheral_index Starts from 0 (for first SMPS peripheral)
 *
 * @param [in] volt_level Variable to pass the volt level to be set.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */

pm_err_flag_type pm_smps_volt_level(uint8 pmic_chip, uint8 perph_index, pm_volt_level_type volt_level);



/**
 * Returns the software enable status (ON/OFF) of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param [out] on_off Variable with the software status to return to the 
 *                     caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_sw_enable_status(uint8 pmic_chip,
										  uint8 smps_peripheral_index,
										  pm_on_off_type* on_off);
										  
/**
 * Selects the current limit for the inductor of a selected SMPS.
 *
 * @datatypes
 * #pm_smps_ilim_mode_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param [in] ilim_level Current limit level. Range:
 *             - 3500 mA to 600 mA
 *             - 2x SMPS subtype current limit range -- 600 mA to 2700 mA
 *             - 3x SMPS subtype current limit range -- 700 mA to 3500 mA @tablebulletend 
 * @param [in] smps_mode SMPS mode.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_inductor_ilim (uint8 pmic_chip,
										uint8 smps_peripheral_index,
										uint16 ilim_level,
										pm_smps_ilim_mode_type smps_mode);
										
/**
 * Obtains the current limit for the inductor of a selected SMPS by
 * reading the SPMI register.
 *
 * @datatypes
 * #pm_smps_ilim_mode_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] smps_peripheral_index SMPS peripheral index. Starts from 0 
 *                                   (for first SMPS peripheral).
 * @param [in] smps_mode SMPS mode.
 * 
 * @param [out] ilim_level Current limit level. Range:
 *              - 3500 mA to 600 mA
 *              - 2x SMPS subtype current limit range -- 600 mA to 2700 mA
 *              - 3x SMPS subtype current limit range -- 700 mA to 3500 mA @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_smps_inductor_ilim_status(uint8 pmic_chip,
											  uint8 smps_peripheral_index,
											  uint16* ilim_level,
											  pm_smps_ilim_mode_type smps_mode);
/** @} */ /* end_addtogroup pm_smps */

#endif    /* PM_SMPS_H */

