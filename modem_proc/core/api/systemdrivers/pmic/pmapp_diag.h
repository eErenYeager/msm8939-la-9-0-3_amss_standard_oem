#ifndef PM_APP_DIAG_H
#define PM_APP_DIAG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pmapp_diag.h PMIC DIAG related declaration.
 *
 * @brief Packet definitions between the diagnostic subsystem
 *        and the external device.
 *
 * @note All member structures of DIAG packets must be PACKED.
 *
 * @warning Each command code number is part of the externalized
 *          diagnostic command interface.  This number *MUST* be
 *			assigned by a member of QCT's tools development team.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pmapp_diag.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/29/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
09/13/11   hs      Changed PACKED struct to PACK(struct) to work with Q6 compiler.
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
04/24/09   wra     Added the pm_app_set_mode diag message definition.
08/25/05   Frank   Added diag lib.
04/05/04   rmd     Featurized PMIC DIAG services under 
                   "FEATURE_PMIC_DIAG_SERVICES".
03/24/04   rmd     Updated file to follow PMIC VU coding standards.
02/29/04   st      Created file.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "diagcmd.h"
#include "diagpkt.h"

#if defined(T_WINNT)
   #error code not present
#endif


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/** 
 * @brief Diag PEEK POKE 
 */
#define PM_APP_DIAG_PEEK_POKE   0


/*==========================================================================
  
                  SUBSYSTEM PACKET FUNCTION PROTOTYPES
   
============================================================================*/

/** @addtogroup pm_diag 
@{ */
/**
 * Registers the diagnostics packet function dispatch table.
 *
 * @return
 * None.
 */
void pm_app_diag_init (void);
/** @} */ /* end_addtogroup pm_diag */

#endif /* PM_APP_DIAG_H */

