#ifndef PM_MISC_H
#define PM_MISC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_misc.h PMIC-MISC Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support 
 *        the PMIC MISC module.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_misc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
05/21/09   wra     New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/* 
 * The following MISC module type defined is not defined in the class because
 * it needs to be accessible by other modules.
 */
 
/** @addtogroup pm_misc 
@{ */
/**
 * @enum pm_misc_legacy_mode_type
 * @brief Legacy mode action type.
 */
typedef enum
{
  PM_MISC_LEGACY_MODE__DESSERT = 0, /**< Legacy deassert. */
  PM_MISC_LEGACY_MODE__ASSERT,      /**< Legacy assert. */
  PM_MISC_LEGACY_MODE__INVALID      /**< Invalid legacy mode. */
}pm_misc_legacy_mode_type;

/**
 * @enum pm_misc_res_state_type
 * @brief Regulator state.
 */
typedef enum
{
  PM_MISC_RES_STATE__FOLLOW_BOOT = 0, /**< Follow boot. */
  PM_MISC_RES_STATE__OFF         = 1, /**< OFF. */
  PM_MISC_RES_STATE__LPM         = 2, /**< LPM. */
  PM_MISC_RES_STATE__HPM         = 3, /**< HPM. */
  PM_MISC_RES_STATE__INVALID          /**< Invalid state. */
}pm_misc_res_state_type;

/**
 * @enum pm_misc_mode_act_type
 * @brief Mode action type.
 */
typedef enum
{
  PM_MISC_MODE_ACT__DESSERT = 0, /**< Deassert. */
  PM_MISC_MODE_ACT__ASSERT,      /**< Assert. */
  PM_MISC_MODE_ACT__INVALID      /**< Invalid mode. */
}pm_misc_mode_act_type;

/**
 * @enum pm_misc_mode_ctrl_mask_type
 * @brief Mode control mask.
 */
typedef enum
{
  PM_MISC_MODE_CTRL_MASK__FOLLOW_MODE_ACT = 0, /**< Follow mode action. */
  PM_MISC_MODE_CTRL_MASK__MODE_ACT_DONTCARE,   /**< Mode action do not care. */
  PM_MISC_MODE_CTRL_MASK__INVALID              /**< Invalid control mask. @newpage */
}pm_misc_mode_ctrl_mask_type;

/**
 * @enum pm_misc_sel_ctrl_pin_type
 * @brief Select control pins.
 * 
 * @note This enumumeration also acts as a ``mask'' for the register.
 */
typedef enum
{
  PM_MISC_SEL_CTRL_PIN0 = 0x01, /**< Pin 0. */
  PM_MISC_SEL_CTRL_PIN1 = 0x02, /**< Pin 1. */
  PM_MISC_SEL_CTRL_PIN2 = 0x04, /**< Pin 2. */
  PM_MISC_SEL_CTRL_MODE = 0x08, /**< Select control mode. */
  PM_MISC_SEL_CTRL_PIN__INVALID /**< Invalid pin. */
}pm_misc_sel_ctrl_pin_type;

/**
 * @enum pm_misc_ctrl_pin_mask_type
 * @brief Control pin mask.
 */
typedef enum
{
  PM_MISC_CTRL_PIN_MASK__ALLOW_PIN_CTRL = 0, /**< Allow pin control. */
  PM_MISC_CTRL_PIN_MASK__DISALLOW_PIN_CTRL,  /**< Disallow pin control. */
  PM_MISC_CTRL_PIN_MASK__INVALID             /**< Invalid pin mask. */
}pm_misc_ctrl_pin_mask_type;

/**
 * @enum pm_misc_term_type
 * @brief Control terminology.
 */
typedef enum
{
  PM_MISC_TERM__FORCE_OFF        = 0x00, /**< Force an OFF. */
  PM_MISC_TERM__FORCE_PULLUP     = 0x01, /**< Force a pullup. */
  PM_MISC_TERM__FORCE_PULLDOWN   = 0x10, /**< Force a pulldown. */
  PM_MISC_TERM__FOLLOW_MODE_CTRL = 0x11, /**< Follow mode control. */
  PM_MISC_TERM__INVALID                  /**< Invalid terminology. */
}pm_misc_term_type;

/**
 * @enum pm_misc_dtest_type
 * @brief DTEST mode type.
 */
typedef enum
{
  PM_MISC_DTEST__NORMAL = 0,          /**< Normal. */
  PM_MISC_DTEST__MODE_CTRL_TO_DTEST3, /**< Mode control to DTEST 3. */
  PM_MISC_DTEST__INVALID              /**< Invalid DTEST. @newpage */
}pm_misc_dtest_type;

/**
 * @enum pm_misc_polarity_type
 * @brief For a CTRLx pin, this means wire enable with an active high OR 
 *        wire enable with an active low.
 *        For a Mode_Ctrl pin, This means Low Power mode with active high OR 
 *        Low Power mode with an active low.
 */
typedef enum
{
  PM_MISC_POLARITY__ACTIVE_H = 0, /**< Active high. */
  PM_MISC_POLARITY__ACTIVE_L,     /**< Active low. */
  PM_MISC_POLARITY__INVALID       /**< Invalid polarity. */
}pm_misc_polarity_type;
/** @} */ /* end_addtogroup pm_misc */

#endif    /* PM_MISC_H */

