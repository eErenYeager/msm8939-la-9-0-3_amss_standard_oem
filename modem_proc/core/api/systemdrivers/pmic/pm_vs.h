#ifndef PM_VS_H
#define PM_VS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_vs.h PMIC VOLTAGE SWITCHER related declaration.
 *
 * @brief This header file contains enums and API definitions for the voltage
 *		  switcher for PMIC regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_vs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/29/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/13   mr      (Tech Pubs) Edited/added Doxygen comments and markup (CR-522045)
01/10/13   kt      Changing the file as per Henry's Phase1 changes.  
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
06/04/10   hw      Creation of Voltage Switch (VS) Module
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_vs 
@{ */
/**
 * @anonenum{Voltage Switcher Peripheral Index}
 * Contains all the required VS regulators. 
 */
enum
{
  PM_VS_LVS_1,  /**< Low Voltage Switch (LVS) 1. */
  PM_VS_LVS_2,  /**< LVS 2. */
  PM_VS_LVS_3,  /**< LVS 3. */
  PM_VS_LVS_4,  /**< LVS 4. */
  PM_VS_LVS_5,  /**< LVS 5. */
  PM_VS_LVS_6,  /**< LVS 6. */
  PM_VS_LVS_7,  /**< LVS 7. */
  PM_VS_LVS_8,  /**< LVS 8. */
  PM_VS_LVS_9,  /**< LVS 9. */
  PM_VS_LVS_10, /**< LVS 10. */
  PM_VS_LVS_11, /**< LVS 11. */
  PM_VS_LVS_12, /**< LVS 12. */
  PM_VS_MVS_1,  /**< Multi voltage switch 1. */
  PM_VS_OTG_1,  /**< On-the-go 1. */
  PM_VS_HDMI_1, /**< High definition multimedia interface 1. */
  PM_VS_INVALID /**< Invalid value. */
};

/*===========================================================================

                VOLTAGE SWITCHER FUNCTION PROTOTYPE

===========================================================================*/
/**
 * Returns the mode status (LPM, NPM, auto, bypass)
 * of the selected power rail. 
 *
 * @note1hang A regulator's mode changes dynamically.
 *
 * @datatypes
 * #pm_sw_mode_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] vs_peripheral_index Voltage Switcher (VS) peripheral index.
 * @param [out] sw_mode Variable with the switcher mode status to return 
 *                      to the caller.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_vs_sw_mode_status(uint8 pmic_chip,
									uint8 vs_peripheral_index,
									pm_sw_mode_type* sw_mode);

/**
 * Returns the pin controlled status or hardware enable status (ON/OFF)
 * of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] vs_peripheral_index VS peripheral index.
 * @param [out] on_off Variable returned to the caller with the switcher pin
 *                     control status.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_vs_pin_ctrled_status(uint8 pmic_chip,
										uint8 vs_peripheral_index,
										pm_on_off_type* on_off);

/**
 * Returns the software enable status (ON/OFF) of the selected power rail.
 *
 * @datatypes
 * #pm_on_off_type
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1.
 * @param [in] vs_peripheral_index VS peripheral index.
 * @param [out] on_off Variable returned to the caller with the switcher 
 *                     enable status.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_vs_sw_enable_status (uint8 pmic_chip,
										 uint8 vs_peripheral_index,
										 pm_on_off_type* on_off);
/** @} */ /* end_addtogroup pm_vs */

#endif    /* PM_VS_H */
