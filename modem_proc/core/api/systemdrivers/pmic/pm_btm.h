#ifndef PM_BTM_H
#define PM_BTM_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_btm.h PMIC-BTM Module related declaration.
 *
 * @brief  This header file contains functions and variable declarations
 *         to support Qualcomm PMIC BTM (Battery Temperature Management) module.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_btm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/02/11   hs      Initial version.
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_btm 
@{ */
/**
 * @enum pm_btm_mode_type
 * @brief BTM mode.
 */
typedef enum
{
   PM_BTM_MODE__MODULE_OFF,  /*!< BTM module is OFF.  */
   PM_BTM_MODE__MODULE_ON    /*!< BTM module is ON.  */
}pm_btm_mode_type;

/**
 * @enum pm_btm_conv_req_cmd_type
 * @brief Commands used to set or clear the conversion requested in the
 *  the BTM module.
 */
typedef enum
{
   PM_BTM_CONV_REQ_CMD__OFF,  /*!< Stop a conversion.  */
   PM_BTM_CONV_REQ_CMD__ON    /*!< Request a conversion.  */
}pm_btm_conv_req_cmd_type;

/**
 * @enum pm_btm_xoadc_conversion_status_type
 * @brief Conversion status.
 */
typedef enum
{
    PM_BTM_XOADC_CONVERSION_STATUS__COMPLETE,  /*!< Conversion is complete.  */
    PM_BTM_XOADC_CONVERSION_STATUS__PENDING    /*!< Waiting for ADC to complete 
                                                   a conversion for another 
                                                   process.  */
} pm_btm_xoadc_conversion_status_type;

/**
 * @enum pm_btm_meas_mode_type
 * @brief BTM measurement mode.
 */

typedef enum
{
   PM_BTM_MEAS_MODE_SINGLE = 0,   /*!< Single.  */
   PM_BTM_MEAS_MODE_CONTINUOUS    /*!< Continuous. */
}pm_btm_meas_mode_type;

/**
 * @enum pm_btm_mux_type
 * @brief Pre-MUX selection.
 */

typedef enum
{
   PM_BTM_AMUX_MAIN,     /*!< BTM AMUX main.  */
   PM_BTM_PREMUX_TO_CH6, /*!< BTM pre-MUX to ch6.  */
   PM_BTM_PREMUX_TO_CH7, /*!< BTM pre-MUX to ch7.  */
   PM_BTM_RSV_DISABLED   /*!< BTM RSV is disabled. */
} pm_btm_mux_type;

/**
 * @enum pm_btm_xoadc_input_select_type
 * @brief XOADC input.
 */
typedef enum
{
   PM_BTM_XOADC_INPUT_XO_IN_XOADC_GND,      /*!< Input XO in XOADC GND. */
   PM_BTM_XOADC_INPUT_PMIC_IN_XOADC_GND,    /*!< Input PMIC in XOADC GND. */
   PM_BTM_XOADC_INPUT_PMIC_IN_BMS_CSP,      /*!< Input PMIC in BMS CSP. */
   PM_BTM_XOADC_INPUT_RESERVED,             /*!< Input is reserved. */
   PM_BTM_XOADC_INPUT_XOADC_GND_XOADC_GND,  /*!< Input XOADC GND XOADC GND. */
   PM_BTM_XOADC_INPUT_XOADC_VDD_XOADC_GND,  /*!< Input XOADC VDD XOADC GND. */
   PM_BTM_XOADC_INPUT_VREFN_VREFN,          /*!< Input VREFN VREFN. */
   PM_BTM_XOADC_INPUT_VREFP_VREFN           /*!< Input VREFP VREFN. */
} pm_btm_xoadc_input_select_type;

/**
 * @enum pm_btm_xoadc_decimation_ratio_type
 * @brief XOADC decimation ratio.
 */
typedef enum
{
  PM_BTM_XOADC_DECIMATION_RATIO_512,   /*!< Ratio is 512. */
  PM_BTM_XOADC_DECIMATION_RATIO_1K,    /*!< Ratio is 1 K. */
  PM_BTM_XOADC_DECIMATION_RATIO_2K,    /*!< Ratio is 2 K. */
  PM_BTM_XOADC_DECIMATION_RATIO_4K,    /*!< Ratio is 4 K. */
} pm_btm_xoadc_decimation_ratio_type;

/**
 * @enum pm_btm_xoadc_conversion_rate_type
 * @brief XOADC conversion rate.
 */
typedef enum
{
  PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_8,  /*!< Conversion is at TCXO/8. */
  PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_4,  /*!< Conversion is at TCXO/4. */
  PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_2,  /*!< Conversion is at TCXO/2. */
  PM_BTM_XOADC_CONVERSION_RATE_TCXO         /*!< Conversion is at TCXO frequency. */
} pm_btm_xoadc_conversion_rate_type;


/*===========================================================================

                 BTM DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Enables/disables the BTM timing interval battery
 * temperature measurements when the ADC Arbiter module is enabled.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_mode_type
 *
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] mode Enables or disables the BTM module.
 *                  - PM_BTM_CMD__ENABLE -- 
 *                    Enable the BTM module
 *                  - PM_BTM_CMD__DISABLE -- 
 *                    Disable the BTM module @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_btm_set_mode(int externalResourceIndex,
                                 pm_btm_mode_type mode);

/**
 * Sets the conversion request.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_conv_req_cmd_type
 *
 * @detdesc 
 * After the request is sent, the ADC starts the battery temperature 
 * measurement. The request is cleared when a single measurement mode is 
 * selected and after the ADC conversion is complete.
 *
 * @param [in] externalResourceIndex External BTM ID. 
 * @param [in] cmd Requests an ADC conversion.
 *                 - PM_BTM_CMD__ENABLE -- 
 *                   Request a conversion; the XOADC will be enabled.
 *                 - PM_BTM_CMD__DISABLE -- 
 *                   Do not request a conversion; the XOADC will be disabled. @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
*/
pm_err_flag_type pm_btm_set_conversion_request(int externalResourceIndex,
                                               pm_btm_conv_req_cmd_type cmd);

/**
 * Gets the conversion status based on the status of the
 * conversion status strobe and the end-of-conversion status flag.
 *
 * @datatypes
 * #pm_btm_xoadc_conversion_status_type
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] status Status of the conversion request. \n
 *                    - PM_BTM_CONVERSION_STATUS__INVALID -- 
 *                      Invalid status
 *                    - PM_BTM_CONVERSION_STATUS__COMPLETE -- 
 *                      Conversion is complete
 *                    - PM_BTM_CONVERSION_STATUS__OCCURRING -- 
 *                      Conversion is in progress
 *                    - PM_BTM_CONVERSION_STATUS__WAITING -- 
 *                      Waiting for ADC to complete another process' conversion 
 *                      request or an interval timer to expire @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid pointer was passed in.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
*/
pm_err_flag_type pm_btm_get_conversion_status(int externalResourceIndex,
                            pm_btm_xoadc_conversion_status_type *status);

/**
 * Sets the battery temperature measurement mode.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_meas_mode_type
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] mode Sets the operating mode of the measurement.
 *                  - PM_XOADC_BTM_MEAS_MODE_SINGLE -- 
 *                    Measure the battery temperature after a single 
 *                    measurement interval time
 *                  - PM_XOADC_BTM_MEAS_MODE_CONTINUOUS -- 
 *                    Continuously measure the battery temperature at 
 *                    measurement interval times @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_measurement_mode(int externalResourceIndex,
                                             pm_btm_meas_mode_type mode);

/**
 * Sets the battery temperature measurement interval time.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] second Interval time. \n
 *      @note1 If the value of this parameter is out of the valid range that 
 *             is acceptable by the PMIC hardware, the value is limited to 
 *             the boundary and the PMIC hardware is set to that value. At 
 *             the same time, PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to 
 *             indicate there was an out-of-bounds <i>second</i> value 
 *             parameter input.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_measurement_interval(int externalResourceIndex,
                                                 uint32 second);

/**
 * Sets up the AMUX, such as channel and pre-MUX selection.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_mux_type
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] channel Analog channel.
 * @param [in] mux MUX.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE -- 
 * Input parameter three is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_amux(int externalResourceIndex,
                                 uint8 channel,
                                 pm_btm_mux_type mux);

/**
 * Gets the prescalar value for the specified channel.
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] channel Analog channel.
 * @param [out] numerator Numerator of the prescalar.
 * @param [out] denominator Denominator of the prescalar.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid input pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_get_prescalar(int externalResourceIndex,
                                      uint8 channel,
                                      uint8 *numerator,
                                      uint8 *denominator);

/**
 * Sets the XOADX input that is used for BTM.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_xoadc_input_select_type
 *
 * @param [in] externalResourceIndex The external BTM ID.
 * @param [in] input Selects the XOADC input. \n
 *                   - PM_BTM_XOADC_INPUT_PMIC -- PMIC_IN
 *                   - PM_BTM_XOADC_INPUT_XO -- XO_IN
 *                   - PM_BTM_XOADC_INPUT_VREFP -- VREFP @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_xoadc_input(int externalResourceIndex,
                                        pm_btm_xoadc_input_select_type input);

/**
 * Sets the XOADC decimation ratio that is used for BTM.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_xoadc_decimation_ratio_type
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] ratio Selects the XOADC decimation ratio.
 *                  - PM_BTM_XOADC_DECIMATION_RATIO_512 -- 512
 *                  - PM_BTM_XOADC_DECIMATION_RATIO_1K -- 1 K
 *                  - PM_BTM_XOADC_DECIMATION_RATIO_2K -- 2 K
 *                  - PM_BTM_XOADC_DECIMATION_RATIO_4K -- 4 K @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_xoadc_decimation_ratio(int externalResourceIndex,
                                    pm_btm_xoadc_decimation_ratio_type ratio);

/**
 * Sets the XOADC clock rate that is used for BTM.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_btm_xoadc_conversion_rate_type
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] rate Selects the XOADC clock rate. \n
 *                  - PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_8 -- TCXO/8
 *                  - PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_4 -- TCXO/4
 *                  - PM_BTM_XOADC_CONVERSION_RATE_TCXO_DIV_2 -- TCXO/2
 *                  - PM_BTM_XOADC_CONVERSION_RATE_TCXO -- TCXO/1 @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_xoadc_conversion_rate(int externalResourceIndex,
                                    pm_btm_xoadc_conversion_rate_type rate);

/**
 * Gets the XOADC conversion result.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [out] data Stores the XOADC conversion result.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Invalid input pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_get_xoadc_data(int externalResourceIndex,
                                       uint32* data);

/**
 * Sets the temperature threshold for the battery to be deemed warm.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] warm_temp_thresh Sets the warm battery temperature threshold.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_batt_warm_threshold(int externalResourceIndex,
                                                uint32 warm_temp_thresh);

/**
 * Sets the temperature threshold for the battery to be deemed cool.
 *
 * @note The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External BTM ID.
 * @param [in] cool_temp_thresh Sets the cool battery temperature threshold.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE -- 
 * The current processor does not have access to the BTM module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED -- 
 * The external resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_BTM_INDEXED -- 
 * The internal resource ID for the BTM module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage 
 */
pm_err_flag_type pm_btm_set_batt_cool_threshold(int externalResourceIndex,
                                                uint32 cool_temp_thresh);
/** @} */ /* end_addtogroup pm_btm */

#endif /* PM_BTM_H */

