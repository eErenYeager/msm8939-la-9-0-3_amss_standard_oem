#ifndef PM_DIAG_H
#define PM_DIAG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_diag.h  PMIC-DIAG related declaration.
 *
 * @brief This file contains variable and function declarations to support 
 *        the PMIC diag interface.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_diag.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/29/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/20/10   wra     Added diag call that will allow targets with multiple
                   PMICs to be specifically written to/read from based on
                   their PMIC device index.
08/04/09   jtn     Fix Lint critical and high warnings.
08/01/09   jtn     Added declaration for pm_app_set_mode_diag_i()
06/16/09   jtn     New file
===========================================================================*/


/*===========================================================================

                    DIAG FUNCTION PROTOTYPES

===========================================================================*/

/** @addtogroup pm_diag 
@{ */
/**
 * Diagnostics-related function.
 * 
 * @param [in] req_pkt Requested command packet.
 * @param [out] data Pointer to the response data.
 *
 * @return
 * None.
 *
 */
void pm_app_diag_lib_diag_i(PACKED void* req_pkt, PACKED void* data);

/**
 * Diagnostics-related function, external.
 *
 * @param [in] req_pkt Requested command packet.
 * @param [out] data Pointer to the response data.
 *
 * @return
 * None.
 *
 */
 void pm_app_diag_lib_diag_ext_i(PACKED void* req_pkt,PACKED void* data);

/**
 * Diagnostics-related function, development.
 *
 * @param [in] req_pkt Requested command packet.
 * @param [out] data Pointer to the response data.
 *
 * @return
 * None.
 *
 */
 void pm_app_diag_lib_diag_dev_i(PACKED void* req_pkt,PACKED void* data);

/**
 * Sets the diagnostics mode.
 *
 * @param [in] req_pkt Requested command packet.
 * @param [out] data Pointer to the response data.
 *
 * @return
 * None.
 *
 */
 void pm_app_set_mode_diag_i(PACKED void* req_pkt, PACKED void* data);
/** @} */ /* end_addtogroup pm_diag */

#endif    /* PM_DIAG_H */

