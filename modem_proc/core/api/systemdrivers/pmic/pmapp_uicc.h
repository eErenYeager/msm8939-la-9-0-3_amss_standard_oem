#ifndef PMAPP_UICC_H
#define PMAPP_UICC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pmapp_uicc.h PMIC UICC APPLICATION related declaration.
 *
 * @brief This file contains functions prototypes and variable/type/constant
 *        declarations to support the HOTSWAP (UICC) feature inside the
 *        Qualcomm PMIC chips.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pmapp_uicc.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/28/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
09/14/11   dy      Move UICC resource to pm_external_resources.h
07/14/11   dy      Rename APIs
05/27/11   vk      Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/** @addtogroup pm_uicc 
@{ */
/**
 * @enum pm_uicc_card_detect_status
 * @brief Specifies all the event types that are supported by
 *        the PMIC driver.
*/
typedef enum
{
    PM_UICC_CARD_INSERTED,     /**< Card is inserted. */
    PM_UICC_CARD_REMOVED,      /**< Card is removed. */
    PM_UICC_CARD_STATE_INVALID /**< Invalid card state. */
}pm_uicc_card_detect_status;


/*===========================================================================

                 UICC DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Queries the real-time status of the UICC card
 * (inserted or removed). This function is called by the UIM driver
 * during boot-up to detect which of the two slots is present or not present.
 * Also, the function is used in the UIM callback function as part of the 
  * software debounce logic.
 *
 * @datatypes
 * #pm_uicc_card_detect_status

 * @param [in] uicc_id UICC ID; either PM_UICC_1 or PM_UICC_2 for the two slots. 
 * @param [out] uicc_state Real-time status of the UICC slot; either 
                           inserted or removed.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage
*/
pm_err_flag_type pmapp_uicc_query_status(int uicc_id, pm_uicc_card_detect_status* uicc_state );

/**
 * The UIM driver uses this function to unmask the UICC interrupt
 * when its software debounce logic is complete.
 *
 * @param [in] uicc_id UICC ID; either PM_UICC_1 or PM_UICC_2 for the two slots. 
 * @param [out] status Status.
 *                     - TRUE -- Unmasking is successful
 *                     - FALSE -- Unmasking is not successful @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage
 */
pm_err_flag_type pmapp_uicc_unmask_interrupt (int uicc_id, boolean* status);

/**
 * Registers the UICC callback that needs to be
 * called inside the corresponding ISR for a UICC slot. 
 *
 * @param [in] uicc_id UICC ID; either PM_UICC_1 or PM_UICC_2 for the two slots. 
 * @param [in] *pm_uicc_cb Pointer to the callback function.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage
 */
pm_err_flag_type pmapp_uicc_register_callback( int uicc_id, void (*pm_uicc_cb) (pm_uicc_card_detect_status));
/** @} */ /* end_addtogroup pm_uicc */

#endif    /* PMAPP_UICC_H */
