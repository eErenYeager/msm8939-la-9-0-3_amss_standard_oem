#ifndef PM_NCP_H
#define PM_NCP_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_ncp.h PMIC-NCP Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support 
 *        the PMIC negative charge pump driver module.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_ncp.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
06/20/09   jtn     New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_lib_cmd.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/** @addtogroup pm_ncp
@{ */
/**
 * @enum pm_ncp_switching_freq_type
 * @brief NCP switching frequency.
 */
typedef enum
{
    PM_NCP_FREQ_9_6MHZ,   /**< 9.6 MHz. */
    PM_NCP_FREQ_4_8MHZ,   /**< 4.8 MHz. */
    PM_NCP_FREQ_3_2MHZ,   /**< 3.2 MHz. */
    PM_NCP_FREQ_2_4MHZ,   /**< 2.4 MHz. */
    PM_NCP_FREQ_1_92MHZ,  /**< 1.92 MHz. */
    PM_NCP_FREQ_1_6MHZ,   /**< 1.6 MHz (default). */
    PM_NCP_FREQ_1_37MHZ,  /**< 1.37 MHz. */
    PM_NCP_FREQ_1_2MHZ,   /**< 1.2 MHz. */
    PM_NCP_FREQ_OUT_OF_RANGE   /**< Frequency is out of range. */
}pm_ncp_switching_freq_type;


/*===========================================================================

                    NCP FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Enables the sample comparator mode for VREG_NCP and allows the user to 
 * set the switching frequency. 
 *
 * @datatypes
 * #pm_ncp_switching_freq_type
 *
 * @param [in] OnOff Enable or disable the sample comparator output.
 * @param [in] freq Switching frequency.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC. 
 * @newpage
 */
pm_err_flag_type pm_vreg_ncp_sample_comp_mode_enable(pm_switch_cmd_type OnOff,
													 pm_ncp_switching_freq_type freq);

/**
 * Enables/disables the NCP functionality.
 *
 * @datatypes
 * #pm_switch_cmd_type
 * 
 * @param [in] on_off PM_ON_CMD Enables the NCP functionality;
 *  				  PM_OFF_CMD otherwise.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC. 
 * @newpage
 */
pm_err_flag_type pm_ncp_control(pm_switch_cmd_type on_off);

/** @} */ /* end_addtogroup pm_ncp */

#endif    /* PM_NCP_H */
