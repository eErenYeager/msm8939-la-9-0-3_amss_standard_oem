#ifndef PM_ERR_FLAGS_H
#define PM_ERR_FLAGS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_err_flags.h PMIC Library error codes declaration.
 *
 * @brief This file is defines error codes returned by PMIC library APIs.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_err_flags.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
01/11/13   kt      Changed file name from pm_lib_err.h to pm_err_flags.h.
03/27/12   hs      Added PM_ERR_FLAG__SPMI_TRANSCOMM_ERR.
02/10/11   hw      Added PM_ERR_FLAG__INVALID_DISPBACKLIGHT_APP_INDEXED
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
01/07/10   wra     Adding SMBC and BMS error codes
08/10/10   wra     Adding PMIC Application Event error codes
06/28/10   wra     Adding 8660 error codes
12/02/09   jtn     Add error code for keypad
08/06/09   jtn     Add PM_ERR_MAX() macro
07/23/09   wra     added vbatt error definitions.
06/30/09   jtn     Updated file documentation
05/01/09   jtn     New file
===========================================================================*/

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_err_flags 
@{ */

/**
 * @enum pm_err_flag_type
 * @brief These are all the error codes returned by PMIC APIs.
 *        PMIC APIs most commonly return errors if the caller has tried
 *        to use parameters that are out of range or if the feature is
 *        not supported by the PMIC model detected at runtime.
 */
typedef enum
{
    /**
     * API completed successfully.
     */
    PM_ERR_FLAG__SUCCESS,
    /*
     * SBI/I2C/SPMI operation failed;
     * extra lines are to support += error codes.
     */
    PM_ERR_FLAG__SBI_OPT_ERR,   /**< Serial Bus Interface (SBI) operation error. */
    PM_ERR_FLAG__SBI_OPT2_ERR,  /**< SBI operation 2 error. */
    PM_ERR_FLAG__SBI_OPT3_ERR,  /**< SBI operation 3 error. */
    PM_ERR_FLAG__SBI_OPT4_ERR,  /**< SBI operation 4 error. */
    PM_ERR_FLAG__I2C_OPT_ERR,   /**< Inter-integrated Circuit (I2C) operation 
                                     error. */
    PM_ERR_FLAG__I2C_OPT2_ERR,  /**< I2C operation 2 error. */
    PM_ERR_FLAG__I2C_OPT3_ERR,  /**< I2C operation 3 error. */
    PM_ERR_FLAG__I2C_OPT4_ERR,  /**< I2C operation 4 error. */
    PM_ERR_FLAG__SPMI_OPT_ERR,  /**< System Power Management Interface (SPMI)
                                     operation error. */
    PM_ERR_FLAG__SPMI_OPT2_ERR, /**< SPMI operation 2 error. */
    PM_ERR_FLAG__SPMI_OPT3_ERR, /**< SPMI operation 3 error. */
    PM_ERR_FLAG__SPMI_OPT4_ERR, /**< SPMI operation 4 error. */
    PM_ERR_FLAG__SPMI_TRANSCOMM_ERR, /**< SPMI transcomm error. */
    /**
     *  Input parameter 1 is out of range.
     */
    PM_ERR_FLAG__PAR1_OUT_OF_RANGE,
    PM_ERR_FLAG__PAR2_OUT_OF_RANGE, /**< Input parameter 2 is out of range. */
    PM_ERR_FLAG__PAR3_OUT_OF_RANGE, /**< Input parameter 3 is out of range. */
    PM_ERR_FLAG__PAR4_OUT_OF_RANGE, /**< Input parameter 4 is out of range. */
    PM_ERR_FLAG__PAR5_OUT_OF_RANGE, /**< Input parameter 5 is out of range. */
    PM_ERR_FLAG__PAR6_OUT_OF_RANGE, /**< Input parameter 6 is out of range. */
    /*
     *  Voltage regulator/level is out of range.
     */
    PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE,   /**< Voltage level is out of range. */
    PM_ERR_FLAG__VREG_ID_OUT_OF_RANGE,  /**< Voltage regulator is out of range. */
    /**
     *  Feature is not supported by this PMIC.
     */
    PM_ERR_FLAG__FEATURE_NOT_SUPPORTED,
    PM_ERR_FLAG__INVALID_PMIC_MODEL,  /**< Invalid PMIC model. */
    PM_ERR_FLAG__SECURITY_ERR,        /**< Security error. */
    PM_ERR_FLAG__IRQ_LIST_ERR,        /**< IRQ list error. */
    PM_ERR_FLAG__DEV_MISMATCH,        /**< Device mismatch. */
    PM_ERR_FLAG__ADC_INVALID_RES,     /**< Invalid response from ADC. */
    PM_ERR_FLAG__ADC_NOT_READY,       /**< ADC is not ready. */
    PM_ERR_FLAG__ADC_SIG_NOT_SUPPORTED, /**< ADC signal is not supported. */
    /**
     *  Real-time Clock (RTC) displayed mode read from the PMIC is invalid.
     */
    PM_ERR_FLAG__RTC_BAD_DIS_MODE,
    /**
     *  Failed to read the time from the PMIC RTC.
     */
    PM_ERR_FLAG__RTC_READ_FAILED,
    /**
     *  Failed to write the time to the PMIC RTC.
     */
    PM_ERR_FLAG__RTC_WRITE_FAILED,
    /**
     *  RTC is not running.
     */
    PM_ERR_FLAG__RTC_HALTED,
    /**
     *  DBUS is already in use by another Multipurpose Pin (MPP).
     */
    PM_ERR_FLAG__DBUS_IS_BUSY_MODE,
    /**
     *  ABUS is already in use by another MPP.
     */
    PM_ERR_FLAG__ABUS_IS_BUSY_MODE,
    /**
     *  Value that is not in the macro_type enumeration was used.
     */
    PM_ERR_FLAG__MACRO_NOT_RECOGNIZED,
    /**
     *  Occurs if the data read from a register does
     *  not match the setting data.
     */
    PM_ERR_FLAG__DATA_VERIFY_FAILURE,
    /**
     *  Value that is not in the pm_register_type enumeration was used.
     */
    PM_ERR_FLAG__SETTING_TYPE_NOT_RECOGNIZED,
    /**
     * Value that is not in the pm_mode_group enumeration was used.
     */
    PM_ERR_FLAG__MODE_NOT_DEFINED_FOR_MODE_GROUP,
    /**
     *  Value that is not in the pm_mode enumeration was used.
     */
    PM_ERR_FLAG__MODE_GROUP_NOT_DEFINED,
    /**
     *  Occurs if the PRESTUB function returns FALSE.
     */
    PM_ERR_FLAG__PRESTUB_FAILURE,
    /**
     *  Occurs if the POSTSTUB function returns FALSE.
     */
    PM_ERR_FLAG__POSTSTUB_FAILURE,
    /**
     *  When modes are set for a mode group, they are recorded and
     *  checked for success.
     */
    PM_ERR_FLAG__MODE_NOT_RECORDED_CORRECTLY,
    /**
     *  Unable to find the mode group in the mode group recording
     *  structure. Fatal memory problem.
     */
    PM_ERR_FLAG__MODE_GROUP_STATE_NOT_FOUND,
    /**
     *  Occurs if the SUPERSTUB function returns FALSE.
     */
    PM_ERR_FLAG__SUPERSTUB_FAILURE,
    /**
     * Processor does not have access to this resource.
     */
   PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE,
    /**
     * Resource is invalid. Resource index was passed
     * into a router that is not defined in the build.
     */
   PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED,
    /**
     * Non-zero means an unsuccessful call. Here, it means the
     * registration failed because the driver ran out of
     * memory. MAX_CLIENTS_ALLOWED needs to be increased
     * and the code needs to be recompiled.
     */
    PM_ERR_FLAG__VBATT_CLIENT_TABLE_FULL,
    /**
     * One of the parameters to the function call was invalid.
     */
    PM_ERR_FLAG__VBATT_REG_PARAMS_WRONG,
    /**
     * Client could not be deregistered because, possibly, it does not exist.
     */
    PM_ERR_FLAG__VBATT_DEREGISTRATION_FAILED,
    /**
     * Client could not be modified because, possibly, it does not exist.
     */
    PM_ERR_FLAG__VBATT_MODIFICATION_FAILED,
    /**
     * Client could not be queried because, possibly, it does not exist.
     */
    PM_ERR_FLAG__VBATT_INTERROGATION_FAILED,
    /**
     * Client's filter could not be set because, possibly, it does not exist
     */
    PM_ERR_FLAG__VBATT_SET_FILTER_FAILED,
    /**
     * Keeps the count of all errors. Any error code equal to or greater
     * than this one means an unknown error. VBATT_LAST_ERROR should be
     * the last error code always with the highest value.
     */
    PM_ERR_FLAG__VBATT_LAST_ERROR,
   /**
     * PMIC is not supported.
     */
    PM_ERR_FLAG__PMIC_NOT_SUPPORTED,
    /**
     * Non-vibrator module is being indexed.
     */
    PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED ,
    /**
     * Non-PWM generator is being indexed.
     */
    PM_ERR_FLAG__INVALID_PWM_GENERATOR_INDEXED ,
    /**
     * Invalid peripheral is being indexed.
     */
    PM_ERR_FLAG__INVALID_CHG_INDEXED ,
    PM_ERR_FLAG__INVALID_CLK_INDEXED,   /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_XO_INDEXED,    /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_XOADC_INDEXED, /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_TCXO_INDEXED,  /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_RTC_INDEXED,   /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_SMBC_INDEXED,  /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__INVALID_BMS_INDEXED,   /**< Invalid peripheral is being indexed. */
    PM_ERR_FLAG__API_NOT_IMPLEMENTED,   /**< API is not implemented. */
    PM_ERR_FLAG__INVALID_PAONCNTRL_INDEXED, /**< Invalid peripheral is being indexed. */
    /**
     * Non-coincell module is being indexed.
     */
    PM_ERR_FLAG__INVALID_COINCELL_INDEXED ,
    /**
     * Non-Flash module is being indexed.
     */
    PM_ERR_FLAG__INVALID_FLASH_INDEXED ,
    /**
     * Non-OVP module is being indexed.
     */
    PM_ERR_FLAG__INVALID_OVP_INDEXED ,
    /**
     * Non-keypad module is being indexed.
     */
    PM_ERR_FLAG__INVALID_KEYPAD_INDEXED ,
    /**
     * Non-LVS module is being indexed.
     */
    PM_ERR_FLAG__INVALID_LVS_INDEXED ,
    /**
     * Non-HSED module is being indexed.
     */
    PM_ERR_FLAG__INVALID_HSED_INDEXED ,
    /**
     * Non-TALM module is being indexed.
     */
    PM_ERR_FLAG__INVALID_TALM_INDEXED ,
    /**
     * Non-NCP module is being indexed.
     */
    PM_ERR_FLAG__INVALID_NCP_INDEXED ,
    /**
     * Non-NFC module is being indexed.
     */
    PM_ERR_FLAG__INVALID_NFC_INDEXED ,
    /**
     * Non-MVS module is being indexed.
     */
    PM_ERR_FLAG__INVALID_MVS_INDEXED,
    /**
     * Non-HDMI module is being indexed.
     */
    PM_ERR_FLAG__INVALID_HDMI_INDEXED,
    /**
     * Non-VS module is being indexed.
     */
    PM_ERR_FLAG__INVALID_VS_INDEXED,
    /**
     * Non-UVLO module is being indexed.
     */
    PM_ERR_FLAG__INVALID_UVLO_INDEXED,
    /**
     * Invalid entity is being indexed.
     */
    PM_ERR_FLAG__INVALID_AMUX_INDEXED,
    PM_ERR_FLAG__INVALID_KEYPAD_EVENT_COUNTER, /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_MPP_INDEXED,          /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_BATTERY_CELL_NUMBER,  /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_PWRON_INDEXED,        /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_VBATT_INDEXED,        /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_HSDKYPD_APP_INDEXED,  /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_PWRKEY_APP_INDEXED,   /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_EVENT_CALLBACK,       /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__SHADOW_REGISTER_INIT_FAILED , /**< Shadow register initialization 
                                                    failed. */
    /**
     * PSDTE error functionality.
     */
    PM_ERR_FLAG__PSDTE_ENV_FAILURE,
    PM_ERR_FLAG__PSDTE_PMIC_POWERUP_FAILED,   /**< PSDTE error: Power-up failed. */
    PM_ERR_FLAG__PSDTE_PMIC_POWERDOWN_FAILED, /**< PSDTE error: Power-down failed. */
    /*
     * Miscellaneous functionality error.
     */
    PM_ERR_FLAG__FTS_CALCULATION_ERROR , /**< FTS calculation error. */
    PM_ERR_FLAG__API_DEPRICATED_ERROR,   /**< API is deprecated. */
    PM_ERR_FLAG__RPC_PROCESSOR_NOT_RECOGNIZED_ERROR, /**< RPC processing is not 
                                                          recognized. */
    /*
     * VREG error. 
     */
    PM_ERR_FLAG__VREG_VOTE_DEREGISTER_ERROR, /**< VREG vote deregister error. */
    PM_ERR_FLAG__VREG_VOTE_REGISTER_ERROR,   /**< VREG vote register error. */
    /**
     * VS error.
     */
    PM_ERR_FLAG__INVALID_VS_STATUS,
    /*
     * DAL service error.
     */
    PM_ERR_FLAG__DAL_SERVICE_REGISTRATION_FAILED, /**< DAL service registration 
                                                       failed. */
    PM_ERR_FLAG__DAL_SERVICE_FAILED,              /**< DAL service failed. */
    /*
     * MUTEX error.
     */
    PM_ERR__MUTEX_CREATION_FAILED , /**< MUTEX creation failed. */
    PM_ERR__MUTEX_RELEASE_FAILED ,  /**< MUTEX release failed. */
    PM_ERR__MUTEX_DELETION_FAILED , /**< MUTEX deletion failed. */
    PM_ERR__MUTEX_UNAVAILBLE ,      /**< MUTEX is unavailable. */
    /**
     * Returned if the Create Comm Channel fails to find the correct commType.
     */
    PM_ERR_FLAG__COMM_TYPE_NOT_RECOGNIZED,
    /**
     * IRQ GPIO set failed. @newpage 
     */
    PM_ERR_FLAG__IRQ_GPIO_SET_FAILED,
    /**
     * Event information table has run out of client entries.
     */
    PM_ERR_FLAG__PMAPP_EVENT_CLIENT_TABLE_FULL,
    /**
     * Client could not be deregistered because, possibly, it does not exist.
     */
    PM_ERR_FLAG__PMAPP_EVENT_DEREGISTRATION_FAILED,
    /**
     * Client is attempting to use a client index that is incorrect
     * or has been previously deregisterd.
     */
    PM_ERR_FLAG__PMAPP_EVENT_CLIENT_INDEX_FAILURE,
    /*
     * Invalid entity is being indexed.
     */
    PM_ERR_FLAG__INVALID_MISC_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_MISC_TBD,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_VREG_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_POINTER,
    /**< Invalid pointer. */
    PM_ERR_FLAG__FUNCTION_NOT_SUPPORTED_AT_THIS_LEVEL,
    /**< Function is not supported at this level. */
    PM_ERR_FLAG__INVALID_LPG_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_DISPBACKLIGHT_APP_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_BUA_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID,
    /**< Invalid entity. */
    PM_ERR_FLAG__INVALID_INT_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_BTM_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_ARB_ADC_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__BMS_INVALID_BATTERY_TEMPERATURE,
    /**< BMS invalid battery temperature. */
    PM_ERR_FLAG__BMS_INVALID_CHARGE_CYCLES,
    /**< BMS invalid charge cycles */
    PM_ERR_FLAG__INVALID_UICC_INDEXED,
    /**< Invalid entity is being indexed. */
    PM_ERR_FLAG__INVALID_MBG_INDEXED
    /**< Invalid entity is being indexed. */
} pm_err_flag_type;

/**
 * This macro is used to return the greater of two values provided 
 * as input parameters.
 */
#define PM_ERR_MAX(a, b) ((a > b) ? (a) : (b))
/** @} */ /* end_addtogroup pm_err_flags */

#endif    /* PM_ERR_FLAGS_H */

