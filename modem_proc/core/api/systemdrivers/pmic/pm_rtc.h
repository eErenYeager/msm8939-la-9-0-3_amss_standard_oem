#ifndef PM_RTC_H
#define PM_RTC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_rtc.h PMIC RTC related declaration.
 *
 * @brief This file contains enums and API definitions for Real Time Clock Driver.
 */

/*===========================================================================

           R T C   S E R V I C E S   H E A D E R   F I L E

DESCRIPTION
  This file contains functions prototypes and variable/type/constant 
  declarations for the RTC services developed for the Qualcomm Power
  Management IC.
  
  Copyright (c) 2003-2009, 2013 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/
/* ======================================================================= */

/* ==========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_rtc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/13   mr      (Tech Pubs) Edited/added Doxygen comments and markup (CR-522045)
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/23/08   jtn     Added API pm_hal_rtc_prescaler_reset()
06/18/07   cng     Added meta comments to miscellaneous RTC APIs
05/31/06   Vish    Fixed LINT warnings.
05/03/05   Vish    Modified pm_hal_rtc_get_time() to read RTC time even in
                   the case when RTC was not running.
01/19/05   Vish    Updated function header for pm_hal_rtc_get_time().
01/28/04   rmd     Added initial support for multiple PMIC models/tiers.
11/07/03   Vish    Added the task of clearing the alarm trigger condition
                   within pm_hal_rtc_disable_alarm() so that the master
                   RTC alarm interrupt could be cleared afterward.
10/02/03   Vish    Added pm_hal_rtc_get_alarm_time() and
                   pm_hal_rtc_get_alarm_status().
09/23/03   Vish    Changed all pm_rtc_xxx variables/fns to pm_hal_rtc_xxx.
09/13/03   Vish    Created.
===========================================================================*/

#include "comdef.h"
#include "pm_err_flags.h"
/*==========================================================================

                         TYPE DEFINITIONS
						 
========================================================================= */

/** @addtogroup pm_rtc 
@{ */
/** 
* @struct pm_hal_rtc_time_type
* @brief Used for setting and retrieving the current time and for 
*        setting the alarm time.
*/
typedef struct
{
   uint32  sec;  /*!< RTC time in seconds. */
} pm_hal_rtc_time_type;

/** 
* @enum pm_hal_rtc_alarm
* @brief Used for selecting the physical alarms available in the RTC.
*/
typedef enum
{
   PM_HAL_RTC_ALARM_1,       /*!< RTC alarm 1. */
   PM_HAL_RTC_ALARM_INVALID  /*!< Invalid alarm. */
} pm_hal_rtc_alarm;

/** 
* @struct pm_hal_rtc_alarm_status_type
* @brief Status of the various physical alarms available in the RTC.
*/
typedef struct
{
   boolean  alarm_1_triggered;  /*!< Alarm 1 is triggered. */
} pm_hal_rtc_alarm_status_type;


/*==========================================================================

                    RTC DRIVER FUNCTION PROTOTYPES

==========================================================================*/
/**
 * Starts the RTC ticking with the indicated time as its current (start-up) 
 * time.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_hal_rtc_time_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] start_time_ptr Pointer to the start-up time for the real-time 
 *                            clock. \n
 *                            A valid input is a valid non-NULL RTC time 
 *                            structure containing any 32-bit number indicating 
 *                            the number of seconds elapsed from a known point 
 *                            in time in history.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_start(), which is deprecated. 
 * Use pm_dev_hal_rtc_start() instead. 
*/
extern pm_err_flag_type pm_dev_hal_rtc_start(unsigned pmic_device_index,
                                             const pm_hal_rtc_time_type *start_time_ptr);
extern pm_err_flag_type pm_hal_rtc_start(const pm_hal_rtc_time_type *start_time_ptr);

/**
 * Stops the RTC from ticking.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_stop(), which is deprecated. 
 * Use pm_dev_hal_rtc_stop() instead. 
*/
extern pm_err_flag_type pm_dev_hal_rtc_stop(unsigned pmic_device_index);
extern pm_err_flag_type pm_hal_rtc_stop(void);

/**
 * Gets the RTC time.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_hal_rtc_time_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [out] time_ptr Pointer to the time structure for the RTC.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_get_time(), which is deprecated. 
 * Use pm_dev_hal_rtc_get_time() instead. 
*/
extern pm_err_flag_type pm_dev_hal_rtc_get_time(unsigned pmic_device_index,
                                                pm_hal_rtc_time_type *time_ptr);
extern pm_err_flag_type pm_hal_rtc_get_time(pm_hal_rtc_time_type *time_ptr);

/**
 * Enables the RTC alarm.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_hal_rtc_alarm \n
 * #pm_hal_rtc_time_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] what_alarm Alarm to be turned ON.
 * @param [in] trigger_time_ptr Pointer to the time at which the alarm is 
 *                              to go off. \n
 *                              A valid input is a valid non-NULL RTC time 
 *                              structure containing any 32-bit value 
 *                              indicating the number of seconds that have 
 *                              elapsed (in relation to a known point in time 
 *                              in history, as specified in the previous 
 *                              pm_hal_rtc_start() call) exactly at the moment 
 *                              the alarm is expected to go off.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() and pm_hal_rtc_start() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_enable_alarm(), which is deprecated. 
 * Use pm_dev_hal_rtc_enable_alarm() instead. 
*/
extern pm_err_flag_type pm_dev_hal_rtc_enable_alarm(unsigned pmic_device_index,
                                                    pm_hal_rtc_alarm what_alarm,
                                                    const pm_hal_rtc_time_type *trigger_time_ptr);
extern pm_err_flag_type pm_hal_rtc_enable_alarm(pm_hal_rtc_alarm what_alarm,
                                                const pm_hal_rtc_time_type *trigger_time_ptr);

/**
 * Disables the specified alarm so that it does not go off in the future.
 * If the alarm has already been triggered by the time this function
 * is called, it also clears the alarm trigger condition so the
 * master RTC alarm interrupt can be cleared afterward.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_hal_rtc_alarm
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] what_alarm Alarm to be turned OFF.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_disable_alarm(), which is deprecated. 
 * Use pm_dev_hal_rtc_disable_alarm() instead.
*/
extern pm_err_flag_type pm_dev_hal_rtc_disable_alarm(unsigned pmic_device_index,
                                                     pm_hal_rtc_alarm  what_alarm);
extern pm_err_flag_type pm_hal_rtc_disable_alarm(pm_hal_rtc_alarm  what_alarm);

/**
 * Gets the alarm time.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @datatypes
 * #pm_hal_rtc_alarm \n
 * #pm_hal_rtc_time_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] what_alarm Indicates the alarm to be read.
 * @param [out] alarm_time_ptr Pointer to the time structure that is used by
 *                             this function to return the time at which the
 *                             specified alarm has been programmed to trigger.
 *                             A valid input is any valid non-NULL RTC time 
 *                             structure.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_get_alarm_time(), which is deprecated. 
 * Use pm_dev_hal_rtc_get_alarm_time() instead.
*/
extern pm_err_flag_type
pm_dev_hal_rtc_get_alarm_time(unsigned pmic_device_index,
                              pm_hal_rtc_alarm what_alarm,
                              pm_hal_rtc_time_type  *alarm_time_ptr);
extern pm_err_flag_type
pm_hal_rtc_get_alarm_time(pm_hal_rtc_alarm what_alarm,
                          pm_hal_rtc_time_type  *alarm_time_ptr);

/**
 * Gets the RTC alarm time status.
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [out] status_ptr Pointer to a valid uint8 value used for returning
 *                         the status information for the various alarms
 *                         (currently, only one alarm is valid: 
 *                         PM_HAL_RTC_ALARM_1).
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_get_alarm_status(), which is deprecated. 
 * Use pm_dev_hal_rtc_get_alarm_status() instead.
*/
extern pm_err_flag_type pm_dev_hal_rtc_get_alarm_status(unsigned pmic_device_index,
                                                        uint8 *status_ptr);
extern pm_err_flag_type
pm_hal_rtc_get_alarm_status(uint8 *status_ptr);

/**
 * Sets the time adjustment correction factor for an RTC crystal
 * oscillator that is slightly off the 32768 Hz frequency. Every
 * 10th second, the frequency divider is switched from the nominal
 * 32768 to (32768 - 64 + time_adjust).
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] time_adjust Adjustment factor. Valid input:
 *				           - 0 to 63 -- Compensates for a slower crystal 
 *                           oscillator
 *				           - 64 -- Provides no compensation
 *				           - 65 to 127 -- Compensates for a faster crystal 
 *                           oscillator @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_set_time_adjust(), which is deprecated. 
 * Use pm_dev_hal_rtc_set_time_adjust() instead.
*/
extern pm_err_flag_type
pm_dev_hal_rtc_set_time_adjust(unsigned pmic_device_index,
                               uint8 time_adjust);
extern pm_err_flag_type
pm_hal_rtc_set_time_adjust(uint8 time_adjust);

/**
 * Returns the current time adjustment correction factor in use, as
 * set by the previous pm_hal_rtc_set_time_adjust().
 *
 * @note1hang Interrupts are disabled during this function.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [out] time_adjust_ptr Pointer to the adjustment factor A valid 
 *                              input is a non-NULL pointer to a valid uint8 
 *                              containing: 
 *                              - 0 to 63 -- Compensation for a slower crystal 
 *                                oscillator
 *                              - 64 -- No compensation
 *                              - 65 to 127 -- Compensation for a faster crystal 
 *                                oscillator @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @dependencies
 * pm_init() and pm_hal_rtc_set_time_adjust() must have been called.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_get_time_adjust(), which is deprecated. 
 * Use pm_dev_hal_rtc_get_time_adjust() instead.
*/
extern pm_err_flag_type
pm_dev_hal_rtc_get_time_adjust(unsigned pmic_device_index,
                               uint8 *time_adjust_ptr);
extern pm_err_flag_type
pm_hal_rtc_get_time_adjust(uint8 *time_adjust_ptr);

/**
 * Resets the 1/32768 prescalar in the RTC module.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
/* Note: Do not use pm_hal_rtc_prescaler_reset(), which is deprecated. 
 * Use pm_dev_hal_rtc_prescaler_reset() instead.
*/
extern pm_err_flag_type pm_dev_hal_rtc_prescaler_reset(unsigned pmic_device_index);
extern pm_err_flag_type pm_hal_rtc_prescaler_reset(void);

/**
 * Initiates a hard reset using the RTC module.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- Success. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 * 
 * @newpage 
 */
extern pm_err_flag_type pm_dev_hard_reset(unsigned pmic_device_index);
/** @} */ /* end_addtogroup pm_rtc */

#endif    /* PM_RTC_H */

