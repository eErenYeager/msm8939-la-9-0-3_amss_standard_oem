#ifndef PM_BUA_H
#define PM_BUA_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_bua.h BATTERY UICC ALARM related declaration.
 *
 * @brief This header file contains API and type definitions for BUA driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_bua.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/31/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
06/16/13   kt      Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "com_dtypes.h"
#include "pm_irq.h"

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_bua 
@{ */
/**
 * @enum pm_bua_alarm_type
 * @brief BUA alarms that are supported across targets.
 */
typedef enum
{
  PM_BUA_BATT_ALARM,   /**< Battery alarm. */
  PM_BUA_UICC1_ALARM,  /**< UICC1 alarm. */
  PM_BUA_UICC2_ALARM,  /**< UICC2 alarm. */
  PM_BUA_UICC3_ALARM,  /**< UICC3 alarm. */
  PM_BUA_UICC4_ALARM,  /**< UICC4 alarm. */
  PM_BUA_INVALID_ALARM /**< Invalid alarm. */
}pm_bua_alarm_type;

/*===========================================================================

                    BUA DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Enables or disables the BUA.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] enable Whether to enable.
 *                    - TRUE -- Enable BUA
 *                    - FALSE -- Disable BUA @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_enable(uint8 pmic_device_index, boolean enable);

/**
 * Returns the BUA enable/disable status (BUA_OK status).
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [out] status Status.
 *                     - TRUE -- BUA is enabled
 *                     - FALSE -- BUA is disabled @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_enable_status(uint8 pmic_device_index, boolean *status);

/**
 * Returns the BUA alarm detected status.
 * The BATT_GONE status is for the BATT_ALARM type and the
 * UICCx_ALARM_DETECTED status is for the UICCx_ALARM type.
 *
 * @datatypes
 * #pm_bua_alarm_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type.
 * @param [out] status Status.
 *                     - TRUE -- BUA alarm is detected
 *                     - FALSE -- BUA alarm is not detected @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_alarm_detect_status(uint8 pmic_device_index,
                                            pm_bua_alarm_type alarm,
                                            boolean *status);

/**
 * Enables or disables the BUA alarm IRQ.
 *
 * @datatypes
 * #pm_bua_alarm_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type.
 * @param [in] enable Whether to enable. \n
 *                    - TRUE -- Enable the BUA alarm interrupt
 *                    - FALSE -- Disable the BUA alarm interrupt @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_irq_enable(uint8 pmic_device_index,
                                   pm_bua_alarm_type alarm,
                                   boolean enable);

/**
 * Clears the BUA alarm IRQ.
 *
 * @datatypes
 * #pm_bua_alarm_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_irq_clear(uint8 pmic_device_index,
                                  pm_bua_alarm_type alarm);

/**
 * Configures the BUA alarm IRQ trigger type.
 *
 * @datatypes
 * #pm_bua_alarm_type \n
 * #pm_irq_trigger_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type.
 * @param [in] trigger IRQ trigger type.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_irq_set_trigger(uint8 pmic_device_index,
                                        pm_bua_alarm_type alarm,
                                        pm_irq_trigger_type trigger);

/**
 * Gets the BUA alarm IRQ trigger type.
 *
 * @datatypes
 * #pm_bua_alarm_type \n
 * #pm_irq_trigger_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type. 
 * @param [out] trigger IRQ trigger type set for the BUA alarm IRQ.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_irq_get_trigger(uint8 pmic_device_index,
                                        pm_bua_alarm_type alarm,
                                        pm_irq_trigger_type *trigger);

/**
 * Returns the BUA alarm IRQ status.
 *
 * @datatypes
 * #pm_bua_alarm_type \n
 * #pm_irq_trigger_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1.
 * @param [in] alarm BUA alarm type.
 * @param [in] type Type of IRQ status to read.
 *
 * @param [out] status IRQ status.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * SPMI errors; see #pm_err_flag_type for descriptions.
 *
 * @newpage 
 */
pm_err_flag_type pm_bua_irq_status(uint8 pmic_device_index,
                                   pm_bua_alarm_type alarm,
                                   pm_irq_status_type type,
                                   boolean *status);
/** @} */ /* end_addtogroup pm_bua */

#endif /* PM_BUA_H */
