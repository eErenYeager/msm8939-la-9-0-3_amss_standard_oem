#ifndef PM_BOOT_H
#define PM_BOOT_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_boot.h PMIC BOOT related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC boot.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_boot.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
09/21/11   hs      Added support for TZ(Trust Zone).
04/21/11   mpt     initial version
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                 BOOT FUNCTION PROTOTYPES

===========================================================================*/

/** @addtogroup pm_boot 
@{ */
/**
 * Executes the PMIC device initialization.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 */
 pm_err_flag_type pm_device_init( void );

/**
 * Executes the PMIC device initialization in the ARM\regns TrustZone\regns 
 * software, such as initializing the Singlewire Serial Bus Interface (SSBI) 
 * module, etc.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 */
pm_err_flag_type pm_tz_device_init( void );

/**
 * Enables the power rail for the QFPROM block.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 */
pm_err_flag_type pm_enable_qfuse_power_rail(void);

/**
 * Disables the power rail for the QFPROM block.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 */
pm_err_flag_type pm_disable_qfuse_power_rail(void);
/** @} */ /* end_addtogroup pm_boot */

#endif /* PM_BOOT_H */
