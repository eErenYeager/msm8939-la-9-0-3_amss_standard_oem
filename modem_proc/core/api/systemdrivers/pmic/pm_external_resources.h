#ifndef PM_EXTERNAL_RESOURCES_H
#define PM_EXTERNAL_RESOURCES_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_external_resources.h PMIC External Resources header file.
 *
 * @brief This file contains enumerations with resource names for different
 *        module types that should be used by external clients when accessing
 *        the resources that are required.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_external_resources.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
01/10/13   kt      Removed Power Rail IDs.
09/29/11   hs      Added CLK resource IDs.
09/14/11   dy      Move UICC resource IDs to this file
04/25/11   wra     Adding Battery Temperature Management BTM, LPG, more LVS, ADC_ARB,
                    OVP & INTerrupt channel enumeration needed for PM8921
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/05/10   wra     Modified Header includes to accomodate MSM8660
=============================================================================*/

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_resources_and_types 
@{ */

/**
 * @anonenum{Chargers}
 * Definitions of all chargers in the target.
 */
enum
{
  PM_CHG_1,      /**< Charger 1. */
  PM_CHG_INVALID /**< Charger is invalid. */
};

/**
 * @anonenum{High Current LEDs}
 * Definitions of all high current LED resources in the target.
 */
enum
{
  PM_LEDH_1,      /**< High current LED 1. */
  PM_LEDH_2,      /**< High current LED 2. */
  PM_LEDH_3,      /**< High current LED 3. */
  PM_LEDH_INVALID /**< High current LED is invalid. */
};

/**
 * @anonenum{Low Current LEDs}
 * Definitions of all low current LED resources in the target.
 */
enum
{
  PM_LEDL_1,      /**< Low current LED 1. */
  PM_LEDL_2,      /**< Low current LED 2. */
  PM_LEDL_3,      /**< Low current LED 3. */
  PM_LEDL_INVALID /**< Low current LED is invalid. */
};

/**
 * @anonenum{PWM Resources}
 * Definitions of all PWM resources in the target.
 */
enum
{
  PM_PWM_1,      /**< PWM 1. */
  PM_PWM_2,      /**< PWM 2. */
  PM_PWM_3,      /**< PWM 3. */
  PM_PWM_INVALID /**< PWM is invalid. @newpage */
};

/** @cond */
/**
 * @anonenum{HSED Resources}
 * Definitions of all HSED resources in the target.
 */
enum
{    
  PM_HSED_1,      /**< HSED 1. */
  PM_HSED_2,      /**< HSED 2. */
  PM_HSED_3,      /**< HSED 3. */
  PM_HSED_INVALID /**< HSED is invalid. */
};
/** @endcond */

/**
 * @anonenum{External Charging Hardware Resources}
 * Definitions of all external charging hardware
 * resources in the target.
 */
enum
{
  PM_CHG_EXT_1,      /**< External charging hardware 1. */
  PM_CHG_EXT_INVALID /**< External charging hardware is invalid. */
}; 

/** @cond */
/**
 * @anonenum{SMBC Resources}
 * Definitions of all Switched-mode Battery Charger (SMBC) 
 *        resources in the target.
 */
enum
{
  PM_SMBC_1,      /**< SMBC 1. */
  PM_SMBC_INVALID /**< SMBC is invalid. */
};
/** @endcond */

/**
 * @anonenum{BMS Resources}
 * Definitions of all Battery Management System (BMS)
 * resources in the target.
 */
enum
{
  PM_BMS_1,      /**< BMS 1. */
  PM_BMS_INVALID /**< BMS is invalid. */
};

/**
 * @anonenum{BTM Resources}
 * Definitions of all Battery Temperature Management (BTM)
 * resources in the target.
 */
enum
{
  PM_BTM_1,      /**< BTM 1. */
  PM_BTM_INVALID /**< BTM is invalid. */
};

/**
 * @anonenum{ARB_ADC Resources}
 * Definitions of all ADC Arbiter (ARB_ADC) channel resources in the 
 * target. Typically, a processor owns a channel and the arbiter 
 * provides equal access to the ADC for each processor.
 */
enum
{
    PM_ARB_ADC_1,      /**< ARB_ADC 1. */
    PM_ARB_ADC_2,      /**< ARB_ADC 2. */
    PM_ARB_ADC_3,      /**< ARB_ADC 3. */
    PM_ARB_ADC_INVALID /**< ARB_ADC is invalid. */
};

/**
 * @anonenum{LPG Resources}
 * Definitions of all Light Pulse Generator (LPG) resources in PMIC.
 */
enum
{
    PM_LPG_1,      /**< LPG 1. */
    PM_LPG_2,      /**< LPG 2. */
    PM_LPG_3,      /**< LPG 3. */
    PM_LPG_4,      /**< LPG 4. */
    PM_LPG_5,      /**< LPG 5. */
    PM_LPG_6,      /**< LPG 6. */
    PM_LPG_7,      /**< LPG 7. */
    PM_LPG_8,      /**< LPG 8. */
    PM_LPG_INVALID /**< LPG is invalid. */
};

/**
 * @anonenum{Interrupt Resources}
 * Definitions of the interrupt resources available on PMIC.
 */
enum
{
    PM_INTERRUPT_1,      /**< Interrupt 1. */ //RPM/SEC
    PM_INTERRUPT_2,      /**< Interrupt 2. */ //MDM
    PM_INTERRUPT_3,      /**< Interrupt 3. */ //APP/USER
    PM_INTERRUPT_INVALID /**< Interrupt is invalid. */
};

/**
 * @anonenum{OVP Resources}
 * Definitions of all Over Voltage Protection (OVP) resources available on 
 * the PMIC component.
 */
enum
{
    PM_OVP_1,      /**< OVP 1. */
    PM_OVP_2,      /**< OVP 2. */
    PM_OVP_INVALID /**< OVP is invalid. */
};

/**
 * @anonenum{Mega XO Resources}
 * Definitions of the Mega XO resources available on the PMIC component.
 */
enum
{
    PM_MEGA_XO_1,       /**< Mega XO 1. */
    PM_MEGA_XO_2,       /**< Mega XO 2. */
    PM_MEGA_XO_3,       /**< Mega XO 3. */
    PM_MEGA_XO_4,       /**< Mega XO 4. */
    PM_MEGA_XO_5,       /**< Mega XO 5. */
    PM_MEGA_XO_6,       /**< Mega XO 6. */
    PM_MEGA_XO_7,       /**< Mega XO 7. */
    PM_MEGA_XO_8,       /**< Mega XO 8. */
    PM_MEGA_XO_9,       /**< Mega XO 9. */
    PM_MEGA_XO_INVALID, /**< Mega XO is invalid. @newpage */
};

/**
 * @anonenum{UICC resources}
 * Definitions of the UICC resources available on the PMIC component.
 */
enum
{
  PM_BATTERY_ALARM, /**< Battery alarm. */
  PM_UICC_1,        /**< UICC 1. */
  PM_UICC_2,        /**< UICC 2. */
  PM_UICC_3,        /**< UICC 3. */
  PM_UICC_INVALID  /**< UICC is invalid. */
};


/** @} */ /* end_addtogroup pm_resources_and_types */

#endif    /* PM_EXTERNAL_RESOURCES_H */

