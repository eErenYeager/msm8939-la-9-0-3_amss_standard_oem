#ifndef PM_BOOT_CHG_H
#define PM_BOOT_CHG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */
 
/**
 * @file pm_boot_chg.h PMIC BOOT-CHG related declaration.
 * 
 * @brief This header file contains functions and variable declarations
 *        to support Qualcomm boot charging.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_boot_chg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
04/21/11   hs      Initial version.
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_boot_chg 
@{ */
/**
 * @enum pm_boot_chg_led_source_type
 * @brief Power source for the anode of the change indicator LED.
 */
typedef enum
{
   PM_BOOT_CHG_LED_SRC__GROUND,  /*!< Ground. */
   PM_BOOT_CHG_LED_SRC__VPH_PWR, /*!< VPH_PWR. */
   PM_BOOT_CHG_LED_SRC__BOOST,   /*!< Boost. */
   PM_BOOT_CHG_LED_SRC__INTERNAL /*!< Internal source. */
}pm_boot_chg_led_source_type;

/**
 * @enum pm_boot_chg_attached_chgr_type
 * @brief Attached charger type.
 */
typedef enum
{
   PM_BOOT_CHG_ATTACHED_CHGR__NONE, /*!< No charger. */
   PM_BOOT_CHG_ATTACHED_CHGR__USB,  /*!< USB path charger is attached. */
   PM_BOOT_CHG_ATTACHED_CHGR__DCIN, /*!< DCIN path charger is attached. */
   PM_BOOT_CHG_ATTACHED_CHGR__BOTH  /*!< Both USB and DCIN are attached. */
}pm_boot_chg_attached_chgr_type;

/**
 * @enum pm_boot_chg_mode_type
 * @brief Charging mode.
 */
typedef enum
{
   PM_BOOT_CHG_MODE__CHARGE_OFF, /*!< Charge Off. */
   PM_BOOT_CHG_MODE__CHARGE_ON,  /*!< Charge On. */
   PM_BOOT_CHG_MODE__ATC_OFF,    /*!< ATC Force Off. */
   PM_BOOT_CHG_MODE__ATC_ON      /*!< ATC Force On. */
}pm_boot_chg_mode_type;


/*===========================================================================

                 FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Sets the battery alarm thresholds for a weak battery and a dead battery.
 *
 * @param [in] dead_battery_threshold 
 *             Voltage threshold in millivolts. When the battery is below 
 *             this value, the dead battery alarm is triggered. \n
 *      @note1 If this value is out of the valid range that is acceptable by 
 *             the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time, 
 *             PM_ERR_FLAG__PAR1_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mV value parameter input.
 * @param [in] weak_battery_threshold 
 *             Voltage threshold in millivolts. When the battery is below 
 *             this value, the weak battery alarm is triggered. \n
 *      @note1 If this value is out of the valid range that is acceptable by
 *             the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time, 
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mV value parameter input.
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @dependencies
 * The battery alarm (if applicable) will be enabled in the same function after 
 * the battery thresholds are set.
 *
 * @newpage 
 */
pm_err_flag_type pm_boot_chg_set_battery_thresholds(uint32 dead_battery_threshold,
                                                    uint32 weak_battery_threshold);

/**
 * Checks whether the battery is weak.
 *
 * @param [out] weak Whether the battery is weak.
 *                   - TRUE -- Battery voltage is below the weak battery threshold
 *                   - FALSE -- Battery voltage is above the weak battery threshold @tablebulletend 
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 *
 */
pm_err_flag_type pm_boot_chg_get_weak_battery_status(boolean *weak);

/**
 * Checks whether the battery is present.
 *
 * @param [out] present Whether the battery is present.
 *                      - TRUE -- Battery is present
 *                      - FALSE -- Battery is not present @tablebulletend 
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_get_battery_present(boolean *present);

/**
 * Gets the attached charger type.
 *
 * @datatypes
 * #pm_boot_chg_attached_chgr_type
 *
 * @param [out] type Attached charger type.
 *              - PM_BOOT_CHG_ATTACHED_CHGR__NONE -- No charger is attached
 *              - PM_BOOT_CHG_ATTACHED_CHGR__USB -- USB device is attached
 *              - PM_BOOT_CHG_ATTACHED_CHGR__DCIN -- DC wall charger is attached
 *              - PM_BOOT_CHG_ATTACHED_CHGR__BOTH -- Both USB device and DC wall 
                                                     charger are attached @tablebulletend 
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_get_attached_charger(pm_boot_chg_attached_chgr_type *type);

/**
 * Sets the maximum USB charge current. The USB charge current is
 * the current going through the USB cable.
 *
 * @note1hang The default setting in the PMIC hardware is 100 mA.
 *
 * @param [in] value_ma Current value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable by
 *             the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time, 
 *             PM_BOOT_CHG_ERROR_TYPE__INVALID_PARAMETER is returned to 
 *             indicate there was an out-of-bounds mV value parameter input.
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_set_iusb_max(uint32 value_ma);

/**
 * Sets the maximum fast charge battery current limit (IBAT).
 *
 * @note1hang The default setting in the PMIC hardware is 325 mA.
 *
 * @param [in] value_ma Maximum IBAT value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable by
 *             the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time, 
 *             PM_BOOT_CHG_ERROR_TYPE__INVALID_PARAMETER is returned to 
 *             indicate there was an out-of-bounds mA value parameter input.
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_set_ibat_max(uint32 value_ma);

/**
 * Sets the system trickle charging current.
 *
 * @note The default setting in the PMIC hardware is 50 mA.
 *
 * @param [in] value_ma System trickle charging current value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable by
 *             the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time, 
 *             PM_BOOT_CHG_ERROR_TYPE__INVALID_PARAMETER is returned to 
 *             indicate there was an out-of-bounds mA value parameter input.
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_set_itrkl(uint32 value_ma);

/**
 * Sets the charge mode, such as enable charging and disable charging, etc.
 *
 * @brief This function sets the charge mode, such as enabling
 *    	  charging and disable charging, etc.
 *
 * @note The default setting in the PMIC hardware is PM_SMBC_MODE__CHARGE_OFF.
 *
 * @param [in] mode Set the specific mode in the Boot Charging module.
 *                  - PM_BOOT_CHG_MODE__CHARGE_OFF -- Sets the BOOT-CHG hardware
 *                    state machine in the Charge Off state.
 *                  - PM_BOOT_CHG_MODE__CHARGE_ON -- Sets the BOOT-CHG hardware
 *                    state machine in the Charge On state.
 *                  - PM_BOOT_CHG_MODE__ATC_OFF -- Sets the BOOT-CHG hardware
 *                    state machine in the ATC Off state; ATC is auto trickle 
 *                    charging.
 *                  - PM_BOOT_CHG_MODE__ATC_ON -- Sets the BOOT-CHG hardware
 *                    state machine in the ATC On state. @tablebulletend 
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_set_mode(pm_boot_chg_mode_type mode);

/**
 * Sets the charge LED and the LED source, if applicable. This function also 
 * sets the system trickle charging current.
 *
 * @note1hang The default setting in the PMIC hardware is 50 mA.
 *
 * @datatypes
 * #pm_boot_chg_led_source_type
 *
 *
 * @param [in] enable Enables/disables the charger LED.
 * @param [in] source Sets the charger LED source, if applicable. \n
 *      @note1 This parameter is ignored if the PMIC does not have a dedicated 
 *             LED source for boot charging.
 *
 * @return
 * See #pm_err_flag_type for descriptions.
 *
 * @newpage 
 *
 */
pm_err_flag_type pm_boot_chg_enable_led (boolean enable, pm_boot_chg_led_source_type source);
/** @} */ /* end_addtogroup pm_boot_chg */

#endif /* PM_BOOT_CHG_H */

