#ifndef VBATT_H
#define VBATT_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file vbatt.h BATTERY LEVEL Utilities related declaration.
 *
 * @brief This header file contains all of the definitions necessary for
 *		  other tasks to interface with the battery level utilities.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/vbatt.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
11/02/10   dy      Expanded battery_adc_params_type struct
12/10/09   jtn     Remove featurization for CMI
10/06/09   jtn     Make changes needed for DAL ADC interface.
05/28/09   APU     Introduced an API to read battery temperature.
03/11/09   APU     Added APIs to support the new VBATT info structure and 
                   also support the CHG callback functionality.
12/25/08   APU     Introduced the API: vbatt_read_mv.  
11/14/03   Vish    Added vbatt_get_adc_count_range_cal_info() for retrieving
                   ADC count range calibration info needed by other drivers
                   like ADC. 
08/16/02   rmd     Added the PMIC low battery alarm.
10/06/00   dsb     Removed TG== code.
09/11/98   aks     Added definitions for dual-battery (5GP) gauging support.
12/08/97   aks     Added definitions for internal charger and multiple battery
                   capability.
11/02/97   BK      Added missing T_Q.
09/17/97   aks     Improved overall precision and temperature compensation.  Power
                   drop and non-linear thermistor compensation added.
08/15/97   aks     Made vbatt_read_gauge() defined only for TGP targets.
08/13/97   aks     Added the externalized vbatt_read_gauge() function and
                   associated return type definitions.
03/01/95   tst     Changed to use VBATT_SCALED_MIN instead of 0.
02/14/95   tst     Added VBATT_SCALED_MIN.
02/04/93   jah     Changed vbatt_calibrate() to take byte inputs.
12/17/92   jah     Created

===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"


/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/** @addtogroup vbatt 
@{ */

/**
 * Minimum value of VBATT.
 */
#define VBATT_SCALED_MIN   0   
/**
 * Maximum value of VBATT.
 */
#define VBATT_SCALE      255

/**
 * @struct battery_adc_params_type
 * @brief The following information from battery_adc_params_type is not provided
 *        by the PMIC. It is intended for customers to provide this information
 *        if they are using an external battery management IC.
 */
typedef struct
{
    uint16 current_batt_adc_voltage ;  /*!< Current ADC read of the battery voltage. */
    uint16 previous_batt_adc_voltage ; /*!< Previous ADC read of the battery voltage. */
    int16  batt_temperature ;          /*!< Battery temperature. */
    uint16 batt_id ;                   /*!< Battery ID. */
    int32  pmic_temperature;           /*!< PMIC temperature. */
    int32  ichg_out_mv;                /*!< Battery current charge out. */
    int32  ichg_mv;                    /*!< Battery current charge. */
    int32  vchg_mv;                    /*!< Battery charge in millivolts. */
    int32  chg_temperature;            /*!< Battery charging temperature. */
    int32  usb_vbus;                   /*!< Status of USB Vbus. */
    uint8  current_batt_adc_raw;       /*!< Raw ADC reading of the battery current. */
    uint8  ACLineStatus;               /*!< AC line status. */
    uint8  BatteryFlag;                /*!< Battery flag. */
    uint8  BatteryLifePercent;         /*!< Life percent of the battery. */
    int32  BatteryLifeTime;            /*!< Lifetime of the battery. */
    int32  BatteryFullLifeTime;        /*!< Full lifetime of the battery. */
    uint8  BackupBatteryFlag;          /*!< Battery backup flag. */
    uint8  BackupBatteryLifePercent;   /*!< Life percent of the battery backup. */
    int32  BackupBatteryLifeTime;      /*!< Lifetime of the battery backup.  */
    int32  BackupBatteryFullLifeTime;  /*!< Full lifetime of the battery backup. */
    int32  BatteryCurrent;             /*!< Current drawn from the battery. */
    int32  BatteryAverageCurrent;      /*!< Battery's average current. */
    int32  BatteryAverageInterval;     /*!< Average of the battery's internal voltage. */
    int32  BatterymAHourConsumed;      /*!< mAH consumed from the battery. */
    int32  BackupBatteryVoltage;       /*!< Battery's backup voltage. */
    uint8  BatteryChemistry;           /*!< Battery chemistry. */
} battery_adc_params_type ;


/*===========================================================================

                    VBAT DRIVER FUNCTION DECLARATIONS

===========================================================================*/
/**
 * Reads the current battery level.
 *
 * @return
 * A scaled version of the battery level, where empty is
 * VBATT_SCALED_MIN and full is VBATT_SCALE.
 */
byte vbatt_read( void );

/**
 * Reads the current battery level.
 *
 * @return
 * Battery voltage in millivolts.
 */
uint16 vbatt_read_mv ( void ) ;

/**
 * Calibrates the battery level reading returned by
 * vbatt_read(). This routine needs to be called only once. This
 * calibration is to correct for the error in the resisters used in
 * the voltage divider at the input to the ADC.
 *
 * @param [in] min Value returned by ADC when the battery is at the minimum
 *                 level at which the phone can operate. Any safety margin
 *                 added to this measurement should be done by the calibration
 *                 to provide the correction at the lowest level.
 * @param [in] max Value returned by ADC when the battery is at the fully
 *                 charged level. Any ``show full when it is at maximum charge''
 *                 margin added to this measurement should be done by the
 *                 calibration to provide the correction at the lowest level.
 *
 * @return
 * None.
 */
void vbatt_calibrate(byte min, byte max);

/**
 * Determines whether the low battery alarm was triggered.
 *
 * @return
 * TRUE if triggered; otherwise, FALSE.
 */
boolean vbatt_is_low_batt_alarm_triggered(void);

/**
 * Gets information on the calibrated ADC counts for
 * the minimum and maximum valid main battery voltage.
 *
 * @param [out] min_count_ptr Minimum ADC count valid main battery voltage.
 * @param [out] max_count_ptr Maximum ADC count valid main battery voltage.
 *
 * @return
 * TRUE if valid calibration data is available; otherwise, FALSE.
 */
boolean vbatt_get_adc_count_range_cal_info(uint8  *min_count_ptr,
											uint8  *max_count_ptr);
   
/**
 * Updates various battery parameters.
 *
 * @return
 * None. 
 *
 * @dependencies
 * Underlying ADC channel should be operative.
 */
void vbatt_update_adc_battery_params ( void ) ;

/**
 * Gets information about various battery parameters.
 *
 * @param [out] batt_ptr Gets the ADC parameter of the battery.
 *
 * @return
 * None. 
 *
 * @dependencies
 * Underlying ADC channel should be operative.
 */
void vbatt_query_batt_adc_params ( battery_adc_params_type *batt_ptr ) ;

/**
 * Reads the battery temperature.
 *
 * @return
 * Battery temperature.
 */
int16 vbatt_read_temperature ( void ) ;

/**
 * Reads the PMIC temperature.
 *
 * @return
 * PMIC temperature.
 */
int32 vbatt_read_pmic_temperature(void);

/**
 * Reads the handset supply voltage (Vdd) determining the current leaving 
 * the battery.
 *
 * @return
 * Handset supply voltage in millivolts.
 */
int32 vbatt_read_ichg_out_mv(void);

/**
 * Reads the charger temperature.
 *
 * @return
 * Charger temperature.
 */
int32 vbatt_read_chg_temperature(void);

/**
 * Reads the USB Vbus voltage.
 *
 * @return
 * USB Vbus voltage.
 */
int32 vbatt_read_usb_vbus(void);

/**
 * Reads the voltage across the charger current sense resistor.
 *
 * @return
 * Voltage in millivolts.
 */
int32 vbatt_read_ichg_mv(void);

/**
 * Reads the battery ID voltage.
 *
 * @return
 * Battery ID voltage.
 */
int32 vbatt_read_batt_id(void);

/**
 * Reads the battery voltage.
 *
 * @return
 * Battery voltage in millivolts.
 *
 * @newpage 
 */
int32 vbatt_read_vchg_mv(void);
/** @} */ /* end_addtogroup vbatt */

#endif    /* VBATT_H */


