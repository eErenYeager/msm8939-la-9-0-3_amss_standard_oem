#ifndef PM_PWRON_H
#define PM_PWRON_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_pwron.h PMIC-PWRON Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the power on driver module.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_pwron.h#1 $
$DateTime: 2015/01/27 06:04:57 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   cm      (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
04/09/12   umr     Add PON Reason API for Badger PMIC family.
01/12/11   tdh     Added badger enumeration types
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
02/07/11   jtn     Change deprecation of enum to fix compiler warnings
01/17/11   jtn     Deprecate pm_pwr_key_delay_type and pm_power_key_setup().
                   Use pm_pwr_key_delay_set(uint32 microseconds) and
                   pm_err_flag_type pm_pwr_key_pullup_set(boolean enable) instead
07/19/10   cwg     Added support for PMIC stay on bit
07/09/10   jtn     Add API to return WDOG Status
04/23/10   jtn     Add APIs for hard reset control
03/15/10   fpe     CMI - Merge
02/02/10   jtn     Added API pm_pwron_hard_reset_enable(boolean enable)
02/01/10   jtn     Added API pm_pwron_soft_reset()
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
06/15/09   jtn     New file for SCMM
06/20/09   jtn     New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
/** @addtogroup pm_pwron
@{ */
/**
 * @brief If power on is deprecated.
 */ 
#ifdef __RVCT__
#define PM_PWRON_DEPRECATED __attribute__((__deprecated__))
#else
#define PM_PWRON_DEPRECATED
#endif

#include "pm_err_flags.h"
#include "pm_lib_cmd.h"
#include "comdef.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_power_reset_signal
 * @brief Power on reset signal.
 */
typedef enum
{
    PM_POWER_RESET_SIGNAL__INVALID /**< Invalid signal. */
}pm_power_reset_signal;

/**
 * @enum pm_power_reset_action_type
 * @brief Power reset action.
 */
typedef enum
{
    PM_PWR_ON_RST_ACTION_TYPE__COMPLETE_FULL_SHUTDOWN, /**< Complete full shutdown. */
    PM_PWR_ON_RST_ACTION_TYPE__FULL_SHUTDOWN,          /**< Full shutdown. */
    PM_PWR_ON_RST_ACTION_TYPE__HARD_RESET,             /**< Hard reset. */
    PM_PWR_ON_RST_ACTION_TYPE__POWER_OFF_NORMAL,       /**< Normal power off. */
    PM_PWR_ON_RST_ACTION_TYPE__SOFT_RESET,             /**< Soft reset. */
    PM_PWR_ON_RST_ACTION_TYPE__WARM_RESET,             /**< Warm reset. */
    PM_PWR_ON_RST_ACTION_TYPE__INVALID                 /**< Invalid action. */
}pm_power_reset_action_type;

/**
 * @enum pm_pwr_key_delay_type
 * @brief Power key hysteresis settings.
 */
typedef enum
{
  PM_PWR_KEY_DELAY_EQ_0_msecs,         /*!< 0.00 ms. */
  PM_PWR_KEY_DELAY_EQ_0p97_msecs,      /*!< 0.97  ms */
  PM_PWR_KEY_DELAY_EQ_1p95_msecs,      /*!< 1.95  ms */
  PM_PWR_KEY_DELAY_EQ_3p90_msecs,      /*!< 3.90  ms */
  PM_PWR_KEY_DELAY_EQ_7p81_msecs,      /*!< 7.81  ms */
  PM_PWR_KEY_DELAY_EQ_15p62_msecs,     /*!< 15.62  ms */
  PM_PWR_KEY_DELAY_EQ_31p25_msecs,     /*!< 31.25  ms */
  PM_PWR_KEY_DELAY_EQ_62p50_msecs,     /*!< 62.50  ms */
  PM_NUM_OF_PWR_KEY_DELAY              /*!< Number of power key hysteresis settings. */
}pm_pwr_key_delay_type;

/**
 * Keypad power on event. */
#define  PM_PWR_ON_EVENT_KEYPAD     0x1
/**
 * RTC power on event. */
#define  PM_PWR_ON_EVENT_RTC        0x2
/**
 * Cable power on event. */
#define  PM_PWR_ON_EVENT_CABLE      0x4
/**
 * SMPL power on event. */
#define  PM_PWR_ON_EVENT_SMPL       0x8
/**
 * Watchdog power on event. */
#define  PM_PWR_ON_EVENT_WDOG       0x10
/**
 * USB charger power on event. */
#define  PM_PWR_ON_EVENT_USB_CHG    0x20
/**
 * Wall charger power on event. */
#define  PM_PWR_ON_EVENT_WALL_CHG   0x40

/**
 * @enum pm_cbl_pwr_on_switch_type
 * @brief Power on switch configuration.
 */
typedef enum
{
  PM_CBL_PWR_ON__ENABLE, /**< Enable the switch. */
  PM_CBL_PWR_ON__DISABLE, /**< Disable the switch. */
  PM_CBL_PWR_ON__DIS_PERMANENTLY, /**< Permanently disable the switch. */
  PM_CBL_PWR_ON__INVALID /**< Invalid value. */
}pm_cbl_pwr_on_switch_type;

/**
 * @enum pm_cbl_pwr_on_pin_type
 * @brief Power on pin congifuration.
 */
typedef enum
{
  PM_CBL_PWR_ON_PIN__0, /**< Pin 0. */
  PM_CBL_PWR_ON_PIN__1, /**< Pin 1. */
  PM_CBL_PWR_ON_PIN__INVALID /**< Invalid value.*/
}pm_cbl_pwr_on_pin_type;


/*===========================================================================

                 POWER ON CONTROL FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Turns the PMIC abort timer switch ON or OFF.
 *
 * @datatypes
 * #pm_switch_cmd_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.  
 * @param [in] OnOff Turn the abort timer ON or OFF.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC. 
 * @newpage
 */
pm_err_flag_type pm_dev_start_up_abort_timer_switch(unsigned pmic_device_index, pm_switch_cmd_type OnOff);

/**
 * Configures the PMIC power key time delay for the keypad and cable power on state change
 * interrupt and triggering.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] microseconds PMIC power key delay setting. The function configures the
 *                          PMIC to the nearest value less than or equal to the requested value.
 *
 * @detdesc 
 * Configures the time delay for keypad and cable power on state
 *          change interrupt and triggering.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.  
 * @newpage
 */
/* Note: Do not use pm_pwr_key_delay_set(), which is deprecated. Use pm_dev_pwr_key_delay_set() instead. 
*/
pm_err_flag_type pm_dev_pwr_key_delay_set(unsigned pmic_device_index, uint32 microseconds);
pm_err_flag_type pm_pwr_key_delay_set(uint32 microseconds);

/**
 * Enables or disables the PMIC power key pull-up resistor
 *
 * @datatypes
 * #pm_switch_cmd_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] pull_up_en Enable/disable the pull-up resistor.
 *                        - TRUE -- Enable
 *                        - FALSE -- Disable @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 *  @newpage
 */
/* Note: Do not use pm_pwr_key_pullup_set(), which is deprecated. Use pm_dev_pwr_key_pullup_set() instead.
*/
pm_err_flag_type pm_dev_pwr_key_pullup_set(unsigned pmic_device_index, pm_switch_cmd_type pull_up_en);
pm_err_flag_type pm_pwr_key_pullup_set(pm_switch_cmd_type pull_up_en);

/**
 * Returns the PMIC watchdog reset status.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [out] dog_status Pointer to the watchdog reset status.
 *                         - TRUE -- Watchdog reset has occurred
 *                         - FALSE -- No watchdog reset has occurred @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC. 
 * @newpage
 */
/* Note: Do not use pm_wdog_status_get(), which is deprecated. Use pm_dev_wdog_status_get() instead.*/
pm_err_flag_type pm_dev_wdog_status_get(unsigned pmic_device_index, boolean *dog_status);
pm_err_flag_type pm_wdog_status_get(boolean *dog_status);

/**
 * Returns the phone power on reason. This is typically used during early boot-up and stored 
 * in memory for later access.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [out] pwr_on_reason Pointer to a 64-bit unsigned integer that stores 
 *                            the power on reasons, including: 
 *                            - Power on 
 *                            - Warm reset
 *                            - Power off
 *                            - Soft reset @tablebulletend 
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
pm_err_flag_type pm_dev_get_power_on_reason(unsigned pmic_device_index, uint64* pwr_on_reason);

/**
 * Returns the phone power on status. This function should be called at the 
 * earliest possible time at boot-up.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 *
 * @param [out] pwr_on_status Pointer to a 32-bit unsigned integer that stores 
 *                            the power on status, including:
 *                            - Keypad power on
 *                            - RTC alarm trigger
 *                            - Cable power on
 *                            - SMPL
 *                            - Watchdog timeout
 *                            - Wall charger
 *                            - USB charger @tablebulletend 
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_get_power_on_status(), which is deprecated. Use pm_dev_get_power_on_status() instead.*/
pm_err_flag_type pm_dev_get_power_on_status(unsigned pmic_device_index, uint32* pwr_on_status);
pm_err_flag_type pm_get_power_on_status(uint32* pwr_on_status);

/**
 * Clears the phone power on status. Call this function before the phone powers down.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_clear_power_on_status(), which is deprecated. Use pm_dev_clear_power_on_status() instead.*/
pm_err_flag_type pm_dev_clear_power_on_status(unsigned pmic_device_index);
pm_err_flag_type pm_clear_power_on_status(void);

/**
 * Enables/disables the PMIC's watchdog reset detection feature. 
 *
 * @datatypes
 * #pm_switch_cmd_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] OnOff Enable/disable the watchdog reset detect feature.
 *                   - PM_ON_CMD -- Enable
 *                   - PM_OFF_CMD -- Disable (default) @tablebulletend 
 *
 * @detdesc
 * If this feature is enabled, the PMIC resets and restarts if the PS_HOLD pin 
 * goes low. It also triggers an IRQ if the Watchdog Timeout IRQ 
 * (PM_WDOG_TOUT_IRQ_HDL) is enabled. If this feature is disabled, the PMIC 
 * shuts off if the PS_HOLD pin goes low; the IRQ is not triggered, even if 
 * enabled.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
 /* Note: Do not use pm_watchdog_reset_detect_switch(), which is deprecated.
 *        Use pm_dev_watchdog_reset_detect_switch() instead.*/
pm_err_flag_type pm_dev_watchdog_reset_detect_switch(unsigned pmic_device_index,
													 pm_switch_cmd_type OnOff);
pm_err_flag_type pm_watchdog_reset_detect_switch(pm_switch_cmd_type OnOff);


/*===========================================================================

                 CABLE POWER ON DRIVER FUNCTION DEFINITIONS

===========================================================================*/
/**
 * Enables, disables, or permanently disables the cable power on.
 *
 * @datatypes
 * #pm_cbl_pwr_on_switch_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] cmd Enable, disable, or blow the fuse (to permanently disable) for
 *                 cable power on.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
 /* Note: Do not use pm_cbl_pwr_on_switch(), which is deprecated. Use pm_dev_cbl_pwr_on_switch() instead.*/
pm_err_flag_type pm_dev_cbl_pwr_on_switch(unsigned pmic_device_index, pm_cbl_pwr_on_switch_type cmd);
pm_err_flag_type pm_cbl_pwr_on_switch(pm_cbl_pwr_on_switch_type cmd);

/**
 * Enables/disables CBL0PWR_N and CBL1PWR_N pin pull up. The pull up is only 
 * effective when cable power on is enabled.
 *
 * @datatypes
 * #pm_switch_cmd_type \n
 * #pm_cbl_pwr_on_pin_type
 * 
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] OnOff Enable/disable the cable power on pull up: 
                     PM_OFF_CMD or PM_ON_CMD.
 * @param [in] cable Cable Power On pin for which to enable/disable the pull up.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
 /* Note: Do not use pm_cbl_pwr_on_pull_up_switch(), which is deprecated. Use pm_dev_cbl_pwr_on_pull_up_switch() instead.*/
pm_err_flag_type pm_dev_cbl_pwr_on_pull_up_switch(unsigned pmic_device_index,
                                                  pm_switch_cmd_type      OnOff,
                                                  pm_cbl_pwr_on_pin_type  cable);
pm_err_flag_type pm_cbl_pwr_on_pull_up_switch(pm_switch_cmd_type      OnOff,
                                              pm_cbl_pwr_on_pin_type  cable);

/**
 * Resets the PMIC to its default state (hard reset). 
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 *
 * @detdesc
 * This function resets all SSBI registers (except for some RTC registers and 
 * trim registers) to default values. Note that, because the RTC time and alarm 
 * values are stored internally and are not SSBI registers, they are not reset.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS = 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_pwron_soft_reset(), which is deprecated. Use pm_dev_pwron_soft_reset() instead.*/
pm_err_flag_type pm_dev_pwron_soft_reset(unsigned pmic_device_index);
pm_err_flag_type pm_pwron_soft_reset(void);

/**
 * Enables a PMIC hardware reset.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] enable Enable/disable the power on hard reset trigger.
 *                    - TRUE -- PMIC powers on again following the reset
 *                    - FALSE -- PMIC remains powered down following the reset 
 *                      signal and requires another power-on trigger to start 
 *                      the power-on sequence @tablebulletend 
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_pwron_hard_reset_enable(), which is deprecated.
 *       Use pm_dev_pwron_hard_reset_enable() instead.*/
pm_err_flag_type pm_dev_pwron_hard_reset_enable(unsigned pmic_device_index, boolean enable);
pm_err_flag_type pm_pwron_hard_reset_enable(boolean enable);

/**
 * Sets the PMIC hard reset delay timer.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] delay_timer_ms Value for the hard reset delay timer. \n
 *                            The allowable range for this parameter is 0 to 
 *                            2000 ms, although the PMIC is set to one of the 
 *                            following discrete values: 0, 10, 50, 100, 250, 
 *                            500, 1000, or 2000. The PMIC driver sets the 
 *                            delay timer to the discrete value equal to or 
 *                            greater than the passed value, e.g., if the 
 *                            caller requested 150, the value is set to 250.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED--
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_pwron_hard_reset_delay_timer_set(), which is deprecated.
 *       Use pm_dev_pwron_hard_reset_delay_timer_set() instead.*/
pm_err_flag_type
pm_dev_pwron_hard_reset_delay_timer_set(unsigned pmic_device_index,
                                        int delay_timer_ms);
pm_err_flag_type
pm_pwron_hard_reset_delay_timer_set(int delay_timer_ms);

/**
 * Sets the PMIC hard reset debounce timer.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] debounce_timer_ms Hard reset debounce timer in milliseconds. \n
 *                               This parameter can range from 0 to 10256 ms, 
 *                               although the PMIC is set to one of the 
 *                               following discrete values: 0, 32, 56, 80, 128, 
 *                               184, 272, 408, 608, 904, 1352, 2048, 3072, 
 *                               4480, 6720, or 10256. The PMIC driver sets the 
 *                               debounce timer to the discrete value equal to 
 *                               or higher than the passed value, e.g., if the 
 *                               caller requested 130, the value is set to 184.
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_pwron_hard_reset_debounce_timer_set(), which is deprecated.
 *       Use pm_dev_pwron_hard_reset_debounce_timer_set() instead.*/
pm_err_flag_type
pm_dev_pwron_hard_reset_debounce_timer_set(unsigned pmic_device_index,
                                           int debounce_timer_ms);
pm_err_flag_type
pm_pwron_hard_reset_debounce_timer_set(int debounce_timer_ms);

/**
 * Enables the PMIC stay-on bit.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] enable Whether to enable the bit. 
 *                    - TRUE -- Set the PMIC stay-on bit
 *                    - FALSE -- Clear the PMIC stay-on bit @tablebulletend 
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @newpage
 */
/* Note: Do not use pm_pwron_stay_on_enable(), which is deprecated. Use
 *       pm_dev_pwron_stay_on_enable() instead. 
 */
pm_err_flag_type pm_dev_pwron_stay_on_enable(unsigned pmic_device_index, boolean enable);
pm_err_flag_type pm_pwron_stay_on_enable(boolean enable);

/** @} */ /* end_addtogroup pm_pwron */

#endif    /* PM_PWRON_H */
