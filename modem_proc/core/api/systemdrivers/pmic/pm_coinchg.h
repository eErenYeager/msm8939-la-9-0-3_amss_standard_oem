#ifndef PM_COINCHG_H
#define PM_COINCHG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_coinchg.h COINCELL CHARGER related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC coin
 *        cell charger driver.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_coinchg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
02/08/11   hw      Added support for 2.5V coin cell charging enum
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
06/30/09   jtn     Updated file documentation
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_coinchg 
@{ */
/**
 * @enum pm_coin_cell_vset_type
 * @brief Coin cell charger voltage setting.
 */
typedef enum
{
    PM_COIN_CELL_VSET_3p0V,   /**< 3.0 V. */
    PM_COIN_CELL_VSET_3p1V,   /**< 3.1 V. */
    PM_COIN_CELL_VSET_3p2V,   /**< 3.2 V. */
    PM_COIN_CELL_VSET_2p5V,   /**< 2.5 V. */
    PM_COIN_CELL_VSET_INVALID /**< Invalid value. */
}pm_coin_cell_vset_type;

/**
 * @enum pm_coin_cell_rset_type
 * @brief Coin cell charger current limiting resistor setting.
 */
typedef enum
{
    PM_COIN_CELL_RSET_2100_OHMS, /**< 2100 Ohm. */
    PM_COIN_CELL_RSET_1700_OHMS, /**< 1700 Ohm. */
    PM_COIN_CELL_RSET_1200_OHMS, /**< 1200 Ohm. */
    PM_COIN_CELL_RSET_800_OHMS,  /**< 800 Ohm. */
    PM_COIN_CELL_RSET_INVALID   /**< Invalid value. */
}pm_coin_cell_rset_type;


/*===========================================================================

                    COINCELL DRIVER FUNCTION PROTOTYPES

===========================================================================*/

/**
 * Turns the coin cell charging ON/OFF.
 *
 * @datatypes
 * #pm_switch_cmd_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] cmd Command.
 *             - PM_ON_CMD -- Enables coin cell charging (without calling 
 *               pm_coin_cell_chg_config()). Turning on the coin cell charger 
 *               configures the charger voltage to 3.2 V and the current 
 *               resistor setting to 2100 Ohm.
 *             - PM_OFF_CMD -- Disables coin cell charging (default). @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE -- Input parameter is invalid. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * PM_ERR_FLAG__SPMI_OPT_ERR -- SPMI errors.
 *
 * @newpage 
 */
/* Note: Do not use pm_coin_cell_chg_switch(), which is deprecated. 
 *       Use pm_dev_coin_cell_chg_switch() instead. 
 */
extern pm_err_flag_type pm_dev_coin_cell_chg_switch(unsigned pmic_device_index,
                                                    pm_switch_cmd_type cmd);
extern pm_err_flag_type pm_coin_cell_chg_switch(pm_switch_cmd_type cmd);

/**
 * Configures the coin cell charger voltage and current limiting resistor 
 * value.
 *
 * @datatypes
 * #pm_coin_cell_vset_type \n
 * #pm_coin_cell_rset_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] vset Configures the coin cell charger voltage.
 *                  - PM_COIN_CELL_VSET_3p0V -- 3.0 V
 *                  - PM_COIN_CELL_VSET_3p1V -- 3.1 V
 *                  - PM_COIN_CELL_VSET_3p2V -- 3.2 V (default) @tablebulletend 
 * @param [in] rset Configures the coin cell charger current limiting resistor 
                    setting.
 *                  - PM_COIN_CELL_RSET_2100_OHMS -- 2100 Ohm (default)
 *                  - PM_COIN_CELL_RSET_1700_OHMS -- 1700 Ohm
 *                  - PM_COIN_CELL_RSET_1200_OHMS -- 1200 Ohm
 *                  - PM_COIN_CELL_RSET_800_OHMS  -- 800 Ohm @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS.
 *
 * @newpage 
 */
/* Note: Do not use pm_coin_cell_chg_config(), which is deprecated. 
 *       Use pm_dev_coin_cell_chg_config() instead. 
 */
extern pm_err_flag_type pm_dev_coin_cell_chg_config(unsigned pmic_device_index,
                                                    pm_coin_cell_vset_type vset,
                                                    pm_coin_cell_rset_type rset);
extern pm_err_flag_type pm_coin_cell_chg_config(pm_coin_cell_vset_type vset,
                                                pm_coin_cell_rset_type rset);

/** @} */ /* end_addtogroup pm_coinchg */

#endif    /* PM_COINCHG_H */

