#ifndef PM_VERSION_H
#define PM_VERSION_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_version.h PMIC MODEL/VERSION related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC model/
 *		  version detection.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_version.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/13   mr      (Tech Pubs) Edited/added Doxygen comments and markup (CR-522045)
10/22/13   rk      Define PM8916 model enum type.
08/26/13   rk      Define PM8916 enum type.
02/14/13   kt      Adding pm_get_pbs_info API to get the pmic device pbs info.  
01/28/13   kt      Adding pm_get_pmic_info API to get the pmic device info.  
01/28/13   kt      Removing pm_set_hardware_version and pm_get_hardware_version APIs.  
01/10/13   kt      Changing the file as per Henry's Phase1 changes.  
03/10/12   wra     Removed old PMIC versions and added Badger PMIC versions. 
                    Reduced hardware versions in enumeration
10/18/11   jtn/mpt Add PM8821 and PM8038
04/04/11   hw      Added pm_get_hardware_version and pm_set_hardware_version API
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/05/10   wra     Added pm_model_type entry for the PM8921 and PM8018
07/05/10   wra     Added file header and pm_model_type entry for the PM8901 
                   and ISL9519
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/** @addtogroup pm_version 
@{ */
/** 
 * @enum pm_model_type
 * @brief PMIC model type values (peripheral subtype values)
 *        for different PMIC models. 
 */
typedef enum
{
   PMIC_IS_UNKNOWN   = 0,  /*!< Unknown. */
   PMIC_IS_PM8941    = 1,  /*!< 1 -- PM8941. */
   PMIC_IS_PM8841    = 2,  /*!< 2 -- PM8841. */
   PMIC_IS_PM8019    = 3,  /*!< 3 -- PM8019. */
   PMIC_IS_PM8026    = 4,  /*!< 4 -- PM8026. */
   PMIC_IS_PM8110    = 5,  /*!< 5 -- PM8110. */
   PMIC_IS_PMA8084   = 6,  /*!< 6 -- PMA8084. */
   PMIC_IS_PMI8962   = 7,  /*!< 7 -- PMI8962. */
   PMIC_IS_PMD9635   = 8,  /*!< 8 -- PMD9635. */   
   PMIC_IS_PM8994    = 9, /*!< PMIC model is PM8994. */
   PMIC_IS_PMI8994   = 10, /*!< PMIC model is PMI8994. */
   PMIC_IS_PM8916    = 11, /*!< 11 -- PMD8916. */
   PMIC_IS_INVALID   = 0x7FFFFFFF,
} pm_model_type;

/** 
 * @struct pm_device_info_type
 * @brief PMIC device information. This stores the PMIC's model type value, 
 *        all layer revision number, and metal revision number. See 
 *        #pm_model_type for information on which PMIC model type value 
 *        represents which PMIC. As an example, for PM8019 v2.1 the 
 *        PMIC model type value is 3 (since PM8019 model type value is 3 
 *        per the pm_model_type enum), all layer revision number is 2 and 
 *        metal revision number is 1.
 */
typedef struct
{
  pm_model_type  ePmicModel;             /*!< Model type. */
  uint32         nPmicAllLayerRevision;  /*!< All layer revision number. */
  uint32         nPmicMetalRevision;     /*!< Metal revision number. */
} pm_device_info_type;

/**
 * Number of Lot IDs. Each Lot ID represents an ASCII value.
 */
#define PM_PBS_INFO_NUM_LOT_IDS   12

/** 
 * @struct pm_pbs_info_type 
 * @brief Stores the PMIC PBS-related information, such as PBS Lot
 *        ID, ROM version, and RAM version. The PBS ROM/RAM revision
 *        ID and variant (or branch) ID are stored in last 16
 *        bits (upper and lower 8 bits) of nPBSROMVersion and
 *        nPBSRAMVersion.
 */
typedef struct
{
  uint8          nLotId[PM_PBS_INFO_NUM_LOT_IDS];  /*!< PMIC device lot number. */
  uint32         nPBSROMVersion;                   /*!< PBS ROM version number. */
  uint32         nPBSRAMVersion;                   /*!< PBS RAM version number. @newpagetable */
} pm_pbs_info_type;


/*===========================================================================

                PMIC MODEL/VERSION READ FUNCTION PROTOTYPE

===========================================================================*/
/** @cond */
/** 
 * @name pm_ft_get_pmic_model 
 * @deprecated New code should use the pm_get_pmic_model api.
 */
#define pm_ft_get_pmic_model pm_get_pmic_model

/** 
 * @name pm_ft_get_rev_id 
 * @deprecated New code should use the pm_get_pmic_revision api.
 */
#define pm_ft_get_rev_id pm_get_pmic_revision
/** @endcond */

/**
 * Returns the PMIC's model type value. For example, this function
 * returns 4 for PM8x26. See #pm_model_type for more information 
 * on which PMIC model type value represents which PMIC.
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 */
pm_model_type pm_get_pmic_model(uint8 pmic_device_index);

/**
 * Returns the PMIC's all layer revision number.
 * For example, this function returns 1 for PMIC v1.x, 2 for PMIC v2.x, etc.
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
uint8 pm_get_pmic_revision(uint8 pmic_device_index);

/**
 * Returns information about a PMIC device for a specific device index
 * in the pmic_device_info argument. This function returns the PMIC's
 * model type value, all layer revision number, and metal revision 
 * number in the #pm_device_info_type structure. 
 *
 * @datatypes
 * #pm_device_info_type
 *
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [out] pmic_device_info Variable returned to the caller with the
 *								 PMIC device information.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 */
pm_err_flag_type pm_get_pmic_info(uint8 pmic_device_index,
								  pm_device_info_type* pmic_device_info);

/**
 * Returns information about the specified PMIC device, such as PBS
 * lot ID, ROM version, RAM version, fab ID, wafer ID, X coordinate,
 * and Y coordinate, for a specified device index in the pbs_info argument.
 *
 * @datatypes
 * #pm_pbs_info_type
 * 
 * @param [in] pmic_device_index Primary: 0. Secondary: 1.
 * @param [out] pbs_info Variable returned to the caller with the PMIC
 *                       device PBS information.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_get_pbs_info(uint8 pmic_device_index, pm_pbs_info_type* pbs_info);
/** @} */ /* end_addtogroup pm_version */

#endif    /* PM_VERSION_H */
