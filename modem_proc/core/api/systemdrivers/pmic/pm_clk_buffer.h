#ifndef PM_CLK_BUFFER_H
#define PM_CLK_BUFFER_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_clk_buffer.h PMIC-MEGA XO CLK BUFFER related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *        to support Qualcomm PMIC MEGA XO module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_clk_buffer.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/14/12   hs      Initial version.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_clk_buffer
@{ */
/**
 * @enum pm_clk_buff_output_drive_strength_type
 * @brief Clock buffer output drive strength.
 */
typedef enum
{
    PM_RF_CLK1,    
    PM_RF_CLK2,    
    PM_RF_CLK3,   
    PM_SLEEP_CLK1,
    PM_ALL_CLKS,
    PM_CLK_INVALID
}pm_clk_type;

/** @addtogroup pm_clk_buffer
@{ */
/**
 * @enum pm_clk_buff_output_drive_strength_type
 * @brief Clock buffer output drive strength.
 */
typedef enum
{
    PM_CLK_BUFF_OUT_DRV__1X, /**< Same strength. */
    PM_CLK_BUFF_OUT_DRV__2X, /**< Double strength. */
    PM_CLK_BUFF_OUT_DRV__3X, /**< Triple strength. */
    PM_CLK_BUFF_OUT_DRV__4X, /**< Quadruple strength. */
    PM_CLK_DRV_STRENGTH_INVALID /**< Output drive strength is invalid. */
} pm_clk_buff_output_drive_strength_type;


/*===========================================================================

                    CLOCK DRIVER FUNCTION PROTOTYPES

===========================================================================*/

/**
 * Sets the output buffer drive strength.
 *
 * @datatypes
 * #pm_clk_buff_output_drive_strength_type
 *
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] resourceIndex Selects which buffer is targeted.
 * @param [in] drive_strength Drive strength.
 *                            - PM_CLK_BUFF_OUT_DRV__1X -- Same strength
 *                            - PM_CLK_BUFF_OUT_DRV__2X -- Double strength
 *                            - PM_CLK_BUFF_OUT_DRV__3X -- Triple strength
 *                            - PM_CLK_BUFF_OUT_DRV__4X -- Quadruple strength @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE -- Drive_strength is invalid. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * PM_ERR_FLAG__SPMI_OPT_ERR -- SPMI errors.
 *
 * @newpage
 */
/* Note: Do not use pm_clk_buff_set_output_drive_strength(),
 *       which is deprecated. Use pm_dev_clk_buff_set_output_drive_strength()
 *       instead.
 */
pm_err_flag_type
pm_dev_clk_buff_set_output_drive_strength(unsigned pmic_device_index,
                                          int resourceIndex,
                                          pm_clk_buff_output_drive_strength_type drive_strength);
pm_err_flag_type
pm_clk_buff_set_output_drive_strength(int resourceIndex,
                                      pm_clk_buff_output_drive_strength_type drive_strength);

/**
 * Enables the clock.
 *
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] resourceIndex Selects which buffer is targeted.
 * @param [in] enable Whether to enable the clock.
 *                    - TRUE -- Enable the clock
 *                    - FALSE -- Do not force the clock to be on @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * PM_ERR_FLAG__SPMI_OPT_ERR -- SPMI errors.
 *
 * @newpage
 */
/* Note: Do not use pm_clk_buff_enable(), which is deprecated.
 *       Use pm_dev_clk_buff_enable() instead.
 */
pm_err_flag_type pm_dev_clk_buff_enable(unsigned pmic_device_index,
                                        int resourceIndex,
                                        boolean enable);
pm_err_flag_type pm_clk_buff_enable(int resourceIndex, boolean enable);

/**
 * Enables the buffer pin control functionality.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] resourceIndex Selects which buffer is targeted.
 * @param [in] pin_control_enable Whether to enable pin control.
 *                                - TRUE -- Enable pin control
 *                                - FALSE -- Do not enable pin control @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * PM_ERR_FLAG__SPMI_OPT_ERR -- SPMI errors.
 *
 * @newpage
 */
/* Note: Do not use pm_clk_buff_pin_control_enable(), which is deprecated.
 *       Use pm_dev_clk_buff_pin_control_enable() instead.
 */
pm_err_flag_type pm_dev_clk_buff_pin_control_enable(unsigned pmic_device_index,
                                                    int resourceIndex,
                                                    boolean pin_control_enable);
pm_err_flag_type pm_clk_buff_pin_control_enable(int resourceIndex, boolean enable);

/**
 * Controls the polarity of the pin control functionality.
 *
 * @note Interrupts are disabled while communicating with the PMIC.
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] resourceIndex Selects which buffer is targeted.
 * @param [in] is_low_enabled Whether clock is enabled when low.
 *                            - TRUE -- Clock is enabled when the PC signal is low
 *                            - FALSE -- Clock is enabled when the PC signal is high @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available. \n
 * PM_ERR_FLAG__SPMI_OPT_ERR -- SPMI errors.
 *
 * @newpage
 */
 pm_err_flag_type pm_dev_clk_buff_pin_control_polarity(unsigned pmic_device_index,
                                                       int resourceIndex,
                                                       boolean is_low_enabled);
 /** @} */ /* end_addtogroup pm_clk_buffer */

#endif /* PM_CLK_BUFFER_H */

