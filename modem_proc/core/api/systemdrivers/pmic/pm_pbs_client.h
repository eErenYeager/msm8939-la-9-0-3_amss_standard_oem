#ifndef PM_PBS_CLIENT_H
#define PM_PBS_CLIENT_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_pbs_client.h PMIC PBS Client related declaration.
 *
 * @brief This header file contains API and type definitions for PBS Client driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_pbs_client.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
06/16/13   kt      Created.
===========================================================================*/

/*===========================================================================

                        HEADER FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "com_dtypes.h"
#include "pm_irq.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/** @addtogroup pm_pbs_client
@{ */
/**
 * @enum pm_pbs_client_type
 * @brief PBS client.
 */
typedef enum 
{
  PM_PBS_CLIENT_0  = 0,  /**< PBS client 0. */
  PM_PBS_CLIENT_1  = 1,  /**< PBS client 1. */
  PM_PBS_CLIENT_2  = 2,  /**< PBS client 2. */
  PM_PBS_CLIENT_3  = 3,  /**< PBS client 3. */
  PM_PBS_CLIENT_4  = 4,  /**< PBS client 4. */
  PM_PBS_CLIENT_5  = 5,  /**< PBS client 5. */
  PM_PBS_CLIENT_6  = 6,  /**< PBS client 6. */
  PM_PBS_CLIENT_7  = 7,  /**< PBS client 7. */
  PM_PBS_CLIENT_8  = 8,  /**< PBS client 8. */
  PM_PBS_CLIENT_9  = 9,  /**< PBS client 9. */
  PM_PBS_CLIENT_10 = 10, /**< PBS client 10. */
  PM_PBS_CLIENT_11 = 11, /**< PBS client 11. */
  PM_PBS_CLIENT_12 = 12, /**< PBS client 12. */
  PM_PBS_CLIENT_13 = 13, /**< PBS client 13. */
  PM_PBS_CLIENT_14 = 14, /**< PBS client 14. */
  PM_PBS_CLIENT_15 = 15, /**< PBS client 15. */
  PM_PBS_CLIENT_INVALID  /**< Invalid PBS client. */
}pm_pbs_client_type;

/**
 * @enum pm_pbs_client_irq_type
 * @brief Types of IRQ bit fields for a PBS client.
 */
typedef enum 
{
  PM_PBS_CLIENT_IRQ_SEQ_ERROR = 0, /**< IRQ sequence error. */
  PM_PBS_CLIENT_IRQ_SEQ_ENDED = 1, /**< IRQ sequence ended. */
  PM_PBS_CLIENT_IRQ_INVALID        /**< Invalid. */
}pm_pbs_client_irq_type;

/**
 * @enum pm_pbs_client_trigger_type
 * @brief PBS trigger.
 */
typedef enum 
{
  PM_PBS_CLIENT_TRIGGER_RISING_EDGE,   /**< Rising edge. */
  PM_PBS_CLIENT_TRIGGER_FALLING_EDGE,  /**< Falling edge. */
  PM_PBS_CLIENT_TRIGGER_DUAL_EDGE,     /**< Dual edge. */
  PM_PBS_CLIENT_TRIGGER_INVALID        /**< Invalid. */
} pm_pbs_client_trigger_type;


/*===========================================================================

                    PBS DRIVER FUNCTUION PROTOTYPE

===========================================================================*/
/**
 * Enables or disables all the triggers corresponding to the specified 
 * PBS client.
 *
 * @datatypes
 * #pm_pbs_client_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC:1  
 * @param [in] client PBS client.
 * @param [in] enable Enable/disable PBS client triggers. 
 *                    - TRUE -- Enable  
 *                    - FALSE -- Disable  @tablebulletend 
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_enable(uint8 pmic_device_index,
                                      pm_pbs_client_type client,
                                      boolean enable);

/**
 * Returns the PBS client enable/disable status.
 *
 * @datatypes
 * #pm_pbs_client_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [out] status Indicates whether the PBS client triggers are enabled.
 *                     - TRUE -- Enabled
 *                     - FALSE -- Disabled @tablebulletend 
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_enable_status(uint8 pmic_device_index,
                                             pm_pbs_client_type client,
                                             boolean *status);

/**
 * Configures the PBS trigger type for the PBS client.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * #pm_pbs_client_trigger_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [in] trigger PBS trigger.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_trigger_cfg(uint8 pmic_device_index,
                                           pm_pbs_client_type client,
                                           pm_pbs_client_trigger_type trigger);

/**
 * Enables or disables the PBS client IRQ.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * #pm_pbs_client_irq_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [in] irq PBS client IRQ.
 * @param [in] enable Whether to enable the PBS client IRQ.
 *                    - TRUE -- Enable 
 *                    - FALSE -- Disable @tablebulletend 
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_irq_enable(uint8 pmic_device_index,
                                          pm_pbs_client_type client,
                                          pm_pbs_client_irq_type irq,
                                          boolean enable);

/**
 * Clears the PBS client IRQ.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * #pm_pbs_client_irq_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [in] irq PBS client IRQ.
 *
 * @return   
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_irq_clear(uint8 pmic_device_index,
                                         pm_pbs_client_type client,
                                         pm_pbs_client_irq_type irq);

/**
 * Configures the PBS client IRQ trigger.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * #pm_pbs_client_irq_type \n
 * #pm_irq_trigger_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [in] irq PBS client IRQ.
 * @param [in] trigger IRQ trigger.
 *
 * @return   
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SBI driver failed to communicate with the PMIC. @newpage
 */
pm_err_flag_type pm_pbs_client_irq_set_trigger(uint8 pmic_device_index,
                                               pm_pbs_client_type client,
                                               pm_pbs_client_irq_type irq,
                                               pm_irq_trigger_type trigger);

/**
 * Returns the PBS client IRQ status.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * #pm_pbs_client_irq_type \n
 * #pm_irq_status_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] client PBS client.
 * @param [in] irq PBS client IRQ.
 * @param [in] type IRQ status to read. 
 * @param [out] status IRQ status.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. @newpage
 *  */
pm_err_flag_type pm_pbs_client_irq_status(uint8 pmic_device_index,
                                          pm_pbs_client_type client,
                                          pm_pbs_client_irq_type irq,
                                          pm_irq_status_type type,
                                          boolean *status);

/**
 * Configures the PBS client SW Trigger.
 *
 * @datatypes
 * #pm_pbs_client_type \n
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1. 
 * @param [in] pbs_client_index PBS client index.
 *
 * @return  
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC. @newpage
 *  */
 pm_err_flag_type pm_pbs_client_sw_trigger(uint8 pmic_device_index,
                                           pm_pbs_client_type pbs_client_index);
 
/** @} */ /* end_addtogroup pm_pbs_client */

#endif /* PM_PBS_CLIENT_H */
