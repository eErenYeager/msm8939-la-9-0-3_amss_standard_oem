#ifndef PM_BOOST_H
#define PM_BOOST_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_boost.h VOLTAGE BOOST related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC voltage
 *        regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_boost.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/31/14   llg     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/** @addtogroup pm_vreg 
@{ */
/**
 * @anonenum{Boost Peripheral Index}@xreflabel{boost_peripheral_index}
 * Contains all the Low Dropout (LDO) regulators that may be required.
 */
enum
{
  PM_BOOST_1,      /**< LDO regulator 1. */
  PM_BOOST_INVALID /**< Invalid LDO regulator. */
};


/*===========================================================================

                VOLTAGE BOOST FUNCTION PROTOTYPE

===========================================================================*/
/**
 * Obtains the regulator's voltage. 
 *
 * @note1hang Different types (LDO, HFS, etc.) may have different 
 *    programmable voltage steps. Only the correct programmable steps 
 *    are supported. Voltages are not rounded if the selected voltage 
 *    is not part of the programmable steps.
 *
 * @param [in] pmic_chip Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] boost_peripheral_index Peripheral index.
 * @param [out] volt_level Voltage level.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_boost_volt_level_status(uint8 pmic_chip,
                                            uint8 boost_peripheral_index,
                                            pm_volt_level_type* volt_level);

/**
 * Obtains the mode status (HPM, LPM, etc.) at an instance.
 *
 * @note1hang A regulator's mode changes dynamically.
 *
 * @datatypes
 * #pm_sw_mode_type
 *
 * @param [in] pmic_chip Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] boost_peripheral_index Peripheral index.
 * @param [out] on_off Selects a regulator's mode; for example: HPM, LPM.
 *
 * @return
 * PM_ERR_FLAG__SUCCESS -- SUCCESS. \n
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- Feature is not available.
 *
 * @newpage 
 */
pm_err_flag_type pm_boost_sw_enable_status (uint8 pmic_chip,
                                            uint8 boost_peripheral_index,
                                            pm_on_off_type* on_off);
/** @} */ /* end_addtogroup pm_vreg */

#endif /* PM_BOOST_H */

