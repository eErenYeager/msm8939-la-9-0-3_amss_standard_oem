#ifndef PM_SMBC_H
#define PM_SMBC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_smbc.h PMIC-SMBC Module related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *        to support Qualcomm PMIC SMBC (Switch-Mode Battery Charger) module.
 *        The Switched-Mode Battery Charger (SMBC) module includes a buck
 *        regulated battery charger with integrated switches. The SMBC module,
 *        along with the Over Voltage Protection (OVP) module will majorly be
 *        used by charger application for charging Li-Ion batteries with high
 *        current (up to 2A).
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_smbc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/02/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/11   hs      Added modes to set the gain for battery current monitor
                   output to XOADC.
01/07/11   hs      Initial version.
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/** @addtogroup pm_smbc 
@{ */
/**
 * @enum pm_smbc_cmd_type
 * @brief Used to enable or disable certain functionalities in
 *        the SMBC module.
 */
typedef enum
{
   PM_SMBC_CMD__DISABLE, /*!< Disable the functionality.  */   
   PM_SMBC_CMD__ENABLE /*!< Enable the functionality.   */
}pm_smbc_cmd_type;

/**
 * @enum pm_smbc_vdd_default_type
 * @brief Default Vdd value.
 */
typedef enum
{
   PM_SMBC_VDD_DEFAULT__LOW,     /*!< Low Vdd.   */   
   PM_SMBC_VDD_DEFAULT__STANDARD /*!< Standard Vdd.  */
}pm_smbc_vdd_default_type;

/**
 * @enum pm_smbc_batt_temp_ref_gnd_type
 * @brief Ground connection for battery thermistor.
 */
typedef enum
{
   PM_SMBC_BATT_TEMP_REF_GND__PACK, /*!< Battery thermistor is inside the 
                                         battery pack.   */   
   PM_SMBC_BATT_TEMP_REF_GND__GND   /*!< Ground.   */
   
}pm_smbc_batt_temp_ref_gnd_type;

/**
 * @enum pm_smbc_batt_temp_cold_thr_type
 * @brief Cold battery temperature threshold as a fraction of VREG_THERM.
 */
typedef enum
{
   PM_SMBC_BATT_TEMP_COLD_THR__LOW, /*!< Low.   */   
   PM_SMBC_BATT_TEMP_COLD_THR__HIGH /*!< High.  */
   
}pm_smbc_batt_temp_cold_thr_type;

/**
 * @enum pm_smbc_batt_temp_hot_thr_type
 * @brief Hot battery temperature threshold as a fraction of VREG_THERM.
 */
typedef enum
{
   PM_SMBC_BATT_TEMP_HOT_THR__LOW, /*!< Low.   */   
   PM_SMBC_BATT_TEMP_HOT_THR__HIGH /*!< High. @newpage  */
   
}pm_smbc_batt_temp_hot_thr_type;

/**
 * @enum pm_smbc_mode_type
 * @brief Charge command.
 */
typedef enum
{
   PM_SMBC_MODE__BOOT_FAIL,            /*!< Boot failed. */   
   PM_SMBC_MODE__BOOT_DONE,            /*!< Boot is complete. */  
   PM_SMBC_MODE__BATT_TEMP_SENSING_OFF,/*!< Battery temperature sensing off. */   
   PM_SMBC_MODE__BATT_TEMP_SENSING_ON, /*!< Battery temperature sensing on.  */  
   PM_SMBC_MODE__IMON_GAIN_LOW,        /*!< Low gain for battery current monitor output to XOADC. */  
   PM_SMBC_MODE__IMON_GAIN_HIGH,       /*!< High gain for battery current monitor output to XOADC. */  
   PM_SMBC_MODE__CHARGE_OFF,           /*!< Charge off. */   
   PM_SMBC_MODE__CHARGE_ON,            /*!< Charge on. */  
   PM_SMBC_MODE__CHARGE_PAUSE,         /*!< Charge pause. */   
   PM_SMBC_MODE__CHARGE_RESUME,        /*!< Charge resume. */   
   PM_SMBC_MODE__FORCE_ATC_OFF,        /*!< ATC force off. */   
   PM_SMBC_MODE__FORCE_ATC_ON,         /*!< ATC force on. */   
   PM_SMBC_MODE__ATC_FAILED_CLEAR_ON,  /*!< ATC failed clear on. */   
   PM_SMBC_MODE__CHG_FAILED_CLEAR_ON,  /*!< Charge failed clear on. */   
   PM_SMBC_MODE__USB_SUSPENDED_OFF,    /*!< USB suspended mode off. */   
   PM_SMBC_MODE__USB_SUSPENDED_ON      /*!< USB suspended mode on. */
}pm_smbc_mode_type;

/**
 * @enum pm_smbc_led_source_type
 * @brief Power source for the anode of the change indicator LED.
 */
typedef enum
{  
   PM_SMBC_LED_SRC__GROUND,  /*!< Ground.  */   
   PM_SMBC_LED_SRC__VPH_PWR, /*!< VPH_PWR. */  
   PM_SMBC_LED_SRC__BOOST,   /*!< Boost.   */   
   PM_SMBC_LED_SRC__INTERNAL /*!< Internal source. */
}pm_smbc_led_source_type; 

/**
 * @enum pm_smbc_attached_chgr_type
 * @brief Attached charger type.
 */
typedef enum
{
   PM_SMBC_ATTACHED_CHGR__NONE, /*!< No charger.   */      
   PM_SMBC_ATTACHED_CHGR__USB,  /*!< USB path charger is attached. */   
   PM_SMBC_ATTACHED_CHGR__DCIN, /*!< DCIN path charger is attached. */   
   PM_SMBC_ATTACHED_CHGR__BOTH  /*!< Both USB and DCIN are attached. @newpage */
}pm_smbc_attached_chgr_type;

/**
 * @enum pm_smbc_chg_path_type
 * @brief Charge path.
 */
typedef enum
{   
   PM_SMBC_CHGR_PATH__USB, /*!< USB. */   
   PM_SMBC_CHGR_PATH__DCIN /*!< DCIN. */
}pm_smbc_chg_path_type;

/**
 * @enum pm_smbc_fsm_state_name_type
 * @brief Charging state names in the FSM.
 */
typedef enum
{
   PM_SMBC_FSM_ST__OFF,                      /*!< 0: FSM entry point. */   
   PM_SMBC_FSM_ST__PWR_ON_CHGR,              /*!< 1: Power on from charger with high current. */  
   PM_SMBC_FSM_ST__ATC_LOW_I,                /*!< 2: ATC with low charging current. */   
   PM_SMBC_FSM_ST__PWR_ON_BATT,              /*!< 3: Power on from the battery. */   
   PM_SMBC_FSM_ST__ATC_FAIL = 4,             /*!< 4: ATC fail.  */   
   PM_SMBC_FSM_ST__PWR_ON_CHGR_AND_BATT = 6, /*!< 6: Power on from the charger and the battery. */ 
   PM_SMBC_FSM_ST__FAST_CHG,                 /*!< 7: Fast charge.    */   
   PM_SMBC_FSM_ST__TRICKLE_CHG,              /*!< 8: Trickle charge. */   
   PM_SMBC_FSM_ST__CHG_FAIL,                 /*!< 9: Charge fail.  */   
   PM_SMBC_FSM_ST__EOC = 10,                 /*!< 10: EOC.  */  
   PM_SMBC_FSM_ST__ATC_PAUSE = 13,           /*!< 13: ATC pause.  */   
   PM_SMBC_FSM_ST__FAST_CHG_PAUSE,           /*!< 14: Fast charge pause.  */  
   PM_SMBC_FSM_ST__TRICKLE_CHG_PAUSE = 15,   /*!< 15: Trickle charge pause. */   
   PM_SMBC_FSM_ST__ATC_HIGH_I = 18,          /*!< 18: ATC with high charging current. */   
   PM_SMBC_FSM_ST__FLCB = 22                 /*!< 22: FLCB.    */
}pm_smbc_fsm_state_name_type;


/*===========================================================================

                 SMBC DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Sets the charge mode, such as enable charging, disable charging, 
 * pause charging, and resume charging.
 *
 * @note1hang The default setting in the PMIC hardware is PM_SMBC_MODE__CHARGE_OFF.
 *
 * @datatypes
 * #pm_smbc_mode_type
 * 
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] mode Set the mode in the SMBC module.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 *
 * @newpage
 *
 */
pm_err_flag_type pm_smbc_set_mode(unsigned pmic_device_index,
                                  int externalResourceIndex,
                                  pm_smbc_mode_type mode);

/**
 * Sets the power on trigger sent with the charge path by the SMBC module
 * to the Power On module.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_smbc_cmd_type \n
 * #pm_smbc_chg_path_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] enable Enable/disable the power on trigger
 *             - PM_SMBC_CMD__ENABLE -- Enable the SMBC module to send the
 *                                    power on trigger to the power on module.
 *             - PM_SMBC_CMD__DISABLE -- Disable the SMBC module from sending the
 *                           power on trigger to the power on module. @tablebulletend
 * @param [in] path The charge path on which to set the power on trigger.
 *                 - PM_SMBC_CHGR_PATH__USB -- USB charge path
 *                 - PM_SMBC_CHGR_PATH__DCIN -- DC charge path @tablebulletend
 *
 * @detdesc 
 * This function enables/disables the power on trigger (charge valid) sent by the 
 * SMBC module to the Power On module when a charge device is plugged into the 
 * phone.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par 
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE --
 * Input parameter three is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_pwron_trig(int externalResourceIndex,
                                        pm_smbc_cmd_type enable,
                                        pm_smbc_chg_path_type path);

/**
 * Gets the availability of the BATFET.
 *
 * @param [in] externalResourceIndex External SMBC ID. 
 * @param [out] present Status of the BATFET.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par 
 * PM_ERR_FLAG__INVALID_POINTER --
 * Input parameter two is a NULL pointer.
 * @par 
 * PM_ERR_FLAG__SUCCESS -- Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_batfet_present(int externalResourceIndex,
                                            boolean *present);

/**
 * Sets the default Vdd (power supply) value.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_smbc_vdd_default_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] vdd Default Vdd in pm_smbc_vdd_default_type.
 *                - PM_SMBC_VDD_DEFAULT__LOW  -- Lower than the standard default Vdd
 *                - PM_SMBC_VDD_DEFAULT__STANDARD -- Standard default Vdd @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_default_vdd(int externalResourceIndex,
                                         pm_smbc_vdd_default_type vdd);

/**
 * Sets the battery temperature configurations, such as the
 * ground connection for the battery thermistor, cold battery
 * temperature threshold, and hot battery temperature threshold.
 *
 * @note1hang The default settings are device specific.
 *
 * @datatypes
 * #pm_smbc_batt_temp_ref_gnd_type \n
 * #pm_smbc_batt_temp_cold_thr_type \n
 * #pm_smbc_batt_temp_hot_thr_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] ref_gnd Ground connection for the battery thermistor.
 * @param [in] cold_thr Cold battery temperature threshold.
 * @param [in] hot_thr Hot battery temperature threshold.
 *
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par 
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE -- 
 * Input parameter three is out of range. 
 * @par
 * PM_ERR_FLAG__PAR4_OUT_OF_RANGE -- 
 * Input parameter four is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_batt_temp_config(int externalResourceIndex,
                                      pm_smbc_batt_temp_ref_gnd_type  ref_gnd,
                                      pm_smbc_batt_temp_cold_thr_type cold_thr,
                                      pm_smbc_batt_temp_hot_thr_type  hot_thr);

/**
 * Gets the ground connection for the battery thermistor.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_smbc_batt_temp_ref_gnd_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] ref_gnd Pointer to the power source.
 *            - PM_SMBC_BATT_TEMP_REF_GND__PACK -- Battery thermistor is inside the
 *                                                 battery pack
 *            - PM_SMBC_BATT_TEMP_REF_GND__GND -- Battery thermistor is not in the
 *                                                battery pack @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_batt_therm_gnd(int externalResourceIndex,
                                     pm_smbc_batt_temp_ref_gnd_type*  ref_gnd);
/**
 * Selects the power source for the anode of the charge indicator LED.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 * 
 * @datatypes
 * #pm_smbc_led_source_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] source Power source.
 *                   - PM_SMBC_LED_SRC__GROUND -- LED gets no power
 *                   - PM_SMBC_LED_SRC__VPH_PWR --LED gets power from the
 *                      main phone voltage rail (VPH_PWR)
 *                   - PM_SMBC_LED_SRC__BOOST -- LED gets power from a specific
 *                      boost regulator
 *                   - PM_SMBC_LED_SRC__INTERNAL -- LED gets power from the
 *                      SMBC module internally @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_led_source(int externalResourceIndex,
                                        pm_smbc_led_source_type source);

/**
 * Checks whether the battery is weak by checking the battery voltage against 
 * the weak battery threshold. This threshold is set by 
 * pm_smbc_set_weak_vbatt_threshold().
 
 *
 * @param [in] externalResourceIndex The external SMBC ID.
 * @param [out] weak Pointer to the battery-weak status.
 *                  - TRUE -- Battery is weak (below the threshold)
 *                  - FALSE -- Battery is normal (above the threshold) @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct.
 * @par 
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_battery_weak(int externalResourceIndex,
                                          boolean *weak);

/**
 * Gets the attached charger type.
 *
 * @datatypes
 * #pm_smbc_attached_chgr_type
 *
 * @param [in] externalResourceIndex External SMBC ID. 
 * @param [out] type Pointer to the attached charger type.
 *                  - PM_SMBC_ATTACHED_CHGR__NONE -- No charger is attached
 *                  - PM_SMBC_ATTACHED_CHGR__USB --  USB device is attached
 *                  - PM_SMBC_ATTACHED_CHGR__DCIN -- DC wall charger is attached
 *                  - PM_SMBC_ATTACHED_CHGR__BOTH -- Both a USB device and a DC wall
 *                     charger are attached @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_attached_charger(int externalResourceIndex,
                                              pm_smbc_attached_chgr_type *type);

/**
 * Checks if the battery is present.
 *
 * @param [in] externalResourceIndex External SMBC ID. 
 * @param [out] present Pointer to the status of the battery presence.
 *                      - TRUE -- Battery is present
 *                      - FALSE -- Battery is not present @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par 
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_battery_present(int externalResourceIndex,
                                             boolean *present);

/**
 * Sets the maximum USB_IN current. The  USB_IN current is the current 
 * going through the USB cable.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_ma Current value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mA value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_iusb_max(int externalResourceIndex,
                                      uint32 value_ma);

/**
 * Sets the maximum Vdd. The Vdd is the power supply to the phone.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_mv Vdd value in mV. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mV value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 *
 */
pm_err_flag_type pm_smbc_set_vdd_max(int externalResourceIndex,
                                     uint32 value_mv);

/**
 * Sets the VBATDET (battery voltage detect).
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_mv VBATDET value in mV. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds mV value parameter input.
 *
 * @detdesc 
 * This function sets the VBATDET. The VBATDET comparator is used to restart charging 
 * when the battery voltage is less than the VBATDET value. When the battery voltage is greater than
 * the VBATDET threshold, charging stops.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_vbatdet(int externalResourceIndex,
                                     uint32 value_mv);

/**
 * Sets the maximum fast charge battery current limit.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_ma Maximum IBAT value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mA value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_ibat_max(int externalResourceIndex,
                                      uint32 value_ma);

/**
 * Sets the minimum charger input voltage.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_mv Minimum input voltage regulation value in mV. \n
 *      @note1 If this value is out of the valid range that is acceptable
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mV value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 *
 */
pm_err_flag_type pm_smbc_set_vin_min(int externalResourceIndex,
                                     uint32 value_mv);

/**
 * Gets the battery current sense resistor value.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID. 
 * @param [out] value_mOhm Battery current sense resistor value in mOhm.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_batt_sense_r(int externalResourceIndex,
                                          uint32* value_mOhm);

/**
 * Sets the system weak battery threshold. This is the battery threshold 
 * at which the high current trickle charging starts.
 * 
 * @note1hang The default setting the in PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex The external SMBC ID.
 * @param [in] value_mv System weak battery threshold value in mV. \n
 *      @note1 If this value is out of the valid range that is acceptable
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds mV value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_weak_vbatt_threshold(int externalResourceIndex,
                                                  uint32 value_mv);

/**
 * Sets the low battery threshold. This is the battery threshold at which 
 * the low current Auto Trickle Charging (ATC) starts.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_mv System low battery threshold value in mV. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds mV value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_low_vbatt_threshold(int externalResourceIndex,
                                                 uint32 value_mv);

/**
 * Sets the system trickle charging current.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_ma System trickle charging current value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds mA value parameter input.
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_itrkl(int externalResourceIndex,
                                   uint32 value_ma);

/**
 * Sets the system End-Of-Charge (EOC) current. This is the current used 
 * to generate the EOC interrupt and terminate charging.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_ma System EOC current value in mA. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there 
 *             was an out-of-bounds mA value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par 
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_iterm(int externalResourceIndex, uint32 value_ma);

/**
 * Sets the maximum time for initiated trickle charging,
 * including software-initiated trickle charging as well as hardware
 * controlled ATC.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_minutes Maximum time value in minutes for trickle charging. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds minute value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_ttrkl(int externalResourceIndex,
                                   uint32 value_minutes);

/**
 * Sets the maximum time for software-initiated charging,
 * including trickle and fast phases.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] value_minutes Maximum time value in minutes for software-initiated
 *                          charging. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds minute value parameter input.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par 
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 *
 */
pm_err_flag_type pm_smbc_set_tchg(int externalResourceIndex,
                                  uint32 value_minutes);

/**
 * Sets the delay and the timeout for the hardware watchdog
 * timer in the SMBC module.
 *
 * @note1hang The default setting in the PMIC hardware is device specific. 
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] delay_seconds Delay in seconds between the watchdog IRQ and stop charging. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds second value parameter input. 
 * @param [in] timeout_seconds Watchdog timer timeout value in seconds. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR3_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds second value parameter input.
 *
 * @detdesc
 * This function sets the delay between the hardware watchdog IRQ and stop
 * charging, and the timeout for the hardware watchdog timer in the PMIC
 * SMBC module. The software-initiated charging stops when this
 * hardware timer expires. This API sets the timeout in order to
 * restart the hardware watchdog timer (pet the dog). This timer can be
 * used along with a software periodic timer (for example, heartbeat)
 * to ensure that charging stops in time when the software crashes.
 * The timeout of the software periodic timer should be set a little
 * shorter than the timeout of the hardware timer to be able
 * to restart the hardware watchdog timer before it times out.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * @newpage 
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__PAR3_OUT_OF_RANGE -- 
 * Input parameter three is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_twdog(int externalResourceIndex,
                                   uint32 delay_seconds,
                                   uint32 timeout_seconds);

/**
 * Sets the charging temperature thresholds.
 *
 * @note1hang The default settings are device specific.
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] stop_deg_c Stop charging temperature value in degrees Celsius. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC harware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR2_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds degrees Celsius value parameter input.
 * @param [in] resume_deg_c Resume charging temperature value in degrees Celsius. \n
 *      @note1 If this value is out of the valid range that is acceptable 
 *             by the PMIC hardware, the value is limited to the boundary 
 *             and the PMIC hardware is set to that value. At the same time,
 *             PM_ERR_FLAG__PAR3_OUT_OF_RANGE is returned to indicate there
 *             was an out-of-bounds degrees Celsius value parameter input.
 *
 * @detdesc 
 * Charging stops (pass device turns OFF) when the charging temperature is 
 * higher than the maximum charging temperature threshold. Charging resumes
 * (pass device turns ON) when the charging temperature has fallen below the
 * resume charging temperature threshold.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success. 
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_temp_threshold(int externalResourceIndex,
                                            uint32 stop_deg_c,
                                            uint32 resume_deg_c);

/**
 * Sets the charging path. The charging path is either the DC wall charger 
 * or USB, but not both at the same time.
 *
 * @note1hang The default setting in the PMIC hardware is device specific.
 *
 * @datatypes
 * #pm_smbc_chg_path_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] path Desired charging path.
 *                - PM_SMBC_CHGR_PATH__USB -- USB device
 *                - PM_SMBC_CHGR_PATH__DCIN -- DC wall charger @tablebulletend
 *
 *
 * @return
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE--
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_charge_path(int externalResourceIndex,
                                         pm_smbc_chg_path_type path);

/**
 * Gets the charging path set by pm_smbc_set_charge_path().
 *
 * @datatypes
 * #pm_smbc_chg_path_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 *
 * @param [out] path Pointer to the current charging path.
 *                - PM_SMBC_CHGR_PATH__USB -- USB device
 *                - PM_SMBC_CHGR_PATH__DCIN -- DC wall charger @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_charge_path(int externalResourceIndex,
                                         pm_smbc_chg_path_type *path);

/**
 * Sets the Finite State Machine (FSM) charge state.
 *
 * @datatypes
 * #pm_smbc_fsm_state_name_type
 *
 * @param [in] externalResourceIndex External SMBC ID.
 * @param [in] state Desired FSM charge state.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED -- 
 * The internal resource ID for the SMBC module is not correct. 
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE -- 
 * Input parameter two is out of range. 
 * @par
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_set_charge_state(int externalResourceIndex,
                                          pm_smbc_fsm_state_name_type state);

/**
 * Gets the current FSM charge state.
 *
 * @datatypes
 * #pm_smbc_fsm_state_name_type
 *
 * @param [in] externalResourceIndex External SMBC ID. 
 * @param [out] state Pointer to the current FSM charge state.
 *
 * @return 
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__TARGET_PROCESSOR_CAN_NOT_ACCESS_RESOURCE --
 * The current processor does not have access to the SMBC module.
 * @par
 * PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED --
 * The external resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_SMBC_INDEXED --
 * The internal resource ID for the SMBC module is not correct.
 * @par
 * PM_ERR_FLAG__INVALID_POINTER -- 
 * Input parameter two is a NULL pointer.
 * @par
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 *
 * @newpage
 */
pm_err_flag_type pm_smbc_get_charge_state(int externalResourceIndex,
                                          pm_smbc_fsm_state_name_type *state);
/** @} */ /* end_addtogroup pm_smbc */

#endif /* PM_SMBC_H */

