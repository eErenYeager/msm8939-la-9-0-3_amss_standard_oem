#ifndef PM_ITEMP_H
#define PM_ITEMP_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_itemp.h PMIC INTERNAL TEMPERATURE driver related declaration.
 *
 * @brief This file is defines error codes returned by PMIC internal
 *		  temperature driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.7.24/api/systemdrivers/pmic/pm_itemp.h#1 $
$DateTime: 2015/01/27 06:04:57 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/14   crm     (Tech Pubs) Edited/added Doxygen comments and markup.
02/27/14   mr      Doxygen complaint PMIC Header (CR-602405)
09/01/09   dy      New API to configure overtemp threshold
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
06/30/09   jtn     Updated file documentation
05/01/09   jtn     New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/

/*===========================================================================


#include "comdef.h"
#include "pm_err_flags.h"
#include "pm_lib_cmd.h"

                        TYPE DEFINITIONS

===========================================================================*/
/** @addtogroup pm_itemp
@{ */
/**
 * @enum pm_item_stage_type
 * @brief Temperature protection stage.
 */
typedef enum
{
    PM_ITEMP_STAGE0,  /*!< Stage 0.  */
    PM_ITEMP_STAGE1,  /*!< Stage 1.  */
    PM_ITEMP_STAGE2,  /*!< Stage 2.  */
    PM_ITEMP_STAGE3   /*!< Stage 3.  */
}pm_item_stage_type;

/**
 * @enum pm_item_oride_type
 * @brief Temperature protection override.
 */
typedef enum
{
    PM_ITEMP_ORIDE_STAGE2,      /*!< Stage 2.  */
    PM_ITEMP_ORIDE_OUT_OF_RANGE /*!< Out of range.  */
}pm_item_oride_type;

/**
 * @enum pm_itemp_threshold_type
 * @brief Temperature protection threshold.
 */
typedef enum
{
    PM_TEMP_THRESH_CTRL0,  /*!< Control 0.  */
    PM_TEMP_THRESH_CTRL1,  /*!< Control 1.  */
    PM_TEMP_THRESH_CTRL2,  /*!< Control 2.  */
    PM_TEMP_THRESH_CTRL3,  /*!< Control 3.  */
    PM_INVALID_INPUT       /*!< Invalid value.  */
} pm_itemp_threshold_type;


/*===========================================================================

                 TEMPERATURE PROTECTION FUNCTION PROTOTYPES

===========================================================================*/
/**
 * Returns the current over temperature protection stage
 * of the PMIC.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * #pm_item_stage_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [out] *itemp_stage Pointer to the current over-temperature stage of the PMIC. 
 
 *  @detdesc 
 *  This function returns the current over-temperature protection stage
 *  of the PMIC. A change of stage can be detected by enabling or pulling
 *  the Temperature Status Changed IRQ.
 *  Temperature protection stages:
 *      - Stage 0:
 *         - Temperature range -- T < 110C
 *         - Normal condition
 *      - Stage 1:
 *         - Temperature range -- 110C > T < 130C
 *         - Over temperature warning, no action
 *      - Stage 2:
 *         - Temperature range -- 130C > T < 150C
 *         - Shuts down high current drivers such as speaker and LED
 *                drivers automatically. Can be overridden via
 *                pm_itemp_stage_override().
 *      - Stage 3:
 *         - Temperature range -- T > 150C
 *         - Powers down the PMIC @tablebulletend
 *
 * @newpage 
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED --
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SPMI errors.
 *
 * @newpage
 */
/* Note: Do not use pm_itemp_get_stage(), which is deprecated. Use pm_dev_itemp_get_stage() instead. 
*/
pm_err_flag_type pm_dev_itemp_get_stage(unsigned pmic_device_index,
                                        pm_item_stage_type *itemp_stage );
pm_err_flag_type pm_itemp_get_stage( pm_item_stage_type *itemp_stage );

/**
 * Overrides the automatic shutdown of the temperature
 * protection stage.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * #pm_switch_cmd_type \n
 * #pm_item_oride_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] oride_cmd Enable/disable automatic shutdown. 
 *             - PM_OFF_CMD (default) -- 
 *               Do not override automatic shutdown
 *             - PM_ON_CMD -- 
 *               Override automatic shutdown @tablebulletend
 * @param [in] oride_stage Stage to configure. 
 *             - PM_ITEMP_ORIDE_STAGE2 --
 *               Only stage 2 can be overridden @tablebulletend
 *
 * @return
 * PM_ERR_FLAG__SUCCESS --
 * Success.
 * @par
 * PM_ERR_FLAG__PAR1_OUT_OF_RANGE --
 * Input parameter one is out of range.
 * @par
 * PM_ERR_FLAG__PAR2_OUT_OF_RANGE --
 * Input parameter two is out of range.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR --
 * SPMI errors.
 *
 * @newpage
 */
/* Note: Do not use pm_itemp_stage_override(), which is deprecated. Use pm_dev_itemp_stage_override() instead. 
*/
pm_err_flag_type pm_dev_itemp_stage_override(unsigned pmic_device_index,
                                             pm_switch_cmd_type oride_cmd,
                                             pm_item_oride_type oride_stage);
pm_err_flag_type pm_itemp_stage_override(pm_switch_cmd_type oride_cmd,
                                         pm_item_oride_type oride_stage);

/**
 * Controls the temperature threshold.
 *
 * @note1hang Interrupts are disabled while communicating with the PMIC.
 *
 * @datatypes
 * #pm_itemp_threshold_type
 *
 * @param [in] pmic_device_index Primary PMIC: 0. Secondary PMIC: 1.
 * @param [in] thresh_value Threshold to configure.
 *             - PM_TEMP_THRESH_CTRL0           -- {105, 125, 145}
 *             - PM_TEMP_THRESH_CTRL1 (default) -- {110, 130, 150}
 *             - PM_TEMP_THRESH_CTRL2           -- {115, 135, 155}
 *             - PM_TEMP_THRESH_CTRL3           -- {120, 140, 160} @tablebulletend
 *
 * @return 
 * PM_ERR_FLAG__SUCCESS -- 
 * Success.
 * @par
 * PM_ERR_FLAG__FEATURE_NOT_SUPPORTED -- 
 * Feature is not available on this version of the PMIC.
 * @par
 * PM_ERR_FLAG__SBI_OPT_ERR -- 
 * SPMI errors. @tablebulletend
 *
 * @newpage
 */
 /* Note: Do not use pm_itemp_thresh_cntrl(), which is deprecated. Use pm_dev_itemp_thresh_cntrl() instead. */
pm_err_flag_type pm_dev_itemp_thresh_cntrl(unsigned pmic_device_index,
                                           pm_itemp_threshold_type thresh_value );
pm_err_flag_type pm_itemp_thresh_cntrl( pm_itemp_threshold_type  thresh_value );

/** @} */ /* end_addtogroup pm_itemp */

#endif    /* PM_ITEMP_H */

