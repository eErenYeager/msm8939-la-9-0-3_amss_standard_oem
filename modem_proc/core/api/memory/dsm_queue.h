#ifndef DSM_QUEUE_H
#define DSM_QUEUE_H
/*===========================================================================

                                  D S M _ Q U E U E . H

DESCRIPTION
  This file contains types and declarations associated with the DMSS Data
  Service Memory pool and services.

Copyright (c) 2009 - 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.7.24/api/memory/dsm_queue.h#1 $

when        who    what, where, why
--------    ---    ---------------------------------------------------------- 
09/05/12    sh     New dsm_priority value added for dsm_enqueue
06/06/12    sh     Added dne_statistics
06/06/12    sh     Added dne_callback
06/06/12    sh     Added priority insert optimization 
04/06/11    rj     Internalize private fields within watermark structure   
03/08/11    ag     Remove FEATURE_DSM_WM_CB
07/24/09    ag     Updation of hi,lo,dne,empty event counts for WM tracing.
06/12/09    ag     Updation of min max counts for watermark tracing.  
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "queue.h"


/*===========================================================================
                        DATA DECLARATIONS
===========================================================================*/

/*---------------------------------------------------------------------------
  Watermark Type definition. This structure provides the means for 2 software
  entities to convey data and control its flow. Note that the function
  pointer fields must 1st be checked to non-NULL prior to invocation.
---------------------------------------------------------------------------*/

struct dsm_watermark_type_s; 
typedef void(*wm_cb_type)(struct dsm_watermark_type_s *, void *);

typedef struct dsm_watermark_type_s
{
  uint32  lo_watermark;		/* Low watermark for total bytes */
  uint32  hi_watermark;		/* Hi watermark for total bytes */
  uint32  dont_exceed_cnt;	/* Discard count for RX bytes */
  uint32  dne_q_cnt;        /* Discard count for WM Packet count (q_cnt)*/
  uint32  current_cnt;		/* Current total byte count */
  uint32  highest_cnt;		/* largest count since powerupp */
  uint64  total_rcvd_cnt;    /* Total RX bytes thru watermark */

  /* Statistics */
  uint32  dne_discard_pkt_cnt; /* Total pkts discarded when dne cnt is hit */
  uint32  dne_q_cnt_discard_pkt_cnt; /* Total pkts discarded when dne_q_cnt is hit */
  uint32  dne_discard_bytes_cnt;/* Total byte cnt in dne_discard_pkt(s) */
  uint32  dne_q_cnt_discard_bytes_cnt;/* Total byte cnt when dne_q_cnt is hit  */

  wm_cb_type each_enqueue_func_ptr; /* Called when any item enqueued  */ 
  void * each_enqueue_func_data;

  wm_cb_type each_dequeue_func_ptr; /* Called when any item dequeued  */ 
  void * each_dequeue_func_data;

  wm_cb_type lowater_func_ptr;	/* Lo Watermark function pointer, Invoke
                                   function when queue hits the Lo watermrk*/
  void * lowater_func_data;

  wm_cb_type gone_empty_func_ptr; /* Gone empty function pointer, invoke when
                                     queue goes empty */
  void * gone_empty_func_data;

  wm_cb_type non_empty_func_ptr; /* Non-empty function pointer, Invoke when
                                    queue goes nonempty */
  void * non_empty_func_data;

  wm_cb_type hiwater_func_ptr;   /* Hi Watermark function pointer, Invoke
                                    function when queue hits the Hi wm     */
  void * hiwater_func_data;

  wm_cb_type dne_func_ptr;   /* Do Not Exceed Wm function pointer, Invoke 
                                    function, when DNE count is reached 
                                    in enqueue(), before dsm pkt is deleted.
                                    parameters - wm ptr,enqueued dsm pkt */
                                                   
  q_type *q_ptr;		 /* Pointer to associated queue */

  q_link_type *q_prio_insert_ptr; /* Position in Q to insert DSM_HIGHEST prio item*/
                                  /* Pointer to link item to insert AFTER */
                                      
  boolean       lock_init;       /* Is lock init'd? */
  dword lock;            /* Lock for the queue */

  uint32 sentinel;               /* cookie to verify initialization */  

  /* START - Fields used only for debugging/profiling watermarks */
  /* Important : Do NOT USE  these fields for regular WM operation.
        May be overwritten as and when needed. */        
  wm_cb_type log_enq_func_ptr; /* each enqueue log callback*/
  wm_cb_type log_deq_func_ptr; /* each dequeue log callback */
  uint32 reserved [3];  /* Initialized with file, line, WM ID.*/
  
  /* END - Fields used only for debugging/profiling watermarks*/


} dsm_watermark_type;



/*---------------------------------------------------------------------------
   Watermark counts structure which will be used to store all the count
   values for watermark tracing. This structure will be populated each
   time a caller calls dsm_wm_get_all_queue_cnts() for a particular
   watermark. The caller is expected to allocate memory for holding
   this structure.
---------------------------------------------------------------------------*/

typedef struct dsm_wm_counts_s
{
  uint32  lo_watermark;                /* Low watermark for total bytes */

  uint32  hi_watermark;                /* Hi watermark for total bytes  */

  uint32  dont_exceed_cnt;             /* Discard count for RX bytes    */

  uint32  current_cnt;                 /* Current total byte count      */

  uint32  highest_cnt;                 /* largest count since powerupp  */

  uint64  total_rcvd_cnt;              /* Total RX bytes thru watermark */

  int     q_cnt;                       /* Count of packets in the queue */

  uint32  min_cnt;                     /* Minimum item count maintained 
                                          for each time period          */

  uint32  max_cnt;                     /* Maximum item count maintained 
                                          for each time period          */

  uint8  hi_event_cnt;                 /* Number of times high watermark is 
                                          hit during a period. */

  uint8  lo_event_cnt;                 /* Number of times low watermark is 
                                          hit during a period. */

  uint8  dne_event_cnt;                /* Number of times dne count is 
                                          reached during a period. */

  uint8  empty_event_cnt;             /* Number of times the watermark goes
                                         empty during a period. */

} dsm_wm_counts_type;

/*---------------------------------------------------------------------------
  Priority Field Values
  ------------------------------------------------
  Priority values determine how items are enqueued in WM Queue 
  Items are dequeued from head of WM queue, irrespective of priority value.
---------------------------------------------------------------------------*/

  #define DSM_NORMAL          0
  /* DSM_NORMAL priority item is enqueued to tail of WM Queue - 
        follows FIFO rule for dequeueing these items */   
  
  #define DSM_HIGHEST         0x1
  /* DSM_HIGHEST priority item is enqueued to head of WM Queue, 
        before DSM_NORMAL priority items ,
        after DSM_PRIORITY_ENQ_HIGHEST items and 
        after previously inserted DSM_HIGHEST priority items.
        - follows FIFO rule for dequeueing DSM_HIGHEST priority items*/

  #define DSM_PRIORITY_ENQ_HIGHEST  0x2
  /* DSM_PRIORITY_ENQ_HIGHEST priority items enqueued
        to head of WM queue, irrespective of other items in queue. 
        - follows LIFO rule for dequeuing these items */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  typedef enum
  {
    DSM_WM_ENQ_RULE_IGNORE_DNE = 0,
    DSM_WM_ENQ_RULE_MAX = 1
  } dsm_wm_enq_rule_enum;

  typedef enum
  {
    DSM_WM_REQUEST_NONE = 0,
    DSM_WM_DEQUEUE_PKT = 1,
    DSM_WM_MODIFY_PKT = 2,
    DSM_WM_MODIFY_AND_DEQUEUE_PKT = 3,
    DSM_WM_REQUEST_MAX = 3
  } dsm_wm_iterator_req_enum_type;

  typedef enum
  {
    DSM_WM_ITERATOR_STOP = 0,
    DSM_WM_ITERATOR_CONTINUE = 1
  }dsm_wm_iterator_enum_type;

  typedef enum
  {
    DSM_WM_ITERATOR_HEAD = 0,
    DSM_WM_ITERATOR_NORMAL_PRIORITY_HEAD = 1
  } dsm_start_iterator_enum_type;


  typedef dsm_wm_iterator_enum_type (*wm_iterator_cb_type)
          ( const dsm_watermark_type *wm_ptr,
            const dsm_item_type *pkt_head_ptr,
            dsm_wm_iterator_req_enum_type *req_type,
            void* cb_usr_data);

  typedef void (*wm_iterator_modifier_cb_type)
          ( const dsm_watermark_type *wm_ptr,
            dsm_item_type *pkt_head_ptr,
            void* cb_usr_data);


#define DSM_GET_WM_CURRENT_CNT(wm_ptr) \
  (((wm_ptr)->current_cnt)) 



/* if using dsm_wm logging */
#ifndef FEATURE_DSM_WM_TRACING

#define DSMI_GET_WM_TRACE_FILE(wm_ptr) \
  ((uint32)((wm_ptr)->reserved[0])) 

#define DSMI_GET_WM_TRACE_LINE(wm_ptr) \
    ((uint32)((wm_ptr)->reserved[1])) 

#define DSMI_GET_WM_TRACE_ID(wm_ptr) \
    ((uint32)((wm_ptr)->reserved[2])) 

#endif


/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/


/*================ QUEUE MANAGMENT FUNCTIONS ==============================*/
/*===========================================================================
FUNCTION DSM_QUEUE_INIT()

DESCRIPTION
   This function initalizes a watermark queue.  Setting all the callbacks and 
   callback data to NULL, watermark levels to 0, and initializing the queue 
   that this will use.  Since this potentially initializes a mutex, the
   dsm_queue_destroy() function needs to be called to tear down a WM queue.


DEPENDENCIES
   None

PARAMETERS
   wm_ptr - Pointer to the watermark to initialize
   dne - Do not exceed level for this watermark
   queue - Pointer to the queue header that this water mark should use

RETURN VALUE
   None

SIDE EFFECTS
   Queue is initialized
===========================================================================*/

#define dsm_queue_init(wm_ptr,dne,queue) \
  dsmi_queue_init(wm_ptr,dne,queue,dsm_file,__LINE__)

extern void dsmi_queue_init
(
  dsm_watermark_type *wm_ptr,
  int dne,
  q_type * queue,
  const char* file,
  uint16 line
);


  
/*===========================================================================
FUNCTION DSM_ENQUEUE()

DESCRIPTION
  This function will put the passed DSM item to the passed shared queue then
  check for and perform any 'put' events.

DEPENDENCIES
  1) Both parameters must NOT be NULL.
  2) The prioritized queuing will always put a DSM_HIGHEST priority item to
     the head of the queue.

PARAMETERS
  wm_ptr - Pinter to Watermark item to put to
  pkt_head_ptr - Pointer to pointer to item to add to queue

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
#define dsm_enqueue(wm_ptr,pkt_head_ptr) \
  dsmi_enqueue(wm_ptr,pkt_head_ptr,dsm_file,__LINE__)

extern void dsmi_enqueue
(
  dsm_watermark_type *wm_ptr,
  dsm_item_type **pkt_head_ptr,
  const char * file,
  uint32 line
);


/*===========================================================================
FUNCTION DSM_SIMPLE_ENQUEUE_ISR()

DESCRIPTION
  This function will put the passed DSM item to the passed shared queue then
  check for and perform any 'put' events.  This function does not check
  for priority.  It simply enqueues to the tail of the queue.

DEPENDENCIES
  1) Both parameters must NOT be NULL.
  2) Does not support packet chaining.
  3) Should only be called from ISR or from within critical section in which
     interrupts are disabled.

PARAMETERS
  wm_ptr - Pointer to watermark to put to
  pkt_head_ptr - Pointer to pointer to item to put.

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
#define dsm_simple_enqueue_isr(wm_ptr,pkt_head_ptr) \
  dsmi_simple_enqueue_isr(wm_ptr,pkt_head_ptr,dsm_file,__LINE__)

extern void dsmi_simple_enqueue_isr
(
  dsm_watermark_type *wm_ptr,
  dsm_item_type **pkt_head_ptr,
  const char * file,
  uint32 line
);

/*==================================================================
FUNCTION DSM_ENQUEUE_SPECIAL()

DESCRIPTION
  This function will handle special cases for dsm_enqueue function. 

DEPENDENCIES
  1) Both parameters must NOT be NULL.

PARAMETERS
  wm_ptr - Pinter to Watermark item to put to
  pkt_head_ptr - Pointer to pointer to item to add to queue
  dsm_enqueue_rule � Enum that specifies overriding DSM enqueue rule 
                                   for this enqueue operation

RETURN VALUE
  None

SIDE EFFECTS:
  This function could break rules of WM including DNE , HI_WM and LO_WM setting and behaviour.
  Behaviour will be based on DSM_ENQUEUE rule for this enqueue operation.
  
  IMPORTANT : Should be used only for very special cases. Avoid using this function for 
   regular DSM WM enqueue operation, use dsm_enqueue() instead.
===================================================================*/
#define dsm_enqueue_special(wm_ptr,pkt_head_ptr,dsm_enqueue_rule) \
  dsmi_enqueue_special(wm_ptr,pkt_head_ptr, dsm_enqueue_rule, dsm_file,__LINE__)

extern void dsmi_enqueue_special
(
  dsm_watermark_type *wm_ptr,
  dsm_item_type **pkt_head_ptr,
  dsm_wm_enq_rule_enum dsm_enqueue_rule,
  const char * file,
  uint32 line
);


/*===========================================================================
FUNCTION DSM_DEQUEUE()

DESCRIPTION
  This function will return a pointer to the next item on the shared queue
  associated with the passed Watermark item. This function will also update
  the 'current_cnt' field in the passed Watermark item and check for and
  perform any relevent 'get' events.

DEPENDENCIES
  The parameter must NOT be NULL.

PARAMETERS
  wm_ptr - Pointer to watermark item to get item from 

RETURN VALUE
  A pointer to a 'dsm_item_type' or NULL if no item_array available.

SIDE EFFECTS
  None
===========================================================================*/
#define dsm_dequeue(wm_ptr) \
  dsmi_dequeue(wm_ptr,dsm_file,__LINE__)

extern dsm_item_type *dsmi_dequeue
(
  dsm_watermark_type *wm_ptr,
  const char * file, 
  uint32 line
);

/*===========================================================================
FUNCTION DSM_EMPTY_QUEUE()

DESCRIPTION
  This routine completely empties a queue.
  
DEPENDENCIES
  None

PARAMETERS
  wm_ptr - Pointer to watermark queue to empty

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
#define dsm_empty_queue(wm_ptr) \
  dsmi_empty_queue(wm_ptr,dsm_file,__LINE__)

extern void dsmi_empty_queue
( 
  dsm_watermark_type *wm_ptr,
  const char * file,
  uint32 line
);

/*===========================================================================
FUNCTION DSM_IS_WM_EMPTY()

DESCRIPTION
 This routine determines whether the input watermark has data queued in
 it or not.

DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark

RETURN VALUE
 TRUE if watermark has no data queued in it, FALSE if it does

SIDE EFFECTS
 None
===========================================================================*/
boolean dsm_is_wm_empty
(
 dsm_watermark_type *wm_ptr
);

/*===========================================================================
FUNCTION DSM_WM_QUEUE_GET()

DESCRIPTION
  Get the front item of the given dsm queue. Item is NOT removed from queue.

DEPENDENCIES
  The parameter must NOT be NULL.

PARAMETERS
  wm_ptr - Pointer to watermark item to get item from 

RETURN VALUE
  A pointer to a 'dsm_item_type' or NULL if no item_array available.

SIDE EFFECTS
  The function provides additional checks on queue's current_cnt. However
  it is not recommended to use the function for error checking or debugging.
===========================================================================*/
dsm_item_type *dsm_wm_queue_get
(
 dsm_watermark_type *wm_ptr
);


/*===========================================================================
FUNCTION DSM_DEQUEUE_NORMAL_PRIO_HEAD()

DESCRIPTION
  The function returns head of Normal PRIORITY ITEMS

DEPENDENCIES
  None

PARAMETERS
  wm_ptr - Pointer to watermark queue for iteration and optional dequeue

RETURN VALUE
  Dequeued Normal Priority Head

SIDE EFFECTS
  None
===========================================================================*/
#define dsm_dequeue_normal_prio_head(wm_ptr) \
		dsmi_dequeue_normal_prio_head(wm_ptr, \
         dsm_file,\
         __LINE__)
extern  dsm_item_type *dsmi_dequeue_normal_prio_head
( dsm_watermark_type *wm_ptr,
  const char * file,
  uint32 line
);


/*===========================================================================
FUNCTION dsmi_wm_iterator_with_dequeue_modify_pkts()

DESCRIPTION
  The function provides an iterator over DSM WM queue with optional dequeue and/or
  modification of enqueued WM packets from any location in WM queue.
  If the enqueued packets are modified, necessary sanity checks are done,
    and the trace of enqueued packets are updated.

  Iterator traverses queue from head or from normal priority head of the queue.

  Iteration on queue is stopped
     1) when q_tail is reached
     2) when client iterator request to stop

DEPENDENCIES
  None

PARAMETERS
  wm_ptr - Pointer to watermark queue for iteration and optional dequeue
  start_iter - Defines where to start the iteration from
  dsm_wm_iterator_cb - User provided iterator function.
     User Function evaluates the packet provided to it and provides input
     for continuing the iteration and to optionally dequeue the packet
     dsm_wm_iterator_enum_type (*dsm_wm_iterator_cb_type)
       (  const dsm_watermark_type *wm_ptr, <= Pointer to Watermark
          const dsm_item_type *pkt_head_ptr,<= Queue Packet passed by DSM iterator
          dsm_wm_iterator_req_enum_type *req_type, <= Set by client to indicate dequeue,modify
          void* cb_usr_data) <= cb_usr_data provided by usr
  dsm_wm_iterator_modifier_cb <= Callback provided by client for modification of
          enqueued packets
          ( const dsm_watermark_type *wm_ptr,<= Pointer to Watermark
            dsm_item_type *pkt_head_ptr,<= Queue Packet passed by DSM iterator, modifiable
            void* cb_usr_data); <= cb_usr_data provided by usr
  cb_usr_data - Passed to dsm_wm_iterator_f_cb

RETURN VALUE
  Chain of dequeued dsm packets, linked using (dsm_item_type)pkt.link->next_ptr.
  Order of dequeue is maintained in the chain.
SIDE EFFECTS
  DSM Packets may be dequeued from the WM queue from any location
  Enqueued DSM Packets may be MODIFIED without being dequeued.

  This function involves queue traversal and modifying enqueued
  DSM Packets and should be used with caution.
  The function can be also used to dequeue all items from the queue. It is up
  to client to limit number of dequeues, to avoid locking WM for longer duration.
===========================================================================*/
#define dsm_wm_iterator_with_dequeue_modify_pkts(wm_ptr,start_iter,dsm_wm_iterator_cb,\
                         dsm_wm_iterator_modifier_cb, cb_usr_data) \
        dsmi_wm_iterator_with_dequeue_modify_pkts(wm_ptr, \
         start_iter, dsm_wm_iterator_cb, dsm_wm_iterator_modifier_cb, cb_usr_data,\
         dsm_file, __LINE__)

extern  dsm_item_type *dsmi_wm_iterator_with_dequeue_modify_pkts
( dsm_watermark_type *wm_ptr,
  dsm_start_iterator_enum_type start_iter,
  wm_iterator_cb_type dsm_wm_iterator_cb,
  wm_iterator_modifier_cb_type dsm_wm_iterator_modifier_cb,
  void* cb_usr_data,
  const char * file,
  uint32 line
);



/*===========================================================================
FUNCTION DSM_SET_LOW_WM()

DESCRIPTION
 This routine resets the low watermark value. This change may trigger
 watermark callbacks.

DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark
 val    - New value for low watermark.

RETURN VALUE
 TRUE if watermark has no data queued in it, FALSE if it does

SIDE EFFECTS
 None
===========================================================================*/
void dsm_set_low_wm
(
  dsm_watermark_type *wm_ptr,
  uint32             val
);

/*===========================================================================
FUNCTION DSM_SET_HI_WM()

DESCRIPTION
 This routine resets the high watermark value. This change may trigger
 watermark callbacks.

DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark
 val    - New value for hi watermark.

RETURN VALUE
 None.

SIDE EFFECTS
 WB callback triggered. Function locks the context for the extent of the
 callback.
===========================================================================*/
void dsm_set_hi_wm
(
  dsm_watermark_type *wm_ptr,
  uint32             val
);

/*===========================================================================
FUNCTION DSM_SET_DNE()

DESCRIPTION
 This routine resets the DNE (do not exceed) value.
 

DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark
 val    - New value for hi watermark.

RETURN VALUE
 None.

SIDE EFFECTS
 WB callback triggered. Function locks the context for the extent of the
 callback.
===========================================================================*/
void dsm_set_dne
(
  dsm_watermark_type *wm_ptr,
  uint32             val
);


/*===========================================================================
FUNCTION DSM_SET_DNE_Q_CNT()

DESCRIPTION
 This routine resets the dne_q_cnt (do not exceed) Queue Count value.
 

DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark
 val    - New value for dne_q_cnt.

RETURN VALUE
 None.

SIDE EFFECTS
 WB callback triggered. Function locks the context for the extent of the
 callback.
===========================================================================*/
void dsm_set_dne_q_cnt
(
  dsm_watermark_type *wm_ptr,
  uint32             val
);



/*===========================================================================
FUNCTION DSM_QUEUE_CNT()

DESCRIPTION
 Returns the number of bytes on the watermark queue.
 
DEPENDENCIES
 None

PARAMETERS
 wm_ptr - Pointer to a watermark

RETURN VALUE
 Number of bytes recorded on queue.

SIDE EFFECTS
 Locks watermark mutex lock before reading the queue cnt. Use 
 DSM_GET_WM_CURRENT_CNT Macro for most purposes.
===========================================================================*/
uint32 dsm_queue_cnt
(
  dsm_watermark_type *wm_ptr
);

/*===========================================================================
FUNCTION DSM_QUEUE_DESTROY()

DESCRIPTION
   This function tears down a watermark queue.
DEPENDENCIES
   None

PARAMETERS
   wm_ptr - Pointer to the watermark to tear down.

RETURN VALUE
   None

SIDE EFFECTS
   Locks might be destroyed.
   Packets might be freed.
   WM pointer will be non-initialized.
===========================================================================*/
#define dsm_queue_destroy(wm_ptr) \
  dsmi_queue_destroy(wm_ptr,dsm_file,__LINE__)

void dsmi_queue_destroy
(
  dsm_watermark_type *wm_ptr,
  const char * file,
  uint32 line
);

/*===========================================================================
FUNCTION DSM_GET_WM_STATS()

DESCRIPTION
   This function returns the min_cnt, max_cnt, HI, LO, DNE counts of a
   watermark queue. It also resets the min and max counts for the next window
   period.
 
DEPENDENCIES
  If FEATURE_DSM_WM_TRACING is not defined, this function will 
  ERR_FATAL().

PARAMETERS
   wm_ptr - Pointer to the watermark queue.
   wm_cnts - Pointer to the memory location where this function will write
     the min_cnt, max_cnt, HI, LO, DNE counts.

RETURN VALUE
   None

SIDE EFFECTS
   None
===========================================================================*/

void dsm_get_wm_stats
(
  dsm_watermark_type*     wm_ptr,
  dsm_wm_counts_type*     wm_cnts
);

#endif /* DSM_QUEUE_H */
