#ifndef SYS_M_FATAL_H
#define SYS_M_FATAL_H
/*===========================================================================

           S Y S _ M _ F A T A L. H

DESCRIPTION

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================
DESCRIPTION
High level system monitor
===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/api/debugtools/sys_m_fatal.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------


===========================================================================*/

#include "rcecb.h"

void sys_m_xpu_handler_register(RCECB_CONTEXT func);


#endif  /* SYS_M_FATAL_H */
