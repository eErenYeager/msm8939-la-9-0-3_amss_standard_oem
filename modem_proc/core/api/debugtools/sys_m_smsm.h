#ifndef SYS_M_SMSM_H
#define SYS_M_SMSM_H
/*===========================================================================

           S Y S _ M _ S M S M. H

DESCRIPTION

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================
DESCRIPTION
High level system monitor
===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/api/debugtools/sys_m_smsm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/16/14   rks     CR689074:bypass RAM dumps for graceful Modem SSR
12/26/13   rks     CR542178:Modify mechanism for sending smp2p notification of 
                   modem failure to HLOS

===========================================================================*/
#include "comdef.h"

#define SYS_M_SMP2P_SUCCESS         0
#define SYS_M_SMP2P_NOT_READY       1
#define SYS_M_SMP2P_INVALID_PARAM   2

#define SYS_M_AP2SUBSYS_SMSM_ERRFATAL       0x1
#define SYS_M_AP2SUBSYS_SMSM_SHUT_DWN       0x2

#define SYS_M_SUBSYS2AP_SMSM_ERRFATAL       0x1
#define SYS_M_SUBSYS2AP_SMSM_ERR_HDL_RDY    0x2
#define SYS_M_SUBSYS2AP_SMSM_PWR_CLK_RDY    0x4
#define SYS_M_SUBSYS2AP_SMSM_SHUT_DWN_ACK   0x8

#define SYS_M_SUBSYS2AP_SMSM_RAM_DUMP_BYPASS_MODIFIER 0x8000 

/**
  Sets bit in sys_m point to point array to apps

  @param[in] bit       Bit to set
  
  @return
  None.
  
  @dependencies
  None.
*/
int32 sys_m_smsm_apps_set(uint32 bit);

/** 
  Single threaded mode version of above (no mutex operations) 
  Sets bit in sys_m point to point array to apps 
                        
  @return
  Error codes
  
  @dependencies
  None.
*/
int32 sys_m_smsm_apps_set_stm(uint32 bits);


#endif  /* SYS_M_SMSM_H */
