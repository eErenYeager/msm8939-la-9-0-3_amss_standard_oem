#ifndef QMI_CCI_TARGET_EXT_H
#define QMI_CCI_TARGET_EXT_H
/******************************************************************************
  @file    qmi_cci_target_ext.h
  @brief   QMI CCI OS Specific macro/types externalized to clients.

  DESCRIPTION
  QMI OS Specific types and macros required by QCCI clients.

  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
#include "rex.h"

/* OS Specific parameters provided by User on client and 
 * Notifier initialization */
typedef struct {
  /***************************
   *     USER SET MEMBERS    *
   ***************************/

  /* Set this to the TCB of the primary client thread */
  rex_tcb_type  *tcb;

  /* Set this to the event signal mask that will
   * be used internally by QCCI */
  rex_sigs_type  sig;

  /* Set this to the timer signal mask that will
   * be used internally by QCCI to provide the 
   * timeout feature. Set this to 0 if the timeout
   * feature is not required. timer_sig and sig
   * may be the same mask */
  rex_sigs_type  timer_sig;

  /***************************
   *     PRIVATE MEMBERS     *
   ***************************/
  boolean        timer_inited;
  rex_timer_type timer;
  boolean        timed_out;
  boolean        inited;
} qmi_cci_os_signal_type;

#define QMI_CCI_SIGNAL_INITED_SUPPORTED 1

typedef qmi_cci_os_signal_type qmi_client_os_params;

#define QMI_CCI_OS_SIGNAL qmi_cci_os_signal_type

#define is_pow_2(val) (((val) & ((val) - 1)) == 0 ? TRUE : FALSE)

#define QMI_CCI_OS_SIGNAL_INIT(ptr, os_params) \
  do { \
    (ptr)->inited = FALSE;  \
    if(!(os_params) || !(os_params)->tcb || !(os_params)->sig || !is_pow_2((os_params)->sig)) \
      break;  \
    (ptr)->tcb = os_params->tcb; \
    (ptr)->sig = os_params->sig; \
    (ptr)->timer_sig = os_params->timer_sig; \
    (ptr)->timer_inited = FALSE;  \
    (ptr)->timed_out = 0; \
    if((os_params)->timer_sig) { \
      if(is_pow_2((os_params)->timer_sig)) {  \
        (ptr)->timer_inited = TRUE; \
        rex_def_timer(&((ptr)->timer), (ptr)->tcb, (ptr)->timer_sig); \
      } else {  \
        MSG_ERROR("QCCI: Invalid timer signal value: 0x%x for tcb:0x%x used in initialization. Timeout will be disabled", (ptr)->timer_sig, (ptr)->tcb, 0); \
      } \
    } \
    (ptr)->inited = TRUE; \
  } while(0)

#define QMI_CCI_OS_SIGNAL_DEINIT(ptr) \
  do {  \
    if(!(ptr)->inited)  \
      break;  \
    (ptr)->tcb = NULL;  \
    (ptr)->sig = 0;     \
    if((ptr)->timer_inited) \
      rex_clr_timer(&((ptr)->timer)); \
  } while(0)

#define QMI_CCI_OS_SIGNAL_CLEAR(ptr) \
  do {  \
    if(!(ptr)->inited)  \
      break;  \
    rex_clr_sigs((ptr)->tcb, (ptr)->sig | (ptr)->timer_sig); \
  } while(0)

#define QMI_CCI_OS_SIGNAL_WAIT(ptr, timeout_ms) \
  do { \
    if(!(ptr)->inited)  \
      break;  \
    (ptr)->timed_out = 0; \
    if(timeout_ms && (ptr)->timer_inited) { \
      (void)rex_timed_wait((ptr)->sig | (ptr)->timer_sig, &((ptr)->timer), timeout_ms); \
      if(rex_clr_timer(&((ptr)->timer)) == 0) { \
        (ptr)->timed_out = 1; \
      } \
    } else { \
      if(timeout_ms) {  \
        MSG_ERROR("QCCI: TCB: 0x%x Tried a timed-wait on a signal without the timer being initialized", (ptr)->tcb, 0, 0); \
      } \
      (void)rex_wait((ptr)->sig); \
    } \
  } while(0)

#define QMI_CCI_OS_SIGNAL_TIMED_OUT(ptr) ((ptr)->timed_out)

#define QMI_CCI_OS_SIGNAL_SET(ptr)  \
  do {  \
    if(!(ptr)->inited)  \
      break;  \
    rex_set_sigs((ptr)->tcb, (ptr)->sig);  \
  } while(0)

    
#endif
