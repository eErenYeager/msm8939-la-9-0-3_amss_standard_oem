#ifndef QMI_CSI_TARGET_EXT_H
#define QMI_CSI_TARGET_EXT_H
/******************************************************************************
  @file    qmi_csi_target_ext.h
  @brief   The QMI Common Service Interface (CSI) Rex specific Header file.

  DESCRIPTION
  Describes the Rex specific OS parameters to QCSI
  
  ---------------------------------------------------------------------------
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------

 *******************************************************************************/
#include "rex.h"

/** OS Parameters provided by the
 * service to the QCSI framework 
 */
typedef struct
{
  /***************************
   *     USER SET MEMBERS    *
   ***************************/

  /** tcb of the service thread. */
  rex_tcb_type  *tcb;

  /** Signal mask to be set on the 
   * provided TCB when there is an
   * event pending for the serivce.
   * User shall wait for an event
   * using rex_wait() and clear the
   * signal before handling the 
   * event using qmi_csi_handle_event() */
  rex_sigs_type  sig;
} qmi_csi_os_params;

#endif
