#ifndef QMI_MB_H
#define QMI_MB_H
/******************************************************************************
  @file    qmi_mb.h
  @brief   The QMI message bus header file.

  DESCRIPTION
  QMI message bus public API.

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  qmi_mb_init() needs to be called before sending or receiving of any
  service specific messages

  ---------------------------------------------------------------------------
  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
*******************************************************************************/
/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.7.24/api/mproc/qmi_mb.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/
#include "stdlib.h"
#include "stdint.h"
#include "qmi_idl_lib.h"

#define QMI_MB_NO_ERR                0
#define QMI_MB_INTERNAL_ERR          (-1)
#define QMI_MB_PARAM_ERR             (-2)
#define QMI_MB_MEMORY_ERR            (-3)
#define QMI_MB_ENCODE_ERR            (-4)
#define QMI_MB_INVALID_BUS           (-5)
#define QMI_MB_BUSY_ERR              (-6)
#define QMI_MB_PERM_ERR              (-7)

typedef struct         qmi_mb_struct *qmi_mb_type;
typedef struct         qmi_mb_struct *qmi_mb_ep_type;
typedef int            qmi_mb_error_type;
typedef struct         qmi_mb_options_struct qmi_mb_options;

typedef enum
{
  QMI_MB_EVENT_JOIN,
  QMI_MB_EVENT_LEAVE,
  QMI_MB_EVENT_EP_CLOSED
} qmi_mb_event_type;

/*=============================================================================
  INTERNAL DEFINES
=============================================================================*/
#define QMI_MB_OPTIONS_SUBGROUP_VALID    (0x1)
#define QMI_MB_OPTIONS_EVENT_CB_VALID    (0x2)
#define QMI_MB_OPTIONS_RAW_FLAG_VALID    (0x4)

/*=============================================================================
  MACRO  QMI_MB_OPTIONS_INIT
=============================================================================*/
/*!
@brief
  Initialize the QMI MB Options object. Always call this before
  setting any other options field.

@param[in]  opt                 Service options object
*/
/*=========================================================================*/
#define QMI_MB_OPTIONS_INIT(opt) (opt).options_set = 0

/*=============================================================================
  MACRO  QMI_MB_OPTIONS_SET_SUBGROUP_ID
=============================================================================*/
/*!
@brief
  Set the subgroup ID of the service. Default: 0

@param[in]  opt                 Service options object
@param[in]  grp                 subgroup ID of the service
*/
/*=========================================================================*/
#define QMI_MB_OPTIONS_SET_SUBGROUP_ID(opt, grp) \
  do { \
    (opt).subgroup_id = grp;  \
    (opt).options_set |= QMI_MB_OPTIONS_SUBGROUP_VALID; \
  } while(0)

/*=============================================================================
  MACRO  QMI_MB_OPTIONS_SET_EVENT_CB
=============================================================================*/
/*!
@brief
  Set the event callback to be called when there are system level 
  events.

@param[in]  opt                 Service options object
@param[in]  evt_cb              Event callback to be called on events
@param[in]  evt_cb_data         cookie which will be passed in the event
                                callback on events.
*/
/*=========================================================================*/
#define QMI_MB_OPTIONS_SET_EVENT_CB(opt, evt_cb, evt_cb_data) \
  do { \
    (opt).event_cb = evt_cb;  \
    (opt).event_cb_data = evt_cb_data;  \
    (opt).options_set |= QMI_MB_OPTIONS_EVENT_CB_VALID; \
  } while(0)

/*=============================================================================
  MACRO  QMI_MB_OPTIONS_SET_RAW_FLAG
=============================================================================*/
/*!
@brief
  Instructs the infrastructure to refrain from encoding or decoding the 
  buffers.

@param[in]  opt                 Service options object
*/
/*=========================================================================*/
#define QMI_MB_OPTIONS_SET_RAW_FLAG(opt) \
  do { \
    (opt).options_set |= QMI_MB_OPTIONS_RAW_FLAG_VALID; \
  } while(0)


/*============================================================================
                            CALLBACK FUNCTIONS
============================================================================*/

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_event_cb
=============================================================================*/
/*!
@brief
  This callback function is called by the infrastructure when a join/leave/
  disconnect event has occurred in the underlying transport.

@param[in]   user_handle      Handle used by the infrastructure to
                              identify different clients.
@param[in]   event            Type of event
@param[in]   ep_handle        Handle for the end-point
@param[in]   event_cb_data    Cookie provided

*/
/*=========================================================================*/
typedef void (*qmi_mb_event_cb)
(
 qmi_mb_type user_handle,
 qmi_mb_event_type event,
 qmi_mb_ep_type ep_handle,
 void *event_cb_data
);

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_msg_cb
=============================================================================*/
/*!
@brief
  This callback function is called by the infrastructure when a
  message is received. This callback is registered at initialization time.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.
@param[in]   ep_handle          Handle for the end-point
@param[in]   msg_id             Message ID
@param[in]   msg_buf            Pointer to the message*
@param[in]   msg_buf_len        Length of the message
@param[in]   msg_cb_data        User-data

@Notes
  * If the raw flag was set on this handle (See QMI_MB_OPTIONS_SET_RAW_FLAG)
    then the user is responsible to decode the message. Else, this is the
    pointer to the C structure of the decoded message.

*/
/*=========================================================================*/
typedef void (*qmi_mb_msg_cb)
(
 qmi_mb_type                    user_handle,
 qmi_mb_ep_type                 ep_handle,
 unsigned int                   msg_id,
 void                           *msg_buf,
 unsigned int                   msg_buf_len,
 void                           *msg_cb_data
);

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_release_cb
=============================================================================*/
/*!
@brief
  This callback function is called by the infrastructure when a connection
  is fully released.


@param[in]   release_cb_data    cookie provided in qmi_mb_release()

*/
/*=========================================================================*/
typedef void (*qmi_mb_release_cb)
(
 void *release_cb_data
);

/*===========================================================================
  FUNCTION  qmi_mb_init
===========================================================================*/
/*!
@brief

  This function is used for initializing a message bus handle

@param[in]   service_obj        Service object
@param[in]   options            Options (See QMI_MB_OPTIONS_*)
@param[in]   msg_cb             Received message callback function
@param[in]   msg_cb_data        Received message callback user-data
@param[out]  user_handle        Handle used by the infrastructure to
                                identify different clients.
@return
  Sets the user_handle and returns QMI_MB_NO_ERR if successful,
  error code if not successful

@note

  - Dependencies
    - None

  - Side Effects
    - None
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_init
(
 qmi_idl_service_object_type service_obj,
 qmi_mb_options *options,
 qmi_mb_msg_cb msg_cb,
 void *msg_cb_data,
 qmi_mb_type *user_handle
);

/*===========================================================================
  FUNCTION  qmi_mb_join
===========================================================================*/
/*!
@brief

  This function is used to join a message bus. Only one connection can be
  made on each user handle.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.

@return
  Sets the user_handle and returns QMI_MB_NO_ERR if successful,
  error code if not successful

@note

  - Dependencies
    - None

  - Side Effects
    - Connection to the message bus is established
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_join
(
 qmi_mb_type user_handle
);

/*===========================================================================
  FUNCTION  qmi_mb_leave
===========================================================================*/
/*!
@brief

  This function is used to leave a message bus.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.

@return
  Sets the user_handle and returns QMI_MB_NO_ERR if successful,
  error code if not successful

@note

  - Dependencies
    - None

  - Side Effects
    - Connection to the message bus is destroyed
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_leave
(
 qmi_mb_type user_handle
);

/*===========================================================================
  FUNCTION  qmi_mb_publish
===========================================================================*/
/*!
@brief
  Publishes a QMI message to the bus specified by the handle. Only
  messages specified as indications in the IDL can be sent.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.
@param[in]   msg_id             Message ID
@param[in]   c_struct           Pointer to the message*
@param[in]   c_struct_len       Length of the message


@return
  QMI_MB_NO_ERR if successful error code otherwise.

@note
  * If the raw flag was set on this handle (See QMI_MB_OPTIONS_SET_RAW_FLAG)
    then this is the encoded message and the framework shall refrain from
    encoding the message. Else, this is the C Structure of the message 
    which shall be encoded by the framework before publishing.

  - Dependencies
    - None

  - Side Effects
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_publish
(
 qmi_mb_type                     user_handle,
 unsigned int                    msg_id,
 void                            *c_struct,
 unsigned int                    c_struct_len
);

/*===========================================================================
  FUNCTION  qmi_mb_send_msg
===========================================================================*/
/*!
@brief
  Sends a unicast QMI message on the specified endpoint handle. Only
  messages specified as indications in the IDL can be sent.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.
@param[in]   ep_handle          Handle for the end-point specified by rx/event
                                callbacks
@param[in]   msg_id             Message ID
@param[in]   c_struct           Pointer to the message*
@param[in]   c_struct_len       Length of the message

@return
  QMI_MB_NO_ERR if function is successful, error code otherwise.

@note
  * If the raw flag was set on this handle (See QMI_MB_OPTIONS_SET_RAW_FLAG)
    then this is the encoded message and the framework shall refrain from
    encoding the message. Else, this is the C Structure of the message 
    which shall be encoded by the framework before transmitting.

  - Dependencies
    - None

  - Side Effects
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_send_msg
(
 qmi_mb_type                     user_handle,
 qmi_mb_ep_type                  ep_handle,
 unsigned int                    msg_id,
 void                            *c_struct,
 unsigned int                    c_struct_len
);


/*===========================================================================
  FUNCTION  qmi_mb_release
===========================================================================*/
/*!
@brief
  Releases the message bus handle.

@param[in]   user_handle        Handle to be released
@param[in]   release_cb         The callback to be called when the 
                                infrastructure successfully releases the
                                handle
@param[in]   user_handle        Cookie which the user will be provided
                                in the release_cb

@return
  QMI_MB_NO_ERR if function is successful, error code otherwise.

@note

  - Dependencies
    - None

  - Side Effects
   - None
*/
/*=========================================================================*/
qmi_mb_error_type
qmi_mb_release
(
 qmi_mb_type         user_handle,
 qmi_mb_release_cb   release_cb,
 void                *release_cb_data
);

/*=============================================================================
  PRIVATE qmi_mb_options_struct
=============================================================================*/
/*!
@brief
  Provide storage class for the options structure. This structure should not
  be directly manipulated. Please use the QMI_MB_OPTIONS_* macros.
*/
/*=========================================================================*/
struct qmi_mb_options_struct
{
  unsigned int        options_set;
  unsigned int        subgroup_id;
  qmi_mb_event_cb     event_cb;
  void                *event_cb_data;
};
#endif /* QMI_MB_H  */
