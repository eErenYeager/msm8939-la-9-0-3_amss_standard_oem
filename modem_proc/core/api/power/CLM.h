/*===========================================================================

FILE:         CLM.h

DESCRIPTION:  Defines the API for the CPU Load Monitor (CLM). The CLM provides 
clients with CPU utilization information on periodic basis or threshold 
triggers. 
 
=========================================================================== 
Copyright � 2013-2014 QUALCOMM Technologies, Incorporated. All Rights Reserved. 
QUALCOMM Confidential and Proprietary. 
=========================================================================== 
 
Edit History $Header: 
//source/qcom/qct/core/power/rbcpr/mpss/inc/cpr_internal.h#5 $ 

=============================================================================*/
#ifndef _CLM_H_
#define _CLM_H_

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DALStdDef.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

#define DCVS_NEW_ALGO_INPUT_ACTIVE 1
#define DCVS_EXTRA_LOGS_ENABLED 1

/**
 * MAX_NAME_LENGTH - the max size of the client name (including NULL termination)
 */
#define CLM_MAX_NAME_LENGTH 16

#define MAX_HW_THREADS 4

#define CLM_PC_EXIT_REDUCED_PERIOD 5000

/**
 * CLM Client Attributes
 */
#define CLM_ATTRIBUTE_DEFAULT                     (1 << 0)
#define CLM_ATTRIBUTE_RESET_MEAS_ON_WAKEUP        (1 << 1)
#define CLM_ATTRIBUTE_REDUCED_PERIOD_ON_PC_EXIT   (1 << 2)                  

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/**
 * CLM Client handle - will be non-zero if this is a valid handle.
 */
typedef uint32 CLM_HandleType;

/**
 * List of reasons why the client is being provided a utilization update.
 */
typedef enum
{
  /**
   * Periodic update at the end of the client specified window size. 
   */
  CLM_UPDATE_REASON_PERIODIC,

  /**
   * CPU utilization is below client specified threshold.
   */
  CLM_UPDATE_REASON_LOW_THRESHOLD,

  /**
   * CPU utilization is at or above client specified threshold.
   */
  CLM_UPDATE_REASON_HIGH_THRESHOLD,

  // Do not use! Force enum to use 32bits
  CLM_UPDATE_REASON_DUMMY_FORCE_INT32 = 0x7FFFffff 

} CLM_UpdateReasonEnumType;

/**
 * List of types of Clients supported by CLM
 */
typedef enum
{

  /** Use this client to access the current state of the PMU registers 
   */   
  CLM_CLIENT_PMU_INFO = 0,

  /** Use this client if basic information about CPU utilization is needed see
   *  the definition of CLM_LoadInfoBasicStruct for the list of values returned
   */
  CLM_CLIENT_BASIC_CPUUTIL, 

  /** Use this client if additional information about CPU utilization is needed
   *  see the definition of CLM_LoadInfoExtendedStruct for the list of values
   *  returned
   */
   CLM_CLIENT_EXTENDED_CPUUTIL

} CLM_ClientOutputType;


/**
 * Container for the CPU utilization data.
 */
typedef struct CLM_LoadInfoBasicStruct 
{
  /**
   * Reason why the update is being provided.
   */
  CLM_UpdateReasonEnumType  updateReason;

  /**
   * Number of microseconds that have elapsed since the last 
   * update. 
   */
  uint32 elapsedTimeUs;

  /**
   * The current clock rate of the CPU in kHz.
   */
  uint32 currentClkKhz;

  /**
   * CPU utilization percentage based on the clock rate when the window ended. 
   * Range: 0..100
   */
  uint32 utilPctAtCurrClk;

  /**
   * CPU utilization percentage scaled to the maximum clock rate supported by 
   * the CPU. Range: 0..100
   */
  uint32 utilPctAtMaxClk;

} CLM_LoadInfoBasicStructType;


/**
 * Container for the PMU Data.
 */
typedef struct CLM_LoadInfoPMUInfoStructType
{
    /**
   * Reason why the update is being provided.
   */
  CLM_UpdateReasonEnumType  updateReason;

  /**
   * Number of microseconds that have elapsed since the last 
   * update. 
   */
  uint32 elapsedTimeUs;

  uint32 pmuIsDisabled;

  #if DCVS_NEW_ALGO_INPUT_ACTIVE
  uint32 pcyclesPerThread[MAX_HW_THREADS];
  #else

  uint32  l2IUmissCnt; 

  uint32 l2DUstallCnt;

  uint32 l2DUstallPcyclesCnt;

  uint32 pps; 

  #endif

}CLM_LoadInfoPMUInfoStructType;

/**
 * Container for the CPU utilization data.
 */
typedef struct CLM_LoadInfoExtendedStructType 
{
  /**
   * Reason why the update is being provided.
   */
  CLM_UpdateReasonEnumType  updateReason;

  /**
   * Number of microseconds that have elapsed since the last 
   * update. 
   */
  uint32 elapsedTimeUs;

  /**
   * The measurement period of the client
   */
  uint32 measurementPeriod;

  /**
   * The current clock rate of the CPU in kHz.
   */
  uint32 currentClkKhz;

  /**
   * CPU utilization percentage based on the clock rate when the window ended. 
   * Range: 0..100
   */
  uint32 utilPctAtCurrClk;

  /**
   * CPU utilization percentage scaled to the maximum clock rate supported by 
   * the CPU. Range: 0..100
   */
  uint32 utilPctAtMaxClk;


  /**
   * Number of processor cycles that were available for use during the 
   * measurement window, excluding time spent when the CPU was not functional 
   * (CPU may have been in a low power mode such as power collapse) since the last
   * update. 
   */
  uint64 availablePcycles;

  /**
   * Number of processor cycles that were actually used to execute instructions 
   * since the last update. 
   */
  uint64 utilizedPcycles;

  #if DCVS_NEW_ALGO_INPUT_ACTIVE

  uint32 pcyclesPerThread[MAX_HW_THREADS];

  #else
  /**
   * Number of L2 Instruction Unit misses since the last update.
   */
  uint32 l2IUmissCnt;

  /**
   * Number of pcycles for which the CPU was stalled due to L2 data unit 
   * cache misses since the last update. 
   */
  uint32 l2DUstallPcyclesCnt;

  /**
   * Number of L2 data unit cache misses since the last update.
   */
  uint32 l2DUstallCnt;

  /**
   * PPS delta count since the last update.
   */
  uint64 pps;
  #endif 

} CLM_LoadInfoExtendedStructType;


/**
 * Client provided Callback function that will be invoked when the measurement 
 * window for the client elapses or when CPU Utilization at 
 * MAX Clock exceeds/drops below the specified high/low busy 
 * thresholds . 
 *
 * @param update : pointer to CPU utilization info. This data structure is 
 *                 allocated and 'owned' by CLM and is only valid in the context
 *                 of this callback. Clients must make their own copy if the
 *                 data is needed outside the context of this callback.
 *                 This data structure should be type casted to
 *                 either CLM_LoadInfoBasicStructType or
 *                 CLM_LoadInfoQ6ExtendedStructType according to
 *                 the client type (caller is responsible for
 *  							 knowing the type and performing the cast). See
 *  							 CLM_ClientEnumType for more info.
 *  
 * @param clientData : Client data that was passed in by the client during 
 *                   registration.
 */
typedef void (*CLM_CallbackFuncPtr)(
                                    void  *update,
                                    void  *clientData
                                    );


/**
 * Client provided configuration data.
 */
typedef struct CLM_ThresholdClientRegistrationStruct 
{

  /**
   * If non-zero, the client will be notified when _scaled_ CPU utilization 
   * at the MAXIMUM clock rate is greater then this value.
   *
   * NOTE: Periodic updates at the end of the measurement window will be 
   * disabled if a non-zero value is set.
   *  
   * Range: 0..100. Must be >= lowThresholdPercentage. 
   */
  uint32                    highThresholdPercentage;

  /**
   * If non-zero, the client will be notified when _scaled_ CPU utilization 
   * at the MAXIMUM clock rate has fallen below this value.
   *
   * NOTE: Periodic updates at the end of the measurement window will be 
   * disabled if a non-zero value is set. 
   *
   * Range: 0..100. Must be <= highThresholdPercentage.
   */
  uint32                    lowThresholdPercentage;

  /** Num Threads for which the highThresholdPercentage should be checked for
     to trigger the callback  */
  uint32                    numThreadsForHighTrigger;

  /** Num Threads for which the lowThresholdPercentage should be checked for
     to trigger the callback  */
  uint32                    numThreadsForLowTrigger;


} CLM_ThresholdClientRegistrationStruct;


/*----------------------------------------------------------------------------
 * Data Declarations (external and forward)
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
/**
 * 
 * <!-- CLM_RegisterThresholdClient -->
 * 
 * Clients needing CPU load information must register using this API. 
 *  
 * Periodic clients (that is those that have not specified a high/low threshold 
 * trigger) will only receive callbacks with reason code 
 * CLM_UPDATE_REASON_PERIODIC. 
 *  
 * Clients that have specified a low/high threshold will only receive callbacks 
 * with reason code CLM_UPDATE_REASON_BELOW_LOW_THRESHOLD or 
 * CLM_UPDATE_REASON_BELOW_HIGH_THRESHOLD and these clients will never 
 * receive a periodic update. 
 *  
 * It is legal to set no thresholds, only a low or a high threshold or to set 
 * both thresholds. 
 *
 * @param params : pointer to the client specific parameters. This data will 
 *                 be copied over by CLM. So the client is free to delete this
 *                 structure after calling CLM_RegisterClient().
 * 
 *   
 * @param clientName example "DCVS" , "LTE" - should be limited to CLM_MAX_NAME_LENGTH 
 *                            (including the terminating NULL character)
 * 
 * @param callbackPtr : pointer of type void * to the function that CLM will use 
 *                      to report back the measurements/triggers. 
 * 
 * @param clientType : specifies the type of Client: CLM_CLIENT_BASIC 
 * 										 or CLM_CLIENT_EXTENDED_Q6
 * 
 * @param clientData : Optional client data that can be used by the client 
 *                     as a 'cookie'. NULL is a valid value.
 *
 * @return CLM_HandleType : A non-zero client handle is returned if registration
 *                          is successful; 0 on failure.
 * 
 */
CLM_HandleType CLM_RegisterThresholdClient( 
                                            const char                              *clientName,
                                            CLM_ClientOutputType                    clientType, 
                                            uint32                                  measPeriodUs,
                                            CLM_ThresholdClientRegistrationStruct   *params,
                                            uint32                                  attributes,
                                            CLM_CallbackFuncPtr		                callbackPtr,
                                            void                                    *clientData
                                           );


/** 
 *  
 *  <!--  CLM_RegisterPeriodicClient -->
 *  
 * Clients needing CPU load information must register using this API. 
 *  
 * Periodic clients (that is those that have not specified a high/low threshold 
 * trigger) will only receive callbacks with reason code 
 * CLM_UPDATE_REASON_PERIODIC. 
 *  
 * Clients that have specified a low/high threshold will only receive callbacks 
 * with reason code CLM_UPDATE_REASON_BELOW_LOW_THRESHOLD or 
 * CLM_UPDATE_REASON_BELOW_HIGH_THRESHOLD and these clients will never 
 * receive a periodic update. 
 *  
 * It is legal to set no thresholds, only a low or a high threshold or to set 
 * both thresholds. 
 *
 * @param measPeriodUs : Measurement period of Client in USEC 
 *   
 * @param clientName example "DCVS" , "LTE" - should be limited to CLM_MAX_NAME_LENGTH 
 *                            (including the terminating NULL character)
 * 
 * @param callbackPtr : pointer of type void * to the function that CLM will use 
 *                      to report back the measurements/triggers. 
 * 
 * @param clientType : specifies the type of Client: CLM_CLIENT_BASIC 
 * 										 or CLM_CLIENT_EXTENDED_Q6
 * 
 * @param clientData : Optional client data that can be used by the client 
 *                     as a 'cookie'. NULL is a valid value.
 *
 * @return CLM_HandleType : A non-zero client handle is returned if registration
 *                          is successful; 0 on failure.
 * 
 */
CLM_HandleType CLM_RegisterPeriodicClient(
                                           const char                    *clientName,
                                           CLM_ClientOutputType          clientType, 
                                           uint32                        measPeriodUs,
                                           uint32                        attributes,
                                           CLM_CallbackFuncPtr		       callbackPtr,
                                           void                          *clientData
                                          );

/**
 *  <!-- CLM_UnregisterClient -->
 * 
 *   @brief This function is used to remove a client from CLM
 * 
 *   @param handle : Client handle of the client to be removed
 *  
 *   @return None
 */
void CLM_UnregisterClient(CLM_HandleType handle);


/**
 *  <!-- CLM_GetLastLPRExitTime -->
 * 
 *   @brief This function is used to obtain the time of the last
 *          CLM LPR exit
 * 
 *  
 *   @return uint64 : the time of the last CLM LPR exit
 */
uint64 CLM_GetLastLPRExitTime( void );


/**
 *  <!-- CLM_SwitchClientMeasurementPeriod -->
 * 
 *   @brief This function switches the measurement period of the
 *          client to new measurement period passed in by the
 *          user
 *  
 *   @param client : Client handle of the client that will
 *                 receive the new measurement period
 *  
 *   @param measurementPeriodUs : The new measurement period
 *  
 *   @return None
 */
void CLM_SwitchClientMeasurementPeriod( CLM_HandleType client, uint32 measurementPeriodUs ); 

#endif // _CLM_H_

