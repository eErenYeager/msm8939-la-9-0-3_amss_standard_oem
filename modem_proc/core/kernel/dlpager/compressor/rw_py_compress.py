#! /usr/bin/env python
import sys
import struct

debug_enabled = False

##
# @brief    Console debug control
# @param    Message to print
def debug(arg):

    if (debug_enabled):
        print arg
        sys.stdout.flush()

def __pushCompressedBits(bits,numBits,compressedWordStream):
    global __numCompressedWords
    global __compressedPartialWord
    global __numCompressedPartialBits
    temp = bits << __numCompressedPartialBits                                 
    __numCompressedPartialBits += numBits                                                                                        
    __compressedPartialWord = __compressedPartialWord | temp   
    if __numCompressedPartialBits >= 32:                                                                               
        compressedWordStream.append(__compressedPartialWord & 0xFFFFFFFF)
        __numCompressedWords += 1                                              
        __compressedPartialWord = __compressedPartialWord >> 32   
        __numCompressedPartialBits -= 32              

def __finalizeCompressedBits(compressedWordStream,__compressedPartialWord,__numCompressedPartialBits):  
    global __numCompressedWords
    if __numCompressedPartialBits > 0:                                                                                                                                                                                                    
        compressedWordStream.append(__compressedPartialWord & 0xFFFFFFFF)  
        __numCompressedWords += 1   

def __checkAnchor(anchor,val,compressed):
    global __anchors
    anchor_val = __anchors[anchor]; 
    if __anchors[anchor] == val: 
        __pushCompressedBits((anchor << 2) + 1,2 + 2,compressed)
        return 1
    elif ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)):
        __pushCompressedBits(((val&0x3FF)<<(2+2))+((anchor<<2)+2),2+2+10,compressed)
        __anchors[anchor] = val; 
        return 1
    else:
        return 0

def __GET_BITS(__hold,n):
     return __hold & ((1 << n) - 1)
def __SKIP_BITS_W_CHECK(compressed,size,n): 
    global __numAvailBits   
    global __hold
    global __in_index
    __numAvailBits -= n                                             
    __hold = __hold >> n                                                  
    if __numAvailBits <= 32: 
        if __in_index != size:
            __hold |= compressed[__in_index] << __numAvailBits              
            __in_index += 1                                                                         
            __numAvailBits += 32                                
def deltaCompress (uncompressed,compressed):
    global __numCompressedWords
    global __compressedPartialWord
    global __numCompressedPartialBits
    global __anchors
    __anchors = [0,0,0,0]
    __numCompressedWords = 0
    __compressedPartialWord = 0
    __numCompressedPartialBits = 0
    anchorIndex = 3
    compressed.append(len(uncompressed)) 
    __numCompressedWords += 1
    # for i in range(0,len(uncompressed)):
    for val in uncompressed:
        if (val == 0):
            __pushCompressedBits(0,2,compressed)
        else:
            anchor = anchorIndex
            if __checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if __checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if __checkAnchor(anchor,val,compressed) == 1:
                continue
            anchor = (anchor + (4 - 1)) & (4 - 1)
            if __checkAnchor(anchor,val,compressed) == 1:
                continue
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            __anchors[anchorIndex] = val
            __pushCompressedBits(3,2,compressed)
            __pushCompressedBits(val,32,compressed)
    __finalizeCompressedBits(compressed,__compressedPartialWord,__numCompressedPartialBits)
    return __numCompressedWords

def deltaUncompress(compressed,uncompressed):
    debug("deltaUncompress")
    global __numAvailBits   
    global __hold
    global __in_index
    __anchors = [0,0,0,0]
    anchorIndex = 3
    __numAvailBits = 0
    __hold = 0
    __in_index = 0
    out_index = 0
    size = compressed[__in_index]
    if size == 0:
        return 0
    __in_index += 1
    compressed_size = len(compressed)
    __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    if size != 1:
        __SKIP_BITS_W_CHECK(compressed,compressed_size,0)
    while out_index < size:
        debug("out_index %d"%out_index)
        code = __GET_BITS(__hold,2)
        if code == 0:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            uncompressed.append(0)
            out_index += 1
        elif code == 1:
            anchor = __GET_BITS(__hold>>2,2)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2)
            uncompressed.append(__anchors[anchor])
            out_index += 1
        elif code == 2:
            anchor = __GET_BITS(__hold>>2,2)
            delta = __GET_BITS(__hold>>(2+2),10)
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2+2+10)
            val = (__anchors[anchor] & 0xFFFFFC00) | delta
            uncompressed.append(val)
            out_index += 1
            __anchors[anchor] = val
        elif code == 3:
            __SKIP_BITS_W_CHECK(compressed,compressed_size,2)
            val = __hold & 0xFFFFFFFF
            __SKIP_BITS_W_CHECK(compressed,compressed_size,32)
            uncompressed.append(val)
            out_index += 1
            anchorIndex = (anchorIndex + 1) & (4 - 1)
            __anchors[anchorIndex] = val 
    return out_index

def __main():
    if len(sys.argv) > 1:
        uncompressed = []
        with open(sys.argv[1], 'rb') as f:
            while True:
                word = f.read(4)
                if len(word) != 4:
                    break
                uncompressed.append( struct.unpack('I', word)[0] )
    else:
        uncompressed = [0xFFFFFFFF] * 1024
    print "len of uncompressed = %d"%(len(uncompressed))
    print "uncompressed[%d] = 0x%x"%(len(uncompressed)/2, uncompressed[len(uncompressed)/2]) 
    compressed = []
    compressed_size = deltaCompress(uncompressed,compressed)
    print "compressed len words = %d"%(compressed_size)
    print "compression ratio = %f" % (float(len(compressed))/len(uncompressed))
    new_uncompressed = []
    uncompressed_size = deltaUncompress(compressed,new_uncompressed)
    print "uncompressed len words = %d"%(uncompressed_size)
    print "uncompressed[%d] = 0x%x"%(len(uncompressed)/2, uncompressed[len(uncompressed)/2])

BLOCK_SIZE = 1024

def rw_py_compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
    instrList=[]
    for word in (input[i:i+4] for i in xrange(0,len(input),4)):
        if len(word) == 4:
            instrList.append( struct.unpack('I',word)[0] )

    n_blocks = len(instrList)/BLOCK_SIZE
    print "n_blocks of RW = %d"%(n_blocks)
    v_addrs = []
    va = VA_start + 2 + 2 + 4 * n_blocks  #2 bytes for n_blocks, 2 for 0, 4 per block start addr

    compressed_text = []
    for block in xrange(n_blocks):
        v_addrs.append( struct.pack('I',va) )
        compressed = []
        #print "calling deltaCompress, block = %d"%(block)
        deltaCompress(instrList[block*BLOCK_SIZE:(block+1)*BLOCK_SIZE],compressed)
        #print "compressed len = %d"%(len(compressed))
        for word in compressed:
            compressed_text.append(struct.pack('I',word))
        va += 4 * len(compressed)

    print "creating metadata for RW"
    metadata = [struct.pack("H",n_blocks), struct.pack("H",0)]
    metadata += v_addrs
    metadata += compressed_text

    return ''.join(metadata)  #joins list elements together as string with no spaces

if __name__ == "__main__":
    __main()
