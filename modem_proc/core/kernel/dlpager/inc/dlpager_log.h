#ifndef __DLPAGER_LOG_H__
#define __DLPAGER_LOG_H__
/*===========================================================================
 * FILE:         dlpager_log.h
 *
 * DESCRIPTION:  Dlpager logging
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.7.24/kernel/dlpager/inc/dlpager_log.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/30/14   ao      Initial revision
===========================================================================*/

typedef enum
{
    DLP_FAULT           = 0,

    DLP_REMOVE_CLK_VOTE = 1,
    DLP_FIRST_CLK_VOTE  = 2,
    DLP_MAX_CLK_VOTE    = 3,

    DLP_DEBUG_TYPE      = 4,

    DLP_RESTART_TIMER   = 4,
    DLP_REMOVE_CLK_REQ  = 5,
    DLP_FIRST_CLK_REQ   = 6,
    DLP_MAX_CLK_REQ     = 7,
    DLP_TIMER_REQ       = 8,
} dlpager_log_event_t;

void dlpager_log_init(void);
unsigned dlpager_log_start(dlpager_log_event_t event, void* fault_addr, int thread_id);
void dlpager_log_end(dlpager_log_event_t event, unsigned log_idx, unsigned int evicted, unsigned clk);

#endif
