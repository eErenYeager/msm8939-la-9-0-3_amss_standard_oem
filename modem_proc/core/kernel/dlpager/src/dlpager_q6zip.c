#include "dlpager_q6zip.h"
#include <dlpager_main.h>
#include "q6zip_uncompress.h"
#include "rw_compress.h"

#include "q6zip_clk.h"

/* if USE_DLPAGER_HEAP is defined, this module uses its own heap.
   else use AMSS heap defined in modem_proc\config\8974\cust_config.xml.
   If using AMSS heap, the AMSS heap size needs to be increased. 
*/
#define USE_DLPAGER_HEAP

#include <stdlib.h>
#include <hexagon_protos.h>
#include "assert.h"
#include "qurt_cycles.h"
#include "qurt_memory.h"
#include <qurt_cycles.h>
#ifdef USE_DLPAGER_HEAP
#include "memheap.h"
#endif
#include <assert.h>
#include "core_variation.h"

#define PAGE_SIZE       (4*1024)
#define MAX_LONG  (0xFFFFFFFF)
#define RW_CIRC_BUF_SIZE 256

#define L2_PREFETCH
#define DCZERO

#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)

/* Type declarations */

/* element of rw table */
typedef struct 
{
  void *ptr;       /* pointer to memory containing a compressed block*/
  uint32_t size;   /* size of allocated block */
} dlpager_rw_table_t;

/* structure used for rw v->p translation and decompression */
typedef struct
{
  unsigned int page_size;   /* size of rw page */
  unsigned int num_elements;    /* number of elements in table */
  dlpager_rw_table_t *table;    /* array of pointers to compressed blocks */
} dlpager_rw_info_t;

typedef struct 
{
  unsigned long int min_cycles;
  unsigned long int max_cycles;
  unsigned long int count;
  unsigned long long int total_cycles;  
}rx_stats_t;

typedef struct 
{
  unsigned long int decompress_min_cycles;
  unsigned long int decompress_max_cycles;
  unsigned long int decompress_count;
  unsigned long long int decompress_total_cycles;

  unsigned long int recompress_min_cycles;
  unsigned long int recompress_max_cycles;
  unsigned long int recompress_count;
  unsigned long long int recompress_total_cycles; 
}rw_stats_t;


/* Symbol definitions */

/* Extern symbols defied in dlpager_main.c */
extern unsigned int __attribute__ ((weak)) __swapped_segments_bss_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_rw_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_end__;

extern unsigned int __attribute__ ((weak)) __dlpager_heap_start__;
extern unsigned int __attribute__ ((weak)) __dlpager_heap_end__;

extern uint32_t __attribute__((section (".data"))) start_va_uncompressed_text;
extern uint32_t __attribute__((section (".data"))) end_va_uncompressed_text ;
extern uint32_t __attribute__((section (".data"))) start_va_compressed_text;
extern uint32_t __attribute__((section (".data"))) end_va_compressed_text;

extern uint32_t __attribute__((section (".data"))) start_va_uncompressed_rw;
extern uint32_t __attribute__((section (".data"))) end_va_uncompressed_rw ;
extern uint32_t __attribute__((section (".data"))) start_va_compressed_rw;
extern uint32_t __attribute__((section (".data"))) end_va_compressed_rw;


/* rx metadata used for v->p translation and decompression */
static dlpager_rx_metadata_t* dlpager_rx_metadata;

/* rw info used for v->p translation and decompression */
static dlpager_rw_info_t dlpager_rw_info;

static volatile uint32_t q6zip_swap_pool_size = 2 * 1024 * 1024;

#ifdef USE_DLPAGER_HEAP
/* dlpager heap structure */
static mem_heap_type dlpager_heap;
/* dlpager heap buffer */
static byte *dlpager_heap_buffer;
#endif

static unsigned int dlpager_rw_temp_buf[(PAGE_SIZE>>2) + (PAGE_SIZE>>4)] __attribute__((aligned(32)));

/* Stats */

static rx_stats_t* rx_stats;
static rw_stats_t* rw_stats;

static unsigned long long int rx_total_decompressions = 0;
static unsigned long long int rw_total_decompressions = 0;
static unsigned long long int rw_total_recompressions = 0;
static long long int rw_initial_compressed_size = 0;

//tracks malloced size with optimization
static unsigned int rw_circ_buf_idx = 0;
static long long int rw_malloced_size[RW_CIRC_BUF_SIZE];

//tracks malloced size with/without optimization
static unsigned int rw_circ_buf_idx_always = 0;
static long long int rw_malloced_size_always[RW_CIRC_BUF_SIZE];

/* Function definitions*/

/* returns 1 if addr belongs to text or ro data */
int dlpager_is_addr_in_rx_range(unsigned int addr)
{
  return (addr >= start_va_uncompressed_text && addr < end_va_uncompressed_text);
}

/* returns 1 if addr belongs to rw data */
int dlpager_is_addr_in_rw_range(unsigned int addr)
{
  return (addr >= start_va_uncompressed_rw && addr < end_va_uncompressed_rw);
}




static inline void dlpager_get_compressed_block_info_rx
(
  uint32_t uncompressed_page_addr,
  uint32_t *compressed_block_addr,
  unsigned long *compressed_block_size
)
{
  uint32_t block_num;

  block_num =
        (uncompressed_page_addr - start_va_uncompressed_text) >> 12 ;

  if (block_num < dlpager_rx_metadata->n_blocks)
    {
        *compressed_block_addr = dlpager_rx_metadata->compressed_va[block_num];
        *compressed_block_size = (block_num + 1 == dlpager_rx_metadata->n_blocks)?
                                 (end_va_compressed_text
                                   - dlpager_rx_metadata->compressed_va[block_num]):
                                 (dlpager_rx_metadata->compressed_va[block_num + 1]
                                   - dlpager_rx_metadata->compressed_va[block_num]);
  }
    }

static inline void dlpager_get_compressed_block_info_rw
(
  uint32_t uncompressed_page_addr,
  uint32_t *compressed_block_addr_ptr,
  unsigned long *compressed_block_size_ptr
)
{
  uint32_t block_num = 
        (uncompressed_page_addr - start_va_uncompressed_rw) / PAGE_SIZE;

  *compressed_block_addr_ptr = (uint32_t) dlpager_rw_info.table[block_num].ptr;
  *compressed_block_size_ptr = dlpager_rw_info.table[block_num].size;
}

static inline void l2fetch_buffer
(
  void *addr,  /*!< (Virtual) address to start fetch */
  uint32_t len   /*!< Length (in bytes) to fetch. */
)
{
  /* Cache-align starting address and length. */
  uint32_t ofs = ((uint32_t) addr) & (CACHE_LINE_SZ-1);
  addr = (void *) ((uint32_t) addr - ofs);
  len  = (len+ofs+CACHE_LINE_SZ-1) / CACHE_LINE_SZ;

  /* Width=cache line, height=# cache lines, stride=cache line */
  asm volatile ("l2fetch(%[addr],%[dim])" : : 
     [addr] "r" (addr), 
     [dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, len)) )
     : "memory");

} /* l2fetch_buffer() */

/* DCZERO the output buffer, 32 bytes at a time.
   Assume page size is a multiple of 32
   Assume dest addr is multiple of 32
*/
static inline void dczero
(
   uint32_t addr,
   uint32_t size
)
{
  uint32_t lines, i;  
   
  lines = size >> CACHE_LINE_SHIFT;
  for (i = 0; i < lines; i++)
  {
    asm("dczeroa(%0)" : : "r" (addr));
    addr += CACHE_LINE_SZ;
  }
}

void dlpager_store_virtual_page(unsigned int page_to_store, unsigned int fault_addr_evicted_page)
{
  //Uncompressed_page_addr = location in swap pool to write to
  //Mapped_page_addr = linked location in uncompressed virtual space (10 million range)
  uint32_t compressed_size;
  uint32_t block_num = (fault_addr_evicted_page - start_va_uncompressed_rw) / PAGE_SIZE;
  unsigned long long int start_pcycle, end_pcycle;
  unsigned long int total_pcycles;
  unsigned int rw_circ_buf_next_idx;

  DL_DEBUG("Recompressing RW page for at mapped addr = 0x%x, uncomp addr = 0x%x\n", fault_addr_evicted_page, page_to_store); 


  start_pcycle = qurt_get_core_pcycles();

#if defined L2_PREFETCH
    l2fetch_buffer((void *)page_to_store, PAGE_SIZE);
#endif     
#if defined DCZERO
    dczero((uint32_t)dlpager_rw_temp_buf, PAGE_SIZE);
#endif
  compressed_size = deltaCompress((unsigned int *)page_to_store,(unsigned int*) dlpager_rw_temp_buf,PAGE_SIZE>>2);
  assert(compressed_size != 0);

  //free and malloc only for first time bss or when new compressed size is larger than original malloced size
  if ((dlpager_rw_info.table[block_num].ptr == NULL) || (dlpager_rw_info.table[block_num].size < compressed_size))
  {
    if (dlpager_rw_info.table[block_num].ptr != NULL) 
    {
#ifdef USE_DLPAGER_HEAP
      mem_free(&dlpager_heap,dlpager_rw_info.table[block_num].ptr);
#else
      free(dlpager_rw_info.table[block_num].ptr); 
#endif
}

#ifdef USE_DLPAGER_HEAP
    dlpager_rw_info.table[block_num].ptr = mem_malloc(&dlpager_heap,compressed_size+4);
#else
    dlpager_rw_info.table[block_num].ptr = malloc(compressed_size+4);
#endif
    assert(dlpager_rw_info.table[block_num].ptr != NULL);


    //make delta memory change on previous entry
    rw_circ_buf_next_idx = (rw_circ_buf_idx + 1) % RW_CIRC_BUF_SIZE;
    rw_malloced_size[rw_circ_buf_next_idx] = rw_malloced_size[rw_circ_buf_idx]  
											+ (compressed_size - dlpager_rw_info.table[block_num].size);
    rw_circ_buf_idx = rw_circ_buf_next_idx;

    //do this for this stats buffer also to track the above
	  rw_circ_buf_next_idx = (rw_circ_buf_idx_always + 1) % RW_CIRC_BUF_SIZE;
    rw_malloced_size_always[rw_circ_buf_next_idx] = rw_malloced_size_always[rw_circ_buf_idx_always]  
											+ (compressed_size - dlpager_rw_info.table[block_num].size);
    rw_circ_buf_idx_always = rw_circ_buf_next_idx;

    dlpager_rw_info.table[block_num].size = compressed_size;
  }
  else
{
    rw_circ_buf_next_idx = (rw_circ_buf_idx_always + 1) % RW_CIRC_BUF_SIZE;
    rw_malloced_size_always[rw_circ_buf_next_idx] = rw_malloced_size_always[rw_circ_buf_idx_always]  
											- (dlpager_rw_info.table[block_num].size - compressed_size);
    rw_circ_buf_idx_always = rw_circ_buf_next_idx;
  }

  memcpy(dlpager_rw_info.table[block_num].ptr,(void*)dlpager_rw_temp_buf,compressed_size);

  end_pcycle = qurt_get_core_pcycles();
  total_pcycles = end_pcycle - start_pcycle;


  rw_stats_t * rw_stat = &rw_stats[block_num]; 

  rw_stat->recompress_count++;
  
  if(total_pcycles < rw_stat->recompress_min_cycles)
    rw_stat->recompress_min_cycles = total_pcycles;

  if(total_pcycles > rw_stat->recompress_max_cycles)
	rw_stat->recompress_max_cycles = total_pcycles;

  rw_stat->recompress_total_cycles += total_pcycles;

  rw_total_recompressions++;
}

void dlpager_load_virtual_page_rx (unsigned int src_addr,
		               unsigned int dest_addr, unsigned int page_size)
{
 
  uint32_t compressed_block_addr = 0;
  unsigned long compressed_block_size = 0;
  int dest_size;
  int ret_val, block_num;
  unsigned long long int start_pcycle, end_pcycle;
  unsigned long int total_pcycles;

  start_pcycle = qurt_get_core_pcycles();

  dlpager_get_compressed_block_info_rx(src_addr,
                        &compressed_block_addr,
                        &compressed_block_size);


  if (compressed_block_addr == 0
          || compressed_block_size == 0)
    return ;  /* retrun failure */

  dest_size = page_size;

#if defined L2_PREFETCH
  l2fetch_buffer((void *)compressed_block_addr, compressed_block_size);
#endif     

#if defined DCZERO

  dczero(dest_addr, page_size);
#endif

  ret_val = q6zip_uncompress(
    (char*)dest_addr,
      &dest_size,  // we discard the output size from uncompress 
    (char*) compressed_block_addr,
    compressed_block_size,
      (char*)dlpager_rx_metadata->dictionary);
  assert(ret_val ==  0);

    /* Cache optimization. */
    ret_val = qurt_mem_cache_clean( (qurt_addr_t)compressed_block_addr,
                                    compressed_block_size,
                                    QURT_MEM_CACHE_INVALIDATE,
                                    QURT_MEM_DCACHE );
    assert( QURT_EOK == ret_val );
						
  end_pcycle = qurt_get_core_pcycles();
  total_pcycles = end_pcycle - start_pcycle;
  block_num = (src_addr - start_va_uncompressed_text) >> 12 ;

  rx_stats_t * rx_stat = &rx_stats[block_num];

  rx_stat->count++;

  if(total_pcycles < rx_stat->min_cycles)
      rx_stat->min_cycles = total_pcycles;
  
  if(total_pcycles > rx_stat->max_cycles)
      rx_stat->max_cycles = total_pcycles; 

  rx_stat->total_cycles += total_pcycles;
  
  rx_total_decompressions++;
}

void dlpager_load_virtual_page_rw (unsigned int src_addr,
                   unsigned int dest_addr, unsigned int page_size)
{
 
  uint32_t compressed_block_addr = 0;
  unsigned long compressed_block_size = 0;
  int ret_val, block_num;
  unsigned long long int start_pcycle, end_pcycle;
  unsigned long int total_pcycles;

  DL_DEBUG("Handling RW page for decompression at addr = 0x%x\n",src_addr);
 
  start_pcycle = qurt_get_core_pcycles();

  dlpager_get_compressed_block_info_rw(src_addr,
                        &compressed_block_addr,
                        &compressed_block_size);

#if defined DCZERO
    dczero(dest_addr, page_size);
#endif
  if (compressed_block_addr == 0) //bss first time
  {
    memset((void*)dest_addr,0,PAGE_SIZE);
  }
  else
  {
#if defined L2_PREFETCH
    l2fetch_buffer((void *)compressed_block_addr, compressed_block_size);
#endif     
    ret_val = deltaUncompress((unsigned int*)compressed_block_addr, (unsigned int*)dest_addr, PAGE_SIZE >> 2);
    assert(ret_val == PAGE_SIZE); 
  }


  end_pcycle = qurt_get_core_pcycles();
  total_pcycles = end_pcycle - start_pcycle;
  block_num = (src_addr - start_va_uncompressed_rw) >> 12 ;


  rw_stats_t * rw_stat = &rw_stats[block_num];

  rw_stat->decompress_count++;
  
  if(total_pcycles < rw_stat->decompress_min_cycles)
    rw_stat->decompress_min_cycles = total_pcycles;

  if(total_pcycles > rw_stat->decompress_max_cycles)
	rw_stat->decompress_max_cycles = total_pcycles;

  rw_stat->decompress_total_cycles += total_pcycles;

  rw_total_decompressions++;
}


void dlpager_load_virtual_page(unsigned int fault_addr_align, unsigned int load_addr, unsigned int page_size)
{
    q6zip_clk_check_vote();

   if (dlpager_is_addr_in_rx_range(fault_addr_align))
        dlpager_load_virtual_page_rx(fault_addr_align, load_addr, page_size);
    else if (dlpager_is_addr_in_rw_range(fault_addr_align))
        dlpager_load_virtual_page_rw(fault_addr_align, load_addr, page_size);
    else
    {
      DL_DEBUG("fault_addr_align does not fall within text or rw sections \n");
      assert(0);
    }
}

/* returns total number of blocks in rw data and bss */
static unsigned int dlpager_make_rw_compress_table(dlpager_rw_metadata_t* rw_metadata_ptr)
{
    uint32_t i,num_rw_blocks,compressed_block_size,num_bss_blocks,bss_size;

  DL_DEBUG("Making RW compress table\n");

  /* check if RW segment is present */
  if (&__swapped_segments_rw_start__ == &__swapped_segments_end__)
    return 0;

  num_rw_blocks = rw_metadata_ptr->n_blocks;

  bss_size = end_va_uncompressed_rw - (unsigned int) &__swapped_segments_bss_start__;
  assert((bss_size & (PAGE_SIZE-1)) == 0); //must be a multiple of page size
  num_bss_blocks = bss_size/PAGE_SIZE;

#ifdef USE_DLPAGER_HEAP
  //initialize DL pager heap
  dlpager_heap_buffer = (byte*) &__dlpager_heap_start__;
  mem_init_heap(&dlpager_heap,dlpager_heap_buffer,(unsigned int)&__dlpager_heap_end__ - (unsigned int)&__dlpager_heap_start__,NULL);
#endif     

  //allocate array of pointers
#ifdef USE_DLPAGER_HEAP
  dlpager_rw_info.table = mem_malloc(&dlpager_heap,sizeof(dlpager_rw_table_t) * (num_rw_blocks + num_bss_blocks));
#else
  dlpager_rw_info.table = malloc(sizeof(dlpager_rw_table_t) * (num_rw_blocks + num_bss_blocks));
#endif

  assert(dlpager_rw_info.table != NULL);

  //allocate memory for each compressed block
  for (i = 0; i < num_rw_blocks; i++)
  {
    compressed_block_size = (i + 1 == num_rw_blocks)?
                                 (end_va_compressed_rw
                                   - rw_metadata_ptr->compressed_va[i]):
                                 (rw_metadata_ptr->compressed_va[i+1]
                                   - rw_metadata_ptr->compressed_va[i]);

#ifdef USE_DLPAGER_HEAP
    dlpager_rw_info.table[i].ptr = mem_malloc(&dlpager_heap,compressed_block_size+4); //1 more for extra 32 bit read into 64 bit word
#else
    dlpager_rw_info.table[i].ptr = malloc(compressed_block_size+4);
#endif

    assert(dlpager_rw_info.table[i].ptr != NULL);
    dlpager_rw_info.table[i].size = compressed_block_size;
    memcpy(dlpager_rw_info.table[i].ptr,(void*)rw_metadata_ptr->compressed_va[i],compressed_block_size);
    rw_initial_compressed_size += compressed_block_size;
  }

  //loop over bss blocks
  //continue i from above loop
  while (i < (num_rw_blocks + num_bss_blocks))
  {
    dlpager_rw_info.table[i].ptr = NULL;
    dlpager_rw_info.table[i].size = 0;
    i++;
  }

  rw_malloced_size[rw_circ_buf_idx] = rw_initial_compressed_size; //idx will be 0
  rw_malloced_size_always[rw_circ_buf_idx_always] = rw_initial_compressed_size; //idx will be 0

  dlpager_rw_info.num_elements = num_rw_blocks + num_bss_blocks;
  return (num_rw_blocks + num_bss_blocks);
}

//Needs to be implemented once we can reclaim
static void dlpager_free_rw_metadata(void)
{
  DL_DEBUG("Freeing compressed RW memory\n");
}

int dlpager_get_attr (dlpager_attr_t *pAttr)
{
    /* Actual interface need to configure the attributes */


    pAttr->swap_pool[0].size = q6zip_swap_pool_size;
    pAttr->swap_pool[0].page_size = PAGE_SIZE;
    pAttr->num_swap_pools = 1;

    return 0;
}

int dlpager_q6zip_init()
  {
    int i;

  unsigned int n_rw_blocks;
  unsigned int n_rx_blocks;

    if( 0 == start_va_compressed_text ) return -1; /* Q6zip symbols not populated */ 
    
  if ((unsigned int)&__swapped_segments_rw_start__ != (unsigned int)&__swapped_segments_end__)
	q6zip_swap_pool_size += 1024 * 1024;

  /* rx init */
  dlpager_rx_metadata = (dlpager_rx_metadata_t*) start_va_compressed_text;
  n_rx_blocks = dlpager_rx_metadata->n_blocks;
    
  rx_stats = (rx_stats_t *)malloc(sizeof(rx_stats_t) * n_rx_blocks);
  assert(rx_stats != NULL);

  for (i = 0; i < n_rx_blocks; i++)
    {
    rx_stats_t * rw_stat = &rx_stats[i];
    rw_stat->count = 0;
    rw_stat->min_cycles = MAX_LONG;
    rw_stat->max_cycles = 0;
    rw_stat->total_cycles = 0;
    }

  /* rw init */
  dlpager_rw_metadata_t* rw_metadata_ptr = (dlpager_rw_metadata_t*) start_va_compressed_rw;
  n_rw_blocks = dlpager_make_rw_compress_table(rw_metadata_ptr);  //create dynamic structure for rw blocks
  if (n_rw_blocks)
    dlpager_free_rw_metadata();  //not implemented yet

  if (n_rw_blocks)
{
    rw_stats = (rw_stats_t *)malloc(sizeof(rw_stats_t) * n_rw_blocks);
    assert(rw_stats != NULL);
    for (i = 0; i < n_rw_blocks; i++)
    {
      rw_stats_t * rw_stat = &rw_stats[i];

      rw_stat->decompress_count = 0;
      rw_stat->decompress_min_cycles = MAX_LONG;
      rw_stat->decompress_max_cycles = 0;
      rw_stat->decompress_total_cycles = 0;

      rw_stat->recompress_count = 0;
      rw_stat->recompress_min_cycles = MAX_LONG;
      rw_stat->recompress_max_cycles = 0;
      rw_stat->recompress_total_cycles = 0;
  }
  }

    q6zip_clk_init();

    return 0;
}
