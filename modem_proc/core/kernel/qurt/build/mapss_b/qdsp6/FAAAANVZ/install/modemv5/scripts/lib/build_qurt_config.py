#
# Build cust_config.o
#

def build_qurt_config(build_flags, asic, tools_path, cfile_path, object_path, use_llvm):
    import os, re, subprocess, sys

    # Do basic handling of the arguments
    if use_llvm:
        compiler = os.path.join('qc','bin','hexagon-clang')
    else:
        compiler = os.path.join('gnu','bin','hexagon-gcc')
    if tools_path:
        compiler = os.path.join(tools_path, compiler)
    else:
        compiler = os.path.basename(compiler)
    if build_flags:
        build_flags = build_flags.split()
    else:
        build_flags = []
    if asic:
        asic = ['-m%s' % asic]
    else:
        asic = []
    if not object_path:
        object_path = '.'

    compiler_command = [compiler, '-g', '-nostdinc', '-O2', '-G0'] + asic + build_flags
    compiler_command += ['-o', object_path, '-c', cfile_path]
    print ' '.join(compiler_command)

    if subprocess.Popen(compiler_command).wait():
        raise Exception('Unable to compile configuration object')
