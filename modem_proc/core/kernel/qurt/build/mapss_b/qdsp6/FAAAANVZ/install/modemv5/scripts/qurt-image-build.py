#! /usr/bin/python

import getopt
import os
import string
import struct
import sys

class Elf_File:
  def __init__(self, infile=None, outfile=None):
    if infile:
      self.appname = infile[0]
      self.filename = infile[1]
      self.file = open(self.filename, 'rb')
      self.restart = infile[2]
    elif outfile:
      self.appname = ''
      self.filename = outfile
      self.file = open(self.filename, 'wb')

class parse_generic:
  def __init__(self, data, fmt):
    tmp = struct.unpack(string.join(['<']+[e[1] for e in fmt],''), data)
    for j in range(len(fmt)):
      setattr(self, fmt[j][0], tmp[j])
    self.fmt = fmt
  def copy(self):
    from copy import copy as docopy
    return docopy(self)
  def output(self):
    return struct.pack(string.join(['<']+[e[1] for e in self.fmt],''), *tuple([getattr(self,e[0]) for e in self.fmt]))

''' Define formats of ELF structures '''
ehdr_format = [['e_ident'    , '16s', '%r'  ],
               ['e_type'     , 'H'  , '%u'  ],
               ['e_machine'  , 'H'  , '%u'  ],
               ['e_version'  , 'L'  , '%u'  ],
               ['e_entry'    , 'L'  , '0x%X'],
               ['e_phoff'    , 'L'  , '%u'  ],
               ['e_shoff'    , 'L'  , '%u'  ],
               ['e_flags'    , 'L'  , '0x%X'],
               ['e_ehsize'   , 'H'  , '%u'  ],
               ['e_phentsize', 'H'  , '%u'  ],
               ['e_phnum'    , 'H'  , '%u'  ],
               ['e_shentsize', 'H'  , '%u'  ],
               ['e_shnum'    , 'H'  , '%u'  ],
               ['e_shstrndx' , 'H'  , '%u'  ]]

phdr_format = [['p_type'  , 'L', '0x%X'],
               ['p_offset', 'L', '0x%X'],
               ['p_vaddr' , 'L', '0x%X'],
               ['p_paddr' , 'L', '0x%X'],
               ['p_filesz', 'L', '0x%X'],
               ['p_memsz' , 'L', '0x%X'],
               ['p_flags' , 'L', '0x%X'],
               ['p_align' , 'L', '0x%X']]

shdr_format = [['sh_name'     , 'L', '0x%X'],
               ['sh_type'     , 'L', '0x%X'],
               ['sh_flags'    , 'L', '0x%X'],
               ['sh_addr'     , 'L', '0x%X'],
               ['sh_offset'   , 'L', '0x%X'],
               ['sh_size'     , 'L', '0x%X'],
               ['sh_link'     , 'L', '0x%X'],
               ['sh_info'     , 'L', '0x%X'],
               ['sh_addralign', 'L', '0x%X'],
               ['sh_entsize'  , 'L', '0x%X']]

no_adjust = None

# zstr = str(bytearray(0x10000))
# Can't use above line with Python 2.4, try this:
zstr = '\0' * 0x10000

def padzero(outfile, length):
  while length > len(zstr):
    outfile.write(zstr)
    length -= len(zstr)
  if length > 0:
    outfile.write(zstr[0:length])

def copydata(outfile, infile, seekloc, length):
  infile.seek(seekloc)
  while length > 0x10000:
    outfile.write(infile.read(0x10000))
    length -= 0x10000
  if length > 0:
    outfile.write(infile.read(length))

def new_strtab_entry(f, s):
  s = s + '\0'
  f.strtab = f.strtab + s
  return len(f.strtab)-len(s)

def aligndown(n, a):
  return n & ~(a-1)

def alignup(n, a):
  return (n+(a-1)) & ~(a-1)

def string_lookup(f, n):
  s = f.strtab
  e = s.index('\0', n)
  return s[n:e]

''' Look through the list for segments which overlap or touch '''
''' Elements are sorted by the start address '''
''' If they overlap, combine them and combine permissions '''
''' If they touch without overlapping, only combine them if permissions are identical '''
''' Edit the list and return True if we were able to optimize '''
''' Leave the list and return False if we were not able to optimize '''
def seg_optimize(segs):
  for n in range(len(segs)-1):
    if segs[n][1] > segs[n+1][0]:
      ''' n and n+1 overlap '''
      if segs[n][2] != segs[n+1][2]:
        raise Exception('Overlapping segments with different memory maps')
      ''' n and n+1 overlap with the same offset '''
      ''' Combine them, including permissions, and return '''
      combined = (segs[n][0],
                  max(segs[n][1], segs[n+1][1]),
                  segs[n][2],
                  segs[n][3] or segs[n+1][3],
                  segs[n][4] or segs[n+1][4])
      segs = segs[:n] + [combined] + segs[n+2:]
      return (False, segs)
    elif segs[n][1] == segs[n+1][0]:
      ''' n and n+1 touch '''
      if segs[n][2:] == segs[n+1][2:]:
        ''' n and n+1 touch and have the same offset and permissions '''
        ''' combine them and return '''
        segs = segs[:n] + [(segs[n][0], segs[n+1][1]) + segs[n][2:]] + segs[n+2:]
        return (False, segs)
    else:
      ''' n and n+1 have separation between them '''
      pass
  return (True, segs)

''' New version of write_eip_info, writes header, variable length records, and string table '''
'''
    Top level header (fixed length, one only):
      uint32_t magic_number (0x32504945)
      uint16_t version_number
      uint16_t number_of_apps
    Second level header (fixed length, one per app):
      uint16_t first_memsection_number
      uint16_t number_of_memsections
      uint16_t entry_memsection_number
      uint16_t string_table_offset_of_app_name
    Third level header (fixed length, one per memsection):
      uint32_t virtual_address_start
      uint32_t physical_address_start
      uint32_t length_in_bytes
      uint32_t permissions
    String table:
      Zero terminated strings for app names
      (App names which are restartable are prepended with '>' greater than sign)
      (App names which are not-restartable are not prepended)
'''
def build_eip_info(f, infiles, island_moves):
  for i in infiles:
    segs = []
    for q in i.phdr:
      p = q.outcopy
      ''' Add the memory described in this program header '''
      virt_start = aligndown(p.p_vaddr, 0x1000)
      virt_end = alignup(p.p_vaddr + p.p_memsz, 0x1000)
      virt_offset = p.p_paddr - p.p_vaddr
      virt_exec = ((p.p_flags & 1) != 0)
      virt_write = ((p.p_flags & 2) != 0)
      if virt_start != virt_end and p.p_type == 1:
        segs += [(virt_start, virt_end, virt_offset, virt_exec, virt_write)]
    done, segs = (False, sorted(segs))
    while not done:
      done, segs = seg_optimize(segs)
    i.segs = segs
  top_hdr = (0x32504945,1,len(infiles))
  eip_data = struct.pack('<LHH', *top_hdr)
  strtab = ''
  for m in island_moves:
    strtab += struct.pack('<LLL', *m)
  strtab += struct.pack('<L', 0)
  secno = 0
  for i in infiles:
    segs = i.segs
    second_hdr = (secno, len(segs), secno, len(strtab))
    eip_data += struct.pack('<HHHH', *second_hdr)
    secno += len(segs)
    if i.restart:
      strtab += '>'
    strtab += i.appname + '\0'
  for i in infiles:
    segs = i.segs
    for s in segs:
      perms = 0
      ''' Add in exec and write permissions '''
      if s[3]:
        perms += 1
      if s[4]:
        perms += 2
      third_hdr = (s[0], s[0]+s[2], s[1]-s[0], perms)
      eip_data += struct.pack('<LLLL', *third_hdr)
  eip_data += strtab
  return eip_data

def parse_elf_header(f):
  try:
    f.file.seek(0)
    f.ehdr = parse_generic(f.file.read(52), ehdr_format)
    if f.ehdr.e_ehsize != 52:
      raise
    if f.ehdr.e_phentsize != 32:
      raise
    if f.ehdr.e_shentsize != 40:
      raise
  except:
    raise Exception('Error in ELF header for file %s' % (f.filename))

def parse_program_headers(f):
  try:
    f.file.seek(f.ehdr.e_phoff)
    f.phdr = []
    for j in range(f.ehdr.e_phnum):
      f.phdr += [parse_generic(f.file.read(f.ehdr.e_phentsize), phdr_format)]
  except:
    raise Exception('Error in ELF program headers for file %s' % (f.filename))

def parse_section_headers(f):
  try:
    f.file.seek(f.ehdr.e_shoff)
    f.shdr = []
    for j in range(f.ehdr.e_shnum):
      f.shdr += [parse_generic(f.file.read(f.ehdr.e_shentsize), shdr_format)]
  except:
    raise Exception('Error in ELF section headers for file %s' % (f.filename))

def read_string_table(f):
  try:
    f.file.seek(f.shdr[f.ehdr.e_shstrndx].sh_offset)
    f.strtab = f.file.read(f.shdr[f.ehdr.e_shstrndx].sh_size)
    if len(f.strtab) != f.shdr[f.ehdr.e_shstrndx].sh_size:
      raise
  except:
    raise Exception('Error in ELF string table for file %s' % (f.filename))

def read_elf_info(f):
  parse_elf_header(f)
  parse_program_headers(f)
  parse_section_headers(f)
  read_string_table(f)

def process_input_files(infiles, physaddr):
  curloc = infiles[0].ehdr.e_ehsize
  for f in infiles:
    for p in f.phdr:
      curloc += f.ehdr.e_phentsize
  for f in infiles:
    fbot = 0
    ftop = 0
    falign = 0
    for p in f.phdr:
      if p.p_type == 1 and p.p_filesz > 0:
        if ftop == 0:
          fbot = p.p_offset
          ftop = p.p_offset+p.p_filesz
          falign = p.p_align
        else:
          fbot = min(fbot, p.p_offset)
          ftop = max(ftop, p.p_offset+p.p_filesz)
          falign = max(falign, p.p_align)
    if falign == 0:
      falign = 1
    ''' Need to increase curloc until it is congruent to fbot modulo falign '''
    curloc += ((fbot%falign)+falign-(curloc%falign))%falign
    f.output_offset = curloc
    f.copy_bot = fbot
    f.copy_top = ftop
    f.filealign = falign
    curloc += (ftop-fbot)
  ''' Above figured out where we put all of the data in the output file '''
  ''' Now figure out where we put it in memory '''
  curloc = physaddr
  for f in infiles:
    mbot = 0
    mtop = 0
    malign = 0
    for p in f.phdr:
      ''' Only examining program headers of type LOAD and non-zero size '''
      if p.p_type == 1 and p.p_memsz > 0:
        if mtop == 0:
          mbot = p.p_vaddr
          mtop = p.p_vaddr+p.p_memsz
          malign = p.p_align
        else:
          mbot = min(mbot, p.p_vaddr)
          mtop = max(mtop, p.p_vaddr+p.p_memsz)
          malign = max(malign,p.p_align)
    if malign == 0:
      malign = 1
    ''' Need to increase curloc until it is congruent to mbot modulo malign '''
    curloc += ((mbot%malign)+malign-(curloc%malign))%malign
    f.output_physaddr = curloc
    f.vmem_bot = mbot
    f.vmem_top = mtop
    f.memalign = malign
    curloc += (mtop-mbot)
    ''' Round up to next 1MB boundary '''
    curloc = (curloc+0xFFFFF) & ~0xFFFFF

def populate_output_file(f, infiles):
  firstfile = infiles[0]
  lastfile = infiles[len(infiles)-1]
  f.phdr = []
  for i in infiles:
    for p in i.phdr:
      np = p.copy()
      p.outcopy = np
      if np.p_type == 6:
        ''' If the program header is of type PHDR, leave it alone '''
        pass
      else:
        if np.p_offset >= i.copy_bot and np.p_offset <= i.copy_top:
          np.p_offset = i.output_offset + (np.p_offset - i.copy_bot)
        if np.p_vaddr >= i.vmem_bot and np.p_vaddr <= i.vmem_top:
          np.p_paddr = i.output_physaddr + (np.p_vaddr - i.vmem_bot)
      f.phdr = f.phdr + [np]
  f.shdr = [firstfile.shdr[0]]
  f.strtab = ''
  new_strtab_entry(f, '\0.shstrtab')
  for i in infiles:
    for s in i.shdr:
      if s.sh_type != 0:
        if (s.sh_flags & 2) != 0 or string_lookup(i, s.sh_name) == '.comment':
          sp = s.copy()
          if sp.sh_offset >= i.copy_bot and sp.sh_offset <= i.copy_top:
            sp.sh_offset = i.output_offset + (sp.sh_offset - i.copy_bot)
          if i is firstfile:
            ''' Don't rename sections in the OS image '''
            sp.sh_name = new_strtab_entry(f, string_lookup(i, s.sh_name))
          else:
            sp.sh_name = new_strtab_entry(f, string_lookup(i, s.sh_name)+'.'+i.appname)
          f.shdr = f.shdr + [sp]
        else:
          pass
  new_strtab_entry(f, '\0')
  f.shdr += [firstfile.shdr[firstfile.ehdr.e_shstrndx]]
  f.shdr[len(f.shdr)-1].sh_name = 1
  f.ehdr = firstfile.ehdr.copy()
  f.ehdr.e_phoff = 52
  f.ehdr.e_entry = firstfile.output_physaddr
  f.ehdr.e_shoff = alignup(lastfile.output_offset + (lastfile.copy_top - lastfile.copy_bot), 4)
  f.ehdr.e_phnum = len(f.phdr)
  f.ehdr.e_shnum = len(f.shdr)
  f.ehdr.e_shstrndx = len(f.shdr)-1

def dump_output_file(f, infiles):
  f.file.write(f.ehdr.output())
  for p in f.phdr:
    f.file.write(p.output())
  for i in infiles:
    padzero(f.file, i.output_offset-f.file.tell())
    copydata(f.file, i.file, i.copy_bot, i.copy_top-i.copy_bot)
  padzero(f.file, f.ehdr.e_shoff-f.file.tell())
  f.shdr[len(f.shdr)-1].sh_offset = f.file.tell()+len(f.shdr)*f.ehdr.e_shentsize
  f.shdr[len(f.shdr)-1].sh_size = len(f.strtab)
  for s in f.shdr:
    f.file.write(s.output())
  f.file.write(f.strtab)
  f.file.flush()
  f.file.close()

def adjust_input_file(i, newnamefunc):
  outname = newnamefunc(i.filename)
  inplace = (outname == i.filename)
  # filemode = ('r+b' if inplace else 'wb')
  # Can't use above line with Python 2.4, try this:
  filemode = ('wb','r+b')[inplace]
  nfile = open(outname, filemode)
  i.ehdr.e_entry = i.output_physaddr
  nfile.write(i.ehdr.output())
  for p in i.phdr:
    p.p_paddr = p.outcopy.p_paddr
    nfile.write(p.output())
  if not inplace:
    ''' Copy rest of input file. '''
    # i.file.seek(0, os.SEEK_END)
    # Can't use above line with Python 2.4, try this:
    i.file.seek(0, 2)
    copydata(nfile, i.file, nfile.tell(), i.file.tell() - nfile.tell())
  nfile.flush()
  nfile.close()

def splitname(s, restart):
  names = s.split('=')
  if len(names) == 1:
    return (os.path.basename(s).replace('elf','pbn'), s, restart)
  if len(names) == 2:
    return (names[0], names[1], restart)
  raise Exception("input file argument cannot contain multiple '=' characters")

def parse_args(argv):
  global no_adjust
  opts, args = getopt.gnu_getopt(argv[1:], "eno:p:t:R:")
  optdict = dict()
  for opt, arg in opts:
    optdict[opt] = optdict.get(opt,[]) + [arg]
  no_adjust = '-n' in optdict
  outfilename = optdict.get('-o',[])
  physaddr = optdict.get('-p',[])
  restarts = optdict.get('-R', [])
  args = args+restarts
  if len(args) == 0:
    raise Exception('must provide at least one input file')
  if len(outfilename) != 1:
    raise Exception('must provide exactly one output file (-o)')
  if len(physaddr) != 1:
    raise Exception('must provide exactly one physical address (-p)')
  try:
    physaddr = long(physaddr[0], 0)
    if physaddr < 0 or physaddr >= pow(2,32):
      raise
    if physaddr & 0xFFF:
      raise
  except:
    raise Exception('physical address (-p) must be a 32-bit unsigned integer on a 4K boundary')
  args = [splitname(arg, arg in restarts) for arg in args]
  return (args, outfilename[0], physaddr)

def rename_input_file(ifilename):
  ''' Replace the last dot (.) in the name with _reloc. '''
  ''' If no dot, this returns unchanged... '''
  return '_reloc.'.join(ifilename.rsplit('.',1))

class HexagonMapping:
  """Class for representing a Hexagon TLB entry."""
  def __init__(self):
    pass
  def get_TLB_entry(self):
    """Return the 64-bit encoded TLB entry."""
    # Get the size in place first
    ret = 1
    while ret*ret < self.size:
      ret *= 2
    if ret*ret != self.size:
      raise Exception('Internal error -- locked TLB entry of illegal size')
    ret |= (1L << 63)       # Set the valid bit
    ret |= self.G << 62
    if self.ppage & 0x800000:
      ret |= (1L << 61)
    ret |= self.A1A0 << 59
    ret |= self.ASID << 52
    ret |= self.vpage << 32
    ret |= self.X << 31
    ret |= self.W << 30
    ret |= self.R << 29
    ret |= self.U << 28
    ret |= self.C << 24
    ret |= (self.ppage & 0x7FFFFF) << 1
    return ret
  def set_from_TLB_entry(self, v, locked=True, atomic=True, reloc=False):
    """Set the fields of the mapping based on the 64-bit TLB entry (v)."""
    if (v & (1L << 63)) == 0:
      raise Exception('Tried to use a TLB entry not set to valid')
    tmp = (v & 0x7F)
    tmp = (tmp & -tmp)    # Sets to 0, 1, 2, 4, 8, 16, 32, or 64
    sz = tmp * tmp       # Sets to 0, 1, 4, 16, 64, 256, 1024, or 4096
    if sz == 0:
      raise Exception('Tried to use a TLB entry with invalid size')
    self.island = False
    self.atomic = atomic
    self.locked = locked
    self.reloc = reloc
    self.G    = ((v >> 62) & 0x01)
    self.A1A0 = ((v >> 59) & 0x03)
    self.ASID = ((v >> 52) & 0x7F)
    self.X    = ((v >> 31) & 0x01)
    self.W    = ((v >> 30) & 0x01)
    self.R    = ((v >> 29) & 0x01)
    self.U    = ((v >> 28) & 0x01)
    self.C    = ((v >> 24) & 0x0F)
    self.vpage = ((v >> 32) & 0xFFFFF) & (~(sz-1))
    self.ppage = ((v >>  1) & 0x7FFFFF) & (~(sz-1))
    if ((v >> 61) & 0x01):
      # EP bit is set, ppage gets high bit set
      self.ppage += 0x800000
    self.size = sz
    return self
  def contains(self, other):
    """Return True if self is a proper superset of other."""
    my_attr = (self.atomic, self.locked, self.G, self.A1A0, self.ASID,
               self.X, self.W, self.R, self.U, self.C, self.reloc)
    his_attr = (other.atomic, other.locked, other.G, other.A1A0, other.ASID,
                other.X, other.W, other.R, other.U, other.C, other.reloc)
    if my_attr != his_attr:
      return False
    if self.vpage - self.ppage != other.vpage - other.ppage:
      return False
    if other.vpage < self.vpage:
      return False
    if other.vpage + other.size > self.vpage + self.size:
      return False
    return True
  def fix_to_TLB_entries(self, maxsize=0x1000):
    """Takes a mapping of any size and returns an equivalent list of mappings of legal sizes."""
    from copy import copy
    ret = []
    off = 0            # Offset from beginning
    sz = self.size     # Number of pages left
    while sz > 0:
      this_sz = 0x1000
      while this_sz > 1:
        if this_sz <= sz and this_sz <= maxsize:
          if ((self.vpage+off) & (self.ppage+off) & (this_sz-1)) == 0:
            break
        this_sz = this_sz >> 2
      tmp = copy(self)
      tmp.vpage += off
      tmp.ppage += off
      tmp.size = this_sz
      ret.append(tmp)
      sz -= this_sz
      off += this_sz
    return ret
  def __str__(self):
    return '<%05X->%06X [%04X] G:%u A:%u ASID:%u X:%u W:%u R:%u U:%u C:%X %s %s %s %s>' % (self.vpage,
                                                                                           self.ppage,
                                                                                           self.size,
                                                                                           self.G,
                                                                                           self.A1A0,
                                                                                           self.ASID,
                                                                                           self.X,
                                                                                           self.W,
                                                                                           self.R,
                                                                                           self.U,
                                                                                           self.C,
                                                                                           ('unlocked','locked')[self.locked],
                                                                                           ('non-atomic','atomic')[self.atomic],
                                                                                           ('non-island','island')[self.island],
                                                                                           ('non-reloc','reloc')[self.reloc])

class QurtElfPatch:
  """Class for doing ELF patch processing on the merged ELF file."""
  #
  # When we call start() on this class, the class becomes basically like
  #  a 4GB bytearray in the ELF's virtual address space.
  # Slices of the virtual address space can be accessed using standard
  #  slice notation.  For instance, to access the 4 bytes at address ADDR
  #  you can use self[ADDR:ADDR+4].
  # To read the same 4 bytes into X as an unsigned 32-bit quantity:
  #  X = struct.unpack('<L', self[ADDR:ADDR+4])
  # To write the same 4 bytes with X which is an unsigned 32-bit quantity:
  #  self[ADDR:ADDR+4] = struct.pack('<L', X)
  #
  # Note that accesses outside of the defined virtual image of the ELF cause
  #  an exception.  Portions of the virtual image which are not backed by
  #  file data (such as bss sections, etc.) can be read, but return zeroes;
  #  attempts to write non-zero data to those cause an exception.
  #
  def __init__(self):
    self.all_mappings = []
    self.opt_mappings = []
    self.island_moves = []
    pass
  def __getitem__(self, key):
    if isinstance(key,slice):
      sz = key.stop - key.start
      addr = key.start
    else:
      sz = key[1]
      addr = key[0]
    ret = ''
    while sz > 0:
      this_sz = 0
      for p in self.phdr:
        if addr >= p.p_vaddr:
          if addr < p.p_vaddr + p.p_filesz:
            # The next byte we need to read is in this segment
            this_sz = min(sz, (p.p_vaddr + p.p_filesz) - addr)
            this_offset = p.p_offset + (addr - p.p_vaddr)
            self.file.seek(this_offset)
            tmp = self.file.read(this_sz)
            this_sz = len(tmp)
            addr += this_sz
            sz -= this_sz
            ret += tmp
            break
      if this_sz == 0:
        raise Exception('Cannot read %u bytes at vaddr %X' % (key.stop - key.start, key.start))
    return ret
  def __setitem__(self, key, value):
    sz = len(value)
    if isinstance(key,slice):
      addr = key.start
    else:
      addr = key[0]
    while sz > 0:
      this_sz = 0
      for p in self.phdr:
        if addr >= p.p_vaddr:
          if addr < p.p_vaddr + p.p_filesz:
            # The next byte we need to read is in this segment
            this_sz = min(sz, (p.p_vaddr + p.p_filesz) - addr)
            this_offset = p.p_offset + (addr - p.p_vaddr)
            self.file.seek(this_offset)
            self.file.write(value[:this_sz])
            self.file.flush()
            addr += this_sz
            sz -= this_sz
            value = value[this_sz:]
            break
      if this_sz == 0:
        raise Exception('Cannot read %u bytes at vaddr %X' % (key.stop - key.start, key.start))
  def start(self, filename):
    self.file = open(filename, 'r+b')
    parse_elf_header(self)
    parse_program_headers(self)
    parse_section_headers(self)
    read_string_table(self)
  def unpack_from(self, fmt, offset):
    return struct.unpack(fmt, self[(offset,struct.calcsize(fmt))])
  def finish(self):
    self.file.flush()
    self.file.close()
  def read_EIP(self, vaddr):
    self.eip_location = vaddr
    eip_header = self.unpack_from('<'+'L'*10, vaddr)
    self.eip_zero             = eip_header[0]     # See kernel/asm/elf_info_patch.inc for the format
    self.eip_version          = eip_header[1]     #  of the EIP area
    self.eip_size             = eip_header[2]
    self.eip_flags            = eip_header[3]
    self.eip_addr_phys_island = eip_header[4]
    self.eip_addr_tlb_dump    = eip_header[5]
    self.eip_size_tlb_dump    = eip_header[6]
    self.eip_addr_mmap_table  = eip_header[7]
    self.eip_addr_tlblock     = eip_header[8]
    self.eip_addr_tlb_relocs  = eip_header[9]
  def check_EIP(self):
    # Raise an exception if the EIP looks wrong.
    if self.eip_zero != 0 or self.eip_version != 6:
      raise Exception('ELF Info Patch header is not valid.  Cannot complete image build.')
  def advanced_EIP(self):
    # Return True if advanced memory handling is requested in the EIP flags
    return ((self.eip_flags & 1) != 0)
  def write_EIP(self, data):
    if len(data) > self.eip_size:
      raise Exception('ELF Info Patch area overflow.  Cannot complete image build.')
    self[(self.eip_location,len(data))] = data
  def read_phys_island(self):
    # Get the phys island data from the ELF file
    addr = self.eip_addr_phys_island
    island_info = []
    while True:
      tmp = self.unpack_from('<L', addr)[0]
      if tmp == 0xFFFFFFFF:
        break
      addr += 4
      island_info.append(tmp)
    self.phys_island_regions = []
    while island_info:
      self.phys_island_regions.append([island_info[0],island_info[1],island_info[2]])
      island_info = island_info[3:]
  def read_tlb_dump(self):
    addr = self.eip_addr_tlb_dump
    tlb_dump = []
    for i in range(self.eip_size_tlb_dump):
      tmp = self.unpack_from('<Q', addr)[0]
      if (tmp & (1L << 63)):
        tlb_dump.append(HexagonMapping().set_from_TLB_entry(tmp))
      addr += 8
    self.tlb_dump = tlb_dump
    self.all_mappings.extend(tlb_dump)
  def write_tlb_dump(self, v, vn):
    addr = self.eip_addr_tlb_dump
    for tmp in v:
      self[(addr,8)] = struct.pack('<Q', tmp)
      addr += 8
    for tmp in vn:
      self[(addr,8)] = struct.pack('<Q', tmp)
      addr += 8
    self[(self.eip_addr_tlb_relocs,4)] = struct.pack('<L', len(v))
  def read_mmap_table(self):
    lock_count = self.unpack_from('<L', self.eip_addr_tlblock)[0]
    addr = self.eip_addr_mmap_table
    mmap_table_locked = []
    mmap_table_unlocked = []
    while True:
      tmp = self.unpack_from('<Q', addr)[0]
      if tmp == 0:
        break
      addr += 8
      if len(mmap_table_locked) < lock_count:
        mmap_table_locked.append(HexagonMapping().set_from_TLB_entry(tmp, locked=True))
      else:
        mmap_table_unlocked.append(HexagonMapping().set_from_TLB_entry(tmp, locked=False, atomic=False))
    self.mmap_table_locked = mmap_table_locked
    self.mmap_table_unlocked = mmap_table_unlocked
    self.all_mappings.extend(mmap_table_locked)
    self.all_mappings.extend(mmap_table_unlocked)
  def synthesize_elf_mappings(self, infiles):
    asid = 0
    for i in infiles:
      for s in i.shdr:
        if (s.sh_flags & 2) != 0 and s.sh_size != 0:        # Section is memory resident
          sname = string_lookup(i, s.sh_name)
          match = []
          for p in self.phdr:
            if s.sh_addr >= p.p_vaddr:
              if s.sh_addr + s.sh_size <= p.p_vaddr + p.p_memsz:
                match.append(p)
          if len(match) != 1:
            raise Exception('Cannot find unique PHDR for section %s' % sname)
          p = match[0]
          paddr = s.sh_addr + p.p_paddr - p.p_vaddr
          perm_write = ((s.sh_flags & 1) != 0)
          perm_exec = ((s.sh_flags & 4) != 0)
          self.add_elf_mapping(s.sh_addr, s.sh_size, paddr, perm_write, perm_exec, asid, sname.endswith('.island'))
      asid += 1
  def add_elf_mapping(self, vaddr, size, paddr, perm_write, perm_exec, asid, island):
    # Round beginning down to 4K boundary
    size += (vaddr & 0xFFF)
    paddr -= (vaddr & 0xFFF)
    vaddr -= (vaddr & 0xFFF)
    # Convert size to page count
    pgcnt = (size + 0xFFF) >> 12
    if (paddr & 0xFFF) != 0:
      raise Exception('Virtual/physical address mis-alignment')
    vpage = vaddr >> 12
    ppage = paddr >> 12
    tmp = HexagonMapping()
    tmp.G = int(asid == 0)
    tmp.A1A0 = 0
    tmp.ASID = asid
    tmp.X = int(perm_exec)
    tmp.W = int(perm_write)
    tmp.R = 1
    tmp.U = int(asid != 0)
    tmp.C = 0x7                # Hard code L2 cacheable
    tmp.vpage = vpage
    tmp.ppage = ppage
    tmp.size = pgcnt
    tmp.locked = ((asid == 0) or island)
    tmp.atomic = False
    tmp.island = island
    tmp.reloc = True
    tmp = tmp.fix_to_TLB_entries(maxsize = 0x100)    # Avoid mappings larger than 1MB (PIL only guarantees 1MB)
    self.all_mappings.extend(tmp)
  def sort_mappings(self):
    """Sort the all_mappings array."""
    for t in self.all_mappings:
      t.sortkey = (t.vpage << 32) + (t.size)
    self.all_mappings.sort(key=lambda x: x.sortkey)
  def get_ranges(self):
    """Compute the list of virtual address ranges to be mapped."""
    current_range = None
    all_ranges = []
    for t in self.all_mappings:
      if t.atomic:
        if current_range:
          all_ranges.append(current_range)
        all_ranges.append([t.vpage, t.vpage+t.size, [t]])
        current_range = None
      elif current_range == None:
        current_range = [t.vpage, t.vpage+t.size, [t]]
      elif t.vpage <= current_range[1]:
        current_range[1] = max(current_range[1], t.vpage+t.size)
        current_range[2].append(t)
      else:
        all_ranges.append(current_range)
        current_range = [t.vpage, t.vpage+t.size, [t]]
    if current_range:
      all_ranges.append(current_range)
    self.all_ranges = all_ranges
  def get_map(self, vaddr, size, mappings):
    # Get the list of mappings that overlap the proposed (vaddr,size)
    overlap = []
    for t in mappings:
      if t.vpage + t.size <= vaddr:
        continue
      if t.vpage >= vaddr + size:
        continue
      overlap.append(t)
    if not overlap:
      raise Exception('Internal error in image builder -- hole found in range')
    from copy import copy
    proposed = copy(overlap[0])
    proposed.vpage = vaddr
    proposed.size = size
    for t in overlap[1:]:
      if proposed.vpage - proposed.ppage != t.vpage - t.ppage:
        return False
      if proposed.G != t.G:
        return False
      if proposed.A1A0 != t.A1A0:
        return False
      if proposed.ASID != t.ASID:
        return False
      if proposed.R != t.R:
        return False
      if proposed.U != t.U:
        return False
      if proposed.C != t.C:
        return False
      if proposed.locked != t.locked:
        return False
      if proposed.atomic or t.atomic:
        return False
      if proposed.island != t.island:
        return False
      if proposed.reloc != t.reloc:
        return False
      if proposed.X and t.W:
        return False
      if proposed.W and t.X:
        return False
      if t.W:
        proposed.W = True
      if t.X:
        proposed.X = True
    return proposed
  def get_opt_mappings(self):
    for a in self.all_ranges:
      base = a[0]
      nsize = a[1]-a[0]
      while nsize:
        best_map = None
        sz_lo = 0
        sz_hi = nsize
        # Do binary search on the size to find the biggest size
        #  that actually works
        while sz_hi > sz_lo:
          tmp = (sz_hi + sz_lo + 1) / 2
          v = self.get_map(base, tmp, a[2])
          if v:
            best_map = v
            sz_lo = tmp
          else:
            sz_hi = tmp - 1
        if best_map.locked:
          # Locked mappings must be actually achievable in hardware
          tmp = 0x1000
          if best_map.reloc:
            tmp = 0x100         # For reloc, don't use entries over 1 MB
          while True:
            if tmp <= sz_lo:
              if (best_map.vpage & (tmp-1)) == 0:
                if (best_map.ppage & (tmp-1)) == 0:
                  break
            tmp = tmp/4
          sz_lo = tmp
          best_map.size = tmp
        base += sz_lo
        nsize -= sz_lo
        self.opt_mappings.append(best_map)
  def move_island_mappings(self):
    # Get the list of island mappings
    w = []
    for t in self.opt_mappings:
      if t.island:
        w.append(t)
    # Set a sort key for each island mapping which is the smallest
    #  power of two that divides both the base address and the size
    for t in w:
      tmp = (t.vpage | t.size)
      tmp = (tmp & -tmp)
      t.sortkey = tmp
    w.sort(key=lambda x: x.sortkey)
    for t in w:
      moved = False
      for m in self.phys_island_regions:
        if not moved:
          if (m[0] & (t.sortkey-1)) == 0:
            # Alignment checks out
            if m[0]+t.size <= m[1]:
              # Size checks out
              t.ppage = m[0]
              t.C = m[2]
              t.reloc = False
              m[0] += t.size
              moved = True
              self.island_moves.append([t.vpage << 12, t.ppage << 12, t.size << 12])
      if not moved:
        raise Exception('Problem with fitting all island sections into the island')
  def ProcessFile(self, filename, infiles):
    self.start(filename)
    # Find the lowest virtual address in the os image
    base_addr = min([p.p_vaddr for p in infiles[0].phdr if p.p_type == 1 and p.p_filesz > 0])
    self.read_EIP(base_addr+64)
    self.check_EIP()
    if self.advanced_EIP():
      self.read_phys_island()
      self.read_tlb_dump()
      self.read_mmap_table()
      self.synthesize_elf_mappings(infiles)
      self.sort_mappings()
      self.get_ranges()
      self.get_opt_mappings()
      if self.phys_island_regions:
        self.move_island_mappings()
      v = []
      vn = []
      for t in self.opt_mappings:
        if t.locked and t.reloc:
          v.append(t.get_TLB_entry())
      for t in self.opt_mappings:
        if t.locked and not t.reloc:
          vn.append(t.get_TLB_entry())
      self.write_tlb_dump(v,vn)
    self.write_EIP(build_eip_info(None, infiles, self.island_moves))
    self.finish()

def qurt_image_build(argv):
  try:
    infilenames, outfilename, physaddr = parse_args(argv)
    ''' Create a list of dictionaries, one for each of the input files '''
    infiles=[Elf_File(infile=f) for f in infilenames]
    for f in infiles:
      read_elf_info(f)
    ''' Now figure out our basic relocation information '''
    process_input_files(infiles, physaddr)
    outfile = Elf_File(outfile=outfilename)
    ''' Populate the output file structure '''
    populate_output_file(outfile, infiles)
    dump_output_file(outfile, infiles)
    QurtElfPatch().ProcessFile(outfilename, infiles)
    if not no_adjust:
      for i in infiles:
        adjust_input_file(i, rename_input_file)
    return 0
  except Exception, err:
    from traceback import format_exc as tbstr
    print tbstr()
    print >> sys.stderr, "%s: %s" % (argv[0], str(err))
    return 1

sys.exit(qurt_image_build(sys.argv))
