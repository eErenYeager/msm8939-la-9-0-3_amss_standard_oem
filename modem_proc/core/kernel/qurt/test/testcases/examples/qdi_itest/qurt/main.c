#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <assert.h>
#include <qurt.h>
#include <qurt_timer.h>
#include <qurt_cycles.h>

#include <string.h>
#include <qurt_qdi.h>
#include <qurt_qdi_driver.h>

#define STKSIZE 16384

struct qurt_msgq {
   unsigned long long * volatile head;
   unsigned long long * volatile tail;
   unsigned long long * volatile start;
   unsigned long long * volatile end;
};

static unsigned long long keep_pcycles;
static char test_string[128];
static unsigned count;

int itest_call(int a, int b)
{
   unsigned long long new_pcycles;

   ++count;

   if (a==1) {
      keep_pcycles = qurt_get_core_pcycles();
   }

   if (a==10000) {
      new_pcycles = qurt_get_core_pcycles();
      printf("%s, count %u, elapsed == %llu pcycles\n", test_string, count, new_pcycles-keep_pcycles);
   }

   return 0;
}

int itest_getstring(int client_handle, const char *msg)
{
   count=0;
   return qurt_qdi_copy_from_user(client_handle, test_string, msg, sizeof(test_string)-1);
}

void servicemain(void *arg)
{
   struct qurt_msgq *ptr;
   unsigned long long *newp;
   unsigned long long *p;
   unsigned long long val;

   ptr = arg;

   p = ptr->tail;
   for (;;) {
      newp = p+1;
      if (newp == ptr->end)
         newp = ptr->start;
      while (ptr->head == p)
         ;
      val = *p;
      ptr->tail = newp;
      p = newp;
      itest_call((unsigned)(val>> 0),
                 (unsigned)(val>>32));
   }
}

int itest_start_queue(int client_handle, struct qurt_msgq **pp)
{
   qurt_thread_attr_t ta;
   qurt_thread_t tid;
   struct qurt_msgq *p;
   // int old;

   p = qurt_qdi_usermalloc(client_handle, 0x4000);    /* 16K in size */

   p->head = (void *)(p+1);
   p->tail = (void *)(p+1);
   p->start = (void *)(p+1);
   p->end = (void *)((char *)p+0x4000);

   *pp = p;

   // old = qurt_space_switch(0);
   qurt_thread_attr_init(&ta);
   qurt_thread_attr_set_stack_size(&ta, STKSIZE);
   qurt_thread_attr_set_stack_addr(&ta, malloc(STKSIZE));
   qurt_thread_attr_set_priority(&ta, 20);
   qurt_thread_create(&tid, &ta, servicemain, p);
   // qurt_space_switch(old);

   return 0;
}

int itest_wait_for_done(void)
{
   volatile int x;

   /* Pause for about 100000 accesses */

   for (x=0; x<=100000; x++)
      ;

   return 0;
}

int itest_opener_invocation(int client_handle, qurt_qdi_obj_t *ptr, int method,
                            qurt_qdi_arg_t a1, qurt_qdi_arg_t a2,
                            qurt_qdi_arg_t a3, qurt_qdi_arg_t a4,
                            qurt_qdi_arg_t a5, qurt_qdi_arg_t a6,
                            qurt_qdi_arg_t a7, qurt_qdi_arg_t a8,
                            qurt_qdi_arg_t a9)
{
   switch (method) {
   case QDI_OPEN:
      return qurt_qdi_new_handle_from_obj_t(client_handle, ptr);
   case QDI_PRIVATE:
      return itest_call(a1.num, a2.num);
   case QDI_PRIVATE+1:
      return itest_getstring(client_handle, a1.ptr);
   case QDI_PRIVATE+2:
      return itest_start_queue(client_handle, a1.ptr);
   case QDI_PRIVATE+3:
      return itest_wait_for_done();
   default:
      return qurt_qdi_method_default(client_handle, ptr, method,
                                     a1, a2, a3, a4, a5, a6, a7, a8, a9);
   }
}

void itest_opener_release(qurt_qdi_obj_t *ptr)
{
   /* Nothing to do, object is permanent */
}

struct itest_opener {
   qurt_qdi_obj_t qdiobj;
};

static const struct itest_opener itest_opener = {
   {
      itest_opener_invocation,
      QDI_REFCNT_PERM,
      itest_opener_release
   }
};

int main(int argc, char **argv)
{
   int status;
   int ret;

   if (argv[1])
      printf("called with argv[1] == %s\n",
             argv[1]);

   qurt_qdi_register_devname("/dev/itest", (void *)&itest_opener);

   qurt_spawn("app1.pbn");

   ret = qurt_wait(&status);

   printf("app1 finished, ret 0x%X, status 0x%X\n",
          ret, status);

   return 0;
}

