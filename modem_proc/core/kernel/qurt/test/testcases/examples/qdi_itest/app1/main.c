#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "qurt_qdi.h"
#include "qurt_mutex.h"
#include "qurt_thread.h"

struct qurt_msgq {
   unsigned long long * volatile head;
   unsigned long long * volatile tail;
   unsigned long long * volatile start;
   unsigned long long * volatile end;
};

void qurt_msgq_send64(struct qurt_msgq *ptr,
                      unsigned long long val)
{
   unsigned long long *newp;
   unsigned long long *p;

   p = ptr->head;
   newp = p+1;
   if (newp == ptr->end)
      newp = ptr->start;
   while (ptr->tail == newp)
      ;
   *p = val;
   ptr->head = newp;
}

int main(int argc, char **argv)
{
   int cnt;
   int h;

   h = qurt_qdi_open("/dev/itest");
   qurt_qdi_handle_invoke(h, QDI_PRIVATE+1, "This test does an invocation per call");
   for (cnt=1; cnt<=10000; cnt++)
      qurt_qdi_handle_invoke(h, QDI_PRIVATE, cnt, 2*cnt);
   qurt_qdi_close(h);

#if 1
   {
      struct qurt_msgq *ptr;

      h = qurt_qdi_open("/dev/itest");
      qurt_qdi_handle_invoke(h, QDI_PRIVATE+1, "This test uses message queue calls");
      /* Get the message queue ptr */
      qurt_qdi_handle_invoke(h, QDI_PRIVATE+2, &ptr);
      for (cnt=1; cnt<=10000; cnt++)
         qurt_msgq_send64(ptr,
                          (((unsigned long long)(2*cnt))<<32) |
                          (((unsigned long long)(  cnt))<< 0));
      qurt_qdi_handle_invoke(h, QDI_PRIVATE+3);
      qurt_qdi_close(h);
   }
#endif

   qurt_thread_stop();

   return 0;
}
