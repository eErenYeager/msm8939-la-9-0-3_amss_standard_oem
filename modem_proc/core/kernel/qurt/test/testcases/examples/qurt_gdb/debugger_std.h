#ifndef __DEBUGGER_STD_H__
#define __DEBUGGER_STD_H__

#define GET_OFFSETOF(type, member)          (((char*)(&((type*)1)->member))-((char*)1))
#define ASSERT_CONCAT_(a, b)                a##b
#define ASSERT_CONCAT(a, b)                 ASSERT_CONCAT_(a, b)
#define CHECK_SIZE(a, size)                 (((a) == (size)))
#define STATIC_ASSERT(e, msg)               enum  {ASSERT_CONCAT(msg, __LINE__) = 1/(!!(e))};

/**
    Changing any of these numbers have to be done very carefully
    and distributed to all images where this is compiled.
*/
#define PROCESSOR_HEXAGON_ID                (0x1)
#define PROCESSOR_ARM_ID                    (0x2)
#define PROCESSOR_ID_START                  (0xF0000000)

#define MIN_PAGE_SIZE                       4096
#define NUM_CACHE_LINE_WORDS                8
#define MAX_NAME_CHARS                      12
#define SIZEOF_PD_ENTRY                     128
/* size of protocol envelope header */
#define SIZEOF_PROTOCOL_ENVELOPE_HEADER     32
#define SIZEOF_PROTOCOL_BUFFER              (MIN_PAGE_SIZE/4)
#define SIZEOF_SCRATCH_BUFFER               (MIN_PAGE_SIZE - SIZEOF_PD_ENTRY - (2*SIZEOF_PROTOCOL_BUFFER))
/* Max number of chars that the stub can recieve per session including null term of the ascii string */
#define MAX_PROTOCOL_PAYLOAD_SIZE           ((SIZEOF_SCRATCH_BUFFER/2) - SIZEOF_PROTOCOL_ENVELOPE_HEADER - sizeof(uint32_t))

/* validation check to ensure sizes of basic data types should be consistent on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(char), 1), static_size_check_failed_line_num_)
STATIC_ASSERT(CHECK_SIZE(sizeof(uint32_t), 4), static_size_check_failed_line_num_)

struct smem_io_buffer_layout
{
    char envelope[SIZEOF_PROTOCOL_ENVELOPE_HEADER];
    uint32_t payload_length;
    char        payload[SIZEOF_PROTOCOL_BUFFER - SIZEOF_PROTOCOL_ENVELOPE_HEADER - sizeof(uint32_t)];
} __attribute__ ((__packed__));
typedef struct smem_io_buffer_layout siobl;
/* validation check to ensure size should be 1K on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(siobl), 1024), static_size_check_failed_line_num_)


/** Offsets into the SMEM page layout for the debugger */
struct smem_page_layout
{
    char pd_header[SIZEOF_PD_ENTRY];                /**   0  -  127 */
    siobl dbgr_to_ss_buffer;                        /**  128 - 1151 */
    siobl ss_to_dbgr_buffer;                        /** 1152 - 2175 */
    char scratch_buffer[SIZEOF_SCRATCH_BUFFER];     /** 2176 - 4095 */
} __attribute__ ((__packed__));
typedef struct smem_page_layout smpgl;
/* validation check to ensure size should be 4K on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(smpgl), 4096), static_size_check_failed_line_num_)

#ifndef PROCESSOR_ID
#define PROCESSOR_ID
typedef uint32_t processor_id;
#endif //PROCESSOR_ID

typedef enum
{
    DBG_CLOSED   = 0x0,
    DBG_OPEN     = 0x1,
    DBG_INVALID  = 0x2
} remote_debugger_status;

typedef enum
{
    PROCESS_RDY         = 0x1,
    PROCESS_EXCEPTION   = 0x2,
    PROCESS_EXITED      = 0x3,
    PROCESS_INVALID     = 0x4
} target_process_status;

/** 
    @brief Data structure that represents a running
    process's information on a subsytem processor.
    This is filled in by the subsystem whenever a 
    process is spawned. Only the subsystem debug stub
    alters this structure's contents.
    
    @note This has to be 32 bytes long.
*/
struct sub_system_process_descriptor
{
    processor_id    debug_proc_id;                  /**< Subsystem processor identifier running the process */
    uint32_t        process_id;                     /**< Process ID assigned to the process */
    char            process_name[MAX_NAME_CHARS];   /**< Process name */
    uint32_t        process_state;                  /**< Current state of the process. When a process is created, 
                                                    its state is PROCESS_RDY and when it exits, its state should be
                                                    PROCESS_EXITED. In the event the process runs into an exception, its
                                                    state will be marked as PROCESS_EXCEPTION */
    uint32_t        local_seq;                      /**< Whenver the debug stub sends out a debug packet, it
                                                    increments this sequence number */
    uint32_t        remote_seq;                     /**< The debug stub maintains this sequence number by looking at the 
                                                    local sequence number of the AP process descriptor. Thus if the AP
                                                    process descriptor is newer, it implies the debug stub has new data to
                                                    be process */
}  __attribute__ ((__packed__));
typedef struct sub_system_process_descriptor ss_process_descriptor;
/* validation check to ensure size should be 32b on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(ss_process_descriptor), 32), static_size_check_failed_line_num_)

/** 
    @brief Data structure that provides information about
    the debug port and shared memory buffer needed for
    communication between the debug agent and the debugger.
    A port and shared memory block is reserved for a process
    whenever it is first created and released whenever it 
    exits. In the case the process runs into an exception,
    assuming the subsystem can keep the runtime view of the 
    process intact, it can mark the process state as 
    PROCESS_EXCEPTION.

    @note: This structure is filled in by the debug agent 
    running on the HLOS processor.
    
    @note This has to be 32 bytes long.
*/
struct ap_process_descriptor
{
    uint32_t        debug_port;
    uint64_t        timestamp;
    uint32_t        phys_start;
    uint32_t        phys_size;
    uint32_t        debugger_state;
    uint32_t        local_seq;
    uint32_t        remote_seq;
} __attribute__ ((__packed__));
typedef struct ap_process_descriptor ap_process_descriptor;
/* validation check to ensure size should be 32b on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(ap_process_descriptor), 32), static_size_check_failed_line_num_)

/** 
    @brief This data structure represents complete debug 
    information about a process running on a subsystem.

    @note: The SS descriptor structure is filled in by 
    a debug stub running on a subsystem processor but is
    read by a debug agent running on the HLOS.
    
    @note: The AP descriptor structure is filled in by 
    the debug agent running on the HLOS processor but is
    read by a debug stub running on a subsystem.
    
    @note This has to be SIZEOF_PD_ENTRY bytes long.
*/
struct process_descriptor
{
    ss_process_descriptor ss_desc;
    uint32_t pad1[NUM_CACHE_LINE_WORDS];
    ap_process_descriptor ap_desc;
    uint32_t pad2[NUM_CACHE_LINE_WORDS];
} __attribute__ ((__packed__));
typedef struct process_descriptor process_descriptor;
/* validation check to ensure size should be 128b on all platforms */
STATIC_ASSERT(CHECK_SIZE(sizeof(process_descriptor), 128), static_size_check_failed_line_num_)

typedef enum
{
    SINGLE_IMAGE    = 1,
    MULTI_IMAGE     = 2,
    INVALID_IMAGE   = 3    
} procssor_image_t;

typedef struct proc_pd_config
{
    processor_id        proc_id;
    uint32_t            proc_pd_count;     /**< This represents the number of descriptors a subsystem can create. */
    uint32_t            proc_pd_offset;
    procssor_image_t    image_type;
} proc_pd_config;

const proc_pd_config* debugger_get_subsystem_config(void);
uint32_t debugger_get_num_subsystems(void);

/**
    API to compare two sequence numbers to determine which is older.
    Sequence numbers are expected to start at 0 and wrap around at 
    UNIT32_MAX.
    
    @param [in] curr_seq_num    Sequence number to compare
    @param [in] new_seq_num     Sequence number to compare against to make
                                the determination which is older.
    
    @return
        0 if new_seq_num is not newer than curr_seq_num
        1 if new_seq_num is newer than curr_seq_num
*/
static inline uint32_t IS_NEWER_SEQ_NUM(uint32_t curr_seq_num, uint32_t new_seq_num)
{
    uint32_t ret_val = 0;

    if (new_seq_num > curr_seq_num)
    {
        /* curr seq is older so return newer */
        ret_val = 1;
    }
    else
    {
        /** 
            @note: new seq is equal to or less than curr seq 
            ((c - n) > 2^31) helps in wrap around detection
        */
        if ((curr_seq_num - new_seq_num) > (0xFFFFFFFF>>1))
        {
            ret_val = 1;
        }
    }

    return ret_val;
}

#endif //__DEBUGGER_STD_H__
