#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <qurt.h>

static qurt_anysignal_t my_signal;
extern int calculator(int x, int y, char op);
static char dbgr_msg[64];
extern int allow_err_fatal;

void my_main(void *param)
{
    int a =5, b = 3, x, y, z;

    qurt_anysignal_init(&my_signal);
    while(1)
    {
        x = calculator(a, b, '+');
        y = calculator(a, b, '-');
        z = calculator(a, b, '*');
        snprintf(dbgr_msg, sizeof(dbgr_msg), "(%d+%d=%d) (%d-%d=%d) (%d*%d=%d)", a, b, x, a, b, y, a, b, z);
        printf("%s\n", dbgr_msg);
        //if (allow_err_fatal)
        //{
        //    ASSERT(!"Deliberate Failure Message Inserted");
        //}
        qurt_anysignal_wait(&my_signal, 1);
        qurt_anysignal_clear(&my_signal, 1);
    }
    qurt_anysignal_destroy(&my_signal);
}

void debug_me_continue(void)
{
    qurt_thread_attr_t attr;
    qurt_thread_t thread_id;
    void *stack_ptr;    
    qurt_thread_attr_init(&attr);
    qurt_thread_attr_set_name(&attr, "DEBUG_ME");
    qurt_thread_attr_set_priority(&attr, (qurt_thread_get_priority(qurt_thread_get_id()) + 1));
    stack_ptr = malloc(0x1000);
    assert(NULL != stack_ptr);
    qurt_thread_attr_set_stack_addr(&attr, stack_ptr);
    qurt_thread_attr_set_stack_size(&attr, 0x1000);
    assert(0 == qurt_thread_create(&thread_id, &attr, my_main, 0));
}

