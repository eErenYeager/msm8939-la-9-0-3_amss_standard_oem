#include "debugger_types.h"
#include "debugger_std.h"

/* IDs of subsystems under debug */
#define IMAGE_ADSP              (PROCESSOR_ID_START + PROCESSOR_HEXAGON_ID  + 0x100)
#define IMAGE_MODEM             (PROCESSOR_ID_START + PROCESSOR_HEXAGON_ID  + 0x200)
#define IMAGE_WCNSS             (PROCESSOR_ID_START + PROCESSOR_ARM_ID      + 0x300)
#define IMAGE_RPM               (PROCESSOR_ID_START + PROCESSOR_ARM_ID      + 0x400)
#define IMAGE_VENUS             (PROCESSOR_ID_START + PROCESSOR_ARM_ID      + 0x500)
#define IMAGE_VPU               (PROCESSOR_ID_START + PROCESSOR_ARM_ID      + 0x600)
#define IMAGE_GSS               (PROCESSOR_ID_START + PROCESSOR_ARM_ID      + 0x700)

#define NUM_ADSP_DESCRIPTORS    32
#define NUM_MODEM_DESCRIPTORS   1
#define NUM_WCNSS_DESCRIPTORS   1
#define NUM_RPM_DESCRIPTORS     1
#define NUM_VENUS_DESCRIPTORS   1
#define NUM_VPU_DESCRIPTORS     1
#define NUM_GSS_DESCRIPTORS     1

#define NUM_PD_TABLE_PAGES                  2
#define MAX_PROCESS_DESCRIPTOR_TABLE_SIZE   (MIN_PAGE_SIZE * NUM_PD_TABLE_PAGES)

/** 
    @brief This represents the maximim number of descriptors an SOC can have. 
*/
#define MAX_NUM_PROCESS_DESCRIPTOR_ENTRIES  (MAX_PROCESS_DESCRIPTOR_TABLE_SIZE / SIZEOF_PD_ENTRY)

/** 
    @brief This represents the total number of descriptors an SOC can have. 
    This can be less than or equal to MAX_NUM_PROCESS_DESCRIPTOR_ENTRIES.
*/
#define TOTAL_DESCRIPTOR_COUNT (NUM_ADSP_DESCRIPTORS + NUM_MODEM_DESCRIPTORS + \
                                NUM_WCNSS_DESCRIPTORS + NUM_RPM_DESCRIPTORS +  \
                                NUM_VENUS_DESCRIPTORS + NUM_VPU_DESCRIPTORS +  \
                                NUM_GSS_DESCRIPTORS)

#if (TOTAL_DESCRIPTOR_COUNT > MAX_NUM_PROCESS_DESCRIPTOR_ENTRIES)
    #error Error in configuring the PD table layout
#endif

/**
    @brief This represents the start offset in the process descriptor table where 
    a particular subsystem's process descriptors can be found. This is derived
    from the number of descriptors assigned to a subsystem.
*/
typedef enum
{
    RPM_DESC_OFFSET         = 0x0,
    VENUS_DESC_OFFSET       = RPM_DESC_OFFSET   + (NUM_RPM_DESCRIPTORS    * sizeof(process_descriptor)),
    VPU_DESC_OFFSET         = VENUS_DESC_OFFSET + (NUM_VENUS_DESCRIPTORS  * sizeof(process_descriptor)),
    GSS_DESC_OFFSET         = VPU_DESC_OFFSET   + (NUM_VPU_DESCRIPTORS    * sizeof(process_descriptor)),
    WCNSS_DESC_OFFSET       = GSS_DESC_OFFSET   + (NUM_GSS_DESCRIPTORS    * sizeof(process_descriptor)),
    MODEM_DESC_OFFSET       = WCNSS_DESC_OFFSET + (NUM_WCNSS_DESCRIPTORS  * sizeof(process_descriptor)),
    ADSP_DESC_OFFSET        = 4096
} process_descriptor_offsets;

const proc_pd_config pd_config[] = 
{
    {IMAGE_RPM,     NUM_RPM_DESCRIPTORS,    RPM_DESC_OFFSET,    SINGLE_IMAGE},
    {IMAGE_VENUS,   NUM_VENUS_DESCRIPTORS,  VENUS_DESC_OFFSET,  SINGLE_IMAGE},
    {IMAGE_VPU,     NUM_VPU_DESCRIPTORS,    VPU_DESC_OFFSET,    SINGLE_IMAGE},
    {IMAGE_GSS,     NUM_GSS_DESCRIPTORS,    GSS_DESC_OFFSET,    SINGLE_IMAGE},
    {IMAGE_WCNSS,   NUM_WCNSS_DESCRIPTORS,  WCNSS_DESC_OFFSET,  SINGLE_IMAGE},
    {IMAGE_MODEM,   NUM_MODEM_DESCRIPTORS,  MODEM_DESC_OFFSET,  SINGLE_IMAGE},
    {IMAGE_ADSP,    NUM_ADSP_DESCRIPTORS,   ADSP_DESC_OFFSET,   MULTI_IMAGE},
    /* note this must always be the last line */
    {0, 0, 0, INVALID_IMAGE}
};

const proc_pd_config* debugger_get_subsystem_config(void)
{
    return pd_config;
}

uint32_t debugger_get_num_subsystems(void)
{
    uint32_t index = 0;
    while(0 != pd_config[index].proc_id)
    {
        index++;
    }
    return index;
}
