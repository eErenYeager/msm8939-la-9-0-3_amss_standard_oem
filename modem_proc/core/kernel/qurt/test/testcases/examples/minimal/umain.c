#include <stdio.h>
#include <stdlib.h>

#include "qurt_qdi.h"
#include "qurt_mutex.h"
#include "qurt_thread.h"

#define STKSIZE 1024

static volatile int stop;
static volatile int A;

void tmain1(void *p)
{
   int n;

   n = 0;
   while (!stop) {
      A = n;
      ++n;
   }

   printf("n is %d\n", n);

   qurt_thread_stop();
}

void tmain3(void *p)
{
   int tmp;
   int i;
   int n;

   n = 0;
   while (!stop) {
      asm volatile("%0 = memw_locked(%2)\n"
                   "memw_locked(%2, p0) = %3\n"
                   "%0 = p0\n"
                   : "=&r" (tmp), "+m" (A)
                   : "r" (&A), "r" (i));
      ++n;
   }

   printf("n is %d\n", n);

   qurt_thread_stop();
}

void tmain2(void *p)
{
   int succ;
   int fail;
   int tmp;
   int i;

   succ = fail = 0;

   for (i=0; i<1000; i++) {
      asm volatile("%0 = memw_locked(%2)\n"
                   "r6 = r6\n"
                   "memw_locked(%2, p0) = %3\n"
                   "%0 = p0\n"
                   : "=&r" (tmp), "+m" (A)
                   : "r" (&A), "r" (i));
      succ += (tmp == 255);
      fail += (tmp == 0);
   }

   printf("Success %d, fail %d\n", succ, fail);

   stop = 1;

   qurt_thread_stop();
}

int main(void)
{
   qurt_thread_attr_t t_attr;
   qurt_thread_t tid2;
   qurt_thread_t tid;
   int st;
   

   printf("In user main\n");

   /* Start a thread which does continuous memory writes to A as
      long as stop is not set */

   stop = 0;

   qurt_thread_attr_init(&t_attr);
   qurt_thread_attr_set_stack_size(&t_attr, STKSIZE);
   qurt_thread_attr_set_priority(&t_attr, 10);
   qurt_thread_attr_set_stack_addr(&t_attr, malloc(STKSIZE));
   qurt_thread_create(&tid, &t_attr, tmain1, 0);

   qurt_thread_attr_init(&t_attr);
   qurt_thread_attr_set_stack_size(&t_attr, STKSIZE);
   qurt_thread_attr_set_priority(&t_attr, 10);
   qurt_thread_attr_set_stack_addr(&t_attr, malloc(STKSIZE));
   qurt_thread_create(&tid2, &t_attr, tmain2, 0);

   qurt_thread_join(tid2, &st);
   qurt_thread_join(tid, &st);

   stop = 0;

   qurt_thread_attr_init(&t_attr);
   qurt_thread_attr_set_stack_size(&t_attr, STKSIZE);
   qurt_thread_attr_set_priority(&t_attr, 10);
   qurt_thread_attr_set_stack_addr(&t_attr, malloc(STKSIZE));
   qurt_thread_create(&tid, &t_attr, tmain3, 0);

   qurt_thread_attr_init(&t_attr);
   qurt_thread_attr_set_stack_size(&t_attr, STKSIZE);
   qurt_thread_attr_set_priority(&t_attr, 10);
   qurt_thread_attr_set_stack_addr(&t_attr, malloc(STKSIZE));
   qurt_thread_create(&tid2, &t_attr, tmain2, 0);

   qurt_thread_join(tid2, &st);
   qurt_thread_join(tid, &st);

   printf("Done\n");
   qurt_thread_stop();

   return 0;
}
