/*****************************************************************/
/* FILE: thread.c                                                */
/*                                                               */
/* SERVICES: Tests for verifying qthread API                     */
/*                                                               */
/* DESCRIPTION:                                                  */
/*****************************************************************/

/************************ COPYRIGHT NOTICE ***********************/
/* Copyright (c) 2006 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*****************************************************************/

#include<assert.h>
#include <qurt.h>
#include <stdlib.h>
#include <stdio.h>

/* consts */
#define THREAD_NUM        5
#define STACK_SIZE        1024
#define LARGE_NUM         60
#define MAX               900
#define MAX1              400
#define THREAD_STACKSIZE  8192
#define THREAD_STACKSIZE1 4096
#define THREAD_PRIORITY   99
#define NOP __asm__ __volatile("nop")

#define MAX_THREADS    160

typedef struct thread_info_type {
        qurt_thread_t id;
        char name[10];
        unsigned prio;
        void* stack_addr;
} thread_info_t;
thread_info_t thread_info[THREAD_NUM];

typedef struct _thread_context_info {
   unsigned int thread_id;
   unsigned char prio;
   char thread_name[16];
   unsigned int proc_id;
   char proc_name[16];
   unsigned int stack_base;
   unsigned int stack_size;
   unsigned long long int thread_pcycles[6];
} thread_context_info;

typedef struct _profile_info {
   unsigned char version;
   unsigned char processor;
   unsigned char hw_thread_num;
   unsigned char sw_thread_num_in_packet;
   unsigned int  sw_thread_num_in_system; 
   unsigned long long int total_pcycles;
   unsigned int  core_freq;
   unsigned long long int total_sclk_ticks;
   thread_context_info thread_block[32];
} profile_info;




void TestMain(void *attr)
{
  int i;
  volatile int temp;

   while (1) {
      for (i = 0; i < 10000; i++)
         temp++;
      qurt_timer_sleep (1000);
   }
}

void ThreadProfiler(void *attr)
{

}


/**
* Main function
*/
int main(void)
{
   int i, status, thread_ret;
   qurt_thread_attr_t thread_attr;
   qurt_thread_t id;
   //qurt_thread_id_node *tid_list_head;
   //qurt_thread_id_node *head = tid_list_head;
   
   qurt_thread_attr_init(&thread_attr);
   while(1);
   for (i=0; i<THREAD_NUM; i++) {
      thread_info[i].stack_addr=malloc(STACK_SIZE);
      assert(thread_info[i].stack_addr!=NULL);
      snprintf(thread_info[i].name, 10, "thread%d", i);
      qurt_thread_attr_set_name(&thread_attr,thread_info[i].name);
      qurt_thread_attr_set_stack_addr(&thread_attr, thread_info[i].stack_addr);
      qurt_thread_attr_set_stack_size(&thread_attr, STACK_SIZE);
      thread_info[i].prio = 100+i;
      qurt_thread_attr_set_priority(&thread_attr,thread_info[i].prio);
      status=qurt_thread_create(&thread_info[i].id, &thread_attr, TestMain, &thread_info[i]);
      printf("thread%d created id = 0x%x\n", i, thread_info[i].id);
      assert(QURT_EOK==status);
   }
   
   qurt_thread_attr_set_name(&thread_attr, "ThreadProfiler");
   qurt_thread_attr_set_stack_addr(&thread_attr, malloc(STACK_SIZE));
   qurt_thread_attr_set_stack_size(&thread_attr, STACK_SIZE);
   qurt_thread_attr_set_priority(&thread_attr,99);
   status=qurt_thread_create(&id, &thread_attr, ThreadProfiler, NULL);
   printf("Profiler thread%d created id = 0x%x\n", i, id);
   assert(QURT_EOK==status);
   
   printf("\nThread ID list: ");
   //while(head!=NULL)
   //{
   //   printf("%d ", head->handle);
   //   head = head->next;
   //}
   
   printf ("\n\n\n");
   
   exit (0);
   while(1);
   return 0;
}

