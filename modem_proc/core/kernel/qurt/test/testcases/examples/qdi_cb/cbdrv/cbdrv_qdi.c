#include <stdlib.h>
#include "qurt_rmutex.h"
#include "qurt_qdi.h"
#include "qurt_qdi_driver.h"
#include "cbdrv_qdi.h"
#include "cbdrv.h"

struct cbq_elem {
   struct cbq_elem *next;
   struct cbdrv_cbinfo *cbinfo;
   int value;
};

struct cbdrv_cbinfo {
   struct cbdrv_object *object;
   void (*pfn)(void *, int);
   void *arg;
};

struct cbdrv_object {
   qurt_qdi_obj_t qdiobj;
   struct cbq_elem *cbqueue;
   qurt_mutex_t mtx;
   int hGroupLocal;
   int hGroupRemote;
   int hSigLocal;
   int hSigRemote;
};

struct cbdrv_opener {
   qurt_qdi_obj_t qdiobj;
};

static void cbdrv_qdi_do_callback(void *ptr, int value)
{
   struct cbdrv_cbinfo *pCbInfo = ptr;
   struct cbq_elem *cbq;

   cbq = malloc(sizeof(*cbq));
   if (cbq) {
      cbq->cbinfo = pCbInfo;
      cbq->value = value;
      qurt_rmutex_lock(&pCbInfo->object->mtx);
      cbq->next = pCbInfo->object->cbqueue;
      pCbInfo->object->cbqueue = cbq;
      qurt_rmutex_unlock(&pCbInfo->object->mtx);
      qurt_qdi_signal_set(pCbInfo->object->hSigLocal);
   }
}

static int cbdrv_qdi_get_callback(int client_handle,
                                  struct cbdrv_object *pCbDrv,
                                  void *buf)
{
   struct cbdrv_qdi_cbinfo info;
   struct cbq_elem *cbq;

   for (;;) {
      qurt_rmutex_lock(&pCbDrv->mtx);
      cbq = pCbDrv->cbqueue;
      if (cbq)
         pCbDrv->cbqueue = cbq->next;
      qurt_rmutex_unlock(&pCbDrv->mtx);
      if (cbq)
         break;
      qurt_qdi_signal_wait(pCbDrv->hSigLocal);
      qurt_qdi_signal_clear(pCbDrv->hSigLocal);
   }

   info.pfn = cbq->cbinfo->pfn;
   info.arg = cbq->cbinfo->arg;
   info.value = cbq->value;

   free(cbq);

   return qurt_qdi_copy_to_user(client_handle, buf, &info, sizeof(info));
}

void cbdrv_qdi_release(qurt_qdi_obj_t *ptr)
{
   struct cbdrv_object *pCbDrv;

   pCbDrv = (struct cbdrv_object *)ptr;
   /* We don't implement full cleanup for this proof of concept */
   if (0)
      cbdrv_deinit();
}

int cbdrv_qdi_invocation(int client_handle, qurt_qdi_obj_t *ptr, int method,
                         qurt_qdi_arg_t a1, qurt_qdi_arg_t a2,
                         qurt_qdi_arg_t a3, qurt_qdi_arg_t a4,
                         qurt_qdi_arg_t a5, qurt_qdi_arg_t a6,
                         qurt_qdi_arg_t a7, qurt_qdi_arg_t a8,
                         qurt_qdi_arg_t a9)
{
   struct cbdrv_object *pCbDrv = ptr;
   struct cbdrv_cbinfo *pCbInfo;
   int ret;

   switch (method) {
   case QDI_OPEN:
      pCbDrv = malloc(sizeof(*pCbDrv));
      if (pCbDrv == NULL)
         return -1;
      pCbDrv->qdiobj.invoke = cbdrv_qdi_invocation;
      pCbDrv->qdiobj.refcnt = QDI_REFCNT_INIT;
      pCbDrv->qdiobj.release = cbdrv_qdi_release;
      pCbDrv->cbqueue = NULL;
      qurt_rmutex_init(&pCbDrv->mtx);
      if (qurt_qdi_signal_group_create(client_handle,
                                       &pCbDrv->hGroupLocal,
                                       &pCbDrv->hGroupRemote) < 0) {
         free(pCbDrv);
         return -1;
      }
      if (qurt_qdi_signal_create(pCbDrv->hGroupLocal,
                                 &pCbDrv->hSigLocal,
                                 &pCbDrv->hSigRemote) < 0) {
         qurt_qdi_release_handle(client_handle, pCbDrv->hGroupRemote);
         qurt_qdi_release_handle(QDI_HANDLE_LOCAL_CLIENT, pCbDrv->hGroupLocal);
         free(pCbDrv);
         return -1;
      }
      ret = qurt_qdi_new_handle_from_obj_t(client_handle, &pCbDrv->qdiobj);
      if (ret < 0) {
         qurt_qdi_release_handle(client_handle, pCbDrv->hSigRemote);
         qurt_qdi_release_handle(client_handle, pCbDrv->hGroupRemote);
         qurt_qdi_release_handle(QDI_HANDLE_LOCAL_CLIENT, pCbDrv->hSigLocal);
         qurt_qdi_release_handle(QDI_HANDLE_LOCAL_CLIENT, pCbDrv->hGroupLocal);
         free(pCbDrv);
      }
      return ret;
   case CBDRV_REGISTER:
      pCbInfo = malloc(sizeof(*pCbInfo));
      if (pCbInfo == NULL)
         return -1;
      pCbInfo->object = pCbDrv;
      pCbInfo->pfn = (void (*)(void *, int))(a1.ptr);
      pCbInfo->arg = a2.ptr;
      cbdrv_register_callback(cbdrv_qdi_do_callback, pCbInfo);
      return 0;
   case CBDRV_NOTIFY:
      cbdrv_notify_callbacks(a1.num);
      return 0;
   case CBDRV_GET_CB:
      return cbdrv_qdi_get_callback(client_handle, pCbDrv, a1.ptr);
   default:
      return qurt_qdi_method_default(client_handle, ptr, method,
                                     a1, a2, a3, a4, a5, a6, a7, a8, a9);
   }
}

const struct cbdrv_opener opener = {
   {
      cbdrv_qdi_invocation,
      QDI_REFCNT_PERM,
      cbdrv_qdi_release
   }
};

void cbdrv_qdi_init(void)
{
   cbdrv_init();
   qurt_qdi_register_devname("/dev/cbdrv",
                        (void *)&opener);
}
