ENTRY(start)
SECTIONS
{
  . = 0xFE000000;
  __kernel_mem_start = .;
  .kv :
    {
      KEEP (*qurtkernel.o(.start))
      KEEP (*qurtkernel.o(.text))
      KEEP (*qurtkernel.o(.rodata .rodata.*))
      KEEP (*cust_config.o(.rodata .rodata.*))
      KEEP (*qurtkernel.o(.data .data.*))
      KEEP (*cust_config.o(.data))
    }
  . = ALIGN(64);
  __kernel_bss_start = .;
  .kbss :
    {
      KEEP (*qurtkernel.o(.bss .bss.*))
      KEEP (*cust_config.o(.bss))
      KEEP (*cust_config.o(COMMON))
    }
  . = ALIGN(64);
  __kernel_bss_end = .;
  .start : { KEEP (*(.start)) } =0x00C0007F
  .init : { KEEP (*(.init)) } =0x00C0007F
  .text : { *(.text .text.*) } =0x00C0007F
  .fini : { KEEP (*(.fini)) } =0x00C0007F
  .rodata : { *(.rodata .rodata.*) }
  .eh_frame : { KEEP (*(.eh_frame)) }
  .ctors : { KEEP (*init.o(.ctors))
             KEEP (*fini.o(.ctors)) }
  .dtors : { KEEP (*init.o(.dtors))
             KEEP (*fini.o(.dtors)) }
  . = ALIGN(4K);
  __kernel_mem_end = .;
  .data : { *(.data .data.*) }
  . = ALIGN(64);
  __bss_start = .;
  .bss : { *(.bss .bss.*) *(COMMON) }
  . = ALIGN(64);
  _end = .;
  end = .;
  _SDA_BASE_ = .;
  __sbss_start = .;
  __sbss_end = .;
  .comment 0 : { *(.comment) }
  .debug_aranges 0 : { *(.debug_aranges) }
  .debug_pubnames 0 : { *(.debug_pubnames) }
  .debug_info 0 : { *(.debug_info) }
  .debug_abbrev 0 : { *(.debug_abbrev) }
  .debug_line 0 : { *(.debug_line) }
  .debug_frame 0 : { *(.debug_frame) }
  .debug_str 0 : { *(.debug_str) }
  .debug_loc 0 : { *(.debug_loc) }
  .debug_ranges 0 : { *(.debug_ranges) }
}


