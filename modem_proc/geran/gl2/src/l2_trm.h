#ifndef _L2_TRM_H_
#define _L2_TRM_H_
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         L 2   TRM CLIENT SIMULATOR

GENERAL DESCRIPTION
   This module simulates the activity of a TRM client

Copyright (c) 2001-2014 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gl2/src/l2_trm.h#1 $
$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who      what, where, why
--------   ---      ----------------------------------------------------------
01/01/14   SJW      Initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "geran_variation.h"
#include "customer.h"
#ifdef FEATURE_GERAN_TRM_EMULATOR
#error code not present
#endif /* FEATURE_GERAN_TRM_EMULATOR */

extern int l2_trm_emulator_enabled(void);

#endif /* _L2_TRM_H_ */
