/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         L 2   TRM CLIENT SIMULATOR

GENERAL DESCRIPTION
   This module simulates the activity of a TRM client using a script file in EFS

Copyright (c) 2001-2014 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gl2/src/l2_trm.c#1 $
$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who      what, where, why
--------   ---      ----------------------------------------------------------
01/01/14   SJW      Initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "geran_variation.h"
#include "customer.h"
#ifdef FEATURE_GERAN_TRM_EMULATOR
#error code not present
#endif /* FEATURE_GERAN_TRM_EMULATOR */

/*===========================================================================
FUNCTION l2_trm_emulator_enabled

DESCRIPTION Dummy function that persists even after stripping this file

RETURN VALUE
  0

SIDE EFFECTS
  None

===========================================================================*/
int l2_trm_emulator_enabled(void)
{
  return(0);
}
