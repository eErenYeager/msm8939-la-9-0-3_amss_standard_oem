/*==============================================================================

                        R R   U A R F C N

GENERAL DESCRIPTION
  Provides general utility functions for manipulating WCDMA UARFCNs.

EXTERNALIZED FUNCTIONS
  rr_uarfcn_get_frequency

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2005-2013 Qualcomm Technologies, Inc.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_uarfcn.c#1 $

when      who     what, where, why
--------  ---     --------------------------------------------------------------
20070928  gdj     Initial version

==============================================================================*/

/*==============================================================================

                      INCLUDE FILES FOR MODULE

==============================================================================*/
#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_GSM
#ifdef FEATURE_WCDMA
#include "rr_seg_load.h"
#include "rr_uarfcn.h"
#include "msg.h"

/*==============================================================================

                LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/

/*==============================================================================
  Private Constants and Macros
==============================================================================*/

#if defined(FEATURE_UMTS_ACQ_CHNL_INFO_SHARING) || defined(PERLUTF)

/* 2100 MHz band (Band I) frequency ranges for UARFCN */
#define RR_UARFCN_DL_I_IMT_2000_MIN     10562
#define RR_UARFCN_DL_I_IMT_2000_MAX     10838
#define RR_UARFCN_DL_I_IMT_2000_OFFSET  ((double) 0)

/* 1900 Band (Band II) frequency ranges for UARFCN */
#define RR_UARFCN_DL_II_PCS_1900_MIN         9662
#define RR_UARFCN_DL_II_PCS_1900_MAX         9938
#define RR_UARFCN_DL_II_PCS_1900_OFFSET      ((double) 0)
#define RR_UARFCN_DL_II_PCS_1900_ADDL_OFFSET ((double) 1850.1)

/* 1800 MHz Band (III) frequency ranges for UARFCN */
#define RR_UARFCN_DL_III_1800_MIN       1162
#define RR_UARFCN_DL_III_1800_MAX       1513
#define RR_UARFCN_DL_III_1800_OFFSET    ((double) 1575)

/* 1700_2100 AWS Band (IV) frequency ranges for UARFCN */
#define RR_UARFCN_DL_IV_1700_MIN         1537
#define RR_UARFCN_DL_IV_1700_MAX         1738
#define RR_UARFCN_DL_IV_1700_OFFSET      ((double) 1805)
#define RR_UARFCN_DL_IV_1700_ADDL_OFFSET ((double) 1735.1)

/* 850 MHz Band (Band V) Band frequency ranges for UARFCN */
#define RR_UARFCN_DL_V_850_MIN          4357
#define RR_UARFCN_DL_V_850_MAX          4458
#define RR_UARFCN_DL_V_850_OFFSET       ((double) 0)
#define RR_UARFCN_DL_V_850_ADDL_OFFSET  ((double) 670.1)

/* 800 MHz Band (VI) frequency ranges for UARFCN */
#define RR_UARFCN_DL_VI_800_MIN         4387
#define RR_UARFCN_DL_VI_800_MAX         4413
#define RR_UARFCN_DL_VI_800_OFFSET      ((double) 0)
#define RR_UARFCN_DL_VI_800_ADDL_OFFSET ((double) 670.1)

/* 900 MHz Band (VIII) frequency ranges for UARFCN */
#define RR_UARFCN_DL_VIII_900_MIN      2937
#define RR_UARFCN_DL_VIII_900_MAX      3088
#define RR_UARFCN_DL_VIII_900_OFFSET   ((double) 340)

/* 1700 MHz Band (IX) frequency ranges for UARFCN */
#define RR_UARFCN_DL_IX_1700_MIN       9237
#define RR_UARFCN_DL_IX_1700_MAX       9387
#define RR_UARFCN_DL_IX_1700_OFFSET    ((double) 0)

#endif /* FEATURE_UMTS_ACQ_CHNL_INFO_SHARING */

/*==============================================================================
  Private Typedefs
==============================================================================*/

/*==============================================================================
  Private Variables
==============================================================================*/

#ifdef FEATURE_UMTS_ACQ_CHNL_INFO_SHARING

/* NOTE: These arrays must be zero-terminated. */

/* Valid additional channels for 1900 MHz band */
static const rrc_rr_freq_type rr_uarfcn_dl_band_ii_addl_channels[] = {
  412, 437, 462, 487, 512, 537, 562, 587, 612, 637, 662, 687, 0
};

/* Valid additional channels for 1700_2100 AWS MHz band (band IV) */
static const rrc_rr_freq_type rr_uarfcn_dl_band_iv_addl_channels[] = {
  1887, 1912, 1937, 1962, 1987, 2012, 2037, 2062, 2087, 0
};

/* Valid additional channels for 850 MHz band */
static const rrc_rr_freq_type rr_uarfcn_dl_band_v_addl_channels[] = {
  1007, 1012, 1032, 1037, 1062, 1087, 0
};

/* Valid additional channels for 800 MHz band */
static const rrc_rr_freq_type rr_uarfcn_dl_band_vi_addl_channels[] = {
  1037, 1062, 0
};

#else

#ifdef PERLUTF
#error code not present
#endif /* PERLUTF */

#endif /* FEATURE_UMTS_ACQ_CHNL_INFO_SHARING */

/*==============================================================================
  Private Function Prototypes
==============================================================================*/


/*==============================================================================

                    FUNCTION DECLARATIONS FOR MODULE

==============================================================================*/

#ifdef FEATURE_UMTS_ACQ_CHNL_INFO_SHARING

double rr_uarfcn_get_frequency(rrc_chnl_info_type uarfcn)
{
  double frequency = 0;

  if (uarfcn.band == SYS_BAND_MASK_WCDMA_I_IMT_2000)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_I_IMT_2000_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_I_IMT_2000_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_I_IMT_2000_OFFSET;
    }
    else
    {
      MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_II_PCS_1900)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_II_PCS_1900_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_II_PCS_1900_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_II_PCS_1900_OFFSET;
    }
    else
    {
      uint16 i = 0;

      while (rr_uarfcn_dl_band_ii_addl_channels[i] != 0)
      {
        if (uarfcn.freq == rr_uarfcn_dl_band_ii_addl_channels[i])
        {
          frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_II_PCS_1900_ADDL_OFFSET;
          break;
        }
        else
        {
          i++;
        }
      }

      if (frequency == 0)
      {
        MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
      }
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_III_1700)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_III_1800_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_III_1800_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_III_1800_OFFSET;
    }
    else
    {
      MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_IV_1700)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_IV_1700_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_IV_1700_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_IV_1700_OFFSET;
    }
    else
    {
      uint16 i = 0;

      while (rr_uarfcn_dl_band_iv_addl_channels[i] != 0)
      {
        if (uarfcn.freq == rr_uarfcn_dl_band_iv_addl_channels[i])
        {
          frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_IV_1700_ADDL_OFFSET;
          break;
        }
        else
        {
          i++;
        }
      }

      if (frequency == 0)
      {
        MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
      }
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_V_850)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_V_850_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_V_850_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_V_850_OFFSET;
    }
    else
    {
      uint16 i = 0;

      while (rr_uarfcn_dl_band_v_addl_channels[i] != 0)
      {
        if (uarfcn.freq == rr_uarfcn_dl_band_v_addl_channels[i])
        {
          frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_V_850_ADDL_OFFSET;
          break;
        }
        else
        {
          i++;
        }
      }

      if (frequency == 0)
      {
        MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
      }
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_VI_800)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_VI_800_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_VI_800_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_VI_800_OFFSET;
    }
    else
    {
      uint16 i = 0;

      while (rr_uarfcn_dl_band_vi_addl_channels[i] != 0)
      {
        if (uarfcn.freq == rr_uarfcn_dl_band_vi_addl_channels[i])
        {
          frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_VI_800_ADDL_OFFSET;
          break;
        }
        else
        {
          i++;
        }
      }

      if (frequency == 0)
      {
        MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
      }
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_VIII_900)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_VIII_900_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_VIII_900_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_VIII_900_OFFSET;
    }
    else
    {
      MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
    }
  }
  else if (uarfcn.band == SYS_BAND_MASK_WCDMA_IX_1700)
  {
    if (uarfcn.freq >= RR_UARFCN_DL_IX_1700_MIN && 
        uarfcn.freq <= RR_UARFCN_DL_IX_1700_MAX)
    {
      frequency = (uarfcn.freq / (double) 5) + RR_UARFCN_DL_IX_1700_OFFSET;
    }
    else
    {
      MSG_GERAN_ERROR_2("UAC: freq %d out of range for band %d", uarfcn.freq, uarfcn.band);
    }
  }
  else
  {
    MSG_GERAN_ERROR_1("UAC: freq band %d unknown", uarfcn.band);
  }

  return frequency;
}

#endif /* FEATURE_UMTS_ACQ_CHNL_INFO_SHARING) || PERLUTF */

#ifdef PERLUTF
#error code not present
#endif /* #ifdef PERLUTF */

#endif /* FEATURE_WCDMA */
#endif /* FEATURE_GSM */
