/*============================================================================
  FILE:         rr_sglte_band_coex_if.c

  OVERVIEW:

  DEPENDENCIES: None

                Copyright (c) 2009 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_sglte_band_coex_if.c#1 $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_SGLTE

#include "comdef.h"
#include "rr_sglte_band_coex_if.h"
#include "rr_rr.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
* Externalized Function Definitions
* -------------------------------------------------------------------------*/

const char * rr_sglte_band_coex_imsg_name(rr_sglte_band_coex_imsg_e imsg)
{
  switch (imsg)
  {
    case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND:      return "RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND";
    case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND:    return "RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND";
    case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:         return "RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND";
    case RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND:    return "RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND";
    case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND: return "RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND";
    case RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND:           return "RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND";
    case RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND:        return "RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND";
    case RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND:         return "RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND";
    case RR_SGLTE_BAND_COEX_IMSG_DONOTGOTOSTATE2_TIMER_EXPIRED_IND:  return "RR_SGLTE_BAND_COEX_IMSG_DONOTGOTOSTATE2_TIMER_EXPIRED_IND";
    default:
      MSG_GERAN_ERROR_1("Unknown message: %d", imsg);
      return "?";
  }
}

const char * rr_sglte_band_coex_omsg_name(rr_sglte_band_coex_omsg_e omsg)
{
  switch (omsg)
  {
    case RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND: return "RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND";
    default:
      MSG_GERAN_ERROR_1("Unknown message: %d", omsg);
      return "?";
  }
}

void rr_sglte_band_coex_send_imsg(
   rr_sglte_band_coex_imsg_e imsg,
   const gas_id_t gas_id)
{
  rr_message_header_t header;

  rr_rr_send(&header,
             (uint8) imsg,
             sizeof(header),
             RR_SGLTE_BAND_COEX_IMSG, gas_id);
}

void rr_sglte_band_coex_send_imsg_ba_list_update_ind(
  ARFCN_T                        scell_arfcn,
  inter_task_BCCH_allocation_T * ba_list,
  const gas_id_t gas_id
)
{
  rr_sglte_band_coex_imsg_ba_list_update_ind_t ba_list_update_ind;

  memset(&ba_list_update_ind, 0, sizeof(ba_list_update_ind));

  ba_list_update_ind.scell_arfcn = scell_arfcn;

  if (ba_list != NULL)
  {
    ba_list_update_ind.ba_list = *ba_list;
  }

  rr_rr_send(&ba_list_update_ind.header,
             RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND,
             sizeof(ba_list_update_ind),
             RR_SGLTE_BAND_COEX_IMSG,
             gas_id);
}

void rr_sglte_band_coex_send_imsg_channel_spec_update_ind(
  ARFCN_T          scell_arfcn,
  channel_spec_T * channel_spec,
  const gas_id_t gas_id
)
{
  rr_sglte_band_coex_imsg_channel_spec_update_ind_t channel_spec_update_ind;

  memset(&channel_spec_update_ind, 0, sizeof(channel_spec_update_ind));

  channel_spec_update_ind.scell_arfcn = scell_arfcn;

  if (channel_spec != NULL)
  {
    channel_spec_update_ind.channel_spec = *channel_spec;
  }

  rr_rr_send(&channel_spec_update_ind.header,
             RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND,
             sizeof(channel_spec_update_ind),
             RR_SGLTE_BAND_COEX_IMSG,
             gas_id);
}

void rr_sglte_band_coex_send_imsg_service_domain_changed_ind(
  sys_srv_domain_e_type service_domain,
  const gas_id_t gas_id
)
{
  rr_sglte_band_coex_imsg_service_domain_changed_ind_t service_domain_changed_ind;

  memset(&service_domain_changed_ind, 0, sizeof(service_domain_changed_ind));

  service_domain_changed_ind.service_domain = service_domain;

  rr_rr_send(&service_domain_changed_ind.header,
             RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND,
             sizeof(service_domain_changed_ind),
             RR_SGLTE_BAND_COEX_IMSG,
             gas_id);
}

void rr_sglte_band_coex_send_omsg(
   rr_sglte_band_coex_omsg_e omsg,
   const gas_id_t gas_id)
{
  rr_message_header_t header;

  rr_rr_send(&header,
             (uint8) omsg,
             sizeof(header),
             RR_SGLTE_BAND_COEX_OMSG,
             gas_id);
}

#endif /* FEATURE_SGLTE */

/* EOF */
