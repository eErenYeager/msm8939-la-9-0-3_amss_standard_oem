#ifndef RR_UARFCN_H
#define RR_UARFCN_H

/*==============================================================================

                   R R   U A R F C N   ---   H E A D E R   F I L E

GENERAL DESCRIPTION
  This module provides utility functions related to handling of UMTS ARFCNs
  (UARFCNs).

EXTERNALIZED FUNCTIONS
  rr_uarfcn_get_frequency
    Returns the frequency (in MHz) of the UARFCN provided.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  No specific initialisation requirements.

Copyright (c) 2007-2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_uarfcn.h#1 $

when     who  what, where, why
-------- ---  ---------------------------------------------------------
20070718 gdj  Initial version
==============================================================================*/

/*==============================================================================

                      INCLUDE FILES FOR MODULE

==============================================================================*/

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_GSM
#ifdef FEATURE_WCDMA

#include "rrc_rr_types.h"

/*==============================================================================

                PUBLIC DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/

/*==============================================================================
  Constants and Macros
==============================================================================*/


/*==============================================================================
  Typedefs
==============================================================================*/
#ifdef PERLUTF
#error code not present
#endif /* PERLUTF */

/*==============================================================================
  Variables
==============================================================================*/


/*==============================================================================

                    PUBLIC FUNCTION DECLARATIONS FOR MODULE

==============================================================================*/

#ifdef FEATURE_UMTS_ACQ_CHNL_INFO_SHARING

/**
 * Calculates the absolute frequency of the UMTS channel (in MHz).
 *
 * @param uarfcn Structure containing the channel number and band of the UMTS
 *               channel.
 */
double rr_uarfcn_get_frequency(rrc_chnl_info_type uarfcn);

#endif /* FEATURE_UMTS_ACQ_CHNL_INFO_SHARING */

#ifdef PERLUTF
#error code not present
#endif /* #ifdef PERLUTF */

#endif /* FEATURE_WCDMA */
#endif /* FEATURE_GSM */

#endif /* RR_UARFCN_H */
