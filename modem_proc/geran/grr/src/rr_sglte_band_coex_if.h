#ifndef RR_SGLTE_BAND_COEX_IF_H
#define RR_SGLTE_BAND_COEX_IF_H

/*============================================================================
  @file rr_sglte_band_coex.h

  Signalling interface for the SGLTE Band Coexistence manager state machine.

                Copyright (c) 2002, 2008 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_sglte_band_coex_if.h#1 $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_SGLTE

#include "comdef.h"
#include "rr_message_header.h"
#include "rr_defs.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/* Input messages. */
typedef enum
{
  RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND,
  RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND,
  RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND,
  RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND,
  RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND,
  RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND,
  RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND,
  RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND,
  RR_SGLTE_BAND_COEX_IMSG_DONOTGOTOSTATE2_TIMER_EXPIRED_IND,
  RR_SGLTE_BAND_COEX_IMSG_COUNT
} rr_sglte_band_coex_imsg_e;

typedef struct
{
  rr_message_header_t          header;
  ARFCN_T                      scell_arfcn;
  inter_task_BCCH_allocation_T ba_list;
} rr_sglte_band_coex_imsg_ba_list_update_ind_t;

typedef struct
{
  rr_message_header_t header;
  ARFCN_T             scell_arfcn;
  channel_spec_T      channel_spec;
} rr_sglte_band_coex_imsg_channel_spec_update_ind_t;

typedef struct
{
  rr_message_header_t   header;
  sys_srv_domain_e_type service_domain;
} rr_sglte_band_coex_imsg_service_domain_changed_ind_t;

typedef union
{
  rr_message_header_t                                  header;
  rr_sglte_band_coex_imsg_ba_list_update_ind_t         ba_list_update_ind;
  rr_sglte_band_coex_imsg_channel_spec_update_ind_t    channel_spec_update_ind;
  rr_sglte_band_coex_imsg_service_domain_changed_ind_t service_domain_changed_ind;
} rr_sglte_band_coex_imsg_u;

/* Output messages. */
typedef enum
{
  RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND,
  RR_SGLTE_BAND_COEX_OMSG_COUNT
} rr_sglte_band_coex_omsg_e;

typedef union
{
  rr_message_header_t                         header;
} rr_sglte_band_coex_omsg_u;

/* Union of all messages. */
typedef union
{
  rr_sglte_band_coex_imsg_u imsg;
  rr_sglte_band_coex_omsg_u omsg;
} rr_sglte_band_coex_msg_u;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

extern const char * rr_sglte_band_coex_imsg_name(
  rr_sglte_band_coex_imsg_e imsg
);

extern const char * rr_sglte_band_coex_omsg_name(
  rr_sglte_band_coex_omsg_e omsg
);

extern void rr_sglte_band_coex_send_imsg(
  rr_sglte_band_coex_imsg_e imsg,
  gas_id_t gas_id
);

extern void rr_sglte_band_coex_send_imsg_ba_list_update_ind(
  ARFCN_T                        scell_arfcn,
  inter_task_BCCH_allocation_T * ba_list,
  gas_id_t gas_id
);

extern void rr_sglte_band_coex_send_imsg_channel_spec_update_ind(
  ARFCN_T          scell_arfcn,
  channel_spec_T * channel_spec,
  gas_id_t gas_id
);

extern void rr_sglte_band_coex_send_imsg_service_domain_changed_ind(
  sys_srv_domain_e_type service_domain,
  gas_id_t gas_id
);

extern void rr_sglte_band_coex_send_omsg(
  rr_sglte_band_coex_omsg_e omsg,
  gas_id_t gas_id
);

#endif /* FEATURE_SGLTE */

#endif /* RR_SGLTE_BAND_COEX_IF_H */
