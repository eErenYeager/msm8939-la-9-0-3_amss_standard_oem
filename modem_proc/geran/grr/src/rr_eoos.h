/*==============================================================================

                             R R   EOOS

GENERAL DESCRIPTION

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2007 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_eoos.h#1 $

==============================================================================*/


/*==============================================================================

                      INCLUDE FILES FOR MODULE

==============================================================================*/

#include "customer.h"
#include "comdef.h"

#include "geran_variation.h"
#ifdef FEATURE_EOOS
#error code not present
#endif /* FEATURE_EOOS */
