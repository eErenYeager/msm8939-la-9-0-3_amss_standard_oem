/*! \file rr_nv.c 
 
  This module contains access functions for NV data.
 
                Copyright (c) 2011-2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_nv.c#1 $ */
/* $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "rr_seg_load.h"
#include "geran_variation.h"
#include "rr_nv.h"
#include "rr_channel_mode.h"
#include "rr_gprs_defs.h"
#include "fs_public.h"
#include "rr_msc.h"
#include "geran_multi_sim.h"
#include "sys.h"
#include "geran_nv.h"
#include "gprs_mem.h"
#include "rr_sim.h"
#ifdef FEATURE_GSM_TDS
#include "ghdi_exp.h"
#include "ghdi_exp_v.h"
#endif /*FEATURE_GSM_TDS*/
#ifdef FEATURE_SGLTE
#include "rr_sglte_band_coex_if.h"
#include "rr_sglte_band_coex.h"
#include "rr_control.h"
#endif /*FEATURE_SGLTE*/

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define RR_SCELL_RESYNC_TIME_DEFAULT (255) /* 255 seconds */
#define RR_PSCAN_STALENESS_DEFAULT   (4)   /*   4 seconds */

// Maximum number of PLMNs in EFS white list
#define MAX_NUM_OF_PLMNS 20
// Number of bytes storage per PLMN id
#define BYTES_PER_PLMN 3

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

typedef struct
{
  uint8    split_page_cycle_enc;
  uint16   split_page_cycle;
  uint8    non_drx_timer_enc;
  uint8    non_drx_timer;
  uint8    multislot_class;
  uint8    anite_gcf_flag;
#ifdef FEATURE_GSM_EGPRS
  boolean  egprs_enabled;
  uint8    egprs_multislot_class;
  boolean  egprs_8psk_ul_enabled;
  uint8    egprs_8psk_power_capability;
#endif /* FEATURE_GSM_EGPRS */
#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1
  boolean  geran_feature_pack_1_enabled;
  uint8    interrat_nacc_support;
#endif /* FEATURE_GSM_GERAN_FEATURE_PACK_1 */
#ifdef FEATURE_GSM_DTM
  boolean  dtm_enabled;
  uint8    dtm_egprs_multislot_subclass;
#ifdef FEATURE_GSM_EDTM
  boolean  edtm_enabled;
#endif /* FEATURE_GSM_EDTM */
#endif /* FEATURE_GSM_DTM */
  uint8    gea_supported;
#ifdef FEATURE_GSM_GPRS_MSC33
  uint8    hmsc_gprs_coded_ms_class;
#ifdef FEATURE_GSM_EGPRS
  uint8    hmsc_egprs_coded_ms_class;
#endif /* FEATURE_GSM_EGPRS */
#endif /* FEATURE_GSM_GPRS_MSC33 */
} rr_nv_data_t;

typedef struct
{
  uint8 num_of_plmns;
  sys_plmn_id_s_type *plmn_list_ptr;
} rr_plmn_white_list_t;

typedef struct
{
#ifdef FEATURE_VAMOS
  boolean vamos_enabled;
#endif /* FEATURE_VAMOS */
  boolean fast_si_acq_during_csfb_enabled;
  boolean fast_si_acq_when_cs_only_enabled;
  rr_plmn_white_list_t negative_c1_white_list;
  uint8 pscan_results_reuse_time_secs;
  boolean plmn_search_freq_pruning_enabled;
#ifdef FEATURE_SGLTE
  boolean sglte_b39_coex_enabled;
  uint32  sglte_b39_coex_hysteresis_timeout;
#endif /* FEATURE_SGLTE */
#ifdef FEATURE_GSM_TDS
  boolean tds_lab_config_enabled;
#endif /*FEATURE_GSM_TDS*/
#ifdef FEATURE_SGLTE_G2X_CONN_MODE_RESEL
  boolean sglte_g2x_conn_mode_resel_enabled;
#endif
  int8    add_crh_db;
  int8    add_crh_scell_thresh_db;
  boolean sys_info_cache_enabled;
#ifdef FEATURE_GSM_BAND_AVOIDANCE
  boolean band_avoid_enabled;
  uint8 band_avoid_ncell_threshold;
  uint8 band_avoid_max_meas_rpts;
#endif /*FEATURE_GSM_BAND_AVOIDANCE*/
  rr_plmn_white_list_t read_pch_during_transfer_whitelist;
  boolean iterative_si_acq_enabled;
#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
  uint8   scell_resync_time_secs;
#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */
} rr_efs_nv_data_t;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

// NV data (Note: per sub)
static rr_nv_data_t rr_nv_data[NUM_GERAN_DATA_SPACES];

// EFS-NV data (Note: shared across subs)
static rr_efs_nv_data_t rr_efs_nv_data;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*!
 * \brief Returns the data-space index appropriate for the given AS-ID.
 *
 *  Note: For the NV data, this index is stored according to AS-ID, not GAS-ID. This is to allow the
 *  AS-ID to GAS-ID mapping to be changed without the NV data needing to be re-sent
 *
 * \param as_id
 *
 * \return uint8
 */
static uint8 rr_nv_get_data_space_index(const sys_modem_as_id_e_type as_id)
{
#ifdef FEATURE_TRIPLE_SIM
  if (as_id == SYS_MODEM_AS_ID_3)
  {
    return GERAN_DATA_SPACE_INDEX_3;
  }
#endif // FEATURE_TRIPLE_SIM

#ifdef FEATURE_DUAL_SIM
  if (as_id == SYS_MODEM_AS_ID_2)
  {
    return GERAN_DATA_SPACE_INDEX_2;
  }
#endif // FEATURE_DUAL_SIM

  return GERAN_DATA_SPACE_INDEX_1;
}

/*!
 * \brief Returns a pointer to the module data.
 *
 * \param as_id
 *
 * \return rr_nv_data_t*
 */
static rr_nv_data_t *rr_nv_get_data_ptr(const sys_modem_as_id_e_type as_id)
{
  uint8 as_index = rr_nv_get_data_space_index(as_id);

  return &rr_nv_data[as_index];
}

/*!
 * \brief Takes the encoded split page cycle code and returns the decoded value
 * 
 * \param split_page_cycle_enc (in)
 * 
 * \return uint16 
 */
static uint16 decode_split_page_cycle(uint8 split_page_cycle_enc)
{
/* Translation of SPLIT PAGE CYCLE (encoded) to SPLIT PAGE CYCLE value for values 65 -> 98 */
  static const uint16 spc_code_to_val[] =
  {
    71, 72, 74, 75, 77, 79, 80, 83, 86, 88, /* indices 0..9   */
    90, 92, 96,101,103,107,112,116,118,128, /* indices 10..19 */
   141,144,150,160,171,176,192,214,224,235, /* indices 20..29 */
   256, 288, 320, 352                       /* indices 30..33 */
  };

  uint16 split_page_cycle;

/*
 SPLIT PAGE CYCLE values - see GSM 04.08 10.5.5.6

 SPLIT PAGE CYCLE CODE   SPLIT PAGE CYCLE value
 0                       704
 1 -> 64                 1 -> 64
 65 -> 98                as constant array above
 all others              1
*/

  if ( split_page_cycle_enc == 0 )
  {
    split_page_cycle = 704;
  }
  else
  if ( ( split_page_cycle_enc >= 1 ) && ( split_page_cycle_enc <= 64 ) )
  {
    split_page_cycle = split_page_cycle_enc;
  }
  else
  if ( ( split_page_cycle_enc >= 65 ) && ( split_page_cycle_enc <= 98 ) )
  {
    split_page_cycle = spc_code_to_val[split_page_cycle_enc - 65];
  }
  else
  {
    split_page_cycle = 1;
  }

  return split_page_cycle;

} /* end decode_split_page_cycle() */

#ifdef FEATURE_GSM_TDS
/*!
 * \brief Reads /nv/item_files/modem/tdscdma/rrc/special_test_setting_enabled.
 * 
 * \return boolean - TRUE is feature enabled, FALSE otherwise
 */
static boolean rr_read_efs_nv_tds_lab_test_config_enabled(void)
{
 uint8 nv_69731 = 0;
 int8 efs_ret_val;
 boolean lab_test_config_enabled = FALSE;

  /* For special settings for test from RRC NV 69731*/
  efs_ret_val = efs_get("/nv/item_files/modem/tdscdma/rrc/special_test_setting_enabled", 
                        &nv_69731, 
                        sizeof(nv_69731));

  if(efs_ret_val == -1)
  {
     MSG_HIGH("NV 69731 efs failed to read",0,0,0);
     /* go with the default configuration */
     lab_test_config_enabled = FALSE;
  }
  else
  {
    MSG_HIGH("Read EFS NV Item 69731: %d", nv_69731, 0, 0);

    if (nv_69731 == 1)
    {
      MSG_HIGH("tds_lab_test_enabled = TRUE",0,0,0);
      lab_test_config_enabled = TRUE;
    }
    else
    {
      MSG_HIGH("tds_lab_test_enabled = FALSE",0,0,0);
      lab_test_config_enabled = FALSE;   
    }
  }

  return lab_test_config_enabled;
}
#endif // FEATURE_GSM_TDS

/*!
 * \brief Reads EFS-NV items and populates the local NV data.
 */
static void read_efs_nv_items(void)
{
  struct fs_stat file_status;

#ifdef FEATURE_VAMOS
  // Read /nv/item_files/modem/geran/vamos_support

  uint8 vamos_support;

  if (efs_get("/nv/item_files/modem/geran/vamos_support",
              &vamos_support,
              sizeof(vamos_support)) < 0)
  {
    MSG_GERAN_HIGH_0("VAMOS-I enabled (default)");
    vamos_support = 1;
  }

  // vamos_support: 0=OFF 1=VAMOS-I 2=VAMOS-II
  if (vamos_support != 0)
  {
    rr_efs_nv_data.vamos_enabled = TRUE;
  }
  else
  {
    rr_efs_nv_data.vamos_enabled = FALSE;
  }
#endif /* FEATURE_VAMOS */

  // Read /nv/item_files/modem/geran/grr/fast_si_acq_during_csfb_control

  if (efs_get("/nv/item_files/modem/geran/grr/fast_si_acq_during_csfb_control", 
              &rr_efs_nv_data.fast_si_acq_during_csfb_enabled, 
              sizeof(rr_efs_nv_data.fast_si_acq_during_csfb_enabled)) < 0)
  {
    MSG_GERAN_HIGH_0("Fast SI acq during CSFB disabled (default)");
    rr_efs_nv_data.fast_si_acq_during_csfb_enabled = TRUE;
  }
  else
  {
    if (rr_efs_nv_data.fast_si_acq_during_csfb_enabled != TRUE &&
        rr_efs_nv_data.fast_si_acq_during_csfb_enabled != FALSE)
    {
      MSG_GERAN_HIGH_1("Fast SI acq during CSFB enabled (default, EFS file incorrect value %d)",
                         rr_efs_nv_data.fast_si_acq_during_csfb_enabled);
      rr_efs_nv_data.fast_si_acq_during_csfb_enabled = TRUE;
    }

    if (rr_efs_nv_data.fast_si_acq_during_csfb_enabled)
    {
      MSG_GERAN_HIGH_0("Fast SI acq during CSFB enabled");
    }
    else
    {
      MSG_GERAN_HIGH_0("Fast SI acq during CSFB disabled");
    }
  }

  // Read /nv/item_files/modem/geran/grr/fast_si_acq_when_cs_only_enabled

  {
    int8 efs_val;

    if (efs_get("/nv/item_files/modem/geran/grr/fast_si_acq_when_cs_only_enabled",
                &efs_val,
                sizeof(efs_val)) < 0)
    {
      MSG_GERAN_HIGH_0("EFS-NV fast_si_acq_when_cs_only_enabled not present");
      efs_val = -1;
    }

    // Sanity-check the result is 0 or 1
    if ((efs_val != 0) && (efs_val != 1))
    {
      efs_val = 0;  // Default is DISABLED for this EFS-NV item
    }

    // Update the module store
    if (efs_val == 1)
    {
      MSG_GERAN_HIGH_0("Fast SI Acq when CS-only is ENABLED");
      rr_efs_nv_data.fast_si_acq_when_cs_only_enabled = TRUE;
    }
    else
    {
      MSG_GERAN_HIGH_0("Fast SI Acq when CS-only is DISABLED");
      rr_efs_nv_data.fast_si_acq_when_cs_only_enabled = FALSE;
    }
  }

  rr_efs_nv_data.negative_c1_white_list.num_of_plmns = 0;
  // Read /nv/item_files/modem/geran/negative_c1_white_list
  if (efs_stat(
        "/nv/item_files/modem/geran/negative_c1_white_list",
        &file_status
      ) == 0)
  {
    // EFS file size in bytes
    fs_size_t file_size_bytes = file_status.st_size;

    // Number of PLMNs that can be represented by this file size, capped at MAX_NUM_OF_PLMNS
    int num_of_plmns = MIN((file_size_bytes / BYTES_PER_PLMN), MAX_NUM_OF_PLMNS);

    // Number of bytes to allocate to store the PLMN list
    int alloc_size_bytes = num_of_plmns * BYTES_PER_PLMN;

    rr_efs_nv_data.negative_c1_white_list.plmn_list_ptr =
      GPRS_MEM_MALLOC(alloc_size_bytes);

    if (rr_efs_nv_data.negative_c1_white_list.plmn_list_ptr != NULL)
    {
      if (efs_get(
            "/nv/item_files/modem/geran/negative_c1_white_list",
            rr_efs_nv_data.negative_c1_white_list.plmn_list_ptr,
            alloc_size_bytes
          ) == alloc_size_bytes)
      {
        rr_efs_nv_data.negative_c1_white_list.num_of_plmns = num_of_plmns;
      }
    }
    else
    {
      MSG_GERAN_ERROR_1("Failed to allocate %d bytes for negative C1 whitelist",
                        alloc_size_bytes);
    }
  }

  rr_efs_nv_data.read_pch_during_transfer_whitelist.num_of_plmns = 0;   
  // Read /nv/item_files/modem/geran/read_pch_during_transfer_whitelist
  if (efs_stat(
        "/nv/item_files/modem/geran/read_pch_during_transfer_whitelist",
        &file_status
      ) == 0)
  {
    // EFS file size in bytes
    fs_size_t file_size_bytes = file_status.st_size;

    // Number of PLMNs that can be represented by this file size, capped at MAX_NUM_OF_PLMNS
    int num_of_plmns = MIN((file_size_bytes / BYTES_PER_PLMN), MAX_NUM_OF_PLMNS);

    // Number of bytes to allocate to store the PLMN list
    int alloc_size_bytes = num_of_plmns * BYTES_PER_PLMN;

    rr_efs_nv_data.read_pch_during_transfer_whitelist.plmn_list_ptr =
      GPRS_MEM_MALLOC(alloc_size_bytes);

    if (rr_efs_nv_data.read_pch_during_transfer_whitelist.plmn_list_ptr != NULL)
    {
      if (efs_get(
            "/nv/item_files/modem/geran/read_pch_during_transfer_whitelist",
            rr_efs_nv_data.read_pch_during_transfer_whitelist.plmn_list_ptr,
            alloc_size_bytes
          ) == alloc_size_bytes)
      {
        rr_efs_nv_data.read_pch_during_transfer_whitelist.num_of_plmns = num_of_plmns;
      }
    }
    else
    {
      MSG_GERAN_ERROR_1("Failed to allocate %d bytes for read_pch_during_transfer_whitelist",
                        alloc_size_bytes);
    }
  }

  // Read /nv/item_files/modem/geran/pscan_results_reuse_time_secs

  if (efs_get("/nv/item_files/modem/geran/pscan_results_reuse_time_secs",
              &rr_efs_nv_data.pscan_results_reuse_time_secs,
              sizeof(rr_efs_nv_data.pscan_results_reuse_time_secs)) < 0)
  {
    MSG_GERAN_HIGH_1("pscan_results_reuse_time_secs not present - default=%ds",
                     RR_PSCAN_STALENESS_DEFAULT);
    rr_efs_nv_data.pscan_results_reuse_time_secs = RR_PSCAN_STALENESS_DEFAULT;
  }
  else
  {
    MSG_GERAN_HIGH_1("pscan_results_reuse_time_secs=%d", rr_efs_nv_data.pscan_results_reuse_time_secs);
  }

  // Read /nv/item_files/modem/geran/plmn_search_freq_pruning_enabled

  {
    int8 efs_val;

    if (efs_get("/nv/item_files/modem/geran/plmn_search_freq_pruning_enabled",
                &efs_val,
                sizeof(efs_val)) < 0)
    {
      MSG_GERAN_HIGH_0("EFS-NV plmn_search_freq_pruning_enabled not present");
      efs_val = -1;
    }

    // Sanity-check the result is 0 or 1
    if ((efs_val != 0) && (efs_val != 1))
    {
      efs_val = 1;  // Default is ENABLED for this EFS-NV item
    }

    // Update the module store
    if (efs_val == 1)
    {
      MSG_GERAN_HIGH_0("PLMN search freq pruning is ENABLED");
      rr_efs_nv_data.plmn_search_freq_pruning_enabled = TRUE;
    }
    else
    {
      MSG_GERAN_HIGH_0("PLMN search freq pruning is DISABLED");
      rr_efs_nv_data.plmn_search_freq_pruning_enabled = FALSE;
    }
  }

#ifdef FEATURE_SGLTE
  if (efs_get("/nv/item_files/modem/geran/sglte_b39_coex_enabled", 
              &rr_efs_nv_data.sglte_b39_coex_enabled, 
              sizeof(rr_efs_nv_data.sglte_b39_coex_enabled)) < 0)
  {
    MSG_GERAN_HIGH_0("SGLTE B39 Coexistence Disabled by (default)");
    rr_efs_nv_data.sglte_b39_coex_enabled = FALSE;
  }
  MSG_GERAN_HIGH_1("sglte_b39_coex_enabled: %d", rr_efs_nv_data.sglte_b39_coex_enabled);

  if (efs_get("/nv/item_files/modem/geran/sglte_b39_coex_hysteresis_timeout", 
              &rr_efs_nv_data.sglte_b39_coex_hysteresis_timeout, 
              sizeof(rr_efs_nv_data.sglte_b39_coex_hysteresis_timeout)) < 0)
  {
    MSG_GERAN_HIGH_0("sglte_b39_coex_hysteresis_timeout: 90000ms (default)");
    rr_efs_nv_data.sglte_b39_coex_hysteresis_timeout = 90000;
  }
  MSG_GERAN_HIGH_1("sglte_b39_coex_hysteresis_timeout: %dms",
                     rr_efs_nv_data.sglte_b39_coex_hysteresis_timeout);
#endif /* FEATURE_SGLTE */

#ifdef FEATURE_GSM_TDS
    rr_efs_nv_data.tds_lab_config_enabled = rr_read_efs_nv_tds_lab_test_config_enabled();
#endif /*FEATURE_GSM_TDS*/

#ifdef FEATURE_SGLTE_G2X_CONN_MODE_RESEL
  if (efs_get("/nv/item_files/modem/geran/sglte_g2x_conn_mode_resel_enabled",
              &rr_efs_nv_data.sglte_g2x_conn_mode_resel_enabled, 
              sizeof(rr_efs_nv_data.sglte_g2x_conn_mode_resel_enabled)) < 0)
  {
    MSG_GERAN_HIGH_0("SGLTE G2X Connected Mode Reselection Enabled by (default)");
    rr_efs_nv_data.sglte_g2x_conn_mode_resel_enabled = TRUE;
  }

  MSG_GERAN_HIGH_1(
    "sglte_g2x_conn_mode_resel_enabled: %d",
    rr_efs_nv_data.sglte_g2x_conn_mode_resel_enabled
  );
#endif

  if (efs_get("/nv/item_files/modem/geran/add_crh_db",
              &rr_efs_nv_data.add_crh_db, 
              sizeof(rr_efs_nv_data.add_crh_db)) < 0)
  {
    // If the value if not configured (file not present), the default value is 0
    rr_efs_nv_data.add_crh_db = 0;
  }

  if (rr_efs_nv_data.add_crh_db != 0)
  {
    if (efs_get("/nv/item_files/modem/geran/add_crh_scell_thresh_db",
                &rr_efs_nv_data.add_crh_scell_thresh_db, 
                sizeof(rr_efs_nv_data.add_crh_scell_thresh_db)) < 0)
    {
      // If the value if not configured (file not present), the default value is -70
      rr_efs_nv_data.add_crh_scell_thresh_db = -70;
    }
  }

  MSG_GERAN_HIGH_2("add_crh_db=%ddB add_crh_scell_thresh_db=%ddB",
                   rr_efs_nv_data.add_crh_db, rr_efs_nv_data.add_crh_scell_thresh_db);

  // Read /nv/item_files/modem/geran/grr/sys_info_cache_enabled

  {
    int8 efs_val;

    if (efs_get("/nv/item_files/modem/geran/grr/sys_info_cache_enabled",
                &efs_val,
                sizeof(efs_val)) < 0)
    {
      MSG_GERAN_HIGH_0("EFS-NV sys_info_cache_enabled not present");
      efs_val = -1;
    }

    // Sanity-check the result is 0 or 1
    if ((efs_val != 0) && (efs_val != 1))
    {
      efs_val = 1;  // Default is ENABLED for this EFS-NV item
    }

    // Update the module store
    if (efs_val == 1)
    {
      MSG_GERAN_HIGH_0("Sys info cache is ENABLED");
      rr_efs_nv_data.sys_info_cache_enabled = TRUE;
    }
    else
    {
      MSG_GERAN_HIGH_0("Sys info cache is DISABLED");
      rr_efs_nv_data.sys_info_cache_enabled = FALSE;
    }
  }

#ifdef FEATURE_GSM_BAND_AVOIDANCE
  if (efs_get("/nv/item_files/modem/geran/band_avoid_enabled", 
              &rr_efs_nv_data.band_avoid_enabled, 
              sizeof(rr_efs_nv_data.band_avoid_enabled)) < 0)
  {
    MSG_GERAN_HIGH_0("Band Avoid enabled (default)");
    rr_efs_nv_data.band_avoid_enabled = TRUE;
  }
  MSG_GERAN_HIGH_1("Band Avoid enabled: %d", rr_efs_nv_data.band_avoid_enabled);

  if (efs_get("/nv/item_files/modem/geran/band_avoid_ncell_threshold", 
              &rr_efs_nv_data.band_avoid_ncell_threshold, 
              sizeof(rr_efs_nv_data.band_avoid_ncell_threshold)) < 0)
  {   
    MSG_GERAN_HIGH_0("Band Avoid Ncell Threshold (default)"); 
    rr_efs_nv_data.band_avoid_ncell_threshold = 21; /*rxlev = -90dBm*/
  }
  MSG_GERAN_HIGH_1("Band Avoid Ncell Threshold: %d", rr_efs_nv_data.band_avoid_ncell_threshold);

  if (efs_get("/nv/item_files/modem/geran/band_avoid_max_meas_rpts", 
              &rr_efs_nv_data.band_avoid_max_meas_rpts, 
              sizeof(rr_efs_nv_data.band_avoid_max_meas_rpts)) < 0)
  {   
    MSG_GERAN_HIGH_0("Band Avoid Max Meas Rpts (default)"); 
    rr_efs_nv_data.band_avoid_max_meas_rpts = 4;
  }
  MSG_GERAN_HIGH_1("Band Avoid Max Meas Rpts: %d", rr_efs_nv_data.band_avoid_max_meas_rpts);
#endif /*FEATURE_GSM_BAND_AVOIDANCE*/

  // Read /nv/item_files/modem/geran/grr/iterative_si_acq_enabled

  {
    int8 efs_val;

    if (efs_get("/nv/item_files/modem/geran/grr/iterative_si_acq_enabled",
                &efs_val,
                sizeof(efs_val)) < 0)
    {
      MSG_GERAN_HIGH_0("EFS-NV iterative_si_acq_enabled not present");
      efs_val = -1;
    }

    // Sanity-check the result is 0 or 1
    if ((efs_val != 0) && (efs_val != 1))
    {
      efs_val = 1;  // Default is ENABLED for this EFS-NV item
    }

    // Update the module store
    if (efs_val == 1)
    {
      MSG_GERAN_HIGH_0("Iterative SI acq is ENABLED");
      rr_efs_nv_data.iterative_si_acq_enabled = TRUE;
    }
    else
    {
      MSG_GERAN_HIGH_0("Iterative SI acq is DISABLED");
      rr_efs_nv_data.iterative_si_acq_enabled = FALSE;
    }
  }

#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
  // Read /nv/item_files/modem/geran/scell_resync_time_secs
  if (efs_get("/nv/item_files/modem/geran/scell_resync_time_secs",
              &rr_efs_nv_data.scell_resync_time_secs,
              sizeof(rr_efs_nv_data.scell_resync_time_secs)) < 0)
  {
    MSG_GERAN_HIGH_1("scell_resync_time_secs not present - default=%ds",
                     RR_SCELL_RESYNC_TIME_DEFAULT);
    rr_efs_nv_data.scell_resync_time_secs = RR_SCELL_RESYNC_TIME_DEFAULT;
  }
  else
  {
    MSG_GERAN_HIGH_1("scell_resync_time_secs=%d", rr_efs_nv_data.scell_resync_time_secs);
  }
#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */

  return;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/

/*!
 * \brief Returns the value of SPLIT_PAGE_CYCLE stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint16 
 */
uint16 rr_get_nv_split_page_cycle(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->split_page_cycle;

} /* end rr_get_nv_split_page_cycle() */

/*!
 * \brief Returns the value of NON_DRX_TIMER.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
uint8 rr_get_nv_non_drx_timer(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->non_drx_timer;

} /* end rr_get_nv_non_drx_timer() */

/*!
 * \brief Returns the value of MULTISLOT_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
uint8 rr_get_nv_multislot_class(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->multislot_class;

} /* end rr_get_nv_multislot_class() */

/*!
 * \brief Returns the value of ANITE_GCF_FLAG stored in NV. 
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_get_nv_anite_gcf_flag(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->anite_gcf_flag;

} /* end rr_get_nv_anite_gcf_flag() */

#ifdef FEATURE_GSM_EGPRS

/*!
 * \brief Returns TRUE if EGPRS is enabled in NV; FALSE otherwise.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
#ifdef FEATURE_NON_DEMAND_PAGED_FUNCTION
#error code not present
#endif
boolean rr_get_nv_egprs_enabled(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->egprs_enabled;

} /* end rr_get_nv_egprs_enabled() */
#ifdef FEATURE_NON_DEMAND_PAGED_FUNCTION
#error code not present
#endif

/*!
 * \brief Returns the value of EGPRS_MULTISLOT_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8
 */
uint8 rr_get_nv_egprs_multislot_class(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->egprs_multislot_class;

} /* end rr_get_nv_egprs_multislot_class() */

/*!
 * \brief Returns TRUE if the NV parameter EGPRS_8PSK_POWER_CLASS is non-zero; FALSE if zero.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
boolean rr_get_nv_egprs_8psk_ul_enabled(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->egprs_8psk_ul_enabled;

} /* end rr_get_nv_egprs_8psk_ul_enabled() */

/*!
 * \brief Returns the egprs 8psk power capability.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
uint8 rr_get_nv_egprs_8psk_power_capability(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->egprs_8psk_power_capability;

} /* end rr_get_nv_egprs_8psk_power_capability() */

#endif /* FEATURE_GSM_EGPRS */

#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1

/*!
 * \brief Returns TRUE if GERAN FEATURE PACK 1 is enabled in NV; FALSE otherwise.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
#ifdef FEATURE_NON_DEMAND_PAGED_FUNCTION
#error code not present
#endif
boolean rr_get_nv_geran_feature_pack_1_enabled(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->geran_feature_pack_1_enabled;
} /* end rr_get_nv_geran_feature_pack_1_enabled() */
#ifdef FEATURE_NON_DEMAND_PAGED_FUNCTION
#error code not present
#endif

/*!
 * \brief Returns the current mask from NV that indicates support for inter-RAT NACC.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 - 0x01 indicates W2G, 0x02 indicates G2W, 0x03 indicates both
 */
uint8 rr_get_nv_interrat_nacc_support(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->interrat_nacc_support;
} /* end rr_get_nv_interrat_nacc_support() */

#endif /* FEATURE_GSM_GERAN_FEATURE_PACK_1 */

#ifdef FEATURE_GSM_DTM

/*!
 * \brief Indicates if DTM is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
boolean rr_get_nv_dtm_enabled(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->dtm_enabled;

} /* end rr_get_nv_dtm_enabled() */

/*!
 * \brief Returns the value of DTM_EGPRS_MULTISLOT_SUBCLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
uint8 rr_get_nv_dtm_egprs_multislot_subclass(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->dtm_egprs_multislot_subclass;

} /* end rr_get_nv_dtm_egprs_multislot_subclass() */

/*!
 * \brief Returns the value of DTM_MULTISLOT_CLASS, derived from DTM_EGPRS_MULTISLOT_SUBCLASS.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
uint8 rr_get_nv_dtm_multislot_class(const sys_modem_as_id_e_type as_id)
{
  uint8 dtm_multislot_subclass = rr_get_nv_dtm_egprs_multislot_subclass(as_id);

  return rr_msc_conv_dtm_multislot_subclass_to_class(dtm_multislot_subclass);
}

#ifdef FEATURE_GSM_EDTM
/*!
 * \brief Indicates if EDTM is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
boolean rr_get_nv_edtm_enabled(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->edtm_enabled;
}
#endif /* FEATURE_GSM_EDTM */

#endif /* FEATURE_GSM_DTM */

/*!
 * \brief Returns the value of GEA_SUPPORTED stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8
 */
uint8 rr_get_nv_gea_supported(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->gea_supported;

} /* end rr_get_nv_gea_supported() */

#ifdef FEATURE_GSM_GPRS_MSC33

/*!
 * \brief Returns the value of HMSC_GPRS_CODED_MS_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8
 */
uint8 rr_get_nv_hmsc_gprs_coded_ms_class(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->hmsc_gprs_coded_ms_class;

} /* end rr_get_nv_hmsc_gprs_coded_ms_class() */

#ifdef FEATURE_GSM_EGPRS
/*!
 * \brief Returns the value of HMSC_EGPRS_CODED_MS_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8
 */
uint8 rr_get_nv_hmsc_egprs_coded_ms_class(const sys_modem_as_id_e_type as_id)
{
  // Obtain a pointer to the module data
  rr_nv_data_t *rr_nv_data_ptr = rr_nv_get_data_ptr(as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  return rr_nv_data_ptr->hmsc_egprs_coded_ms_class;

} /* end rr_get_nv_hmsc_egprs_coded_ms_class() */
#endif /* FEATURE_GSM_EGPRS */

#endif /* FEATURE_GSM_GPRS_MCS33 */

#ifdef FEATURE_VAMOS
/*!
 * \brief Indicates if VAMOS is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE is VAMOS is enabled in NV (VAMOS-I or VAMOS-II); FALSE otherwise
 */
boolean rr_get_nv_vamos_enabled(const sys_modem_as_id_e_type as_id)
{
  (void)as_id;
  return rr_efs_nv_data.vamos_enabled;
}
#endif /* FEATURE_VAMOS */

/*!
 * \brief Indicates if Fast SI Acq during CSFB is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if Fast SI Acq during CSFB is enabled, FALSE otherwise
 */
boolean rr_get_nv_fast_si_acq_during_csfb_enabled(const sys_modem_as_id_e_type as_id)
{
  (void)as_id;
  return rr_efs_nv_data.fast_si_acq_during_csfb_enabled;
}

/*!
 * \brief Indicates if Fast SI Acq when CS-only is enabled in NV.
 *        Note: Feature is disabled for a Type Approval SIM. 
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_nv_fast_si_acq_when_cs_only_is_enabled(const sys_modem_as_id_e_type as_id)
{
  // Feature is disabled for a Type Approval SIM
  if (rr_get_nv_anite_gcf_flag(as_id))
  {
    return FALSE;
  }

  return rr_efs_nv_data.fast_si_acq_when_cs_only_enabled;
}

/*!
 * \brief Indicates if the supplied PLMN is present in the C1 relaxation white-list.
 * 
 * \return boolean - TRUE if present, FALSE otherwise
 */
static boolean rr_nv_check_plmn_in_whitelist(sys_plmn_id_s_type *plmn_id_ptr, int num_list_entries, sys_plmn_id_s_type plmn)
{
  int i;

  MSG_GERAN_HIGH_1("Num of PLMN %d ", num_list_entries);
  
  if (plmn_id_ptr != NULL)
  {
    sys_mcc_type mcc;
    sys_mnc_type mnc;
    boolean is_undefined;
    boolean includes_pcsd;

    sys_plmn_get_mcc_mnc(
      plmn,
      &is_undefined,
      &includes_pcsd,
      &mcc,
      &mnc
    );
    MSG_GERAN_HIGH_2("Current  PLMN=(%d,%d)",mcc,mnc);

    for (i = 0; ((i < num_list_entries) && (i < MAX_NUM_OF_PLMNS)); i++)
    {
      sys_plmn_get_mcc_mnc(
        *plmn_id_ptr,
        &is_undefined,
        &includes_pcsd,
        &mcc,
        &mnc
      );

      MSG_GERAN_HIGH_2("White list PLMN=(%d,%d)",mcc,mnc);

      if (sys_plmn_match(plmn, *plmn_id_ptr))
      {
        return TRUE;
      }

      // Point to the next PLMN in the list
      ++plmn_id_ptr;
    }
  }

  return FALSE;
}

boolean rr_nv_read_pch_during_transfer_is_plmn_match(sys_plmn_id_s_type plmn,const gas_id_t gas_id)
{
  if (rr_get_nv_anite_gcf_flag(RR_GAS_ID_TO_AS_ID))
  {
    MSG_GERAN_HIGH_0_G("Read PCH during xfer disabled (GCF is ON)");  
    return FALSE;
  }
 
  return(rr_nv_check_plmn_in_whitelist(rr_efs_nv_data.read_pch_during_transfer_whitelist.plmn_list_ptr,
                                       rr_efs_nv_data.read_pch_during_transfer_whitelist.num_of_plmns,
                                       plmn));
}

boolean rr_nv_c1_relaxation_is_plmn_match(sys_plmn_id_s_type plmn, const gas_id_t gas_id)
{
  if (rr_get_nv_anite_gcf_flag(RR_GAS_ID_TO_AS_ID))
  {
    MSG_GERAN_HIGH_0_G("C1 relaxation disabled (GCF is ON)");  
    return FALSE;
  }

  return(rr_nv_check_plmn_in_whitelist(rr_efs_nv_data.negative_c1_white_list.plmn_list_ptr,
                                       rr_efs_nv_data.negative_c1_white_list.num_of_plmns,
                                       plmn));
}

/*!
 * \brief Returns the value of the power-scan re-use timer (0=disabled)
 * 
 * \return rex_timer_cnt_type - timer vaue in milliseconds
 */
rex_timer_cnt_type rr_nv_get_pscan_results_reuse_time_millisecs(void)
{
  rex_timer_cnt_type timeout_secs = rr_efs_nv_data.pscan_results_reuse_time_secs;

  return(CONVERT_SECONDS_TO_MILLISECONDS(timeout_secs));
}

#ifdef FEATURE_SGLTE

/*!
 * \brief Indicates if B39 coex feature is enabled in NV.
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_get_nv_sglte_b39_coex_enabled(void)
{
  return rr_efs_nv_data.sglte_b39_coex_enabled;
}

/*!
 * \brief Returns the timeout value for RR_SGLTE_BAND_COEX_HYSTERESIS_TIMEOUT.
 * 
 * \return uint32 - timeout value (ms)
 */
uint32 rr_get_nv_sglte_b39_coex_hysteresis_timeout(void)
{
  return rr_efs_nv_data.sglte_b39_coex_hysteresis_timeout;
}

#endif /* FEATURE_SGLTE */

#ifdef FEATURE_SGLTE_G2X_CONN_MODE_RESEL
/*!
 * \brief Indicates if SGLTE Connected Mode Reselection feature is enabled in NV.
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_get_nv_sglte_g2x_conn_mode_resel_enabled(void)
{
  return rr_efs_nv_data.sglte_g2x_conn_mode_resel_enabled;
}
#endif /* FEATURE_SGLTE_G2X_CONN_MODE_RESEL */

#ifdef FEATURE_GSM_TDS
/*!
 * \brief Indicates if TDS lab-test config is enabled in NV.
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_is_efs_nv_tds_lab_test_config_enabled(void)
{
  return rr_efs_nv_data.tds_lab_config_enabled;
}
#endif /*FEATURE_GSM_TDS*/

/*!
 * \brief Returns the additional C2 cell reselection hysteresis values.
 * 
 * \param add_crh_db_ptr (out)
 * \param add_crh_scell_thresh_db_ptr (out)
 * 
 * \return boolean - TRUE if configured, FALSE otherwise
 */
boolean rr_get_nv_add_crh_enabled(int8 *add_crh_db_ptr, int8 *add_crh_scell_thresh_db_ptr)
{
  if (rr_efs_nv_data.add_crh_db != 0)
  {
    *add_crh_db_ptr = rr_efs_nv_data.add_crh_db;
    *add_crh_scell_thresh_db_ptr = rr_efs_nv_data.add_crh_scell_thresh_db;

    return TRUE;
  }

  return FALSE;
}

/*!
 * \brief Returns the value of the plmn_search_freq_pruning_enabled. 
 *        Note: Feature is disabled for a Type Approval SIM. 
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return TRUE if enabled, FALSE otherwise
 */
boolean rr_nv_get_plmn_search_freq_pruning_enabled(const sys_modem_as_id_e_type as_id)
{
  // Feature is disabled for a Type Approval SIM
  if (rr_get_nv_anite_gcf_flag(as_id))
  {
    return FALSE;
  }

  return rr_efs_nv_data.plmn_search_freq_pruning_enabled;
}

/*!
 * \brief Indicates if the sys info cache functionality is enabled.
 *        Note: Feature is disabled for a Type Approval SIM. 
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_nv_sys_info_cache_is_enabled(const sys_modem_as_id_e_type as_id)
{
  // Feature is disabled for a Type Approval SIM
  if (rr_get_nv_anite_gcf_flag(as_id))
  {
    return FALSE;
  }

  return rr_efs_nv_data.sys_info_cache_enabled;
}

/*!
 * \brief Indicates if the iterative updating during SI acq feature is enabled.
 *        Note: Feature is not disabled for a Type Approval SIM.
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
boolean rr_nv_iterative_si_acq_is_enabled(void)
{
  // Note: Feature is not disabled for a Type Approval SIM

  return rr_efs_nv_data.iterative_si_acq_enabled;
}

/*!
 * \brief Called when the NV data is received from NAS.
 *  
 * The NV data is copied into local storage. The Public Store is updated later when PS access is enabled. 
 * Note: the NV data is stored according to AS-ID in this message, not GAS-ID
 * 
 * \param msg_ptr (in) - pointer to the RR_GMM_GPRS_NV_PARAMS_IND message
 */
void rr_nv_process_nv_params_ind(rr_gmm_gprs_nv_params_ind_t *msg_ptr)
{
  rr_nv_data_t *rr_nv_data_ptr;

  // Sanity-check the input
  RR_NULL_CHECK_RETURN_VOID(msg_ptr);

  // Obtain a pointer to the module data
  rr_nv_data_ptr = rr_nv_get_data_ptr(msg_ptr->as_id);
  RR_NULL_CHECK_FATAL(rr_nv_data_ptr);

  rr_nv_data_ptr->multislot_class              = msg_ptr->nv_multislot_class;
  rr_nv_data_ptr->non_drx_timer_enc            = msg_ptr->nv_non_drx_timer;
  rr_nv_data_ptr->split_page_cycle_enc         = msg_ptr->nv_split_paging_cycle;
  rr_nv_data_ptr->anite_gcf_flag               = msg_ptr->nv_anite_gcf_flag;
#ifdef FEATURE_GSM_EGPRS
  rr_nv_data_ptr->egprs_enabled                = msg_ptr->nv_edge_feature_support;
  rr_nv_data_ptr->egprs_multislot_class        = msg_ptr->nv_edge_multislot_class;
  rr_nv_data_ptr->egprs_8psk_ul_enabled        = msg_ptr->nv_edge_8psk_power_class != 0;
  rr_nv_data_ptr->egprs_8psk_power_capability  = msg_ptr->nv_edge_8psk_power_capability;
#endif /* FEATURE_GSM_EGPRS */
#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1
  rr_nv_data_ptr->geran_feature_pack_1_enabled = msg_ptr->nv_edge_nw_ass_cell_change;
  rr_nv_data_ptr->interrat_nacc_support        = msg_ptr->nv_interrat_nacc_support;
#endif /* FEATURE_GSM_GERAN_FEATURE_PACK_1 */
#ifdef FEATURE_GSM_DTM
  rr_nv_data_ptr->dtm_enabled                  = msg_ptr->nv_dtm_feature_support;
  rr_nv_data_ptr->dtm_egprs_multislot_subclass = msg_ptr->nv_dtm_multislot_subclass;
#ifdef FEATURE_GSM_EDTM
  rr_nv_data_ptr->edtm_enabled                 = msg_ptr->nv_edtm_feature_support;
#endif /* FEATURE_GSM_EDTM */
#endif /* FEATURE_GSM_DTM */
  rr_nv_data_ptr->gea_supported                = msg_ptr->nv_gea_supported;
#ifdef FEATURE_GSM_GPRS_MSC33
  rr_nv_data_ptr->hmsc_gprs_coded_ms_class     = msg_ptr->nv_hmsc_gprs_coded_ms_class;
#ifdef FEATURE_GSM_EGPRS
  rr_nv_data_ptr->hmsc_egprs_coded_ms_class    = msg_ptr->nv_hmsc_egprs_coded_ms_class;
#endif /* FEATURE_GSM_EGPRS */
#endif /* FEATURE_GSM_GPRS_MSC33 */

#if defined(NV_DISABLE_VOICE_SUPPORT_MASK) || defined(NV_DISABLE_CSDATA_SUPPORT_MASK)
  {
    // Assume all call types are allowed
    boolean voice_calls_supported = TRUE;
    boolean csdata_calls_supported = TRUE;

    /* NAS will provide the mask in the RR_GMM_GPRS_NV_PARAMS_IND primitive */
#ifdef NV_DISABLE_VOICE_SUPPORT_MASK
    if ((msg_ptr->nv_disable_call_type & NV_DISABLE_VOICE_SUPPORT_MASK) != 0)
    {
      MSG_GERAN_HIGH_1("ds%d:Voice calls are disabled by NV",(int)msg_ptr->as_id+1);
      voice_calls_supported = FALSE;
    }
#endif /* NV_DISABLE_VOICE_SUPPORT_MASK */

#ifdef NV_DISABLE_CSDATA_SUPPORT_MASK
    if ((msg_ptr->nv_disable_call_type & NV_DISABLE_CSDATA_SUPPORT_MASK) != 0)
    {
      MSG_GERAN_HIGH_1("ds%d:CS data calls are disabled by NV",(int)msg_ptr->as_id+1);
      csdata_calls_supported = FALSE;
    }
#endif /* NV_DISABLE_CSDATA_SUPPORT_MASK */

    rr_channel_mode_set_supported(
      voice_calls_supported,    // voice_calls_supported
      csdata_calls_supported,   // csdata_calls_supported
      msg_ptr->as_id            // as_id
    );
  }
#endif /* NV_DISABLE_VOICE_SUPPORT_MASK | NV_DISABLE_CSDATA_SUPPORT_MASK */


  MSG_GERAN_HIGH_0("NV params from MM");
  MSG_GERAN_HIGH_3("multislot:%d non_drx:%d, spc:%d",
                   (int)rr_nv_data_ptr->multislot_class,
                   (int)rr_nv_data_ptr->non_drx_timer_enc,
                   (int)rr_nv_data_ptr->split_page_cycle_enc );

#ifdef FEATURE_GSM_EGPRS
  MSG_GERAN_HIGH_3("egprs:%d, egprs_multislot:%d, 8psk_ul:%d",
                   (int)rr_nv_data_ptr->egprs_enabled,
                   (int)rr_nv_data_ptr->egprs_multislot_class,
                   (int)rr_nv_data_ptr->egprs_8psk_ul_enabled );
#endif /* FEATURE_GSM_EGPRS */

#ifdef FEATURE_GSM_DTM
  {
    int edtm_support;

#ifdef FEATURE_GSM_EDTM
    edtm_support = (int)rr_nv_data_ptr->edtm_enabled;
#else
    edtm_support = 0xFF;
#endif /*  FEATURE_GSM_EDTM */

    MSG_GERAN_HIGH_3("DTM:%1d MSC:%d (EDTM:%02X)",
                     (int)rr_nv_data_ptr->dtm_enabled,
                     (int)rr_nv_data_ptr->dtm_egprs_multislot_subclass,
                     edtm_support);
  }
#endif /* FEATURE_GSM_DTM */

#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1
  MSG_GERAN_HIGH_2("geran_feature_pack_1:%d, interrat_nacc 0x%02X",
                   (int)rr_nv_data_ptr->geran_feature_pack_1_enabled,
                   (int)rr_nv_data_ptr->interrat_nacc_support);
#endif /* FEATURE_GSM_GERAN_FEATURE_PACK_1 */

  /* Calculate value for split_page_cycle based on encoded value */
  rr_nv_data_ptr->split_page_cycle = decode_split_page_cycle(rr_nv_data_ptr->split_page_cycle_enc);

  /* Calculate value for non_drx_timer based on encoded value */
  rr_nv_data_ptr->non_drx_timer = rr_gprs_decode_drx_timer(rr_nv_data_ptr->non_drx_timer_enc);

  if (GERAN_DATA_SPACE_INDEX_1 == rr_nv_get_data_space_index(msg_ptr->as_id))
  {
    // Use the reception of the first NV message to read the GERAN EFS-NV items
    geran_read_efs_nv_items();
    read_efs_nv_items();
  }

  /* Inform the Multi-Slot Class module that NV data is available */
  rr_msc_nv_ready(msg_ptr->as_id);

  return;
} /* end rr_nv_process_nv_params_ind() */

#ifdef FEATURE_GSM_BAND_AVOIDANCE
/**
  @brief Returns if band avoidance is enabled
*/
boolean rr_is_band_avoidance_enabled(void)
{
  return rr_efs_nv_data.band_avoid_enabled;
}

/**
  @brief Returns the threshold for band avoidance ncells as 
         rxlev (0.63)
*/
uint8 rr_get_band_avoid_ncell_threshold(void)
{
  return rr_efs_nv_data.band_avoid_ncell_threshold;
}

/**
  @brief Returns the max number of meas repts that can be sent 
  when doing band avoidance. 
*/
uint8 rr_get_band_avoid_max_meas_rpts(void)
{
  return rr_efs_nv_data.band_avoid_max_meas_rpts;
}
#endif /*FEATURE_GSM_BAND_AVOIDANCE*/

#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
uint32 rr_nv_get_scell_resync_time_millisecs(void)
{
  uint32 timeout_secs = (uint32)(rr_efs_nv_data.scell_resync_time_secs);

  if (RR_SCELL_RESYNC_TIME_DEFAULT == timeout_secs)
  {
    return(0xFFFFFFFF);
  }

  return(CONVERT_SECONDS_TO_MILLISECONDS(timeout_secs));
}
#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */

/* EOF */

