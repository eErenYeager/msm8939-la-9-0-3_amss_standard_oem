#ifndef RR_PLMN_FREQ_DB_H
#define RR_PLMN_FREQ_DB_H

#include "geran_variation.h"
#ifdef FEATURE_MULTIMODE_GSM_STORED_FREQ

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                   RR PLMN frequency database header file

GENERAL DESCRIPTION
   This module contains functions for dealing with ciphers.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


Copyright (c) 2002 by Qualcomm Technologies Incorporated. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/GSM/RR/vcs/rr_plmn_freq_db.h_v   1.0   21 May 2002 15:12:30   rodgerc  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_plmn_freq_db.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/00   JAC     Started

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "rr_defs.h"        // For cell_selection_parameters, etc..

/*===========================================================================
                         DATA DECLARATIONS
===========================================================================*/



/*RAM storage*/
// max number of ranges in each frequency list
#define RR_MAX_BA_RANGES 32

// max number of PLMN entries in PLMN database
#define RR_MAX_RAM_PLMN_ENTRIES 32

// An ARFCN range in the stored frequency list
typedef struct
{
  word low_range;
  word high_range;
} RR_BA_range_T;

/*The BA ranges associated with a specific plmn*/
typedef struct
{
  // the PLMN_id of the entry
  PLMN_id_T      plmn_id;

  // number of ranges in the entry
  byte           no_of_ranges;

  // Array of ranges describing the stored frequency list
  RR_BA_range_T  RR_BA_ranges[RR_MAX_BA_RANGES];
} RR_PLMN_BA_range_T;

// stored frequency list record for a specific PLMN
typedef struct RR_freq_list_record_T
{
  // pointer to the next record in the database
  struct RR_freq_list_record_T* next;

  // contents of the record
  RR_PLMN_BA_range_T data;
} RR_freq_list_record_T;

//Contains a list of plmns
typedef struct
{
  // number of PLMNs in the database
  byte no_of_plmns;

  // pointer to first record entry
  RR_freq_list_record_T* first_entry;
} RR_RAM_PLMN_list_T;

/*===========================================================================
                      PUBLIC FUNCTION DECLARATIONS
===========================================================================*/

// writes stored_plmn_list into SIM and NV
boolean rr_save_stored_freq(const gas_id_t gas_id);

// reads BA list in SIM and NV and puts it in stored_plmn_list
boolean rr_restore_stored_freq(const gas_id_t gas_id);

// reads entire stored plmn list in NV and writes it into RAM table
boolean rr_read_freq_NV(const gas_id_t gas_id);

// writes entire stored plmn list into NV
boolean rr_write_freq_NV(const gas_id_t gas_id);

// Adds/merges a frequency list into to RAM table
// new_list is the frequency list to be added
// When merge==true the new list is merged, otherwise it overwrites
boolean rr_add_to_stored_freq_list(RR_PLMN_BA_range_T* new_list, boolean merge, const gas_id_t gas_id);

// Reduces the number of ranges to RR_MAX_BA_RANGES
void rr_reduce_ranges(RR_BA_range_T* new_list,byte* no_of_entries);

// delete plmn freq database
void rr_clear_plmn_freq_db(const gas_id_t gas_id);

#else

extern void rr_plmn_freq_db_dummy_fn(void);

#endif //FEATURE_MULTIMODE_GSM_STORED_FREQ

#endif //RR_PLMN_FREQ_DB_H
