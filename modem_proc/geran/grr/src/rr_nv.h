#ifndef RR_NV_H
#define RR_NV_H

/*! \file rr_nv.h
 
  This is the header file for rr_nv.c.
  This module contains access functions for NV data.
 
                Copyright (c) 2011-2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_nv.h#1 $ */
/* $DateTime: 2015/01/27 06:42:19 $$Author: mplp4svc $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"
#include "mm_rr.h"
#include "rr_defs.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/*!
 * \brief Returns the value of SPLIT_PAGE_CYCLE stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint16 
 */
extern uint16 rr_get_nv_split_page_cycle(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of NON_DRX_TIMER.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_non_drx_timer(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of MULTISLOT_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_multislot_class(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of ANITE_GCF_FLAG stored in NV. 
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_get_nv_anite_gcf_flag(const sys_modem_as_id_e_type as_id);

#ifdef FEATURE_GSM_EGPRS

/*!
 * \brief Indicates if EGPRS is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
extern boolean rr_get_nv_egprs_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of EGPRS_MULTISLOT_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_egprs_multislot_class(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns TRUE if the NV parameter EGPRS_8PSK_POWER_CLASS is non-zero; FALSE if zero.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
extern boolean rr_get_nv_egprs_8psk_ul_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the EGPRS 8PSK power capability.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_egprs_8psk_power_capability(const sys_modem_as_id_e_type as_id);

#endif /* FEATURE_GSM_EGPRS */

#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1

/*!
 * \brief Indicates if GERAN Feature Pack 1 is enabled in NV
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
extern boolean rr_get_nv_geran_feature_pack_1_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the current mask from NV that indicates support for inter-RAT NACC.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 - 0x01 indicates W2G, 0x02 indicates G2W, 0x03 indicates both
 */
extern uint8 rr_get_nv_interrat_nacc_support(const sys_modem_as_id_e_type as_id);

#endif /* FEATURE_GSM_GERAN_FEATURE_PACK_1 */

#ifdef FEATURE_GSM_DTM

/*!
 * \brief Indicates if DTM is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
extern boolean rr_get_nv_dtm_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of DTM_EGPRS_MULTISLOT_SUBCLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_dtm_egprs_multislot_subclass(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Returns the value of DTM_MULTISLOT_CLASS, derived from DTM_EGPRS_MULTISLOT_SUBCLASS.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_dtm_multislot_class(const sys_modem_as_id_e_type as_id);

#ifdef FEATURE_GSM_EDTM
/*!
 * \brief Indicates if EDTM is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled; FALSE otherwise
 */
extern boolean rr_get_nv_edtm_enabled(const sys_modem_as_id_e_type as_id);
#endif /* FEATURE_GSM_EDTM */

#endif /* FEATURE_GSM_DTM */

/*!
 * \brief Returns the value of GEA_SUPPORTED stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_gea_supported(const sys_modem_as_id_e_type as_id);

#ifdef FEATURE_GSM_GPRS_MSC33

/*!
 * \brief Returns the value of HMSC_GPRS_CODED_MS_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_hmsc_gprs_coded_ms_class(const sys_modem_as_id_e_type as_id);

#ifdef FEATURE_GSM_EGPRS
/*!
 * \brief Returns the value of HMSC_EGPRS_CODED_MS_CLASS stored in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return uint8 
 */
extern uint8 rr_get_nv_hmsc_egprs_coded_ms_class(const sys_modem_as_id_e_type as_id);
#endif /* FEATURE_GSM_EGPRS */

#endif /* FEATURE_GSM_GPRS_MCS33 */

#ifdef FEATURE_VAMOS
/*!
 * \brief Indicates if VAMOS is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE is VAMOS is enabled in NV (VAMOS-I or VAMOS-II); FALSE otherwise
 */
extern boolean rr_get_nv_vamos_enabled(const sys_modem_as_id_e_type as_id);
#endif /* FEATURE_VAMOS */

/*!
 * \brief Indicates if Fast SI Acq during CSFB is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return TRUE if Fast SI Acq during CSFB is enabled, FALSE otherwise.
 */
extern boolean rr_get_nv_fast_si_acq_during_csfb_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Inidicates if Fast SI Acq when CS-only is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_nv_fast_si_acq_when_cs_only_is_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Indicates if the supplied PLMN is Read PCH during transfer is enabled in NV.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_nv_read_pch_during_transfer_is_plmn_match(sys_plmn_id_s_type plmn, const gas_id_t gas_id);

/*!
 * \brief Indicates if the supplied PLMN is present in the white-list for C1 relaxation info.
 * 
 * \return boolean - TRUE if present, FALSE otherwise
 */
extern boolean rr_nv_c1_relaxation_is_plmn_match(sys_plmn_id_s_type plmn, const gas_id_t gas_id);

/*!
 * \brief Returns the value of the power-scan re-use timer (0=disabled)
 * 
 * \return rex_timer_cnt_type - timer vaue in milliseconds
 */
extern rex_timer_cnt_type rr_nv_get_pscan_results_reuse_time_millisecs(void);

/*!
 * \brief Returns the value of the plmn_search_freq_pruning_enabled.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return TRUE if enabled, FALSE otherwise
 */
extern boolean rr_nv_get_plmn_search_freq_pruning_enabled(const sys_modem_as_id_e_type as_id);

#ifdef FEATURE_SGLTE

/*!
 * \brief Indicates if SGLTE B39 Coexistence feature is enabled in NV.
 * 
 * \return boolean - TRUE if SGLTE B39 Coexistence feature is enabled, FALSE otherwise
 */
extern boolean rr_get_nv_sglte_b39_coex_enabled(void);

#ifdef FEATURE_SGLTE_G2X_CONN_MODE_RESEL
/*!
 * \brief Indicates if SGLTE Connected Mode Reselection feature is enabled in NV.
 * 
 * \return boolean - TRUE if SGLTE SGLTE Connected Mode Reselection is enabled, FALSE otherwise
 */
extern boolean rr_get_nv_sglte_g2x_conn_mode_resel_enabled(void);
#endif /* FEATURE_SGLTE_G2X_CONN_MODE_RESEL */

/*!
 * \brief Returns the timeout value for RR_SGLTE_BAND_COEX_HYSTERESIS_TIMEOUT.
 * 
 * \return uint32 - timeout value in ms
 */
extern uint32 rr_get_nv_sglte_b39_coex_hysteresis_timeout(void);

#endif /* FEATURE_SGLTE */

#ifdef FEATURE_GSM_TDS
/*!
 * \brief Indicates if TDS lab-test config is enabled in NV.
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_is_efs_nv_tds_lab_test_config_enabled(void);
#endif /*FEATURE_GSM_TDS*/

/*!
 * \brief Returns the additional C2 cell reselection hysteresis values.
 * 
 * \param add_crh_db_ptr (out)
 * \param add_crh_scell_thresh_db_ptr (out)
 * 
 * \return boolean - TRUE if configured, FALSE otherwise
 */
extern boolean rr_get_nv_add_crh_enabled(int8 *add_crh_ptr, int8 *add_crh_scell_thresh_db_ptr);

/*!
 * \brief Indicates if the sys info cache functionality is enabled.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_nv_sys_info_cache_is_enabled(const sys_modem_as_id_e_type as_id);

/*!
 * \brief Indicates if the iterative updating during SI acq feature is enabled.
 *        Note: Feature is not disabled for a Type Approval SIM.
 * 
 * \param as_id (in) - AS-ID (Note: Not GAS-ID)
 * 
 * \return boolean - TRUE if enabled, FALSE otherwise
 */
extern boolean rr_nv_iterative_si_acq_is_enabled(void);

/*!
 * \brief Called when the NV data is received from NAS (RR_GMM_GPRS_NV_PARAMS_IND).
 * 
 * \param nv_params_ind_ptr 
 */
extern void rr_nv_process_nv_params_ind(rr_gmm_gprs_nv_params_ind_t *nv_params_ind_ptr);

#ifdef FEATURE_GSM_BAND_AVOIDANCE

extern boolean rr_is_band_avoidance_enabled(void);

/**
  @brief Returns the threshold for band avoidance ncells as 
         rxlev (0.63)
*/
extern uint8 rr_get_band_avoid_ncell_threshold(void);

/**
  @brief Returns the max number of meas repts that can be sent 
  when doing band avoidance. 
*/
extern uint8 rr_get_band_avoid_max_meas_rpts(void);
#endif /*FEATURE_GSM_BAND_AVOIDANCE*/

#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
/*!
 * \brief Returns the value of the serving cell resync timer
 * 
 * \return uint32 - timer value in milliseconds
 */
extern uint32 rr_nv_get_scell_resync_time_millisecs(void);
#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */

#endif /* #ifndef RR_NV_H */

/* EOF */

