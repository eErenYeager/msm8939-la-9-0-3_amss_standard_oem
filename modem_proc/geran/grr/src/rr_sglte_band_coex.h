#ifndef RR_SGLTE_BAND_COEX_H
#define RR_SGLTE_BAND_COEX_H

/*============================================================================
  @file rr_sglte_band_coex.h


                Copyright (c) 2009-2011 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_sglte_band_coex.h#1 $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_SGLTE

#include "comdef.h"
#include "rr_events.h"
#include "rr_task.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
  @brief Check whether an ARFCN is in the LTE / TDSCDMA B39 coexistence range.

  @return TRUE if the ARFCN is in the B39 coexistence range, FALSE otherwise.
*/
extern boolean rr_sglte_band_coex_arfcn_is_in_b39_coex_range(
  ARFCN_T arfcn
);

/**
  @brief Access the current set of bands disabled due to SGLTE band co-ex.

  @return A bitmask of the GSM bands that are currently disabled due to SGLTE
          band co-ex.
*/
extern sys_band_mask_type rr_sglte_band_coex_disabled_bands(const gas_id_t gas_id);

/**
  @brief Prioritises the power scan results according to SGLTE rules.

  Prioritises power scan results in to the following order:
  1. GSM 900
  2. DCS 1800 except B39 ARFCNs
  3. DSC 1800 B39 ARFCNs
  4. Other bands if present
*/
extern void rr_sglte_band_coex_prioritise_pscan_results(
  rr_l1_pscan_results_T * pscan_results
);

/**
  @brief Perform one-time initialisation of the module.
*/
extern void rr_sglte_band_coex_task_start_init(void);

/**
  @brief Reset the module.
*/
extern void rr_sglte_band_coex_reset(const gas_id_t gas_id);

/**
  @brief This is the main state machine for this module.

  @param message An input message for this module.
*/
extern void rr_sglte_band_coex_control(
  rr_cmd_bdy_type * message,
  gas_id_t gas_id
);

/**
  @brief Indicates if B3 is disabled

  @param
*/
extern boolean rr_sglte_band_coex_b3_is_disabled(const gas_id_t gas_id);

/**
  @brief Indicates if an ARFCN can be scanned

  @param
*/
extern boolean rr_sglte_band_coex_scan_arfcn(ARFCN_T ARFCN, const gas_id_t gas_id);

#endif /* FEATURE_SGLTE */

#endif /* RR_SGLTE_BAND_COEX_H */

/* EOF */

