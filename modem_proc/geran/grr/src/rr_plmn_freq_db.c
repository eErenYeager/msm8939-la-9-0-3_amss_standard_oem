/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            RR PLMN Frequency Database Module

GENERAL DESCRIPTION
  Handles saving the frequency list into the database

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2002-2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/GSM/RR/vcs/rr_plmn_freq_db.c_v   1.0   21 May 2002 15:12:22   rodgerc  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_plmn_freq_db.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/24/02   rmc     Modified for SIM interface changes
04/01/00   JAC     Started
===========================================================================*/
#include "geran_variation.h"
#include "customer.h"
#include "rr_seg_load.h"
#include "rr_plmn_freq_db.h"
#include "stringl/stringl.h"

#ifdef FEATURE_MULTIMODE_GSM_STORED_FREQ
#include "string.h"
#include "gs.h"
#include "rr_decode_ie.h"
#include "rr_general.h"
#include "msg.h"
#include "err.h"
#include "ghdi_exp.h"
#include "ghdi_exp_v.h"
#include "rr_multi_sim.h"

/*NV storage*/
#define MAX_NV_RR_STORED_PLMN_LIST_SIZE 255*2

typedef PACKED struct PACKED_POST
{
  byte no_of_plmns;
  byte data[MAX_NV_RR_STORED_PLMN_LIST_SIZE];
} NV_RR_PLMN_DB_T;

//The data member is a packed struct which contains a list of plmns
//and their associated ba_lists, the format will be
// byte[3] plmn_id;
// byte[1] no_of_ba_ranges;
// NV_RR_PLMN_PACKED_RANGE_T[variable no of ranges] ba_range;
//The number of stored ba_ranges will be variable,
//the maximum number of ba_ranges will be the constant MAX_BA_RANGES
typedef PACKED struct PACKED_POST
{
  unsigned short low_range:10;
  unsigned short high_range:10;
} NV_RR_PLMN_PACKED_RANGE_T;

typedef struct
{
  /* RAM PLMN stored frequency list database */
  RR_RAM_PLMN_list_T rr_plmn_freq_db;
} rr_plmn_freq_db_data_t;

static rr_plmn_freq_db_data_t  rr_plmn_freq_db_data[NUM_GERAN_DATA_SPACES];
static rr_plmn_freq_db_data_t *rr_plmn_freq_db_data_ptr = rr_plmn_freq_db_data;

//used to store distances between ranges
typedef struct rr_distance_range_T
{
  int distance;
  byte index;
} rr_distance_range_T;

//maximum distance between 2 ranges before they are merged
#define rr_merge_freq_limit 1

/*===========================================================================*/
/**
 * Stores the PLMN frequency database in NV (NOTE that SIM BCCH information is
 * reported to MM and not stored by the RR directly)
 *
 * @side_effects Writes to the NV store
 *
 * @return TRUE - Successfully stored in NV
 *         FALSE - Failed to store in NV
 */
/*===========================================================================*/
boolean rr_save_stored_freq(const gas_id_t gas_id)
{
  return rr_write_freq_NV(gas_id);
} // rr_save_stored_freq

/*===========================================================================*/
/**
 * Reads stored PLMN frequency information from NV and stores in database.
 * Note that SIM information is provided by MM separately at a later time.
 *
 * @return TRUE - Information was retrieved from NV
 *         FALSE - Failed to read NV
 */
/*===========================================================================*/
boolean rr_restore_stored_freq(const gas_id_t gas_id)
{
  rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns = 0;
  rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry = NULL;

  return rr_read_freq_NV(gas_id);
} // rr_restore_stored_freq

/*===========================================================================*/
/**
 * Clears PLMN stored frequency list database and deletes all entries
 *
 * @side_effects Deletes data structures in plmn database
 */
/*===========================================================================*/
void rr_clear_plmn_freq_db(const gas_id_t gas_id)
{
  int count = 0;
  RR_freq_list_record_T* temp_plmn    = rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
  RR_freq_list_record_T* temp2_plmn   = rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;

  // traverse database and delete plmn entries
  while (temp_plmn != NULL)
  {
    temp2_plmn = temp_plmn->next;
    GS_FREE(temp_plmn);
    temp_plmn = temp2_plmn;
    count++
  }

  if(count != (int) rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns)
  {
    MSG_GERAN_ERROR_2_G("PLMN Count Mismatch(exp=%d,act=%d)",(int) rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns,count);
  }

  // reset the entries
  rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns=0;
  rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=NULL;
} // rr_clear_plmn_freq_db

/*===========================================================================*/
/**
 * Reads entire stored plmn list in NV and stores it into RAM database
 *
 * @side_effects Reads from NV and writes to the RAM plmn database
 *
 * @return TRUE - Read is successful
 *         FALSE - Read FAILED
 */
/*===========================================================================*/
boolean rr_read_freq_NV(const gas_id_t gas_id)
{
#ifdef FEATURE_GSM_PLMN_STORED_LIST_NV
  int plmn_counter;
  RR_freq_list_record_T* temp_plmn;

  //temporary storage to read in values from nv
  NV_RR_PLMN_DB_T temp_nv;
  byte* temp_nv_data=&temp_nv.data[0];

  //used to keep track of how many bytes have been copied
  int nv_space_read=0;

#ifdef PERLUTF
  #error code not present
#else
  /*TODO add support in NV*/
  return FALSE;
#endif

  if (temp_nv.no_of_plmns > RR_MAX_RAM_PLMN_ENTRIES)
  {
    MSG_GERAN_ERROR_0_G("Stored freq list NV corrupt");
    rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=NULL;
    rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns=0;
    return FALSE;
  }
  else if(temp_nv.no_of_plmns==0)
  {
    //handle case when plmn list is empty
    rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=NULL;
    temp_plmn=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
  }


  for (plmn_counter=0;plmn_counter<temp_nv.no_of_plmns;plmn_counter++)
  {
    /*copy over record*/

    int range_count; //keeps track of how many ranges were copied


    //allocate space for plmn
    if (plmn_counter==0)
    {
      rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=(RR_freq_list_record_T*)GS_ALLOC(sizeof(RR_freq_list_record_T));
      temp_plmn=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
    }
    else
    {
      //set next pointer to new plmn
      temp_plmn->next=(RR_freq_list_record_T*)GS_ALLOC(sizeof(RR_freq_list_record_T));
      //go to next plmn
      temp_plmn=temp_plmn->next;
    }

    if (temp_plmn==NULL)
    {
      MSG_GERAN_ERROR_0_G("Ran out of memory!");

      //adjust the number of plmns to the amount successfully copied
      rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns=temp_nv.no_of_plmns;
      return FALSE;
    }

    //first 3 bytes are plmn id
    memscpy(&temp_plmn->data.plmn_id[0],sizeof(temp_plmn->data.plmn_id),temp_nv_data+nv_space_read,3);
    nv_space_read += 3;

    //copy number of range
    memscpy(&temp_plmn->data.no_of_ranges,sizeof(temp_plmn->data.no_of_ranges),temp_nv_data+nv_space_read,1);
    nv_space_read += 1;

    //check if number of ranges valid
    if(temp_plmn->data.no_of_ranges>RR_MAX_BA_RANGES)
    {
      MSG_GERAN_ERROR_0_G("Stored freq list NV corrupt");

      //database corrupted delete entire db
      rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns=temp_nv.no_of_plmns;
      rr_clear_plmn_freq_db(gas_id);

      return FALSE;
    }

    //read ranges
    for (range_count=0;range_count<temp_plmn->data.no_of_ranges;range_count++)
    {
      NV_RR_PLMN_PACKED_RANGE_T nv_range;
      memscpy(&nv_range,sizeof(nv_range),temp_nv_data+nv_space_read,sizeof(NV_RR_PLMN_PACKED_RANGE_T));
      temp_plmn->data.RR_BA_ranges[range_count].low_range=nv_range.low_range;
      temp_plmn->data.RR_BA_ranges[range_count].high_range=nv_range.high_range;
      nv_space_read += sizeof(NV_RR_PLMN_PACKED_RANGE_T);
    }
  }

  //set no_of_plmns
  rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns=temp_nv.no_of_plmns;

  if (temp_plmn!=NULL)
  {
    //set next plmn to empty
    temp_plmn->next=NULL;
  }

#endif // FEATURE_GSM_PLMN_STORED_LIST_NV
  return TRUE;
}

/*===========================================================================*/
/**
 * Stores the RAM PLMN database frequency list into NV
 *
 * @side_effects Writes to NV (blocking)
 *
 * @return TRUE - Database successfully stored<BR>
 *         FALSE - Database store failed
 */
/*===========================================================================*/
boolean rr_write_freq_NV(const gas_id_t gas_id)
{
#ifdef FEATURE_GSM_PLMN_STORED_LIST_NV
  int counter;
  int plmn_counter=0; //number of plmns successfully stored

  //temporary space used to save nv database into
  NV_RR_PLMN_DB_T temp_nv;
  byte* temp_nv_data=&temp_nv.data[0];
  //amount of space used by the nv database
  int nv_space_used=0;

  RR_freq_list_record_T* temp_plmn;

  temp_plmn=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;

  ASSERT(rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns<=RR_MAX_RAM_PLMN_ENTRIES);

  //loop on each plmn in the database
  for (counter=0; counter<rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns; counter++)
  {
    if(temp_plmn==NULL)
    {
        MSG_GERAN_ERROR_0_G("Invalid plmn database");
        return FALSE;
    }

    //check if we have enough space to store plmn
    if (MAX_NV_RR_STORED_PLMN_LIST_SIZE < (nv_space_used+3+1+temp_plmn->data.no_of_ranges*sizeof(NV_RR_PLMN_PACKED_RANGE_T)))
    {
      //NV full can't store this one, but keep on going maybe the next one is smaller and we can store that
      MSG_GERAN_HIGH_0_G("Stored NV freq full");
    }
    else
    {
      int range_count;

      //have a plmn, that can fit, copy plmn_id
      memscpy(temp_nv_data+nv_space_used,sizeof(NV_RR_PLMN_PACKED_RANGE_T),&temp_plmn->data.plmn_id[0],3);
      nv_space_used += 3;

      //copy number of range
	  memscpy(temp_nv_data+nv_space_used,sizeof(NV_RR_PLMN_PACKED_RANGE_T),&temp_plmn->data.no_of_ranges,1);

      nv_space_used += 1;

      //record ba ranges
      ASSERT(temp_plmn->data.no_of_ranges<=RR_MAX_BA_RANGES);
      for (range_count=0;range_count<temp_plmn->data.no_of_ranges;range_count++)
      {
        NV_RR_PLMN_PACKED_RANGE_T nv_range;
        nv_range.low_range=temp_plmn->data.RR_BA_ranges[range_count].low_range;
        nv_range.high_range=temp_plmn->data.RR_BA_ranges[range_count].high_range;
		memscpy(temp_nv_data+nv_space_used,sizeof(NV_RR_PLMN_PACKED_RANGE_T),&nv_range,sizeof(NV_RR_PLMN_PACKED_RANGE_T));
        nv_space_used += sizeof(NV_RR_PLMN_PACKED_RANGE_T);

      }

      plmn_counter++;
    }
    //move to next plmn
    temp_plmn=temp_plmn->next;
  }

  //store no_of_plmns successfully copied
  temp_nv.no_of_plmns=plmn_counter;

#ifdef PERLUTF
  #error code not present
#else
  //TODO add support in NV
  return FALSE;
#endif

#endif // FEATURE_GSM_PLMN_STORED_LIST_NV
  return TRUE;
} // rr_write_freq_NV

/*===========================================================================*/
/**
 * Takes 2 ranges and produces a single merged range (the first range is
 * changed to contain the merged range)
 *
 * @param [in/out]new_list First range (merged)
 *
 * @param list2    Second range
 */
/*===========================================================================*/
void rr_merge_single_range(RR_BA_range_T* new_list, RR_BA_range_T* list2)
{
  new_list->low_range  = min(new_list->low_range,list2->low_range);
  new_list->high_range = max(new_list->high_range,list2->high_range);
} // rr_merge_single_range

/*===========================================================================

FUNCTION rr_combine_freq_overlap

DESCRIPTION
 Combine 2 frequencies if they overlap or close enough together (rr_merge_freq_limit)
 Assume ranges in new_list come before list2

PARAMETERS
 RR_BA_range_T* new_list-first range
 RR_BA_range_T* list2-second range

DEPENDENCIES
  None

RETURN VALUE
  TRUE-ranges merged
  FALSE-ranges do not need merging

SIDE EFFECTS
  modifies new_list with new range

===========================================================================*/
boolean rr_combine_freq_overlap(RR_BA_range_T* new_list, RR_BA_range_T* list2)
{
  // only need to merge if the high of new_list is within the merge limit of
  // the low of range 2
  if (!(new_list->high_range + rr_merge_freq_limit < list2->low_range))
  {
    rr_merge_single_range(new_list,list2);
    return TRUE;
  }

  return FALSE;
} // rr_combine_freq_overlap

//compares ranges... comparison is based on low value of the range
int rr_compare_range( const void *arg1, const void *arg2 )
{
  // <0 elem1 less than elem2
  // 0  elem1 equivalent to elem2
  // >0 elem1 greater than elem2

  return ((RR_BA_range_T*)arg1)->low_range - ((RR_BA_range_T*)arg2)->low_range;
}

//compare distances between ranges
int rr_compare_distance_range( const void *arg1, const void *arg2 )
{
  // <0 elem1 less than elem2
  // 0  elem1 equivalent to elem2
  // >0 elem1 greater than elem2

  return ((rr_distance_range_T*)arg1)->distance - ((rr_distance_range_T*)arg2)->distance;
}

//compare indexes
int rr_compare_index( const void *arg1, const void *arg2 )
{
  // <0 elem1 less than elem2
  // 0  elem1 equivalent to elem2
  // >0 elem1 greater than elem2

  return ((rr_distance_range_T*)arg1)->index - ((rr_distance_range_T*)arg2)->index;
}

/*===========================================================================

FUNCTION rr_reduce_ranges

DESCRIPTION
 Reduces the number of ranges to RR_MAX_BA_RANGES

PARAMETERS
 RR_BA_range_T* new_list - list to reduce
 byte* no_of_entries - number of entries in list

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  modifies new_list and no_of_entries with merged list

===========================================================================*/

void rr_reduce_ranges(RR_BA_range_T* new_list, byte* no_of_entries)
{
  byte new_list_no_of_entries=*no_of_entries;
  int count;

  //quicksort ranges in increasing order
  qsort((void*)&new_list[0],new_list_no_of_entries,sizeof(RR_BA_range_T),rr_compare_range);

  //check if list too big
  if (new_list_no_of_entries > RR_MAX_BA_RANGES)
  {
    //too many ranges, have to merge closest links

    //number of merges needed
    int num_merges = new_list_no_of_entries-RR_MAX_BA_RANGES;
    //number of merges done
    int merges_done = 0;
    //used to hold the distances between ranges
    rr_distance_range_T range[RR_MAX_BA_RANGES*2];
    int last_valid_entry_index;

    //get distance between ranges
    for (count=0;count<(new_list_no_of_entries-1);count++)
    {
      //record distances
      range[count].distance=new_list[count+1].low_range-new_list[count].high_range;
      //record index
      range[count].index=count+1;
    }

    //sort distance in increasing order
    qsort((void*)&range,new_list_no_of_entries-1,sizeof(rr_distance_range_T),rr_compare_distance_range);

    //take the smallest distances and sort them by their indexes
    qsort((void*)&range,num_merges,sizeof(rr_distance_range_T),rr_compare_index);

    last_valid_entry_index=range[0].index-1;

    //combine ranges based on distances
    for (count=range[0].index;count<new_list_no_of_entries;count++)
    {
      //compare last valid entry with new entry
      if (count==range[merges_done].index && merges_done<num_merges)
      {
        //Merge
        rr_merge_single_range(&new_list[last_valid_entry_index],&new_list[count]);
        merges_done++;
      }
      else
      {
        //copy to valid position
        last_valid_entry_index++;
        new_list[last_valid_entry_index]=new_list[count];
      }
    }

    new_list_no_of_entries=last_valid_entry_index+1;
    ASSERT(new_list_no_of_entries==RR_MAX_BA_RANGES);
  }

  *no_of_entries=new_list_no_of_entries;
} // rr_reduce_ranges

/*===========================================================================

FUNCTION rr_merge_ba_range

DESCRIPTION
  Merges added entries into new list


PARAMETERS
  RR_PLMN_BA_range_T* new_list - original list to be merged into
  RR_PLMN_BA_range_T* added_entries - new entries to be merged

DEPENDENCIES
  None

RETURN VALUE
  None
SIDE EFFECTS
  Alters new_list

===========================================================================*/
void rr_merge_ba_range(RR_PLMN_BA_range_T* old_list, RR_PLMN_BA_range_T* added_entries)
{

  int count;

  //used to combine 2 lists together
  RR_BA_range_T new_list[RR_MAX_BA_RANGES*2];
  byte new_list_no_of_entries=old_list->no_of_ranges+added_entries->no_of_ranges;
  byte last_valid_entry_index;


  if (new_list_no_of_entries==0)
  {
    //both list empty can quit
    return;
  }

  //combine list
  memscpy(&new_list[0],sizeof(new_list),&old_list->RR_BA_ranges[0],sizeof(RR_BA_range_T)*old_list->no_of_ranges);
  memscpy(&new_list[old_list->no_of_ranges],sizeof(new_list),&added_entries->RR_BA_ranges[0],
         sizeof(RR_BA_range_T)*added_entries->no_of_ranges);


  //quicksort ranges in increasing order
  qsort((void*)&new_list[0],new_list_no_of_entries,sizeof(RR_BA_range_T),rr_compare_range);


  //used to keep track of which index contains a valid range
  //we have a non empty list, so first index is always valid
  last_valid_entry_index=0;

  //remove overlap of entries in new list, search through entire list
  for (count=1;count<new_list_no_of_entries;count++)
  {
    //compare last valid entry with new entry
    if (rr_combine_freq_overlap(&new_list[last_valid_entry_index],&new_list[count])==FALSE)
    {
      //they don't overlap, need to make this the new valid entry

      //copy to next position
      last_valid_entry_index++;
      new_list[last_valid_entry_index]=new_list[count];

    }
  }

  new_list_no_of_entries=last_valid_entry_index+1;


  //check if list too big
  if (new_list_no_of_entries>RR_MAX_BA_RANGES)
  {
    //too many ranges, have to merge closest links
    rr_reduce_ranges(new_list,&new_list_no_of_entries);
  }
  memscpy(&(old_list->RR_BA_ranges[0]),sizeof(old_list->RR_BA_ranges),&new_list[0],sizeof(RR_BA_range_T)*new_list_no_of_entries);

  old_list->no_of_ranges=new_list_no_of_entries;
} // rr_merge_ba_range

/*===========================================================================

FUNCTION rr_add_to_stored_ba_list

DESCRIPTION
 Adds/merges a frequency list into to RAM table

PARAMETERS
 RR_PLMN_BA_range_T new_list - is the frequency list to be added
 merge - if true the new list is merged, otherwise it overwrites


DEPENDENCIES
  None

RETURN VALUE
  TRUE-successful
  FALSE-failed

SIDE EFFECTS
  modifies stored plmn database

===========================================================================*/
boolean rr_add_to_stored_freq_list(RR_PLMN_BA_range_T* new_list, boolean merge, const gas_id_t gas_id)
{

  int counter;
  RR_freq_list_record_T* prev_plmn=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
  RR_freq_list_record_T* temp_plmn=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
  boolean plmn_found=FALSE; //indicates whether plmn is in datatabase

  //search database for plmn
  for (counter=0; counter<rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns;counter++)
  {
    ASSERT(temp_plmn!=NULL);

    if (sys_plmn_match(new_list->plmn_id,temp_plmn->data.plmn_id))
    {
      plmn_found=TRUE;
      break;
    }

    if (counter!=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns-1)
    {
      prev_plmn=temp_plmn;
      temp_plmn=temp_plmn->next;
    }
  }

  //temp_plmn will point to plmn we are searching for or the last plmn if nothing found
  //prev_plmn will point to plmn before temp_plmn assuming that the found plmn is the second plmn or greater

  if (plmn_found==FALSE)
  {
    //cannot merge since plmn not in list, must add new entry
    merge=FALSE;

    if (rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns<RR_MAX_RAM_PLMN_ENTRIES)
    {
      RR_freq_list_record_T* mem_block;

      //allocate a new plmn entry
      mem_block=(RR_freq_list_record_T*)GS_ALLOC(sizeof(RR_freq_list_record_T));

      if (mem_block!=NULL)
      {
        //save last entry in prev_plmn
        //temp_plmn may be null if list is empty, this is ok
        prev_plmn=temp_plmn;

        temp_plmn=mem_block;
        temp_plmn->next=NULL;

        rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns++;

        if (rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbno_of_plmns==1)
        {  //only 1 entry adjust head pointer
           rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=temp_plmn;
        }
      }
      else
      {
        MSG_GERAN_ERROR_0_G("Ran out of memory");

        //memory couldn't be allocated, will be using last entry in temp_plmn to store new plmn
        if (temp_plmn==NULL)
        {
          //no entry even in temp plmn, list was empty, can't allocate new entry have to quit
          return FALSE;
        }
      }
    }
    else
    {
      MSG_GERAN_HIGH_0_G("Plmn freq table full!");
      //don't need to do anything special
      //temp_plmn will be pointing at last entry
    }
  }

  if (merge==TRUE)
  {
    //merge ba list
    rr_merge_ba_range(&temp_plmn->data,new_list);
  }
  else
  {
    //overwrite ba list
    temp_plmn->data=*new_list;
  }

  //temp plmn is not at the top of the plmn list must reorder list
  //since temp plmn is not at the top of the list there must be at least 2 entries
  //so prev_plmn and temp_plmn will be pointing to valid locations
  if (rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry!=temp_plmn)
  {
    //take found plmn out of the list by hooking up the plmn before it to the one after it
    prev_plmn->next=temp_plmn->next;
    //take plmn currently in front and hook it up after the found plmn
    temp_plmn->next=rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry;
    //change the first entry to the found plmn
    rr_plmn_freq_db_data_ptr->rr_plmn_freq_dbfirst_entry=temp_plmn;
  }
  return TRUE;
}

#else

/* Dummy function to allow warning-free compilation when feature undefined */
void rr_plmn_freq_db_dummy_fn(void){}

#endif /* FEATURE_MULTIMODE_GSM_STORED_FREQ */


/* EOF */

