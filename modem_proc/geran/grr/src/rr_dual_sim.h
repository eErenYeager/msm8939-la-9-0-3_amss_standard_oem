/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            GRR DUAL SIM HEADER

GENERAL DESCRIPTION
  Provides Dual SIM support for GERAN Radio Resources (GRR)

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010-2013 by QUALCOMM Technologies, Incorporated. All Rights Reserved.

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_dual_sim.h#1 $
$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
31/05/13    SJW     Deprecated : functionality moved to rr_multi_sim.c
dd/mm/yy    SJW     Sample text for edit history
===========================================================================*/
#ifndef __RR_DUAL_SIM_H__
#define __RR_DUAL_SIM_H__

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"
#include "geran_multi_sim.h"


extern uint8 rr_ds_get_data_space_index(const gas_id_t);

#define DEFAULT_GAS_ID_IF_UNKNOWN geran_map_nas_id_to_gas_id(SYS_MODEM_AS_ID_1)

#endif /* __RR_DUAL_SIM_H__ */

/* EOF */

