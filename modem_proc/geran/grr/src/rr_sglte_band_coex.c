/*============================================================================
  @file rr_sglte_band_coex.c

  @brief This module implements a state machine that manages band coexistence
         on SGLTE platforms.

  B3 = DCS 1800
  B9 = EGSM 900
  B39 = LTE / TDSCDMA band that uses a subset of the DCS 1800 frequency range

  The state machine within this module is initialised in NULL state.  When
  SGLTE mode is activated, it by default moves to the IDLE_CS_ONLY_ALL_BANDS_ENABLED
  state.

  If while in this state the
  
  (serving cell ARFCN or a neighbour cell ARFCN is in B39)
  AND
  (serving cell ARFCN or a neighbour cell ARFCN is in B8)

  then the state will change to IDLE_CS_ONLY_B3_DISABLED.  In this state, B9
  support is temporarily disabled in RR.  Note that this may result in cell
  selection being triggered, as the UE may need to move to a cell in B8.

                Copyright (c) 2009-2013 QUALCOMM Technologies, Inc
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/grr/src/rr_sglte_band_coex.c#1 $ */

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_SGLTE

#include "comdef.h"
#include "rr_sglte_band_coex.h"
#include "rr_defs.h"
#include "rr_gprs_debug.h"
#include "gprs_mem.h"
#include "rr_msgr.h"
#include "rr_nv.h"
#include "rr_log.h"
#include "rr_control.h"
/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *--------------------------------------------------------------------------*/

#define RR_SGLTE_BAND_COEX_DCS_1800_COEX_LOW_ARFCN  736
#define RR_SGLTE_BAND_COEX_DCS_1800_COEX_HIGH_ARFCN 885

/*----------------------------------------------------------------------------
 * Type Declarations
 *--------------------------------------------------------------------------*/

typedef enum
{
  RR_SGLTE_BAND_COEX_STATE_NULL, 
  RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1,
  RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2,
  RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3,
  RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C,
  RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C,
  RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C,
  RR_SGLTE_BAND_COEX_STATE_MAX
} rr_sglte_band_coex_state_e;

typedef struct
{
  rr_sglte_band_coex_state_e   state;
  rr_sglte_band_coex_state_e   old_state;
  ARFCN_T                      scell_arfcn;
  inter_task_BCCH_allocation_T ba_list;
  channel_spec_T               channel_spec;
  uint32                       do_not_disable_b9_timeout;
} rr_sglte_band_coex_data_t;

/*----------------------------------------------------------------------------
 * Global Data Definitions
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 *--------------------------------------------------------------------------*/

static rr_sglte_band_coex_data_t rr_sglte_band_coex_data[NUM_GERAN_DATA_SPACES];

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 *--------------------------------------------------------------------------*/

static rr_sglte_band_coex_data_t *rr_sglte_get_coex_data_ptr(const gas_id_t gas_id)
{
  return(&(rr_sglte_band_coex_data[GERAN_MAP_GAS_ID_TO_DATA_SPACE_INDEX(gas_id)]));
}


static const char * rr_sglte_band_coex_state_name(rr_sglte_band_coex_state_e state)
{
  switch (state)
  {
    case RR_SGLTE_BAND_COEX_STATE_NULL:
      return "NULL"; 
    case RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1:
      return "IDLE_ALL_BANDS_ENABLED_S1";
    case RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2:
      return "IDLE_CS_ONLY_B3_DISABLED_S2";
    case RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3:
      return "IDLE_B39_DISABLED_S3";
    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C:
      return "CONNECTED_ALL_BANDS_ENABLED_S1C";
    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C:
      return "CONNECTED_B3_DISABLED_S2C";
    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C:
      return "CONNECTED_B39_DISABLED_S3C";
    default:
      MSG_GERAN_ERROR_1("Unexpected state: %d", state);
      return "?";
  }
}

static void rr_send_geran_grr_update_forbidden_bands_ind(
  sys_band_mask_type   geran_bands,
  sys_band_mask_type   tds_bands,
  sys_band_mask_type   lte_bands,
  const gas_id_t       gas_id
)
{
  geran_grr_update_forbidden_bands_ind_t update_forbidden_bands_ind;
  errno_enum_type                        result;

  memset(&update_forbidden_bands_ind, 0, sizeof(update_forbidden_bands_ind));

  msgr_init_hdr(
    &update_forbidden_bands_ind.hdr,
    MSGR_GERAN_GRR,
    GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND
  );
  update_forbidden_bands_ind.geran_bands = geran_bands;
  update_forbidden_bands_ind.tds_bands = tds_bands;
  update_forbidden_bands_ind.lte_bands = lte_bands;

  MSG_4(MSG_SSID_DFLT, MSG_LEGACY_LOW, "Sending GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND(geran: %x, tds: %d, lte: %04x%04x)",
                     geran_bands,
                     tds_bands,
                     lte_bands >> 32, lte_bands & 0xffffffff);

  result = rr_msgr_send(&update_forbidden_bands_ind.hdr, sizeof(update_forbidden_bands_ind), gas_id);
  if (result != E_SUCCESS)
  {
    MSG_GERAN_ERROR_1("rr_msgr_send failed: %d", result);
  }
}

static boolean rr_sglte_band_coex_ba_list_contains_b39(
  inter_task_BCCH_allocation_T * ba_list
)
{
  uint32 i;

  RR_NULL_CHECK_RETURN_PARAM(ba_list, FALSE);

  for (i = 0;
       i < ba_list->no_of_entries &&
       i < ARR_SIZE(ba_list->BCCH_ARFCN);
       ++i)
  {
    if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(ba_list->BCCH_ARFCN[i]))
    {
      return TRUE;
    }
  }
  return FALSE;
}

static boolean rr_sglte_band_coex_arfcn_is_in_b8(
  ARFCN_T arfcn
)
{
  if (arfcn.band == SYS_BAND_PGSM_900 ||
      arfcn.band == SYS_BAND_EGSM_900)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

static boolean rr_sglte_band_coex_ba_list_contains_b8(
  inter_task_BCCH_allocation_T * ba_list
)
{
  uint32 i;

  RR_NULL_CHECK_RETURN_PARAM(ba_list, FALSE);

  for (i = 0;
       i < ba_list->no_of_entries &&
       i < ARR_SIZE(ba_list->BCCH_ARFCN);
       ++i)
  {
    if (rr_sglte_band_coex_arfcn_is_in_b8(ba_list->BCCH_ARFCN[i]))
    {
      return TRUE;
    }
  }
  return FALSE;
}

static boolean rr_sglte_band_coex_channel_spec_contains_b39(
  channel_spec_T * channel_spec
)
{
  uint32 i;

  RR_NULL_CHECK_RETURN_PARAM(channel_spec, FALSE);

  RR_ASSERT_WARN(channel_spec->number_of_ded_channels <= 2);

  /* Search 'before' parameters first. */
  if (channel_spec->before_parameters_valid)
  {
    if (channel_spec->number_of_ded_channels > 0)
    {
      frequency_list_T * frequency_list = &channel_spec->channel_info_1_before.frequency_list;

      for (i = 0; i < frequency_list->no_of_items && i < ARR_SIZE(frequency_list->channel); ++i)
      {
        if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(frequency_list->channel[i]))
        {
          MSG_GERAN_HIGH_3("SGLTE B39: channel_info_1_before->frequency_list[%d] (ARFCN %d (band %d) is in B39",
                           i, frequency_list->channel[i].num, frequency_list->channel[i].band);
          return TRUE;
        }
      }

      if (channel_spec->number_of_ded_channels > 1)
      {
        frequency_list = &channel_spec->channel_info_2_before.frequency_list;

        for (i = 0; i < frequency_list->no_of_items && i < ARR_SIZE(frequency_list->channel); ++i)
        {
          if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(frequency_list->channel[i]))
          {
            MSG_GERAN_HIGH_3("SGLTE B39: channel_info_2_before->frequency_list[%d] (ARFCN %d (band %d) is in B39",
                             i, frequency_list->channel[i].num, frequency_list->channel[i].band);
            return TRUE;
          }
        }
      }
    }
  }

  /* Now search 'after' parameters. */
  if (channel_spec->number_of_ded_channels > 0)
  {
    frequency_list_T * frequency_list = &channel_spec->channel_info_1_after.frequency_list;

    for (i = 0; i < frequency_list->no_of_items && i < ARR_SIZE(frequency_list->channel); ++i)
    {
      if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(frequency_list->channel[i]))
      {
        MSG_GERAN_HIGH_3("SGLTE B39: channel_info_1_after->frequency_list[%d] (ARFCN %d (band %d) is in B39",
                         i, frequency_list->channel[i].num, frequency_list->channel[i].band);
        return TRUE;
      }
    }

    if (channel_spec->number_of_ded_channels > 1)
    {
      frequency_list = &channel_spec->channel_info_2_after.frequency_list;

      for (i = 0; i < frequency_list->no_of_items && i < ARR_SIZE(frequency_list->channel); ++i)
      {
        if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(frequency_list->channel[i]))
        {
          MSG_GERAN_HIGH_3("SGLTE B39: channel_info_2_after->frequency_list[%d] (ARFCN %d (band %d) is in B39",
                           i, frequency_list->channel[i].num, frequency_list->channel[i].band);
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

static uint8 rr_sglte_band_coex_get_arfcn_sort_value(
  ARFCN_T arfcn
)
{
  uint8 sort_value;

  if (arfcn.band == SYS_BAND_PGSM_900 ||
      arfcn.band == SYS_BAND_EGSM_900)
  {
    sort_value = 3;
  }
  else if (arfcn.band == SYS_BAND_DCS_1800)
  {
    if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(arfcn))
    {
      sort_value = 1;
    }
    else
    {
      sort_value = 2;
    }
  }
  else
  {
    sort_value = 0;
  }

  return sort_value;
}

static int rr_sglte_band_coex_cmp_pscan_entries_fn(
  const void * a,
  const void * b
)
{
  uint8 a_band_sort_value;
  uint8 b_band_sort_value;

  RR_NULL_CHECK_RETURN_PARAM(a, 0);
  RR_NULL_CHECK_RETURN_PARAM(b, 0);

  a_band_sort_value = rr_sglte_band_coex_get_arfcn_sort_value(
    ((rr_l1_freq_power_T *) a)->arfcn
  );

  b_band_sort_value = rr_sglte_band_coex_get_arfcn_sort_value(
    ((rr_l1_freq_power_T *) b)->arfcn
  );

  if (a_band_sort_value > b_band_sort_value)
  {
    return -1;
  }
  else if (b_band_sort_value > a_band_sort_value)
  {
    return 1;
  }
  else
  {
    /* Equal bands, so sort based on RXLEV */
    if (((rr_l1_freq_power_T *) a)->RXLEV_average > ((rr_l1_freq_power_T *) b)->RXLEV_average)
    {
      return -1;
    }
    else if (((rr_l1_freq_power_T *) a)->RXLEV_average < ((rr_l1_freq_power_T *) b)->RXLEV_average)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
}

/*-----------------------------------------------------------------------------
 * Externalised Function Definitions
 *---------------------------------------------------------------------------*/

void rr_sglte_band_coex_task_start_init(void)
{
  memset(rr_sglte_band_coex_data, 0, sizeof(rr_sglte_band_coex_data));
}

void rr_sglte_band_coex_reset(const gas_id_t gas_id)
{
  rr_sglte_band_coex_data_t *rr_sglte_band_coex_data_ptr = rr_sglte_get_coex_data_ptr(gas_id);

  RR_NULL_CHECK_FATAL(rr_sglte_band_coex_data_ptr);

  memset(rr_sglte_band_coex_data_ptr, 0, sizeof(rr_sglte_band_coex_data_t));

  return;
}

boolean rr_sglte_band_coex_arfcn_is_in_b39_coex_range(
  ARFCN_T arfcn
)
{
  if (arfcn.band == SYS_BAND_DCS_1800)
  {
    if (arfcn.num >= RR_SGLTE_BAND_COEX_DCS_1800_COEX_LOW_ARFCN &&
        arfcn.num <= RR_SGLTE_BAND_COEX_DCS_1800_COEX_HIGH_ARFCN)
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }
  else
  {
    return FALSE;
  }
}

sys_band_mask_type rr_sglte_band_coex_disabled_bands(const gas_id_t gas_id)
{
  rr_sglte_band_coex_data_t *rr_sglte_band_coex_data_ptr = rr_sglte_get_coex_data_ptr(gas_id);

  RR_NULL_CHECK_FATAL(rr_sglte_band_coex_data_ptr);

  switch (rr_sglte_band_coex_data_ptr->state)
  {
    case RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2:
    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C:
    {
      return SYS_BAND_MASK_GSM_DCS_1800;
    }

    default:
    {
      return SYS_BAND_MASK_EMPTY;
    }
  }
}

void rr_sglte_band_coex_prioritise_pscan_results(
  rr_l1_pscan_results_T * pscan_results
)
{
  RR_NULL_CHECK_RETURN_VOID(pscan_results);

  qsort(pscan_results->meas,
        pscan_results->num_frequencies,
        sizeof(rr_l1_freq_power_T),
        rr_sglte_band_coex_cmp_pscan_entries_fn);
}

void rr_sglte_band_coex_control(
   rr_cmd_bdy_type * message,
   const gas_id_t gas_id)
{
  rr_sglte_band_coex_data_t *rr_sglte_band_coex_data_ptr = rr_sglte_get_coex_data_ptr(gas_id);

  RR_NULL_CHECK_FATAL(rr_sglte_band_coex_data_ptr);

  /* Validate input */
  RR_NULL_CHECK_RETURN_VOID(message);

  if (rr_get_nv_sglte_b39_coex_enabled() == FALSE)
  {
    MSG_GERAN_HIGH_0_G("SGLTE B39 Coexistence feature disabled in NV");
    return;
  }

  /* Stateless handlers:
  
  BA_LIST_UPDATE_IND - stores the contents of this message
  CHANNEL_SPEC_UPDATE_IND - stores the contents of this message
  OUT_OF_SERVICE_IND - clears all stored frequency information
  CS_DISCONNECTED - clears channel_spec stored frequency information
  
  These messages are still processed within the state machine below, in order to
  trigger state transitions appropriately. */
  switch (message->message_header.message_set)
  {
    case MS_RR_RR:
    {
      switch (message->rr.header.rr_message_set)
      {
        case RR_SGLTE_BAND_COEX_IMSG:
        {
          switch (message->message_header.message_id)
          {
            case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
            {
              rr_sglte_band_coex_data_ptr->scell_arfcn = message->rr.sglte_band_coex.imsg.ba_list_update_ind.scell_arfcn;
              rr_sglte_band_coex_data_ptr->ba_list = message->rr.sglte_band_coex.imsg.ba_list_update_ind.ba_list;

              rr_log_ba_list(&(rr_sglte_band_coex_data_ptr->ba_list), gas_id);
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND */

            case RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND:
            {
              rr_sglte_band_coex_data_ptr->scell_arfcn = message->rr.sglte_band_coex.imsg.channel_spec_update_ind.scell_arfcn;
              rr_sglte_band_coex_data_ptr->channel_spec = message->rr.sglte_band_coex.imsg.channel_spec_update_ind.channel_spec;
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND */

            case RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND:
            {
              rr_sglte_band_coex_data_ptr->scell_arfcn.num = INVALID_ARFCN;
              rr_sglte_band_coex_data_ptr->scell_arfcn.band = SYS_BAND_NONE;
              memset(&rr_sglte_band_coex_data_ptr->ba_list, 0, sizeof(rr_sglte_band_coex_data_ptr->ba_list));
              memset(&rr_sglte_band_coex_data_ptr->channel_spec, 0, sizeof(rr_sglte_band_coex_data_ptr->channel_spec));
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND */

            case RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND:
            {
              /* Clear the channel_spec as it is no longer valid. */
              memset(
                &rr_sglte_band_coex_data_ptr->channel_spec,
                0,
                sizeof(rr_sglte_band_coex_data_ptr->channel_spec)
              );
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND */

            default: ; /* No action necessary. */
          }
          break;
        } /* RR_SGLTE_BAND_COEX_IMSG */

        default: ; /* No action necessary. */
      }
      break;
    } /* MS_RR_RR */

    default: ; /* No action necessary. */
  }

  MSG_HIGH("RR SGLTE Band Coex signal%d, arfcn:%d, state:%d",
           message->message_header.message_id,
           rr_sglte_band_coex_data_ptr->scell_arfcn.num,
           rr_sglte_band_coex_data_ptr->state);

  /*Self correct band coex SM 
    If coex SM is NULL and SGLTE is active then self correct if the signal
    received is not RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND.
   
    This handles use case where band coex SM is reset for LPM and UE mode is not reset.*/
  if (rr_sglte_band_coex_data_ptr->state == RR_SGLTE_BAND_COEX_STATE_NULL &&
      message->message_header.message_set == MS_RR_RR &&
      message->rr.header.rr_message_set == RR_SGLTE_BAND_COEX_IMSG &&
      message->message_header.message_id != RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND &&
      rr_is_sglte_mode_active(gas_id))
  { 
    MSG_HIGH("RR SGLTE BAND COEX activated, auto corrected",0,0,0);
    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
    rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout = rr_get_nv_sglte_b39_coex_hysteresis_timeout();
    MSG_HIGH("RR SGLTE DonotGoToState2 timer intialized to %d",rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout,0,0);
  }

  switch (rr_sglte_band_coex_data_ptr->state)
  {
    case RR_SGLTE_BAND_COEX_STATE_NULL:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND:
                {
                  MSG_HIGH("RR SGLTE BAND COEX activated",0,0,0);
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout = rr_get_nv_sglte_b39_coex_hysteresis_timeout();
                  MSG_HIGH("RR SGLTE DonotGoToState2 timer intialized to %d",rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout,0,0);				  
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_ACTIVE_IND */

                default: LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state)); /* All other messages are ignored in this state. */
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default: ;  /* All other messages are ignored in this state. */
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_NULL */

    /* In this state, GERAN is CS only registered and has all its SGLTE band
    capabilities (i.e. B8 and B3) and GERAN enabled Band 39 for LTE/TDS
    operation. GERAN enters this state �at power up� or �GERAN OOS from this
    state� or �if the camped frequency and all neighbors in neighbor list are
    not in 1850-1880MHz range�, or �when OOS search in state 2 resulted in
    adding B3 to GERAN capability�, or �when OOS search in state 3 resulted in
    adding B39 to LTE/TDS capability�, or �the domain changes from CS+PS to CS
    only in state 4�. */
    case RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
                {
                  if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                      rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list))
                  {
                    if (rr_sglte_band_coex_arfcn_is_in_b8(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                        rr_sglte_band_coex_ba_list_contains_b8(&rr_sglte_band_coex_data_ptr->ba_list))
                    {
                      if (rr_timer_is_running(RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER, gas_id))
                      {
                        MSG_GERAN_HIGH_0_G("RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER running, not disabling B3");
                        /* Send update to Policy Manager informing it that B39 should
                        be disabled in LTE. */
                        rr_send_geran_grr_update_forbidden_bands_ind(
                          SYS_BAND_MASK_EMPTY,
                          SYS_BAND_MASK_TDS_BANDF,
                          SYS_BAND_MASK_LTE_BAND39,
                          gas_id
                        );

                        rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3;						
                      }
                      else
                      {
                        rr_PLMN_request_details_T * rr_PLMN_request_details_ptr = rr_get_PLMN_request_details_ptr(gas_id);

                        RR_NULL_CHECK_FATAL(rr_PLMN_request_details_ptr);

                        if (rr_PLMN_request_details_ptr->service_domain == SYS_SRV_DOMAIN_CS_ONLY)
                        {
                          /* Send an OMSG to indicate to GRR that it should check
                          to make sure the serving cell is still in a supported band. */
                          rr_sglte_band_coex_send_omsg(RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND, gas_id);
  
                          rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2;
                        }
                        else
                        {
                          MSG_GERAN_HIGH_0_G("RR_SGLTE service domain CS Only do not move to IDLE_CS_ONLY_B3_DISABLED_S2");
                        }
                      }
                    }
                    else
                    {
                      /* Send update to Policy Manager informing it that B39 should
                      be disabled in LTE. */
                      rr_send_geran_grr_update_forbidden_bands_ind(
                        SYS_BAND_MASK_EMPTY,
                        SYS_BAND_MASK_TDS_BANDF,
                        SYS_BAND_MASK_LTE_BAND39,
                        gas_id
                      );

                      rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3;
                    }
                  }
                  else
                  {
                    /* No action necessary, no chance of moving to B39 frequencies from here. */
                    MSG_MED("No action scell ARFCN, BA list not in B39",0,0,0);
                  }                  
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_NULL;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND:
                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                {
                  /* No action necessary. */
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND */

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1 */

    /* In this state, GERAN is CS only registered and removes B3 from its
    capability , enables GERAN B8 operation and GERAN enables LTE/TDS B39
    operation. GERAN enters this state if �the camped frequency or any of
    neighbors in the neighbor list is in 1850-1880 MHz range� and �the neighbor
    list contain a band B8 neighbor or serving cell is in B8  and
    DonotGoToState2Timer  expired or not running�, or �OOS is triggered in this
    state�, or �OOS is triggered in state 2C�. */
    case RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
                {
                  if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                      rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list))
                  {
                    /* No action necessary. */
                    MSG_HIGH("RR SGLTE BA_LIST_UPDATE_IND no action necessary",0,0,0);
                  }
                  else
                  {
                    /* Send an OMSG to indicate to GRR that it should check
                    to make sure the serving cell is still in a supported band. */
                    rr_sglte_band_coex_send_omsg(RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND, gas_id);
 
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  }                  
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND:
                {
                  if (rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout)
                  {
                    MSG_GERAN_HIGH_1_G("Starting RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER, %dms",
                                       rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout);

                    rr_timer_start(
                      RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER,
                      rr_sglte_band_coex_data_ptr->do_not_disable_b9_timeout,
                      gas_id
                    );
                  }
                  else
                  {
                    MSG_GERAN_HIGH_0_G("Not starting RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER, timeout set to 0m");
                  }

                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND:
                {
                  rr_sglte_band_coex_send_omsg(RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND, gas_id);

                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_NULL;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                {
                  switch (message->rr.sglte_band_coex.imsg.service_domain_changed_ind.service_domain)
                  {
                    case SYS_SRV_DOMAIN_CS_PS:
                    {
                      rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                      break;
                    } /* SYS_SRV_DOMAIN_CS_PS */

                    default:
                    {
                      /* No action necessary. */
                      MSG_HIGH("RR SGLTE SERVICE_DOMAIN_CHANGED_IND no action necessary service:%d",
                           message->rr.sglte_band_coex.imsg.service_domain_changed_ind.service_domain,0,0);
                    }
                  }
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND */

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2 */

    /* In this state, GERAN is CS only registered and has both B3 and B8 in its
    capability, and GERAN disables B39 for LTE/TDS operation. GERAN enters this
    state if �the camped frequency or any of neighbors in the neighbor list is
    in 1850-1880 MHz range� and �the neighbor list contains no neighbor in B8
    and  serving cell is not in B8� or �OOS is triggered in this state�, or
    �OOS is triggered in state 3C�, or �[the camped frequency or any of
    neighbors in the neighbor list is in 1850-1880 MHz range� and �the neighbor
    list contains a band B8 neighbor or serving cell is in B8� and
    DonotGoToState2Timer  is  running]�. */
    case RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
                {
                  if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                      rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list))
                  {
                    if (rr_sglte_band_coex_arfcn_is_in_b8(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                        rr_sglte_band_coex_ba_list_contains_b8(&rr_sglte_band_coex_data_ptr->ba_list))
                    {
                      if (rr_timer_is_running(RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER, gas_id))
                      {
                        MSG_GERAN_HIGH_0_G("RR_SGLTE_BAND_COEX_DO_NOT_DISABLE_B9_TIMER running, not disabling B3 or enabled B39");
                      }
                      else
                      {
                        rr_PLMN_request_details_T * rr_PLMN_request_details_ptr = rr_get_PLMN_request_details_ptr(gas_id);

                        RR_NULL_CHECK_FATAL(rr_PLMN_request_details_ptr);

                        if (rr_PLMN_request_details_ptr->service_domain == SYS_SRV_DOMAIN_CS_ONLY)
                        {
                          rr_sglte_band_coex_send_omsg(RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND, gas_id);
  
                          /* Send update to Policy Manager informing it that B39
                          should no longer be disabled in LTE. */
                          rr_send_geran_grr_update_forbidden_bands_ind(
                            SYS_BAND_MASK_EMPTY,
                            SYS_BAND_MASK_EMPTY,
                            SYS_BAND_MASK_EMPTY,
                            gas_id
                          );

                          rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2;
                        }
                        else
                        {
                          MSG_GERAN_HIGH_0_G("RR_SGLTE CS_PS domain do not move to IDLE_CS_ONLY_B3_DISABLED_S2");
                        }
                      }
                    }
                    else
                    {
                      /* No action necessary. */
                      MSG_HIGH("RR SGLTE BA_LIST_UPDATE_IND no action necessary",0,0,0);
                    }
                  }
                  else
                  {
                    /* B39 no longer needs to be disabled, so inform Policy Manager
                    and switch state. */
                    rr_send_geran_grr_update_forbidden_bands_ind(
                      SYS_BAND_MASK_EMPTY,
                      SYS_BAND_MASK_EMPTY,
                      SYS_BAND_MASK_EMPTY,
                      gas_id
                    );
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  }                  
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_OUT_OF_SERVICE_IND:
                {
                  /* B39 no longer needs to be disabled, so inform Policy Manager
                  and switch state. */
                  rr_send_geran_grr_update_forbidden_bands_ind(
                    SYS_BAND_MASK_EMPTY,
                    SYS_BAND_MASK_EMPTY,
                    SYS_BAND_MASK_EMPTY,
                    gas_id
                  );
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_CONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND:
                {
                  rr_send_geran_grr_update_forbidden_bands_ind(
                    SYS_BAND_MASK_EMPTY,
                    SYS_BAND_MASK_EMPTY,
                    SYS_BAND_MASK_EMPTY,
                    gas_id
                  );
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_NULL;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SGLTE_MODE_INACTIVE_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                {  
                  /*no action*/
                  break;
                }

                case RR_SGLTE_BAND_COEX_IMSG_DONOTGOTOSTATE2_TIMER_EXPIRED_IND:
                {
                  if ( (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                        rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list)) &&
                       (rr_sglte_band_coex_arfcn_is_in_b8(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                        rr_sglte_band_coex_ba_list_contains_b8(&rr_sglte_band_coex_data_ptr->ba_list)) )
                  {

                    rr_PLMN_request_details_T * rr_PLMN_request_details_ptr = rr_get_PLMN_request_details_ptr(gas_id);

                    RR_NULL_CHECK_FATAL(rr_PLMN_request_details_ptr);

                    if (rr_PLMN_request_details_ptr->service_domain == SYS_SRV_DOMAIN_CS_ONLY)
                    {
                      rr_sglte_band_coex_send_omsg(RR_SGLTE_BAND_COEX_OMSG_SUPPORTED_BANDS_UPDATED_IND,  gas_id);

                      /* Send update to Policy Manager informing it that B39
                      should no longer be disabled in LTE. */
                      rr_send_geran_grr_update_forbidden_bands_ind(
                        SYS_BAND_MASK_EMPTY,
                        SYS_BAND_MASK_EMPTY,
                        SYS_BAND_MASK_EMPTY,
                        gas_id
                      );

                      rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2;
                    }
                    else
                    {
                      MSG_GERAN_HIGH_0_G("RR_SGLTE CS_PS domain do not move to IDLE_CS_ONLY_B3_DISABLED_S2");
                    }
                  }
                  break;
                }

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3 */

    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
                case RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND:
                {
                  if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                      rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list) ||
                      rr_sglte_band_coex_channel_spec_contains_b39(&rr_sglte_band_coex_data_ptr->channel_spec))
                  {
                    /* Send update to Policy Manager informing it that B39 should
                    be disabled in LTE. */
                    rr_send_geran_grr_update_forbidden_bands_ind(
                      SYS_BAND_MASK_EMPTY,
                      SYS_BAND_MASK_TDS_BANDF,
                      SYS_BAND_MASK_LTE_BAND39,
                      gas_id
                    );
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C;
                  }
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATED_IND ... */

                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                {
                  /*no action*/
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND */

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C */

    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND:
                {
                  rr_PLMN_request_details_T * rr_PLMN_request_details_ptr = rr_get_PLMN_request_details_ptr(gas_id);

                  RR_NULL_CHECK_FATAL(rr_PLMN_request_details_ptr);

                  if (rr_PLMN_request_details_ptr->service_domain == SYS_SRV_DOMAIN_CS_ONLY)
                  {
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2;
                  }
                  else if (rr_PLMN_request_details_ptr->service_domain == SYS_SRV_DOMAIN_CS_PS)
                  {
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_ALL_BANDS_ENABLED_S1;
                  }
                  else
                  {
                    MSG_GERAN_ERROR_1_G("Unexpected service domain:%d",rr_PLMN_request_details_ptr->service_domain);
                  }
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                  /*no action*/
                break;

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C */

    case RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C:
    {
      switch (message->message_header.message_set)
      {
        case MS_RR_RR:
        {
          switch (message->rr.header.rr_message_set)
          {
            case RR_SGLTE_BAND_COEX_IMSG:
            {
              switch (message->message_header.message_id)
              {
                case RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND:
                {
                  rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_IDLE_B39_DISABLED_S3;
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_CS_DISCONNECTED_IND */

                case RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND:
                case RR_SGLTE_BAND_COEX_IMSG_CHANNEL_SPEC_UPDATE_IND:
                {
                  if (rr_sglte_band_coex_arfcn_is_in_b39_coex_range(rr_sglte_band_coex_data_ptr->scell_arfcn) ||
                      rr_sglte_band_coex_ba_list_contains_b39(&rr_sglte_band_coex_data_ptr->ba_list) ||
                      rr_sglte_band_coex_channel_spec_contains_b39(&rr_sglte_band_coex_data_ptr->channel_spec))
                  {
                    /* No change necessary. */
                    MSG_HIGH("RR SGLTE no action necessary, service_domain:%d",0,0,0);
                  }
                  else
                  {
                    /* Send update to Policy Manager informing it that B39 need
                    no longer be disabled in LTE. */
                    rr_send_geran_grr_update_forbidden_bands_ind(
                      SYS_BAND_MASK_EMPTY,
                      SYS_BAND_MASK_EMPTY,
                      SYS_BAND_MASK_EMPTY,
                      gas_id
                    );
                    rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_CONNECTED_ALL_BANDS_ENABLED_S1C;
                  }
                  break;
                } /* RR_SGLTE_BAND_COEX_IMSG_BA_LIST_UPDATE_IND ... */

                case RR_SGLTE_BAND_COEX_IMSG_SERVICE_DOMAIN_CHANGED_IND:
                  /*no action*/
                break;

                default:
                {
                  LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
                }
              }
              break;
            } /* RR_SGLTE_BAND_COEX_IMSG */

            default:
            {
              LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
            }
          }
          break;
        } /* MS_RR_RR */

        default:
        {
          LOG_UNEXPECTED_MESSAGE(message, rr_sglte_band_coex_state_name(rr_sglte_band_coex_data_ptr->state));
        }
      }
      break;
    } /* RR_SGLTE_BAND_COEX_STATE_CONNECTED_B39_DISABLED_S3C */

    default:
    {
      MSG_GERAN_ERROR_1_G("Unexpected state %d, resetting to NULL", rr_sglte_band_coex_data_ptr->state);
      rr_sglte_band_coex_data_ptr->state = RR_SGLTE_BAND_COEX_STATE_NULL;
    }
  }

#ifdef DEBUG_RR_TRACE_MSG
  rr_store_trace_msg_buf(RR_SGLTE_BAND_COEX_SM, 
                         EV_INPUT_MESSAGE,
                         (byte) rr_sglte_band_coex_data_ptr->state,
                         message,
                         gas_id);
#endif

  if (rr_sglte_band_coex_data_ptr->state != rr_sglte_band_coex_data_ptr->old_state)
  {
    RR_LOG_STATE_TRANSITION("rr_sglte_band_coex_control",
                            rr_sglte_band_coex_data_ptr->old_state,
                            rr_sglte_band_coex_data_ptr->state,
                            rr_sglte_band_coex_state_name,
                            gas_id);
    
    rr_sglte_band_coex_data_ptr->old_state = rr_sglte_band_coex_data_ptr->state;
  }
}

boolean rr_sglte_band_coex_scan_arfcn(ARFCN_T ARFCN, const gas_id_t gas_id)
{
  boolean scan_arfcn = TRUE;

  if (rr_is_sglte_mode_active(gas_id)           &&
      rr_sglte_band_coex_b3_is_disabled(gas_id) &&
      (ARFCN.band == SYS_BAND_DCS_1800))
  {
    MSG_GERAN_HIGH_1_G("SGLTE: Skipping ARFCN %d, B39 co-ex, ARFCN in 1800 band!",ARFCN.num);
    scan_arfcn = FALSE;
  }

  return scan_arfcn;
}


boolean rr_sglte_band_coex_b3_is_disabled(const gas_id_t gas_id)
{
  rr_sglte_band_coex_data_t *rr_sglte_band_coex_data_ptr = rr_sglte_get_coex_data_ptr(gas_id);
  boolean                    b3_disabled = FALSE;

  RR_NULL_CHECK_FATAL(rr_sglte_band_coex_data_ptr);

  if (rr_sglte_band_coex_data_ptr->state == RR_SGLTE_BAND_COEX_STATE_IDLE_CS_ONLY_B3_DISABLED_S2 ||
      rr_sglte_band_coex_data_ptr->state == RR_SGLTE_BAND_COEX_STATE_CONNECTED_B3_DISABLED_S2C)
  {
    b3_disabled = TRUE;
  }

  return b3_disabled;
}

#endif /* FEATURE_SGLTE */
