#ifndef L1_OS_H
#define L1_OS_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                  L1   OPERATING SYSTEM SIGNALS
                       H E A D E R   F I L E

DESCRIPTION
   Layer 1 Rex Signal definitions

EXTERNALIZED FUNCTIONS

Copyright (c) 2002-2014 Qualcomm Technologies, Inc.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gl1/inc/l1_os.h#1 $
$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who      what, where, why
--------   -------- ---------------------------------------------
28/11/13   cja       CR521061 Remove unused RF API and mainline FEATURE_GSM_RFA_TASK.
16/09/13   cs        CR503042 Updated XO RGS handling for Triton
14/09/13   ap/aga    CR545423 - G2W Tune Away Feature
21/06/13   cs        Major Triton TSTS Syncup
25/03/13   mc        CR464496 : COEX Support - GERAN arbitrator SW CXM integration
19/06/12   cs        CR370453 Use the correct wait sig for the TRM/TCXO Manager timeouts
25/05/12   pg        Add support for RF Task
18/05/12   ky        Async message router Interface Changes between SW & FW
10/02/12   ws        CR 36194 Enable TD-SCDMA IRAT for GERAN
21/01/11   cs        Remove NCELL power monitors signal
29/11/10   og        Adding G2L support.
28/04/10   nt        CR234959 Optimisation to NCELL power monitors in the idle mode
10/05/07   cs        Frequency error update changes for Aries GPS
06/01/07   cs        Remove common signal definitions
10/11/06   agv       Update for 7200A TRM and TCXO Mgr3.0
26/09/06   agv       Initial Version.

===========================================================================*/


/*===========================================================================

                DEFINITIONS AND DECLARATIONS

===========================================================================*/



/*===========================================================================

                         REX SIGNALS

===========================================================================*/

/* A command is available in the queue. */
#define L1_CMD_Q_SIG                 0x0001
/* Used to signal that  WD rpt is reqd. */
#define L1_RPT_TIMER_SIG             0x0002

/* Used by the HW module to signal rdy. */
#define L1_WAIT_HW_SIG               0x0004
/* MDSP download timeout signal */
#define L1_TIMEOUT_HW_SIG            0x0008

#include "geran_variation.h"
#ifdef FEATURE_GSM_ISR_WATCHDOG
#define L1_ISR_WATCHDOG_TIMER_SIG    0x0010
#endif
#ifdef  FEATURE_GSM_COEX_SW_CXM
#define GL1_CXM_MSGR_SIG 0x0020
#endif
#ifdef  FEATURE_GSM_ASYNC_FW_INTF
#define GL1_GFW_ASYNC_INTF_MSG_ROUTER_SIG   0x0040
#endif
#define GL1_RF_TASK_MSGR_SIG          0x0080
#ifdef FEATURE_GSM_GPRS_L1
/*  Used by isr to signal task  */
#define SERV_MEAS_ISR_SIG            0x0100
#define GPL1_WAIT_FOR_ISR_TO_GET_FN  0x0200

/* This is in gl1rlc.h so be careful        */
/* #define RLC_PH_DATA_REQ_SIG       0x0400 */
#endif

#if defined(FEATURE_GSM_TO_LTE) || defined(FEATURE_GSM_TDS)
#define GL1_MSG_ROUTER_SIG           0x8000
#endif

/* CM task signals that it's running (so gsm_diag fn that calls it can safely be registered) */
#define CM_TASK_STARTED_SIG          0x10000
/*===========================================================================

                EXTERN DEFINITIONS AND DECLARATIONS

===========================================================================*/

/* Used when waiting for confirmations from TRM */
#if defined (FEATURE_GSM_TRM) || defined (FEATURE_GSM_TCXOMGR30)
extern rex_timer_type  gsm_l1_timeout_timer[];
#endif

#endif /* L1_OS_H */
