#===============================================================================
#
#       G E R A N    G D I A G   S C O N S   C O N F I G U R A T I O N
#
# GENERAL DESCRIPTION
#    SCons build script for the GDIAG VU
#
# Copyright (c) 2010-2013 Qualcomm Technologies, Inc.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gdiag/build/gdiag.scons#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 20100906   tjw     New file
#
#===============================================================================
import sys

Import('env')
env = env.Clone()

# If USES_WPLT is set this library is required
if 'USES_WPLT' not in env:
  # Otherwise verify that USES_GSM or USES_UMTS is set, if not bail out now
  if 'USES_GSM' not in env and 'USES_UMTS' not in env:
    # Tell cleanpack to delete all files (not just private ones) before shipping
    env.CleanPack('CLEANPACK_TARGET', env.FindFiles('*', '..'))
    Return()

################################################################
# SIMPLE CONFIGURABLE ITEMS:
#-------------------------------------------------------------------------------
# Name of the subsystem to which this unit belongs
#-------------------------------------------------------------------------------
SU_NAME = 'GERAN'

#-------------------------------------------------------------------------------
# Name of this unit
#-------------------------------------------------------------------------------
UNIT_NAME = 'GDIAG'

#-------------------------------------------------------------------------------
# Source path
#-------------------------------------------------------------------------------
# The path to the source files
SRCPATH = '../src'

#-------------------------------------------------------------------------------
# Source files
#-------------------------------------------------------------------------------
# The source files. Either '*.c' or list the files individually.
# If listing the files individually, include any private files, they will be stripped later
# e.g. SRC_FILES = ['foo.c', 'bar.c']
SRC_FILES = ['umtsdiag.c']
if not env.has_key('USES_WPLT'):
    SRC_FILES.append('gsmdiag.c')

PRIVATE_SRC_FILES = []


#-------------------------------------------------------------------------------
# Required internal APIs
#-------------------------------------------------------------------------------
REQUIRED_SU_APIS = [
    'GCOMMON',
    'GDIAG',
    'GDRIVERS',
    'GL1',
    'GL2',
    'GLLC',
    'GMAC',
    'GMDSP',
    'GRLC',
    'GRR',
    'GSNDCP'
    ]

#-------------------------------------------------------------------------------
# Images the SU will be loaded into (MODEM_APPS and/or MODEM_MODEM)
#-------------------------------------------------------------------------------
IMAGES = ['MODEM_MODEM', 'MOB_GERAN']

################################################################
################################################################



################################################################
# UNIT-SPECIFIC SCONS MAGIC
################################################################
# (for build specialists to play their tricks)

################################################################
# DATA ASSEMBLED, PUT IT INTO ACTION USING THE SHARED
# FUNCTIONS IN build/geran_build_tools.py
################################################################
sys.path.append('${GERAN_BUILD_TOOLS}')
import geran_build_tools

geran_build_tools.set_up_component_build_config(
  env,
  SU_NAME=                  SU_NAME,
  UNIT_NAME=                UNIT_NAME,
  SRCPATH=                  SRCPATH,
  SRC_FILES=                SRC_FILES,
  PRIVATE_SRC_FILES=        PRIVATE_SRC_FILES,
    REQUIRED_SU_APIS=         REQUIRED_SU_APIS,
  IMAGES=                   IMAGES
)

env.LoadSoftwareUnits()
