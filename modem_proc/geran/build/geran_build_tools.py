#===============================================================================
#
#       G E R A N    C O M M O N   S C O N S   C O N F I G U R A T I O N
#
# GENERAL DESCRIPTION
#    General SCons magic shared by all GERAN components
#
# Copyright (c) 2013 Qualcomm Technologies, Inc.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/build/geran_build_tools.py#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 20130204   tjw     New file factored out of component SCons file
#
#===============================================================================

from glob import glob
from os.path import join, basename, exists
import os, sys

#-------------------------------------------------------------------------------

def build_variation(env, variation):
  """
  This function handles all build variations found in different baselines and
  build environments (compilers)
  """
  #-------------------------------------------------------------------------------
  def _PL_check(env, PL_list):
    """
    Return True if environment variable 'PRODUCT_LINE' matches any of 'PL_list'
    """
    try:
      for PL in PL_list:
        if env['PRODUCT_LINE'].find(PL) != -1:
          if debug:
            env.PrintDebugInfo('geran', 'GeRaN %s matched %s ' % (env['PRODUCT_LINE'], PL))
          return True
      if env['PRODUCT_LINE'] == '':
        env.PrintError('!!!! ERROR: Environment variable PRODUCT_LINE is blank. GERAN build errors probable !!!!')
        env.PrintError('!!!! ERROR: QUITTING BUILD !!!!')
        sys.exit(98)
      if debug:
        env.PrintDebugInfo('geran', 'GeRaN %s did not match %s ' % (env['PRODUCT_LINE'], PL))
    except:
      env.PrintError('!!!! ERROR: Environment variable PRODUCT_LINE is not defined. GERAN build errors probable !!!!')
      env.PrintError('!!!! ERROR: QUITTING BUILD !!!!')
      sys.exit(99)
    return False
  #-------------------------------------------------------------------------------
  def _version_tuple(v):
    """
    convert version number x.y.z into a tuple so it can be compared
    """
    try:  # To cope with 5.1_hw_errata.2
      result = tuple(map(int, (v.split('.'))))
    except:
      result = [5, 1, 2]
    return result
  #-------------------------------------------------------------------------------

  # If the build command line USES switch SCONS_DEBUG_GERAN is defined
  debug = ('SCONS_DEBUG_GERAN' in env )

  if _PL_check(env, ['MPSS.NI.', \
                     'MPSS.DI.1.0', 'MPSS.DI.2.0', 'MPSS.DI.2.1', \
                     'MPSS.TR.1', 'MPSS.TR.2', 'MPSS.TR.3']):
    env.PrintError('!!!! ERROR: This Product Line is not supported by this version of the GERAN build files. !!!!')
    env.PrintError('!!!! ERROR: GERAN build errors probable !!!!')
    env.PrintError('!!!! ERROR: QUITTING BUILD !!!!')
    sys.exit(99)

  if variation == 'API_VIOLATIONS_AS_ERRORS':
    # Report API violations as warnings vs. errors
    result = not (_PL_check(env, ['MPSS.BO.', 'MPSS.JO.', 'MPSS.TH.', 'MPSS.TA.']) )

  elif variation == 'API_ALLOWED_geran_variation.h':
    # API allowed to include geran_variation.h for FEATUREs
    result = (_PL_check(env, ['MPSS.BO.1']) )

  elif variation == 'GERAN_INTERNAL_APIS':
    # Protocol and L1 have APIs for each other
    result = env.PathExists('${GERAN_ROOT}/gprotocol')

  elif variation == 'MSDEV_COMPILER':
    # Building with the Visual C compiler vs. some flavour of gcc / LLVM
    result = (env['PROC'] in ['msvc'] )

  elif variation == 'LLVM_COMPILER':
    # Building with the LLVM compiler
    if build_variation(env, 'MSDEV_COMPILER'):
      result = False
    else:
      result = (os.environ.get('HEXAGON_RTOS_RELEASE').startswith('6') )
      # or _version_tuple(os.environ.get('HEXAGON_RTOS_RELEASE')) >= _version_tuple('6.0.0')

  elif variation == 'LLVM_COMPILER_WARNING_LEVEL':
    # Phasing the clean up of warnings, different PLs can be a different points
    if build_variation(env, 'LLVM_COMPILER'):
      result = 5
      if _PL_check(env, ['MPSS.DI.3']):
        result = 4
      if _PL_check(env, ['MPSS.DPM.1.']):
        result = 4
      if _PL_check(env, ['MPSS.DI.4', 'MPSS.DPM.2.']):
        result = 2.5  # L3 warnings are errors but don't turn on L2 warnings
      if _PL_check(env, ['MPSS.BO.2', 'MPSS.JO.1.', 'MPSS.TH.1.', 'MPSS.TA.']):
        result = 1
    else:
      result = 0

  elif variation == 'COMPILER_WARNINGS_AS_ERRORS':
    # Report warnings as errors
    # When using the GNU compiler
    if build_variation(env, 'MSDEV_COMPILER'):
      result = False
    # DPM which is using GNU compiler on files written under LLVM
    elif _PL_check(env, ['MPSS.DPM.']):
      result = False
    elif build_variation(env, 'LLVM_COMPILER'):
      # (except LLVM toolchain (6.x.x) during migration)
      result = False
    else:
      # Ensure that continuous build processes will reject code with warnings
      result = True

  elif variation == 'GERANFOLDER':
    # if the file geran\build\geranFolder.txt exists, build the GERAN files found there
    # (for development purposes only)
    result = ''
    try:
      geranFolderTxt = 'geranFolder.txt'
      fp = open(geranFolderTxt)
      geranFolder = fp.readline().strip()
      fp.close()
      env.PrintInfo('Building GERAN files in %s as requested by presence of %s' % \
        (geranFolder, os.path.abspath(geranFolderTxt)))
      if debug:
        env.PrintDebugInfo('geran', 'GeRaN building %s: ' % geranFolder)
      result = geranFolder
    except: # the file geran\build\geranFolder.txt doesn't exist, build the GERAN files normally
      pass

  elif variation == 'SCONS_DEBUG':
    # If the build command line USES switch SCONS_DEBUG_GERAN is defined
    result = ('SCONS_DEBUG_GERAN' in env )

  elif variation == 'DO_STATIC_ANALYSIS':
    # If the build command line USES switch STATIC_ANALYSIS_GERAN is defined
    result = ('STATIC_ANALYSIS_GERAN' in env )

  elif variation == 'PREPROCESS_GERAN':
    # If the build command line USES switch PREPROCESS_GERAN is defined
    if not build_variation(env, 'MSDEV_COMPILER'):
      # and using GCC / LLVM
      result = ('PREPROCESS_GERAN' in env )
    else:
      result = False

  elif variation == 'COMPILER_HAS_STATIC_ANALYSIS':
    # The LLVM compiler has Static Analysis
    result = (_version_tuple(os.environ.get('HEXAGON_RTOS_RELEASE')) >= _version_tuple('6.2.05') )

  elif variation == 'NUMBER_OF_SUBS':
    # Return the number of subscriptions (SIMs) supported
    subs = 1
    if 'USES_TSTS' in env:
      subs = 3
    elif 'USES_DUAL_SIM' in env:
      subs = 2
    result = subs

  elif variation == 'DO_EXTERNS_VIOLATIONS_CHECK':
    # If the build command line USES switch EXTERNS_CHECK_GERAN is defined
    result = ('EXTERNS_CHECK_GERAN' in env )

  elif variation == 'EXTERNS_VIOLATIONS_AS_ERRORS':
    # Report externs in .c files as errors
    result = False

  elif variation == 'DO_FEATURE_VIOLATIONS_CHECK':
    # If the build command line USES switch FEATURE_CHECK_GERAN is defined
    result = ('FEATURE_CHECK_GERAN' in env )

  elif variation == 'FEATURE_VIOLATIONS_AS_ERRORS':
    # Report unlisted FEATUREs in files as errors
    result = False

  else:
    env.PrintError('!!! GERAN SCONS ERROR: unknown build variation "%s"' % variation)
    return False

  if debug:
    env.PrintDebugInfo('geran', 'GeRaN %s: ' % variation, result)
  return result

#-------------------------------------------------------------------------------
def setup_static_analysis(env, unit):
  """
  Add command line switches for LLVM static analyser if requested
  """
  if build_variation(env, 'DO_STATIC_ANALYSIS') \
  and build_variation(env, 'COMPILER_HAS_STATIC_ANALYSIS'):
    SA_folder = os.path.join(env.subst("${GERAN_ROOT}"), '_static_analysis', unit)
    # Create the folder
    if not os.path.exists(SA_folder):
      os.makedirs(SA_folder)
    env.Append(CFLAGS = ' --compile-and-analyze %s' % SA_folder)
    env.PrintInfo('Performing static analysis on %s' % unit)

#-------------------------------------------------------------------------------

def set_up_component_build_config(
  env,
  SU_NAME,
  UNIT_NAME,
  SRCPATH,
  SRC_FILES,
  PRIVATE_SRC_FILES,
  REQUIRED_SU_APIS,
  IMAGES,
  MSG_BT_SSID_DFLT = None,                # default is 'MSG_SSID_GSM_GPRS_' + UNIT_NAME
  DELIVER_SOURCE = True,                  # False => deliver as a binary to some customers
  PUBLISHED_HDR_FILES = [],               # List of the files not to be cleaned by cleanpack
  UNIT_TEST = False,
  CMI4_VIOLATIONS = [],                   # Work round CMI4 rules where necessary for unit test
  debug = False
):
  if not debug:
    # if it's not set locally, see if it's set globally
    debug =  build_variation(env, 'SCONS_DEBUG')

  if debug:
    env.PrintDebugInfo('geran', 'GeRaN SU_NAME %s' %                  SU_NAME)
    env.PrintDebugInfo('geran', 'GeRaN UNIT_NAME %s' %                UNIT_NAME)
    env.PrintDebugInfo('geran', 'GeRaN SRCPATH %s' %                  SRCPATH)
    env.PrintDebugInfo('geran', 'GeRaN SRC_FILES %s' %                SRC_FILES)
    env.PrintDebugInfo('geran', 'GeRaN PRIVATE_SRC_FILES %s' %        PRIVATE_SRC_FILES)
    env.PrintDebugInfo('geran', 'GeRaN REQUIRED_SU_APIS %s' %         REQUIRED_SU_APIS)
    env.PrintDebugInfo('geran', 'GeRaN IMAGES %s' %                   IMAGES)
    env.PrintDebugInfo('geran', 'GeRaN MSG_BT_SSID_DFLT %s' %         MSG_BT_SSID_DFLT)
    env.PrintDebugInfo('geran', 'GeRaN DELIVER_SOURCE %s' %           DELIVER_SOURCE)
    env.PrintDebugInfo('geran', 'GeRaN PUBLISHED_HDR_FILES %s' %      PUBLISHED_HDR_FILES)
    env.PrintDebugInfo('geran', 'GeRaN UNIT_TEST %s' %                UNIT_TEST)
    env.PrintDebugInfo('geran', 'GeRaN CMI4_VIOLATIONS %s' %          CMI4_VIOLATIONS)

  #-------------------------------------------------------------------------------
  # Setup source PATH
  #-------------------------------------------------------------------------------
  env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

  #-------------------------------------------------------------------------------
  # Set MSG_BT_SSID_DFLT for legacy MSG macros
  #-------------------------------------------------------------------------------
  if MSG_BT_SSID_DFLT == None:  # Not specified => follows the naming convention
    env.Append(CPPDEFINES = [
       'MSG_BT_SSID_DFLT=MSG_SSID_GSM_GPRS_' + UNIT_NAME,
    ])
  else:
    env.Append(CPPDEFINES = [
       'MSG_BT_SSID_DFLT=' + MSG_BT_SSID_DFLT,
    ])

  """
  API terminology:
  Public        visible to other AUs
  Restricted    visible to other SUs
  Protected     visible only within the SU
  """

  #-------------------------------------------------------------------------------
  # Necessary Public APIs
  #-------------------------------------------------------------------------------
  if UNIT_TEST:
    ################################################################
    # Work round CMI4 rules where necessary for unit test
    ################################################################
    # Handle source trees that have modem/ and those that don't
    if env.PathExists('${INC_ROOT}/modem/geran'):
      modemRoot = '${INC_ROOT}/modem'
    else:
      modemRoot = '${INC_ROOT}'
    for incpath in CMI4_VIOLATIONS:
      env.PublishPrivateApi('VIOLATIONS', [join(modemRoot, incpath)])


  if debug or 'MOB_GERAN' in env:
    env.RequirePublicApi(['QTF'], area='MOB') # QTF comes from MOB area in no-modem-dir
    env.RequireRestrictedApi(['QTF', 'MCS_OFFTARGET']) # older MOBs
    env.RequireProtectedApi(['GL1TEST'])
    env.RequireProtectedApi(['GMDSP6TEST'])
    env.RequireProtectedApi(['GTEST'])

  # Need to get access to SU's own Public headers
  env.RequireRestrictedApi(SU_NAME)

  if UNIT_NAME != 'API' or build_variation(env, 'API_ALLOWED_geran_variation.h'):
    # The API must not access the SU's internal cust files
    # Require these for all components within the SU as they all have compile time
    # variation.  In the future this can be pushed down to the component .scons
    # files only for components that have compile time variation.
    env.RequireProtectedApi(['GERAN'])

    # The API must not access the SU's internal inc files either
    if build_variation(env, 'GERAN_INTERNAL_APIS'):
      # separate API for L1 and Protocol
      L1_UNITS =       ['GDRIVERS', 'GL1', 'GMDSP', 'GPLT']
      PROTOCOL_UNITS = ['GCOMMON', 'GDIAG', 'GL2', 'GLLC', 'GMAC', 'GRLC', 'GRR', 'GSNDCP']

      if UNIT_NAME in L1_UNITS:
        env.RequireProtectedApi([
        'GDRIVERS',
        'GTRACER_IDS',
        'GL1',
        'GMDSP',
        'GPLT',
        'GMSGRIF',
        'GERAN_COMMON',
        'GERAN_PROTOCOL',
        'GERAN_L1'
        ]
        )

      if UNIT_NAME in PROTOCOL_UNITS :
        env.RequireProtectedApi([
        'GCOMMON',
        'GDIAG',
        'GL2',
        'GLLC',
        'GMAC',
        'GRLC',
        'GRR',
        'GSNDCP',
        'GPLT',
        'GERAN_COMMON',
        'GERAN_PROTOCOL',
        'GERAN_L1'
        ]
        )
    else:
        # All GERAN units access all internal header files
        env.RequireProtectedApi([
        'GCOMMON',
        'GDIAG',
        'GDRIVERS',
        'GTRACER_IDS',
        'GL1',
        'GL2',
        'GLLC',
        'GMAC',
        'GMDSP',
        'GRLC',
        'GRR',
        'GSNDCP',
        'GPLT',
        'GMSGRIF']
        )

  # Now add the include paths outside GERAN
  # The order of APIs in the geran_public_api_list determines the order the paths
  # are listed on the build command line.
  # To optimize build times the API list is sorted in descending order of use.

  geran_public_api_list = [
        # api_area              api_name
        ('CORE',                'KERNEL'),
        ('GERAN',               'GERAN'),
        ('MMCP',                'MMCP'),
        ('CORE',                'DAL'),
        ('MCS',                 'MCS'),
        ('RFA',                 'COMMON'),
        ('CORE',                'SYSTEMDRIVERS'),
        ('LTE',                 'LTE'),
        ('MMCP',                'PUBLIC'),
        ('CORE',                'MEMORY'),
        ('CORE',                'DEBUGTOOLS'),
        ('RFA',                 'TDSCDMA'),
        ('RFA',                 'CDMA'),
        ('RFA',                 'MEAS'),
        ('RFA',                 'GSM'),
        ('AVS',                 'MVS'),
        ('RFA',                 'WCDMA'),
        ('TDSCDMA',             'TDSCDMA'),
        ('CORE',                'MPROC'),
        ('WCDMA',               'WCDMA'),
        ('CORE',                'POWER'),
        ('ONEX',                'PUBLIC'),
        ('CORE',                'BUSES'),
        ('UIM',                 'UIM'),
        ('UIM',                 'PUBLIC'),
        ('RFA',                 'GNSS'),
        ('CORE',                'SERVICES'),
        ('GERAN',               'PUBLIC'),
        ('RFA',                 'LTE'),
        ('WCDMA',               'PUBLIC'),
        ('FW',                  'RF'),
        ('RFA',                 'LM'),
        ('RFLM',                'DTR'),
        ('FW',                  'GERAN'),
        ('FW',                  'TARGET'),
        ('FW_CCS',              'FW_CCS'),
        ('FW',                  'COMMON'),
        ('FW_GERAN',            'FW_GERAN'),
        ('MPOWER',              'MPOWER'),
        ('MCS',                 'PUBLIC'),
        ('DATAMODEM',           'PUBLIC'),
        ('GPS',                 'GPS'),
        ('FW',                  'WCDMA'),
        ('FW_WCDMA',            'FW_WCDMA'),
        ('UTILS',               'OSYS'),
        ('DATAMODEM',           'DATAMODEM'),
        ('CORE',                'STORAGE'),
        ('UTILS',               'A2'),
        ('AVS',                 'VS'),
        ('AVS',                 'MVS'),
        ('AVS',                 'MMUTILS'),
        ('CORE',                'SECUREMSM'),
        ('CORE',                'DEBUGTRACE'),
        ('MCFG',                'MCFG'),
        ('RFLM',                'RFLM'),
        ]

  for api_area,api_name in geran_public_api_list:
      env.RequirePublicApi([api_name], area=api_area)


  #-------------------------------------------------------------------------------
  # Generate the library and add to an image
  #-------------------------------------------------------------------------------

  if SRC_FILES == '*.c' or SRC_FILES == '*.c*':
    # Construct the list of source files by looking for *.c
    SOURCES = ['${BUILDPATH}/' + basename(fname)
      for fname in glob(join(env.subst(SRCPATH), SRC_FILES))]
    if debug:
      env.PrintDebugInfo('geran', 'GeRaN SOURCES')
      env.PrintDebugInfo('geran',  SOURCES)
  else:
    # Construct the list of source files from the given list
    SOURCES = []
    for fname in SRC_FILES:
      if exists(join(env.subst(SRCPATH), fname)):
        SOURCES.append('${BUILDPATH}/' + basename(fname))

  for private_file in PRIVATE_SRC_FILES:
    private_fname = '${BUILDPATH}/' + basename(private_file)
    if SOURCES.count(private_fname) > 0:    # the private source file is in the list
      SOURCES.remove(private_fname)

  # Construct the list of private source files from the given list
  PRIVATE_SOURCES = []
  for fname in PRIVATE_SRC_FILES:
    PRIVATE_SOURCES.append('${BUILDPATH}/' + basename(fname))

  setup_static_analysis(env, UNIT_NAME.lower())

  if UNIT_TEST:
    # UT's must be added as objects
    env.AddObject(['MOB_GERAN_UT'], SOURCES)
  else:
    # Add our library to the ModemApps image
    if DELIVER_SOURCE:
      # Source for this component is delivered to customer
      env.AddLibrary( IMAGES, join('${BUILDPATH}', '%s_%s' % (SU_NAME, UNIT_NAME)), [SOURCES] )
    else:
      # This component is delivered as a binary
      env.AddBinaryLibrary( IMAGES, join('${BUILDPATH}', '%s_%s' % (SU_NAME, UNIT_NAME)), [SOURCES],
                            pack_exception=['USES_COMPILE_TDSCDMA_AND_ALL_L1_PROTECTED_LIBS',
                                            'USES_COMPILE_L1_OPT_AC_PROTECTED_LIBS'] )
      # Construct the list of local header files by looking for *.h
      LOCAL_HEADERS = ['${BUILDPATH}/' + basename(fname)
        for fname in glob(join(env.subst(SRCPATH), '*.h'))]
      if debug:
        env.PrintDebugInfo('geran', 'GeRaN LOCAL_HEADERS')
        env.PrintDebugInfo('geran', LOCAL_HEADERS)
      env.CleanPack("CLEANPACK_TARGET", LOCAL_HEADERS,
                    pack_exception=['USES_COMPILE_TDSCDMA_AND_ALL_L1_PROTECTED_LIBS'])

    # Some source is never delivered
    if PRIVATE_SOURCES != []:
      env_private = env.Clone()
      env_private.Append(CPPDEFINES = [
        'FEATURE_LIBRARY_ONLY'
      ])
      env_private.AddBinaryLibrary( IMAGES, join('${BUILDPATH}', 'lib_%s_%s' %
                                    (SU_NAME, UNIT_NAME)), [PRIVATE_SOURCES],
                                    pack_exception=['USES_CUSTOMER_GENERATE_LIBS',
                                                    'USES_COMPILE_L1_OPT_AC_PROTECTED_LIBS'] )

    # extra cleanpack processing over and above that dealt with by "pack_exception"
    if PUBLISHED_HDR_FILES != []:
      # Construct the list of header files by looking for *.h
      HEADERS = ['${BUILDPATH}/' + basename(fname)
        for fname in glob('../inc/*.h')]
      # and removing the "white list" files (using set subtraction)
      PUBLISHED_HEADERS = ['${BUILDPATH}/' + basename(fname)
        for fname in PUBLISHED_HDR_FILES]
      HDR_SET = set(HEADERS) - set(PUBLISHED_HEADERS)
      HEADERS = list(HDR_SET)
      if debug:
        env.PrintDebugInfo('geran', 'GeRaN HEADERS')
        env.PrintDebugInfo('geran', HEADERS)
      env.CleanPack("CLEANPACK_TARGET", HEADERS,
                    pack_exception=['USES_COMPILE_TDSCDMA_AND_ALL_L1_PROTECTED_LIBS'])

################################################################
# Set up the Run Control Initialisation (RCInit) data to define
# the task(s) that controls this unit (if there is one).
################################################################

def set_up_component_tasks(
  env,
  IMAGES,
  INITFUNC_PARAMETER_DICTIONARIES = None,   # old style explicit list of dictionaries
  INITTASK_PARAMETER_DICTIONARIES = None    # list of dictionaries
  ):
  #-------------------------------------------------------------------------------
  # Generate the RCInit tables
  #-------------------------------------------------------------------------------

  if 'USES_MODEM_RCINIT' in env:
    num_subs = build_variation(env, 'NUMBER_OF_SUBS')

    sub = 1
    for INITFUNC_PARAMETER_DICTIONARY in INITFUNC_PARAMETER_DICTIONARIES:
      # init function descriptor : add to specific build products
      env.AddRCInitFunc(IMAGES, INITFUNC_PARAMETER_DICTIONARY)
      sub += 1
      if sub > num_subs:  # Only do as many as there are subscriptions in this build.
        break

    sub = 1
    for INITTASK_PARAMETER_DICTIONARY in INITTASK_PARAMETER_DICTIONARIES:
      # task descriptor : add to specific build products
      env.AddRCInitTask(IMAGES, INITTASK_PARAMETER_DICTIONARY)
      sub += 1
      if sub > num_subs:
        break

