#===============================================================================
#
# GERAN Subsystem SCons build script
#
# Copyright (c) 2010-2014 Qualcomm Technologies, Inc.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/build/geran.scons#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 20100906   tjw     Initial revision
#
#===============================================================================
import os, sys, glob

Import('env')

# GERAN_ROOT will already defined if this is a development build
# using GERANFOLDER (see below) in a parent build. Normally it won't.
if not env.PathExists('${GERAN_ROOT}'):
  # (No source trees have modem/ anymore)
  env.Replace(GERAN_ROOT = '${INC_ROOT}/geran')

env.Replace(GERAN_BUILD_TOOLS = '${GERAN_ROOT}/build')
sys.path.append('${GERAN_BUILD_TOOLS}')
import geran_build_tools

geranFolder = geran_build_tools.build_variation(env, 'GERANFOLDER')
if geranFolder != '':
  # build the GERAN files specified
  # (for development purposes only)
  env.Replace(GERAN_ROOT = geranFolder)
  env.Print('')
  env.Print('')
  env.PrintStageBanner('>>>>>> GERAN BUILDING FOLDER %s <<<<<<<' % geranFolder)
  env.Print('')
  env.Print('')
  #for more visibility, change the title. This may not work on Linux but
  #it's only for development.
  os.system('title ********** GERAN BUILDING FOLDER %s **********' % geranFolder)
  env.LoadSoftwareUnits(dir_patterns = ['%s/build' % geranFolder])
else: # build the GERAN files normally

  """
  API terminology:
  Public        visible to other AUs
  Restricted    visible to other SUs
  Protected     visible only within the SU
  """

  # Publish GERAN's internal header file paths for internal use
  # Separate L1 and Protocol internal APIs
  env.PublishProtectedApi('GERAN_COMMON', ["${GERAN_ROOT}/gcommon/gapi"])
  env.PublishProtectedApi('GERAN_PROTOCOL',["${GERAN_ROOT}/gprotocol/gapi"])
  env.PublishProtectedApi('GERAN_L1',     ["${GERAN_ROOT}/gl1/gapi"])
  # Protocol only
  env.PublishProtectedApi('GCOMMON',      ["${GERAN_ROOT}/gcommon/inc"])
  env.PublishProtectedApi('GDIAG',        ["${GERAN_ROOT}/gdiag/inc"])
  env.PublishProtectedApi('GL2',          ["${GERAN_ROOT}/gl2/inc"])
  env.PublishProtectedApi('GLLC',         ["${GERAN_ROOT}/gllc/inc"])
  env.PublishProtectedApi('GMAC',         ["${GERAN_ROOT}/gmac/inc"])
  env.PublishProtectedApi('GRLC',         ["${GERAN_ROOT}/grlc/inc"])
  env.PublishProtectedApi('GRR',          ["${GERAN_ROOT}/grr/inc"])
  env.PublishProtectedApi('GSNDCP',       ["${GERAN_ROOT}/gsndcp/inc"])
  # L1 only
  env.PublishProtectedApi('GDRIVERS',     ["${GERAN_ROOT}/gdrivers/inc"])
  # The generated gdrivers_tracer_event_ids.h must go into BUILDPATH along with
  # the generated .o and .lib files. Set the include path for it.
  # Add it explicitly otherwise it is not created unless the path already exists
  # (Understanding the reason requires deeper knowledge of SCons than I have)
  env.Append(CPPPATH = ["${GERAN_ROOT}/gdrivers/build/${BUILDPATH}"])
  env.PublishProtectedApi('GTRACER_IDS',  ["${GERAN_ROOT}/gdrivers/build/${BUILDPATH}"])
  env.PublishProtectedApi('GL1',          ["${GERAN_ROOT}/gl1/inc"])
  env.PublishProtectedApi('GMDSP',        ["${GERAN_ROOT}/gmdsp6/inc"])
  env.PublishProtectedApi('GPLT',         ["${GERAN_ROOT}/gl1/gplt/inc"])
  env.PublishProtectedApi('GMSGRIF',      ["${GERAN_ROOT}/gmsgrif/inc"])
  # Test directories may not be present
  env.PublishProtectedApi('GL1TEST',      ["${GERAN_ROOT}/gl1/test"])
  env.PublishProtectedApi('GMDSP6TEST',   ["${GERAN_ROOT}/gmdsp6/test"])
  env.PublishProtectedApi('GTEST',        ["${GERAN_ROOT}/test/inc"])


  # Add the GERAN API
  env.RequireRestrictedApi(['GERAN'])

  # cust file relocation
  # Protected is "private within the SU" vs. Restricted which is visible to other SUs
  env.PublishProtectedApi('GERAN', [
                                   '${GERAN_ROOT}/cust/inc',
                                   ])
  # /variation/inc/geran_variation.h is now internal, moved to cust/inc.
  # A null file /variation/inc/geran_variation.h has been added for other SUs that
  # reference it.

  #---------------------------------------------------------------------------
  # Load cleanpack script to remove private files etc. from customer builds.
  if os.path.exists('geran_cleanpack.py'):
     env.LoadToolScript('geran_cleanpack.py')

  if geran_build_tools.build_variation(env, 'PREPROCESS_GERAN'):
    # Preprocessor and assembler files can be found in build/ms
    env.Append(CFLAGS = ['-save-temps'])

  #---------------------------------------------------------------------------
  # Handling compiler warnings: specific to compiler and PL
  if geran_build_tools.build_variation(env, 'MSDEV_COMPILER'):
    # suppress annoyingly frequent msdev warnings
    # warning C4013: '....' undefined; assuming extern
    # warning C4018: '>' : signed/unsigned mismatch
    # warning C4200: nonstandard extension used : zero-sized array in struct/union
    #  That warning is still generated
    # warning C4244: '=' : conversion from '...' to '...'
    # Errors in LLVM:
    # warning 4090: 'operation' : different 'modifier' qualifiers
    # -wd Disables the compiler warning that is specified in n.
    # -wo Treats as an error the compiler warning that is specified in n.
    env.Append(CFLAGS = ' -wd4013 -wd4018 -wd4200 -wd4244 -we4090 ')

  else:
    if geran_build_tools.build_variation(env, 'COMPILER_WARNINGS_AS_ERRORS'):
      # This ensures that continuous build processes will reject code with warnings
      env.Append(CFLAGS = ' -Werror ')

    # for compatibility prevent variable declarations in code as the
    # MSDEV compiler does not permit them
    if geran_build_tools.build_variation(env, 'LLVM_COMPILER'):
      env.Append(CFLAGS = ' -Werror=c99-declaration-after-statement ')
    else:
      env.Append(CFLAGS = ' -Wdeclaration-after-statement ')
      # Add warnings generated by LLVM but not gcc
      env.Append(CFLAGS = ' -Wunused-function -Wunused-variable')

    # LLVM Static Analyser command switches are handled in geran_build_tools.py
    # so as to create a separate folder for each unit's reports

    if geran_build_tools.build_variation(env, 'LLVM_COMPILER'):
      warning_level = geran_build_tools.build_variation(env, 'LLVM_COMPILER_WARNING_LEVEL')
      # Corner case:
      if geran_build_tools.build_variation(env, 'PREPROCESS_GERAN'):
        # When detecting tautological comparisons, clang has a heuristic 
        # that suppresses the diagnostic if it originates from a macro
        # expansion. However when compiling from pre-processed source 
        # (which is a side-effect of -save-temps), that information is 
        # lost and the diagnostic is not suppressed.
        # Avoid that by making sure that diagnostic does not result in an error.
        # (Alternatively append --preprocess to the build command.)
        warning_level = 3
      # Highest numerical level cleaned up first. -Wno turns off a warning
      #
      # Escalation goes:
      #  1st: -Wno<warning>     Don't report it
      #  2nd: -W<warning>       Warn about it
      #  3rd: -Werror=<warning> Report it as an error (regardless of whether -Werror is set)
      #  Suppress, Warn, Error.
      #
      #  At any time  -Wno-error=<warning> stops it being reported as an error
      #               e.g. -Werror=medium -Wno-error=switch
      #

      warnings5 = ['medium', 'nested-externs'] # override the top-level -Wno-medium otherwise
                                               # implicit declarations are not reported
      warnings4 = ['parentheses-equality', 'tautological-compare', 'sometimes-uninitialized', 'uninitialized', 'switch']
      warnings3 = ['self-assign']
      warnings2 = ['unused-value', 'static-in-inline', 'ignored-attributes', 'int-conversion', 'all'] #diagpkt.h and log.h warning: '__packed__' attribute ignored
      warnings1 = ['unused-function', 'unused-variable', 'missing-braces', 'comment']
      warnings0 = ['enum-conversion', 'cast-align']
      warnings = [warnings0, warnings1, warnings2, warnings3, warnings4, warnings5]

      cflags = ' -Werror'         # Some warnings are not categorised so default to "error"
      for level in [5,4,3,2,1,0]: # later switches override earlier ones
                                  # so make sure it's -Wmedium -Wno-switch

        if level > warning_level: # we have warned about these, now report them as errors
          for warning in warnings[level]:
            cflags += ' -Werror=%s' % warning
        if level == warning_level: # we are warning about these (but not reporting as errors)
          for warning in warnings[level]:
            cflags += ' -W%s -Wno-error=%s' % (warning, warning)
        if level < warning_level: # we are not warning about these (and not reporting as errors)
          for warning in warnings[level]:
            cflags += ' -Wno-%s -Wno-error=%s' % (warning, warning)

      cflags += ' '
      env.Append(CFLAGS = cflags)
      # When we get to level 0 leave the remainder to the overall 'COMPILER_WARNINGS_AS_ERRORS'

  if geran_build_tools.build_variation(env, 'DO_EXTERNS_VIOLATIONS_CHECK'):
    extern_errors = geran_build_tools.build_variation(env, 'EXTERNS_VIOLATIONS_AS_ERRORS')

    #-------------------------------------------------------------------------------
    # Execute the script that finds extern declarations in .c files (if the script hasn't been stripped)
    #-------------------------------------------------------------------------------
    if os.path.exists('geran_static_analysis.py'):
      if not 'GERAN_EXTERNS_TEST_DONE' in os.environ:  # So the warnings are only reported once
        os.environ['GERAN_EXTERNS_TEST_DONE'] = 'Done'
        from geran_static_analysis import getListOfFiles, externsInSource, readFile

        dirs = glob.glob('../../geran/*')
        debug = False
        externWarns = True
        for dirname in dirs:
          src_folder = os.path.abspath(os.path.join(dirname, 'src'))
          dirname = os.path.basename(dirname)
          if os.path.exists(src_folder):
            files = getListOfFiles(src_folder)
            if debug:
              for fpath, fname in files:
                env.PrintInfo(fpath, fname)
            for fpath, fname in files:
              source = readFile(fpath)

              externs_o = externsInSource(source)
              externWarnings = externs_o.getWarnings()
              if len(externWarnings):
                externWarns = True
                for line, line_no, probable_extern, name in externWarnings:
                  env.PrintWarning('%s (%d): %s extern warning: %s' % (fpath, line_no, probable_extern, name))
                  env.PrintWarning(line)

        if externWarns:
          # The tests failed
          """
          If 'extern_errors' then report Error and stop the build rather than Warning
          """
          if extern_errors:
            env.PrintError('!! ERROR: %s externs test error(s)' % dirname.upper())
            Exit(1)   # Stop the build
          #else:
          #  env.PrintWarning('!! WARNING: %s externs test warning(s)' % dirname.upper())

  if geran_build_tools.build_variation(env, 'DO_FEATURE_VIOLATIONS_CHECK'):
    FEATURE_errors = geran_build_tools.build_variation(env, 'FEATURE_VIOLATIONS_AS_ERRORS')

    #-------------------------------------------------------------------------------
    # Execute the script that finds unlisted FEATUREs in source files (if the script hasn't been stripped)
    #-------------------------------------------------------------------------------
    if os.path.exists('geran_static_analysis.py'):
      if not 'GERAN_FEATURE_TEST_DONE' in os.environ:  # So the warnings are only reported once
        os.environ['GERAN_FEATURE_TEST_DONE'] = 'Done'
        from geran_static_analysis import getListOfFiles, FEATUREs, readFile

        dirs = glob.glob('../../geran/*')
        debug = False
        featureWarns = True
        for dirname in dirs:
          src_folder = os.path.abspath(os.path.join(dirname, 'src'))
          dirname = os.path.basename(dirname)
          if os.path.exists(src_folder):
            files = getListOfFiles(src_folder)
            if debug:
              for fpath, fname in files:
                env.PrintInfo(fpath, fname)
            for fpath, fname in files:
              source = readFile(fpath)

              features_o = FEATUREs(source)
              featureWarnings = features_o.getWarnings()
              if len(featureWarnings):
                featureWarns = True
                for line, line_no, feature in featureWarnings:
                  env.PrintWarning('%s (%d): non-permitted FEATURE warning: %s' % (fpath, line_no, feature))
                  env.PrintWarning(line)

        if featureWarns:
          # The tests failed
          """
          If 'feature_errors' then report Error and stop the build rather than Warning
          """
          if feature_errors:
            env.PrintError('!! ERROR: %s FEATURE test error(s)' % dirname.upper())
            Exit(1)   # Stop the build
          #else:
          #  env.PrintWarning('!! WARNING: %s FEATURE test warning(s)' % dirname.upper())

    else:
      env.PrintInfo(('(Note: geran_static_analysis.py not present.)'))

  if geran_build_tools.build_variation(env, 'SCONS_DEBUG'):
      env.PrintDebugInfo('geran', 'GeRaN CFLAGS',  env['CFLAGS'])

  env.LoadSoftwareUnits() # discover other .scons files

