#ifndef LLC_LOG_H
#define LLC_LOG_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            LLC Logging Facilities

GENERAL DESCRIPTION
   This module contains functions for logging LLC packets.

Copyright (c) 2002 by Qualcomm Technologies Incorporated. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gllc/src/gllclog.h#1 $

when       who      what, where, why
--------   ---      ----------------------------------------------------------
09/05/02   ADG     Initial Revision.
===========================================================================*/

#include "geran_variation.h"
#if defined(FEATURE_DIAG_GPRS)

#include "comdef.h" /* for types */
#include "gs.h"

/* INITIALISATION */
extern void llc_log_init( gas_id_t gas_id );

/* LOG PACKETS */
extern void llc_log_me_info(void);
extern void llc_log_sapi_states(uint8 sapi);
extern void llc_log_xid_info(uint8 sapi);
extern void llc_log_pdu_stats(uint8 sapi, uint8 zero_stats);
extern void llc_log_periodic_stats(uint8 sapi, uint8 zero_stats);
extern void llc_log_ota_message( gas_id_t     gas_id,
                                 boolean      downlink,
                                 uint8        message_type,
                                 uint16       message_length,
                                 const uint8 *message_ptr
                               );
extern void llc_log_ready_timer_status(void);
extern void llc_log_send_ui_frame(gas_id_t gas_id, uint16 seq_num, uint8 sapi, uint16 pdu_len, uint8 cipher_mode);
extern void llc_log_recv_ui_frame(gas_id_t gas_id, uint16 seq_num, uint8 sapi, uint16 pdu_len, uint8 cipher_mode);


/* EVENTS */
/* extern void llc_log_some_event(args); */

#else /* !defined(FEATURE_DIAG_GPRS) */

#define llc_log_init(a)                         /* */
#define llc_log_me_info(a)                      /* */
#define llc_log_sapi_states(a)                  /* */
#define llc_log_xid_info(a)                     /* */
#define llc_log_pdu_stats(a,b)                  /* */
#define llc_log_periodic_stats(a,b)             /* */
#define llc_log_ota_message(a,b,c,d,e)          /* */
#define llc_log_ready_timer_status()            /* */
#define llc_log_send_ui_frame(a,b,c,d,e);
#define llc_log_recv_ui_frame(a,b,c,d,e);

#endif /* defined(FEATURE_DIAG_GPRS) */


extern uint8 gllc_gs_queue[ NUM_GERAN_DATA_SPACES ];

#endif /* LLC_LOG_H */
