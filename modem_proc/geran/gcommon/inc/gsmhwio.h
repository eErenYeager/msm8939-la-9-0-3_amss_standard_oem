#ifndef __GSMHWIO_H__
#define __GSMHWIO_H__
/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/inc/gsmhwio.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who       what, where, why
--------   ---       --------------------------------------------------------
14/03/12   pg        Inital version

===========================================================================*/ 

#include "geran_variation.h"
#ifdef FEATURE_DIME_MODEM
#include "msm.h"
#else
#error LOCAL GSM HWIO MACROS NOT SUPPORTED FOR THIS TARGET
#endif

#endif
