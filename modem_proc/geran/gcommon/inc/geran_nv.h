#ifndef GERAN_NV_H
#define GERAN_NV_H

/*! \file geran_nv.h
 
  This is the header file for geran_nv.c.
  This module contains access functions for NV data.
 
                Copyright (c) 2012-2013 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/inc/geran_nv.h#2 $ */
/* $DateTime: 2017/08/20 23:30:54 $$Author: neelabhs $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "customer.h"
#include "comdef.h"
#include "sys.h"
#include "geran_dual_sim.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define GERAN_TA_SUPPORTED 1
#define GERAN_TA_NOT_SUPPORTED 0

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/*!
 * \brief Read general GERAN EFS-NV items.
 */
extern void geran_read_efs_nv_items(
  void
);

#ifdef FEATURE_DUAL_SIM
/*!
 * \brief Returns the value of EFS-NV item default_rf_chain.
 * 
 * \return int - {0,1} or -1 if not defined
 */
extern int geran_nv_get_default_rf_chain(
  void
);
#endif /*FEATURE_DUAL_SIM*/

#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
/*!
 * \brief Returns the g2w support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
extern int geran_nv_g2w_ta_supported(
  void
);
/*!
 * \brief Returns the g2t support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
extern int geran_nv_g2t_ta_supported(
  void
);

/*!
 * \brief Returns the g21x support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
extern int geran_nv_g21x_ta_supported(
  void
);

#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */

/*!
 * \brief Module initialisation function. Called during RR task initialisation.
 */
extern void geran_nv_init(
  void
);

extern boolean geran_ciphering_logging_enabled(gas_id_t gas_id);

#endif /* GERAN_NV_H */

/* EOF */

