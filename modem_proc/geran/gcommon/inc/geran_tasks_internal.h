#ifndef GERAN_TASKS_INTERNAL_H
#define GERAN_TASKS_INTERNAL_H
/*==============================================================================

                  G E R A N   T A S K   I N F O R M A T I O N

GENERAL DESCRIPTION

      This provides GERAN task infomation used within GERAN.

Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/inc/geran_tasks_internal.h#1 $

when       who    what, where, why
--------   ---    --------------------------------------------------------------
2012-04-24 tjw    New file
==============================================================================*/

/*==============================================================================
  Typedefs
==============================================================================*/

/*==============================================================================
  Constants and Macros
==============================================================================*/

/*==============================================================================

                    FUNCTION DECLARATIONS FOR MODULE

==============================================================================*/

/*==========================================================================

  FUNCTION    geran_tcb_set

  DESCRIPTION

    This function sets the tcb for the specified GERAN task

  PARAMETERS
  
    geran_task_id_t  task_id
    rex_tcb_type *tcb.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void geran_tcb_set(geran_task_id_t task_id, rex_tcb_type *tcb, void (*mutex_lock)(void),  void (*mutex_unlock)(void));
#endif
