#ifndef GERAN_TASKS_H
#define GERAN_TASKS_H
/*===========================================================================

          G S M   T A S K   F U N C T I O N S   H E A D E R   F I L E

GENERAL DESCRIPTION
  This file contains declarations for all the tasks in GERAN.

Copyright (c) 2009-2013 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/* <EJECT>^L */
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/inc/geran_tasks.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09-07-26   tjw     Initial revision.

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "rex.h"

/*===========================================================================

                    DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed to interface with the QDSP Services module.

===========================================================================*/

/*==============================================================================
  Typedefs
==============================================================================*/

/* These IDs are used by the task data access functions (geran_tcb_read etc.) */

typedef enum {
  GERAN_TASK_ID_GL1,
  GERAN_TASK_ID_GL1_2,
  GERAN_TASK_ID_GL1_3,
  GERAN_TASK_ID_GL2,
  GERAN_TASK_ID_GLLC_1,
  GERAN_TASK_ID_GLLC_2,
  GERAN_TASK_ID_GLLC_3,
  GERAN_TASK_ID_GMAC,
  GERAN_TASK_ID_GMAC2,
  GERAN_TASK_ID_GMAC3,
  GERAN_TASK_ID_GRLC_DL,
  GERAN_TASK_ID_GRLC_DL_2,
  GERAN_TASK_ID_GRLC_DL_3,
  GERAN_TASK_ID_GRLC_UL,
  GERAN_TASK_ID_GRLC_UL_2,
  GERAN_TASK_ID_GRLC_UL_3,
  GERAN_TASK_ID_GRR,
  GERAN_TASK_ID_GPLT,
  GERAN_TASK_ID_GL1_TCXO_TIMER,
  GERAN_TASK_ID_GL1_MSGR_IF_1,
  GERAN_TASK_ID_GL1_MSGR_IF_2,
  GERAN_TASK_ID_GL1_MSGR_IF_3,
  NUM_GERAN_TASK_IDS
} geran_task_id_t;

/*===========================================================================

                        PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

extern void gsm_l1_main   (uint32 rex_ignored);
extern void gsm_l2_main   (uint32 rex_ignored);
extern void rr_main       (uint32 rex_ignored);
extern void gprs_mac_task (uint32 rex_ignored);
extern void grlc_ul_task  (uint32 rex_ignored);
extern void grlc_dl_task  (uint32 rex_ignored);
extern void gllc_task     (uint32 rex_ignored);

/*==========================================================================

  FUNCTION    geran_tcb_read

  DESCRIPTION

    This function provides the tcb for the specified GERAN task

  PARAMETERS

    geran_task_id_t  task_id

  DEPENDENCIES
    None.

  RETURN VALUE
    rex_tcb_type *tcb.

  SIDE EFFECTS
    None.

===========================================================================*/
rex_tcb_type* geran_tcb_read(geran_task_id_t task_id);
#endif /* GERAN_TASKS_H */
