/*! \file geran_nv.c 
 
  @brief This module contains functionality related to GERAN NV items
    Create /nv/item_files/conf/geran.conf in EFS.
    The geran.conf file lists the EFS-NV items owned by GERAN.
    This file is used by tools such as QXDM to access the items, 
    and determines which items are included in QCN back-ups.
 
                Copyright (c) 2012-2013 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
*/
/* $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/src/geran_nv.c#3 $ */
/* $DateTime: 2017/08/20 23:30:54 $ */

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"
#include "comdef.h"
#include "geran_nv.h"
#include "geran_efs_nv_items.h"
#include "string.h"
#include <stringl.h>
#include "fs_public.h"
#include "msg.h"
#include "gprs_mem.h"
#include "geran_multi_sim.h"
#include "mmgsdilib.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 *--------------------------------------------------------------------------*/

typedef struct
{
  const char* str_ptr;
  uint8 str_len;
} item_info_t;

#define G2W_DSDS_TA_SUPPORT  0x01
#define G2W_TSTS_TA_SUPPORT  0x02
#define G2T_DSDS_TA_SUPPORT  0x04
#define G2T_TSTS_TA_SUPPORT  0x08
#define G21X_DSDS_TA_SUPPORT 0x10
#define G21X_TSTS_TA_SUPPORT 0x20

#define G2X_TA_SUPPORT 0xFF

/*----------------------------------------------------------------------------
 * Global Data Definitions
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 *--------------------------------------------------------------------------*/

struct
{
  boolean valid;
#ifdef FEATURE_DUAL_SIM
  uint8 default_rf_chain;
  uint8 g2x_ta_support;
#endif // FEATURE_DUAL_SIM
} geran_nv_data;

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 *--------------------------------------------------------------------------*/

/*!
 * \brief Creates /nv/item_files/conf/geran.conf
 */
static void create_efs_nv_conf_file(
  void
)
{
  int fd;     // File Descriptor

  // Create geran.conf if it doesn't exist
  fd = efs_open("/nv/item_files/conf/geran.conf",
                O_WRONLY | O_CREAT | O_EXCL,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

  // A return of 0 or greater means success
  if (fd >= 0)
  {
    // Limit string lengths to 127
    #define MAX_STR_LEN 127

    // Limit number of items to 255
    #define MAX_NUM_OF_ITEMS 255

    // Number of items defined in geran_efs_nv_items.h
    #define NUM_OF_ITEMS (sizeof(geran_efs_nv_items) / sizeof(const char*))

    item_info_t *items_ptr;
    uint8 num_of_items;

    // Cap the number of items at MAX_NUM_OF_ITEMS
    num_of_items = MIN(NUM_OF_ITEMS, MAX_NUM_OF_ITEMS);

    // Allocate memory to store the information about the items
    items_ptr = GPRS_MEM_MALLOC(sizeof(item_info_t) * num_of_items);

    if (items_ptr != NULL)
    {
      int i;
      item_info_t *item_ptr;
      int result; // result of file operation
      uint16 file_len = 0;
      char *file_buffer_ptr;

      // Start at the beginning of the list of items
      item_ptr = items_ptr;

      for (i = 0; i < num_of_items; i++)
      {
        const char* str_ptr = geran_efs_nv_items[i]; // see geran_efs_nv_items.h
        uint8 str_len = strlen(str_ptr);  // find the length of the string

        // Cap the string length at MAX_STR_LEN
        str_len = MIN(str_len, MAX_STR_LEN);

        // Store a pointer to the text string and its length
        item_ptr->str_ptr = str_ptr;
        item_ptr->str_len = str_len;

        // Increase the total file length count by the size of the string
        file_len += str_len;

        // Add 2 because \r\n will be added to each string
        file_len += 2;

        // Point to next item
        ++item_ptr;
      }

      // Allocate a buffer to store the file contents
      file_buffer_ptr = (char*)GPRS_MEM_MALLOC(file_len);

      if (file_buffer_ptr != NULL)
      {
        char* file_buffer_write_ptr = file_buffer_ptr;

        // Start at the beginning of the list of items
        item_ptr = items_ptr;

        for (i = 0; i < num_of_items; i++)
        {
          // Copy the string in to the file contents buffer
          // Buffer overflow cannot occur as we have worked out above how much we're going
          // to write but use memscpy anyway so all use of memcpy is expunged
          memscpy(file_buffer_write_ptr, file_len, item_ptr->str_ptr, item_ptr->str_len);

          // Increase the file buffer write pointer by the length of the string just copied
          file_buffer_write_ptr += item_ptr->str_len;

          // Add \r\n and increase the file buffer write pointer by 2
          memscpy(file_buffer_write_ptr, 2, "\r\n", 2);
          file_buffer_write_ptr += 2;

          // Point to next item
          ++item_ptr;
        }

        // Write to the file
        result = efs_write(fd, (const void*)file_buffer_ptr, file_len);

        if (result <= 0)
        {
          MSG_GERAN_ERROR_1("Error (%d) writing geran.conf",result);
        }

        // Free allocated memory
        GPRS_MEM_FREE(file_buffer_ptr);
      }

      // Free allocated memory
      GPRS_MEM_FREE(items_ptr);
    }

    // Close the file
    (void) efs_close(fd);
  }
}

/*!
 * \brief Reads general GERAN EFS-NV items
 */
void geran_read_efs_nv_items(
  void
)
{
#ifdef FEATURE_DUAL_SIM
  uint8 default_rf_chain;
  uint8 g2x_ta_support;

  // -1 indicates file not present
  geran_nv_data.default_rf_chain = -1;

  // If no nv item file is present, G2W/G2T/G21x TA is enabled for DSDS.
  geran_nv_data.g2x_ta_support = (G2W_DSDS_TA_SUPPORT | G2T_DSDS_TA_SUPPORT | G21X_DSDS_TA_SUPPORT);

  if (efs_get("/nv/item_files/modem/geran/default_rf_chain",
              &default_rf_chain,
              sizeof(default_rf_chain)) > 0)
  {
    if ((default_rf_chain == 0) || (default_rf_chain == 1))
    {
      MSG_GERAN_HIGH_1("default_rf_chain=%d", default_rf_chain);
      geran_nv_data.default_rf_chain = default_rf_chain;
    }
  }
  if(efs_get("/nv/item_files/modem/geran/g2x_ta_support",
              &g2x_ta_support,
              sizeof(g2x_ta_support)) > 0)
  {
      MSG_GERAN_HIGH_1("g2x_ta_support=%d", g2x_ta_support);
      geran_nv_data.g2x_ta_support = g2x_ta_support;
  }
#endif // FEATURE_DUAL_SIM

  geran_nv_data.valid = TRUE;
}

#ifdef FEATURE_DUAL_SIM
/*!
 * \brief Returns the value of EFS-NV item default_rf_chain.
 * 
 * \return int - {0,1} or -1 if not defined
 */
int geran_nv_get_default_rf_chain(
  void
)
{
  if (geran_nv_data.valid)
  {
    return geran_nv_data.default_rf_chain;
  }

  return -1;
}
#endif // FEATURE_DUAL_SIM

#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_TRIPLE_SIM)
/*!
 * \brief Returns the g2w support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
int geran_nv_g2w_ta_supported(
  void
)
{
  sys_modem_dual_standby_pref_e_type mode_pref = geran_get_sys_multi_sim_pref();

  if(geran_nv_data.valid)
  {
    if(SYS_MODEM_DS_PREF_DUAL_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G2W_DSDS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else if(SYS_MODEM_DS_PREF_TRIPLE_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G2W_TSTS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else
    {
      return 0;
    }
  }
  return -1;
}
/*!
 * \brief Returns the g2t support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
int geran_nv_g2t_ta_supported(
  void
)
{
  sys_modem_dual_standby_pref_e_type mode_pref = geran_get_sys_multi_sim_pref();

  if(geran_nv_data.valid)
  {
    if(SYS_MODEM_DS_PREF_DUAL_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G2T_DSDS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else if(SYS_MODEM_DS_PREF_TRIPLE_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G2T_TSTS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else
    {
      return 0;
    }
  }
  return -1;
}

/*!
 * \brief Returns the g21x support based on the values of EFS-NV items.
 * 
 * \return int - {0,1} or -1 if not defined
 */
int geran_nv_g21x_ta_supported(
  void
)
{
  sys_modem_dual_standby_pref_e_type mode_pref = geran_get_sys_multi_sim_pref();

  if(geran_nv_data.valid)
  {
    if(SYS_MODEM_DS_PREF_DUAL_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G21X_DSDS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else if(SYS_MODEM_DS_PREF_TRIPLE_STANDBY == mode_pref)
    {
      return ((geran_nv_data.g2x_ta_support & G21X_TSTS_TA_SUPPORT)?GERAN_TA_SUPPORTED:GERAN_TA_NOT_SUPPORTED);
    }
    else
    {
      return 0;
    }
  }
  return -1;
}

#endif /* FEATURE_DUAL_SIM || FEATURE_TRIPLE_SIM */

/*-----------------------------------------------------------------------------
 * Externalised Function Definitions
 *---------------------------------------------------------------------------*/

/*!
 * \brief Module initialisation function. Called during RR task initialisation.
 */
void geran_nv_init(
  void
)
{
  // Zero the NV data structure
  memset(&geran_nv_data, 0, sizeof(geran_nv_data));

  // Create /nv/item_files/conf/geran.conf if it doesn't exist
  create_efs_nv_conf_file();
}

/*!
 * \Function to check Ciphering logging enabled or not. 
 * 
 * \return boolean - By default TRUE if Test sim is configured, FALSE otherwise.
 */
boolean geran_ciphering_logging_enabled(gas_id_t gas_id)
{
  sys_modem_as_id_e_type  as_id = SYS_MODEM_AS_ID_NONE;

  as_id = geran_map_gas_id_to_nas_id(gas_id);

  /* Call UIM API "mmgsdi_is_test_mode_logging_enabled_for_subscription" is used to get whether security key logging is enabled. 
   * If enabled, logging of keys is allowed in the caller.
   * If disabled, If a test card is in use,it will always return TRUE.If it is a commercial card,
   * it will take into account the value in the protected NV item (/nv/item_files/modem/uim/mmgsdi/test_mode_logging_enabled".
   * Based on above efs value, it act accordingly. 
   */
  if (TRUE == mmgsdi_is_test_mode_logging_enabled_for_subscription(as_id))
  {
    return TRUE;
  }

  MSG_GERAN_HIGH_0("Ciphering related logging is disabled at UIM Level.");
  return FALSE;
}

/* EOF */

