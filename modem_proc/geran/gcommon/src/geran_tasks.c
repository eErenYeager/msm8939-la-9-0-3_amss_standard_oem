/*==============================================================================

                  G E R A N   T A S K   I N F O R M A T I O N

GENERAL DESCRIPTION

      This module contains the GERAN task infomation such as TCBs used
      within GERAN and by GERAN clients

EXTERNALIZED FUNCTIONS

      geran_tcb_read()  (API)
      geran_tcb_set()


Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gcommon/src/geran_tasks.c#1 $

when       who    what, where, why
--------   ---    --------------------------------------------------------------
2012-04-24 tjw    New file
==============================================================================*/

/*==============================================================================

                      INCLUDE FILES FOR MODULE

==============================================================================*/
#include "customer.h"
#include "comdef.h"
#include "rex.h"
#include "geran_tasks.h"
#include "geran_tasks_internal.h"
#include "msg.h"

/*==============================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/

/*==============================================================================
  Typedefs
==============================================================================*/
typedef struct
{
   boolean initialised;
   rex_tcb_type* tcb;
} geran_task_info_t;

/*==============================================================================
  Constants and Macros
==============================================================================*/

/*==============================================================================
  Variables
==============================================================================*/
geran_task_info_t geran_task_info[NUM_GERAN_TASK_IDS];
boolean geran_task_info_initialised = FALSE;

/*==============================================================================

                    FUNCTION DECLARATIONS FOR MODULE

==============================================================================*/

static void geran_info_initialise(void);

/*==========================================================================

  FUNCTION    geran_tcb_read

  DESCRIPTION

    This function provides the tcb for the specified GERAN task

  PARAMETERS
  
    geran_task_id_t  task_id

  DEPENDENCIES
    None.

  RETURN VALUE
    rex_tcb_type *tcb.

  SIDE EFFECTS
    None.

===========================================================================*/
rex_tcb_type* geran_tcb_read(geran_task_id_t task_id)
{
  rex_tcb_type *result = NULL;
  
  if (geran_task_info_initialised)
  {
    if (geran_task_info[task_id].initialised)
    {
      result = geran_task_info[task_id].tcb;
    }
  }
  if (result == NULL)
  {
#ifdef PERLUTF
    #error code not present
#else
    MSG_FATAL("",0,0,0); /* geran_task_info not initialised */
#endif /*PERLUTF*/

  }
  return result;
}

/*==========================================================================

  FUNCTION    geran_tcb_set

  DESCRIPTION

    This function sets the tcb for the specified GERAN task

  PARAMETERS
  
    geran_task_id_t  task_id
    rex_tcb_type *tcb.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
void geran_tcb_set(geran_task_id_t task_id, rex_tcb_type *tcb, void (*mutex_lock)(void),  void (*mutex_unlock)(void))
{
  mutex_lock();
  if (geran_task_info_initialised == FALSE)
  {
    geran_info_initialise();
  }
  geran_task_info[task_id].tcb = tcb;
  geran_task_info[task_id].initialised = TRUE;
  mutex_unlock();
}

/*==========================================================================

  FUNCTION    geran_info_initialise

  DESCRIPTION

    This function provides the tcb for the specified GERAN task

  PARAMETERS
  
    None.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    None.

===========================================================================*/
static void geran_info_initialise(void)
{
  int32 task_id;
  
  for (task_id = 0; task_id < NUM_GERAN_TASK_IDS; task_id++)
  {
    geran_task_info[task_id].initialised = FALSE;
  }
  geran_task_info_initialised = TRUE;
}
