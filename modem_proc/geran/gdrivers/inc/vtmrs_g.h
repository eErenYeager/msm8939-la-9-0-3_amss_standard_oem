#ifndef VTMRS_G_H
#define VTMRS_G_H
/*===========================================================================

                 G S M   T I M E R S  H E A D E R    F I L E

DESCRIPTION
  This module contains declarations and definitions to interface
  with the GSM timers module.

Copyright (c) 2001-2014 Qualcomm Technologies, Inc.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/gdrivers/inc/vtmrs_g.h#1 $
$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who       what, where, why
--------   ---     --------------------------------------------------------
24/01/14  cah       CR598552: enable sleep for DSDS GSTMR+VSTMR
15/10/13   cah     created new header file for common vstmr geran api's         

===========================================================================*/
#if defined (FEATURE_MSIM_VSTMR)

#include "geran_variation.h"
#ifndef CUSTOMER_H
  #include "customer.h"
#endif
#ifndef COMDEF_H
  #include "comdef.h"
#endif
//#include "timetick.h"

#include "vstmr_geran.h"
//#include "geran_multi_sim.h"
#include "gtmrs.h"
#include "gtmrs_g.h"

#define GERAN_VSTMR_EVENT_INDEX_0 0 //periodic event
#define GERAN_VSTMR_EVENT_INDEX_1 1 //one shot event

#define VSTMR_SLAM_MARGIN 1000 /*extend the frame if attempting to slam within 1000 Qs before end of frame*/


extern void remove_gstmr_from_hw_registration (gas_id_t gas_id);

extern void vstmr_wake_up (gas_id_t gas_id);
extern void vstmr_sleep (gas_id_t gas_id);

extern void gstmr_schedule_event ( gas_id_t gas_id , uint16 fn);


#define GERAN_VSTMR_MAX_FN       (VSTMR_GERAN_RTC_MODULO/VSTMR_GERAN_FRAME_RANGE)
 
extern uint32 vstmr_rd_modMaxFN_frame_num( gas_id_t gas_id  );

extern uint32 get_vstmr_FN_modMaxFN( sys_modem_as_id_e_type as_id  );

extern void reset_vstmr_FN_modMaxFN (gas_id_t gas_id );

extern void vstmr_adjust_terminal_count_geran (int16 quarter_symbol_count
                                  ,sys_modem_as_id_e_type as_id);

extern void vstmr_register_vstmr_view (gtmrs_hw_t gtmrs_hw, gas_id_t gas_id);

extern void    vstmr_enable_interrupt (gtmrs_hw_t gtmrs_hw, gas_id_t gas_id);

extern int32 vstmr_get_tc_adjustment(gas_id_t gas_id);



extern uint32 vstmr_rd_symbol_count( gas_id_t gas_id );

extern uint32 vstmr_rd_qsymbol_count_geran( sys_modem_as_id_e_type as_id );

extern uint32 vstmr_rd_odd_even( gas_id_t gas_id );

extern uint32 vstmr_rd_modMaxFN_frame_num( gas_id_t gas_id );

extern uint32 vstmr_rd_mod4_frame_num( gas_id_t gas_id );

extern uint32 geran_rd_hw_frame_count_vstmr(gas_id_t gas_id );

extern void gl1_ustmr_vstmr_snapshot_before_sleep(gas_id_t gas_id);

extern void gl1_vstmr_gstmr_sync(gas_id_t gas_id);

extern void vstmr_deactivate(gas_id_t gas_id);

extern void gl1_gstmr_vstmr_swap(gas_id_t gas_id);

extern void gl1_vstmr_gstmr_swap(gas_id_t gas_id);

extern void vstmr_geran_vstmr_event_cancel (gas_id_t gas_id);

extern void vstmr_geran_vstmr_deregister (gas_id_t gas_id);

extern void vstmr_snapshot_before_vstmr_deregister (gas_id_t gas_id);

#endif /* defined (FEATURE_MSIM_VSTMR)*/

#endif /* VTMRS_G_H */

