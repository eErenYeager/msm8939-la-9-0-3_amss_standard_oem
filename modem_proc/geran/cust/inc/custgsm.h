#ifndef CUSTGSM_H
#define CUSTGSM_H
/*===========================================================================

                           C U S T G S M

DESCRIPTION
  Customer file for GSM.

  Copyright (c) 2002-2013 by QUALCOMM Technologies, Inc. All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/geran/cust/inc/custgsm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/11/14    nm      CR753150 Disable FEATURE_GSM_GPRS_READ_CCCH_IN_XFER_DEBUG
22/10/14    ws      CR744361 Data+MMS reduce MSC if commanded by CFCM
22/09/14    ka      CR727787:SRLTE+G_DSDS || DPM.2.0 || APS is not getting enabled
21/07/14    sp      CR655478:Handle Unsupported Bands on WTRs while RANE during Powerscan.
10/07/14    js      CR593074- ROTA/QBTA Feature implementation
15/05/13    nn     CR 487943 - Enable FEATURE_GSM_SEARCH_ALL_WCELL
04/07/14   nm      CR668062 Own PCH read in transfer (phase 1)
17/06/14   aga     CR675223: G2T TA feature and GTA/G2W TA re-worked optimized solution  
21/05/14   ssh    CR668643: gl1_hw_qta_gap_active() should return FALSE for multimode sub
05/11/13   cja     CR523987 Add option of FEATURE_GSM_DED_SCELL_MONITOR for extra SCell monitor in voice call
23/05/13   pg      CR460555: Enable 3GPP Rel10 RACH Power Reduction feature 
14/09/13    ap/aga CR545423 - G2W Tune Away Feature
15/08/13    sk     CR524039 GBTA bringup changes
29/07/13    sk     CR519663 Partial QBTA bringup changes
08/07/13    ab     CR 509759 Enable FEATURE_GSM_EGPRS_IR_WS_REDUCTION
16/05/13    ab     CR487396 - Support EGPRS IR Reduced Window in GFW
15/07/13    mko    CR511298: Undefining the Macro "FEATURE_GSM_HW_TASK_REQUEST"
29/04/13    ws     CR481359 enable GTA support
10/09/12   ss      CR363991 : Enable FEATURE_G2T_SRCH_LOGGING for G2T meas and acq response
20/07/12    rc     376792 - Enable FEATURE_SPEC_CR_2567_GTOW_SECURITY_KEY_HANDLING
11/06/12    tjw    CR367302 - remove unused FEATURES (NAS_NVMEM_HW_CLASSMARK, SCE,
                   COMMON_PWR_HANDLER, STOPPING_CELL_ID_SRCH, STOPPING_DED_CELL_ID_SRCH,
                   RR_LOCAL_SIMINFO, GSM_GPRS_ENABLE_RESELECTION_IN_TRANSFER,
                   GSM_GPRS_ENABLE_NC1_NC2_MODE_RESELECTION, GSM_GPRS_GRR_NO_NCELL_PSI1_READ,
                   GSM_GPRS_GRR_PSI_REFRESH, INTERRAT_CELL_RESELECTION_GTOW_GPRS,
                   INTERRAT_CELL_RESELECTION_GTOW_XFER, INTERRAT_NC_RESELECTION_GTOW_GPRS,
                   GPRS_GRR_GMM_PCCCH_PAGING_COORD, GHDI_MVS_MODE_TO_GL1
12/04/12    rc     Enable FEATURE_GSM_TDS_PSEUDO_FAST_RETURN & FEATURE_GSM_TDS_G2T_REDIRECTION
10/04/12    pm	   Enabled FEATURE_GSM_TDS_JDS
21/03/12    pg     Add FEATURE_GERAN_CRIT_SECTION_SUPPORT for DIME modem
16/03/12    rc	   Enabled FEATURE_GSM_QPA for QFE 1320
15/03/12    pg     Target feature MSM8974 now replaced with FEATURE_DIME_MODEM
12/03/12    pg     Enable initial MSM8974 feature set, derived from MSM8960
10/02/12    da     Enabled FEATURE_GERAN_SNDCP_REORDER for Nikel.
29/09/11    tjw    Duplicate definition of FEATURE_GSM_CB_DRX_SUPPORTED removed from
                   custgsmdrv.h instead
01/07/11    rt     Removing duplicate definition of FEATURE_GSM_CB_DRX_SUPPORTED
24/06/11    rt     Defining FEATURE_GERAN_CRIT_SECTION_SUPPORT for 8660 only
24/06/11    rt     Enabled FEATURE_GERAN_CRIT_SECTION_SUPPORT and FEATURE_GSM_CB_DRX_SUPPORTED
24/06/11    nf     CR289274 ported
01/03/11    rc     Enable FEATURE_GHDI_MVS_MODE_TO_GL1
23/02/11    pg     Enable IR Opt feature for Pegasus
26/01/11    pg     Disable IR Opt feature temporarily for Pegasus
20/01/11    rc     Enabled IR Opt feature. CB DRX Enabled only when DSDS is not enabled
07/01/11    pm     Enabled FEATURE_GSM_CB_DRX_SUPPORTED.
06/12/10   sjw     Disable mainline features during DSDS bringup on Poseidon
30/11/10    ab     Enabled IR MEMORY REDUCTION FEATURE.
                   FEATURE_GSM_MDSP_IR_3_BIT_PACKING
               FEATURE_GSM_EGPRS_IR_OPT_PER_TBF
                   FEATURE_GSM_EGPRS_IR_WS_REDUCTION
17/10/10    ps     Enabled FEATURE_GSM_WDOG_DYNAMIC for targets supporting
                   DYNAMIC WDOG; currently 9K targets
16/08/10    rc     Enabled FEATURE_GSM_TO_LTE for Genesis 3.x
23/07/10    rc     Disabled FEATURE_GSM_TO_LTE for Genesis
19/07/10    rc     Enabled FEATURE_GSM_TO_LTE for Genesis
18/06/10    rc     Moved FEATURE_NONBLIND_HANDOVER_DEBUG from cutsusurf.h
03/06/10    ws     Enabled FEATURE_GSM_DISABLE_SLEEP for 8660
03/06/10    ws     Enable FEATURE_GSM_8K_QDSP6_AUDIO for T_MSM8660 Blackbird
20/05/10    dv     Move FEATURE_GSM_RFA_IRAT so only applicable to 9k
20/05/10    ps     Enabled FEATURE_GSM_PCH_SINGLE_BURST_DEC
19/05/10    rc     Enabled FEATURE_GSM_FEMTO_SEARCH feature
07/05/10    ps     Removed FEATURE_GSM_TO_LTE; Not required for MDM9x00 1.x PL
20/04/10    rc     Enabling 2H09 feature set (L2 Random Padding) for Genesis Modem
30/03/10    ps     FEATURE_BPLMN_SEARCH_PRIORITY required for GOBI3000 product
16/02/10    ws     Enabled FEATURE_GSM_GPRS_AGC_UPDATE for 9K targets
02/02/10    rn     Added FEATURE_GSM_RR_CELL_CALLBACK for all targets, move away from custusurf.h target specific featurisation.
15/01/10    rc     Enabled FEATURE_GSM_GPRS_AGC_UPDATE for Pos-2 targets
30/11/09    rc     Disabling LIST_SEARCH_IN_PAGING_BLOCK for Pos-2 targets for G2W
27/10/09    rc     Featurized FEATURE_GTOW_REDIRECTION_AFTER_CONN_RELEASE under FEATURE_WCDMA
27/10/09    rc     Enabled Single Burst PCH decode & AMR-WB for Pos-2
07/10/09    rc     Enabled L2 Random padding, CSI, EDGE IR Opt for Pos-2
25/09/09    rc     Removed FEATURE_GERAN_STORED_LIST_CELL_SELECTION as the RR_ACQ_DB feature covers the functionality
28/09/09    ws     Added Featurisation for 7x30 target to mirror SCMM
23/09/09    ws     Added featurisation for MDM9000 to enable MUTEX and
                   QDSP6 GERAN features
09/07/09    ws     Disabled 1H09 feature for POSEIDON for now
09/06/09    nf     Added FEATURE_GSM_PCH_SINGLE_BURST_DEC
01/04/09    ws     Removed SCMM exclusion of R-SACCH
03/03/09    ws     Corrected featurisation for SCMM
26/02/09    ps     Added FEATURE_GSM_L2_RANDOM_PADDING. Shall be enabled later.
29/01/09    rc     Enabled FEATURE_GSM_GPRS_ENH_SNR_RESEL
23/01/09    ws     Commented features for SCMM bringup
19/12/08    pm     Commented Cell selection after Channel Release, G2W redirection after connection release
19/12/08    pm     Enabled DYNAMIC SAIC, Cell selection after Channel Release, G2W redirection after connection release and Removed EMR, PEMR
06/10/08    rc     Added FEATURE_BPLMN_SEARCH_PRIORITY for GOBI-2000 - Fix for CR:152804
02/10/08  ps     Enable AMR-WB for LCU
18/09/08    rc     Removed FEATURE_USB_WITH_ARM_HALT, Enabled FEATURE_GAN dependent features & FEATURE_LAPDM_REESTABLISHMENT
04/09/08    rc     Enabled FEATURE_GSM_RGS_MULTIPASS
01/09/08    rc     Enable R-SACCH for LCU
04/08/08    rc     Disable AMR-WB and R-SACCH feature in LCU for now.
08/07/08    ws     Changes for raptor2 Target, Disable AMR-WB and define FEATURE_GSM_8K_QDSP6_AUDIO
13/06/08    rc     Added 2H07 GERAN Feature AMR-WB
07/05/08    rc     Added 2H07 GERAN Features R-SACCH & PS HO
07/05/08    rc     Added FEATURE_GPRS_FAST_RACH
30/04/08    ws     Moved ESAIC feature to cutgsmdrv.h
30/04/08    ws     Enable ESAIC for LCU
06/02/08    ws     Added FEATURE_GSM_MDSP_ESAIC
05/02/08    ws     Added FEATURE_GSM_L1_IMPLEMENT_LPM
22/01/08    ws     Added T_QSC6270 specific featurisation
22/10/07    ws     Addede Enable ACQ database feature
14/09/07    rc     Added FEATURE_GPRS_COMP_ENUM_TYPES to re-use SNDCP VU that support IPHC with other layers that does not support IPHC
14/08/07    rc     Replaced FEATURE_GSM_SNDCP_IPHC_ROHC with FEATURE_GSM_SNDCP_IPHC
                   Added FEATURE_GSM_L1_OOS_SEARCH_TIME_ENH
09/07/07    rc     Removed FEATURE_CSN_REFACTORED
31/05/07    ws     Enabled FEATURE_GSM_L1_CONTROL_SET_BAND
16/04/07    npr    Enabled 2H06 features: EDTM, RFACCH, IPA, CSN
19/01/07    rc     Enabled FEATURE_GSM_GPRS_LLC_GEA_NV_SUPPORT
21/11/06    rc     Enabled FEATURE_GSM_L1_HFREQ_ERR_NCELL_SCH_DECODE
10/02/06    ws     Enabled DTM,EDA and PFC
09/06/05    ws     Removed FEATURE_GSM_DTM  since new build Id's use them
08/26/05    ws     Merged from Raven Branch features DISABLE_XFER_SNR_RESELECTION
                   and FEATURE_GPRS_GRR_GMM_PCCCH_PAGING_COORD
08/25/05    ws     Enabled FEATURE_GSM_DTM
06/23/05   src     Enabled FEATURE_GSM_GPRS_PEMR
05/24/05    gw     Enable WCDMA ID searches in transfer mode via
                   FEATURE_GTOW_RESELECTION_XFER_CELL_ID
04/24/05    bk     Temp disabled SNR based reselection in transfer via DISABLE_XFER_SNR_RESELECTION
11/19/04   gfr     Moved DEBUG_HW_SELECT_STAGE2_OUTPUT_CHANNEL_FILTER to
                   custgsmdrv.h
13/05/04   npr     Initial revision.
===========================================================================*/

#ifdef FEATURE_GSM
/***************************************************************************
                                 GSM
****************************************************************************/
/* DSDA DIME X+G specific feature */

#if defined FEATURE_DIME_MODEM
#define FEATURE_GERAN_CRIT_SECTION_SUPPORT
#elif defined (T_MDM8200) || defined (T_MDM9X00)
#define FEATURE_GERAN_KXMUTEX_SUPPORT
#endif

#ifdef FEATURE_DUAL_SIM

#define FEATURE_GERAN_TRM_PAGE_SKIPPING

/**** Enable DSDS specific featured ****/
#if defined (FEATURE_DIME_2_1)
/* Enable Single GSTMR and EDGE core HW */
#define FEATURE_GSM_SINGLE_HW
#endif
/* Enable Voice Services DRIVER for GSM only Sub if not using single GSTMR HW */ 
#if !defined (FEATURE_GSM_SINGLE_HW)
#define FEATURE_GSM_VS_DRIVER
#endif

#if defined (FEATURE_WCDMA)
#define FEATURE_QBTA
#endif

#define FEATURE_CXM_QTA
/* Feature used to log the QTA events*/
#if defined (FEATURE_QTA) || defined (FEATURE_CXM_QTA)
#define QTA_EVENT_DATA_LOGGING
#endif

#define FEATURE_DUAL_SIM_GERAN_TUNEAWAY
//#define FEATURE_G2W_TUNEAWAY
#define FEATURE_DUAL_SIM_GTA_NAS_VOTING
#endif /*FEATURE_DUAL_SIM*/

#if defined (T_MDM9X00) || defined (FEATURE_DIME_MODEM)
/* Enable QDSP6,GENESIS specific code */
#define FEATURE_GSM_GPRS_QDSP6
/* MUSTANG FEATURE needed to port MVS code from MUSTANG */
#define FEATURE_GSM_GPRS_MUSTANG
/* 9k specific Inter-RAT changes */
#define FEATURE_GSM_RFA_IRAT
#endif
/* Enable GSM multiband support
*/
//top level feature removed: #define FEATURE_GSM_MULTIBAND
//top level feature removed: #define FEATURE_GSM_BAND_PREF
//top level feature removed: #define FEATURE_GSM_QUADBAND

/* Enable L1/RR Panic Reset Recovery
*/
#define FEATURE_GSM_GPRS_PANIC_RESET

#if (defined T_MSM8660)
#define FEATURE_GERAN_CRIT_SECTION_SUPPORT
#endif

/*---------------------------------------------------------------------------
                               GSM L1
---------------------------------------------------------------------------*/

/* Enable L1 state debug
*/
#define FEATURE_GSM_L1_STATE_LOG

/* Enable GSM Half Rate support
*/
//top level feature removed: #define FEATURE_GSM_HALF_RATE

/* Enable transfer of BSIC when transitioning between Idle and Transfer
*/
#define FEATURE_GSM_TRANSFER_BSIC

/* Enable AMR support
*/
//top level feature removed: #define FEATURE_GSM_AMR
#define FEATURE_GSM_AMR_HALF_RATE
//top level feature removed: #define FEATURE_GSM_EXT_SPEECH_PREF_LIST

/* Increase the Cell ID search periodicity to 25sec, and send the
   measurement report to RR right after Cell ID search.
   Increase the Cell Reconf periodicity to 5sec
*/
#define FEATURE_INCREASED_PERIOD_FOR_CELL_ID_CELL_RECONF_SRCHS

/* Move WCDMA reconfirm into the paging block */
#if (!defined (T_QSC6695) && !defined (T_MSM7X30))
#define LIST_SEARCH_IN_PAGING_BLOCK
#endif

/* This feature is additionally required to enable MDSP memory dumping on error. */
#undef FEATURE_GSM_HW_TASK_REQUEST

/* This feature is Enhanced Extended Uplink TBF specified in Rel-6 */
#define FEATURE_GPRS_EXT_ULTBF_NO_DATA

/* This feature handles ncell SCH FCCH decode in High Frequency Error Conditions */
#define FEATURE_GSM_L1_HFREQ_ERR_NCELL_SCH_DECODE

/* this feature enable l1 control of setting frequency plan for quad band scaning */
#define FEATURE_GSM_L1_CONTROL_SET_BAND

/* enable Low power mode for Game,Airplane mode */
#define FEATURE_GSM_L1_IMPLEMENT_LPM

/* Enable DYNAMIC SAIC */
#define FEATURE_GSM_DYNAMIC_SAIC

#if (defined T_MSM8650B) || (defined T_MSM8660)
/* On Raptor2 and Blackbird Enable Endian Byte swapping for QDSP6 ADSP */
#define FEATURE_GSM_8K_QDSP6_AUDIO
#endif

/* Disable Sleep on 8660 for now */
#if (defined T_MSM8660)
#define FEATURE_GSM_DISABLE_SLEEP
#endif

#define FEATURE_GSM_GPRS_ENH_SNR_RESEL

#define FEATURE_GSM_FEMTO_SEARCH

//enable QPA support
#define FEATURE_GSM_QPA
#define FEATURE_GSM_SEARCH_ALL_WCELL

// enable 3GPP rel 10 feature: RACH power reduction
#define FEATURE_GSM_RACH_PWR_RED



/*---------------------------------------------------------------------------
                               GSM L2
---------------------------------------------------------------------------*/

/* SMS enhancements that the RR team have developed in conjunction with NAS
*/
#define FEATURE_RR_DATA_REQ_CALLBACK

#define FEATURE_LAPDM_REESTABLISHMENT

#define FEATURE_GSM_L2_RANDOM_PADDING

/*---------------------------------------------------------------------------
                               GSM RR
---------------------------------------------------------------------------*/

/* Enable background HPLMN searching */
#define FEATURE_GSM_BACKGROUND_HPLMN_SEARCH

/* Enable Cell Broadcast
*/
//top level feature removed: #define FEATURE_GSM_CB

/* Enable ACQ database feature */
//top level feature removed: #define FEATURE_RR_ACQ_DB

#define FEATURE_SPEC_CR_2567_GTOW_SECURITY_KEY_HANDLING

/***************************************************************************
                                 GPRS
****************************************************************************/

/* Enable GPRS as Standard */
//top level feature removed: #define FEATURE_GSM_GPRS

/* Enable EDGE Code Components as Standard */
//top level feature removed: #define FEATURE_GSM_EGPRS

/* Enable GPRS Logging */
#define FEATURE_DIAG_GPRS

/* Enable GERAN Feature pack 1 */
//top level feature removed: #define FEATURE_GSM_GERAN_FEATURE_PACK_1

/* Enable DTM */
//top level feature removed: #define FEATURE_GSM_DTM
//top level feature removed: #define FEATURE_GSM_EDTM

/* Enable Extended Dyanmic Allocation for Mulit-slot class 11,12 */
//top level feature removed: #define FEATURE_GPRS_EDA

/* Enable Packet Flow context PFC for EGPRS and GPRS */
//top level feature removed: #define FEATURE_GSM_GPRS_PFC

/* Enable Repeated FACCH support */
//top level feature removed: #define FEATURE_GSM_R_FACCH

/* DARP for GSM GPRS and EDGE (SAIC) */
//top level feature removed: #define FEATURE_GSM_EGPRS_SAIC
#ifdef FEATURE_GSM_EGPRS_SAIC

   /* NAS support for Rel 6 feature */
//top level feature removed:    #define FEATURE_REL_6_SUPPORT

#endif /* FEATURE_GSM_EGPRS_SAIC */



/*---------------------------------------------------------------------------
                           GPRS SNDCP
---------------------------------------------------------------------------*/

/* Enable GPRS SNDCP Compression Features*/
#define FEATURE_GSM_GPRS_SNDCP_PCOMP
#define FEATURE_GSM_GPRS_SNDCP_DCOMP
#define FEATURE_GSM_SNDCP_IPHC
//top level feature removed: #define FEATURE_GPRS_COMP_ENUM_TYPES
#define FEATURE_GERAN_SNDCP_REORDER

/*---------------------------------------------------------------------------
                           GPRS LLC
---------------------------------------------------------------------------*/

/* Aglilent 8960 does not size it's SN-PDUs correctly according to N201-U/I
   defaults. This reduces the stictness of length checking as performed by
   LLC on received frames. */
#undef DEBUG_GSM_GPRS_LLC_AGILENT8960_WORKAROUND

/* Enable NV Provisioning of supported GEA Algorithms */
#define FEATURE_GSM_GPRS_LLC_GEA_NV_SUPPORT

/*---------------------------------------------------------------------------
                           GPRS RLC
---------------------------------------------------------------------------*/

#undef DEBUG_GSM_GPRS_GRLC_DISABLE_TWO_PHASE_REQ

#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1

   /* NAS support for Rel 4 feature */
//top level feature removed:    #define FEATURE_REL_4_SUPPORT

  /* Enable Extended uplink TBF feature, which is part of
   * GERAN Feature Pack 1
   */
  #define FEATURE_GSM_EXT_UL_TBF

#endif

#define FEATURE_GPRS_GRLC_STALE_PDU_DELETE_TIMER

/*---------------------------------------------------------------------------
                           GPRS MAC
---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
                           GPRS L1
---------------------------------------------------------------------------*/

/* PL1 Features */
#define FEATURE_GSM_GPRS_L1
#define FEATURE_GPRS_MEAS
#define FEATURE_GPRS_TBF_SUSPEND
#define FEATURE_GSM_GPRS_MULTIBAND
#define FEATURE_GTOW_RESELECTION_XFER_CELL_ID
#define FEATURE_GSM_L1_OOS_SEARCH_TIME_ENH

/*---------------------------------------------------------------------------
                           GPRS RR
---------------------------------------------------------------------------*/

/* Agilent 8960 includes the SGSNR bit in PSI13, but not SI13, leading to
   multiple notifications to MM of changing SGSNR. This workaround sets the
   SGSNR to that received in the previous SI13 whenever PSI13 is received */
#define DEBUG_GSM_GPRS_GRR_SGSNR_AGILENT8960_WORKAROUND

/* Enable SNR assisted reslection.
*/
#define FEATURE_RR_L1_EXTRA_RESELECTION_INFO

/* Local inter-rat feature defines for ongoing development. */
#define FEATURE_INTERRAT_NC_RESELECTION_GTOW_GPRS
#define FEATURE_REL5_SPEC_COMPLIANT_G2W_RESELECTION

#ifdef FEATURE_GSM_GERAN_FEATURE_PACK_1

  /* Enable NACC Feature, which is part of
   * GERAN Feature Pack 1
   */
  #define FEATURE_GSM_NACC

#endif

/* Fix for CR-124527 */
#define FEATURE_GPRS_FAST_RACH

/* 2H07 GERAN Feature Set */
//top level feature removed: #define FEATURE_GPRS_PS_HANDOVER

//top level feature removed: #define FEATURE_GSM_R_SACCH

#if ( !defined(T_MSM8650B) && (!defined (T_MDM9X00) ) )
/* Don't enable W-AMR on Raptor2 and SCMM for now */
//top level feature removed: #define FEATURE_GSM_AMR_WB
#endif

#if (!defined (T_QSC6695) && !defined (T_MSM7X30) )
#define FEATURE_GSM_RGS_MULTIPASS
#endif

#ifdef FEATURE_GAN
  #error code not present
#endif

#if defined (T_GOBI2000) || defined (FEATURE_GOBI)
   #define FEATURE_BPLMN_SEARCH_PRIORITY
#endif

/* Status IND for SM Signalling */
//top level feature removed: #define FEATURE_GPRS_LAYER3_TX_CONFIRM

#define FEATURE_GSM_GPRS_AGC_UPDATE
//top level feature removed: #define FEATURE_GSM_RR_CELL_CALLBACK

/* Enable Cell selection after Channel Release */
#define FEATURE_CELL_SELECTION_AFTER_CONN_RELEASE

#define FEATURE_GSM_PCH_SINGLE_BURST_DEC

/* Flag for enabling FCCH SNR based cell selection during CSFB. 
 * This implementation was done for MAV. 
 * Disabling this flag for DI3.0 and will be enabled based on cutomer requirement. */
/* #define FEATURE_SNR_BASED_CELL_SEL_FOR_CSFB */

// For 1x+GSM combo builds following feature is not needed hence wrapped within FEATURE_WCDMA
#ifdef FEATURE_WCDMA
//top level feature removed:   #define FEATURE_GTOW_REDIRECTION_AFTER_CONN_RELEASE
#endif

/* Following moved from custusurf.h */
#define FEATURE_NONBLIND_HANDOVER_DEBUG
#define FEATURE_G2W_SRCH_LOGGING_ENABLED

/* Temporary feature to help synchronise MOB builds.
   Allows truncation of the RAC IE in a PRR. Affects NAS/MAC interface */
#define FEATURE_RAC_TRUNCATION

/* Enable G to L only for targets that support LTE */
#if defined(FEATURE_LTE)
#define FEATURE_GSM_TO_LTE
#endif

/* Enable TD-SCDMA IRAT for GERAN */
#if defined(FEATURE_TDSCDMA)
#define FEATURE_GSM_TDS
#define FEATURE_GSM_TDS_JDS
#define FEATURE_GSM_TDS_PSEUDO_FAST_RETURN
#define FEATURE_GSM_TDS_G2T_REDIRECTION
#define FEATURE_G2T_SRCH_LOGGING  
#endif

/* Enable DYNAMIC DOG v1.5 support */
#ifdef FEATURE_WDOG_DYNAMIC
#define FEATURE_GSM_WDOG_DYNAMIC
#endif

/* 1H10 Feature set - Enable Cell Broadcast in DRX Mode for all 1H10 targets */
//top level feature removed: #define FEATURE_GSM_CB_DRX_SUPPORTED

/* Voice related features */
//top level feature removed: #define FEATURE_GSM_AMR_WB
#define FEATURE_QDSP6_ADSP_MDSP_RADIO_FUSION

#if defined (FEATURE_WCDMA)
#define FEATURE_GSM_CHECK_NAS_ACQUIRED_VOC
#endif

#define FEATURE_GSM_GPRS_TIME_PWR_AVG

#if !defined (FEATURE_DIME_2_1)
#define FEATURE_GSM_EGPRS_IR_WS_REDUCTION

#if defined(FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY)
#define FEATURE_GSM_TX_DIV_ANTNA_SWITCHING
#define FEATURE_GSM_QSC_TX_DIV
#endif

#endif

#if defined (FEATURE_SGLTE)
#define FEATURE_SGLTE_G2X_CONN_MODE_RESEL
#endif

#if defined (FEATURE_DIME_2_1)
/* Feature for extra SCell monitor in Dedicated */
#define FEATURE_GSM_DED_SCELL_MONITOR
#endif

#ifdef FEATURE_DUAL_SIM
#define FEATURE_GPRS_GBTA
#define FEATURE_G2W_TUNEAWAY
#define FEATURE_G2X_TUNEAWAY
#define FEATURE_GSM_GPRS_READ_CCCH_IN_XFER
#endif /*FEATURE_DUAL_SIM*/

#define FEATURE_VAMOS_II

#ifdef FEATURE_SGLTE_DUAL_SIM
#define FEATURE_ASYMMETRIC_GERAN_BANDS
#endif

// FR 17504 Better handling of BSR scans when there is GERAN Data
#define FEATURE_GERAN_BSR_PS_STATUS

/* Manual PLMN search partial results updating feature (GRR) */
#define FEATURE_MPLMN_PARTIAL_UPDATING

/*FR 19557*/
#define FEATURE_GSM_BAND_AVOIDANCE

// Data + MMS
#ifdef FEATURE_DUAL_DATA
#define FEATURE_GSM_CFCM
#endif

/*-------------------------------------------------------------------------*/
#endif /* FEATURE_GSM */
#include "geran_msgs.h"
#endif /* CUSTGSM_H */

