/*!
  @file
  ftm_wcdma_selftest.c

  @brief
  This module contains definitions and prototypes for WCDMA selftest functionality.
*/

/*==============================================================================

  Copyright (c) 2012 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/05/14   jmf     [SelfTest] Port Dime Defs for VSWR result return
06/30/14   daa     Created.

==============================================================================*/

#include "ftm.h"
#include "DALStdDef.h"
#include "ftm_wcdma_ctl.h"
#include "rfcom.h"
#include "ftm_common_control.h"
#include "rfcommon_nv.h"
// #include "ftm_wcdma_data.h"
#include "rfm.h"
#include "ftm_msg.h"
#include "rf_test_task_v.h"
#include "ftm_common_iq_processing.h"
#include "ftm_common_dispatch.h"
#include "stringl.h"
#include "math.h"
#include "ftm_wcdma_selftest.h"
#include "ftm_common_selftest.h"

ftm_rf_fbrx_iq_acquired_samples_data_type *ftm_rf_fbrx_iq_acquired_samples_data;
// Variable used as Response packet for FTM_RF_MEAS_TX_PARAMS command
ftm_rf_fbrx_meas_tx_params_rsp_pkt_type *ftm_rf_fbrx_meas_tx_params_rsp_pkt;

int16 ftm_wcdma_tx_power_dbm;


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Determine FBRX gain NV based on WCDMA band

  @details

  @return
  None
*/

boolean ftm_wcdma_obtain_fbrx_gain_swtichpoint_nv_item
(
   rfcom_wcdma_band_type band,
   rfnv_item_id_enum_type *nv_item
)
{
  boolean ret_value = TRUE;
  switch (band)
  {

  case   RFCOM_BAND_IMT:
    *nv_item = RFNV_WCDMA_B1_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  case   RFCOM_BAND_1900:
    *nv_item = RFNV_WCDMA_B2_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  case   RFCOM_BAND_BC3:
    *nv_item = RFNV_WCDMA_B3_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  case   RFCOM_BAND_BC4:
    *nv_item = RFNV_WCDMA_B4_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  case   RFCOM_BAND_800:
    *nv_item = RFNV_WCDMA_B5_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  case   RFCOM_BAND_BC8:
    *nv_item = RFNV_WCDMA_B8_FBRX_GAIN_SWITCH_POINT_I ;
    break;

  default:

    *nv_item = RFNV_WCDMA_B1_FBRX_GAIN_SWITCH_POINT_I ;
    ret_value = FALSE;
  }

  return ret_value;
}
#endif


// Commented out for 9x35 bringup
#if 0
void ftm_wcdma_obtain_fbrx_gain_stage(void)
{

    rfcom_wcdma_band_type band;
    rfnv_item_id_enum_type nv_id;
    boolean nv_valid;
    int16 switch_pt;
    int32 txagc;
    rfcommon_nv_fbrx_attn_switch_pt_type fbrx_gain_switchpoint_nv_data;
    ftm_common_selftest_fbrx_attn_type fb_attenuation_state;


    band = rfwcdma_mc_state.car_path_state[RF_PATH_0][0].curr_band_tx;

   /* determine the correct NV item location */

   nv_valid =  ftm_wcdma_obtain_fbrx_gain_swtichpoint_nv_item(band, &nv_id);

   if (nv_valid)
   {

      FTM_MSG_2(FTM_HIGH, "ftm_wcdma_obtain_fbrx_gain_stage: NV id =%d, band = %d",
                nv_id, band);


      /* read from the correct NV and populate the data structure for the current band from the NV*/

      rfcommon_nv_load_fbrx_attn_switch_pts_from_nv
         (&fbrx_gain_switchpoint_nv_data, nv_id,
          NULL, 0, NULL);

      switch_pt = fbrx_gain_switchpoint_nv_data.switch_pwr_dbm[0];

     FTM_MSG_1(FTM_HIGH, "ftm_wcdma_obtain_fbrx_gain_stage: obtained from NV: gain switch point = %d",
                 switch_pt);

   }
   else
   {

     switch_pt = 140;

     FTM_MSG_1(FTM_HIGH, "ftm_wcdma_obtain_fbrx_gain_stage: no valid NV for the band, using default gain switch point = %d",
                 switch_pt);
   }

    /* Obtain the TXAGC from SM */
    txagc=ftm_wcdma_tx_power_dbm;


  if (txagc<switch_pt) {

    fb_attenuation_state = FTM_COMMON_SELFTEST_FBRX_ATTN_0;
    FTM_MSG_2(FTM_HIGH,
          " ftm_wcdma_obtain_fbrx_gain_stage: TXAGC = :%d < switch pt = %d; set attenuation to LOW",
          txagc, switch_pt);
  } else
  {
    fb_attenuation_state = FTM_COMMON_SELFTEST_FBRX_ATTN_1;
    FTM_MSG_2(FTM_HIGH,
          " ftm_wcdma_obtain_fbrx_gain_stage: TXAGC = :%d >= switch pt = %d; set attenuation to HIGH",
          txagc, switch_pt);
  }


  ftm_common_set_coupler_wtr_fb_path_state(fb_attenuation_state);

}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Determine FBRX Noise Cal NV item enum based on WCDMA band

  @details

  @return
  None
*/

boolean ftm_wcdma_obtain_fbrx_noise_nv_item(rfcom_wcdma_band_type band,rfnv_item_id_enum_type *nv_item)
{
  boolean ret_value = TRUE;
  switch (band)
  {

  case   RFCOM_BAND_IMT:
    *nv_item = RFNV_WCDMA_C0_B1_FBRX_NOISE_DATA_I;
    break;

  case   RFCOM_BAND_1900:
    *nv_item = RFNV_WCDMA_C0_B2_FBRX_NOISE_DATA_I;
    break;

  case   RFCOM_BAND_BC3:
    *nv_item = RFNV_WCDMA_C0_B3_FBRX_NOISE_DATA_I;
    break;

  case   RFCOM_BAND_BC4:
    *nv_item = RFNV_WCDMA_C0_B4_FBRX_NOISE_DATA_I;
    break;

  case   RFCOM_BAND_800:
    *nv_item = RFNV_WCDMA_C0_B5_FBRX_NOISE_DATA_I;
    break;

  case   RFCOM_BAND_BC8:
    *nv_item = RFNV_WCDMA_C0_B8_FBRX_NOISE_DATA_I;
    break;

  default:
    *nv_item = RFNV_WCDMA_C0_B1_FBRX_NOISE_DATA_I;
    ret_value = FALSE;
  }

  return ret_value;
}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Process FBRX Noise Cal NV, find noise subtraction values for given band
  and return

  @details

  @return
  Noise binsum powers to subtract for ACLR correction
*/

static void ftm_wcdma_fetch_fbrx_noise(rfcommon_nv_fbrx_noise_binsum_type * fbrx_noise_from_nv)
{
  rfcommon_nv_fbrx_noise_data_type fbrx_noise_struct_from_nv;
  boolean nv_valid;
  boolean nv_data_valid;
  boolean nv_read_status;
  rfcom_wcdma_band_type band;
  rfnv_item_id_enum_type nv_id;
  band = rfwcdma_mc_state.car_path_state[RF_PATH_0][0].curr_band_tx;

  /* determine the correct NV item location */
  nv_valid =  ftm_wcdma_obtain_fbrx_noise_nv_item(band, &nv_id);

  if (nv_valid == FALSE)
  {
    memset((void *)fbrx_noise_from_nv, 0, sizeof(rfcommon_nv_fbrx_noise_binsum_type));
    FTM_MSG_1(FTM_HIGH, "ftm_wcdma_fetch_fbrx_noise: no valid NV enum for the band %d, using 0 noise cancellation", band);
    return;
  }

  FTM_MSG_2(FTM_HIGH, "ftm_wcdma_fetch_fbrx_noise: NV id =%d, band = %d",
            nv_id, band);
  /* read from the correct NV and populate the data structure for the current band from the NV*/
  nv_read_status = rfcommon_nv_load_fbrx_noise_data_from_nv(&fbrx_noise_struct_from_nv, nv_id,NULL, 0, NULL);

  if (nv_read_status == FALSE)
  {
    memset((void *)fbrx_noise_from_nv, 0, sizeof(rfcommon_nv_fbrx_noise_binsum_type));
    FTM_MSG_1(FTM_HIGH, "ftm_wcdma_fetch_fbrx_noise: NV data read failed for band %d, using 0 noise cancellation",band);
    return;
  }

  nv_data_valid = fbrx_noise_struct_from_nv.data_valid[0]; // 0-th entry is valid WCDMA location

  if (nv_data_valid == FALSE)
  {
    memset((void *)fbrx_noise_from_nv, 0, sizeof(rfcommon_nv_fbrx_noise_binsum_type));
    FTM_MSG_1(FTM_HIGH, "ftm_wcdma_fetch_fbrx_noise: NV indicates invalid data for band %d, using 0 noise cancellation",band);
    return;
  }

  memscpy((void *)fbrx_noise_from_nv, sizeof(rfcommon_nv_fbrx_noise_binsum_type),
          (void*)(&fbrx_noise_struct_from_nv.rfcommon_nv_fbrx_noise_binsum[0]),
          sizeof(rfcommon_nv_fbrx_noise_binsum_type));

  return;
}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Interpolate FBRX gain v.s. channel based on FBRX gain NV

  @details

  @return
  None
*/
void ftm_wcdma_calculate_fbrx_gain
(
  rfcommon_nv_fbrx_gain_vs_freq_type fbrx_gain_nv_data,
  uint16 channel,
  ftm_common_selftest_fbrx_attn_type gain_stage,
  float *fbrx_gain
)
{
  uint8 ch_size, idx;
  int16 ch_idx, gain_idx;
  double x0, y0, x1, y1, y;

  gain_idx = (int16) gain_stage;

  ch_size = fbrx_gain_nv_data.fbrx_gain_chan_size;

  if (ch_size == 0) {

    FTM_MSG(FTM_ERROR,"ftm_wcdma_calculate_fbrx_gain: NV has 0 channel data, error" );

    return;
  }

  if (ch_size == 1) {

    /* only has one data point */

    *fbrx_gain = (float) fbrx_gain_nv_data.fbrx_gain[0][gain_idx].gain;

    FTM_MSG_1(FTM_HIGH,"ftm_wcdma_calculate_fbrx_gain: NV has 1 channel data, FBRX gain = %d",  (int16) *fbrx_gain);

    return;

  } else
  {
    /* Has more than one data point */

    /* Take care of the extreme cases*/

    if (channel<=fbrx_gain_nv_data.fbrx_gain_chan[0])
    {
      *fbrx_gain = (float)fbrx_gain_nv_data.fbrx_gain[0][gain_idx].gain;
      FTM_MSG_3(FTM_HIGH,"ftm_wcdma_calculate_fbrx_gain: NV has %d channel data, channel %d <channel[0], FBRX gain = %d",
                ch_size, channel, (int16) *fbrx_gain);
      return;
    }

    if (channel>=fbrx_gain_nv_data.fbrx_gain_chan[ch_size-1])
    {
      *fbrx_gain = (float)fbrx_gain_nv_data.fbrx_gain[ch_size-1][gain_idx].gain;
      FTM_MSG_3(FTM_HIGH,"ftm_wcdma_calculate_fbrx_gain: NV has %d channel data, channel %d >channel[max], FBRX gain = %d",
                ch_size, channel, (int16) *fbrx_gain);
      return;
    }

    /* Determine location of the current channel in the list */

    ch_idx = 0;

    for (idx=0; idx<ch_size; idx++)
    {
      if (channel <= fbrx_gain_nv_data.fbrx_gain_chan[idx])
      {
        ch_idx = idx;
        break;
      }
    }

    FTM_MSG_3(FTM_HIGH,"ftm_wcdma_calculate_fbrx_gain: channel[%d]<channel = %d <channel[%d]",
              idx-1, channel, idx);

    /* linear intepolation */

    x0 = (double) fbrx_gain_nv_data.fbrx_gain_chan[ch_idx-1];

    x1 = (double) fbrx_gain_nv_data.fbrx_gain_chan[ch_idx];

    y0 = (double) fbrx_gain_nv_data.fbrx_gain[ch_idx-1][gain_idx].gain;

    y1 = (double) fbrx_gain_nv_data.fbrx_gain[ch_idx][gain_idx].gain;

    y = y0 + (y1 - y0)*((double)channel-x0)/(x1-x0);

    *fbrx_gain = (float) y;

    FTM_MSG_1(FTM_HIGH,"ftm_wcdma_calculate_fbrx_gain: interpolate, FBRX gain = %d",
                (int16) *fbrx_gain);

    //*fbrx_gain = fbrx_gain_nv_data.fbrx_gain[ch_idx][gain_idx].gain;
  }

  return;

}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Determine FBRX gain NV based on WCDMA band

  @details

  @return
  None
*/

boolean ftm_wcdma_obtain_fbrx_gain_nv_item
(
   rfcom_wcdma_band_type band,
   rfnv_item_id_enum_type *nv_item
)
{
  boolean ret_value = TRUE;
  switch (band)
  {

  case   RFCOM_BAND_IMT:
    *nv_item = RFNV_WCDMA_B1_FBRX_GAIN_FREQ_I;
    break;

  case   RFCOM_BAND_1900:
    *nv_item = RFNV_WCDMA_B2_FBRX_GAIN_FREQ_I;
    break;

  case   RFCOM_BAND_BC3:
    *nv_item = RFNV_WCDMA_B3_FBRX_GAIN_FREQ_I;
    break;

  case   RFCOM_BAND_BC4:
    *nv_item = RFNV_WCDMA_B4_FBRX_GAIN_FREQ_I;
    break;

  case   RFCOM_BAND_800:
    *nv_item = RFNV_WCDMA_B5_FBRX_GAIN_FREQ_I;
    break;

  case   RFCOM_BAND_BC8:
    *nv_item = RFNV_WCDMA_B8_FBRX_GAIN_FREQ_I;
    break;

  default:

    *nv_item = RFNV_WCDMA_B1_FBRX_GAIN_FREQ_I;
    ret_value = FALSE;
  }

  return ret_value;
}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Process FBRX gain NV, inteporlate gain v.s. freq, and calculate power dBm
  at antenna for a given band/channel RMS power

  @details

  @return
  None
*/
void ftm_wcdma_calculate_power
(
   uint32 pwr_rms,
   rfcom_wcdma_band_type band,
   uint16 channel,
   ftm_common_selftest_fbrx_attn_type gain_stage,
   int16 *pwr_ant
)
{

   rfnv_item_id_enum_type nv_id;
   rfcommon_nv_fbrx_gain_vs_freq_type fbrx_gain_nv_data;
   double nbee_rms_offset = 0;
   float fbrx_gain;
   boolean nv_valid;

   if (pwr_ant != NULL) {

   /* determine the correct NV item location */

   nv_valid =  ftm_wcdma_obtain_fbrx_gain_nv_item(band, &nv_id);

   if (nv_valid)
   {

        FTM_MSG_4(FTM_HIGH, "ftm_wcdma_calculate_power: NV id =%d, channel = %d, band = %d, 200log10(power_rms)=%d",
                  nv_id, channel, band, (int16)(200*log10(pwr_rms)));


      /* read from the correct NV and populate the data structure for the current band from the NV*/

      rfcommon_nv_load_fbrx_gain_from_nv
         (&fbrx_gain_nv_data, nv_id,
          NULL, 0, NULL);


      /* determine the NBEE and RMS Power offset */

      nbee_rms_offset = 0;


      /* interpolate the FBRX cal data across freq to get the calculated gain for the current channel*/

      ftm_wcdma_calculate_fbrx_gain(fbrx_gain_nv_data, channel, gain_stage, &fbrx_gain);

      /* power at ant is in dBm*100 */

      *pwr_ant = (int16)FTM_SELFTEST_ROUND(2000 * log10(pwr_rms) - (fbrx_gain + nbee_rms_offset*100));


      FTM_MSG_1(FTM_HIGH, "ftm_wcdma_calculate_power: using interpolated FBRX gain NV, calculated pwr_ant = %d",
                 * pwr_ant);
   }
   else
   {

      /* band invalid, no NV stored, use default to bail out */
      *pwr_ant = (int16)FTM_SELFTEST_ROUND((20 * log10(pwr_rms) - nbee_rms_offset)*100);

      FTM_MSG_1(FTM_HIGH, "ftm_wcdma_calculate_power: no valid NV for the band, using default FBRX gain pwr_ant = %d",
                 * pwr_ant);
   }
}
}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Calculate power dBm at antenna

  @details

  @return
  power at antenna
*/

int16 ftm_wcdma_ant_pwr
(
  uint32 pwr_rms
)
{
  rfcom_wcdma_band_type band;
  uint16 channel;
  ftm_common_selftest_fbrx_attn_type gain_stage;
  int16 ant_pwr;

  band = rfwcdma_mc_state.car_path_state[RF_PATH_0][0].curr_band_tx;

  channel = rfwcdma_mc_state.car_path_state[RF_PATH_0][0].curr_chan_tx;


  gain_stage = ftm_common_get_coupler_wtr_fb_path_state();

  FTM_MSG_3(FTM_HIGH,"ftm_wcdma_ant_pwr: band = %d, channel = %d, gain_stage = %d",
          band, channel, gain_stage);

  ftm_wcdma_calculate_power(pwr_rms, band, channel,  gain_stage, &ant_pwr);

  FTM_MSG_1(FTM_HIGH,"ftm_wcdma_ant_pwr return value: pwr_ant = %d (dBm * 10)",
            ant_pwr);

  return ant_pwr;
}
#endif

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Initialize selftest params

  @details

  @return
  0 for success, 1 for failure
*/
uint8 ftm_wcdma_selftest_init
(
  ftm_wcdma_selftest_meas_params_struct *params,
  ftm_rsp_pkt_type *rsp_pkt,
  // ftm_common_power_per_bin_struct_type *noise_pwr_to_subtract,
  ftm_rf_wcdma_payload_tx_params_type **ftm_rf_wcdma_payload_tx_params,
  uint8 *ftm_selftest_pwr_only
)
{
  // memset((void*)noise_pwr_to_subtract, 0, sizeof(ftm_common_power_per_bin_struct_type));
  /* Get pointer to static structures */
  *ftm_rf_wcdma_payload_tx_params = &(ftm_rf_fbrx_iq_acquired_samples_data->meas_results);

  params->enable_tx_pwr_meas = (params->tx_measurement_config & FTM_FBRX_MEASURE_TX_POWER)? TRUE: FALSE;
  params->enable_aclr_meas   = (params->tx_measurement_config & FTM_FBRX_MEASURE_ACLR)? TRUE: FALSE;
  params->enable_evm_meas    = (params->tx_measurement_config & FTM_FBRX_MEASURE_EVM)? TRUE: FALSE;
  // params->enable_vswr_meas   = (tx_measurement_config & FTM_FBRX_MEASURE_VSWR)? TRUE: FALSE;

  // if (((params->enable_tx_pwr_meas) || (params->enable_vswr_meas)) &&
  //     ((!params->enable_evm_meas) && (!params->enable_aclr_meas)) ) {

  //   *ftm_selftest_pwr_only = TRUE;
  // }
  // else
  // {
  //   *ftm_selftest_pwr_only = FALSE;
  // }

  // FTM_MSG_1( FTM_HIGH, "WCDMA self_test TX or VSWR only =  %d ", *ftm_selftest_pwr_only );

   /* print some debug */
   FTM_MSG_3(FTM_MED, "Inputs to Meas Tx params command -> num_averages : %d : capture_offset : %d : tx_measurement_config : %x",
             params->num_averages,
             params->capture_offset,
             params->tx_measurement_config);

  /***************** Fill up RSP packet with standard data *****************/

  // Fill in the constant fields of the response packet.
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->cmd_code = 75;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->subsys_id = FTM;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->subsys_cmd_code = (params->device == RFM_DEVICE_0) ? FTM_WCDMA_C : FTM_WCDMA_RX_2_C ;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_rf_cmd = FTM_RF_MEAS_TX_PARAMS;

  // Start with sucess flag and change it if we fail any check conditions.
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_error_code = FTM_FBRX_TEST_SUCCESS;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->num_averages = params->num_averages;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->capture_offset = params->capture_offset;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->tx_measurement_config = params->tx_measurement_config;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->payload_size = FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE;

  // Fill in the constant fields of response packet.Size of the packet will be based on the requested Tx measurement config
  rsp_pkt->cmd = FTM_RSP_DO_NEW_PKT;
  rsp_pkt->pkt_payload = ftm_rf_fbrx_meas_tx_params_rsp_pkt;
  rsp_pkt->delete_payload = FALSE;
  rsp_pkt->pkt_len = FTM_RF_MEAS_TX_PARAMS_HDR_SIZE;

  // if (!(rfm_get_calibration_state())) {

  //    if not in calibration mode, obtain and set overral FB path gain stage
  //   ftm_wcdma_obtain_fbrx_gain_stage();

  //   /* otherwise use the settings set during cal */
  // }

  if( ftm_get_current_state(params->device) != FTM_STATE_WCDMA)
  {
    ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_error_code = FTM_FBRX_TEST_GENERAL_FAILURE;
    FTM_MSG(FTM_ERROR, "FTM Mode is not WCDMA, bailing out");

    return 1;
  }
  return 0;
}


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Initialize sample buffer and fft params for ACLR

  @details

  @return
  0 for success, 1 for failure
*/
uint8 ftm_wcdma_selftest_buffer_init
(
  const uint16 fft_size,
  const rfcom_lte_bw_type lte_bw,

  // ftm_common_power_per_bin_struct_type *noise_pwr_to_subtract,
  // rfcommon_nv_fbrx_noise_binsum_type *fbrx_noise_from_nv,
  uint32 *sample_buffer_size,
  int32 **sample_buffer,
  uint16 *pcfl_data_size
)
{
  // ftm_wcdma_fetch_fbrx_noise(fbrx_noise_from_nv);
  // noise_pwr_to_subtract->accum_count = 1;
  // noise_pwr_to_subtract->carrier_chan = 0;
  // noise_pwr_to_subtract->eutra_neg = (int64)fbrx_noise_from_nv->eutra_neg;
  // noise_pwr_to_subtract->eutra_pos = (int64)fbrx_noise_from_nv->eutra_pos;
  // noise_pwr_to_subtract->utra1_neg = (int64)fbrx_noise_from_nv->utra1_neg;
  // noise_pwr_to_subtract->utra1_pos = (int64)fbrx_noise_from_nv->utra1_pos;
  // noise_pwr_to_subtract->utra2_neg = (int64)fbrx_noise_from_nv->utra2_neg;
  // noise_pwr_to_subtract->utra2_pos = (int64)fbrx_noise_from_nv->utra2_pos;

  /* Buffer for samples and FFT - 32 bit I/32 bit Q*/
  *sample_buffer_size = (fft_size * 2 * 4);
  *sample_buffer = (int32 *)ftm_malloc(*sample_buffer_size);

  /* If pointer is NULL even after allocating memory, then declare it as failure*/
  if (*sample_buffer == NULL)
  {
     ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_error_code = FTM_FBRX_TEST_MEM_ALLOCATION_FAILURE;
     FTM_MSG(FTM_ERROR, "Unable to allocate memory for Rx - IQ buffer");
     return 1;
  }

  /* init FFT - required before calling FFT API for ACLR*/
  ftm_common_init_fft_data(FTM_STATE_WCDMA, lte_bw);

  *pcfl_data_size = (fft_size * 20) / 8;

  return 0;
}


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Capture iq samples and process via fft

  @details

  @return
  None
*/
void ftm_wcdma_selftest_capture_and_fft
(
  const ftm_wcdma_selftest_meas_params_struct params,
  const uint16 fft_size,
  const uint16 pcfl_data_size,
  const uint16 sample_capture_size,
  const uint32 sample_buffer_size,

  int32 *sample_buffer,
  uint32 *evm_value_acc,
  uint64 *rx_rms_mag_acc
)
{

  uint8 average_count  = 0;
  boolean capture_done = FALSE;
  int32 *iq_buf_rx_ptr = NULL;
  uint16 fft_step      = 0;
  uint16 evm_value     = 0;
  uint32 rx_rms_mag    = 0;

  uint32 capture_type = (params.enable_tx_pwr_meas) | (params.enable_aclr_meas<<1) | (params.enable_vswr_meas<<3);

  for (average_count = 0; average_count < params.num_averages; average_count++)
  {
    /* Trigger I/Q capture*/
    capture_done = ftm_common_fbrx_iq_capture_processing(
                                       FTM_RF_TECH_WCDMA,
                                       1, /* FBRX chain*/
                                       capture_type,
                                       sample_capture_size,
                                       &(ftm_rf_fbrx_iq_acquired_samples_data->iq_buf_rx),
                                       &(ftm_rf_fbrx_iq_acquired_samples_data->iq_buf_tx),
                                       &evm_value,
                                       &rx_rms_mag);

    if (capture_done == TRUE)
    {
      ftm_rf_fbrx_iq_acquired_samples_data->req_samples = sample_capture_size;
      ftm_rf_fbrx_iq_acquired_samples_data->data_available = TRUE;
    }

    if (params.enable_aclr_meas && sample_buffer != NULL)
    {
      iq_buf_rx_ptr = ftm_rf_fbrx_iq_acquired_samples_data->iq_buf_rx;

      for (fft_step = 0; fft_step < sample_capture_size / fft_size; fft_step++)
      {
         /*get FFT_SIZE samples from the Rx buffer*/
         memscpy((void *)sample_buffer, sample_buffer_size, (void *)iq_buf_rx_ptr, pcfl_data_size);

         /* increase the ptr to point to next Rx samples*/
         iq_buf_rx_ptr = iq_buf_rx_ptr + (pcfl_data_size / sizeof(int32));

         /* Parse the data*/
         ftm_common_parse_PCFL20_to_IQ(FTM_STATE_WCDMA, sample_buffer, fft_size);

         /* take FFT of this data*/
         ftm_common_get_samples_fft_avg(fft_size, sample_buffer);
      }
    }

    if (params.enable_evm_meas)
    {
      *evm_value_acc = *evm_value_acc + evm_value;
      FTM_MSG_3(FTM_HIGH, "ftm_wcdma_selftest_capture_and_fft: EVM reading #%d :  %d, accum : %d ", average_count, evm_value, *evm_value_acc);
    }

    if (params.enable_tx_pwr_meas|params.enable_vswr_meas)
    {
      *rx_rms_mag_acc = *rx_rms_mag_acc + rx_rms_mag;
      FTM_MSG_3(FTM_HIGH, "ftm_wcdma_selftest_capture_and_fft: IQ RMS Reading #%d: RxRMS=%d, RxRMSAcc=%d", average_count, rx_rms_mag, *rx_rms_mag_acc);
    }
  }
}


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Calculate average tx power

  @details

  @return
  None
*/
void ftm_wcdma_selftest_calculate_tx_power
(
  const ftm_wcdma_selftest_meas_params_struct params,

  uint32 *rx_rms_mag_avg,
  uint64 *rx_rms_mag_acc
)
{
  *rx_rms_mag_avg = (uint32)(*rx_rms_mag_acc/params.num_averages);
  FTM_MSG_3(FTM_HIGH, "ftm_wcdma_selftest_calculate_tx_power: Averaging :  %d / %d = %d", *rx_rms_mag_acc, params.num_averages, *rx_rms_mag_avg);
  if (rfm_get_calibration_state())
  {
    /* Self-test cal mode */
    /* for cal unit is dbm*100 */
    ftm_rf_fbrx_iq_acquired_samples_data->meas_results.tx_power= (int16)FTM_SELFTEST_ROUND(2000 * log10(*rx_rms_mag_avg));
  }
  else
  {
    ftm_rf_fbrx_iq_acquired_samples_data->meas_results.tx_power  = ftm_wcdma_ant_pwr(*rx_rms_mag_avg); /* calculate the TX power at antenna */
  }
}
#endif


// Commented out for 9x35 bringup
#if 0
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Reverse coupler direction and calculate vswr

  @details

  @return
  None
*/
void ftm_wcdma_selftest_calculate_vswr
(
  const ftm_wcdma_selftest_meas_params_struct params,
  const uint16 sample_capture_size,

  uint32 *rx_rms_reverse_mag_avg,
  uint32 *rx_rms_mag_avg,
  uint64 *rx_rms_reverse_mag_acc,
  uint8 *status
)
{
  uint8 average_count = 0;
  boolean capture_done = FALSE;
  uint32 rx_rms_reverse_mag = 0;
  uint16 evm_value = 0;

  ftm_common_set_coupler_direction(COUPLER_DIRECTION_REVERSE);
  for (average_count = 0; average_count < params.num_averages; average_count++)
  {
    /* Trigger I/Q capture*/
    capture_done = ftm_common_fbrx_iq_capture_processing(
                                               FTM_RF_TECH_WCDMA,
                                               1,
                                               ENABLE_VSWR_MEAS,
                                               sample_capture_size,
                                               &(ftm_rf_fbrx_iq_acquired_samples_data->iq_buf_rx),
                                               &(ftm_rf_fbrx_iq_acquired_samples_data->iq_buf_tx),
                                               &evm_value,
                                               &rx_rms_reverse_mag);
    *rx_rms_reverse_mag_acc = *rx_rms_reverse_mag_acc + rx_rms_reverse_mag;
    FTM_MSG_4(FTM_MED, "ftm_wcdma_selftest_calculate_vswr: Forward Avg %d Reverse reading #%d = %d , Reverse Acc = %d",
                 *rx_rms_mag_avg,
                 average_count,
                 rx_rms_reverse_mag,
                 *rx_rms_reverse_mag_acc);

  }

  *rx_rms_reverse_mag_avg = (uint32)(*rx_rms_reverse_mag_acc/params.num_averages);
  FTM_MSG_3(FTM_HIGH, "ftm_wcdma_selftest_calculate_vswr: Reverse IQ RMS Averaging :  %d / %d = %d",
               *rx_rms_reverse_mag_acc,
               params.num_averages,
               *rx_rms_reverse_mag_avg);

  if ( *rx_rms_mag_avg > *rx_rms_reverse_mag_avg )
  {
    ftm_rf_fbrx_iq_acquired_samples_data->meas_results.vswr_ratio_dB10 =
                                (200 * log10(*rx_rms_mag_avg)) - (200 * log10(*rx_rms_reverse_mag_avg));

    FTM_MSG_4(FTM_HIGH, "ftm_wcdma_selftest_calculate_vswr: Reverse IQ RMS Averaging :  %d / %d = %d, VSWR(dB10) = %d",
                 *rx_rms_reverse_mag_acc,
                 params.num_averages,
                 *rx_rms_reverse_mag_avg,
                 ftm_rf_fbrx_iq_acquired_samples_data->meas_results.vswr_ratio_dB10);
  }
  else
  {
    ftm_rf_fbrx_iq_acquired_samples_data->meas_results.vswr_ratio_dB10 = 9999;
    *status = FTM_FBRX_TEST_GENERAL_FAILURE;
    FTM_MSG_2(FTM_ERROR, "ftm_wcdma_selftest_calculate_vswr: Reverse IQ RMS Averaged %d > Forward IQ RMS Averaged %d, returning 9999",
                 *rx_rms_reverse_mag_avg,
                 *rx_rms_mag_avg);

  }
  ftm_common_set_coupler_direction(COUPLER_DIRECTION_FORWARD);
}
#endif

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Calculate ACLR from sample data

  @details

  @return
  None
*/
void ftm_wcdma_selftest_calculate_aclr
(
  const uint16 fft_size,
  const rfcom_lte_bw_type lte_bw,
  // const ftm_common_power_per_bin_struct_type noise_pwr_to_subtract,

  int32 *sample_buffer
)
{

  ftm_common_aclr_result_struct_type ftm_common_aclr_result;

  ftm_common_get_aclr(fft_size, FTM_STATE_WCDMA, lte_bw, &ftm_common_aclr_result);

  ftm_common_deinit_fft_data();

  ftm_free(sample_buffer);

  /* Copy ACLR values to FTM data structures*/
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[0] = ftm_common_aclr_result.aclr1_neg;
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[1] = ftm_common_aclr_result.aclr1_pos;
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[2] = ftm_common_aclr_result.aclr2_neg;
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[3] = ftm_common_aclr_result.aclr2_pos;
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[4] = ftm_common_aclr_result.aclr_eutra_neg;
  ftm_rf_fbrx_iq_acquired_samples_data->meas_results.aclr_value[5] = ftm_common_aclr_result.aclr_eutra_pos;
}


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Populate response packet for dispatcher

  @details

  @return
  None
*/
void ftm_wcdma_selftest_populate_rsp_pkt
(
  const ftm_wcdma_selftest_meas_params_struct params,
  const ftm_rf_wcdma_payload_tx_params_type *ftm_rf_wcdma_payload_tx_params,
  const uint32 *iq_capture_frequency,
  const uint32 *max_samples_per_diag_packet_tx,
  const uint32 *max_samples_per_diag_packet_rx,

  ftm_rsp_pkt_type *rsp_pkt
)
{

  int payload_size_count = 0;

  // Adding payload secion of response packet.
  // Check if Tx Power measurement type is enabled and add Tx Power results to the response packet
  if (params.tx_measurement_config & FTM_FBRX_MEASURE_TX_POWER)
  {
    /*Populate Tx power data*/
    memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
            (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
            &(ftm_rf_wcdma_payload_tx_params->tx_power),
            sizeof(ftm_rf_wcdma_payload_tx_params->tx_power));
    payload_size_count += sizeof(ftm_rf_wcdma_payload_tx_params->tx_power);
  }
  // Check if EVM measurement type is enabled and add EVM results to the response packet
  if (params.tx_measurement_config & FTM_FBRX_MEASURE_EVM)
  {
    /*Populate EVM data*/
    memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
            (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
            &(ftm_rf_wcdma_payload_tx_params->evm_value),
            sizeof(ftm_rf_wcdma_payload_tx_params->evm_value));
    payload_size_count += sizeof(ftm_rf_wcdma_payload_tx_params->evm_value);
  }
  // Check if ACLR measurement type is enabled and add ACLR results to the response packet
  if (params.tx_measurement_config & FTM_FBRX_MEASURE_ACLR)
  {
    /*Populate ACLR data*/
    memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
           (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
           &(ftm_rf_wcdma_payload_tx_params->aclr_value),
           sizeof(ftm_rf_wcdma_payload_tx_params->aclr_value));
    payload_size_count += sizeof(ftm_rf_wcdma_payload_tx_params->aclr_value);
  }
  if ((params.tx_measurement_config & FTM_FBRX_SAVE_TX_IQ_SAMPLES)
    || (params.tx_measurement_config & FTM_FBRX_SAVE_RX_IQ_SAMPLES))
  {
    /* Check if Tx IQ Capture measurement type is enabled and add Tx IQ capture results to the response packet*/
    if (params.tx_measurement_config & FTM_FBRX_SAVE_TX_IQ_SAMPLES)
    {
      /*Populate Tx IQ capture data format inforamtion*/
      ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count++] = (uint8) FTM_WCDMA_FBRX_IQ_SAMPLE_DATA_FORMAT_TX;
      ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count++] = (uint8) FTM_FBRX_SAMPLE_SIZE_9216;

      /* Todo Check for Tx BW and accordingly choose sampling frequency*/
      memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
         (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
         iq_capture_frequency,
         sizeof(*iq_capture_frequency));

      payload_size_count += sizeof(*iq_capture_frequency);

      memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
         (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
         max_samples_per_diag_packet_tx,
         sizeof(*max_samples_per_diag_packet_tx));

      payload_size_count += sizeof(*max_samples_per_diag_packet_tx);
    }
    // Check if Rx IQ Capture measurement type is enabled and add Rx IQ capture results to the response packet
    if (params.tx_measurement_config & FTM_FBRX_SAVE_RX_IQ_SAMPLES)
    {
      /*Populate Rx IQ capture data format inforamtion*/
      ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count++] = (uint8) FTM_WCDMA_FBRX_IQ_SAMPLE_DATA_FORMAT_RX;
      ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count++] = (uint8) FTM_FBRX_SAMPLE_SIZE_9216;

      memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
         (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
         iq_capture_frequency,
         sizeof(*iq_capture_frequency));
      payload_size_count += sizeof(*iq_capture_frequency);

      memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
         (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
         max_samples_per_diag_packet_rx,
         sizeof(*max_samples_per_diag_packet_rx));
      payload_size_count += sizeof(*max_samples_per_diag_packet_rx);
    }
  }
  // Check if VSWR measurement type is enabled and append VSWR result
  if (params.tx_measurement_config & FTM_FBRX_MEAS_VSWR)
  {
    /*Populate VSWR data*/
    memscpy(&ftm_rf_fbrx_meas_tx_params_rsp_pkt->result[payload_size_count],
           (FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE - payload_size_count),
           &(ftm_rf_wcdma_payload_tx_params->vswr_ratio_dB10),
           sizeof(ftm_rf_wcdma_payload_tx_params->vswr_ratio_dB10));
    payload_size_count += sizeof(ftm_rf_wcdma_payload_tx_params->vswr_ratio_dB10);
  }

  if (payload_size_count > FTM_RF_MEAS_TX_PARAMS_MAX_PAYLOAD_SIZE)
  {
    ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_error_code = FTM_FBRX_TEST_EXCEEDS_PAYLOAD_SIZE_LIMIT;
    FTM_MSG( FTM_HIGH, "FTM_FBRX_TEST_EXCEEDS_PAYLOAD_SIZE_LIMIT");
  } else
  {
    rsp_pkt->pkt_len +=  payload_size_count;
    ftm_rf_fbrx_meas_tx_params_rsp_pkt->payload_size = payload_size_count;
    FTM_MSG_1( FTM_HIGH, "Payload size = %d", payload_size_count);
  }

}


/*----------------------------------------------------------------------------*/
/*!
  @brief
  Calculates selftest values, primary function

  @details

  @return
  0 for success, 1 for failure
*/
uint8 ftm_wcdma_selftest
(
  ftm_wcdma_selftest_meas_params_struct params,

  ftm_rsp_pkt_type *rsp_pkt,
  ftm_rf_fbrx_iq_acquired_samples_data_type *ftm_rf_fbrx_iq_acquired_samples_data_input,
  uint8 *ftm_selftest_pwr_only,
  ftm_rf_fbrx_meas_tx_params_rsp_pkt_type *ftm_rf_fbrx_meas_tx_params_rsp_pkt_input
)
{

  uint8 status                          = FTM_FBRX_TEST_SUCCESS;
  uint32 iq_capture_frequency           = FTM_WCDMA_FBRX_IQ_SAMPLE_FREQ;
  uint32 max_samples_per_diag_packet_tx = FTM_WCDMA_FBRX_IQ_DIAG_MAX_SAMPLES_TX;
  uint32 max_samples_per_diag_packet_rx = FTM_WCDMA_FBRX_IQ_DIAG_MAX_SAMPLES_RX;

  rfcom_lte_bw_type lte_bw = RFCOM_BW_LTE_INVALID;      /* This parameter is LTE specific and will be ignored in WCDMA case */

  uint32 sample_buffer_size = 0;
  int32 *sample_buffer      = NULL;
  ftm_rf_wcdma_payload_tx_params_type *ftm_rf_wcdma_payload_tx_params;

  uint32 evm_value_acc = 0;
  uint16 evm_value_avg = 0;

  uint64 rx_rms_mag_acc         = 0;
  // uint32 rx_rms_mag_avg         = 0;
  // uint64 rx_rms_reverse_mag_acc = 0;
  // uint32 rx_rms_reverse_mag_avg = 0;

  uint16 pcfl_data_size      = 0;
  uint16 sample_capture_size = 9216;
  uint16 fft_size            = 1024;
  /* binsum power structure containing noise to subtract to pass into ACLR calc API*/
  // ftm_common_power_per_bin_struct_type noise_pwr_to_subtract;
  /* binsum power structure to contain noise to subtract from cal nv */
  // rfcommon_nv_fbrx_noise_binsum_type    fbrx_noise_from_nv;

  uint8 selftest_error = 0;

  ftm_rf_fbrx_iq_acquired_samples_data = ftm_rf_fbrx_iq_acquired_samples_data_input;
  ftm_rf_fbrx_meas_tx_params_rsp_pkt   = ftm_rf_fbrx_meas_tx_params_rsp_pkt_input;

  selftest_error = ftm_wcdma_selftest_init(
                             &params,
                             rsp_pkt,
                             // &noise_pwr_to_subtract,
                             &ftm_rf_wcdma_payload_tx_params,
                             ftm_selftest_pwr_only);
  if (selftest_error)
  {
    return 1;
  }


  if (params.enable_aclr_meas)
  {
    selftest_error = ftm_wcdma_selftest_buffer_init(
                                fft_size,
                                lte_bw,
                                // &noise_pwr_to_subtract,
                                // &fbrx_noise_from_nv,
                                &sample_buffer_size,
                                &sample_buffer,
                                &pcfl_data_size);
    if (selftest_error)
    {
      return 1;
    }
  }

  ftm_wcdma_selftest_capture_and_fft(
                 params,
                 fft_size,
                 pcfl_data_size,
                 sample_capture_size,
                 sample_buffer_size,
                 sample_buffer,
                 &evm_value_acc,
                 &rx_rms_mag_acc);

  // if (params.enable_tx_pwr_meas|params.enable_vswr_meas)
  // {
  //   ftm_wcdma_selftest_calculate_tx_power(
  //                  num_averages,
  //                  &rx_rms_mag_avg,
  //                  &rx_rms_mag_acc);
  // }

  if (params.enable_evm_meas)
  {
    evm_value_avg = (uint16)(evm_value_acc/params.num_averages);
    ftm_rf_fbrx_iq_acquired_samples_data->meas_results.evm_value = evm_value_avg;
    FTM_MSG_3(FTM_HIGH, "ftm_wcdma_selftest: EVM Averaging :  %d / %d = %d", evm_value_acc, params.num_averages, evm_value_avg);
  }


  /* If VSWR measuremnt is enabled change coupler direction and again measure the IQ RMS based power estimate*/
  // if(params.enable_vswr_meas)
  // {
  //   ftm_wcdma_selftest_calculate_vswr(
  //                  params,
  //                  sample_capture_size,
  //                  &rx_rms_reverse_mag_avg,
  //                  &rx_rms_mag_avg,
  //                  &rx_rms_reverse_mag_acc,
  //                  &status);
  // }


  if (params.enable_aclr_meas)
  {
    ftm_wcdma_selftest_calculate_aclr(
                   fft_size,
                   lte_bw,
                   // noise_pwr_to_subtract,
                   sample_buffer);
  }

  // update error code
  ftm_rf_fbrx_meas_tx_params_rsp_pkt->ftm_error_code = status;

  if (status == FTM_FBRX_TEST_SUCCESS)
  {
    FTM_MSG( FTM_HIGH, "ftm_wcdma_selftest: Rsp pkt being populated");
    ftm_wcdma_selftest_populate_rsp_pkt(
                   params,
                   ftm_rf_wcdma_payload_tx_params,
                   &iq_capture_frequency,
                   &max_samples_per_diag_packet_tx,
                   &max_samples_per_diag_packet_rx,
                   rsp_pkt);
  }
  else
  {
    FTM_MSG_1( FTM_HIGH, "ftm_wcdma_selftest: Failed to populate rsp pkt, selftest status = %d", status);
    return 1;
  }


  return 0;

}

