/*!
  @file
  rfwcdma_core_rxctl.c

  @brief
  Provides WCDMA Rx control functionality

  @details

*/

/*===========================================================================

Copyright (c) 2008, 2009, 2010, 2011,2012 by Qualcomm Technologies, Inc.  All Rights Reserved.
Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rftech_wcdma/rf/core/src/rfwcdma_core_rxctl.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/14/14   aa     Fix overflow in Rx CGAGC params calculation
10/07/14   kr      Implement rf/fw intf. to set dc gain loop. 
09/11/14   ak      Added Support to Pick the Static DVGA gain offsets from RXLM SS
10/09/14   dw      Fix LNA frequency offset 2/3 not applied correctly
09/03/14   nv      Disable TxAGC during Tx bw change
08/08/14   zhh     RF warmup profiling
07/23/14   dw      Added LNA dependent RSB programming support
08/04/14   rmb     Change in name in the usage of APIs.
06/23/14   zhh     implement ASM sleep during retune or any ifreq/irat 
06/06/14   dw      Added rfwcdma_core_rxctl_update_alt_path
06/05/14   dw      Added rfwcdma_core_rxctl_disable_sleep_asm()
06/06/14   ac      nbr support
04/18/14   rmb     Add IRAT meas enum info to config_band and config_chan APIs.
04/09/14   dw      Add Rx Cal mode flag in AGC command for NBEE
04/07/14   ac      jammer detect support
04/03/14   dw      Add dynamic RxLM update support
03/25/14   rmb     Change send RxAGC command API to use a new common API. 
03/24/14   rmb     Move the btf delay paramter udpate from Tx config to Rx config band.
03/13/14   rmb     Pass the IRAT flag to config chan and band API.
03/11/13   dw      Add Bolt DCHSUPA support
03/03/14   dw      Resetting LNA state in DRx disable (RF ON)
02/27/14   rmb     Add fix to tune to channel for multi-carrier scenario.
02/21/14   rmb     Add global NV pointers for each of band specific static NVs.
02/13/14   dw      [1]Change AGC command function to support multiple device
                   [2]Code cleanup
02/14/14    rmb    Computation for CGAGC Default LNA State change from G2 to G1.
02/05/14   dw      Increase generic script size, clear buffer after creation
01/31/14   rmb     Added new FED API to get RX information.
01/02/14   dw      [1]Bolt cleanup on legacy mdsp function call 
                   [2]Added warm time, PLL settling time and RF transaction time to DM interface
12/12/13   rmb     Fix to pick up NVs for the band asked.
10/28/13   dw      Add LNA state logging support
10/07/13   sbo     Fixed bug in rxctl_update_freq_offsets api to compare with freq.
9/25/13    vbh     Warning fixes 
09/24/13   dw      Added default lna_gain_state_oride setting
09/20/13   dw      Fix FGAGC LNA switchpoint query from new NV 
09/18/13   dw      Fix DRx script query
09/16/13   dw      Added support for FTM Cal mode and DRx scripts
09/10/13   dw      [1]Remove DBDC featurization and code clean up.
                   [2]Optimiza the code based on review comments
09/09/13   dw      Core changes for new Bolt MDSP module - new API to config Rx band/chan
08/30/13   vs      Modifying MDSP interface to make common across DI/BO/TR
08/29/13   sbo     Added support for 3 carriers in cgagc table
08/28/13   vs      Removal of featurization and moving to new NV structure
08/23/13   vs      Cleanup of dual carrier APIs
08/16/13   ndb     Clean-up the SPI based antenna tuner support
08/08/13   sma     KW warning fix
07/26/13   ac      compiler warning fix on dime 2.0
07/18/13   ac      DBDC bringup related changes
07/17/13   sma     Clear compiler warning and Fix RSB bug for non 1625/20 dev
07/08/13   sma     Modify support for rsb comp with temperature change
05/06/13   tks     Added support for rsb comp with temperature change
04/04/13   ka      WCDMA state machine updates
02/06/13   rmb     Added support for wakeup optimization.
01/30/13   ka      Updated interfaces to logical device
12/11/12   dw      Add support for DTx and DRx/LPM.
10/16/12   ac      rxagc update for rxd
10/06/12   ac      reverting the change, Wl1 submit once Wli dependency is added to correct branch
09/28/12   ac      device interface to pass band to rfc_wcdma_get_device
06/27/12   sbo     Corrected check for primary in rfwcdma_core_rxctl_set_cpc_drx
05/18/12   dw      Increase the AGC command timeout to 10ms. 
05/14/12   kai     Removed FW RXAGC cmd time out error fatal 
05/08/12   tks     Add support for CPC Drx.  
02/14/12   swb     Clear xoslamflag in FTM upon cmd response
11/04/11   gh      Add ant_tuner state check
10/25/11   ac/ka   Added support for FTM dual carrier freq comp feature 
10/21/11    tnt    Pass non-NULL ptr to rfcommon_ant_tuner_get_bins_and_idx()
10/17/11    gh     Add support for ant_tuner
10/13/11   swb     Added API for freezing Rx AGC accumulator
09/26/11   tks     Fixed KW warning.   
08/24/11    dw     Support polling based AGC cmd response handling
08/21/11    ac     BC19 related changes
06/29/11    dw     Rail cmLnaRise_3 to fix CGAGC hanging in high LNA state
06/23/11    dw     Add rfwcdma_core_rxctl_enable_dual_carrier
06/13/11    dw     Get zero db digital gain from MSM HAL
05/23/11    dw     Remove rfwcdmalib_msm.h 
02/22/11    dw     Include RxLM buffer index in AGC cmd
02/09/11    dw     Add support for WCDMA AGC cmd control
01/05/10    dw     Initial support for RxLM
09/10/10    dw     Add missing Chain1 CGAGC mdsp parameters for DC
08/26/10    av     Rail LNA rise 2. Fixes Ec/Io fluctuations. 
06/28/10    ac     lint fix
06/22/10    ka     KW warning fix
06/09/10    ac     compiler error fix
06/07/10    ac     freq comp for DC mode
05/20/10    tws    Add flag so CGAGC params are only initialised once.
05/12/10    ac     B11 bringup
3/17/10     kguo   Remove feature RF_HAS_WCDMA_RX_DUAL_CARRIER 
3/16/10     kguo   Merged from scmm_9k_final_merge branch to main\latest 
03/03/10    wen    Fixed compilation errors 
02/24/10    xw     Klocwork fix 
01/18/10   kguo    Fixed compiler errors for merged build 
01/10/10   kguo    Merged mdm9k dev branch to main\latest
08/14/09    ka     Added include file required for MDM9x00
08/13/09   jhe     freq comp change
07/29/09   ckl     Fixed type casting error of double data type
07/29/09   ckl     Removed compiler warnings
07/07/09   ckl     Changed cgagc parameters calculation
07/03/09   ckl     Delete mdsp_start_rx_agc function call in enable_rx_agc
03/19/09   ckl     Arrange order of header files
03/17/09   ckl     Added RF_HAS_LINEAR_INTERPOLATION compile option
03/15/09   ckl     Deleted rfnv.h
03/09/09   ckl     Code cleanup
11/06/08   av      Initial version.

============================================================================*/
#include "rfa_variation.h"
#include <math.h>
#include "comdef.h"
#include "rfcom.h"
#include "msg.h"
#include "rfumts.h"
#include "rfnv_wcdma.h"
#include "rfwcdma_mdsp.h"
#include "rfwcdma_msm.h"
#include "rfwcdma_core_rxctl.h"
#include "rfwcdma_core.h"
#include "rfwcdma_mc.h"
#include "wfw_sw_cmd_proc_intf.h"
#include "DALSys.h"
#include "rfnv_wcdma.h"
#include "rfdevice_wcdma_intf.h"
#include "rfcommon_msg.h"
#include "rfwcdma_core_temp_comp.h"
#include "rfwcdma_core_util.h"
#include "rfcommon_core.h"

#include "rfwcdma_mdsp_async.h"
#include "rf_hal_buffer.h"
#include "rfc_card_wcdma.h"
#include "rfwcdma_mdsp_sync.h"
#include "ftm_wcdma_ctl.h"
#include "ftm_wcdma_calv3.h"
#include "rfdevice_wcdma_asm_intf.h"
#include "rfcommon_concurrency_manager.h"
#include "rfcommon_core_utils.h"
#include "rfwcdma_core_antenna_tuner.h"

#include "rfc_wcdma_data.h"
#include "rfcommon_msm.h"
/*---------------------------------------------------------
                   Constant Definitions
---------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/*! 
    @brief
    This number defines Polling interval after AGC command in us 
*/
#define RFWCDMA_CORE_AGC_POLLING_INTERVAL 100

/*----------------------------------------------------------------------------*/
/*! 
    @brief
    This number defines Polling timeout after AGC command in ust 
*/
#define RFWCDMA_CORE_AGC_POLLING_TIMEOUT 10000

/*----------------------------------------------------------------------------*/
/*! 
    @brief
    This number defines generic SSBI script size(max) for a Rx event 
*/
#define RF_WDMA_RX_GEN_SSBI_SCRIPT_SIZE                                       10

/*----------------------------------------------------------------------------*/
/*! 
    @brief
    This number defines generic RFFE script size(max) for a Rx event
 
*/
#define RF_WDMA_RX_GEN_RFFE_SCRIPT_SIZE                                       10

/*----------------------------------------------------------------------------*/
/*! 
    @brief
    This number defines generic GRFC script size(max) for a Rx event
*/
#define RF_WDMA_RX_GEN_GRFC_SCRIPT_SIZE                                       10

/*---------------------------------------------------------
                   Rx Primary chain
---------------------------------------------------------*/
/* LNA offset frequency compensation value */
int8 rfwcdma_core_rxctl_lna_offset_vs_freq_val[RF_WDMA_MAX_NUM_LNA_STATES][RF_PATHS_TABLE_SIZ] = { 0 };

/* DVGA gain offset frequency compensation value */
int8 rfwcdma_core_rxctl_vga_gain_offset_vs_freq_val[RF_PATHS_TABLE_SIZ] = { 0, 0 };

/* DVGA gain offset temperature compensation value */
int16 rfwcdma_core_rxctl_vga_gain_offset_vs_temp_val = 0;

/*---------------------------------------------------------
                   Rx Secondary chain
---------------------------------------------------------*/
/* DVGA gain offset temperature compensation value */
int16 rfwcdma_core_rxctl_c1_vga_gain_offset_vs_temp_val = 0;

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Set this to TRUE for chipsets that use an internal thermistor
*/
//static boolean rfwcdma_core_rxctl_uses_internal_therm = TRUE;

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Rx Frequency compensation variables
*/
typedef struct
{
  int8 i_vga_gain_offset_vs_freq[RF_PATHS_TABLE_SIZ];
  int8 i_lna_offset_vs_freq[RF_PATHS_TABLE_SIZ];
  int8 i_lna_2_offset_vs_freq[RF_PATHS_TABLE_SIZ];
  int8 i_lna_3_offset_vs_freq[RF_PATHS_TABLE_SIZ];
  int8 i_lna_4_offset_vs_freq[RF_PATHS_TABLE_SIZ];
  uint16 last_chan_checked[RF_PATHS_TABLE_SIZ];
  rf_path_enum_type last_chain_checked;
  rfcom_wcdma_band_type last_band_checked[RF_PATHS_TABLE_SIZ];
  uint32 converted_rx_channel_list_arr[NV_FREQ_TABLE_SIZ];
  boolean channel_list_converted_flag[RF_PATHS_TABLE_SIZ];
} rfwcdma_core_rxctl_data_type;

static rfwcdma_core_rxctl_data_type rfwcdma_rxfreq_comp;

extern rfwcdma_cgagc_param_type rfwcdma_mdsp_cgagc_param_table[RFM_PATH_2][RFCOM_MAX_CARRIERS][RFCOM_NUM_WCDMA_BANDS];
extern rfnv_wcdma_nv_tbl_type* rfnv_wcdma_get_nv_tbl_ptr(rfnv_wcdma_supported_mode_type band);

/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function enables Rx AGC operation.

  @detail
  This function manages the FW RxAGC cmd cfg parameters, and send command to FW to enable/disable
  RxAGC on the given chain/carriers with the provided RxLM buffer.
  It supports enable/disable RxAGC on multiple chains and carriers, but does not support multiple
  carrier on different devices (DBDC), i.e. following are supported.
 
  1. AGC cmd on Single band multi-carrier
  2. AGC cmd on PRx chain and DRx chain on the same CA path
 
  Following are not supported:
 
  AGC cmd of multi-carrier on different CA path.
 
  For AGC cmds on multiple chains/carrier, cmd action has to be same. Cmd actions on other device
  has to be set to invalid.

  @param device rfm_device 
  @param multi_carrier_idx: bit mask for carrier indices that maps to the AGC indices
  which needs to be enabled/disabled
  @param cmd Select AGC operation command: enable, disable, or noop
  @param rxlm buffer index
  @param rf_warmup_time RF warmup time used in RF wake up 
  @param rf_wakeup enable or disable rf wakeup optimization
 */
void rfwcdma_core_rxctl_enable_rxagc
(
  uint32 multi_carrier_idx,
  rfwcdma_mdsp_agc_action_type cmd_action[RFM_DEVICE_4],
  uint32 rxlm_buf_idx[RFM_DEVICE_4],
  boolean rf_warmup_en,
  boolean nbr_en_flag,
  uint8 nbr_rxlm_buf_idx
  )
{
  /* Carrier ID*/
  uint8 agc_car_id = 0;
  /* RFM device */
  rfm_device_enum_type device = 0;
  /* Antenna ID */
  uint8 ant_id = 0;

  /* Initialize all agc command parameters to 0 */
  rfwcdma_mdsp_agc_cmd_param_type agc_cmd_param;
  memset(&agc_cmd_param, 0, sizeof(rfwcdma_mdsp_agc_cmd_param_type));

  /* Populate mdsp AGC cmd parameter structure*/
  for (device = RFM_DEVICE_0; device < RFM_DEVICE_4; device++)
  {
    if (cmd_action[device]!=RFWCDMA_MDSP_AGC_CMD_INVALID)
    {
      /* Convert device to antenna idx */
      ant_id = rfwcdma_core_util_device_to_antenna(device);

      if (ant_id >= RF_WCDMA_MAX_NUM_ANTENNAS )
      {
        MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "invalid antenna: %d", ant_id);
        continue;
      }

  /* populate agc cmd parameters. command action and rxlm buffer index will be the same for all AGCs in the
  config */
      for (agc_car_id = 0; agc_car_id < RF_WCDMA_MAX_NUM_RX_CARRIERS ; agc_car_id++)
      {
        if ((multi_carrier_idx) & (1 << agc_car_id))
        {
          agc_cmd_param.agc_action[ant_id][agc_car_id] = cmd_action[device];
          agc_cmd_param.rxlm_buf_idx[ant_id][agc_car_id] = rxlm_buf_idx[device];
        }
      }
    }
  }

  /* Populate warmup flag */
  agc_cmd_param.rf_warmup_en = rf_warmup_en;

  /* Populate nbr flag */
  agc_cmd_param.nbr_en_flag = (uint8)nbr_en_flag;
  agc_cmd_param.nbr_rxlm_buf_idx = nbr_rxlm_buf_idx;

  /* Populate Cal mode flag */
  if (ftm_wcdma_rxcal_opt_enabled() && IS_FTM_IN_WCAL_MODE())
  {
    agc_cmd_param.cal_mode = TRUE;
  }
  else
  {
    agc_cmd_param.cal_mode = FALSE;
  }
  agc_cmd_param.dc_loop_gain_burst_mode = FALSE;

  /* Send command to FW */
  (void)rfwcdma_mdsp_sync_send_cmd( RFWCDMA_MDSP_RXAGC_CMD_ID, &agc_cmd_param );

}
/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function sends a command to FW to enable DC loop for some BPGs.

  @detail
  This function sends a command to FW to enable DC loop for some BPGs.  
 */
void rfwcdma_core_rxctl_enable_dc_gain(void)
{

  /* Initialize all agc command parameters to 0 */
  rfwcdma_mdsp_agc_cmd_param_type agc_cmd_param;
  memset(&agc_cmd_param, 0, sizeof(rfwcdma_mdsp_agc_cmd_param_type));

  agc_cmd_param.dc_loop_gain_burst_mode = TRUE;

  /* Send command to FW */
  (void)rfwcdma_mdsp_sync_send_cmd( RFWCDMA_MDSP_RXAGC_CMD_ID, &agc_cmd_param );
  MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "DEBUG_RF: DC loop gain enabled", 0);
 
}

/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function calculate CGAGC parameters from NV

  @detail

  @param nv_node NV node for WCDMA operation
  @param rfcom_rf_band Operational WCDMA band
 */
void rfwcdma_core_rxctl_cal_cgagc_params
(
  rfnv_wcdma_supported_mode_type  nv_mode,
  rfcom_wcdma_band_type  rfcom_rf_band
)
{
  rfnv_wcdma_nv_tbl_type        *nv_tbl_ptr;
  rfwcdma_core_rxctl_cgagc_param_type *cgagc_param_ptr;
  rfwcdma_core_rxctl_cgagc_param_type *c1_cgagc_param_ptr;
  int16 cminit_0, cminit_1, cminit_2, cminit_3;
  int32 lna_trans1, lna_trans2, lna_trans3;
  double power_value;
  rfwcdma_core_rxctl_cgagc_param_type *cgagc_param_ptr_dual_carrier;
  rfwcdma_core_rxctl_cgagc_param_type *c1_cgagc_param_ptr_dual_carrier;
  int16 cminit_5;
  int16 cminit_0_car1, cminit_1_car1, cminit_2_car1, cminit_3_car1, cminit_4_car1, cminit_5_car1;
  int32 lna_trans1_car1, lna_trans2_car1, lna_trans3_car1;
  double power_value_car1;
  uint32 zero_db_digital_gain = rfwcdma_msm_get_zero_db_digital_gain();
  MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "zero_db_digital_gain %d", zero_db_digital_gain);

  nv_tbl_ptr = rfnv_wcdma_get_nv_tbl_ptr(nv_mode);

  /* TODO:
     Add code for car3 and also use values from new NV
  */

  cgagc_param_ptr =
    (rfwcdma_core_rxctl_cgagc_param_type *)&(rfwcdma_mdsp_cgagc_param_table[RFM_PATH_0][RFCOM_SINGLE_CARRIER][rfcom_rf_band]);
  c1_cgagc_param_ptr =
    (rfwcdma_core_rxctl_cgagc_param_type *)&(rfwcdma_mdsp_cgagc_param_table[RFM_PATH_1][RFCOM_SINGLE_CARRIER][rfcom_rf_band]);
  cgagc_param_ptr_dual_carrier	=
    (rfwcdma_core_rxctl_cgagc_param_type *)&(rfwcdma_mdsp_cgagc_param_table[RFM_PATH_0][RFCOM_DUAL_CARRIER][rfcom_rf_band]);
  c1_cgagc_param_ptr_dual_carrier =
    (rfwcdma_core_rxctl_cgagc_param_type *)&(rfwcdma_mdsp_cgagc_param_table[RFM_PATH_1][RFCOM_DUAL_CARRIER][rfcom_rf_band]);


  if (nv_tbl_ptr == NULL)
  {
    MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "NV table pointer is NULL - Couldn't calculate AGC params from NV !", 0);
    return;
  }

  /* Primary chain */
  /* Calculate RxAgcVal_CmInit values */
  cminit_0 = 0 - nv_tbl_ptr->vga_gain_offset;

  cminit_0_car1 = 0 - nv_tbl_ptr->vga_gain_offset_car1;

  cgagc_param_ptr->rxAgcVal_CmInit_0 = (uint16)cminit_0;

  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_0 = (uint16)cminit_0_car1;

  cminit_1 = nv_tbl_ptr->lna_range_offset - nv_tbl_ptr->vga_gain_offset;
  cgagc_param_ptr->rxAgcVal_CmInit_1 = (uint16)cminit_1;

  cminit_1_car1 = nv_tbl_ptr->lna_range_offset_car1 - nv_tbl_ptr->vga_gain_offset_car1;
  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_1 = (uint16)cminit_1_car1;


  cminit_2 = nv_tbl_ptr->lna_range_offset_2 - nv_tbl_ptr->vga_gain_offset;
  cgagc_param_ptr->rxAgcVal_CmInit_2 = (uint16)cminit_2;

  cminit_2_car1 = nv_tbl_ptr->lna_range_offset_2_car1 - nv_tbl_ptr->vga_gain_offset_car1;
  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_2 = (uint16)cminit_2_car1;


  cminit_3 = nv_tbl_ptr->lna_range_offset_3 - nv_tbl_ptr->vga_gain_offset;
  cgagc_param_ptr->rxAgcVal_CmInit_3 = (uint16)cminit_3;

  cminit_3_car1 = nv_tbl_ptr->lna_range_offset_3_car1 - nv_tbl_ptr->vga_gain_offset_car1;
  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_3 = (uint16)cminit_3_car1;

  cgagc_param_ptr->rxAgcVal_CmInit_4 = RFAGC_MAX_AGC_VAL;
  cminit_4_car1 = nv_tbl_ptr->lna_range_offset_4_car1 - nv_tbl_ptr->vga_gain_offset_car1;
  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_4 = (uint16)cminit_4_car1;

  cminit_5 = nv_tbl_ptr->lna_range_offset_5 - nv_tbl_ptr->vga_gain_offset;
  cgagc_param_ptr->rxAgcVal_CmInit_5 = (uint16)cminit_5;
  cminit_5_car1 = nv_tbl_ptr->lna_range_offset_5_car1 - nv_tbl_ptr->vga_gain_offset_car1;
  cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_5 = (uint16)cminit_5_car1;

  //cgagc_param_ptr->rxAgcVal_CmInit_4 = RFAGC_MAX_AGC_VAL;

  /* Calculate Lna transition values */
  lna_trans1 = (int16)(nv_tbl_ptr->lna_range_rise + nv_tbl_ptr->lna_range_fall) / 2;

  lna_trans1_car1 = (int16)(nv_tbl_ptr->lna_range_rise_car1 + nv_tbl_ptr->lna_range_fall_car1) / 2;

  lna_trans2 = (int16)(nv_tbl_ptr->lna_range_rise_2 + nv_tbl_ptr->lna_range_fall_2) / 2;

  lna_trans2_car1 = (int16)(nv_tbl_ptr->lna_range_rise_2_car1 + nv_tbl_ptr->lna_range_fall_2_car1) / 2;


  lna_trans3 = (int16)(nv_tbl_ptr->lna_range_rise_3 + nv_tbl_ptr->lna_range_fall_3) / 2;


  lna_trans3_car1 = (int16)(nv_tbl_ptr->lna_range_rise_3_car1 + nv_tbl_ptr->lna_range_fall_3_car1) / 2;

  /* Calculate Z value for CmLnaFall_1, CmLnaFall2, and CmLnaRise1 */
  power_value = pow(10.0, ((double)lna_trans1 - (double)cminit_1) / 100.0);
  cgagc_param_ptr->Z_cmLnaFall_1 = (uint32)(power_value * zero_db_digital_gain);
  MSG_1(MSG_SSID_RF, MSG_LEGACY_ERROR, "power_value %d", power_value);
  power_value_car1 = pow(10.0, ((double)lna_trans1_car1 - (double)cminit_1_car1) / 100.0);
  cgagc_param_ptr_dual_carrier->Z_cmLnaFall_1 = (uint32)(power_value_car1 * zero_db_digital_gain);

  cgagc_param_ptr->Z_cmLnaFall_2 = 0; //Set LNA Fall 2 to zero as the default LNA state in G1
  cgagc_param_ptr_dual_carrier->Z_cmLnaFall_2 = 0; //Set LNA Fall 2 to zero as the default LNA state in G1


  power_value = pow(10.0, ((double)lna_trans2 - (double)cminit_1) / 100.0);
  cgagc_param_ptr->Z_cmLnaRise_1 = (uint32)(power_value * zero_db_digital_gain);

  power_value_car1 = pow(10.0, ((double)lna_trans2_car1 - (double)cminit_1_car1) / 100.0);
  cgagc_param_ptr_dual_carrier->Z_cmLnaRise_1 = (uint32)(power_value_car1 * zero_db_digital_gain);

  power_value = pow(10.0, ((double)lna_trans3 - (double)cminit_1) / 100.0);
  cgagc_param_ptr->Z_cmLnaRise_2 = (uint32) ( power_value * zero_db_digital_gain );
  power_value_car1 = pow(10.0, ((double)lna_trans3_car1 - (double)cminit_1_car1) / 100.0);
  cgagc_param_ptr_dual_carrier->Z_cmLnaRise_2 = (uint32)(power_value_car1 * zero_db_digital_gain);

  //power_value = pow(10.0, ((double)lna_trans4 - (double)cminit_1) / 100.0);
  //cgagc_param_ptr->Z_cmLnaRise_3 = (uint32)(power_value * zero_db_digital_gain);
  //power_value_car1 = pow(10.0, ((double)lna_trans4_car1 - (double)cminit_1_car1) / 100.0);
  //cgagc_param_ptr_dual_carrier->Z_cmLnaRise_3 = (uint32)(power_value_car1 * zero_db_digital_gain);

  /* temporary fix: Need to find a target independent solutino in rfwcdma_core_rxctl.c file*/
  cgagc_param_ptr->Z_cmLnaRise_3 = 0x7FFFFFFF;
  cgagc_param_ptr_dual_carrier->Z_cmLnaRise_3 = 0x7FFFFFFF;

  /* Secondary chain */
  /* Calculate RxAgcVal_CmInit values */
  cminit_0 = 0 - nv_tbl_ptr->c1_vga_gain_offset;
  cminit_0_car1 = 0 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr->rxAgcVal_CmInit_0 = (uint16)cminit_0;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_0 = (uint16)cminit_0_car1;

  cminit_1 = nv_tbl_ptr->c1_lna_range_offset - nv_tbl_ptr->c1_vga_gain_offset;
  cminit_1_car1 = nv_tbl_ptr->c1_lna_range_offset_car1 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr->rxAgcVal_CmInit_1 = (uint16)cminit_1;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_1 = (uint16)cminit_1_car1;

  cminit_2 = nv_tbl_ptr->c1_lna_range_offset_2 - nv_tbl_ptr->c1_vga_gain_offset;
  cminit_2_car1 = nv_tbl_ptr->c1_lna_range_offset_2_car1 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr->rxAgcVal_CmInit_2 = (uint16)cminit_2;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_2 = (uint16)cminit_2_car1;

  cminit_3 = nv_tbl_ptr->c1_lna_range_offset_3 - nv_tbl_ptr->c1_vga_gain_offset;
  cminit_3_car1 = nv_tbl_ptr->c1_lna_range_offset_3_car1 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr->rxAgcVal_CmInit_3 = (uint16)cminit_3;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_3 = (uint16)cminit_3_car1;

  cminit_4_car1 = nv_tbl_ptr->c1_lna_range_offset_4_car1 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr->rxAgcVal_CmInit_4 = RFAGC_MAX_AGC_VAL;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_4 = (uint16)cminit_4_car1;

  cminit_5_car1 = nv_tbl_ptr->c1_lna_range_offset_5_car1 - nv_tbl_ptr->c1_vga_gain_offset_car1;
  c1_cgagc_param_ptr_dual_carrier->rxAgcVal_CmInit_5 = (uint16)cminit_5_car1;

  /* Calculate Lna transition values */
  lna_trans1 = ((int32)nv_tbl_ptr->c1_lna_range_rise + (int32)nv_tbl_ptr->c1_lna_range_fall) / 2;
  lna_trans1_car1 = ((int32)nv_tbl_ptr->c1_lna_range_rise_car1 + (int32)nv_tbl_ptr->c1_lna_range_fall_car1) / 2;

  lna_trans2 = ((int32)nv_tbl_ptr->c1_lna_range_rise_2 + (int32)nv_tbl_ptr->c1_lna_range_fall_2) / 2;
  lna_trans2_car1 = ((int32)nv_tbl_ptr->c1_lna_range_rise_2_car1 + (int32)nv_tbl_ptr->c1_lna_range_fall_2_car1) / 2;

  lna_trans3 = ((int32)nv_tbl_ptr->c1_lna_range_rise_3 + (int32)nv_tbl_ptr->c1_lna_range_fall_3) / 2;
  lna_trans3_car1 = ((int32)nv_tbl_ptr->c1_lna_range_rise_3_car1 + (int32)nv_tbl_ptr->c1_lna_range_fall_3_car1) / 2;

  /* Calculate Z value for CmLnaFall_1, CmLnaFall2, and CmLnaRise1 */
  power_value = pow(10.0, ((double)lna_trans1 - (double)cminit_1) / 100.0);
  c1_cgagc_param_ptr->Z_cmLnaFall_1 = (uint32)(power_value * zero_db_digital_gain);

  power_value_car1 = pow(10.0, ((double)lna_trans1_car1 - (double)cminit_1_car1) / 100.0);
  c1_cgagc_param_ptr_dual_carrier->Z_cmLnaFall_1 = (uint32)(power_value_car1 * zero_db_digital_gain);

  c1_cgagc_param_ptr->Z_cmLnaFall_2 = 0; //Set LNA Fall 2 to zero as the default LNA state in G1
  c1_cgagc_param_ptr_dual_carrier->Z_cmLnaFall_2 = 0; //Set LNA Fall 2 to zero as the default LNA state in G1

  power_value = pow(10.0, ((double)lna_trans2 - (double)cminit_1) / 100.0);
  c1_cgagc_param_ptr->Z_cmLnaRise_1 = (uint32)(power_value * zero_db_digital_gain);

  power_value_car1 = pow(10.0, ((double)lna_trans2_car1 - (double)cminit_1_car1) / 100.0);
  c1_cgagc_param_ptr_dual_carrier->Z_cmLnaRise_1 = (uint32)(power_value_car1 * zero_db_digital_gain);

  power_value = pow(10.0, ((double)lna_trans3 - (double)cminit_1) / 100.0);
  c1_cgagc_param_ptr->Z_cmLnaRise_2 = (uint32)(power_value * zero_db_digital_gain);

  power_value_car1 = pow(10.0, ((double)lna_trans3_car1 - (double)cminit_1_car1) / 100.0);
  c1_cgagc_param_ptr_dual_carrier->Z_cmLnaRise_2 = (uint32)(power_value_car1 * zero_db_digital_gain);

  c1_cgagc_param_ptr->Z_cmLnaRise_3 = 0x7FFFFFFF;
  c1_cgagc_param_ptr_dual_carrier->Z_cmLnaRise_3 = 0x7FFFFFFF;

  return;
}

/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function initializes CGAGC parameters from NV.

  @detail

  @param None
 */
void rfwcdma_core_rxctl_init_cgagc_params(void)
{
  static boolean rfwcdma_core_cgagc_params_done = FALSE;

  if (!rfwcdma_core_cgagc_params_done)
  {
    /* initialize CGAGC parameters */
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_2100, RFCOM_BAND_IMT);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_1900, RFCOM_BAND_1900);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_1800, RFCOM_BAND_BC3);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_BC4,  RFCOM_BAND_BC4);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_800,  RFCOM_BAND_800);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_800,  RFCOM_BAND_BC19);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_900,  RFCOM_BAND_BC8);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_1800, RFCOM_BAND_BC9);
    rfwcdma_core_rxctl_cal_cgagc_params(RFNV_WCDMA_1500, RFCOM_BAND_BC11);

    rfwcdma_core_cgagc_params_done = TRUE;
  }

  return;
}

/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function updates frequency offset data.

  @detail
  This function interpolates frequency compensation items from the channel
  number. And, applies the interpolation results to all addected frequency
  compensation items.

  @param rx_chain RF primary or secondary path
  @param band Operational WCDMA band
  @param channel Operational channel
 */
void rfwcdma_core_rxctl_update_freq_offsets
(
  rf_path_enum_type rx_chain,
  rfcom_wcdma_band_type band,
  uint16 channel
)
{
  uint8 rx_start_bin = 0;   /* starting RX cal channel for bin containing */
  int32 rx_bin_size;        /* number of channels in the bin (not constant) */
  int32 rx_delta_x;         /* scale of channel number in bin */
  int16 delta_y;            /* used to store delta_y between adjacent nv items */
  //int16 lin_tbl;            /* increment value for linearizer values */
  boolean rx_channel_under = 0; /* flag if lowest RX cal channel > tune value */
  uint32 rx_freq;
  uint32 bin_rx_freq;
  uint16 bin_rx_chan; 
  boolean out_of_range = FALSE;

  rfagc_receive_chain_type receive_chain;

  /* Offsets Vs Freq are considered same across carriers */
  rfcom_multi_carrier_id_type carr_id  = RFCOM_MULTI_CARRIER_ID__0;


  /* check if the channel passed in is the same channel (from the same
     band) that was passed in earlier.  If so, return, no need to
     calculate again.  The first time through this function
     rfwcdma_rxfreq_comp.last_chan_checked will not yet have a value and the
     if statement should fail fall through to execute the function */

  if (((channel == rfwcdma_rxfreq_comp.last_chan_checked[rx_chain]) &&
       (band == rfwcdma_rxfreq_comp.last_band_checked[rx_chain]) &&
       (rx_chain == rfwcdma_rxfreq_comp.last_chain_checked)) ||
      (rx_chain > RF_PATH_1))
  {
    return;
  }


  if (band < RFCOM_NUM_WCDMA_BANDS)
  {
    if (rfnv_wcdma_tbl_ptr[band] == NULL)
  {
    RF_MSG_1(RF_ERROR,
             "rfwcdma_core_rxctl_update_freq_offsets: NULL NV tbl ptr for band %d",
             band);
    return;
  }
  }

  else
  {
    RF_MSG_1(RF_ERROR,
               "rfwcdma_core_rxctl_update_freq_offsets: Not a valid band %d",
               band);
    return;
  }

  if (rx_chain == RF_PATH_0)
  {
    receive_chain = RFAGC_RECEIVE_CHAIN_0;
  }
  else if (rx_chain == RF_PATH_1)
  {
    receive_chain = RFAGC_RECEIVE_CHAIN_1;
  }
  else
  {
    RF_MSG_1(RF_ERROR,
             "rfwcdma_core_rxctl_update_freq_offsets: Invalid Rx Chain %d",
             rx_chain);
    return;
  }

  /* update the last channel and band checked with the channel passed in */

  rfwcdma_rxfreq_comp.last_chan_checked[rx_chain] = channel;
  rfwcdma_rxfreq_comp.last_band_checked[rx_chain] = band;
  rfwcdma_rxfreq_comp.last_chain_checked = rx_chain;

  /* convert dis-joint cdma channel scheme to continuious.
     The cal channels are NV items and could be initilized
     to continuous,rather than calculating each time. */

  /* PATH 1 or 0 - RX ITEMS */
  rx_freq = rfwcdma_core_get_freq_from_uarfcn(band, channel, RFWCDMA_CORE_UARFCN_DL);

  /* do the same thing, checking against the 1st element of the
     RX cal channel list */

  bin_rx_chan = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                              carr_id,RX_CAL_CHAN_LIST,0,&(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ); 
  bin_rx_freq = rfwcdma_core_get_freq_from_uarfcn(band,bin_rx_chan,RFWCDMA_CORE_UARFCN_DL);

  if (rx_freq <= bin_rx_freq) 
  {
    rx_start_bin = 0;
    rx_channel_under++;    /* set flag if we are under the cal table
                              range flatline the value. */
  }

  /* search for frequency bin that contains the channel
     that is being compensated
     or if the bin contains zero we went off the table, use the last value */

  /* do the same thing, traversing the RX cal channel list */
  else
  {
    out_of_range = FALSE;
    while (rx_start_bin < (NV_FREQ_TABLE_SIZ - 1))
    {
      /* note: tables with less than 16 elements in them must be
         padded with 0's */
      bin_rx_chan = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,carr_id,
                             RX_CAL_CHAN_LIST,rx_start_bin+1,&(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );
      bin_rx_freq = rfwcdma_core_get_freq_from_uarfcn(band,bin_rx_chan,RFWCDMA_CORE_UARFCN_DL);
      if( bin_rx_freq == 0) 
      {
        out_of_range = TRUE;
        break;
      }

      if(rx_freq <= bin_rx_freq) 
      {
        break;            /* exit*/
      }
      else
      {
        rx_start_bin++;      /* check next bin*/
      }
    }
  }

  if (rx_start_bin == (NV_FREQ_TABLE_SIZ - 1) || out_of_range)
  {
    rx_bin_size = 0;
  }
  else
  {

     rx_bin_size = 
        rfwcdma_core_get_freq_from_uarfcn(band,rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
           carr_id,RX_CAL_CHAN_LIST,rx_start_bin+1,&(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ),RFWCDMA_CORE_UARFCN_DL) -
        rfwcdma_core_get_freq_from_uarfcn(band,rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
           carr_id,RX_CAL_CHAN_LIST,rx_start_bin,&(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ),RFWCDMA_CORE_UARFCN_DL);
    
  }

  /* These are the RX items */

  /* outside the array case */
  if ((rx_bin_size <= 0) || (rx_channel_under == 1))
  {
    /* update rx freq comp items */
    rfwcdma_rxfreq_comp.i_vga_gain_offset_vs_freq[rx_chain] =
      (int8)rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain, carr_id,
                                                     VGA_OFFSET_VS_FREQ, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    rfwcdma_rxfreq_comp.i_lna_offset_vs_freq[rx_chain] =
      (int8)rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain, carr_id,
                                                     LNA_OFFSET_VS_FREQ_1, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    rfwcdma_rxfreq_comp.i_lna_2_offset_vs_freq[rx_chain] =
      (int8)rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain, carr_id,
                                                     LNA_OFFSET_VS_FREQ_2, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    rfwcdma_rxfreq_comp.i_lna_3_offset_vs_freq[rx_chain] =
      (int8)rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain, carr_id,
                                                     LNA_OFFSET_VS_FREQ_3, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    rfwcdma_rxfreq_comp.i_lna_4_offset_vs_freq[rx_chain] =
      (int8)rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain, carr_id,
                                                     LNA_OFFSET_VS_FREQ_4, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

  }
  else  /* interpolate the item if it's delta_y is not zero */
  {
    /* ensures that dx/binsize <1 at bin boundary, when delta_y = 1. */
    /* delta_x is the distance from your local channel to the beginning of the
       bin, -1 */
    bin_rx_chan = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                         carr_id,RX_CAL_CHAN_LIST,rx_start_bin,&(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );
    
    bin_rx_freq = rfwcdma_core_get_freq_from_uarfcn(band,bin_rx_chan,RFWCDMA_CORE_UARFCN_DL);
      
    rx_delta_x = (int32)(rx_freq - bin_rx_freq - 1);


    /* DVGA GAIN OFFSET VS FREQ */
    /* delta_y is the difference between the lim_vs_freq values
       of the bin edges to the left and right of our rx frequency */
    /*lint -e{661} Suppress Lint */
    delta_y = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                                       carr_id, VGA_OFFSET_VS_FREQ, rx_start_bin + 1, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ) -
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, VGA_OFFSET_VS_FREQ, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    (delta_y < 0) ? delta_y-- : delta_y++;

    /* take the ratio of the delta_x to the entire bin size, and apply
       it to the delta_y.  This is the value you need to add to the
       comp_vs_freq value of your left bin edge to get the comp_vs_freq
       value of your rx frequency */

    rfwcdma_rxfreq_comp.i_vga_gain_offset_vs_freq[rx_chain] =
      (int8)(delta_y * rx_delta_x / rx_bin_size) +
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, VGA_OFFSET_VS_FREQ, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    /* LNA OFFSETS VS FREQ */
    /*lint -e{661} Suppress Lint */
    delta_y = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                                       carr_id, LNA_OFFSET_VS_FREQ_1, rx_start_bin + 1, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ) -
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_1, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    (delta_y < 0) ? delta_y-- : delta_y++;

    rfwcdma_rxfreq_comp.i_lna_offset_vs_freq[rx_chain] =
      (int8)(delta_y * rx_delta_x / rx_bin_size) +
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_1, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

    /*lint -e{661} Suppress Lint */
    delta_y = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                                       carr_id, LNA_OFFSET_VS_FREQ_2, rx_start_bin + 1, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ) -
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_2, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );


    (delta_y < 0) ? delta_y-- : delta_y++;

    rfwcdma_rxfreq_comp.i_lna_2_offset_vs_freq[rx_chain] =
      (int8)(delta_y * rx_delta_x / rx_bin_size) +
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_2, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );


    /*lint -e{661} Suppress Lint */
    delta_y = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                                       carr_id, LNA_OFFSET_VS_FREQ_3, rx_start_bin + 1, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ) -
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_3, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );


    (delta_y < 0) ? delta_y-- : delta_y++;

    rfwcdma_rxfreq_comp.i_lna_3_offset_vs_freq[rx_chain] =
      (int8)(delta_y * rx_delta_x / rx_bin_size) +
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_3, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );


    /*lint -e{661} Suppress Lint */
    delta_y = rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                                       carr_id, LNA_OFFSET_VS_FREQ_4, rx_start_bin + 1, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) ) -
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_4, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );


    (delta_y < 0) ? delta_y-- : delta_y++;

    rfwcdma_rxfreq_comp.i_lna_4_offset_vs_freq[rx_chain] =
      (int8)(delta_y * rx_delta_x / rx_bin_size) +
      rfnv_wcdma_get_info_from_rx_dynamic_data(receive_chain,
                                               carr_id, LNA_OFFSET_VS_FREQ_4, rx_start_bin, &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );

  }

  /* update lna offset frequency compensation value */

  /* LNA offset for G0 is 0*/
  rfwcdma_core_rxctl_lna_offset_vs_freq_val[0][rx_chain] = 0;

  /* update lna offset 1 frequency compensation value */

  rfwcdma_core_rxctl_lna_offset_vs_freq_val[1][rx_chain] =
    rfwcdma_rxfreq_comp.i_lna_offset_vs_freq[rx_chain];

  /* update lna offset 2 frequency compensation value */
  rfwcdma_core_rxctl_lna_offset_vs_freq_val[2][rx_chain] =
    rfwcdma_rxfreq_comp.i_lna_2_offset_vs_freq[rx_chain];

  /* update lna offset 3 frequency compensation value */
  rfwcdma_core_rxctl_lna_offset_vs_freq_val[3][rx_chain] =
    rfwcdma_rxfreq_comp.i_lna_3_offset_vs_freq[rx_chain];

  /* update lna offset 4 frequency compensation value */
   rfwcdma_core_rxctl_lna_offset_vs_freq_val[4][rx_chain] =
    rfwcdma_rxfreq_comp.i_lna_4_offset_vs_freq[rx_chain];

  /* update DVGA gain offset frequency compensation value */
  rfwcdma_core_rxctl_vga_gain_offset_vs_freq_val[rx_chain] =
    rfwcdma_rxfreq_comp.i_vga_gain_offset_vs_freq[rx_chain];

  return;
}
/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function will peform Rx freq comp.

  @detail
  This function update Rx freq comp items and update to mdsp

  @param rx_chain RF primary or secondary path
  @param band Operational WCDMA band
  @param channel Operational channel
 */

void rfwcdma_core_rxctl_freq_comp
(
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band,
  uint16 channel
)
{
  rf_path_enum_type rx_chain;

  /* Getting the Rx chain from the device*/
  rx_chain = rfcommon_core_device_to_path(device);

  rfwcdma_core_rxctl_update_freq_offsets(rx_chain, band, channel);

}

/*--------------------------------------------------------------------------*/
/*!
  @brief
  This function updates vga gain offset data.

  @detail

 */
void rfwcdma_core_rxctl_temp_val_update
(
  int16 vga_gain_offset_vs_temp_val,
  int16 c1_vga_gain_offset_vs_temp_val
  )
{
  rfwcdma_core_rxctl_vga_gain_offset_vs_temp_val = vga_gain_offset_vs_temp_val;
  rfwcdma_core_rxctl_c1_vga_gain_offset_vs_temp_val = c1_vga_gain_offset_vs_temp_val;
}

/*--------------------------------------------------------------------------*/
/*!
  @brief
 
  @details
  This function freezes/unfreezes the RX accumulator.
 
  @param path : transceiver to freeze/unfreeze
  @param freeze : flag to specify freeze/unfreeze
 */

void rfwcdma_core_rxctl_set_rx_freeze_accum(rfcom_device_enum_type device, boolean freeze)
{
  rfwcdma_mdsp_set_rx_freeze_accum(device, freeze);
}

/*----------------------------------------------------------------------------*/

/*!
  @brief
  This function will be called during online mode to do the temp comp calculation
  of rsb coefficient

  @details
  This function get the thermistor reading and calculate the rsb coefficient,
  write it to shared memory and send sync command to FW


  @param curr_temp
  This is the current temprature from thermistor
*/

boolean rfwcdma_core_rxctl_update_rsb_coeff(uint8 scaled_therm)
{
  uint8 valid_num_of_carrier                  = 0;
  int32 a_coeff                               = 0;
  int32 b_coeff                               = 0;
  rfm_device_enum_type rfm_device             = RFM_INVALID_DEVICE;
  rfcommon_msm_num_carr_bw bw = {0};
  rfwcdma_mdsp_set_data_param_type mdsp_data = { 0 };
  uint8 temp_lna_state = 0;

  rfwcdma_mc_car_path_state_type car_path_state[RFCOM_MAX_CARRIERS];
  rxlm_chain_type rxlm_chain = RXLM_CHAIN_MAX;
  rfc_device_info_type *device_info_ptr = NULL;
  rfc_cfg_params_type cfg = { RF_PATH_MAX, RFM_INVALID_DEVICE, RFC_CONFIG_RXTX_INVALID, 0, RFC_REQ_DEFAULT_GET_DATA };

  boolean api_status = TRUE;

  memset(car_path_state, 0, sizeof(rfwcdma_mc_car_path_state_type));

  /* Update the shared mem with new iqmc coefficients and ask fw 
     to perform dynamic rxlm update */
  for (rfm_device = RFM_DEVICE_0; rfm_device < RFM_DEVICE_4; rfm_device++)
  {
    valid_num_of_carrier = rfwcdma_mc_get_car_state_info_from_device(rfm_device, car_path_state);

    if ((valid_num_of_carrier > 0) &&
        ((car_path_state[valid_num_of_carrier - 1].rf_state == RFWCDMA_MC_STATE_RX) ||
         (car_path_state[valid_num_of_carrier - 1].rf_state == RFWCDMA_MC_STATE_RXTX)))
    {

      for (temp_lna_state = 0; temp_lna_state < RF_WDMA_MAX_NUM_LNA_STATES; temp_lna_state++)
      {
        bw.num_carr = rfwcdma_mc_state.rx_carriers[rfm_device];
        /* Initialize valid flag to TRUE */
        mdsp_data.data.iqmc_param.iqmc_valid[temp_lna_state] = TRUE;
      /* call rsb temp comp to get updated rsb coefficients A and B */
      rfdevice_wcdma_rx_get_rsb_coeff_online_temp_comp(rfm_device,
                                                       car_path_state[valid_num_of_carrier - 1].curr_band_rx,
                                                         bw.num_carr,
                                                       rfwcdma_core_temp_comp_get_raw_therm_read(scaled_therm),
                                                         temp_lna_state,
                                                       &a_coeff,
                                                       &b_coeff);

        /* Only update when there is valid B coefficiet*/
      if ((b_coeff != 0))
      {
          /* Get RxLm chain from RFC */

          cfg.req = RFC_REQ_DEFAULT_GET_DATA;
          cfg.alternate_path = 0;
          cfg.band = car_path_state[valid_num_of_carrier - 1].curr_band_rx;
          cfg.logical_device = rfm_device;
          cfg.rx_tx = RFC_CONFIG_RX;

          if (rfc_wcdma_data_get_device_info(&cfg, &device_info_ptr) == TRUE)
          {
            rxlm_chain = device_info_ptr->modem_chain;
        }
        else
        {
            rxlm_chain = rxlm_get_chain(rfwcdma_mc_state.rxlm_buf_idx[rfm_device]);
      }
          /* Query DTR to calcuate IQMC coefficient and update DM */
          mdsp_data.data.iqmc_param.iqmc_coeff[temp_lna_state] = rfcommon_msm_get_iqmc_coeffs(rfwcdma_mc_state.rxlm_buf_idx[rfm_device],
                                                                                              rxlm_chain,
                                                                                              LM_UMTS,
                                                                                              bw,
                                                                                              a_coeff,
                                                                                              b_coeff
                                                                                              );
         MSG_7(MSG_SSID_RF, MSG_LEGACY_HIGH,
            "RSB A:%d, RSB B:%d, lna_state:%d, rxlm_chain:%d, iqmc:%d, device:%d, rxlm_buf_idx:%d",
            a_coeff, b_coeff, temp_lna_state, rxlm_chain, mdsp_data.data.iqmc_param.iqmc_coeff[temp_lna_state],rfm_device,rfwcdma_mc_state.rxlm_buf_idx[rfm_device]);

    }
    else
    {
          /* If any LNA state has invalid data, invalidate all the IQMC data */
          mdsp_data.data.iqmc_param.iqmc_valid[temp_lna_state] = FALSE;
    }
  }

      mdsp_data.rflm_handle = rfwcdma_mc_state.rxlm_buf_idx[rfm_device];
      mdsp_data.set_data_cmd = RFWCDMA_MDSP_SET_RX_IQMC;
      api_status &= rfwcdma_mdsp_async_set_data(&mdsp_data);
  }
  }

  return api_status;

}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  This function return the ant_mask

  @details
  depending on which device has been called in returns the ant_mask

  @param rfm_device
  This is the current rfm device

 @return ant_mask
*/

void rfwcdma_core_rxctl_get_ant_mask(rfm_device_enum_type rfm_device, uint32 *ant_mask)
{
  if (rfm_device == RFM_DEVICE_1 || rfm_device == RFM_DEVICE_3)
  {
    *ant_mask = 0x3;
  }
  else
  {
    *ant_mask = 0x1;
  }
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Configure Rx band parameters to MDSP module 

  @details
  This function will populate Rx band parameter data structure and pass down to
  RF-MDSP module.
 
  @param rxlm_handle
  RxLm handle
 
  @param rfm_device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param multi_carrier_idx
  Multi-Carrier index (bit mask) for the carrier/cell to be configured
 
  @param meas_scenario
  Enum to indicate IRAT scenario
 
  @return
  Flag indicating the function execution success
*/
boolean rfwcdma_core_rxctl_config_band
(
  uint32 rxlm_handle,
  rfm_device_enum_type rfm_device,
  rfcom_wcdma_band_type band,
  uint32 multi_carrier_idx,
  rfwcdma_mdsp_rx_irat_meas_type meas_scenario
)
{
  rfwcdma_mdsp_rx_cfg_bsp_type mdsp_cfg;
  rf_buffer_intf *lna_script_buffer[RF_WDMA_MAX_NUM_LNA_STATES] = { NULL };
  rf_buffer_intf *drx_script_buffer[RF_WCDMA_DRX_CTL_NUM] = { NULL };
  uint8 lna_state, drx_ctl;
  rfdevice_wcdma_lna_param_type lna_param;
  boolean api_status = TRUE;
  rfwcdma_cgagc_param_type *cgagc_param_tbl_ptr = NULL;
  uint16 tx_chan, rx_chan = 0;
  uint64 tx_freq, rx_freq = 0;
  rfc_wcdma_core_config_type rfc_core_config = {0};

  /* NV table pointer needs to be intialized */
  if (band < RFCOM_NUM_WCDMA_BANDS)
  {
    if (rfnv_wcdma_tbl_ptr[band] == NULL)
    {
      RF_MSG_1(RF_ERROR,
               "rfwcdma_core_rxctl_config_band: NULL NV tbl ptr for band %d",
               band);
      return FALSE;
    }
  }

  else
  {
     RF_MSG_1(RF_ERROR,
               "rfwcdma_core_rxctl_config_band: Not a valid band %d",
               band);
      return FALSE;
  }

  memset(&mdsp_cfg, 0, sizeof(rfwcdma_mdsp_rx_cfg_bsp_type));
  /* Get LNA script RF buffer from device driver */
  lna_param.device = rfm_device;
  lna_param.band = band;
  lna_param.carrier = rfwcdma_core_count_carriers(multi_carrier_idx);

  for (lna_state = 0; lna_state < RF_WDMA_MAX_NUM_LNA_STATES; lna_state++)
  {
    /* Create local RF buffers for device script population */
    lna_script_buffer[lna_state] = rf_buffer_create(RF_WDMA_RX_GEN_SSBI_SCRIPT_SIZE,
                                                    RF_WDMA_RX_GEN_RFFE_SCRIPT_SIZE,
                                                    RF_WDMA_RX_GEN_GRFC_SCRIPT_SIZE);

    if (lna_script_buffer[lna_state] == NULL)
    {
      RF_MSG(RF_ERROR,
             "rfwcdma_core_rxctl_band_config: rf buffer allocation failed!");
      api_status = FALSE;
      break;
    }

    (void)rf_buffer_clear(lna_script_buffer[lna_state]);

    /* Set current LNA state */
    lna_param.current_state = lna_state;

    /* Retrieve LNA settings per gain state from rf device */
    rfdevice_wcdma_rx_lna_setup(rfm_device,
                                band,
                                &lna_param,
                                RFDEVICE_CREATE_SCRIPT,
                                lna_script_buffer[lna_state]);

    mdsp_cfg.device_cfg.lna[lna_state].script = lna_script_buffer[lna_state];
    mdsp_cfg.device_cfg.lna[lna_state].delay = 0;

    /* Disable per-LNA state IQMC programming in RxAGC by default */
    mdsp_cfg.iqmc_param.iqmc_valid[lna_state] = FALSE;
  }

  /* Get dRx script RF buffer from device driver */
  for (drx_ctl = RF_WCDMA_DRX_ENABLE; drx_ctl < RF_WCDMA_DRX_CTL_NUM; drx_ctl++)
  {
    /* Create local RF buffers for device script population */
    drx_script_buffer[drx_ctl] = rf_buffer_create(RF_WDMA_RX_GEN_SSBI_SCRIPT_SIZE,
                                                  RF_WDMA_RX_GEN_RFFE_SCRIPT_SIZE,
                                                  RF_WDMA_RX_GEN_GRFC_SCRIPT_SIZE);

    if (drx_script_buffer[drx_ctl] == NULL)
    {
      RF_MSG(RF_ERROR,
             "rfwcdma_core_rxctl_band_config: rf buffer allocation failed!");
      api_status = FALSE;
      break;
    }

    (void)rf_buffer_clear(drx_script_buffer[drx_ctl]);

    rfdevice_wcdma_rx_get_cpc_drx_val(rfm_device, band, (boolean)drx_ctl, RFDEVICE_CREATE_SCRIPT, drx_script_buffer[drx_ctl]);


    /* In DRx diable (Rx ON), need to reconfigure LNA state back to default before CGAGC gets restarted */
    if (drx_ctl == RF_WCDMA_DRX_DISABLE)
    {
      /* Populate LNA param to default LNA state before querying device driver and append script */
      lna_param.current_state = RF_WCDMA_DEFAULT_LNA_STATE;

      rfdevice_wcdma_rx_lna_setup(rfm_device,
                                  band,
                                  &lna_param,
                                  RFDEVICE_CREATE_SCRIPT,
                                  drx_script_buffer[drx_ctl]);
    }

    mdsp_cfg.device_cfg.drx[drx_ctl].script = drx_script_buffer[drx_ctl];
    mdsp_cfg.device_cfg.drx[drx_ctl].delay = 0;
  }
  
  /* Get Rx PLL settling time from RFC*/
  (void)rfc_wcdma_command_dispatch(rfm_device, RFC_WCDMA_GET_CORE_CONFIG, (void *)(&rfc_core_config));
  mdsp_cfg.device_cfg.pll_settling_time = (uint32)WCDMA_CONV_US_TO_USTMR(rfc_core_config.tune_time);

  /* Convert antenna index from RFM device */
  mdsp_cfg.ant_idx = (uint8)rfwcdma_core_util_device_to_antenna(rfm_device);

  /* Convert LNA index from RFM device */
  mdsp_cfg.lna_idx = (uint8)rfwcdma_core_util_device_to_lna_id(rfm_device, meas_scenario);

  /* Populate Multi carrier index and RFLM handle */
  mdsp_cfg.multi_car_idx = multi_carrier_idx;
  mdsp_cfg.rflm_handle = rxlm_handle;

  /* Populate IRAT scenario */
  mdsp_cfg.meas_scenario = meas_scenario;

  /* Populate CGAGC parameters */
  cgagc_param_tbl_ptr =
    &rfwcdma_mdsp_cgagc_param_table[mdsp_cfg.ant_idx][rfwcdma_core_count_carriers(multi_carrier_idx)][band];

  mdsp_cfg.cgagc_param.cm_init[0] = cgagc_param_tbl_ptr->rxagc_val_cm_init0;
  mdsp_cfg.cgagc_param.cm_init[1] = cgagc_param_tbl_ptr->rxagc_val_cm_init1;
  mdsp_cfg.cgagc_param.cm_init[2] = cgagc_param_tbl_ptr->rxagc_val_cm_init2;
  mdsp_cfg.cgagc_param.cm_init[3] = cgagc_param_tbl_ptr->rxagc_val_cm_init3;
  mdsp_cfg.cgagc_param.cm_init[4] = cgagc_param_tbl_ptr->rxagc_val_cm_init4;
  mdsp_cfg.cgagc_param.cm_init[5] = cgagc_param_tbl_ptr->rxagc_val_cm_init5;

  mdsp_cfg.cgagc_param.lna_fall_1 = cgagc_param_tbl_ptr->rxagc_cm_lnafall_1;
  mdsp_cfg.cgagc_param.lna_fall_2 = cgagc_param_tbl_ptr->rxagc_cm_lnafall_2;
  mdsp_cfg.cgagc_param.lna_rise_1 = cgagc_param_tbl_ptr->rxagc_cm_lnarise_1;
  mdsp_cfg.cgagc_param.lna_rise_2 = cgagc_param_tbl_ptr->rxagc_cm_lnarise_2;
  mdsp_cfg.cgagc_param.lna_rise_3 = cgagc_param_tbl_ptr->rxagc_cm_lnarise_3;

  /* Populate BTF delay (Tx band paramter) in the Rx Config band() as FW reads BTF delay before Tx init */
  mdsp_cfg.btf_delay = rfnv_wcdma_tbl_ptr[band]->enc_btf_dly;

  /* Calculate Tx to Rx frequency Ratio of the mid band channel  */
  rx_chan = rfwcdma_core_get_middle_DL_chan_from_band(band);
  tx_chan = rfwcdma_core_map_rx_to_tx_chan(rx_chan, band);
  rx_freq = (uint64)rfwcdma_core_get_freq_from_uarfcn(band, rx_chan, RFWCDMA_CORE_UARFCN_DL);
  tx_freq = (uint64)rfwcdma_core_get_freq_from_uarfcn(band, tx_chan, RFWCDMA_CORE_UARFCN_UL);

  /* Q15 format*/
  mdsp_cfg.fgagc_band_param.tx_rx_freq_ratio = (uint16)((tx_freq << 15) / rx_freq);

  /* Get FGAGC LNA switchpoints */
  for (lna_state = 0; lna_state < RF_WDMA_MAX_NUM_LNA_STATES - 1; lna_state++)
  {
    mdsp_cfg.fgagc_band_param.lnarise[lna_state] =
      rfnv_wcdma_get_info_from_rx_static_data((rfagc_receive_chain_type)mdsp_cfg.ant_idx,
                                              (rfcom_multi_carrier_hspa_id_type)rfwcdma_core_count_carriers(multi_carrier_idx),
                                              LNA_RISE,
                                              (lna_state),
                                              &(rfnv_wcdma_tbl_ptr[band]->rx_static_data) );

    mdsp_cfg.fgagc_band_param.lnafall[lna_state] =
      rfnv_wcdma_get_info_from_rx_static_data((rfagc_receive_chain_type)mdsp_cfg.ant_idx,
                                              (rfcom_multi_carrier_hspa_id_type)rfwcdma_core_count_carriers(multi_carrier_idx),
                                              LNA_FALL,
                                              (lna_state),
                                              &(rfnv_wcdma_tbl_ptr[band]->rx_static_data) );
  }

  /* Populate the jam detect params from nv so it can be ported to FED api, for now using a temp unused nvs until the new ones are created*/
   mdsp_cfg.jam_det_param.jam_det_en = RFLM_WCDMA_JAM_DET_EN;
   mdsp_cfg.jam_det_param.rxagc_noise_floor_sq = RFLM_WCDMA_RXAGC_NOISE_FLOOR_SQUARED;
   mdsp_cfg.jam_det_param.rxagc_sat_det_threshold = RFLM_WCDMA_RXAGC_SAT_DET_THRESHOLD;

  /* Set LNA oride flag for FTM Cal mode */
  if (IS_FTM_CAL_MODE())
  {
    mdsp_cfg.fgagc_band_param.lna_oride_flag = TRUE;
    /* Set default oride state to 0 */
    mdsp_cfg.fgagc_band_param.lna_gain_state_oride = 0;
  }
  else
  {
    mdsp_cfg.fgagc_band_param.lna_oride_flag = FALSE;
  }

  /* Pass the data structure to MDSP async module */
  if (api_status == TRUE)
  {
    api_status = rfwcdma_mdsp_async_config_rx_band(&mdsp_cfg);
  }

  /* Destroy the local RF buffer to free up the memory */
  for (lna_state = 0; lna_state < RF_WDMA_MAX_NUM_LNA_STATES; lna_state++)
  {
    api_status &= rf_buffer_destroy(lna_script_buffer[lna_state]);
  }

  for (drx_ctl = RF_WCDMA_DRX_ENABLE; drx_ctl < RF_WCDMA_DRX_CTL_NUM; drx_ctl++)
  {
    api_status &= rf_buffer_destroy(drx_script_buffer[drx_ctl]);
  }

  return api_status;

}
/*----------------------------------------------------------------------------*/
/*!
  @brief
  Configure Rx band parameters to MDSP module 

  @details
  This function will populate Rx band parameter data structure and pass down to
  RF-MDSP module.
 
  @param rxlm_handle
  RxLm handle
 
  @param rfm_device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param multi_carrier_idx
  Multi-Carrier index (bit mask) for the carrier/cell to be configured
 
  @param meas_scenario
  Enum to indicate IRAT scenario 
 
  @return
  Flag indicating the function execution success
 
  @note
  rfwcdma_core_rxctl_config_band needs to be called for band config first. Then can be called
  within the same band channel tuning without re-config band.
*/
boolean rfwcdma_core_rxctl_config_chan
(
  uint32 rxlm_handle,
  rfm_device_enum_type rfm_device,
  rfcom_wcdma_band_type band,
  uint16 channel[RFCOM_MAX_CARRIERS],
  uint32 multi_carrier_idx,
  rfwcdma_mdsp_rx_irat_meas_type meas_scenario
)
{
  rfwcdma_mdsp_rx_cfg_chsp_type mdsp_cfg;
  uint8 lna_state = 0;
  uint32 dl_freq;
  boolean api_status;
  uint16 mid_chan = 0;
  rfcom_multi_carrier_hspa_id_type num_car = RFCOM_MAX_CARRIERS;

  /* NV table pointer needs to be intialized */
  if (band < RFCOM_NUM_WCDMA_BANDS)
  {
    if (rfnv_wcdma_tbl_ptr[band] == NULL)
    {
      RF_MSG_1(RF_ERROR,
               "rfwcdma_core_rxctl_config_chan: NULL NV tbl ptr for band %d",
               band);
      return FALSE;
    }
  }

  else
  {
     RF_MSG_1(RF_ERROR,
               "rfwcdma_core_rxctl_config_chan: Not a valid band %d",
               band);
      return FALSE;
  }

  memset(&mdsp_cfg, 0, sizeof(rfwcdma_mdsp_rx_cfg_chsp_type));

  /* Convert antenna index from RFM device */
  mdsp_cfg.ant_idx = (uint8)rfwcdma_core_util_device_to_antenna(rfm_device);

  /* Convert LNA index from RFM device */
  mdsp_cfg.lna_idx = (uint8)rfwcdma_core_util_device_to_lna_id(rfm_device, meas_scenario);

  /* Populate Multi carrier index and RFLM handle */
  mdsp_cfg.multi_car_idx = multi_carrier_idx;
  mdsp_cfg.rflm_handle = rxlm_handle;

  /* Populate IRAT Scenario */
  mdsp_cfg.meas_scenario = meas_scenario;

  /* Update Rx freq comp data */
  rfwcdma_core_rxctl_update_freq_offsets((rf_path_enum_type)mdsp_cfg.ant_idx, band, channel[0]);

  /* ============================Populate LNA and DVGA offsets ====================================== */


  for (lna_state = 0; lna_state < RF_WDMA_MAX_NUM_LNA_STATES; lna_state++)
  {
    if (lna_state == 0)
    {
      /* LNA offset 0 is always 0 */
      mdsp_cfg.fgagc_chan_param.lna_offset[0] = 0;
    }
    else
    {
      if (!IS_FTM_CAL_MODE())
      {
        /* Get other LNA state offset from NV module */
        mdsp_cfg.fgagc_chan_param.lna_offset[lna_state] =
          rfwcdma_core_rxctl_lna_offset_vs_freq_val[lna_state][mdsp_cfg.ant_idx] +
          rfnv_wcdma_get_info_from_rx_dynamic_data((rfagc_receive_chain_type)mdsp_cfg.ant_idx,
                                                   (rfcom_multi_carrier_hspa_id_type)rfwcdma_core_count_carriers(multi_carrier_idx),
                                                   LNA_OFFSET,
                                                   (lna_state - 1),
                                                   &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );
      }
      else
      {
        /* Zero out LNA offset for FTM Cal mode */
        mdsp_cfg.fgagc_chan_param.lna_offset[lna_state] = 0;
      }
    }
    /* RxAGC min value = LNA offset - 512 */
    mdsp_cfg.fgagc_chan_param.rxagcmin[lna_state] = mdsp_cfg.fgagc_chan_param.lna_offset[lna_state] - 512;

    /* Set RxAGC max value to 512 for each lna state */
    mdsp_cfg.fgagc_chan_param.rxagcmax[lna_state] = 512;
  }

  /* ============================Populate XO paramters - DL freq and Inverse DL frequency ====================================== */

   num_car = rfwcdma_core_count_carriers(multi_carrier_idx);

   if (num_car == RFCOM_SINGLE_CARRIER)
   {
     mid_chan = channel[0];
   }

   else if (num_car == RFCOM_DUAL_CARRIER)
   {
     mid_chan = (channel[0] + channel[1]) / 2;
   }

   else if (num_car == RFCOM_3_CARRIER)
   {
     mid_chan = (channel[0] + channel[1] + channel[2]) / 3;
   }

   else
   {
     MSG_2(MSG_SSID_RF, MSG_LEGACY_ERROR, "Invalid multi carrier index - %d and num of carriers - %d", multi_carrier_idx, num_car);
     return 0;
   }

   dl_freq = rfwcdma_core_get_freq_from_uarfcn( band,
                                                (uint32)mid_chan,
                                                RFWCDMA_CORE_UARFCN_DL );

   mdsp_cfg.xo_chan_param.dl_freq = ((uint64)dl_freq * 1000) << 6 ; // DL frequency(Hz) in Q6 format

   /* Check for division by Zero */
   if (mdsp_cfg.xo_chan_param.dl_freq != 0)
   {
     mdsp_cfg.xo_chan_param.inv_dl_freq = (uint32)( ((uint64)1 << 52) / (dl_freq * 1000) ); // Inverse DL frequency(Hz) = (2^33) / (DL freq in Hz) in Q19 format; 52 = 33 + 19
   }

   else
   {
     MSG_3(MSG_SSID_RF, MSG_LEGACY_ERROR, "Invalid band %d, channel %d and DL frequency %d", band, mid_chan, dl_freq);
     return 0;
   }

  if (!IS_FTM_CAL_MODE())
  {
    /* Get VGA gain offset */
    mdsp_cfg.fgagc_chan_param.dvga_gain_offset =
    rfwcdma_core_rxctl_vga_gain_offset_vs_freq_val[mdsp_cfg.ant_idx] +
    rfwcdma_core_rxctl_vga_gain_offset_vs_temp_val +
    rfnv_wcdma_get_info_from_rx_dynamic_data((rfagc_receive_chain_type)mdsp_cfg.ant_idx,
                                             (rfcom_multi_carrier_hspa_id_type)rfwcdma_core_count_carriers(multi_carrier_idx),
                                             VGA_OFFSET,
                                             0,
                                              &(rfnv_wcdma_tbl_ptr[band]->rx_dyn_data) );
    /* Get Static VGA gain offset from RXLM*/
    rfwcdma_msm_get_static_dvga_offset(mdsp_cfg.rflm_handle, (int32*)mdsp_cfg.fgagc_chan_param.static_dvga_offset);
  }
  else
  {
    /* Zero out DVGA gain offset for FTM Cal mode */
    mdsp_cfg.fgagc_chan_param.dvga_gain_offset = 0;
  }


  /* Pass the data structure to MDSP async module */
  api_status = rfwcdma_mdsp_async_config_rx_chan(&mdsp_cfg);

  return api_status;

}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  This function disable and sleep ASM module for Rx

  @details
  It will call ASM disable Rx and ASM sleep for the given device. If it is
  primary device, it will loop through all supported WCDMA bands and sleep all
  associcated ASMs
 
  @param device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param script
  RF buffer script pointer
 
  @param execution_type
  Execution type of the script (immeidate or creat script)

  @param script_timing
  Execution timing of the script 

  @param disable_ind
  Indicate the disable operation type
 
  @return
  Flag indicating the function execution success
 
*/
boolean rfwcdma_core_rxctl_disable_sleep_asm
(
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band,
  rf_buffer_intf *script,
  rf_device_execution_type execution_type,
  int16 script_timing,
  rfwcdma_core_rxctl_dis_first_ind_type disable_ind
)
{
  rfcom_wcdma_band_type temp_band = RFCOM_BAND_IMT;
  boolean status = TRUE;

  if(RFWCDMA_CORE_ASM_TUNER_DIS_IND_BAND == disable_ind)
  {
    if ((rfwcdma_core_is_band_supported(band,RF_PATH_0)) &&
         rfwcdma_mc_get_asm_status_bit(device, band))
    {
      /*disable ASM RX before sleep*/
      status &= rfdevice_wcdma_asm_disable_rx(device,
                                              band,
                                              script,
                                              execution_type,
                                              script_timing
                                              );
      /* Put the ASM to sleep */
      status &= rfdevice_wcdma_asm_sleep_rx(device,
                                            band,
                                            script,
                                            execution_type,
                                            script_timing
                                            );
      status &= rfwcdma_mc_config_asm_status_bit_map(device,
                                                     temp_band,
                                                     FALSE);
    }
  }
  else if(RFWCDMA_CORE_ASM_TUNER_DIS_ALL_BANDS == disable_ind)
  {
    for (temp_band = RFCOM_BAND_IMT; temp_band<RFCOM_NUM_WCDMA_BANDS; temp_band++)
    {
      if ((rfwcdma_core_is_band_supported(temp_band,RF_PATH_0)) &&
           rfwcdma_mc_get_asm_status_bit(device, temp_band))
      {
        /*disable ASM RX before sleep*/
          status &= rfdevice_wcdma_asm_disable_rx(device,
                                                  temp_band,
                                                  script,
                                                  execution_type,
                                                  script_timing
                                                  );

         /* Put the ASM to sleep */
         status &= rfdevice_wcdma_asm_sleep_rx(device,
                                               temp_band,
                                               script,
                                               execution_type,
                                               script_timing
                                               );
         status &= rfwcdma_mc_config_asm_status_bit_map(device,
                                                        temp_band,
                                                        FALSE);
      }
    }
  }
  else
  {
    ;
  }

  return status;
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Update alternate path info to RFC for concurrency management

  @details
  This function will query concurrency manager for alternat path info and
  update port mapping in RFC
 
  @param device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @return
  Flag indicating the function execution success
 
*/
boolean rfwcdma_core_rxctl_update_alt_path
(  
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band
)
{
  rf_card_band_type rfc_band = RF_BCI_BAND;
  rfcmn_status_type alt_path_status = RFCMN_PATH_SEL_SUCCESS;
  uint8 alt_path = 0;

  /* --------------------- Update concurrency manager for alt path info ---------------------*/

  rfc_band = rfwcdma_core_convert_band_rfcom_to_rf_card(band);

    alt_path_status =  rfcmn_concurrency_mgr_update_rx_path( device,
                                                            RFCOM_WCDMA_MODE,
                                                            (int)band,
                                                            &alt_path);
  if (alt_path_status == RFCMN_PATH_SEL_FAILED)
  {
    RF_MSG_3(RF_ERROR, "rfwcdma_mc_rx_prep_tune: failed to query alt path "
                       "from concurrecy mgr on device:%d, band:%d, status:%d",
             device, rfc_band, alt_path_status);
    return FALSE;
  }
  else 
  {
    if (FALSE == rfc_update_alt_port_mapping( device, 
                                              RFCOM_WCDMA_MODE, 
                                              rfc_band, 
                                              alt_path))
    {
      RF_MSG_3(RF_ERROR, "rfwcdma_mc_rx_prep_tune: failed to update alt path "
                         "to concurrecy mgr on device:%d, band:%d, alt path:%d",
               device, rfc_band, alt_path);
      return FALSE;
    }
  }
  return TRUE;
  /* ------------------End of Update Alt. Path ------------------*/
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  This function enable and wakeup ASM module for Rx

  @details
  It will call ASM enable Rx and ASM wakeup for the given device.
 
  @param device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param script
  RF buffer script pointer
 
  @param execution_type
  Execution type of the script (immeidate or creat script)
 
  @return
  Flag indicating the function execution success
 
*/
boolean rfwcdma_core_rxctl_enable_asm
(
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band,
  rf_buffer_intf *script,
  rf_device_execution_type execution_type,
  int16 script_timing
)
{
  boolean api_status = TRUE;

  if((device >= RFM_DEVICE_0) && (device < RFM_DEVICE_4) && 
    (band >= RFCOM_BAND_IMT) && (band < RFCOM_NUM_WCDMA_BANDS))
  {
    /* Wake up ASM device */
    rfdevice_wcdma_asm_wakeup_rx(device,
                                 band,
                                 script,
                                 execution_type,
                                 script_timing);
    /* Configure ASM Rx */
    api_status &= rfdevice_wcdma_asm_enable_rx(device,
                                 band,
                                 script,
                                 execution_type,
                                 script_timing);
    api_status &= rfwcdma_mc_config_asm_status_bit_map(device,
                                 band,
                                 TRUE);
  }
  else
  {
    api_status = FALSE;
    RF_MSG_2(RF_HIGH, "rfwcdma_core_rxctl_enable_asm: Invalid param - device %d band %d",
                      device, band);
  }

  return api_status;
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  This function enable and wakeup TUNER module for Rx

  @details
  It will call TUNER enable Rx.
 
  @param device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param script
  RF buffer script pointer
 
  @param execution_type
  Execution type of the script (immeidate or creat script)

  @param script_timing
  Execution timing of the script 
 
  @return
  Variable indicating the status of API execution.
 
*/
boolean rfwcdma_core_rxctl_enable_tuner
(
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band,
  uint16 chan_num,
  rf_device_execution_type execution_type,
  rf_buffer_intf *script
)
{
  boolean api_status = TRUE;

  if((device >= RFM_DEVICE_0) && (device < RFM_DEVICE_4) && 
    (band >= RFCOM_BAND_IMT) && (band < RFCOM_NUM_WCDMA_BANDS))
  {
    api_status &= rfwcdma_core_antenna_tuner_init(device,
                                                  band,
                                                  script,
                                                  execution_type);
    api_status &= rfwcdma_core_antenna_tuner_program_rx_mode (device,
                                                              band,
                                                              chan_num,
                                                              execution_type,
                                                              script);
    api_status &= rfwcdma_mc_config_tuner_status_bit_map(device,
                                                         band,
                                                         TRUE);
    
  }
  else
  {
    api_status = FALSE;
    RF_MSG_2(RF_HIGH, "rfwcdma_core_rxctl_enable_tuner: Invalid param - device %d band %d",
                      device, band);
  }

  return api_status;
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  This function disable TUNER module for Rx

  @details
  It will call TUNER disable Rx for the given device. 
 
  @param device
  RFM device enum ID
 
  @param band
  WCDMA operation band
 
  @param script
  RF buffer script pointer
 
  @param execution_type
  Execution type of the script (immeidate or creat script)

  @param script_timing
  Execution timing of the script 

  @param disable_ind
  Indicate the disable operation type
 
  @return
  Variable indicating the status of API execution.
 
*/
boolean rfwcdma_core_rxctl_disable_tuner
(
  rfm_device_enum_type device,
  rfcom_wcdma_band_type band,
  rf_device_execution_type execution_type,
  rf_buffer_intf *script,
  int16 script_timing,
  rfwcdma_core_rxctl_dis_first_ind_type disable_ind
)
{
  boolean api_status = TRUE;
  rfcom_wcdma_band_type temp_band = RFCOM_BAND_IMT;

  if(RFWCDMA_CORE_ASM_TUNER_DIS_IND_BAND == disable_ind)
  {
    if ((rfwcdma_core_is_band_supported(band,RF_PATH_0)) &&
         rfwcdma_mc_get_tuner_status_bit(device, band))
    {
      api_status &= rfwcdma_core_antenna_tuner_disable(device,
                                         band,
                                         execution_type,
                                         script,
                                         script_timing);
      api_status &= rfwcdma_mc_config_tuner_status_bit_map(device,
                                                           band,
                                                           FALSE);
    }
  }
  else if(RFWCDMA_CORE_ASM_TUNER_DIS_ALL_BANDS == disable_ind)
  {
    for (temp_band = RFCOM_BAND_IMT; temp_band<RFCOM_NUM_WCDMA_BANDS; temp_band++)
    {
      if ((rfwcdma_core_is_band_supported(temp_band,RF_PATH_0)) &&
           rfwcdma_mc_get_tuner_status_bit(device, temp_band))
      {
        api_status &= rfwcdma_core_antenna_tuner_disable(device,
                                           temp_band,
                                           execution_type,
                                           script,
                                           script_timing);
        api_status &= rfwcdma_mc_config_tuner_status_bit_map(device,
                                                             temp_band,
                                                             FALSE);
      }
    }
  }

  return api_status;
}

