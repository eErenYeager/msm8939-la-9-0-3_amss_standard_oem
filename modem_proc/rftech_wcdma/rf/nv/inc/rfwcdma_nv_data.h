#ifndef RFWCDMA_NV_DATA_H
#define RFWCDMA_NV_DATA_H

/*! 
  @file
  rfwcdma_nv_data.h
 
  @brief
  This file contains prototypes and definitions to be used by 
  WCDMA NV Data.
*/

/*==============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rftech_wcdma/rf/nv/inc/rfwcdma_nv_data.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/11/14   aa     Initial Revision.

==============================================================================*/

#include "comdef.h"
#include "rfnv_wcdma_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------*/
/*! Data structure used to keep track of WCDMA NV */
typedef struct
{
  rfcommon_nv_status_type nv_load_status;
  /*!< WCDMA NV load status */

  boolean is_nv_loaded[RFNV_WCDMA_NUM_TABLES];
  /*!< Flag indicating if the NV is loaded successfully for each Band */

  boolean nv_lists_init_done;
  /*!< Flag indicating if the NV List init is done */

} rfwcdma_nv_data_type;


#ifdef __cplusplus
}
#endif

#endif /* RFWCDMA_NV_DATA_H */


