#ifndef RFNV_WCDMA_1500_H
#define RFNV_WCDMA_1500_H

/*!
  @file
  rfnv_wcdma_1500.h

  @brief

  @details
  This header file contains definitions  to interface with RF NV for WCDMA.

*/

/*===========================================================================

Copyright (c) 2008-2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rftech_wcdma/rf/nv/inc/rfnv_wcdma_1500.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/08/13   kcj     Can't use designated initialization with OFT's compiler. 
06/05/13   kcj     Dynamically allocated NV struct memory. 
09/30/10   ac      added efs support
05/12/10   ac      added B11 nv support for  j8220
08/13/09   ckl     Merge diversity NV items into main NV item list.
03/15/09   ckl     Deleted rfnv.h
03/09/09   ckl     Code cleanup
08/21/03   ap      Final modifications to support WCDMA 1500
06/17/03   eh      Initial Revision.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "rfnv_wcdma_types.h"

/*===========================================================================

                       PUBLIC DATA DECLARATIONS

===========================================================================*/

extern rfnv_wcdma_nv_items_list rfnv_wcdma_1500_nv_items_list;

/*===========================================================================

                         FUNCTION PROTOTYPES

===========================================================================*/

/*--------------------------------------------------------------------------*/
void rfnv_wcdma_1500_init_nv_item_list(void);

/*--------------------------------------------------------------------------*/
extern void *rfnv_wcdma_1500_item_table(void);

#endif /* RFNV_WCDMA_1500_H */
