#ifndef __DRV_API_H__
#define __DRV_API_H__

/*
   Copyright (C) 2012 QUALCOMM Technologies Incorporated.
   All rights reserved.
   Qualcomm Confidential and Proprietary

   $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/audio_avs/vsd/inc/protected/drv_api.h#1 $
   $Author: mplp4svc $
*/

/* public */
#define DRV_CMDID_INIT ( 0x00012A6F )
#define DRV_CMDID_POSTINIT ( 0x00012A70 )
#define DRV_CMDID_PREDEINIT ( 0x00012A71 )
#define DRV_CMDID_DEINIT ( 0x00012A72 )

/* private */
#define DRV_CMDID_RUN ( 0x00012A77 )

#endif /* __DRV_API_H__ */

