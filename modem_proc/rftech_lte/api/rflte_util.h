/*! 
 @file rflte_util.h 
 
 @brief 
  This is the header file for utility functions for the LTE technology. 
 
 @details 
 
*/ 
  
/*=========================================================================== 
  
Copyright (c) 2013-14 by Qualcomm Technologies, Inc.  All Rights Reserved. 
  
                           EDIT HISTORY FOR FILE 
  
This section contains comments describing changes made to this file. 
Notice that changes are listed in reverse chronological order. 

$Header: 
  
When       who     what, where, why 
-------------------------------------------------------------------------------
11/17/14   dw      Changes to support split bands based on split specified in RFC.
09/02/14   php     Use Char channel list for Pin compensation
07/31/14   ndb     Added support for extended EARFCN for LTE
06/18/14   sbm     Added api to determine if hdet Nv is present or not.
06/04/14    pv     Added support for FBRx RB Based Freq comp.
04/17/14   svi     PA static NV and code redesign
04/11/14   bsh     Split Band Support
03/14/14   bsh     NlIC changes - move ftl calculation to NLIC component 
03/12/14   bsh     Adding NLIC support - calcualte ftl based on rx freq error
03/13/14   svi     Fix UL DL EARFCN Offset for LTE
02/04/14    pv     Added support for FBRx Gain error processing.
08/31/13   bsh     Initial version. 
  
============================================================================*/ 
#ifndef RFLTE_UTIL_H
#define RFLTE_UTIL_H

#include "appmgr.h"
#include "msgr.h"
#include "rfa_msgr.h"
#include "rfcom.h"
#include "rflte_nv.h"

/*! FBRx Related Includes */
#include "rfcommon_fbrx_api.h"

#ifdef __cplusplus
extern "C" {
#endif

#define LTE_IQ_BUFFER_SIZE  16384  /*hardcoded for 16KB*/

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Band and NS dependent AMPR constant definitions
 
*/
/* B23 - NS11: Table 6.2.4-5 36.101 (Release 11.2) */
#define RFLTE_UTIL_B23_NS_11_LOWER_FREQ_BOUND_KHZ  2000000
#define RFLTE_UTIL_B23_NS_11_UPPER_FREQ_BOUND_KHZ  2012500

/* B26 - NS15: Table 6.2.4-9\6.2.4-10 36.101 (Release 11) */
#define RFLTE_UTIL_B26_NS_15_LOWER_FREQ_BOUND_KHZ  845000
#define RFLTE_UTIL_B26_NS_15_UPPER_FREQ_BOUND_KHZ  849000
/* Size of the RB in KHZ  Please refer the TS 32.101 V11.2.0 (2012-09)*/

#define RFLTE_UTIL_RB_IN_KHZ 180

/*Please refer the table 6.2.2-1 Note 2. Bandedge in Khz where the maxpower can be relaxed*/
#define RFLTE_UTIL_BAND_EDGE_LIMIT_IN_KHZ 4000

/*Absolute value macro*/
#define RFLTE_UTIL_ABS(x)               (((x) < 0) ? -(x) : (x))
/*Round Macro*/
#define RFLTE_UTIL_FLOOR(x,y)           rflte_util_floor(x, y)
/*Round value macro*/
#define RFLTE_UTIL_ROUND(x,y)           RFLTE_UTIL_FLOOR(((2 * (x)) + (y)), (2 * (y)))
/* Saturate Macro: This is used for clipping the results of adjustment
  calculations where the newly calculated value may exceed the legal range
  for the control.*/
#define RFLTE_UTIL_SATURATE( val, min, max )  MAX( MIN( (val), (max) ), (min) )
/*Sign Macro*/
#define RFLTE_UTIL_SIGN(x)              (((x) < 0) ? -1 : 1)
#define RFLTE_UTIL_INT(x)    ((int)x)
#define RFLTE_UTIL_FRAC(x,y) ( RFLTE_UTIL_ABS((int)((float)(((float)x) - ((int)x))*y)) )

#define RFLTE_UTIL_MIN(val1,val2,val3,val4,val5,val6)   MIN( MIN(val1,val2),MIN(val3,MIN(val4,MIN(val5,val6))))

/*----------------------------------------------------------------------------*/
/*!
  @brief
  FDD UL and DL EARFCN Offset Macros
 
*/
/*!> For FDD Bands between B1 to B28 */
#define RFLTE_UTIL_FDD_DL_UL_EARFCN_OFFSET_B1_TO_B28 18000

/*!> For FDD Bands  B30 and B31 */
#define RFLTE_UTIL_FDD_DL_UL_EARFCN_OFFSET_B30_B31   17890

/*----------------------------------------------------------------------------*/
/*!
  @brief
  TDD UL and DL EARFCN Offset Macros
 
*/
#define RFLTE_UTIL_TDD_DL_UL_EARFCN_OFFSET           0


/*----------------------------------------------------------------------------*/


typedef enum
{
  RF_LTE_DISABLED_DEBUG_MASK = 0x0,  
  RF_LTE_LEGACY_ENABLE_ALL_MASK = 0x1,  
  RF_LTE_TX_PLL_ERROR_CORRECTION_DEBUG_MASK = 0x2,
  RF_LTE_SMPS_PDM_INTERPOLATE_DEBUG_MASK = 0x4,
  RF_LTE_TEMP_COMP_DEBUG_MASK = 0x8,
  RF_LTE_MTPL_DEBUG_MASK = 0x10,
  RF_LTE_FREQ_COMP_DEBUG_MASK = 0x20,  
  RF_LTE_IRAT_DEBUG_MASK = 0x40,
  RF_LTE_MSG_DISPATCH_DEBUG_MASK = 0x80
} rf_lte_debug_msg_enums;

/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_uarfcn_from_rx_freq(uint32 rx_freq, rfcom_lte_band_type band);

/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_uarfcn_from_tx_freq(uint32 tx_freq, rfcom_lte_band_type band);

/*----------------------------------------------------------------------------*/
void rflte_util_get_tx_freq_lo_hi_from_band(rfcom_lte_band_type band, uint32 *f_ul_low, uint32 *f_ul_hi);

/*----------------------------------------------------------------------------*/
rfcom_lte_band_type rflte_util_get_band_from_rx_uarfcn(rfcom_lte_earfcn_type rx_chan);

/*----------------------------------------------------------------------------*/
rfcom_lte_band_type rflte_util_get_band_from_tx_uarfcn(rfcom_lte_earfcn_type tx_chan);

/*----------------------------------------------------------------------------*/
uint32 rflte_util_get_tx_freq_from_tx_chan(rfcom_lte_earfcn_type tx_chan);

/*----------------------------------------------------------------------------*/
void rflte_util_get_tx_freq_lo_hi_from_rb(rfcom_lte_earfcn_type tx_chan, 
                                          rfcom_lte_bw_type tx_bw,
                                          uint8 rb_start, 
                                          uint8 rb_block, 
                                          uint32 *f_ul_low, 
                                          uint32 *f_ul_hi);

/*----------------------------------------------------------------------------*/
rflte_nv_tbl_type *rflte_util_init_nv_table(rfcom_device_enum_type device, rfcom_lte_band_type band);

/* ----------------------------------------------------------------------- */
rf_card_band_type  rflte_util_lte_band_helper(rfcom_lte_band_type rfcom_band);

/* ----------------------------------------------------------------------- */
rfcom_lte_band_type rflte_util_convert_rfc_band_to_rfcom_band(rf_card_band_type rfc_band);

/* ----------------------------------------------------------------------- */
rflte_nv_supported_band_type rflte_util_convert_rfcom_band_to_rfnv_band(rfcom_lte_band_type rfcom_band);
/* ----------------------------------------------------------------------- */
void rflte_util_get_tx_cal_boundaries(tx_band_cal_type *tx_band_cal_data, rfcom_device_enum_type device, rfcom_lte_band_type band);
/* ----------------------------------------------------------------------- */
void rflte_util_get_fbrx_cal_boundaries(rfcom_lte_earfcn_type *fbrx_cal_chan,uint8 fbrx_cal_chan_size);
/* ----------------------------------------------------------------------- */
rfcommon_fbrx_tx_cfg_type rflte_util_get_fbrx_bw_for_sys_bw(rfcom_lte_bw_type bw);

/*----------------------------------------------------------------------------*/
uint8 
rflte_util_get_pa_range_from_pa_state
(
  uint8                  pa_state, 
  rflte_nv_tbl_type     *rflte_nv_tbl_ptr,
  rfm_device_enum_type   rfm_device
);

/*----------------------------------------------------------------------------*/
rfcom_lte_band_type rflte_util_get_first_band_from_mask(uint64 band_mask);

/*----------------------------------------------------------------------------*/
boolean rflte_util_is_band_tdd( rfcom_lte_band_type rf_band);

/*----------------------------------------------------------------------------*/
boolean rflte_util_is_hdet_nv_present(rflte_nv_tbl_type   *rflte_nv_tbl_ptr);

/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_rx_uarfcn_from_tx_uarfcn(rfcom_lte_earfcn_type tx_chan,rfcom_lte_band_type band);

/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_tx_uarfcn_from_rx_uarfcn(rfcom_lte_earfcn_type rx_chan,rfcom_lte_band_type band);

/*----------------------------------------------------------------------------*/
rfnv_item_id_enum_type rflte_util_get_txlin_nvid(rfcom_lte_band_type band);

/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_ul_arfcn_from_freq (uint32 freq);

/*----------------------------------------------------------------------------*/
rfcom_lte_band_type rflte_util_convert_nv_band_to_rfcom_band(rflte_nv_supported_band_type band);
/*----------------------------------------------------------------------------*/
boolean rflte_util_check_rfc_band_support(void);
/*----------------------------------------------------------------------------*/
rfnv_item_id_enum_type rflte_util_get_hdetVsAgc_nvid(rfcom_lte_band_type band);
/*----------------------------------------------------------------------------*/
uint16 rflte_util_get_num_rb_from_bandwidth(rfcom_lte_bw_type bw);
/*----------------------------------------------------------------------------*/
boolean rflte_util_get_min_max_rx_freqs(rfcom_lte_band_type band, uint32* min, uint32* max);
/*----------------------------------------------------------------------------*/
uint64 rflte_util_get_band_mask(rfcom_lte_band_type band);
/*----------------------------------------------------------------------------*/
void rflte_util_get_min_max_tx_freqs(rfcom_lte_band_type band,
                                   uint32* ful_low,
                                   uint32* ful_high);
/*----------------------------------------------------------------------------*/
boolean rflte_util_get_band_num(rfcom_lte_band_type band,uint16* num);
/*----------------------------------------------------------------------------*/
void rflte_util_get_fullband_from_subband(rfcom_lte_band_type* lte_rfcom_band);
/*----------------------------------------------------------------------------*/
uint32 rflte_util_get_tx_freq_from_uarfcn(rfcom_lte_earfcn_type tx_chan, rfcom_lte_band_type band);
/*----------------------------------------------------------------------------*/
uint16 rflte_util_get_bw_in_khz(rfcom_lte_bw_type bw);
/*----------------------------------------------------------------------------*/
uint32 rflte_util_get_rx_freq_from_uarfcn(rfcom_lte_earfcn_type rx_chan, rfcom_lte_band_type band);
/*----------------------------------------------------------------------------*/
int32 rflte_util_floor(int32 dividend, int32 divisor);
/*----------------------------------------------------------------------------*/
int32 rflte_util_get_txagc_from_dbm10( int16 dbm_10);
/*----------------------------------------------------------------------------*/
int16 rflte_util_get_dbm10_from_txagc( int32 txagc);
/*----------------------------------------------------------------------------*/
rfcom_lte_earfcn_type rflte_util_get_ul_dl_earfcn_offset( rfcom_lte_band_type lte_band );
/*----------------------------------------------------------------------------*/
/* New APIs to support band detection from RFC based on channel, bw and path override index */
rfcom_lte_band_type 
rflte_util_get_band_from_rx_uarfcn_bw_path_override(rfcom_lte_band_type current_band,
                                                    rfcom_device_enum_type device,
                                                    rfcom_lte_earfcn_type rx_chan,
                                                    rfcom_lte_bw_type bw,
                                                    uint8 path_override_index);
/*----------------------------------------------------------------------------*/
rfcom_lte_band_type 
rflte_util_get_band_from_tx_uarfcn_bw_path_override(rfcom_lte_band_type current_band,
                                                    rfcom_lte_earfcn_type tx_chan,
                                                    rfcom_lte_bw_type bw,
                                                    uint8 path_override_index);
/*----------------------------------------------------------------------------*/
uint64 rflte_util_get_splitbands_from_fullband(rfcom_lte_band_type band);
/*----------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif

