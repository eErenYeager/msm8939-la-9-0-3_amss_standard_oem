#ifndef FTM_LTE_SELFTEST_H
#define FTM_LTE_SELFTEST_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                              FTM LTE Self Test

GENERAL DESCRIPTION
  This is the header file for FTM LTE Self Testing.

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/10/14   daa     Created

===========================================================================*/

/*===========================================================================

                         INCLUDE FILES FOR MODULE

===========================================================================*/
#include "ftm.h"
#include "ftm_lte_common_dispatch.h"

/*===========================================================================

                         EXTERNAL GLOBAL VARIABLES

===========================================================================*/

/*===========================================================================

                              DEFINITIONS

===========================================================================*/

typedef struct {

  boolean enable_tx_pwr_meas;
  boolean enable_aclr_meas;
  boolean enable_evm_meas;
  boolean enable_vswr_meas;

  uint16 num_averages;
  uint32 capture_offset;
  uint32 tx_measurement_config;

} ftm_lte_selftest_meas_params_struct;

/* Only expose necessary functions, no reason to expose the helper functions */
uint8 ftm_lte_selftest
(
  ftm_lte_selftest_meas_params_struct params,
  ftm_rf_lte_fbrx_iq_acquired_samples_data_type *ftm_rf_lte_fbrx_iq_acquired_samples_data_input,
  boolean *ftm_selftest_pwr_only
);

boolean ftm_lte_obtain_fbrx_noise_nv_item(rfcom_lte_band_type band, rfnv_item_id_enum_type *nv_item);

void ftm_lte_obtain_fbrx_gain_stage(void);


#endif /* FTM_LTE_SELFTEST_H */