
#ifndef QFE2550_CALIBRATION_DATA_H
#define QFE2550_CALIBRATION_DATA_H
/*
WARNING: This QFE2550 physical device driver is auto-generated.

Generated using: qfe_physical_device_autogen.pl 
Generated from-  

	File: QFE2550_RFFE_Settings.xlsx 
	Released: 8/14/2014
	Author: ycchiou
	Revision: v0.7
	Change Note: � Remove Reg0x2B on Init and Disable
� Add linearizer disable on preconfig
� Modify the �tuner_dac_cmn_linearity_improved_settings� API

*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of QFE2550 CHIP.

Copyright (c) 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

$Header: //source/qcom/qct/modem/rfdevice/qfe2550/main/latest/etc/qfe_physical_device_autogen.pl#1 : ybansal : 2014/03/29 07:35:10 60============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/


#include "rfdevice_physical_device.h"

#include "qfe2550_tuner_typedef_ag.h"
 
typedef struct
{
 /* C tuner cal data */
  boolean c_tuner_data_valid;  
  uint8 c_tuner_val[QFE2550_NUM_C];
 
}qfe2550_rc_tuner_cal_data_type;

/*Calibrated data */ 
typedef struct qfe2550_cal_data
{
  /* Device cfg info */
  rfc_device_cfg_info_type cfg;

  /* RC tuner cal data */
  qfe2550_rc_tuner_cal_data_type rc_tuner_data;
   
}qfe2550_cal_data_type_declaration;

#endif