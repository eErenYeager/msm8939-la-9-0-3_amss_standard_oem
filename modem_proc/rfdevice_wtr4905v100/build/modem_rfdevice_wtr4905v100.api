#====include path optimized=====================================================
#
#
# GENERAL DESCRIPTION
#
# Copyright (c) 2011 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved
#
# Qualcomm Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
# $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr4905v100/build/modem_rfdevice_wtr4905v100.api#1 $
# $DateTime: 2015/01/27 06:42:19 $
#
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 10/03/14   sd      Support both v100 andd v200 chips in the same component
# 06/02/13   ra      Add initial changes to support MDM9x35 build
# 02/04/13   dej     Branch MSM layer to RFMODEM component(s)
# 09/05/12   shb     Added '${INC_ROOT}/rfdevice_interface/api'
# 07/27/12   sar     Updated for Nikel 3.0 and 4.0 compatibility.
# 07/25/12   sar     Updated RFDEVICE Restricted api
# 06/23/12   sar     Created
#===============================================================================

Import('env')

if env.PathExists('${INC_ROOT}/rfdevice_wtr4905v100'):
    env.Replace(RFDEVICE_WTR4905V100_ROOT = '${INC_ROOT}/rfdevice_wtr4905v100')
else:    
    env.Replace(RFDEVICE_WTR4905V100_ROOT = '${INC_ROOT}/modem/rfdevice_wtr4905v100')

if env.PathExists('${INC_ROOT}/rfa'):
    env.Replace(INC_TMP_ROOT = '${INC_ROOT}')
else:    
    env.Replace(INC_TMP_ROOT = '${INC_ROOT}/modem')

if env.get('CHIPSET') in ('mdm9x35'):
	env.Replace(RFMODEM_ROOT = '${INC_TMP_ROOT}/rfmodem_bolt')
else:
	env.Replace(RFMODEM_ROOT = '${INC_TMP_ROOT}/rfmodem_dime')

env.PublishPublicApi('RFDEVICE_WTR4905V100', [ '${RFDEVICE_WTR4905V100_ROOT}/api',
                                               '${RFDEVICE_WTR4905V100_ROOT}/ag/api', ])

env.PublishRestrictedApi('VIOLATIONS',[
        '${INC_ROOT}/rfa/device/rfdev_intf/inc',
        '${INC_ROOT}/rfa/rf/common/rf/core/inc',
        '${INC_ROOT}/rfa/rf/hal/common/inc',
        '${INC_ROOT}/geran/cust/inc',
        '${INC_ROOT}/rfa/variation/inc',
        '${INC_ROOT}/rfa/rf/common/rf/rfc/inc',
        '${INC_ROOT}/rfa/rfc/target/${CHIPSET}/inc',
        '${INC_ROOT}/rfa/cust/inc',
        '${INC_ROOT}/fw/api/common',
        '${INC_ROOT}/geran/variation/inc',
        '${INC_ROOT}/rfa/rf/common/rf/nv/inc',
        '${INC_ROOT}/mcs/hwio/inc',
        '${INC_ROOT}/rfa/rf/cdma/common/rf/inc',
        '${INC_ROOT}/rfa/rf/rfd/common/inc',
        '${INC_ROOT}/rfa/rf/rfd/nikel/inc',
        '${INC_ROOT}/rfa/rfc/common/inc',
        '${INC_ROOT}/rfnv/api',
        '${RFMODEM_ROOT}/hal/common/inc',
        '${INC_ROOT}/mcs/hwio/inc/${CHIPSET}',
        '${INC_ROOT}/rfa/rf/gsm/rf/core/src',
        '${INC_ROOT}/rfa/rf/hal/nikel/common/inc',
        '${INC_ROOT}/rfa/rf/gsm/rf/rfc/inc',
        '${INC_ROOT}/rfa/rf/mdsp/qdsp6_gsm/inc',
        '${INC_ROOT}/rfa/rf/wcdma/rf/core/src',
        '${INC_ROOT}/rfa/rf/mdsp/qdsp6_common/inc',
        '${INC_ROOT}/rfa/rf/tdscdma/rf/nv/inc',
        '${INC_ROOT}/rfa/rf/gsm/rf/nv/inc',
        '${INC_ROOT}/rfa/rf/hal/gnss/gen8a/inc',
        '${INC_ROOT}/rfa/rf/common/rf/mc/inc',
        '${INC_ROOT}/rfa/rf/lte/rf/core/inc',
        '${INC_ROOT}/rfa/rf/lte/rf/nv/inc',
        '${INC_ROOT}/rfa/rf/lte/rf/rfc/inc',
        '${INC_ROOT}/rfa/rf/mdsp/qdsp6_lte/inc',
        '${INC_ROOT}/rfa/rf/mdsp/qdsp6_wcdma/inc',
        '${INC_ROOT}/rfa/rf/task/common/inc',
        '${INC_ROOT}/rfa/rf/tdscdma/rf/core/inc',
        '${INC_ROOT}/rfa/rf/tdscdma/rf/rfc/inc',
        '${INC_ROOT}/rfa/rf/wcdma/rf/nv/inc',
        '${INC_ROOT}/rfa/rf/wcdma/rf/rfc/inc',
        '${INC_ROOT}/rfa/rf/rfd/common/protected',
        '${INC_ROOT}/rfdevice_interface/api',
        ])

env.PublishRestrictedApi('RFDEVICE_WTR4905V100', [ '${RFDEVICE_WTR4905V100_ROOT}/api',
                                                   '${RFDEVICE_WTR4905V100_ROOT}/ag/api', ])
