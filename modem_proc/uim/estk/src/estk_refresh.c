/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                      ESTK  SHORT  MESSAGE


GENERAL DESCRIPTION : ENHANCED STK layer REFRESH support

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2013-2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/estk/src/estk_refresh.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/03/14   vr      Do not send Alpha to client when IGNORE_ALPHA is enabled
01/16/14   hn      Replace MSG_* with UIM_MSG_* macros to save memory
12/26/13   sw      Removed error print for refresh success result codes
10/04/13   sw      Reduced F3 messages
09/13/13   gm      Support for Recovery and hot-swap
07/01/13   hn      Send command_number, additional_info and other_info to ESTK  
03/29/13   hn      Initial version

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "uim_variation.h"
#include "target.h"
#include "customer.h"
#include "comdef.h"
#include "intconv.h"
#include "uim_msg.h"

#ifdef FEATURE_ESTK
#include "estk_refresh.h"

/*===========================================================================
FUNCTION: estk_process_refresh_cnf

DESCRIPTION:
  GSTK calls this ESTK function to process the REFRESH CNF.

PARAMETERS:
  cmd_ptr : [Input] ESTK command pointer
 
DEPENDENCIES:
  None

RETURN VALUE:
  estk_status_enum_type
    ESTK_SUCCESS: - When REFRESH is successfully handled
    ESTK_ERROR:   - When there is failure in handling REFRESH

COMMENTS:
  None

SIDE EFFECTS:
  None

===========================================================================*/
estk_result_enum_type estk_process_refresh_cnf(
  estk_cmd_type *cmd_ptr
)
{
  UIM_MSG_HIGH_1("ESTK_REFRESH: estk_process_refresh_cnf: cmd_ptr=0x%x",
                 cmd_ptr);

  if(cmd_ptr == NULL)
  {
    return ESTK_ERROR;
  }
  
  if (!ESTK_IS_VALID_SLOT_ID(cmd_ptr->hdr.sim_slot_id)
      || cmd_ptr->hdr.sim_slot_id == GSTK_SLOT_ID_MAX)
  {
    UIM_MSG_ERR_1("invalid slot id 0x%x!", cmd_ptr->hdr.sim_slot_id);
    return ESTK_ERROR;
  }

  UIM_MSG_HIGH_1("ESTK_REFRESH: Refresh result %d received",
                 cmd_ptr->cmd_data.refresh_cnf.refresh_result);
  switch(cmd_ptr->cmd_data.refresh_cnf.refresh_result)
  {
    case GSTK_COMMAND_PERFORMED_SUCCESSFULLY:
    case GSTK_COMMAND_PERFORMED_WITH_PARTIAL_COMPREHENSION:
    case GSTK_COMMAND_PERFORMED_WITH_MISSING_INFORMATION:
    case GSTK_PCMD_REFRESH_PERFORMED_WITH_ADDITIONAL_EFS_READ:
    case GSTK_COMMAND_SUCCESSFUL_BUT_REQUESTED_ICON_NOT_DISPLAYED:
    case GSTK_REFRESH_PERFORMED_BUT_USIM_NOT_ACTIVE:
      /* Refresh succeeded */
      if((estk_shared_info.features_enabled & GSTK_CFG_FEATURE_ESTK_QMI) &&
         (estk_curr_inst_ptr->state == ESTK_WAIT_FOR_DISPLAY_ALPHA_CNF_ST))
      {
        UIM_MSG_HIGH_0("ESTK_REFRESH - Refresh succeeded, pending Icon displayed response");
        estk_curr_inst_ptr->state = ESTK_PENDING_TR_ST;
      }
      else
      {
        UIM_MSG_HIGH_0("ESTK_REFRESH - Refresh succeeded, send TR");
        (void)estk_send_terminal_response(
              cmd_ptr->hdr.cmd_ref_id,                      /* command_details_ref */
              GSTK_REFRESH_CNF,                             /* command response    */
              cmd_ptr->cmd_data.refresh_cnf.refresh_result, /* general result      */
              cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr, /* additional result   */
              cmd_ptr->cmd_data.refresh_cnf.other_info_ptr);/* extra param         */
      }
      break;
    default:
      /* Refresh errors, send TR immediately */
      UIM_MSG_HIGH_0("ESTK_REFRESH - Refresh error, send TR");
      (void)estk_send_terminal_response(
                  cmd_ptr->hdr.cmd_ref_id,                      /* command_details_ref */
                  GSTK_REFRESH_CNF,                             /* command response    */
                  cmd_ptr->cmd_data.refresh_cnf.refresh_result, /* general result      */
                  cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr, /* additional result   */
                  cmd_ptr->cmd_data.refresh_cnf.other_info_ptr);
      break;
  }

  if (cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr)
  {
    if (cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr->additional_info_ptr)
    {
      gstk_free(cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr->additional_info_ptr);
      cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr->additional_info_ptr = NULL;
    }
    gstk_free(cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr);
    cmd_ptr->cmd_data.refresh_cnf.additional_result_ptr = NULL;
  }

  if (cmd_ptr->cmd_data.refresh_cnf.other_info_ptr)
  {
    gstk_free(cmd_ptr->cmd_data.refresh_cnf.other_info_ptr);
    cmd_ptr->cmd_data.refresh_cnf.other_info_ptr = NULL;
  }

  return ESTK_SUCCESS;
} /* estk_process_refresh_cnf */


/*===========================================================================
FUNCTION: estk_process_refresh_req

DESCRIPTION:
  GSTK calls this ESTK function to process the REFRESH request.

PARAMETERS:
  gstk_req_ptr : [Input] gstk REFRESH cmd pointer
 
DEPENDENCIES:
  None

RETURN VALUE:
  estk_status_enum_type
    ESTK_SUCCESS: - When REFRESH is successfully handled
    ESTK_ERROR:   - When there is failure in handling REFRESH

COMMENTS:
  None

SIDE EFFECTS:
  None

===========================================================================*/
estk_result_enum_type estk_process_refresh_req(
  gstk_cmd_from_card_type *gstk_req_ptr
)
{
  UIM_MSG_HIGH_1("estk_process_refresh_req: gstk_req_ptr=0x%x", gstk_req_ptr);

  ESTK_CHECK_NULL_PTR(estk_curr_inst_ptr);

  /* Check input parameter */
  if (gstk_req_ptr == NULL)
  {
    UIM_MSG_ERR_0("NULL ptr in estk_process_refresh_req()");
    return ESTK_ERROR;
  }
  UIM_MSG_HIGH_2("In estk_process_refresh_req(): alpha_length=%d, alpha_text=%s",
                 gstk_req_ptr->cmd.refresh_pro_cmd_req.alpha.length,
                 gstk_req_ptr->cmd.refresh_pro_cmd_req.alpha.text);
  if(!(estk_shared_info.features_enabled & GSTK_CFG_FEATURE_ESTK_IGNORE_ALPHA))
  {
    /* Check Alpha availability */
    /* Even in case of null data object in ALPHA (i.e. length = '00' and no value part),
       GSTK make sures at least NULL char is present in alpha text and length includes
       that NULL character. So the min length is one and hence we need to check for
       length more than one */
    if(((gstk_req_ptr->cmd.refresh_pro_cmd_req.alpha.length) > 1 &&
       (gstk_req_ptr->cmd.refresh_pro_cmd_req.alpha.text)) ||
       (estk_shared_info.features_enabled & GSTK_CFG_FEATURE_ESTK_QMI))
    {
      if (estk_send_alpha_to_display_client(gstk_req_ptr, TRUE) != ESTK_SUCCESS)
      {
        UIM_MSG_ERR_0("Send Refresh Alpha Display failed");
        /* priv data allocated is freed when a TR is sent or
                    when a End Proactive Session is received */
        return ESTK_ERROR;
      }
      else
      {
        UIM_MSG_HIGH_0("ESTK state: ESTK_WAIT_FOR_DISPLAY_ALPHA_CNF_ST");
        estk_curr_inst_ptr->state = ESTK_WAIT_FOR_DISPLAY_ALPHA_CNF_ST;
        /* Even though this is only a Display Alpha, start a timer to wait 
           for the user response */
        (void)rex_clr_timer(&estk_shared_info.user_conf_timer);
        (void)rex_set_timer(&estk_shared_info.user_conf_timer,
                            ESTK_USER_CONF_TIMEOUT);
      }
    }
  }

  /* If not waiting for display confirmation, move to pending TR state */
  if(estk_curr_inst_ptr->state != ESTK_WAIT_FOR_DISPLAY_ALPHA_CNF_ST)
  {
    UIM_MSG_HIGH_0("ESTK state: ESTK_PENDING_TR_ST");
    estk_curr_inst_ptr->state = ESTK_PENDING_TR_ST;
  }

  return ESTK_SUCCESS;
} /* estk_process_refresh_req */

#endif /* FEATURE_ESTK */

