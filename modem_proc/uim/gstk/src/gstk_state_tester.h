#ifndef GSTK_STATE_TESTER_H
#define GSTK_STATE_TESTER_H
/*===========================================================================


              G S T K    S T A T E     T E S T E R     H E A D E R


GENERAL DESCRIPTION

  This header file is the gstk state tester header protocol and function
  prototypes.

EXTERNALIZED FUNCTIONS
  gstk_start_state_test
    This function start the state test


INITIALIZATION AND SEQUENCING REQUIREMENTS


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2004-2005 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/gstk/src/gstk_state_tester.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/11/05   sst     Lint fixes
03/05/04   tml     Initial Version



===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

#include "uim_variation.h"
#include "gstki.h"
#include "gstk_exp.h"
#include "uim.h"

#ifdef FEATURE_GSTK_STATE_TESTER





typedef enum {
  GSTK_TEST_GSDI_SIG,
  GSTK_TEST_TERM_PROFILE,
  GSTK_TEST_MODE_CHANGE,
  GSTK_TEST_TERM_PROFILE_RSP,
  GSTK_TEST_RESET,
  GSTK_TEST_END
} gstk_state_tester_event_enum_type;


typedef struct {
  gstk_state_enum_type                            expected_state;
  gstk_slot_id_enum_type                          expected_pref_slot;
  gstk_slot_id_enum_type                          expected_curr_slot;
  gstk_state_tester_event_enum_type               next_event_type;
  union {
    gsdi_sim_events_T                               simulated_gsdi_event;
    uim_slot_type                                   simulated_mode_chg_slot;
    boolean                                         simulate_tp_rsp;
  }next_event;
} gstk_state_tester_event_state_type;


/*===========================================================================
FUNCTION gstk_start_state_test

DESCRIPTION
  This function starts the state tests

PARAMETERS
  None

DEPENDENCIES
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None

SEE ALSO
  None
===========================================================================*/
extern void gstk_start_state_test();

/*===========================================================================
FUNCTION gstk_start_next_state_test

DESCRIPTION
  This function starts the next sequence in the state tests

PARAMETERS
  None

DEPENDENCIES
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None

SEE ALSO
  None
===========================================================================*/
extern void gstk_start_next_state_test();

/*===========================================================================
FUNCTION gstk_start_next_state_test

DESCRIPTION
  This function starts the next sequence in the state tests

PARAMETERS
  None

DEPENDENCIES
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None

SEE ALSO
  None
===========================================================================*/
extern uim_slot_type gstk_state_tester_get_slot_for_app(void );

#endif /*FEATURE_GSTK_STATE_TESTER */
#endif /* GSTK_H */
