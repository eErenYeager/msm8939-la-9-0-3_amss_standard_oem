#ifndef CUSTPBM_H
  #define CUSTPBM_H
  /*===========================================================================

              " C u s t -  P B M "   H E A D E R   F I L E

  DESCRIPTION
  Configuration for PBM Feature.

  Copyright (c) 2008-2010 by QUALCOMM Technologies, Inc(QTI). 
  All Rights Reserved.
  ===========================================================================*/
  /*===========================================================================

                        EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/cust/inc/custpbm.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

  when       who     what, where, why
  --------   ---     ----------------------------------------------------------
  12/06/10   pk   Initial Revision.
  ===========================================================================*/

/* SCMM 3.2 */
#if defined(T_QSC6695) && defined (FEATURE_DUAL_SIM)
#define FEATURE_PBM_CSIM_DSDS_SUPPORT
#endif

#if defined (FEATURE_MDM9K_TGT_4_0)
#define PBM_SESSIONLIB /* for clients to use the PBM session based interface, currently defined for MDM 9K 4.0 */
#endif
#define FEATURE_QMI_PBM_TASK
#endif /* CUSTPBM_H */