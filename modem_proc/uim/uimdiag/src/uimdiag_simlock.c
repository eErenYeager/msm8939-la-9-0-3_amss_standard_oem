/*===========================================================================

===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimdiag/src/uimdiag_simlock.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who   what, where, why
--------   ---   -----------------------------------------------------------
04/15/14   vv    Added support for quad SIM
03/25/14   tl    SIM Lock secondary revisions
02/24/14   tl    Initial Revision -introduce new SIM Lock feature

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_variation.h"
#if defined(FEATURE_UIMDIAG) && defined(FEATURE_SIMLOCK)
#error code not present
#endif /* FEATURE_UIMDIAG && FEATURE_SIMLOCK */
