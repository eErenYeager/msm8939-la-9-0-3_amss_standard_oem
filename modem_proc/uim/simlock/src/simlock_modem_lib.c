/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                        S I M L O C K  L I B R A R Y


GENERAL DESCRIPTION

  This file contains Library function that can be used to externally
  access and configure SIM Lock.

INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/25/14   tl      Secondary revisions
03/18/14   tl      Initial Version


=============================================================================*/

/*=============================================================================

                         INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_msg.h"
#include "intconv.h"
#include <stringl/stringl.h>
#include "simlock_platform.h"
#include "simlock_modem_lib.h"
#include "simlock_modem.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */

