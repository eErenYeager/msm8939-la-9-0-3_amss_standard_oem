/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                    S I M L O C K   M A I N   T A S K


GENERAL DESCRIPTION

  This file contains the main SIMLOCK task and supporting functions.

INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/simlock/src/simlock_modem.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
06/17/14   tl      Featurize tmc.h
04/24/14   tl      Fix callback NULL check
04/15/14   vv      Added the support for FEATURE_SEC_SFS
03/25/14   tl      Secondary revisions
03/18/14   tl      Initial Version

=============================================================================*/

/*=============================================================================

                         INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_msg.h"
#include "simlock_modem.h"
#include "simlocklib.h"
#include "simlock_platform.h"
#ifndef FEATURE_MODEM_RCINIT
#include "tmc.h"
#endif /* !FEATURE_MODEM_RCINIT */
#ifndef FEATURE_UIM_TEST_FRAMEWORK
#include "rex_types.h"
#endif /* !FEATURE_UIM_TEST_FRAMEWORK */

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */


/*===========================================================================
FUNCTION SIMLOCK_TASK_INIT

DESCRIPTION
  Function called to initialize the SIMLOCK task queue

DEPENDENCIES
  None

RETURN VALUE
  Void

SIDE EFFECTS
  None.  Function should only be called once during task initialization.
  On targets with RCInit enabled, this function is called in RCInit context.
  Therefore, it should not include references to TCBs, NV
  operations, etc.
===========================================================================*/
void simlock_task_init
(
  void
)
{
#ifdef FEATURE_SIMLOCK
  #error code not present
#endif /* FEATURE_SIMLOCK */
} /* simlock_task_init */


/*===========================================================================
FUNCTION SIMLOCK_MAIN

DESCRIPTION
  Entry point for the SIMLOCK task.  This function performs task initialization,
  then sits in an infinite loop, waiting on an input queue, and responding
  to messages received.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void simlock_main
(
  dword dummy
)
{
#ifdef FEATURE_SIMLOCK
  #error code not present
#endif /* FEATURE_SIMLOCK */
#ifdef FEATURE_MODEM_RCINIT
  RCEVT_SIGEX_SIGREX        stop_sig;
  RCINIT_GROUP              simlock_group;
#endif /* FEATURE_MODEM_RCINIT */

  /* Clear all signals */
  (void) rex_clr_sigs(UIM_SIMLOCK_TCB, (rex_sigs_type)~0);

#ifdef FEATURE_MODEM_RCINIT
  stop_sig.signal   = UIM_SIMLOCK_TCB;
  stop_sig.mask     = SIMLOCK_TASK_STOP_SIG;
  simlock_group     = rcinit_lookup_group_rextask(UIM_SIMLOCK_TCB);

  rcinit_register_term_group(simlock_group, RCEVT_SIGEX_TYPE_SIGREX, &stop_sig);

  rcinit_handshake_startup();
#else
  /* Initalize the SIMLOCK task structures */
  simlock_task_init();

  /* Wait for start signal from the Main Control Task */
  tmc_task_start();
#endif /* FEATURE_MODEM_RCINIT */
  (void)dummy;

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */

#ifdef FEATURE_MODEM_RCINIT
      rcinit_unregister_term_group(simlock_group, RCEVT_SIGEX_TYPE_SIGREX, &stop_sig);
      rcinit_handshake_term();
#endif /* FEATURE_MODEM_RCINIT */
} /* simlock_main */

