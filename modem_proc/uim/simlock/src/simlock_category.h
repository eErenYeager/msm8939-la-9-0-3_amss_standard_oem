#ifndef SIMLOCK_CATEGORY_H
#define SIMLOCK_CATEGORY_H
/*===========================================================================


            S I M L O C K   C A T E G O R Y   H E A D E R


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/simlock/src/simlock_category.h#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
04/14/14   vv      Added support for identifying conflicting codes
02/14/14   vv      Initial revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */

#endif /* SIMLOCK_CATEGORY_H */

