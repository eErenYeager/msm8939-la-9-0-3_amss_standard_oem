#ifndef SIMLOCK_MODEM_H
#define SIMLOCK_MODEM_H
/*===========================================================================


            S I M L O C K   D E F I N I T I O N S

                      A N D   F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/25/14   tl      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_variation.h"
#include "comdef.h"
#include "simlock_common.h"
#include "simlock_modem_lib.h"
#include "simlock_modem_p.h"
#include "rex.h"
#include "queue.h"
#ifdef FEATURE_MODEM_RCINIT
#include "rcinit.h"
/* Need this as we are referring to UIM task handle */
#define UIM_SIMLOCK_TCB                   rcinit_lookup_rextask("simlock")
#else
#include "uim_stubs_simlock.h"
#define UIM_SIMLOCK_TCB                   &simlock_tcb
#endif /* FEATURE_MODEM_RCINIT */

/*=============================================================================

                       DATA DECLARATIONS

=============================================================================*/
/*--------------------------------------------------------------------------
                             Signal Masks
--------------------------------------------------------------------------*/
#define SIMLOCK_CMD_Q_SIG                 0x00000001
#define SIMLOCK_TASK_STOP_SIG             0x00004000

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */
#endif /* SIMLOCK_MODEM_H */
