#ifndef SIMLOCK_PLATFORM_H
#define SIMLOCK_PLATFORM_H
/*===========================================================================


            S I M L O C K   P L A T F O R M   D E F I N I T I O N S

                      A N D

                F U N C T I O N S


GENERAL DESCRIPTION

  This source file contains the simlock platform definitions.


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/simlock/src/simlock_platform.h#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
04/14/14   vv      Removed un-needed macro
01/07/14   vv      Initial revision
===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#include "uim_msg.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */

#endif /* SIMLOCK_PLATFORM_H */
