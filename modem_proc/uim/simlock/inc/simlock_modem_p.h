#ifndef SIMLOCK_MODEM_P_H
#define SIMLOCK_MODEM_P_H
/*===========================================================================


            S I M L O C K   D E F I N I T I O N S

                      A N D   F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/25/14   tl      Secondary revisions
02/25/14   tl      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "uim_variation.h"
#include "comdef.h"
#include "simlock_common.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */
#endif /* SIMLOCK_MODEM_P_H */
