#ifndef UIM_MSG_H
#define UIM_MSG_H
/*===========================================================================


           U I M   L O G   H E A D E R S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/common/inc/uim_msg.h#1 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
12/30/14   lm      Fix compilation failure
01/29/14   sam     minor correction in macro
01/10/14   df      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/

#include "msg.h"

/*=============================================================================

                          UIM MSG MACROS
               MACRO Definitions used for logging throughout UIM

=============================================================================*/

#define UIM_MSG_HIGH_0(xx_fmt) \
  MSG(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt)

#define UIM_MSG_HIGH_1(xx_fmt, xx_arg1) \
  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1)

#define UIM_MSG_HIGH_2(xx_fmt, xx_arg1, xx_arg2) \
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1, xx_arg2)

#define UIM_MSG_HIGH_3(xx_fmt, xx_arg1, xx_arg2, xx_arg3) \
  MSG_3(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1, xx_arg2, xx_arg3)

#define UIM_MSG_HIGH_4(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4) \
  MSG_4(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4)

#define UIM_MSG_HIGH_5(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5) \
  MSG_5(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5)

#define UIM_MSG_HIGH_6(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6) \
  MSG_6(MSG_SSID_DFLT, MSG_LEGACY_HIGH, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6)

#define UIM_MSG_MED_0(xx_fmt) \
  MSG(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt)

#define UIM_MSG_MED_1(xx_fmt, xx_arg1) \
  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1)

#define UIM_MSG_MED_2(xx_fmt, xx_arg1, xx_arg2) \
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1, xx_arg2)

#define UIM_MSG_MED_3(xx_fmt, xx_arg1, xx_arg2, xx_arg3) \
  MSG_3(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1, xx_arg2, xx_arg3)

#define UIM_MSG_MED_4(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4) \
  MSG_4(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4)

#define UIM_MSG_MED_5(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5) \
  MSG_5(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5)

#define UIM_MSG_MED_6(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6) \
  MSG_6(MSG_SSID_DFLT, MSG_LEGACY_MED, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6)

#define UIM_MSG_LOW_0(xx_fmt) \
  MSG(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt)

#define UIM_MSG_LOW_1(xx_fmt, xx_arg1) \
  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1)

#define UIM_MSG_LOW_2(xx_fmt, xx_arg1, xx_arg2) \
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1, xx_arg2)

#define UIM_MSG_LOW_3(xx_fmt, xx_arg1, xx_arg2, xx_arg3) \
  MSG_3(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1, xx_arg2, xx_arg3)

#define UIM_MSG_LOW_4(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4) \
  MSG_4(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4)

#define UIM_MSG_LOW_5(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5) \
  MSG_5(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5)

#define UIM_MSG_LOW_6(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6) \
  MSG_6(MSG_SSID_DFLT, MSG_LEGACY_LOW, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6)

#define UIM_MSG_ERR_0(xx_fmt) \
  MSG(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt)

#define UIM_MSG_ERR_1(xx_fmt, xx_arg1) \
  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1)

#define UIM_MSG_ERR_2(xx_fmt, xx_arg1, xx_arg2) \
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2)

#define UIM_MSG_ERR_3(xx_fmt, xx_arg1, xx_arg2, xx_arg3) \
  MSG_3(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2, xx_arg3)

#define UIM_MSG_ERR_4(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4) \
  MSG_4(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4)

#define UIM_MSG_ERR_5(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5) \
  MSG_5(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5)

#define UIM_MSG_ERR_6(xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6) \
  MSG_6(MSG_SSID_DFLT, MSG_LEGACY_ERROR, xx_fmt, xx_arg1, xx_arg2, xx_arg3, xx_arg4, xx_arg5, xx_arg6)

#endif /* UIM_MSG_H */
