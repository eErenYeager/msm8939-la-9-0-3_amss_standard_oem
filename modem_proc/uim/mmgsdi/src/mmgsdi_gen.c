/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                    M M G S D I   I C C   F U N C T I O N S


GENERAL DESCRIPTION

  This source file contains the Generic commands that are not protocol dependent

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2004 - 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_gen.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   tl      Introduce new SIM Lock feature
02/06/14   tl      Check slot before updating channel state when streaming apdu
01/11/14   df      Fix off-target compilation error
01/10/14   df      Use v2.0 diag macros
01/09/14   ar      Replace old Macros with MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE
12/11/13   df      Remove unused memory paging feature
12/01/13   df      Convert to use prov session type to session ID util
11/18/13   vv      Added support for parallel REFRESH
10/16/13   df      Removing legacy gsdi conversion function calls
10/04/13   rp      F3 message reduction
09/26/13   av      Enable OnChip on bootup based on EFS item
08/30/13   tl      Convert channel info table from static to dynamic allocation
06/24/13   vdc     Added support for triple SIM
06/04/13   yt      Fix Klocwork errors
05/30/13   at      Fix to skip 6C XX for GET RESPONSE of P3=0
05/28/13   spo     Replaced instances of memcpy with memscpy
05/16/13   vdc     Replace memcpy with safer version memscpy
05/08/13   at      Supporting 62XX & 63XX warnings in APDU transactions
05/06/13   spo     Memory optimization for get response Cache
03/04/13   av      Updates for T3245 timer support to mark APP as legal/valid
02/04/13   vv      Added mmgsdi_session_read_prl support for RUIM card
12/05/12   vv      Added support for parallel processing in MMGSDI
10/11/12   spo     Removed unnecessary F3 messages
10/03/12   abg     Removed Queuing related F3 messages
10/01/12   abg     Removed Problem determining protocol type F3 message
09/28/12   abg     Updated ERR to MSG_ERROR
09/05/12   av      Handling of VPE response from the card
07/25/12   bcho    Set channel status to READY_WITH_STRMG while sending APDU to
                   UIM
05/22/12   shr     Add support for variable length Send APDU response
03/30/12   av      Replaced feature flags with NVITEM
03/29/12   shr     Added support for Send APDU extension API
03/01/12   kk      Added support for app state extraction in get_session_info
03/01/12   bcho    Add check for app type CSIM and app state READY in function
                   mmgsdi_gen_session_read_prl
02/18/12   av      Cleaned up remaining ALWAYS OFF features
01/14/12   bcho    Null check for app info ptr in function
                   mmgsdi_gen_session_illegal_sim
01/09/12   js      Reset sanity timer value on card error
12/21/11   shr     Legacy GSDI removal updates
10/13/11   shr     Adding support for full recovery
09/06/11   shr     Do not attempt a Card Reset as part of EFRESH RESET/Card
                   power-up handling, if the REFRESH has been stopped
08/22/11   yt      Send CARD_PDOWN to UIM without checking for MMGSDI state
08/18/11   av      Fixes for MMGSDI Klocwork CRITICAL warnings
05/19/11   at      Ignore card pdown on no card present or card error
05/17/11   shr     Updates for Thread safety
01/12/11   yt      Fixed compiler warnings
12/22/10   yt      Removed FEATURE_UIM_ZI_MEMORY_REDUCTION
10/14/10   yt      Updated handling of client req table entries in case of error
10/05/10   nmb     Removed extra bytes from task_cmd buffer
09/15/10   nmb     Allocate mmgsdi_client_id_reg_table dynamically
08/30/10   shr     Perso clean up for Sessions
08/09/10   yt      Fixed compiler warnings
07/14/10   js      Fixed compilation warnings
07/09/10   tkl     Fixed enum mapping between mmgsdi_perso_feature_enum_type
                   & gsdi_perso_enum_type
07/01/10   nb      Added Support for 2G send apdu command
06/25/10   shr     Perso commands need to be allowed when no Card is inserted
06/24/10   yt      Klocwork fixes
06/19/10   vs      Illegal SIM req post-processing
05/03/10   shr     Added Dual Slot support for REFRESH RESET and REFRESH
                   RESET_AUTO
03/12/10   adp     Fixing KW errors
03/09/10   shr     Clean up REFRESH FCN, INIT, INIT_FCN and APP_RESET
03/02/10   kk      Fixed get all available apps command handling
02/19/10   shr     Session get info API implementation
02/01/10   jk      EPRL Support
12/18/09   nb      Moved Dual Slot Changes
12/17/09   kk      Added app state check for Perso handling
09/26/09   kp      ZI memory reduction changes
07/28/09   mib     Added mmgsdi_perso_lock_down_and_enable_limited_access
05/15/09   nb      Added support for perso_get_unblock_dck_retries command
03/24/09   nb      Fixed problem with PERSO_UNBLOCK command
02/24/09   nb      Perso Command support in MMGSDI
11/18/08   js      Updated featurization for Send APDU related code
11/15/08   js      Removed MFLO featurization for Send APDU related code
10/20/08   sun     Allow protocol to be sent as part of Card Reset
09/16/08   kk      Updated arg to mmgsdi_util_queue_mmgsdi_uim_report_rsp in
                   create pkcs15 command handler
09/06/08   kk      Changed pkcs15 command handling
08/28/08   sun     If command fails in the original state, then return original
                   error mmgsdi status to client
11/15/07   sun     Added support for NAA refresh
09/27/07   tml     Lint fixes
09/10/07   tml     Added MFLO Supports.
05/07/07   sp      Replaced calls to mem_malloc/free with mmgsdi_malloc/free
01/17/07   sp      Removed pin_just_verified flag
07/26/06   sun     Added support for Multi USIM App and MMGSDI PIN
06/19/06   sun     Fixed Slot for Refresh
04/18/06   nk      featurized file and converted macro calls to function calls
03/21/06   tml     lint
03/14/06   sun     If the refresh fails , then remember to refresh
                   after the conditions are satisified
02/23/05   tml     Assign a discard cnf cb pointer to avoid overwriting
                   gsdilib cmd q during the refresh reset process.
12/14/05   tml     MMGSDI Cleanup
11/09/05   sun     Fixed Lint Errors
11/03/05   tml     Fixed header
11/03/05   sun     Changed Refresh to Refresh Command
08/25/05   pv      Initial version
===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "uim_variation.h"
#include "intconv.h"
#include "mmgsdi.h"
#include "uim.h"
#include "uim_msg.h"
#include "mmgsdiutil.h"
#include "mmgsdi_gen.h"
#include "mmgsdi_uim_uicc.h"
#include "mmgsdi_uim_icc.h"
#include "mmgsdi_evt.h"
#include "mmgsdi_cnf.h"
#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
#include "mmgsdi_perso_engine.h"
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
#include "mmgsdi_nv.h"
#include "fs_sys_types.h"
#include "fs_public.h"

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

/* Offset for GET RESPONSE APDU Len */
#define MMGSDI_APDU_GET_RESPONSE_LEN_OFFSET 4

/*===========================================================================

            DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/


/* =============================================================================
   FUNCTION:      MMGSDI_GEN_CARD_RESET_SYNC

   DESCRIPTION:
     This function shall send a reset command to UIM and wait for the response
     synchronously

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type

     MMGSDI_SUCCESS:          The command processing was successful.
     MMGSDI_ERROR:            The command processing was not successful.
     MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                              within appropriate ranges.

   SIDE EFFECTS:
     None
=============================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_card_reset_sync(
  mmgsdi_slot_id_enum_type slot,
  uim_protocol_type        protocol)
{
  uim_cmd_type *          uim_cmd_ptr   = NULL;
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;
  uim_slot_type           uim_slot      = UIM_SLOT_NONE;
  mmgsdi_slot_data_type  *slot_data_ptr = NULL;
  mmgsdi_task_enum_type   mmgsdi_task   = MMGSDI_TASK_MAX;

  /* Convert to UIM slot */
  mmgsdi_status = mmgsdi_util_convert_uim_slot(slot, &uim_slot);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(slot);
  if(slot_data_ptr == NULL)
  {
    return MMGSDI_ERROR;
  }

  /* Get the current task */
  MMGSDIUTIL_GET_CURRENT_MMGSDI_TASK(&mmgsdi_task);
  if(mmgsdi_task == MMGSDI_TASK_MAX)
  {
    UIM_MSG_HIGH_0("MMGSDI task is undetermined");
  }

  /* Get a UIM buffer for the request */
  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(uim_cmd_ptr,
                                         sizeof(uim_cmd_type),mmgsdi_status);

  if ((mmgsdi_status != MMGSDI_SUCCESS) || (uim_cmd_ptr == NULL))
  {
    return MMGSDI_ERROR;
  }

  /* Populate UIM information */
  uim_cmd_ptr->hdr.protocol = protocol;
  uim_cmd_ptr->hdr.channel  = UIM_CHANNEL0;
  uim_cmd_ptr->hdr.command  = UIM_RESET_F;
  uim_cmd_ptr->hdr.slot     = uim_slot;

  /* Reset Sanity Timer to default value */
  slot_data_ptr->mmgsdi_uim_sanity_timer_value
    = MMGSDI_UIM_SANITY_TIMER_VALUE;

  /* Reset GSTK loc envelope (VPE) response count */
  mmgsdi_util_reset_gstk_loc_env_rsp_count(slot);

  /* Send the command to UIM synchronously */
  mmgsdi_status = mmgsdi_send_cmd_to_uim_server_synch(uim_cmd_ptr);

  /* Clear any memory that has been allocated */
  if (mmgsdi_task != MMGSDI_TASK_MAX &&
      mmgsdi_internal_synch_uim_rpt_rsp[mmgsdi_task] != NULL)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(
      mmgsdi_internal_synch_uim_rpt_rsp[mmgsdi_task]->rsp_data.data_ptr);
    MMGSDIUTIL_TMC_MEM_FREE(mmgsdi_internal_synch_uim_rpt_rsp[mmgsdi_task]);
  }

  /* Free the UIM command pointer that has been allocated */
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(uim_cmd_ptr);

  return mmgsdi_status;
} /* mmgsdi_gen_card_reset_sync */


/* =============================================================================
   FUNCTION:      MMGSDI_GEN_PROCESS_CARD_PUP

   DESCRIPTION:
     This function shall do the procedures similar to SIM REFRESH.  There by
     sending a reset command to UIM.

   DEPENDENCIES:

   LIMITATIONS:

   RETURN VALUE:
     mmgsdi_return_enum_type

     MMGSDI_SUCCESS:          The command processing was successful.
     MMGSDI_ERROR:            The command processing was not successful.
     MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                              within appropriate ranges.
     MMGSDI_NOT_SUPPORTED:    When the file has an entry in the Service Table
                              but the service table indicates the card does not
                              have the support

   SIDE EFFECTS:

=============================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_card_pup(
  const mmgsdi_card_pup_req_type  *req_ptr)
{
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_SUCCESS;

  /*
   * Parameter checks.
   */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);

  if (req_ptr->option == MMGSDI_CARD_POWER_UP_INITIAL_PUP)
  {
    UIM_MSG_ERR_0("Should not handle CardPower Up Initial PUP Option in this function");
    return MMGSDI_ERROR;
  }
  else
  {
    uim_cmd_type*                      uim_cmd_ptr     = NULL;
    uim_slot_type                      uim_slot        = UIM_SLOT_NONE;
    int32                              index           = 0;
    mmgsdi_client_req_extra_info_type* extra_param_ptr = NULL;

    /*
     * Convert to UIM slot.
     */
    mmgsdi_status = mmgsdi_util_convert_uim_slot(
                      req_ptr->request_header.slot_id, &uim_slot);

    if (mmgsdi_status != MMGSDI_SUCCESS)
    {
      return mmgsdi_status;
    }
    /*
     * Allocate and populate extra parameter.
     */
    mmgsdi_status =  mmgsdi_util_alloc_and_populate_extra_param(
                       MMGSDI_CARD_PUP_REQ, (void*)req_ptr, &extra_param_ptr);

    if (mmgsdi_status != MMGSDI_SUCCESS)
    {
      return mmgsdi_status;
    }
    /*
     * 1) Get a UIM buffer for the request.
     * 2) Check for if the buffer is NULL or not.
     */
    MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(uim_cmd_ptr, sizeof(uim_cmd_type),mmgsdi_status);

    if ((mmgsdi_status != MMGSDI_SUCCESS) || (uim_cmd_ptr == NULL))
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
      return mmgsdi_status;
    }
    /*
     * Populate UIM information.
     */
    uim_cmd_ptr->hdr.command  = UIM_POWER_UP_UIM_PASSIVE_F;
    uim_cmd_ptr->hdr.protocol = UIM_NO_SUCH_MODE;
    uim_cmd_ptr->hdr.slot     = uim_slot;
    /*
     * Get a new index from client req info table for response data.
     * Set the uim data pointer to the client request table data buffer.
     */
    mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);
    if (mmgsdi_status != MMGSDI_SUCCESS)
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
      MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
      return mmgsdi_status;
    }
    mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                      index,
                      &req_ptr->request_header,
                      uim_cmd_ptr,
                      extra_param_ptr);

    if (mmgsdi_status != MMGSDI_SUCCESS)
    {
      mmgsdi_util_free_client_request_table_index(index);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
      MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
      return mmgsdi_status;
    }
    /*
     * Assign uim_cmd_ptr's user data.
     */
    uim_cmd_ptr->hdr.user_data = int32touint32(index);
    UIM_MSG_HIGH_0("Sending down card power up command to UIM");
    mmgsdi_status = mmgsdi_send_cmd_to_uim_server(uim_cmd_ptr);
    if (mmgsdi_status != MMGSDI_SUCCESS)
    {
      mmgsdi_util_free_client_request_table_index(index);
    }
  }
  return mmgsdi_status;
} /* mmgsdi_gen_process_card_pup */


/* =============================================================================
   FUNCTION:      MMGSDI_GEN_PROCESS_CARD_PDOWN

   DESCRIPTION:
     This function will send down a power down command to the UIM
     module to power down the CARD.

   DEPENDENCIES:

   LIMITATIONS:

   RETURN VALUE:
     mmgsdi_return_enum_type

     MMGSDI_SUCCESS:          The command processing was successful.
     MMGSDI_ERROR:            The command processing was not successful.
     MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                              within appropriate ranges.
     MMGSDI_NOT_SUPPORTED:    When the file has an entry in the Service Table
                              but the service table indicates the card does not
                              have the support

   SIDE EFFECTS:

=============================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_card_pdown(
  const mmgsdi_card_pdown_req_type *req_ptr)
{
  uim_cmd_type*                      uim_cmd_ptr     = NULL;
  uim_slot_type                      uim_slot        = UIM_SLOT_NONE;
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_SUCCESS;
  int32                              index           = 0;
  mmgsdi_client_req_extra_info_type* extra_param_ptr = NULL;

  /*
   * Parameter checks.
   */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);
  /*
   * Convert to UIM slot.
   */
  mmgsdi_status = mmgsdi_util_convert_uim_slot(
                    req_ptr->request_header.slot_id, &uim_slot);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }
  /*
   * Allocate and populate extra parameter.
   */
  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param(
                        MMGSDI_CARD_PDOWN_REQ,
                        (void*)req_ptr, &extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }
  /*
   * 1) Get a UIM buffer for the request.
   * 2) Check for if the buffer is NULL or not.
   */
  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(uim_cmd_ptr,
                                         sizeof(uim_cmd_type),mmgsdi_status);

  if ((mmgsdi_status != MMGSDI_SUCCESS) || (uim_cmd_ptr == NULL))
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }
  /*
   * Populate UIM information.
   */
  uim_cmd_ptr->hdr.command        = UIM_POWER_DOWN_F;
  uim_cmd_ptr->hdr.protocol       = UIM_NO_SUCH_MODE;
  uim_cmd_ptr->hdr.slot           = uim_slot;

   /* Populate the power down option to UIM*/
  if (req_ptr->option == MMGSDI_CARD_POWER_DOWN_NOTIFY_GSDI)
  {
    uim_cmd_ptr->pdown.pdown_option = UIM_OPTION_NOTIFY_PDOWN;
  }
  else
  {
    uim_cmd_ptr->pdown.pdown_option = UIM_OPTION_NO_NOTIFY_PDOWN;
  }

  /*
   * Get a new index from client req info table for response data.
   * Set the uim data pointer to the client request table data buffer.
   */
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
    return mmgsdi_status;
  }
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                    index,
                    &req_ptr->request_header,
                    uim_cmd_ptr,
                    extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
    return mmgsdi_status;
  }
  /*
   * Assign uim_cmd_ptr's user data.
   */
  uim_cmd_ptr->hdr.user_data = int32touint32(index);
  UIM_MSG_HIGH_0("Sending POWER down command to UIM");
  mmgsdi_status = mmgsdi_send_cmd_to_uim_server(uim_cmd_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
  }
  return mmgsdi_status;
} /* mmgsdi_gen_process_card_pdown */


/* =============================================================================
   FUNCTION:      MMGSDI_GEN_PROCESS_CARD_RESET

   DESCRIPTION:
     This function shall do the procedures similar to SIM REFRESH.  There by
     sending a reset command to UIM.

   DEPENDENCIES:

   LIMITATIONS:

   RETURN VALUE:
     mmgsdi_return_enum_type

     MMGSDI_SUCCESS:          The command processing was successful.
     MMGSDI_ERROR:            The command processing was not successful.
     MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                              within appropriate ranges.
     MMGSDI_NOT_SUPPORTED:    When the file has an entry in the Service Table
                              but the service table indicates the card does not
                              have the support

   SIDE EFFECTS:

=============================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_card_reset(
  const mmgsdi_card_reset_req_type  *req_ptr)
{
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_SUCCESS;
  uim_cmd_type*                      uim_cmd_ptr     = NULL;
  uim_slot_type                      uim_slot        = UIM_SLOT_NONE;
  int32                              index           = 0;
  mmgsdi_sw_status_type              status_word     = {0};
  mmgsdi_slot_data_type             *slot_data_ptr   = NULL;
  uint8                              slot_index      = MMGSDI_SLOT_1_INDEX;

  /*
   * Parameter checks.
   */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);

  mmgsdi_status = mmgsdi_util_get_slot_index(req_ptr->request_header.slot_id,
                                             &slot_index);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return MMGSDI_ERROR;
  }
  if((slot_index != MMGSDI_SLOT_1_INDEX) &&
     (slot_index != MMGSDI_SLOT_2_INDEX) &&
     (slot_index != MMGSDI_SLOT_3_INDEX))
  {
    return MMGSDI_ERROR;
  }

  /* If the Card Reset is triggered due to a REFRESH (Refresh Reset/Card
     power-up), then proceed with queuing a Reset request to UIM only if
     the REFRESH is still in progress. A Card Error for example could have
     stopped the REFRESH and in this case, do not queue the Reset request */
  if((req_ptr->reset_cause == MMGSDI_RESET_CAUSE_REFRESH) &&
     ((mmgsdi_refresh_info_ptr[slot_index] == NULL) ||
      (mmgsdi_refresh_info_ptr[slot_index]->request_header.slot_id !=
       req_ptr->request_header.slot_id)))
  {
    UIM_MSG_ERR_0("Cannot proceed with Card Reset, REFRESH no longer in progress");

    /* Reporting the Card Reset as Success - a failure status would
       lead to any new REFRESH ongoing to be stopped in
       mmgsdi_internal_refresh_cb() */
    mmgsdi_status = mmgsdi_cnf_build_and_queue(MMGSDI_SUCCESS,
                                               &req_ptr->request_header,
                                               NULL,
                                               NULL,
                                               FALSE,
                                               status_word);

    return mmgsdi_status;
  }

  /*
   * Convert to UIM slot.
   */
  mmgsdi_status = mmgsdi_util_convert_uim_slot(
                    req_ptr->request_header.slot_id, &uim_slot);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(req_ptr->request_header.slot_id);
  if(slot_data_ptr == NULL)
  {
    return MMGSDI_ERROR;
  }

  /*
   * 1) Get a UIM buffer for the request.
   * 2) Check for if the buffer is NULL or not.
   */
  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(uim_cmd_ptr, sizeof(uim_cmd_type),mmgsdi_status);
  if ((mmgsdi_status != MMGSDI_SUCCESS) || (uim_cmd_ptr == NULL))
  {
    return mmgsdi_status;
  }
  /*
   * Populate UIM information.
   */
  uim_cmd_ptr->hdr.protocol = req_ptr->protocol;
  uim_cmd_ptr->hdr.channel  = UIM_CHANNEL0;
  uim_cmd_ptr->hdr.command  = UIM_RESET_F;
  uim_cmd_ptr->hdr.slot     = uim_slot;
  /*
   * Get a new index from client req info table for response data.
   * Set the uim data pointer to the client request table data buffer.
   */
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
    return mmgsdi_status;
  }
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                    index,
                    &req_ptr->request_header,
                    uim_cmd_ptr,
                    NULL);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    MMGSDIUTIL_TMC_MEM_FREE(uim_cmd_ptr);
    return mmgsdi_status;
  }
  /*
   * Assign uim_cmd_ptr's user data.
   */
  uim_cmd_ptr->hdr.user_data = int32touint32(index);

  /* Reset Sanity Timer to default value */
  slot_data_ptr->mmgsdi_uim_sanity_timer_value
      = MMGSDI_UIM_SANITY_TIMER_VALUE;

  UIM_MSG_HIGH_0("Sending down reset command to UIM");
  mmgsdi_status = mmgsdi_send_cmd_to_uim_server(uim_cmd_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
  }
  return mmgsdi_status;
} /* mmgsdi_gen_process_card_reset */


/*===========================================================================
FUNCTION:      MMGSDI_GEN_BUILD_CARD_RESET

DESCRIPTION:
  This function is used to build a card reset command

DEPENDENCIES:

LIMITATIONS:
  None.

RETURN VALUE:
  mmgsdi_return_enum_type:

    MMGSDI_SUCCESS:          The command structure was properly generated
                             and queued onto the MMGSDI Command Queue.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
    MMGSDI_CMD_QUEUE_FULL:   The command was not Queued to the MMGSDI Task
                             because the max number of commands are already
                             queued.

SIDE EFFECTS:
  Will result in your callback getting called everytime MMGSDI Needs to notify
  any client of any events.
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_build_card_reset(
  mmgsdi_client_id_type              client_id,
  mmgsdi_slot_id_enum_type           slot_id,
  mmgsdi_cmd_enum_type               curr_req_type,
  uim_protocol_type                  protocol,
  mmgsdi_card_reset_cause_enum_type  reset_cause,
  mmgsdi_callback_type               response_cb_ptr,
  mmgsdi_client_data_type            client_ref)
{
  mmgsdi_task_cmd_type                 *task_cmd_ptr       = NULL;
  mmgsdi_return_enum_type               mmgsdi_status      = MMGSDI_SUCCESS;
  mmgsdi_card_reset_req_type           *card_reset_ptr     = NULL;
  int32                                 total_mem_len      = 0;
  uint32                                task_cmd_len       = 0;
  mmgsdi_slot_data_type                *slot_data_ptr      = 0;

  UIM_MSG_HIGH_1("MMGSDI_GEN_BUILD_CARD_RESET, client_id 0x%x", client_id);

  /* If Onchip is activated, do not queue card reset command and send an ERROR
     response. If we return SUCCESS here, the caller might perform the mmgsdi
     clean up or continue with refresh procedure which we would mess up the
     Onchip configuration */
  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(slot_id);

  if(slot_data_ptr != NULL &&
     slot_data_ptr->onchip_sim_data.state == MMGSDI_ONCHIP_SIM_INIT)
  {
    UIM_MSG_HIGH_0("Onchip is activated; build_card_reset ignored");
    return MMGSDI_ERROR;
  }

  total_mem_len = sizeof(mmgsdi_task_cmd_type);

  task_cmd_len = int32touint32(total_mem_len);

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(task_cmd_ptr, task_cmd_len);
  if (task_cmd_ptr == NULL)
  {
    return MMGSDI_MEMORY_ERROR_HEAP_EXHAUSTED;
  }

  /*---------------------------------------------------------------------------
    - Align pointers of the various structures members
  ---------------------------------------------------------------------------*/
  mmgsdi_util_align_mem( &task_cmd_ptr->cmd.cmd.card_reset_req,
                         curr_req_type);

  card_reset_ptr = &task_cmd_ptr->cmd.cmd.card_reset_req;

  /*---------------------------------------------------------------------------
    POPULATE THE REQUEST MESSAGE PAYLOAD
    1. client_id       - Client ID Obtained
    2. request_type    - Request Command Type
    3. request_len     - Length of message + header
    4. payload_len     - Length of the message payload
    5. slot_id         - Slot that the request is to be performed on
    6. client_data     - Pointer to Client Data
    7. response_cb     - Response Callback Pointer
  ---------------------------------------------------------------------------*/
  task_cmd_ptr->cmd.cmd_enum                       = curr_req_type;
  card_reset_ptr->request_header.client_id         = client_id;
  card_reset_ptr->request_header.request_type      = curr_req_type;
  card_reset_ptr->request_header.orig_request_type = curr_req_type;
  card_reset_ptr->request_header.slot_id           = slot_id;
  card_reset_ptr->request_header.client_data       = client_ref;
  card_reset_ptr->request_header.response_cb       = response_cb_ptr;
  card_reset_ptr->request_header.request_len       = total_mem_len;
  card_reset_ptr->request_header.payload_len       = card_reset_ptr->request_header.request_len -
                                                          uint32toint32(sizeof(mmgsdi_request_header_type));

  card_reset_ptr->protocol    = protocol;
  card_reset_ptr->reset_cause = reset_cause;

  /* PLACE ON GSDI QUEUE */
  mmgsdi_status = mmgsdi_cmd(task_cmd_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS) {
    /* Free the pointer since the task_cmd_ptr has not been put onto the command queue */
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(task_cmd_ptr);
    return mmgsdi_status;
  }
  /* Reset GSTK loc envelope (VPE) response count */
  mmgsdi_util_reset_gstk_loc_env_rsp_count(slot_id);
  return mmgsdi_status;
} /* mmgsdi_gen_build_card_reset */


/*=============================================================================
  FUNCTION: MMGSDI_GEN_PROCESS_GET_ATR

  DESCRIPTION:
    Function will get the ATR (Answer to Reset)from the UIM.

  DEPENDENCIES
    None

  LIMITATIONS
    NONE

  RETURN VALUE
    mmgsdi_return_enum_type

  SIDE EFFECTS
    NONE
=============================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_get_atr (
  mmgsdi_cmd_enum_type           cmd,
  const mmgsdi_get_atr_req_type* req_ptr
)
{
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_SUCCESS;
  int32                              index           = 0;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);

  if (cmd == MMGSDI_SAP_GET_ATR_REQ && !mmgsdi_sap_connected)
  {
    UIM_MSG_ERR_0("SAP GET ATR Req error: BT is not connected");
    return MMGSDI_ERROR;
  }

  /* No Extra Param */

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info( index, &req_ptr->request_header,
                                                                  NULL,
                                                                  NULL);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, cmd, MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
} /* mmgsdi_gen_process_get_atr */
/*lint !e715 */


/*===========================================================================
FUNCTION MMGSDI_GEN_PROCESS_CARD_READER_STATUS

DESCRIPTION
  This function is called to send a request to the UIM To retrieve the
  Card Reader Status.

DEPENDENCIES
  NONE

LIMITATIONS
  NONE

RETURN VALUE
  mmgsdi_return_enum_type
  MMGSDI_SUCCESS          - Command processed,
  MMGSDI_INCORRECT_PARAMS - Wrong input parameters provided to function
  MMGSDI_ERROR            - Other unknown failure

SIDE EFFECTS
  NONE
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_card_reader_status (
  mmgsdi_cmd_enum_type                       cmd,
  const mmgsdi_card_reader_status_req_type * req_ptr
)
{
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_SUCCESS;
  int32                              index           = 0;

  /* Parameter checks.*/
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);

  if (cmd == MMGSDI_SAP_CARD_READER_STATUS_REQ && !mmgsdi_sap_connected)
  {
    UIM_MSG_ERR_0("SAP card reader status Req error: BT is not connected");
    return MMGSDI_ERROR;
  }

  /* No Extra Param */
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_populate_client_request_table_info( index,
                                                                  &req_ptr->request_header,
                                                                  NULL,
                                                                  NULL);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, cmd,  MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  return MMGSDI_SUCCESS;
} /* mmgsdi_gen_process_card_reader_status */


/*===========================================================================
FUNCTION MMGSDI_GEN_RSP_TABLE_RETRIEVE

DESCRIPTION
  Function will retrieve the APDU Response Data cached from the previous
  applicable APDU Exchange with the Card inserted.

DEPENDENCIES
  The APDU_PTR provided must contain a valid pointer and all memory must
  be appropriately allocated to match the data maintained in this table.

  A requested length less than or equal to the amount of data
  to retrieve will be acceptable.

  A request length exceeding the amount of data to retrive will be unacceptable
  and will be handled by an error to the client.

RETURN VALUE
  mmgsdi_return_enum_type:
  MMGSDI_SUCCESS:           Get Response Data populated in the apdu_ptr
                            provided.
  MMGSDI_NOT_FOUND:         Get Response Data for the CLIENT ID, SLOT ID,
                            CHANNEL ID is no longer present
  MMGSDI_INCORRECT_PARAMS:  Parameter check failure. NULL DATA PTR provided
                            or Get Response Value exceeds the maximum.

SIDE EFFECTS
  None.
===========================================================================*/
static mmgsdi_return_enum_type  mmgsdi_gen_get_rsp_table_retrieve(
  mmgsdi_client_id_type      client_id,
  mmgsdi_slot_id_enum_type   slot_id,
  uint8                      channel_id,
  int32                      req_data_len,
  mmgsdi_data_type          *apdu_ptr,
  uint8                     *sw_ptr
)
{
  mmgsdi_return_enum_type status     = MMGSDI_SUCCESS;
  int32                   tbl_index  = 0;
  mmgsdi_len_type         apdu_len   = 0;

  UIM_MSG_HIGH_3("GET APDU Get Response: CLIENT 0x%x SLOT 0x%x CH ID 0x%x",
                 client_id, slot_id, channel_id);

  /* --------------------------------------------------------------------------
     Validate the input Parameters Provided
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(apdu_ptr, sw_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(apdu_ptr->data_ptr);
  MMGSDIUTIL_RETURN_IF_OUT_OF_RANGE(req_data_len,0x00,MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN);

  /* --------------------------------------------------------------------------
     Search the APDU Response Cache Data by using the following matching
     algorithm.  Match CLIENT_ID.  Then Match SLOT_ID.  Then Match CHANNEL_ID
     ----------------------------------------------------------------------- */
  for (tbl_index = 0; tbl_index < MMGSDI_MAX_AVAIL_GET_RSP_CACHE_ENTRIES; tbl_index++)
  {
    if(mmgsdi_apdu_get_rsp_table_ptr[tbl_index] == NULL)
    {
      continue;
    }

    if (mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->client_id != client_id )
    {
      /* Client IDs did not match for the client */
      continue;
    }

    if (mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->slot_id != slot_id )
    {
      /* Not the correct Slot ID for which APDU Data is requested
      ** Client may be sending APDUs over various SLOT IDs.
      ** Continue Searching */
      continue;
    }

    if (mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->channel_id != channel_id )
    {
      /* Not the Channel ID for which APDU Data requested
      ** Client may be sending APDUs over various SLOT IDs using
      ** different channels */
      continue;
    }
    /* Break out of the for loop.  Have a match */
    break;
  }

  if (tbl_index >= MMGSDI_MAX_AVAIL_GET_RSP_CACHE_ENTRIES)
  {
    /* No client ID match */
    MMGSDIUTIL_DEBUG_MSG_ERROR_1("Invalid tbl_index 0x%x ", tbl_index);
    return MMGSDI_ERROR;
  }

  /* At this point, the Client ID Matches, the Slot ID Matches
  ** and the channel id matches.  This is the correct APDU Data
  ** Copy the data over to the requester's buffer after a few
  ** length checks to ensure we can protect against buffer overrun*/
  apdu_len = mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len;


  UIM_MSG_HIGH_2("Req. length = 0, Get Resp. cache total length 0x%x",
                 req_data_len, apdu_len);

  /* If valid Get Response data is available in the cache */
  if (apdu_len > MMGSDI_GEN_SW_LEN)
  {
    /* In case of warning status words, we expect the client to send a
       GET RESPONSE with Le = 0, per TS 102.221, Section C.1.7
       If data available is < 256, send intermediate 6C XX as SW bytes
       If data available is >= 256, skip sending intermediate 6C XX SW bytes,
        instead send data directly */
    if ((mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->implicit_sw1 == GSDI_SW1_WARNINGS_PART_1 ||
         mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->implicit_sw1 == GSDI_SW1_WARNINGS_PART_2) &&
        (req_data_len == 0x00) &&
        (apdu_len <= MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + MMGSDI_GEN_SW_LEN))
    {
      /* Now update the SW1 & SW2 */
      apdu_ptr->data_len = 0;
      sw_ptr[0] = GSDI_SIM_SW1_PROCEDURE_BYTE_6C;
      sw_ptr[2] = GSDI_SIM_SW1_PROCEDURE_BYTE_6C;

      /* Update implicit SW2 with the pending Get Response data length */
      sw_ptr[1] =  int32touint8(
                       mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len -
                       MMGSDI_GEN_SW_LEN);
      sw_ptr[3] = sw_ptr[1];
      return MMGSDI_SUCCESS;
    }

    /* If 256 bytes or above of Get Response data is available */
    if(apdu_len > MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + MMGSDI_GEN_SW_LEN)
    {
      if ( req_data_len == 0)
      {
        /* Return 256 bytes of Get Resp. data when requested data length is 0 */
        req_data_len = MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + 1;
      }
    }
    else
    {
      if ( req_data_len > (apdu_len - MMGSDI_GEN_SW_LEN))
      {
        /* Length Requested exceeds length cached */
        UIM_MSG_ERR_2("Req Length Exceed Max, Req 0x%x Avail 0x%x",
                      req_data_len, (apdu_len - MMGSDI_GEN_SW_LEN));

        status = MMGSDI_ERROR;
      }
      else if ( req_data_len == 0)
      {
        /* Return entire cached Get Resp. data when requested data length is 0 */
        req_data_len = apdu_len - MMGSDI_GEN_SW_LEN;
      }
    }
    if (status == MMGSDI_SUCCESS )
    {
      /* Store the contents into the buffer provided by the
      ** Requester
      */
      mmgsdi_memscpy(apdu_ptr->data_ptr,
        MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + 1,
        mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_ptr,
        (size_t)req_data_len);

      apdu_ptr->data_len = req_data_len;

      /* Update the APDU cache data length */
      mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len =
        apdu_len - req_data_len;

      /* If Get Response data is still available in cache */
      if(mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len >
         MMGSDI_GEN_SW_LEN)
      {
         /* Move the remaining data to the start of the data buffer */
        mmgsdi_memsmove(mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_ptr,
          apdu_len,
          &mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_ptr[req_data_len],
          (apdu_len - req_data_len));

        /* Return Implicit SW1 (0x61/0x9F) back to client to indicate more
           data is pending to be Fetched */
        sw_ptr[0] = mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->implicit_sw1;
        sw_ptr[2] = mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->implicit_sw1;

        /* If pending Get Response data length exceeds 0xFF, then return
           implicit SW2 as 0x00 to indicate the same */
        if(mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len >
           MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + MMGSDI_GEN_SW_LEN)
        {
          sw_ptr[1] = 0x00;
          sw_ptr[3] = 0x00;
        }
        /* If pending Get Response data length is less than or equal to 0xFF,
           return implicit SW2 with the the pending length */
        else
        {
          sw_ptr[1] =  int32touint8(
                         mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len -
                         MMGSDI_GEN_SW_LEN);
          sw_ptr[3] = sw_ptr[1];
        }
      }
      /* All Get Response data has been fetched, return the final SW1/SW2 */
      else
      {
        mmgsdi_memscpy(sw_ptr,
          MMGSDI_GEN_SW_LEN,
          &mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_ptr[apdu_len - MMGSDI_GEN_SW_LEN],
          MMGSDI_GEN_SW_LEN);
      }
    }
  }
  else
  {
    /* Invalid APDU Len */
    MMGSDIUTIL_DEBUG_MSG_ERROR_1("Invalid apdu len stored 0x%x ", apdu_len);

    status = MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     If all Get Response data has been fetched, or there is an error in
     handling the Get Response request, clear out cached item.
     Free out the memory location and wipe out the data associated with the
     index for the client's data
     ----------------------------------------------------------------------- */
  if((status == MMGSDI_ERROR) ||
     (mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_len <= MMGSDI_GEN_SW_LEN))
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(mmgsdi_apdu_get_rsp_table_ptr[tbl_index]->apdu.data_ptr);
    MMGSDIUTIL_TMC_MEM_FREE(mmgsdi_apdu_get_rsp_table_ptr[tbl_index]);
  }

  return status;
} /* mmgsdi_gen_get_rsp_table_retrieve */


/*===========================================================================
FUNCTION MMGSDI_GEN_RSP_TABLE_FLUSH_ALL

DESCRIPTION
  Function will clear all all entries in the GET APDU Get Response
  Cache Table.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Will Free Up any memory allocated by any of the Add Functions

===========================================================================*/
void  mmgsdi_gen_get_rsp_table_flush_all(
  mmgsdi_slot_id_enum_type  slot_id
)
{
  int32 index = 0;

  UIM_MSG_HIGH_1("Flushing out entire Get RSP APDU Cache for slot: 0x%x", slot_id);

  for (index=0; index < MMGSDI_MAX_AVAIL_GET_RSP_CACHE_ENTRIES; index++)
  {
    if(mmgsdi_apdu_get_rsp_table_ptr[index] == NULL )
    {
      continue;
    }
    if(mmgsdi_apdu_get_rsp_table_ptr[index]->slot_id == slot_id)
    {
      if (mmgsdi_apdu_get_rsp_table_ptr[index]->apdu.data_ptr != NULL)
      {
        /* Free up any allocated memory for any outstanding
        ** APDUs that resulted in a Get Response
        */
        MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(
        mmgsdi_apdu_get_rsp_table_ptr[index]->apdu.data_ptr);
      }
      /* Free up allocated memory */
      MMGSDIUTIL_TMC_MEM_FREE(mmgsdi_apdu_get_rsp_table_ptr[index]);
    }
  }
} /* mmgsdi_gen_get_rsp_table_flush_all */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_DO_GET_RESPONSE

  DESCRIPTION:
    This function will search the Get Response cache table for the
    response of the APDU just sent.

  DEPENDENCIES:
    None

  LIMITATIONS:
    TNone

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.

  SIDE EFFECTS:
     None

===========================================================================*/
static mmgsdi_return_enum_type mmgsdi_gen_do_get_response(
  const mmgsdi_send_apdu_req_type * req_ptr
)
{
  uim_rpt_type                     * report_ptr      = NULL;
  mmgsdi_return_enum_type            mmgsdi_status   = MMGSDI_ERROR;
  mmgsdi_return_enum_type            get_rsp_status  = MMGSDI_ERROR;
  mmgsdi_data_type                   get_rsp;
  int32                              index           = 0;
  mmgsdi_client_req_extra_info_type* extra_param_ptr = NULL;
  int16                              get_resp_len    = 0;
  uint8                              channel_id      = 0;
  uint8                              sw[MMGSDI_GEN_SW_LEN];
  uint8                              get_rsp_data[MMGSDI_GEN_MAX_GET_RESPONSE_REQ_LEN + 1];

  memset(sw, 0x00, MMGSDI_GEN_SW_LEN);
  memset(get_rsp_data, 0x00, sizeof(get_rsp_data));
  memset(&get_rsp, 0x00, sizeof(mmgsdi_data_type));
  /* --------------------------------------------------------------------------
     Input validation
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->data.data_ptr);

  UIM_MSG_HIGH_1("APDU length:0x%x for GET RESPONSE", req_ptr->data.data_len);

  /* Minimum mandated APDU length is 4, treat anything less as an error */
  if(req_ptr->data.data_len < MMGSDI_APDU_GET_RESPONSE_LEN_OFFSET)
  {
    return MMGSDI_INCORRECT_PARAMS;
  }
  /* APDU length of 4 is valid, assume Le=0 in this case */
  else if(req_ptr->data.data_len == MMGSDI_APDU_GET_RESPONSE_LEN_OFFSET)
  {
    get_resp_len = 0;
  }
  /* When APDU length is greater than 4, retrieve Le from APDU */
  else
  {
    get_resp_len = req_ptr->data.data_ptr[MMGSDI_APDU_GET_RESPONSE_LEN_OFFSET];
  }

  /* --------------------------------------------------------------------------
     Retrieve the Channel ID from the CLA byte
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_channel_id_from_cla(req_ptr->data.data_ptr[0],
                                                      req_ptr->request_header.slot_id,
                                                      &channel_id);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_0("Could not retrieve Channel ID from CLA");
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Retrieve the Data from the Get Response Table
     ------------------------------------------------------------------------*/
  get_rsp.data_ptr = get_rsp_data;

  get_rsp_status = mmgsdi_gen_get_rsp_table_retrieve(
    req_ptr->request_header.client_id,
    req_ptr->request_header.slot_id,
    channel_id,
    get_resp_len,
    &get_rsp,
    sw);

  UIM_MSG_HIGH_1("Get response retrieval status: 0x%x", get_rsp_status);

  /*
   * Allocate and populate extra parameter.
   */
  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param(
                    req_ptr->request_header.request_type, (void*)req_ptr, &extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Register the transaction with the Client Command Table maintained
     to align the response with the request
     ---------------------------------------------------------------------- */
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
        index,
        &req_ptr->request_header,
        NULL,
        extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Allocate Data for the UIM Response so that the Send APDU
     Response can be provided
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(report_ptr,
                                         sizeof(uim_rpt_type),
                                         mmgsdi_status);
  if ((mmgsdi_status != MMGSDI_SUCCESS) || (report_ptr == NULL))
  {
    /* Not much we can do except use the static conf to indicate
    ** there has been a memory heap exhuastion error
    */
    mmgsdi_util_free_client_request_table_index(index);
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Proceed with populating the UIM Report Pointer to be provided to
     MMGSDI.  This must be a successful command.
     ------------------------------------------------------------------------*/
  report_ptr->cmd_transacted = TRUE;
  report_ptr->rpt_status     = UIM_PASS;
  report_ptr->rpt_type       = UIM_STREAM_ISO7816_APDU_R;
  report_ptr->user_data      = int32touint32(index);

  if(get_rsp_status == MMGSDI_SUCCESS)
  {
    report_ptr->sw1            = (uim_sw1_type)sw[0];
    report_ptr->sw2            = (uim_sw2_type)sw[1];

    report_ptr->rpt.stream_iso7816_apdu.get_resp_sw1 = (uim_sw1_type)sw[2];
    report_ptr->rpt.stream_iso7816_apdu.get_resp_sw2 = (uim_sw2_type)sw[3];

    report_ptr->rpt.stream_iso7816_apdu.num_data_bytes = int32touint16(get_rsp.data_len);

    if(get_rsp.data_len > 0)
    {
      MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(
        report_ptr->rpt.stream_iso7816_apdu.data_ptr,
        report_ptr->rpt.stream_iso7816_apdu.num_data_bytes,
        mmgsdi_status);

      if (mmgsdi_status != MMGSDI_SUCCESS || report_ptr == NULL)
      {
        mmgsdi_util_free_client_request_table_index(index);
        MMGSDIUTIL_TMC_MEM_FREE(report_ptr);
        return MMGSDI_ERROR;
      }

      mmgsdi_memscpy(report_ptr->rpt.stream_iso7816_apdu.data_ptr,
                     report_ptr->rpt.stream_iso7816_apdu.num_data_bytes,
                     get_rsp.data_ptr,
                     int32touint32(get_rsp.data_len));
    }
  }
  /* If Get Response data is not cached, return SW1/SW2 as 0x6F/0x00 -
     Technical Problem */
  else
  {
    report_ptr->sw1            = SW1_PROBLEM;
    report_ptr->sw2            = SW2_NORMAL_END;

    report_ptr->rpt.stream_iso7816_apdu.num_data_bytes = 0;
  }

  /* --------------------------------------------------------------------------
     Call the mmgsdi_uim_report function which is the callback for the UIM
     Task to call.  This is the way to get the MMGSDI Task to give itself
     a Response that would normally come back from the UIM Task.
     ----------------------------------------------------------------------- */
  mmgsdi_uim_report (report_ptr);

  /* --------------------------------------------------------------------------
     Clean up / free up any memory allocated in this function
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(report_ptr->rpt.stream_iso7816_apdu.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE(report_ptr);

  return mmgsdi_status;

}/* mmgsdi_gen_do_get_response */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_PROCESS_SEND_APDU

  DESCRIPTION:
    This function will send an APDU to the UICC.

  DEPENDENCIES:
    None

  LIMITATIONS:
    This function is limited to the use of UICC ie. (technologies of type UICC only).

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.

  SIDE EFFECTS:
     None

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_process_send_apdu (
  mmgsdi_send_apdu_req_type * req_ptr
)
{
  mmgsdi_protocol_enum_type  protocol           = MMGSDI_NO_PROTOCOL;
  mmgsdi_return_enum_type    mmgsdi_status      = MMGSDI_SUCCESS;
  uint8                      i                  = 0;
  uint8                      channel_id         = 0;

  /* --------------------------------------------------------------------------
     Input validation
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  if ( req_ptr->data.data_ptr[1] == GET_RESPONSE /* For UICC && ICC */ )
  {
    /* Client is trying to issue a Get Response.
    ** Generate a Fake UIM Response Pointer.  This command
    ** is no longer going to the UIM due to the Get Response
    ** Table which not caches the Get response so that the clients
    ** don't need to.  upon completion of this function call,
    ** the Get Response data will be sent and Queued for MMGSDI
    ** to process as if the UIM sent a response.
    */
    return mmgsdi_gen_do_get_response(req_ptr);
  }

   /* ---------------------------------------------------------------------------
     Get protocol
     NOTE THAT THIS DOES NOT CURRENTLY SUPPORT DUAL SLOTS
     ------------------------------------------------------------------------ */
  if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_ALLOW_SEND_APDU_ONLY) == MMGSDI_FEATURE_ENABLED
     && (req_ptr->request_header.orig_request_type == MMGSDI_SEND_APDU_REQ
         || req_ptr->request_header.orig_request_type == MMGSDI_SEND_APDU_EXT_REQ))
  {
    protocol = MMGSDI_UICC;
  }
  else
  {
    mmgsdi_status = mmgsdi_util_determine_protocol(&protocol,req_ptr->request_header.slot_id);
  }
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  } /* mmgsdi_status is not SUCCESS */

  /*set channel state to READY_WITH_STREAMING*/
  mmgsdi_status = mmgsdi_util_get_channel_id_from_cla(
                    req_ptr->data.data_ptr[0],
                    req_ptr->request_header.slot_id,
                    &channel_id);

  MMGSDIUTIL_ENTER_CLIENT_APP_DATA_CRIT_SECT;

  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
    for (i = 0; i < MMGSDI_MAX_CHANNEL_INFO; i++)
    {
      if(mmgsdi_channel_info_ptr_table[i] != NULL &&
         mmgsdi_channel_info_ptr_table[i]->channel_id == channel_id &&
         mmgsdi_channel_info_ptr_table[i]->slot_id == req_ptr->request_header.slot_id &&
         mmgsdi_channel_info_ptr_table[i]->channel_state == MMGSDI_CHANNEL_STATE_READY)
      {
        mmgsdi_channel_info_ptr_table[i]->channel_state = MMGSDI_CHANNEL_STATE_APDU_STREAMING_DONE;
        break;
      }
    }
  }

  MMGSDIUTIL_LEAVE_CLIENT_APP_DATA_CRIT_SECT;

  if(protocol == MMGSDI_UICC)
  {
    mmgsdi_status = mmgsdi_uim_uicc_send_apdu(req_ptr);
  }
  else if (protocol == MMGSDI_ICC)
  {
    mmgsdi_status = mmgsdi_uim_icc_send_apdu(req_ptr);
  }
  else
  {
    /* Currently not supported */
    mmgsdi_status = MMGSDI_NOT_SUPPORTED;
  }
  return mmgsdi_status;
}/* mmgsdi_gen_send_apdu */


#ifdef FEATURE_MMGSDI_PKCS15
/*===========================================================================
  FUNCTION:      MMGSDI_GEN_CREATE_PKCS15_TABLE_CMD

  DESCRIPTION:
    This function will create PKCS15 lookup table

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.

  SIDE EFFECTS:
     None

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_create_pkcs15_table_cmd (
  const mmgsdi_create_pkcs15_table_type * req_ptr
)
{
  mmgsdi_return_enum_type              mmgsdi_status                 = MMGSDI_SUCCESS;
  mmgsdi_create_pkcs15_extra_info_type pkcs_lookup_table_extra_param;
  mmgsdi_client_req_extra_info_type    *extra_param_ptr              = NULL;
  int32                                client_cmd_index              = 0;

  /* build and Queue the response */
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&client_cmd_index);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Unable to find valid index, 0x%x", mmgsdi_status);
    return mmgsdi_status;
  }

  /*-------------------------------------------------------------------------
  allocate and build extra param ptr
  -------------------------------------------------------------------------*/
  /*KW doesnt like "={0}" style initialzation of pkcs_lookup_table_extra_parm*/
  memset(&pkcs_lookup_table_extra_param,
        0x00,
        sizeof(mmgsdi_create_pkcs15_extra_info_type));
  pkcs_lookup_table_extra_param.index = client_cmd_index;

  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param(
                    MMGSDI_CREATE_PKCS15_TABLE_REQ,
                    (void*)&pkcs_lookup_table_extra_param,
                    &extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Unable to populate extra param 0x%x", mmgsdi_status);
    mmgsdi_util_free_client_request_table_index(client_cmd_index);
    return mmgsdi_status;
  }/* end of if (mmgsdi_status != MMGSDI_SUCCESS) */

  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                    client_cmd_index,
                    &req_ptr->request_header,
                    NULL,
                    extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Unable to populate client request 0x%x", mmgsdi_status);
    /* Release the client table */
    mmgsdi_util_free_client_request_table_index(client_cmd_index);
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }/* end of if (mmgsdi_status != MMGSDI_SUCCESS) */

  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                  client_cmd_index, MMGSDI_CREATE_PKCS15_TABLE_REQ, MMGSDI_SUCCESS);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(client_cmd_index);
    return mmgsdi_status;
  }/* end of if (mmgsdi_status != MMGSDI_SUCCESS) */

  return mmgsdi_status;
} /* mmgsdi_gen_create_pkcs15_table_cmd */
#endif /* FEATURE_MMGSDI_PKCS15 */


#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/*===========================================================================
  FUNCTION:      MMGSDI_GEN_PERSO_REQ

  DESCRIPTION:
    This function will generate a perso request to be sent to GSDI.

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_perso_req
(
  const mmgsdi_perso_req_type       *req_ptr
)
{
  mmgsdi_return_enum_type            mmgsdi_status       = MMGSDI_ERROR;
  mmgsdi_return_enum_type            perso_mmgsdi_status = MMGSDI_MAX_RETURN_ENUM;
  int32                              index               = 0;
  mmgsdi_client_req_extra_info_type* extra_param_ptr     = NULL;

  /*---------------------------------------------------------------------------
    FUNCTION PARAMETER CHECKS
    1. req_p                                  - Cannot be NULL
    2. req_p->request_header.response_cb      - Cannot be NULL
    3. Verify params are set properly
  ---------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr->request_header.response_cb);

  UIM_MSG_MED_1("MMGSDI_GEN_PERSO_REQ: Perso Action = 0x%x",
                req_ptr->perso_action);

  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param( MMGSDI_PERSO_REQ,
                                                              (void*)req_ptr,
                                                              &extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Failed to allocate memeory for extra param 0x%x",
                  mmgsdi_status);
    return mmgsdi_status;
  }

  switch(req_ptr->perso_action)
  {
    case MMGSDI_PERSO_ACTIVATE:
    {
      gsdi_perso_act_feature_ind_req_T     act_req = {{0}};

      (void)mmgsdi_util_convert_to_gsdi_perso_feature(
                   req_ptr->feature_ind, &act_req.feature);
      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      act_req.num_bytes = req_ptr->perso_data.feature_activate_req.num_bytes;

      if(act_req.num_bytes <= GSDI_PERSO_MAX_CK)
      {
        mmgsdi_memscpy(act_req.ck_buffer,
                       sizeof(act_req.ck_buffer),
                       req_ptr->perso_data.feature_activate_req.ck_buffer,
                       int32touint32(act_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_2("Key Size 0x%x greater than GSDI_PERSO_MAX_CK, Truncating. Perso Action 0x%x",
                       act_req.num_bytes, req_ptr->perso_action);

        mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
      }

      if(mmgsdi_status == MMGSDI_SUCCESS)
      {
        perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                     &act_req,
                                                     GSDI_PERSO_ACT_FEATURE_IND_REQ,
                                                     extra_param_ptr->perso_data.ret_data.data_ptr,
                                                     MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                     &extra_param_ptr->perso_data.ret_data.data_len);
      }
    } /*MMGSDI_PERSO_ACTIVATE */
    break;

    case MMGSDI_PERSO_DEACTIVATE:
    {
      gsdi_perso_deact_feature_ind_req_T     deact_req = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &deact_req.feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      deact_req.num_bytes = req_ptr->perso_data.feature_deactivate_req.num_bytes;

      if(deact_req.num_bytes <= GSDI_PERSO_MAX_CK)
      {
        mmgsdi_memscpy(deact_req.ck_buffer,
                       sizeof(deact_req.ck_buffer),
                       req_ptr->perso_data.feature_deactivate_req.ck_buffer,
                       int32touint32(deact_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_2("Key Size 0x%x greater than GSDI_PERSO_MAX_CK, Truncating. Perso Action 0x%x",
                       deact_req.num_bytes, req_ptr->perso_action);

        mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
      }

      if(mmgsdi_status == MMGSDI_SUCCESS)
      {
        perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                     &deact_req,
                                                     GSDI_PERSO_DEACT_FEATURE_IND_REQ,
                                                     extra_param_ptr->perso_data.ret_data.data_ptr,
                                                     MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                     &extra_param_ptr->perso_data.ret_data.data_len);
      }
    } /*MMGSDI_PERSO_DEACTIVATE */
    break;

    case MMGSDI_PERSO_GET_FEATURE_IND:
    {
      gsdi_perso_get_feature_ind_req_T       get_feature_ind_req = {{0}};

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &get_feature_ind_req,
                                                   GSDI_PERSO_GET_FEATURE_IND_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /*MMGSDI_PERSO_GET_FEATURE_IND */
    break;

    case MMGSDI_PERSO_GET_PERM_DISABLE_IND:
    {
      gsdi_perso_get_perm_feature_ind_req_T  feature_disable_req = {{0}};

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &feature_disable_req,
                                                   GSDI_PERSO_GET_PERM_FEATURE_IND_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /*MMGSDI_PERSO_GET_PERM_DISABLE_IND */
    break;

    case MMGSDI_PERSO_OTA_DEPERSO:
    {
      gsdi_perso_ota_deperso_req_T           ota_deperso_req = {{0}};

      ota_deperso_req.num_bytes = req_ptr->perso_data.ota_deperso_req.num_bytes;

      if(ota_deperso_req.num_bytes <= GSDI_PERSO_MAX_OTA_DEPERSO_BYTES)
      {
        mmgsdi_memscpy(ota_deperso_req.data_buffer,
                       sizeof(ota_deperso_req.data_buffer),
                       req_ptr->perso_data.ota_deperso_req.data_buffer,
                       int32touint32(ota_deperso_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_1("Deperso Data Size 0x%x greater than GSDI_PERSO_MAX_OTA_DEPERSO_BYTES",
                       ota_deperso_req.num_bytes);

        mmgsdi_memscpy(ota_deperso_req.data_buffer,
                       sizeof(ota_deperso_req.data_buffer),
                       req_ptr->perso_data.ota_deperso_req.data_buffer,
                       (uint32)GSDI_PERSO_MAX_OTA_DEPERSO_BYTES);
      }

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &ota_deperso_req,
                                                   GSDI_PERSO_OTA_DERPERSO_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /*MMGSDI_PERSO_OTA_DEPERSO */
    break;

    case MMGSDI_PERSO_SET_FEATURE_DATA:
    {
      gsdi_perso_set_feature_data_req_T        set_data_req = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &set_data_req.feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      set_data_req.num_bytes = req_ptr->perso_data.set_data_req.num_bytes;

      if(set_data_req.num_bytes <= GSDI_PERSO_MAX_DATA_LEN)
      {
        mmgsdi_memscpy(set_data_req.data_buffer,
                       sizeof(set_data_req.data_buffer),
                       req_ptr->perso_data.set_data_req.data_buffer,
                       int32touint32(set_data_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_2("Data Size 0x%x greater than GSDI_PERSO_MAX_DATA_LEN: Perso Action 0x%x",
                       set_data_req.num_bytes, req_ptr->perso_action);

        mmgsdi_memscpy(set_data_req.data_buffer,
                       sizeof(set_data_req.data_buffer),
                       req_ptr->perso_data.set_data_req.data_buffer,
                       (uint32)GSDI_PERSO_MAX_DATA_LEN);
      }

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &set_data_req,
                                                   GSDI_PERSO_SET_FEATURE_DATA_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /*MMGSDI_PERSO_SET_FEATURE_DATA */
    break;

    case MMGSDI_PERSO_GET_FEATURE_DATA:
    {
      gsdi_perso_get_feature_data_req_T        get_data_req = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &get_data_req.feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &get_data_req,
                                                   GSDI_PERSO_GET_FEATURE_DATA_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    }
    break;

    case MMGSDI_PERSO_DISABLE:
    {
      gsdi_perso_perm_disabled_feature_req_T    disable_req = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &disable_req.perso_feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      disable_req.num_bytes     = req_ptr->perso_data.feature_disable_req.num_bytes;

      if(disable_req.num_bytes <= GSDI_PERSO_MAX_CK)
      {
        mmgsdi_memscpy(disable_req.ck_buffer,
                       sizeof(disable_req.ck_buffer),
                       req_ptr->perso_data.feature_disable_req.ck_buffer,
                       int32touint32(disable_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_2("Key Size 0x%x greater than GSDI_PERSO_MAX_CK, Truncating. Perso Action 0x%x",
                       disable_req.num_bytes, req_ptr->perso_action);

        mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
      }

      if(mmgsdi_status == MMGSDI_SUCCESS)
      {
        perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                     &disable_req,
                                                     GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ,
                                                     extra_param_ptr->perso_data.ret_data.data_ptr,
                                                     MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                     &extra_param_ptr->perso_data.ret_data.data_len);
      }
    } /*MMGSDI_PERSO_DISABLE */
    break;

    case MMGSDI_PERSO_GET_DCK_RETRIES:
    {
      gsdi_perso_get_dcks_num_retries_req_T num_retries_req = {{0}};

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &num_retries_req,
                                                   GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    }
    break;

    case MMGSDI_PERSO_UNBLOCK:
    {
      gsdi_perso_unblock_feature_ind_req_T   unblock_req = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &unblock_req.feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      unblock_req.num_bytes = req_ptr->perso_data.feature_unblock_req.num_bytes;

      if(unblock_req.num_bytes <= GSDI_PERSO_MAX_CK)
      {
        mmgsdi_memscpy(unblock_req.unblock_ck_buffer,
                       sizeof(unblock_req.unblock_ck_buffer),
                       req_ptr->perso_data.feature_unblock_req.ck_buffer,
                       int32touint32(unblock_req.num_bytes));
      }
      else
      {
        UIM_MSG_HIGH_2("Key Size 0x%x greater than GSDI_PERSO_MAX_CK, Truncating. Perso Action 0x%x",
                       unblock_req.num_bytes, req_ptr->perso_action);

        mmgsdi_status = MMGSDI_INCORRECT_PARAMS;
      }

      if(mmgsdi_status == MMGSDI_SUCCESS)
      {
        perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                     &unblock_req,
                                                     GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ,
                                                     extra_param_ptr->perso_data.ret_data.data_ptr,
                                                     MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                     &extra_param_ptr->perso_data.ret_data.data_len);
      }
    } /*MMGSDI_PERSO_UNBLOCK */
    break;

    case MMGSDI_PERSO_GET_FEATURE_KEY:
    {
      gsdi_perso_get_feature_key_req_T    msg = {{0}};

      mmgsdi_status = mmgsdi_util_convert_to_gsdi_perso_feature(
                             req_ptr->feature_ind, &msg.feature);

      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("gsdi convert perso feature type failed.");
        break;
      }

      msg.is_unblock_key = req_ptr->perso_data.get_key_data.is_unblock_key;

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &msg,
                                                   GSDI_PERSO_GET_FEATURE_KEY_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /* MMGSDI_PERSO_GET_FEATURE_KEY */
    break;

    case MMGSDI_PERSO_LOCK_DOWN:
    {
      gsdi_perso_lock_down_req_T    msg = {{0}};
      msg.locked_command  =
        (gsdi_perso_lock_down_enum_type) req_ptr->perso_data.locked_command;
      extra_param_ptr->perso_data.locked_command = req_ptr->perso_data.locked_command;

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &msg,
                                                   GSDI_PERSO_LOCK_DOWN_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /* MMGSDI_PERSO_LOCK_DOWN */
    break;

    case MMGSDI_PERSO_GET_UNBLOCK_DCK_RETRIES:
    {
      gsdi_perso_get_dck_unblock_num_retries_req_T  unblock_retries_req = {{0}};

      perso_mmgsdi_status = gsdi_perso_engine_main(req_ptr->request_header.session_id,
                                                   &unblock_retries_req,
                                                   GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ,
                                                   extra_param_ptr->perso_data.ret_data.data_ptr,
                                                   MMGSDI_PERSO_RETURN_DATA_SIZE,
                                                   &extra_param_ptr->perso_data.ret_data.data_len);
    } /* MMGSDI_PERSO_GET_UNBLOCK_DCK_RETRIES */
    break;

    default:
      UIM_MSG_ERR_1("INVALID PERSO ACTION 0x%x", req_ptr->perso_action);
      mmgsdi_status = MMGSDI_ERROR;
      break;
  }

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_populate_client_request_table_info( index, &req_ptr->request_header,
                                                                  NULL,
                                                                  extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  if(perso_mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_DEBUG_MSG_ERROR_1("Error Processing Perso Command mmgsdi_status = 0x%x",
                                 perso_mmgsdi_status);
  }

  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(index,
                                                          MMGSDI_PERSO_REQ,
                                                          perso_mmgsdi_status);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
  }
  return mmgsdi_status;
} /* mmgsdi_gen_perso_req */
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_SESSION_GET_INFO

  DESCRIPTION:
    This function will handle session get info request.

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_session_get_info(
  const mmgsdi_session_get_info_req_type       *req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status            = MMGSDI_INCORRECT_PARAMS;
  int32                                   index                    = 0;
  mmgsdi_client_req_extra_info_type      *extra_param_ptr          = NULL;
  mmgsdi_session_info_type               *session_info_ptr         = NULL;
  mmgsdi_channel_info_type               *channel_info_ptr         = NULL;
  mmgsdi_int_app_info_type               *app_info_ptr             = NULL;
  mmgsdi_session_id_type                  mmgsdi_session_id        = 0;
  mmgsdi_session_get_info_extra_info_type session_info_extra_param;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  memset(&session_info_extra_param,
         0x00,
         sizeof(mmgsdi_session_get_info_extra_info_type));

  /* get the session info */
  if(req_ptr->session_info_query.query_type == MMGSDI_SESSION_INFO_QUERY_BY_ID)
  {
    mmgsdi_session_id = req_ptr->session_info_query.query_key.session_id;
  }
  else if(req_ptr->session_info_query.query_type ==
                                        MMGSDI_SESSION_INFO_QUERY_BY_TYPE)
  {
    if (MMGSDI_SUCCESS != mmgsdi_util_get_mmgsdi_session_id_from_provisioning_type(
                            req_ptr->session_info_query.query_key.session_type,
                            &mmgsdi_session_id))
    {
       UIM_MSG_ERR_1("Invalid Session type requested=0x%x ",
                     req_ptr->session_info_query.query_key.session_type);
       return MMGSDI_INCORRECT_PARAMS;
    }
  }
  else
  {
    UIM_MSG_ERR_1("Invalid Session Query type=0x%x ",
                  req_ptr->session_info_query.query_type);
    return MMGSDI_INCORRECT_PARAMS;
  }

  /* Get app info corresponding to the Session ID */
  mmgsdi_status = mmgsdi_util_get_session_app_info(
                    mmgsdi_session_id, NULL, NULL, &app_info_ptr,
                    &channel_info_ptr,&session_info_ptr, NULL);

  if(mmgsdi_status != MMGSDI_SUCCESS ||
     session_info_ptr == NULL ||
     channel_info_ptr == NULL ||
     app_info_ptr == NULL)
  {
    UIM_MSG_ERR_0("Session Info unavailable");
    return MMGSDI_ERROR;
  }
  session_info_extra_param.slot_id = channel_info_ptr->slot_id;
  session_info_extra_param.channel_id = channel_info_ptr->channel_id;
  session_info_extra_param.session_type = session_info_ptr->session_type;
  mmgsdi_memscpy(&session_info_extra_param.app_data,
                 sizeof(session_info_extra_param.app_data),
                 &app_info_ptr->app_data,
                 sizeof(mmgsdi_aid_type));
  session_info_extra_param.app_state = app_info_ptr->app_state;

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param(
                    MMGSDI_SESSION_GET_INFO_REQ,
                    (void*)&session_info_extra_param,
                    &extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Unable to populate extra param 0x%x", mmgsdi_status);
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }/* end of if (mmgsdi_status != MMGSDI_SUCCESS) */

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
}/* mmgsdi_gen_session_get_info */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_SESSION_READ_PRL

  DESCRIPTION:
    This function will handle session read prl request.

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_session_read_prl(
  const mmgsdi_session_read_prl_req_type       *req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status            = MMGSDI_INCORRECT_PARAMS;
  int32                                   index                    = 0;
  mmgsdi_client_req_extra_info_type      *extra_param_ptr          = NULL;
  mmgsdi_session_info_type               *session_info_ptr         = NULL;
  mmgsdi_session_read_prl_extra_info_type session_read_prl_extra_param;
  mmgsdi_int_app_info_type               *int_app_info_ptr         = NULL;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  mmgsdi_status = mmgsdi_util_get_session_app_info(
                          req_ptr->request_header.session_id,NULL,NULL,
                          &int_app_info_ptr,
                          NULL,&session_info_ptr,NULL);

  if(mmgsdi_status != MMGSDI_SUCCESS ||
     session_info_ptr == NULL ||
     int_app_info_ptr == NULL)
  {
    UIM_MSG_ERR_1("Invalid session ID = 0x%x",
                  req_ptr->request_header.session_id);
    return MMGSDI_INCORRECT_PARAMS;
  }

  if((int_app_info_ptr->app_data.app_type != MMGSDI_APP_CSIM ||
      int_app_info_ptr->app_data.app_type != MMGSDI_APP_RUIM) &&
     (int_app_info_ptr->app_state != MMGSDI_APP_STATE_READY))
  {
    UIM_MSG_ERR_2("app type: 0x%x app state: 0x%x",
                  int_app_info_ptr->app_data.app_type,
                  int_app_info_ptr->app_state);
    return MMGSDI_INCORRECT_PARAMS;
  }

  if(session_info_ptr->session_type == MMGSDI_1X_PROV_PRI_SESSION)
  {
    session_read_prl_extra_param.prl_ptr = mmgsdi_1x_prl_ptr[MMGSDI_PRI_PRL_INDEX];
  }
  else if(session_info_ptr->session_type == MMGSDI_1X_PROV_SEC_SESSION)
  {
    session_read_prl_extra_param.prl_ptr = mmgsdi_1x_prl_ptr[MMGSDI_SEC_PRL_INDEX];
  }
  else if(session_info_ptr->session_type == MMGSDI_1X_PROV_TER_SESSION)
  {
    session_read_prl_extra_param.prl_ptr = mmgsdi_1x_prl_ptr[MMGSDI_TER_PRL_INDEX];
  }
  else
  {
    UIM_MSG_ERR_0("Non 1X PROV Session Type for READ PRL");
    return MMGSDI_INCORRECT_PARAMS;
  }

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_alloc_and_populate_extra_param(
                    MMGSDI_SESSION_READ_PRL_REQ,
                    (void*)&session_read_prl_extra_param,
                    &extra_param_ptr);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Unable to populate extra param 0x%x", mmgsdi_status);
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }/* end of if (mmgsdi_status != MMGSDI_SUCCESS) */

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              extra_param_ptr);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(extra_param_ptr);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
} /* mmgsdi_gen_session_read_prl */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_PROV_APP_INIT_COMPLETE

  DESCRIPTION:
    This function will handle prov app init complete request

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:        The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_prov_app_init_complete(
  const mmgsdi_session_prov_app_init_complete_req_type  *req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status        = MMGSDI_SUCCESS;
  int32                                   index                = 0;
  mmgsdi_session_info_type               *session_info_ptr     = NULL;
  int32                                   client_index         = 0;
  int32                                   session_index        = 0;
  mmgsdi_slot_id_enum_type                slot_id              = MMGSDI_MAX_SLOT_ID_ENUM;
  uint8                                   slot_index           = MMGSDI_SLOT_1_INDEX;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  mmgsdi_status = mmgsdi_util_get_client_and_session_index(
                    req_ptr->request_header.session_id,
                    &client_index,
                    &session_index);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Invalid session id 0x%x", req_ptr->request_header.session_id);
    return MMGSDI_INCORRECT_PARAMS;
  }
  /* mmgsdi_util_get_client_and_session_index success guarantees valid dereferences
     into mmgsdi_client_id_reg_table and session_info_ptr */
  session_info_ptr =
    mmgsdi_client_id_reg_table[client_index]->session_info_ptr[session_index];

  if(session_info_ptr == NULL)
  {
    UIM_MSG_ERR_1("Null session_ptr for session_id 0x%x",
                  (req_ptr->request_header.session_id & 0xFFFFFFFF));
    return MMGSDI_ERROR;
  }

  if(!session_info_ptr->notify_init)
  {
    UIM_MSG_ERR_0("App init complete not expected for this session");
    return MMGSDI_ERROR;
  }

  session_info_ptr->init_complete = req_ptr->init_status;

  mmgsdi_status = mmgsdi_util_get_session_app_info(req_ptr->request_header.session_id,
                                                   &slot_id, NULL, NULL, NULL,
                                                   NULL, NULL);
  if((mmgsdi_status != MMGSDI_SUCCESS) ||
     (slot_id == MMGSDI_MAX_SLOT_ID_ENUM))
  {
    UIM_MSG_ERR_0("Cannot retrieve slot id");
    return MMGSDI_ERROR;
  }

  mmgsdi_status = mmgsdi_util_get_slot_index(slot_id, &slot_index);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return MMGSDI_ERROR;
  }
  if((slot_index != MMGSDI_SLOT_1_INDEX) &&
     (slot_index != MMGSDI_SLOT_2_INDEX) &&
     (slot_index != MMGSDI_SLOT_3_INDEX))
  {
    return MMGSDI_ERROR;
  }

  if(mmgsdi_refresh_info_ptr[slot_index] != NULL)
  {
    mmgsdi_status = mmgsdi_session_refresh_complete(
                      req_ptr->request_header.session_id,
                      req_ptr->init_status,
                      mmgsdi_util_internal_cb,
                      0);

    if (MMGSDI_SUCCESS != mmgsdi_status)
    {
      UIM_MSG_ERR_1("Unable to send Refresh Complete 0x%x", mmgsdi_status);
    }
  } /* if(mmgsdi_refresh_info_ptr[slot_index] != NULL) */

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              NULL);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
}/* mmgsdi_gen_prov_app_init_complete */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_SESSION_MANAGE_ILLEGAL_SUBSCRIPTION

  DESCRIPTION:
    This function will handle a manage subscription request which is triggered
    when a client wants to mark an application illegal based on network
    rejection or later mark it back as legal.

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    Makes application connected with session as illegal or ready, sends illegal
    subscription event or legal subscription event to all sessions associated
    with that app

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_session_manage_illegal_subscription(
  const mmgsdi_session_manage_illegal_subscription_req_type  *req_ptr
)
{
  mmgsdi_channel_info_type            *channel_info_ptr  = NULL;
  mmgsdi_evt_session_notify_type       notify_type;
  mmgsdi_return_enum_type              mmgsdi_status = MMGSDI_SUCCESS;

  UIM_MSG_MED_0("MMGSDI GEN SESSION MANAGE ILLEGAL SUBSCRIPTION CMD");
  memset(&notify_type, 0x00, sizeof(mmgsdi_evt_session_notify_type));

  if(req_ptr == NULL)
  {
    UIM_MSG_ERR_0("Request pointer is NULL in  mmgsdi_gen_session_manage_illegal_subscription");
    return mmgsdi_status;
  }

  mmgsdi_status = mmgsdi_util_get_session_and_channel_info(
                    req_ptr->request_header.session_id,
                    NULL, &channel_info_ptr);

  if((mmgsdi_status != MMGSDI_SUCCESS) ||
     (channel_info_ptr == NULL) ||
     (channel_info_ptr->app_info_ptr == NULL))
  {
    return MMGSDI_ERROR;
  }

  /* Build the appropriate notification */
  notify_type.notify_type = MMGSDI_EVT_NOTIFY_ALL_SESSIONS;
  notify_type.slot_id     = channel_info_ptr->slot_id;
  notify_type.session_id  = 0;

  switch(req_ptr->legal_status)
  {
    /* Mark this application as legal */
    case MMGSDI_SESSION_APP_IS_LEGAL:
      if (channel_info_ptr->app_info_ptr->app_state != MMGSDI_APP_STATE_ILLEGAL)
      {
        UIM_MSG_ERR_1("Manage illegal subscription - Invalid app state: 0x%x",
                      channel_info_ptr->app_info_ptr->app_state);
        return MMGSDI_ERROR;
      }

      channel_info_ptr->app_info_ptr->app_state =
        MMGSDI_APP_STATE_READY;

      UIM_MSG_HIGH_1("Application for session %d is MARKED AS LEGAL BY REQUEST",
                     req_ptr->request_header.session_id);

      mmgsdi_evt_build_and_send_legal_session(
        notify_type,
        (const mmgsdi_int_app_info_type*) channel_info_ptr->app_info_ptr);
      break;

    /* Mark this Application as illegal */
    case MMGSDI_SESSION_APP_IS_ILLEGAL:
      channel_info_ptr->app_info_ptr->app_state =
        MMGSDI_APP_STATE_ILLEGAL;

      UIM_MSG_HIGH_1("Application for session %d is MARKED AS ILLEGAL BY REQUEST",
                     req_ptr->request_header.session_id);

      mmgsdi_evt_build_and_send_illegal_session(
        notify_type,
        (const mmgsdi_int_app_info_type*) channel_info_ptr->app_info_ptr);
      break;

    default:
      UIM_MSG_ERR_0("Invalid manage illegal subscription request attempted.");
      return MMGSDI_ERROR;
  }

  return mmgsdi_status;
}/* mmgsdi_gen_session_manage_illegal_subscription */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_AVAILABLE_APPS_REQ

  DESCRIPTION:
    This function will handle get available apps request

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:        The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_available_apps_req(
  const mmgsdi_available_apps_req_type * req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status        = MMGSDI_SUCCESS;
  int32                                   index                = 0;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              NULL);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
}/* mmgsdi_gen_available_apps_req */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_GET_CPHS_INFO

  DESCRIPTION:
    This function will handle get cphs information request

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:        The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_get_cphs_info(
  const mmgsdi_session_get_cphs_info_req_type *req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status = MMGSDI_SUCCESS;
  int32                                   index         = 0;
  mmgsdi_session_type_enum_type           session_type  = MMGSDI_MAX_SESSION_TYPE_ENUM;

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  /* --------------------------------------------------------------------------
     Validate input Session type: has to be a GW provisioning type
     ----------------------------------------------------------------------- */
  mmgsdi_status = mmgsdi_util_get_session_type(req_ptr->request_header.session_id,
                                               &session_type,
                                               NULL);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("Could not retrieve Session Type for Session ID: 0x%x",
                  req_ptr->request_header.session_id);
    return MMGSDI_INCORRECT_PARAMS;
  }

  switch (session_type)
  {
    case MMGSDI_GW_PROV_PRI_SESSION:
    case MMGSDI_GW_PROV_SEC_SESSION:
    case MMGSDI_GW_PROV_TER_SESSION:
      break;

    default:
      UIM_MSG_ERR_1("Session type is invalid; type=0x%x", session_type);
      return MMGSDI_INCORRECT_PARAMS;
  }

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              NULL);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
}/* mmgsdi_gen_get_cphs_info */


/*===========================================================================
  FUNCTION:      MMGSDI_GEN_GET_APP_CAPABILITIES

  DESCRIPTION:
    This function will handle get app capability request

  DEPENDENCIES:
    NONE

  LIMITATIONS:
    NONE

  RETURN VALUE:
    MMGSDI_SUCCESS:        The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
  SIDE EFFECTS:
    NONE

===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_get_app_capabilities(
  const mmgsdi_session_get_app_capabilities_req_type *req_ptr
)
{
  mmgsdi_return_enum_type                 mmgsdi_status        = MMGSDI_SUCCESS;
  int32                                   index                = 0;

  UIM_MSG_MED_0("MMGSDI GEN GET APP CAPABILITIES CMD");

  /* --------------------------------------------------------------------------
     Validate the Input Parameters checks.
     ----------------------------------------------------------------------- */
  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  /* --------------------------------------------------------------------------
     Get an index from the table used to store client information.
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_get_client_request_table_free_index(&index);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    return mmgsdi_status;
  }

  /* --------------------------------------------------------------------------
     Store the Client Information for later use when the Response is provided
     by the UIM
     ------------------------------------------------------------------------*/
  mmgsdi_status = mmgsdi_util_populate_client_request_table_info(
                                              index,
                                              &req_ptr->request_header,
                                              NULL,
                                              NULL);
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }

  /* send out the response */
  mmgsdi_status = mmgsdi_util_queue_mmgsdi_uim_report_rsp(
                    index, req_ptr->request_header.request_type,
                    MMGSDI_SUCCESS);

  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    mmgsdi_util_free_client_request_table_index(index);
    return mmgsdi_status;
  }
  return MMGSDI_SUCCESS;
}/* mmgsdi_gen_get_app_capabilities */


/*===========================================================================

FUNCTION mmgsdi_gen_onchip_auto_activate

DESCRIPTION
    This function auto activates the OnChip USIM/SIM feature for the given
    slot. This feature is controlled by an EFS item. If the EFS item indicates
    that OnChip feature is to be auto-activated, the function queues the
    command to MMGSDI for activating it for SIM or USIM (which is also
    indicated in the EFS item). The configuration required for activating
    OnChip is read from an EFS item. If the OnChip auto-activation has been
    successfully kicked off, the function returns SUCCESS.

DEPENDENCIES
    None

RETURN VALUE
    MMGSDI_SUCCESS: If OnChip activation req has been successfully queued
    MMGSDI_ERROR:   Otherwise

SIDE EFFECTS
    None
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_gen_onchip_auto_activate(
  mmgsdi_slot_id_enum_type req_slot_id
)
{
  mmgsdi_return_enum_type             mmgsdi_status               = MMGSDI_ERROR;
  mmgsdi_slot_data_type              *slot_data_ptr               = NULL;
  struct fs_stat                      efs_onchip_config_file_stat = {0};
  int                                 fd                          = 0;
  mmgsdi_onchip_sim_config_data_type  mmgsdi_onchip_data;
  uint8                               mmgsdi_onchip_data_buf[MMGSDI_ACTIVATE_ONCHIP_SIM_CONFIG_MAX_SIZE];

  memset(mmgsdi_onchip_data_buf, 0x00, sizeof(mmgsdi_onchip_data_buf));
  mmgsdi_onchip_data.data_ptr = mmgsdi_onchip_data_buf;

  if(req_slot_id != MMGSDI_SLOT_1)
  {
    /* Onchip activation currently supported only for slot1 */
    return MMGSDI_ERROR;
  }

  /* Check the auto-onchip-activation global to see if Onchip should be auto
     activated or not */

  if(mmgsdi_generic_data_ptr == NULL ||
     mmgsdi_generic_data_ptr->onchip_act_data_slot1.auto_activation_enabled == FALSE)
  {
    UIM_MSG_ERR_1("Auto onchip activation disabled for slot 0x%x", req_slot_id);
    return MMGSDI_ERROR;
  }

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(req_slot_id);

  /* If Onchip is already activated, do not re-activate it and return SUCCESS */
  if(slot_data_ptr != NULL &&
     slot_data_ptr->onchip_sim_data.state == MMGSDI_ONCHIP_SIM_INIT)
  {
    UIM_MSG_HIGH_1("Onchip already activated for slot 0x%x", req_slot_id);
    return MMGSDI_SUCCESS;
  }

  /* Read the onchip config file and continue with activating the OnChip
     support with the data in that file */

  /* Get the OnChip config file data for slot specific simulation */
  if(efs_stat(MMGSDI_ONCHIP_CONFIG_EFS_FILE_SLOT1, &efs_onchip_config_file_stat) != 0)
  {
    UIM_MSG_ERR_1("OnChip config file for slot 0x%x not present", req_slot_id);
    return MMGSDI_ERROR;
  }

  if(efs_onchip_config_file_stat.st_size < MMGSDI_ONCHIP_TLV_PAYLOAD ||
     efs_onchip_config_file_stat.st_size > MMGSDI_ACTIVATE_ONCHIP_SIM_CONFIG_MAX_SIZE)
  {
    UIM_MSG_ERR_1("Invalid data in OnChip config file for slot 0x%x",
                  req_slot_id);
    return MMGSDI_ERROR;
  }

  /* Read the OnChip config file for slot1 as we currently support only slot1
     for onchip activation */
  fd = efs_open(MMGSDI_ONCHIP_CONFIG_EFS_FILE_SLOT1, O_RDONLY);
  if(fd < 0)
  {
    return MMGSDI_ERROR;
  }

  if(efs_onchip_config_file_stat.st_size != efs_read(fd,
                                                     mmgsdi_onchip_data.data_ptr,
                                                     efs_onchip_config_file_stat.st_size))
  {
    UIM_MSG_ERR_1("efs_read on OnChip config file for slot 0x%x failed",
                  req_slot_id);
    (void) efs_close(fd);
    return MMGSDI_ERROR;
  }

  (void) efs_close(fd);

  /* Lets construct the length of onchip data to be fed into the MMGSDI API.
     The Onchip data present in the global(read from EFS item) starts with a
     MASTER_TAG (1 byte), followed by 2 bytes for data_length followed by the
     actual data. So, lets calculate the total data length:
     1. Get the number of bytes stored in second and third byte of the onchip
        data buf (reserved for length of onchip data)
     2. Increment it by the OVERHEAD length (1 byte for MASTER_TAG and two
        bytes for length of onchip data */
  mmgsdi_onchip_data.data_len = mmgsdi_onchip_data.data_ptr[1];
  mmgsdi_onchip_data.data_len += mmgsdi_onchip_data.data_ptr[2] * 0x100;
  mmgsdi_onchip_data.data_len += MMGSDI_ONCHIP_TLV_PAYLOAD;

  /* In order to avoid buffer overflow, make sure the data length does not
     exceed the data buffer size */
  if(mmgsdi_onchip_data.data_len > sizeof(mmgsdi_onchip_data_buf))
  {
    UIM_MSG_ERR_0("Invalid Onchip activation data length");
    return MMGSDI_ERROR;
  }

  switch(mmgsdi_generic_data_ptr->onchip_act_data_slot1.app_type)
  {
    case MMGSDI_APP_SIM:
      /* start OnChipSIM command processing for SIM */
      mmgsdi_status = mmgsdi_activate_onchip_sim(
               mmgsdi_generic_data_ptr->client_id, /* MMGSDI client ID */
               req_slot_id,
               mmgsdi_onchip_data,
               mmgsdi_util_internal_cb,
               0x00);
      break;
    case MMGSDI_APP_USIM:
      /* start OnChipSIM command processing for USIM */
      mmgsdi_status = mmgsdi_activate_onchip_usim(
               mmgsdi_generic_data_ptr->client_id, /* MMGSDI client ID */
               req_slot_id,
               mmgsdi_onchip_data,
               mmgsdi_util_internal_cb,
               0x00);
      break;
    default:
      mmgsdi_status = MMGSDI_ERROR;
      break;
  }

  return mmgsdi_status;
} /* mmgsdi_gen_onchip_auto_activate */

