#ifndef MMGSDI_SFS_H
#define MMGSDI_SFS_H
/*===========================================================================


            M M G S D I   T A S K   A N D  S F S  I N T E R F A C E

                                 H E A D E R


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2013 QUALCOMM Technologies, Incorporated and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_sfs.h#1 $$ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
-------------------------------------------------------------------------------
06/11/13   av      Initial Revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "customer.h"
#include "comdef.h"
#include "mmgsdi.h"
///#include "IxErrno.h"

#ifdef FEATURE_MMGSDI_PERSO_SFS
/* <EJECT> */
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/* ------------------------------------------------------------------------*/
/*                 M M G S D I   S F S   I N T E R F A C E                 */
/*                                                                         */
/*                 F U N C T I O N        P R O T Y P E S                  */
/* ------------------------------------------------------------------------*/

/*===========================================================================
FUNCTION MMGSDI_SFS_GET_SIZE

DESCRIPTION
  This function will get the size of the sfs file.


DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type:  MMGSDI_SUCCES:    File size found
                            MMGSDI_ERROR:     SFS error
                            MMGSDI_NOT_FOUND: SFS file did not exist
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_sfs_get_size(
  uint32      *size,
  const char  *sfs_file_name
);

/*===========================================================================
FUNCTION MMGSDI_SFS_MKDIR

DESCRIPTION
  This function will make the sfs directory for perso.


DEPENDENCIES
  The parent directory has already been created.

RETURN VALUE
  mmgsdi_return_enum_type:  MMGSDI_SUCCES:    Directory was created
                            GSDI_ERROR:     Error creating directory
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_sfs_mkdir(
  const char* sfs_file_name
);

/*===========================================================================
FUNCTION MMGSDI_SFS_RMFILE

DESCRIPTION
  This function will clean up if the file is present on the sfs.


DEPENDENCIES
  The file does not exist.

RETURN VALUE
  mmgsdi_return_enum_type:  MMGSDI_SUCCES:    File was removed
                            MMGSDI_ERROR:     File removal error
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_sfs_rmfile(
  const char* sfs_file_name
);

/*===========================================================================
FUNCTION MMGSDI_SFS_WRITE

DESCRIPTION
  This function will write to the sfs file.


DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type:  MMGSDI_SUCCES:    Successful write
                            MMGSDI_ERROR:     SFS error
                            MMGSDI_NOT_FOUND: Unable to open file
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_sfs_write(
  uint8      *buffer_ptr,
  uint32     *len,
  const char *sfs_file_name
);

/*===========================================================================
FUNCTION MMGSDI_SFS_READ

DESCRIPTION
  This function will read from the sfs file.


DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type:  MMGSDI_SUCCES:    Successful read
                            MMGSDI_ERROR:     SFS error
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_sfs_read(
  uint8      *buffer_ptr,
  uint32      len,
  const char *sfs_file_name
);
#endif /* FEATURE MMGSDI_PERSO_SFS */

#endif /* MMGSDI_SFS_H */

