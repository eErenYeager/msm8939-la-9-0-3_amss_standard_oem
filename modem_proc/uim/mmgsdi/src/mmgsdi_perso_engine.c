
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


            G E N E R I C   P E R S O N A L I Z A T I O N   E N G I N E


GENERAL DESCRIPTION

  This source file contains the GSDI PERSONALIZATION ENGINE which adheres to
  the personalization interface to do the personalization procedures.

                        COPYRIGHT INFORMATION

Copyright (c) 2001 - 2015 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_perso_engine.c#2 $$ $DateTime: 2015/11/24 03:21:58 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/24/15   ar      Fake the fourth byte of EF AD for buggy card
02/13/14   ar      Replace session checks with utility functions
01/10/14   df      Use v2.0 diag macros
12/12/13   yt      Ensure correct buffer bounds checking
12/11/13   ar      Slot 3 support in personalization check for additional slots
12/05/13   ar      Replaced all mmgsdi_malloc direct calls with macro
10/16/13   df      Removing legacy function calls and unused functions
10/04/13   bcho    F3 message reduction
08/30/13   yt      Add check for integer overflow
08/30/13   tl      Convert channel info table from static to dynamic allocation
08/13/13   vv      Fix for a possible integer over flow
08/07/13   yt      Use correct buffer size while copying data from Perso file
06/24/13   vdc     Added support for triple SIM
06/19/13   av      Fixed compilation error on W-flavor build
06/18/13   av      Fallback to SFS if SFP not available
05/21/13   vv      Added support for session closed cause
05/16/13   vdc     Replace memcpy with safer version memscpy
05/14/13   vdc     Memory optimizations for slot, app, pin, upin tables
04/16/13   yt      Avoid efs_sync during MMGSDI initialization
03/27/13   spo     Added support for no personalization on additional slots
01/28/13   vv      Fix compiler warnings
01/02/13   at      Fix for making sure EFS is ready by doing an EFS sync
11/12/12   bcho    Use utility function to get length of MNC in IMSI
10/15/12   spo     Power down the card when perso sanity error occurs
09/28/12   abg     Updated ERR to MSG_ERROR
08/23/12   abg     Fix the perso imsi encoding scheme
08/07/12   tl      Create perso.txt and perso_ruim.txt for single mode devices
04/20/12   av      Enabled detouring of nv/efs calls to offtarget framework
03/30/12   av      Replaced feature flags with NVITEM
03/21/12   bcho    function gsdi_perso_engine_send_mmgsdi_event modified to
                   send perso event with done/wait for external perso status
                   based on halt subscription config value in efs
03/16/12   yt      Remove inclusion of tmc.h
03/02/12   kk      Fixed perso handling for erroneous RUIM cards
12/23/11   shr     Removed featurization for ALWAYS ON features
12/21/11   shr     Legacy GSDI removal updates
12/21/11   shr     Send Card Error (due to Perso sanity error) only on the
                   slot correspnding to the sanity error, if its due to
                   a card specific failure
08/22/11   bcho    Check efs server status before proceeding with
                   perso requests which do write operation on efs
06/17/11   nmb     Corrected perso file TLV interpretation
06/13/11   nmb     Create RUIM perso file in case of SIM file create failure
05/23/11   yt      Added comments to featurization
05/17/11   yt      Added gsdi_fusion_perso_register_force_cleanup()
05/04/11   ms      Send the PERSO_EVT to the late clients in perso state
                   as WAIT_FOR_DEPERSO_S and INIT_ERROR_S
04/28/11   nk      Fixed compiler warning
04/13/11   nmb     Remove dependency on hardware ESN
04/07/11   ms      Implementation of 2nd Slot Special Behavior for perso
                   by Locking only one of the SIM slots.
02/17/11   kk      Fixed perso handling for slot2
01/20/11   nmb     Fixed Lint errors
01/11/11   kk      Fixed perso refresh req handling
12/22/10   yt      Removed FEATURE_UIM_ZI_MEMORY_REDUCTION
10/20/10   yt      KW fixes
10/11/10   nmb     Write perso data to SFS instead of EFS when SFS is enabled
09/23/10   ms      Changed the perso file path
09/15/10   nmb     Allocate mmgsdi_client_id_reg_table dynamically
08/30/10   shr     Perso clean up for Sessions
07/29/10   mib     Fixed check on slot for perso requests
07/12/10   nmb     Include indicator retry values in events
07/12/10   nmb     Fixed PERSO_RUIM state progression
06/26/10   adp     Phone is not replying to GET_PERM_FEATURE_INDICATION_CMD
06/25/10   shr     Perso commands need to be allowed when no Card is inserted
06/24/10   nmb     SFS support while PERSO_SIM and PERSO_RUIM are enabled
06/14/10   nb      Fixed efs_sync compilation error on LA builds
06/09/10   nmb     Enabled presence of application without associated perso
05/26/10   yt      Lint fixes
05/19/10   nb      Updated call to mmgsdi_uim_icc_write_transparent
05/03/10   adp     Adding Perso related mapping from gsdi perso status to
                   mmgsdi perso status.
04/26/10   nb      Forced sync after EFS write
04/23/10   yt      Fixed KW errors
03/24/10   adp     Eliminated FEATURE_MMGSDI_NO_TCB_PTR_OR_CRIT_SEC feature,
                   which was reintroduced because of a merge
03/18/10   kp      Fix for the KW errors
12/23/09   shr     Fixed Perso Lock Down handling
12/18/09   nb      Moved Dual Slot Changes
12/16/09   nb      Klockwork Fixes
11/29/09   js      Fix for invalid return value in case of perso read failure
10/28/09   mib     Fixed klocwork warning
10/21/09   rn      Fixed klocwork errors
10/17/09   mib     Fixed Klocwork warning
10/16/09   mib     Fixed MNC length when EF-AD has only 3 bytes
09/26/09   kp      ZI memory reduction changes
09/16/09   js      Fixed KW Errors
09/08/09   rn      Fixed klocwork errors
09/01/09   js      Remove incorrect featurization in
                   gsdi_perso_register_force_cleanup()
08/17/09   js      Implemented RPC cleanup func for gsdi_perso_register_task
07/30/09   mib     Added mmgsdi_perso_lock_down_and_enable_limited_access
07/29/09   shr     Fixed Lint Warnings
07/20/09   mib     Replaced flag TEST_FRAMEWORK with FEATURE_UIM_TEST_FRAMEWORK
                   Fixed F3 log with string
06/15/09   sg      Support to make PERSO FEATURE work for qtf
06/01/09   ssr     Merging of SIM and RUIM PERSO
05/13/09   kk      Eliminated FEATURE_MMGSDI_NO_TCB_PTR_OR_CRIT_SEC feature
03/26/09   tml     Enabled Perso procedure to continue at power up after
                   unblock perso is successfully
03/18/09   ssr     Fixed perso read length
02/18/09   tml     Fixed memory leak
01/28/09   kk      Fixed KW errors - 'may be NULL and may be dereferenced'
01/16/09   nb      Changed gsdi_fs_name_test() to gsdi_efs_name_test() and
                   gsdi_fs_mkdir() to gsdi_efs_mkdir()
12/18/08   sun     Removed old Perso
09/25/08   kk      Added support for extracting unblock dck retries available
09/04/08   sp      Process get_feature_data and get_feature_key requests in
                   GSDI_PERSO_ENG_CARD_ERROR_S.
08/27/08   sp      Added support for get_dck_num_retries.
07/21/08   sp      Added debug messages.
06/13/08   KK      Fixed more compilation warnings
06/10/08   kk      Fixed compilation warnings
05/07/08   kk      Handling of set_feature_data when no sim
04/16/08   kk      Added support of get command on sim lock data
11/15/07   sun     Removed unnecessary check
08/01/07   sun     Allow activation/deactivation also when there is no card.
07/20/07   sun     Initialize Data before Sim Lock procedures are run
05/07/07   tml     Sent Perso Complete event as needed
05/07/07   sp      Replaced calls to mem_malloc/free with mmgsdi_malloc/free
03/26/07   sun     Fixed Buffer overflow
02/08/07   sun     Allocate enough memory for Autolock
01/29/07   sun     Checked for NULLS
01/17/07   pv      Save the last sent perso event to gsdi_data_ptr->perso_event
                   in gsdi_perso_engine_notify_clients_of_perso_event,
                   send the last saved event when a new registration comes in
                   irrespective of the current perso_engine_state.
                   Get rid of gsdi_perso_notify_clients_of_perso_event and
                   externalize gsdi_perso_engine_notify_clients_of_perso_event.
01/15/07   nk      Updated EFS Path depending if apps mount feature defined
10/31/06   sun     Fixed Warning
10/27/06   sun     Send the entire mask down to the security instead of just
                   one feature.
10/23/06   sun     Send the entire mask down to the security instead of just
                   one feature.
09/27/06   sun     Fixed Lint Error
09/13/06   wli     Fixed bugs when converting between masks and features
08/09/06   sun     Send last perso error event to the newly registered
                   clients
                   In the Card Error State, allow the GET_FEATURE_IND req
                   to be handled.
07/27/06   sun     Removed the return status for gsdi_perso_phone_init. The
                   function will be able to write to NV only once.
                   Added new state for card error
05/08/06   nk      Featurized gsdi_perso_engine_init_phone_code call
05/04/06   sun     Fixed Length Conversion
04/21/06   tml     RPC compilation fix
04/28/06   sun     Initialize Phone code values at Perso Init
04/24/06   sun     Passed control keys to security lib for Disable Feature
                   Indicator
04/21/06   tml     Revert the PERSO_ENGINE_GET_SIM_ENCRYPTED DATA for loop
                   change
04/21/06   nk      Fixed GSDI_PERSO_ENGINE_GET_RUIM_ENCRYPTED_DATA for loop
04/06/06   sun     Added support for RUIM Lock
03/21/06   tml     lint
03/11/06   tml     return success if imei is not present in nv
03/10/06   jar     Lint Fixes.
02/01/06   tml     fixed refresh dck logic so that we won't try to deactivate
                   when dck ef is not found
01/25/06   sun     Write to FS only if the data was modified.
01/12/06   sp      Removed early return in gsdi_perso_engine_write_back_fs()
12/21/05   sun     Updated File system when counters are updated
11/22/05   jk      Changes to support multiprocessor RPC
11/17/05   sun     Added support for GSDI_PERSO_ENG_WAIT_FOR_DEPERSO_S
                   and GSDI_PERSO_ENG_INIT_ERROR_S
11/09/05   sun     Fixed lint errors
10/28/05   sun     Fixed Memory leak and return error if file is zero length
10/03/05   sun     Pass the event from the perso engine to the security and back
09/23/5    sun     Skip the length field from data buffer
09/22/05   sun     Copy the data from the file buffer to the sec data pointer
08/30/05   jar     3 Digit MNC Bug Fix
08/19/05   tml     Fixed compilation error
08/17/05   sun     Memset all Encrypted Data to 0 and fixed copy of data from
                   the perso file
07/20/05   sun     Added support with sfs
07/12/05   sun     Fixed Perso File Compatibility issues with Perso-Engine Files
06/13/05   sun     Removed architectural drawing
05/20/05   sun     Fixed return values
05/13/05   sun     Consolidated perso Engine files into one file
05/05/05   sun     Added support for Perso Refresh
05/03/05   sun     NON QC Library Fixes for Check All Locks
05/01/05   jar     Compilation fir for NON QC Security Libraries
04/30/05   jar     Fixed bad Debug Message.  Updated Analyze Indicators to
                   skip autolock if all states are inactive.  Various Logic
                   Fixes
01/05/05   sun     Initial Release


===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#include "mmgsdi_perso_engine.h"
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
#include "sfs_private.h"
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#include "mmgsdi_sfs.h"
#endif /* FEATURE_MMGSDI_PERSO_SFS */
#include "mmgsdi_efs.h"
#include "fs_public.h"
#include "mmgsdi_perso_sec.h"
#include "mmgsdi_perso_engine_ruim.h"
#include "mmgsdi_nv.h"
#include "uim.h"
#include "uim_msg.h"

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
#include "intconv.h"

#include "mmgsdiutil.h"
#include "mmgsdi_uim_uicc.h"
#include "mmgsdi_uim_icc.h"
#include "mmgsdi_uicc.h"
#include "mmgsdi_icc.h"
#include "mmgsdi_evt.h"
#include "mmgsdi_card_init.h"

#ifdef FEATURE_SDCC_BOOT
#include "remotefs_cli_api.h"
#endif /* FEATURE_SDCC_BOOT */

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

/* Ensure that either FEATURE_PERSO_SIM or FEATURE_PERSO_RUIM is defined
   when Perso is enabled */
#if !defined(FEATURE_PERSO_SIM) && !defined(FEATURE_PERSO_RUIM)
#error Personalization cannot be enabled when both FEATURE_PERSO_SIM and FEATURE_PERSO_RUIM are not defined!!!
#endif /* !FEATURE_PERSO_SIM && !FEATURE_PERSO_RUIM */


/* <EJECT> */
/*===========================================================================

            DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/

/* Personalization Files */
#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#else
  static char *mmgsdi_efs_mmgsdi_dir  = "mmgsdi";
#if defined(FEATURE_MMGSDI_PERSO_SFS)
  static char *mmgsdi_sfs_perso_dir    = "/mmgsdi/perso_sfs";
  extern char *mmgsdi_ruim_perso_sfs_file;
#if !defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM)
  /* SFP is disabled */
  static char *mmgsdi_sfs_perso_file   = "/mmgsdi/perso_sfs/perso.txt";
#endif /* !FEATURE_MMGSDI_PERSO_SFP_AVAILABLE && FEATURE_PERSO_SIM */
#else
  static char *mmgsdi_efs_perso_dir        = "mmgsdi/perso";
  static char *mmgsdi_efs_perso_file       = "mmgsdi/perso/perso.txt";
#endif /* FEATURE_MMGSDI_PERSO_SFS */
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

/* Temp EFS file path for making sure EFS is ready */
#define MMGSDI_EFS_CHECK_TEMP_PATH  "nv/item_files/modem/uim/mmgsdi/temp"

#ifdef FEATURE_SDCC_BOOT
/* Holds the status of the EFS sync */
static boolean mmgsdi_first_efs_sync_done = FALSE;
#endif /* FEATURE_SDCC_BOOT */

/* ----------------------------------------------------------------------------
   Table to hold the list of event handlers
   --------------------------------------------------------------------------*/
gsdi_perso_events_cb gsdi_perso_event_handler_table[GSDI_PERSO_ENGINE_MAX_REG_CLIENTS];

/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_BUILD_AND_QUEUE_INIT_PROC_REQ

DESCRIPTION
  This function is used to build a PERSO INIT PROC request and put it in
  the GSDI command queue

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_build_and_queue_init_proc_req(
  mmgsdi_session_id_type              session_id,
  gsdi_perso_internal_command_req_T   *req_ptr
)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;

  MMGSDIUTIL_RETURN_IF_NULL(req_ptr);

  mmgsdi_status = mmgsdi_card_init_build_internal_perso_req(
                    session_id,
                    req_ptr->active_lock_mask,
                    req_ptr->autolock_mask);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_1("PERSO: Could not queue Perso Init. command, status: 0x%x",
                  mmgsdi_status);
    return MMGSDI_ERROR;
  }

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_build_and_queue_init_proc_req */


/* ==========================================================================
FUNCTION GSDI_PERSO_ENGINE_GET_NEXT_STATE

DESCRIPTION
  This function is used to get the next Perso state based on the Perso
  event.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_next_state(
  gsdi_perso_event_enum_type     event,
  mmgsdi_perso_state_enum_type  *perso_state
)
{
  /* --------------------------------------------------------------------------
     Validate the Input Parameters
     ------------------------------------------------------------------------*/
  if(NULL == perso_state)
  {
    UIM_MSG_ERR_0("PERSO: NULL POINTER gsdi_perso_engine_send_event");
    return MMGSDI_INCORRECT_PARAMS;
  }
  *perso_state = MMGSDI_PERSO_ENG_MAX_S;

  switch ( event )
  {
    case GSDI_PERSO_NO_EVENT:
      return MMGSDI_INCORRECT_PARAMS;
#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_NW_FAILURE:
    case GSDI_PERSO_NS_FAILURE:
    case GSDI_PERSO_SP_FAILURE:
    case GSDI_PERSO_CP_FAILURE:
    case GSDI_PERSO_SIM_FAILURE:
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
    case GSDI_PERSO_RUIM_NW1_FAILURE:
    case GSDI_PERSO_RUIM_NW2_FAILURE:
    case GSDI_PERSO_RUIM_HRPD_FAILURE:
    case GSDI_PERSO_RUIM_SP_FAILURE:
    case GSDI_PERSO_RUIM_CP_FAILURE:
    case GSDI_PERSO_RUIM_RUIM_FAILURE:
#endif /* FEATURE_PERSO_RUIM */
      *perso_state = MMGSDI_PERSO_ENG_WAIT_FOR_DEPERSO_S;
      break;

#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_NW_DEACTIVATED:
    case GSDI_PERSO_NS_DEACTIVATED:
    case GSDI_PERSO_SP_DEACTIVATED:
    case GSDI_PERSO_CP_DEACTIVATED:
    case GSDI_PERSO_SIM_DEACTIVATED:
    case GSDI_PERSO_NCK_UNBLOCKED:
    case GSDI_PERSO_NSK_UNBLOCKED:
    case GSDI_PERSO_SPK_UNBLOCKED:
    case GSDI_PERSO_CCK_UNBLOCKED:
    case GSDI_PERSO_PPK_UNBLOCKED:
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
    case GSDI_PERSO_RUIM_NW1_DEACTIVATED:
    case GSDI_PERSO_RUIM_NW2_DEACTIVATED:
    case GSDI_PERSO_RUIM_HRPD_DEACTIVATED:
    case GSDI_PERSO_RUIM_SP_DEACTIVATED:
    case GSDI_PERSO_RUIM_CP_DEACTIVATED:
    case GSDI_PERSO_RUIM_RUIM_DEACTIVATED:
    case GSDI_PERSO_RUIM_NCK1_UNBLOCKED:
    case GSDI_PERSO_RUIM_NCK2_UNBLOCKED:
    case GSDI_PERSO_RUIM_HNCK_UNBLOCKED:
    case GSDI_PERSO_RUIM_SPCK_UNBLOCKED:
    case GSDI_PERSO_RUIM_CCK_UNBLOCKED:
    case GSDI_PERSO_RUIM_PCK_UNBLOCKED:
#endif /* FEATURE_PERSO_RUIM */
      *perso_state = MMGSDI_PERSO_ENG_INIT_S;
      break;

#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_NCK_BLOCKED:
    case GSDI_PERSO_NSK_BLOCKED:
    case GSDI_PERSO_SPK_BLOCKED:
    case GSDI_PERSO_CCK_BLOCKED:
    case GSDI_PERSO_PPK_BLOCKED:
#endif /* FEATURE_PERSO_SIM */
    case GSDI_PERSO_SANITY_ERROR:
#ifdef FEATURE_PERSO_RUIM
    case GSDI_PERSO_RUIM_NCK1_BLOCKED:
    case GSDI_PERSO_RUIM_NCK2_BLOCKED:
    case GSDI_PERSO_RUIM_HNCK_BLOCKED:
    case GSDI_PERSO_RUIM_SPCK_BLOCKED:
    case GSDI_PERSO_RUIM_CCK_BLOCKED:
    case GSDI_PERSO_RUIM_PCK_BLOCKED:
#endif /* FEATURE_PERSO_RUIM */
      *perso_state = MMGSDI_PERSO_ENG_INIT_ERROR_S;
      break;

    default:
      break;
  }
  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_get_next_state */


/* ==========================================================================
FUNCTION GSDI_PERSO_ENGINE_SEND_MMGSDI_EVENT_TO_ALL_PROV_SESSIONS

DESCRIPTION
  This function is used to send Perso event to all relevant provisioning
  sessions

DEPENDENCIES
  None

RETURN VALUE
  None
===========================================================================*/
static void gsdi_perso_engine_send_mmgsdi_event_to_all_prov_sessions(
  mmgsdi_perso_status_enum_type   status,
  mmgsdi_perso_feature_enum_type  feature,
  mmgsdi_perso_state_enum_type    perso_state,
  uint32                          num_retries,
  uint32                          num_unblock_retries
)
{
  mmgsdi_session_info_type        * curr_session_ptr = NULL;
  int32                             client_index     = 0;
  int32                             session_index    = 0;
  boolean                           notify_session   = FALSE;
  mmgsdi_len_type                   num_sessions     = 0;
  mmgsdi_evt_session_notify_type    notify_type;
  gsdi_perso_internal_command_req_T req;


  memset(&notify_type, 0x00, sizeof(mmgsdi_evt_session_notify_type));
  notify_type.notify_type = MMGSDI_EVT_NOTIFY_ALL_SESSIONS;

  memset(&req, 0x00, sizeof(gsdi_perso_internal_command_req_T));

  /* Scan through all open sessions of MMGSDI and send Perso event/start Perso
     initialization for relevant provisioning sessions.
     (All provisioning apps shall be linked to MMGSDI sessions,
     hence no need to check for other client Sessions) */
  num_sessions = mmgsdi_client_id_reg_table[client_index]->num_session;

  for (session_index = 0; (num_sessions > 0) &&
       (session_index < MMGSDI_MAX_SESSION_INFO); session_index++)
  {
    curr_session_ptr =
      mmgsdi_client_id_reg_table[client_index]->session_info_ptr[session_index];

    if(curr_session_ptr == NULL)
    {
      /*Invalid Session, Continue to next*/
      continue;
    }
    num_sessions--;

    /* If MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1 is enabled
       and if the current session is stuck at Perso, and there has been a
       successful Deperso operation, restart the Perso initialization for
       the session active on only slot_1. */

    if((curr_session_ptr->channel_info_index < MMGSDI_MAX_CHANNEL_INFO) &&
       (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index] != NULL) &&
       (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr != NULL))
    {
      if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1) == MMGSDI_FEATURE_ENABLED &&
         mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->slot_id != MMGSDI_SLOT_1)
      {
        continue;
      }
      switch(curr_session_ptr->session_type)
      {
        case MMGSDI_GW_PROV_PRI_SESSION:
        case MMGSDI_GW_PROV_SEC_SESSION:
        case MMGSDI_GW_PROV_TER_SESSION:
          /* If the feature for which the event is being sent out is specific
             to GW provisioning sessions or if it is a generic event applicable
             to all provisioning sessions, send out the event to the GW
             provisioning sessions */
          if((feature == MMGSDI_MAX_PERSO_FEATURE_ENUM) ||
             ((feature >= MMGSDI_PERSO_NW) &&
             (feature <= MMGSDI_PERSO_SIM))
            )
          {
            notify_session = TRUE;
          }
          break;
        case MMGSDI_1X_PROV_PRI_SESSION:
        case MMGSDI_1X_PROV_SEC_SESSION:
        case MMGSDI_1X_PROV_TER_SESSION:
          /* If the feature for which the event is being sent out is specific
             to 1X provisioning sessions or if it is a generic event applicable
             to all provisioning sessions, send out the event to the 1X
             provisioning sessions */
          if((feature == MMGSDI_MAX_PERSO_FEATURE_ENUM) ||
             ((feature >= MMGSDI_PERSO_RUIM_NW1) &&
             (feature <= MMGSDI_PERSO_RUIM_RUIM))
            )
          {
            notify_session = TRUE;
          }
          break;
        default:
          break;
      }
      /* If session needs to be notified of the Perso event */
      if(notify_session)
      {
        /* If Perso initizaliation needs to be run for the Session */
        if(perso_state == MMGSDI_PERSO_ENG_INIT_S)
        {
          /* If the current session is stuck at Perso, and there has been a
             successful Deperso operation, restart the Perso initialization
             for the session */
          if((mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state ==
              MMGSDI_APP_STATE_READY_FOR_PERSO) &&
             (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state !=
              MMGSDI_PERSO_ENG_INIT_COMPLETED_S))
          {
            /* When Perso init needs to be done due to a REFRESH, start
               Perso init. from MMGSDI_PERSO_ENG_INIT_S state */
            if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state ==
              MMGSDI_PERSO_ENG_REFRESH_S)
            {
              mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state =
                MMGSDI_PERSO_ENG_NOT_INIT_S;
            }
            else
            {
              mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state =
                MMGSDI_PERSO_ENG_INIT_S;
            }

            (void) gsdi_perso_engine_build_and_queue_init_proc_req(
                     curr_session_ptr->session_id, &req);
          }
        }
        notify_type.slot_id = mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->slot_id;

        /* Store the Personalization info in app_info_ptr,
        To be used for Late Clients */
        mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_info.feature             =
          feature;
        mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_info.num_retries         =
          num_retries;
        mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_info.num_unblock_retries =
          num_unblock_retries;

        /* Notify Session of the Perso event */
        mmgsdi_evt_build_and_send_perso(
          notify_type,
          (const mmgsdi_int_app_info_type*)
            mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr,
          status,
          feature,
          num_retries,
          num_unblock_retries);

        notify_session = FALSE;
      }
    }
  }
}/* gsdi_perso_engine_send_mmgsdi_event_to_all_prov_sessions */


/* ==========================================================================
FUNCTION GSDI_PERSO_ENGINE_SEND_MMGSDI_EVENT

DESCRIPTION
  This function is used send the mmgsdi event.

DEPENDENCIES
  None

RETURN VALUE
  boolean: Indicating if the event has been sent or not
===========================================================================*/
static boolean gsdi_perso_engine_send_mmgsdi_event(
  gsdi_perso_event_enum_type      event,
  mmgsdi_slot_id_enum_type        mmgsdi_slot,
  mmgsdi_int_app_info_type       *app_info_ptr,
  boolean                         send_to_all_prov
)
{
  mmgsdi_perso_status_enum_type         status              = MMGSDI_PERSO_STATUS_NONE;
  mmgsdi_perso_feature_enum_type        feature             = MMGSDI_MAX_PERSO_FEATURE_ENUM;
  mmgsdi_perso_state_enum_type          perso_state         = MMGSDI_PERSO_ENG_MAX_S;
  mmgsdi_slot_data_type                *slot_data_ptr       = NULL;
  mmgsdi_return_enum_type               mmgsdi_status       = MMGSDI_SUCCESS;
  mmgsdi_evt_session_notify_type        notify_type;
  gsdi_perso_get_dcks_num_retries_req_T req;
  uint32                                num_retries         = 0;
  uint32                                num_unblock_retries = 0;
  int32                                 offset              = GSDI_PERSO_STATUS_IND_OFFSET_MAX;
  int32                                 ret_data_len        = 0;
  uint8                                 ret_data[GSDI_PERSO_NUM_OF_SIM_FEATURES
                                                   + GSDI_PERSO_NUM_OF_RUIM_FEATURES];

  memset(&notify_type, 0x00, sizeof(mmgsdi_evt_session_notify_type));
  memset(&req, 0x00, sizeof(gsdi_perso_get_dcks_num_retries_req_T));

  if(mmgsdi_slot != MMGSDI_SLOT_1 &&
     mmgsdi_slot != MMGSDI_SLOT_2 &&
     mmgsdi_slot != MMGSDI_SLOT_3)
  {
    UIM_MSG_ERR_1(" Invalid Card Slot 0x%x in gsdi_perso_engine_send_mmgsdi_event",mmgsdi_slot);
    mmgsdi_slot = MMGSDI_MAX_SLOT_ID_ENUM;
  }

  switch ( event )
  {
    case GSDI_PERSO_NO_EVENT:
      return FALSE;

#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_NW_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_NW;
      offset  = GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NS_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_NS;
      offset  = GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SP_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_SP;
      offset  = GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_CP_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_CP;
      offset  = GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SIM_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_SIM;
      offset  = GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NW_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_NW;
      offset  = GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NS_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_NS;
      offset  = GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SP_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_SP;
      offset  = GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_CP_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_CP;
      offset  = GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SIM_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_SIM;
      offset  = GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_NW;
      offset  = GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NSK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_NS;
      offset  = GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SPK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_SP;
      offset  = GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_CCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_CP;
      offset  = GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_PPK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_SIM;
      offset  = GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_NW;
      offset  = GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_NSK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_NS;
      offset  = GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_SPK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_SP;
      offset  = GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_CCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_CP;
      offset  = GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_PPK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_SIM;
      offset  = GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET;
      break;
#endif /* FEATURE_PERSO_SIM */

#ifdef FEATURE_PERSO_RUIM
    case GSDI_PERSO_RUIM_NW1_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_NW1;
      offset  = GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NW2_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_NW2;
      offset  = GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_HRPD_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_HRPD;
      offset  = GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_SP_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_SP;
      offset  = GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_CP_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_CP;
      offset  = GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_RUIM_FAILURE:
      status  = MMGSDI_PERSO_STATUS_LOCKED;
      feature = MMGSDI_PERSO_RUIM_RUIM;
      offset  = GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NW1_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW1;
      offset  = GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NW2_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW2;
      offset  = GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_HRPD_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_HRPD;
      offset  = GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_SP_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_SP;
      offset  = GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_CP_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_CP;
      offset  = GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_RUIM_DEACTIVATED:
      status  = MMGSDI_PERSO_STATUS_UNLOCKED;
      feature = MMGSDI_PERSO_RUIM_RUIM;
      offset  = GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NCK1_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW1;
      offset  = GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NCK2_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW2;
      offset  = GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_HNCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_HRPD;
      offset  = GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_SPCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_SP;
      offset  = GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_CCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_CP;
      offset  = GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_PCK_UNBLOCKED:
      status  = MMGSDI_PERSO_STATUS_UNBLOCKED;
      feature = MMGSDI_PERSO_RUIM_RUIM;
      offset  = GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NCK1_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW1;
      offset  = GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_NCK2_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_NW2;
      offset  = GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_HNCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_HRPD;
      offset  = GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_SPCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_SP;
      offset  = GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_CCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_CP;
      offset  = GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET;
      break;
    case GSDI_PERSO_RUIM_PCK_BLOCKED:
      status  = MMGSDI_PERSO_STATUS_BLOCKED;
      feature = MMGSDI_PERSO_RUIM_RUIM;
      offset  = GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET;
      break;
#endif /* FEATURE_PERSO_RUIM */

    case GSDI_PERSO_SANITY_ERROR:
      status = MMGSDI_PERSO_STATUS_SANITY_ERROR;
      break;

    case GSDI_PERSO_EVT_INIT_COMPLETED:
      {
        /*Perso initialization procedures completed,
          check halt subscription config value from efs and if it is TRUE,
          inform client that mmgsdi is waiting external personalization to be
          performed.
          This enables internal perso to co exist with external perso.
        */
        char    *mmgsdi_halt_sub_file    = MMGSDI_HALT_SUBSCRIPTION_NV_PATH;
        boolean  mmgsdi_halt_sub_setting = FALSE;
        int      efs_read_status         = 0;

        /* Get halt subscription config value from efs */
        efs_read_status = efs_get(mmgsdi_halt_sub_file,
                                  &mmgsdi_halt_sub_setting,
                                  sizeof(boolean));

        if (efs_read_status < 0)
        {
          UIM_MSG_HIGH_1("Halt subscription config read failed, status = 0x%x. Sending perso evt with DONE status",
                     efs_read_status);
          status = MMGSDI_PERSO_STATUS_DONE;
        }
        else if (mmgsdi_halt_sub_setting == FALSE)
        {
          UIM_MSG_HIGH_0("Halt subscription config disabled. Sending perso evt with DONE status");
          status = MMGSDI_PERSO_STATUS_DONE;
        }
        else
        {
          UIM_MSG_HIGH_0("Halting subscription. Waiting for SUB OK request");

          /* No change in state. Continue to remain in wait for SUB OK state */
          status = MMGSDI_PERSO_STATUS_WAIT_FOR_EXT_PERSO;
        }
      }
      break;

    default:
      UIM_MSG_ERR_1("Invalid GSDI Perso event 0x%x", event);
      return FALSE;
  }

  /* Based on the Perso event, determine if the app Perso state needs
     to transition */
  mmgsdi_status = gsdi_perso_engine_get_next_state(event, &perso_state);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return FALSE;
  }

  /* Determine the number of retries and unblock retries if needed */
  if(offset < GSDI_PERSO_STATUS_IND_OFFSET_MAX)
  {
    mmgsdi_status = gsdi_perso_engine_handle_request(
                      0,
                      NULL,
                      MMGSDI_MAX_SLOT_ID_ENUM,
                      &req,
                      GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ,
                      ret_data,
                      sizeof(ret_data),
                      &ret_data_len,
                      &event);

    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
      num_retries = (uint32)ret_data[offset];
    }

    mmgsdi_status = gsdi_perso_engine_handle_request(
                      0,
                      NULL,
                      MMGSDI_MAX_SLOT_ID_ENUM,
                      &req,
                      GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ,
                      ret_data,
                      sizeof(ret_data),
                      &ret_data_len,
                      &event);

    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
      num_unblock_retries = (uint32)ret_data[offset];
    }
  }

  /* If event needs to be sent to all relevant provisioning applications */
  if(send_to_all_prov)
  {
    gsdi_perso_engine_send_mmgsdi_event_to_all_prov_sessions(status,
                                                             feature,
                                                             perso_state,
                                                             num_retries,
                                                             num_unblock_retries);
  }
  /* Send event only to the Sessions mapping to the app passed in */
  else
  {
    if(app_info_ptr == NULL)
    {
      UIM_MSG_ERR_0("Invalid input parameter");
      return FALSE;
    }

    /* Transition Perso state for the app */
    if(perso_state != MMGSDI_PERSO_ENG_MAX_S)
    {
     /* Do not change to MMGSDI_PERSO_ENG_INIT_S when we are in
        the MMGSDI_PERSO_ENG_INIT_COMPLETED_S. Since Perso initialization
        has already completed successfully for the app, there is
        not need to restart it due to a successful deactivate/unblock
        operation */
      if(perso_state == MMGSDI_PERSO_ENG_INIT_S)
      {
        if(app_info_ptr->perso_state !=
           MMGSDI_PERSO_ENG_INIT_COMPLETED_S)
        {
          app_info_ptr->perso_state = perso_state;
        }
      }
      else
      {
        app_info_ptr->perso_state = perso_state;
      }
    }

    notify_type.notify_type = MMGSDI_EVT_NOTIFY_ALL_SESSIONS;
    notify_type.slot_id     = mmgsdi_slot;
    /* Store the Personalization info in app_info_ptr,
      To be used for Late Clients */
    app_info_ptr->perso_info.feature             = feature;
    app_info_ptr->perso_info.num_retries         = num_retries;
    app_info_ptr->perso_info.num_unblock_retries = num_unblock_retries;
    /* Notify Session of the Perso event */
    mmgsdi_evt_build_and_send_perso(
      notify_type,
      (const mmgsdi_int_app_info_type*)app_info_ptr,
      status,
      feature,
      num_retries,
      num_unblock_retries);
  }

  /* If a sanity error has happened, clean up all Sessions and send
     out CARD_ERROR */
  if(status == MMGSDI_PERSO_STATUS_SANITY_ERROR)
  {
    /* If the Perso Sanity error is caused due to a generic failure or due to a
            Slot1 specific issue, send out a CARD_ERROR */
    if((send_to_all_prov) || (mmgsdi_slot == MMGSDI_SLOT_1))
    {
      slot_data_ptr = mmgsdi_util_get_slot_data_ptr(MMGSDI_SLOT_1);

      if(slot_data_ptr == NULL)
      {
        return FALSE;
      }

      /* If card is present on slot 1 */
      if(slot_data_ptr->mmgsdi_state == MMGSDI_STATE_CARD_INSERTED)
      {
        /* Check if the slot does not support hotswap and
           power down the card as we are sending card error indication */
        if(!uim_is_hotswap_enabled_slot(UIM_SLOT_1))
        {
          (void)mmgsdi_card_pdown(mmgsdi_generic_data_ptr->client_id,
                                  MMGSDI_SLOT_1,
                                  mmgsdi_util_internal_cb,
                                  MMGSDI_CARD_POWER_DOWN_ONLY,
                                  0);
        }
        slot_data_ptr->card_error_condition = MMGSDI_CARD_ERR_PWR_DN_PERSO_ERR;
        mmgsdi_evt_build_and_send_card_error(TRUE, 0, MMGSDI_SLOT_1);

        /* Update the session closed cause. The session changed event
           broadcasts the cause */
        mmgsdi_util_update_session_closed_cause_for_slot(MMGSDI_SLOT_1, MMGSDI_SESSION_CLOSED_CAUSE_PERSO_OPERATION_FAIL);
        mmgsdi_util_close_all_sessions_cleanup_and_notify(MMGSDI_SLOT_1);
        mmgsdi_util_reset_app_data(FALSE,MMGSDI_SLOT_1);
      }
    }

    /* If the Perso Sanity error is caused due to a generic failure or due to a
            Slot2 specific issue, send out a CARD_ERROR */
    if((send_to_all_prov) || (mmgsdi_slot == MMGSDI_SLOT_2))
    {
      slot_data_ptr = mmgsdi_util_get_slot_data_ptr(MMGSDI_SLOT_2);

      if(slot_data_ptr == NULL)
      {
        return FALSE;
      }

      /* If card is present on slot 2 */
      if(slot_data_ptr->mmgsdi_state == MMGSDI_STATE_CARD_INSERTED)
      {
        /* Check if the slot does not support hotswap and
           power down the card as we are sending card error indication */
        if(!uim_is_hotswap_enabled_slot(UIM_SLOT_2))
        {
          (void)mmgsdi_card_pdown(mmgsdi_generic_data_ptr->client_id,
                                  MMGSDI_SLOT_2,
                                  mmgsdi_util_internal_cb,
                                  MMGSDI_CARD_POWER_DOWN_ONLY,
                                  0);
        }
        slot_data_ptr->card_error_condition = MMGSDI_CARD_ERR_PWR_DN_PERSO_ERR;
        mmgsdi_evt_build_and_send_card_error(TRUE, 0, MMGSDI_SLOT_2);

        /* Update the session closed cause. The session changed event
           broadcasts the cause */
        mmgsdi_util_update_session_closed_cause_for_slot(MMGSDI_SLOT_2, MMGSDI_SESSION_CLOSED_CAUSE_PERSO_OPERATION_FAIL);
        mmgsdi_util_close_all_sessions_cleanup_and_notify(MMGSDI_SLOT_2);
        mmgsdi_util_reset_app_data(FALSE,MMGSDI_SLOT_2);
      }
    }

    /* If the Perso Sanity error is caused due to a generic failure or due to a
            Slot3 specific issue, send out a CARD_ERROR */
    if((send_to_all_prov) || (mmgsdi_slot == MMGSDI_SLOT_3))
    {
      slot_data_ptr = mmgsdi_util_get_slot_data_ptr(MMGSDI_SLOT_3);

      if(slot_data_ptr == NULL)
      {
        return FALSE;
      }

      /* If card is present on slot 3 */
      if(slot_data_ptr->mmgsdi_state == MMGSDI_STATE_CARD_INSERTED)
      {
        /* Check if the slot does not support hotswap and
           power down the card as we are sending card error indication */
        if(!uim_is_hotswap_enabled_slot(UIM_SLOT_3))
        {
          (void)mmgsdi_card_pdown(mmgsdi_generic_data_ptr->client_id,
                                  MMGSDI_SLOT_3,
                                  mmgsdi_util_internal_cb,
                                  MMGSDI_CARD_POWER_DOWN_ONLY,
                                  0);
        }
        mmgsdi_evt_build_and_send_card_error(TRUE, 0, MMGSDI_SLOT_3);
        mmgsdi_util_close_all_sessions_cleanup_and_notify(MMGSDI_SLOT_3);
        mmgsdi_util_reset_app_data(FALSE,MMGSDI_SLOT_3);
      }
    }
  }
  return TRUE;
}/* gsdi_perso_engine_send_mmgsdi_event */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_NOTIFY_CLIENTS_OF_PERSO_EVENT

DESCRIPTION
  This function is used to notify the clients of any Personalization
  Events.

DEPENDENCIES
  None

RETURN VALUE
  void
===========================================================================*/
void gsdi_perso_engine_notify_clients_of_perso_event(
  mmgsdi_slot_id_enum_type    slot,
  gsdi_perso_event_enum_type  event,
  mmgsdi_int_app_info_type   *app_info_ptr,
  boolean                     send_to_all_prov
)
{
  UIM_MSG_HIGH_1("PERSO: Notifying Perso Client 0x%x", event);
  /* If no valid event provided, return */
  if(event == GSDI_PERSO_NO_EVENT)
  {
    UIM_MSG_ERR_0("No valid event provided");
    return;
  }

  /* Send out the MMGSDI Perso event */
  (void)gsdi_perso_engine_send_mmgsdi_event(event, slot, app_info_ptr, send_to_all_prov);
} /* gsdi_perso_engine_notify_clients_of_perso_event */

/* =============================================================================
   FUNCTION:      GSDI_PERSO_ENGINE_GET_FEATURE_OFFSET

   DESCRIPTION:
     This function provides the offset for a Personalization feature

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type

   SIDE EFFECTS:
     None
=============================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_get_feature_offset(
  mmgsdi_perso_feature_enum_type  feature_ind,
  uint8                          *offset_ptr
)
{
  MMGSDIUTIL_RETURN_IF_NULL(offset_ptr);

  switch (feature_ind)
  {
#ifdef FEATURE_PERSO_SIM
    case MMGSDI_PERSO_NW:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_NW_OFFSET;
      break;

    case MMGSDI_PERSO_NS:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_NS_OFFSET;
      break;

    case MMGSDI_PERSO_SP:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_SP_OFFSET;
      break;

    case MMGSDI_PERSO_CP:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_CP_OFFSET;
      break;

    case MMGSDI_PERSO_SIM:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_SIM_OFFSET;
      break;
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
    case MMGSDI_PERSO_RUIM_NW1:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_NW1_OFFSET;
      break;

    case MMGSDI_PERSO_RUIM_NW2:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_NW2_OFFSET;
      break;

    case MMGSDI_PERSO_RUIM_HRPD:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_HRPD_OFFSET;
      break;

    case MMGSDI_PERSO_RUIM_SP:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_SP_OFFSET;
      break;

    case MMGSDI_PERSO_RUIM_CP:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_CP_OFFSET;
      break;

    case MMGSDI_PERSO_RUIM_RUIM:
      *offset_ptr = GSDI_PERSO_SEC_IND_OFFSET_RUIM_RUIM_OFFSET;
      break;
#endif /* FEATURE_PERSO_RUIM */
    default:
      UIM_MSG_ERR_1("gsdi_perso_engine_get_feature_offset: Invalid Feature Indicator 0x%x",
                    feature_ind);
      return MMGSDI_INCORRECT_PARAMS;
  } /* switch */
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_feature_offset */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_INITIALIZE_DATA

DESCRIPTION
  This function is called after the Verification procedures have occurred on
  PIN1.
  It will set gsdi_data.perso_ability to GSDI_PERSO_NOT_APPLICABLE,
  MMGSDI_PERSO_SIM_ALLOWED, or  MMGSDI_PERSO_RUIM_ALLOWED, depending on the
  active application and card capability.


DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_initialize_data(
  mmgsdi_session_id_type    session_id
)
{
  mmgsdi_int_app_info_type  *app_info_ptr= NULL;
  mmgsdi_slot_id_enum_type  slot         = MMGSDI_MAX_SLOT_ID_ENUM;
  mmgsdi_return_enum_type   mmgsdi_status= MMGSDI_ERROR;

  /* Base on the session ID, retrieve the prov index and slot */
  mmgsdi_status = mmgsdi_util_get_prov_session_info(session_id,
                                                    NULL,
                                                    &slot,
                                                    &app_info_ptr);

  if((mmgsdi_status != MMGSDI_SUCCESS) || (app_info_ptr == NULL))
  {
    return MMGSDI_ERROR;
  }

  /* Initialize gsdi perso ability to no perso allowed  */
  app_info_ptr->perso_ability    = MMGSDI_PERSO_NOT_APPLICABLE;

  /* --------------------------------------------------------------------------
     If there is a CDMA Application, run the CDMA get Application.
     ------------------------------------------------------------------------*/
  if(app_info_ptr->app_data.app_type  == MMGSDI_APP_RUIM)
  {
#ifdef FEATURE_PERSO_RUIM
    UIM_MSG_HIGH_0("PERSO: GET RUIM Capabilities");
    mmgsdi_status = mmgsdi_icc_get_ruim_capabilities(session_id, slot);
    if (MMGSDI_SUCCESS == mmgsdi_status)
    {
      app_info_ptr->perso_ability |= MMGSDI_PERSO_RUIM_ALLOWED;
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_S;
    }
    else
    {
      UIM_MSG_ERR_1("Perso init data: RUIM get cap failed,status=0x%x",
                    mmgsdi_status);
      return MMGSDI_ERROR;
    }
#else
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
#endif /* FEATURE_PERSO_RUIM */
    return MMGSDI_SUCCESS;
  }

  /* --------------------------------------------------------------------------
     If there is a CDMA Application, run the CDMA get Application.
     ------------------------------------------------------------------------*/
  if(app_info_ptr->app_data.app_type  == MMGSDI_APP_CSIM)
  {
#ifdef FEATURE_PERSO_RUIM
    UIM_MSG_HIGH_0("PERSO: GET CSIM Capabilities");
    mmgsdi_status = mmgsdi_uicc_get_capabilities(session_id,slot,MMGSDI_APP_CSIM);
    if (MMGSDI_SUCCESS == mmgsdi_status)
    {
      app_info_ptr->perso_ability |= MMGSDI_PERSO_RUIM_ALLOWED;
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_S;
    }
    else
    {
      UIM_MSG_ERR_1("Perso init data: CSIM get cap failed,status=0x%x",
                    mmgsdi_status);
      return MMGSDI_ERROR;
    }
#else
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
#endif /* FEATURE_PERSO_RUIM */
    return MMGSDI_SUCCESS;
  }

  /* --------------------------------------------------------------------------
     If there is a USIM application, run the USIM Get Aplications
     Do not run the GSM Get Capabilities if available
     as it is mutually exclusive with the USIM application.
     ------------------------------------------------------------------------*/
  if(app_info_ptr->app_data.app_type  == MMGSDI_APP_USIM)
  {
#ifdef FEATURE_PERSO_SIM
    UIM_MSG_HIGH_0("PERSO: GET USIM Capabilities");
    mmgsdi_status = mmgsdi_uicc_get_capabilities(session_id,slot,MMGSDI_APP_USIM);
    if (MMGSDI_SUCCESS == mmgsdi_status)
    {
      app_info_ptr->perso_ability |= MMGSDI_PERSO_SIM_ALLOWED;
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_S;
    }
    else
    {
      UIM_MSG_ERR_1("Perso init data: USIM get cap failed,status=0x%x",
                    mmgsdi_status);
      return MMGSDI_ERROR;
    }
#else
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
#endif /* FEATURE_PERSO_SIM */
    return MMGSDI_SUCCESS;
  }

  /* --------------------------------------------------------------------------
     If there is a GSM Application, run the GSM get Application.
     ------------------------------------------------------------------------*/
  if(app_info_ptr->app_data.app_type  == MMGSDI_APP_SIM)
  {
#ifdef FEATURE_PERSO_SIM
    UIM_MSG_HIGH_0("PERSO: GET SIM Capabilities");
    mmgsdi_status = mmgsdi_icc_get_sim_capabilities(session_id, slot);
    if (MMGSDI_SUCCESS == mmgsdi_status)
    {
      app_info_ptr->perso_ability |= MMGSDI_PERSO_SIM_ALLOWED;
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_S;
    }
    else
    {
      UIM_MSG_ERR_1("Perso init data: SIM get cap failed,status=0x%x",
                    mmgsdi_status);
      return MMGSDI_ERROR;
    }
#else
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
#endif /* FEATURE_PERSO_SIM */
    return MMGSDI_SUCCESS;
  }

  return MMGSDI_ERROR;
} /* gsdi_perso_engine_initialize_data */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_IMEI

DESCRIPTION:
  Will retrieve the IMEI number from NV.

DEPENDENCIES:
  None

LIMITATIONS:
  The memory allocated in this function will need to be released by the
  caller of this function.

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
mmgsdi_return_enum_type gsdi_perso_engine_get_imei (
  boolean  parse,
  uint8   *imei_data_ptr,
  size_t   imei_buf_size,
  uint32  *imei_len_ptr
)
{
  nv_item_type       gsdi_nv_data_buf;             /* NV data buffer */
  nv_stat_enum_type  nv_status         = NV_DONE_S;
  uint8              dec_index         = 0;
  uint8              bcd_index         = 0;

  /* ------------------------------------------------------------------------
    Is the IMEI Provided a valid IMEI
    ----------------------------------------------------------------------*/
  nv_status = mmgsdi_nv_get_item(NV_UE_IMEI_I,
                                 &gsdi_nv_data_buf);

  if ( nv_status != NV_DONE_S )
  {
    *imei_len_ptr = 0;

    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not get IMEI");
    return MMGSDI_SUCCESS;
  }

  if ( imei_data_ptr == NULL )
  {
    *imei_len_ptr = 0;

    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not get IMEI");
    return MMGSDI_SUCCESS;
  }

  if ( gsdi_nv_data_buf.ue_imei.ue_imei[0] == 0x00 ||
       gsdi_nv_data_buf.ue_imei.ue_imei[0] == 0xFF ||
       gsdi_nv_data_buf.ue_imei.ue_imei[0] > GSDI_PERSO_MAX_IMEI_LEN )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: IMEI is not set or improperly set");
    return MMGSDI_ERROR;
  }

  if((!parse && (imei_buf_size < GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN)) ||
     (parse && (imei_buf_size < (GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN - 1) * 2 - 1) &&
      (imei_buf_size < gsdi_nv_data_buf.ue_imei.ue_imei[0] * 2)))
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: insufficient memory for IMEI data");
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Check the Parsing function to determine whether or not we need to use
     the parsed IMEI or the non parsed IMEI
     ------------------------------------------------------------------------*/
  if ( !parse )
  {
    int i = 0;
    while ( i < GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN )
    {
      imei_data_ptr[i] = gsdi_nv_data_buf.ue_imei.ue_imei[i];
      i++;
    }
    *imei_len_ptr = GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN;
    return MMGSDI_SUCCESS;
  }

  bcd_index++;
  /* --------------------------------------------------------------------------
     Convert the IMEI from BCD To Decimal
     ------------------------------------------------------------------------*/
  while (( bcd_index <= gsdi_nv_data_buf.ue_imei.ue_imei[0]) &&
         ( bcd_index < GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN ))
  {
    /* Place High Byte in Index 0 (Even) Low Byte in Index 1 (Odd) ... */
    if ( dec_index % 2 == 0 )
    {
      imei_data_ptr[dec_index] = 0x30 +
            (((gsdi_nv_data_buf.ue_imei.ue_imei[bcd_index] &
               GSDI_MASK_FOR_HIGH_BYTE) >> 0x04) & 0x0F);
      dec_index++;
      bcd_index++;
    }
    else
    {
      /* Get the Low Byte of BCD  0x0N */
      imei_data_ptr[dec_index] = 0x30 +
        (gsdi_nv_data_buf.ue_imei.ue_imei[bcd_index] & GSDI_MASK_FOR_LOW_BYTE);
      dec_index++;
    }
  }
  *imei_len_ptr = dec_index;

  return MMGSDI_SUCCESS;
  /*lint +esym(613,imei_len_ptr)*/
} /* gsdi_perso_engine_get_imei */

#ifdef FEATURE_PERSO_SIM
/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_ENCRYPTED_DATA

DESCRIPTION:
  Will gather all of the information required to activate the feature
  indication provided.

  Information gather is as follows:

  1)  Secured / Encrypted Data - Personalization Keys
  2)  Secured / Encrypted Data - Feature Indicators
  3)  Secured / Encrypted Data - Depersonalization Key Status
  4)  Get the IMEI Value
  5)  Get the Mobile Serial Number

DEPENDENCIES:
  Based on the Personalization State and Self Personalization State.

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_encrypted_data(
  gsdi_perso_sec_me_data_type        * sec_data_ptr,
  gsdi_perso_engine_mem_pointer_type * mem_pointer_ptr,
  gsdi_perso_engine_proc_enum_type     feature_type,
  const void *                         req_ptr
)
{
  mmgsdi_return_enum_type  mmgsdi_status         = MMGSDI_ERROR;
  uint32                   file_size             = 0;
  uint8                   *file_buffer_ptr       = NULL;
  uint8                   *orig_file_buffer_ptr  = NULL;
  int                      i;
#if defined FEATURE_MMGSDI_PERSO_SFS && !defined FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  uint32                   sfs_size              = 0;
#endif /* FEATURE_MMGSDI_PERSO_SFS && !FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */

  /* --------------------------------------------------------------------------
     Perform Paramter checking
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(sec_data_ptr, mem_pointer_ptr);

  memset(&sec_data_ptr->feat_inds,         0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->perm_feat_inds,    0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->sim_lock_codes,    0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->dcks,              0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->dck_counts,        0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->unblock_cks,       0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->unblock_ck_counts, 0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->lock_down,         0x00, sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->me_imei,           0x00, sizeof(gsdi_perso_sec_data_type));

  /* --------------------------------------------------------------------------
     Using the Secure FS Interface, get the Perso Data.
     ------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  /* SFP is enabled */
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
    file_buffer_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE)
  MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);
  file_size = sfs_priv_simloc_read((uint8*)file_buffer_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE);
  if (file_size   == 0 )
#else
  /* SFP not enabled, fallback to SFS */
  mmgsdi_status = mmgsdi_sfs_get_size(&sfs_size, mmgsdi_sfs_perso_file);
  file_size = uint32toint32(sfs_size);
  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(file_buffer_ptr, file_size)
    MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);

    mmgsdi_status = mmgsdi_sfs_read(file_buffer_ptr,
                                    file_size,
                                    mmgsdi_sfs_perso_file);
  }
  if( mmgsdi_status != MMGSDI_SUCCESS ||
      file_size == 0)
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#else
  mmgsdi_status = gsdi_fs_get_file_size(mmgsdi_efs_perso_file,(int32*)&file_size);

  if ( mmgsdi_status != MMGSDI_SUCCESS ||
       file_size == 0 )
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: FILE Status: 0x%x File Size: 0x%x",
                             mmgsdi_status,file_size);
    return MMGSDI_ERROR;
  }

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(file_buffer_ptr, file_size)
  MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);

  mmgsdi_status =  gsdi_efs_read_file(mmgsdi_efs_perso_file,
                                      (int32*)&file_size,
                                      file_buffer_ptr);

  if ( mmgsdi_status != MMGSDI_SUCCESS ||
       file_size   == 0 )
#endif /*FEATURE_MMGSDI_PERSO_SFS*/
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: FS Status: 0x%x Read Size 0x%x",
                             mmgsdi_status,file_size);

    MMGSDIUTIL_TMC_MEM_FREE(file_buffer_ptr);
    return mmgsdi_status;
  }

  orig_file_buffer_ptr = file_buffer_ptr;
  for(i=0;i<GSDI_PERSO_ENGINE_NUMBER_OF_TAGS;i++)
  {
    uint8  tag         = 0;
    uint16 len         = 0;
    uint16 buffer_size = 0;

    /* Check if there is space for an additional perso tag */
    if (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE >
        file_size - (file_buffer_ptr - orig_file_buffer_ptr))
    {
      UIM_MSG_ERR_1("PERSO: Invalid Perso file (0x%x)",i);
      MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Extract tag and length */
    tag = *file_buffer_ptr;
    len = (*(file_buffer_ptr + 1) * GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT) + *(file_buffer_ptr + 2);

    /* Check if there is space for an additional perso tag */
    if (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE + len >
        file_size - (file_buffer_ptr - orig_file_buffer_ptr))
    {
      UIM_MSG_ERR_1("PERSO: Invalid Perso file (0x%x)",i);
      MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Check for integer overflow in buffer size calculation */
    if (mem_pointer_ptr->buffer_size - mem_pointer_ptr->curr_offset <
        GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE)
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Update size of destination buffer for copying perso data */
     buffer_size = mem_pointer_ptr->buffer_size -
                     (mem_pointer_ptr->curr_offset + GSDI_PERSO_ENGINE_LEN_SIZE +
                      GSDI_PERSO_ENGINE_TAG_SIZE);

    /*Switch on the Tags*/
    switch(tag)
    {
      case GSDI_PERSO_ENGINE_TAG_FEATURE_INDS:
        mem_pointer_ptr->curr_offset     += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                  += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->feat_inds.buffer_ptr= mem_pointer_ptr->buffer_start +
                                              mem_pointer_ptr->curr_offset +
                                              GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->feat_inds.len       = len;

        mmgsdi_memscpy(sec_data_ptr->feat_inds.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->feat_inds.len);

        mem_pointer_ptr->curr_offset     += GSDI_PERSO_ENGINE_LEN_SIZE +
                                            sec_data_ptr->feat_inds.len;
        file_buffer_ptr                  += GSDI_PERSO_ENGINE_LEN_SIZE +
                                            sec_data_ptr->feat_inds.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_PERM_FEATURE_INDS:
        mem_pointer_ptr->curr_offset         += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                      += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->perm_feat_inds.buffer_ptr = mem_pointer_ptr->buffer_start +
                                                mem_pointer_ptr->curr_offset +
                                                GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->perm_feat_inds.len      =  len;

        mmgsdi_memscpy(sec_data_ptr->perm_feat_inds.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr +GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->perm_feat_inds.len);

        mem_pointer_ptr->curr_offset         += GSDI_PERSO_ENGINE_LEN_SIZE +
                                                sec_data_ptr->perm_feat_inds.len;
        file_buffer_ptr                      += GSDI_PERSO_ENGINE_LEN_SIZE +
                                                sec_data_ptr->perm_feat_inds.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_SIM_LOCK_CODE:
        mem_pointer_ptr->curr_offset         += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                      += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->sim_lock_codes.buffer_ptr = mem_pointer_ptr->buffer_start +
                                                mem_pointer_ptr->curr_offset +
                                                GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->sim_lock_codes.len      = len;

        mmgsdi_memscpy(sec_data_ptr->sim_lock_codes.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->sim_lock_codes.len);

        mem_pointer_ptr->curr_offset         += GSDI_PERSO_ENGINE_LEN_SIZE +
                                                sec_data_ptr->sim_lock_codes.len;

        /* Check to see if new SIM Lock Codes have to be generated*/
        if(feature_type == GSDI_PERSO_ENGINE_AUTO_LOCK)
        {
          /* allocate Enough memory for any additional codes that may be required to be genereated*/
           mem_pointer_ptr->curr_offset      += GSDI_PERSO_BUFFER_LEN -
                                                sec_data_ptr->sim_lock_codes.len;
        }
        else if(feature_type == GSDI_PERSO_ENGINE_SET_DATA_FEATURE)
        {
          if(req_ptr == NULL)
          {
            MMGSDIUTIL_DEBUG_MSG_ERROR_0("Attempt to use NULL POINTER");
            MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
            return MMGSDI_INCORRECT_PARAMS;
          }
          /* Check for integer overflow */
          if(((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes > MMGSDI_INT32_MAX / 2 ||
             ((uint32)mem_pointer_ptr->curr_offset) > MMGSDI_UINT32_MAX - ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2)
          {
            MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
            return MMGSDI_INCORRECT_PARAMS;
          }
          mem_pointer_ptr->curr_offset += ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2;
        }
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->sim_lock_codes.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_DEPERSO_CK:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr              += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->dcks.buffer_ptr   =  mem_pointer_ptr->buffer_start +
                                         mem_pointer_ptr->curr_offset +
                                         GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->dcks.len        =  len;

        mmgsdi_memscpy(sec_data_ptr->dcks.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->dcks.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE +
                                        sec_data_ptr->dcks.len;
        file_buffer_ptr              += GSDI_PERSO_ENGINE_LEN_SIZE +
                                        sec_data_ptr->dcks.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_DCK_COUNTER:
        mem_pointer_ptr->curr_offset     += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                  += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->dck_counts.buffer_ptr =  mem_pointer_ptr->buffer_start +
                                               mem_pointer_ptr->curr_offset +
                                               GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->dck_counts.len      =  len;

        mmgsdi_memscpy(sec_data_ptr->dck_counts.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->dck_counts.len);

        mem_pointer_ptr->curr_offset     += GSDI_PERSO_ENGINE_LEN_SIZE +
                                            sec_data_ptr->dck_counts.len;
        file_buffer_ptr                  += GSDI_PERSO_ENGINE_LEN_SIZE +
                                            sec_data_ptr->dck_counts.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_UNBLOCK:
        mem_pointer_ptr->curr_offset      += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                   += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->unblock_cks.buffer_ptr =  mem_pointer_ptr->buffer_start +
                                              mem_pointer_ptr->curr_offset +
                                              GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->unblock_cks.len      =  len;

        mmgsdi_memscpy(sec_data_ptr->unblock_cks.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->unblock_cks.len);

        mem_pointer_ptr->curr_offset      += GSDI_PERSO_ENGINE_LEN_SIZE +
                                             sec_data_ptr->unblock_cks.len;
        file_buffer_ptr                   += GSDI_PERSO_ENGINE_LEN_SIZE +
                                             sec_data_ptr->unblock_cks.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_UNBLOCK_COUNTER:
        mem_pointer_ptr->curr_offset            += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr                         += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->unblock_ck_counts.buffer_ptr =  mem_pointer_ptr->buffer_start +
                                                      mem_pointer_ptr->curr_offset +
                                                      GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->unblock_ck_counts.len      = len;

        mmgsdi_memscpy(sec_data_ptr->unblock_ck_counts.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->unblock_ck_counts.len);

        mem_pointer_ptr->curr_offset            += GSDI_PERSO_ENGINE_LEN_SIZE +
                                                   sec_data_ptr->unblock_ck_counts.len;
        file_buffer_ptr                         += GSDI_PERSO_ENGINE_LEN_SIZE +
                                                   sec_data_ptr->unblock_ck_counts.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_LOCK_DOWN:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->lock_down.buffer_ptr = mem_pointer_ptr->buffer_start +
                                             mem_pointer_ptr->curr_offset +
                                             GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->lock_down.len        = len;

        mmgsdi_memscpy(sec_data_ptr->lock_down.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr +GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->lock_down.len);
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->lock_down.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->lock_down.len;
        break;

      default:
        UIM_MSG_ERR_1("PERSO: Invalid Tag 0x%x",*file_buffer_ptr);
        MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
        return MMGSDI_ERROR;
    }

  /* --------------------------------------------------------------------------
     The operation was successfull. Need to update the current offset and
     ensure that curr_offset does not exceed buffer_size.
     ------------------------------------------------------------------------*/
    if ( mem_pointer_ptr->curr_offset > mem_pointer_ptr->buffer_size )
    {
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: MEMORY POINTER USAGE ERROR");
      MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }
  }

  MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);

  /* --------------------------------------------------------------------------
     Using the NV Items Get IMEI Value
     ------------------------------------------------------------------------*/
  sec_data_ptr->me_imei.buffer_ptr = mem_pointer_ptr->buffer_start + mem_pointer_ptr->curr_offset;

#ifndef FEATURE_UIM_TEST_FRAMEWORK
  mmgsdi_status = gsdi_perso_engine_get_imei(TRUE,
                                             sec_data_ptr->me_imei.buffer_ptr,
                                             mem_pointer_ptr->buffer_size - mem_pointer_ptr->curr_offset,
                                             (uint32 *)&sec_data_ptr->me_imei.len);
  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: PERSO ENGINE Get IMEI Failure: 0x%x",
                             mmgsdi_status);
    return mmgsdi_status;
  }
#else
  #error code not present
#endif /*FEATURE_UIM_TEST_FRAMEWORK*/

  /* --------------------------------------------------------------------------
     The operation was successfull. Need to update the current offset and
     ensure that curr_offset does not exceed buffer_size.
     ------------------------------------------------------------------------*/
  if ( (mem_pointer_ptr->buffer_size - mem_pointer_ptr->curr_offset) <
         sec_data_ptr->me_imei.len )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: MEMORY POINTER USAGE ERROR");
    return MMGSDI_ERROR;
  }

  mem_pointer_ptr->curr_offset += sec_data_ptr->me_imei.len;

  return MMGSDI_SUCCESS;
}/*gsdi_perso_engine_get_encrypted_data*/


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_WRITE_DCK

DESCRIPTION
  This is the generic wrapper function used to write data to the SIM.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_write_dck(
  mmgsdi_session_id_type   session_id,
  mmgsdi_slot_id_enum_type slot_id,
  mmgsdi_app_enum_type     app_type
)
{
  mmgsdi_write_req_type             * write_req_ptr      = NULL;
  mmgsdi_write_cnf_type             * write_cnf_ptr      = NULL;
  mmgsdi_client_id_type               client_id          = 0;
  mmgsdi_uim_additional_params_type   add_params;
  mmgsdi_return_enum_type             mmgsdi_status      = MMGSDI_ERROR;

  mmgsdi_status = mmgsdi_util_get_client_id(session_id,
                                            &client_id);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return MMGSDI_INCORRECT_PARAMS;
  }

  /* --------------------------------------------------------------------------
     Allocate Memory for the Write Request
  ------------------------------------------------------------------------*/
  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(write_req_ptr,
                                         sizeof(mmgsdi_write_req_type),
                                         mmgsdi_status);

  if((mmgsdi_status != MMGSDI_SUCCESS) ||
     (write_req_ptr == NULL))
  {
    UIM_MSG_ERR_0("PERSO: Could not allocate memory for the write Request");
    return MMGSDI_ERROR;
  }
  /* --------------------------------------------------------------------------
     Build the write request based on the information provided in the
     call.
     ------------------------------------------------------------------------*/
  write_req_ptr->request_header.client_id         = client_id;
  write_req_ptr->request_header.session_id        = session_id;
  write_req_ptr->request_header.request_type      = MMGSDI_WRITE_REQ;
  write_req_ptr->request_header.orig_request_type = MMGSDI_WRITE_REQ;
  write_req_ptr->request_header.slot_id           = slot_id;
  write_req_ptr->request_header.client_data       = 0;
  write_req_ptr->request_header.response_cb       = 0;
  write_req_ptr->access.access_method             = MMGSDI_EF_ENUM_ACCESS;
  write_req_ptr->data.data_len                    = GSDI_PERSO_ENGINE_DCK_CK_LEN * 4;
  write_req_ptr->int_client_data                  = 0;


  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(write_req_ptr->data.data_ptr,
                                         write_req_ptr->data.data_len,
                                         mmgsdi_status);

  if((mmgsdi_status != MMGSDI_SUCCESS) ||
     (write_req_ptr->data.data_ptr == NULL))
  {
    UIM_MSG_ERR_0("Perso: Allocation Failed");
    MMGSDIUTIL_TMC_MEM_FREE(write_req_ptr);
    return MMGSDI_ERROR;
  }

  memset(write_req_ptr->data.data_ptr,0xFF, write_req_ptr->data.data_len);

  /* --------------------------------------------------------------------------
     Determine which file should be written and write to it
     ------------------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    write_req_ptr->access.file.file_enum = MMGSDI_USIM_DCK;
    memset(&add_params, 0x00, sizeof(mmgsdi_uim_additional_params_type));

    mmgsdi_status = mmgsdi_uim_uicc_write_transparent(
                     (const mmgsdi_write_req_type*)(write_req_ptr),
                     &add_params,
                     TRUE,
                     &write_cnf_ptr);
  }
  else
  {
    write_req_ptr->access.file.file_enum = MMGSDI_GSM_DCK;

    mmgsdi_status = mmgsdi_uim_icc_write_transparent(
                     (const mmgsdi_write_req_type*)(write_req_ptr),
                     TRUE,
                     &write_cnf_ptr);
  }

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not clear EF-DCK ");
  }
  else
  {
    MMGSDIUTIL_TMC_MEM_FREE(write_cnf_ptr);
  }
  MMGSDIUTIL_TMC_MEM_FREE(write_req_ptr->data.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE(write_req_ptr);

  return mmgsdi_status;
}/*gsdi_perso_engine_write_dck*/


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_READ_AND_FORMAT_IMSI

DESCRIPTION
  This function will be called to read the IMSI and convert it to a usable
  format.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_read_and_format_imsi(
  mmgsdi_session_id_type       session_id,
  mmgsdi_app_enum_type         app_type,
  mmgsdi_slot_id_enum_type     slot,
  mmgsdi_data_type            *imsi_ptr
)
{
  byte                     digit1            = 0;
  byte                     digit2            = 0;
  uint8                    num_mnc_digits    = 0;
  mmgsdi_file_enum_type    file_name         = MMGSDI_MAX_FILE_ENUM;
  mmgsdi_data_type        *data_ptr          = NULL;
  mmgsdi_return_enum_type  mmgsdi_status     = MMGSDI_ERROR;
  uint8                    imsi_buffer[GSDI_PERSO_ENGINE_IMSI_LEN];

  MMGSDIUTIL_RETURN_IF_NULL(imsi_ptr);

  memset(imsi_buffer, 0, GSDI_PERSO_ENGINE_IMSI_LEN);
  /* --------------------------------------------------------------------------
     Read the Administrative Data Elementary File
     ------------------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    file_name = MMGSDI_USIM_AD;
  }
  else
  {
    file_name = MMGSDI_GSM_AD;
  }

  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(data_ptr,
                                         sizeof(mmgsdi_data_type),
                                         mmgsdi_status);
  if((mmgsdi_status != MMGSDI_SUCCESS) || (data_ptr == NULL))
  {
    UIM_MSG_ERR_0("Failed to Allocate Memory");
    return MMGSDI_ERROR;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_type,
                                                slot,
                                                file_name,
                                                data_ptr);

  if ( mmgsdi_status != MMGSDI_SUCCESS &&
       mmgsdi_status != MMGSDI_NOT_SUPPORTED )
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(data_ptr->data_ptr);
    MMGSDIUTIL_TMC_MEM_FREE(data_ptr);
    UIM_MSG_ERR_1("PERSO: read AD failed or not supported 0x%x",
                  mmgsdi_status);
    return MMGSDI_NOT_SUPPORTED;
  }

  mmgsdi_status = mmgsdi_util_get_num_of_mnc_digits(
                    *imsi_ptr, *data_ptr, &num_mnc_digits);

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(data_ptr->data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE(data_ptr);

  if ( mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_0("get num of mnc digits from EF-AD: failed");
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Now format the IMSI from:
     B1       B2       B3       B4     B5   B6   B7    B8
     MCC1PB   MCC3MCC2 MNC2MNC1 D1MNC3 D3D2 D5D4 D7D6 D9D8

     TO (2 Digit MNC)
     B1       B2       B3       B4    B5   B6   B7    B8
     MCC2MCC1 0xFMCC3 MNC2MNC1 D1MNC3 D3D2 D5D4 D7D6 D9D8

     OR (3 Digit MNC)
     B1       B2       B3       B4    B5   B6   B7    B8
     MCC2MCC1 MNC3MCC3 MNC2MNC1 D2D1 D4D3 D6D5 D8D7 0xFD9
     ------------------------------------------------------------------------*/
  digit1 = imsi_ptr->data_ptr[GSDI_PERSO_ENG_IMSI_DIGIT_1_OFFSET] & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
  digit2 = imsi_ptr->data_ptr[GSDI_PERSO_ENG_IMSI_DIGIT_2_OFFSET] & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
  digit1 = (digit1 >> 4) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
  digit2 = (digit2 << 4) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
  imsi_buffer[0] = digit1 | digit2;

  digit1 = imsi_ptr->data_ptr[GSDI_PERSO_ENG_IMSI_DIGIT_3_OFFSET] & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
  digit1 = (digit1 >> 4) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
  if ( num_mnc_digits == MMGSDIUTIL_MNC_2_DIGITS )
  {
    digit2 = 0xF0;
    imsi_buffer[1] = digit1 | digit2;

    imsi_buffer[2] = imsi_ptr->data_ptr[3];
    imsi_buffer[3] = imsi_ptr->data_ptr[4];
    imsi_buffer[4] = imsi_ptr->data_ptr[5];
    imsi_buffer[5] = imsi_ptr->data_ptr[6];
    imsi_buffer[6] = imsi_ptr->data_ptr[7];
    imsi_buffer[7] = imsi_ptr->data_ptr[8];
  }
  else
  {
    digit2  = imsi_ptr->data_ptr[GSDI_PERSO_ENG_IMSI_DIGIT_6_OFFSET] & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = (digit2 << 4) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
    imsi_buffer[1] = digit1 | digit2;

    imsi_buffer[2] = imsi_ptr->data_ptr[GSDI_PERSO_ENG_IMSI_DIGITS_4_5_OFFSET];

    digit1 = ( imsi_ptr->data_ptr[4] >> 4 ) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = ( imsi_ptr->data_ptr[5] << 4 ) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
    imsi_buffer[3] = digit1 | digit2;

    digit1 = ( imsi_ptr->data_ptr[5] >> 4 ) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = ( imsi_ptr->data_ptr[6] << 4 ) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
    imsi_buffer[4] = digit1 | digit2;

    digit1 = ( imsi_ptr->data_ptr[6] >> 4 ) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = ( imsi_ptr->data_ptr[7] << 4 ) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
    imsi_buffer[5] = digit1 | digit2;

    digit1 = ( imsi_ptr->data_ptr[7] >> 4 ) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = ( imsi_ptr->data_ptr[8] << 4 ) & GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK;
    imsi_buffer[6] = digit1 | digit2;

    digit1 = ( imsi_ptr->data_ptr[8] >> 4 ) & GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK;
    digit2 = 0xF0;
    imsi_buffer[7] = digit1 | digit2;

  }

  mmgsdi_memscpy(imsi_ptr->data_ptr,
                 int32touint32(imsi_ptr->data_len),
                 imsi_buffer,
                 GSDI_PERSO_ENGINE_IMSI_LEN - 1);

  imsi_ptr->data_len = 8;

  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_read_and_format_imsi */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_SIM_DATA

DESCRIPTION:
  Will gather all of the information from the SIM/USIM/RUIM inserted.
  It will retrieve:
  EF-IMSI
  EF-CNL
  EF-GID1
  EF-GID2

DEPENDENCIES:
  Card must be inserted before being called and the PIN Must be verified.

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_sim_data(
  mmgsdi_session_id_type               session_id,
  mmgsdi_app_enum_type                 app_type,
  mmgsdi_slot_id_enum_type             slot,
  gsdi_perso_sec_sim_data_type        *sim_data_ptr,
  gsdi_perso_engine_proc_enum_type     feature_type
)
{
  mmgsdi_file_enum_type      file_name     = MMGSDI_MAX_FILE_ENUM;
  mmgsdi_return_enum_type    mmgsdi_status = MMGSDI_SUCCESS;

  /* --------------------------------------------------------------------------
     Perform Validation Checks
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(sim_data_ptr);

  /* ---------------------------------------------------------------
     Get the DCK information from the SIM/USIM only if request type is refresh
     -------------------------------------------------------------*/
  if(feature_type == GSDI_PERSO_ENGINE_REFRESH)
  {
    if(app_type == MMGSDI_APP_USIM)
    {
      file_name = MMGSDI_USIM_DCK;
    }
    else
    {
      file_name = MMGSDI_GSM_DCK;
    }

    mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                  app_type,
                                                  slot,
                                                  file_name,
                                                  &sim_data_ptr->dck);

    if ( mmgsdi_status != MMGSDI_SUCCESS)
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->dck.data_ptr);
      sim_data_ptr->dck.data_len  = 0;
      sim_data_ptr->dck.data_ptr = NULL;
    }
    return MMGSDI_SUCCESS;
  }

  /* --------------------------------------------------------------------------
     Get the IMSI information from the SIM/USIM
     If retrieval of the IMSI information fails,
     the sec_data_ptr->buffer_ptr memory allocated for
     this request will already be freed.
     ------------------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    file_name = MMGSDI_USIM_IMSI;
  }
  else
  {
    file_name = MMGSDI_GSM_IMSI;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_type,
                                                slot,
                                                file_name,
                                                &sim_data_ptr->imsi);

  if ( mmgsdi_status != MMGSDI_SUCCESS &&
       mmgsdi_status != MMGSDI_NOT_SUPPORTED )
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->imsi.data_ptr);
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: IMSI read failed 0x%x",mmgsdi_status);
    return MMGSDI_ERROR;
  }

  mmgsdi_status = gsdi_perso_engine_read_and_format_imsi(session_id,
                                                         app_type,
                                                         slot,
                                                        &sim_data_ptr->imsi);

  if ( mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->imsi.data_ptr);
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Read and format IMSI failed 0x%x",
                             mmgsdi_status);
    return mmgsdi_status;
  }

  /* ---------------------------------------------------------------
     Get the CNL information from the SIM/USIM
    -------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    file_name = MMGSDI_USIM_CNL;
  }
  else
  {
    file_name = MMGSDI_GSM_CNL;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_type,
                                                slot,
                                                file_name,
                                                &sim_data_ptr->cnl);
  if ( mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->cnl.data_ptr);
    sim_data_ptr->cnl.data_len = 0;
    sim_data_ptr->cnl.data_ptr = NULL;
  }

  /* ---------------------------------------------------------------
     Get the GID1 information from the SIM/USIM
     -------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    file_name = MMGSDI_USIM_GID1;
  }
  else
  {
    file_name = MMGSDI_GSM_GID1;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_type,
                                                slot,
                                                file_name,
                                                &sim_data_ptr->gid1);

  if ( mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->gid1.data_ptr);
    sim_data_ptr->gid1.data_len = 0;
    sim_data_ptr->gid1.data_ptr = NULL;
  }

 /* ---------------------------------------------------------------
     Get the GID2 information from the SIM/USIM
    -------------------------------------------------------------*/
  if(app_type == MMGSDI_APP_USIM)
  {
    file_name = MMGSDI_USIM_GID2;
  }
  else
  {
    file_name = MMGSDI_GSM_GID2;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_type,
                                                slot,
                                                file_name,
                                                &sim_data_ptr->gid2);

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data_ptr->gid2.data_ptr);
    sim_data_ptr->gid2.data_len = 0;
    sim_data_ptr->gid2.data_ptr = NULL;
  }

  return MMGSDI_SUCCESS;

} /* gsdi_perso_engine_get_sim_data */
#endif /* FEATURE_PERSO_SIM */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_MAP_TO_MASK

DESCRIPTION:
  Will gather all of the information from the SIM/USIM/RUIM inserted.
  It will retrieve:
  EF-IMSI
  EF-CNL
  EF-GID1
  EF-GID2

DEPENDENCIES:
  Card must be inserted before being called and the PIN Must be verified.

LIMITATIONS:
  None

RETURN TYPE:
  gsdi_returns_T

-----------------------------------------------------------------------------*/
static gsdi_returns_T gsdi_perso_engine_map_to_mask (
  gsdi_perso_enum_type              feature,
  gsdi_perso_sec_client_req_type *  client_data_ptr
)
{
  /* --------------------------------------------------------------------------
     validate the parmeters provided
     ------------------------------------------------------------------------*/
  MMGSDI_RETURN_IF_NULL(client_data_ptr);

  /* --------------------------------------------------------------------------
     Now set the Mask according to the feature
     ------------------------------------------------------------------------*/
  switch ( feature )
  {
#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_NW:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_NW;
      break;

    case GSDI_PERSO_NS:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_NS;
      break;

    case GSDI_PERSO_SP:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_SP;
      break;

    case GSDI_PERSO_CP:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_CP;
      break;

    case GSDI_PERSO_SIM:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_SIM;
      break;
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
    case GSDI_PERSO_RUIM_NW1:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW1;
      break;

    case GSDI_PERSO_RUIM_NW2:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW2;
      break;

    case GSDI_PERSO_RUIM_HRPD:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_HRPD;
      break;

    case GSDI_PERSO_RUIM_SP:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_SP;
      break;

    case GSDI_PERSO_RUIM_CP:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_CP;
      break;

    case GSDI_PERSO_RUIM_RUIM:
      client_data_ptr->feature_mask = GSDI_PERSO_SEC_MASK_FEAT_RUIM_RUIM;
      break;
#endif /* FEATURE_PERSO_RUIM */
    case GSDI_PERSO_NONE:
    default:
      MMGSDI_DEBUG_MSG_ERROR_1("PERSO: BAD FEATURE RECEIVED: 0x%x",feature);
      return GSDI_ERROR;
  }

  return GSDI_SUCCESS;
} /* gsdi_perso_engine_map_to_mask */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_MAP_LOCK_DOWN_TO_MASK

DESCRIPTION:
  Converts the enum gsdi_perso_lock_down_enum_type into the corresponding
  value in the gsdi_perso_sec_client_req_type structure.

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  gsdi_returns_T

-----------------------------------------------------------------------------*/
static gsdi_returns_T gsdi_perso_engine_map_lock_down_to_mask (
  gsdi_perso_lock_down_enum_type    command,
  gsdi_perso_sec_client_req_type *  client_data_ptr
)
{
  /* --------------------------------------------------------------------------
     validate the parmeters provided
     ------------------------------------------------------------------------*/
  MMGSDI_RETURN_IF_NULL(client_data_ptr);

  /* --------------------------------------------------------------------------
     Now set the Mask according to the feature
     ------------------------------------------------------------------------*/
  switch ( command )
  {
    case GSDI_PERSO_LOCK_DOWN_GET_FEATURE_KEY:
      client_data_ptr->lock_down = GSDI_PERSO_SEC_LOCK_DOWN_GET_FEATURE_KEY;
      break;

    default:
      MMGSDI_DEBUG_MSG_ERROR_1("PERSO: BAD COMMAND RECEIVED: 0x%x",command);
      return GSDI_ERROR;
  }

  return GSDI_SUCCESS;
} /* gsdi_perso_engine_map_lock_down_to_mask */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_DCK

DESCRIPTION:
  Will gather the client's depersonalization information from the request to
  the Security Library Structure.

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  gsdi_returns_T

-----------------------------------------------------------------------------*/
static gsdi_returns_T gsdi_perso_engine_get_dck (
  uint32                      len,
  const uint8 *               dck_ptr,
  gsdi_perso_sec_data_type   *client_data_ptr
)
{
  /* --------------------------------------------------------------------------
     Validate all parameters provided.
     ------------------------------------------------------------------------*/
  MMGSDI_RETURN_IF_NULL_2(client_data_ptr, dck_ptr);
  MMGSDI_RETURN_IF_NULL(client_data_ptr->buffer_ptr);
  MMGSDI_RETURN_IF_EXCEEDS(len,GSDI_PERSO_SEC_MAX_DCK_LEN);

  /* -------------------------------------------------------------------------
     Copy over the DCK and complete populating the client_data_ptr
     -----------------------------------------------------------------------*/
  mmgsdi_memscpy(client_data_ptr->buffer_ptr,
                 len,
                 dck_ptr,
                 len);

  client_data_ptr->len = len;

  return GSDI_SUCCESS;
} /* gsdi_perso_engine_get_dck */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_CLIENT_GET_FEATURE_DATA

DESCRIPTION:
  Copies the original req_ptr data into the client data structure

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  gsdi_returns_T

-----------------------------------------------------------------------------*/
static gsdi_returns_T gsdi_perso_engine_get_client_feature_data(
  uint32                    len,
  const uint8              *data_buffer,
  gsdi_perso_sec_data_type *client_data_ptr
)
{
 /* --------------------------------------------------------------------------
     Validate all parameters provided.
 ------------------------------------------------------------------------*/
  MMGSDI_RETURN_IF_NULL_2(client_data_ptr, data_buffer);
  MMGSDI_RETURN_IF_NULL(client_data_ptr->buffer_ptr);
  MMGSDI_RETURN_IF_EXCEEDS(len,GSDI_PERSO_MAX_DATA_LEN);

  client_data_ptr->len = len;
  mmgsdi_memscpy(client_data_ptr->buffer_ptr,
                 len,
                 data_buffer,
                 len);

  return GSDI_SUCCESS;
}/*gsdi_perso_engine_get_client_feature_data*/


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_CLIENT_DATA

DESCRIPTION:
  Will gather all information required for the request made by the
  client and put into a manner acceptable for the security Library.

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  gsdi_returns_T

-----------------------------------------------------------------------------*/
static gsdi_returns_T gsdi_perso_engine_get_client_data(
   gsdi_perso_engine_proc_enum_type           perso_proc,
   gsdi_perso_sec_client_req_type           * client_data_ptr,
   const gsdi_perso_engine_mem_pointer_type * mem_pointer,
   const void *                               req_ptr
)
{
  gsdi_returns_T gsdi_status = GSDI_SUCCESS;

  /* --------------------------------------------------------------------------
     Verify the parameters provided.
     ------------------------------------------------------------------------*/
  MMGSDI_RETURN_IF_NULL_3(client_data_ptr, req_ptr, mem_pointer);

  /* --------------------------------------------------------------------------
     Start transferring the information from the Request
     to the client data
     ------------------------------------------------------------------------*/
  switch ( perso_proc )
  {
    case GSDI_PERSO_ENGINE_ACTIVATE_FEATURE:
      /* Extract the Personalization Feature to activate */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                        ((gsdi_perso_act_feature_ind_req_T *)req_ptr)->feature,
                        client_data_ptr);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x",gsdi_status);
        return gsdi_status;
      }

      client_data_ptr->dck.buffer_ptr = mem_pointer->buffer_start;

      /* Extract the Depersonalization Control Keys */
      gsdi_status = gsdi_perso_engine_get_dck(
                        (uint32)((gsdi_perso_act_feature_ind_req_T *)req_ptr)->num_bytes,
                        ((gsdi_perso_act_feature_ind_req_T *)req_ptr)->ck_buffer,
                        &client_data_ptr->dck);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Deperso Keys 0x%x",gsdi_status);
        return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_UNBLOCK_DCK:
      /* Extract the Personalization Feature to deactivate */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                        ((gsdi_perso_unblock_feature_ind_req_T *)req_ptr)->feature,
                        client_data_ptr);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x",gsdi_status);
        return GSDI_ERROR;
      }

      /* Extract the Depersonalization Control Keys */
      client_data_ptr->dck.buffer_ptr = mem_pointer->buffer_start;

      gsdi_status = gsdi_perso_engine_get_dck(
                    (uint32)((gsdi_perso_unblock_feature_ind_req_T *)req_ptr)->num_bytes,
                    ((gsdi_perso_unblock_feature_ind_req_T *)req_ptr)->unblock_ck_buffer,
                    &client_data_ptr->dck);
      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Deperso Keys 0x%x",
                                 gsdi_status);
        return GSDI_ERROR;
      }
      break;

    case GSDI_PERSO_ENGINE_DEACTIVATE_FEATURE:
      /* Extract the Personalization Feature to deactivate */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                        ((gsdi_perso_deact_feature_ind_req_T *)req_ptr)->feature,
                        client_data_ptr);

      if ( gsdi_status != GSDI_SUCCESS )
      {
          MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x",
                                   gsdi_status);
          return gsdi_status;
      }

      /* Extract the Depersonalization Control Keys */
      client_data_ptr->dck.buffer_ptr = mem_pointer->buffer_start;

      gsdi_status = gsdi_perso_engine_get_dck(
                    (uint32)((gsdi_perso_deact_feature_ind_req_T *)req_ptr)->num_bytes,
                    ((gsdi_perso_deact_feature_ind_req_T *)req_ptr)->ck_buffer,
                    &client_data_ptr->dck);
      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Deperso Keys 0x%x",gsdi_status);
        return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_SET_DATA_FEATURE:
      /* Extract the Personalization Feature for the data that needs to be set */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                    ((gsdi_perso_set_feature_data_req_T *)req_ptr)->feature,
                    client_data_ptr);
      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x", gsdi_status);
        return gsdi_status;
      }

      client_data_ptr->feature_data.buffer_ptr = mem_pointer->buffer_start;

      /* Extract the Data to be Set */
      gsdi_status = gsdi_perso_engine_get_client_feature_data(
                    (uint32)((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes,
                    ((gsdi_perso_set_feature_data_req_T *)req_ptr)->data_buffer,
                    &client_data_ptr->feature_data);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Client Feature Data 0x%x",
                               gsdi_status);
        return gsdi_status;
      }

      break;

    case GSDI_PERSO_ENGINE_GET_FEATURE_DATA:
      /* Extract the Personalization Feature */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                    ((gsdi_perso_get_feature_data_req_T *)req_ptr)->feature,
                    client_data_ptr);
      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x",
                               gsdi_status);
        return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS:
      client_data_ptr->feature_mask =
#ifdef FEATURE_PERSO_SIM
      (int)GSDI_PERSO_SEC_MASK_FEAT_NW |
      (int)GSDI_PERSO_SEC_MASK_FEAT_NS |
      (int)GSDI_PERSO_SEC_MASK_FEAT_SP |
      (int)GSDI_PERSO_SEC_MASK_FEAT_CP |
      (int)GSDI_PERSO_SEC_MASK_FEAT_SIM |
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW1 |
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW2 |
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_HRPD|
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_SP |
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_CP |
      (int)GSDI_PERSO_SEC_MASK_FEAT_RUIM_RUIM|
#endif /* FEATURE_PERSO_RUIM */
      0; /*Do not remove, required because of featurization*/
      break;

    case GSDI_PERSO_ENGINE_AUTO_LOCK:
      client_data_ptr->feature_mask = ((gsdi_perso_internal_command_req_T *)req_ptr)->autolock_mask;
      break;

    case GSDI_PERSO_ENGINE_PERM_DISABLE_FEATURE_IND:
      /* Extract the Personalization Feature to disable */
      gsdi_status = gsdi_perso_engine_map_to_mask (
                       ((gsdi_perso_perm_disabled_feature_req_T *)req_ptr)->perso_feature,
                       client_data_ptr);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x",gsdi_status);
        return gsdi_status;
      }
      client_data_ptr->dck.buffer_ptr = mem_pointer->buffer_start;

      /* Extract the Depersonalization Control Keys */
      gsdi_status = gsdi_perso_engine_get_dck(
                        (uint32)((gsdi_perso_perm_disabled_feature_req_T *)req_ptr)->num_bytes,
                        ((gsdi_perso_perm_disabled_feature_req_T *)req_ptr)->ck_buffer,
                        &client_data_ptr->dck);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Deperso Keys 0x%x", gsdi_status);
        return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_GET_FEATURE_KEY:
      gsdi_status = gsdi_perso_engine_map_to_mask (
                    ((gsdi_perso_get_feature_key_req_T *)req_ptr)->feature,
                    client_data_ptr);

      client_data_ptr->get_unblock_key = ((gsdi_perso_get_feature_key_req_T *)req_ptr)->is_unblock_key;

      if ( gsdi_status != GSDI_SUCCESS )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Map Feature 0x%x", gsdi_status);
        return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_LOCK_DOWN:
      gsdi_status = gsdi_perso_engine_map_lock_down_to_mask (
                    ((gsdi_perso_lock_down_req_T *)req_ptr)->locked_command,
                    client_data_ptr);

      if ( gsdi_status != GSDI_SUCCESS )
      {
        return GSDI_ERROR;
      }
      break;

    case GSDI_PERSO_ENGINE_OTA_DERPERSO:

      client_data_ptr->feature_data.buffer_ptr = mem_pointer->buffer_start;

      /* Extract the Data to be Set */
      gsdi_status = gsdi_perso_engine_get_client_feature_data(
                    (uint32)((gsdi_perso_ota_deperso_req_T *)req_ptr)->num_bytes,
                    ((gsdi_perso_ota_deperso_req_T *)req_ptr)->data_buffer,
                    &client_data_ptr->feature_data);

      if ( gsdi_status != GSDI_SUCCESS )
      {
          MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could Not Get Client Feature Data 0x%x",
                                   gsdi_status);
          return gsdi_status;
      }
      break;

    case GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL:
      client_data_ptr->feature_mask =
        ((gsdi_perso_internal_command_req_T *)req_ptr)->active_lock_mask;
      break;

    default:
      return gsdi_status;
  }
  return gsdi_status;
}/* gsdi_perso_engine_get_client_data */


#ifdef FEATURE_PERSO_SIM
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINGE_GET_IMEI_SIZE

DESCRIPTION
  This function is called to calcuate the total memory required for the
  IMEI BUFFER

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_imei_size(
  uint32 * len_ptr
)
{
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);
  *len_ptr = GSDI_PERSO_MAX_IMEI_LEN;
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_get_imei_size */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINGE_GET_SERIAL_NUM_SIZE

DESCRIPTION
  This function is called to calcuate the total memory required for the
  IMEI BUFFER

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_serial_num_size(
  int32 * len_ptr
)
{
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);
  *len_ptr = GSDI_PERSO_ENGINE_SERIAL_NUMBER_LEN;
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_get_serial_num_size */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CALC_ENC_DATA_LEN

DESCRIPTION
  This function is called to calcuate the total memory required for a
  particular item.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_enc_data_len(
  gsdi_perso_engine_proc_enum_type      feature_ind,
  const void                           *req_ptr,
   int32                               *len_ptr
)
{
  mmgsdi_return_enum_type  mmgsdi_status = MMGSDI_ERROR;
  int32                    len           = 0;
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  uint8                   *data_buf_ptr  = NULL;
#else
  uint32                   sfs_size      = 0;
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#endif /* FEATURE_MMGSDI_PERSO_SFS */

  /* -----------------------------------------------------------------------
     Do the parameter checking
  ---------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);

  /* -----------------------------------------------------------------------
     Retrieve the File Size from FS
     ---------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  /* SFP is enabled */
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
    data_buf_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE)
  len = sfs_priv_simloc_read(data_buf_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE);

  MMGSDIUTIL_TMC_MEM_FREE(data_buf_ptr);

  if ( len == 0 )
#else
  /* SFP not enabled, fallback to SFS */
  mmgsdi_status = mmgsdi_sfs_get_size(&sfs_size, mmgsdi_sfs_perso_file);
  len = uint32toint32(sfs_size);
  if(mmgsdi_status != MMGSDI_SUCCESS ||
     len == 0)
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#else
  mmgsdi_status = gsdi_fs_get_file_size(mmgsdi_efs_perso_file,&len);
  if ( mmgsdi_status != MMGSDI_SUCCESS ||
       len == 0 )
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO:  FILE Status: 0x%x File Size: 0x%x",
                           mmgsdi_status, len);
    return MMGSDI_ERROR;
  }

  *len_ptr = len;

  /*Also cater for the new data that may be added*/
  if(feature_ind == GSDI_PERSO_ENGINE_SET_DATA_FEATURE)
  {
    MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
    /* Check for integer overflow */
    if(((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes > MMGSDI_INT32_MAX / 2 ||
       *len_ptr > MMGSDI_INT32_MAX - ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2)
    {
      return MMGSDI_ERROR;
    }
    *len_ptr += ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2;
  }
  /* Check to see if new SIM Lock Codes have to be generated */
  else if(feature_ind == GSDI_PERSO_ENGINE_AUTO_LOCK)
  {
    *len_ptr += (int32)(GSDI_PERSO_BUFFER_LEN -
                       (GSDI_PERSO_ENGINE_TAG_SIZE * GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS));
  }

  /* ------------------------------------------------------------------------
    Get the total size of the Mobile's IMEI
    -----------------------------------------------------------------------*/
  len =0;
  mmgsdi_status = gsdi_perso_engine_get_imei_size((uint32*)&len);

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    *len_ptr = 0;
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: get imei size failed: 0x%x",
                           mmgsdi_status);
    return mmgsdi_status;
  }

  /* Accumulate the length of the Deperso keys */
  *len_ptr = *len_ptr + len;

  /* ------------------------------------------------------------------------
    Get the total size of the Mobile's Serial Number
    -----------------------------------------------------------------------*/
  len =0;
  mmgsdi_status = gsdi_perso_engine_get_serial_num_size(&len);

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    *len_ptr = 0;
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO:  get mobile serial number failed 0x%x",
                             mmgsdi_status);
    return mmgsdi_status;
  }

  /* Accumulate the length of the Deperso keys */
  *len_ptr = *len_ptr + len;

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_calc_enc_data_len */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CALC_SIM_DATA_LEN

DESCRIPTION
  This function is called to calcuate the total memory required for the
  SIM Items to be retrieved.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_sim_data_len(
  int32 * len_ptr
)
{
  /* ------------------------------------------------------------------------
     Validate all parameters
     ----------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);

  /* ------------------------------------------------------------------------
     Calculate the total memory required.
     ----------------------------------------------------------------------*/
  *len_ptr = GSDI_PERSO_ENGINE_IMSI_LEN +
           GSDI_PERSO_ENGINE_GID1_LEN +
           GSDI_PERSO_ENGINE_GID2_LEN +
           GSDI_PERSO_ENGINE_CNL_LEN;

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_calc_sim_data_len */
#endif /*FEATURE_PERSO_SIM*/


/*===========================================================================
FUNCTION GSDI_PERSO_SEC_CALC_TOTAL_MEM_REQUIRED

DESCRIPTION
  This function is called to calcuate the total memory required for a
  particular item.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_client_data_len(
  gsdi_perso_engine_proc_enum_type      feature_ind,
  const void  *                         req_ptr,
  int32 *                               len_ptr
)
{
  /* ------------------------------------------------------------------------
     Validate all parameters
     ----------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);

  /* ------------------------------------------------------------------------
     Conditionally check the input parameters.  OK to have
     req_ptr of NULL if we are going to get all feature indicators
     since this is an internal request generated by our internal
     initialization procedures.
     ----------------------------------------------------------------------*/
  if ( feature_ind != GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS)
  {
    MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  }

  /* ------------------------------------------------------------------------
     Calculate the total memory required.
     ----------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------
     GSDI_PERSO_ENGINE_GET_FEATURE_DATA case does not require any data to be
     copied from the client but to enable copying of feature indicator we
     assign length to be the number of perso features
     ----------------------------------------------------------------------*/
/* ------------------------------------------------------------------------
     GSDI_PERSO_ENGINE_LOCK_DOWN case does not require any data to be
     copied from the client but to enable copying of lock down command we
     assign length to be the size of uint8
     ----------------------------------------------------------------------*/
  switch ( feature_ind )
  {
    case   GSDI_PERSO_ENGINE_ACTIVATE_FEATURE:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_act_feature_ind_req_T *)req_ptr)->num_bytes;
      break;

    case GSDI_PERSO_ENGINE_DEACTIVATE_FEATURE:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_deact_feature_ind_req_T *)req_ptr)->num_bytes;
      break;

    case GSDI_PERSO_ENGINE_UNBLOCK_DCK:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_unblock_feature_ind_req_T *)req_ptr)->num_bytes;
      break;

    case GSDI_PERSO_ENGINE_GET_DCK_NUM_RETRIES:
    case GSDI_PERSO_ENGINE_GET_DCK_UNBLOCK_NUM_RETRIES:
      *len_ptr = 0;
      break;

    case GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS:
    case GSDI_PERSO_ENGINE_GET_FEATURE_IND:
    case GSDI_PERSO_ENGINE_GET_FEATURE_DATA:
    case GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL:
    case GSDI_PERSO_ENGINE_AUTO_LOCK:
    case GSDI_PERSO_ENGINE_GET_PERM_FEATURE_IND:
      *len_ptr = GSDI_PERSO_NUM_OF_SIM_FEATURES + GSDI_PERSO_NUM_OF_RUIM_FEATURES;
      break;

    case GSDI_PERSO_ENGINE_GET_FEATURE_KEY:
      *len_ptr = GSDI_PERSO_MAX_CK;
      break;

    case GSDI_PERSO_ENGINE_LOCK_DOWN:
      *len_ptr = sizeof(uint8);
      break;

    case GSDI_PERSO_ENGINE_PERM_DISABLE_FEATURE_IND:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_perm_disabled_feature_req_T*)req_ptr)->num_bytes;
      break;

   case GSDI_PERSO_ENGINE_SET_DATA_FEATURE:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes;
      break;

    case GSDI_PERSO_ENGINE_OTA_DERPERSO:
      MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
      *len_ptr = ((gsdi_perso_ota_deperso_req_T *)req_ptr)->num_bytes;
      break;

    case GSDI_PERSO_ENGINE_REFRESH:
      *len_ptr = GSDI_PERSO_NUM_OF_SIM_FEATURES;
      break;

    default:
      UIM_MSG_ERR_1("PERSO: UNKNOWN feature_ind: 0x%x",feature_ind);
      *len_ptr = 0;
      return MMGSDI_ERROR;
  }
  return MMGSDI_SUCCESS;

} /* gsdi_perso_engine_calc_client_data_len */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CALC_MEM_REQUIRED

DESCRIPTION
  This function is called to calcuate the total memory required for a
  particular item.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_mem_required(
  gsdi_perso_eng_calc_req_mem_enum_type calc_type,
  gsdi_perso_engine_proc_enum_type      feature_ind,
  const void *                          req_ptr,
  int32 *                               required_len_ptr
)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;
  /* -----------------------------------------------------------------------
     Validate the Parameters provided
     ---------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(required_len_ptr, req_ptr);

  /* --------------------------------------------------------------------------
     Initialize all variables
     ------------------------------------------------------------------------*/
  *required_len_ptr = 0;

  /* --------------------------------------------------------------------------
     Get the total length of all memory which needs to be allocated.
     Please see the notes on each calc_type to understand how the total
     memory size is allocated.
     ------------------------------------------------------------------------*/
  switch ( calc_type )
  {
#ifdef FEATURE_PERSO_SIM
    case GSDI_PERSO_ENG_CALC_ENC_DATA:
      /* ----------------------------------------------------------------------
         The total memory required is the sum of the following:
          Buffer size for the Feature Indicators
          Buffer size for the Depersonalization Keys
          Buffer size for the Depersonalization Counters
          Buffer size for the Mobile's IMEI
          Buffer size for the Mobile's Serial Number
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_calc_enc_data_len(feature_ind,
                                                          req_ptr,
                                                          required_len_ptr);
      if ( mmgsdi_status != MMGSDI_SUCCESS )
      {
        return mmgsdi_status;
      }

      break;

    case GSDI_PERSO_ENG_CALC_SIM_DATA:
      /* ----------------------------------------------------------------------
         The total memory required is the sum of the following:
          Buffer size for the IMSI
          Buffer size for the CNL
          Buffer size for the GID1
          Buffer size for the GID2
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_calc_sim_data_len(required_len_ptr);
      if ( mmgsdi_status != MMGSDI_SUCCESS )
      {
        return mmgsdi_status;
      }
      break;


#endif /*FEATURE_PERSO_SIM*/
    case GSDI_PERSO_ENG_CALC_CLIENT_DATA:
      /* ----------------------------------------------------------------------
         The total memory required is dependent on the
         Action to be performed.  Please see the following function to
         determine how the block of memory is allocated.
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_calc_client_data_len(feature_ind,
                                                             req_ptr,
                                                             required_len_ptr);
      if ( mmgsdi_status != MMGSDI_SUCCESS )
      {
        return mmgsdi_status;
      }
      break;

    default:
      *required_len_ptr = 0x00;
      UIM_MSG_ERR_1("PERSO: Wrong calc_type 0x%x",calc_type);
      return MMGSDI_ERROR;
  } /* switch ( calc_type ) */

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_calc_mem_required */


/* ============================================================================
FUNCTION: GSDI_PERSO_ENGINE_WRITE_BACK_FS

DESCRIPTION:
  Function will write back to FS any data indicated to be modified.  The
  indication is set by the Security Library.

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

============================================================================ */
static mmgsdi_return_enum_type gsdi_perso_engine_write_back_fs(
  gsdi_perso_sec_me_data_type * encrypted_me_data_ptr,
  boolean                       is_sync_required
)
{
  mmgsdi_return_enum_type    mmgsdi_status = MMGSDI_SUCCESS;
  uint32                     len           = 0;
  uint8 *                    buffer_ptr    = NULL;
  uint32                     buffer_len    = 0;
  gsdi_perso_sec_data_type  *sec_data_ptr  = NULL;
  int32                      i             = 0;
#ifdef FEATURE_MMGSDI_PERSO_SFS
  int                        total         = 0;
#endif /*FEATURE_MMGSDI_PERSO_SFS*/

  /* --------------------------------------------------------------------------
     Validate the Paramaters provided
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(encrypted_me_data_ptr);

  /* If update fs is not required, exist out of the function */
  if ( !encrypted_me_data_ptr->data_mod.update_fs )
  {
    return MMGSDI_SUCCESS;
  }
  /* Update to FS is required, continue with the process */
  buffer_len = encrypted_me_data_ptr->feat_inds.len +
        encrypted_me_data_ptr->perm_feat_inds.len +
        encrypted_me_data_ptr->sim_lock_codes.len +
        encrypted_me_data_ptr->dcks.len +
        encrypted_me_data_ptr->dck_counts.len +
        encrypted_me_data_ptr->unblock_cks.len +
        encrypted_me_data_ptr->unblock_ck_counts.len +
        encrypted_me_data_ptr->lock_down.len +
        (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE) * GSDI_PERSO_ENGINE_NUMBER_OF_TAGS;

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(buffer_ptr, buffer_len)
  /*Write the Feature Inds*/

  MMGSDIUTIL_RETURN_IF_NULL(buffer_ptr);

  for(i =1;i<=GSDI_PERSO_ENGINE_NUMBER_OF_TAGS;i++)
  {
    switch(i)
    {
      case GSDI_PERSO_ENGINE_TAG_FEATURE_INDS:
        sec_data_ptr = &encrypted_me_data_ptr->feat_inds;
        break;
      case GSDI_PERSO_ENGINE_TAG_PERM_FEATURE_INDS:
        sec_data_ptr = &encrypted_me_data_ptr->perm_feat_inds;
        break;
      case GSDI_PERSO_ENGINE_TAG_SIM_LOCK_CODE:
        sec_data_ptr = &encrypted_me_data_ptr->sim_lock_codes;
        break;
      case GSDI_PERSO_ENGINE_TAG_DEPERSO_CK:
        sec_data_ptr = &encrypted_me_data_ptr->dcks;
        break;
      case GSDI_PERSO_ENGINE_TAG_DCK_COUNTER:
        sec_data_ptr = &encrypted_me_data_ptr->dck_counts;
        break;
      case GSDI_PERSO_ENGINE_TAG_UNBLOCK:
        sec_data_ptr = &encrypted_me_data_ptr->unblock_cks;
        break;
      case GSDI_PERSO_ENGINE_TAG_UNBLOCK_COUNTER:
        sec_data_ptr = &encrypted_me_data_ptr->unblock_ck_counts;
        break;
      case GSDI_PERSO_ENGINE_TAG_LOCK_DOWN:
        sec_data_ptr = &encrypted_me_data_ptr->lock_down;
        break;
      default:
        UIM_MSG_ERR_0("PERSO: Incorrect tag");
        MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(buffer_ptr);
        return MMGSDI_ERROR;
    }

    buffer_ptr[len] = (uint8)i;
    len += GSDI_PERSO_ENGINE_TAG_SIZE;
    buffer_ptr[len] = (uint8)(sec_data_ptr->len/GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT);
    buffer_ptr[len+1] = (uint8)(sec_data_ptr->len%GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT);
    len += GSDI_PERSO_ENGINE_LEN_SIZE;

    mmgsdi_memscpy(buffer_ptr + len,
                   buffer_len - len,
                   sec_data_ptr->buffer_ptr,
                   sec_data_ptr->len);
    len += sec_data_ptr->len;
  }
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  /* SFP is enabled */
  total = sfs_priv_simloc_write(buffer_ptr, len);
#else
  /* SFP not enabled, fallback to SFS */
  mmgsdi_status = mmgsdi_sfs_write(buffer_ptr, &len, mmgsdi_sfs_perso_file);
  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
    total = uint32toint32(len);
  }
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
  if ( total != len )
#else
  mmgsdi_status = gsdi_efs_write_file(mmgsdi_efs_perso_file,
                                      (int32)len,
                                      buffer_ptr);
  if ( mmgsdi_status != MMGSDI_SUCCESS )
#endif /*FEATURE_MMGSDI_PERSO_SFS*/
  {
     MMGSDI_DEBUG_MSG_ERROR_0("Failure to write SIM perso file");
  }
  else
  {
    /* Data written successfully to RAM.
     * Force it to be transferred from memory to eMMC card only if
     * is_sync_required flag is TRUE
     */
    if(is_sync_required)
    {
      if(efs_sync("/") != 0)
      {
        UIM_MSG_ERR_1("efs_sync failed 0x%x", efs_errno);
        mmgsdi_status = MMGSDI_ERROR;
      }
    }
  }

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(buffer_ptr);

  return mmgsdi_status;
} /* gsdi_perso_engine_write_back_fs */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_GET_TECHNOLOGY

DESCRIPTION
  This function returns either umts or cdma or both based on the
  feature type

DEPENDENCIES
  None

RETURN VALUE
  void
===========================================================================*/
static void gsdi_perso_engine_get_technology(
  gsdi_perso_engine_proc_enum_type feature_type,
  uint32                           feature,
  boolean                          *umts,
  boolean                          *ruim)
{
  switch(feature_type)
  {
    case GSDI_PERSO_ENGINE_ACTIVATE_FEATURE:
    case GSDI_PERSO_ENGINE_DEACTIVATE_FEATURE:
    case GSDI_PERSO_ENGINE_SET_DATA_FEATURE:
    case GSDI_PERSO_ENGINE_UNBLOCK_DCK:
    case GSDI_PERSO_ENGINE_PERM_DISABLE_FEATURE_IND:
    case GSDI_PERSO_ENGINE_GET_FEATURE_KEY:
    case GSDI_PERSO_ENGINE_GET_FEATURE_DATA:
      switch(feature)
      {
#ifdef FEATURE_PERSO_SIM
        case GSDI_PERSO_SEC_MASK_FEAT_NW:
        case GSDI_PERSO_SEC_MASK_FEAT_NS:
        case GSDI_PERSO_SEC_MASK_FEAT_SP:
        case GSDI_PERSO_SEC_MASK_FEAT_CP:
        case GSDI_PERSO_SEC_MASK_FEAT_SIM:
         *umts = TRUE;
         break;
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW1:
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW2:
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_HRPD:
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_SP:
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_CP:
        case GSDI_PERSO_SEC_MASK_FEAT_RUIM_RUIM:
          *ruim = TRUE;
          break;
#endif /* FEATURE_PERSO_RUIM */
        case GSDI_PERSO_NONE:
        default:
          break;
      }
      break;

    case GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS:
    case GSDI_PERSO_ENGINE_GET_FEATURE_IND:
    case GSDI_PERSO_ENGINE_GET_PERM_FEATURE_IND:
    case GSDI_PERSO_ENGINE_GET_DCK_NUM_RETRIES:
    case GSDI_PERSO_ENGINE_GET_DCK_UNBLOCK_NUM_RETRIES:
    case GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL:
    case GSDI_PERSO_ENGINE_AUTO_LOCK:
    case GSDI_PERSO_ENGINE_LOCK_DOWN:
      *umts = TRUE;
      *ruim = TRUE;
      break;

    case GSDI_PERSO_ENGINE_OTA_DERPERSO:
    case GSDI_PERSO_ENGINE_REFRESH:
      *umts = TRUE;
      break;

    default:
      break;
  }
} /* gsdi_perso_engine_get_technology */


#ifdef FEATURE_PERSO_SIM
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_HANDLE_UMTS_REQUEST

DESCRIPTION
  This function is the main handler for all the perso requests.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_handle_umts_request(
  mmgsdi_session_id_type           session_id,
  mmgsdi_int_app_info_type        *app_info_ptr,
  mmgsdi_slot_id_enum_type         slot,
  gsdi_perso_engine_proc_enum_type feature_type,
  gsdi_perso_sec_client_req_type  *client_data,
  const void *                     req_ptr,
  uint8 *                          ret_data_ptr,
  uint32                           ret_data_max_len,
  int32*                           ret_data_len_ptr,
  gsdi_perso_event_enum_type      *perso_event_ptr)
{
  gsdi_perso_sec_sim_data_type       sim_data = {{0}};
  uint32                             total_mem_req      = 0x00;
  mmgsdi_return_enum_type            mmgsdi_status      = MMGSDI_ERROR;
  gsdi_returns_T                     gsdi_status;
  gsdi_perso_engine_mem_pointer_type enc_mem_pointer;
  gsdi_perso_sec_me_data_type        encrypted_data;
  boolean                            sim_data_required  = FALSE;

  /* Checking for card data necessity for given feature */
  switch(feature_type)
  {
    case GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL:
    case GSDI_PERSO_ENGINE_AUTO_LOCK:
      sim_data_required  = TRUE;
      if (GSDI_PERSO_SEC_MASK_FEAT_NOT_INIT == client_data->feature_mask)
      {
        /* Return successful when no lock is enabled */
        return MMGSDI_SUCCESS;
      }
      break;

    case GSDI_PERSO_ENGINE_REFRESH:
      sim_data_required  = TRUE;
      break;

    default:
      sim_data_required = FALSE;
      break;
  } /* end of switch(feature_type) */

  if( TRUE == sim_data_required)
  {
    if(app_info_ptr == NULL)
    {
      UIM_MSG_ERR_0("App Info Ptr is NULL");
      return MMGSDI_ERROR;
    }

    /* When SIM PERSO is not applicable to the card,  return success. */
    if (MMGSDI_PERSO_SIM_ALLOWED != (app_info_ptr->perso_ability & MMGSDI_PERSO_SIM_ALLOWED))
    {
      UIM_MSG_HIGH_2("Card does not have SIM PERSO capability. Given feature 0x%x, feature Mask 0x%x",
                     feature_type, client_data->feature_mask);
      return MMGSDI_SUCCESS;
    }
  }

  /* --------------------------------------------------------------------------
     Initialize Data as necessary
     ------------------------------------------------------------------------*/
  memset(&enc_mem_pointer,0x00, sizeof(gsdi_perso_engine_mem_pointer_type));
  memset(&encrypted_data.data_mod, 0, sizeof(gsdi_perso_sec_data_mod_inds_type));
  memset(&sim_data, 0, sizeof(gsdi_perso_sec_sim_data_type));

  /* --------------------------------------------------------------------------
   The Perso Engine Has an Interface to allow the Personalization
   Engine to determine how much memory needs to be allocated.
   ------------------------------------------------------------------------*/
  mmgsdi_status = gsdi_perso_engine_calc_mem_required(GSDI_PERSO_ENG_CALC_ENC_DATA,
                                                    feature_type,
                                                    (void *)req_ptr,
                                                    (int32*)&total_mem_req);
  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get total memory required");
    return mmgsdi_status;
  }

  /* ---------------------------------------------------------------------------
     Allocate the entire buffer required for the encrypted data structure
     Memset it to 0x00 when successful and the length required is greater
     than 0.
     Set memory_pointer.memory_curr = memory_pointer.memory_start
     Set memory_pointer.memory_end  = memory_pointer.memory_start + total_mem_req
     -------------------------------------------------------------------------*/
  if ( total_mem_req > 0 )
  {
    MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
      enc_mem_pointer.buffer_start, total_mem_req)
    MMGSDIUTIL_RETURN_IF_NULL(enc_mem_pointer.buffer_start);

    enc_mem_pointer.curr_offset = 0;
    enc_mem_pointer.buffer_size = total_mem_req;
    /* ---------------------------------------------------------------------------
       Populate the encrypted data into the encrypted_data structure.
      The memory pointer will be used to provide memory for the
      encrypted_data structure buffers
      -------------------------------------------------------------------------*/
    mmgsdi_status = gsdi_perso_engine_get_encrypted_data(&encrypted_data,
                                                         &enc_mem_pointer,
                                                         feature_type,
                                                         (void *)req_ptr);

    if ( mmgsdi_status != MMGSDI_SUCCESS )
    {
      /* An error occurred getting the Encrypted
         information from FS. All memory will be freed.
      */
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
      return mmgsdi_status;
    }
  }
  else
  {
    UIM_MSG_HIGH_0("PERSO: Encrypted Data Not Required from GSDI ENGINE");
  }

  if(TRUE == sim_data_required)
  {
    /* --------------------------------------------------------------------------
       Get the required SIM Data in the case that the SIM Lock State is in
       AUTOLOCK for the Feature Indicated.
       ------------------------------------------------------------------------*/
    mmgsdi_status = gsdi_perso_engine_get_sim_data(session_id,
                                                   app_info_ptr->app_data.app_type,
                                                   slot,
                                                   &sim_data,
                                                   feature_type);
    if ( mmgsdi_status != MMGSDI_SUCCESS )
    {
      /* An error occurred getting the  data
         All memory will be freed.
      */
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get SIM Data");
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
      return MMGSDI_PERSO_CARD_DATA_ERROR;
    }
  } /* if(TRUE == sim_data_required) */

  /* --------------------------------------------------------------------------
       Now send the data to the security component
       of the SIM Lock to perform the validation
       of the data provided.
      ------------------------------------------------------------------------*/
  gsdi_status = gsdi_perso_security_handle_request(feature_type,
                                                   client_data,
                                                   &encrypted_data,
                                                   &sim_data,
                                                   ret_data_ptr,
                                                   ret_data_max_len,
                                                   ret_data_len_ptr,
                                                   perso_event_ptr);
  mmgsdi_util_convert_gsdi_status_to_mmgsdi(gsdi_status, &mmgsdi_status);
  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
    UIM_MSG_HIGH_1("PERSO: SECURITY 0x%x:  SUCCESS", feature_type);
  }
  else
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: SECURITY 0x%x: FAILURE 0x%x",
                             feature_type, mmgsdi_status);
  }

   /* --------------------------------------------------------------------------
     Wipe out DCK if refresh boolean is set
     ------------------------------------------------------------------------*/
  if((encrypted_data.data_mod.refresh) && (app_info_ptr != NULL))
  {
    mmgsdi_status = gsdi_perso_engine_write_dck(session_id,
                                                 slot,
                                                 app_info_ptr->app_data.app_type);
    UIM_MSG_HIGH_1("PERSO: gsdi_perso_engine_write_dck status: 0x%x",
                   mmgsdi_status);

    mmgsdi_status = MMGSDI_SUCCESS;
  }
   /* --------------------------------------------------------------------------
     See if any data was mofified.  If so, write it back to FS
     ------------------------------------------------------------------------*/
  if(encrypted_data.data_mod.update_fs)
  {
    if(MMGSDI_SUCCESS !=  gsdi_perso_engine_write_back_fs(&encrypted_data, TRUE))
    {
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not write back FS");
      mmgsdi_status = MMGSDI_ERROR;
    }
  }

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data.imsi.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data.cnl.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data.gid1.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data.gid2.data_ptr);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(sim_data.dck.data_ptr);
  return mmgsdi_status;
}/* gsdi_perso_engine_handle_umts_request */


#endif /*FEATURE_PERSO_SIM*/
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_HANDLE_REQUEST

DESCRIPTION
  This function is the main handler for all the perso requests.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_handle_request(
  mmgsdi_session_id_type       session_id,
  mmgsdi_int_app_info_type    *app_info_ptr,
  mmgsdi_slot_id_enum_type     slot,
  const void *                 req_ptr,
  byte                         msg_id,
  uint8 *                      ret_data_ptr,
  uint32                       ret_data_max_len,
  int32*                       ret_data_len_ptr,
  gsdi_perso_event_enum_type * perso_event_ptr
)
{
  gsdi_returns_T                     gsdi_status        = GSDI_SUCCESS;
  mmgsdi_return_enum_type            mmgsdi_status      = MMGSDI_SUCCESS;
  gsdi_perso_sec_client_req_type     client_data;            /* Client info  */
  gsdi_perso_engine_mem_pointer_type client_mem_pointer;
  uint32                             total_mem_req      = 0x00;
  gsdi_perso_engine_proc_enum_type   feature_type;
  boolean                            umts               = FALSE;
  boolean                            ruim               = FALSE;
#ifdef FEATURE_PERSO_SIM
  uint32                             sim_feature_mask   =
                                             GSDI_PERSO_SEC_MASK_FEAT_NOT_INIT;
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
  uint32                             ruim_feature_mask  =
                                              GSDI_PERSO_SEC_MASK_FEAT_NOT_INIT;
#endif /* FEATURE_PERSO_RUIM */


  /* ------------------------------------------------------------------------
     Conditionally check the input parameters.  OK to have
     req_ptr of NULL if we are going to get all feature indicators
     since this is an internal request generated by our internal
     initialization procedures.
     ----------------------------------------------------------------------*/
  if ( msg_id != GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS_REQ)
  {
    MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
  }

  MMGSDIUTIL_RETURN_IF_NULL_3(ret_data_len_ptr, ret_data_ptr, perso_event_ptr);

  memset(&client_mem_pointer,0x00, sizeof(gsdi_perso_engine_mem_pointer_type));
  memset(&client_data,0x00, sizeof(gsdi_perso_sec_client_req_type));

  *ret_data_len_ptr = 0;

  switch(msg_id)
  {
    case GSDI_PERSO_ACT_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_ACTIVATE_FEATURE;
      break;

    case GSDI_PERSO_DEACT_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_DEACTIVATE_FEATURE;
      break;

    case GSDI_PERSO_SET_FEATURE_DATA_REQ:
      feature_type = GSDI_PERSO_ENGINE_SET_DATA_FEATURE;
      break;

    case GSDI_PERSO_GET_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_FEATURE_IND;
      break;

    case GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_UNBLOCK_DCK;
      break;

    case GSDI_PERSO_OTA_DERPERSO_REQ:
      feature_type = GSDI_PERSO_ENGINE_OTA_DERPERSO;
      break;

    case GSDI_PERSO_INT_PROC_REQ:
      feature_type = GSDI_PERSO_ENGINE_INT_PROC;
      break;

    case GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_PERM_DISABLE_FEATURE_IND;
      break;

    case GSDI_PERSO_GET_PERM_FEATURE_IND_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_PERM_FEATURE_IND;
      break;

    case GSDI_PERSO_GET_FEATURE_KEY_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_FEATURE_KEY;
      break;

    case GSDI_PERSO_LOCK_DOWN_REQ:
      feature_type = GSDI_PERSO_ENGINE_LOCK_DOWN;
      break;

    case GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS;
      break;

    case GSDI_PERSO_ENGINE_CHECK_ALL_LOCKS_REQ:
      feature_type = GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL;
      break;

     case GSDI_PERSO_ENGINE_CHECK_AUTOLOCKS_REQ:
      feature_type = GSDI_PERSO_ENGINE_AUTO_LOCK;
      break;

    case GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_DCK_NUM_RETRIES;
      break;

    case GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_DCK_UNBLOCK_NUM_RETRIES;
      break;

    case GSDI_PERSO_ENGINE_SIM_REFRESH_REQ:
      feature_type = GSDI_PERSO_ENGINE_REFRESH;
      break;

    case GSDI_PERSO_GET_FEATURE_DATA_REQ:
      feature_type = GSDI_PERSO_ENGINE_GET_FEATURE_DATA;
      break;

    default:
      UIM_MSG_ERR_1("PERSO: Cannot Handle msg_id 0x%x",msg_id);
      return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     The Perso Engine Has an Interface to allow the Personalization
     Engine to determine how much memory needs to be allocated.
     ------------------------------------------------------------------------*/
  mmgsdi_status = gsdi_perso_engine_calc_mem_required(GSDI_PERSO_ENG_CALC_CLIENT_DATA,
                                                      feature_type,
                                                      (void *)req_ptr,
                                                      (int32*)&total_mem_req);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get total memory required");
    return mmgsdi_status;
  }

  if(total_mem_req > 0)
  {
    /* ---------------------------------------------------------------------------
       Allocate the entire buffer required for the encrypted data structure
       Memset it to 0x00 when successful
       Set memory_pointer.memory_curr = memory_pointer.memory_start
       Set memory_pointer.memory_end  = memory_pointer.memory_start + total_mem_req
       -------------------------------------------------------------------------*/
    MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
      client_mem_pointer.buffer_start, total_mem_req)
    if(client_mem_pointer.buffer_start == NULL)
    {
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: NULL pointer in gsdi_perso_engine_handle_request");
      return MMGSDI_ERROR;
    }

    client_mem_pointer.curr_offset = 0;
    client_mem_pointer.buffer_size = total_mem_req;

    /* --------------------------------------------------------------------------
       Get the client data from the request.
       ------------------------------------------------------------------------*/
    gsdi_status = gsdi_perso_engine_get_client_data(feature_type,
                                                    &client_data,
                                                    &client_mem_pointer,
                                                    (void *)req_ptr);
    mmgsdi_util_convert_gsdi_status_to_mmgsdi(gsdi_status, &mmgsdi_status);

    if(mmgsdi_status != MMGSDI_SUCCESS)
    {
      /* An error occurred getting the  data
       All memory will be freed.
      */
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get Client Data");
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(client_mem_pointer.buffer_start);

      return mmgsdi_status;
    }

    /* Read Client Feature mask, that contains both SIM and RUIM mask */
    /* Separate mask for SIM and RUIM */
#ifdef FEATURE_PERSO_SIM
    sim_feature_mask = client_data.feature_mask & GSDI_PERSO_SEC_SIM_MASK;
#endif /* FEATURE_PERSO_SIM */

#ifdef FEATURE_PERSO_RUIM
    ruim_feature_mask = client_data.feature_mask & GSDI_PERSO_SEC_RUIM_MASK;
#endif /* FEATURE_PERSO_RUIM */
  } /*  if(total_mem_req > 0) */

  gsdi_perso_engine_get_technology(feature_type,
                                   client_data.feature_mask,
                                   &umts,
                                   &ruim);

#ifdef FEATURE_PERSO_SIM
  if(umts)
  {
    /* Only take SIM mask */
    client_data.feature_mask = sim_feature_mask;
    UIM_MSG_HIGH_1("PERSO: SIM Feature Mask 0x%x", client_data.feature_mask);
    mmgsdi_status = gsdi_perso_engine_handle_umts_request(session_id,
                                                          app_info_ptr,
                                                          slot,
                                                          feature_type,
                                                          &client_data,
                                                          req_ptr,
                                                          ret_data_ptr,
                                                          ret_data_max_len,
                                                          ret_data_len_ptr,
                                                          perso_event_ptr);
    if(mmgsdi_status != MMGSDI_SUCCESS)
    {
      UIM_MSG_ERR_1("PERSO: UMTS Personalization failed 0x%x", mmgsdi_status);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(client_mem_pointer.buffer_start);
      return mmgsdi_status;
    }
  }/* if(umts) */
#endif /*FEATURE_PERSO_SIM*/

#ifdef FEATURE_PERSO_RUIM
  if(ruim)
  {
    /* Take a backup of umts return data length */
    int32 umts_ret_data_len = *ret_data_len_ptr;
    /* Only take RUIM mask */
    client_data.feature_mask = ruim_feature_mask;
    UIM_MSG_HIGH_1("PERSO: RUIM Feature Mask 0x%x", client_data.feature_mask);
    /* Append data on ret_data_ptr */
    mmgsdi_status = gsdi_perso_engine_handle_ruim_request(session_id,
                                                          app_info_ptr,
                                                          slot,
                                                          feature_type,
                                                          &client_data,
                                                          req_ptr,
                                                          (ret_data_ptr + umts_ret_data_len),
                                                          ret_data_max_len - umts_ret_data_len,
                                                          ret_data_len_ptr,
                                                          perso_event_ptr);

    if(mmgsdi_status != MMGSDI_SUCCESS)
    {
      UIM_MSG_ERR_1("PERSO: RUIM Personalization failed 0x%x", mmgsdi_status);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(client_mem_pointer.buffer_start);
      return mmgsdi_status;
    }
    else
    {
      /* Update return data length */
      *ret_data_len_ptr += umts_ret_data_len;
    }
  }/* if(ruim) */
#endif /*FEATURE_PERSO_RUIM*/

  /* --------------------------------------------------------------------------
     Free Up all memory allocated during these procedures
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(client_mem_pointer.buffer_start);
  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_handle_request */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_GET_ALL_LOCKS

DESCRIPTION
  This function will get all of the Personalization Locks and determine
  whether or not a Personalization Check is required for each lock.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_all_locks(
  mmgsdi_session_id_type       session_id,
  mmgsdi_int_app_info_type   * app_info_ptr,
  mmgsdi_slot_id_enum_type     slot,
  int32                      * ret_data_len_ptr,
  uint8                      * ret_data_ptr,
  uint32                       ret_data_max_len,
  gsdi_perso_event_enum_type * perso_event_ptr
)
{
  mmgsdi_return_enum_type            mmgsdi_status = MMGSDI_SUCCESS;
  gsdi_perso_get_feature_ind_req_T * req_ptr       = NULL;
  byte                               msg_id        = 0x00;

  /* --------------------------------------------------------------------------
     Validate the Parameters provided
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_3(ret_data_len_ptr, ret_data_ptr, perso_event_ptr);

  /* --------------------------------------------------------------------------
     build a makeshift get feature indicators req for the purpose of handling
     the request properly
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
    req_ptr, sizeof(gsdi_perso_get_feature_ind_req_T))

  if ( req_ptr == NULL )
  {
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Now reuse the Perso Engine Handle Request to gather all data
     and send to the security library so that the feature indicators can
     be returned.
     ------------------------------------------------------------------------*/
  msg_id = GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS_REQ;
  req_ptr->message_header.message_id  = msg_id;

  mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                   app_info_ptr,
                                                   slot,
                                                   req_ptr,
                                                   msg_id,
                                                   ret_data_ptr,
                                                   ret_data_max_len,
                                                   ret_data_len_ptr,
                                                   perso_event_ptr);

  MMGSDIUTIL_TMC_MEM_FREE(req_ptr);

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Could not get all feature indicators: 0x%x",
                             mmgsdi_status);

  }
  return mmgsdi_status;
}/*gsdi_perso_engine_get_all_locks*/


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_ANALYZE_INDICATORS

DESCRIPTION
  This function is used to analyze the indicators.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_analyze_indicators(
  mmgsdi_int_app_info_type           *app_info_ptr,
  const uint8 *                       lock_data_ptr,
  const int32 *                       lock_ret_data_len_ptr,
  gsdi_perso_internal_command_req_T   *req_ptr
)
{
  int32   index                = 0;
  boolean nok                  = FALSE;
  uint32  allowed_feature_mask = GSDI_PERSO_SEC_MASK_FEAT_NOT_INIT;

  /* --------------------------------------------------------------------------
     Validate all parameters provided to this function
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_3(req_ptr, app_info_ptr, lock_data_ptr);
  MMGSDIUTIL_RETURN_IF_NULL(lock_ret_data_len_ptr);

  if ( *lock_ret_data_len_ptr > GSDI_PERSO_SEC_MAX_NUM_INDS )
  {
    UIM_MSG_ERR_1("PERSO: Too many locks indicated 0x%x",*lock_ret_data_len_ptr);
    app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_ERROR_S;
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Initialize the req_ptr lock masks
     ------------------------------------------------------------------------*/
  req_ptr->active_lock_mask = 0x00;
  req_ptr->autolock_mask    = 0x00;

  /* --------------------------------------------------------------------------
     Now validate the Data Provided
     ------------------------------------------------------------------------*/
  while ( index < *lock_ret_data_len_ptr )
  {
    switch ( lock_data_ptr[index] )
    {
      case GSDI_PERSO_ENG_NOT_ACTIVE_STATE:
      case GSDI_PERSO_ENG_AUTOLOCK_STATE:
      case GSDI_PERSO_ENG_ACTIVE_STATE:
      case GSDI_PERSO_ENG_UNLOCKED_STATE:
      case GSDI_PERSO_ENG_DISABLED_STATE:
        nok |= FALSE;
        break;

      default:
        nok |= TRUE;
        break;
    }
    index++;
  }

  /* --------------------------------------------------------------------------
     Check to see if the data is ok.  if not, exit out but first set the
     next state to the Error State
     ------------------------------------------------------------------------*/
  if ( nok == TRUE )
  {
    app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_ERROR_S;
    return MMGSDI_ERROR;
  }

  /* --------------------------------------------------------------------------
     Now set the autolock lock states as indicated by the data
     ------------------------------------------------------------------------*/
  index = 0;
  req_ptr->autolock_mask =0;

  while ( index < *lock_ret_data_len_ptr )
  {
    if ( lock_data_ptr[index] == (int)GSDI_PERSO_ENG_AUTOLOCK_STATE)
    {
      switch ( index )
      {
#ifdef FEATURE_PERSO_SIM
        case GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_NW;
          break;

        case GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_NS;
          break;

        case GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_SP;
          break;

        case GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_CP;
          break;

        case GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_SIM;
          break;

#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
        case GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW1;
          break;

        case GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW2;
          break;

        case GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET:
        case GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET:
        case GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET:
          /* Skip AUTOLOCK until feature logic is added */
          break;

        case GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET:
          req_ptr->autolock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_RUIM;
          break;
#endif /* FEATURE_PERSO_RUIM */
        default:
          break;
      }
    }

    index++;
  }

/* Set mask according to Perso support on the card */
#ifdef FEATURE_PERSO_SIM
  if (MMGSDI_PERSO_SIM_ALLOWED == (app_info_ptr->perso_ability
                                 & MMGSDI_PERSO_SIM_ALLOWED))
  {
    allowed_feature_mask |= req_ptr->autolock_mask & GSDI_PERSO_SEC_SIM_MASK;
  }
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
  if (MMGSDI_PERSO_RUIM_ALLOWED == (app_info_ptr->perso_ability
                                  & MMGSDI_PERSO_RUIM_ALLOWED))
  {
    allowed_feature_mask |= req_ptr->autolock_mask & GSDI_PERSO_SEC_RUIM_MASK;
  }
#endif /* FEATURE_PERSO_RUIM */

  /* Update req_ptr autolock mask */
  req_ptr->autolock_mask = allowed_feature_mask;

  /* --------------------------------------------------------------------------
     Move the state to the next state.
     If there is an item set in the autolock mask, then it needs to be updated
     against the data in the File Syste.
     ------------------------------------------------------------------------*/
  if ( req_ptr->autolock_mask != 0 )
  {
    if(app_info_ptr->perso_state == MMGSDI_PERSO_ENG_INIT_S)
    {
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_AUTOLOCK_S;
    }
    return MMGSDI_SUCCESS;
  }

  /* --------------------------------------------------------------------------
     Now set the active lock states as indicated by the data
     ------------------------------------------------------------------------*/
  index = 0;
  req_ptr->active_lock_mask =0;

  while ( index < *lock_ret_data_len_ptr )
  {
    if ( lock_data_ptr[index] == (int)GSDI_PERSO_ENG_ACTIVE_STATE)
    {
      switch ( index )
      {
#ifdef FEATURE_PERSO_SIM
        case GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_NW;
          break;

        case GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_NS;
          break;

        case GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_SP;
          break;

        case GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_CP;
          break;

        case GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_SIM;
          break;

#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
        case GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW1;
          break;

        case GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_NW2;
          break;

        case GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_HRPD;
          break;

        case GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_SP;
          break;

        case GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_CP;
          break;

        case GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET:
          req_ptr->active_lock_mask |= GSDI_PERSO_SEC_MASK_FEAT_RUIM_RUIM;
          break;
#endif /* FEATURE_PERSO_RUIM */

        default:
          break;
      }
    }

    index++;
  }

  /* Set mask according to Perso support on the card */
#ifdef FEATURE_PERSO_SIM
  if (MMGSDI_PERSO_SIM_ALLOWED == (app_info_ptr->perso_ability
                                 & MMGSDI_PERSO_SIM_ALLOWED))
  {
    allowed_feature_mask |= req_ptr->active_lock_mask & GSDI_PERSO_SEC_SIM_MASK;
  }
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
  if (MMGSDI_PERSO_RUIM_ALLOWED == (app_info_ptr->perso_ability
                                  & MMGSDI_PERSO_RUIM_ALLOWED))
  {
    allowed_feature_mask |= req_ptr->active_lock_mask & GSDI_PERSO_SEC_RUIM_MASK;
  }
#endif /* FEATURE_PERSO_RUIM */
  /* Update req_ptr autolock mask */
  req_ptr->active_lock_mask = allowed_feature_mask;
  /* --------------------------------------------------------------------------
     Move the state to the next state.
     If there is an item set in the lock mask, then it needs to be "checked"
     against the data in the File sytem.
     If there are not items set in the lock mask, then we need to check for
     ActiveLock by moving to the ActiveLock state.
     ------------------------------------------------------------------------*/
  if ( req_ptr->active_lock_mask != 0 )
  {
    if(app_info_ptr->perso_state == MMGSDI_PERSO_ENG_INIT_S)
    {
      app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S;
    }
    return MMGSDI_SUCCESS;
  }
  /* --------------------------------------------------------------------------
     If we get this far, this means that the indicators are not in Active State
     or in Autolock State.  This means that we are pretty much done with
     the Personalization Intitialization procedures
     ------------------------------------------------------------------------*/
  app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_analyze_indicators */


#ifdef FEATURE_SDCC_BOOT
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CHECK_EFS_STATUS

DESCRIPTION
  This function is used to check the EFS status. The intention of performing
  certain EFS operations and subsequently an efs_sync() before
  remotefs_get_server_status() is to make sure that we get a guaranteed
  answer if the EFS is ready.

DEPENDENCIES
  None

RETURN VALUE
  gsdi_returns_T
===========================================================================*/
static gsdi_returns_T gsdi_perso_engine_check_efs_status
(
  void
)
{
  /* If the EFS sync was already done, nothing to do, return success. The assumption
     here is that, if EFS was already successfully sync'd once, remote FS is ready*/
  if (mmgsdi_first_efs_sync_done)
  {
    return GSDI_SUCCESS;
  }

  /* For first time, dirty the EFS on purpose - create a 1 byte file & delete it */
  if (efs_put(MMGSDI_EFS_CHECK_TEMP_PATH, "0", 1, O_CREAT|O_AUTODIR, 0777) != 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: efs_put failed, errno: 0x%x",efs_errno);
    return GSDI_NOT_INIT;
  }

  /* Now, go ahead & delete it */
  if (efs_unlink(MMGSDI_EFS_CHECK_TEMP_PATH) != 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: efs_unlink failed, errno: 0x%x",efs_errno);
    return GSDI_NOT_INIT;
  }

  /* Do an EFS sync since EFS is dirty */
  if (efs_sync("/") != 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_1("PERSO: efs_sync failed, errno: 0x%x",efs_errno);
    return GSDI_NOT_INIT;
  }

  /* On success, update the global flag */
  mmgsdi_first_efs_sync_done = TRUE;

  return GSDI_SUCCESS;
} /* gsdi_perso_engine_check_efs_status */
#endif /* FEATURE_SDCC_BOOT */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CHECK_AND_RESTART_PERSO_FOR_SLOT

DESCRIPTION
  This function is used to restart the perso for requested slot when a slot 1 app
  reaches INIT_COMPLETED_S,
  If All active apps on slot 1 have reached INIT_COMPLETED_S Then, move
  Perso state of all non-slot 1 apps stuck in MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S
  to INIT_COMPLETED_S and send out corresponding Perso event.

DEPENDENCIES
  None

RETURN VALUE
 mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_check_and_restart_perso_for_slot(
  mmgsdi_slot_id_enum_type mmgsdi_slot
)
{
  boolean                        perso_init_incomplete      = FALSE;
  mmgsdi_session_info_type      *curr_session_ptr           = NULL;
  int32                          session_index              = 0;
  mmgsdi_slot_data_type         *slot_data_ptr              = NULL;
  boolean                        apps_active                = FALSE;

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(mmgsdi_slot);
  if(slot_data_ptr == NULL)
  {
    return MMGSDI_ERROR;
  }

  if (mmgsdi_client_id_reg_table[0] == NULL)
  {
    UIM_MSG_ERR_0("Client ID table index 0 is invalid");
    return MMGSDI_ERROR;
  }

  /* If slot doesn't have a card, NO need to check the app states and perso states of
     Apps active on slot1 and consequently updating the states of apps on slot.
  */
  if(slot_data_ptr->mmgsdi_state != MMGSDI_STATE_NO_CARD)
  {
    for (session_index = 0; session_index < MMGSDI_MAX_SESSION_INFO; session_index++)
    {
      curr_session_ptr = mmgsdi_client_id_reg_table[0]->session_info_ptr[session_index];
      if ((curr_session_ptr != NULL) &&
          (curr_session_ptr->channel_info_index < MMGSDI_MAX_CHANNEL_INFO) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index] != NULL) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr != NULL) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->slot_id == MMGSDI_SLOT_1))
      {
        if (mmgsdi_util_is_prov_session(curr_session_ptr->session_type))
        {
          /* If All active apps on slot 1 have reached INIT_COMPLETED_S Then, move
             Perso state of all the slot apps stuck in MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S
             to INIT_COMPLETED_S
          */
          if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state
               > MMGSDI_APP_STATE_DETECTED &&
             mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state
               <= MMGSDI_APP_STATE_ILLEGAL)
          {
            apps_active = TRUE;
            if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state
                 != MMGSDI_PERSO_ENG_INIT_COMPLETED_S)
            {
              perso_init_incomplete = TRUE;
            }
          }
        }
      }
    }
  }
  /* If All active apps on slot 1 have reached INIT_COMPLETED_S
     Then, move Perso state of all slot apps stuck in MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S
     State to INIT_COMPLETED_S and send out corresponding Perso event.
  */
  if(apps_active && !perso_init_incomplete)
  {
    for (session_index = 0; session_index < MMGSDI_MAX_SESSION_INFO; session_index++)
    {
      curr_session_ptr = mmgsdi_client_id_reg_table[0]->session_info_ptr[session_index];

      if((curr_session_ptr != NULL) &&
         (curr_session_ptr->channel_info_index < MMGSDI_MAX_CHANNEL_INFO) &&
         (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index] != NULL) &&
         (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr != NULL) &&
         (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->slot_id ==  mmgsdi_slot))
      {
        if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state
             == MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S &&
           mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state
             == MMGSDI_APP_STATE_READY_FOR_PERSO)
        {
          mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state
            = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
          UIM_MSG_HIGH_1("Perso. state for slot 0x%x is :  MMGSDI_PERSO_ENG_INIT_COMPLETED_S",
                     mmgsdi_slot);
          gsdi_perso_engine_notify_clients_of_perso_event(mmgsdi_slot,
                                                          GSDI_PERSO_EVT_INIT_COMPLETED,
                                                          mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr,
                                                          FALSE);
        }
      }
    }
  }
  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_check_and_restart_perso_for_slot */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_PERSO_FOR_SLOT_LOCKED_ON_SLOT1

DESCRIPTION
  This function is used to perso checks for non-slot1 slots.
  non-slot 1 Perso App. can use only 3 states:
  MMGSDI_PERSO_ENG_NOT_INIT_S,
  MMGSDI_PERSO_ENG_INIT_COMPLETED_S,
  MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S.

  If atleast one Perso feature is active and if
    - No Card in slot 1
    - No prov. apps active on slot1
    - Atleast one of active prov. app. on slot1 has not reached Perso State: INIT_COMPLETED_S
    Then, move Perso state of non-slot 1 app to MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S State.
  Else
    Move Perso state of the non-slot 1 app to INIT_COMPLETED_S and send out Perso event w.r.t the same.

DEPENDENCIES
  None

RETURN VALUE
 mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_perso_for_slot_locked_on_slot1(
  mmgsdi_session_id_type    session_id,
  mmgsdi_int_app_info_type* app_info_ptr,
  mmgsdi_slot_id_enum_type  mmgsdi_slot_id
)
{
  mmgsdi_slot_data_type              *slot1_data_ptr          = NULL;
  boolean                             all_prov_perso_verified = TRUE;
  boolean                             apps_active             = FALSE;
  mmgsdi_session_info_type           *curr_session_ptr        = NULL;
  gsdi_perso_event_enum_type          perso_event             = GSDI_PERSO_NO_EVENT;
  int32                               session_index           = 0;
  boolean                             feature_active          = FALSE;
  uint32                              index                   = 0;
  mmgsdi_return_enum_type             mmgsdi_status           = MMGSDI_ERROR;
  boolean                             slot_perso_completed    = FALSE;
  int32                               ret_data_len            = 0;
  gsdi_perso_get_feature_ind_req_T    get_feature_ind_req;
  uint8                               ret_data[GSDI_PERSO_NUM_OF_SIM_FEATURES
                                                   + GSDI_PERSO_NUM_OF_RUIM_FEATURES];

  memset(&get_feature_ind_req, 0x00, sizeof(gsdi_perso_get_feature_ind_req_T));

  mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                   NULL,
                                                   MMGSDI_MAX_SLOT_ID_ENUM,
                                                   &get_feature_ind_req,
                                                   GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS_REQ,
                                                   ret_data,
                                                   sizeof(ret_data),
                                                   &ret_data_len,
                                                   &perso_event);
  if (mmgsdi_status != MMGSDI_SUCCESS )
  {
    UIM_MSG_ERR_1("PERSO: PERSO GET ALL FEATURE INDS FAILED: 0x%x", mmgsdi_status);

    return mmgsdi_status;
  }

  /* Check if any feature is active */
  while(index < ret_data_len)
  {
    if( (ret_data[index] == (byte)GSDI_PERSO_ENG_ACTIVE_STATE)  ||
        (ret_data[index] == (byte)GSDI_PERSO_ENG_AUTOLOCK_STATE) )
    {
      feature_active = TRUE;
      break;
    }
    index++;
  }

  if(feature_active == TRUE)
  {
    slot1_data_ptr = mmgsdi_util_get_slot_data_ptr(MMGSDI_SLOT_1);
    if(slot1_data_ptr == NULL)
    {
      return MMGSDI_ERROR;
    }
    /* If slot_1 has No card then move the non-slot 1 perso state
       to MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S state */
    if(slot1_data_ptr->mmgsdi_state == MMGSDI_STATE_NO_CARD)
    {
      UIM_MSG_HIGH_0("PERSO: Error occurred ,NO_CARD on slot_1");

      app_info_ptr->perso_state =  MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S;
      UIM_MSG_HIGH_0("Perso. state for slot_2 is : MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S");
      return MMGSDI_SUCCESS;
    }
    /* In following scenarios:
       - No apps active on slot1
       - Atleast one of active app. on slot1 has not reached Perso State: INIT_COMPLETED_S
         Move Perso state of non-slot 1 app to MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S State.*/
    if (mmgsdi_client_id_reg_table[0] == NULL)
    {
      UIM_MSG_ERR_0("Client ID table index 0 is invalid");
      return MMGSDI_ERROR;
    }
    for (session_index = 0; session_index < MMGSDI_MAX_SESSION_INFO; session_index++)
    {
      curr_session_ptr =
        mmgsdi_client_id_reg_table[0]->session_info_ptr[session_index];
      if ((curr_session_ptr != NULL) &&
          (curr_session_ptr->channel_info_index < MMGSDI_MAX_CHANNEL_INFO) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index] != NULL) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr != NULL) &&
          (mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->slot_id ==  MMGSDI_SLOT_1))
      {
        if (mmgsdi_util_is_prov_session(curr_session_ptr->session_type))
        {
          if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state
              > MMGSDI_APP_STATE_DETECTED &&
             mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->app_state
              <= MMGSDI_APP_STATE_ILLEGAL)
          {
            apps_active = TRUE;
            if(mmgsdi_channel_info_ptr_table[curr_session_ptr->channel_info_index]->app_info_ptr->perso_state
                 != MMGSDI_PERSO_ENG_INIT_COMPLETED_S)
            {
              all_prov_perso_verified = FALSE;
            }
          }
        }
      }
    }

    if(apps_active && all_prov_perso_verified )
    {
      UIM_MSG_HIGH_0("All Prov. apps on SLOT_1 are in perso_verified state");
      slot_perso_completed = TRUE;
    }
    else
    {
      UIM_MSG_HIGH_0("Either no app on slot_1 is active or at least 1 app is in pre perso verified state");
      app_info_ptr->perso_state =  MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S;
      UIM_MSG_HIGH_1("Perso. state for slot 0x%x is : MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S",
                 mmgsdi_slot_id);
      return MMGSDI_SUCCESS;
    }
  }
  else
  {
    UIM_MSG_HIGH_1("No Features are active: moving slot 0x%x to MMGSDI_PERSO_ENG_INIT_COMPLETED_S",
               mmgsdi_slot_id);
    slot_perso_completed = TRUE;
  }

  if(slot_perso_completed == TRUE)
  {
    app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
    UIM_MSG_HIGH_1("Perso. state for slot 0x%x is : MMGSDI_PERSO_ENG_INIT_COMPLETED_S",
               mmgsdi_slot_id);
    gsdi_perso_engine_notify_clients_of_perso_event(mmgsdi_slot_id,
                                                    GSDI_PERSO_EVT_INIT_COMPLETED,
                                                    app_info_ptr,
                                                    FALSE);
  }
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_perso_for_slot_locked_on_slot1 */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_MAIN

DESCRIPTION
  This function is called after the Verification procedures have occurred on
  PIN1.

  POSSIBLE STATES:
  MMGSDI_PERSO_ENG_NOT_INIT_S         -> MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S
  MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S -> MMGSDI_PERSO_ENG_INIT_AUTOLOCK_S
  MMGSDI_PERSO_ENG_INIT_AUTOLOCK_S    -> MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S
  MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S -> MMGSDI_PERSO_ENG_INIT_S

  or
  MMGSDI_PERSO_ENG_NOT_INIT_S         -> MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S
  MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S -> MMGSDI_PERSO_ENG_INIT_S

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_main(
  mmgsdi_session_id_type     session_id,
  const void *               cmd_ptr,
  byte                       msg_id,
  uint8 *                    ret_data_ptr,
  uint32                     ret_data_max_len,
  int32 *                    ret_data_len_ptr
)
{
  gsdi_perso_internal_command_req_T   req;
  gsdi_perso_event_enum_type          perso_event  = GSDI_PERSO_NO_EVENT;
  mmgsdi_int_app_info_type           *app_info_ptr = NULL;
  mmgsdi_slot_id_enum_type            slot         = MMGSDI_MAX_SLOT_ID_ENUM;
  mmgsdi_return_enum_type             mmgsdi_status= MMGSDI_ERROR;

  /* Process commands which do not require any data from the card */
  if (msg_id == GSDI_PERSO_ACT_FEATURE_IND_REQ   ||
      msg_id == GSDI_PERSO_DEACT_FEATURE_IND_REQ    ||
      msg_id == GSDI_PERSO_GET_FEATURE_IND_REQ  ||
      msg_id == GSDI_PERSO_SET_FEATURE_DATA_REQ   ||
      msg_id == GSDI_PERSO_OTA_DERPERSO_REQ    ||
      msg_id == GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ   ||
      msg_id == GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ ||
      msg_id == GSDI_PERSO_GET_PERM_FEATURE_IND_REQ ||
      msg_id == GSDI_PERSO_GET_FEATURE_KEY_REQ ||
      msg_id == GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ ||
      msg_id == GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ ||
      msg_id == GSDI_PERSO_GET_FEATURE_DATA_REQ ||
      msg_id == GSDI_PERSO_LOCK_DOWN_REQ)
  {
    if (cmd_ptr == NULL)
    {
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: cmd_ptr is NULL, cannot process command");
      return MMGSDI_ERROR;
    }

#ifdef FEATURE_SDCC_BOOT
    /* Proceed with Perso requests which write to EFS only when remote EFS
       server is running. When remote EFS server is not running, the writes
       do not get updated in the FS, which can expose security vulnerabilities.
       For ex: User can potentially attempt a Perso feature deactivation
       operation infinite number of times (by power-cycling the phone after
       running out of retry attempts and before Remote EFS server becomes
       available) */
    if (msg_id == GSDI_PERSO_ACT_FEATURE_IND_REQ   ||
        msg_id == GSDI_PERSO_DEACT_FEATURE_IND_REQ   ||
        msg_id == GSDI_PERSO_SET_FEATURE_DATA_REQ   ||
        msg_id == GSDI_PERSO_OTA_DERPERSO_REQ   ||
        msg_id == GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ   ||
        msg_id == GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ   ||
        msg_id == GSDI_PERSO_LOCK_DOWN_REQ)
    {
      if (gsdi_perso_engine_check_efs_status() != GSDI_SUCCESS)
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: EFS problem, cannot process req: 0x%x",
                                 msg_id);
        return MMGSDI_NOT_INIT;
      }
    }
#endif /* FEATURE_SDCC_BOOT */

    /* ----------------------------------------------------------------------
       Process the command provided
       --------------------------------------------------------------------*/
    mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                     NULL,
                                                     MMGSDI_MAX_SLOT_ID_ENUM,
                                                     cmd_ptr,
                                                     msg_id,
                                                     ret_data_ptr,
                                                     ret_data_max_len,
                                                     ret_data_len_ptr,
                                                     &perso_event);

    if ( mmgsdi_status == MMGSDI_ERROR )
    {
      gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                      GSDI_PERSO_SANITY_ERROR,
                                                      NULL,
                                                      TRUE);
      UIM_MSG_ERR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x : msg_id 0x%x",
                    mmgsdi_status, msg_id);
      return MMGSDI_ERROR;
    }

    if ( perso_event != GSDI_PERSO_NO_EVENT )
    {
      gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                      perso_event,
                                                      NULL,
                                                      TRUE);
    }
    return mmgsdi_status;
  }

  /* Based on the session ID, retrieve the prov index and slot */
  mmgsdi_status = mmgsdi_util_get_prov_session_info(session_id,
                                                    NULL,
                                                    &slot,
                                                    &app_info_ptr);

  if ((mmgsdi_status != MMGSDI_SUCCESS) || (app_info_ptr == NULL))
  {
    return MMGSDI_ERROR;
  }

  if ((slot != MMGSDI_SLOT_1) &&
      (slot != MMGSDI_SLOT_2) &&
      (slot != MMGSDI_SLOT_3))
  {
    return MMGSDI_ERROR;
  }

  /* ----------------------------------------------------------------------
    Check the cmd_ptr.  Personalization Commands are not allowed in this
    state.
    No Cmd ptr is provided when perso has not yet started.
  --------------------------------------------------------------------*/
  if ( cmd_ptr == NULL &&
       app_info_ptr->perso_state != MMGSDI_PERSO_ENG_NOT_INIT_S )
  {
    UIM_MSG_ERR_1("PERSO: Cannot process command in state 0x%x",
                  app_info_ptr->perso_state);
    return MMGSDI_ERROR;
  }

  UIM_MSG_HIGH_1("PERSO: MMGSDI PERSO ENG MAIN: Perso state 0x%x",
                 app_info_ptr->perso_state);

  /* --------------------------------------------------------------------------
     Check the Personalization State of MMGSDI.
     ------------------------------------------------------------------------*/
  switch ( app_info_ptr->perso_state )
  {
    case MMGSDI_PERSO_ENG_NOT_INIT_S:
       UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_NOT_INIT_S");

      /* perso_ability is initialized in gsdi_perso_engine_initialize_data
         but it will not  be  used for this feature as perso for slot_2 and slot_3  is
         locked on slot_1.
      */
      if((mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1) == MMGSDI_FEATURE_ENABLED) &&
         (slot == MMGSDI_SLOT_2 ||
          slot == MMGSDI_SLOT_3))
      {
        mmgsdi_status = gsdi_perso_engine_perso_for_slot_locked_on_slot1(session_id, app_info_ptr, slot);
        if (mmgsdi_status != MMGSDI_SUCCESS )
        {
          UIM_MSG_ERR_2("PERSO: PERSO FOR SLOT 0x%x FAILED: 0x%x",
                        slot, mmgsdi_status);

          return mmgsdi_status;
        }
        break;
      }
      else if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_NO_PERSONALIZATION_ON_ADDITIONAL_SLOTS) ==
              MMGSDI_FEATURE_ENABLED && (slot != MMGSDI_SLOT_1) )
      {
        /* Since there is no personalization feature for any additional slots,
           move the perso_state to MMGSDI_PERSO_ENG_INIT_COMPLETED_S and
           notify clients */
        app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
        UIM_MSG_HIGH_1("Perso. state for slot 0x%x is : MMGSDI_PERSO_ENG_INIT_COMPLETED_S",
                       slot);
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_EVT_INIT_COMPLETED,
                                                        app_info_ptr,
                                                        FALSE);

        return MMGSDI_SUCCESS;
      }
      else
      {
        mmgsdi_status = gsdi_perso_engine_initialize_data(session_id);
        if (mmgsdi_status != MMGSDI_SUCCESS )
        {
          MMGSDI_DEBUG_MSG_ERROR_1("PERSO: PERSO INITIALIZE DATA FAILED: 0x%x",
                                   mmgsdi_status);
          /* error return will take care of deactivating current session */
          return mmgsdi_status;
        }

        /* ----------------------------------------------------------------------
           Queue an internal command to process in the INIT S so that the
           Engine can retrieve the Lock Indicators
           --------------------------------------------------------------------*/
        mmgsdi_status = gsdi_perso_engine_build_and_queue_init_proc_req(
                          session_id, &req);
        break;
      }
    case MMGSDI_PERSO_ENG_INIT_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_INIT_S");
      /* ------------------------------------------------------------------------
         Now perform the checks for all locks.
         If GSDI_STATUS == GSDI_ERROR, this is a Procedural failure while trying
         to get to the security library.  Need to go to NO Card.
         Otherwise, check the event generated by the procedure and send to the
         clients if set.
         ----------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_get_all_locks(session_id,
                                                      app_info_ptr,
                                                      slot,
                                                      ret_data_len_ptr,
                                                      ret_data_ptr,
                                                      ret_data_max_len,
                                                      &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        UIM_MSG_ERR_0("PERSO: Could not get all the locks");

        /* Update the session closed cause. The session changed event
           broadcasts the cause */
        app_info_ptr->session_closed_cause = MMGSDI_SESSION_CLOSED_CAUSE_PERSO_OPERATION_FAIL;
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        FALSE);
      }

      /* ----------------------------------------------------------------------
         Pass all information to the analyze indicators function.  It will
         do a couple of things.
         1.  Analyze the Feature Indicators provided to ensure they are valid
         2.  Once valid, the req_ptr is populated to set the feature mask.
         3.  The next state is then determined.
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_analyze_indicators(app_info_ptr,
                                                           ret_data_ptr,
                                                           ret_data_len_ptr,
                                                           &req);
      if ( mmgsdi_status != MMGSDI_SUCCESS )
      {
        UIM_MSG_ERR_1("PERSO: Failed to Analyze Locks, status = 0x%x",
                      mmgsdi_status);

        /* Update the session closed cause. The session changed event
           broadcasts the cause */
        app_info_ptr->session_closed_cause = MMGSDI_SESSION_CLOSED_CAUSE_PERSO_OPERATION_FAIL;
        /* --------------------------------------------------------------------
           Failed to analyze the lock types, i.e. may be corrupted
           Move the state to Error State
           ------------------------------------------------------------------*/
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
      }

      /* ----------------------------------------------------------------------
           Set the rest of the req_ptr data and Queue to the head of the
           MMGSDI Task Command Queue
           --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_build_and_queue_init_proc_req(
                        session_id, &req);
      break;

    case MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_INIT_CHECK_LOCKS_S");
      /* ----------------------------------------------------------------------
         Check the cmd_ptr.  if  NULL...this is an indication that there is
         a problem somewhere.
         --------------------------------------------------------------------*/
      if ( cmd_ptr == NULL )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Cannot process NULL Command",
                                 app_info_ptr->perso_state);
        return MMGSDI_ERROR;
      }

      req.active_lock_mask =
               (((gsdi_perso_internal_command_req_T*)cmd_ptr)->active_lock_mask);
      msg_id = GSDI_PERSO_ENGINE_CHECK_ALL_LOCKS_REQ;

      /* ----------------------------------------------------------------------
        Send the command to the Perso Engine Handler.  If an error occurs...
        exit out immediately and go to Error State.

        Otherwise, determine if an event was generated by the Library and
        notify the clients.
      --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       &req,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Error occurred while doing a Check Locks");

        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        return MMGSDI_ERROR;
      }
      /* If Check lock operation failed due to a Card specific issue,
         notify Perso Sanity Error only on corresponding provisioning
         Session, Card Error only on the corresponding slot */
      else if( mmgsdi_status == MMGSDI_PERSO_CARD_DATA_ERROR )
      {
        UIM_MSG_ERR_0("PERSO: Card specific error occurred while doing Check Locks");

        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        app_info_ptr,
                                                        FALSE);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        FALSE);
      }

      /* An Event was not generated or did not change state...therefore,
         if successful move to the next state.  */
      if ( (mmgsdi_status == MMGSDI_SUCCESS) &&
           (perso_event == GSDI_PERSO_NO_EVENT) )
      {
        /* Reset the status to MMGSDI_SUCCESS */
        mmgsdi_status = MMGSDI_SUCCESS;
        app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_COMPLETED_S;
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_EVT_INIT_COMPLETED,
                                                        app_info_ptr,
                                                        FALSE);

        if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1) == MMGSDI_FEATURE_ENABLED &&
           slot == MMGSDI_SLOT_1)
        {
          mmgsdi_status = gsdi_perso_engine_check_and_restart_perso_for_slot(MMGSDI_SLOT_2);
          if (mmgsdi_status != MMGSDI_SUCCESS )
          {
            UIM_MSG_ERR_1("PERSO: RESTART PERSO FOR SLOT_2 FAILED: 0x%x",
                          mmgsdi_status);

            return mmgsdi_status;
          }
          mmgsdi_status = gsdi_perso_engine_check_and_restart_perso_for_slot(MMGSDI_SLOT_3);
          if (mmgsdi_status != MMGSDI_SUCCESS )
          {
            UIM_MSG_ERR_1("PERSO: RESTART PERSO FOR SLOT_3 FAILED: 0x%x",
                          mmgsdi_status);

            return mmgsdi_status;
          }
        }
      }
      break;

    case MMGSDI_PERSO_ENG_INIT_AUTOLOCK_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_INIT_AUTOLOCK_S");
      msg_id = GSDI_PERSO_ENGINE_CHECK_AUTOLOCKS_REQ;

     /* ----------------------------------------------------------------------
         Check the cmd_ptr.  if  NULL...this is an indication that there is
         a problem somewhere.
         --------------------------------------------------------------------*/
      if ( cmd_ptr == NULL )
      {
        MMGSDI_DEBUG_MSG_ERROR_1("PERSO: Cannot process NULL Command",
                                 app_info_ptr->perso_state);
        return MMGSDI_ERROR;

      }

      req.autolock_mask = ((gsdi_perso_internal_command_req_T*)cmd_ptr)->autolock_mask;
      req.message_header.message_id = msg_id;

      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       &req,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        UIM_MSG_ERR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x msg_id 0x%x",
                      mmgsdi_status,msg_id);
        return MMGSDI_ERROR;
      }
      /* If Autlock operation failed due to a Card specific issue,
         notify Perso Sanity Error only on corresponding provisioning
         Session, Card Error only on the corresponding slot */
      else if( mmgsdi_status == MMGSDI_PERSO_CARD_DATA_ERROR )
      {
        UIM_MSG_ERR_0("PERSO: Card specific error occurred while doing Autolock");

        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        app_info_ptr,
                                                        FALSE);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        FALSE);
      }

      if ( mmgsdi_status == MMGSDI_SUCCESS             ||
           mmgsdi_status == MMGSDI_PERSO_PERM_DISABLED )
      {

        app_info_ptr->perso_state = MMGSDI_PERSO_ENG_INIT_S;
        /* ----------------------------------------------------------------------
           Set the rest of the req_ptr data and Queue to the head of the
           MMGSDI Task Command Queue
          --------------------------------------------------------------------*/
        mmgsdi_status = gsdi_perso_engine_build_and_queue_init_proc_req(
                          session_id, &req);
      }
      else
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
      }
      break;

    case MMGSDI_PERSO_ENG_INIT_COMPLETED_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_INIT_COMPLETED_S");
      /* ---------------------------------------------------------------
         This state indicates that the Personalization procedures are now
         active.  The mobile will perform according to the personalization
         indicators.
         -------------------------------------------------------------*/
      if (((gsdi_perso_internal_command_req_T *)cmd_ptr)->message_header.message_id ==
          GSDI_PERSO_INT_PROC_REQ)
      {
        /* An Internal Command is taking us to Init Completed which
        ** is done as a result of the initialization procedures
        ** which signals the end of the Perso Init Procedures
        */
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_EVT_INIT_COMPLETED,
                                                        app_info_ptr,
                                                        FALSE);

        if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1) == MMGSDI_FEATURE_ENABLED &&
           slot == MMGSDI_SLOT_1)
        {
          mmgsdi_status = gsdi_perso_engine_check_and_restart_perso_for_slot(MMGSDI_SLOT_2);
          if (mmgsdi_status != MMGSDI_SUCCESS )
          {
            UIM_MSG_ERR_1("PERSO: RESTART PERSO FOR SLOT_2 FAILED: 0x%x", mmgsdi_status);

            return mmgsdi_status;
          }
          mmgsdi_status = gsdi_perso_engine_check_and_restart_perso_for_slot(MMGSDI_SLOT_3);
          if (mmgsdi_status != MMGSDI_SUCCESS )
          {
            UIM_MSG_ERR_1("PERSO: RESTART PERSO FOR SLOT_3 FAILED: 0x%x", mmgsdi_status);

            return mmgsdi_status;
          }
        }

        return MMGSDI_SUCCESS;
      }

      /* ----------------------------------------------------------------------
         Process the command provided in the init completed state
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       cmd_ptr,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        MMGSDI_DEBUG_MSG_ERROR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x msg_id 0x%x",
                                 mmgsdi_status,msg_id);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        FALSE);
      }
      break;

    case MMGSDI_PERSO_ENG_INIT_ERROR_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_INIT_ERROR_S");

      /* ----------------------------------------------------------------------
         Send the command to the Perso Engine Handler
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       cmd_ptr,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        UIM_MSG_ERR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x msg_id:",
                      mmgsdi_status, msg_id);
        return MMGSDI_ERROR;
      }
      break;

    case MMGSDI_PERSO_ENG_WAIT_FOR_DEPERSO_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_WAIT_FOR_DEPERSO_S");

      if ( cmd_ptr == NULL )
      {
        MMGSDI_DEBUG_MSG_ERROR_0("PERSO: NULL pointer in gsdi_perso_engine_main");
        return MMGSDI_ERROR;
      }

      /* Do not process Internal Commands in this state */
      if (((gsdi_perso_internal_command_req_T *)cmd_ptr)->message_header.message_id ==
            GSDI_PERSO_INT_PROC_REQ)
      {
        /* An Internal Command is taking us to Init Completed which
        ** is done as a result of the initialization procedures
        ** which signals the end of the Perso Init Procedures
        */
        return MMGSDI_SUCCESS;
      }

      /* ---------------------------------------------------------------
         This state indicates that the Personalization procedures are now
         active.  The mobile will perform according to the personalization
         indicators.
         -------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       cmd_ptr,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        UIM_MSG_ERR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x msg_id",
                      mmgsdi_status,msg_id);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        FALSE);
      }
      break;

    case MMGSDI_PERSO_ENG_REFRESH_S:
      UIM_MSG_HIGH_0("PERSO: MMGSDI_PERSO_ENG_REFRESH_S");

      if(msg_id != GSDI_PERSO_ENGINE_SIM_REFRESH_REQ)
      {
        UIM_MSG_ERR_1("Cannot handle msg: 0x%x in Perso REFRESH state",
                      msg_id);
        return MMGSDI_ERROR;

      }
      /* ----------------------------------------------------------------------
         Process the command provided in the Perso refresh state
         --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_handle_request(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       cmd_ptr,
                                                       msg_id,
                                                       ret_data_ptr,
                                                       ret_data_max_len,
                                                       ret_data_len_ptr,
                                                       &perso_event);
      if ( mmgsdi_status == MMGSDI_ERROR )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(MMGSDI_SLOT_1,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        NULL,
                                                        TRUE);
        MMGSDI_DEBUG_MSG_ERROR_2("PERSO: gsdi_perso_engine_handle_request failed 0x%x msg_id 0x%x",
                                 mmgsdi_status,msg_id);
        return MMGSDI_ERROR;
      }
      /* If Peros REFRESH operation failed due to a Card specific issue,
         notify Perso Sanity Error only on corresponding provisioning
         Session, Card Error only on the corresponding slot */
      else if( mmgsdi_status == MMGSDI_PERSO_CARD_DATA_ERROR )
      {
        UIM_MSG_ERR_0("PERSO: Card specific error occurred while doing Perso REFRESH");

        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        GSDI_PERSO_SANITY_ERROR,
                                                        app_info_ptr,
                                                        FALSE);
        return MMGSDI_ERROR;
      }

      if ( perso_event != GSDI_PERSO_NO_EVENT )
      {
        gsdi_perso_engine_notify_clients_of_perso_event(slot,
                                                        perso_event,
                                                        app_info_ptr,
                                                        TRUE);
      }
      /* If no activity happened (probably because no keys were
         present in EF-DCK), start the regular Perso initizalization */
      if(app_info_ptr->perso_state != MMGSDI_PERSO_ENG_NOT_INIT_S)
      {
        app_info_ptr->perso_state = MMGSDI_PERSO_ENG_NOT_INIT_S;

        /* ----------------------------------------------------------------------
           Queue an internal command to process in the INIT S so that the
           Engine can retrieve the Lock Indicators
         --------------------------------------------------------------------*/
        mmgsdi_status = gsdi_perso_engine_build_and_queue_init_proc_req(
                          session_id, &req);
      }
      break;

      case MMGSDI_PERSO_ENG_WAIT_FOR_SLOT1_S:
        if(mmgsdi_nv_get_feature_status(MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1) == MMGSDI_FEATURE_ENABLED)
        {
          /* Nothing to do here */
          break;
        }
        /* else Allow Fallthrough to default case*/

    default:
      UIM_MSG_ERR_1("PERSO: Unknown Perso State: 0x%x",app_info_ptr->perso_state);
      return MMGSDI_ERROR;
  }
  return mmgsdi_status;
} /* gsdi_perso_engine_main */


/*===========================================================================
FUNCTION GSDI_PERSO_FS_DIR_TEST

DESCRIPTION
  This function is used to test whether the directory already exists
  in FS.

DEPENDENCIES
  None

RETURN VALUE
gsdi_returns_T:  GSDI_SUCCES:    Directory present
                 GSDI_ERROR:     FS error or illegal param
                 GSDI_NOT_FOUND: Directory missing
===========================================================================*/
static gsdi_returns_T gsdi_perso_engine_fs_dir_test(
  const char * dir_to_test
)
{
  gsdi_returns_T gsdi_status = GSDI_ERROR;

  if (dir_to_test == NULL)
  {
    return GSDI_ERROR;
  }

  gsdi_status = gsdi_efs_name_test(dir_to_test); /* Pointer to Directory Name */

  if (gsdi_status == GSDI_SUCCESS)
  {
    return GSDI_SUCCESS;
  }
  else
  {
    return GSDI_NOT_FOUND;
  }

}/* gsdi_perso_engine_fs_dir_test */


#ifndef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_PERSO_SIM
/*===========================================================================
FUNCTION GSDI_PERSO_FS_FILE_TEST

DESCRIPTION
  This function is used to test whether the file already exists
  in FS.

DEPENDENCIES
  None

RETURN VALUE
  void
===========================================================================*/
static boolean gsdi_perso_engine_fs_file_test(
  const char * file_to_test
)
{
  gsdi_returns_T gsdi_status = GSDI_ERROR;

  gsdi_status = gsdi_efs_name_test(file_to_test); /* Pointer to File Name */

  if ( gsdi_status == GSDI_SUCCESS )
    return TRUE;
  else
    return FALSE;
}/*gsdi_perso_engine_fs_file_test*/
#endif /*FEATURE_PERSO_SIM*/


/*===========================================================================
FUNCTION GSDI_PERSO_ARE_ALL_FILES_PRESENT

DESCRIPTION
  This function is used to verify all files required for use in
  personalization solution are present in the EFS.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type:
                 MMGSDI_SUCCESS:   Required files are present
                 MMGSDI_ERROR:     Problem finding the required files
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_are_all_files_present(
  void
)
{
#if defined(FEATURE_PERSO_SIM)
  /* --------------------------------------------------------------------------
     Determine if the Feature Indicators File was created
     ------------------------------------------------------------------------*/
  if (TRUE != gsdi_perso_engine_fs_file_test(mmgsdi_efs_perso_file))
  {
      UIM_MSG_ERR_0("EFS PERSO_FILE MISSING");
      return MMGSDI_ERROR;
  }
#endif /* FEATURE_PERSO_SIM */

#if defined(FEATURE_PERSO_RUIM)
  if (TRUE != gsdi_perso_engine_ruim_fs_file_test())
  {
    UIM_MSG_ERR_0("EFS RUIM PERSO_FILE MISSING");
    return MMGSDI_ERROR;
  }
#endif /* FEATURE_PERSO_RUIM */

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_are_all_files_present */
#endif /* !FEATURE_MMGSDI_PERSO_SFS */


/*===========================================================================
FUNCTION GSDI_PERSO_CREATE_DIR

DESCRIPTION
  This function is used to create the directories.  This function
  should only be called once during the personalization phase.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type:  MMGSDI_SUCCESS : Directories were created
                          MMGSDI_ERROR   : Error creating directories
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_create_dir(void)
{
#if defined(FEATURE_MMGSDI_PERSO_SFS) && defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && (!defined(FEATURE_PERSO_SIM) || !defined(FEATURE_PERSO_RUIM))
  return MMGSDI_SUCCESS;
#else
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;

  /* --------------------------------------------------------------------------
     We now have to create the file Structure since this is the first
     time it is being created.  Once created, it will exist permanently
     ------------------------------------------------------------------------*/
  /* --------------------------------------------------------------------------
     Create the directory for the first file.
     Note: If root MMGSDI directory is already present, skip creation step
     ------------------------------------------------------------------------*/
  if (gsdi_perso_engine_fs_dir_test(mmgsdi_efs_mmgsdi_dir) != GSDI_SUCCESS)
  {
    mmgsdi_status = gsdi_efs_mkdir(mmgsdi_efs_mmgsdi_dir);
    if ( mmgsdi_status != MMGSDI_SUCCESS )
    {
      MMGSDI_DEBUG_MSG_ERROR_0("Could not create MMGSDI directory");
      return mmgsdi_status;
    }
  }

  /* --------------------------------------------------------------------------
     Create the directory for the Perso Dir
     ------------------------------------------------------------------------*/
#if defined(FEATURE_MMGSDI_PERSO_SFS)
  /* Either SIM or RUIM perso files need to be housed in the SFS directory, so,
     create the perso_sfs directory */
  mmgsdi_status = mmgsdi_sfs_mkdir(mmgsdi_sfs_perso_dir);
#else
  mmgsdi_status = gsdi_efs_mkdir(mmgsdi_efs_perso_dir);
  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("Could not create Perso directory");
  }
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  return mmgsdi_status;
#endif /* FEATURE_MMGSDI_PERSO_SFS && FEATURE_MMGSDI_PERSO_SFP_AVAILABLE && (!FEATURE_PERSO_SIM || !FEATURE_PERSO_RUIM)*/
} /* gsdi_perso_engine_create_dir */


#if !defined(FEATURE_UIM_TEST_FRAMEWORK)
/*===========================================================================
FUNCTION GSDI_PERSO_CREATE_FILE

DESCRIPTION
  This function is used to create the directories and files.  This function
  should only be called once during the personalization phase.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_create_file(void)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;

  /* --------------------------------------------------------------------------
     Create the Perso File
     ------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_PERSO_SFS
#if defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE)
  /* SFP is enabled. No action reqd. */
#else
  /* SFP is disabled. Create SIM perso file on SFS */
  (void) mmgsdi_sfs_rmfile(mmgsdi_sfs_perso_file);
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#else
  mmgsdi_status = gsdi_efs_create_file(mmgsdi_efs_perso_file);
  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("Could not create Perso File");
  }
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  return mmgsdi_status;
}/* gsdi_perso_engine_create_file */
#endif /* !FEATURE_UIM_TEST_FRAMEWORK*/


/*===========================================================================
FUNCTION gsdi_perso_engine_set_mem_data

DESCRIPTION
  This function is used to create all of the relevant Personalization Files
  used for storing information in EFS.

  This procedure is ran every time the mobile is powered up, but will not
  recreate any files already created.

  If all personalization files are not created properly or upon entering
  this function again, only some of the files exist...then an Error Fatal
  will occur.  This could be evidence of tampering.

  It will be acceptable for the file to be empty.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_set_mem_data
(
  gsdi_perso_sec_me_data_type    *enc_data,
  uint8                         **memory_pptr
)
{
  gsdi_perso_engine_mem_pointer_type mem_pointer;
  uint32 total_len;

  MMGSDIUTIL_RETURN_IF_NULL_2(enc_data, memory_pptr);

  memset(enc_data, 0x00, sizeof(gsdi_perso_sec_me_data_type));

  /*------------------------------------------------------------------------
    Feature Inds
  --------------------------------------------------------------------------*/
  enc_data->feat_inds.len = GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS * 2;
  total_len = enc_data->feat_inds.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    Perm Feature Inds
  --------------------------------------------------------------------------*/
  enc_data->perm_feat_inds.len = GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS * 2;
  total_len += enc_data->perm_feat_inds.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    SIM Lock Codes
  --------------------------------------------------------------------------*/
  enc_data->sim_lock_codes.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE) *
                                     GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS;
  total_len += enc_data->sim_lock_codes.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    DCK
  --------------------------------------------------------------------------*/
  enc_data->dcks.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE) *
                                    GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS;
  enc_data->dcks.len += GSDI_PERSO_MAX_CK * 2* GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS;
  total_len +=  enc_data->dcks.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    DCK Counter
  --------------------------------------------------------------------------*/
  enc_data->dck_counts.len = GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS * 2;
  total_len += enc_data->dck_counts.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    UNBLOCK CK
  --------------------------------------------------------------------------*/
  enc_data->unblock_cks.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE) * GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS;
  enc_data->unblock_cks.len += GSDI_PERSO_MAX_CK * 2* GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS;
  total_len +=  enc_data->unblock_cks.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);


  /*------------------------------------------------------------------------
    UNBLOCK CK Counter
  --------------------------------------------------------------------------*/
  enc_data->unblock_ck_counts.len = GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS * 2;
  total_len += enc_data->unblock_ck_counts.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    Lock down
  --------------------------------------------------------------------------*/
  enc_data->lock_down.len = sizeof(uint8) * 2;
  total_len += enc_data->lock_down.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

 /* ---------------------------------------------------------------------------
     Allocate the entire buffer required for the encrypted data structure
     Memset it to 0x00 when successful
    -------------------------------------------------------------------------*/
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(mem_pointer.buffer_start, total_len)
  MMGSDIUTIL_RETURN_IF_NULL(mem_pointer.buffer_start);
  mem_pointer.curr_offset  = 0;
  mem_pointer.buffer_size  = total_len;


  /* free this memory at the calling end */
  *memory_pptr = mem_pointer.buffer_start;

  /*-------------------------------------------------------------------------
    Set the Feat Ind Buffer Pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  enc_data->feat_inds.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->feat_inds.len;

  /*-------------------------------------------------------------------------
    Set the Perm Feat Ind Buffer Pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->perm_feat_inds.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->perm_feat_inds.len;

  /*-------------------------------------------------------------------------
    Set the SIM Lock Code Buffer Pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->sim_lock_codes.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->sim_lock_codes.len;

  /*-------------------------------------------------------------------------
    Set the DCK Memory Buffer pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->dcks.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->dcks.len;

  /*-------------------------------------------------------------------------
    Set the DCK Counter Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->dck_counts.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->dck_counts.len;


  /*-------------------------------------------------------------------------
    Set the UNblock CK Memory Buffer pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->unblock_cks.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->unblock_cks.len;


  /*-------------------------------------------------------------------------
    Set the Unblock CK Counter Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->unblock_ck_counts.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->unblock_ck_counts.len;


  /*-------------------------------------------------------------------------
    Set the Lock down Buffer Pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->lock_down.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->lock_down.len;

  /*-------------------------------------------------------------------------
    Check memory usage
    -------------------------------------------------------------------------*/
  if (mem_pointer.curr_offset != mem_pointer.buffer_size)
  {
    MMGSDIUTIL_TMC_MEM_FREE(*memory_pptr);
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Memory Corruption!");
    return MMGSDI_ERROR;
  }

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_set_mem_data */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CREATE_UMTS_DATA

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_create_umts_data(
  void
)
{
  gsdi_perso_sec_me_data_type        encrypted_data;
  mmgsdi_return_enum_type            mmgsdi_status = MMGSDI_SUCCESS;
  uint8                             *memory_ptr    = NULL;

  memset(&encrypted_data, 0x00, sizeof(gsdi_perso_sec_me_data_type));

  UIM_MSG_HIGH_0("PERSO: In gsdi_perso_engine_create_umts_data");

  /* --------------------------------------------------------------------------
    The Perso Engine Has an Interface to allow the Personalization
    Engine to determine how much memory needs to be allocated.
  ------------------------------------------------------------------------*/
  mmgsdi_status = gsdi_perso_engine_set_mem_data(&encrypted_data, &memory_ptr);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get total memory required");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = gsdi_perso_security_create_data(&encrypted_data);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not encrypt data");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return mmgsdi_status;
  }

#if !defined (FEATURE_UIM_TEST_FRAMEWORK)
  mmgsdi_status = gsdi_perso_engine_create_file();
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not create dirs or files");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return mmgsdi_status;
  }
#endif /* !defined (FEATURE_UIM_TEST_FRAMEWORK)*/

  mmgsdi_status = gsdi_perso_engine_write_back_fs(&encrypted_data, FALSE);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not write back FS");
  }
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
  return mmgsdi_status;
}/* gsdi_perso_engine_create_umts_data */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CREATE_DATA

DESCRIPTION

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_create_data(
  void
)
{
  mmgsdi_return_enum_type mmgsdi_umts_status = MMGSDI_SUCCESS;
  mmgsdi_return_enum_type mmgsdi_ruim_status = MMGSDI_SUCCESS;

  mmgsdi_umts_status = gsdi_perso_engine_create_umts_data();
  /* At this point the directories have been created so we
     should attempt to make the RUIM perso file even if
     the SIM file creation fails - so don't return yet */

  mmgsdi_ruim_status = gsdi_perso_engine_create_ruim_data();

  if(mmgsdi_umts_status != MMGSDI_SUCCESS ||
     mmgsdi_ruim_status != MMGSDI_SUCCESS)
  {
    return MMGSDI_ERROR;
  }

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_create_data */


#ifdef FEATURE_MMGSDI_PERSO_SFS
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_SFS_TEST

DESCRIPTION
  This function is used to test whether the necessary components of the
  perso SFS system are present.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type: MMGSDI_SUCCESS  : Directory and files present
                         MMGSDI_ERROR    : Missing file or illegal param
                         MMGSDI_NOT_FOUND: Directory and files absent
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_sfs_test(
  void
)
{
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  uint8                   buf[100];
  int                     len           = 100;
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
  uint32                  sfs_len       = 0;
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_NOT_FOUND;

#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
  /* SFP is enabled - either SIM or RUIM perso files must be on SFP */
  len = sfs_priv_simloc_read(buf,len);

  if(len == 0)
  {
    /* If there is neither SIM nor RUIM perso file on SFP, we assume
       there isn't any perso file in SFS either. So, return to create
       the files for the first time */
    UIM_MSG_HIGH_0("gsdi_perso_engine_sfs_test returned MMGSDI_NOT_FOUND");
    return MMGSDI_NOT_FOUND;
  }
#if defined(FEATURE_PERSO_SIM) && defined(FEATURE_PERSO_RUIM)
  /* SIM perso file is on SFP and RUIM perso file is on SFS */
  if((mmgsdi_status = mmgsdi_sfs_get_size(&sfs_len, mmgsdi_ruim_perso_sfs_file)) != MMGSDI_SUCCESS)
  {
    /* If the files in sfs are missing assume tampering */
    return mmgsdi_status;
  }
#endif /* FEATURE_PERSO_SIM && FEATURE_PERSO_RUIM */
#else
  /* SFP is disabled - both SIM and RUIM perso files must be on SFS */
  if((mmgsdi_status = mmgsdi_sfs_get_size(&sfs_len, mmgsdi_ruim_perso_sfs_file)) != MMGSDI_SUCCESS)
  {
    /* If the files in sfs are missing assume tampering */
    return mmgsdi_status;
  }
  if((mmgsdi_status = mmgsdi_sfs_get_size(&sfs_len, mmgsdi_sfs_perso_file)) != MMGSDI_SUCCESS)
  {
    /* If the files in sfs are missing assume tampering */
    return mmgsdi_status;
  }
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
  (void) sfs_len;
  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_sfs_test */
#endif /* FEATURE_MMGSDI_PERSO_SFS */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_INIT

DESCRIPTION
  This function is used to create all of the relevant Personalization Files
  used for storing information in EFS.

  This procedure runs every time the mobile is powered up, but will not
  recreate any files already created.

  If all personalization files are not created properly or upon entering
  this function again, only some of the files exist... then a CARD_ERROR
  will occur.  This could be evidence of tampering.

  It will be acceptable for the file to be empty.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type:
                MMGSDI_SUCCESS : All perso files exist
                MMGSDI_ERROR   : Finding/creating the required files failed
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_init(
  void
)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_ERROR;
  /* --------------------------------------------------------------------------
     Does the Directory structure already exist
     ------------------------------------------------------------------------*/
#ifndef FEATURE_MMGSDI_PERSO_SFS
  if(gsdi_perso_engine_fs_dir_test(mmgsdi_efs_perso_dir) == GSDI_SUCCESS)
  {
    /* if the directory structure exists without the perso
       files we must assume they were tampered with */
    return gsdi_perso_engine_are_all_files_present();
  }
  else
  {
    /* If the directory structure does not exist it must be created
       along with all the necessary perso files */
    mmgsdi_status = MMGSDI_NOT_FOUND;
  }
#else
  mmgsdi_status = gsdi_perso_engine_sfs_test();
#endif /* FEATURE_MMGSDI_PERSO_SFS */

  if(mmgsdi_status == MMGSDI_NOT_FOUND)
  {
    UIM_MSG_HIGH_0("PERSO: Creating the perso file structure for the first time");

    mmgsdi_status = gsdi_perso_engine_create_dir();

    if(mmgsdi_status != MMGSDI_SUCCESS)
    {
      UIM_MSG_HIGH_1("gsdi_perso_engine_create_dir failed 0x%x", mmgsdi_status);
      return MMGSDI_ERROR;
    }

    mmgsdi_status = gsdi_perso_engine_create_data();
  }

  return mmgsdi_status;
}/* gsdi_perso_engine_init */

#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

