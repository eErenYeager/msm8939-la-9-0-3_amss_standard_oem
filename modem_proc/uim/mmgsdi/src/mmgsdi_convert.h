#ifndef MMGSDI_CONVERT_H
#define MMGSDI_CONVERT_H
/*===========================================================================


           M M G S D I   U T I L I T Y   H E A D E R S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2007-2008, 2011, 2013 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or
software is regulated by the U.S. Government. Diversion contrary to U.S. law
prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_convert.h#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
09/29/13   df      Removing legacy gsdi conversion functions
12/21/11   kk      Legacy GSDI removal updates
12/18/08   sun     Removed redundant declarations
12/11/07   js      Added conversion function to map gsdi and mmgsdi status
11/19/07   sun     Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/

#include "mmgsdi.h"
#include "mmgsdilib.h"
#include "gsdi_p.h"

/*=============================================================================

                       FUNCTION PROTOTYPES

=============================================================================*/

/* ==========================================================================
   FUNCTION:      MMGSDI_UTIL_CONVERT_GSDI_STATUS_TO_MMGSDI

   DESCRIPTION:
     This function checks for valid range of the gsdi_returns_T,  and
     returns the corresponding mmgsdi_return_enum_type

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     None

   SIDE EFFECTS:
     None
==========================================================================*/
void mmgsdi_util_convert_gsdi_status_to_mmgsdi(
  gsdi_returns_T            gsdi_status,
  mmgsdi_return_enum_type  *mmgsdi_status_ptr
);
#endif /* MMGSDI_CONVERT_H */
