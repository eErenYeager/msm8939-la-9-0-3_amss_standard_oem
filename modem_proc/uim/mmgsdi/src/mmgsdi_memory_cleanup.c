/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                 MMGSDI - MEMORY CLEANUP RELATED FUNCTIONS


GENERAL DESCRIPTION

  This source file contains functions for freeing up the memory allocated
  and held by MMGSDI during its normal operation in off-target environment.
  This is called only after all MMGSDI tasks have stopped

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Techologies, Inc (QTI) and its licensors.  All Rights
Reserved.  QUALCOMM Techologies Proprietary.  Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_memory_cleanup.c#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/06/14    am     Initial version
===========================================================================*/

/*===========================================================================

                         INCLUDE FILES FOR MODULE

===========================================================================*/
#include "mmgsdi.h"
#include "mmgsdi_memory_cleanup.h"
#include "mmgsdi_nv.h"
#include "mmgsdi_eons_opl_pnn.h"
#include "mmgsdicache.h"
#include "mmgsdi_refresh.h"
#include "mmgsdi_gen.h"
#include "uim_msg.h"

#if defined FEATURE_MODEM_RCINIT && defined FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_MODEM_RCINIT && FEATURE_UIM_TEST_FRAMEWORK */

