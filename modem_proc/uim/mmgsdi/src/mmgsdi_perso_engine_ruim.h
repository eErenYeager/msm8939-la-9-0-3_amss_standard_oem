#ifndef MMGSDI_PERSO_ENGINE_RUIM_H
#define MMGSDI_PERSO_ENGINE_RUIM_H
/*===========================================================================


        G S D I   P E R S O N A L I Z A T I O N   E N G I N E

     S T R U C T U R E S  A N D  F U N C T I O N  P R O T O T Y P E S


DESCRIPTION
  Structure and Function prototype definitions for the personalization
  engine.  The gsdi_perso.c functions are the entry point into the
  engine.  The engine is going to retrieve all of the information required
  to allow the security library to perform the security procedures.


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2001-2011, 2013 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary. 
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_perso_engine_ruim.h#1 $$ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     --------------------------------------------------------
10/16/13   df      Removing gsdi legacy conversion functions
06/18/13   av      Fallback to SFS if SFP not available
05/16/13   vdc     Replace memcpy with safer version memscpy
04/25/13   yt      Fix compilation for non-CDMA builds
12/21/11   shr     Legacy GSDI removal updates
04/13/11   nmb     Removed dependency on hardware ESN
10/11/10   nmb     Write perso data to SFS instead of EFS when SFS is enabled
06/01/09   ssr     Merging of SIM and RUIM PERSO
11/12/08   ssr     Removed compiler warnings
05/15/06   jk      Added definition for gsdi_perso_engine_get_esn
04/06/06   sun     Initial Revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "uim_variation.h"
#include "customer.h"
#include "gsdi_p.h"

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE

/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CREATE_RUIM_DATA

DESCRIPTION
  This function is called to calcuate the total memory required for the
  SIM Items to be retrieved.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_create_ruim_data(void);

#ifdef FEATURE_PERSO_RUIM
#ifndef FEATURE_MMGSDI_PERSO_SFS
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_RUIM_FILE_TEST

DESCRIPTION
  This function is used to create the directories and files.  This function
  should only be called once during the personalization phase.

DEPENDENCIES
  None

RETURN VALUE
  boolean (true if successful)
===========================================================================*/
boolean gsdi_perso_engine_ruim_fs_file_test(void);
#endif /*FEATURE_MMGSDI_PERSO_SFS */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_HANDLE_RUIM_REQUEST

DESCRIPTION
  This function is the main handler for all the perso requests.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_handle_ruim_request(
  mmgsdi_session_id_type           session_id,
  mmgsdi_int_app_info_type        *app_info_ptr,
  mmgsdi_slot_id_enum_type         slot,
  gsdi_perso_engine_proc_enum_type feature_type,
  gsdi_perso_sec_client_req_type  *client_data,
  const void                      *req_ptr,
  uint8                           *ret_data_ptr,
  uint32                           ret_data_max_len,
  int32                           *ret_data_len_ptr,
  gsdi_perso_event_enum_type      *perso_event_ptr);

#endif /*FEATURE_PERSO_RUIM*/
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
#endif /* MMGSDI_PERSO_ENGINE_RUIM_H */
