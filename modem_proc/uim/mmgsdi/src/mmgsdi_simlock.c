/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                 M M G S D I   S I M L O C K  F U N C T I O N S


GENERAL DESCRIPTION

  This source file contains functions to perform MMGSDI specific SIM Lock
  tasks.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_simlock.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/29/14   vv      Added check for halt subscription
04/15/14   vv      Added support for quad SIM
04/09/14   tl      Notify all session associated with application
03/25/14   tl      Secondary revisions
02/25/14   tl      Initial version
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "uim_msg.h"
#include "mmgsdiutil.h"
#include "mmgsdi_simlock.h"
#include "mmgsdilib_common.h"
#include "mmgsdicache.h"
#include "mmgsdi_card_init.h"
#include "simlock_modem_lib.h"

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */

