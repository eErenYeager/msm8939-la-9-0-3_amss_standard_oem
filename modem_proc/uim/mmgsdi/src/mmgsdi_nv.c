/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


            G E N E R I C   S I M   D R I V E R   I N T E R F A C E

                                    A N D

                     N O N V O L A T I L E   M E M O R Y

                              I N T E R F A C E


GENERAL DESCRIPTION

  This file contains the header information for the GSDI to NV Interface.  This
  interface will be exercised by the GSDI Task and Virtual SIM Task.

EXTERNALIZED FUNCTIONS

  mmgsdi_nv_get_item()                       - Used get items from nv

  mmgsdi_nv_put_item()                       - Used put items into nv

  mmgsdi_nv_init_features_status_nv_lookup() - Used to populate global
                                               mmgsdi_features_status frm NVITEM
                                               MMGSDI_FEATURES_STATUS_LIST

  mmgsdi_nv_get_feature_status()             - Used to fetch status of features
                                               listed in
                                               mmgsdi_feature_status_enum
                                               (linked to NV ITEM MMGSDI_
                                               FEATURES_STATUS_LIST NVITEM)

INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2001 - 2005, 2009 - 2015, 2017 QUALCOMM Technologies, Inc (QTI) and
its licensors. All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_nv.c#3 $$ $DateTime: 2017/04/24 00:03:48 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/20/17   dd      Add test mode logging enabled APIs for sensitive info control
08/04/15   vr      Non UI and non NW proactive command parallel processing
06/02/14   bcho    New item to decide on blocking of CDMA EPRL access
05/09/14   vv      Deprecate MMGSDI_FEATURE_UICC_RAT_INDICATOR_SUPPORT
04/06/14   am      Added mmgsdi heap alloc clean up logic
04/01/14   av      NVRUIM headers cleanup
03/25/14   vdc     Added new item for verifying PIN when PIN is changed
02/27/14   dy      Add default_apn, bip_apn_mapping_tbl, rat_priority NV item
02/25/14   hh      Purge temporary mobile identities during UICC Reset Refresh
02/25/14   bcho    Added support to store OPLMN List in EFS and cache it
01/16/14   av      New item for deciding bw card and nv for 3gpd credentials
01/10/14   bcho    Refresh 3G Session Reset as per ETSI spec under NV feature
01/10/14   df      Use v2.0 diag macros
01/09/14   ar      Replace old Macros with MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE
12/24/13   am      Added NV protection to Recovery API
12/18/13   yt      Skip app termination as part of session deactivation
12/09/13   vdc     Send RTRE configuration to NV asynchronously
12/05/13   ar      Replaced all mmgsdi_malloc direct calls with macro
11/21/13   vv      Halt 3gpp subscription if 3gpp2 perso fails
10/17/13   am      Added new item for plmn comparison in the nvitem 67211
10/08/13   tl      Addition of an RPM info EFS structure type
10/04/13   bcho    F3 message reduction
10/04/13   vv      Added NV support for ENS sub-features
09/26/13   av      Enable OnChip on bootup based on EFS item
08/30/13   tl      Convert channel info table from static to dynamic allocation
08/20/13   vv      Update EF-TST to indicate support for IMS
08/20/13   spo     Automatic Subscription provisioning enhancements in UIM
07/29/13   am      Added support to determine CSG support dynamically
07/26/13   tkl     Add NV support for enabling use of SMS-PP envelope command
06/24/13   vdc     Added support for triple SIM
05/17/13   av      Halt 3gpp2 subscription if 3gpp perso fails
05/16/13   vdc     Replace memcpy with safer version memscpy
05/15/13   bcho    Added new items to mmgsdi features status nvitem 67211
05/13/13   av      Fixed KW errors
04/25/13   vdc     Added select_dfcdma to mmgsdi feature status nvitem 67211
04/17/13   tl      Replace Featurization with NV
04/11/13   tkl     Add NV suport for SETUP Call display alpha value
04/05/13   av      Move UIM tasks to group4 of RCINIT
04/04/13   bcho    Added new items to mmgsdi features status nvitem 67211
03/27/13   spo     Added new items to mmgsdi feature status list
02/19/13   av      Merge mmgsdi,gstk,qmiuim/cat conf files to mmgsdi.conf
01/04/13   vv      Handle NV write command in main task
01/03/13   abg     Added API mmgsdi_nv_init_read_and_cache_nv_items to read NV during task-init
12/26/12   av      Do not allow writing to the file that is being refreshed
11/05/12   av      Expand features status list nvitem to control skipping
                   checking security attributes before reading EF
10/31/12   tl      CSIM conformance updating EF MECRP
10/31/12   tl      CSIM conformance updating EF MODEL
10/21/12   av      Update conf file only if necessary
10/16/12   yt      Add new items to mmgsdi feature status list
09/28/12   yt      Update user profile parameters after successful OTASP Commit
08/24/12   abg     fix default value of ignore_init_error
04/20/12   av      Enabled detouring of nv/efs calls to offtarget framework
03/08/12   av      Added new items to mmgsdi features status nvitem 67211
01/30/12   shr     Use GSTK NV item (65674) to determine if the OTASP IMSI_M
                   enhancement is enabled or not
01/13/12   at      Added NV item caching for NV_DISABLE_CM_CALL_TYPE_I
12/21/11   shr     Legacy GSDI removal updates
11/15/11   nmb     RPM Updates
10/10/11   av      Updated default values of MMGSDI Features
09/16/11   av      Replace feature #ifdefs with an NVITEM
07/12/11   at      Added NV item caching for NV_SERVICE_DOMAIN_PREF_I
06/05/11   vs      Updated NV item caching logic to include NV_ESN_ME_I
12/22/10   yt      Removed FEATURE_UIM_ZI_MEMORY_REDUCTION
11/24/10   js      Resolve race condition between GSDI NV READ/WRITE
10/25/10   nmb     Copy NV item data to NV command buffer
10/04/10   nmb     Added NV queue to address NV deadlocks
09/08/10   nmb     mmgsdi_task_cmd_type memory reduction
07/14/10   js      Fixed compilation warnings
05/13/10   vs      Fix for NV and GSDI deadlock
10/21/09   rn      Fixed klocwork errors
09/26/09   kp      ZI memory reduction changes
07/20/09   mib     Replaced flag TEST_FRAMEWORK with FEATURE_UIM_TEST_FRAMEWORK
02/27/09   nd      Virtuim changes for QTF
03/08/06   jar     Merged in Branched SIM Lock
05/11/05   sst     Lint fixes
11/15/04   jar     Clear out GSDI NV Signal when NV Response is received
02/21/04   jar     Removed gsdi_wait_nv() since it is not being used and
                   causes compiler errors.
11/05/03   jar     Added support for Slot Preference Changes.  Switch from
                   gsdi_wait_nv() to gsdi_wait() to allow for GSDI_RPT_SIG
                   handling.
07/02/03   tml     Fixed gsdi_put_nv
12/05/02   jar     Added #include "err.h" for ERR Macros.
11/26/02   jar     Removed RTRE Code.
08/30/02 jar/ck    Added gsdi_send_rtre_command.  changed gsdi_nvi to gsdi_nv
                   _cmd.  Localized gsdi_wait_nv.  Added internal feature
                   MMGSDI_RTRE.
08/18/02   jar     Created gsdi_nv.h
===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#include "mmgsdi_nv.h"
#include "nv.h"
#include "uim_msg.h"
#include "err.h"
#include "mmgsdiutil.h"
#include "mmgsdisessionlib_v.h"
#include "mmgsdi.h"
#include "queue.h"
#include "fs_public.h"
#include "gstk_exp_v.h"
#include "mmgsdilib_v.h"

#ifdef FEATURE_FEMTO_CSG
#include "sys_v.h"
#endif /* FEATURE_FEMTO_CSG */

#include "fs_lib.h"
#include "fs_public.h"
#include "fs_diag_access.h"
#include "fs_stdlib.h"

#include "qmi_uim.h"
#include "qmi_cat.h"
#ifdef FEATURE_GSTK
#include "gstk_p.h"
#endif /* FEATURE_GSTK */
#ifdef FEATURE_ESTK
#include "estk.h"
#endif /* FEATURE_ESTK */

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

static mmgsdi_nv_item_cache_table_type *mmgsdi_nv_item_cache_table_ptr = NULL;

static mmgsdi_return_enum_type mmgsdi_nv_put_nv_item_in_cache (
  nv_items_enum_type  item_enum,
  nv_item_type       *data_ptr,
  nv_stat_enum_type   item_status
);

/* defines for RPM ICCID change identification support */
#define MMGSDI_RPM_ICCID_FILE "/nv/item_files/modem/uim/mmgsdi/rpm_iccid"

/* GSTK file in EFS corresponding to NV 65674 (GSTK Features Status) */
#define GSTK_FEATURES_FILE    "/nv/item_files/modem/uim/gstk/feature_bmsk"

/* NAS file in EFS corresponding to NV 70361 (CSG Support Flag) */
#define NAS_CSG_SUPPORT_FILE  "/nv/item_files/modem/nas/csg_support_configuration"

/* IMS file in EFS corresponding to NV 67218 (IMS Support Flag) */
#define IMS_SUPPORT_FILE      "/nv/item_files/ims/IMS_enable"

/* RPM info file in EFS corresponding to NV 67210 (RPM INFO) */
#define RPM_INFO_FILE         "/nv/item_files/modem/mmode/rpm_info"

/*---------------------------------------------------------------------------
  To store list of status of mmgsdi features
---------------------------------------------------------------------------*/
static mmgsdi_features_status_list_type features_status_list;

/*---------------------------------------------------------------------------
  To store list of status of external features (i.e. features outside of MMGSDI)
---------------------------------------------------------------------------*/
static mmgsdi_external_features_status_list_type external_features_status_list;

/*---------------------------------------------------------------------------
  List of total number of MMGSDI features present in NVITEM in each version
  of feature_status_list. Whenever a newer version of the NVITEM is exposed,
  size of the array grows by 1...So, if 8 features/items were added in VERSION0
  and 2 in VERSION1, the array should have 2 items listing the number of total
  features at each version.. {8,10} in this case...
---------------------------------------------------------------------------*/
uint8  mmgsdi_features_total_till_version[] =
  {MMGSDI_FEATURES_STATUS_LIST_VERSION0_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION1_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION2_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION3_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION4_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION5_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION6_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION7_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION8_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION9_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION10_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION11_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION12_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION13_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION14_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION15_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION16_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION17_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION18_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION19_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION20_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION21_COUNT,
   MMGSDI_FEATURES_STATUS_LIST_VERSION22_COUNT};

/*---------------------------------------------------------------------------
  To store status of mmgsdi ENS sub features
---------------------------------------------------------------------------*/
static mmgsdi_ens_sub_features_status_list_type ens_sub_features_status_list;

/*===========================================================================
FUNCTION MMGSDI_NV_GET_ITEM

DESCRIPTION
  Get an item from the nonvolatile memory.

RETURN VALUE
  The NV return code, except for NV_NOTACTIVE_S, which is handled
  internally.

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done.
===========================================================================*/
nv_stat_enum_type mmgsdi_nv_get_item(
  nv_items_enum_type  item,
  nv_item_type       *data_ptr
)
{
  nv_cmd_type             *mmgsdi_nv_cmd_ptr = NULL;
  mmgsdi_return_enum_type  mmgsdi_status     = MMGSDI_ERROR;
  nv_stat_enum_type        nv_status         = NV_NOTALLOC_S;
  mmgsdi_task_enum_type    mmgsdi_task       = MMGSDI_TASK_MAX;

  UIM_MSG_HIGH_1("In mmgsdi_nv_get_item, item: %d", item);

  if (mmgsdi_nv_is_nv_item_cached(item))
  {
    return mmgsdi_nv_get_nv_item_from_cache(item, data_ptr);
  }

  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(mmgsdi_nv_cmd_ptr,
                                         sizeof(nv_cmd_type),
                                         mmgsdi_status);

  if((mmgsdi_status != MMGSDI_SUCCESS) || (mmgsdi_nv_cmd_ptr == NULL))
  {
    UIM_MSG_ERR_0("Failed to allocate memory");
    return NV_NOTALLOC_S;
  }

  /* Notify this task when done */
  mmgsdi_nv_cmd_ptr->tcb_ptr = rex_self();
  mmgsdi_nv_cmd_ptr->sigs = MMGSDI_NV_READ_SIG;

  /* Command goes on no queue when done */
  mmgsdi_nv_cmd_ptr->done_q_ptr = NULL;

  /* Item to read */
  mmgsdi_nv_cmd_ptr->item = item;
  mmgsdi_nv_cmd_ptr->cmd = NV_READ_F;

  /* Set the Data Ptr for NV Item to read */
   mmgsdi_nv_cmd_ptr->data_ptr = data_ptr;

  /* Clear REX Signals */
  (void) rex_clr_sigs( rex_self(), MMGSDI_NV_READ_SIG );

  /* Send Command to NV */
  nv_cmd(mmgsdi_nv_cmd_ptr);

  /* Get current task info */
  MMGSDIUTIL_GET_CURRENT_MMGSDI_TASK(&mmgsdi_task);

  if(mmgsdi_task == MMGSDI_TASK_MAX)
  {
    MMGSDIUTIL_TMC_MEM_FREE(mmgsdi_nv_cmd_ptr);

    UIM_MSG_ERR_0("Could not determine MMGSDI Task");
    return NV_BADPARM_S;
  }

  /* Wait for Signal from NV that it's complete */
  (void)mmgsdi_wait(MMGSDI_NV_READ_SIG);

  /* Clear Signal */
  (void) rex_clr_sigs( rex_self(), MMGSDI_NV_READ_SIG );

  UIM_MSG_HIGH_2("Get NV Status = 0x%x for item: %d",
             mmgsdi_nv_cmd_ptr->status, mmgsdi_nv_cmd_ptr->item);

  (void)mmgsdi_nv_put_nv_item_in_cache(
          item,
          data_ptr,
          mmgsdi_nv_cmd_ptr->status);

  nv_status = mmgsdi_nv_cmd_ptr->status;
  MMGSDIUTIL_TMC_MEM_FREE(mmgsdi_nv_cmd_ptr);

  return nv_status;
} /* mmgsdi_nv_get_item */


/*===========================================================================
FUNCTION MMGSDI_NV_PUT_ITEM

DESCRIPTION
  Put an item on to the nonvolatile memory.

RETURN VALUE
  The NV return code, except for NV_NOTACTIVE_S, which is handled
  internally.

DEPENDENCIES
  This routine is not reentrant.  Shouldn't be a problem, as it doesn't exit
  till we're done.
===========================================================================*/
nv_stat_enum_type mmgsdi_nv_put_item(
  nv_items_enum_type item,
  nv_item_type      *data_ptr
)
{
  nv_cmd_type             *mmgsdi_nv_cmd_ptr  = NULL;
  mmgsdi_return_enum_type  mmgsdi_status      = MMGSDI_ERROR;
  nv_item_type            *cmd_data_ptr       = NULL;
  int32                    mmgsdi_nv_cmd_size = 0;

  UIM_MSG_HIGH_1("In mmgsdi_nv_put_item, item: %d", item);

  if(data_ptr == NULL)
  {
    return NV_BADPARM_S;
  }

  /* Allocate memory for the NV command */
  mmgsdi_util_cal_align_size(sizeof(nv_cmd_type), &mmgsdi_nv_cmd_size);

  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(mmgsdi_nv_cmd_ptr,
                                         (mmgsdi_nv_cmd_size + sizeof(nv_item_type)),
                                          mmgsdi_status);

  if((mmgsdi_status != MMGSDI_SUCCESS) || (mmgsdi_nv_cmd_ptr == NULL))
  {
    UIM_MSG_ERR_0("Failed to allocate memory");
    return NV_NOTALLOC_S;
  }

  /* Copy data to write */
  cmd_data_ptr = (nv_item_type*)((uint8*)mmgsdi_nv_cmd_ptr + mmgsdi_nv_cmd_size);
  mmgsdi_memscpy(cmd_data_ptr,
                 sizeof(nv_item_type),
                 data_ptr,
                 sizeof(nv_item_type));

  /* Notify main MMGSDI task when done */
  mmgsdi_nv_cmd_ptr->tcb_ptr    = UIM_GSDI_TCB;
  mmgsdi_nv_cmd_ptr->sigs       = MMGSDI_NV_WRITE_SIG;
  mmgsdi_nv_cmd_ptr->done_q_ptr = &mmgsdi_nv_q;

  /* Item to write */
  mmgsdi_nv_cmd_ptr->item     = item;
  mmgsdi_nv_cmd_ptr->cmd      = NV_WRITE_F;
  mmgsdi_nv_cmd_ptr->data_ptr = cmd_data_ptr;

  /* Send Command to NV */
  nv_cmd(mmgsdi_nv_cmd_ptr);

  mmgsdi_status = mmgsdi_nv_put_nv_item_in_cache(
                    item,
                    data_ptr,
                    NV_DONE_S);

  UIM_MSG_HIGH_1("MMGSDI NV Cache write status=0x%x", mmgsdi_status);

  return NV_DONE_S;
}/* mmgsdi_nv_put_item */


/*===========================================================================
FUNCTION MMGSDI_NV_IS_NV_ITEM_CACHED

DESCRIPTION
  This function returns TRUE if the NV item is cached by MMGSDI

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  Boolean

SIDE EFFECTS
  None
===========================================================================*/
boolean mmgsdi_nv_is_nv_item_cached (
  nv_items_enum_type item_enum
)
{
  uint32 i    = 0x0;
  uint32 mask = 0x1;

  if (mmgsdi_nv_item_cache_table_ptr == NULL)
  {
    UIM_MSG_ERR_0("mmgsdi_nv_item_cache_table_ptr is NULL");
    return FALSE;
  }

  for (i=0; i<MMGSDI_NV_ITEM_CACHE_SIZE; i++)
  {
    if (mmgsdi_nv_item_cache_table_ptr->item[i].item_enum == item_enum)
    {
      /* Create bit mask using the index of the item to determine if cached */
      if (mmgsdi_nv_item_cache_table_ptr->is_cached_mask &
          (mask << i))
      {
        return TRUE;
      }
      else
      {
        return FALSE;
      }
    }
  }

  UIM_MSG_ERR_1("Getting item: %d will cause deadlocks. Add to NV cache",
                item_enum);
  /* This function should be called only with items that are in the cache
     to see if they are currently valid before calling
     mmgsdi_nv_get_nv_item_from_cache. Return true if item is not a cached
     item so that getting the item from the cache fails. */
  return TRUE;
} /* mmgsdi_nv_is_nv_item_cached */


/*===========================================================================
FUNCTION MMGSDI_NV_GET_NV_ITEM_FROM_CACHE

DESCRIPTION
  This function provides the cached value of the NV item

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  nv_stat_enum_type

SIDE EFFECTS
  None
===========================================================================*/
nv_stat_enum_type mmgsdi_nv_get_nv_item_from_cache (
  nv_items_enum_type  item_enum,
  nv_item_type       *data_ptr
)
{
  uint32 i = 0x0;

  if (mmgsdi_nv_item_cache_table_ptr == NULL)
  {
    UIM_MSG_ERR_0("mmgsdi_nv_item_cache_table_ptr is NULL");
    return NV_NOTALLOC_S;
  }

  for (i =0; i<MMGSDI_NV_ITEM_CACHE_SIZE; i++)
  {
    if (mmgsdi_nv_item_cache_table_ptr->item[i].item_enum == item_enum)
    {
      *data_ptr =
        mmgsdi_nv_item_cache_table_ptr->item[i].item_value;
      UIM_MSG_HIGH_1("Got NV item: %d in MMGSDI NV Cache", item_enum);
      return mmgsdi_nv_item_cache_table_ptr->item[i].item_status;
    }
  }

  UIM_MSG_ERR_1("Did not find item %d in cache", item_enum);
  return NV_NOTALLOC_S;
} /* mmgsdi_nv_get_nv_item_from_cache */


/*===========================================================================
FUNCTION MMGSDI_NV_PUT_NV_ITEM_IN_CACHE

DESCRIPTION
  This function copies the NV item to the cache

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  mmgsdi_return_enum_type

SIDE EFFECTS
  None
===========================================================================*/
static mmgsdi_return_enum_type mmgsdi_nv_put_nv_item_in_cache (
  nv_items_enum_type  item_enum,
  nv_item_type       *data_ptr,
  nv_stat_enum_type   item_status
)
{
  uint32 i    = 0x0;
  uint32 mask = 0x1;

  if (mmgsdi_nv_item_cache_table_ptr == NULL)
  {
    UIM_MSG_ERR_0("mmgsdi_nv_item_cache_table_ptr is NULL");
    return MMGSDI_ERROR;
  }

  for (i=0; i<MMGSDI_NV_ITEM_CACHE_SIZE; i++)
  {
    if (mmgsdi_nv_item_cache_table_ptr->item[i].item_enum == item_enum)
    {
      mmgsdi_nv_item_cache_table_ptr->is_cached_mask |= (mask << i);
      mmgsdi_nv_item_cache_table_ptr->item[i].item_status =
        item_status;
      if (item_status == NV_DONE_S)
      {
        mmgsdi_nv_item_cache_table_ptr->item[i].item_value =
          *data_ptr;
      }
      UIM_MSG_HIGH_1("Put NV item: %d in MMGSDI NV Cache", item_enum);
      return MMGSDI_SUCCESS;
    }
  }

  UIM_MSG_ERR_1("Using item: %d will cause deadlocks. Add to NV cache",
                item_enum);
  return MMGSDI_ERROR;
} /* mmmmgsdi_nv_put_nv_item_in_cache */


/*===========================================================================
  FUNCTION MMGSDI_NV_DIAG_ACCESS_CHECK_CB

  DESCRIPTION
    This function is called by the Diag module as a result of any Diag access
    of the MMGSDI access controlled EFS directory. It is initially registered
    with Diag for knowing the access of the EFS directory.

    Note: In order to enable the NVs in the restricted NV files list, the
    corresponding MCFG HW MBN entry needs to be updated.

  PARAMETERS
    directory_name_ptr: Name of the file that needs to be permitted
    op_type      : File operation type

  RETURN VALUE
    TRUE:  If we want to allow access
    FALSE: If we want to reject access

  DEPENDENCIES
    QMI UIM service must be registered with Diag module via
    diag_fs_register_access_check API.

  SIDE EFFECTS
    None

===========================================================================*/
static boolean mmgsdi_nv_diag_access_check_cb(
  char      * directory_name_ptr,
  uint32      op_type
)
{
  (void)directory_name_ptr;

  if ( (op_type == DIAG_FS_CHECK_OPEN) ||
       (op_type == DIAG_FS_CHECK_DISP_DIRS) ||
       (op_type == DIAG_FS_CHECK_DISP_FILES) ||
       (op_type == DIAG_FS_CHECK_ITERATE) ||
       (op_type == DIAG_FS_CHECK_READ) ||
       (op_type == DIAG_FS_CHECK_GET_ATTRIB) ||
       (op_type == DIAG_FS_CHECK_CLOSE) ||
       (op_type == DIAG_FS_CHECK_OPENDIR) ||
       (op_type == DIAG_FS_CHECK_READDIR) ||
       (op_type == DIAG_FS_CHECK_CLOSEDIR) ||
       (op_type == DIAG_FS_CHECK_STAT) ||
       (op_type == DIAG_FS_CHECK_LSTAT) ||
       (op_type == DIAG_FS_CHECK_FSTAT) ||
       (op_type == DIAG_FS_CHECK_STATFS) ||
       (op_type == DIAG_FS_CHECK_ACCESS))
  {
    /* Allow access */
    return TRUE;
  }

  return FALSE;
} /* mmgsdi_nv_diag_access_check_cb */


/*===========================================================================
FUNCTION MMGSDI_NV_ITEM_CACHE_INIT

DESCRIPTION
  Allocate memory and initialize the items in the cache

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  None

SIDE EFFECTS
  None
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_nv_item_cache_init (
  void
)
{
  static const char                         *efs_directory_list[]  = {MMGSDI_SECURE_DIR};

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(mmgsdi_nv_item_cache_table_ptr,
    sizeof(mmgsdi_nv_item_cache_table_type));
  if (mmgsdi_nv_item_cache_table_ptr == NULL)
  {
    return MMGSDI_MEMORY_ERROR_HEAP_EXHAUSTED;
  }
  mmgsdi_nv_item_cache_table_ptr->is_cached_mask      = 0x0;
  mmgsdi_nv_item_cache_table_ptr->item[0x0].item_enum = NV_GPRS_ANITE_GCF_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x1].item_enum = NV_UIM_CDMA_PREF_SLOT_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x2].item_enum = NV_UIM_GSM_PREF_SLOT_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x3].item_enum = NV_RTRE_CONFIG_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x4].item_enum =
    NV_MMGSDI_ME_CONFIG_PARAM_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x5].item_enum = NV_JCDMA_UIM_LOCK_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x6].item_enum = NV_JCDMA_RUIM_ID_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x7].item_enum =
    NV_UIM_SELECT_DEFAULT_USIM_APP_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x8].item_enum = NV_MEID_ME_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x9].item_enum = NV_UE_IMEI_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xA].item_enum = NV_ENS_ENABLED_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xB].item_enum = NV_HOMEZONE_ENABLED_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xC].item_enum = NV_FTM_MODE_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xD].item_enum = NV_ESN_ME_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xE].item_enum = NV_SERVICE_DOMAIN_PREF_I;
  mmgsdi_nv_item_cache_table_ptr->item[0xF].item_enum = NV_DISABLE_CM_CALL_TYPE_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x10].item_enum = NV_SCM_I;
  mmgsdi_nv_item_cache_table_ptr->item[0x11].item_enum = NV_MOB_CAI_REV_I;

  /* Register call back for mmgsdi secure efs directory */
  diag_fs_register_access_check((const char **)efs_directory_list,
                                 sizeof(efs_directory_list) / sizeof(efs_directory_list[0]),
                                 mmgsdi_nv_diag_access_check_cb);

  return MMGSDI_SUCCESS;
} /* mmgsdi_nv_item_cache_init */


/*===========================================================================
FUNCTION MMGSDI_NV_FEATURE_WRITE_DEFAULT_VALUE

DESCRIPTION
  Function used to write the default value of MMGSDI Feature (controlled by
  MMGSDI_Feature_Status_list NVITEM) to global features_status_list.

DEPENDENCIES

RETURN VALUE
  mmgsdi_return_enum_type

SIDE EFFECTS
  None
===========================================================================*/
static mmgsdi_return_enum_type mmgsdi_nv_feature_write_default_value(
  mmgsdi_nv_features_enum_type nv_feature)
{
  switch(nv_feature)
  {
    case MMGSDI_FEATURE_ABSENT_MANDATORY_FILES_INIT:
      features_status_list.absent_mandatory_files_init = MMGSDI_FEATURE_ABSENT_MANDATORY_FILES_INIT_DEFAULT;
      break;
    case MMGSDI_FEATURE_ECC_NOT_PRESENT_SUPPORT:
      features_status_list.ecc_not_present_support = MMGSDI_FEATURE_ECC_NOT_PRESENT_SUPPORT_DEFAULT;
      break;
    case MMGSDI_FEATURE_SPECIAL_USIM_ARR:
      features_status_list.special_usim_arr = MMGSDI_FEATURE_SPECIAL_USIM_ARR_DEFAULT;
      break;
    case MMGSDI_FEATURE_DEPRECATED_1:
      features_status_list.deprecated_1 = MMGSDI_FEATURE_DEPRECATED_1_DEFAULT;
      break;
    case MMGSDI_FEATURE_CDMA_DF_SELECT_BEFORE_TP:
      features_status_list.cdma_df_select_before_tp = MMGSDI_FEATURE_CDMA_DF_SELECT_BEFORE_TP_DEFAULT;
      break;
    case MMGSDI_FEATURE_CHANGE_INST_CLASS:
      features_status_list.change_inst_class = MMGSDI_FEATURE_CHANGE_INST_CLASS_DEFAULT;
      break;
    case MMGSDI_FEATURE_CARD_SLOT_CHECK:
      features_status_list.card_slot_check = MMGSDI_FEATURE_CARD_SLOT_CHECK_DEFAULT;
      break;
    case MMGSDI_FEATURE_TMSI_PLMN_CHECK:
      features_status_list.tmsi_plmn_check = MMGSDI_FEATURE_TMSI_PLMN_CHECK_DEFAULT;
      break;
    case MMGSDI_FEATURE_IGNORE_INIT_ERROR:
      features_status_list.ignore_init_error = MMGSDI_FEATURE_IGNORE_INIT_ERROR_DEFAULT;
      break;
    case MMGSDI_FEATURE_ALLOW_SEND_APDU_ONLY:
      features_status_list.allow_send_apdu_only = MMGSDI_FEATURE_ALLOW_SEND_APDU_ONLY_DEFAULT;
      break;
    case MMGSDI_FEATURE_PREF_MODE_NOT_FROM_RUIM:
      features_status_list.pref_mode_not_from_ruim = MMGSDI_FEATURE_PREF_MODE_NOT_FROM_RUIM_DEFAULT;
      break;
    case MMGSDI_FEATURE_RUIM_ALLOW_ESN_FAIL:
      features_status_list.ruim_allow_esn_fail = MMGSDI_FEATURE_RUIM_ALLOW_ESN_FAIL_DEFAULT;
      break;
    case MMGSDI_FEATURE_RUIM_ZERO_ESN_FAIL:
      features_status_list.ruim_zero_esn_fail = MMGSDI_FEATURE_RUIM_ZERO_ESN_FAIL_DEFAULT;
      break;
    case MMGSDI_FEATURE_UIM_RUIM_SUPPORT_SCI:
      features_status_list.uim_ruim_support_sci = MMGSDI_FEATURE_UIM_RUIM_SUPPORT_SCI_DEFAULT;
      break;
    case MMGSDI_FEATURE_UIM_AN_HRPD_FALLBACK:
      features_status_list.uim_an_hrpd_fallback = MMGSDI_FEATURE_UIM_AN_HRPD_FALLBACK_DEFAULT;
      break;
    case MMGSDI_FEATURE_UIM_MISCONFIG_RUIM_N5_WORKAROUND:
      features_status_list.uim_misconfig_ruim_n5_workaround = MMGSDI_FEATURE_UIM_MISCONFIG_RUIM_N5_WORKAROUND_DEFAULT;
      break;
    case MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1:
      features_status_list.personalization_slots_locked_on_slot1 = MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1_DEFAULT;
      break;
    case MMGSDI_FEATURE_CSIM_UPP_UPDATE_AFTER_OTASP:
      features_status_list.csim_upp_update_after_otasp = MMGSDI_FEATURE_CSIM_UPP_UPDATE_AFTER_OTASP_DEFAULT;
      break;
    case MMGSDI_FEATURE_ALLOW_ACCESS_BEFORE_APP_READY:
      features_status_list.allow_access_before_app_ready = MMGSDI_FEATURE_ALLOW_ACCESS_BEFORE_APP_READY_DEFAULT;
      break;
    case MMGSDI_FEATURE_SKIP_UICC_SECURITY_ATTR_CHECK_BEFORE_READ:
      features_status_list.skip_uicc_security_attr_check_before_read = MMGSDI_FEATURE_SKIP_UICC_SECURITY_ATTR_CHECK_BEFORE_READ_DEFAULT;
      break;
    case MMGSDI_FEATURE_BLOCK_WRITES_TO_REFRESH_FILES:
      features_status_list.block_writes_to_refresh_files = MMGSDI_FEATURE_BLOCK_WRITES_TO_REFRESH_FILES_DEFAULT;
      break;
    case MMGSDI_FEATURE_SE13_TABLE_LOOKUP_GET_FIRST_GSMA_MATCH:
      features_status_list.se13_table_lookup_get_first_gsma_match =
        MMGSDI_FEATURE_SE13_TABLE_LOOKUP_GET_FIRST_GSMA_MATCH_DEFAULT;
      break;
    case MMGSDI_FEATURE_NO_PERSONALIZATION_ON_ADDITIONAL_SLOTS:
      features_status_list.no_personalization_on_additional_slots = MMGSDI_FEATURE_NO_PERSONALIZATION_ON_ADDITIONAL_SLOTS_DEFAULT;
      break;
    case MMGSDI_FEATURE_MMGSDI_UNBLOCK_PIN_HANDLE_INVALID_SW:
      features_status_list.unblock_pin_handle_invalid = MMGSDI_FEATURE_UNBLOCK_PIN_HANDLE_INVALID_SW_DEFAULT;
      break;
    case MMGSDI_FEATURE_UIM_GSM_DCS_1800:
      features_status_list.uim_gsm_dcs_1800 = MMGSDI_FEATURE_UIM_GSM_DCS_1800_DEFAULT;
      break;
    case MMGSDI_FEATURE_DEPRECATED_2:
      features_status_list.deprecated_2 = MMGSDI_FEATURE_DEPRECATED_2_DEFAULT;
      break;
    case MMGSDI_FEATURE_EXPLICIT_SELECT_DFCDMA_BEFORE_TELECOM_ADN_READ:
      features_status_list.explicit_select_dfcdma_before_telecom_adn_read = MMGSDI_FEATURE_EXPLICIT_SELECT_DFCDMA_BEFORE_TELECOM_ADN_READ_DEFAULT;
      break;
    case MMGSDI_FEATURE_OPL_PNN_LOOKUP_FOR_NON_RPLMN:
      features_status_list.opl_pnn_lookup_for_non_rplmn = MMGSDI_FEATURE_OPL_PNN_LOOKUP_FOR_NON_RPLMN_DEFAULT;
      break;
    case MMGSDI_FEATURE_HALT_3GPP2_SUBS_IF_3GPP_PERSO_FAILS:
      features_status_list.halt_3gpp2_subs_if_3gpp_perso_fails = MMGSDI_FEATURE_HALT_3GPP2_SUBS_IF_3GPP_PERSO_FAILS_DEFAULT;
      break;
    case MMGSDI_FEATURE_MULTISIM_AUTO_PROVISIONING:
      features_status_list.multisim_auto_provisioning = MMGSDI_FEATURE_MULTISIM_AUTO_PROVISIONING_DEFAULT;
      break;
    case MMGSDI_FEATURE_NORTH_AMERICAN_PLMN_COMPARISON:
      features_status_list.north_american_plmn_comparison = MMGSDI_FEATURE_NORTH_AMERICAN_PLMN_COMPARISON_DEFAULT;
      break;
    case MMGSDI_FEATURE_HALT_3GPP_SUBS_IF_3GPP2_PERSO_FAILS:
      features_status_list.halt_3gpp_subs_if_3gpp2_perso_fails = MMGSDI_FEATURE_HALT_3GPP_SUBS_IF_3GPP2_PERSO_FAILS_DEFAULT;
      break;
    case MMGSDI_FEATURE_SKIP_PROV_APP_TERMINATION:
      features_status_list.skip_prov_app_termination = MMGSDI_FEATURE_SKIP_PROV_APP_TERMINATION_DEFAULT;
      break;
    case MMGSDI_FEATURE_REFRESH_3G_SESSION_RESET_PER_ETSI_SPEC:
      features_status_list.refresh_3g_session_reset_per_etsi_spec = MMGSDI_FEATURE_REFRESH_3G_SESSION_RESET_PER_ETSI_SPEC_DEFAULT;
      break;
    case MMGSDI_FEATURE_USE_3GPD_CREDENTIALS_FROM_NV:
      features_status_list.use_3gpd_credentials_from_nv = MMGSDI_FEATURE_USE_3GPD_CREDENTIALS_FROM_NV_DEFAULT;
      break;
    case MMGSDI_FEATURE_USE_SFI:
      features_status_list.use_sfi = MMGSDI_FEATURE_USE_SFI_DEFAULT;
      break;
    case MMGSDI_FEATURE_OPLMN_LIST_CONFIGURABLE_OTA:
      features_status_list.oplmn_list_configurable_ota = MMGSDI_FEATURE_OPLMN_LIST_CONFIGURABLE_OTA_DEFAULT;
      break;
    case MMGSDI_FEATURE_PURGE_TEMP_IDENTITIES:
      features_status_list.purge_temp_identities = MMGSDI_FEATURE_PURGE_TEMP_IDENTITIES_DEFAULT;
      break;
    case MMGSDI_FEATURE_VERIFY_PIN_AFTER_CHANGE:
      features_status_list.verify_pin_after_change = MMGSDI_FEATURE_VERIFY_PIN_AFTER_CHANGE_DEFAULT;
      break;
    case MMGSDI_FEATURE_BLOCK_RUIM_EPRL_ACCESS:
      features_status_list.block_ruim_eprl_access = MMGSDI_FEATURE_BLOCK_RUIM_EPRL_ACCESS_DEFAULT;
      break;
    default:
      UIM_MSG_ERR_1("Invalid Feature 0x%x", nv_feature);
      return MMGSDI_ERROR;
  }
  return MMGSDI_SUCCESS;
} /* mmgsdi_nv_feature_write_default_value */


/*===========================================================================
FUNCTION MMGSDI_NV_WRITE_DEFAULT_VALUE_OF_DELTA_FEATURES

DESCRIPTION
  Function used to write the default values of MMGSDI Features (controlled by
  MMGSDI_Feature_Status_list NVITEM) that are delta between the two
  versions of Feature_Status_List to global features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void mmgsdi_nv_write_default_value_of_delta_features(
  uint8 version_in_nv,
  uint8 version_in_build
)
{
  uint8 mmgsdi_feature;
  uint8 num_features_in_nv;
  uint8 num_features_in_build;

  if((version_in_nv >= sizeof(mmgsdi_features_total_till_version)/sizeof(uint8)) ||
     (version_in_build >= sizeof(mmgsdi_features_total_till_version)/sizeof(uint8)))
  {
    UIM_MSG_ERR_0("Array out-of-bounds while writing default values for MMGSDI features");
    return;
  }

  num_features_in_nv    = mmgsdi_features_total_till_version[version_in_nv];
  num_features_in_build = mmgsdi_features_total_till_version[version_in_build];

  /* Since mmgsdi_features_total_till_version array is indexed by the
     feature_status_list *version*, it's value at version version_in_nv is
     essentially the first feature of the delta list...no need to bail out in
     case writing fails... */
  for(mmgsdi_feature = num_features_in_nv; mmgsdi_feature < num_features_in_build; mmgsdi_feature++)
  {
    (void)mmgsdi_nv_feature_write_default_value((mmgsdi_nv_features_enum_type )mmgsdi_feature);
  }
} /* mmgsdi_nv_write_default_value_of_delta_features */


/*===========================================================================
FUNCTION MMGSDI_NV_WRITE_DEFAULT_VALUE_OF_ALL_FEATURES

DESCRIPTION
  Function used to write the default values of all the MMGSDI Features
  (controlled by MMGSDI_Feature_Status_list NVITEM), defined currently, to
  global features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void mmgsdi_nv_write_default_value_of_all_features(
  void)
{
  uint8 mmgsdi_feature;
  uint8 num_features_in_build;

  num_features_in_build = mmgsdi_features_total_till_version[MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION];

  memset(&features_status_list, 0x00, sizeof(features_status_list));

  /* Update the Version in features_status_list to the current build version */
  features_status_list.version = MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION;

  /* Update features_status_list with default values */
  for(mmgsdi_feature = 0; mmgsdi_feature < num_features_in_build; mmgsdi_feature++)
  {
    (void)mmgsdi_nv_feature_write_default_value((mmgsdi_nv_features_enum_type )mmgsdi_feature);
  }
} /* mmgsdi_nv_write_default_value_of_all_features */


/*===========================================================================
FUNCTION MMGSDI_NV_FEATURES_STATUS_LIST_UPDATE_VERSION

DESCRIPTION
  Function used to validate the MMGSDI Features Status NVITEM's version.
  If what's in the NV is less than the one currently defined in the build,
  updates it to the current version defined and writes it back to the NV.
  It also writes back to the NV the default values of the delta items in
  in the list.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Version gets updated in the NV if it doesn't match the one in the build. If
  the version is updated, next time when the user reads the NVITEM, a
  dropdown of items corresponding to the updated version is shown instead of
  what the user saw the first time the NV read was done (corresponding to
  the version present in the NV at that point).
===========================================================================*/
static void mmgsdi_nv_features_status_list_update_version(
  void)
{
  int status = 0;

  UIM_MSG_HIGH_2("MMGSDI Features Status NV ITEM version - in NV: 0x%x, in build: 0x%x",
                 features_status_list.version,
                 MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION);

  if (features_status_list.version < MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION)
  {
    /* If the features_status_list version in NV is less than current
       features_status_list version in our code, meaning new features have been
       added since the version in NV till the current version, we write back
       the default value of all those delta features to the NV. */
    mmgsdi_nv_write_default_value_of_delta_features(
      features_status_list.version,
      MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION);

    features_status_list.version = MMGSDI_FEATURES_STATUS_LIST_CURRENT_VERSION;
    status = efs_put(MMGSDI_FEATURES_STATUS_LIST_FILE, &features_status_list,
                     sizeof(features_status_list),
                     O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
    if (status != 0)
    {
      UIM_MSG_ERR_1("MMGSDI Features Status NV ITEM version update failed!, error %d",
                    status);
    }
  }
} /* mmgsdi_nv_features_status_list_update_version */


/*===========================================================================
FUNCTION MMGSDI_NV_INIT_FEATURES_STATUS_NV_LOOKUP

DESCRIPTION
  Function used to get the status of MMGSDI features as defined by the NVITEM
  "MMGSDI_FEATURES_STATUS_LIST" (67211).
  The global, "features_status_list", used to hold the NVITEM, is expandable,
  yet the size is fixed. See the struct definition for more detailed doc on
  that. The global struct has a "version" item that is updated everytime we
  add new items to this global struct (starting with version 0 in the first
  implementation)
  The mmgsdi code has full control over version of NVITEM. Taking an example,
  if in future, in version 2 of this struct(that has 10 items), the delta is 3
  items (from version 0 that had 7 items), and this build is loaded onto the
  device that already had the items set using version 0 of this NVITEM, then,
  on boot-up after loading the build, mmgsdi will validate the version it read
  from NV. If it is not 2, it will update it to 2 and write it back to NV. At
  this point, however, the value of features/items 8 through 10 will be set to
  their default values by mmgsdi code.

DEPENDENCIES
  Minimum QXDM version required to be able to set this NVITEM (67211) is
  QXDM 3.12.997.

RETURN VALUE
  None

SIDE EFFECTS
  "version" item in this NVITEM is writable through QXDM (no QXDM support to
  hide it or grey it out) or QCN. However if what is written explicitly doesn't
  match what's in the build, mmgsdi will change it to what's in the build and
  it could be annoying for the QXDM user to see it change to something else
  (when the user reads the NVITEM next time).
===========================================================================*/
void mmgsdi_nv_init_features_status_nv_lookup(
  void)
{
  /* Start off with the global struct initialised to 0 */
  memset(&features_status_list, 0x00, sizeof(features_status_list));

  if (efs_get(MMGSDI_FEATURES_STATUS_LIST_FILE, &features_status_list,
              sizeof(features_status_list))
              == sizeof(features_status_list))
  {
    UIM_MSG_MED_0("Successfully read the MMGSDI Features Status from NV");
    mmgsdi_nv_features_status_list_update_version();
  }
  else
  {
    UIM_MSG_HIGH_0("Failed to read MMGSDI Features Status from NV. Writing back default values");

    /* Updating global features_status_list with default value of all the
       defined features */
    mmgsdi_nv_write_default_value_of_all_features();

    /* Write features' status to the EFS, erasing previous contents,
    create if not present */
    (void)efs_put(MMGSDI_FEATURES_STATUS_LIST_FILE, &features_status_list,
                  sizeof(features_status_list),
                  O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
  }
} /* mmgsdi_nv_init_features_status_nv_lookup */


/*===========================================================================
FUNCTION MMGSDI_NV_INIT_EXTERNAL_FEATURES_STATUS_NV_LOOKUP

DESCRIPTION
  Function used to get the status of external features (i.e. features
  outside of MMGSDI), stored in EFS based NV.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void mmgsdi_nv_init_external_features_status_nv_lookup(void)
{
  uint32 gstk_features_data = 0;
  uint8  ims_support_status = 0;

#ifdef FEATURE_FEMTO_CSG
  sys_csg_support_e_type   csg_support_status = SYS_CSG_SUPPORT_NONE;
#endif /* FEATURE_FEMTO_CSG */

  PACKED struct PACKED_POST
  {
    uint8                      is_rpm_enabled;
    uint8                      app_max_num_reset;
    uint8                      app_reset_counter;
    uint32                     app_reset_timer;
    uint32                     app_reset_block_leak_rate_timer;
    uint8                      average_reset_time;
    uint32                     leak_rate_timer;
  } rpm_info;

  /* Start off with the global struct initialised to 0 */
  memset(&external_features_status_list, 0x00,
         sizeof(external_features_status_list));

  /* Read the GSTK Features file (corresponding to NV 65674) from EFS */
  if (efs_get(GSTK_FEATURES_FILE,
              &gstk_features_data,
              sizeof(gstk_features_data)) ==
        sizeof(gstk_features_data))
  {
    /* Retrieve and store the status of the OTASP IMSI_M enahncement feature */
    if(gstk_features_data & GSTK_CFG_FEATURE_IMSI_M_ENH)
    {
      external_features_status_list.gstk_cfg_feature_imsi_m_enh =
        MMGSDI_FEATURE_ENABLED;
    }

    UIM_MSG_MED_1("Successfully read the GSTK features status from NV, OTASP IMSI_M ENH status: 0x%x",
                  external_features_status_list.gstk_cfg_feature_imsi_m_enh);
  }
  else
  {
    UIM_MSG_HIGH_0("Failed to read GSTK features Status from NV, setting OTASP IMSI_M ENH status to FALSE");
  }

#ifdef FEATURE_FEMTO_CSG
  /* Default value is support enabled */
  external_features_status_list.nas_cfg_feature_csg = MMGSDI_FEATURE_ENABLED;

  /* Read the CSG Features file (corresponding to NV 70361) from EFS. */
  if (efs_get(NAS_CSG_SUPPORT_FILE,
              &csg_support_status,
              sizeof(csg_support_status)) ==  sizeof(csg_support_status))
  {
    /* Retrieve and store the status of the CSG feature */
    if(csg_support_status == SYS_CSG_SUPPORT_DISABLED)
    {
      external_features_status_list.nas_cfg_feature_csg =
        MMGSDI_FEATURE_DISABLED;
    }

    UIM_MSG_MED_1("Successfully read the CSG features status from NV, CSG support status: 0x%x",
                  external_features_status_list.nas_cfg_feature_csg);
  }
  else
  {
    UIM_MSG_HIGH_0("Failed to read CSG features Status from NV, setting status to ENABLED (Default)");
  }
#else
  external_features_status_list.nas_cfg_feature_csg = MMGSDI_FEATURE_DISABLED;
#endif /* FEATURE_FEMTO_CSG */

  /* Default value is support disabled */
  external_features_status_list.ims_cfg_feature_ims_support = MMGSDI_FEATURE_DISABLED;

  /* Read the IMS Features file (corresponding to NV 67218) from EFS. */
  if (efs_get(IMS_SUPPORT_FILE,
              &ims_support_status,
              sizeof(ims_support_status)) ==  sizeof(ims_support_status))
  {
    /* Retrieve and store the status of the IMS support
       0 - Disabled
       1 - Enabled
       2 - Proprietary IMS support */
    if(ims_support_status != 0)
    {
      external_features_status_list.ims_cfg_feature_ims_support =
        MMGSDI_FEATURE_ENABLED;
    }

    UIM_MSG_MED_1("Successfully read the IMS support status from NV, IMS support status: 0x%x",
                  external_features_status_list.ims_cfg_feature_ims_support);
  }
  else
  {
    UIM_MSG_HIGH_0("Failed to read IMS features Status from NV, setting status to DISABLED (Default)");
  }

  /* Read the RPM info file (corresponding to NV 67210) from EFS */
  if (efs_get(RPM_INFO_FILE,
              &rpm_info,
              sizeof(rpm_info)) ==
        sizeof(rpm_info))
  {
    if(rpm_info.is_rpm_enabled == TRUE)
    {
      external_features_status_list.rpm_enabled =
        MMGSDI_FEATURE_ENABLED;
    }

    UIM_MSG_MED_1("Successfully read the RPM info file from NV, RPM enabled: 0x%x",
                  external_features_status_list.rpm_enabled);
  }
  else
  {
    UIM_MSG_HIGH_0("Failed to read the RPM info file from NV, setting RPM to disabled");
  }
} /* mmgsdi_nv_init_external_features_status_nv_lookup */


/*===========================================================================
FUNCTION MMGSDI_NV_GET_FEATURE_STATUS

DESCRIPTION
  Function used to get the status of MMGSDI features listed in enum
  mmgsdi_nv_features_enum_type  as defined by the NVITEM 67211 -
  "MMGSDI_FEATURES_STATUS_LIST".

DEPENDENCIES
  Minimum QXDM version required to be able to read/write this NVITEM is
  QXDM 3.12.997.

RETURN VALUE
  mmgsdi_feature_status_enum

SIDE EFFECTS
  None
===========================================================================*/
mmgsdi_feature_status_enum_type mmgsdi_nv_get_feature_status(
  mmgsdi_nv_features_enum_type  nv_feature)
{
  mmgsdi_feature_status_enum_type ret_value = MMGSDI_FEATURE_DISABLED;

  switch(nv_feature)
  {
    case MMGSDI_FEATURE_ABSENT_MANDATORY_FILES_INIT:
      ret_value = features_status_list.absent_mandatory_files_init;
      break;
    case MMGSDI_FEATURE_ECC_NOT_PRESENT_SUPPORT:
      ret_value = features_status_list.ecc_not_present_support;
      break;
    case MMGSDI_FEATURE_SPECIAL_USIM_ARR:
      ret_value = features_status_list.special_usim_arr;
      break;
    case MMGSDI_FEATURE_CDMA_DF_SELECT_BEFORE_TP:
      ret_value = features_status_list.cdma_df_select_before_tp;
      break;
    case MMGSDI_FEATURE_CHANGE_INST_CLASS:
      ret_value = features_status_list.change_inst_class;
      break;
    case MMGSDI_FEATURE_CARD_SLOT_CHECK:
      ret_value = features_status_list.card_slot_check;
      break;
    case MMGSDI_FEATURE_TMSI_PLMN_CHECK:
      ret_value = features_status_list.tmsi_plmn_check;
      break;
    case MMGSDI_FEATURE_IGNORE_INIT_ERROR:
      ret_value = features_status_list.ignore_init_error;
      break;
    case MMGSDI_FEATURE_ALLOW_SEND_APDU_ONLY:
      ret_value = features_status_list.allow_send_apdu_only;
      break;
    case MMGSDI_FEATURE_PREF_MODE_NOT_FROM_RUIM:
      ret_value = features_status_list.pref_mode_not_from_ruim;
      break;
    case MMGSDI_FEATURE_RUIM_ALLOW_ESN_FAIL:
      ret_value = features_status_list.ruim_allow_esn_fail;
      break;
    case MMGSDI_FEATURE_RUIM_ZERO_ESN_FAIL:
      ret_value = features_status_list.ruim_zero_esn_fail;
      break;
    case MMGSDI_FEATURE_UIM_RUIM_SUPPORT_SCI:
      ret_value = features_status_list.uim_ruim_support_sci;
      break;
    case MMGSDI_FEATURE_UIM_AN_HRPD_FALLBACK:
      ret_value = features_status_list.uim_an_hrpd_fallback;
      break;
    case MMGSDI_FEATURE_UIM_MISCONFIG_RUIM_N5_WORKAROUND:
      ret_value = features_status_list.uim_misconfig_ruim_n5_workaround;
      break;
    case MMGSDI_FEATURE_PERSONALIZATION_SLOTS_LOCKED_ON_SLOT1:
      ret_value = features_status_list.personalization_slots_locked_on_slot1;
      break;
    case MMGSDI_FEATURE_CSIM_UPP_UPDATE_AFTER_OTASP:
      ret_value = features_status_list.csim_upp_update_after_otasp;
      break;
    case MMGSDI_FEATURE_ALLOW_ACCESS_BEFORE_APP_READY:
      ret_value = features_status_list.allow_access_before_app_ready;
      break;
    case MMGSDI_FEATURE_SKIP_UICC_SECURITY_ATTR_CHECK_BEFORE_READ:
      ret_value = features_status_list.skip_uicc_security_attr_check_before_read;
      break;
    case MMGSDI_FEATURE_BLOCK_WRITES_TO_REFRESH_FILES:
      ret_value = features_status_list.block_writes_to_refresh_files;
      break;
    case MMGSDI_FEATURE_SE13_TABLE_LOOKUP_GET_FIRST_GSMA_MATCH:
      ret_value = features_status_list.se13_table_lookup_get_first_gsma_match;
      break;
    case MMGSDI_FEATURE_NO_PERSONALIZATION_ON_ADDITIONAL_SLOTS:
      ret_value = features_status_list.no_personalization_on_additional_slots;
      break;
    case MMGSDI_FEATURE_MMGSDI_UNBLOCK_PIN_HANDLE_INVALID_SW:
      ret_value = features_status_list.unblock_pin_handle_invalid;
      break;
    case MMGSDI_FEATURE_UIM_GSM_DCS_1800:
      ret_value = features_status_list.uim_gsm_dcs_1800;
      break;
    case MMGSDI_FEATURE_EXPLICIT_SELECT_DFCDMA_BEFORE_TELECOM_ADN_READ:
      ret_value = features_status_list.explicit_select_dfcdma_before_telecom_adn_read;
      break;
    case MMGSDI_FEATURE_OPL_PNN_LOOKUP_FOR_NON_RPLMN:
      ret_value = features_status_list.opl_pnn_lookup_for_non_rplmn;
      break;
    case MMGSDI_FEATURE_HALT_3GPP2_SUBS_IF_3GPP_PERSO_FAILS:
      ret_value = features_status_list.halt_3gpp2_subs_if_3gpp_perso_fails;
      break;
    case MMGSDI_FEATURE_MULTISIM_AUTO_PROVISIONING:
      ret_value = features_status_list.multisim_auto_provisioning;
      break;
    case MMGSDI_FEATURE_NORTH_AMERICAN_PLMN_COMPARISON:
      ret_value = features_status_list.north_american_plmn_comparison;
      break;
    case MMGSDI_FEATURE_HALT_3GPP_SUBS_IF_3GPP2_PERSO_FAILS:
      ret_value = features_status_list.halt_3gpp_subs_if_3gpp2_perso_fails;
      break;
    case MMGSDI_FEATURE_SKIP_PROV_APP_TERMINATION:
      ret_value = features_status_list.skip_prov_app_termination;
      break;
    case MMGSDI_FEATURE_REFRESH_3G_SESSION_RESET_PER_ETSI_SPEC:
      ret_value = features_status_list.refresh_3g_session_reset_per_etsi_spec;
      break;
    case MMGSDI_FEATURE_USE_3GPD_CREDENTIALS_FROM_NV:
      ret_value = features_status_list.use_3gpd_credentials_from_nv;
      break;
    case MMGSDI_FEATURE_USE_SFI:
      ret_value = features_status_list.use_sfi;
      break;
    case MMGSDI_FEATURE_OPLMN_LIST_CONFIGURABLE_OTA:
      ret_value = features_status_list.oplmn_list_configurable_ota;
      break;
    case MMGSDI_FEATURE_PURGE_TEMP_IDENTITIES:
      ret_value = features_status_list.purge_temp_identities;
      break;
    case MMGSDI_FEATURE_VERIFY_PIN_AFTER_CHANGE:
      ret_value = features_status_list.verify_pin_after_change;
      break;
    case MMGSDI_FEATURE_BLOCK_RUIM_EPRL_ACCESS:
      ret_value = features_status_list.block_ruim_eprl_access;
      break;
    default:
      UIM_MSG_HIGH_1("Invalid Feature 0x%x", nv_feature);
      return ret_value;
  }
  UIM_MSG_HIGH_2("Status of feature 0x%x is 0x%x", nv_feature, ret_value);
  return ret_value;
} /* mmgsdi_nv_get_feature_status */


/*===========================================================================
FUNCTION MMGSDI_NV_GET_EXTERNAL_FEATURE_STATUS

DESCRIPTION
  Function used to get the status of external features listed in the enum:
  mmgsdi_nv_external_features_enum.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_feature_status_enum

SIDE EFFECTS
  None
===========================================================================*/
mmgsdi_feature_status_enum_type mmgsdi_nv_get_external_feature_status(
  mmgsdi_nv_external_features_enum nv_feature)
{
  mmgsdi_feature_status_enum_type ret_value = MMGSDI_FEATURE_DISABLED;

  switch(nv_feature)
  {
    case MMGSDI_EXT_FEAT_GSTK_CFG_FEATURE_IMSI_M_ENH:
      ret_value = external_features_status_list.gstk_cfg_feature_imsi_m_enh;
      break;

    case MMGSDI_EXT_FEAT_NAS_CFG_FEATURE_CSG:
      ret_value = external_features_status_list.nas_cfg_feature_csg;
      break;

    case MMGSDI_EXT_FEAT_IMS_CFG_FEATURE_IMS_SUPPORT:
      ret_value = external_features_status_list.ims_cfg_feature_ims_support;
      break;

    case MMGSDI_EXT_FEAT_RPM_ENABLED:
      ret_value = external_features_status_list.rpm_enabled;
      break;

    default:
      UIM_MSG_HIGH_1("Invalid external feature 0x%x", nv_feature);
      return ret_value;
  }

  UIM_MSG_HIGH_2("Status of external feature 0x%x is 0x%x", nv_feature, ret_value);

  return ret_value;
} /* mmgsdi_nv_get_external_feature_status */


/*===========================================================================
FUNCTION MMGSDI_NV_UPDATE_RPM_ICCID

DESCRIPTION
  Compare the previously stored ICCID to the current ICCID of the card.  If the
  values are different update nv with the new ICCID from the card.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  None

SIDE EFFECTS
  mmgsdi_rpm_iccid_has_changed_slot will be:
    TRUE : if the ICCID is different than the last card inserted in the ME
    FALSE: if the ICCID is the same as the last card inserted in the ME
===========================================================================*/
mmgsdi_return_enum_type mmgsdi_nv_update_rpm_iccid (
  mmgsdi_data_type         new_rpm_iccid,
  mmgsdi_slot_id_enum_type mmgsdi_slot
)
{
  uint8                   mmgsdi_rpm_iccid[MMGSDI_MAX_NUM_SLOTS][MMGSDI_ICCID_LEN];
  int                     status                               = 0;
  mmgsdi_slot_data_type  *slot_data_ptr                        = NULL;
  uint8                   slot_index                           = MMGSDI_SLOT_1_INDEX;
  mmgsdi_return_enum_type mmgsdi_status                        = MMGSDI_SUCCESS;

  UIM_MSG_HIGH_0("UPDATE RPM ICCID");

  memset(mmgsdi_rpm_iccid, 0x00, sizeof(mmgsdi_rpm_iccid));

  if((new_rpm_iccid.data_ptr == NULL) ||
     (new_rpm_iccid.data_len != MMGSDI_ICCID_LEN))
  {
    return MMGSDI_INCORRECT_PARAMS;
  }

  status = efs_get(MMGSDI_RPM_ICCID_FILE, mmgsdi_rpm_iccid, sizeof(mmgsdi_rpm_iccid));
  if(status < 0)
  {
    UIM_MSG_ERR_0("Could not get RPM ICCID from NV");
  }

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(mmgsdi_slot);
  if(slot_data_ptr == NULL)
  {
    return MMGSDI_ERROR;
  }

  mmgsdi_status = mmgsdi_util_get_slot_index(mmgsdi_slot, &slot_index);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    return MMGSDI_ERROR;
  }

  if(memcmp(mmgsdi_rpm_iccid[slot_index],
            new_rpm_iccid.data_ptr,
            MMGSDI_ICCID_LEN))
  {
    UIM_MSG_HIGH_1("RPM ICCID has changed for SLOT 0x%x", mmgsdi_slot);
    slot_data_ptr->mmgsdi_rpm_iccid_has_changed = TRUE;
    mmgsdi_memscpy(mmgsdi_rpm_iccid[slot_index],
                   sizeof(mmgsdi_rpm_iccid[slot_index]),
                   new_rpm_iccid.data_ptr,
                   MMGSDI_ICCID_LEN);
    status = efs_put(MMGSDI_RPM_ICCID_FILE, mmgsdi_rpm_iccid, sizeof(mmgsdi_rpm_iccid),
                       O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
    if(status < 0)
    {
      UIM_MSG_ERR_1("Failed writing RPM ICCID for SLOT 0x%x", mmgsdi_slot);
      return MMGSDI_ERROR;
    }
    return MMGSDI_SUCCESS;
  }
  else
  {
    UIM_MSG_HIGH_1("RPM ICCID has not changed for SLOT 0x%x", mmgsdi_slot);
    slot_data_ptr->mmgsdi_rpm_iccid_has_changed = FALSE;
    return MMGSDI_SUCCESS;
  }
} /* mmgsdi_nv_update_rpm_iccid */


/*===========================================================================
FUNCTION MMGSDI_NV_GET_RPM_ICCID_HAS_CHANGED

DESCRIPTION
  Return whether the ICCID is different than the last ICCID read from the card

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  Boolean - TRUE : if the ICCID is changed
            FALSE: if the ICCID is the same

SIDE EFFECTS
  None
===========================================================================*/
boolean mmgsdi_nv_get_rpm_iccid_has_changed (
  mmgsdi_slot_id_enum_type mmgsdi_slot
)
{
  mmgsdi_slot_data_type  *slot_data_ptr = NULL;
  boolean                 rpm_iccid_has_changed = FALSE;

  /* Protect access to slot data table */
  MMGSDIUTIL_ENTER_CLIENT_APP_DATA_CRIT_SECT

  slot_data_ptr = mmgsdi_util_get_slot_data_ptr(mmgsdi_slot);
  if(slot_data_ptr == NULL)
  {
    MMGSDIUTIL_LEAVE_CLIENT_APP_DATA_CRIT_SECT
    return FALSE;
  }
  rpm_iccid_has_changed = slot_data_ptr->mmgsdi_rpm_iccid_has_changed;

  MMGSDIUTIL_LEAVE_CLIENT_APP_DATA_CRIT_SECT
  return rpm_iccid_has_changed;
} /* mmgsdi_nv_get_rpm_iccid_has_changed */


/*===========================================================================
FUNCTION MMGSDI_NV_COMPARE_AND_UPDATE_CONF_FILE

DESCRIPTION
  This function attempts to open the conf file, create it if it does not exist,
  compares its contents against the passed buffer and if the two are different,
  empties the file and writes back the buffer data to the file. It also writes
  the buffer data to the file if file couldn't be read.

PARAMETERS
  file_path_ptr: File path
  buf_ptr:       Buffer to compare against the file data
  buf_len:       Buffer length

DEPENDENCIES
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None

SEE ALSO
  None
===========================================================================*/
void mmgsdi_nv_compare_and_update_conf_file(
  const char *                file_path_ptr,
  const char *                buf_ptr,
  uint16                      buf_len
)
{
  int32                   fd                    = 0;
  char                   *file_data_ptr         = NULL;
  struct fs_stat          conf_stat;

  if (!file_path_ptr || !buf_ptr || buf_len == 0)
  {
    return;
  }

  /* Get the size of the conf file */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if(efs_stat(file_path_ptr, &conf_stat) >= 0 &&
     conf_stat.st_size == buf_len)
  {
    /* Stat succeeded meaning the file exists - open it and read the data */
    fd = efs_open(file_path_ptr, O_RDONLY);
    if (fd < 0)
    {
      UIM_MSG_ERR_0("Error opening file");
      return;
    }

    /* If the file size is non-zero, read data from file to compare against the
       buffer and write back to the file only if the data is different. If file
       read fails, still write the buffer to the file  */
    MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(file_data_ptr, conf_stat.st_size)
    if (file_data_ptr != NULL)
    {
      if(efs_read(fd, file_data_ptr, conf_stat.st_size) == conf_stat.st_size)
      {
        if(memcmp(file_data_ptr, buf_ptr, buf_len) == 0)
        {
          /* file data and the buffer data are same,
             so return without updating the file */
          MMGSDIUTIL_TMC_MEM_FREE(file_data_ptr);
          (void)efs_close(fd);
          return;
        }
      }
      MMGSDIUTIL_TMC_MEM_FREE(file_data_ptr);
    }

    /* Close it since we will anyways have to open it again in
       TRUNC mode in order to write to it */
    (void)efs_close(fd);
  }

  /* File doesn't exist or it's contents do not match buffer data;
     write buffer to the file */
  fd = efs_open (file_path_ptr, O_WRONLY | O_TRUNC | O_CREAT);
  if (fd >= 0)
  {
    (void)efs_write(fd, buf_ptr, buf_len);
    (void)efs_close(fd);
  }
} /* mmgsdi_nv_compare_and_update_conf_file */


/*===========================================================================
FUNCTION MMGSDI_NV_CREATE_CONF_FILE

DESCRIPTION
  Function used to create a .conf file in EFS if not present. the .conf file
  is used to specify UIM NV items (except UIMDRV items) that can be backed up
  in a QCN file.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void mmgsdi_nv_create_conf_file(
  void
)
{
  char           *buffer_ptr       = NULL;
  uint16          buffer_len       = 0;
  uint16          buffer_index     = 0;
  uint8           file_index       = 0;
  uint8           num_config_files = 0;
  struct fs_stat  conf_stat;
  char           *conf_files[] =
  {
    /* MMGSDI EFS items */
    MMGSDI_CONFIG_FILE_1,
    MMGSDI_CONFIG_FILE_2,
    MMGSDI_CONFIG_FILE_3,
    MMGSDI_CONFIG_FILE_4,
    MMGSDI_CONFIG_FILE_5,
    MMGSDI_CONFIG_FILE_6,
    MMGSDI_FEATURES_STATUS_LIST_FILE,
    MMGSDI_ONCHIP_SUPPORT_FILE,
    MMGSDI_ONCHIP_CONFIG_EFS_FILE_SLOT1,
    MMGSDI_ENS_SUB_FEATURES_STATUS_LIST_FILE,
    MMGSDI_BUILTIN_PLMN_LIST_FILE,
    /* GSTK EFS items */
#ifdef FEATURE_GSTK
    GSTK_CONFIG_FILE_FEATURE_BMSK,
    GSTK_CONFIG_FILE_TP_CLIENTS,
    GSTK_CONFIG_FILE_ATTACH_CTRL_TIMER,
    GSTK_CONFIG_FILE_REDIAL_CFG,
    GSTK_CONFIG_FILE_PRO_FEAT_ENABLE_CFG,
    GSTK_CONFIG_FILE_CAT_VERSION,
    GSTK_CONFIG_FILE_QMI_CAT_MODE,
    GSTK_CONFIG_FILE_BIP_OTASP_CONCURRENCY,
    GSTK_CONFIG_FILE_TAL_CONF,
    GSTK_CONFIG_FILE_RAT_PRIORITY_CFG,
    GSTK_CONFIG_FILE_FEATURE_BMSK_2,
    GSTK_CONFIG_FILE_ME_FEATURE_BMSK,
#endif /* FEATURE_GSTK */
#ifdef FEATURE_ESTK
    ESTK_CONFIG_FILE_DEFAULT_APN,
    ESTK_CONFIG_FILE_BIP_APN_MAPPING_TBL,
#endif /* FEATURE_ESTK */
    /* QMI_CAT EFS items */
    CATI_CONFIG_FILE,
    CATI_GOBI_VS_CONFIG_FILE,
    CATI_CONFIG_TP,
    CATI_DEFAULT_LANG,
    CATI_DISPLAY_ALPHA_FILE,
    CATI_BLOCK_SMS_PP_ENV,
    /* QMI_UIM EFS items */
    QMI_UIM_CONFIG_SAP_SEC_RESTR,
    QMI_UIM_CONFIG_AUTH_SEC_RESTR,
    QMI_UIM_CONFIG_APDU_SEC_RESTR,
    QMI_UIM_CONFIG_SILENT_PIN1_FILE,
    QMI_UIM_CONFIG_APDU_SEC_AID_LIST,
    QMI_UIM_CONFIG_CLOSE_CHANNEL_SYNC,
    QMI_UIM_CONFIG_SILENT_RECOVERY
  };

  /* Create directory, if needed */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if (efs_stat(MMGSDI_CONF_DIR1, &conf_stat) == -1 )
  {
    (void)efs_mkdir(MMGSDI_CONF_DIR1, 0777);
    (void)efs_mkdir(MMGSDI_CONF_DIR2, 0777);
    (void)efs_mkdir(MMGSDI_CONF_DIR3, 0777);
  }
  else if (efs_stat(MMGSDI_CONF_DIR2, &conf_stat) == -1 )
  {
    (void)efs_mkdir(MMGSDI_CONF_DIR2, 0777);
    (void)efs_mkdir(MMGSDI_CONF_DIR3, 0777);
  }
  else if (efs_stat(MMGSDI_CONF_DIR3, &conf_stat) == -1 )
  {
    (void)efs_mkdir(MMGSDI_CONF_DIR3, 0777);
  }

  /* Calculate total length needed to write, note on total size needed:
     (num_config_files * (string len of each file + '\n')) */
  num_config_files = sizeof(conf_files)/sizeof(conf_files[0]);
  for (file_index = 0; file_index < num_config_files; file_index++)
  {
    buffer_len += strlen(conf_files[file_index]) + sizeof(char);
  }

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(buffer_ptr, buffer_len)
  if (buffer_ptr == NULL)
  {
    UIM_MSG_ERR_0( "Memory allocation failed for conf files buffer!");
    return;
  }

  /* If created first time, write the items */
  for (file_index = 0; file_index < num_config_files; file_index++)
  {
    uint16 len_to_write = strlen(conf_files[file_index]) + sizeof(char);

    /* Check if buffer_ptr has enough space left */
    if (len_to_write > (buffer_len - buffer_index))
    {
      UIM_MSG_ERR_1( "Insufficient buffer space for conf_files[%d] !",
                    file_index);
      MMGSDIUTIL_TMC_MEM_FREE(buffer_ptr);
      return;
    }
    /* Content of the file. Note: there should not be spaces before the names! */
    mmgsdi_memscpy((void*)&buffer_ptr[buffer_index],
                    buffer_len - buffer_index,
                   (void*)conf_files[file_index],
                   strlen(conf_files[file_index]));
    buffer_index += strlen(conf_files[file_index]);
    buffer_ptr[buffer_index++] = '\n';
  }

  mmgsdi_nv_compare_and_update_conf_file(MMGSDI_CONF_FILE,
                                         buffer_ptr,
                                         (uint16)buffer_len);

  MMGSDIUTIL_TMC_MEM_FREE(buffer_ptr);
} /* mmgsdi_nv_create_conf_file */


/*===========================================================================
FUNCTION MMGSDI_NV_SEND_RTRE_COMMAND_TO_NV

DESCRIPTION
  Sends RTRE configuration to NV, bypassing CM.

RETURN VALUE
  None

DEPENDENCIES
  None
===========================================================================*/
void mmgsdi_nv_send_rtre_command_to_nv(
  nv_item_type *data_ptr
)
{
  nv_cmd_type             *mmgsdi_nv_cmd_ptr  = NULL;
  mmgsdi_return_enum_type  mmgsdi_status      = MMGSDI_ERROR;
  nv_item_type            *cmd_data_ptr       = NULL;
  int32                    mmgsdi_nv_cmd_size = 0;

  if(data_ptr == NULL)
  {
    return;
  }

  /* Allocate memory for the NV command */
  mmgsdi_util_cal_align_size(sizeof(nv_cmd_type), &mmgsdi_nv_cmd_size);

  MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(mmgsdi_nv_cmd_ptr,
                                         (mmgsdi_nv_cmd_size + sizeof(nv_item_type)),
                                          mmgsdi_status);

  if((mmgsdi_status != MMGSDI_SUCCESS) || (mmgsdi_nv_cmd_ptr == NULL))
  {
    UIM_MSG_ERR_0("Failed to allocate memory");
    return;
  }

  /* Copy data to write */
  cmd_data_ptr = (nv_item_type*)((uint8*)mmgsdi_nv_cmd_ptr + mmgsdi_nv_cmd_size);
  mmgsdi_memscpy(cmd_data_ptr,
                 sizeof(nv_item_type),
                 data_ptr,
                 sizeof(nv_item_type));

  /* Notify main MMGSDI task when done */
  mmgsdi_nv_cmd_ptr->tcb_ptr    = UIM_GSDI_TCB;
  mmgsdi_nv_cmd_ptr->sigs       = MMGSDI_NV_WRITE_SIG;
  mmgsdi_nv_cmd_ptr->done_q_ptr = &mmgsdi_nv_q;

  /* Item to write */
  mmgsdi_nv_cmd_ptr->item = NV_RTRE_CONFIG_I;
  mmgsdi_nv_cmd_ptr->cmd  = NV_RTRE_OP_CONFIG_F;
  mmgsdi_nv_cmd_ptr->data_ptr = cmd_data_ptr;

  /* Send Command to NV */
  nv_cmd(mmgsdi_nv_cmd_ptr);

  mmgsdi_status = mmgsdi_nv_put_nv_item_in_cache(
                    NV_RTRE_CONFIG_I,
                    data_ptr,
                    NV_DONE_S);

  UIM_MSG_HIGH_2("RTRE CONFIG NV Status = 0x%x, Cache write status: 0x%x",
                 mmgsdi_nv_cmd_ptr->status, mmgsdi_status);
} /* mmgsdi_nv_send_rtre_command_to_nv */


/*===========================================================================
FUNCTION MMGSDI_NV_INIT_ONCHIP_AUTO_ACTIVATION_DATA

DESCRIPTION
  Reads the EFS item to figure out if the OnChip should be enabled on boot up
  or not. If it dictates that OnChip be auto activated on boot up, the EFS
  item contents are validated and are read into the MMGSDI global.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE:
  None

SIDE EFFECTS
  None
===========================================================================*/
void mmgsdi_nv_init_onchip_auto_activation_data (
  void
)
{
  int                                      efs_data_len         = 0;
  mmgsdi_app_enum_type                     mmgsdi_app_type      = MMGSDI_APP_NONE;
  boolean                                  auto_act_enabled     = FALSE;

  struct
  {
    uint8   auto_activation_enabled;
    uint8   app_type;
    uint8   slot_id;
    uint8   rfu[13];
  } onchip_auto_act_data;

  memset(&onchip_auto_act_data, 0x00, sizeof(onchip_auto_act_data));

  if(mmgsdi_generic_data_ptr == NULL)
  {
    return;
  }

  efs_data_len = efs_get(MMGSDI_ONCHIP_SUPPORT_FILE,
                         &onchip_auto_act_data,
                         sizeof(onchip_auto_act_data));

  /* Lets parse the EFS data; we copy it to our global only if it is valid */
  if(efs_data_len != sizeof(onchip_auto_act_data))
  {
    UIM_MSG_ERR_1("Invalid onchip EFS item len:0x%x", efs_data_len);
    return;
  }

  /* Validate the input that indicates whether onchip auto activation is
     enabled or not.  */
  switch(onchip_auto_act_data.auto_activation_enabled)
  {
    case 1:
      auto_act_enabled = TRUE;
      break;
    case 0:
    default:
      /* EFS item suggested that onchip auto-activation should not be performed.
         No need to validate rest of the contents of the EFS item */
      return;
  }

  /* Validate app type from the EFS item. OnChip currently supported only
     for SIM and USIM */
  switch(onchip_auto_act_data.app_type)
  {
    case 1:
      mmgsdi_app_type = MMGSDI_APP_SIM;
      break;
    case 3:
      mmgsdi_app_type = MMGSDI_APP_USIM;
      break;
    default:
      UIM_MSG_ERR_1("Invalid app type mentioned in OnChip EFS item: 0%x",
                    onchip_auto_act_data.app_type);
      return;
  }

  /* Validate the slot. Onchip currently is supported only for SLOT_1 so
     reject the request if some other slotid is mentioned in the EFS item */
  switch(onchip_auto_act_data.slot_id)
  {
    case 1:
      mmgsdi_generic_data_ptr->onchip_act_data_slot1.auto_activation_enabled =
        auto_act_enabled;
      mmgsdi_generic_data_ptr->onchip_act_data_slot1.app_type =
        mmgsdi_app_type;
      break;
    default:
      UIM_MSG_ERR_1("Invalid slot id 0x%x  app type mentioned in OnChip EFS item",
                    onchip_auto_act_data.app_type);
      break;
  }
} /* mmgsdi_nv_init_onchip_auto_activation_data */


/*===========================================================================
FUNCTION MMGSDI_NV_SET_DEFAULT_VALUE_OF_ALL_ENS_SUB_FEATURES

DESCRIPTION
  Function used to set the default values of all the ENS sub features
  (controlled by ens_sub_feature_status_list NVITEM), to the
  global ens_sub_features_status_list.

DEPENDENCIES
  None

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void mmgsdi_nv_set_default_value_of_all_ens_sub_features(
  void
)
{
  memset(&ens_sub_features_status_list, 0x00, sizeof(ens_sub_features_status_list));

  /* Update ens_sub_features_status_list with default values */
  ens_sub_features_status_list.scan_control_application_support = MMGSDI_FEATURE_ENABLED;
  ens_sub_features_status_list.nw_selection_menu_control_application_support = MMGSDI_FEATURE_ENABLED;
  ens_sub_features_status_list.acting_hplmn_rescan_feature_support = MMGSDI_FEATURE_ENABLED;
  ens_sub_features_status_list.rat_balancing_support = MMGSDI_FEATURE_ENABLED;
} /* mmgsdi_nv_set_default_value_of_all_ens_sub_features */


/*===========================================================================
FUNCTION MMGSDI_NV_INIT_ENS_SUB_FEATURES_STATUS_NV_LOOKUP

DESCRIPTION
  Function used to get the status of ens sub features stored in EFS based NV.
  If it fails to read, it sets the ens_sub_features_status_list members
  to a default value.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void mmgsdi_nv_init_ens_sub_features_status_nv_lookup(
  void
)
{
  /* Start off with the global struct initialised to 0 */
  memset(&ens_sub_features_status_list, 0x00, sizeof(ens_sub_features_status_list));

  UIM_MSG_HIGH_0("mmgsdi_nv_init_ens_sub_features_status_nv_lookup");

  if (efs_get(MMGSDI_ENS_SUB_FEATURES_STATUS_LIST_FILE, &ens_sub_features_status_list,
              sizeof(ens_sub_features_status_list))
              == sizeof(ens_sub_features_status_list))
  {
    /* EFS read is a success */
  }
  else
  {
    /* Updating global ens_sub_features_status_list with default values */
    mmgsdi_nv_set_default_value_of_all_ens_sub_features();
  }
} /* mmgsdi_nv_init_ens_sub_features_status_nv_lookup */


/*===========================================================================
FUNCTION MMGSDI_NV_GET_ENS_SUB_FEATURE_STATUS

DESCRIPTION
  Function used to get the status of ENS sub features

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_feature_status_enum

SIDE EFFECTS
  None
===========================================================================*/
mmgsdi_feature_status_enum_type mmgsdi_nv_get_ens_sub_feature_status(
  mmgsdi_cached_nv_item_enum_type ens_feature
)
{
  mmgsdi_feature_status_enum_type ret_value = MMGSDI_FEATURE_DISABLED;

  switch(ens_feature)
  {
    case MMGSDI_NV_SCAN_CONTROL_APPLICATION_SUPPORT_I:
      ret_value = ens_sub_features_status_list.scan_control_application_support;
      break;
    case MMGSDI_NV_NETWORK_SELECTION_MENU_CONTROL_APPLICATION_SUPPORT_I:
      ret_value = ens_sub_features_status_list.nw_selection_menu_control_application_support;
      break;
    case MMGSDI_NV_ACTING_HPLMN_RESCAN_FEATURE_SUPPORT_I:
      ret_value = ens_sub_features_status_list.acting_hplmn_rescan_feature_support;
      break;
    case MMGSDI_NV_RAT_BALANCING_SUPPORT_I:
      ret_value = ens_sub_features_status_list.rat_balancing_support;
      break;
    default:
      UIM_MSG_HIGH_1("Invalid Feature 0x%x", ens_feature);
      return ret_value;
  }
  UIM_MSG_HIGH_2("Status of ENS sub feature 0x%x is 0x%x", ens_feature, ret_value);
  return ret_value;
} /* mmgsdi_nv_get_ens_sub_feature_status */


#if defined FEATURE_MODEM_RCINIT && defined FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_MODEM_RCINIT && FEATURE_UIM_TEST_FRAMEWORK */

