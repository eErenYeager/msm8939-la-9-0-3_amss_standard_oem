#ifndef MMGSDI_SIMLOCK_H
#define MMGSDI_SIMLOCK_H
/*===========================================================================


                 M M G S D I   S I M L O C K  F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/25/14   tl      Secondary revisions
02/25/14   tl      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/

#include "comdef.h"
#include "mmgsdi.h"
#include "mmgsdilib_common.h"
#include "mmgsdi_evt.h"
#include "simlock_modem_p.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */
#endif /* MMGSDI_SIMLOCK_H */
