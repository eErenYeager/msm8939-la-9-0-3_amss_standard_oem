/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                 M M G S D I   U T I L I T Y   F U N C T I O N S


GENERAL DESCRIPTION

  This source file contains the ICC protocol processing support for MMGSDI
  before the command is being sent to UIM Module.

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS


                        COPYRIGHT INFORMATION

Copyright (c) 2004 - 2011, 2013 - 2014 QUALCOMM Technologies, Inc (QTI) and its
licensors. All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_convert.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   tl      Introduce new SIM Lock feature
01/11/14   df      Fix off-target compilation error
01/10/14   df      Use v2.0 diag macros
10/16/13   df      Removing legacy gsdi conversion functions
06/24/13   vdc     Added support for triple SIM
05/14/13   vdc     Memory optimizations for slot, app, pin, upin tables
04/25/13   yt      Fix compilation for non-CDMA builds
12/21/11   kk      Legacy GSDI removal updates
10/13/11   nb      Added handling of UIM_CARD_REMOVED_S
12/22/10   yt      Removed FEATURE_UIM_ZI_MEMORY_REDUCTION
11/10/10   yt      Memory reduction changes
10/06/10   yt      Setting correct response type for DCK retries request
09/13/10   yt      Sending correct status when unblock code is blocked
08/10/10   nmb     Meaningful error results for perso_cnf activate/block
07/14/10   js      Fixed compiler warnings
07/07/10   tkl     Fixed enum mapping between mmgsdi_perso_feature_enum_type
                   & gsdi_perso_enum_type
05/03/10   adp     Adding Perso related mapping from gsdi perso status to
                   mmgsdi perso status.
12/18/09   nb      Moved Dual Slot Changes
12/04/09   kp      Replacing mmgsdi_data_slot2 with mmgsdi_data_slot2_ptr
12/03/09   shr     Cache read process enhancement
10/28/09   nb      Release 7 Enhanced Network Selection
09/26/09   kp      ZI memory reduction changes
07/28/09   mib     Added mmgsdi_perso_lock_down_and_enable_limited_access
21/04/09   ssr     Updated generic callback for perso commands
11/04/08   nb      Klockwork Fixes
09/02/08   nb      Klockwork Fixes
12/11/07   js      Added conversion function to map gsdi and mmgsdi status
11/19/07   sun     Initial version

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "uim_variation.h"
#include "comdef.h"
#include "intconv.h"
#include "mmgsdi.h"
#include "mmgsdilib.h"
#include "uim_msg.h"
#include "mmgsdiutil.h"


/*===========================================================================

            DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/


/* ==========================================================================
   FUNCTION:      MMGSDI_UTIL_CONVERT_GSDI_STATUS_TO_MMGSDI

   DESCRIPTION:
     This function checks for valid range of the gsdi_returns_T,  and
     returns the corresponding mmgsdi_return_enum_type

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     None

   SIDE EFFECTS:
     None
==========================================================================*/
void mmgsdi_util_convert_gsdi_status_to_mmgsdi(
  gsdi_returns_T            gsdi_status,
  mmgsdi_return_enum_type  *mmgsdi_status_ptr)
{
  if (mmgsdi_status_ptr == NULL)
  {
    UIM_MSG_ERR_0("mmgsdi_status_ptr Null in mmgsdi_util_convert_gsdi_status_to_mmgsdi");
    return;
  }
  switch ( gsdi_status )
  {
    case GSDI_SUCCESS:
      *mmgsdi_status_ptr = MMGSDI_SUCCESS;
      break;

    case GSDI_ERROR:
      *mmgsdi_status_ptr = MMGSDI_ERROR;
      break;

    case GSDI_NOT_FOUND:
      *mmgsdi_status_ptr = MMGSDI_NOT_FOUND;
      break;

    case GSDI_INCORRECT_PARAMS:
      *mmgsdi_status_ptr = MMGSDI_INCORRECT_PARAMS;
      break;

    case GSDI_NOT_INIT:
      *mmgsdi_status_ptr = MMGSDI_NOT_INIT;
      break;

    case GSDI_NOT_SUPPORTED:
      *mmgsdi_status_ptr = MMGSDI_NOT_SUPPORTED;
      break;

    case GSDI_CMD_QUEUE_FULL:
     *mmgsdi_status_ptr = MMGSDI_CMD_QUEUE_FULL;
     break;

    case GSDI_INCORRECT_CODE:
      *mmgsdi_status_ptr = MMGSDI_INCORRECT_CODE;
      break;

    case GSDI_ACCESS_DENIED:
      *mmgsdi_status_ptr = MMGSDI_ACCESS_DENIED;
      break;

    case GSDI_INCOMPAT_PIN_STATUS:
      *mmgsdi_status_ptr = MMGSDI_INCOMPAT_PIN_STATUS;
      break;

    case GSDI_CODE_BLOCKED:
      *mmgsdi_status_ptr = MMGSDI_CODE_BLOCKED;
      break;

    case GSDI_SIM_TECHNICAL_PROBLEMS:
      *mmgsdi_status_ptr = MMGSDI_SIM_TECHNICAL_PROBLEMS;
      break;

    case GSDI_PIN_NOT_INITIALIZED:
      *mmgsdi_status_ptr = MMGSDI_PIN_NOT_INITIALIZED;
      break;

    case GSDI_UNKNOWN_INST_CLASS:
      *mmgsdi_status_ptr = MMGSDI_UNKNOWN_INST_CLASS;
      break;

    case GSDI_WARNING_NO_INFO_GIVEN:
      *mmgsdi_status_ptr = MMGSDI_WARNING_NO_INFO_GIVEN;
      break;

    case GSDI_UIM_CMD_TIMEOUT:
      *mmgsdi_status_ptr = MMGSDI_UIM_CMD_TIMEOUT;
      break;

    case GSDI_SIM_BUSY:
      *mmgsdi_status_ptr = MMGSDI_SIM_BUSY;
      break;

    case GSDI_INVALIDATION_CONTRADICTION_STATUS:
      *mmgsdi_status_ptr = MMGSDI_INVALIDATION_CONTRADICTION_STATUS;
      break;

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
    case GSDI_CODE_PERM_BLOCKED:
    case GSDI_PERSO_UNBLOCK_CK_BLOCKED:
      *mmgsdi_status_ptr = MMGSDI_CODE_PERM_BLOCKED;
      break;

    case GSDI_PERSO_CHECK_FAILED:
    case GSDI_PERSO_INCONSISTENT_W_IND:
      *mmgsdi_status_ptr = MMGSDI_PERSO_CHECK_FAILED;
      break;

    case GSDI_PERSO_INVALID_CK:
      *mmgsdi_status_ptr = MMGSDI_PERSO_INVALID_CK;
      break;

    case GSDI_PERSO_CK_BLOCKED:
      *mmgsdi_status_ptr = MMGSDI_PERSO_CK_BLOCKED;
      break;

    case GSDI_PERSO_INVALID_DATA:
      *mmgsdi_status_ptr = MMGSDI_PERSO_INVALID_DATA;
      break;

    case GSDI_PERSO_PERM_DISABLED:
      *mmgsdi_status_ptr = MMGSDI_PERSO_PERM_DISABLED;
      break;
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

    default:
      UIM_MSG_ERR_1("Unrecognized value in mmgsdi_util_convert_gsdi_status_to_mmgsdi: 0x%x",
                    gsdi_status);
      *mmgsdi_status_ptr = MMGSDI_ERROR;
      break;
  }
} /* mmgsdi_util_convert_gsdi_status_to_mmgsdi */

