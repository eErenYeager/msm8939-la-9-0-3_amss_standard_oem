
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


      G E N E R I C   P E R S O N A L I Z A T I O N   E N G I N E


GENERAL DESCRIPTION

  This source file contains the GSDI PERSONALIZATION ENGINE which adheres to
  the personalization interface to do the personalization procedures.

            COPYRIGHT INFORMATION

Copyright (c) 2001 - 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================
            EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_perso_engine_ruim.c#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
04/01/14   av      NVRUIM headers cleanup
03/18/14   tl      Introduce new SIM Lock feature
01/10/14   df      Use v2.0 diag macros
12/12/13   yt      Ensure correct buffer bounds checking
12/05/13   ar      Replaced all mmgsdi_malloc direct calls with macro
11/05/13   df      Fix LLVM warning in mmgsdi_perso_engine_ruim_read_imsi
10/16/13   df      Removing gsdi legacy conversion functions
10/04/13   bcho    F3 message reduction
08/30/13   yt      Add check for integer overflow
08/13/13   vv      Fix for a possible integer over flow
08/07/13   yt      Use correct buffer size while copying data from Perso file
06/18/13   av      Fallback to SFS if SFP not available
05/16/13   vdc     Replace memcpy with safer version memscpy
04/25/13   yt      Fix compilation for non-CDMA builds
04/16/13   yt      Avoid efs_sync during MMGSDI initialization
10/12/12   tl      Fix incorrect merge of perso RUIM
09/28/12   abg     Updated ERR to MSG_ERROR
06/22/12   tl      Create perso.txt and perso_ruim.txt for single mode devices
06/06/12   bcho    Hardcoded value of max key length replaced by MACRO
03/16/12   yt      Remove inclusion of tmc.h
01/13/12   nmb     Fixed compile error in case of FEATURE_MMGSDI_PERSO_SFS
12/23/11   shr     Removed featurization for ALWAYS ON features
12/21/11   shr     Legacy GSDI removal updates
12/21/11   shr     Send Card Error (due to Perso sanity error) only on the
                   slot correspnding to the sanity error, if its due to
                   a card specific failure
11/08/11   nmb     Corrected status initialization for encrypted file reads
08/23/11   nmb     Update internal IMSI representation to match GSDI_DIAG_ICD
08/04/11   shr     Halt Perso operations if mandatory card data missing or
                   invalid
06/17/11   nmb     Corrected perso file TLV interpretation
04/13/11   nmb     Removed dependency on hardware ESN
02/08/11   nmb     Removed featurization around efs_sync
01/20/11   nmb     Fixed Lint errors
10/27/10   ms      Changes for enablement of C+G and
                   Fix for failing test cases
10/11/10   nmb     Write perso data to SFS instead of EFS when SFS is enabled
08/30/10   shr     Perso clean up for Sessions
08/18/10   yt      Fixed Klocwork errors
08/18/10   mib     Fixed Lint warnings
06/24/10   nmb     SFS support while PERSO_SIM and PERSO_RUIM are enabled
06/14/10   nb      Fixed efs_sync compilation error on LA builds
04/26/10   nb      Forced sync after EFS write
03/19/10   mib     Fixed compilation errors
12/23/09   shr     Fixed Perso Lock Down handling
07/28/09   mib     Added mmgsdi_perso_lock_down_and_enable_limited_access
07/20/09   mib     Fixed F3 log with string
06/01/09   ssr     Merging of SIM and RUIM PERSO.
03/03/09   sun     Added Private header file
01/16/09   nb      Changed gsdi_fs_name_test() to gsdi_efs_name_test()
12/18/08   sun     Removed old perso
11/12/08   ssr     Removed compiler warnings
09/05/08   ssr     Adding mmgsdiutil.h for FEATURE_PERSO_RUIM
07/24/08   sp      Modified debug messages
05/07/07   sp      Replaced calls to mem_malloc/free with mmgsdi_malloc/free
01/15/07   nk      Updated EFS Path depending if apps mount feature defined
08/23/06   sun     Fixed Lint Errors
05/21/06   nk      Fixed Compilation Warning
05/19/06   wli     Fixed a lint error
05/16/06   tml     Fixed compilation warning
05/11/06   wli     Fixed formatting IMSI.
04/21/06   nk      Fixed warning, fixed
                   GSDI_PERSO_ENGINE_GET_RUIM_ENCRYPTED_DATA for loop
04/13/06   sun     Removed perm_feat_ind from the file
04/06/06   sun     Initial Release
===========================================================================*/


/* <EJECT> */
/*===========================================================================

           INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#include "customer.h"
#include "uim_msg.h"

#include "mmgsdi_card_init.h"

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
#include "mmgsdi_perso_engine.h"

#include "nvruim_p.h"

#include "mmgsdiutil.h"
#include "fs_public.h"
#ifdef FEATURE_MMGSDI_PERSO_SFS
#ifdef FEATURE_MMGSDI_PERSO_SFP_AVAILABLE
#include "sfs_private.h"
#endif /* FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#include "mmgsdi_sfs.h"
#else
#include "mmgsdi_efs.h"
#endif /* FEATURE_MMGSDI_PERSO_SFS */
#include "mmgsdi_perso_sec.h"
#include "uim.h"
#include "mmgsdi_perso_engine_ruim.h"
#include "intconv.h"

#define GSDI_PERSO_HW_ESN_LENGTH 4
#define GSDI_PERSO_ESN_LAST_BYTE_MASK 0xFF
#define GSDI_PERSO_ESN_LAST_BYTE_SHIFT 8

#ifndef FEATURE_MMGSDI_PERSO_SFS
static char *mmgsdi_efs_ruim_perso_file  = "mmgsdi/perso/ruim_perso.txt";
#endif /* FEATURE_MMGSDI_PERSO_SFS */

#ifdef FEATURE_PERSO_RUIM
// this should come from txt file.
static  boolean gsdi_perso_ruim_use_imsi_m = TRUE;
#endif /* FEATURE_PERSO_RUIM */

#ifdef FEATURE_MMGSDI_PERSO_SFS
char *mmgsdi_ruim_perso_sfs_file   = "/mmgsdi/perso_sfs/ruim_perso.txt";
#endif /* FEATURE_MMGSDI_PERSO_SFS */

#ifdef FEATURE_PERSO_RUIM

/*===========================================================================
FUNCTION MMGSDI_PERSO_ENGINE_RUIM_READ_IMSI

DESCRIPTION
  This function will be called to read the IMSI and convert it to a usable
  format.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type mmgsdi_perso_engine_ruim_read_imsi(
  mmgsdi_session_id_type               session_id,
  mmgsdi_int_app_info_type            *app_info_ptr,
  mmgsdi_slot_id_enum_type             slot,
  mmgsdi_data_type                    *imsi_ptr
)
{
  mmgsdi_return_enum_type mmgsdi_status     = MMGSDI_SUCCESS;
  mmgsdi_data_type        imsi_buf          = {0};
  mmgsdi_file_enum_type   file_name;
#ifndef FEATURE_UIM_TEST_FRAMEWORK
  uint8                    imsi_digits[NVRUIM_NUM_MIN_DIGITS];  /* converted digits */
  word                    imsi_mcc;
  byte                    imsi_mnc;
  dword                   imsi_min1;
  word                    imsi_min2;
  byte                    index;
#endif /* FEATURE_UIM_TEST_FRAMEWORK */
  /* --------------------------------------------------------------------------
     Perform Validation Checks
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(app_info_ptr, imsi_ptr);

  /* --------------------------------------------------------------------------
     Read IMSI from the card
  ------------------------------------------------------------------------*/
  if(app_info_ptr->app_data.app_type == MMGSDI_APP_CSIM)
  {
    file_name = MMGSDI_CSIM_IMSI_M;
  }
  else
  {
    file_name = MMGSDI_CDMA_IMSI_M;
  }

  mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                app_info_ptr->app_data.app_type,
                                                slot,
                                                file_name,
                                                &imsi_buf);

  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    UIM_MSG_ERR_2("PERSO: mmgsdi_card_init_cache_binary failed 0x%x for file 0x%x",
                  mmgsdi_status, file_name);
  }

  if(mmgsdi_status == MMGSDI_SUCCESS)
  {
#ifndef FEATURE_UIM_TEST_FRAMEWORK
    /* Format the coded IMSI to BCD */
    /* Contry code */
    imsi_mcc = (word) (((imsi_buf.data_ptr[9] & 0x03) << 8)
                      + imsi_buf.data_ptr[8]);
    /* S_11_12 */
    imsi_mnc = imsi_buf.data_ptr[6] & 0x7F;
    /* S1 */
    imsi_min1 = (dword) ((imsi_buf.data_ptr[5] <<16)
                         +(imsi_buf.data_ptr[4] <<8)
                         +imsi_buf.data_ptr[3]);
    /* S2 */
    imsi_min2 = (word) ((imsi_buf.data_ptr[2] <<8)
                         +imsi_buf.data_ptr[1]);


    /* Convert to decimal digits */
    if(nvruim_decode_min((byte *)&imsi_digits[0], &imsi_mcc,
                         &imsi_mnc, &imsi_min1, &imsi_min2))
    {

      /* Formatted IMSI is BCD coded:
       *  MCC   MNC   NW2   Last6digits
       *  ----   ---  ----   -----------
       *  B1B2    B3  B4B5    B6B7B8
       */
      /* Convert from ascii to BCD */
      MMGSDIUTIL_TMC_MEM_MALLOC_AND_VALIDATE(imsi_ptr->data_ptr,
                                             imsi_buf.data_len,
                                             mmgsdi_status);
      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        UIM_MSG_ERR_0("NULL data");
        MMGSDIUTIL_TMC_MEM_FREE(imsi_buf.data_ptr);
        return MMGSDI_PERSO_INVALID_DATA;
      }

      /* Encode according to 3gpp IMSI as specified in the GSDI_DIAG_ICD(80-V7032-1):
         B1     B2     B3     B4     B5     B6       B7       B8
         D2 D1  xF D3  D5 D4  D7 D6  D9 D8  D11 D10  D13 D12  D15 D14 */
      imsi_ptr->data_ptr[0] = (imsi_digits[0] - '0') | ((imsi_digits[1] - '0') << 4);
      imsi_ptr->data_ptr[1] = (imsi_digits[2] - '0') | 0xF0;

      for (index=2; index < 8; index++)
      {
        imsi_ptr->data_ptr[index] = (imsi_digits[2*index-1] - '0')
                                        + ((imsi_digits[2*index] - '0') << 4);
      }
      imsi_ptr->data_len = 8;
    }
    else
    {
      mmgsdi_status = MMGSDI_PERSO_INVALID_DATA;
    }
#endif /*FEATURE_UIM_TEST_FRAMEWORK*/
  }
  else
  {
    UIM_MSG_ERR_1("PERSO: IMSI read from card failed 0x%x", mmgsdi_status);
  }

  MMGSDIUTIL_TMC_MEM_FREE(imsi_buf.data_ptr);
  return mmgsdi_status;
} /* mmgsdi_perso_engine_ruim_read_imsi */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_RUIM_GET_RUIM_DATA

DESCRIPTION:
  Will gather all of the information from the SIM/USIM/RUIM inserted.
  It will retrieve:
  EF-IMSI
  EF-CNL
  EF-GID1
  EF-GID2

DEPENDENCIES:
  Card must be inserted before being called and the PIN Must be verified.

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_ruim_data(
  mmgsdi_session_id_type          session_id,
  mmgsdi_int_app_info_type       *app_info_ptr,
  mmgsdi_slot_id_enum_type        slot,
  gsdi_perso_sec_sim_data_type   *ruim_data_ptr
)
{
  mmgsdi_return_enum_type    mmgsdi_status  = MMGSDI_SUCCESS;
  mmgsdi_file_enum_type      file_name      = MMGSDI_MAX_FILE_ENUM;

  /* --------------------------------------------------------------------------
     Perform Validation Checks
     ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(ruim_data_ptr, app_info_ptr);

  /* --------------------------------------------------------------------------
     Get the IMSI information from the RUIM
     If retrieval of the IMSI information fails,
     the sec_data_ptr->buffer_ptr memory allocated for
     this request will already be freed.
     ------------------------------------------------------------------------*/
  if(gsdi_perso_ruim_use_imsi_m)
  {
    /* Read IMSI_M */
    mmgsdi_status = mmgsdi_perso_engine_ruim_read_imsi(session_id,
                                                       app_info_ptr,
                                                       slot,
                                                       (void*)&ruim_data_ptr->imsi);
  }
  else
  {
    if(app_info_ptr->app_data.app_type == MMGSDI_APP_CSIM)
    {
      file_name = MMGSDI_CSIM_IMSI_T;
    }
    else
    {
      file_name = MMGSDI_CDMA_IMSI_T;
    }

    mmgsdi_status = mmgsdi_card_init_cache_binary(session_id,
                                                  app_info_ptr->app_data.app_type,
                                                  slot,
                                                  file_name,
                                                  &ruim_data_ptr->imsi);

    if ( mmgsdi_status != MMGSDI_SUCCESS &&
         mmgsdi_status != MMGSDI_NOT_SUPPORTED )
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(ruim_data_ptr->imsi.data_ptr);
      MMGSDI_DEBUG_MSG_ERROR_1("PERSO: IMSI read failed 0x%x", mmgsdi_status);
    }
  }

  if( mmgsdi_status != MMGSDI_SUCCESS )
  {
    return MMGSDI_ERROR;
  }

  ruim_data_ptr->cnl.data_len = 0;
  ruim_data_ptr->gid1.data_len = 0;
  ruim_data_ptr->gid2.data_len = 0;
  ruim_data_ptr->dck.data_len = 0;

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_get_ruim_data */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CALC_RUIM_DATA_LEN

DESCRIPTION
  This function is called to calcuate the total memory required for the
  SIM Items to be retrieved.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_ruim_data_len(
  int32 * len_ptr
)
{
  /* ------------------------------------------------------------------------
   Validate all parameters
   ----------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);
  /* ------------------------------------------------------------------------
   Calculate the total memory required.
   ----------------------------------------------------------------------*/
  *len_ptr = GSDI_PERSO_ENGINE_IMSI_LEN;

  return MMGSDI_SUCCESS;

} /* gsdi_perso_engine_calc_ruim_data_len */
#endif /* FEATURE_PERSO_RUIM */


/*===========================================================================
FUNCTION gsdi_perso_engine_set_ruim_mem_data

DESCRIPTION
  This function is used to create all of the relevant Personalization Files
  used for storing information in EFS.

  This procedure is ran every time the mobile is powered up, but will not
  recreate any files already created.

  If all personalization files are not created properly or upon entering
  this function again, only some of the files exist...then an Error Fatal
  will occur.  This could be evidence of tampering.

  It will be acceptable for the file to be empty.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_set_ruim_mem_data(
  gsdi_perso_sec_me_data_type *enc_data,
  uint8                       **memory_ptr
)
{
  gsdi_perso_engine_mem_pointer_type mem_pointer;
  uint32 total_len;

  MMGSDIUTIL_RETURN_IF_NULL_2(enc_data, memory_ptr);

  memset(enc_data, 0x00, sizeof(gsdi_perso_sec_me_data_type));

  /*------------------------------------------------------------------------
  Feature Inds
  --------------------------------------------------------------------------*/
  enc_data->feat_inds.len = GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS * 2;
  total_len = enc_data->feat_inds.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
  SIM Lock Codes
  --------------------------------------------------------------------------*/
  enc_data->sim_lock_codes.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE)
                                     * GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS;
  total_len += enc_data->sim_lock_codes.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
  DCK
  --------------------------------------------------------------------------*/
  enc_data->dcks.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE) *
                                 GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS;
  enc_data->dcks.len += GSDI_PERSO_MAX_CK * 2 *
                                 GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS;
  total_len +=  enc_data->dcks.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
  DCK Counter
  --------------------------------------------------------------------------*/
  enc_data->dck_counts.len = GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS * 2;
  total_len += enc_data->dck_counts.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
  UNBLOCK CK
  --------------------------------------------------------------------------*/
  enc_data->unblock_cks.len = (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE) *
                                 GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS;
  enc_data->unblock_cks.len += GSDI_PERSO_MAX_CK * 2 *
                                 GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS;
  total_len +=  enc_data->unblock_cks.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
  UNBLOCK CK Counter
  --------------------------------------------------------------------------*/
  enc_data->unblock_ck_counts.len = GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS * 2;
  total_len += enc_data->unblock_ck_counts.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /*------------------------------------------------------------------------
    Lock down
  --------------------------------------------------------------------------*/
  enc_data->lock_down.len = sizeof(uint8) * 2;
  total_len += enc_data->lock_down.len;
  total_len += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);

  /* ---------------------------------------------------------------------------
    Allocate the entire buffer required for the encrypted data structure
    Memset it to 0x00 when successful
    Set memory_pointer.memory_curr = memory_pointer.memory_start
    Set memory_pointer.memory_end  = memory_pointer.memory_start + total_mem_req
   -------------------------------------------------------------------------*/
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(mem_pointer.buffer_start, total_len)
  MMGSDIUTIL_RETURN_IF_NULL(mem_pointer.buffer_start);
  mem_pointer.curr_offset  = 0;
  mem_pointer.buffer_size  = total_len;


  /* free this memory at the calling end */
  *memory_ptr = mem_pointer.buffer_start;

  /*-------------------------------------------------------------------------
  Set the Feat Ind Buffer Pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->feat_inds.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->feat_inds.len;

  /*-------------------------------------------------------------------------
  Set the SIM Lock Code Buffer Pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->sim_lock_codes.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->sim_lock_codes.len;

  /*-------------------------------------------------------------------------
  Set the DCK Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->dcks.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->dcks.len;

  /*-------------------------------------------------------------------------
  Set the DCK Counter Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->dck_counts.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->dck_counts.len;

  /*-------------------------------------------------------------------------
  Set the UNblock CK Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->unblock_cks.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->unblock_cks.len;

  /*-------------------------------------------------------------------------
  Set the Unblock CK Counter Memory Buffer pointer
  -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->unblock_ck_counts.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->unblock_ck_counts.len;

  /*-------------------------------------------------------------------------
    Set the Lock down Buffer Pointer
    -------------------------------------------------------------------------*/
  mem_pointer.curr_offset += (GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE);
  enc_data->lock_down.buffer_ptr = mem_pointer.buffer_start + mem_pointer.curr_offset;
  mem_pointer.curr_offset += enc_data->lock_down.len;

  /*-------------------------------------------------------------------------
    Check memory usage
    -------------------------------------------------------------------------*/
  if(mem_pointer.curr_offset != mem_pointer.buffer_size)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Memory Corruption!");
    MMGSDIUTIL_TMC_MEM_FREE(*memory_ptr);
    return MMGSDI_ERROR;
  }

  enc_data->data_mod.update_fs = TRUE;
  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_set_ruim_mem_data */


#ifdef FEATURE_PERSO_RUIM
#if !defined(FEATURE_MMGSDI_PERSO_SFS)
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_RUIM_FILE_TEST

DESCRIPTION
  This function is used to create the directories and files.  This function
  should only be called once during the personalization phase.

DEPENDENCIES
  None

RETURN VALUE
  boolean (true if successful)
===========================================================================*/
boolean gsdi_perso_engine_ruim_fs_file_test(void)
{
  gsdi_returns_T gsdi_status;
  gsdi_status = gsdi_efs_name_test(mmgsdi_efs_ruim_perso_file);
  if(GSDI_SUCCESS != gsdi_status)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: EFS PERSO_FILE MISSING");
    return FALSE;
  }
  return TRUE;
} /* gsdi_perso_engine_ruim_fs_file_test */
#endif /* !FEATURE_MMGSDI_PERSO_SFS */
#endif /* FEATURE_PERSO_RUIM */


/*===========================================================================
FUNCTION GSDI_PERSO_CREATE_RUIM_FILE

DESCRIPTION
  This function is used to create the directories and files on EFS. This
  function does not create files on SFS. This function should only be
  called once during the personalization phase.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type:
                 MMGSDI_SUCCESS:    RUIM file created successfully
                 MMGSDI_ERROR:      Problem creating RUIM file
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_create_ruim_file(void)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;
  /* --------------------------------------------------------------------------
   Create the Perso File
   ------------------------------------------------------------------------*/
#ifdef FEATURE_MMGSDI_PERSO_SFS
/* If SFP is disabled, create RUIM perso file in SFS without worrying about
   SIM perso file.
   If SFP is enabled and FEATURE_PERSO_SIM is defined, it means SIM perso file
   is created in SFP and we should go ahead and create RUIM perso file in SFS. */
#if !defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) || (defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM))
  /* SIM perso file is created on SFP. Create RUIM perso file on SFS */
  (void) mmgsdi_sfs_rmfile(mmgsdi_ruim_perso_sfs_file);
#endif /* (!FEATURE_MMGSDI_PERSO_SFP_AVAILABLE || (FEATURE_MMGSDI_PERSO_SFP_AVAILABLE && FEATURE_PERSO_SIM))*/
#else
  /* Create the RUIM perso file on EFS */
  mmgsdi_status = gsdi_efs_create_file(mmgsdi_efs_ruim_perso_file);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not create Perso File");
    return mmgsdi_status;
  }
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  return mmgsdi_status;
} /* gsdi_perso_engine_create_ruim_file */


/* ============================================================================
FUNCTION: GSDI_PERSO_ENGINE_WRITE_BACK_RUIM_FS

DESCRIPTION:
  Function will write back to FS any data indicated to be modified.  The
  indication is set by the Security Library.

DEPENDENCIES:
  None

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

============================================================================ */
static mmgsdi_return_enum_type gsdi_perso_engine_write_back_ruim_fs
(
  gsdi_perso_sec_me_data_type * encrypted_me_data_p,
  boolean                       is_sync_required
)
{
  mmgsdi_return_enum_type   mmgsdi_status = MMGSDI_SUCCESS;
  uint32                    len           = 0;
  uint32                    buffer_len    = 0;
  uint8 *                   buffer_ptr    = NULL;
  gsdi_perso_sec_data_type *sec_data_ptr  = NULL;
  int32                     i             = 0;
#if defined(FEATURE_MMGSDI_PERSO_SFS)
  int                       total         = 0;
#endif /* FEATURE_MMGSDI_PERSO_SFS */

  /* --------------------------------------------------------------------------
   Validate the Paramaters provided
   ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(encrypted_me_data_p);

  /* If update fs is not required, exist out of the function */
  if(!encrypted_me_data_p->data_mod.update_fs)
  {
    return MMGSDI_SUCCESS;
  }
  /* Update to FS is required, continue with the process */
  buffer_len = encrypted_me_data_p->feat_inds.len +
        encrypted_me_data_p->sim_lock_codes.len +
        encrypted_me_data_p->dcks.len +
        encrypted_me_data_p->dck_counts.len +
        encrypted_me_data_p->unblock_cks.len +
        encrypted_me_data_p->unblock_ck_counts.len +
        encrypted_me_data_p->lock_down.len +
        (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE) * GSDI_PERSO_ENGINE_NUMBER_OF_TAGS;

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(buffer_ptr, buffer_len)
  /*Write the Feature Inds*/

  MMGSDIUTIL_RETURN_IF_NULL(buffer_ptr);

  for(i =1;i<=GSDI_PERSO_ENGINE_NUMBER_OF_TAGS;i++)
  {
    switch(i)
    {
      case GSDI_PERSO_ENGINE_TAG_FEATURE_INDS:
        sec_data_ptr = &encrypted_me_data_p->feat_inds;
        break;
      case GSDI_PERSO_ENGINE_TAG_PERM_FEATURE_INDS:
        continue;
      case GSDI_PERSO_ENGINE_TAG_SIM_LOCK_CODE:
        sec_data_ptr = &encrypted_me_data_p->sim_lock_codes;
        break;
      case GSDI_PERSO_ENGINE_TAG_DEPERSO_CK:
        sec_data_ptr = &encrypted_me_data_p->dcks;
        break;
      case GSDI_PERSO_ENGINE_TAG_DCK_COUNTER:
        sec_data_ptr = &encrypted_me_data_p->dck_counts;
        break;
      case GSDI_PERSO_ENGINE_TAG_UNBLOCK:
        sec_data_ptr = &encrypted_me_data_p->unblock_cks;
        break;
      case GSDI_PERSO_ENGINE_TAG_UNBLOCK_COUNTER:
        sec_data_ptr = &encrypted_me_data_p->unblock_ck_counts;
        break;
      case GSDI_PERSO_ENGINE_TAG_LOCK_DOWN:
        sec_data_ptr = &encrypted_me_data_p->lock_down;
        break;
      default:
        UIM_MSG_ERR_0("PERSO: Incorrect tag");
        MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(buffer_ptr);
        return MMGSDI_ERROR;
    }

    buffer_ptr[len]   = (uint8)i;
    len            += GSDI_PERSO_ENGINE_TAG_SIZE;
    buffer_ptr[len]   = (uint8)(sec_data_ptr->len/GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT);
    buffer_ptr[len+1] = (uint8)(sec_data_ptr->len%GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT);
    len            += GSDI_PERSO_ENGINE_LEN_SIZE;

    mmgsdi_memscpy(buffer_ptr + len, buffer_len - len, sec_data_ptr->buffer_ptr, sec_data_ptr->len);
    len += sec_data_ptr->len;

  }
#if defined(FEATURE_MMGSDI_PERSO_SFS)
#if (defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && !defined(FEATURE_PERSO_SIM))
  /* If SFP is enabled and FEATURE_PERSO_SIM is not enabled, write RUIM
     perso data to SFP */
  total = sfs_priv_simloc_write(buffer_ptr, len);
#else
#if ((defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM)) || !defined (FEATURE_MMGSDI_PERSO_SFP_AVAILABLE))
  /* If SFP is enabled and FEATURE_PERSO_SIM is also enabled, write RUIM
     perso data to SFS.
     If SFP is disabled, do not bother checking FEATURE_SIM and write RUIM
     perso data to SFS */
  {
    mmgsdi_status = mmgsdi_sfs_write(buffer_ptr, &len, mmgsdi_ruim_perso_sfs_file);
    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
      UIM_MSG_HIGH_1("Wrote 0x%x bytes to sfs file", len);
      total = uint32toint32(len);
    }
  }
#endif
#endif /* !FEATURE_PERSO_SIM && FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
  if(total != len)
#else
  mmgsdi_status = gsdi_efs_write_file(mmgsdi_efs_ruim_perso_file,
                                      (int32)len,
                                      buffer_ptr);

  if(mmgsdi_status != MMGSDI_SUCCESS)
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failure to write ruim perso file");
  }
  else
  {
    /* Data written successfully to RAM.
     * Force it to be transferred from memory to eMMC card only if
     * is_sync_required flag is TRUE
     */
    if(is_sync_required)
    {
      if(efs_sync("/") != 0)
      {
        UIM_MSG_ERR_1("efs_sync failed 0x%x", efs_errno);
        mmgsdi_status = MMGSDI_ERROR;
      }
    }
  }

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(buffer_ptr);
  return mmgsdi_status;
} /* gsdi_perso_engine_write_back_ruim_fs */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CREATE_RUIM_DATA

DESCRIPTION
  This function is called to calcuate the total memory required for the
  SIM Items to be retrieved.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_create_ruim_data(
  void
)
{
  gsdi_perso_sec_me_data_type  encrypted_data;
  mmgsdi_return_enum_type      mmgsdi_status = MMGSDI_SUCCESS;
  uint8                       *memory_ptr    = NULL;

  UIM_MSG_HIGH_0("PERSO: gsdi_perso_engine_create_ruim_data");

  /* --------------------------------------------------------------------------
    The Perso Engine Has an Interface to allow the Personalization
    Engine to determine how much memory needs to be allocated.
  ------------------------------------------------------------------------*/
  mmgsdi_status = gsdi_perso_engine_set_ruim_mem_data(&encrypted_data,&memory_ptr);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get total memory required");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return MMGSDI_ERROR;
  }

  mmgsdi_status = gsdi_perso_ruim_security_create_data(&encrypted_data);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not encrypt data");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = gsdi_perso_engine_create_ruim_file();
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not create dirs or files");
    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
    return mmgsdi_status;
  }

  mmgsdi_status = gsdi_perso_engine_write_back_ruim_fs(&encrypted_data, FALSE);
  if(mmgsdi_status != MMGSDI_SUCCESS)
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not write back FS");
  }
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(memory_ptr);
  return MMGSDI_SUCCESS;
}/* gsdi_perso_engine_create_ruim_data */


#ifdef FEATURE_PERSO_RUIM
/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_CALC_RUIM_ENC_DATA_LEN

DESCRIPTION
  This function is called to calcuate the total memory required for a
  particular item.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_calc_ruim_enc_data_len(
  gsdi_perso_engine_proc_enum_type      feature_ind,
  const void                           *req_ptr,
  int32                                *len_ptr
)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;
  int32                   len           = 0;

#if defined(FEATURE_MMGSDI_PERSO_SFS)
#if !defined(FEATURE_PERSO_SIM)
  uint8         data_buf[GSDI_PERSO_ENGINE_MAX_FILE_SIZE];
#endif /* !FEATURE_PERSO_SIM */
#if ((defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM)) || !defined (FEATURE_MMGSDI_PERSO_SFP_AVAILABLE))
  uint32          sfs_size    = 0;
#endif
#endif /* FEATURE_MMGSDI_PERSO_SFS */

  /* -----------------------------------------------------------------------
   Do the parameter checking
  ---------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL(len_ptr);

  /* -----------------------------------------------------------------------
   Retrieve the File Size from FS
   ---------------------------------------------------------------------*/
#if defined(FEATURE_MMGSDI_PERSO_SFS)
#if (defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && !defined(FEATURE_PERSO_SIM))
  /* If SFP is enabled and FEATURE_PERSO_SIM is not enabled, read RUIM
     perso data from SFP */
   len = sfs_priv_simloc_read(&data_buf, GSDI_PERSO_ENGINE_MAX_FILE_SIZE);
#else
#if ((defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM)) || !defined (FEATURE_MMGSDI_PERSO_SFP_AVAILABLE))
  /* If SFP is enabled and FEATURE_PERSO_SIM is also enabled, write RUIM
     perso data to SFS.
     If SFP is disabled, do not bother checking FEATURE_SIM and write RUIM
     perso data to SFS */
  {
    mmgsdi_status = mmgsdi_sfs_get_size(&sfs_size, mmgsdi_ruim_perso_sfs_file);
    len = uint32toint32(sfs_size);
    UIM_MSG_HIGH_1("SFS file is 0x%x bytes", len);
  }
#endif
#endif /* !FEATURE_PERSO_SIM && FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#else
  mmgsdi_status = gsdi_fs_get_file_size(mmgsdi_efs_ruim_perso_file, &len);
#endif /* FEATURE_MMGSDI_PERSO_SFS */
  if(mmgsdi_status != MMGSDI_SUCCESS ||
     len == 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: FILE Status: 0x%x File Size: 0x%x",
                             mmgsdi_status, len);
    return MMGSDI_ERROR;
  }

  *len_ptr = len;

  /*Also cater for the new data that may be added*/
  if(feature_ind == GSDI_PERSO_ENGINE_SET_DATA_FEATURE)
  {
    MMGSDIUTIL_RETURN_IF_NULL(req_ptr);
    /* Check for integer overflow */
    if(((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes > MMGSDI_INT32_MAX / 2 ||
       *len_ptr > MMGSDI_INT32_MAX - ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2)
    {
      return MMGSDI_ERROR;
    }
    *len_ptr += ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2;
  }
  /* Check to see if new SIM Lock Codes have to be generated */
  else if(feature_ind == GSDI_PERSO_ENGINE_AUTO_LOCK)
  {
    *len_ptr += (int32)(GSDI_PERSO_BUFFER_LEN -
                      ((int32)GSDI_PERSO_ENGINE_TAG_SIZE * GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS));
  }

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_calc_ruim_enc_data_len */


/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_RUIM_ENCRYPTED_DATA

DESCRIPTION:
  Will gather all of the information required to activate the feature
  indication provided.

  Information gather is as follows:

  1)  Secured / Encrypted Data - Personalization Keys
  2)  Secured / Encrypted Data - Feature Indicators
  3)  Secured / Encrypted Data - Depersonalization Key Status
  4)  Get the IMEI Value
  5)  Get the Mobile Serial Number

DEPENDENCIES:
  Based on the Personalization State and Self Personalization State.
  gsdi_perso_self_perso_state_enum_type
  gsdi_perso_state_enum_type

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
static mmgsdi_return_enum_type gsdi_perso_engine_get_ruim_encrypted_data(
  gsdi_perso_sec_me_data_type        * sec_data_ptr,
  gsdi_perso_engine_mem_pointer_type * mem_pointer_ptr,
  gsdi_perso_engine_proc_enum_type     feature_type,
  const void                         * req_ptr
)
{
  mmgsdi_return_enum_type  mmgsdi_status        = MMGSDI_ERROR;
  int32                    file_size            = 0;
  uint8                   *file_buffer_ptr      = NULL;
  uint8                   *orig_file_buffer_ptr = NULL;
  int                      i                    = 0;

  /* --------------------------------------------------------------------------
   Perform Paramter checking
   ------------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(sec_data_ptr, mem_pointer_ptr);

  memset(&sec_data_ptr->feat_inds,      0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->sim_lock_codes ,0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->dcks,           0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->dck_counts,     0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->unblock_cks,    0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->unblock_ck_counts, 0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->lock_down,      0x00 , sizeof(gsdi_perso_sec_data_type));
  memset(&sec_data_ptr->me_imei,        0x00 , sizeof(gsdi_perso_sec_data_type));

  /* --------------------------------------------------------------------------
   Using the Secure FS Interface, get the Perso Data.
   ------------------------------------------------------------------------*/
#if defined(FEATURE_MMGSDI_PERSO_SFS)
#if (defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && !defined(FEATURE_PERSO_SIM))
  /* If SFP is enabled and FEATURE_PERSO_SIM is not enabled, read RUIM
     perso data from SFP */
  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
    file_buffer_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE)
  MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);
  file_size = sfs_priv_simloc_read((uint8*)file_buffer_ptr, GSDI_PERSO_ENGINE_MAX_FILE_SIZE);
#else
#if ((defined(FEATURE_MMGSDI_PERSO_SFP_AVAILABLE) && defined(FEATURE_PERSO_SIM)) || !defined (FEATURE_MMGSDI_PERSO_SFP_AVAILABLE))
  /* If SFP is enabled and FEATURE_PERSO_SIM is also enabled, write RUIM
     perso data to SFS.
     If SFP is disabled, do not bother checking FEATURE_SIM and write RUIM
     perso data to SFS */
  {
    uint32                  sfs_size      = 0;

    mmgsdi_status = mmgsdi_sfs_get_size(&sfs_size, mmgsdi_ruim_perso_sfs_file);
    file_size = uint32toint32(sfs_size);
    if(mmgsdi_status == MMGSDI_SUCCESS)
    {
      MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(file_buffer_ptr, file_size)
      MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);

      mmgsdi_status = mmgsdi_sfs_read(file_buffer_ptr,
                                      file_size,
                                      mmgsdi_ruim_perso_sfs_file);
    }
  }
#endif
#endif /* !FEATURE_PERSO_SIM && FEATURE_MMGSDI_PERSO_SFP_AVAILABLE */
#else
  mmgsdi_status = gsdi_fs_get_file_size(mmgsdi_efs_ruim_perso_file,(int32*)&file_size);

  if(mmgsdi_status != MMGSDI_SUCCESS ||
     file_size == 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: FILE Status: 0x%x File Size: 0x%x",
                             mmgsdi_status, file_size);
    return MMGSDI_ERROR;
  }

  MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(file_buffer_ptr, file_size)
  MMGSDIUTIL_RETURN_IF_NULL(file_buffer_ptr);

  mmgsdi_status =  gsdi_efs_read_file(mmgsdi_efs_ruim_perso_file,
                                      (int32*)&file_size,
                                      file_buffer_ptr);

#endif /* FEATURE_MMGSDI_PERSO_SFS */
  if(mmgsdi_status != MMGSDI_SUCCESS ||
     file_size   == 0)
  {
    MMGSDI_DEBUG_MSG_ERROR_2("PERSO: FS Status: 0x%x Read Size 0x%x",
                             mmgsdi_status, file_size);

    MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(file_buffer_ptr);
    return mmgsdi_status;
  }

  orig_file_buffer_ptr = file_buffer_ptr;
  for(i=0;i<GSDI_PERSO_ENGINE_NUMBER_OF_TAGS-1;i++)
  {
    uint8  tag         = 0;
    uint16 len         = 0;
    uint16 buffer_size = 0;

    /* Check if there is space for an additional perso tag */
    if (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE >
        file_size - (file_buffer_ptr - orig_file_buffer_ptr))
    {
      UIM_MSG_ERR_1("PERSO: Invalid Perso file (0x%x)", i);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Extract tag and length */
    tag = *file_buffer_ptr;
    len = (*(file_buffer_ptr + 1) * GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT) + *(file_buffer_ptr + 2);

    /* Check if there is space for an additional perso tag */
    if (GSDI_PERSO_ENGINE_TAG_SIZE + GSDI_PERSO_ENGINE_LEN_SIZE + len >
        file_size - (file_buffer_ptr - orig_file_buffer_ptr))
    {
      UIM_MSG_ERR_1("PERSO: Invalid Perso file (0x%x)", i);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Check for integer overflow in buffer size calculation */
    if (mem_pointer_ptr->buffer_size - mem_pointer_ptr->curr_offset <
        GSDI_PERSO_ENGINE_LEN_SIZE + GSDI_PERSO_ENGINE_TAG_SIZE)
    {
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }

    /* Update size of destination buffer for copying perso data */
    buffer_size = mem_pointer_ptr->buffer_size -
                    (mem_pointer_ptr->curr_offset + GSDI_PERSO_ENGINE_LEN_SIZE +
                     GSDI_PERSO_ENGINE_TAG_SIZE);

    /*Switch on the Tags*/
    switch(tag)
    {
      case GSDI_PERSO_ENGINE_TAG_FEATURE_INDS:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->feat_inds.buffer_ptr = mem_pointer_ptr->buffer_start +
                                             mem_pointer_ptr->curr_offset +
                                             GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->feat_inds.len        = len;

        mmgsdi_memscpy(sec_data_ptr->feat_inds.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->feat_inds.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->feat_inds.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->feat_inds.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_SIM_LOCK_CODE:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->sim_lock_codes.buffer_ptr = mem_pointer_ptr->buffer_start +
                                                  mem_pointer_ptr->curr_offset +
                                                  GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->sim_lock_codes.len        = len;

        mmgsdi_memscpy(sec_data_ptr->sim_lock_codes.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->sim_lock_codes.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->sim_lock_codes.len;

        /* Check to see if new SIM Lock Codes have to be generated*/
        if(feature_type == GSDI_PERSO_ENGINE_AUTO_LOCK)
        {
          mem_pointer_ptr->curr_offset += GSDI_PERSO_BUFFER_LEN -
                                        sec_data_ptr->sim_lock_codes.len;
          /* The sec_adta_p->dck is set to memory_curr within
            gsdi_perso_engine_get_enc_dcks */
        }
        else if(feature_type == GSDI_PERSO_ENGINE_SET_DATA_FEATURE)
        {
          if(req_ptr == NULL)
          {
            MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
            return MMGSDI_INCORRECT_PARAMS;
          }
          /* Check for integer overflow */
          if(((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes > MMGSDI_INT32_MAX / 2 ||
             ((uint32)mem_pointer_ptr->curr_offset) > MMGSDI_UINT32_MAX - ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2)
          {
            MMGSDIUTIL_TMC_MEM_FREE(orig_file_buffer_ptr);
            return MMGSDI_INCORRECT_PARAMS;
          }
          mem_pointer_ptr->curr_offset += ((gsdi_perso_set_feature_data_req_T *)req_ptr)->num_bytes *2;
        }
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->sim_lock_codes.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_DEPERSO_CK:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->dcks.buffer_ptr = mem_pointer_ptr->buffer_start +
                                        mem_pointer_ptr->curr_offset +
                                        GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->dcks.len        = len;

        mmgsdi_memscpy(sec_data_ptr->dcks.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->dcks.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->dcks.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->dcks.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_DCK_COUNTER:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->dck_counts.buffer_ptr = mem_pointer_ptr->buffer_start +
                                              mem_pointer_ptr->curr_offset +
                                              GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->dck_counts.len        = len;

        mmgsdi_memscpy(sec_data_ptr->dck_counts.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->dck_counts.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->dck_counts.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->dck_counts.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_UNBLOCK:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->unblock_cks.buffer_ptr = mem_pointer_ptr->buffer_start +
                                               mem_pointer_ptr->curr_offset +
                                               GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->unblock_cks.len        = len;

        mmgsdi_memscpy(sec_data_ptr->unblock_cks.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->unblock_cks.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->unblock_cks.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->unblock_cks.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_UNBLOCK_COUNTER:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->unblock_ck_counts.buffer_ptr = mem_pointer_ptr->buffer_start +
                                                     mem_pointer_ptr->curr_offset +
                                                     GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->unblock_ck_counts.len        = len;

        mmgsdi_memscpy(sec_data_ptr->unblock_ck_counts.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr + GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->unblock_ck_counts.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->unblock_ck_counts.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->unblock_ck_counts.len;
        break;

      case GSDI_PERSO_ENGINE_TAG_LOCK_DOWN:
        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_TAG_SIZE;
        file_buffer_ptr += GSDI_PERSO_ENGINE_TAG_SIZE;
        sec_data_ptr->lock_down.buffer_ptr = mem_pointer_ptr->buffer_start +
                                             mem_pointer_ptr->curr_offset +
                                             GSDI_PERSO_ENGINE_LEN_SIZE;
        sec_data_ptr->lock_down.len        = len;

        mmgsdi_memscpy(sec_data_ptr->lock_down.buffer_ptr,
                       buffer_size,
                       file_buffer_ptr +GSDI_PERSO_ENGINE_LEN_SIZE,
                       sec_data_ptr->lock_down.len);

        mem_pointer_ptr->curr_offset += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->lock_down.len;
        file_buffer_ptr += GSDI_PERSO_ENGINE_LEN_SIZE + sec_data_ptr->lock_down.len;
        break;

      default:
        UIM_MSG_ERR_0("PERSO: Invalid Tag");
        MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
        return MMGSDI_ERROR;
    }

    /* --------------------------------------------------------------------------
     The operation was successfull.
     Need to move the current Pointer over so that the data it points to isn't
     accidentally overwritten.  Ensure memory_curr does not exceed
     memory_end.
     ------------------------------------------------------------------------*/
    if(mem_pointer_ptr->curr_offset > mem_pointer_ptr->buffer_size)
    {
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: MEMORY POINTER USAGE ERROR");
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);
      return MMGSDI_ERROR;
    }
  }

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(orig_file_buffer_ptr);

  return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_get_ruim_encrypted_data */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_RUIM_CALC_MEM_REQUIRED

DESCRIPTION
  This function is called to calcuate the total memory required for a
  particular item.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
static mmgsdi_return_enum_type gsdi_perso_engine_ruim_calc_mem_required(
  gsdi_perso_eng_calc_req_mem_enum_type calc_type,
  gsdi_perso_engine_proc_enum_type      feature_ind,
  const void                          * req_ptr,
  int32                               * required_len_ptr
)
{
  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;
  /* -----------------------------------------------------------------------
     Validate the Parameters provided
     ---------------------------------------------------------------------*/
  MMGSDIUTIL_RETURN_IF_NULL_2(required_len_ptr, req_ptr);

  switch(calc_type)
  {
    case GSDI_PERSO_ENG_CALC_RUIM_ENC_DATA:
     /* ----------------------------------------------------------------------
       The total memory required is the sum of the following:
        Buffer size for the Feature Indicators
        Buffer size for the Depersonalization Keys
        Buffer size for the Depersonalization Counters
        Buffer size for the Mobile's IMEI
        Buffer size for the Mobile's Serial Number
       --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_calc_ruim_enc_data_len(feature_ind,
                                                               req_ptr,
                                                               required_len_ptr);
      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        return mmgsdi_status;
      }
      break;

    case GSDI_PERSO_ENG_CALC_RUIM_DATA:
      /* ----------------------------------------------------------------------
       The total memory required is the sum of the following:
        Buffer size for the IMSI
        Buffer size for the CNL
        Buffer size for the GID1
        Buffer size for the GID2
       --------------------------------------------------------------------*/
      mmgsdi_status = gsdi_perso_engine_calc_ruim_data_len(required_len_ptr);
      if(mmgsdi_status != MMGSDI_SUCCESS)
      {
        return mmgsdi_status;
      }
      break;
    default:
      mmgsdi_status = MMGSDI_ERROR;
      UIM_MSG_ERR_1("PERSO: Wrong calc_type 0x%x", calc_type);
      return mmgsdi_status;
   }
   return MMGSDI_SUCCESS;
} /* gsdi_perso_engine_ruim_calc_mem_required */


/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_HANDLE_RUIM_REQUEST

DESCRIPTION
  This function is the main handler for all the perso requests.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_handle_ruim_request(
  mmgsdi_session_id_type           session_id,
  mmgsdi_int_app_info_type        *app_info_ptr,
  mmgsdi_slot_id_enum_type         slot,
  gsdi_perso_engine_proc_enum_type feature_type,
  gsdi_perso_sec_client_req_type  *client_data,
  const void *                     req_ptr,
  uint8 *                          ret_data_ptr,
  uint32                           ret_data_max_len,
  int32*                           ret_data_len_ptr,
  gsdi_perso_event_enum_type      *perso_event_ptr)
{
  gsdi_perso_sec_sim_data_type       ruim_data;
  gsdi_returns_T                     gsdi_status;
  mmgsdi_return_enum_type            mmgsdi_status      = MMGSDI_SUCCESS;
  uint32                             total_mem_req      = 0x00;
  gsdi_perso_engine_mem_pointer_type enc_mem_pointer;
  gsdi_perso_sec_me_data_type        encrypted_data;
  boolean                            ruim_data_required = TRUE;

  /* Checking for card data necessity for given feature. */
  switch(feature_type)
  {
    case GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL:
    case GSDI_PERSO_ENGINE_AUTO_LOCK:
      ruim_data_required = TRUE;
      if (GSDI_PERSO_SEC_MASK_FEAT_NOT_INIT == client_data->feature_mask)
      {
        /* Return successful when no feature is masked */
        return MMGSDI_SUCCESS;
      }
      break;

    case GSDI_PERSO_ENGINE_REFRESH:
      ruim_data_required = TRUE;
      break;

    default:
      ruim_data_required = FALSE;
      break;
  } /* end of switch(feature_type) */

  if (TRUE == ruim_data_required)
  {
    if(app_info_ptr == NULL)
    {
      UIM_MSG_ERR_0("App Info Ptr is NULL");
      return MMGSDI_ERROR;
    }

    /* When RUIM PERSO is not applicable to the card,  return success. */
    if (MMGSDI_PERSO_RUIM_ALLOWED != (app_info_ptr->perso_ability
                                    & MMGSDI_PERSO_RUIM_ALLOWED))
    {
      UIM_MSG_HIGH_2("Card does not have RUIM PERSO capability. Given feature 0x%x, feature mask 0x%x",
                 feature_type, client_data->feature_mask);
      return MMGSDI_SUCCESS;
    }
  } /* if (TRUE == ruim_data_required) */

  /* --------------------------------------------------------------------------
    Initialize Data as necessary
  ------------------------------------------------------------------------*/
  memset(&enc_mem_pointer,0x00, sizeof(gsdi_perso_engine_mem_pointer_type));
  memset(&encrypted_data.data_mod,0x00, sizeof(gsdi_perso_sec_data_mod_inds_type));
  memset(&ruim_data,0x00, sizeof(gsdi_perso_sec_sim_data_type));

  /* --------------------------------------------------------------------------
    The Perso Engine Has an Interface to allow the Personalization
    Engine to determine how much memory needs to be allocated.
    ------------------------------------------------------------------------*/
  mmgsdi_status = gsdi_perso_engine_ruim_calc_mem_required(
                                            GSDI_PERSO_ENG_CALC_RUIM_ENC_DATA,
                                            feature_type,
                                            (void *)req_ptr,
                                            (int32*)&total_mem_req);

  if ( mmgsdi_status != MMGSDI_SUCCESS )
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get total memory required");
    return mmgsdi_status;
  }

  /* ---------------------------------------------------------------------------
    Allocate the entire buffer required for the encrypted data strcture
    Memset it to 0x00 when successful and the length required is greater
    than 0.
    Set memory_pointer.memory_curr = memory_pointer.memory_start
    Set memory_pointer.memory_end  = memory_pointer.memory_start + total_mem_req
  -------------------------------------------------------------------------*/
  if (total_mem_req > 0)
  {
   MMGSDIUTIL_MEM_MALLOC_AND_VALIDATE(
      enc_mem_pointer.buffer_start, total_mem_req)
    MMGSDIUTIL_RETURN_IF_NULL(enc_mem_pointer.buffer_start);;

    enc_mem_pointer.curr_offset = 0;
    enc_mem_pointer.buffer_size  = total_mem_req;
    /* ---------------------------------------------------------------------------
      Populate the encrypted data into the encrypted_data structure.
      The memory pointer will be used to provide memory for the
      encrypted_data structure buffers
      -------------------------------------------------------------------------*/
    mmgsdi_status = gsdi_perso_engine_get_ruim_encrypted_data(&encrypted_data,
                                                              &enc_mem_pointer,
                                                              feature_type,
                                                              (void *)req_ptr);
    if(mmgsdi_status != MMGSDI_SUCCESS)
    {
      /* An error occurred getting the Encrypted
         information from FS. All memory will be freed.
      */
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
      return mmgsdi_status;
    }
  } /* if (total_mem_req > 0) */
  else
  {
     UIM_MSG_HIGH_0("PERSO: Encrypted Data Not Required from GSDI ENGINE");
  }

  if( TRUE == ruim_data_required)
  {
    /* --------------------------------------------------------------------------
      Get the required RUIM Data.
      ------------------------------------------------------------------------*/
    mmgsdi_status = gsdi_perso_engine_get_ruim_data(session_id,
                                                    app_info_ptr,
                                                    slot,
                                                   &ruim_data);
    if ( mmgsdi_status != MMGSDI_SUCCESS )
    {
      /* An error occurred getting the  data
         All memory will be freed.
      */
      MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Failed to get SIM Data");
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
      MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(ruim_data.imsi.data_ptr);
      return MMGSDI_PERSO_CARD_DATA_ERROR;
    }
  } /* if( TRUE == ruim_data_required) */
  gsdi_status = gsdi_perso_security_handle_request(feature_type,
                                                   client_data,
                                                   &encrypted_data,
                                                   &ruim_data,
                                                   ret_data_ptr,
                                                   ret_data_max_len,
                                                   ret_data_len_ptr,
                                                   perso_event_ptr);
  mmgsdi_util_convert_gsdi_status_to_mmgsdi(gsdi_status, &mmgsdi_status);

  if ( mmgsdi_status == MMGSDI_SUCCESS )
  {
    UIM_MSG_HIGH_1("PERSO SECURITY 0x%x:  Success", feature_type);
  }
  else
  {
    UIM_MSG_ERR_2("PERSO SECURITY 0x%x: FAILURE 0x%x",
                  feature_type, mmgsdi_status);
  }

  if(MMGSDI_SUCCESS !=  gsdi_perso_engine_write_back_ruim_fs(&encrypted_data, TRUE))
  {
    MMGSDI_DEBUG_MSG_ERROR_0("PERSO: Could not write back FS");
    mmgsdi_status = MMGSDI_ERROR;
  }

  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(enc_mem_pointer.buffer_start);
  MMGSDIUTIL_TMC_MEM_FREE_NULL_OK(ruim_data.imsi.data_ptr);
  return mmgsdi_status;

}/* gsdi_perso_engine_handle_ruim_request */

#endif /*FEATURE_PERSO_RUIM*/
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

