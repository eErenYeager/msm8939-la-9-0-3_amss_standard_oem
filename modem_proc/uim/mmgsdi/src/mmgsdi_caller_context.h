#ifndef MMGSDICALLERCONTEXT_H
#define MMGSDICALLERCONTEXT_H
/*===========================================================================


            M M G S D I  P R O C E S S  I N  C L I E N T  C O N T E X T
            
                 D E F I N I T I O N S  A N D   F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 - 2015 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_caller_context.h#2 $$ $DateTime: 2015/02/20 05:06:00 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
02/20/15   stv      Optimize app capabilities with cached data in client context
08/11/14   bcho    Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/

#include "uim_variation.h"
#include "mmgsdi.h"


/*=============================================================================

                       FUNCTION PROTOTYPES

=============================================================================*/

/* ==========================================================================
   FUNCTION:      MMGSDI_CALLER_CONTEXT_CHECK_SERVICE_AND_NOTIFY_CLIENT

   DESCRIPTION:
     This function tries to retrieve service status from cached service table.
     If the function is invoked from MMGSDI tasks context we simply return
     without any processing.
     This function uses critical sections.

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type:

      MMGSDI_SUCCESS:          The retrieval of service status from cached
                               service table succeeded and client has
                               notified with the cnf
      MMGSDI_ERROR:            Otherwise

   SIDE EFFECTS:

==========================================================================*/
mmgsdi_return_enum_type mmgsdi_caller_context_check_service_and_notify_client(
  const mmgsdi_task_cmd_type             *task_cmd_ptr
);

/* ==========================================================================
   FUNCTION:      MMGSDI_CALLER_CONTEXT_READ_CACHE_AND_NOTIFY_CLIENT

   DESCRIPTION:
     This function tries to read the item from MMGSDI cache. If the read
     succeeds or the item is not found on the card, a response is built
     immediately and cnf is sent to the client. App state must be ready;
     otherwise we simply return without doing anything.
     If the function is invoked from MMGSDI tasks context, we simply return
     without any processing.
     This function uses critical sections.

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type:

      MMGSDI_SUCCESS:          The read from cache succeeded and client has
                               notified with the cnf
      MMGSDI_ERROR:            Otherwise

   SIDE EFFECTS:

==========================================================================*/
mmgsdi_return_enum_type mmgsdi_caller_context_read_cache_and_notify_client(
  const mmgsdi_task_cmd_type             *task_cmd_ptr
);

/* ==========================================================================
   FUNCTION:      MMGSDI_CALLER_CONTEXT_CHECK_APP_CAPABILITIES_AND_NOTIFY_CLIENT

   DESCRIPTION:
     This function tries to retrieve app capabilites from cached app info.
     If the function is invoked from MMGSDI tasks context we simply return
     without any processing.
     This function uses critical sections.

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type:

      MMGSDI_SUCCESS:          The retrieval of service status from cached
                               service table succeeded and client has
                               notified with the cnf
      MMGSDI_ERROR:            Otherwise

   SIDE EFFECTS:

==========================================================================*/

mmgsdi_return_enum_type mmgsdi_caller_context_check_app_capabilities_and_notify_client(
  const mmgsdi_task_cmd_type             *task_cmd_ptr
);

#endif /* MMGSDICALLERCONTEXT_H */
