#ifndef MMGSDI_MEMORY_CLEANUP_H
#define MMGSDI_MEMORY_CLEANUP_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


       M M G S D I    M E M O R Y    C L E A N U P    H E A D E R


*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_memory_cleanup.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/06/14    am     Initial Version
===========================================================================*/
#include "uim_variation.h"

#if defined FEATURE_MODEM_RCINIT && defined FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_MODEM_RCINIT && FEATURE_UIM_TEST_FRAMEWORK */

#endif /* MMGSDI_MEMORY_CLEANUP_H */

