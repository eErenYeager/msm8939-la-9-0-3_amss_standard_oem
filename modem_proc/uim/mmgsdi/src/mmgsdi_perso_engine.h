#ifndef MMGSDI_PERSO_ENGINE_H
#define MMGSDI_PERSO_ENGINE_H
/*===========================================================================


        G S D I   P E R S O N A L I Z A T I O N   E N G I N E

     S T R U C T U R E S  A N D  F U N C T I O N  P R O T O T Y P E S


DESCRIPTION
  Structure and Function prototype definitions for the personalization
  engine.  The gsdi_perso.c functions are the entry point into the
  engine.  The engine is going to retrieve all of the information required
  to allow the security library to perform the security procedures.


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2001-2012 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/src/mmgsdi_perso_engine.h#1 $$ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     --------------------------------------------------------
12/12/13   yt      Ensure correct buffer bounds checking
10/16/13   df      Removing gsdi legacy conversion functions
09/04/13   df      Adjust macro to include parenthesis around arithmetic
08/27/13   yt      Remove usage of secapi_get_random retry from perso
05/16/13   vdc     Replace memcpy with safer version memscpy
08/07/12   tl      Removed perso/perso_ruim.txt specific featurization
12/21/11   shr     Legacy GSDI removal updates
06/17/11   nmb     Corrected perso file TLV interpretation
08/30/10   shr     Perso clean up for Sessions
07/28/09   mib     Added mmgsdi_perso_lock_down_and_enable_limited_access
06/01/09   ssr     Merging of SIM and RUIM PERSO
04/01/09   ssr     Adding index for mmgsdi perso cnf
03/23/09   ssr     Fixed perso read length
03/02/09   sun     Added Private header file
12/18/08   sun     Removed old Perso
11/13/08   nk      Changed #elsif to #elif to fix compiler warning
09/25/08   kk      Added support for extracting unblock dck retries available
04/16/08   kk      Added support of get command on sim lock data
01/17/07   pv      Get rid of gsdi_perso_notify_clients_of_perso_event and
                   externalize gsdi_perso_engine_notify_clients_of_perso_event.
08/03/06   sun     Added new state for card error
04/28/06   sun     Added #define for MAX Length of NV Unlock Code
04/06/06   sun     Added support for RUIM Lock
03/21/06   tml     lint
03/08/06   jar     Merged in Referece Security Library
12/14/05   sun     Fixed more lint errors
11/09/05   sun     Fixed lint errors
10/03/05   sun     Pass the event from the perso engine to the security and back
05/13/05   sun     Consolidated all perso files into one file
05/05/05   sun     Added support for Perso Refresh
05/01/05   jar     Fix for NON QC Security Library Builds
04/30/05   jar     Added an IMEI Unparsed Len
04/28/05   jar     Initial release of the GSDI SIM Lock Architecutre to support
                   Plug In Proprietary Security Libraries driven by a Perso Engine.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "uim_variation.h"
#include "customer.h"
#include "uim.h"
#include "mmgsdi.h"
#include "gsdi_p.h"

#define MMGSDI_PERSO_NOT_APPLICABLE            0x00
#define MMGSDI_PERSO_SIM_ALLOWED               0x01
#define MMGSDI_PERSO_RUIM_ALLOWED              0x02

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* Offsets used when taking data_p information and populating the cnf rsp */
#ifdef FEATURE_PERSO_SIM
#define GSDI_PERSO_MM_SIM_NW_STATUS_IND_OFFSET                   0x00
#define GSDI_PERSO_MM_SIM_NS_STATUS_IND_OFFSET                   0x01
#define GSDI_PERSO_MM_SIM_SP_STATUS_IND_OFFSET                   0x02
#define GSDI_PERSO_MM_SIM_CP_STATUS_IND_OFFSET                   0x03
#define GSDI_PERSO_MM_SIM_SIM_STATUS_IND_OFFSET                  0x04
#endif /* FEATURE_PERSO_SIM */
#ifdef FEATURE_PERSO_RUIM
#define GSDI_PERSO_MM_RUIM_NW1_STATUS_IND_OFFSET  (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x00)
#define GSDI_PERSO_MM_RUIM_NW2_STATUS_IND_OFFSET  (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x01)
#define GSDI_PERSO_MM_RUIM_HRPD_STATUS_IND_OFFSET (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x02)
#define GSDI_PERSO_MM_RUIM_SP_STATUS_IND_OFFSET   (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x03)
#define GSDI_PERSO_MM_RUIM_CP_STATUS_IND_OFFSET   (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x04)
#define GSDI_PERSO_MM_RUIM_RUIM_STATUS_IND_OFFSET (GSDI_PERSO_NUM_OF_SIM_FEATURES + 0x05)
#endif /* FEATURE_PERSO_RUIM */
#define GSDI_PERSO_STATUS_IND_OFFSET_MAX          (GSDI_PERSO_NUM_OF_SIM_FEATURES + \
                                                   GSDI_PERSO_NUM_OF_RUIM_FEATURES)


/* -------------------------------------------------------------------------
   Makeshift request not part of the Normal Set of Requests
   used to fake the Personalization Engine into makeing a request
   for all Personalization Feature Indicators
   -----------------------------------------------------------------------*/
#define GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS_REQ               0xFF
#define GSDI_PERSO_ENGINE_CHECK_ALL_LOCKS_REQ                    0xFE
#define GSDI_PERSO_ENGINE_CHECK_AUTOLOCKS_REQ                    0xFD
#define GSDI_PERSO_ENGINE_SIM_REFRESH_REQ                        0xFC
#define GSDI_PERSO_ENGINE_SIM_AUTO_LOCK_REQ                      0xFB

#define GSDI_PERSO_ENGINE_MAX_CK_W_NULL 0x09

/* Max Number of Clients supported for Personalization Events */
#define GSDI_PERSO_ENGINE_MAX_REG_CLIENTS                        0x0A

/* Non Valid Index Values which indicate different statuses   */
#define GSDI_PERSO_ENGINE_MIN_TABLE_INDEX                        0x0000
#define GSDI_PERSO_ENGINE_EVENT_HANDLER_ALREADY_REGISTERD        0x00FE
#define GSDI_PERSO_ENGINE_NO_EVENT_HANDLER_TABLE_INDEX_AVAILABLE 0x00FF

#define GSDI_PERSO_ENGINE_SERIAL_NUMBER_LEN    0x16
#define GSDI_PERSO_ENGINE_IMSI_LEN             0x09
#define GSDI_PERSO_ENGINE_GID1_LEN             0x64 /* 100 Bytes */
#define GSDI_PERSO_ENGINE_GID2_LEN             0x64 /* 100 Bytes */
#define GSDI_PERSO_ENGINE_CNL_LEN              0xFF /* 255 Bytes */

#define MS_PERSO_ENGINE_GSDI                   13
#define GSDI_PERSO_ENGINE_IMEI_UNPARSED_LEN    8
#define GSDI_PERSO_ENGINE_IMEI_UNPARSED_NV_LEN  9

#define GSDI_PERSO_ENGINE_NO_IND_INFO          0xFF;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_ENGINE_DCK_CK_LEN
   The maximum length for a Control Key in the DCK EF
   --------------------------------------------------------------------------*/
#define GSDI_PERSO_ENGINE_DCK_CK_LEN              0x04
#define GSDI_PERSO_BUFFER_LEN                     500

#define GSDI_PERSO_OTA_DEPERSO_RSP_LEN         20
/* OTA Deperso Depersonalization CK Len */
#define GSDI_PERSO_OTA_DEPERSO_CK_LEN          0x08

/* Shift for significant length byte stored in the perso file*/
#define GSDI_PERSO_SIGNIFICANT_LEN_BYTE_SHIFT 256

/* ----------------------------------------------------------------------------
   Table to hold the list of event handlers
   --------------------------------------------------------------------------*/
extern gsdi_perso_events_cb gsdi_perso_event_handler_table[GSDI_PERSO_ENGINE_MAX_REG_CLIENTS];

/* --------------------------------------------------------------------------
   STRUCTURE:    GSDI_PERSO_SEC_SIM_ELE_DATA_TYPE

   DESCRIPTION:  This is the basic SIM Data Element provided to the security
                 library.

                 This structure can be used for the data elements required
                 in the SIM Personalization Procedures.

                 EF-IMSI
                 EF-CNL
                 EF-GID1
                 EF-GID2
   ------------------------------------------------------------------------*/
typedef struct {
  int32   len;
  uint8 * buffer_p;
}gsdi_perso_eng_sim_ele_data_type;


typedef struct {
  gsdi_cmdhdr_T             message_header;
}
gsdi_perso_init_proc_req_T;

typedef enum {
  GSDI_PERSO_ENGINE_ACTIVATE_FEATURE   = 0x00000000,
  GSDI_PERSO_ENGINE_DEACTIVATE_FEATURE,
  GSDI_PERSO_ENGINE_GET_FEATURE_IND,
  GSDI_PERSO_ENGINE_GET_ALL_FEATURE_INDS,
  GSDI_PERSO_ENGINE_SET_DATA_FEATURE,
  GSDI_PERSO_ENGINE_UNBLOCK_DCK,
  GSDI_PERSO_ENGINE_OTA_DERPERSO,
  GSDI_PERSO_ENGINE_INT_PROC,
  GSDI_PERSO_ENGINE_PERM_DISABLE_FEATURE_IND,
  GSDI_PERSO_ENGINE_GET_PERM_FEATURE_IND,
  GSDI_PERSO_ENGINE_GET_DCK_NUM_RETRIES,
  GSDI_PERSO_ENGINE_GET_FEATURE_KEY,
  GSDI_PERSO_ENGINE_CHECK_LOCKS_ALL,
  GSDI_PERSO_ENGINE_AUTO_LOCK,
  GSDI_PERSO_ENGINE_REFRESH,
  GSDI_PERSO_ENGINE_GET_FEATURE_DATA,
  GSDI_PERSO_ENGINE_GET_DCK_UNBLOCK_NUM_RETRIES,
  GSDI_PERSO_ENGINE_LOCK_DOWN,
  GSDI_PERSO_ENGINE_LT                = 0x7FFFFFFF
}gsdi_perso_engine_proc_enum_type;

#define GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS   5
#define GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS  6

#if defined(FEATURE_PERSO_SIM) && defined(FEATURE_PERSO_RUIM)
#define GSDI_PERSO_ENGINE_TOTAL_LOCKS (GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS + GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS)
#elif defined(FEATURE_PERSO_SIM)
#define GSDI_PERSO_ENGINE_TOTAL_LOCKS GSDI_PERSO_ENGINE_SIM_NUM_OF_LOCKS
#elif defined(FEATURE_PERSO_RUIM)
#define GSDI_PERSO_ENGINE_TOTAL_LOCKS GSDI_PERSO_ENGINE_RUIM_NUM_OF_LOCKS
#endif

#define GSDI_PERSO_ENGINE_TAG_SIZE      sizeof(uint8)
#define GSDI_PERSO_ENGINE_LEN_SIZE      (sizeof(uint8) * 0x02)
#define GSDI_PERSO_ENGINE_HI_LEN        sizeof(uint8)
#define GSDI_PERSO_ENGINE_LO_LEN        sizeof(uint8)
#define GSDI_PERSO_ENGINE_TAG_CODE      0x00
#define GSDI_PERSO_ENGINE_TAG_DCK       0x01


#define GSDI_PERSO_ENGINE_TAG_FEATURE_INDS      0x01
#define GSDI_PERSO_ENGINE_TAG_PERM_FEATURE_INDS 0x02
#define GSDI_PERSO_ENGINE_TAG_SIM_LOCK_CODE     0x03
#define GSDI_PERSO_ENGINE_TAG_DEPERSO_CK        0x04
#define GSDI_PERSO_ENGINE_TAG_DCK_COUNTER       0x05
#define GSDI_PERSO_ENGINE_TAG_UNBLOCK           0x06
#define GSDI_PERSO_ENGINE_TAG_UNBLOCK_COUNTER   0x07
#define GSDI_PERSO_ENGINE_TAG_LOCK_DOWN         0x08
#define GSDI_PERSO_ENGINE_NUMBER_OF_TAGS        0x08
#define GSDI_PERSO_ENGINE_MAX_FILE_SIZE         (GSDI_PERSO_ENGINE_NUMBER_OF_TAGS * \
                                                 (GSDI_MAX_DATA_BLOCK_LEN + \
                                                  GSDI_PERSO_ENGINE_TAG_SIZE + \
                                                  GSDI_PERSO_ENGINE_LEN_SIZE))

/* ----------------------------------------------------------------------------
   GSDI_PERSO_DCK_NUM_CKS
   The maximum number of Control Keys as specified in 11.11 and 31.102.
   When a control key is not used, it will contain the default 0xFFFFFFFF
   --------------------------------------------------------------------------*/
#ifdef FEATURE_PERSO_SIM
#define GSDI_PERSO_ENGINE_SIM_DCK_NUM_CKS             0x04
#endif
#ifdef FEATURE_PERSO_RUIM
#define GSDI_PERSO_ENGINE_RUIM_DCK_NUM_CKS            0x05
#endif


#define GSDI_PERSO_INT_INIT_REQ 0
#define GSDI_PERSO_SELF_INIT_REQ 1
#define GSDI_PERSO_ENGINE_INIT_REQ 2

/* IMEI Masking Digits */
#define GSDI_PERSO_ENG_IMEI_HIGH_BYTE              0xF0
#define GSDI_PERSO_ENG_IMEI_LOW_BYTE               0x0F

/* IMSI Digit Offset */
#define GSDI_PERSO_ENG_IMSI_DIGIT_1_OFFSET         0x01
#define GSDI_PERSO_ENG_IMSI_DIGIT_2_OFFSET         0x02
#define GSDI_PERSO_ENG_IMSI_DIGIT_3_OFFSET         0x02
#define GSDI_PERSO_ENG_IMSI_DIGIT_4_OFFSET         0x03
#define GSDI_PERSO_ENG_IMSI_DIGIT_5_OFFSET         0x03
#define GSDI_PERSO_ENG_IMSI_DIGITS_4_5_OFFSET      0x03
#define GSDI_PERSO_ENG_IMSI_DIGIT_6_OFFSET         0x04
#define GSDI_PERSO_ENG_IMSI_HIGH_DIGIT_MASK        GSDI_PERSO_ENG_IMEI_HIGH_BYTE
#define GSDI_PERSO_ENG_IMSI_LOW_DIGIT_MASK         GSDI_PERSO_ENG_IMEI_LOW_BYTE

/* ----------------------------------------------------------------------------
   ENUMERATION:  GSDI_PERSO_STATE_ENUM_TYPE
   DESCRIPTION:  Will indicate the different personalization states.
                 GSDI_PERSO_INIT_STATE:       State prior to running any
                                              personalization Code.  Only taken
                                              out of the NOT INIT State into the
                                              not active state after the
                                              Init data procedures are completed
                 GSDI_PERSO_NOT_ACTIVE_STATE: No personalization inds
                                              are active.  All depersonalized
                 GSDI_PERSO_AUTOLOCK_STATE:   Run the autolocking procedures
                 GSDI_PERSO_ACTIVE_SATE:      At lease one personalization
                                              indicator is active.
  --------------------------------------------------------------------------*/
typedef enum {
  GSDI_PERSO_ENG_NOT_ACTIVE_STATE = 0x00000000,
  GSDI_PERSO_ENG_AUTOLOCK_STATE,
  GSDI_PERSO_ENG_ACTIVE_STATE,
  GSDI_PERSO_ENG_NOT_INIT_STATE,
  GSDI_PERSO_ENG_UNLOCKED_STATE,
  GSDI_PERSO_ENG_DISABLED_STATE,
  GSDI_PERSO_ENG_MAX              = 0x7FFFFFFF
} gsdi_perso_engine_state_enum_state;


/* ----------------------------------------------------------------------------
   ENUMERATION:  GSDI_PERSO_RETURN_STATUS_ENUM_TYPE
   DESCRIPTION:  Will be used as the return type for the GSDI
  --------------------------------------------------------------------------*/
typedef enum {
  GSDI_PERSO_ENGINE_SUCCESS               = 0x00000000,
  GSDI_PERSO_ENGINE_SUCCESS_WRITE_BACK,
  GSDI_PERSO_ENGINE_ERROR,
  GSDI_PERSO_ENGINE_ERROR_WRITE_BACK,
  GSDI_PERSO_ENGINE_ERROR_GET_NV_FAILURE,
  GSDI_PERSO_ENGINE_ERROR_PUT_NV_FAILURE,
  GSDI_PERSO_ENGINE_ERROR_GET_MEMMALLOC,
  GSDI_PERSO_ENGINE_ERROR_REQ_DCK,                     /* PERSO MISMATCH */
  GSDI_PERSO_ENGINE_ERROR_REQ_MASTER_CODE,             /* DCK BLOCKED    */
  GSDI_PERSO_ENGINE_INCORRECT_PARAMS,                  /* BAD Params     */
  GSDI_PERSO_ENGINE_STATUS_MAX            = 0x7FFFFFFF
}gsdi_perso_engine_status_enum_type;

/* ----------------------------------------------------------------------------
   STRUCTURE:    GSDI_PERSO_ENGINE_MEM_POINTER_TYPE
   DESCRIPTION:  Will provide information pertaining to the larget memory
                 allocations that occur in the Personalization Engine.
  --------------------------------------------------------------------------*/
typedef struct {
  uint8 * buffer_start;
  size_t  curr_offset;
  size_t  buffer_size;
}gsdi_perso_engine_mem_pointer_type;

/* ----------------------------------------------------------------------------
   STRUCTURE:    gsdi_perso_eng_calc_req_mem_enum_type

   DESCRIPTION:  Used to calcuate the total memory required for a particular
                 data type sent to the security Library.  It is up to the
                 personalization engine to determine how much memory is
                 required.
   --------------------------------------------------------------------------*/
typedef enum {
  GSDI_PERSO_ENG_CALC_ENC_DATA    = 0x0000,
  GSDI_PERSO_ENG_CALC_SIM_DATA,
  GSDI_PERSO_ENG_CALC_CLIENT_DATA,
  GSDI_PERSO_ENG_CALC_RUIM_ENC_DATA,
  GSDI_PERSO_ENG_CALC_RUIM_DATA
} gsdi_perso_eng_calc_req_mem_enum_type;


/* <EJECT> */
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/
/* ---------------------------------------------------------------------------
   FUNCTION:      GSDI_PERSO_ENGINE_GET_FEATURE_OFFSET

   DESCRIPTION:
     This function provides the offset for a Personalization feature

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type

   SIDE EFFECTS:
     None
-----------------------------------------------------------------------------*/
mmgsdi_return_enum_type gsdi_perso_engine_get_feature_offset(
  mmgsdi_perso_feature_enum_type  feature_ind,
  uint8                          *offset_ptr
);

/* ---------------------------------------------------------------------------
FUNCTION:  GSDI_PERSO_ENGINE_INITIALIZE_DATA

DESCRIPTION:
  Will initialize the MMGSDI Perisonalization engine with the data required
  to start the Personalization Intialization Procedures.

DEPENDENCIES:
  Function must be called only after PIN1 has been verified.

LIMITATIONS:
  None

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
mmgsdi_return_enum_type gsdi_perso_engine_initialize_data(
  mmgsdi_session_id_type  session_id
);

/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_HANDLE_REQUEST

DESCRIPTION
  This function is the main handler for all the perso requests.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_handle_request(
  mmgsdi_session_id_type       session_id,
  mmgsdi_int_app_info_type    *app_info_ptr,
  mmgsdi_slot_id_enum_type     slot,
  const void *                 req_p,
  byte                         msg_id,
  uint8 *                      ret_data_p,
  uint32                       ret_data_max_len,
  int32*                       ret_data_len_p,
  gsdi_perso_event_enum_type * perso_event_p
);

/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_MAIN

DESCRIPTION
  This function is called after the Verification procedures have occurred on
  PIN1.

DEPENDENCIES
  None

RETURN VALUE
  mmgsdi_return_enum_type
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_main(
  mmgsdi_session_id_type    session_id,
  const void *              cmd_p,
  byte                      msg_id,
  uint8 *                   ret_data_p,
  uint32                    ret_data_max_len,
  int32 *                   ret_data_len_p
);

/* ----------------------------------------------------------------------------
FUNCTION: GSDI_PERSO_ENGINE_GET_IMEI

DESCRIPTION:
  Will retrieve the IMEI number from NV.

DEPENDENCIES:
  None

LIMITATIONS:
  The memory allocated in this function will need to be released by the
  caller of this function.

RETURN TYPE:
  mmgsdi_return_enum_type

-----------------------------------------------------------------------------*/
mmgsdi_return_enum_type gsdi_perso_engine_get_imei (
  boolean  parse,
  uint8 *  imei_data_p,
  size_t   imei_buf_size,
  uint32 * imei_len_p
);

/*===========================================================================
FUNCTION GSDI_PERSO_NOTIFY_CLIENTS_OF_PERSO_EVENT

DESCRIPTION
  This function is used to notify the clients of any Personalization
  Events.

DEPENDENCIES
  None

RETURN VALUE
  void
===========================================================================*/
void gsdi_perso_engine_notify_clients_of_perso_event(
  mmgsdi_slot_id_enum_type    slot,
  gsdi_perso_event_enum_type  event,
  mmgsdi_int_app_info_type   *app_info_ptr,
  boolean                     send_to_all_prov
);

/*===========================================================================
FUNCTION GSDI_PERSO_ENGINE_INIT

DESCRIPTION
  This function is used to create all of the relevant Personalization Files
  used for storing information in EFS.

  This procedure is ran every time the mobile is powered up, but will not
  recreate any files already created.

  If all personalization files are not created properly or upon entering
  this function again, only some of the files exist...then an Error Fatal
  will occur.  This could be evidence of tampering.

  It will be acceptable for the file to be empty.

DEPENDENCIES
  None

RETURN VALUE
mmgsdi_return_enum_type:
                MMGSDI_SUCCESS : All perso files exist
                MMGSDI_ERROR   : Finding/creating the required files failed
===========================================================================*/
mmgsdi_return_enum_type gsdi_perso_engine_init(
  void
);

#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
#endif /* MMGSDI_PERSO_ENGINE_H */
