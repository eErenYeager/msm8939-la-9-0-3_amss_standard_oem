#ifndef GSDI_P_H
#define GSDI_P_H
/*===========================================================================


        G S D I   P R I V A T E   D E F I N I T I O N S   A N D
                           F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2012-2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved. QUALCOMM Technologies Proprietary.  
Export of this technology or software is regulated by the
U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/mmgsdi/inc/gsdi_p.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/18/14   tl      Introduce new SIM Lock feature
10/16/13   df      Removing unused gsdi_returns_T values
06/24/13   vdc     Added support for triple SIM
04/16/13   tl      Clean up of radio access features from MMGSDI code
09/04/12   vv      Removed duplicate entry for GSDI_PERSO_MAX_CK
08/10/12   av      Removed RPC support along with RPC style comments
12/21/11   shr     Legacy GSDI removal updates
10/13/11   shr     Adding support for full recovery
06/27/11   yt      Removed featurization from UIM public headers
06/06/11   yt      Updated P4 path template
06/06/11   yt      Added P4 path template
05/17/11   shr     Updates for Thread safety
04/28/11   yt      Initial revision

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "uim_variation.h"
#ifdef FEATURE_MMGSDI_3GPP
#include "ms.h"
#endif /* FEATURE_MMGSDI_3GPP */
#include "mmgsdilib_common.h"

/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

#define MS_UIM_SERVER               0x30
#define GSDI_LINK_ESTABLISHED_MSG   0x10

#ifndef MS_GSDI
#define MS_GSDI                     13
#endif

#ifndef MS_TIMER
#define MS_TIMER                    0xF0
#endif

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* Number of SIM lock features */
#ifdef FEATURE_PERSO_SIM
#define GSDI_PERSO_NUM_OF_SIM_FEATURES   5
#else
#define GSDI_PERSO_NUM_OF_SIM_FEATURES   0
#endif
#ifdef FEATURE_PERSO_RUIM
#define GSDI_PERSO_NUM_OF_RUIM_FEATURES  6
#else
#define GSDI_PERSO_NUM_OF_RUIM_FEATURES  0
#endif /* FEATURE_PERSO_RUIM */
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

/* ----------------------------------------------------------------------------
** MESSAGES - Command / Request
** --------------------------------------------------------------------------*/
#define MID_GSDI_MMI_SIM_IND                0x01
#define MMI_SIM_IND                         MID_GSDI_MMI_SIM_IND
#define GSDI_CNF                            0x11
#define GSDI_SIM_INIT_REQ                   0x80
#define GSDI_SIM_READ_REQ                   0x81
#define GSDI_SIM_WRITE_REQ                  0x82
#define GSDI_SIM_INCREASE_REQ               0x83
#define GSDI_RUN_GSM_ALGORITHM_REQ          0x84
#define GSDI_GET_FILE_ATTRIBUTES_REQ        0x85
#define GSDI_GET_PIN_STATUS_REQ             0x86
#define GSDI_VERIFY_PIN_REQ                 0x87
#define GSDI_CHANGE_PIN_REQ                 0x88
#define GSDI_UNBLOCK_PIN_REQ                0x89
#define GSDI_DISABLE_PIN_REQ                0x8A
#define GSDI_ENABLE_PIN_REQ                 0x8B
#define GSDI_INIT_REQ                       0x8C
#define GSDI_GET_SIM_CAPABILITIES_REQ       0x8D
#define GSDI_ENABLE_FDN_REQ                 0x8E
#define GSDI_DISABLE_FDN_REQ                0x8F
#define GSDI_ILLEGAL_SIM_REQ                0x90
#define GSDI_CALL_STATE_REQ                 0x91
#define GSDI_SIM_SEARCH_REQ                 0x92
#define GSDI_USIM_AUTHENTICATION_REQ        0x93
#define GSDI_REGISTER_CALLBACK_REQ          0x94
#define GSDI_SELECT_REQ                     0x95
#define GSDI_STORE_ESN_REQ                  0x96
#define GSDI_GET_IMAGE_REQ                  0x97
#define GSDI_SIM_REFRESH_REQ                0x98
#define GSDI_REG_FOR_REFRESH_VOTE_REQ       0x9A
#define GSDI_REG_FOR_FILE_NOTIFY_REQ        0x9B
#define GSDI_SIM_REFRESH_COMPLETE_REQ       0x9C
#define GSDI_INTERNAL_REQ                   0x9D
#define GSDI_REFRESH_UPDATE_CALL_STACK_REQ  0x9F
#define GSDI_NOTIFIED_ERROR_REQ             0xA0
#define GSDI_NOTIFY_CLIENT_REQ              0xA1
#define GSDI_SEND_APDU_REQ                  0xA2
#define GSDI_SWITCH_SLOT_PREF_REQ           0xA3
#define GSDI_GET_ATR_REQ                    0xA4
#define GSDI_OPEN_CHANNEL_REQ               0xA5
#define GSDI_CLOSE_CHANNEL_REQ              0xA6

#define GSDI_SEND_CHANNEL_DATA_REQ          0xA7
#define GSDI_GET_ECC_REQ                    0xA8
#define GSDI_GET_PBR_TABLE_REQ              0xA9
#define GSDI_SIM_ACCESS_REQ                 0xAB
#define GSDI_GET_ALL_PIN_INFO_REQ           0xAC

/* ----------------------------------------------------------------------------
   Common PCN Handset Specification (CPHS) REQ (MESSAGE IDS
   --------------------------------------------------------------------------*/
#define GSDI_SIM_GET_CPHS_INFORMATION_REQ        0xD0

#define GSDI_INTERNAL_POST_PIN1_INIT_REQ         0xD1
#define GSDI_ACTIVATE_ONCHIP_SIM_REQ             (GSDI_INTERNAL_POST_PIN1_INIT_REQ + 1)

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* ----------------------------------------------------------------------------
   PERSONALIZATION REQ (MESSAGE) IDS
   --------------------------------------------------------------------------*/
#define GSDI_PERSO_REGISTER_TASK_REQ               0xB0
#define GSDI_PERSO_ACT_FEATURE_IND_REQ             (GSDI_PERSO_REGISTER_TASK_REQ   + 1)
#define GSDI_PERSO_DEACT_FEATURE_IND_REQ           (GSDI_PERSO_ACT_FEATURE_IND_REQ + 1)
#define GSDI_PERSO_GET_FEATURE_IND_REQ             (GSDI_PERSO_DEACT_FEATURE_IND_REQ+1)
#define GSDI_PERSO_SET_FEATURE_DATA_REQ            (GSDI_PERSO_GET_FEATURE_IND_REQ + 1)
#define GSDI_PERSO_OTA_DERPERSO_REQ                (GSDI_PERSO_SET_FEATURE_DATA_REQ+ 1)
#define GSDI_PERSO_INT_PROC_REQ                    (GSDI_PERSO_OTA_DERPERSO_REQ + 1)
#define GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ         (GSDI_PERSO_INT_PROC_REQ + 1)
#define GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ    (GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ + 1)
#define GSDI_PERSO_GET_PERM_FEATURE_IND_REQ        (GSDI_PERSO_PERM_DISABLE_FEATURE_IND_REQ + 1)
#define GSDI_PERSO_GET_FEATURE_KEY_REQ             (GSDI_PERSO_GET_PERM_FEATURE_IND_REQ + 1)
#define GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ         (GSDI_PERSO_GET_FEATURE_KEY_REQ + 1)
#define GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ (GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ + 1)
#define GSDI_PERSO_GET_FEATURE_DATA_REQ            (GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ + 1)
#define GSDI_PERSO_LOCK_DOWN_REQ                   (GSDI_PERSO_GET_FEATURE_DATA_REQ + 1)
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

#define GSDI_INTERNAL_PUP_INIT_REQ          0xAD

#define MS_GSDI_UIM_SANITY_TIMER_EXPIRE     0x20

#define GSDI_MAX_MSG_LEN             575

#define GSDI_PUT_IMH_LEN(len,IMHP) \
        ((IMH_T *)(void*)IMHP)->message_len_msb = (unsigned char)((len) / 0x100), \
        ((IMH_T *)(void*)IMHP)->message_len_lsb = (unsigned char)((len) % 0x100);

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* --------------------------------------------------------------------------
   DATA / BUFFER LENGTHS
   Max number of bytes allowed in the Control Key.  As
   currently spec'ed out, there shall be a max of 8 characters
   allowed per control key.  Since they are NULL Terminated, the
   buffer is 8 Chars + 1 Null Char.
   ------------------------------------------------------------------------*/
#define GSDI_PERSO_MAX_CK               0x0008
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

/* GSDI_SLOT_1 is the default value for single slot solution */
typedef enum {
  GSDI_SLOT_NONE = 0x00,
  GSDI_SLOT_1 = 0x01,
  GSDI_SLOT_2 = 0x02,
  GSDI_SLOT_3 = 0x03,
  GSDI_SLOT_AUTOMATIC = 0x04,
  GSDI_SLOT_MAX = 0xFF
}gsdi_slot_id_type;

typedef enum {
    GSDI_SUCCESS = 0,
    GSDI_ERROR,
    GSDI_ACCESS_DENIED,
    GSDI_NOT_FOUND,
    GSDI_INCOMPAT_PIN_STATUS,
    GSDI_INCORRECT_CODE,
    GSDI_CODE_BLOCKED,
    GSDI_INCREASE_IMPOSSIBLE,
    GSDI_INCORRECT_PARAMS,
    GSDI_NOT_SUPPORTED,

    GSDI_NOT_INIT,
    GSDI_SUCCESS_BUT_ILLEGAL_SIM,
    GSDI_AUTH_ERROR_INCORRECT_MAC,
    GSDI_AUTH_ERROR_GSM_CNTXT_NOT_SUP,
    GSDI_SIM_TECHNICAL_PROBLEMS,
    GSDI_NO_EF_SELECTED,
    GSDI_EF_INCONSISTENT,
    GSDI_ERROR_NO_EVENT_NEEDED,
    GSDI_PIN_NOT_INITIALIZED,
    GSDI_UNKNOWN_INST_CLASS,

    GSDI_WARNING_NO_INFO_GIVEN,
    GSDI_WARNING_POSSIBLE_CORRUPTION,
    GSDI_INCORRECT_LENGTH,
    GSDI_UIM_CMD_TIMEOUT,
    GSDI_CODE_PERM_BLOCKED,
    GSDI_REFRESH_SUCCESS,
    GSDI_REFRESH_IN_PROGRESS,
    GSDI_CAN_NOT_REFRESH,
    GSDI_PATHS_DECODE_ERROR,
    GSDI_SUCCESS_AFTER_CARD_INTERNAL_UPDATE,

    GSDI_SIM_BUSY,
    GSDI_INVALIDATION_CONTRADICTION_STATUS,
    GSDI_INCREASE_MAX_REACHED,
    GSDI_AUTH_FAIL,
#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
    GSDI_PERSO_CHECK_FAILED,    /* Personalization Check Failed               */
    GSDI_PERSO_INVALID_CK,      /* Control Key Verification Failed            */
    GSDI_PERSO_DUPLICATE_CK,    /* Attempted to set Control Key already used  */
    GSDI_PERSO_CK_BLOCKED,      /* Wrong Control Key Verification Exceeded Max*/
    GSDI_PERSO_INVALID_DATA,    /* Perso Data Not To spec                     */
    GSDI_PERSO_PERM_DISABLED,    /* Perso Feature is Perm Disabled             */

    GSDI_PERSO_UNBLOCK_CK_BLOCKED,
    GSDI_PERSO_INCONSISTENT_W_IND,
    GSDI_PERSO_SUCCESS_DEL_DCK, /* Status which indicates success & delete DCK   */
    GSDI_PERSO_CARD_DATA_ERROR,/* Personalization error due to invalid Card data */
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
    GSDI_CMD_QUEUE_FULL,        /* Command Queue is full */
    GSDI_UIM_RST_INSTN_CHG_SUCCESS, /* A command to reset uim with new
                                       instruction class is successful and
                                       is in progress */
    GSDI_REFRESH_SUCCESS_BUT_NO_AID_ACTIVE,
    GSDI_REFRESH_FAIL_INVALID_AID,
    GSDI_REFRESH_FCN_ADDITIONAL_FCN_READ,
    GSDI_RPC_ERROR,            /* Error code for ONRPC error */
    GSDI_APP_STATE_DENIED,     /* Command is not allowed in APP state,
                                  specifically use for our internal purpose */

    GSDI_MAX_RETURN_STATUS = 0x7FFFFFFF
}gsdi_returns_T;

/* ----------------------------------------------------------------------
   ENUM       :   GSDI_RESP_TYPE
   DESCRIPTION:   GSDI ASYNCH RESPONSE TYPES
   --------------------------------------------------------------------------*/
typedef enum {
    GSDI_NO_RSP = 0x00,
    GSDI_SIM_READ_RSP,
    GSDI_SIM_WRITE_RSP,
    GSDI_SIM_INCREASE_RSP,
    GSDI_RUN_GSM_ALGORITHM_RSP,
    GSDI_SELECT_RSP,
    GSDI_GET_FILE_ATTRIBUTES_RSP,
    GSDI_GET_PIN_STATUS_RSP,
    GSDI_VERIFY_PIN_RSP,
    GSDI_CHANGE_PIN_RSP,

    /*10 0x0A*/
    GSDI_UNBLOCK_PIN_RSP,
    GSDI_DISABLE_PIN_RSP,
    GSDI_ENABLE_PIN_RSP,
    GSDI_INIT_RSP,
    GSDI_GET_SIM_CAPABILITIES_RSP,
    GSDI_ENABLE_FDN_RSP,
    GSDI_DISABLE_FDN_RSP,
    GSDI_ILLEGAL_SIM_RSP,
    GSDI_CALL_STATE_RSP,
    GSDI_SIM_SEARCH_RSP,

    /*20 0x14*/
    GSDI_USIM_AUTHENTICATION_RSP,
    GSDI_STORE_ESN_RSP,
    GSDI_GET_IMAGE_RSP,
    GSDI_SIM_REFRESH_RSP,
    GSDI_REFRESH_REGISTRATION_VOTE_RSP,
    GSDI_REFRESH_REGISTRATION_FCN_RSP,
    GSDI_REFRESH_COMPLETED_RSP,
    GSDI_SEND_APDU_RSP,
    GSDI_SWITCH_SLOT_PREF_RSP,
    GSDI_OPEN_CHANNEL_RSP,

    /*30 0x1F*/
    GSDI_CLOSE_CHANNEL_RSP,
    GSDI_SEND_CHANNEL_DATA_RSP,
    GSDI_GET_ATR_RSP,
#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
    GSDI_PERSO_REG_TASK_RSP,
    GSDI_PERSO_ACT_IND_RSP,
    GSDI_PERSO_DEACT_IND_RSP,
    GSDI_PERSO_GET_IND_RSP,
    GSDI_PERSO_SET_IND_RSP,
    GSDI_PERSO_SET_DATA_RSP,
    GSDI_PERSO_OTA_DEPERSO_RSP,

    /*40 0x29*/
    GSDI_PERSO_INTERNAL_PROC_RSP,
    GSDI_PERSO_UNBLOCK_IND_RSP,
    GSDI_PERSO_PERM_DISABLE_IND_RSP,
    GSDI_PERSO_GET_PERM_IND_RSP,
    GSDI_PERSO_GET_FEATURE_KEY_RSP,
    GSDI_PERSO_GET_DCK_NUM_RETRIES_RSP,
    GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_RSP,
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
    GSDI_GET_CPHS_RSP,
    GSDI_GET_ECC_RSP,
    GSDI_GET_PRB_TABLE_RSP,

    /*50 0x33*/
    GSDI_SIM_ACCESS_RSP,
    GSDI_ACTIVATE_ONCHIP_SIM_RSP,
    GSDI_GET_ALL_PIN_INFO_RSP,
    GSDI_INTERNAL_RSP,
    GSDI_PERSO_GET_DATA_RSP,
    GSDI_INTERNAL_PUP_INIT_RSP,
    GSDI_PERSO_LOCK_DOWN_RSP,
    GSDI_MAX_RSP = 0xFF
}gsdi_resp_type;

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* ----------------------------------------------------------------------
   ENUM  :   GSDI_PERSO_ENUM_TYPE
   DESCRIPTION:   Identifiers used for each personalization feature.
   --------------------------------------------------------------------------*/
typedef enum {
  GSDI_PERSO_NW = 0x0,      /* Network Personalization Feature              */
  GSDI_PERSO_NS,            /* Network Subset Personalization Feature       */
  GSDI_PERSO_SP,            /* Service Provider Personalization Feature     */
  GSDI_PERSO_CP,            /* Corporate Personalization Feature            */
  GSDI_PERSO_SIM,           /* SIM/USIM Personalization Feature             */
  GSDI_PERSO_PROP1,         /* Proprietary Lock 1                           */

  GSDI_PERSO_RUIM_NW1,      /* Network1 Personalization Feature             */
  GSDI_PERSO_RUIM_NW2,      /* Network2 Personalization Feature             */
  GSDI_PERSO_RUIM_HRPD,     /* HRPD Personalization Feature                 */
  GSDI_PERSO_RUIM_SP,       /* Service Provider Personalization Feature     */
  GSDI_PERSO_RUIM_CP,       /* Corporate Personalization Feature            */
  GSDI_PERSO_RUIM_RUIM,     /* RUIM Personalization Feature                 */

  GSDI_PERSO_NONE           /* No personalization indication feature        */
}gsdi_perso_enum_type;

/*----------------------------------------------------------------------------
** GSDI_PERSO_LOCK_DOWN_ENUM_TYPE
** Identifiers used to lock down perso commands.
** -------------------------------------------------------------------------*/
typedef enum
{
  GSDI_PERSO_LOCK_DOWN_GET_FEATURE_KEY = 0x00000000,
  GSDI_PERSO_LOCK_DOWN_MAX             = 0x7FFFFFFF
}gsdi_perso_lock_down_enum_type;

/* ----------------------------------------------------------------------
   ENUM  :   GSDI_PERSO_SELF_PERSO_STATE_ENUM_TYPE
   DESCRIPTION:   These are substates of the GSDI_PERSO_NONE ENUM in the
                  GSDI_PERSO_INIT_STATE.
   These substates are used to self personalize each feature
   --------------------------------------------------------------------------*/
typedef enum {
  GSDI_PERSO_SELF_INIT_STATE          = 0x00,
  GSDI_PERSO_DEACTIVATE_STATE         = 0x01,
  GSDI_PERSO_WAIT_FOR_DEACT_STATE     = 0x02,
  GSDI_PERSO_SAVE_SIM_TO_FS_STATE     = 0x03,
  GSDI_PERSO_ACTIVATE_STATE           = 0x04,
  GSDI_PERSO_WAIT_FOR_ACT_STATE       = 0x05,
  GSDI_PERSO_DONE_STATE               = 0x06,
  GSDI_PERSO_ERROR_STATE              = 0x07
}gsdi_perso_self_perso_state_enum_type;

/* Max Length Allowed for IMEI */
#define GSDI_PERSO_MAX_IMEI_LEN                           0x0F

#define GSDI_PERSO_MAX_KEY_IMEI_LEN     (GSDI_PERSO_MAX_IMEI_LEN+1+GSDI_PERSO_MAX_CK)

/* --------------------------------------------------------------------------
   MAX NUMBER OF REGISTERED TASKS THAT
   CAN RECEIVE PERSONALIZATION EVENTS
   ------------------------------------------------------------------------*/
#define GSDI_PERSO_MAX_NUM_REG_TASKS      0x000A

/* --------------------------------------------------------------------------
   MAX NUMBER OF BYTES ALLOWED FOR THE OTA DEPERSONALIZATION KEYS
   ------------------------------------------------------------------------*/
#define GSDI_PERSO_MAX_OTA_DEPERSO_BYTES  0x64

/* --------------------------------------------------------------------------
   DATA / BUFFER LENGTHS
   Max number of bytes allowed in a the data buffer
   ------------------------------------------------------------------------*/
#define GSDI_PERSO_MAX_DATA_LEN           0x0100

/* --------------------------------------------------------------------------
   DATA / BUFFER LENGTHS
   Max number of retries_allowed
   ------------------------------------------------------------------------*/
#define GSDI_PERSO_MAX_CK_RETRIES           0x10

/* --------------------------------------------------------------------------
** GSDI_PERSO_EVENT_ENUM_TYPE
** Events Identifiers sent to registered clients.
** ------------------------------------------------------------------------*/
typedef enum
{
  GSDI_PERSO_NO_EVENT = 0x0, /* PERSO Event Init Value                      */
  GSDI_PERSO_NW_FAILURE,     /* NW Personalization Init Failure             */
  GSDI_PERSO_NS_FAILURE,     /* NS Personalization Init Failure             */
  GSDI_PERSO_SP_FAILURE,     /* PS Personalization Init Failure             */
  GSDI_PERSO_CP_FAILURE,     /* CP Personalization Init Failure             */
  GSDI_PERSO_SIM_FAILURE,    /* SIM/USIM Personalization Deactivated        */
  GSDI_PERSO_NW_DEACTIVATED, /* NW Personalization Deactivated              */
  GSDI_PERSO_NS_DEACTIVATED, /* NS Personalization Deactivated              */
  GSDI_PERSO_SP_DEACTIVATED, /* PS Personalization Deactivated              */
  GSDI_PERSO_CP_DEACTIVATED, /* CP Personalization Deactivated              */
  GSDI_PERSO_SIM_DEACTIVATED,/* SIM/USIM Personalization Deactivated        */
  GSDI_PERSO_NCK_BLOCKED,    /* Network Control Key was Blocked             */
  GSDI_PERSO_NSK_BLOCKED,    /* Network Subset Control Key was Blocked      */
  GSDI_PERSO_SPK_BLOCKED,    /* Service Provider Control Key was Blocked    */
  GSDI_PERSO_CCK_BLOCKED,    /* Corporate Control Key was Blocked           */
  GSDI_PERSO_PPK_BLOCKED,    /* SIM/USIM Control Key was Blocked            */
  GSDI_PERSO_NCK_UNBLOCKED,    /* Network Control Key was Unblocked         */
  GSDI_PERSO_NSK_UNBLOCKED,    /* Network Subset Control Key was Unblocked  */
  GSDI_PERSO_SPK_UNBLOCKED,    /* Service Provider Control Key Was Unblocked*/
  GSDI_PERSO_CCK_UNBLOCKED,    /* Corporate Control Key was Unblocked       */
  GSDI_PERSO_PPK_UNBLOCKED,    /* SIM/USIM Control Key was Unblocked        */

  GSDI_PERSO_RUIM_NW1_FAILURE,    /* NW TYPE1 Personalization Init Failure  */
  GSDI_PERSO_RUIM_NW2_FAILURE,    /* NW TYPE2 Personalization Init Failure  */
  GSDI_PERSO_RUIM_HRPD_FAILURE,   /* HRPD Personalization Init Failure      */
  GSDI_PERSO_RUIM_SP_FAILURE,     /* PS Personalization Init Failure        */
  GSDI_PERSO_RUIM_CP_FAILURE,     /* CP Personalization Init Failure        */
  GSDI_PERSO_RUIM_RUIM_FAILURE,   /* RUIM Personalization Init Failure      */

  GSDI_PERSO_RUIM_NW1_DEACTIVATED,  /* NW TYPE1Personalization Deactivated  */
  GSDI_PERSO_RUIM_NW2_DEACTIVATED,  /* NW TYPE2 Personalization Deactivated */
  GSDI_PERSO_RUIM_HRPD_DEACTIVATED, /*HRPD Personalization Deactivated      */
  GSDI_PERSO_RUIM_SP_DEACTIVATED,   /* SP Personalization Deactivated       */
  GSDI_PERSO_RUIM_CP_DEACTIVATED,   /* CP Personalization Deactivated       */
  GSDI_PERSO_RUIM_RUIM_DEACTIVATED, /* RUIM Personalization Deactivated     */

  GSDI_PERSO_RUIM_NCK1_BLOCKED,    /* Network1 Control Key was Blocked      */
  GSDI_PERSO_RUIM_NCK2_BLOCKED,    /* Network2 Control Key was Blocked      */
  GSDI_PERSO_RUIM_HNCK_BLOCKED,    /* HRPD Control Key was Blocked          */
  GSDI_PERSO_RUIM_SPCK_BLOCKED,    /* Service Provider Control Key was Blocked*/
  GSDI_PERSO_RUIM_CCK_BLOCKED,     /* Corporate Control Key was Blocked       */
  GSDI_PERSO_RUIM_PCK_BLOCKED,     /* RUIM Control Key was Blocked            */

  GSDI_PERSO_RUIM_NCK1_UNBLOCKED,  /* Network1 Control Key was Unblocked        */
  GSDI_PERSO_RUIM_NCK2_UNBLOCKED,  /* Network2 Control Key was Unblocked        */
  GSDI_PERSO_RUIM_HNCK_UNBLOCKED,  /* HRPD Control Key was Unblocked            */
  GSDI_PERSO_RUIM_SPCK_UNBLOCKED,  /* Service Provider Control Key Was Unblocked*/
  GSDI_PERSO_RUIM_CCK_UNBLOCKED,   /* Corporate Control Key was Unblocked       */
  GSDI_PERSO_RUIM_PCK_UNBLOCKED,   /* RUIM Control Key was Unblocked            */

  GSDI_PERSO_SANITY_ERROR,         /* Files required when created are not avail   */
  GSDI_PERSO_EVT_INIT_COMPLETED
}gsdi_perso_event_enum_type;
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

/* ----------------------------------------------------------------------------

               GSDI   REQ   COMMAND   STRUCTURES

   --------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------
   ENUM  :   GSDI_PIN_STATUS_T
   DESCRIPTION:   PIN Status
   --------------------------------------------------------------------------*/
typedef enum {
    GSDI_PIN_NOT_INITIALISED = 0,
    GSDI_PIN_DISABLED,
    GSDI_PIN_ENABLED,
    GSDI_PIN_BLOCKED,
    GSDI_PIN_PERM_BLOCKED,

    GSDI_MAX_PIN_STATUS
}gsdi_pin_status_T;

#define GSDI_MAX_DATA_BLOCK_LEN         256
#define GSDI_IMSI_NOT_PROVISIONED  0xFF

#define GSDI_MIN_VALID_IMSI_LEN       3
#define GSDI_MAX_VALID_IMSI_LEN       9

#define SST_PIN_DISABLING_OFFSET     0x00 /* 1st byte in SST */
#define SST_PIN_DISABLING_MASK       0x03 /* 0000 0011 */

#define CST_PIN_DISABLING_OFFSET     0x00
#define CST_PIN_DISABLING_MASK       0x03

#define MMGSDI_RAND_LEN                   16

/* ----------------------------------------------------------------------
   GSDI_RSPHDR_T
   General response message header.
   --------------------------------------------------------------------------*/
typedef struct {
    byte               message_set;
    byte               message_id;
    byte               message_len_lsb;
    byte               message_len_msb;
    uint32             wait_sig;
    gsdi_returns_T     gsdi_status;   /* Status of Command              */
    uint32             client_ref;    /* User Data assigned by client   */
    gsdi_resp_type     resp_type;     /* Type of Response               */
    gsdi_slot_id_type  slot;          /* Slot for which rsp applies     */
}
gsdi_rsphdr_T;

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_REG_TASK_CNT_T
   DESCRIPTION: Confirmation / Response for the
                GSDI_PERSO_REG_TASK_REQ_T
   Request/Command.  Indicates whether or not the task was
   successfully registered.
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
}gsdi_perso_reg_task_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_ACT_FEATURE_IND_CNF_T
   DESCRIPTION: Confirmation / Response for the
                GSDI_PERSO_ACT_FEATURE_IND_REQ_T
   Request / Command.  Indicates whether or not the Personalization
   Feature was properly activated
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    gsdi_perso_enum_type        perso_feature;  /* Feature operated on       */
    uint32                      num_retries;      /* Num bytes for CK          */
}gsdi_perso_act_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_FEATURE_IND_CNF_T
   DESCRIPTION: Confirmation / Response for the GSDI_PERSO_GET_FEATURE_IND_REQ_T
   Request / Command.  Will return the current status of all
   Personalization Features.
   NW Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   NS Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   SP Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   CP Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   SIM Indicator -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    boolean                     nw_ind_status;  /* Network Indicator Status  */
    boolean                     ns_ind_status;  /* NW Subset Indicator Status*/
    boolean                     sp_ind_status;  /* Servic Provider Ind Status*/
    boolean                     cp_ind_status;  /* Corporate Ind Status      */
    boolean                     sim_ind_status; /* SIM Indicator Status      */

    boolean                     ruim_nw1_ind_status;  /* Network Indicator Status  */
    boolean                     ruim_nw2_ind_status;  /* NW Subset Indicator Status*/
    boolean                     ruim_hrpd_ind_status;  /* NW Subset Indicator Status*/
    boolean                     ruim_sp_ind_status;  /* Servic Provider Ind Status*/
    boolean                     ruim_cp_ind_status;  /* Corporate Ind Status      */
    boolean                     ruim_ruim_ind_status; /* SIM Indicator Status      */
} gsdi_perso_get_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_PERSO_DACT_FEATURE_IND_CNF_T
   DESCRIPTION:  Confirmation / Response for the GSDI_PERSO_DEACT_FEATURE_IND_REQ_T
   Request / Command.  Will return whether or not the depersonalization
   deactivation request was successful or not.  It will also indicate the
   number of retries remaining.
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    gsdi_perso_enum_type        perso_feature;  /* Feature operated on       */
    uint32                      num_retries;    /* Number of retries left    */
}gsdi_perso_deact_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_SET_FEATURE_DATA_CNF_T
   DESCRIPTION:  Confirmation / Response for the GSDI_PERSO_SET_FEATURE_DATA_REQ_T
   Request / Command.  Will return whether or not the setting of the
   personalization data was successful or not.
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T     message_header;
}gsdi_perso_set_feature_data_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_FEATURE_DATA_CNF_T
   DESCRIPTION:  Confirmation / Response for the GSDI_PERSO_GET_FEATURE_DATA_REQ_T
   Request / Command.  Will return whether or not get of
   personalization data was successful or not.
   --------------------------------------------------------------------------*/
typedef struct {
     gsdi_rsphdr_T     message_header;
     int32             num_bytes;     /* Number of bytes to follow */
     uint8 *           data_buffer_ptr;   /* Personalization Data*/
}gsdi_perso_get_feature_data_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_OTA_DEPERSO_CNF_CNF_T
   DESCRIPTION:  Confirmation / Response for the GSDI_PERSO_OTA_DEPERSO_REQ__T
   Request / Command.  Will return whether or not get of
   personalization data was successful or not.
   --------------------------------------------------------------------------*/
typedef struct {
     gsdi_rsphdr_T               message_header;/* Message Header            */
     int32                       num_bytes;     /* Number of bytes to follow */
                                                /* 20 Byte return expected   */
     uint8 *                     data_buffer;   /* Personalization Indicators*/

}gsdi_perso_ota_deperso_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_INT_INIT_PROCEDURE_CNF_T
   DESCRIPTION:  This structure is used as a confirmation for the request
            gsdi_perso_int_init_procedure_req_T.  The feature returned is used
            to base which personalization check to perform next.
   --------------------------------------------------------------------------*/
typedef struct {
     gsdi_rsphdr_T                         message_header;/* Message Header      */
     gsdi_perso_enum_type                  feature;       /* Feature for Conf    */
     gsdi_perso_self_perso_state_enum_type int_init_s;    /* Internal Init State */
     gsdi_perso_enum_type                  int_init_feature; /* Feature to self perso */
}gsdi_perso_int_init_procedure_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_UNBLOCK_FEATURE_IND_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ_T
   Request / Command.  Indicates whether or not the Personalization
   Feature was properly unblock
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    gsdi_perso_enum_type        perso_feature;  /* Feature operated on       */
    uint32                      num_retries;    /* Num bytes for CK          */
}gsdi_perso_unblock_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_DISABLE_FEATURE_IND_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_DISABLE_FEATURE_IND_REQ_T
   Request / Command.  Indicates whether or not the Personalization
   Feature was properly unblock
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    gsdi_perso_enum_type        perso_feature;  /* Feature operated on       */
    uint32                      num_retries;    /* Num bytes for CK          */
}gsdi_perso_perm_disable_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_PERM_FEATURE_IND_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_GET_PERM_FEATURE_IND_REQ_T
   Request / Command.  Will return the current status of all
   Personalization Features.
   NW Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   NS Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   SP Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   CP Indicator  -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   SIM Indicator -> TRUE / FALSE (ACTIVE / NOT ACTIVE)
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    boolean                     perm_nw_ind_status;  /* Network Indicator Status  */
    boolean                     perm_ns_ind_status;  /* NW Subset Indicator Status*/
    boolean                     perm_sp_ind_status;  /* Servic Provider Ind Status*/
    boolean                     perm_cp_ind_status;  /* Corporate Ind Status      */
    boolean                     perm_sim_ind_status; /* SIM Indicator Status      */

    boolean                     perm_ruim_nw1_ind_status;  /* Network Indicator Status  */
    boolean                     perm_ruim_nw2_ind_status;  /* NW Subset Indicator Status*/
    boolean                     perm_ruim_hrpd_ind_status;  /* NW Subset Indicator Status*/
    boolean                     perm_ruim_sp_ind_status;  /* Servic Provider Ind Status*/
    boolean                     perm_ruim_cp_ind_status;  /* Corporate Ind Status      */
    boolean                     perm_ruim_ruim_ind_status; /* SIM Indicator Status      */

} gsdi_perso_get_perm_feature_ind_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_FEATURE_KEY_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_GET_FEATURE_KEY_REQ_T
   Request / Command.  Will return the control key for the feature
   --------------------------------------------------------------------------*/
typedef struct {
    gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
    gsdi_perso_enum_type        feature;        /* Feature operated on       */
    int32                       key_len;
    uint8                       key[GSDI_PERSO_MAX_KEY_IMEI_LEN];
} gsdi_perso_get_feature_key_cnf_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_LOCK_DOWN_CNF_T
   Confirmation / Response for the GSDI_PERSO_LOCK_DOWN_REQ
   Request / Command.  Will return the locked down command
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_rsphdr_T                   message_header; /* Rsp Message Header   */
    gsdi_perso_lock_down_enum_type  locked_command; /* Locked command       */
} gsdi_perso_lock_down_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_DCK_NUM_RETRIES_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_GET_NUM_RETRIES_REQ_T
   Request / Command.  This will get the number of incorrect DCK
   Verifications are still remaining.
   --------------------------------------------------------------------------*/
typedef struct {
  gsdi_rsphdr_T               message_header; /* Rsp Message Header        */

  int32                       nw_num_retries;
  int32                       ns_num_retries;
  int32                       sp_num_retries;
  int32                       cp_num_retries;
  int32                       sim_num_retries;

  int32                       ruim_nw1_num_retries;
  int32                       ruim_nw2_num_retries;
  int32                       ruim_hrpd_num_retries;
  int32                       ruim_sp_num_retries;
  int32                       ruim_cp_num_retries;
  int32                       ruim_ruim_num_retries;
}gsdi_perso_get_dck_num_retries_cnf_T;

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_CNF_T
   DESCRIPTION:  Confirmation / Response for the
                 GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ_T.  This will
   get the number of incorrect DCK UNBLOCK Verifications still remaining.
   --------------------------------------------------------------------------*/
typedef struct {
  gsdi_rsphdr_T               message_header; /* Rsp Message Header        */

  int32                       unblock_nw_num_retries;
  int32                       unblock_ns_num_retries;
  int32                       unblock_sp_num_retries;
  int32                       unblock_cp_num_retries;
  int32                       unblock_sim_num_retries;

  int32                       ruim_unblock_nw1_num_retries;
  int32                       ruim_unblock_nw2_num_retries;
  int32                       ruim_unblock_hrpd_num_retries;
  int32                       ruim_unblock_sp_num_retries;
  int32                       ruim_unblock_cp_num_retries;
  int32                       ruim_unblock_ruim_num_retries;
}gsdi_perso_get_dck_unblock_num_retries_cnf_T;
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

/* ---------------------------------------------------------------------------
   STRUCTURE:   GSDI_INTERNAL_PUP_INIT_CNF_T

   DESCRIPTION: Confirmation / Response for the
                GSDI_INTERNAL_PUP_INIT_REQ.
   -------------------------------------------------------------------------*/
typedef struct
{
  gsdi_rsphdr_T               message_header; /* Rsp Message Header        */
  uint64                      session_id;
  uint32                      curr_card_state;
  uint32                      curr_app_state;
}gsdi_internal_pup_init_cnf_T;

typedef union{
    gsdi_rsphdr_T                                message_header;
#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
    gsdi_perso_reg_task_cnf_T                    reg_task_cnf;       /* Cnf for Register Req     */
    gsdi_perso_act_feature_ind_cnf_T             act_ind_cnf;        /* Cnf for Act Ind Req      */
    gsdi_perso_get_feature_ind_cnf_T             get_ind_cnf;        /* Cnf for Get Ind Req      */
    gsdi_perso_deact_feature_ind_cnf_T           dact_ind_cnf;       /* Cnf for D-Act Ind Req    */
    gsdi_perso_set_feature_data_cnf_T            set_data_cnf;       /* Cnf for Set Data Req     */
    gsdi_perso_get_feature_data_cnf_T            get_data_cnf;       /* Cnf for Get Data Req     */
    gsdi_perso_ota_deperso_cnf_T                 ota_cnf;            /* Cnf for OTA Deperso Req  */
    gsdi_perso_int_init_procedure_cnf_T          int_init_cnf;       /* Cnf for Inter Init Procs */
    gsdi_perso_unblock_feature_ind_cnf_T         unblock_ind_cnf;    /* Cnf for Unblock Ind Req */
    gsdi_perso_perm_disable_feature_ind_cnf_T    disable_ind_cnf;    /* Cnf for Disable Ind Req */
    gsdi_perso_get_perm_feature_ind_cnf_T        get_perm_ind_cnf;   /* Cnf for Perm Feature Ind Req */
    gsdi_perso_get_feature_key_cnf_T             get_ind_key;        /* Cnf for get key Req */
    gsdi_perso_lock_down_cnf_T                   lock_down_cnf;      /* Cnf foe lock down Req */
    gsdi_perso_get_dck_num_retries_cnf_T         dck_num_retries_cnf;/* Cnf for get Num Retries */
    gsdi_perso_get_dck_unblock_num_retries_cnf_T dck_unblock_num_retries_cnf;/* Cnf for get Num Retries */
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */
    gsdi_internal_pup_init_cnf_T                 internal_pup_init_cnf;
 }
gsdi_cnf_T;

typedef struct {
    byte               message_set;
    byte               message_id;
    byte               message_len_lsb;
    byte               message_len_msb;
    rex_tcb_type       *tcb_ptr;
    rex_sigs_type      wait_sig;
    rex_crit_sect_type *crit_sec;
    void              (*gsdi_async_callback)(gsdi_cnf_T*);
}gsdi_cmdhdr_T;

#ifdef FEATURE_MMGSDI_PERSONALIZATION_ENGINE
/*---------------------------------------------------------------------------
** GSDI_PERSO_ADDITIONAL_INFORMATION
** Structure Used to Hold Additional Information for
** the generated events.
** -------------------------------------------------------------------------*/
typedef struct
{
  int32             num_bytes;          /* Num bytes in additional info     */
  uint8 *           buffer_p;           /* Pointer to additional info       */
}gsdi_perso_additional_info_type;

/* ----------------------------------------------------------------------
   CALLBACK :   GSDI_PERSO_EVENTS_CB
   DESCRIPTION: Structure used to contain the callback for
                Personalization Events.
   --------------------------------------------------------------------------*/
typedef void (*gsdi_perso_events_cb)
(
  gsdi_perso_event_enum_type event,             /* Personalization Event  */
  gsdi_slot_id_type          slot,              /* Slot id for perso event*/
  boolean additional_info_avail,                /* Indicator to check the
                                                   additional info sturct */
  gsdi_perso_additional_info_type * info        /* Will contain data for
                                                     the event generated    */
);
/* ----------------------------------------------------------------------------
   GSDI_PERSO_REG_TASK_REQ_T
   Command / Request structure used to register a task
   with the GSDI Personalization / Depersonalziation Engines.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_events_cb        reg_task_cb_p;  /* Pointer to task cb        */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_reg_task_req_T;
/* ----------------------------------------------------------------------------
   GSDI_PERSO_DEACT_FEATURE_IND_REQ_T
   Command / Request structure used to deactivate a
   Personalization Feature Indicator (i.e. set the
   Indicator to off so that Personalziation does occurs at
   power up)
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        feature;        /* Perso Feature affected    */
    int32                       num_bytes;      /* Num bytes for CK          */
    uint8                       ck_buffer[GSDI_PERSO_MAX_CK]; /* Buffer to
                                                   CK String                 */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_deact_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_ACT_FEATURE_IND_REQ_T
   Command / Request structure used to activate a
   Personalization Feature Indicator (i.e. set the
   Indicator to on so that Personalziation occurs at
   power up)
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        feature;  /* Perso Feature affected    */
    int32                       num_bytes;      /* Num bytes for CK          */
    uint8                       ck_buffer[GSDI_PERSO_MAX_CK]; /* Buffer to
                                                   CK String                 */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_act_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_GET_FEATURE_IND_REQ_T
   Command / Request structure used retrieve the
   status of all personalization indicators
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_get_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_LOCK_DOWN_REQ_T
   Command / Request structure used lock down personalization commands
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T                  message_header; /* Request Message Header    */
    gsdi_perso_lock_down_enum_type locked_command; /* Perso command affected    */
    uint32                         client_ref;     /* Data returned to caller   */
    uint8                          queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_lock_down_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_DACT_FEATURE_IND_REQ_T
   Command / Request structure used to deactivate a
   Personalization Feature Indicator (i.e. set the
   Indicator to "off" so that Personalziation does not
   occur at power up)
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Headder   */
    gsdi_perso_enum_type        perso_feature;  /* Perso Feature affected    */
    int32                       num_bytes;      /* Number of bytes for CK    */
    uint8                       ck_buffer[GSDI_PERSO_MAX_CK];
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_dact_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_OTA_DEPERSO_DATA_REQ_T
   Command / Request structure used to send the Depersonalization
   Control Keys from an SMS PP Message to the Depersonalization
   Engine.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    int32                       num_bytes;      /* Num Bytes of OTA Data     */
    uint8                       data_buffer[GSDI_PERSO_MAX_OTA_DEPERSO_BYTES];
                                                /* Buffer IMEI and CKs       */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_ota_deperso_req_T;

/* ----------------------------------------------------------------------------
   GSDI_PERSO_OTA_DEPERSO_DATA_REQ_T
   Command / Request structure used to send the Depersonalization
   Control Keys from an SMS PP Message to the Depersonalization
   Engine.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        feature;        /* Internal Init for feature */
    gsdi_perso_self_perso_state_enum_type self_perso_state; /* Self Perso S  */
    gsdi_perso_enum_type        int_init_feature;/* Feature to personalize    */
    boolean                     deactivated;    /* Notify self if deactivated*/
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_int_init_procedure_req_T;

/* ----------------------------------------------------------------------------
   STRUCT:  gsdi_perso_internal_command_req_T
            This structure is used to queue up an internal Peronsalization
            command.  There is not a confirmation associated with this
            command.
   --------------------------------------------------------------------------*/
typedef struct {
 gsdi_cmdhdr_T               message_header;   /* Request Message Header  */
 mmgsdi_session_id_type      session_id;
 gsdi_perso_enum_type        feature;          /* Feature for Conf        */
 uint32                      active_lock_mask; /* Mask of Active Locks    */
 uint32                      autolock_mask;    /* Mask of Auto Locks      */
 uint32                      client_ref;       /* Data returned to caller   */
 uint8                       queue_id;         /* Legacy Queue ID Used      */
}gsdi_perso_internal_command_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_SET_FEATURE_DATA_REQ_T
   DESCRIPTION: Command / Request structure used to Personalize
   or store the codes for the particular personalization
   feature.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        feature;        /* Perso Feature affected    */
    int32                       num_bytes;      /* Num Bytes of OTA Data     */
    uint8                       data_buffer[GSDI_PERSO_MAX_DATA_LEN];
                                                /* Buffer with perso codes   */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_set_feature_data_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_FEATURE_DATA_REQ_T
   DESCRIPTION: Command / Request structure used to Personalize
   or store the codes for the particular personalization
   feature.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header       */
    gsdi_perso_enum_type        feature;        /* Perso Feature to be accessed */
    uint32                      client_ref;     /* Data returned to caller      */
    uint8                       queue_id;       /* Legacy Queue ID Used         */
}gsdi_perso_get_feature_data_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_DCK_NUM_RETRIES_REQ_T
   DESCRIPTION: Command / Request structure used to retrieve DCK number of
   retries available
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_get_dcks_num_retries_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_DCK_UNBLOCK_NUM_RETRIES_REQ_T
   DESCRIPTION: Command / Request structure used to retrieve unblock DCK
   number of retries available
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_get_dck_unblock_num_retries_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_PERM_DISBALE_FEATURE_IND_REQ_T
   DESCRIPTION: Command / Request structure used retrieve the
   status of all personalization indicators
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_get_perm_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_PERM_DISABLE_FEATURE_REQ_T
   DESCRIPTION: Command / Request structure used to permanently disable the
                personalization feature when turned off.
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        perso_feature;  /* Perso Feature affected    */
    int32                       num_bytes;      /* Num Bytes of OTA Data     */
    uint8                       ck_buffer[GSDI_PERSO_MAX_CK];
                                                /* Buffer with perso codes   */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_perm_disabled_feature_req_T;


/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_UNBLOCK_FEATURE_IND_REQ_T
   DESCRIPTION: Command / Request structure used to unblock a blocked
   Personalization Feature Indicator
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    gsdi_perso_enum_type        feature;        /* Perso Feature affected    */
    int32                       num_bytes;      /* Num bytes for CK          */
    uint8                       unblock_ck_buffer[GSDI_PERSO_MAX_CK];
                                                /* Buffer to CK String       */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
}gsdi_perso_unblock_feature_ind_req_T;

/* ----------------------------------------------------------------------------
   STRUCTURE:   GSDI_PERSO_GET_FEATURE_KEY_REQ_T
   DESCRIPTION: Command / Request structure used retrieve the
   key of the personalization level
   --------------------------------------------------------------------------*/
typedef struct
{
    gsdi_cmdhdr_T               message_header; /* Request Message Header    */
    boolean                     is_unblock_key; /* If we are retriving
                                                   unblock key or not        */
    gsdi_perso_enum_type        feature;        /* Perso Feature affected    */
    uint32                      client_ref;     /* Data returned to caller   */
    uint8                       queue_id;       /* Legacy Queue ID Used      */
} gsdi_perso_get_feature_key_req_T;
#endif /* FEATURE_MMGSDI_PERSONALIZATION_ENGINE */

/* GSDI Status that indicates whether
** or Not a Slot is present on the Card
*/
/*Set by UIM */
typedef enum{
    GSDI_SLOT_ERROR = 0,
    GSDI_SLOT_PRESENT = 1
}gsdi_slot_status_type;


/* ----------------------------------------------------------------------
   STRUCTURE  :   PHONE_BOOK_T
   DESCRIPTION:   Structure defining the entries in phonebook
   --------------------------------------------------------------------------*/
typedef struct {
  boolean  present;
  boolean  multiple_pbr_entries;
  boolean  status_unknown;
}
phone_book_T;

/*===========================================================================

                       FUNCTION DECLARATIONS

===========================================================================*/
/*===========================================================================
FUNCTION GSDIMAIN

DESCRIPTION
  Entry point for the MM task.  This function performs task initialization,
  then sits in an infinite loop, waiting on an input queue, and responding
  to messages received.

DEPENDENCIES
  None

RETURN VALUE
  None
===========================================================================*/
void gsdimain (
  dword dummy
);

#endif /* GSDI_P_H */
