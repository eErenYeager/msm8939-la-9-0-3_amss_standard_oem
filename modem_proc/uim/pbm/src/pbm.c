/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


                          PHONEBOOK MANAGER FUNCTIONS

GENERAL DESCRIPTION
  This file contains the PBM write and init functions.

  Copyright (c) 2002 - 2015 by QUALCOMM Technologies, Inc(QTI).
  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/pbm/src/pbm.c#3 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/05/15   stv     Feature to maintain sim absent hardcoded ecc list till pin is verified
07/20/15   nr      Enter & leave critical section inside pbm_ecc_set_hardcoded_eccs()
10/06/14   NR      AAS sim field info is not getting copied during boot up 
                   for all sessions except card session.
09/30/14   stv     Checking emergency mode and service category as well while building 
                   hardcoded ecc cache
07/29/14   am      Correctly direct/redirect atomic_ops APIs across MOBs 
07/25/14   am      Correct atomic APIs linkage
05/01/14   NR      FR 19033: Reduce Code Duplication pbm
02/25/14   stv     CM SS event handling changes for SGLTE 
02/10/14   stv     Potential de-referencing of NULL pointer without checking for it 
01/09/14   kk      Fix compilation error for SBM MOB
01/09/14   NR      potential memory leak issues in PBM funcions
01/07/13   NR      Pointer Null check is missing for the pointer ecc_cache returned 
                   from pbm_device_id_to_cache () in function pbm_ecc_clear_all_id ()
12/19/13   NR      PBM: LLVM compiler warnings
09/17/10   krishna Clear NW ECC no's when the corresponding emergency list len is 0
05/10/10   krishna provided the clients Session based field info
03/18/10   krishna Used pbmtask_i.h instead if pbmtask.h as part of CMI activity
03/18/10   krishna Resolved the compilation errors in WM7
07/10/09  ashwanik Support for CSIM and Dual Sim Features
05/04/09   krishna changes for CR 167785
12/29/08   pvphani Adding Null pointer check in pbm_list_recover function. Fix for CR: 164527
03/04/08   clm     Add logic to revert records containing USIM only fields upon
                   failure to add, delete, or update.
01/17/08   clm     Add FEATURE_PBM_USIM_SUPPORT to featurize USIM features.
01/16/07   cvs     Add event for phonebooks becoming ready
12/06/06   cvs     Fix double read of files after full card refresh
12/06/06   cvs     Remove banned APIs
10/10/06   cvs     Move messages to PBM's diag msg ID
10/03/06   cvs     Add support for MBN
07/12/06   cvs     Fix compile warnings
05/03/06   cvs     convert to MMGSDI APIs.  Lint cleanup.
04/10/06   cvs     add debug feature to ignore WMS init
03/24/06   cvs     %x -> 0x%x in log messages
02/28/06   cvs     Add support for MBDN
02/23/06   cvs     rework memory allocation macros
01/19/06   cvs     Emergency number cleanup, fix bug in network emerg nums
01/09/06   cvs     Lint fixes
01/04/06   cvs     Fix compile error without GSDI
12/14/05   cvs     Better log messages
12/06/05   cvs     Wait to init sim phonebooks until after WMS
11/15/05   cvs     Lint fixes
11/10/05   cvs     Add network ECC numbers
09/29/05   cvs     Fix usage of PBM_TEXT_SIZE and PBM_MAX_TEXT_LENGTH
08/22/05   AT      Moved contents inside of a FEATURE_PBM_TASK.
08/12/05   AT      Added UCS2 support for strings.
08/04/05   cvs     Check for committed list growth
08/01/05   cvs     Code review comments
07/29/05   AT      Added additional range checking.
07/06/05   cvs     Add 999 to emergency list if CPHS is enabled.
06/14/05   cvs     Fix GCF failures related to emergency numbers
05/26/05   cvs     support for API changes to make APIs more multiprocessor friendly
05/23/05   cvs     Fix off-by-one error in loading ECC list
04/29/05   cvs     Fix Error Handling for USIM failures
03/31/05   AT      Created feature FEATURE_CPHS_UK to hardcode the
                   emergency number 999 in all cases.  This feature
                   causes a GCF test case to fail, but meets the CPHS
                   requirement.  Note these requirements are in conflict.
                   Also added featurization for non_MMGSDI builds.
03/28/05   AT      Updated ECC requirements.
03/08/05   PA      Support for PBM notifications
01/20/05   PA      Added support for read lock, non-blocking write lock and
                   support for MSISDN.
01/24/05   PA      Changed NV_EMERGENCY_NUMBER_START to
                   PBM_NV_EMERGENCY_NUMBER_START
01/20/05   PA      Add only emergency numbers to ecc_cache.
01/12/05   PA      PBM USIM Support
12/09/04   PA      Set cache status to ready after reading NV items
12/03/04   PA      Fix category used in mirroring
12/02/04   PA      Add reference when new record in added to committed list.
11/19/04   PA      Support non-GSDI builds
11/18/04   PA      Support for write lock
09/29/04   PA      Initial Revision
===========================================================================*/
#include "uim_variation.h"
#include "customer.h"
#include "pbm.h"

#include "pbm_common_types.h"

#include "pbmefs.h"
#include "pbm_conversion.h"
#include "pbmtask_i.h"
#include "pbmlib.h"
#include "pbmutils.h"
#include "pbmuim.h"
#include "pbmgsdi.h"

#include "cm.h"
#ifdef FEATURE_DUAL_SIM
#include "cm_dualsim.h"
#endif
#include "atomic_ops.h"
#include "stringl/stringl.h"

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif

/*===========================================================================
                              MACROS
===========================================================================*/
#define PBM_CONFIG_FILE_1      "/nv/item_files/pbm/pbm_ecc_nums"
#define PBM_CONFIG_FILE_2      "/nv/item_files/pbm/pbm_hardcoded_ecc_config"
#define PBM_CONFIG_FILE_3      "/nv/item_files/pbm/pbm_hardcoded_ecc_list"
#define PBM_CONFIG_FILE_4      "/nv/item_files/pbm/pbm_sim_ecc_airplane"
#define PBM_CONFIG_FILE_5      "/nv/item_files/pbm/pbm_nv_ecc_config"
#define PBM_CONFIG_FILE_6      "/nv/item_files/pbm/pbm_efs_support_disable"
#define PBM_FEATURES_STATUS_LIST_FILE "/nv/item_files/pbm/features_status_list"
#define PBM_CONF_DIR1          "/nv"
#define PBM_CONF_DIR2          "/nv/item_files"
#define PBM_CONF_DIR3          "/nv/item_files/conf"
#define PBM_CONF_FILE          "/nv/item_files/conf/pbm.conf"


/*--------------------------------------------------------
  To store list of status of pbm features
----------------------------------------------------------*/
pbm_features_status_list_type pbm_features_status_list;

/*===========================================================================
FUNCTION PBM_NV_FEATURES_STATUS_LIST_UPDATE_VERSION

DESCRIPTION
  Function used to validate the PBM Features Status NVITEM's version.
  If what's in the NV is less than the one currently defined in the build,
  updates it to the current version defined and writes it back to the NV.
  It also writes back to the NV the default values of the delta items in
  in the list.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Version gets updated in the NV if it doesn't match the one in the build. If
  the version is updated, next time when the user reads the NVITEM, a
  dropdown of items corresponding to the updated version is shown instead of
  what the user saw the first time the NV read was done (corresponding to
  the version present in the NV at that point).
===========================================================================*/
static void pbm_nv_features_status_list_update_version(void);

/*===========================================================================
FUNCTION PBM_NV_INIT_FEATURES_STATUS_NV_LOOKUP

DESCRIPTION
  Function used to get the status of PBM features as defined by the NVITEM
  "PBM_FEATURES_STATUS_LIST" (71535).
  The global, "features_status_list", used to hold the NVITEM, is expandable,
  yet the size is fixed. See the struct definition for more detailed doc on
  that. The global struct has a "version" item that is updated everytime we
  add new items to this global struct (starting with version 0 in the first
  implementation)
  The pbm code has full control over version of NVITEM. Taking an example,
  if in future, in version 2 of this struct(that has 10 items), the delta is 3
  items (from version 0 that had 7 items), and this build is loaded onto the
  device that already had the items set using version 0 of this NVITEM, then,
  on boot-up after loading the build, pbm will validate the version it read
  from NV. If it is not 2, it will update it to 2 and write it back to NV. At
  this point, however, the value of features/items 8 through 10 will be set to
  their default values by pbm code.

DEPENDENCIES
  Minimum QXDM version required to be able to set this NVITEM (71535) is
  QXDM 3.14.757

RETURN VALUE
  None

SIDE EFFECTS
  "version" item in this NVITEM is writable through QXDM (no QXDM support to
  hide it or grey it out) or QCN. However if what is written explicitly doesn't
  match what's in the build, pbm will change it to what's in the build and
  it could be annoying for the QXDM user to see it change to something else
  (when the user reads the NVITEM next time).
===========================================================================*/
static void pbm_nv_init_features_status_nv_lookup(void);

/*===========================================================================
FUNCTION PBM_NV_WRITE_DEFAULT_VALUE_OF_DELTA_FEATURES

DESCRIPTION
  Function used to write the default values of PBM Features (controlled by
  PBM_Feature_Status_list NVITEM) that are delta between the two
  versions of Feature_Status_List to global features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void pbm_nv_write_default_value_of_delta_features(
  uint8 version_in_nv,
  uint8 version_in_build
);

/*===========================================================================
FUNCTION PBM_NV_WRITE_DEFAULT_VALUE_OF_ALL_FEATURES

DESCRIPTION
  Function used to write the default values of all the PBM Features
  (controlled by PBM_Feature_Status_list NVITEM), defined currently, to
  global pbm_features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void pbm_nv_write_default_value_of_all_features(void);

/*===========================================================================
FUNCTION PBM_NV_FEATURE_WRITE_DEFAULT_VALUE

DESCRIPTION
  Function used to write the default value of MMGSDI Feature (controlled by
  PBM_Feature_Status_list NVITEM) to global pbm_features_status_list.

DEPENDENCIES

RETURN VALUE
  pbm_return_type

SIDE EFFECTS
  None
===========================================================================*/
static pbm_return_type pbm_nv_feature_write_default_value(
  pbm_nv_features_enum_type nv_feature);

/*-------------------------------------------------------------------
  List of total number of PBM features present in NVITEM in each version
  of feature_status_list. Whenever a newer version of the NVITEM is exposed,
  size of the array grows by 1...So, if 8 features/items were added in VERSION0
  and 2 in VERSION1, the array should have 2 items listing the number of total
  features at each version.. {8,10} in this case...
---------------------------------------------------------------------*/
uint8  pbm_features_total_till_version[] =
  {PBM_FEATURES_STATUS_LIST_VERSION0_COUNT,
   PBM_FEATURES_STATUS_LIST_VERSION1_COUNT};

/*===========================================================================
                       Declare data for the ECC.
===========================================================================*/
uint32 ecc_has_entries = 0;


uint32 pbm_wms_init_finished = 0;

/* This variable is read from NV pbm_hardcode_ecc_config (NV 69736 ) to decide the
     source of hardcoded ecc list ( i.e. either from code or from NV 69737)*/
boolean                 pbm_hardcode_ecc_config = FALSE ;

rex_crit_sect_type pbm_crit_sect;

/* PBM Write Lock */
pbm_lock_s_type g_lock = {NULL, PBM_LOCK_NOT_INIT, FALSE};

/* List of notification functions registered with PBM */
pbm_notify_node_s_type *pbm_notify_list = NULL;

/* This list contains the last known good record for each pending write. This is
 * used in bringing the cache to a sane state when a write operation fails. If a
 * record does not exist in this list, the existing cache entry is implied to be
 * the last committed record.
 * */
pbm_addr_node_s_type *pbm_committed_list = NULL;
/* CM stuff */

static cm_client_id_type pbm_cm_client_id = (cm_client_id_type) -1;


static pbm_return_type pbm_ecc_init(void);

static pbm_return_type pbm_cm_init(void);

static pbm_return_type pbm_recover_usim_record(const pbm_write_record_s_type
                                                *write);

/*===========================================================================*
 *              PHONE BOOK MANAGER INITIALIZATION FUNCTIONS                  *
 *                                                                           *
 * These functions will initialize the phone book managers phone books       *
 *===========================================================================*/


/*===========================================================================
FUNCTION PBM_BUILD_ECC_RECORD

DESCRIPTION
  This function takes a name, number, and category and builds the appropriate
  record for PBM_ECC.  It then adds this record to the cache.

DEPENDENCIES
  None

SIDE EFFECTS
===========================================================================*/
pbm_return_type pbm_build_ecc_record(pbm_field_id_e_type field_id,
                                     uint32 data_length,
                                     const char *ecc_string,
                                     uint32 name_length,
                                     const uint16 *name_string,
                                     boolean usim_ecc_serv_cat_valid,
                                     uint8 usim_ecc_serv_cat,
                                     pbm_session_enum_type session_type,
                                     pbm_ecc_mode_type emergency_mode)
{
   pbm_phonebook_type pb_id = {PBM_GPB};
   uint32 number_of_fields = 0;
   uint32 field_length[4];
   int index = 0;
   pbm_addr_cache_s_type *record;
   PB_CACHE *ecc_cache = NULL;
   uint16 location;

   switch (field_id)
   {
      case PBM_FIELD_HARDCODED_ECC:
      case PBM_FIELD_NV_ECC:
         pb_id.device_type = PBM_ECC_OTHER;
         ecc_cache = pbm_pb_id_to_cache (pb_id);
         break;
      case PBM_FIELD_NETWORK_ECC:
        if(PBM_LPB == pbm_session_type_to_pb_category(session_type))
        {
         ecc_cache = pbm_file_id_to_cache(session_type, PBM_FILE_ECC);
        }
        else //the else case would be applicable for NV only builds
        {
         pb_id.device_type = PBM_ECC_OTHER;
         ecc_cache = pbm_pb_id_to_cache (pb_id);
        }
        break;
      case PBM_FIELD_SIM_ECC:
         ecc_cache = pbm_file_id_to_cache(session_type, PBM_FILE_ECC);
         break;
      default:
         PBM_MSG_HIGH("invalid ecc category 0x%x", field_id,0,0);
         return PBM_ERROR;
   }

   PBM_CHECK_PTR2_RET(ecc_cache, ecc_string, PBM_ERROR);

   if ( usim_ecc_serv_cat == 0xFF )
   {
     usim_ecc_serv_cat = PBM_EMERGENCY_SERVICE_CAT_DEFAULT ;
   }
   else if ( usim_ecc_serv_cat > PBM_EMERGENCY_SERVICE_CAT_VALID_MASK )
   {
     usim_ecc_serv_cat &= PBM_EMERGENCY_SERVICE_CAT_VALID_MASK;
   }

   if (data_length > 0)
   {
      PBM_MSG_HIGH("Adding ECC num, type 0x%x len %d emergency_mode %d", field_id,
                   data_length, emergency_mode);
      PBM_MSG_HIGH("Adding ECC num, 0x%x 0x%x 0x%x",
                   (data_length>0?ecc_string[0]:-1),
                   (data_length>1?ecc_string[1]:-1),
                   (data_length>2?ecc_string[2]:-1));
      PBM_MSG_HIGH("Adding ECC num, 0x%x 0x%x 0x%x",
                   (data_length>3?ecc_string[3]:-1),
                   (data_length>4?ecc_string[4]:-1),
                   (data_length>5?ecc_string[5]:-1));
      PBM_MSG_HIGH("Adding ECC num, cat 0x%x valid %d %d", usim_ecc_serv_cat,
                   usim_ecc_serv_cat_valid, ecc_cache->pb_id.device_type);

      field_length[number_of_fields++] =  data_length;
      if (name_length > 0)
      {
         field_length[number_of_fields++] =  name_length;
      }
      if (usim_ecc_serv_cat_valid)
      {
         field_length[number_of_fields++] = 1;
      }
      if(emergency_mode != NOT_EMERGENCY)
      {
         field_length[number_of_fields++] = 1;
      }

      // Next format the number for the ECC cache indicating that it is from
      // the SESSION. If we ever need to figure out which application it came
      // from, that would be a different the field ID for now, each SESSION
      // has a unique ID.
      record = pbm_cache_record_new();
      PBM_CHECK_PTR_RET(record, PBM_ERROR_MEM_FULL);
      if (!pbm_allocate_fields_internal(&record->fields, number_of_fields,
                                        field_length))
      {
         PBM_MEM_FREEIF(record);
         PBM_MSG_ERR("Could not allocate fields", 0, 0, 0);
         return PBM_ERROR;
      }
      record->unique_id = 0;

      //hardcoded no's should skip the first PBM_NV_EMERGENCY_NUMBERS locations
      //since they are used by NV
      if(field_id == PBM_FIELD_HARDCODED_ECC)
      {
        uint8 loc_index;
        for (loc_index = PBM_NV_EMERGENCY_NUMBERS+1; loc_index <= ecc_cache->num_of_records; loc_index++)
        {
           if ((ecc_cache->pb_cache_array[loc_index] == NULL) || (ecc_cache->pb_cache_array[loc_index]->num_fields == 0))
              break;
        }

        if (loc_index > ecc_cache->num_of_records)  /* No empty slot was found*/
        {
          pbm_free_fields_internal(&record->fields, 1);
          PBM_MEM_FREEIF(record);
          return PBM_ERROR_MEM_FULL;
        }
        location = loc_index;
      }
      /* Allocate the location.  Return an error if it is full. */
      else if (0 == (location = pbm_get_first_available(ecc_cache, FALSE)))
      {
         pbm_free_fields_internal(&record->fields, 1);
         PBM_MEM_FREEIF(record);
         return PBM_ERROR_MEM_FULL;
      }

      switch (field_id)
      {
         case PBM_FIELD_HARDCODED_ECC:
         case PBM_FIELD_NV_ECC:
            pb_id.device_type = PBM_ECC_OTHER;
            record->record_id = pbm_session_location_to_record_id(pb_id, location);
            break;
         case PBM_FIELD_NETWORK_ECC:
         case PBM_FIELD_SIM_ECC:
            pb_id = pbm_device_type_to_phonebook_type(session_type, PBM_ECC);
            record->record_id = pbm_session_location_to_record_id(pb_id, location);
            break;
         default:
            PBM_MSG_HIGH("invalid ecc category 0x%x", field_id,0,0);
            return PBM_ERROR;
      }

      record->category = (int) PBM_CAT_ECC;
      record->num_fields = (uint16) number_of_fields;
      index = 0;
      pbm_fill_field(&record->fields[index++], field_id,
                     (pbm_field_type_e_type) PBM_FT_PHONE,
                     (byte *) ecc_string, (uint16) data_length);

      if (name_length > 0)
      {
         pbm_fill_field(&record->fields[index++], PBM_FIELD_NAME,
                        (pbm_field_type_e_type) PBM_FT_UCS2,
                        (byte *) name_string, (uint16) name_length);
      }

      if (usim_ecc_serv_cat_valid)
      {
         pbm_fill_field(&record->fields[index++],
                        PBM_FIELD_ECC_SERVICE_CATEGORY,
                        (pbm_field_type_e_type) PBM_FT_BYTE,
                        (byte *) &usim_ecc_serv_cat, 1);
      }
      if(emergency_mode != NOT_EMERGENCY)
      {
        pbm_fill_field(&record->fields[index++],
                       PBM_FIELD_EMERGENCY_MODE,
                       (pbm_field_type_e_type) PBM_FT_BYTE,
                       (byte *) &emergency_mode, 1);
      }

      // Finally, add the number to the cache.
      if (PBM_SUCCESS != pbm_cache_add(ecc_cache, record, TRUE))
      {
         pbm_free_fields_internal(&record->fields, 1);
         PBM_MEM_FREEIF(record);
         return PBM_ERROR;
      }
   }
   return PBM_SUCCESS;
}


/*===========================================================================
FUNCTION PBM_ECC_SET_HARDCODED_ECCS

DESCRIPTION
  This function is called at initialization and whenever a SIM is
  inserted or removed.  If uim_present indicates no SIM is present,
  then the longer hardcoded list of emergency numbers is used.  If
  the SIM is inserted, then the shorter list is used.

DEPENDENCIES
  None

SIDE EFFECTS
===========================================================================*/
void pbm_ecc_set_hardcoded_eccs (boolean uim_present, boolean ecc_present)
{
  pbm_phonebook_type            pb_id                                 = {PBM_GPB};
  typedef struct pbm_ecc_tbl
  {
     char     *dial_string;
     uint32   string_length;
     boolean  already_in_pbm;
     uint8     service_cat_len;
     pbm_service_category_val service_cat_val;
     pbm_ecc_mode_type emergency_mode;
  } PBM_ECC_TBL;
  int                           efs_status                            = 0;
  int                           list_size                             = 0;
  int                           i                                     = 0;
  int                           j                                     = 0;
  char                         *pbm_nv_hardcoded_ecc_file             = "/nv/item_files/pbm/pbm_hardcoded_ecc_list";
  char                        **num                                   = NULL;
  pbm_hardcoded_ecc_s_type     *pbm_nv_hardcoded_ecc_ptr              = NULL;
  pbm_return_type               ret_val                               = PBM_SUCCESS;
  PBM_ECC_TBL                  *nv_hardcoded_list                     = NULL;
  struct pbm_ecc_tbl           *current_list                          = NULL;
  PB_CACHE                     *ecc_cache                             = NULL;
  boolean                       add_entry_to_list                     = FALSE;
  pbm_feature_status_enum_type  ecc_hardcode_sim_absent_till_pin_lock = PBM_FEATURE_DISABLED ;

    PBM_ECC_TBL hardcoded_with_uim[] = {
	  { "911",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW_1X},              /* 911 is ALWAYS an emergency number */

#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM)
	  { "112",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},               /* 112 is ALWAYS one if GSM/UMTS */
#if defined (FEATURE_PBM_EMERGENCY_CHINA)
			  { "118",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
			  { "000",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
			  { "08",  3, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
#endif
#endif
#if (defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900))
	  { "*911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},               /* These are specific to CDMA */
	  { "#911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},
#endif
#if defined (FEATURE_CPHS_UK) || defined (FEATURE_PBM_EMERGENCY_CHINA)
	  { "999",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},               /* This is for a CPHS requirement */
#endif
     };

     PBM_ECC_TBL hardcoded_with_uim_but_no_ecc[] = {
	 { "911",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW_1X},              /* 911 is ALWAYS an emergency number */
#if (defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900))
	 { "*911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},               /* These are specific to CDMA */
	 { "#911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},
#endif
#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM)
	 { "112",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},               /* 112 is ALWAYS one if GSM/UMTS */
#if defined (FEATURE_PBM_EMERGENCY_CHINA)
	 { "118",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	 { "000",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	 { "08",  3, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
#endif
#endif

#if defined (FEATURE_CPHS_UK) || defined (FEATURE_PBM_EMERGENCY_CHINA)
	 { "999",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
#endif
     };

     PBM_ECC_TBL hardcoded_with_no_uim[] = {
	  { "911",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW_1X},
#if (defined (FEATURE_CDMA_800) || defined (FEATURE_CDMA_1900))
	  { "*911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},
	  { "#911", 5, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_1X},
#endif
#if defined (FEATURE_WCDMA) || defined (FEATURE_GSM)
	  { "112",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "000",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "08",   3, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "110",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "999",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "118",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
	  { "119",  4, FALSE,PBM_EMERGENCY_SERVICE_CAT_LEN,PBM_EMERGENCY_SERVICE_CAT_DEFAULT,EMERGENCY_GW},
#endif
     };

     ecc_hardcode_sim_absent_till_pin_lock = pbm_nv_get_feature_status(PBM_FEATURE_ECC_HARDCODE_SIM_ABSENT_LIST_TILL_PIN_LOCK);

     if (!pbm_hardcode_ecc_config)
     {
       // Select the table of ECCs which is used when no SIM is inserted.
       if (uim_present)
       {
         if (ecc_present)
         {
           current_list = hardcoded_with_uim;
           list_size = ARR_SIZE(hardcoded_with_uim);
         } else
         {
           current_list = hardcoded_with_uim_but_no_ecc;
           list_size = ARR_SIZE(hardcoded_with_uim_but_no_ecc);
         }
       } else
       {
         current_list = hardcoded_with_no_uim;
         list_size = ARR_SIZE(hardcoded_with_no_uim);
       }
     }
     else
     {
       PBM_MEM_ALLOC(nv_hardcoded_list,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(PBM_ECC_TBL));
       if( !nv_hardcoded_list )
       {
         PBM_MSG_ERR("Could not allocate memeory for nv_hardcoded_list",0,0,0);
         return ;
       }
       PBM_MEM_ALLOC(pbm_nv_hardcoded_ecc_ptr,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(pbm_hardcoded_ecc_s_type));
       if( !pbm_nv_hardcoded_ecc_ptr )
       {
         PBM_MSG_ERR("Could not allocate memeory for pbm_nv_hardcoded_ecc_ptr",0,0,0);
         PBM_MEM_FREEIF(nv_hardcoded_list);
         return ;
       }
       memset((byte *)nv_hardcoded_list, 0,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(PBM_ECC_TBL));
       memset((byte *)pbm_nv_hardcoded_ecc_ptr, 0,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(pbm_hardcoded_ecc_s_type));

       efs_status = efs_get(pbm_nv_hardcoded_ecc_file,pbm_nv_hardcoded_ecc_ptr,
                                PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(pbm_hardcoded_ecc_s_type));
       PBM_MSG_HIGH("read of hardcode list efs_status %d ",efs_status,0,0);

       if (efs_status < 0)
       {
         PBM_MSG_ERR("PBM Hardcoded ECC read from NV failed %d",efs_status,0,0);
         ret_val = PBM_ERROR;
       }

       PBM_MEM_ALLOC(num,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(char *));
       if( !num )
       {
          PBM_MSG_ERR("Could not allocate memeory for num",0,0,0);
          PBM_MEM_FREEIF(nv_hardcoded_list);
          PBM_MEM_FREEIF(pbm_nv_hardcoded_ecc_ptr);
          return ;
       }

       memset((byte *)num, 0,PBM_HARDCODED_NV_EMERGENCY_NUMBERS*sizeof(char *));


       for ( i=0; i < PBM_HARDCODED_NV_EMERGENCY_NUMBERS&&ret_val==PBM_SUCCESS; i++ )
       {
          add_entry_to_list   =  FALSE ; 
          if( pbm_nv_hardcoded_ecc_ptr[i].num_digits == 0 )
          {
             continue ;
          }
          else if (pbm_nv_hardcoded_ecc_ptr[i].num_digits > PBM_NV_MAX_DIAL_DIGITS)
          {
           pbm_nv_hardcoded_ecc_ptr[i].num_digits = PBM_NV_MAX_DIAL_DIGITS;
          }

         if( pbm_nv_hardcoded_ecc_ptr[i].category_length > PBM_EMERGENCY_SERVICE_CAT_LEN )
         {
           pbm_nv_hardcoded_ecc_ptr[i].category_length = PBM_EMERGENCY_SERVICE_CAT_LEN ;
         }

         if ( pbm_nv_hardcoded_ecc_ptr[i].category_length )
         {
           pbm_nv_hardcoded_ecc_ptr[i].emergency_category  &= PBM_EMERGENCY_SERVICE_CAT_VALID_MASK ;
         }

         switch ( pbm_nv_hardcoded_ecc_ptr[i].hardcoded_type )
         {
             case PBM_HARDCODE_ALWAYS:
               add_entry_to_list = TRUE;
             break;

             case PBM_HARDCODE_CARD_ABSENT:
               add_entry_to_list = ( !uim_present  || 
                                     (uim_present && 
                                      ecc_hardcode_sim_absent_till_pin_lock && 
                                      !(pbm_pin1_verified.value & PBM_MASK_CARD_SESSION_PIN_VALUES)));
             break;

             case PBM_HARDCODE_CARD_PRESENT:
               add_entry_to_list = uim_present;
             break;

             default:
               add_entry_to_list = FALSE;
             break;
         }
         PBM_MSG_HIGH("add_entry_to_list 0x%x and hardcode type 0x%x",add_entry_to_list, pbm_nv_hardcoded_ecc_ptr[i].hardcoded_type, 0);

         if (add_entry_to_list)
         {
             PBM_MEM_ALLOC(num[i], pbm_nv_hardcoded_ecc_ptr[i].num_digits + 1);
             if( !num[i] )
             {
               PBM_MSG_ERR("Could not allocate memeory of %d bytes for dial_string",pbm_nv_hardcoded_ecc_ptr[i].num_digits + 1,0,0);
               PBM_MEM_FREEIF(pbm_nv_hardcoded_ecc_ptr);
               PBM_MEM_FREEIF(nv_hardcoded_list);
               PBM_MEM_FREEIF(num);
               return ;
             }
             else
             {
               PBM_MSG_HIGH("allocated %d for num[%d]",pbm_nv_hardcoded_ecc_ptr[i].num_digits + 1 , i,0);
               memset((byte *)num[i], 0,pbm_nv_hardcoded_ecc_ptr[i].num_digits + 1);
             }

             for (j = 0; j < pbm_nv_hardcoded_ecc_ptr[i].num_digits; j++)
             {
               num[i][j] = pbm_nv_hardcoded_ecc_ptr[i].digits[j];
               PBM_MSG_HIGH("num[%d][%d] is %d ",i,j , pbm_nv_hardcoded_ecc_ptr[i].digits[j]);
             }
             num[i][j] = '\0';


             nv_hardcoded_list[i].dial_string = num[i] ;
             nv_hardcoded_list[i].string_length = pbm_nv_hardcoded_ecc_ptr[i].num_digits + 1;
             nv_hardcoded_list[i].already_in_pbm = FALSE ;
             nv_hardcoded_list[i].service_cat_len = pbm_nv_hardcoded_ecc_ptr[i].category_length;
             if( pbm_nv_hardcoded_ecc_ptr[i].category_length )
             {
                nv_hardcoded_list[i].service_cat_val = pbm_nv_hardcoded_ecc_ptr[i].emergency_category ;
             }
             nv_hardcoded_list[i].emergency_mode = (pbm_ecc_mode_type)(pbm_nv_hardcoded_ecc_ptr[i].emergency_mode) ;//>>
             PBM_MSG_HIGH(" PBM hardcode %d string_length %d emergency_mode %d",pbm_nv_hardcoded_ecc_ptr[i].hardcoded_type,nv_hardcoded_list[i].string_length,nv_hardcoded_list[i].emergency_mode);           }
       }

       PBM_MEM_FREEIF(pbm_nv_hardcoded_ecc_ptr);

       current_list = nv_hardcoded_list;
       list_size = PBM_HARDCODED_NV_EMERGENCY_NUMBERS ;

  }

   rex_enter_crit_sect(&pbm_crit_sect);

   // Search through the ECC cache for hardcoded entries
   pb_id.device_type = PBM_ECC_OTHER;
   ecc_cache = pbm_pb_id_to_cache(pb_id);
  if(ecc_cache == NULL)
  {
    rex_leave_crit_sect(&pbm_crit_sect);
    return;
  }
   for (i = 1; i <= ecc_cache->num_of_records; i++)
   {
      if (ecc_cache->pb_cache_array[i])
      {
         if (match_found(PBM_CAT_NONE, PBM_FIELD_HARDCODED_ECC, NULL, 0, 0,
                         ecc_cache->pb_cache_array[i]))
         {
            // We found an entry that is a hardcoded ECC
            // If the entry is not in the current list, remove it.
            for (j = 0; j < list_size; j++)
            {
              if ( (current_list[j].string_length > 0) && 
                   (match_found(PBM_CAT_NONE, PBM_FIELD_HARDCODED_ECC,
                               current_list[j].dial_string,
                               (uint16) current_list[j].string_length,
                               PBM_SEARCHTYPE_EXACT,
                               ecc_cache->pb_cache_array[i])) &&
                    (match_found(PBM_CAT_NONE, PBM_FIELD_EMERGENCY_MODE,
                               &current_list[j].emergency_mode,
                               (uint16) sizeof(current_list[j].emergency_mode),
                               PBM_SEARCHTYPE_EXACT,
                               ecc_cache->pb_cache_array[i])) && 
                    (current_list[j].service_cat_len > 0) &&
                    (match_found(PBM_CAT_NONE, PBM_FIELD_ECC_SERVICE_CATEGORY,
                               &current_list[j].service_cat_val,
                               (uint16) current_list[j].service_cat_len,
                               PBM_SEARCHTYPE_EXACT,
                               ecc_cache->pb_cache_array[i])))
               {
                  current_list[j].already_in_pbm = TRUE;
                  break;
               }
            }
            if (j == list_size)
            {
               // The entry was not in the current list, so remove it.
               ret_val = pbm_cache_delete(ecc_cache, ecc_cache->pb_cache_array[i]->record_id);
               if (PBM_SUCCESS != ret_val)
               {
                   PBM_MSG_ERR("Error %d while removing record %d from ECC cache", ret_val, ecc_cache->pb_cache_array[i]->record_id,0);
               }
            }
         }
      }
   }
   // If any entry is not in the current list, add it.
   for (i = 0; i < list_size; i++)
   {
      if (FALSE == current_list[i].already_in_pbm)
      {
         /* Use dummy session for HARDCODED ECC/ EFS records */
         ret_val = pbm_build_ecc_record(PBM_FIELD_HARDCODED_ECC,
                                        current_list[i].string_length,
                                        current_list[i].dial_string,
                                        0,
                                        NULL,
                                        ((current_list[i].service_cat_val) ? TRUE : FALSE),
                                        current_list[i].service_cat_val,
                                        PBM_SESSION_DEFAULT,
                                        current_list[i].emergency_mode);
         if (PBM_SUCCESS != ret_val)
         {
           PBM_MSG_ERR("Error %d in building hardcoded record.", ret_val, 0, 0);
         }
      }
   }
   rex_leave_crit_sect(&pbm_crit_sect);

   if (pbm_hardcode_ecc_config)
   {
      PBM_MSG_HIGH("Freeing the allocated NV HCC ECC table 0x%x",nv_hardcoded_list,0,0);
      PBM_MEM_FREEIF(nv_hardcoded_list);
      for ( i = 0;i < PBM_HARDCODED_NV_EMERGENCY_NUMBERS ; i ++ )
      {
        if ( num[i])
        {
          PBM_MEM_FREEIF(num[i]);
        }
      }
      PBM_MEM_FREEIF(num);
   }
}


/*===========================================================================
FUNCTION PBM_ECC_CLEAR_ALL_ID

DESCRIPTION
  This function is called at initialization and whenever a SIM is
  removed.  It removes all entries in the emergency_cache that
  have the ID of the given field.

DEPENDENCIES
  None

SIDE EFFECTS
===========================================================================*/
void pbm_ecc_clear_all_id(pbm_session_enum_type session_type, pbm_field_id_e_type field_id)
{
   PB_CACHE *ecc_cache;
   int       i;
   boolean   uses_sim = FALSE;
   pbm_phonebook_type pb_id = {PBM_GPB};

   switch (field_id)
   {
      case PBM_FIELD_SIM_ECC:
         uses_sim = TRUE;
         pb_id = pbm_device_type_to_phonebook_type(session_type, PBM_ECC);
         ecc_cache = pbm_pb_id_to_cache(pb_id);
         break;
      case PBM_FIELD_NETWORK_ECC:
         uses_sim = FALSE;
         pb_id = pbm_device_type_to_phonebook_type(session_type, PBM_ECC);
         ecc_cache = pbm_pb_id_to_cache(pb_id);
         break;
      default:
         PBM_MSG_ERR("pbm_ecc_clear_all_id works only for SIM_ECC, NETWORK_ECC",
                     0, 0, 0);
         return;
   }

   /* If this ECC was not initialized, nothing to do. */
   if (!uses_sim ||
       (0 != (ecc_has_entries & PBM_SESSIONID_TO_SESSION_MASK(session_type))))
   {
      // Search through the ECC cache for hardcoded entries
      ecc_cache = pbm_device_id_to_cache(session_type, PBM_ECC);
      PBM_CHECK_PTR_RET(ecc_cache, VOID);
      for (i = 1; i <= ecc_cache->num_of_records; i++)
      {
         if (ecc_cache->pb_cache_array[i])
         {
            if (match_found(PBM_CAT_NONE, field_id, NULL, 0, 0,
                            ecc_cache->pb_cache_array[i]))
            {
               pbm_return_type ret_val;
               ret_val = pbm_cache_delete(ecc_cache,ecc_cache->pb_cache_array[i]->record_id);
               if (PBM_SUCCESS != ret_val)
               {
                  PBM_MSG_ERR("Error %d in deleting ECC entry %d from ECC cache 0x%x",
                              ret_val, ecc_cache->pb_cache_array[i]->record_id, ecc_cache);
               }
            }
         }
      }
   }
   if (uses_sim)
   {
      /* Clear out the flag now. */
      PBM_SESSION_RESET(ecc_has_entries, session_type);
      }

   return;
}


/*===========================================================================
FUNCTION PBM_INIT_ECC_NV

DESCRIPTION
  This function is called during boot.  It reads the ECC list as stored
  in NV.

  The idea is to always allow Emergency Calls through by keeping a list
  of valid Emergency Call Codes (ECC's) in NV in order for the ME to
  be able to call an emergency call even if no SIM is installed.

DEPENDENCIES
  None

SIDE EFFECTS
===========================================================================*/
static pbm_return_type pbm_init_ecc_nv ( void )
{
   int i,j;
   pbm_phonebook_type   pb_id = {PBM_GPB};
   PB_CACHE             *cache;
   pbm_return_type      ret_val = PBM_SUCCESS;
   uint16               name[PBM_TEXT_SIZE_CHARS];
   uint16               size;
   pbm_nv_ecc_type      *nv_ecc_item = NULL;
   int                  efs_read_status  = 0;
   uint8                num_of_nv_ecc = 0,num_nv_ecc_left = PBM_NV_EMERGENCY_NUMBERS;
   uint8                read_ecc_from = 0;
   char                 *pbm_nv_ecc_file = "nv/item_files/pbm/pbm_ecc_nums";
   char                 *pbm_read_ecc_num_from = "nv/item_files/pbm/pbm_nv_ecc_config";


   efs_read_status = efs_get(pbm_read_ecc_num_from,
                             &read_ecc_from,sizeof(uint8));
   PBM_MSG_HIGH(" Reading NV ECC from  %d",read_ecc_from,0,0);
   if (efs_read_status < 0)
   {
     PBM_MSG_ERR("NV read failed for nv_ecc_config item status=0x%x",efs_read_status,0,0);
     ret_val = PBM_ERROR;
   }
   if ( read_ecc_from > PBM_READ_ECC_FROM_MAX )
          read_ecc_from = PBM_READ_ECC_FROM_DEFAULT ;


   /* Add all the hardcoded numbers. */
   PBM_MSG_HIGH("Start PBM hardcoded/NV ECC init",0,0,0);

   pbm_ecc_set_hardcoded_eccs(FALSE, FALSE);

   PBM_MSG_HIGH("Finish PBM hardcoded ECC init",0,0,0);
   pb_id.device_type = PBM_ECC_OTHER;
   cache = pbm_pb_id_to_cache(pb_id);

   PBM_CHECK_PTR_RET(cache, PBM_ERROR);

   if( read_ecc_from == PBM_READ_ECC_FROM_BOTH  || read_ecc_from == PBM_READ_ECC_FROM_DIAL_I )
   {
     for ( i=0; i < PBM_NV_EMERGENCY_NUMBERS; i++ )
     {
        nv_dial_type   dial_nv;
        memset ((byte *) &dial_nv, 0, sizeof(nv_dial_type));

        dial_nv.address = (byte) ( i + PBM_NV_EMERGENCY_NUMBER_START );
        (void) pbm_get_nv(NV_DIAL_I, (nv_item_type *) &dial_nv);

        if (dial_nv.num_digits > NV_MAX_DIAL_DIGITS)
           dial_nv.num_digits = NV_MAX_DIAL_DIGITS;

        if (dial_nv.num_digits > 0 && dial_nv.status == NV_EMERGENCY_SD_NUM)
        {
           //Need to add a NULL to the end of dial_nv.
           char *dial_string = NULL;
           PBM_MEM_ALLOC(dial_string, dial_nv.num_digits + 1);
           PBM_CHECK_PTR_RET(dial_string, PBM_ERROR_MEM_FULL);

           for (j = 0; j < dial_nv.num_digits; j++)
           {
              dial_string[j] = dial_nv.digits[j];
           }
           dial_string[j] = '\0';
           size = (uint16) strlen((char *) dial_nv.letters);
           if (size > PBM_TEXT_SIZE_CHARS)
           {
              size=PBM_TEXT_SIZE_CHARS;
           }
           if (!pbmconvert_pbm8_to_ucs2((uint8 *) dial_nv.letters, size,
                                        name, ARR_SIZE(name)))
           {
              PBM_MSG_ERR("Could not convert all %d octets of the name",
                          size, 0, 0);
           }
           if (PBM_SUCCESS != pbm_build_ecc_record(PBM_FIELD_NV_ECC,
                                                   dial_nv.num_digits + 1,
                                                   dial_string,
                                                   (pbmutils_wstrlen(name) + 1)* sizeof(uint16),
                                                   name,
                                                   FALSE, 0, PBM_SESSION_DEFAULT,EMERGENCY_GW_1X)  )  // num considered as emergency in both GW and 1x
           {
              PBM_MSG_ERR("Could not build ECC record", 0, 0, 0);
           }
           else
           {
                num_of_nv_ecc++; // gives the number of ECC records built through the NV item
           }

           PBM_MEM_FREEIF(dial_string);
        }
   }

   num_nv_ecc_left = PBM_NV_EMERGENCY_NUMBERS - num_of_nv_ecc ; // gives the number of records that can be written through NV_ECC_ITEM
 }

 if( num_nv_ecc_left != 0 && (read_ecc_from == PBM_READ_ECC_FROM_BOTH  || read_ecc_from == PBM_READ_ECC_FROM_EFS_NV))
 {
   PBM_MSG_HIGH("PBM EFS NV read started for %d numbers",num_nv_ecc_left,0,0);
   PBM_MEM_ALLOC(nv_ecc_item,
                num_nv_ecc_left*sizeof(pbm_nv_ecc_type));
   PBM_CHECK_PTR_RET(nv_ecc_item, PBM_ERROR);
   memset ((byte *)nv_ecc_item, 0,
          num_nv_ecc_left*sizeof(pbm_nv_ecc_type));

   /* Get halt subscription config value from efs */
   efs_read_status = efs_get(pbm_nv_ecc_file,
                             nv_ecc_item,
                             num_nv_ecc_left*
                             sizeof(pbm_nv_ecc_type));

   if (efs_read_status < 0)
   {
     PBM_MSG_ERR("NV read failed.status=0x%x",
              efs_read_status,0,0);
     ret_val = PBM_ERROR;
   }

   /* Read in the numbers from NV. */
   for ( i=0; i < num_nv_ecc_left&&ret_val==PBM_SUCCESS; i++ )
   {

      if (nv_ecc_item[i].num_digits > NV_MAX_DIAL_DIGITS)
         nv_ecc_item[i].num_digits = NV_MAX_DIAL_DIGITS;

      if (nv_ecc_item[i].num_digits > 0 && nv_ecc_item[i].status == NV_EMERGENCY_SD_NUM)
      {
         //Need to add a NULL to the end of dial_nv.
         char *dial_string = NULL;
         PBM_MEM_ALLOC(dial_string, nv_ecc_item[i].num_digits + 1);
         PBM_CHECK_PTR_RET(dial_string, PBM_ERROR_MEM_FULL);

         for (j = 0; j < nv_ecc_item[i].num_digits; j++)
         {
            dial_string[j] = nv_ecc_item[i].digits[j];
         }
         dial_string[j] = '\0'; //add NULL
         size = (uint16) strlen((char *) nv_ecc_item[i].letters);
         if (size > PBM_TEXT_SIZE_CHARS) /*  KW error */
         {
            size=PBM_TEXT_SIZE_CHARS;
         }
         if (!pbmconvert_pbm8_to_ucs2((uint8 *) nv_ecc_item[i].letters, size,
                                      name, ARR_SIZE(name)))
         {
            PBM_MSG_ERR("Could not convert all %d octets of the name",
                        size, 0, 0);
         }

         if (PBM_SUCCESS != pbm_build_ecc_record(PBM_FIELD_NV_ECC,
                                                 nv_ecc_item[i].num_digits + 1,
                                                 dial_string,
                                                 (pbmutils_wstrlen(name) + 1)* sizeof(uint16),
                                                 name,
                                                 FALSE, 0,
                                                 PBM_SESSION_DEFAULT,
                                                 nv_ecc_item[i].emergency_mode))
         {
            PBM_MSG_ERR("Could not build ECC record", 0, 0, 0);
         }

         PBM_MEM_FREEIF(dial_string);
      }
   }
   PBM_MEM_FREEIF(nv_ecc_item);

  }
  PBM_SET_PB_CACHE_READY(cache);
  return ret_val;
}


/*===========================================================================
FUNCTION PBM_INIT_CREATE_CONF_FILE

DESCRIPTION
  Function used to create a .conf file in EFS if not present. the .conf file
  is used to specify the PBM NV items that can be backed up in a QCN file.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
static void pbm_init_create_conf_file(void)
{
  char           *buffer_ptr       = NULL;
  uint16          buffer_len       = 0;
  uint16          buffer_index     = 0;
  uint8           file_index       = 0;
  uint8           num_config_files = 0;
  struct fs_stat  conf_stat;
  char           *conf_files[] =
  {
    PBM_CONFIG_FILE_1,
    PBM_CONFIG_FILE_2,
    PBM_CONFIG_FILE_3,
    PBM_CONFIG_FILE_4,
    PBM_CONFIG_FILE_5,
    PBM_CONFIG_FILE_6,
    PBM_FEATURES_STATUS_LIST_FILE
  };

  /* Create directory, if needed */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if (efs_stat(PBM_CONF_DIR1, &conf_stat) == -1 )
  {
    (void)efs_mkdir(PBM_CONF_DIR1, 0777);
    (void)efs_mkdir(PBM_CONF_DIR2, 0777);
    (void)efs_mkdir(PBM_CONF_DIR3, 0777);
  }
  else if (efs_stat(PBM_CONF_DIR2, &conf_stat) == -1 )
  {
    (void)efs_mkdir(PBM_CONF_DIR2, 0777);
    (void)efs_mkdir(PBM_CONF_DIR3, 0777);
  }
  else if (efs_stat(PBM_CONF_DIR3, &conf_stat) == -1 )
  {
    (void)efs_mkdir(PBM_CONF_DIR3, 0777);
  }


  /* Calculate total length needed to write, note on total size needed:
     (num_config_files * (string len of each file + '\n')) */
  num_config_files = sizeof(conf_files)/sizeof(conf_files[0]);
  for (file_index = 0; file_index < num_config_files; file_index++)
  {
    buffer_len += strlen(conf_files[file_index]) + sizeof(char);
  }


  PBM_MEM_ALLOC(buffer_ptr,buffer_len);
  if (buffer_ptr == NULL)
   {
    PBM_MSG_ERR( "Memory allocation failed for conf files buffer!",0,0,0);
    return;
   }

  memset(buffer_ptr, 0, buffer_len);

  /* If created first time, write the items */
  for (file_index = 0; file_index < num_config_files; file_index++)
  {
    uint16 len_wrote = 0;
    uint16 len_to_write = strlen(conf_files[file_index]);

    /* Content of the file. Note: there should not be spaces before the names! */
    len_wrote  = memscpy((void*)&buffer_ptr[buffer_index],
                                    buffer_len - buffer_index,
                                   (void*)conf_files[file_index],
                                  len_to_write);
    buffer_index += len_wrote;
  
    /* Check ensures space for '\n' */
    if ( len_wrote != len_to_write || buffer_index >= buffer_len) 
    {
      PBM_MSG_ERR( "Insufficient buffer space for conf_files[%d] !",file_index,0,0);
      PBM_MEM_FREEIF(buffer_ptr);
      return;
    }
  
    buffer_ptr[buffer_index++] = '\n';
   }
   pbm_compare_and_update_file(PBM_CONF_FILE,buffer_ptr,(uint32)buffer_len);

   PBM_MEM_FREEIF(buffer_ptr);
} /* pbm_init_create_conf_file */

/*===========================================================================
FUNCTION PBM_NV_GET_FEATURE_STATUS

DESCRIPTION
  Function used to get the status of PBM features listed in enum
  pbm_nv_features_enum_type as defined by the NVITEM 71535 -
  "PBM_FEATURES_STATUS_LIST".

DEPENDENCIES
  Minimum QXDM version required to be able to read/write this NVITEM is
  latest QXDM(or any version later than 3.14.757)

RETURN VALUE
  pbm_feature_status_enum

SIDE EFFECTS
  None
===========================================================================*/
pbm_feature_status_enum_type pbm_nv_get_feature_status(
  pbm_nv_features_enum_type  nv_feature)
{
  pbm_feature_status_enum_type ret_value = PBM_FEATURE_DISABLED;

  switch(nv_feature)
  {
    case PBM_FEATURE_FDN_SPECIAL_HANDLING:
      ret_value = pbm_features_status_list.fdn_special_handling;
      break;
    case PBM_FEATURE_ECC_HARDCODE_SIM_ABSENT_LIST_TILL_PIN_LOCK :
      ret_value = pbm_features_status_list.pbm_ecc_hardcode_sim_absent_list_till_pin_lock;
      break;
    default:
      PBM_MSG_HIGH("Invalid Feature 0x%x", nv_feature,0,0);
      return ret_value;
  }
  
  PBM_MSG_HIGH("Status of feature 0x%x is 0x%x", nv_feature, ret_value,0);
  return ret_value;
} /* pbm_nv_get_feature_status */

/*===========================================================================
FUNCTION PBM_NV_FEATURE_WRITE_DEFAULT_VALUE

DESCRIPTION
  Function used to write the default value of MMGSDI Feature (controlled by
  PBM_Feature_Status_list NVITEM) to global pbm_features_status_list.

DEPENDENCIES

RETURN VALUE
  pbm_return_type

SIDE EFFECTS
  None
===========================================================================*/
static pbm_return_type pbm_nv_feature_write_default_value(
  pbm_nv_features_enum_type nv_feature)
{
  switch(nv_feature)
  {
    case PBM_FEATURE_FDN_SPECIAL_HANDLING:
      pbm_features_status_list.fdn_special_handling = PBM_FEATURE_DISABLED;
      break;
    case PBM_FEATURE_ADN_CACHING:
    {
      uint8 slot_id;
      for(slot_id = 0;slot_id < PBM_MAX_SLOTS;slot_id++)
      {
        pbm_features_status_list.pbm_adn_phonebook_caching_status[slot_id].
                                   gpb_caching_status    = PBM_FEATURE_ENABLED;
        pbm_features_status_list.pbm_adn_phonebook_caching_status[slot_id].
                                   lpb_1x_caching_status = PBM_FEATURE_ENABLED;
        pbm_features_status_list.pbm_adn_phonebook_caching_status[slot_id].
                                   lpb_gw_caching_status = PBM_FEATURE_ENABLED;
      }
    }
      break;
    case PBM_FEATURE_ECC_HARDCODE_SIM_ABSENT_LIST_TILL_PIN_LOCK :
      pbm_features_status_list.pbm_ecc_hardcode_sim_absent_list_till_pin_lock = PBM_FEATURE_DISABLED;
    break;

    default:
      PBM_MSG_ERR("Invalid Feature 0x%x", nv_feature,0,0);
      return PBM_ERROR;
  }
  return PBM_SUCCESS;
} /* pbm_nv_feature_write_default_value */

/*===========================================================================
FUNCTION PBM_NV_WRITE_DEFAULT_VALUE_OF_ALL_FEATURES

DESCRIPTION
  Function used to write the default values of all the PBM Features
  (controlled by PBM_Feature_Status_list NVITEM), defined currently, to
  global pbm_features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void pbm_nv_write_default_value_of_all_features(void)
{
  uint8 pbm_feature;
  uint8 num_features_in_build;

  num_features_in_build = pbm_features_total_till_version[PBM_FEATURES_STATUS_LIST_CURRENT_VERSION];

  memset(&pbm_features_status_list, 0x00, sizeof(pbm_features_status_list));

  /* Update the Version in features_status_list to the current build version */
  pbm_features_status_list.version = PBM_FEATURES_STATUS_LIST_CURRENT_VERSION;

  /* Update features_status_list with default values */
  for(pbm_feature = 0; pbm_feature < num_features_in_build; pbm_feature++)
  {
    (void)pbm_nv_feature_write_default_value((pbm_nv_features_enum_type )pbm_feature);
  }
} /* pbm_nv_write_default_value_of_all_features */

/*===========================================================================
FUNCTION PBM_NV_WRITE_DEFAULT_VALUE_OF_DELTA_FEATURES

DESCRIPTION
  Function used to write the default values of PBM Features (controlled by
  PBM_Feature_Status_list NVITEM) that are delta between the two
  versions of Feature_Status_List to global features_status_list.

DEPENDENCIES

RETURN VALUE
  void

SIDE EFFECTS
  None
===========================================================================*/
static void pbm_nv_write_default_value_of_delta_features(
  uint8 version_in_nv,
  uint8 version_in_build
)
{
  uint8 pbm_feature;
  uint8 num_features_in_nv;
  uint8 num_features_in_build;

  if((version_in_nv >= sizeof(pbm_features_total_till_version)/sizeof(uint8)) ||
     (version_in_build >= sizeof(pbm_features_total_till_version)/sizeof(uint8)))
  {
    PBM_MSG_ERR("Array out-of-bounds while writing default values for PBM features",0,0,0);
    return;
  }

  num_features_in_nv    = pbm_features_total_till_version[version_in_nv];
  num_features_in_build = pbm_features_total_till_version[version_in_build];

  /* Since pbm_features_total_till_version array is indexed by the
     feature_status_list *version*, it's value at version version_in_nv is
     essentially the first feature of the delta list...no need to bail out in
     case writing fails... */
  for(pbm_feature = num_features_in_nv; pbm_feature < num_features_in_build; pbm_feature++)
  {
    (void)pbm_nv_feature_write_default_value((pbm_nv_features_enum_type )pbm_feature);
  }
} /* pbm_nv_write_default_value_of_all_features */

/*===========================================================================
FUNCTION PBM_NV_FEATURES_STATUS_LIST_UPDATE_VERSION

DESCRIPTION
  Function used to validate the PBM Features Status NVITEM's version.
  If what's in the NV is less than the one currently defined in the build,
  updates it to the current version defined and writes it back to the NV.
  It also writes back to the NV the default values of the delta items in
  in the list.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  Version gets updated in the NV if it doesn't match the one in the build. If
  the version is updated, next time when the user reads the NVITEM, a
  dropdown of items corresponding to the updated version is shown instead of
  what the user saw the first time the NV read was done (corresponding to
  the version present in the NV at that point).
===========================================================================*/
static void pbm_nv_features_status_list_update_version(void)
{
  int status = 0;

  PBM_MSG_HIGH("PBM Features Status NV ITEM version - in NV: 0x%x, in build: 0x%x",
                 pbm_features_status_list.version,
                 PBM_FEATURES_STATUS_LIST_CURRENT_VERSION,0);

  if (pbm_features_status_list.version < PBM_FEATURES_STATUS_LIST_CURRENT_VERSION)
  {
    /* If the features_status_list version in NV is less than current
       pbm_features_status_list version in our code, meaning new features have been
       added since the version in NV till the current version, we write back
       the default value of all those delta features to the NV. */
    pbm_nv_write_default_value_of_delta_features(
      pbm_features_status_list.version,
      PBM_FEATURES_STATUS_LIST_CURRENT_VERSION);

    pbm_features_status_list.version = PBM_FEATURES_STATUS_LIST_CURRENT_VERSION;
    status = efs_put(PBM_FEATURES_STATUS_LIST_FILE, &pbm_features_status_list,
                     sizeof(pbm_features_status_list),
                     O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
    if (status != 0)
    {
      PBM_MSG_ERR("PBM Features Status NV ITEM version update failed!, error %d",
                    status,0,0);
    }
  }
} /* pbm_nv_features_status_list_update_version */


/*===========================================================================
FUNCTION PBM_NV_INIT_FEATURES_STATUS_NV_LOOKUP

DESCRIPTION
  Function used to get the status of PBM features as defined by the NVITEM
  "PBM_FEATURES_STATUS_LIST" (71535).
  The global, "features_status_list", used to hold the NVITEM, is expandable,
  yet the size is fixed. See the struct definition for more detailed doc on
  that. The global struct has a "version" item that is updated everytime we
  add new items to this global struct (starting with version 0 in the first
  implementation)
  The pbm code has full control over version of NVITEM. Taking an example,
  if in future, in version 2 of this struct(that has 10 items), the delta is 3
  items (from version 0 that had 7 items), and this build is loaded onto the
  device that already had the items set using version 0 of this NVITEM, then,
  on boot-up after loading the build, pbm will validate the version it read
  from NV. If it is not 2, it will update it to 2 and write it back to NV. At
  this point, however, the value of features/items 8 through 10 will be set to
  their default values by pbm code.

DEPENDENCIES
  Minimum QXDM version required to be able to set this NVITEM (71535) is
  QXDM 3.12.997.

RETURN VALUE
  None

SIDE EFFECTS
  "version" item in this NVITEM is writable through QXDM (no QXDM support to
  hide it or grey it out) or QCN. However if what is written explicitly doesn't
  match what's in the build, pbm will change it to what's in the build and
  it could be annoying for the QXDM user to see it change to something else
  (when the user reads the NVITEM next time).
===========================================================================*/
static void pbm_nv_init_features_status_nv_lookup(void)
{
  /* Start off with the global struct initialised to 0 */
  memset(&pbm_features_status_list, 0x00, sizeof(pbm_features_status_list));

  if (efs_get(PBM_FEATURES_STATUS_LIST_FILE, &pbm_features_status_list,
              sizeof(pbm_features_status_list))
              == sizeof(pbm_features_status_list))
  {
    PBM_MSG_HIGH("Successfully read the PBM Features Status from NV",0,0,0);
    pbm_nv_features_status_list_update_version();
  }
  else
  {
    PBM_MSG_HIGH("Failed to read PBM Features Status from NV.Writing back default values",0,0,0);

    /* Updating global pbm_features_status_list with default value of all the
       defined features */
    pbm_nv_write_default_value_of_all_features();

    /* Write features' status to the EFS, erasing previous contents,
    create if not present */
    (void)efs_put(PBM_FEATURES_STATUS_LIST_FILE, &pbm_features_status_list,
                  sizeof(pbm_features_status_list),
                  O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
  }
} /* pbm_nv_init_features_status_nv_lookup */


/*===========================================================================
FUNCTION PBM_INIT

DESCRIPTION
  This function is called during PBM task startup after the task related
  initialization is done.

  - Initialize Caches
  - Register functions with MMGSDI
  - Initialize EFS based phonebooks
  - Initialize Emergency Call Codes

  Note that UIM based phonebook is read after SIM initialization is complete.

DEPENDENCIES
  pbm_task_init has been called before.

SIDE EFFECTS
===========================================================================*/
pbm_return_type pbm_init(void)
{
   pbm_return_type ret_val;
   uint8 i;

   /* Intilizing the global last updated TxID to zero */
   pbm_free_txid();

   pbm_init_info.pbm_efs_init_done = FALSE;
   for (i = 0; i < (int) MAX_UIM_APPS; i++)
   {
      pbm_init_info.pbm_session_init_done[i] = FALSE;
   }
   for (i = 1; i < (int) MAX_UIM_APPS; i++)
   {
      memscpy(g_sim_field_info[i],sizeof(g_sim_field_info[0][0])*4,
              g_sim_field_info[i-1],sizeof(g_sim_field_info[0][0])*4);
   }
   /* pbm usim path lookup table initialization */
   for (i = 1; i < (int) MAX_UIM_APPS; i++)
   {
      memscpy(pbm_usim_path_lookup_table[i],
              sizeof(pbm_usim_path_lookup_table[0][0])*PBM_NUM_FILES_IN_LOOKUP_TABLE,
              pbm_usim_path_lookup_table[i-1],
              sizeof(pbm_usim_path_lookup_table[0][0])*PBM_NUM_FILES_IN_LOOKUP_TABLE);
   }
   pbm_init_create_conf_file();

   /* initializing the seek data */
   memset(pbm_seek_data, 0, sizeof(pbm_seek_data));

   /* initializing atomic variables */
   atomic_init(&pbm_pin1_verified, (atomic_plain_word_t) 0 );
   atomic_init(&pbm_pin2_verified, (atomic_plain_word_t) 0);

   /* Initialize caches to zero */
   ret_val = pbm_cache_init();
   PBM_CHECK_ERR_RET(ret_val, "Cache init failed");

   /* Initialize GSDI
   * UIM is initialized based on events we receive from GSDI */
   ret_val = pbm_mmgsdi_init();
   PBM_CHECK_ERR(ret_val, "GSDI init failed");

   /* Initialize EFS based phonebook */
   ret_val = pbm_efs_init();
   pbm_init_info.pbm_efs_init_done = TRUE;
   PBM_CHECK_ERR(ret_val, "EFS init failed");

   /* Initialize Emergency Call Codes */
   ret_val = pbm_ecc_init();
   PBM_CHECK_ERR(ret_val, "ECC init failed");

   /* Initialize CM client */
   ret_val = pbm_cm_init();
   PBM_CHECK_ERR(ret_val, "PBM CM init failed");

   /* Get the pbm features' status via NV look up */
   pbm_nv_init_features_status_nv_lookup();

   return PBM_SUCCESS;
}


/*===========================================================================
FUNCTION pbm_compare_and_update_file

DESCRIPTION
  This function opens the file, compares its contents against the passed
  buffer and if the two are different, empties the file and writes back
  the buffer data to the file. It also writes the buffer data to the file
  if file couldn't be read.

PARAMETERS
  file_path_ptr: File path
  buf_ptr:       Buffer to compare against the file data
  buf_len:       Buffer length

DEPENDENCIES
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None

SEE ALSO
  None
===========================================================================*/
void pbm_compare_and_update_file
(
  const char *               file_path_ptr,
  const char *               buf_ptr,
  uint16                     buf_len
)
{
  int32                   fd                    = 0;
  char                   *file_data_ptr         = NULL;
  struct fs_stat          conf_stat;


  if (!file_path_ptr || !buf_ptr || buf_len == 0)
  {
    return;
  }

  /* Get the size of the file */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if(efs_stat(file_path_ptr, &conf_stat) >= 0 &&
     conf_stat.st_size == buf_len)
  {
    /* Stat succeeded meaning the file exists - open it and read the data */
    fd = efs_open(file_path_ptr, O_RDONLY);
    if (fd < 0)
    {
      PBM_MSG_ERR("Error opening file", 0, 0, 0);
      return;
    }

    /* If the file size is non-zero, read data from file to compare against the
       buffer and write back to the file only if the data is different. If file
       read fails, still write the buffer to the file  */
    PBM_MEM_ALLOC(file_data_ptr,conf_stat.st_size)
    if (file_data_ptr != NULL)
    {
      if(efs_read(fd, file_data_ptr, conf_stat.st_size) == conf_stat.st_size)
      {
        if(memcmp(file_data_ptr, buf_ptr, buf_len) == 0)
        {
          /* file data and the buffer data are same,
             so return without updating the file */

          PBM_MEM_FREEIF(file_data_ptr);
          (void)efs_close(fd);
          return;
        }
      }
      PBM_MEM_FREEIF(file_data_ptr);
    }

    /* Close it since we will anyways have to open it again in
       TRUNC mode in order to write to it */
    (void)efs_close(fd);
  }

  /* File doesn't exist or it's contents do not match buffer data;
     write buffer to the file */
  fd = efs_open (file_path_ptr, O_WRONLY | O_TRUNC | O_CREAT);
  if (fd >= 0)
  {
     PBM_MSG_HIGH("Different conf file , fd %d buf_len %d",fd,buf_len,0);
    (void)efs_write(fd, buf_ptr, buf_len);
    (void)efs_close(fd);
  }
} /* pbm_compare_and_update_file */



/*===========================================================================
FUNCTION PBM_ECC_INIT

DESCRIPTION
  Initialize Emergency Call Codes.

  The idea is to always allow Emergency Calls through by keeping a list
  of valid Emergency Call Codes (ECC's) in NV in order for the ME to
  be able to call an emergency call even if no SIM is installed.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
static pbm_return_type pbm_ecc_init( void )
{
   pbm_phonebook_type      pb_id           = {PBM_GPB};
   pbm_return_type         ret_val         = PBM_SUCCESS;
   int                     efs_read_status = 0;

   pb_id.device_type = PBM_ECC_OTHER;
   /* Initialize Emergency Call Codes from NV */
   ret_val = pbm_efs_cache_structure_init(&ecc_other_cache,
                                          pb_id,
                                          PBM_MAX_ECC_NUMBERS,
                                          FALSE);
   if (PBM_SUCCESS != ret_val)
   {
      PBM_MSG_ERR("Error %d initializing emergency numbers", ret_val, 0, 0);
   }

   if ((efs_read_status = efs_get(PBM_CONFIG_FILE_2,
                                  &pbm_hardcode_ecc_config,
                                  sizeof(boolean)) )  < 0 )
   {
      PBM_MSG_ERR("PBM NV read failed during reading Hardcoded Config file %d", efs_read_status, 0, 0);
   }
   else
   {
      PBM_MSG_HIGH("pbm_hardcode_ecc_config from nv is 0x%x", pbm_hardcode_ecc_config, 0, 0);
   }

   ret_val = pbm_init_ecc_nv();
   if (PBM_SUCCESS != ret_val)
   {
      PBM_MSG_ERR("Error %d in loading emergency numbers from NV", ret_val, 0, 0);
   }

   return PBM_SUCCESS;
}


/*===========================================================================
FUNCTION PBM_INT_RECORD_WRITE

DESCRIPTION
  This function performs the actual write of the record to UIM or EFS.
  It is always called from within PBM Task. The address record is already
  added to cache from within the library function.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
pbm_return_type pbm_int_record_write (pbm_write_record_s_type *write)
{
   pbm_phonebook_type pb_id = {PBM_GPB};
   pbm_device_type device_type;
   pbm_record_id_type rec_id;

   pbm_return_type ret_val = PBM_ERROR ;
   pbm_addr_cache_s_type *record;
   int fields_size;
   int index;
   pbm_device_type outgoing_call_mirror[] = { PBM_LND, PBM_OCI};
   pbm_device_type incoming_call_mirror[] = { PBM_ICI};
   pbm_device_type *mirror = NULL;
   int mirror_size = 0;
   PB_CACHE *cache;

   PBM_CHECK_PTR_RET(write, PBM_ERROR);
   rec_id = write->rec_id;
   device_type = (pbm_device_type) pbm_record_id_to_device_type(rec_id);
   pb_id = pbm_session_record_id_to_phonebook_type(rec_id);

   /* This can happen when GSDI sends a refresh */
   if (PBM_SUCCESS != (ret_val = check_pb_ready(pb_id)))
   {
      pbm_write_notify(write, ret_val);
      pbm_write_cmd_free(write);
      return ret_val;
   }

   /* Mirror callhistory entry to SIM if it is not a deletion.
   * Because sizes may be different between the SIM and
   * EFS versions, deletions in one file do not necessarily mean
   * a record will be deleted from the mirror file.  Thus, if the
   * SIM file must be deleted, that needs to be done seperately.
   */
   record = write->curr_rec;
   if ( record != NULL )
   {
      if (record->num_fields)
   {
      switch (device_type)
      {
         case PBM_DIALED:
            mirror = outgoing_call_mirror;
            mirror_size = ARR_SIZE(outgoing_call_mirror);
            break;
         case PBM_RCVD:
         case PBM_MISS:
            mirror = incoming_call_mirror;
            mirror_size = ARR_SIZE(incoming_call_mirror);
            break;
         default:
            mirror = NULL;
            mirror_size = 0;
            break;
      } /*  switch(pb_id)... */

      if (NULL != mirror)
      {
         for (index = 0; index < mirror_size; index++)
         {
            pbm_phonebook_type mirror_pb_id = pb_id;
            mirror_pb_id.device_type = mirror[index];
            cache = pbm_pb_id_to_cache(mirror_pb_id);
            if (!cache)
               continue;

            if (0 < cache->num_of_records)
            {
               rec_id = PBM_SESSION_GET_FIRST_AVAILABLE;
               /* Write the fields to SIM cache too.
               * pbm_record_write() makes a copy */
               fields_size = pbm_calculate_size_from_fields(record->fields,
                                                            record->num_fields);

               ret_val = pbm_session_record_write (mirror_pb_id, &rec_id,
                                                   (uint16) PBM_CAT_NONE,
                                                   (uint32) record->num_fields,
                                                   (uint8*) record->fields,
                                                   fields_size, NULL, NULL);
               if (PBM_SUCCESS != ret_val)
               {
                  PBM_MSG_ERR("Mirroring Call hist to Session %d file failed with error %d",
                              mirror[index], ret_val, 0);
               }
            }
         }
      }
   }

   /* Now do actual write */
   switch (pb_id.device_type)
   {
      case PBM_DIALED:
      case PBM_RCVD:
      case PBM_MISS:
      case PBM_EFS:
      case PBM_SDL:
         ret_val = pbm_record_write_to_efs(write);
         break;

      case PBM_ADN:
      case PBM_SDN:
      case PBM_FDN:
      case PBM_LND:
      case PBM_MSISDN:
      case PBM_MBN:
      case PBM_GRPNAME:
      case PBM_AAS:
      case PBM_OCI:
      case PBM_ICI:
      case PBM_MBDN:
         ret_val = pbm_record_write_to_sim(write);
         break;

      default:
         PBM_MSG_ERR("Unrecognized phonebook for write, %d.",
                     pb_id.device_type, 0, 0);
         ret_val = PBM_ERROR;
         break;
   } /* end switch */
   }
   if (ret_val != PBM_SUCCESS)
   {
      pbm_write_notify(write, ret_val);
      pbm_write_cmd_free(write);
   }
   return ret_val;
}


/*===========================================================================
FUNCTION PBM_WRITE_CMD_FREE

DESCRIPTION
  This function frees any references to cache records and frees
  the command structure itself. It also signals the PBM task to go
  ahead and process the next command.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
void pbm_write_cmd_free(pbm_write_record_s_type *write)
{
   PBM_CHECK_PTR_RET(write, VOID);

   if (write->prev_rec)
      (void) pbm_cache_record_free(write->prev_rec);

   if (write->curr_rec)
      (void) pbm_cache_record_free(write->curr_rec);

   PBM_MEM_FREEIF(write->curr_usim_index);
   PBM_MEM_FREEIF(write->prev_usim_index);

   pbm_cmd_free(PBM_CMD_PTR(write));
   (void) rex_set_sigs(PBM_TCB, PBM_CMD_COMPLETE_SIG);
}


/*===========================================================================
FUNCTION PBM_WRITE_NOTIFY

DESCRIPTION
  This function sends the asynchronous result of a PBM write operation.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
void pbm_write_notify (const pbm_write_record_s_type *write,
                       pbm_return_type ret_val)
{
   pbm_notify_event_e_type event;
   pbm_writecb_data_s_type cb_data;
   pbm_session_event_data_u_type evt_data = {0};
   pbm_phonebook_type pb_id;
   PB_CACHE *cache = NULL;


   PBM_CHECK_PTR_RET(write, VOID);

   /* notify async callback passed in with write cmd */
   cb_data.ret = ret_val;
   //since this is the old rec_id we would need just 16 LSBs
   (void)pbm_new_recid_to_old_recid(&cb_data.rec_id,write->rec_id);
   //copy the new 32 bit rec_id
   cb_data.sess_rec_id = write->rec_id;
   cb_data.user_data = write->user_data;

   if (write->proc_func)
      (*write->proc_func)(&cb_data);

   /* Send notifications using registered functions */
   if (PBM_SUCCESS == ret_val)
   {
      /* Check # fields to find if there is a record.
      * If both current and previous records are present, this is an update.
      * If current record is present and previous record is not present,
      * this is an add operation. Otherwise this is a deletion.
      */
      if (write->curr_rec && write->curr_rec->num_fields)
      {
         if (write->prev_rec && write->prev_rec->num_fields)
            event = PBM_EVENT_REC_UPDATE;
         else
            event = PBM_EVENT_REC_ADD;
      } else
      {
         event = PBM_EVENT_REC_DELETE;
      }
   } else
   {
      event = PBM_EVENT_REC_FAILED;
   }
   evt_data.rec_id = write->rec_id;
   pb_id = pbm_session_record_id_to_phonebook_type(write->rec_id);
   cache = pbm_pb_id_to_cache(pb_id);
   PBM_CHECK_PTR_RET(cache,VOID);

   if ( !( (cache->status == PBM_STATUS_INIT_SYNC) && (pb_id.device_type == PBM_ADN)))
   {
   pbm_add_notify(event, evt_data);
}
   else
   {
      PBM_MSG_HIGH("Event not notified for rec_id 0x%x",write->rec_id,0,0);
   }
}


/*===========================================================================
FUNCTION PBM_LIST_BACKUP

DESCRIPTION
  This function makes sure that there is a valid entry in the committed list for
  this record id. If there are no pending writes on this record, the current
  cache entry is saved to committed list. If the previous entry is a NULL, we
  replace it with a dummy entry.

DEPENDENCIES
  Cache should be initialized before. Record id should be valid.
  Should be called from within pbm_crit_sect.

SIDE EFFECTS
===========================================================================*/
pbm_return_type pbm_list_backup(pbm_record_id_type rec_id)
{
   PB_CACHE *cache = pbm_pb_id_to_cache(pbm_session_record_id_to_phonebook_type(rec_id));
   uint16 location = pbm_record_id_to_location(rec_id);
   pbm_addr_node_s_type *curr_node = NULL;
   pbm_addr_node_s_type *prev_node = NULL;
   pbm_addr_node_s_type *node = NULL;
   pbm_addr_cache_s_type *dummy = NULL;
   int committed_list_size = 0;

   PBM_CHECK_PTR_RET(cache, PBM_ERROR);

   if (PBM_SESSION_GET_FIRST_AVAILABLE == rec_id)
      return PBM_ERROR;

   /* Search for a node with given record id or go till the end */
   curr_node = pbm_committed_list;
   while (curr_node)
   {
      if (pbm_compare_rec_ids(curr_node->rec_id , rec_id))
         return PBM_SUCCESS; /* We are done */
      prev_node = curr_node;
      curr_node = curr_node->next;
   }

   /* Allocate a new node */
   PBM_MEM_ALLOC(node, sizeof(pbm_addr_node_s_type));
   PBM_CHECK_PTR_RET(node, PBM_ERROR_MEM_FULL);

   /* If entry is NULL, add a dummy entry in cache for future comparison */
   if (!cache->pb_cache_array[location])
   {
      dummy = pbm_cache_record_new();
      if (!dummy)
      {
         PBM_MSG_ERR("Unable to allocate dummy record for 0x%x", rec_id, 0, 0);
         PBM_MEM_FREEIF(node);
         return PBM_ERROR_MEM_FULL;
      }
      pbm_cache_record_fill(dummy, rec_id, 0, 0, 0, NULL);
      cache->pb_cache_array[location] = dummy;
   }

   node->rec_id = rec_id;
   node->record = pbm_cache_record_get(rec_id);
   node->next = NULL;

   /* Add node to committed list (end of list) */
   if (prev_node)
      prev_node->next = node;
   else
      pbm_committed_list = node;

   /* check committed list size */
   node = pbm_committed_list;
   committed_list_size = 0;
   while (node != NULL)
   {
      committed_list_size++;
      node = node->next;
   }
   if (committed_list_size > 10)
   {
      PBM_MSG_ERR("Committed list unusual size %d", committed_list_size, 0, 0);
   }
   return PBM_SUCCESS;
}


/*===========================================================================
FUNCTION PBM_LIST_UPDATE

DESCRIPTION
  This function updates the PBM committed list after a successful write.

  If the cache entry is same as current write record, there are no other
  pending writes. So we delete the corresponding node from within PBM
  committed list. A dummy record wthin cache is also relaplced with the
  original NULL entry.

  If cache entry is different, we update the corresponding node within
  committed list with the latest record.

DEPENDENCIES
  No critical section dependencies.

SIDE EFFECTS
===========================================================================*/
void pbm_list_update(const pbm_write_record_s_type *write)
{
   pbm_addr_cache_s_type *cache_rec;
   PB_CACHE *cache;
   pbm_addr_node_s_type *curr_node = NULL;
   pbm_addr_node_s_type *prev_node = NULL;
   boolean found = FALSE;
   uint16 location;

   PBM_CHECK_PTR_RET(write, VOID);

   cache = pbm_pb_id_to_cache(pbm_session_record_id_to_phonebook_type(write->rec_id));
   PBM_CHECK_PTR_RET(cache, VOID);

   location = pbm_record_id_to_location(write->rec_id);
   if (location > cache->num_of_records)
   {
      PBM_MSG_HIGH("Location for record ID out of bounds %d of %d",
                   write->rec_id, location, cache->num_of_records);
      return;
   }

   rex_enter_crit_sect(&pbm_crit_sect);
   cache_rec = cache->pb_cache_array[location];

   if (!cache_rec)
   {
      PBM_MSG_HIGH("NULL cache entry found for record id %d",
                   write->rec_id, 0, 0);
      rex_leave_crit_sect(&pbm_crit_sect);
      return;
   }

   /* Search for the record id within committed list */
   PBM_LIST_SEARCH(write->rec_id, prev_node, curr_node, found);
   if (!found || !curr_node)
   {
      PBM_MSG_HIGH("Can't find entry in committed list for rec 0x%x, node 0x%x",
                   write->rec_id, curr_node, 0);
      rex_leave_crit_sect(&pbm_crit_sect);
      return;
   }

   if (cache_rec == write->curr_rec)
   {
      /* There are no more pending writes.
      * Remove node from committed list. */
      (void) pbm_cache_record_free(curr_node->record);
      PBM_LIST_REMOVE(prev_node, curr_node);

      /* Remove dummy record */
      if (!cache_rec->num_fields)
      {
         (void) pbm_cache_record_free(cache_rec);
         cache->pb_cache_array[location] = NULL;
      }
   } else
   {
      /* Update node with new record */
      (void) pbm_cache_record_free(curr_node->record);
      curr_node->record = write->curr_rec;
      curr_node->record->nref++;
   }
   rex_leave_crit_sect(&pbm_crit_sect);
}


/*===========================================================================
FUNCTION PBM_LIST_RECOVER

DESCRIPTION
   This function is called to recover a cache entry in response to a write error.
   The entry is obtained from the PBM committed list.

   If cache entry is same as current write record, there are no other pending
   writes. In this case we have to do the recovery operation. In the normal case
   we take the record from the corresponding node in commit list and add it to
   the cache.

   If the current operation was partially successful ie deletion was successful
   and add failed, we delete the current cache entry. This can happen only
   for an Update operation on EFS or while adding USIM entries.

   In both cases above, we no longer need the node in commiitted list after this
   and hence the node is removed. Also, if the entry in cache is a dummy record,
   it is converted to a NULL.

DEPENDENCIES
   None. No need to be in critical section to call this function.

SIDE EFFECTS
===========================================================================*/
void pbm_list_recover (const pbm_write_record_s_type *write,
                       boolean partial_success)
{
   PB_CACHE *cache;
   pbm_addr_cache_s_type *cache_rec;
   pbm_addr_node_s_type *curr_node = NULL;
   pbm_addr_node_s_type *prev_node = NULL;
   uint16 location;
   boolean found;
   pbm_return_type ret;

   PBM_CHECK_PTR_RET(write, VOID);

   cache = pbm_pb_id_to_cache(pbm_session_record_id_to_phonebook_type(write->rec_id));
   PBM_CHECK_PTR_RET(cache, VOID);
   PBM_CHECK_PTR_RET(cache->pb_cache_array, VOID);

   location = pbm_record_id_to_location(write->rec_id);
   rex_enter_crit_sect(&pbm_crit_sect);

   cache_rec = cache->pb_cache_array[location];
   if (!cache_rec)
   {
      PBM_MSG_HIGH("NULL cache entry found for record %d", write->rec_id, 0, 0);
      rex_leave_crit_sect(&pbm_crit_sect);
      return;
   }

   /* make sure there isn't another write to the same record pending */
   if (write->curr_rec == cache_rec)
   {
      /* Search for the record id within committed list */
      PBM_LIST_SEARCH(write->rec_id, prev_node, curr_node, found);
      if (!found || !curr_node)
      {
         PBM_MSG_HIGH("Can't find entry in committed list- rec 0x%x, node 0x%x",
                      write->rec_id, curr_node, 0);
         rex_leave_crit_sect(&pbm_crit_sect);
         return;
      }

      if (partial_success)
      {
         if ((write->curr_rec && pbm_is_usim_required(write->rec_id,
                                                      write->curr_rec->fields,
                                                      write->curr_rec->num_fields)) ||
             (write->prev_rec && pbm_is_usim_required(write->rec_id,
                                                      write->prev_rec->fields,
                                                      write->prev_rec->num_fields)))
         {
            /*Recover USIM record when Add, Delete or Update operation failed*/
            ret = pbm_recover_usim_record(write);
            PBM_CHECK_ERR(ret, "USIM Record Recovery after error failed");
         } else
         {
            /* Deletion was successful and Add failed for EFS entries.
            * We ignore the commit list in this case. */
            ret = pbm_cache_delete(cache, write->rec_id);
            PBM_CHECK_ERR(ret, "Recovery after partial update failed");
         }
      } else
      {
         /* Normal recovery case when Add, Delete or Update operation failed.
         * In this case we add entry from commit list to the cache. */
         ret = pbm_cache_add(cache, curr_node->record, FALSE);
         PBM_CHECK_ERR(ret, "Recovery after error failed");
         curr_node->record->nref++;
      }

      /* we are done with the node in committed list, so it can be removed */
      (void) pbm_cache_record_free(curr_node->record);
      PBM_LIST_REMOVE(prev_node, curr_node);

      /* Remove dummy record, if present */
      if (cache->pb_cache_array[location] &&
          !cache->pb_cache_array[location]->num_fields)
      {
         (void) pbm_cache_record_free(cache->pb_cache_array[location]);
         cache->pb_cache_array[location] = NULL;
      }
   }
   rex_leave_crit_sect(&pbm_crit_sect);
}

/*===========================================================================
FUNCTION PBM_RECOVER_USIM_RECORD

DESCRIPTION
   Restores pb cache to previous entry.  Creates a write command for restoring
   data written to SESSION.  Backup list mechanism is not used, so if this write
   command request fails, then we give up on trying to fix it.

DEPENDENCIES
   Should be called only from PBM task and from pbm_list_recover.  Only used when
   failure adding, deleting, or updating fails on a current or previous record
   entry that contains a USIM field.

SIDE EFFECTS
===========================================================================*/
static pbm_return_type pbm_recover_usim_record (const
                                                pbm_write_record_s_type *write)
{
   pbm_write_record_s_type *write_cmd = NULL;
   pbm_phonebook_type pb_id;
   PB_CACHE                *cache = NULL;
   pbm_return_type          ret;
   pbm_addr_cache_s_type   *prev_rec = NULL;
   pbm_addr_cache_s_type   *record   = NULL;
   uint16                   unique_id;
   uint8                   *curr_index = NULL;
   uint8                   *prev_index = NULL;

   PBM_CHECK_PTR_RET(write, PBM_ERROR);
   PBM_CHECK_PTR_RET(write->prev_rec, PBM_ERROR);
   pb_id = pbm_session_record_id_to_phonebook_type(write->rec_id);
   cache = pbm_pb_id_to_cache(pb_id);

   if (!cache)
   {
      PBM_MSG_ERR("Invalid phonebook id %d", pb_id.device_type, 0, 0);
      return PBM_ERROR;
   }

   do
   {
      /* Allocate index array if USIM fields are present in previous or current
      * record and fill it.
      * There's a race condition here.  If you write 2 records back to back
      * that both require allocating USIM fields, the second one will get the
      * same copy of the IAP, and will overwrite the first record's fields */
      if (!pbm_allocate_index(write->rec_id, write->prev_rec->fields,
                              write->prev_rec->num_fields, &prev_index, &curr_index) ||
          (curr_index && !pbm_get_usim_index(write->rec_id,
                                             write->prev_rec->fields, write->prev_rec->num_fields, curr_index))
          || (prev_index && !pbm_copy_usim_index(write->rec_id, prev_index)))
      {
         PBM_MSG_ERR("No space available for USIM fields record %d %d %d",
                     write->rec_id, prev_index, curr_index);
         ret = PBM_ERROR_MEM_FULL;
         break;
      }

      /* Allocate PBM write command */
      write_cmd = (pbm_write_record_s_type *)
                  pbm_cmd_alloc(sizeof(pbm_write_record_s_type));
      if (!write_cmd)
      {
         PBM_MSG_ERR("Unable to allocate PBM command", 0, 0, 0);
         ret = PBM_ERROR_MEM_FULL;
         break;
      }

      /* Get a reference to failed current record.
      This will now be our previous record entry for clean up purposes*/
      prev_rec = pbm_cache_record_get(write->rec_id);
      if (!prev_rec)
      {
         PBM_MSG_ERR("Unexpected NULL previous rec 0x%x", write->rec_id, 0, 0);
         ret = PBM_ERROR;
         break;
      }

      /* Use our old record as our restoring record */
      record = write->prev_rec;
      if (!record)
      {
         PBM_MSG_ERR("Unable to allocate PBM command", 0, 0, 0);
         ret = PBM_ERROR;
         break;
      }

      if (pb_id.device_type == PBM_ADN)
      {
         if (prev_rec->num_fields) /* deletion or update */
         {
            (void) pbm_uim_generate_sync_values(cache, &write_cmd->sync, FALSE);
            unique_id = prev_rec->unique_id;
            /* we didn't generate new UID, so we don't care about wrap around */
         } else
         {
            unique_id = pbm_uim_generate_sync_values(cache,
                                                     &write_cmd->sync, TRUE);
            if (!unique_id)
            {
               /* Do not allow unique id to wrap around */
               PBM_MSG_ERR("Unable to allocate SIM unique id", 0, 0, 0);
               /* This could return an error as well, but we don't use SIM UID
                * anywhere, so let's just keep going
                * This really shouldn't happen, as we regerate Sim UIDs when we
                * detect rollover inside pbm_uim_generate_sync_values() */
            }
         }
      } else
      {
         unique_id = 0xFFFF;
      }
      record->unique_id = unique_id;

      /* Restore prevoius record into pbm cache */
      ret = pbm_cache_add(cache, record, FALSE);
      if (ret != PBM_SUCCESS)
      {
         PBM_MSG_ERR("Cache add failed with error %d.", ret, 0, 0);
         break;
      }

      /* pbm_cache_add doesn't do this, so we need to do this here. */
      record->nref++;

      /* Allocate index array if USIM fields are present in previous or current
      * record and fill it */
      if (pbm_master_usim_cache_update(write->rec_id, prev_index, curr_index)
          != PBM_SUCCESS)
      {
         (void) pbm_cache_delete(cache, write->rec_id);
         PBM_MSG_ERR("Unable to update master usim cache for %d",
                     write->rec_id, 0, 0);
         ret = PBM_ERROR;
         break;
      }

      /* Build and Send restoring PBM command */
      write_cmd->cmd_type   = PBM_CMD_WRITE;
      write_cmd->rec_id     = write->rec_id;
      write_cmd->prev_rec   = prev_rec;
      write_cmd->proc_func  = NULL;
      write_cmd->user_data  = NULL;
      write_cmd->curr_rec        = pbm_cache_record_get(write->rec_id);
      write_cmd->curr_usim_index = curr_index;
      write_cmd->prev_usim_index = prev_index;
      pbm_client_cmd_q_put(PBM_CMD_PTR(write_cmd));

      ret = PBM_SUCCESS;
   } while (0);

   /* Error Handling */
   if (ret != PBM_SUCCESS)
   {
      pbm_free_index(prev_index, curr_index);
      if (write_cmd)
         pbm_cmd_free(PBM_CMD_PTR(write_cmd));
      if (prev_rec)
         (void) pbm_cache_record_free(prev_rec);
      if (record)
         (void) pbm_cache_record_free(record);
   }
   return ret;
}

/*===========================================================================
FUNCTION PBM_CM_INIT

DESCRIPTION
   Initialize CM Client, for network emergency numbers.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
static pbm_return_type pbm_cm_init ( void )
{

   cm_client_status_e_type cm_status;
   cm_status = cm_client_init(CM_CLIENT_TYPE_PBM, &pbm_cm_client_id);
   if (cm_status != CM_CLIENT_OK)
   {
      PBM_MSG_ERR("cm_client_init failed, %d %d",cm_status,pbm_cm_client_id,0);
      return PBM_ERROR;
   }
   cm_status = cm_mm_client_ss_reg_msim(pbm_cm_client_id, pbm_cm_ss_event_cb,
                                   CM_CLIENT_EVENT_REG, CM_SS_EVENT_EMERG_NUM_LIST,
                                   CM_SS_EVENT_EMERG_NUM_LIST, NULL, 
                                   SYS_MODEM_AS_ID_MASK_ANY);

   if (CM_CLIENT_OK != cm_status)
   {
      PBM_MSG_ERR("cm_mm_client_ss_reg_msim failed, %d %d", cm_status,
                  pbm_cm_client_id, 0);
      return PBM_ERROR;
   }

   cm_status = cm_client_act(pbm_cm_client_id);
   if (CM_CLIENT_OK != cm_status)
   {
      PBM_MSG_ERR("cm_client_act failed,%d %d", cm_status, pbm_cm_client_id, 0);
      return PBM_ERROR;
   }
   return PBM_SUCCESS;
}

/*===========================================================================
FUNCTION pbm_handle_cm_ss_evt

DESCRIPTION
   This function is called when we get an SS event.
   This will only look at the emergency number event, and add those numbers to
   the emergency number phonebook.


DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
void pbm_handle_cm_ss_evt (pbm_cm_ss_evt_type* cm_ss_evt_ptr)
{
   int i,j;
   PB_CACHE *ecc_cache = NULL;
   pbm_return_type ret_val=PBM_SUCCESS;
   switch (cm_ss_evt_ptr->ss_event)
   {
      case CM_SS_EVENT_EMERG_NUM_LIST:
      {
         cm_mm_msim_ss_info_s_type *ii_info_ptr = cm_ss_evt_ptr->ii_info_ptr;
         pbm_session_enum_type sess_type ;
         PBM_CHECK_PTR_RET(ii_info_ptr, VOID);
         sess_type = pbm_mapping_as_id_to_pbm_session(ii_info_ptr->asubs_id);
         PBM_MSG_HIGH("PBM received CM SS event for sess_type %d as_id %d ",sess_type ,ii_info_ptr->asubs_id,0);
         if ( sess_type != PBM_SESSION_DEFAULT )
         {
           rex_enter_crit_sect(&pbm_crit_sect);
           pbm_ecc_clear_all_id(sess_type, PBM_FIELD_NETWORK_ECC);
           for (i=0; i < ii_info_ptr->number_of_stacks && i < CM_NO_STACKS; i++)
           {
              PBM_MSG_HIGH("PBM CM SS stack %d list_len %d ",i ,ii_info_ptr->stack_info[i].emerg_num_list.num_list_len ,0);
              for ( j = 0 ; j < ii_info_ptr->stack_info[i].emerg_num_list.num_list_len && j < CM_MAX_EMERGENCY_NUM_COUNT ; j ++ ) 
              {
                char emerg_num[sizeof(ii_info_ptr->stack_info[i].emerg_num_list.num_list[j].num.buf)+1];
                uint16 emerg_num_len = ii_info_ptr->stack_info[i].emerg_num_list.num_list[j].num.len;

                if (emerg_num_len == 0)
                {
                  PBM_MSG_ERR("Emergency number length is 0 for item %d of %d stack of len %d", j,i,
                  ii_info_ptr->stack_info[i].emerg_num_list.num_list_len);
                      continue;
                }

                if (emerg_num_len >= sizeof(emerg_num))
                {
                  PBM_MSG_ERR("ECC num len from cm is too large, allow %d, reported %d",
                               sizeof(emerg_num)-1, emerg_num_len, 0);
                  emerg_num_len = sizeof(emerg_num) - 1;
                }

                memset(emerg_num, 0, sizeof(emerg_num));
                (void)memscpy(emerg_num,
                              sizeof(emerg_num),
                              (char*)ii_info_ptr->stack_info[i].emerg_num_list.num_list[j].num.buf,
                              emerg_num_len);

                ret_val = pbm_build_ecc_record(PBM_FIELD_NETWORK_ECC,
                                               emerg_num_len + 1, /*Add 1 for NULL*/
                                               emerg_num, 0, NULL, TRUE,
                                               (uint8)ii_info_ptr->stack_info[i].emerg_num_list.num_list[j].num_type,
                                                sess_type,
                                                EMERGENCY_GW);
                if (PBM_SUCCESS != ret_val)
                {
                  PBM_MSG_ERR("Error %d in building network ECC record %d of %d stack", ret_val, i, j);
                   break;
                }
              }
           }
           rex_leave_crit_sect(&pbm_crit_sect);
           ecc_cache = pbm_file_id_to_cache(sess_type, PBM_FILE_ECC);
           PBM_SET_PB_CACHE_READY(ecc_cache);
        }
      }
      break;

      default:
         break;
   }
}

/*===========================================================================
FUNCTION pbm_wms_cfg_cb

DESCRIPTION
WMS callback.  Used to start reading from SIM.

DEPENDENCIES

SIDE EFFECTS
===========================================================================*/
void pbm_wms_cfg_cb (pbm_wms_resp_s_type* wms_resp_ptr)
{
  pbm_session_enum_type pbm_session = PBM_SESSION_GPB_1;

  pbm_session = pbmif_mmgsdi_session_to_pbm_session(wms_resp_ptr->info_ptr.session_type );
  PBM_MSG_HIGH("PBM Receieved WMS session %d TxId %d PBM Last TxId %d",
                wms_resp_ptr->info_ptr.session_type,wms_resp_ptr->info_ptr.t_id,
                pbm_session_info[pbm_session].last_txid);
  if(pbm_session != PBM_SESSION_MAX && 
     pbm_session_info[pbm_session].last_txid == wms_resp_ptr->info_ptr.t_id)
  {
    PBM_SESSION_SET(pbm_wms_init_finished,pbm_session);
    PBM_MSG_HIGH("PBM Receieved WMS Ready Event:pbm_wms_init_finished %d pbm_session_subs_ready %d pbm_session %d",
                  pbm_wms_init_finished,pbm_session_subs_ready,pbm_session);
    if(pbm_session_subs_ready & PBM_SESSIONID_TO_SESSION_MASK(pbm_session))
    {
      PBM_MSG_HIGH("PBM session %d in_use_adn_pb %d pbm_session_subs_ready %d",
                    pbm_session ,pbm_session_info[pbm_session].in_use_adn_pb,pbm_session_subs_ready );
      if ( pbm_session_info[pbm_session].in_use_adn_pb != PBM_PB_NOT_KNOWN )
      {
        pbm_make_fileinfo_request(pbm_session,0);
      }
    }
  }
  else
  {
    PBM_MSG_HIGH("PBM: Session or TxId mismatch for WMS Ready pbm_session %d",pbm_session,0,0);
  }
}


/*===========================================================================
FUNCTION PBM_POPULATE_ECC_OTHER_IF_PIN_STATUS_CHANGE

DESCRIPTION
  This function takes effect when ecc_hardcode_sim_pin_lock_feature is enabled 
  along with NV 69736 pbm_hardcode_ecc_config

  This will return TRUE if hardcode cache is re-built and FALSE if it is NOT re-built

PARAMETERS

SIDE EFFECTS

===========================================================================*/
boolean  pbm_populate_ecc_other_if_pin1_status_change(atomic_word_t pin1_prev_status)
{
  boolean   ecc_sim_pin_lock_feature = (pbm_nv_get_feature_status(PBM_FEATURE_ECC_HARDCODE_SIM_ABSENT_LIST_TILL_PIN_LOCK)) ? TRUE : FALSE;
  boolean   ret_val                  = FALSE ; 

  PBM_MSG_HIGH("In pbm_populate_ecc_other_if_pin1_status_change pbm_hardcode_ecc_config 0x%x", pbm_hardcode_ecc_config, 0, 0);

  if(pbm_hardcode_ecc_config && ecc_sim_pin_lock_feature)
  {
     boolean prev_pin_state    = ( pin1_prev_status.value & PBM_MASK_CARD_SESSION_PIN_VALUES ) ? TRUE : FALSE ;
     boolean updated_pin_state = ( pbm_pin1_verified.value & PBM_MASK_CARD_SESSION_PIN_VALUES ) ? TRUE : FALSE ; 

     PBM_MSG_HIGH("Masked prev pin 0x%x and current pin 0x%x ",(pin1_prev_status.value & PBM_MASK_CARD_SESSION_PIN_VALUES) ,
                                                              (pbm_pin1_verified.value & PBM_MASK_CARD_SESSION_PIN_VALUES),
                                                               0);
     if (prev_pin_state != updated_pin_state)
     {
       pbm_ecc_set_hardcoded_eccs(TRUE,FALSE);
       ret_val = TRUE ;
     }
  }

  return ret_val;
} /* pbm_populate_ecc_other_if_pin1_status_change */

