/*==============================================================================
  FILE:         uim_nvcfg.c

  OVERVIEW:     FIle conatins the functions to intilalize, set and get the nv
                items.

  DEPENDENCIES: N/A

                Copyright (c) 2014 QUALCOMM Technologies, Inc.
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
==============================================================================*/

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimdrv/src/uim_nvcfg.c#4 $
$DateTime: 2016/12/13 21:37:18 $
$Author: gmotika $

when        who        what, where, why
------      ----       ---------------------------------------------------------
12/14/16    gm         Disable UIMDRV_FEATURE_LOG_APDU_TO_EFS by default
04/05/16    ks         Delay local card power up in Modem SSR in BTSAP mode
03/02/15    ks         SIM tray by tieing the card detect gpios
11/05/14    na         Support of le for stream 7816 apdu controlled with NV
11/05/14    na         Attempting power-up with 3v after few recoveries
11/05/14    na         Adding support to trigger recovery on reciept of bad status word
07/15/14    lxu        Increase usb uicc command time out to 16 seconds by default
06/11/14    lxu        Read cmd_rsp_time from nv for usb mode, give cmd_rsp_time and
                       voltage_class to ap when send usb power up to ap
05/28/14    ll         Write NV 4205 with default values if not initialized
04/10/14    ak         Fix compile error when FEATURE_UIM_SUPPORT_DUAL_SLOTS is defined
03/31/14    ll         Change stop at infinite NULL flag name for readability
03/27/14    lxu        USB UICC modem implement
03/21/14    ll         Remove return_ptr and remove/change uim_free to UIM_FREE
02/20/14    ks         Provision for explicit MF or ADF selction
                       And provision for inverse convention bootup
02/18/14    akv        Version needed updating for UIMDRV features NV
02/08/14    sam        Fix for Klocwork issues
01/15/14    yk         The first revision
==============================================================================*/
#include "rex.h"
#include "fs_public.h"

#include "uim_variation.h"
#include "uimi.h"
#include "uimgen.h"
#include "uimglobals.h"
#include "uim_remote.h"
#include "uim_hotswap.h"
#ifdef FEATURE_UIM_DS_SUBSCRIPTION_MANAGER
#include "uimsub_manager.h"
#endif /* FEATURE_UIM_DS_SUBSCRIPTION_MANAGER */
#include "uim_nvcfg.h"

/* This should be incremented whenever there's a change to struct
   uim_features_status_list_type */
#define UIM_FEATURES_STATUS_LIST_CURRENT_VERSION    11
/* This should be incremented whenever there's a change to struct
   uim_feature_support_hotswap_type */
#define UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION 0

#define UIM_FEATURES_STATUS_LIST_FILE     "/nv/item_files/modem/uim/uimdrv/uim_features_status_list"
#define UIM_FEATURE_SUPPORT_HOTSWAP_FILE  "/nv/item_files/modem/uim/uimdrv/feature_support_hotswap"
#define UIM_HW_CONFIG_NV_EF               "/nv/item_files/modem/uim/uimdrv/uim_hw_config"

/* Total # of features at each version of NVITEM.. starting with VERSION 0 */
#define UIM_FEATURES_STATUS_LIST_VERSION0_COUNT    15
#define UIM_FEATURES_STATUS_LIST_VERSION1_COUNT    16
#define UIM_FEATURES_STATUS_LIST_VERSION2_COUNT    17
#define UIM_FEATURES_STATUS_LIST_VERSION3_COUNT    18
#define UIM_FEATURES_STATUS_LIST_VERSION4_COUNT    19
#define UIM_FEATURES_STATUS_LIST_VERSION5_COUNT    21
#define UIM_FEATURES_STATUS_LIST_VERSION6_COUNT    23
#define UIM_FEATURES_STATUS_LIST_VERSION7_COUNT    24
#define UIM_FEATURES_STATUS_LIST_VERSION8_COUNT    25
#define UIM_FEATURES_STATUS_LIST_VERSION9_COUNT    26
#define UIM_FEATURES_STATUS_LIST_VERSION10_COUNT   27
#define UIM_FEATURES_STATUS_LIST_VERSION11_COUNT   28
#define UIM_FEATURE_SUPPORT_HOTSWAP_VERSION0_COUNT 1

#define UIM_FEATURES_VERSION_COUNT                 12
#define UIM_HOTSWAP_FEATURES_VERSION_COUNT         1

/* Default value of each uimdrv feature controlled by
  UIM_FEATURES_STATUS_LIST NVITEM */
#define UIMDRV_FEATURE_HANDLE_NO_ATR_IN_40000_CLK_CYCLES_DEFAULT         FALSE
#define UIMDRV_FEATURE_LOG_TO_EFS_DEFAULT                                TRUE
#define UIMDRV_FEATURE_DISABLE_RECOVERY_UPON_INFINITE_NULL_DEFAULT       FALSE
#define UIMDRV_FEATURE_DEBUG_LOG_DEFAULT                                 TRUE
#define UIMDRV_FEATURE_RPT_BAD_SW_ON_POLL_DEFAULT                        FALSE
#define UIMDRV_FEATURE_HANDLE_ICCID_READ_FAILURE_DEFAULT                 FALSE
#define UIMDRV_FEATURE_SUPPORT_NO_ICCID_DEFAULT                          TRUE
#define UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT_DEFAULT                     FALSE
#define UIMDRV_FEATURE_HANDLE_UNKNOWN_PROC_BYTES_AS_CMD_TIMEOUT_DEFAULT  FALSE
#define UIMDRV_FEATURE_INTERFACE_NOT_USED_DEFAULT                        FALSE
#define UIMDRV_FEATURE_LOG_APDU_TO_EFS_DEFAULT                           FALSE
#define UIMDRV_FEATURE_NO_SWITCH_INST_ON_WWT_EXPIRY_DEFAULT              FALSE
#define UIMDRV_FEATURE_SEND_STATUS_BEFORE_AUTH_DEFAULT                   FALSE
#define UIMDRV_FEATURE_TRY_DEFAULT_BAUD_RATE_FOR_F372_D12_CARD_DEFAULT   FALSE
#define UIMDRV_FEATURE_COLD_RESET_DUE_TO_CARD_SWITCH_DEFAULT             FALSE
#define UIMDRV_FEATURE_SM_USE_SLOT_1_DEFAULT                             FALSE
#define UIMDRV_FEATURE_USE_DUAL_LDO_DEFAULT                              FALSE
#define UIMDRV_FEATURE_UIM_POLLING_ONLY_AT_POLLING_TIMER_EXPIRY_DEFAULT  FALSE
#define UIMDRV_FEATURE_UIM_SET_CLK_FREQ_AT_4_8_MHZ_DEFAULT               TRUE
#define UIMDRV_FEATURE_HANDLE_TC1_BYTE_FOR_EXTRA_GUARD_TIME_DEFAULT      FALSE
#define UIMDRV_FEATURE_ENABLE_SIM_MODE_CHANGE_VIA_WARM_RESET_DEFAULT     FALSE
#define UIMDRV_FEATURE_ENABLE_EXPLICIT_SELECTION_OF_MF_OR_ADF_DEFAULT    FALSE
#define UIMDRV_FEATURE_ENABLE_BOOT_UP_IN_INVERSE_CONVENTION_DEFAULT      FALSE
#define UIMDRV_FEATURE_RECOVERY_ON_BAD_STATUS_WORD_DEFAULT               FALSE
#define UIMDRV_FEATURE_ATTEMPT_PUP_3V_FROM_nTH_RECOVERY_DEFAULT          0
#define UIMDRV_FEATURE_LE_SUPPORT_FOR_7816_STREAM_APDU_DEFAULT           TRUE
#define UIMDRV_FEATURE_DISABLE_CARD_STATUS_CHECK_AT_POWER_UP_DEFAULT     FALSE
#define UIMDRV_FEATURE_SIMTRAY_WITH_GPIOS_TIED_DEFAULT                   FALSE

/*=============================================================================
  ENUM:   UIM_NV_FEATURE_SUPPORT_HOTSWAP_ENUM_TYPE
=======================================================================*/
typedef enum {
  UIMDRV_FEATURE_SUPPORT_HOTSWAP = 0
} uim_nv_feature_support_hotswap_enum_type;

/*=============================================================================
  ENUM:   For all the "version" based efs nv items
=======================================================================*/
typedef enum {
  UIMDRV_FEATURES_STATUS_NVITEM_ENUM = 0,
  UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM,
  UIMDRV_MAX_NVITEM_ENUM
} uim_nvitems_enum;

#ifdef FEATURE_UIM_POLLING_DISABLE_CAPABILITY
#error code not present
#endif /* FEATURE_UIM_POLLING_DISABLE_CAPABILITY */

/* EFS item to configrue Busy Response timers */
#define UIM_FEATURE_BUSY_RESPONSE_FILE                 "/nv/item_files/modem/uim/uimdrv/sim_response_timers_config"
/* Default value of uim_ind_timer in case the EFS item is not set */
#define UIM_BUSY_IND_TIMER_VAL_MS_DEFAULT              15000
/* Default value of uim_trans_timer in case the EFS item is not set */
#define UIM_TRANS_TIMER_VAL_MS_DEFAULT                 120000

/* EFS item to configrue Busy Response simulate NULL */
#define UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE   "/nv/item_files/modem/uim/uimdrv/uim_busy_response_simulate_null_config"

/* JCDMA Feature */
/* JCDMA ef file path */
#define UIM_JCDMA_MODE_FILE   "/nv/item_files/jcdma/uim_jcdma_mode"
#define UIM_CONF_DIR1         "/nv"
#define UIM_CONF_DIR2         "/nv/item_files"
#define UIM_CONF_DIR3         "/nv/item_files/conf"

#define UIM_CONF_FILE    "/nv/item_files/conf/uimdrv.conf"

/* NV to configure max technical problem count */
#define UIM_PDOWN_NV_EF  "/nv/item_files/modem/uim/uimdrv/nv_pdown_uim_consecutive_techproblems"

/* Subscription Manager: Active slot configuration*/
#define UIM_ACTIVE_SLOT_NV  "/nv/item_files/modem/uim/uimdrv/nv_active_slot_configuration"

/* Hotswap: NV to configure Debounce Logic*/
#define UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE    "/nv/item_files/modem/uim/uimdrv/me_hotswap_configuration"

/* All of UIM instances shall have to read the NV file separately. Although the
   contents may be same but it will help in avoding race conditions.
   Else there has to be a signalling mechanism between two tasks
   to convey if the NV files have been read or not.
   */

/*---------------------------------------------------------------------------
  List of total number of UIMDRV features present in NVITEM in each version
  of uimdrv_feature_status_list. Whenever a newer version of the NVITEM is
  exposed, size of the array grows by 1...So, if 8 features/items were added
  in VERSION0 and 2 in VERSION1, the array should have 2 items listing the
  number of total features at each version.. {8,10} in this case...
---------------------------------------------------------------------------*/
uint8 uimdrv_features_total_till_version[UIM_FEATURES_VERSION_COUNT] =
{
  UIM_FEATURES_STATUS_LIST_VERSION0_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION1_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION2_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION3_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION4_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION5_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION6_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION7_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION8_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION9_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION10_COUNT,
  UIM_FEATURES_STATUS_LIST_VERSION11_COUNT
};

uint8 uimdrv_feature_support_hotswap_total_till_version[UIM_HOTSWAP_FEATURES_VERSION_COUNT] =
{
  UIM_FEATURE_SUPPORT_HOTSWAP_VERSION0_COUNT
};


/**
 * DECLARATIONS OF INTERNAL FUNCTIONS
 */
static void uim_read_nv_from_efs(uim_instance_global_type  *uim_ptr);

static void uim_init_nv_globals_with_read_values_from_efs
(
  uim_instance_global_type *uim_ptr
);

static void uim_read_legacy_nv
(
  uim_instance_global_type *uim_ptr,
  rex_tcb_type             *task_ptr,
  rex_sigs_type            task_wait_sig,
  void                    (*task_wait_handler)( rex_sigs_type, uim_instance_global_type*)
);

static void uim_compare_and_update_conf_file
(
  const char *                file_path_ptr,
  const char *                buf_ptr,
  uint16                      buf_len,
  uim_instance_global_type   *uim_ptr
);

static void uim_update_conf_file(uim_instance_global_type *uim_ptr);

static uim_nvitems_enum uim_map_nvitem_path_to_enum(const char* item_path);

static void uim_nv_feature_write_default_value
(
  uint8                  item_subfeature,
  const char*            item_path,
  uim_instance_global_type *uim_ptr
);

static void uim_nv_write_default_value_of_delta_features
(
  uint8                  version_in_nv,
  uint8                  version_in_build,
  const char*            item_path,
  uim_instance_global_type *uim_ptr
);

static void uim_nv_write_default_value_of_all_features
(
  const char               *item_path,
  uim_instance_global_type *uim_ptr
);

static void uim_nv_features_status_list_update_version
(
  const char* item_path,
  uim_instance_global_type *uim_ptr
);

static void uim_validate_and_set_config_params
(
  nv_uim_config_params_type        *out_uim_config_params,
  nv_uim_config_params_type const  *read_from_nv_uim_config_params,
  nv_stat_enum_type                 nv_read_status,
  uim_instance_global_type         *uim_ptr
);

/**
 * DEFINITIONS OF FUNCTIONS
 */

/**
 * This procedure is called by all UIM tasks to initialize all
 * UIMDRV NV items but the NV's are read only once by the task
 * that initializes first.
 *
 * @param uim_ptr Pointer to the global data common.
 */
void uim_nv_init( uim_instance_global_type *uim_ptr )
{
  uim_shared_global_type *shared_globals_ptr = uim_get_common_globals_ptr();

  RETURN_IF_INSTANCE_INVALID(uim_ptr->id);

  /* Enter critical section */
  rex_enter_crit_sect(&shared_globals_ptr->nv_read_crit_section);

  if (FALSE == shared_globals_ptr->nv_read_completed)
  {
    shared_globals_ptr->nv_read_by_instance = uim_ptr->id;
    uim_read_nv_from_efs(uim_ptr);
    uim_read_legacy_nv(uim_ptr, rex_self(),
                       UIM_NV_CMD_SIG,
                       (void (*)(rex_sigs_type, uim_instance_global_type*))uim_pet_and_wait);
    uim_update_conf_file(uim_ptr);
    shared_globals_ptr->nv_read_completed = TRUE;
  }
  else if (TRUE == shared_globals_ptr->nv_read_completed)
  {
    /* NV's have already been read from efs. Use the values read by the task
       that initialized first.*/
    uim_init_nv_globals_with_read_values_from_efs(uim_ptr);
  }

  rex_leave_crit_sect(&shared_globals_ptr->nv_read_crit_section);

  UIMDRV_MSG_HIGH_0(uim_ptr->id, "UIM_NV_INIT completed");
}  /* uim_nv_init */


/**
 * Get the status of UIMDRV subfeatures controlled by various
 * nvitems such as UIM_FEATURES_STATUS_LIST_FILE,
 * UIM_FEATURE_SUPPORT_HOTSWAP_FILE.
 *
 * @param nv_subfeature NV enum of sub feature.
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Enabled or not.
 */
boolean uim_nv_is_feature_enabled
(
  uim_nv_features_enum_type  nv_subfeature,
  uim_instance_global_type  *uim_ptr
)
{
  boolean ret_value = FALSE;
  switch(nv_subfeature)
  {
    case UIMDRV_FEATURE_HANDLE_NO_ATR_IN_40000_CLK_CYCLES:
#ifdef FEATURE_UIM_BTSAP_CLIENT
      if(!(IS_BTSAP_HANDLE_ACTIVE(uim_ptr)))
#endif
      {
        ret_value = uim_ptr->setting.features_status_list.handle_no_atr_in_40000_clk_cycles;
      }
      break;
    case UIMDRV_FEATURE_LOG_TO_EFS:
      ret_value = uim_ptr->setting.features_status_list.log_to_efs;
      break;
    case UIMDRV_FEATURE_DISABLE_RECOVERY_UPON_INFINITE_NULL:
#ifdef FEATURE_UIM_BTSAP_CLIENT
      if(!(IS_BTSAP_HANDLE_ACTIVE(uim_ptr)))
#endif
      {
        ret_value = uim_ptr->setting.features_status_list.disable_recovery_upon_infinite_null;
      }
      break;
    case UIMDRV_FEATURE_DEBUG_LOG:
      ret_value = uim_ptr->setting.features_status_list.debug_log;
      break;
    case UIMDRV_FEATURE_RPT_BAD_SW_ON_POLL:
      ret_value = uim_ptr->setting.features_status_list.rpt_bad_sw_on_poll;
      break;
    case UIMDRV_FEATURE_HANDLE_ICCID_READ_FAILURE:
      ret_value = uim_ptr->setting.features_status_list.handle_iccid_read_failure;
      break;
    case UIMDRV_FEATURE_SUPPORT_NO_ICCID:
      ret_value = uim_ptr->setting.features_status_list.support_no_iccid;
      break;
    case UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT:
      ret_value = uim_ptr->setting.features_status_list.min_tpl_iccid_support;
      break;
    case UIMDRV_FEATURE_HANDLE_UNKNOWN_PROC_BYTES_AS_CMD_TIMEOUT:
      ret_value = uim_ptr->setting.features_status_list.handle_unknown_proc_bytes_as_cmd_timeout;
      break;
    case UIMDRV_FEATURE_INTERFACE_NOT_USED:
      ret_value = uim_ptr->setting.features_status_list.interface_not_used;
      break;
    case UIMDRV_FEATURE_LOG_APDU_TO_EFS:
      ret_value = uim_ptr->setting.features_status_list.log_apdu_to_efs;
      break;
    case UIMDRV_FEATURE_NO_SWITCH_INST_ON_WWT_EXPIRY:
      ret_value = uim_ptr->setting.features_status_list.no_switch_inst_on_wwt_expiry;
      break;
    case UIMDRV_FEATURE_SEND_STATUS_BEFORE_AUTH:
      ret_value = uim_ptr->setting.features_status_list.send_status_before_auth;
      break;
    case UIMDRV_FEATURE_TRY_DEFAULT_BAUD_RATE_FOR_F372_D12_CARD:
      ret_value = uim_ptr->setting.features_status_list.try_default_baud_rate_for_f372_d12_card;
      break;
    case UIMDRV_FEATURE_COLD_RESET_DUE_TO_CARD_SWITCH:
      ret_value = uim_ptr->setting.features_status_list.cold_reset_due_to_card_switch;
      break;
    case UIMDRV_FEATURE_SM_USE_SLOT_1:
      ret_value = uim_ptr->setting.features_status_list.sm_prefer_slot1;
      break;
    case UIMDRV_FEATURE_USE_DUAL_LDO:
      ret_value = uim_ptr->setting.features_status_list.use_dual_ldo;
      break;
    case UIMDRV_FEATURE_UIM_POLLING_ONLY_AT_POLLING_TIMER_EXPIRY:
      ret_value = uim_ptr->setting.features_status_list.uim_polling_only_at_polling_timer_expiry;
      break;
    case UIMDRV_FEATURE_UIM_SET_CLK_FREQ_AT_4_8_MHZ:
      ret_value = uim_ptr->setting.features_status_list.uim_set_clk_freq_at_4_8_MHz;
      break;
    case UIMDRV_FEATURE_HANDLE_TC1_BYTE_FOR_EXTRA_GUARD_TIME:
      ret_value = uim_ptr->setting.features_status_list.handle_tc1_byte_for_extra_guard_time;
      break;
    case UIMDRV_FEATURE_ENABLE_SIM_MODE_CHANGE_VIA_WARM_RESET:
      ret_value = uim_ptr->setting.features_status_list.enable_sim_mode_change_via_warm_reset;
      break;
    case UIMDRV_FEATURE_ENABLE_EXPLICIT_SELECTION_OF_MF_OR_ADF:
      ret_value = uim_ptr->setting.features_status_list.enable_explicit_selection_of_mf_or_adf;
      break;
    case UIMDRV_FEATURE_ENABLE_BOOT_UP_IN_INVERSE_CONVENTION:
      ret_value =  uim_ptr->setting.features_status_list.enable_boot_up_in_inverse_convention;
      break;
    case UIMDRV_FEATURE_RECOVERY_ON_BAD_STATUS_WORD:
      ret_value =  uim_ptr->setting.features_status_list.recovery_on_bad_status_word ;
      break;
    case UIMDRV_FEATURE_ATTEMPT_PUP_3V_FROM_nTH_RECOVERY:
      ret_value =  uim_ptr->setting.features_status_list.attempt_pup_3v_from_nth_recovery ;
      break;
    case UIMDRV_FEATURE_LE_SUPPORT_FOR_7816_STREAM_APDU:
      ret_value =  uim_ptr->setting.features_status_list.enable_le_support_for_stream_7816_apdu ;
      break;
    case UIMDRV_FEATURE_DISABLE_CARD_STATUS_CHECK_AT_POWER_UP:
      ret_value =  uim_ptr->setting.features_status_list.disable_card_status_check_at_power_up;
      break;
    case UIMDRV_FEATURE_SIMTRAY_WITH_GPIOS_TIED:
      ret_value =  uim_ptr->setting.features_status_list.simtray_with_gpios_tied;
      break;
    default:
      UIMDRV_MSG_ERR_1(uim_ptr->id,"Invalid Feature 0x%x", nv_subfeature);
      break;
  }
  return ret_value;
} /* uim_nv_is_feature_enabled */


/**
 * Put an item to nonvolatile memory.
 *
 * @param item NV item to be saved.
 * @param data_ptr Pointer to the NV data.
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
boolean uim_put_nv(
  nv_items_enum_type     item,
  nv_item_type          *data_ptr,
  uim_instance_global_type *uim_ptr
)
{
  nv_cmd_type nv_cmd_buf;           /* nv command buffer */
  UIMDRV_MSG_HIGH_0(uim_ptr->id,"In UIM_PUT_NV");

  if(data_ptr == NULL)
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"ERR: data_ptr is NULL!!!");
    return FALSE;
  }
  /* Prepare command buffer to NV. */
  nv_cmd_buf.cmd        = NV_WRITE_F;     /* Write request        */
  nv_cmd_buf.tcb_ptr    = rex_self();     /* Notify back to uim   */
  nv_cmd_buf.sigs       = UIM_NV_CMD_SIG; /* With this signal     */
  nv_cmd_buf.done_q_ptr = NULL;           /* No buffer to return  */
  nv_cmd_buf.item       = item;           /* Item to write        */
  nv_cmd_buf.data_ptr   = data_ptr;       /* Data to be written   */

  /* Clear signal, issue the command, and wait for the respone. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( rex_self(), UIM_NV_CMD_SIG );

  /* Issue the request    */
  nv_cmd( &nv_cmd_buf );

  /* Wait for completion  */
  (void)uim_pet_and_wait((UIM_NV_CMD_SIG), uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( rex_self(), UIM_NV_CMD_SIG );

  /* Check NV read status */
  if (nv_cmd_buf.status != NV_DONE_S)
  {
    UIMDRV_MSG_ERR_1(uim_ptr->id,"Failed to write item to NV 0x%x",
                     nv_cmd_buf.item);
    return FALSE;
  }
  return TRUE;
} /* uim_put_nv */


/**
 * Get an item to nonvolatile memory.
 *
 * @param item NV item to get.
 * @param data_ptr Pointer to the data to be saved.
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
boolean uim_get_nv(
  nv_items_enum_type     item,
  nv_item_type          *data_ptr,
  uim_instance_global_type *uim_ptr
)
{
  nv_cmd_type nv_cmd_buf;           /* nv command buffer */
  UIMDRV_MSG_HIGH_0(uim_ptr->id,"In UIM_PUT_NV");

  if(data_ptr == NULL)
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"ERR: data_ptr is NULL!!!");
    return FALSE;
  }
  /* Prepare command buffer to NV. */
  nv_cmd_buf.cmd        = NV_READ_F;      /* Read request         */
  nv_cmd_buf.tcb_ptr    = rex_self();     /* Notify back to uim   */
  nv_cmd_buf.sigs       = UIM_NV_CMD_SIG; /* With this signal     */
  nv_cmd_buf.done_q_ptr = NULL;           /* No buffer to return  */
  nv_cmd_buf.item       = item;           /* Item to write        */
  nv_cmd_buf.data_ptr   = data_ptr;       /* Data to be written   */

  /* Clear signal, issue the command, and wait for the respone. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( rex_self(), UIM_NV_CMD_SIG );

  /* Issue the request    */
  nv_cmd( &nv_cmd_buf );

  /* Wait for completion  */
  (void)uim_pet_and_wait((UIM_NV_CMD_SIG), uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( rex_self(), UIM_NV_CMD_SIG );

  /* Check NV read status */
  if (nv_cmd_buf.status != NV_DONE_S)
  {
    UIMDRV_MSG_ERR_1(uim_ptr->id,"Failed to read item to NV 0x%x",
                     nv_cmd_buf.item);
    return FALSE;
  }
  return TRUE;
} /* uim_get_nv */


/**
 * Return the JCDMA Service status from EFS.
 *
 * @return uim_jcdma_service_status_enum
 *         UIM_JCDMA_SERVICE_DISABLED or
 *         UIM_JCDMA_SERVICE_ENABLED
 */
uim_jcdma_service_status_enum uim_get_jcdma_service_status(void)
{
  static uim_jcdma_me_capability_type uim_jcdma_mode = {UIM_JCDMA_SERVICE_DISABLED};

#if defined(FEATURE_UIM_SUPPORT_DUAL_SLOTS) || defined(FEATURE_UIM_SUPPORT_TRIPLE_SLOTS)
    uim_jcdma_mode.service = UIM_JCDMA_SERVICE_DISABLED;
    UIM_MSG_HIGH_0("JCDMA Service is disabled");
#else
  static boolean uim_jcdma_init_done = FALSE;

  if (FALSE == uim_jcdma_init_done)
  {
    int status = 0;
    if (efs_get(UIM_JCDMA_MODE_FILE, &uim_jcdma_mode, sizeof(uim_jcdma_mode))
         == sizeof(uim_jcdma_mode))
    {
      UIM_MSG_HIGH_0("Successfully read the JCDMA Capability parameter");
    }
    else
    {
      UIM_MSG_HIGH_0("JCDMA Capability parameter failed to read");
      uim_jcdma_mode.service = UIM_JCDMA_SERVICE_DISABLED;
      /* Write jcdma capability to the EFS, erasing previous contents, create if not present  */
      status = efs_put(UIM_JCDMA_MODE_FILE, &uim_jcdma_mode, sizeof(uim_jcdma_mode),
                       O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
      if (status != 0)
      {
        UIM_MSG_ERR_1("Writting to default value to EFS failed, error %d",
                       status);
      }
      UIM_MSG_HIGH_0("Successfully write default JCDMA Capability to EFS");
    }
    uim_jcdma_init_done = TRUE;

    /* QCN backup support for JCDMA service - JCDMA EFS item is now moved
       to UIMDRV conf file */
  }
  UIM_MSG_HIGH_1("JCDMA Service Enabled 0x%x", uim_jcdma_mode.service);
#endif /* defined(FEATURE_UIM_SUPPORT_DUAL_SLOTS) || defined(FEATURE_UIM_SUPPORT_TRIPLE_SLOTS) */

  return uim_jcdma_mode.service;
}/* uim_get_jcdma_service_status */


/**
 * Reads all the required UIMDRV NV items using efs_get().
 *
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_read_nv_from_efs(uim_instance_global_type  *uim_ptr)
{
  uim_pdown_on_tech_problem_type uim_pdown_on_tech_prob;

#ifdef FEATURE_UIM_REMOTE_UIM
  uim_remote_cmd_rsp_timer_nv_type uim_remote_cmd_rsp_timer_nv;
#endif /* FEATURE_UIM_REMOTE_UIM */

  RETURN_IF_INSTANCE_INVALID(uim_ptr->id);

  /* === Read UIMDRV Features Status List from EFS === */
  if (efs_get(UIM_FEATURES_STATUS_LIST_FILE,
              &uim_ptr->setting.features_status_list,
              sizeof(uim_features_status_list_type))
              == sizeof(uim_features_status_list_type))
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"UIMDRV Features Status read from NV");
    uim_nv_features_status_list_update_version(UIM_FEATURES_STATUS_LIST_FILE,
                                               uim_ptr);
  }
  else
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"Writing default values to UIMDRV Features Status NV");
    uim_nv_write_default_value_of_all_features(UIM_FEATURES_STATUS_LIST_FILE,
                                               uim_ptr);
    (void)efs_put(UIM_FEATURES_STATUS_LIST_FILE,
                  &uim_ptr->setting.features_status_list,
                  sizeof(uim_features_status_list_type),
                  O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
  }

    /* === Read HOTSWAP Debounce Config from EFS === */
  if (efs_get(UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE, &uim_ptr->hotswap.hotswap_me_conf, sizeof(uim_ptr->hotswap.hotswap_me_conf))
         == sizeof(uim_ptr->hotswap.hotswap_me_conf))
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"Hotswap Debounce Config read from NV");
  }
  else
  {
    uim_ptr->hotswap.hotswap_me_conf.auxiliary_period_for_card_detect = UIM_HOTSWAP_AUX_SLOT_PERIOD;
    uim_ptr->hotswap.hotswap_me_conf.maximum_debounce_retry_counter   = UIM_HOTSWAP_MAX_DEBOUNCE_RETRY;
    uim_ptr->hotswap.hotswap_me_conf.num_of_sample_for_insertion      = UIM_HOTSWAP_NUM_OF_SAMPLES_TO_DETECT_CARD_INSERTION;
    uim_ptr->hotswap.hotswap_me_conf.num_of_sample_for_removal        = UIM_HOTSWAP_NUM_OF_SAMPLES_TO_DETECT_CARD_REMOVAL;
    if ( efs_put(UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE, &uim_ptr->hotswap.hotswap_me_conf, sizeof(uim_ptr->hotswap.hotswap_me_conf),
                  O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777) != 0)
    {
      UIMDRV_MSG_ERR_0(uim_ptr->id,"Writing default value to Hotswap Debounce Config EFS failed");
    }
    else
    {
      UIMDRV_MSG_HIGH_0(uim_ptr->id,"Writing default values to Hotswap Debounce Config EFS");
    }
  }

  /* === Read Active slot configuration for Sub Manager from EFS === */
#ifdef FEATURE_UIM_DS_SUBSCRIPTION_MANAGER
  if(UIM_SLOT_NONE == uim_sm_data.new_active_slot)
  {
    if (efs_get(UIM_ACTIVE_SLOT_NV,
                &uim_active_slot_configuration,
                sizeof(uim_active_slot_configuration))
                == sizeof(uim_active_slot_configuration))
    {
      uim_sm_data.new_active_slot = uim_active_slot_configuration.active_slot;
      UIMDRV_MSG_HIGH_1(uim_ptr->id,"Active slot configuration read from NV, %x ",
                        uim_sm_data.new_active_slot);
    }
    else
    {
      uim_sm_data.new_active_slot = UIM_SLOT_1;
      UIMDRV_MSG_HIGH_0(uim_ptr->id,"Read to NV active slot configuration unsuccessful");
    }
    if (uim_sm_data.new_active_slot < UIM_SLOT_1 || uim_sm_data.new_active_slot > UIM_NUM_PHY_SLOTS)
    {
      UIMDRV_MSG_HIGH_1(uim_ptr->id,"Invalid active slot value read from NV %x, resetting to slot1",
                        uim_sm_data.new_active_slot);
      uim_sm_data.new_active_slot = UIM_SLOT_1;
    }
    uim_sm_data.uim_slot_info[uim_sm_data.new_active_slot -1].activity_status = UIM_SLOT_STATE_ACTIVE;
  }
#endif /* FEATURE_UIM_DS_SUBSCRIPTION_MANAGER */

  /* === Read the Power down configuration from NV === */
  if (efs_get(UIM_PDOWN_NV_EF,
              &uim_pdown_on_tech_prob, sizeof(uim_pdown_on_tech_prob))
              == sizeof(uim_pdown_on_tech_prob))
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"Power down configuration on technical problems read from NV");
    if (TRUE == uim_pdown_on_tech_prob.to_pdown)
    {
      uim_ptr->flag.nv_ens_enabled_flag = TRUE;
      uim_ptr->setting.repeated_tech_problems_cnt = uim_pdown_on_tech_prob.cnt_tech_problem;
    }
    else
    {
       uim_ptr->setting.repeated_tech_problems_cnt = UIM_MAX_REPEATED_TECH_PROBLEMS;
    }
  }
  else
  {
    uim_ptr->setting.repeated_tech_problems_cnt = UIM_MAX_REPEATED_TECH_PROBLEMS;
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"Failed to read NV power down configuration");
  }

#ifdef FEATURE_UIM_POLLING_DISABLE_CAPABILITY
  #error code not present
#endif /* FEATURE_UIM_POLLING_DISABLE_CAPABILITY */

  /* === Read the UIM_RESPONSE_TIMERS_CONFIG from EFS === */
  if (efs_get(UIM_FEATURE_BUSY_RESPONSE_FILE,
              &uim_ptr->setting.uim_feature_busy_response,sizeof(uim_feature_busy_response_type))
              == sizeof(uim_feature_busy_response_type))
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id, "Successfully read the UIM_RESPONSE_TIMERS_CONFIG from EFS");
    /* Validate timer values and set both to default if invalid*/
    if ( uim_ptr->setting.uim_feature_busy_response.uim_busy_ind_timer_val_ms >= uim_ptr->setting.uim_feature_busy_response.uim_trans_timer_val_ms
         && uim_ptr->setting.uim_feature_busy_response.uim_trans_timer_val_ms != 0 )
    {
      uim_ptr->setting.uim_feature_busy_response.uim_busy_ind_timer_val_ms = (uint32)UIM_BUSY_IND_TIMER_VAL_MS_DEFAULT;
      uim_ptr->setting.uim_feature_busy_response.uim_trans_timer_val_ms    = (uint32)UIM_TRANS_TIMER_VAL_MS_DEFAULT;
      efs_put(UIM_FEATURE_BUSY_RESPONSE_FILE, &uim_ptr->setting.uim_feature_busy_response, sizeof(uim_feature_busy_response_type),
              O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
      UIMDRV_MSG_HIGH_0(uim_ptr->id, "Invalid inputs, uim_ind_timer_val_ms > uim_trans_timer_val_ms, force set to defaults.");
    }
  }
  else
  {
    uim_ptr->setting.uim_feature_busy_response.uim_busy_ind_timer_val_ms = (uint32)UIM_BUSY_IND_TIMER_VAL_MS_DEFAULT;
    uim_ptr->setting.uim_feature_busy_response.uim_trans_timer_val_ms    = (uint32)UIM_TRANS_TIMER_VAL_MS_DEFAULT;
    efs_put(UIM_FEATURE_BUSY_RESPONSE_FILE, &uim_ptr->setting.uim_feature_busy_response, sizeof(uim_feature_busy_response_type),
            O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
    UIMDRV_MSG_HIGH_0(uim_ptr->id, "Failed to read UIM_RESPONSE_TIMERS_CONFIG, force set to defaults");
  }

  /* Read the UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE from EFS */
  if (efs_get(UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE,
              &uim_ptr->debug.uim_feature_busy_response_simulate_null, sizeof(uim_feature_busy_response_simulate_null_type))
              == sizeof(uim_feature_busy_response_simulate_null_type))
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id, "Successfully read the UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE from EFS");
  }
  else
  {
    /* simulate null timer is disabled by default */
    uim_ptr->debug.uim_feature_busy_response_simulate_null.uim_disable_simulate_null = TRUE;
    uim_ptr->debug.uim_feature_busy_response_simulate_null.uim_simulate_null_ins = UIM_SIMULATE_NULL_INS_DEFAULT;
    efs_put(UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE, &uim_ptr->debug.uim_feature_busy_response_simulate_null, sizeof(uim_feature_busy_response_simulate_null_type),
            O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
    UIMDRV_MSG_HIGH_0(uim_ptr->id, "Failed to read UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE, force set to defaults");
  } /* end efs_get(UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE) */

  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_busy_ind_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);
  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_trans_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);
  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_simulate_null_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);

#ifdef FEATURE_UIM_REMOTE_UIM
#ifdef FEATURE_UIM_BTSAP_CLIENT
  /* Read remote command response time from NV */
  if (efs_get(UIM_REMOTE_COMMAND_RESP_TIMER, &uim_remote_cmd_rsp_timer_nv, sizeof(uim_remote_cmd_rsp_timer_nv_type))
       == sizeof(uim_remote_cmd_rsp_timer_nv_type))
  {
    uim_ptr->remote.btsap.btsap_cmd_rsp_timer_value = uim_remote_cmd_rsp_timer_nv.timer;
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"BTSAP:Successfully read the remote command response timer from NV, 0x%x ",
                      uim_remote_cmd_rsp_timer_nv.timer);
  }
  else
  {
    uim_ptr->remote.btsap.btsap_cmd_rsp_timer_value = UIM_BTSAP_TRANSACTION_TIME_DURATION;
    UIMDRV_MSG_HIGH_0(uim_ptr->id," BTSAP:Fail to read the remote command response timer from NV ");
  }
#endif /* FEATURE_UIM_BTSAP_CLIENT */
#ifdef FEATURE_UIM_USB_UICC
  /* Read remote command response time from NV */
  if (efs_get(UIM_REMOTE_COMMAND_RESP_TIMER, &uim_remote_cmd_rsp_timer_nv, sizeof(uim_remote_cmd_rsp_timer_nv_type))
       == sizeof(uim_remote_cmd_rsp_timer_nv_type))
  {
    uim_ptr->usb.usb_interface_status.cmd_rsp_time  = uim_remote_cmd_rsp_timer_nv.timer;
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"USB UICC:Successfully read the remote command response timer from NV, 0x%x ",
                      uim_remote_cmd_rsp_timer_nv.timer);
  }
  else
  {
    uim_ptr->usb.usb_interface_status.cmd_rsp_time = UIM_USB_DEFAULT_CMD_RSP_TIME;
    UIMDRV_MSG_HIGH_0(uim_ptr->id," USB UICC:Fail to read the remote command response timer from NV ");
  }
#endif /* FEATURE_UIM_USB_UICC */
#endif /* FEATURE_UIM_REMOTE_UIM */
}/* uim_read_nv_from_efs */


/**
 * NV items are read only once during power up by the task that
 * attempts to initialize first. This procedure initializes the
 * nv globals of the uim tasks that follow, with the values read
 * by the uim instance that initializes first.
 *
 * @param uim_ptr Pointer to the global common data.
 */
static void uim_init_nv_globals_with_read_values_from_efs( uim_instance_global_type *uim_ptr )
{
  uim_shared_global_type   *shared_globals_ptr = uim_get_common_globals_ptr();
  uim_instance_global_type *uim_temp_ptr =
                               uim_get_instance_ptr(shared_globals_ptr->nv_read_by_instance);

  RETURN_IF_INSTANCE_INVALID(uim_ptr->id);

  if (uim_temp_ptr == NULL)
  {
    return;
  }

  /* === NV_UIM_FIRST_INST_CLASS_I === */
  uim_ptr->state.first_inst_class = uim_temp_ptr->state.first_inst_class;

  /* === NV_GPRS_ANITE_GCF_I === */
  uim_ptr->flag.gcf_testing_flag = uim_temp_ptr->flag.gcf_testing_flag;

  /* === NV_ENS_ENABLED_I === */
  uim_ptr->flag.nv_ens_enabled_flag = uim_temp_ptr->flag.nv_ens_enabled_flag;

  /* === NV_UIM_PREF_PROTOCOL_I === */
  uim_ptr->t1.nv_pref_protocol = uim_temp_ptr->t1.nv_pref_protocol;

  /* === NV_UIM_PAR_ERR_WORKAROUND_I === */
  uim_ptr->setting.par_err_workaround = uim_temp_ptr->setting.par_err_workaround;

  /* === NV_UIM_CONFIG_PARAMS_I === */
  uim_memscpy(&uim_ptr->setting.config_params,
          sizeof(uim_ptr->setting.config_params),
          &uim_temp_ptr->setting.config_params,
          sizeof(uim_temp_ptr->setting.config_params));

  /* === UIM_FEATURES_STATUS_LIST_FILE === */
  uim_memscpy(&uim_ptr->setting.features_status_list,
              sizeof(uim_features_status_list_type),
              &uim_temp_ptr->setting.features_status_list,
              sizeof(uim_features_status_list_type));

  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT,
                               uim_ptr) == TRUE)
  {
    if(NULL == uim_ptr->state.iccid_tpl_ptr)
    {
      uim_ptr->state.iccid_tpl_ptr = uim_malloc(sizeof(nv_uim_iccid_tpl_type));
      if(NULL == uim_ptr->state.iccid_tpl_ptr)
      {
        UIMDRV_MSG_ERR_0(uim_ptr->id,"Malloc failed for NV_UIM_ICCID_TPL_I");
      }
      else
      {
        memset(uim_ptr->state.iccid_tpl_ptr->iccid_data,0x00,NV_ICCID_MAX_SIZE);
        uim_ptr->state.iccid_tpl_ptr->terminal_profile_len = UIM_MINIMUM_TP_LENGTH;
      }
    }
    if(NULL != uim_temp_ptr->state.iccid_tpl_ptr &&
       NULL != uim_ptr->state.iccid_tpl_ptr )
    {
      uim_memscpy( uim_ptr->state.iccid_tpl_ptr->iccid_data,
                   sizeof(uim_ptr->state.iccid_tpl_ptr->iccid_data),
                   uim_temp_ptr->state.iccid_tpl_ptr->iccid_data,
                   sizeof(uim_temp_ptr->state.iccid_tpl_ptr->iccid_data) );
      uim_ptr->state.iccid_tpl_ptr->terminal_profile_len =
                                uim_temp_ptr->state.iccid_tpl_ptr->terminal_profile_len;
    }
  }

  /* === UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE === */
  uim_memscpy( &uim_ptr->hotswap.hotswap_me_conf,
               sizeof(uim_ptr->hotswap.hotswap_me_conf),
               &uim_temp_ptr->hotswap.hotswap_me_conf,
               sizeof(uim_temp_ptr->hotswap.hotswap_me_conf) );

  /* === UIM_PDOWN_NV_EF === */
  uim_ptr->flag.nv_ens_enabled_flag           = uim_temp_ptr->flag.nv_ens_enabled_flag;
  uim_ptr->setting.repeated_tech_problems_cnt = uim_temp_ptr->setting.repeated_tech_problems_cnt;

  /* === UIM_RESPONSE_TIMERS_CONFIG === */
  uim_memscpy( &uim_ptr->setting.uim_feature_busy_response,
               sizeof(uim_ptr->setting.uim_feature_busy_response),
               &uim_temp_ptr->setting.uim_feature_busy_response,
               sizeof(uim_temp_ptr->setting.uim_feature_busy_response) );
  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_busy_ind_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);
  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_trans_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);

  /* === UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE === */
  uim_memscpy( &uim_ptr->debug.uim_feature_busy_response_simulate_null,
               sizeof(uim_ptr->debug.uim_feature_busy_response_simulate_null),
               &uim_temp_ptr->debug.uim_feature_busy_response_simulate_null,
               sizeof(uim_temp_ptr->debug.uim_feature_busy_response_simulate_null) );
  /* Define timer and Set the timer state to NOT_STARTED from NOT_USE*/
  uim_simulate_null_timer_state_handler(uim_ptr, UIM_BUSY_RESPONSE_EVT_NV_INIT);
#ifdef FEATURE_UIM_POLLING_DISABLE_CAPABILITY
  #error code not present
#endif /* FEATURE_UIM_POLLING_DISABLE_CAPABILITY */
#ifdef FEATURE_UIM_BTSAP_CLIENT
  uim_ptr->remote.btsap.btsap_cmd_rsp_timer_value =
                               uim_temp_ptr->remote.btsap.btsap_cmd_rsp_timer_value;
#endif /* FEATURE_UIM_BTSAP_CLIENT */
#ifdef FEATURE_UIM_USB_UICC
  uim_ptr->usb.usb_interface_status.cmd_rsp_time =
                               uim_temp_ptr->usb.usb_interface_status.cmd_rsp_time;
#endif /* FEATURE_UIM_USB_UICC */
}/* uim_init_nv_globals_with_read_values_from_efs */


/**
 * Reads the legacy NV items that cannot be read using efs_get()
 * but using nv_cmd() instead.
 *
 * @param uim_ptr Pointer to the global data common.
 * @param task_ptr Pointer to the current TCB.
 * @param task_wait_sig Signal mask to be waited.
 * @param task_wait_handler Handler for waiting task_wait_sig
 */
static void uim_read_legacy_nv
(
  uim_instance_global_type *uim_ptr,
  rex_tcb_type             *task_ptr,
  rex_sigs_type            task_wait_sig,
  void                    (*task_wait_handler)( rex_sigs_type, uim_instance_global_type*)
)
{
  /* Command buffer to NV */
  nv_cmd_type  *nv_cmd_buf_ptr      = NULL;
  /* NV data buffer */
  nv_item_type *uim_nv_data_buf_ptr = NULL;

  nv_cmd_buf_ptr      = uim_malloc(sizeof(nv_cmd_type));
  uim_nv_data_buf_ptr = uim_malloc(sizeof(nv_item_type));

  if (task_wait_handler == NULL)
  {
    ERR_FATAL("uim_nv_init task_wait_handler is NULL",0,0,0);
  }
  if( NULL == nv_cmd_buf_ptr || NULL == uim_nv_data_buf_ptr)
  {
    ERR_FATAL("uim_malloc failed",0,0,0);
  }

  /* Prepare command buffer to NV. */
  nv_cmd_buf_ptr->cmd        = NV_READ_F;             /* Read request        */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;              /* Notify back to me   */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;         /* With this signal    */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                  /* No buffer to return */
  nv_cmd_buf_ptr->item       = NV_UIM_FIRST_INST_CLASS_I; /* Item to get     */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Check NV read status */
  if (nv_cmd_buf_ptr->status != NV_DONE_S)
  {
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"Bad NV read status 0x%x",
                      nv_cmd_buf_ptr->status );
    uim_ptr->state.first_inst_class = NV_UIM_FIRST_INST_CLASS_UMTS_SIM;
  }
  else
  {
    /* Check if valid value is read from NV (NV value is always >= 0 hence
       that check is not necessary */
    if(uim_nv_data_buf_ptr->uim_first_inst_class <=
      NV_UIM_FIRST_INST_CLASS_UMTS_SIM)
    {
      uim_ptr->state.first_inst_class = uim_nv_data_buf_ptr->uim_first_inst_class;
    }
    else /* Set default value for instruction class */
    {
      UIMDRV_MSG_HIGH_1(uim_ptr->id,"Undefined value for 1st inst class %d, setting to default",
                        uim_nv_data_buf_ptr->uim_first_inst_class );
      uim_ptr->state.first_inst_class = NV_UIM_FIRST_INST_CLASS_UMTS_SIM;
    }
  }

  /* Get the GCF testing FLAG */
  /* Prepare command buffer to NV. */
  nv_cmd_buf_ptr->cmd        = NV_READ_F;           /* Read request        */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;            /* Notify back to m    */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;       /* With this signal    */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                /* No buffer to return */
  nv_cmd_buf_ptr->item       = NV_GPRS_ANITE_GCF_I; /* Item to get         */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Check NV read status */
  uim_ptr->flag.gcf_testing_flag = (nv_cmd_buf_ptr->status == NV_DONE_S) ?
    uim_nv_data_buf_ptr->gprs_anite_gcf : FALSE;

    /* Get the ENS enabled FLAG */
  /* Prepare command buffer to NV. */
  nv_cmd_buf_ptr->cmd        = NV_READ_F;           /* Read request         */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;            /* Notify back to me    */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;       /* With this signal     */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                /* No buffer to return  */
  nv_cmd_buf_ptr->item       = NV_ENS_ENABLED_I;    /* Item to get          */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;   /* Where to return it   */

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Check NV read status */
  uim_ptr->flag.nv_ens_enabled_flag = (nv_cmd_buf_ptr->status == NV_DONE_S) ?
      uim_nv_data_buf_ptr->ens_enabled : FALSE;

#ifdef FEATURE_UIM_T_1_SUPPORT
/* Prepare command buffer to NV. */
  nv_cmd_buf_ptr->cmd        = NV_READ_F;               /* Read request         */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;                /* Notify back to me    */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;           /* With this signal     */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                    /* No buffer to return  */
  nv_cmd_buf_ptr->item       = NV_UIM_PREF_PROTOCOL_I;  /* Item to get        */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;       /* Where to return it   */

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Check NV read status */
  if (nv_cmd_buf_ptr->status != NV_DONE_S)
  {
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"Bad NV read status 0x%x",
                      nv_cmd_buf_ptr->status );
    uim_ptr->t1.nv_pref_protocol = NV_UIM_PREF_PROTOCOL_T_0;
  }
  else
  {
    uim_ptr->t1.nv_pref_protocol = uim_nv_data_buf_ptr->uim_pref_protocol;
  }
#endif /* FEATURE_UIM_T_1_SUPPORT */

  /* Prepare command buffer to NV. */
  nv_cmd_buf_ptr->cmd        = NV_READ_F;                    /* Read request         */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;                     /* Notify back to me    */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;                /* With this signal     */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                         /* No buffer to return  */
  nv_cmd_buf_ptr->item       = NV_UIM_PAR_ERR_WORKAROUND_I;  /* Item to get  */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;            /* Where to return it   */

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Check NV read status */
  if (nv_cmd_buf_ptr->status != NV_DONE_S)
  {
    /* If the NV read status is a failure, then the SW workaround for
       the GCF 27.11.1.5 testcase doesn't execute. */
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"Bad NV read status 0x%x",
                      nv_cmd_buf_ptr->status );
    uim_ptr->setting.par_err_workaround = FALSE;
  }
  else
  {
    uim_ptr->setting.par_err_workaround = uim_nv_data_buf_ptr->uim_par_err_workaround;
  }

  /* Read the configurable parameters from NV */

  nv_cmd_buf_ptr->cmd        = NV_READ_F;              /* Read request         */
  nv_cmd_buf_ptr->tcb_ptr    = task_ptr;               /* Notify back to me    */
  nv_cmd_buf_ptr->sigs       = task_wait_sig;          /* With this signal     */
  nv_cmd_buf_ptr->done_q_ptr = NULL;                   /* No buffer to return  */
  nv_cmd_buf_ptr->item       = NV_UIM_CONFIG_PARAMS_I; /* Item to get        */
  nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;      /* Where to return it   */

  /* Clear signal, issue the command, and wait for the response. */
  /* Clear signal for NV  */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  /* Issue the request    */
  nv_cmd( nv_cmd_buf_ptr );

  /* Wait for completion  */
  task_wait_handler( task_wait_sig, uim_ptr);

  /* clear the signal again */
  (void) rex_clr_sigs( task_ptr, task_wait_sig );

  uim_validate_and_set_config_params(&uim_ptr->setting.config_params,
                                    &(uim_nv_data_buf_ptr->uim_config_params),
                                    nv_cmd_buf_ptr->status,
                                     uim_ptr);
  /* Write back NV4205 if NV is not initialized */
  if (NV_DONE_S != nv_cmd_buf_ptr->status)
  {
    if (NV_NOTACTIVE_S == nv_cmd_buf_ptr->status)
    {
      /* Copy the default values from uim config params back to the nv_cmd_buf */
      uim_memscpy(&(uim_nv_data_buf_ptr->uim_config_params),
                   sizeof(uim_nv_data_buf_ptr->uim_config_params),
                  &uim_ptr->setting.config_params,
                   sizeof(&uim_ptr->setting.config_params));
      /* In case if the NV Item is inactivated state  */
      uim_put_nv( NV_UIM_CONFIG_PARAMS_I,
                  (nv_item_type *)(&(uim_nv_data_buf_ptr->uim_config_params)),
                  uim_ptr);
    }
    else
    {
      /* NV item is not supported, freeing memory */
      UIMDRV_MSG_HIGH_1(uim_ptr->id,"Bad NV read status for NV_UIM_CONFIG_PARAMS_I 0x%x",
                        nv_cmd_buf_ptr->status);
    }
  }
  /* Check NV read status */
  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT,
                               uim_ptr) == TRUE)
  {
    if(NULL == uim_ptr->state.iccid_tpl_ptr)
    {
      uim_ptr->state.iccid_tpl_ptr = uim_malloc(sizeof(nv_uim_iccid_tpl_type));
      if(NULL == uim_ptr->state.iccid_tpl_ptr)
      {
        UIMDRV_MSG_ERR_0(uim_ptr->id,"Not able to allocate space for NV_UIM_ICCID_TPL_I");
      }
      else
      {
        memset(uim_ptr->state.iccid_tpl_ptr->iccid_data,0x00,NV_ICCID_MAX_SIZE);
        uim_ptr->state.iccid_tpl_ptr->terminal_profile_len = UIM_MINIMUM_TP_LENGTH;
      }
    }

    /* Read ICCID and Terminal Profile length from nv */
    nv_cmd_buf_ptr->cmd        = NV_READ_F;           /* Read request         */
    nv_cmd_buf_ptr->tcb_ptr    = task_ptr;            /* Notify back to me    */
    nv_cmd_buf_ptr->sigs       = task_wait_sig;       /* With this signal     */
    nv_cmd_buf_ptr->done_q_ptr = NULL;                /* No buffer to return  */
    nv_cmd_buf_ptr->item       = NV_UIM_ICCID_TPL_I;  /* Item to get          */
    nv_cmd_buf_ptr->data_ptr   = uim_nv_data_buf_ptr ;   /* Where to return it   */

    /* Clear signal, issue the command, and wait for the response. */
    /* Clear signal for NV  */
    (void) rex_clr_sigs( task_ptr, task_wait_sig );

    /* Issue the request    */
    nv_cmd( nv_cmd_buf_ptr );

    /* Wait for completion  */
    task_wait_handler( task_wait_sig, uim_ptr);

    /* clear the signal again */
    (void) rex_clr_sigs( task_ptr, task_wait_sig );

     /* Check NV read status */
    if (nv_cmd_buf_ptr->status != NV_DONE_S)
    {
      if (NV_NOTACTIVE_S == nv_cmd_buf_ptr->status)
      {
        /* In case if the NV Item is inactivated state  */
        if ( FALSE == uim_put_nv(
                       NV_UIM_ICCID_TPL_I,
                       (nv_item_type *)uim_ptr->state.iccid_tpl_ptr,
                       uim_ptr)
            )
        {
          UIMDRV_MSG_ERR_0(uim_ptr->id,"Error during writing the TPL to NV_UIM_ICCID_TPL_I");
          /* NV item is not supported, freeing memory */
          UIM_FREE(uim_ptr->state.iccid_tpl_ptr);
        }
      }
      else
      {
        /* NV item is not supported, freeing memory */
        UIMDRV_MSG_HIGH_1(uim_ptr->id,"Bad NV read status for NV_UIM_ICCID_TPL_I  0x%x",
                          nv_cmd_buf_ptr->status);
        UIM_FREE(uim_ptr->state.iccid_tpl_ptr);
      }
    }
    else
    {
      if(NULL != uim_ptr->state.iccid_tpl_ptr)
      {
        /* Reading ICCID and Terminal profile length */
        uim_memscpy(uim_ptr->state.iccid_tpl_ptr->iccid_data,
               sizeof(uim_ptr->state.iccid_tpl_ptr->iccid_data),
               uim_nv_data_buf_ptr->uim_iccid_tpl.iccid_data,
               NV_ICCID_MAX_SIZE);
        uim_ptr->state.iccid_tpl_ptr->terminal_profile_len =
                                uim_nv_data_buf_ptr->uim_iccid_tpl.terminal_profile_len;
      }
    }
  } /* endif uim_nv_is_feature_enabled */

  UIM_FREE(nv_cmd_buf_ptr);
  UIM_FREE(uim_nv_data_buf_ptr);
}/* uim_read_legacy_nv */


/**
 * This function opens the file, compares its contents against
 * the passed buffer and if the two are different, empties the
 * file and writes back the buffer data to the file. It also
 * writes the buffer data to the file if file couldn't be read.
 *
 * @param file_path_ptr File path.
 * @param buf_ptr Buffer to compare against the file data.
 * @param buf_len Buffer length.
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_compare_and_update_conf_file(
  const char *                file_path_ptr,
  const char *                buf_ptr,
  uint16                      buf_len,
  uim_instance_global_type   *uim_ptr
)
{
  int32                   fd                    = 0;
  char                   *file_data_ptr         = NULL;
  struct fs_stat          conf_stat;

  if (!file_path_ptr || !buf_ptr || buf_len == 0)
  {
    return;
  }

  /* Get the size of the file */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if(efs_stat(file_path_ptr, &conf_stat) >= 0 &&
     conf_stat.st_size == buf_len)
  {
    /* Stat succeeded meaning the file exists - open it and read the data */
    fd = efs_open(file_path_ptr, O_RDONLY);
    if (fd < 0)
    {
      UIM_MSG_ERR_0("Error opening file");
      return;
    }

    /* If the file size is non-zero, read data from file to compare against the
       buffer and write back to the file only if the data is different. If file
       read fails, still write the buffer to the file  */
    file_data_ptr = uim_malloc(conf_stat.st_size);
    if(file_data_ptr != NULL)
    {
      if(efs_read(fd, file_data_ptr, conf_stat.st_size) == conf_stat.st_size)
      {
        if(memcmp(file_data_ptr, buf_ptr, buf_len) == 0)
        {
          /* file data and the buffer data are same,
             so return without updating the file */
          UIM_FREE(file_data_ptr);
          (void)efs_close(fd);
          return;
        }
      }
      UIM_FREE(file_data_ptr);
    }

    /* Close it since we will anyways have to open it again in
       TRUNC mode in order to write to it */
    (void)efs_close(fd);
  }

  /* File doesn't exist or it's contents do not match buffer data;
     write buffer to the file */
  fd = efs_open (file_path_ptr, O_WRONLY | O_TRUNC | O_CREAT);
  if (fd >= 0)
  {
    (void)efs_write(fd, buf_ptr, buf_len);
    (void)efs_close(fd);
  }
} /* uim_compare_and_update_conf_file */


/**
 * Creates the .conf file if not present
 *
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_update_conf_file(uim_instance_global_type *uim_ptr)
{
  char           *buffer      = NULL;
  uint16          index       = 0;
  uint16          buf_len     = 0;
  struct fs_stat  conf_stat;

  buf_len = strlen(UIM_JCDMA_MODE_FILE)
            + strlen(UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE)
            + strlen(UIM_FEATURES_STATUS_LIST_FILE)
            + strlen(UIM_HW_CONFIG_NV_EF)
            + strlen(UIM_FEATURE_SUPPORT_HOTSWAP_FILE)
            + strlen(UIM_ACTIVE_SLOT_NV)
#ifdef FEATURE_UIM_POLLING_DISABLE_CAPABILITY
            #error code not present
#endif /* FEATURE_UIM_POLLING_DISABLE_CAPABILITY */
            + strlen(UIM_PDOWN_NV_EF)
            + strlen(UIM_FEATURE_BUSY_RESPONSE_FILE)
            + strlen(UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE)
#ifdef FEATURE_UIM_REMOTE_UIM
            + strlen(UIM_REMOTE_COMMAND_RESP_TIMER)
#endif
#ifdef FEATURE_UIM_BTSAP_CLIENT
            + strlen(UIM_INSTANCE1_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT)
            + strlen(UIM_INSTANCE2_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT)
#endif
            + 50;

  /*Allocating memory to the buffer to hold all the string names plus additional 50 bytes */
  buffer = uim_malloc(buf_len);

  if(NULL == buffer)
  {
    UIM_MSG_ERR_0("Buffer Memory Allocation Failed");
    /* if allocation fails return */
    return;
  }

  /* Content of the file. Note: there should not be spaces before the names! */
  /* Copy the JCDMA conf contents - jcdma.conf no longer exists */
  (void)strlcpy((char*)&buffer[index], UIM_JCDMA_MODE_FILE, buf_len);
  index += strlen(UIM_JCDMA_MODE_FILE);
  buffer[index++] = '\n';

  /* Copy the HOTSWAP Debounce Config NV item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE, buf_len - index);
  index += strlen(UIM_HOTSWAP_DEBOUNCE_CONFIG_FILE);
  buffer[index++] = '\n';

  /* Copy the uim_features_status NV item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_FEATURES_STATUS_LIST_FILE, buf_len - index);
  index += strlen(UIM_FEATURES_STATUS_LIST_FILE);
  buffer[index++] = '\n';

  /* Copy the UIM_HW_CONFIG NV item file name*/
  (void)strlcpy((char*)&buffer[index],(void *)UIM_HW_CONFIG_NV_EF, buf_len - index);
  index += strlen(UIM_HW_CONFIG_NV_EF);
  buffer[index++] = '\n';

    /* Copy the feature_support_hotswap NV item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_FEATURE_SUPPORT_HOTSWAP_FILE, buf_len - index);
  index += strlen(UIM_FEATURE_SUPPORT_HOTSWAP_FILE);
  buffer[index++] = '\n';

  /* Copy the uim_current_active_slot NV item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_ACTIVE_SLOT_NV, buf_len - index);
  index += strlen(UIM_ACTIVE_SLOT_NV);
  buffer[index++] = '\n';

 #ifdef FEATURE_UIM_POLLING_DISABLE_CAPABILITY
  #error code not present
#endif /* FEATURE_UIM_POLLING_DISABLE_CAPABILITY */

  /* Copy the uim_null_proc_byte_timer NV item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_PDOWN_NV_EF, buf_len - index);
   index += strlen(UIM_PDOWN_NV_EF);
   buffer[index++] = '\n';

  /* Copy the uim_feature_busy_response EFS item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_FEATURE_BUSY_RESPONSE_FILE, buf_len - index);
  index += strlen(UIM_FEATURE_BUSY_RESPONSE_FILE);
  buffer[index++] = '\n';

  /* Copy the uim_feature_busy_response_simulate_null EFS item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE, buf_len - index);
  index += strlen(UIM_FEATURE_BUSY_RESPONSE_SIMULATE_NULL_FILE);
  buffer[index++] = '\n';

#ifdef FEATURE_UIM_REMOTE_UIM
    /* Copy the uim_remote_command_response_timer EFS item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_REMOTE_COMMAND_RESP_TIMER, buf_len - index);
  index += strlen(UIM_REMOTE_COMMAND_RESP_TIMER);
  buffer[index++] = '\n';
#endif /* FEATURE_UIM_REMOTE_UIM */

#ifdef FEATURE_UIM_BTSAP_CLIENT

   /* Copy the uim_remote_command_response_timer EFS item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_INSTANCE1_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT, buf_len - index);
  index += strlen(UIM_INSTANCE1_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT);
  buffer[index++] = '\n';

   /* Copy the uim_remote_command_response_timer EFS item file name*/
  (void)strlcpy((char*)&buffer[index], UIM_INSTANCE2_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT, buf_len - index);
  index += strlen(UIM_INSTANCE2_LAST_MODE_AND_CONFIG_DELAY_FOR_REMOTE_CONNECT);
  buffer[index++] = '\n';

#endif

  buffer[index++] = 0x0;
  /* Create directory, if needed */
  memset(&conf_stat, 0, sizeof(conf_stat));
  if (efs_stat(UIM_CONF_DIR1, &conf_stat) == -1 )
  {
    (void)efs_mkdir(UIM_CONF_DIR1, 0777);
    (void)efs_mkdir(UIM_CONF_DIR2, 0777);
    (void)efs_mkdir(UIM_CONF_DIR3, 0777);
  }
  else if (efs_stat(UIM_CONF_DIR2, &conf_stat) == -1 )
  {
    (void)efs_mkdir(UIM_CONF_DIR2, 0777);
    (void)efs_mkdir(UIM_CONF_DIR3, 0777);
  }
  else if (efs_stat(UIM_CONF_DIR3, &conf_stat) == -1 )
  {
    (void)efs_mkdir(UIM_CONF_DIR3, 0777);
  }

  uim_compare_and_update_conf_file(UIM_CONF_FILE,
                                   buffer,
                                   (uint16) strlen(buffer),
                                   uim_ptr);

  UIM_FREE(buffer);
} /* uim_update_conf_file */


/**
 * Utility function used to map efs based item path to an enum.
 *
 * @param item_path Item path.
 *
 * @return uim_nvitems_enum NV item enum.
 */
static uim_nvitems_enum uim_map_nvitem_path_to_enum(
  const char* item_path)
{
  if(!strcmp(item_path, UIM_FEATURES_STATUS_LIST_FILE))
  {
    return UIMDRV_FEATURES_STATUS_NVITEM_ENUM;
  }
  else if(!strcmp(item_path, UIM_FEATURE_SUPPORT_HOTSWAP_FILE))
  {
    return UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM;
  }
  else
  {
    return UIMDRV_MAX_NVITEM_ENUM;
  }
}


/**
 * Function used to write the default value of UIMDRV Features
 * (controlled by various NVITEMs such as
 * UIM_FEATURES_STATUS_LIST_FILE to the respective globals
 * controlling the nvitem.
 *
 * @param item_subfeature Sub feature item number.
 * @param item_path Item path.
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_nv_feature_write_default_value(
  uint8                  item_subfeature,
  const char*            item_path,
  uim_instance_global_type *uim_ptr  )
{
  switch(uim_map_nvitem_path_to_enum(item_path))
  {
    case UIMDRV_FEATURES_STATUS_NVITEM_ENUM:
      switch((uim_nv_features_enum_type) item_subfeature)
      {
        case UIMDRV_FEATURE_HANDLE_NO_ATR_IN_40000_CLK_CYCLES:
          uim_ptr->setting.features_status_list.handle_no_atr_in_40000_clk_cycles = UIMDRV_FEATURE_HANDLE_NO_ATR_IN_40000_CLK_CYCLES_DEFAULT;
          break;
        case UIMDRV_FEATURE_LOG_TO_EFS:
          uim_ptr->setting.features_status_list.log_to_efs = UIMDRV_FEATURE_LOG_TO_EFS_DEFAULT;
          break;
        case UIMDRV_FEATURE_DISABLE_RECOVERY_UPON_INFINITE_NULL:
          uim_ptr->setting.features_status_list.disable_recovery_upon_infinite_null =  UIMDRV_FEATURE_DISABLE_RECOVERY_UPON_INFINITE_NULL_DEFAULT;
          break;
        case UIMDRV_FEATURE_DEBUG_LOG:
          uim_ptr->setting.features_status_list.debug_log = UIMDRV_FEATURE_DEBUG_LOG_DEFAULT;
          break;
        case UIMDRV_FEATURE_RPT_BAD_SW_ON_POLL:
          uim_ptr->setting.features_status_list.rpt_bad_sw_on_poll = UIMDRV_FEATURE_RPT_BAD_SW_ON_POLL_DEFAULT;
          break;
        case UIMDRV_FEATURE_HANDLE_ICCID_READ_FAILURE:
          uim_ptr->setting.features_status_list.handle_iccid_read_failure = UIMDRV_FEATURE_HANDLE_ICCID_READ_FAILURE_DEFAULT;
          break;
        case UIMDRV_FEATURE_SUPPORT_NO_ICCID:
          uim_ptr->setting.features_status_list.support_no_iccid = UIMDRV_FEATURE_SUPPORT_NO_ICCID_DEFAULT;
          break;
        case UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT:
          uim_ptr->setting.features_status_list.min_tpl_iccid_support= UIMDRV_FEATURE_MIN_TPL_ICCID_SUPPORT_DEFAULT;
          break;
        case UIMDRV_FEATURE_HANDLE_UNKNOWN_PROC_BYTES_AS_CMD_TIMEOUT:
          uim_ptr->setting.features_status_list.handle_unknown_proc_bytes_as_cmd_timeout = UIMDRV_FEATURE_HANDLE_UNKNOWN_PROC_BYTES_AS_CMD_TIMEOUT_DEFAULT;
          break;
        case UIMDRV_FEATURE_INTERFACE_NOT_USED:
          uim_ptr->setting.features_status_list.interface_not_used = UIMDRV_FEATURE_INTERFACE_NOT_USED_DEFAULT;
          break;
        case UIMDRV_FEATURE_LOG_APDU_TO_EFS:
          uim_ptr->setting.features_status_list.log_apdu_to_efs = UIMDRV_FEATURE_LOG_APDU_TO_EFS_DEFAULT;
          break;
        case UIMDRV_FEATURE_NO_SWITCH_INST_ON_WWT_EXPIRY:
          uim_ptr->setting.features_status_list.no_switch_inst_on_wwt_expiry = UIMDRV_FEATURE_NO_SWITCH_INST_ON_WWT_EXPIRY_DEFAULT;
          break;
        case UIMDRV_FEATURE_SEND_STATUS_BEFORE_AUTH:
          uim_ptr->setting.features_status_list.send_status_before_auth = UIMDRV_FEATURE_SEND_STATUS_BEFORE_AUTH_DEFAULT;
          break;
        case UIMDRV_FEATURE_TRY_DEFAULT_BAUD_RATE_FOR_F372_D12_CARD:
          uim_ptr->setting.features_status_list.try_default_baud_rate_for_f372_d12_card = UIMDRV_FEATURE_TRY_DEFAULT_BAUD_RATE_FOR_F372_D12_CARD_DEFAULT;
          break;
        case UIMDRV_FEATURE_COLD_RESET_DUE_TO_CARD_SWITCH:
          uim_ptr->setting.features_status_list.cold_reset_due_to_card_switch = UIMDRV_FEATURE_COLD_RESET_DUE_TO_CARD_SWITCH_DEFAULT;
          break;
        case UIMDRV_FEATURE_SM_USE_SLOT_1:
          uim_ptr->setting.features_status_list.sm_prefer_slot1 = UIMDRV_FEATURE_SM_USE_SLOT_1_DEFAULT;
          break;
        case UIMDRV_FEATURE_USE_DUAL_LDO:
          uim_ptr->setting.features_status_list.use_dual_ldo = UIMDRV_FEATURE_USE_DUAL_LDO_DEFAULT;
          break;
        case UIMDRV_FEATURE_UIM_POLLING_ONLY_AT_POLLING_TIMER_EXPIRY:
          uim_ptr->setting.features_status_list.uim_polling_only_at_polling_timer_expiry = UIMDRV_FEATURE_UIM_POLLING_ONLY_AT_POLLING_TIMER_EXPIRY_DEFAULT;
          break;
        case UIMDRV_FEATURE_UIM_SET_CLK_FREQ_AT_4_8_MHZ:
          uim_ptr->setting.features_status_list.uim_set_clk_freq_at_4_8_MHz = UIMDRV_FEATURE_UIM_SET_CLK_FREQ_AT_4_8_MHZ_DEFAULT;
          break;
        case UIMDRV_FEATURE_HANDLE_TC1_BYTE_FOR_EXTRA_GUARD_TIME:
          uim_ptr->setting.features_status_list.handle_tc1_byte_for_extra_guard_time = UIMDRV_FEATURE_HANDLE_TC1_BYTE_FOR_EXTRA_GUARD_TIME_DEFAULT;
          break;
        case UIMDRV_FEATURE_ENABLE_SIM_MODE_CHANGE_VIA_WARM_RESET:
          uim_ptr->setting.features_status_list.enable_sim_mode_change_via_warm_reset = UIMDRV_FEATURE_ENABLE_SIM_MODE_CHANGE_VIA_WARM_RESET_DEFAULT;
          break;
        case UIMDRV_FEATURE_ENABLE_EXPLICIT_SELECTION_OF_MF_OR_ADF:
          uim_ptr->setting.features_status_list.enable_explicit_selection_of_mf_or_adf = UIMDRV_FEATURE_ENABLE_EXPLICIT_SELECTION_OF_MF_OR_ADF_DEFAULT;
          break;
        case UIMDRV_FEATURE_ENABLE_BOOT_UP_IN_INVERSE_CONVENTION:
          uim_ptr->setting.features_status_list.enable_boot_up_in_inverse_convention = UIMDRV_FEATURE_ENABLE_BOOT_UP_IN_INVERSE_CONVENTION_DEFAULT;
          break;
        case UIMDRV_FEATURE_RECOVERY_ON_BAD_STATUS_WORD:
          uim_ptr->setting.features_status_list.recovery_on_bad_status_word = UIMDRV_FEATURE_RECOVERY_ON_BAD_STATUS_WORD_DEFAULT;
          break;
        case UIMDRV_FEATURE_ATTEMPT_PUP_3V_FROM_nTH_RECOVERY:
          uim_ptr->setting.features_status_list.attempt_pup_3v_from_nth_recovery = UIMDRV_FEATURE_ATTEMPT_PUP_3V_FROM_nTH_RECOVERY_DEFAULT;
          break;
        case UIMDRV_FEATURE_LE_SUPPORT_FOR_7816_STREAM_APDU:
          uim_ptr->setting.features_status_list.enable_le_support_for_stream_7816_apdu = UIMDRV_FEATURE_LE_SUPPORT_FOR_7816_STREAM_APDU_DEFAULT;
          break;
        case UIMDRV_FEATURE_DISABLE_CARD_STATUS_CHECK_AT_POWER_UP:
          uim_ptr->setting.features_status_list.disable_card_status_check_at_power_up = UIMDRV_FEATURE_DISABLE_CARD_STATUS_CHECK_AT_POWER_UP_DEFAULT;
          break;
        case UIMDRV_FEATURE_SIMTRAY_WITH_GPIOS_TIED:
          uim_ptr->setting.features_status_list.simtray_with_gpios_tied = UIMDRV_FEATURE_SIMTRAY_WITH_GPIOS_TIED_DEFAULT;
          break;
        default:
          UIMDRV_MSG_ERR_1(uim_ptr->id,"Invalid Feature 0x%x", item_subfeature);
      }
      break;
    case UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM:
      switch((uim_nv_feature_support_hotswap_enum_type) item_subfeature)
      {
        case UIMDRV_FEATURE_SUPPORT_HOTSWAP:
          uim_ptr->hotswap.feature_support_hotswap.feature_support_hotswap = FALSE;
          break;
        default:
          UIMDRV_MSG_ERR_1(uim_ptr->id,"Invalid Feature 0x%x", item_subfeature);
      }
      break;
    default:
      UIMDRV_MSG_ERR_0(uim_ptr->id,"Write attempted on invalid uim NVITEM ");
      break;
  }
} /* uim_nv_feature_write_default_value */


/**
 * Function used to write the default value of subfeatures
 * controlled by a nvitem that are delta between the two
 * versions of the nvitem to the respective globals.
 *
 * @param version_in_nv NV version in NV file itself.
 * @param version_in_build NV version in Build.
 * @param item_path Item path
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_nv_write_default_value_of_delta_features
(
  uint8                  version_in_nv,
  uint8                  version_in_build,
  const char*            item_path,
  uim_instance_global_type *uim_ptr
)
{
  uint8 uimdrv_feature;
  uint8 num_features_in_nv;
  uint8 num_features_in_build;

  switch(uim_map_nvitem_path_to_enum(item_path))
  {
    case UIMDRV_FEATURES_STATUS_NVITEM_ENUM:
      if(
           (version_in_nv >=
            sizeof(uimdrv_features_total_till_version)) ||
           (version_in_build >=
            sizeof(uimdrv_features_total_till_version))
        )
      {
        UIMDRV_MSG_ERR_0(uim_ptr->id,"Array out-of-bounds while writing default values for UIMDRV features");
        return;
      }

      num_features_in_nv    = uimdrv_features_total_till_version[version_in_nv];
      num_features_in_build = uimdrv_features_total_till_version[version_in_build];
      /* Since uimdrv_features_total_till_version array is indexed by the
         feature_status_list *version*, it's value at version version_in_nv is
         essentially the first feature of the delta list...no need to bail out if
         case writing fails... */
      for(uimdrv_feature = num_features_in_nv; uimdrv_feature < num_features_in_build; uimdrv_feature++)
      {
        uim_nv_feature_write_default_value(uimdrv_feature, item_path, uim_ptr);
      }
      return;

    case UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM:
      if(
           (version_in_nv >=
            sizeof(uimdrv_feature_support_hotswap_total_till_version)) ||
           (version_in_build >=
            sizeof(uimdrv_feature_support_hotswap_total_till_version))
        )
      {
        UIMDRV_MSG_ERR_0(uim_ptr->id,"Array out-of-bounds while writing default value for HOTSWAP SUPPORT feature");
        return;
      }

      num_features_in_nv    = uimdrv_feature_support_hotswap_total_till_version[version_in_nv];
      num_features_in_build = uimdrv_feature_support_hotswap_total_till_version[version_in_build];
      for(uimdrv_feature = num_features_in_nv; uimdrv_feature < num_features_in_build; uimdrv_feature++)
      {
        uim_nv_feature_write_default_value(uimdrv_feature, item_path, uim_ptr);
      }
      return;
    default:
      return;
  }
} /* uim_nv_write_default_value_of_delta_features */


/**
 * Function used to write the default values of all the
 * sub-features controlled by various uimdrv NVITEMs to their
 * respective globals.
 *
 * @param item_path Item path.
 * @param uim_ptr Pointer to the global data common
 */
static void uim_nv_write_default_value_of_all_features
(
  const char               *item_path,
  uim_instance_global_type *uim_ptr
)
{
  uint8 uimdrv_feature;
  uint8 num_features_in_build;

  RETURN_IF_INSTANCE_INVALID(uim_ptr->id);

  switch(uim_map_nvitem_path_to_enum(item_path))
  {
    case UIMDRV_FEATURES_STATUS_NVITEM_ENUM:
      num_features_in_build = uimdrv_features_total_till_version[UIM_FEATURES_STATUS_LIST_CURRENT_VERSION];

      memset(&uim_ptr->setting.features_status_list, 0x00, sizeof(uim_features_status_list_type));
      /* Update the Version in features_status_list to the current build version */
      uim_ptr->setting.features_status_list.version = UIM_FEATURES_STATUS_LIST_CURRENT_VERSION;

      /* Update uim_features_status_list with default values */
      for(uimdrv_feature = 0; uimdrv_feature < num_features_in_build;
          uimdrv_feature++)
      {
        uim_nv_feature_write_default_value(
           (uim_nv_features_enum_type)uimdrv_feature,
           item_path, uim_ptr);
      }
      break;
    case UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM:
      num_features_in_build = uimdrv_feature_support_hotswap_total_till_version[UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION];

      memset(&uim_ptr->hotswap.feature_support_hotswap, 0x00,
             sizeof(uim_feature_support_hotswap_type));
      /* Update the Version in features_status_list to the current build version */
      uim_ptr->hotswap.feature_support_hotswap.version =
        UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION;

      /* Update uim_feature_support_hotswap with default values */
      for(uimdrv_feature = 0; uimdrv_feature < num_features_in_build;
           uimdrv_feature++)
      {
        uim_nv_feature_write_default_value(
           (uim_nv_feature_support_hotswap_enum_type)uimdrv_feature,
            item_path, uim_ptr);
      }
      break;
    default:
      break;
  }
} /* uim_nv_write_default_value_of_all_features */



/**
 * Function used to validate the various NVITEM's versions that
 * support it.If what's in the NV is not the one currently
 * defined in the build, updates it to the current version
 * defined and writes it back to the NV.
 * Version gets updated in the NV if it doesn't match the one in
 * the build. If the version is updated, next time when the user
 * reads the NVITEM, a dropdown of items corresponding to the
 * updated version is shown instead of what the user saw the
 * first time the NV read was done (corresponding to the version
 * present in the NV at that point).
 *
 * @param item_path Item path.
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_nv_features_status_list_update_version
(
  const char* item_path,
  uim_instance_global_type *uim_ptr
)
{
  int status = 0;

  switch(uim_map_nvitem_path_to_enum(item_path))
  {
    case UIMDRV_FEATURES_STATUS_NVITEM_ENUM:
      UIMDRV_MSG_HIGH_2(uim_ptr->id,"UIMDRV Features Status NV ITEM version - in NV: 0x%x, in build: 0x%x",
                        uim_ptr->setting.features_status_list.version,
                        UIM_FEATURES_STATUS_LIST_CURRENT_VERSION);
      if ((int)uim_ptr->setting.features_status_list.version < UIM_FEATURES_STATUS_LIST_CURRENT_VERSION)
      {
        /* If the uim_features_status_list version in NV is less than current
           uim_features_status_list version in our code, meaning new features
           have been added since the version in NV till the current version, we
           write back the default value of all those delta features to the NV. */
        uim_nv_write_default_value_of_delta_features(
          uim_ptr->setting.features_status_list.version,
          UIM_FEATURES_STATUS_LIST_CURRENT_VERSION,
          item_path,
          uim_ptr);

        uim_ptr->setting.features_status_list.version = UIM_FEATURES_STATUS_LIST_CURRENT_VERSION;
        status = efs_put(UIM_FEATURES_STATUS_LIST_FILE, &uim_ptr->setting.features_status_list,
                         sizeof(uim_features_status_list_type),
                         O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
        if (status != 0)
        {
          UIMDRV_MSG_ERR_1(uim_ptr->id,"UIMDRV Features Status NV ITEM version update failed!, error %d",
                           status);
        }
      }
      break;
    case UIMDRV_SUPPORT_HOTSWAP_NVITEM_ENUM:
      UIMDRV_MSG_HIGH_2(uim_ptr->id,"UIMDRV Hotswap Support NV ITEM version - in NV: 0x%x, in build: 0x%x",
                        uim_ptr->hotswap.feature_support_hotswap.version,
                        UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION);
      if ((int)uim_ptr->hotswap.feature_support_hotswap.version < UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION)
      {
        /* If the uim_feature_support_hotswap version in NV is less than current
           uim_feature_support_hotswap version in our code, meaning new features
           have been added since the version in NV till the current version, we
           write back the default value of all those delta features to the NV. */
        uim_nv_write_default_value_of_delta_features(
          uim_ptr->setting.features_status_list.version,
          UIM_FEATURES_STATUS_LIST_CURRENT_VERSION,
          item_path,
          uim_ptr);

        uim_ptr->hotswap.feature_support_hotswap.version = UIM_FEATURE_SUPPORT_HOTSWAP_CURRENT_VERSION;
        status = efs_put(UIM_FEATURE_SUPPORT_HOTSWAP_FILE, &uim_ptr->hotswap.feature_support_hotswap,
                         sizeof(uim_feature_support_hotswap_type),
                         O_CREAT|O_RDWR|O_AUTODIR|O_TRUNC,0777);
        if (status != 0)
        {
          UIMDRV_MSG_ERR_1(uim_ptr->id,"UIMDRV Hotswap Support NV ITEM version update failed!, error %d",
                           status);
        }
      }
      break;
    default:
      break;
  }
} /* uim_nv_features_status_list_update_version */


/**
 * Set config params with default value or with values from NVs.
 *
 * @param out_uim_config_params Output data of config params
 * @param read_from_nv_uim_config_params Config params from NVs.
 * @param nv_read_status NV status.
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_validate_and_set_config_params(
  nv_uim_config_params_type        *out_uim_config_params,
  nv_uim_config_params_type const  *read_from_nv_uim_config_params,
  nv_stat_enum_type                 nv_read_status,
  uim_instance_global_type         *uim_ptr
)
{
  uint8 rfu_bytes_index = 0;
  if (nv_read_status != NV_DONE_S)
  {
    UIMDRV_MSG_HIGH_0(uim_ptr->id,"Nv Read UIM CONFIGURATION PARAMS failed - Setting defaults");
    /* Additional delay to receive ATR over and above 40000 clk cycles */
    out_uim_config_params->additional_delay_for_atr = 0;
    /* Number of times an external uim command shall be re-tried over and above the default 2 */
    out_uim_config_params->ext_cmd_additional_re_try_cnt = 0;
    /* Number of parity errors that would be tolerated over and above 20 */
    out_uim_config_params->additional_parity_err_cnt = 0;
    /* Number of rx_break errors that would be tolerated over and above 10 */
    out_uim_config_params->additional_rx_break_err_cnt = 0;
    /* Additional delay when direction of transmission changes from card to ME above 150 micro seconds */
    out_uim_config_params->additional_delay_when_dir_change = 0;
    /* Boolean to indicate whether or not a poll time out would be recovered if in a call */
    out_uim_config_params->uim_recover_poll_t_out_while_in_call = 1;
    /* Whether or not CPHS is read after internal reset if UICC */
    out_uim_config_params->read_usim_cphs = TRUE;
    /* Whether or not UST is read after internal reset if UICC */
    out_uim_config_params->read_usim_ust = TRUE;
    /* Whether or not EST is read after internal reset if UICC */
    out_uim_config_params->read_usim_est = TRUE;
    /* Whether or not NETPAR is read after internal reset if UICC */
    out_uim_config_params->read_usim_netpar = TRUE;
    /* Whether or not RPLMNACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_rplmnact = TRUE;
    /* Whether or not USIM_AD is read after internal reset if UICC */
    out_uim_config_params->read_usim_ad = TRUE;
    /* Whether or not IMSI is read after internal reset if UICC */
    out_uim_config_params->read_usim_imsi = TRUE;
    /* Whether or not USIM_ACC is read after internal reset if UICC */
    out_uim_config_params->read_usim_acc = FALSE;
    /* Whether or not USIM_HPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_hplmn = FALSE;
    /* Whether or not HPLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_hplmnwact = FALSE;
    /* Whether or not PLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_plmnwact = FALSE;
    /* Whether or not OPLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_oplmnwact = FALSE;
    /* Whether or not LOCI is read after internal reset if UICC */
    out_uim_config_params->read_usim_loci = FALSE;
    /* Whether or not PSLOCI is read after internal reset if UICC */
    out_uim_config_params->read_usim_psloci = FALSE;
    /* Whether or not KEYS is read after internal reset if UICC */
    out_uim_config_params->read_usim_keys = FALSE;
    /* Whether or not KEYSPS is read after internal reset if UICC */
    out_uim_config_params->read_usim_keysps = FALSE;
    /* Whether or not FPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_fplmn = FALSE;
    /* Whether or not START_HFN is read after internal reset if UICC */
    out_uim_config_params->read_usim_start_hfn = FALSE;
    /* Whether or not THRESHOLD is read after internal reset if UICC */
    out_uim_config_params->read_usim_threshold = FALSE;
    /* Whether or not CBMID is read after internal reset if UICC */
    out_uim_config_params->read_usim_cbmid = FALSE;
    /* Whether or not KC is read after internal reset if UICC */
    out_uim_config_params->read_usim_kc = FALSE;
    /* Whether or not KCGPRS is read after internal reset if UICC */
    out_uim_config_params->read_usim_kcgprs = FALSE;
    /* Whether or not ACM is read after internal reset if UICC */
    out_uim_config_params->read_usim_acm = FALSE;
    /* Whether or not ACM_MAX is read after internal reset if UICC */
    out_uim_config_params->read_usim_acm_max = FALSE;
    /* Whether or not EHPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_ehplmn = FALSE;


    /* Whether or not CPHS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_cphs = TRUE;
    /* Whether or not SST is read after internal reset if GSM */
    out_uim_config_params->read_gsm_sst = TRUE;
    /* Whether or not HPLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_hplmn = TRUE;
    /* Whether or not AD is read after internal reset if GSM */
    out_uim_config_params->read_gsm_ad = TRUE;
    /* Whether or not IMSI is read after internal reset if GSM */
    out_uim_config_params->read_gsm_imsi = TRUE;
    /* Whether or not ACC is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acc = FALSE;
    /* Whether or not LOCI is read after internal reset if GSM */
    out_uim_config_params->read_gsm_loci = FALSE;
    /* Whether or not KC is read after internal reset if GSM */
    out_uim_config_params->read_gsm_kc = FALSE;
    /* Whether or not FPLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_fplmn = FALSE;
    /* Whether or not PLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_plmn = FALSE;
    /* Whether or not HPLMNACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_hplmnact = FALSE;
    /* Whether or not PLMNWACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_plmnwact = FALSE;
    /* Whether or not OPLMNWACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_oplmnwact = FALSE;
    /* Whether or not LOCIGPRS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_locigprs = FALSE;
    /* Whether or not KCGPRS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_kcgprs = FALSE;
    /* Whether or not CBMID is read after internal reset if GSM */
    out_uim_config_params->read_gsm_cbmid = FALSE;
    /* Whether or not ACM is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acm = FALSE;
    /* Whether or not ACM_MAX is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acm_max = FALSE;
    /* Configuration parameters to add extra wwt, bwt, cwt, guard time,
       delay before and after clock stop, extra Vreg voltage and few
       flags to control power up delay, switching instruction class,
       setting status length, selecting GSM DF */
    for( rfu_bytes_index = 0; rfu_bytes_index < UIM_MAX_CONFIG_PARAMS_RFU_BYTES; rfu_bytes_index++)
    {
      out_uim_config_params->future_use[rfu_bytes_index] = 0;
    }
    out_uim_config_params->future_use[UIM_RFU_INDEX_DELAY_BEFORE_CLK_STOP]   = UIM_DELAY_BEFORE_CLOCK_STOP;
    out_uim_config_params->future_use[UIM_RFU_INDEX_DELAY_AFTER_CLK_RESTART] = UIM_DELAY_AFTER_CLOCK_RESTART;
  }
  else
  {
    /* Additional delay to receive ATR over and above 40000 clk cycles
     * Could go to the max limit of 65536 additional clock cycles
     */
    out_uim_config_params->additional_delay_for_atr =
      read_from_nv_uim_config_params->additional_delay_for_atr;
    /* Number of times an external uim command shall be re-tried over and above the default 2
     * let us limit it to a maximum of 10
     */
    out_uim_config_params->ext_cmd_additional_re_try_cnt =
      ((read_from_nv_uim_config_params->ext_cmd_additional_re_try_cnt > 10)
      ? 10 : read_from_nv_uim_config_params->ext_cmd_additional_re_try_cnt);

    /* Number of parity errors that would be tolerated over and above 20
     * Could go to the max limit of 255
     */
    out_uim_config_params->additional_parity_err_cnt =
      read_from_nv_uim_config_params->additional_parity_err_cnt;

    /* Number of rx_break errors that would be tolerated over and above 10
     * Could go to the max limit of 255
     */
    out_uim_config_params->additional_rx_break_err_cnt =
      read_from_nv_uim_config_params->additional_rx_break_err_cnt;

    /* Additional delay when direction of transmission changes from card to ME above 150 micro seconds
     * Could go to the max limit of 255
     */
    out_uim_config_params->additional_delay_when_dir_change =
      read_from_nv_uim_config_params->additional_delay_when_dir_change;
    /* Add this to the var which uses the delay */
    uim_ptr->setting.response_byte_delay +=
      out_uim_config_params->additional_delay_when_dir_change;

    /* Boolean to indicate whether or not a poll time out would be recovered if in a call */
    out_uim_config_params->uim_recover_poll_t_out_while_in_call =
      read_from_nv_uim_config_params->uim_recover_poll_t_out_while_in_call;


    /* Whether or not CPHS is read after internal reset if UICC */
    out_uim_config_params->read_usim_cphs =
      read_from_nv_uim_config_params->read_usim_cphs;
    /* Whether or not UST is read after internal reset if UICC */
    out_uim_config_params->read_usim_ust =
      read_from_nv_uim_config_params->read_usim_ust;
    /* Whether or not EST is read after internal reset if UICC */
    out_uim_config_params->read_usim_est =
      read_from_nv_uim_config_params->read_usim_est;
    /* Whether or not NETPAR is read after internal reset if UICC */
    out_uim_config_params->read_usim_netpar =
      read_from_nv_uim_config_params->read_usim_netpar;
    /* Whether or not RPLMNACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_rplmnact =
      read_from_nv_uim_config_params->read_usim_rplmnact;
    /* Whether or not USIM_AD is read after internal reset if UICC */
    out_uim_config_params->read_usim_ad =
      read_from_nv_uim_config_params->read_usim_ad;
    /* Whether or not IMSI is read after internal reset if UICC */
    out_uim_config_params->read_usim_imsi =
      read_from_nv_uim_config_params->read_usim_imsi;
    /* Whether or not USIM_ACC is read after internal reset if UICC */
    out_uim_config_params->read_usim_acc =
      read_from_nv_uim_config_params->read_usim_acc;
    /* Whether or not USIM_HPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_hplmn =
      read_from_nv_uim_config_params->read_usim_hplmn;
    /* Whether or not HPLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_hplmnwact =
      read_from_nv_uim_config_params->read_usim_hplmnwact;
    /* Whether or not PLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_plmnwact =
      read_from_nv_uim_config_params->read_usim_plmnwact;
    /* Whether or not OPLMNWACT is read after internal reset if UICC */
    out_uim_config_params->read_usim_oplmnwact =
      read_from_nv_uim_config_params->read_usim_oplmnwact;
    /* Whether or not LOCI is read after internal reset if UICC */
    out_uim_config_params->read_usim_loci =
      read_from_nv_uim_config_params->read_usim_loci;
    /* Whether or not PSLOCI is read after internal reset if UICC */
    out_uim_config_params->read_usim_psloci =
      read_from_nv_uim_config_params->read_usim_psloci;
    /* Whether or not KEYS is read after internal reset if UICC */
    out_uim_config_params->read_usim_keys =
      read_from_nv_uim_config_params->read_usim_keys;
    /* Whether or not KEYSPS is read after internal reset if UICC */
    out_uim_config_params->read_usim_keysps =
      read_from_nv_uim_config_params->read_usim_keysps;
    /* Whether or not FPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_fplmn =
      read_from_nv_uim_config_params->read_usim_fplmn;
    /* Whether or not START_HFN is read after internal reset if UICC */
    out_uim_config_params->read_usim_start_hfn =
      read_from_nv_uim_config_params->read_usim_start_hfn;
    /* Whether or not THRESHOLD is read after internal reset if UICC */
    out_uim_config_params->read_usim_threshold =
      read_from_nv_uim_config_params->read_usim_threshold;
    /* Whether or not CBMID is read after internal reset if UICC */
    out_uim_config_params->read_usim_cbmid =
      read_from_nv_uim_config_params->read_usim_cbmid;
    /* Whether or not KC is read after internal reset if UICC */
    out_uim_config_params->read_usim_kc =
      read_from_nv_uim_config_params->read_usim_kc;
    /* Whether or not KCGPRS is read after internal reset if UICC */
    out_uim_config_params->read_usim_kcgprs =
      read_from_nv_uim_config_params->read_usim_kcgprs;
    /* Whether or not ACM is read after internal reset if UICC */
    out_uim_config_params->read_usim_acm =
      read_from_nv_uim_config_params->read_usim_acm;
    /* Whether or not ACM_MAX is read after internal reset if UICC */
    out_uim_config_params->read_usim_acm_max =
      read_from_nv_uim_config_params->read_usim_acm_max;
    /* Whether or not EHPLMN is read after internal reset if UICC */
    out_uim_config_params->read_usim_ehplmn =
      read_from_nv_uim_config_params->read_usim_ehplmn;


    /* Whether or not CPHS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_cphs =
      read_from_nv_uim_config_params->read_gsm_cphs;
    /* Whether or not SST is read after internal reset if GSM */
    out_uim_config_params->read_gsm_sst =
      read_from_nv_uim_config_params->read_gsm_sst;
    /* Whether or not HPLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_hplmn =
      read_from_nv_uim_config_params->read_gsm_hplmn;
    /* Whether or not AD is read after internal reset if GSM */
    out_uim_config_params->read_gsm_ad =
      read_from_nv_uim_config_params->read_gsm_ad;
    /* Whether or not IMSI is read after internal reset if GSM */
    out_uim_config_params->read_gsm_imsi =
      read_from_nv_uim_config_params->read_gsm_imsi;
    /* Whether or not ACC is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acc =
      read_from_nv_uim_config_params->read_gsm_acc;
    /* Whether or not LOCI is read after internal reset if GSM */
    out_uim_config_params->read_gsm_loci =
      read_from_nv_uim_config_params->read_gsm_loci;
    /* Whether or not KC is read after internal reset if GSM */
    out_uim_config_params->read_gsm_kc =
      read_from_nv_uim_config_params->read_gsm_kc;
    /* Whether or not FPLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_fplmn =
      read_from_nv_uim_config_params->read_gsm_fplmn;
    /* Whether or not PLMN is read after internal reset if GSM */
    out_uim_config_params->read_gsm_plmn =
      read_from_nv_uim_config_params->read_gsm_plmn;
    /* Whether or not HPLMNACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_hplmnact =
      read_from_nv_uim_config_params->read_gsm_hplmnact;
    /* Whether or not PLMNWACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_plmnwact =
      read_from_nv_uim_config_params->read_gsm_plmnwact;
    /* Whether or not OPLMNWACT is read after internal reset if GSM */
    out_uim_config_params->read_gsm_oplmnwact =
      read_from_nv_uim_config_params->read_gsm_oplmnwact;
    /* Whether or not LOCIGPRS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_locigprs =
      read_from_nv_uim_config_params->read_gsm_locigprs;
    /* Whether or not KCGPRS is read after internal reset if GSM */
    out_uim_config_params->read_gsm_kcgprs =
      read_from_nv_uim_config_params->read_gsm_kcgprs;
    /* Whether or not CBMID is read after internal reset if GSM */
    out_uim_config_params->read_gsm_cbmid =
      read_from_nv_uim_config_params->read_gsm_cbmid;
    /* Whether or not ACM is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acm =
      read_from_nv_uim_config_params->read_gsm_acm;
    /* Whether or not ACM_MAX is read after internal reset if GSM */
    out_uim_config_params->read_gsm_acm_max =
      read_from_nv_uim_config_params->read_gsm_acm_max;
    /* Configuration parameters to add extra wwt, bwt, cwt, guard time,
       delay before and after clock stop, extra Vreg voltage and few
       flags to control power up delay, switching instruction class,
       setting status length, selecting GSM DF */
    for( rfu_bytes_index = 0; rfu_bytes_index < UIM_MAX_CONFIG_PARAMS_RFU_BYTES; rfu_bytes_index++)
    {
      out_uim_config_params->future_use[rfu_bytes_index] = read_from_nv_uim_config_params->future_use[rfu_bytes_index];
    }
  }
} /* uim_validate_and_set_config_params */



