#ifndef UIMDRV_PMIC_H
#define UIMDRV_PMIC_H
/*============================================================================
  FILE:          uimdrv_pmic.h

  OVERVIEW:     File contains the inclusions and definitions necesssary for
                the pmic interface.

  DEPENDENCIES: N/A

                Copyright (c) 2012-2014QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimdrv/src/hw/pmic/uimdrv_pmic.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

  when      who        what, where, why
------      ----       -----------------------------------------------------------
05/07/14    lxu        Read cmd_rsp_time from nv for usb mode, give cmd_rsp_time and
                       voltage_class to ap when send usb power up to ap
04/11/14    lxu        Unregister automatic UICC shutdown on card removal in USB mode
11/27/13    akv        Initialization of hw enumeration based on dal chip family
10/08/13    nmb        HW header file cleanup
07/12/13    nmb        Global Reorganization
07/12/13    akv        HW Enumeration - modifying target specific hardware values
                       and organizing them in value-key pairs
02/22/13    js         8x26 bring up changes
12/05/12    js         UIM Parallel Processing changes
10/26/12    akv/ssr    HW Enumeration changes
============================================================================*/
#include "comdef.h"
#include "uimdrv_main.h"

/* MACROS AND FUNCTIONS FOR PMIC CONTROL */

/* This value represents the desired voltage in mV */
#define UIM_1_8V_PROG         1800

#if defined( FEATURE_UIM_DRIVE_MAX_PMIC_VOLTAGE )
  #define UIM_3V_PROG         3050
#else
  #define UIM_3V_PROG         3000
#endif /* FEATURE_UIM_DRIVE_MAX_PMIC_VOLTAGE */

/* Table holding the default PMIC NPA reference for different UIM instances. */
extern const uim_pmic_npa_resource_table_type   uim_pmic_npa_resource_table[];

/* Function to initialize Pmic */
void initialize_pmic(uim_instance_enum_type uim_instance);

void npaIssueRequiredRequest(uim_instance_global_type *uim_ptr , boolean req);

void UIM_PROG_VOLTAGE_CLASS(uim_instance_global_type *uim_ptr, uint32 voltage_ctl);
void UIM_POWER_OFF_SLOT(uim_instance_global_type *uim_ptr);
void UIM_POWER_ON_SLOT(uim_instance_global_type *uim_ptr);
void uim_set_shutdown_control(uim_instance_enum_type uim_instance, boolean enable);
uint32 uim_get_pmic_npa_mode(uim_instance_enum_type uim_instance);
#endif /* UIMDRV_PMIC_H */
