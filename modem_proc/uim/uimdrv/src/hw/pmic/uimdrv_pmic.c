/*============================================================================
  FILE:         uimdrv_pmic.c

  OVERVIEW:     File conatins the functions used to initialize and control
                the pmic interface.

  DEPENDENCIES: N/A

                Copyright (c) 2012 - 2016 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimdrv/src/hw/pmic/uimdrv_pmic.c#2 $
$DateTime: 2016/04/28 02:16:22 $
$Author: naushada $

when        who        what, where, why
------      ----       -----------------------------------------------------------
04/28/16    na         Not wait for QMI ACK after receiving TASK_STOP
04/28/16    na         UIMDRV-QMI interactions - Indications and acks for LDO events
01/14/15    ks         8929 bringup code
06/11/14    lxu        Read cmd_rsp_time from nv for usb mode, give cmd_rsp_time and
                       voltage_class to ap when send usb power up to ap
05/19/14    ks         8936 bring up changes
04/11/14    lxu        Unregister automatic UICC shutdown on card removal in USB mode
02/18/14    sam        Usage of newer style diag macros
01/24/14    yk         Code Clean up
12/27/13    ks         8916 bring up changes
11/27/13    akv        Initialization of hw enumeration based on dal chip family
10/08/13    nmb        HW header file cleanup
10/03/13    sam        F3 log reduction
09/12/12    na         8x62 bring up changes
08/29/13    na         allow UIM2 config on UIM1 when UIM1 is disabled and UIM2 enabled
08/06/13    rm/ak      Bolt RUMI code
07/12/13    nmb        Global Reorganization
07/12/13    akv        HW Enumeration - modifying target specific hardware values
                       and organizing them in value-key pairs
06/24/13    js         TSTS changes
05/22/13    akv        Configuration of UICC shutdown gpio's and feature
                       enablement based on NV
05/16/13    akv        Fix for compilation warning on Triton
03/27/13    akv        UICC shutdown upon battery removal
03/18/13    js         8x26 UIM3 interface bring up fixes.
02/22/13    js         8x26 bring up changes
02/12/13    js         General F3 macro cleanup
02/01/13    akv        Run time configuration of UICC shutdown feature
12/05/12    js         UIM Parallel Processing changes
10/26/12    akv/ssr    HW Enumeration changes
============================================================================*/
#include "uimdrv_pmic.h"
#include "uimdrv_uartdm.h"
#include "uimdrv_rumi.h"
#include "DDIChipInfo.h"
#include "DDIPlatformInfo.h"

#include "uimglobals.h"

static char pmic_npa_group_id_uim_enable[][30]      =
   {PMIC_NPA_GROUP_ID_UIM1_ENABLE, PMIC_NPA_GROUP_ID_UIM2_ENABLE,
    PMIC_NPA_GROUP_ID_UIM3_ENABLE};
static char pmic_npa_group_id_uim_activity[][30]    =
   {PMIC_NPA_GROUP_ID_UIM1_ACTIVITY, PMIC_NPA_GROUP_ID_UIM2_ACTIVITY,
   PMIC_NPA_GROUP_ID_UIM3_ACTIVITY};

/* Table holding the default PMIC NPA reference for different UIM instances. */

const uim_pmic_npa_resource_table_type   uim_pmic_npa_resource_table[] = {

  /* ARAGORN_8974 */  { DALCHIPINFO_FAMILY_MSM8974,
                         /*  UIM1_UART_NUM,           UIM2_UART_NUM,             UIM3_UART_NUM,           UIM4_UART_NUM  */
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      },  /* ARAGORN_8974 */

    /* ARAGORN_PRO */ { DALCHIPINFO_FAMILY_MSM8974_PRO,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      },  /* ARAGORN_PRO */

     /* ELAN_9X25 */  { DALCHIPINFO_FAMILY_MDM9x25,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      },  /* ELAN_9X25 */

     /* DINO_8X10 */  { DALCHIPINFO_FAMILY_MSM8x10,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* DINO_8X10 */

     /* FRODO_8X26 */ { DALCHIPINFO_FAMILY_MSM8x26,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* FRODO_8X26 */

     /* GIMLI_8926 */ { DALCHIPINFO_FAMILY_MSM8926,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* GIMLI_8926 */

     /* TORINO_9X35 */{ DALCHIPINFO_FAMILY_MDM9x35,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      },  /* TORINO_9X35 */

     /* THORIN_8X62 */{ DALCHIPINFO_FAMILY_MSM8x62,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      },  /* THORIN_8X62 */

  /* BAGHEERA_8916 */ { DALCHIPINFO_FAMILY_MSM8916,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* BAGHEERA_8916 */

     /* SHERE_8936 */ { DALCHIPINFO_FAMILY_MSM8936,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* SHERE_8936 */

     /* KICHI_8929 */ { DALCHIPINFO_FAMILY_MSM8929,
                       { E_PMIC_NPA_RESOURCE_UIM1, E_PMIC_NPA_RESOURCE_UIM2, E_PMIC_NPA_RESOURCE_UIM3, INVALID_PMIC_NPA_RESOURCE }
                      },  /* KICHI_8929 */

     /* DALCHIPINFO_FAMILY_UNKNOWN */{ DALCHIPINFO_FAMILY_UNKNOWN,
                       { INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE, INVALID_PMIC_NPA_RESOURCE }
                      }  /* THORIN_8X62 */

}; /* uim_pmic_npa_resource_table[MAX_UIM_CHIPSET_INDEX] */


/* Function to initialize Pmic */
void initialize_pmic(uim_instance_enum_type uim_instance)
{
  uimVccEnum  pmic_ref;

  uim_hw_if.pmic[uim_instance].m_pmicNpaHandle = NULL;
  uim_hw_if.pmic[uim_instance].m_pmicActivityHandle  = NULL;

  if (uim_instance>= UIM_MAX_INSTANCES)
  {
    UIMDRV_MSG_ERR_0 ( uim_instance,"Unknown UIM Instance in PMIC Reference");
    return;
  }

  pmic_ref = m_HWConfig.uim_slot_config[uim_instance].uimVcc;

  /* store the instance of the slot that are active */
  uim_hw_if.pmic[uim_instance].m_Vcc                 = pmic_ref;
  uim_hw_if.pmic[uim_instance].m_uiccShutBattRemoval = m_HWConfig.uicc_shutdown_feature.uiccShutdownBattRemoval;
  uim_hw_if.pmic[uim_instance].m_uiccShutCardRemoval = m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval[uim_instance];

  uim_hw_if.pmic[uim_instance].m_pmicNpaHandle =
    npa_create_sync_client(pmic_npa_group_id_uim_enable[pmic_ref],
                           "uim", NPA_CLIENT_REQUIRED);
  uim_hw_if.pmic[uim_instance].m_pmicActivityHandle =
    npa_create_sync_client (pmic_npa_group_id_uim_activity[pmic_ref],
                            "uim", NPA_CLIENT_REQUIRED);

  if(NULL == uim_hw_if.pmic[uim_instance].m_pmicActivityHandle)
  {
    UIMDRV_MSG_ERR_0 ( uim_instance,"UIM pmicActivityHandle registration failed for pmic instance");
  }
} /* initialize_pmic */


/*
 @brief power up the LDO and setting the dual voltage UIM GPIO pads
        to the appropriate level. ,
*/
boolean uimPowerOn(uim_instance_enum_type uim_instance)
{
  uint32 npa3vModeId  = 0;

  if (uim_instance>= UIM_MAX_INSTANCES)
  {
    UIMDRV_MSG_ERR_0 ( uim_instance,"Unknown UIM Instance in uimPowerOn");
    return FALSE;
  }

  if(!(uim_hw_if.pmic[uim_instance].m_pmicNpaHandle))
  {
    UIMDRV_MSG_ERR_0 ( uim_instance,"UIM PMIC NPA registration failed");
    return FALSE;
  }

  uim_set_shutdown_control(uim_instance, TRUE);

#ifdef FEATURE_UIM_DRIVE_MAX_PMIC_VOLTAGE
  npa3vModeId = PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B_HIGH;
#else
  npa3vModeId = PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_B;
#endif /* FEATURE_UIM_DRIVE_MAX_PMIC_VOLTAGE */

  if (UIM_1_8V_PROG == uim_hw_if.pmic[uim_instance].m_voltage_class)
  {
    if (uim_hw_if.pmic[uim_instance].m_pmicNpaHandle)
    {
      npa_issue_required_request (uim_hw_if.pmic[uim_instance].m_pmicNpaHandle,
                                  PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C);
      uim_hw_if.pmic[uim_instance].m_pmic_npa_mode = PMIC_NPA_MODE_ID_UIM_ACTIVE_CLASS_C;
    }

    uimdrv_rumi_vsel_on_class_c(uim_instance);

    /* Wait to ensure that the 20ns sequencing requirement from the pad
       specification is met */
    uim_clk_busy_wait(1);
    prgUIMCFG(uim_instance, 0, TRUE, UIM_SET_MODE_18, SET);
  }
  else if(UIM_3V_PROG == uim_hw_if.pmic[uim_instance].m_voltage_class)
  {
    prgUIMCFG(uim_instance, 0, TRUE, UIM_CLR_MODE_18, CLR);
    /* Wait to ensure that the 20ns sequencing requirement from the pad
       specification is met */
    uim_clk_busy_wait(1);
    if (uim_hw_if.pmic[uim_instance].m_pmicNpaHandle)
    {
      npa_issue_required_request (uim_hw_if.pmic[uim_instance].m_pmicNpaHandle,
                                  npa3vModeId);
      uim_hw_if.pmic[uim_instance].m_pmic_npa_mode = npa3vModeId;
    }

    uimdrv_rumi_vsel_on_class_b(uim_instance);
  }
  uim_clk_busy_wait(200);
  return TRUE;
} /* uimPowerOn */


/*
 @brief This function to power down the LDO,
          VCC line will set to low.
*/
boolean uimPowerOff(uim_instance_enum_type uim_instance)
{
  if(!(uim_hw_if.pmic[uim_instance].m_pmicNpaHandle))
  {
    UIMDRV_MSG_ERR_0(uim_instance,"UIM PMIC NPA registration failed");
    return FALSE;
  }
  npa_issue_required_request (uim_hw_if.pmic[uim_instance].m_pmicNpaHandle,
                              PMIC_NPA_MODE_ID_UIM_STANDBY);

  uimdrv_rumi_vsel_off(uim_instance);

  return TRUE;
} /* uimPowerOff */


void npaIssueRequiredRequest(uim_instance_global_type *uim_ptr, boolean req)
{
  if(!(uim_hw_if.pmic[uim_ptr->id].m_pmicNpaHandle))
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"UIM PMIC NPA registration failed");
    return;
  }
  npa_issue_required_request(uim_hw_if.pmic[uim_ptr->id].m_pmicActivityHandle,
                             req);
  return;
} /* npaIssueRequiredRequest */


/*===========================================================================
FUNCTION UIM_PROG_VOLTAGE_CLASS

DESCRIPTION
  Sets the voltage class global variable for UIM1 or UIM2 depending on the
  value of uim_drv_slot.

  UIM1_voltage_class and UIM2_voltage_class are used in the UIM_POWER_ON
  function to determine which voltage level to set for the PMIC LDO and the
  GPIO pads.
===========================================================================*/
void UIM_PROG_VOLTAGE_CLASS
(
  uim_instance_global_type *uim_ptr,
  uint32 voltage_ctl
)
{
  uim_hw_if.pmic[uim_ptr->id].m_voltage_class = voltage_ctl;
  return;
} /* UIM_PROG_VOLTAGE_CLASS */


/*===========================================================================
FUNCTION UIMDRV_POWER_MANAGER_WAIT_FOR_QMI_ACK

DESCRIPTION
  Power manager decides to either wait on the required ACK from QMI_UIM 
  for an indication that was sent or proceed.
===========================================================================*/
void uimdrv_power_manager_wait_for_qmi_ack(uim_instance_global_type          *uim_ptr, 
                                           uimdrv_qmi_acknowledgements_type  required_ack)
{
  if( required_ack == UIMDRV_QMI_IND_ACK_NOT_REQUIRED )
  {
    return;
  }
  uimdrv_wait_for_qmi_acknowledgement(uim_ptr, required_ack);
} /* uimdrv_power_manager_wait_for_qmi_ack */


/*===========================================================================
FUNCTION UIMDRV_QMI_POWER_MANAGEMENT_CALLBACK

DESCRIPTION
  Callback to manage the acknowledgements from QMI that are related to UIM LDO.
  The acknowledgements are for the ldo indications that we sent to QMI_UIM.
===========================================================================*/
static void uimdrv_qmi_power_management_callback(uim_slot_type  uim_slot, 
                                                 uimdrv_qmi_acknowledgements_type received_ack)
{
  uim_instance_global_type *uim_ptr = NULL;
  uim_instance_enum_type uim_instance = (uim_instance_enum_type) uim_slot - 1;
  
  RETURN_IF_INSTANCE_INVALID(uim_instance);
  
  uim_ptr = uim_get_instance_ptr(uim_instance);

  if (received_ack == UIMDRV_QMI_INVALID_IND_ACK) 
  {
    return;
  }

  UIMDRV_MSG_HIGH_1(uim_ptr->id,"UIMDRV received ACK from QMI - 0x%x",received_ack);

  /* ACK received from QMI for an Indication that was sent.
     The supplied callback would match the required ack and received ack and  
     sets the UIM_QMI_ACK_RECEIVED_SIG accordingly. */

  uim_ptr->uim_qmi_interaction.received_ack_from_qmi = received_ack;

  if ( uim_ptr->uim_qmi_interaction.required_ack_from_qmi == uim_ptr->uim_qmi_interaction.received_ack_from_qmi ) 
  {
    rex_set_sigs(uim_ptr->tcb_ptr, UIM_QMI_ACK_RECEIVED_SIG);
  }

} /* uimdrv_qmi_power_management_callback */


/*===========================================================================
FUNCTION UIM_POWER_OFF_SLOT

DESCRIPTION
  Powers off UIM1 or UIM2.  This involves calling the appropriate PMIC
  functions to power down the LDO.
===========================================================================*/
void UIM_POWER_OFF_SLOT(uim_instance_global_type *uim_ptr)
{
  if(!uim_ptr->flag.powering_down_task)
  {
    qmi_uim_supply_voltage_ind((uim_ptr->id + 1),
                                UIMDRV_LDO_AWAITING_DEACTIVATION_IND,
                                uimdrv_qmi_power_management_callback);
    UIMDRV_MSG_HIGH_1(uim_ptr->id,"UIM_POWER_OFF_SLOT notified QMI with Indication - 0x%x",UIMDRV_LDO_AWAITING_DEACTIVATION_IND);
    uimdrv_power_manager_wait_for_qmi_ack( uim_ptr, UIMDRV_LDO_AWAITING_DEACTIVATION_IND_ACK );
  }
  uimdrv_rumi_config_gpios(uim_ptr->id);
  uimPowerOff(uim_ptr->id);
  return;
} /* UIM_POWER_OFF_SLOT */


/*===========================================================================
FUNCTION UIM_POWER_ON_SLOT

DESCRIPTION
  Powers on UIM1 or UIM2 depending on the value of the global variable
  uim_drv_slot.  This involves calling the appropriate PMIC
  functions to power up the LDO and setting the dual voltage UIM GPIO pads
  to the appropriate level.

  From 80-N2129-4 GPIO and Custom I/Os Datasheet:
  A control signal �mode18� is used to set the pad operation to either a
  1.8V or 3V signaling environment. This signal must be set to 0 at least
  20ns before VDDPX rises from 1.8V to 3V and at least 20ns after 3V drops to 1.8V.
===========================================================================*/
void UIM_POWER_ON_SLOT(uim_instance_global_type *uim_ptr)
{
  uimdrv_rumi_config_gpios(uim_ptr->id);
  uimPowerOn(uim_ptr->id);
  qmi_uim_supply_voltage_ind ( (uim_ptr->id + 1), UIMDRV_LDO_ACTIVATED_IND, NULL);
  UIMDRV_MSG_HIGH_1(uim_ptr->id,"UIM_POWER_ON_SLOT notified QMI with Indication - 0x%x",UIMDRV_QMI_IND_ACK_NOT_REQUIRED);
  uimdrv_power_manager_wait_for_qmi_ack( uim_ptr, UIMDRV_QMI_IND_ACK_NOT_REQUIRED );
  return;
} /* UIM_POWER_ON_SLOT */

/*===========================================================================
FUNCTION UIM_SET_SHUTDOWN_CONTROL

DESCRIPTION
  Control UICC shutdown upon card removal and UICC shutdown upon battery
  removal.
===========================================================================*/
void uim_set_shutdown_control(uim_instance_enum_type uim_instance, boolean enable)
{
  /* UICC Shutdown upon card removal */
  /*
     PMIC interface expects UIM to call pm_uicc_cntrl_enable with parameters
      -- 1 and TRUE for enabling UICC shutdown upon UIM1 card removal,
      -- 2 and TRUE for enabling UICC shutdown upon UIM2 card removal,
      -- 3 and TRUE for enabling UICC shutdown upon UIM3 card removal.
  */
  UIMDRV_MSG_HIGH_3(uim_instance, "UICC set shutdown upon card removal for slot is %d. m_uiccShutCardRemoval = %d, m_hs_enable = %d",
                 enable,
                 uim_hw_if.pmic[uim_instance].m_uiccShutCardRemoval,
                 uim_hw_if.intctrl[uim_instance].m_hs_enable);
  if (TRUE == uim_hw_if.pmic[uim_instance].m_uiccShutCardRemoval &&
      TRUE == uim_hw_if.intctrl[uim_instance].m_hs_enable )
  {
    pm_uicc_cntrl_enable(uim_hw_if.pmic[uim_instance].m_Vcc + 1, enable);
  }

  /* UICC Shutdown upon battery removal */
  /*
     PMIC interface expects UIM to call pm_uicc_cntrl_enable with parameters
      -- 0 and TRUE for enabling UICC shutdown upon battery removal.
     Battery removal configuration is for all the UIM slots supporting
     UIM controller.
  */
  UIMDRV_MSG_HIGH_2(uim_instance, "UICC set shutdown upon batt removal for slot is %d. m_uiccShutBattRemoval = %d",
		        enable,uim_hw_if.pmic[uim_instance].m_uiccShutBattRemoval);
  if (TRUE == uim_hw_if.pmic[uim_instance].m_uiccShutBattRemoval)
  {
    pm_uicc_cntrl_enable(0, enable);
  }
  return;
} /* UIM_SHUTDOWN_CONTROL_ENABLE */

/*===========================================================================
FUNCTION UIM_GET_PMIC_NPA_MODE

DESCRIPTION
  RETURN PMIC NPA MODE VALUE.
===========================================================================*/
uint32 uim_get_pmic_npa_mode(uim_instance_enum_type uim_instance)
{
  return uim_hw_if.pmic[uim_instance].m_pmic_npa_mode;
}/* uim_get_pmic_npa_mode */
