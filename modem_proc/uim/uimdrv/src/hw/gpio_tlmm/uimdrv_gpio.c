/*============================================================================
  FILE:         uimdrv_gpio.c

  OVERVIEW:     Contains functions used by uim driver to control and initialize
                gpio/tlmm interface

  DEPENDENCIES: N/A

                Copyright (c) 2012 - 2014 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.  Please
use ISO format for dates.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimdrv/src/hw/gpio_tlmm/uimdrv_gpio.c#2 $
$DateTime: 2015/03/06 03:55:34 $
$Author: ksekuru $

 when       who        what, where, why
------      ----       -----------------------------------------------------------
03/02/15    ks         SIM tray by tieing the card detect gpios
01/14/15    ks         8929 bringup code
05/19/14    ks         8936 bring up changes
04/21/14    rm/akv     Fix for glitch seen on the IO line while powering down
01/29/14    sam        Usage of newer style diag macros
12/27/13    ks         8916 bring up changes
11/27/13    akv        Initialization of hw enumeration based on dal chip family
10/22/13    ll         Check UIM GPIOs as inputs upon timeout w/recovery disabled
10/08/13    nmb        HW header file cleanup
10/08/13    nmb        Changed default card detect GPIO to pull up
09/11/13    na         8x62 bring up changes
08/06/13    rm/ak      Bolt RUMI code
07/12/13    nmb        Global Reorganization
07/12/13    akv        HW Enumeration - modifying target specific hardware values
                       and organizing them in value-key pairs
05/23/13    ak         Apply NV values for PULL and DRIVE STRENGTH for UIM1 dedicated
                       UIM controller pads
05/23/13    ak         Reset CLK, DATA, and RESET PULL to NO-PULL when setting to
                       UIM control
05/22/13    akv        Warm reset fixes for Triton
02/21/13    ak         Use UIM instance number for GPIO configuration logic
02/21/13    ak         UIM1 support on 9x25 v2
02/12/13    js         General F3 macro cleanup
12/05/12    js         UIM Parallel Processing changes
11/21/12    rm         Fix to set the GPIO state to LOW after configuring it them
                       to TLMM ownership
11/09/12    akv        BATT ALARM configuration for Dime
10/26/12    akv/ssr    HW Enumeration changes
============================================================================*/
#include "uimdrv_gpio.h"
#include "uimglobals.h"

#define UIM_INVALID_GPIO_NUM                   0xFFFF /* Indicates that there is no GPIO number for the item */

#define UIM_RESET_DEFAULT_GPIO  \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ResetGpioNum, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_ResetGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_ResetGpioDrvStrength)

#define UIM_DATA_DEFAULT_GPIO  \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_DataGpioNum, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_DataGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_DataGpioDrvStrength)

#define UIM_CLK_DEFAULT_GPIO \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ClkGpioNum, 0, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_ClkGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_ClkGpioDrvStrength)

#define UIM_RESET_UIMCNTRL_GPIO  \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ResetGpioNum, uim_hw_if.gpio[uim_instance].m_ResetGpioFuncSel, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_ResetGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_ResetGpioDrvStrength)

#define UIM_DATA_UIMCNTRL_GPIO  \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_DataGpioNum, uim_hw_if.gpio[uim_instance].m_DataGpioFuncSel, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_DataGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_DataGpioDrvStrength)

#define UIM_CLK_UIMCNTRL_GPIO \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ClkGpioNum, uim_hw_if.gpio[uim_instance].m_ClkGpioFuncSel, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_ClkGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_ClkGpioDrvStrength)

#define UIM_HOTSWAP_GPIO \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_CardDetectGpioNum, uim_hw_if.gpio[uim_instance].m_CardDetectGpioFuncSel, (uint32)DAL_GPIO_INPUT, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioDrvStrength)

#define UIM_BATT_ALARM_UIMCNTRL_GPIO  \
            DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_BattAlarmGpioNum, uim_hw_if.gpio[uim_instance].m_BattAlarmGpioFuncSel, (uint32)DAL_GPIO_OUTPUT, (uint32)uim_hw_if.gpio[uim_instance].m_BattAlarmGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_BattAlarmGpioDrvStrength)

#define UIM_RESET_GPIO_PU  \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ResetGpioNum, 0, (uint32)DAL_GPIO_INPUT, (uint32)DAL_GPIO_PULL_UP, (uint32)DAL_GPIO_2MA)

#define UIM_CLK_GPIO_PU    \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_ClkGpioNum, 0, (uint32)DAL_GPIO_INPUT, (uint32)DAL_GPIO_PULL_UP, (uint32)DAL_GPIO_2MA)

#define UIM_DATA_GPIO_PU   \
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_DataGpioNum, 0, (uint32)DAL_GPIO_INPUT, (uint32)DAL_GPIO_PULL_UP, (uint32)DAL_GPIO_2MA)

#define UIM_HOTSWAP_GPIO_PU\
        DAL_GPIO_CFG(uim_hw_if.gpio[uim_instance].m_CardDetectGpioNum, 0, (uint32)DAL_GPIO_INPUT, (uint32)DAL_GPIO_PULL_UP, (uint32)DAL_GPIO_2MA)

#define UIM_HOTSWAP_INSTANCE1_GPIO \
        DAL_GPIO_CFG(uim_gpio_table[nChipindex].uimGpioSettings[UIM_INSTANCE_1][CARD_DETECT].uimGpioNum, \
                      0, (uint32)DAL_GPIO_INPUT, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioDrvStrength)

#define UIM_HOTSWAP_INSTANCE2_GPIO \
        DAL_GPIO_CFG(uim_gpio_table[nChipindex].uimGpioSettings[UIM_INSTANCE_2][CARD_DETECT].uimGpioNum, \
                      0, (uint32)DAL_GPIO_INPUT, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioPullSetting, (uint32)uim_hw_if.gpio[uim_instance].m_CardDetectGpioDrvStrength)


/* Table holding the default GPIO initialization for different UIM instances. */

const uim_gpio_table_type     uim_gpio_table[] = {

  { DALCHIPINFO_FAMILY_MSM8974,
                 {
       /* UIM 1 */ {                          /* GPIO_NUM, FUNC_SEL, DRIVE_STRENGTH, PULL_SETTING */
                      /* RESET_GPIO */        { 99,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 98,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 97,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 100, GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/  {
                           /* BATT_ALARM_GPIO */   { 101, GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                        }
                 }
  },  /* ARAGORN_8974 */


  { DALCHIPINFO_FAMILY_MSM8974_PRO,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 99,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 98,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 97,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 100, GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/  {
                           /* BATT_ALARM_GPIO */   { 101, GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                        }
                 }
  },  /* ARAGORN_8974_PRO */


  {  DALCHIPINFO_FAMILY_MDM9x25,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 2,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 3,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 0,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 1,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 69, GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* ELAN_9X25 */


  { DALCHIPINFO_FAMILY_MSM8x10,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 38,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 37,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 36,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 39,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 34,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 33,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 32,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 35,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 96,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 95,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 94,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 97,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 40,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* DINO_8X10 */


  {  DALCHIPINFO_FAMILY_MSM8x26,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 59,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 58,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 57,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 60,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 55,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 54,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 53,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 56,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_2, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 5,   GPIO_FUNC_SEL_4, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 4,   GPIO_FUNC_SEL_4, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_2, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 61,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* FRODO_8X26 */


  {  DALCHIPINFO_FAMILY_MSM8926,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 59,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 58,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 57,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 60,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 55,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 54,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 53,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 56,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_2, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 5,   GPIO_FUNC_SEL_4, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 4,   GPIO_FUNC_SEL_4, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_2, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 61,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* FRODO_8X26 */


  {  DALCHIPINFO_FAMILY_MDM9x35,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 78,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 79,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 76,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 77,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 2,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 3,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 0,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 1,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/  {
                           /* BATT_ALARM_GPIO */   { 69, GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                        }
                 }
  },  /* TORINO_9X35 */

  { DALCHIPINFO_FAMILY_MSM8x62,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 99,   GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 98,   GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 97,   GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 100,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 33,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 32,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 31,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 34,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/  {
                           /* BATT_ALARM_GPIO */   { 101, GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                        }
                 }
  },  /* THORIN_8X62 */

  {  DALCHIPINFO_FAMILY_MSM8916,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 59,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 58,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 57,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 60,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 55,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 54,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 53,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 56,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 61,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* BAGHEERA_8916 */

  {  DALCHIPINFO_FAMILY_MSM8936,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 59,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 58,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 57,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 60,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 55,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 54,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 53,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 56,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 61,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* SHERE_8936 */

 {  DALCHIPINFO_FAMILY_MSM8929,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { 59,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 58,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 57,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 60,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { 55,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 54,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 53,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 56,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { 51,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { 50,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { 49,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { 52,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/{
                         /* BATT_ALARM_GPIO */   { 61,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                         /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                      }
                 }
  },  /* KICHI_8929 */

  { DALCHIPINFO_FAMILY_UNKNOWN,
                 {
       /* UIM 1 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 2 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 3 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /* UIM 4 */ {
                      /* RESET_GPIO */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /*  CLK_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* DATA_GPIO  */        { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                      /* CARD_DETECT_GPIO */  { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, PULL_UP }
                   },
       /*COMMON_GPIO*/  {
                           /* BATT_ALARM_GPIO */   { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL },
                           /*  RFU  */             { UIM_INVALID_GPIO_NUM,  GPIO_FUNC_SEL_1, DS_2MA, NO_PULL }
                        }
                 }
  }  /* DALCHIPINFO_FAMILY_UNKNOWN */

}; /* uim_gpio_table[MAX_UIM_CHIPSET_INDEX] */


/* Function to initialize GPIO */
void initialize_gpio(uim_instance_enum_type uim_instance)
{
  DALResult dal_attach = DAL_SUCCESS;

  uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr = NULL;

  uim_hw_if.gpio[uim_instance].m_BattAlarmGpioNum                    = m_HWConfig.uimBattAlarmGpioNum;
  uim_hw_if.gpio[uim_instance].m_BattAlarmGpioFuncSel       = m_HWConfig.uimBattAlarmGpioFuncSel;
  uim_hw_if.gpio[uim_instance].m_BattAlarmGpioDrvStrength   = m_HWConfig.uimBattAlarmGpioDrvStrength;
  uim_hw_if.gpio[uim_instance].m_BattAlarmGpioPullSetting   = m_HWConfig.uimBattAlarmGpioPullSetting;
  uim_hw_if.gpio[uim_instance].m_ResetGpioNum         = m_HWConfig.uim_slot_config[uim_instance].uimResetGpioNum;
  uim_hw_if.gpio[uim_instance].m_ResetGpioFuncSel     = m_HWConfig.uim_slot_config[uim_instance].uimResetGpioFuncSel;
  uim_hw_if.gpio[uim_instance].m_ResetGpioDrvStrength = m_HWConfig.uim_slot_config[uim_instance].uimResetGpioDrvStrength;
  uim_hw_if.gpio[uim_instance].m_ResetGpioPullSetting = m_HWConfig.uim_slot_config[uim_instance].uimResetGpioPullSetting;
  uim_hw_if.gpio[uim_instance].m_DataGpioNum          = m_HWConfig.uim_slot_config[uim_instance].uimDataGpioNum;
  uim_hw_if.gpio[uim_instance].m_DataGpioFuncSel      = m_HWConfig.uim_slot_config[uim_instance].uimDataGpioFuncSel;
  uim_hw_if.gpio[uim_instance].m_DataGpioDrvStrength  = m_HWConfig.uim_slot_config[uim_instance].uimDataGpioDrvStrength;
  uim_hw_if.gpio[uim_instance].m_DataGpioPullSetting  = m_HWConfig.uim_slot_config[uim_instance].uimDataGpioPullSetting;
  uim_hw_if.gpio[uim_instance].m_ClkGpioNum           = m_HWConfig.uim_slot_config[uim_instance].uimClkGpioNum;
  uim_hw_if.gpio[uim_instance].m_ClkGpioFuncSel       = m_HWConfig.uim_slot_config[uim_instance].uimClkGpioFuncSel;
  uim_hw_if.gpio[uim_instance].m_ClkGpioDrvStrength   = m_HWConfig.uim_slot_config[uim_instance].uimClkGpioDrvStrength;
  uim_hw_if.gpio[uim_instance].m_ClkGpioPullSetting   = m_HWConfig.uim_slot_config[uim_instance].uimClkGpioPullSetting;
  uim_hw_if.gpio[uim_instance].m_CardDetectGpioNum           = m_HWConfig.uim_slot_config[uim_instance].uimCardDetectGpioNum;
  uim_hw_if.gpio[uim_instance].m_CardDetectGpioFuncSel       = m_HWConfig.uim_slot_config[uim_instance].uimCardDetectGpioFuncSel;
  uim_hw_if.gpio[uim_instance].m_CardDetectGpioDrvStrength   = m_HWConfig.uim_slot_config[uim_instance].uimCardDetectGpioDrvStrength;
  uim_hw_if.gpio[uim_instance].m_CardDetectGpioPullSetting   = m_HWConfig.uim_slot_config[uim_instance].uimCardDetectGpioPullSetting;

  dal_attach = DAL_DeviceAttach(DALDEVICEID_TLMM, &(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr) );
  if((uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)||(dal_attach != DAL_SUCCESS))
  {
    UIMDRV_MSG_ERR_0(uim_instance,"Failed to register with TLMM DAL");
  }
  (void)DalDevice_Open(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr,DAL_OPEN_SHARED);
} /* initialize_gpio */


/*
  @ brief Configures the DATA and CLOCK lines
*/
boolean enableUartInterface(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the data/clock GPIO number is set to INVALID, then the lines are a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimDataGpioNum ||
      UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimClkGpioNum)
  {
    return FALSE;
  }


  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_DATA_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE);
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_CLK_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE);

  return TRUE;
} /* enableUartInterface */


/*
  @brief  Configures RESET GPIO.
*/
boolean setResetAsOutput(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the reset GPIO number is set to INVALID, then the reset line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimResetGpioNum)
  {
    return FALSE;
  }

  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_RESET_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE );
  return TRUE;
} /* setResetAsOutput */


/*
  @brief  Brings the RESET line LOW.
*/
boolean assertReset(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the reset GPIO number is set to INVALID, then the reset line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimResetGpioNum)
  {
    return FALSE;
  }

  (void)DalTlmm_GpioOut(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_RESET_DEFAULT_GPIO, DAL_GPIO_LOW_VALUE);
  return TRUE;
}/* assertReset */


/* Configure Reset, data, clock and battery alarm for UIM Controller */
boolean uimConfigureResetUimController(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the reset GPIO number is set to INVALID, then the reset line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimResetGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set PULL value from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_PULL_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_ResetGpioPullSetting << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_PULL_SHFT);

      /* Set the drive strength from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_HDRV_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_ResetGpioDrvStrength << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_HDRV_SHFT);

      /* Set bit 3 - TLMM_UIM1_RST_EN in the TLMM_SPARE register to route Reset line to UIM controller */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_RST_EN_BMSK, 0x1 << HWIO_TLMM_SPARE_TLMM_UIM1_RST_EN_SHFT);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureResetUimController: INVALID RESET GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_RESET_UIMCNTRL_GPIO, DAL_TLMM_GPIO_ENABLE);
  }
  return TRUE;
} /* uimConfigureResetUimController */


/*
  @brief Brings the RESET line HIGH.
*/
boolean deassertReset(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }
  /* Switch the user of the RESET line to UIM controller */
  uimConfigureResetUimController(uim_instance);
  return TRUE;
}/* deassertReset */

/*
  @brief Configures hotswap gpios
*/
boolean configHotswapGpios(uim_instance_enum_type uim_instance)
{

  uint8       nChipindex	   = 0;

  uim_instance_global_type *uim_ptr;
  uim_ptr = uim_get_instance_ptr(uim_instance); 

  if((uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL) || (uim_ptr == NULL))
  {
    return FALSE;
  }

  /* If the card detect GPIO number is set to INVALID, then the card detect line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimCardDetectGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set PULL value from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_PRESENT_PULL_BMSK,
                                     uim_hw_if.gpio[uim_instance].m_CardDetectGpioPullSetting << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_PRESENT_PULL_SHFT);

      /* Set the drive strength from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_PRESENT_HDRV_BMSK,
                                     uim_hw_if.gpio[uim_instance].m_CardDetectGpioDrvStrength << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_PRESENT_HDRV_SHFT);
#endif /* FEATURE_BOLT_MODEM */
      return TRUE;
    }
    else
    {
      UIM_MSG_ERR_0("configHotswapGpios: INVALID HOTSWAP GPIO CONFIG");
      return FALSE;
    }
  }

  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_SIMTRAY_WITH_GPIOS_TIED,
                                   uim_ptr) == TRUE)
  {
    nChipindex = uim_get_chip_index_from_table(uimdrv_hw_nv_config_info.dalChipFamily, UIM_GPIO_TABLE);
    if(uim_hw_slot_info.slot_status[UIM_INSTANCE_1].slot_enabled == FALSE)
    {
      (void)DalTlmm_ConfigGpio(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_HOTSWAP_INSTANCE1_GPIO, DAL_TLMM_GPIO_ENABLE );
    }
    else if((uim_hw_slot_info.slot_status[UIM_INSTANCE_2].slot_enabled == FALSE))
    {
      (void)DalTlmm_ConfigGpio(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_HOTSWAP_INSTANCE2_GPIO, DAL_TLMM_GPIO_ENABLE );
    }
  }

  (void)DalTlmm_ConfigGpio(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_HOTSWAP_GPIO, DAL_TLMM_GPIO_ENABLE );
  return TRUE;
} /* configHotswapGpios */


/* Configure Reset for GPIO/TLMM */
boolean uimConfigureResetDefaultGpio(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the reset GPIO number is set to INVALID, then the reset line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimResetGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set the dedicated UIM1 reset line PULL to PULL-Down */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_PULL_BMSK, 0x1 << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_RESET_PULL_SHFT);

      /* Set bit 3 to 0 - TLMM_UIM1_RST_EN in the TLMM_SPARE register to disable the UIM controller for the Reset line */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_RST_EN_BMSK, HWIO_TLMM_SPARE_TLMM_UIM1_RST_EN_DISABLE_FVAL);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureResetDefaultGpio: INVALID RESET GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_RESET_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE );
    (void)DalTlmm_GpioOut   ( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_RESET_DEFAULT_GPIO, DAL_GPIO_LOW_VALUE);
  }
  return TRUE;
} /* uimConfigureResetDefaultGpio */


/* Configure data for GPIO/TLMM */
boolean uimConfigureDataDefaultGpio(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the data GPIO number is set to INVALID, then the data line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimDataGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set the dedicated UIM1 data line PULL to PULL-Down */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_PULL_BMSK, 0x1 << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_PULL_SHFT);

      /* Set bit 1 to 0 - TLMM_UIM1_DATA_EN in the TLMM_SPARE register to disable UIM controller on the DATA line */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_DATA_EN_BMSK, HWIO_TLMM_SPARE_TLMM_UIM1_DATA_EN_DISABLE_FVAL);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureDataDefaultGpio: INVALID DATA GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_GpioOut   ( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_DATA_DEFAULT_GPIO, DAL_GPIO_LOW_VALUE);
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_DATA_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE);
  }
  return TRUE;
} /* uimConfigureDataDefaultGpio */


/* configure clock for GPIO/TLMM */
boolean uimConfigureClkDefaultGpio(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the clock GPIO number is set to INVALID, then the clock line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimClkGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set the dedicated UIM1 clock line PULL to PULL-Down */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_PULL_BMSK, 0x1 << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_PULL_SHFT);

      /* Set bit 2 to 0 - TLMM_UIM1_CLK_EN in the TLMM_SPARE register to disable UIM controller on the CLOCK line */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_CLK_EN_BMSK, HWIO_TLMM_SPARE_TLMM_UIM1_CLK_EN_DISABLE_FVAL);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureClkDefaultGpio: INVALID CLOCK GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_CLK_DEFAULT_GPIO, DAL_TLMM_GPIO_ENABLE);
    (void)DalTlmm_GpioOut   ( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_CLK_DEFAULT_GPIO, DAL_GPIO_LOW_VALUE);
  }
  return TRUE;
} /* uimConfigureClkDefaultGpio */


/* configure data for UIM controller */
boolean uimConfigureDataUimController(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the data GPIO number is set to INVALID, then the data line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimDataGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set PULL value from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_PULL_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_DataGpioPullSetting << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_PULL_SHFT);

      /* Set the drive strength from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_HDRV_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_DataGpioDrvStrength << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_DATA_HDRV_SHFT);

      /* Set bit 1 - TLMM_UIM1_DATA_EN in the TLMM_SPARE register to route Data line to UIM controller */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_DATA_EN_BMSK, 0x1 << HWIO_TLMM_SPARE_TLMM_UIM1_DATA_EN_SHFT);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureDataUimController: INVALID DATA GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_DATA_UIMCNTRL_GPIO, DAL_TLMM_GPIO_ENABLE);
  }
  return TRUE;
} /* uimConfigureDataUimController */


/* configure clock for UIM controller */
boolean uimConfigureClkUimController(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }

  /* If the clock GPIO number is set to INVALID, then the clock line is a dedicated UIM controller input */
  if (UIM_INVALID_GPIO_NUM == m_HWConfig.uim_slot_config[uim_instance].uimClkGpioNum)
  {
    if (0 == uim_instance)
    {
#ifndef FEATURE_BOLT_MODEM
      /* Set PULL value from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_PULL_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_ClkGpioPullSetting << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_PULL_SHFT);

      /* Set the drive strength from NV item */
      HWIO_TLMM_IE_CTRL_DISABLE_OUTM(HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_HDRV_BMSK,
                                   uim_hw_if.gpio[uim_instance].m_ClkGpioDrvStrength << HWIO_TLMM_IE_CTRL_DISABLE_SMT_UIM1_CLK_HDRV_SHFT);

      /* Set bit 2 - TLMM_UIM1_CLK_EN in the TLMM_SPARE register to route Clock line to UIM controller */
      HWIO_TLMM_SPARE_OUTM(HWIO_TLMM_SPARE_TLMM_UIM1_CLK_EN_BMSK, 0x1 << HWIO_TLMM_SPARE_TLMM_UIM1_CLK_EN_SHFT);
#endif /* FEATURE_BOLT_MODEM */
    }
    else
    {
      UIM_MSG_ERR_0("uimConfigureClkUimController: INVALID CLK GPIO CONFIG");
      return FALSE;
    }
  }
  else
  {
    (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_CLK_UIMCNTRL_GPIO, DAL_TLMM_GPIO_ENABLE);
  }
  return TRUE;
} /* uimConfigureClkUimController */


/* configure bat alarm GPIO */
boolean uimConfigureBattAlarmUimController(uim_instance_enum_type uim_instance)
{
  if(uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr == NULL)
  {
    return FALSE;
  }
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (DALGpioSignalType)UIM_BATT_ALARM_UIMCNTRL_GPIO, DAL_TLMM_GPIO_ENABLE);
  return TRUE;
} /* uimConfigureBattAlarmUimController */

/* Configure all UIM lines as pull-up input GPIOs to log their values for debugging */
void UIM_CONFIGURE_ALL_UIM_AS_INPUT_PULLUP_GPIOS(uim_instance_enum_type uim_instance)
{
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr,(DALGpioSignalType)UIM_RESET_GPIO_PU, DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr,(DALGpioSignalType)UIM_CLK_GPIO_PU, DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr,(DALGpioSignalType)UIM_DATA_GPIO_PU, DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr,(DALGpioSignalType)UIM_HOTSWAP_GPIO_PU, DAL_TLMM_GPIO_ENABLE );
}


void UIM_RESTORE_ALL_UIM_GPIOS(uim_instance_enum_type uim_instance)
{
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (uint32)UIM_RESET_DEFAULT_GPIO, (uint32)DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (uint32)UIM_DATA_DEFAULT_GPIO, (uint32)DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (uint32)UIM_CLK_DEFAULT_GPIO, (uint32)DAL_TLMM_GPIO_ENABLE );
  (void)DalTlmm_ConfigGpio( uim_hw_if.gpio[uim_instance].m_TlmmHandle_ptr, (uint32)UIM_HOTSWAP_GPIO, (uint32)DAL_TLMM_GPIO_ENABLE );
}

/*===========================================================================
FUNCTION UIM_SET_RESET_AS_OUTPUT_SLOT

DESCRIPTION
  UIM_SET_RESET_AS_OUTPUT configures the GPIO connected to the RESET line.
  UIM1 RESET: GPIO1, FUNC_SEL = 0
  UIM2 RESET: GPIO5, FUNC_SEL = 0
===========================================================================*/
void UIM_SET_RESET_AS_OUTPUT_SLOT(uim_instance_global_type *uim_ptr)
{
  setResetAsOutput(uim_ptr->id);
  return;
} /* UIM_SET_RESET_AS_OUTPUT_SLOT */


/*===========================================================================
FUNCTION ENABLE_UART_INTERFACE_SLOT

DESCRIPTION
  ENABLE_UART_INTERFACE configures the GPIOs connected to the DATA and
  CLOCK lines to utilize the UART (func_sel=1).

  UIM1 DATA: GPIO3, FUNC_SEL = 1
  UIM1 CLOCK: GPIO2, FUNC_SEL = 1
  UIM2 DATA: GPIO7, FUNC_SEL = 1
  UIM2 CLOCK: GPIO6, FUNC_SEL = 1
===========================================================================*/
void ENABLE_UART_INTERFACE_SLOT(uim_instance_global_type *uim_ptr)
{
  enableUartInterface(uim_ptr->id);
  return;
} /* ENABLE_UART_INTERFACE_SLOT */


/*===========================================================================
FUNCTION UIM_CONFIGURE_RESET_DEFAULT_GPIO

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_RESET_DEFAULT_GPIO(uim_instance_global_type *uim_ptr)
{
  uimConfigureResetDefaultGpio(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_RESET_DEFAULT_GPIO */


/*===========================================================================
FUNCTION UIM_CONFIGURE_DATA_DEFAULT_GPIO

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_DATA_DEFAULT_GPIO(uim_instance_global_type *uim_ptr)
{
  uimConfigureDataDefaultGpio(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_DATA_DEFAULT_GPIO */


/*===========================================================================
FUNCTION UIM_CONFIGURE_CLK_DEFAULT_GPIO

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_CLK_DEFAULT_GPIO(uim_instance_global_type *uim_ptr)
{
  uimConfigureClkDefaultGpio(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_CLK_DEFAULT_GPIO */


/*===========================================================================
FUNCTION UIM_CONFIGURE_DATA_FOR_UIM_CONTROLLER

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_DATA_FOR_UIM_CONTROLLER(uim_instance_global_type *uim_ptr)
{
  uimConfigureDataUimController(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_DATA_FOR_UIM_CONTROLLER */


/*===========================================================================
FUNCTION UIM_CONFIGURE_CLK_FOR_UIM_CONTROLLER

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_CLK_FOR_UIM_CONTROLLER(uim_instance_global_type *uim_ptr)
{
  uimConfigureClkUimController(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_CLK_FOR_UIM_CONTROLLER */


/*===========================================================================
FUNCTION UIM_CONFIGURE_BATT_ALARM_FOR_UICC_SHUTDOWN

DESCRIPTION

===========================================================================*/
void UIM_CONFIGURE_BATT_ALARM_FOR_UICC_SHUTDOWN(uim_instance_global_type *uim_ptr)
{
  uimConfigureBattAlarmUimController(uim_ptr->id);
  return;
} /* UIM_CONFIGURE_BATT_ALARM_FOR_UICC_SHUTDOWN */


/*===========================================================================
FUNCTION UIM_ASSERT_RESET_SLOT

DESCRIPTION
  Brings the RESET line LOW.
===========================================================================*/
void UIM_ASSERT_RESET_SLOT(uim_instance_global_type *uim_ptr)
{
  assertReset(uim_ptr->id);
  return;
} /* UIM_ASSERT_RESET_SLOT */


/*===========================================================================
FUNCTION UIM_DEASSERT_RESET_SLOT

DESCRIPTION
  Brings the RESET line LOW.
===========================================================================*/
void UIM_DEASSERT_RESET_SLOT(uim_instance_global_type *uim_ptr)
{
  deassertReset(uim_ptr->id);
  return;
} /* UIM_DEASSERT_RESET_SLOT */


/*===========================================================================

FUNCTION UIM_CONFIG_HOTSWAP_GPIOS

DESCRIPTION
  This function configures the GPIO that is connected to the card detection
  switch.

===========================================================================*/
void uim_config_hotswap_gpios(uim_instance_global_type *uim_ptr)
{
if(uim_hw_if.intctrl[uim_ptr->id].m_hs_enable == FALSE)
  {
    return;
  }
  configHotswapGpios(uim_ptr->id);
  return;
} /* uim_config_hotswap_gpios */


/*===========================================================================

FUNCTION UIM_POPULATE_HW_GPIO_INFO

DESCRIPTION
  Retrieve uim gpio values and configurations

===========================================================================*/
boolean uim_populate_hw_gpio_info
(
  uim_instance_global_type *uim_ptr,
  uim_gpio_info_struct     *uim_gpio_info_ptr
)
{
  /* If the data/clock GPIO number is set to INVALID, then the lines are a dedicated UIM controller input */
  if ( UIM_INVALID_GPIO_NUM == uim_hw_if.gpio[uim_ptr->id].m_ResetGpioNum ||
       UIM_INVALID_GPIO_NUM == uim_hw_if.gpio[uim_ptr->id].m_ClkGpioNum   ||
       UIM_INVALID_GPIO_NUM == uim_hw_if.gpio[uim_ptr->id].m_DataGpioNum )
  {
    return FALSE;
  }
  //get GPIO values
  uim_gpio_info_ptr->gpio_input.reset  = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_ResetGpioNum) & 0x1;
  uim_gpio_info_ptr->gpio_input.clk    = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_ClkGpioNum) & 0x1;
  uim_gpio_info_ptr->gpio_input.data   = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_DataGpioNum) & 0x1;
  uim_gpio_info_ptr->gpio_input.detect = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_CardDetectGpioNum) & 0x1;

  //get GPIO configs and protocal setting
  uim_gpio_info_ptr->reset_config = HWIO_INI(TLMM_GPIO_CFGn, uim_hw_if.gpio[uim_ptr->id].m_ResetGpioNum);
  uim_gpio_info_ptr->clk_config   = HWIO_INI(TLMM_GPIO_CFGn, uim_hw_if.gpio[uim_ptr->id].m_ClkGpioNum);
  uim_gpio_info_ptr->data_config  = HWIO_INI(TLMM_GPIO_CFGn, uim_hw_if.gpio[uim_ptr->id].m_DataGpioNum);

  if(uim_ptr->flag.runtime_disable_recovery)
  {
    UIM_CONFIGURE_ALL_UIM_AS_INPUT_PULLUP_GPIOS(uim_ptr->id);
    uim_clk_busy_wait (100);

    //get resetted GPIO values
    uim_gpio_info_ptr->gpio_input_pull_up.reset = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_ResetGpioNum) & 0x1;
    uim_gpio_info_ptr->gpio_input_pull_up.clk   = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_ClkGpioNum) & 0x1;
    uim_gpio_info_ptr->gpio_input_pull_up.data  = HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_DataGpioNum) & 0x1;
    uim_gpio_info_ptr->gpio_input_pull_up.detect= HWIO_INI(TLMM_GPIO_IN_OUTn, uim_hw_if.gpio[uim_ptr->id].m_CardDetectGpioNum) & 0x1;

    UIM_RESTORE_ALL_UIM_GPIOS(uim_ptr->id);
  }
  return TRUE;
} /* uim_populate_hw_gpio_info */
