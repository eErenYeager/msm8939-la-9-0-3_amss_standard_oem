#ifndef SIMLOCK_COMMON_H
#define SIMLOCK_COMMON_H
/*===========================================================================


            S I M L O C K   C O M M O N   D E F I N I T I O N S

                      A N D
 
                F U N C T I O N S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2014 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/api/simlock_common.h#1 $$ $DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
04/15/14   vv      Added support for quad SIM
04/14/14   vv      Added support for identifying conflicting codes
03/26/14   tl      Secondary revisions
01/07/14   vv      Initial revision
===========================================================================*/


/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */
#endif /* SIMLOCK_COMMON_H */
