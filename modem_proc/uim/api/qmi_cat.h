#ifndef QMI_CAT_H
#define QMI_CAT_H
/*===========================================================================

                         Q M I _ C A T . H

DESCRIPTION

 The Data Services QMI Card Application Toolkit Service header file.

EXTERNALIZED FUNCTIONS

   qmi_cat_init()
     Register the CAT service with QMUX for all applicable QMI links

Copyright (c) 2009-2010,2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/api/qmi_cat.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
01/07/15    hh     Added support to retrieve supported proactive commands
01/09/14    kb     Add de-init function when gstk task is stopped
04/11/13    tkl    Add NV suport for SETUP Call display alpha value
04/02/13    av     Move UIM tasks to group4 of RCINIT
02/19/13    av     Merge mmgsdi,gstk,qmiuim/cat conf files to mmgsdi.conf
02/14/13    tl     Move QMI CAT to GSTK context from QMI modem context
02/23/12    nmb    QMI QMUX Transition
08/05/10    tkl    QMI UIM & QMI CAT split from data package
03/25/10    dd     Adding decoded tlvs
11/01/09    mj     Initial check-in of QMI_CAT
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "gstk_exp.h"

/*===========================================================================

                            CONSTANT DEFINITIONS

===========================================================================*/
#define CATI_CONFIG_FILE             "/nv/item_files/modem/qmi/cat/qmi_cat_mode"
#define CATI_GOBI_VS_CONFIG_FILE     "/nv/item_files/modem/qmi/cat/qmi_cat_vs_id"
#define CATI_CONFIG_TP               "/nv/item_files/modem/qmi/cat/qmi_cat_custom_tp"
#define CATI_DEFAULT_LANG            "/nv/item_files/modem/qmi/cat/qmi_cat_default_lang"
#define CATI_DISPLAY_ALPHA_FILE      "/nv/item_files/modem/qmi/cat/qmi_cat_display_alpha"
#define CATI_BLOCK_SMS_PP_ENV        "/nv/item_files/modem/qmi/cat/qmi_cat_block_sms_pp_env"

/*===========================================================================
  FUNCTION QMI_CAT_INIT()

  DESCRIPTION
    Internal init and registration with qmi_framework

  PARAMETERS
    unsinged long int to satisfy rex_timer_cb_type type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern void  qmi_cat_init
(
  unsigned long int input
);

/*===========================================================================
QMI_CAT_SIG_HANDLER

DESCRIPTION
  QMI CAT signal handler for GSTK signals

PARAMETERS
  None.

  Returns TRUE if signal should be cleared from the set signal mask,
  FALSE if further signal processing is needed and hence signal
  should not be cleared.
===========================================================================*/
extern boolean qmi_cat_sig_handler
(
  void
);

/*===========================================================================
  FUNCTION QMI_CAT_GET_TERMINAL_PROFILE_VALUE_LIST()

  DESCRIPTION
    Get terminal profile value list according to the QMI_CAT config mode.

  PARAMETERS
    none

  RETURN VALUE
    gstk_profile_dl_support_ext_type* : array of terminal profile

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern gstk_profile_dl_support_ext_type* qmi_cat_get_terminal_profile_value_list
(
  uint8                            * tp_count_ptr
);

/*===========================================================================
  FUNCTION QMI_CAT_RECOVERY_COMPLETE()

  DESCRIPTION
    This function is called by the QMI UIM when a recovery indication
    has completed successfully

  PARAMETERS
    slot: Indicates the slot in which the recovery occured

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_cat_recovery_complete
(
  mmgsdi_slot_id_enum_type  slot
);

/*===========================================================================
FUNCTION: qmi_cat_deinit

DESCRIPTION:
  This function is used to free the qmicat private resources. 
  This is called when GSTK is powering down.

PARAMETERS:
  None

DEPENDENCIES:
  None

RETURN VALUE:
  None

COMMENTS:
  None

SIDE EFFECTS:
  None
===========================================================================*/
extern void qmi_cat_deinit
(
  void
);

#endif /* QMI_CAT_H */
