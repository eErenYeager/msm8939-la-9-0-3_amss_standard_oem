#ifndef SIMLOCK_MODEM_LIB_H
#define SIMLOCK_MODEM_LIB_H

/**
@file simlock_modem_lib.h
@brief This library contains functions for the SIMLock Feature

This interface allows clients to initalize, access and configure
the SIM Lock feature. 
*/

/*===========================================================================
NOTE: The @brief description and any detailed descriptions above do not appear
      in the PDF.

      The simlock_mainpage.dox file contains all file/group descriptions that are
      in the output PDF generated using Doxygen and Latex. To edit or update
      any of the file/group text in the PDF, edit the mmgsdi_mainpage.dox file or
      contact Tech Pubs.

      The above description for this file is part of the "session_library" group
      description in the simlock_mainpage.dox file.
===========================================================================*/

/*================================================================================

  This file contains Library function that can be used to externally
  initalize, access and configure the SIM Lock feature.

                        COPYRIGHT INFORMATION
Copyright (c) 2014 Qualcomm Technologies, Incorporated and its licensors.  All
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/26/14   tl      Add comdef.h
03/14/14   tl      Initial Version


=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/
#include "comdef.h"
#include "simlock_common.h"

#ifdef FEATURE_SIMLOCK
#error code not present
#endif /* FEATURE_SIMLOCK */
#endif /* SIMLOCK_MODEM_LIB_H */
