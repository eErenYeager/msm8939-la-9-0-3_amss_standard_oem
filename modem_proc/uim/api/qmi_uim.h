#ifndef QMI_UIM_H
#define QMI_UIM_H
/*===========================================================================

                         Q M I _ U I M . H

DESCRIPTION

 The Data Services QMI User Identity Module Service header file.

Copyright (c) 2010,2012-2016 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/api/qmi_uim.h#2 $ $DateTime: 2016/04/28 02:16:22 $ $Author: naushada $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
04/28/16    na     Add support for supply voltage command and indication
01/07/15    hh     Added support to notify recovery completion
12/24/13    am     Add NV protection to Recovery API
02/19/13    av     Merge mmgsi,gstk,qmiuim/cat conf files to mmgsdi.conf
02/14/13    tl     Move QMI UIM to MMGSDI context from QMI modem context
02/23/12    nmb    QMI QMUX Changes
08/05/10    tkl    QMI UIM & QMI CAT split from data package
01/14/10    mib    Initial check-in of QMI_UIM
===========================================================================*/

/*===========================================================================

                            CONSTANT DEFINITIONS

===========================================================================*/
#define QMI_UIM_CONFIG_SAP_SEC_RESTR      "/nv/item_files/modem/qmi/uim/sap_security_restrictions"
#define QMI_UIM_CONFIG_AUTH_SEC_RESTR     "/nv/item_files/modem/qmi/uim/auth_security_restrictions"
#define QMI_UIM_CONFIG_APDU_SEC_RESTR     "/nv/item_files/modem/qmi/uim/apdu_security_restrictions"
#define QMI_UIM_CONFIG_SILENT_PIN1_FILE   "/nv/item_files/modem/qmi/uim/qmi_uim_silent_pin1"
#define QMI_UIM_CONFIG_APDU_SEC_AID_LIST  "/nv/item_files/modem/qmi/uim/apdu_security_aid_list"
#define QMI_UIM_CONFIG_CLOSE_CHANNEL_SYNC "/nv/item_files/modem/qmi/uim/close_channel_sync"
#define QMI_UIM_CONFIG_SILENT_RECOVERY    "/nv/item_files/modem/qmi/uim/silent_recovery"

/*===========================================================================
  FUNCTION QMI_UIM_INIT_PRE_STARTUP()

  DESCRIPTION
    Initializes the QMI UIM global variables and register with MMGSDI

  PARAMETERS
    void

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_uim_init_pre_startup
(
  void
);

/*===========================================================================
  FUNCTION QMI_UIM_INIT_POST_STARTUP()

  DESCRIPTION
    Registers the QMI UIM service to the QMI framework to initalize
    the service.

  PARAMETERS
    unsinged long int to satisfy rex_timer_cb_type type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_uim_init_post_startup
(
  unsigned long int input
);

/*===========================================================================
QMI_UIM_SIG_HANDLER

DESCRIPTION
  QMI UIM signal handler for MMGSDI task signals

PARAMETERS
  None.

  Returns TRUE if signal should be cleared from the set signal mask,
  FALSE if further signal processing is needed and hence signal
  should not be cleared.
===========================================================================*/
extern boolean qmi_uim_sig_handler
(
  void
);

/*===========================================================================
  FUNCTION QMI_UIM_RECOVERY_COMPLETE_IND()

  DESCRIPTION
    This function is invoked by the UIM drivers when a recovery indication
    has completed successfully to inform clients of QMI UIM.

  PARAMETERS
    slot: Indicates the slot in which the recovery occured

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_uim_recovery_complete_ind
(
  const uim_slot_type slot
);


/*===========================================================================
  FUNCTION QMI_UIM_SUPPLY_VOLTAGE_IND()

  DESCRIPTION
    This function is invoked by the UIM drivers when the Vcc needs to
    be deactivated or when Vcc has been activated.

  PARAMETERS
    slot                                      : UIM slot id
    uimdrv_qmi_indications_type               : LDO state
    uimdrv_qmi_power_management_callback_type : callback to ack drivers

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_uim_supply_voltage_ind
(
  uim_slot_type                                     slot,
  uimdrv_qmi_indications_type                       ldo_state,
  uimdrv_qmi_power_management_callback_type         uim_callback_ptr
);

#endif /* QMI_UIM_H */
