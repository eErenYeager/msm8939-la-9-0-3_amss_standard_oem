/*================================================================================

DESCRIPTION


Copyright (c) 2012 - 2013QUALCOMM Technologies, Inc(QTI).
All Rights Reserved.
QUALCOMM Technologies Confidential and Proprietary

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimqmi/src/qmi_uim_cat_common.h#1 $$ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/27/14    at     Usage of common QMI APIs from DS header
07/02/13    kb     Added support for memory leak detection
06/12/13    kb     Added QMI malloc and free wrapper functions
09/07/12    at     Initial version
=============================================================================*/

#ifndef QMI_UIM_CAT_COMMON_H
#define QMI_UIM_CAT_COMMON_H

/*=============================================================================

                         INCLUDE FILES FOR MODULE

=============================================================================*/
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

/*=============================================================================

                                  FUNCTIONS

=============================================================================*/

/*===========================================================================
  FUNCTION UIMQMI_MALLOC

  DESCRIPTION
    The uimqmi_malloc calls modem_mem_calloc to request memory from the Modem Heap.

  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
void * uimqmi_malloc
(
  uint32 size
);

/*===========================================================================
  FUNCTION UIMQMI_FREE

  DESCRIPTION
    The uimqmi_free frees the pointer from the Modem Heap.

  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
void uimqmi_free
(
  void * ptr
);
#endif /* QMI_UIM_CAT_COMMON_H */
