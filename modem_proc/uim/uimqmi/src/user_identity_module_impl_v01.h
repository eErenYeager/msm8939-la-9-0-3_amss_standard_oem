#ifndef USER_IDENTITY_MODULE_V01IMPL_H
#define USER_IDENTITY_MODULE_V01IMPL_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

              U S E R _ I D E N T I T Y _ M O D U L E _ I M P L _ V 0 1  . h

GENERAL DESCRIPTION
  This is the file which defines the uim service Data structures.

  Copyright (c) 2010-2013 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimqmi/src/user_identity_module_impl_v01.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.2
   It was generated on: Fri May 10 2013 (Spin 0)
   From IDL File: user_identity_module_v01.idl */

#include "qmi_si.h"
#ifdef __cplusplus
extern "C" {
#endif
const qmi_implemented_messages *uim_get_service_impl_v01 (void);


#ifdef __cplusplus
}
#endif
#endif

