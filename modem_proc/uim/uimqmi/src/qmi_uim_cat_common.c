/*================================================================================

DESCRIPTION


Copyright (c) 2013 - 2014 QUALCOMM Technologies, Inc(QTI).
All Rights Reserved.
QUALCOMM Technologies Confidential and Proprietary

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/uim/uimqmi/src/qmi_uim_cat_common.c#1 $$ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
03/27/14    at     Usage of common QMI APIs from DS header
02/12/14   at      Additional check in the free transaction API
01/10/14   df      Use v2.0 diag macros
10/08/13   gm      Removed unused comments
10/04/13   gm      F3 message reduction
06/12/13   kb      Added QMI malloc and free wrapper functions
04/24/13   tkl     Hardening qmi_svc_new_cmd_buf against overflow
09/07/12   at      Initial version
=============================================================================*/


#include "customer.h"
#include "comdef.h"
#include "uim_msg.h"
#include "err.h"
#include "amssassert.h"
#include "qmi_uim_cat_common.h"
#include "modem_mem.h"


/*===========================================================================
  FUNCTION UIMQMI_MALLOC

  DESCRIPTION
    The uimqmi_malloc calls modem_mem_calloc to request memory from the Modem Heap.

  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
void * uimqmi_malloc
(
  uint32 size
)
{
  return modem_mem_calloc(1, (size), MODEM_MEM_CLIENT_UIM);
}/* uimqmi_malloc */


/*===========================================================================
  FUNCTION UIMQMI_FREE

  DESCRIPTION
    The uimqmi_free frees the pointer from the Modem Heap.

  DEPENDENCIES
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None
===========================================================================*/
void uimqmi_free
(
  void * ptr
)
{
  modem_mem_free((void*)(ptr), MODEM_MEM_CLIENT_UIM);
}/* uimqmi_free */
