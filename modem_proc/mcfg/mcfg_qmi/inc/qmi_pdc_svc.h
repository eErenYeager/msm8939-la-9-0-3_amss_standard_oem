#ifndef QMI_PDC_SVC_H
#define QMI_PDC_SVC_H
/*==============================================================================

   Q M I   P E R S I S T E N T   D E V I C E   C O N F I G   S E R V I C E
 
GENERAL DESCRIPTION
  Persistent Device Configuration (PDC) QMI service header file

Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_qmi/inc/qmi_pdc_svc.h#3 $ 
$DateTime: 2016/03/31 23:27:01 $ 
$Author: ravikira $ 
$Change: 10184892 $ 

when     who  what, where, why
-------- ---  ----------------------------------------------------------------
04/30/10 mj   Initial creation

==============================================================================*/

#include "qmi_csi.h"
#include "mcfg_common.h"

/*==============================================================================

                PUBLIC DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/
/*==============================================================================
  Constants and Macros
==============================================================================*/


/*==============================================================================
  Typedefs
==============================================================================*/


/*==============================================================================
  Variables
==============================================================================*/


/*============================================================================== 
 
                    PUBLIC FUNCTION DECLARATIONS FOR MODULE
 
==============================================================================*/

/*===========================================================================

  FUNCTION pdc_generate_config_change_ind

  DESCRIPTION
    This function generates and sends QMI_PDC indications to all clients that
    have registered for the config file change event.

  DEPENDENCIES
    QMI_PDC must be initialized

  PARAMETERS
    type      [in] type of config file
    id        [in] ID of new config file

  RETURN VALUE
    None

  SIDE EFFECTS
    None

===========================================================================*/
void pdc_generate_config_change_ind
( 
  mcfg_config_type_e_type type,
  mcfg_config_id_s_type  *id
);


/*===========================================================================

  FUNCTION pdc_svc_init

  DESCRIPTION
    This function registers the PDC service with the CSI framework.  The service
    is also registered with QSAP so that it is available via QMUX framework.

  DEPENDENCIES
    QCSI/QSAP must be initialized

  PARAMETERS
    os_params [in] task parameters for the service

  RETURN VALUE
    Service handle from the CSI framework

  SIDE EFFECTS
    Registers with the CSI framework and QSAP

===========================================================================*/
qmi_csi_service_handle pdc_svc_init
(
  qmi_csi_os_params *os_params
);

#endif /* QMI_PDC_SVC_H */

