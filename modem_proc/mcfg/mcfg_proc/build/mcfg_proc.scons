#===============================================================================
#
# MODEM_CFG Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2012-2013 by QUALCOMM Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_proc/build/mcfg_proc.scons#3 $
#  $DateTime: 2016/03/31 23:27:01 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 01/14/12   sbt     Initial creation
#===============================================================================

#-----------------------------------------
# Import and clone the SCons environment
#-----------------------------------------
Import('env')
env = env.Clone()

#--------------------------------------------------------------------------------
# Required Public, Restricted & Protected APIs
#--------------------------------------------------------------------------------

env.RequirePublicApi([
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'BUSES',
    'HAL',
    'HWENGINES',
    'IODEVICES',
    'MPROC',
    'POWER',
    'SECUREMSM',
    'SERVICES',
    'STORAGE',
    'DAL',
    # needs to be last also contains wrong comdef.h
    'KERNEL',
    ], area='core')

env.RequirePublicApi([
    'MCFG',
    'MCS',
    ])


env.RequirePublicApi([
    'MCS',
    ], area='mcs')

env.RequireRestrictedApi([
    'MCFG',
    'MCS',
    ])


env.RequireProtectedApi([
    'MCFG',
    ])

#----------------------------------------------------------------------------#
################ Temp Workaround for "miprogressive.h" dependency ############
#### Next version to remove this dependency ##################################
#----------------------------------------------------------------------------#
env.PublishPrivateApi('VIOLATIONS',
                      ['${INC_ROOT}/core/boot/secboot3/src'])

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = '../src'

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for legacy MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_MCFG",
])

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
MCFG_PROC_SOURCES = [
	'${BUILDPATH}/mcfg_task.c',
	'${BUILDPATH}/mcfg_proc.c',
	'${BUILDPATH}/mcfg_default.c',
]

# Add our library to the MODEM_AMSS image
env.AddLibrary(['MODEM_MODEM'], '${BUILDPATH}/mcfg_proc', MCFG_PROC_SOURCES)

# Build image for which this task belongs
RCINIT_MCFG = ['MODEM_MODEM']

# RC Init Function Dictionary
RCINIT_INIT_MCFG = {
            'sequence_group'      : 'RCINIT_GROUP_2',
            'init_name'           : 'modem_cfg_init',
            'init_function'       : 'modem_cfg_init_before_task_start'
    }

# RC Init Task Dictionary
RCINIT_TASK_MCFG = {
            'thread_name'    : 'modem_cfg',
            'sequence_group' : 'RCINIT_GROUP_2',
            'stack_size_bytes'     : '16384',
            'priority_amss_order'  : 'SHARED_BACKGROUND_PRI_ORDER',
            'thread_entry'         : 'modem_cfg_task',
    }
    
# Add init function to RCInit
if 'USES_RCINIT' in env:
	env.AddRCInitFunc(RCINIT_MCFG, RCINIT_INIT_MCFG)
	env.AddRCInitTask(RCINIT_MCFG, RCINIT_TASK_MCFG)
