/*===========================================================================
                        modem_cfg_task.h

DESCRIPTION

   Internal header file for modem_cfg_task
 
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_proc/src/mcfg_task.h#3 $ $DateTime: 2016/03/31 23:27:01 $ $Author: ravikira $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
1/18/12      sbt   Create

===========================================================================*/

#ifndef MODEM_CFG_TASK_I_H
#define MODEM_CFG_TASK_I_H

#include "task.h"

/* REX Task Signals declarations.                                           */

#define  MODEM_CFG_DOG_RPT_TIMER_SIG     0x0001      /* Watchdog report signal    */
#define  MODEM_CFG_CMD_Q_INT_SIG         0x0002      /* Command queue signal      */

/* Function prototypes */
void modem_cfg_init_before_task_start(void);


#endif /* MODEM_CFG_TASK_I_H */
