#===============================================================================
#
# MODEM_CFG Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/build/mcfg.scons#3 $
#  $DateTime: 2016/03/31 23:27:01 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 01/14/12   sbt     Initial creation
#===============================================================================

#-----------------------------------------
# Import and clone the SCons environment
#-----------------------------------------
Import('env')

#-----------------------------------------
# Publish Protected Include Paths only avail to the rest of MCFG
#-----------------------------------------
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_proc/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_auth/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_nv/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_gen/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_qmi/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_sel/inc"])
env.PublishProtectedApi('MCFG', ["${INC_ROOT}/mcfg/mcfg_utils/inc"])

#-------------------------------------------------------------------------------
# Load cleanpack script to remove private files etc. from customer builds
#-------------------------------------------------------------------------------
import os
if os.path.exists('mcfg_cleanpack.py'):
    env.LoadToolScript('mcfg_cleanpack.py')

env.Append(HEXAGONCC_WARN = ' -Werror ')
env.Append(HEXAGONCXX_WARN = ' -Werror ')

if env['PRODUCT_LINE'].startswith("MPSS.DPM.2"):
  env.Append(CPPDEFINES = ["MCFG_MODEM_INITITIATED_GRACEFUL_RESET_SUPPORT"])
  env.Append(CPPDEFINES = ["FEATURE_DYNREC_RECLAIMING"])

#-----------------------------------------
# Search for additional Units (must come last)
#-----------------------------------------
env.LoadSoftwareUnits()
