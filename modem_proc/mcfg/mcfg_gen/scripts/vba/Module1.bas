Attribute VB_Name = "Module1"
Sub createQCVersion()
'
' createQCVersion Macro
'

'
    Rows("433:435").Select
    Selection.Copy
    Rows("448:448").Select
    Selection.Insert Shift:=xlDown
    Range("F448").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "0x05"
    Range("A448").Select
    ActiveCell.FormulaR1C1 = "QC_version_type"
    Range("A449").Select
    ActiveCell.FormulaR1C1 = "QC_version_len"
    Range("A450").Select
    ActiveCell.FormulaR1C1 = "QC_Version"
    Range("A451").Select
End Sub

Sub Macro1()
Attribute Macro1.VB_ProcData.VB_Invoke_Func = "T\n14"
'
' Macro2 Macro
'
' Keyboard Shortcut: Ctrl+Shift+T
'
    Selection.Copy
    Range("B43").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Rows("44:44").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlUp
    Range("B43").Select
    ActiveWindow.ScrollColumn = 2
    ActiveWindow.ScrollColumn = 3
    ActiveWindow.ScrollColumn = 4
    ActiveWindow.ScrollColumn = 5
    ActiveWindow.ScrollColumn = 7
    ActiveWindow.ScrollColumn = 8
    Range("B43:Q44").Select
    Selection.Copy
    ActiveWindow.ScrollColumn = 7
    ActiveWindow.ScrollColumn = 5
    ActiveWindow.ScrollColumn = 4
    ActiveWindow.ScrollColumn = 3
    ActiveWindow.ScrollColumn = 2
    ActiveWindow.ScrollColumn = 1
    Range("B46").Select
    Selection.PasteSpecial Paste:=xlPasteAll, Operation:=xlNone, SkipBlanks:= _
        False, Transpose:=True
    Rows("43:44").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlUp
    Range("B44").Select
    ActiveWindow.SmallScroll Down:=6
    Range("B44:C59").Select
    Selection.Copy
End Sub

Sub Increment_cell_value()
'
' Increment_cell_value Macro
'
' Keyboard Shortcut: Ctrl+Shift+I
'
   ' Range("F28").Value = Range("F28").Value + 1
   For Each c In Selection
     c.Value = c.Value + 1
   Next
End Sub

Sub Macro2()
'
' Macro2 Macro
'
' Keyboard Shortcut: Ctrl+Shift+L
'
    ActiveSheet.OLEObjects.Add(ClassType:="Forms.CommandButton.1", Link:=False _
        , DisplayAsIcon:=False, Left:=450.882352941176, Top:=382.941176470588, _
        Width:=72.3529411764706, Height:=23.8235294117647).Select
    ActiveSheet.Shapes.Range(Array("buildVersionTextBox")).Select
    Selection.Delete
    Selection.Cut
    ActiveSheet.Shapes.Range(Array("CommandButton1")).Select
    Selection.Delete
    Selection.Cut
    Rows("24:24").Select
    Selection.Delete Shift:=xlUp
    ActiveSheet.Shapes.Range(Array("buildRootTextBox")).Select
    ActiveSheet.Shapes("buildRootTextBox").IncrementLeft 313.2352755906
    ActiveSheet.Shapes("buildRootTextBox").IncrementTop -223.2352755906
    ActiveSheet.Shapes.Range(Array("buildIDTextBox")).Select
    ActiveSheet.Shapes("buildIDTextBox").IncrementLeft 312.3529133858
    ActiveSheet.Shapes("buildIDTextBox").IncrementTop -225
    Rows("10:10").Select
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    ActiveSheet.Shapes.Range(Array("buildIDTextBox")).Select
    ActiveSheet.Shapes("buildIDTextBox").IncrementLeft -4.4117322835
    ActiveSheet.Shapes("buildIDTextBox").IncrementTop 63.5294488189
    ActiveSheet.Shapes.Range(Array("buildRootTextBox")).Select
    ActiveSheet.Shapes("buildRootTextBox").IncrementLeft -5.2940944882
    ActiveSheet.Shapes("buildRootTextBox").IncrementTop 52.0588188976
    Rows("17:17").Select
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 7
    ActiveSheet.Shapes.Range(Array("Button 55")).Select
    ActiveSheet.Shapes("Button 55").IncrementLeft 292.9411811024
    ActiveSheet.Shapes("Button 55").IncrementTop -361.7647244094
    Rows("30:32").Select
    Selection.Cut
    Rows("11:11").Select
    Selection.Insert Shift:=xlDown
    ActiveSheet.Shapes.Range(Array("buildRootTextBox")).Select
    ActiveSheet.Shapes("buildRootTextBox").IncrementLeft -248.8235433071
    ActiveSheet.Shapes("buildRootTextBox").IncrementTop 11.4705511811
    ActiveSheet.Shapes.Range(Array("buildIDTextBox")).Select
    ActiveSheet.Shapes("buildIDTextBox").IncrementLeft -249.7058267717
    ActiveSheet.Shapes("buildIDTextBox").IncrementTop 12.3529133858
    Range("A11").Select
    Selection.Cut
    Range("A10").Select
    ActiveSheet.Paste
    Range("A13").Select
    Selection.Cut
    Range("A12").Select
    ActiveSheet.Paste
    ActiveSheet.Shapes.Range(Array("buildIDTextBox")).Select
    ActiveSheet.Shapes("buildIDTextBox").IncrementTop -14.1176377953
    Range("A12").Select
    Selection.Cut
    Range("A11").Select
    ActiveSheet.Paste
    ActiveSheet.Shapes.Range(Array("Button 55")).Select
    ActiveSheet.Shapes("Button 55").IncrementLeft -254.1177165354
    ActiveSheet.Shapes("Button 55").IncrementTop 12.352992126
    ActiveSheet.Shapes.Range(Array("Button 8")).Select
    ActiveSheet.Shapes("Button 8").IncrementLeft 109.4117322835
    ActiveSheet.Shapes("Button 8").IncrementTop -397.9411811024
    ActiveSheet.Shapes.Range(Array("Button 13")).Select
    ActiveSheet.Shapes("Button 13").IncrementLeft 465
    ActiveSheet.Shapes("Button 13").IncrementTop -452.647007874
    ActiveSheet.Shapes.Range(Array("Button 22")).Select
    ActiveSheet.Shapes("Button 22").IncrementLeft 809.1176377953
    ActiveSheet.Shapes("Button 22").IncrementTop -514.4117322835
    Rows("35:35").Select
    Selection.Cut
    Rows("17:17").Select
    Selection.Insert Shift:=xlDown
    Range("C19").Select
    ActiveSheet.Shapes.Range(Array("keepBuildWindowOpen")).Select
    ActiveSheet.Shapes("keepBuildWindowOpen").IncrementLeft -180.8823622047
    ActiveSheet.Shapes("keepBuildWindowOpen").IncrementTop 7.0588188976
    Range("B17").Select
    ActiveSheet.Shapes.Range(Array("Button 23")).Select
    ActiveSheet.Shapes("Button 23").IncrementLeft 576.1764566929
    ActiveSheet.Shapes("Button 23").IncrementTop -138.5293700787
    ActiveSheet.Shapes.Range(Array("Button 24")).Select
    ActiveSheet.Shapes("Button 24").IncrementLeft 695.2941732283
    ActiveSheet.Shapes("Button 24").IncrementTop -154.4118110236
    Range("A24").Select
    ActiveWindow.SmallScroll ToRight:=12
    Range("A24:R29").Select
    Selection.Copy
    ActiveWindow.ScrollColumn = 12
    ActiveWindow.ScrollColumn = 11
    ActiveWindow.ScrollColumn = 9
    ActiveWindow.ScrollColumn = 6
    ActiveWindow.ScrollColumn = 3
    ActiveWindow.ScrollColumn = 1
    Range("A31").Select
    Selection.PasteSpecial Paste:=xlPasteAll, Operation:=xlNone, SkipBlanks:= _
        False, Transpose:=True
    Rows("19:20").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlUp
    Rows("18:19").Select
    Selection.Delete Shift:=xlUp
    Rows("20:26").Select
    Selection.Delete Shift:=xlUp
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE1")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE1", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE2")).Select
    Selection.ShapeRange.IncrementLeft 0.8823622047
    Selection.ShapeRange.IncrementLeft -0.8823622047
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE1", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE2", "CMCC_SGLTE_SinglSIM_LabConf_UE3")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2")).Select
    ActiveWindow.SmallScroll ToRight:=5
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", _
        "CMCC_CSFB_SingleSIM_UE3")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1", "CMCC_CSFB_DSDS_UE2")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1", "CMCC_CSFB_DSDS_UE2", "CMCC_CSFB_DSDS_UE3")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1", "CMCC_CSFB_DSDS_UE2", "CMCC_CSFB_DSDS_UE3", _
        "CMCC_SGLTE_DSDA_LabConf_UE1")).Select
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1", "CMCC_CSFB_DSDS_UE2", "CMCC_CSFB_DSDS_UE3", _
        "CMCC_SGLTE_DSDA_LabConf_UE1", "CMCC_SGLTE_DSDA_LabConf_UE2")).Select
    ActiveWindow.SmallScroll ToRight:=2
    ActiveSheet.Shapes.Range(Array("CMCC_CSFB_DSDS_Commercial", _
        "CMCC_SGLTE_DSDA_Commercial", "CMCC_SGLTE_SingleSIM_Commercial", _
        "CMCC_CSFB_SingleSIM_Commercial", "CMCC_SGLTE_SinglSIM_LabConf_UE3", _
        "CMCC_SGLTE_SinglSIM_LabConf_UE1", "CMCC_SGLTE_SinglSIM_LabConf_UE2", _
        "CMCC_CSFB_SingleSIM_UE1", "CMCC_CSFB_SingleSIM_UE2", "CMCC_CSFB_SingleSIM_UE3" _
        , "CMCC_CSFB_DSDS_UE1", "CMCC_CSFB_DSDS_UE2", "CMCC_CSFB_DSDS_UE3", _
        "CMCC_SGLTE_DSDA_LabConf_UE1", "CMCC_SGLTE_DSDA_LabConf_UE2", _
        "CMCC_SGLTE_DSDA_LabConf_UE3")).Select
    Selection.Delete
    ActiveWindow.ScrollColumn = 7
    ActiveWindow.ScrollColumn = 6
    ActiveWindow.ScrollColumn = 4
    ActiveWindow.ScrollColumn = 3
    ActiveWindow.ScrollColumn = 1
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 1
    ActiveSheet.Shapes.Range(Array("Default")).Select
    Selection.Delete
    Selection.Cut
    Rows("20:20").Select
    Selection.Insert Shift:=xlDown
    Selection.Insert Shift:=xlDown
    Range("A22").Select
    Selection.Cut
    Range("A21").Select
    ActiveSheet.Paste
    Range("A22").Select
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 9
    ActiveWindow.ScrollRow = 10
    Range("A22:A38").Select
    Selection.Cut
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    Range("A22").Select
    ActiveCell.FormulaR1C1 = "Carrier Name"
    Columns("B:B").Select
    Range("B10").Activate
    Columns("A:A").EntireColumn.AutoFit
    Range("A22:A38").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.InsertIndent 1
    Columns("A:A").EntireColumn.AutoFit
    Range("C22:C38").Select
    Selection.Cut
    Range("E22").Select
    Selection.Insert Shift:=xlToRight
    Range("C22").Select
    ActiveCell.FormulaR1C1 = "Revision"
    Range("E22:E38").Select
    Selection.Delete Shift:=xlToLeft
    Range("A21").Select
    Selection.Copy
    Range("A21:D21").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    Rows("20:20").Select
    Selection.Insert Shift:=xlDown
    Rows("22:22").Select
    Selection.Copy
    Rows("20:20").Select
    ActiveSheet.Paste
    Range("A20").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "Summary Table"
    Range("A22").Select
    ActiveCell.FormulaR1C1 = "Carrier Listings"
    Rows("22:22").Select
    Selection.Insert Shift:=xlDown
    Range("A21").Select
    ActiveCell.FormulaR1C1 = "Configuration Type"
    Range("B21").Select
    ActiveCell.FormulaR1C1 = "1"
    Rows("21:21").Select
    Selection.Font.Bold = False
    Rows("21:21").Select
    Range("A22").Select
    ActiveCell.FormulaR1C1 = "Compatability Version"
    Rows("22:22").Select
    Selection.Font.Bold = False
    Range("B22").Select
    ActiveCell.FormulaR1C1 = "4"
    Range("A20:D41").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Range("A24:A40").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlMedium
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlMedium
    End With
    Selection.Borders(xlInsideVertical).LineStyle = xlNone
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
    Range("A24:A41").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Selection.Borders(xlInsideVertical).LineStyle = xlNone
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
    Range("B41").Select
    Selection.Copy
    Range("A41").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    Range("A20:D41").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Range("A23:D24").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Selection.Borders(xlEdgeTop).LineStyle = xlNone
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideHorizontal)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Range("A20:D41").Select
    Selection.Cut
    Range("B20").Select
    ActiveSheet.Paste
    Columns("B:B").EntireColumn.AutoFit
    Range("B21").Select
    ActiveWindow.LargeScroll Down:=-1
    Range("A4:A8").Select
    With Selection
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Columns("A:A").EntireColumn.AutoFit
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 9
    Range("E20:E41").Select
    Selection.Copy
    Range("F20:F41").Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False
    Range("F24").Select
    ActiveCell.FormulaR1C1 = "Selected Configurations"
    Range("B21:F22").Select
    Range("F21").Activate
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlMedium
    End With
    Selection.Borders(xlEdgeBottom).LineStyle = xlNone
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Selection.Borders(xlInsideVertical).LineStyle = xlNone
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
    Range("G24").Select
    ActiveWindow.LargeScroll Down:=-1
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    Range("B17").Select
    ActiveSheet.Shapes.Range(Array("keepBuildWindowOpen")).Select
    ActiveSheet.Shapes("keepBuildWindowOpen").IncrementLeft 181.7647244094
    ActiveSheet.Shapes("keepBuildWindowOpen").IncrementTop -2.647007874
    Range("A17").Select
    Selection.Cut
    Range("B17").Select
    ActiveSheet.Paste
    Columns("A:A").EntireColumn.AutoFit
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 1
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 9
    ActiveWindow.ScrollRow = 10
    ActiveWindow.ScrollRow = 11
    ActiveWindow.ScrollRow = 12
    ActiveWindow.ScrollRow = 13
    ActiveWindow.ScrollRow = 12
    ActiveWindow.ScrollRow = 11
    ActiveWindow.ScrollRow = 10
    ActiveWindow.ScrollRow = 9
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 1
    ActiveSheet.Shapes.Range(Array("Button 23")).Select
    Selection.Characters.Text = "Select All"
    With Selection.Characters(Start:=1, Length:=10).Font
        .Name = "Arial"
        .FontStyle = "Regular"
        .Size = 10
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ColorIndex = 1
    End With
    Range("E17").Select
    ActiveSheet.Shapes.Range(Array("Button 23")).Select
    ActiveSheet.Shapes("Button 23").IncrementLeft 153.5293700787
    ActiveSheet.Shapes("Button 23").IncrementTop 18.5294488189
    ActiveSheet.Shapes.Range(Array("Button 24")).Select
    ActiveSheet.Shapes("Button 24").IncrementLeft 147.3529133858
    ActiveSheet.Shapes("Button 24").IncrementTop 15.8823622047
    ActiveSheet.Shapes("Button 24").IncrementLeft -0.8823622047
    ActiveSheet.Shapes("Button 24").IncrementTop 2.6470866142
    Range("G20").Select
    ActiveWindow.ScrollRow = 2
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 9
    ActiveWindow.ScrollRow = 10
    ActiveWindow.ScrollRow = 11
    ActiveWindow.ScrollRow = 12
    ActiveWindow.ScrollRow = 11
    ActiveWindow.ScrollRow = 10
    ActiveWindow.ScrollRow = 9
    ActiveWindow.ScrollRow = 8
    ActiveWindow.ScrollRow = 7
    ActiveWindow.ScrollRow = 6
    ActiveWindow.ScrollRow = 5
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 2
    Range("A4:A8").Select
    Selection.InsertIndent 1
    Selection.InsertIndent 1
    Range("C6").Select
    ActiveWindow.ScrollRow = 3
    ActiveWindow.ScrollRow = 4
    ActiveWindow.ScrollRow = 5
End Sub

Sub Macro7()
Attribute Macro7.VB_ProcData.VB_Invoke_Func = "R\n14"
'
' Macro7 Macro
'
' Keyboard Shortcut: Ctrl+Shift+R
'
    ActiveSheet.Buttons.Add(44.25, 300, 105.75, 15.75).Select
    Selection.OnAction = "RefreshSummary"
    Selection.Characters.Text = "Refresh Summary List"
    With Selection.Characters(Start:=1, Length:=20).Font
        .Name = "Arial"
        .FontStyle = "Regular"
        .Size = 10
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ColorIndex = 1
    End With
    Range("B17").Select
End Sub

Sub sumUpdate()
'
' sumUpdate Macro
'
' Keyboard Shortcut: Ctrl+Shift+U
'
    Rows("19:19").Select
    ActiveSheet.Buttons.Add(44.25, 302.25, 109.5, 18).Select
    Selection.OnAction = "RefreshSummary"
    Selection.Characters.Text = "Refresh Summary Table"
    With Selection.Characters(Start:=1, Length:=21).Font
        .Name = "Arial"
        .FontStyle = "Regular"
        .Size = 10
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ColorIndex = 1
    End With
    Range("C18").Select
    ActiveSheet.OLEObjects.Add(ClassType:="Forms.CommandButton.1", Link:=False _
        , DisplayAsIcon:=False, Left:=435.882352941176, Top:=390, Width:= _
        72.3529411764706, Height:=23.8235294117647).Select
    ActiveSheet.Shapes.Range(Array("buildVersionTextBox")).Select
    Selection.Delete
    Selection.Cut
    ActiveSheet.Shapes.Range(Array("CommandButton1")).Select
    Selection.Delete
    Selection.Cut
    Range("A24").Select
    Selection.Cut
    Range("A24").Select
    Application.CutCopyMode = False
    Selection.ClearContents
End Sub
 
'If you have more then one occurrence of the value this will select the last occurrence.
Function Find_Last_Entry(cfg As String, Optional FindString As String = "") As Range
    Dim rng As Range
    
    If FindString = "" Then
       FindString = InputBox("Enter a Search value")
    End If
    If Trim(FindString) <> "" Then
         With Sheets(cfg).Range("A:A")
             Set rng = .Find(What:=FindString, _
                             After:=.Cells(1), _
                             LookIn:=xlValues, _
                             LookAt:=xlWhole, _
                             SearchOrder:=xlByRows, _
                             SearchDirection:=xlPrevious, _
                             MatchCase:=False)
         End With
    End If
    
    If Not rng Is Nothing Then
       Set Find_Last_Entry = rng
    Else
       Set Find_Last_Entry = Nothing
    End If
    
End Function

'Carrier names in trailer records are changing a lot. This function makes it easy to modify all of them
Sub SetTRLCarrierNames()
  Dim prefix As String
  Dim currCell As Range
  Dim cfg As String
  Dim i As Integer
  
  'Carrier name located at B9 on Summary page
  prefix = Sheets("Summary").Cells(9, "B").Value
  
  'Make sure config list is up to date
  SetConfigList
  
  For i = LBound(ConfigListings) To UBound(ConfigListings)
    Set currCell = Find_Last_Entry(ConfigListings(i), "carrier_name")
    If Not currCell Is Nothing Then
      currCell.offset(0, 5).Value = ConfigListings(i) & "-" & prefix
    End If
  Next
    
End Sub
