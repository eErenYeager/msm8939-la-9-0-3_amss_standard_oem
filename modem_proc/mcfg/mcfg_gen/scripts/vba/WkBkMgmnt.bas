Attribute VB_Name = "WkBkMgmnt"
'*****************************************************
'History   : 10/25/14 (asandstr) - Created
'*****************************************************
'File Purpose:
'  Manage settings that apply to the workbook itself

Option Explicit


'Displays currently enabled references in immediate window for easily obtaining GUID values from immediate window
Private Sub refG()
    Dim r As Reference
    With ActiveWorkbook.VBProject
        For Each r In .References
            Debug.Print r.GUID, r.Name, r.Major, r.Minor, r.Description
        Next r
    End With
End Sub

