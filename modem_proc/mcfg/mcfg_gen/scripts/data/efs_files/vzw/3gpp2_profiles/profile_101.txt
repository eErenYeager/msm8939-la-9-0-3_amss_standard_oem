APN_String:vzwims;
PDN_IP_Version_Type:V4_V6;
RAN_Type:EHRPD;
Failure_Timer_1:0;
Failure_Timer_2:0;
Failure_Timer_3:60;
Failure_Timer_4:120;
Failure_Timer_5:480;
Failure_Timer_6:900;
Disallow_Timer_1:0;
Disallow_Timer_2:0;
Disallow_Timer_3:60;
Disallow_Timer_4:120;
Disallow_Timer_5:480;
Disallow_Timer_6:900;
APN_Class:1;
PDN_Label:1;
OP_Reserved_PCO_ID:0xff00;
MCC:311;
MNC:480;