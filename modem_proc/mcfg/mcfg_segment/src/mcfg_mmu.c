/**
  @file mcfg_mmu.c

  @brief  
*/
/*
  Copyright (c) 2013 QUALCOMM Technologies Incorporated.
          All Rights Reserved.
    Qualcomm Confidential and Proprietary
*/

/*===========================================================================
                      EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_segment/src/mcfg_mmu.c#3 $
  $DateTime: 2016/03/31 23:27:01 $


  when        who     what, where, why
  ---------   ---     ---------------------------------------------------------------------------
  07/25/14    hzhi    Updated for CR 699655: optimize MMU_table lookup API to fix MPPS performance hit.
  06/10/14    hzhi    Created for CR 670990: removing unnecessary dependency of mmu update on segment loading feature.

===========================================================================*/

#include "comdef.h"
#include "customer.h"
#include "target.h"
#include "sys_type.h"
#include "sys.h"
#include "dsm_init.h"

#include "mcfg_int.h"
#include "mcfg_mmu.h"
#include "modem_mem.h"
#include "memheap.h"

/*------------------------------------------------------------------*
 * local data types to manage mcfg MMU table to translate        *
 * heap section VA to PA upon query.                                *
 *------------------------------------------------------------------*/
mcfg_MMU_entry_t mcfg_MMU_table[MCFG_MMU_ENTRY_MAX_NUMBER];

/*------------------------------------------------------------------*
 * mcfg_mmu_table_last_entry points to last non-zero entry in MMU.  *
 *------------------------------------------------------------------*/
mcfg_MMU_entry_t *mcfg_mmu_table_last_entry = &mcfg_MMU_table[0];

/*------------------------------------------------------------------*
 * Global data recording last error of mmu update for debug         *
 * purpose.                                                         *
 *------------------------------------------------------------------*/
mcfg_mmu_update_status_t mcfg_mmu_update_last_error = MCFG_MMU_UPDATE_SUCCESS;

/*===========================================================================
variable used in segload_update_MMU_table() to make sure target translation
is loaded in TLB. 
===========================================================================*/
int tmp_mem_read = 0;

/*------------------------------------------------------------------*
 * Global data recording heap information returned from enumeration,*
 * Kept global for debugging purpose.                               *
 *------------------------------------------------------------------*/
mem_heap_section *section_info;

/*===========================================================================

FUNCTION mcfg_mmu_update

DESCRIPTION
  This function update mcfg_MMU_table[] after segment loading is done. 
  Every heap section is supposed to be added into this MMU table as 
  a separate entry, and for mcfg_VA_to_PA() and mcfg_PA_to_VA() api to
  query on. 

DEPENDENCIES
  corebsp supports modem mem heap enumeration api modem_mem_get_section_info(). 

RETURN VALUE
  MCFG_MMU_UPDATE_SUCCESS             --> mcfg_MMU_table[] updated.
  MCFG_MMU_UPDATE_TRANSLATION_FAIL    --> given VA cannot be translated to
                                          leagal PA by qurt_lookup_physaddr().
  MCFG_MMU_UPDATE_ENTRY_OVERFLOW_FAIL --> too many heap sections according to
                                          enumeration result. 
  MCFG_MMU_UPDATE_MALLOC_FAIL         --> cannot allocate enough memory for
                                          section_info[] ADT.

SIDE EFFECTS
  None.

===========================================================================*/
mcfg_mmu_update_status_t mcfg_mmu_update(void)
{
  int i = 0, j = 0;
  qurt_addr_t tmp_va, tmp_va_end;
  qurt_paddr_t tmp_pa;
  unsigned int tmp_offset, pre_offset = image_pstart - image_vstart;

  //first mmu table entry set for entire modem image except for dynamic heap.
  mcfg_MMU_table[0].va_start = 0;
  mcfg_MMU_table[0].va_end = 0xFFFFFFFF;
  mcfg_MMU_table[0].pa_start = 0;
  mcfg_MMU_table[0].pa_end = 0xFFFFFFFF;
  mcfg_MMU_table[0].offset = pre_offset;

  
  section_info = (mem_heap_section*)modem_mem_alloc(MEM_HEAP_SECTIONS_MAX*sizeof(mem_heap_section),44); 
  if(!section_info)
  {
    mcfg_mmu_update_last_error = MCFG_MMU_UPDATE_MALLOC_FAIL;
    return mcfg_mmu_update_last_error;
  }
  

  modem_mem_get_section_info((void **)&section_info);

  while(i<MEM_HEAP_SECTIONS_MAX)
  {
    if((section_info[i].start_addr != NULL)&&(section_info[i].size != 0))
    {
      tmp_va = (qurt_addr_t)(section_info[i].start_addr);
      tmp_va_end = tmp_va + (qurt_size_t)(section_info[i].size) - 1;
      
      tmp_mem_read += *((int *)tmp_va);
       
      if((tmp_pa = qurt_lookup_physaddr(tmp_va)) == 0)
      {
        mcfg_mmu_update_last_error = MCFG_MMU_UPDATE_TRANSLATION_FAIL;
        break;
      }

      tmp_offset = (unsigned int)tmp_pa - (unsigned int)tmp_va;

      j++;
      if(j >= MCFG_MMU_ENTRY_MAX_NUMBER)
      {
        mcfg_mmu_update_last_error = MCFG_MMU_UPDATE_ENTRY_OVERFLOW_FAIL;
        break;
      }
      mcfg_MMU_table[j].va_start = tmp_va;
      mcfg_MMU_table[j].va_end = tmp_va_end;
      mcfg_MMU_table[j].offset = tmp_offset;
    }
    i++;
  }
  //going back and update mmu table for sections from heap. 
  j = 1;

  while(mcfg_MMU_table[j].va_start != 0)
  {
    mcfg_MMU_table[j].pa_start = mcfg_MMU_table[j].va_start + mcfg_MMU_table[j].offset;
    mcfg_MMU_table[j].pa_end = mcfg_MMU_table[j].va_end + mcfg_MMU_table[j].offset;
    //mem_heap_add_section(&modem_mem_heap, (void *)mcfg_MMU_table[i].va_start, (unsigned long)(mcfg_MMU_table[i].va_end + 1 - mcfg_MMU_table[i].va_start));
    j++;
  }
  
  //point mcfg_mmu_table_last_entry to updated last non-zero entry of MMU table.
  mcfg_mmu_table_last_entry = &mcfg_MMU_table[j-1];
  return mcfg_mmu_update_last_error;
}


/*===========================================================================

FUNCTION mcfg_mmu_update_init

DESCRIPTION
  This function is entry point to update mcfg_MMU_table[] after segment 
  loading is done. 

DEPENDENCIES
  modem_cfg_task(). 

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void mcfg_mmu_update_init(void)
{
  if(mcfg_mmu_update() != MCFG_MMU_UPDATE_SUCCESS)
  {
    MCFG_MSG_HIGH("Modem Config MMU Table Update Failed.");
  }
  return;
}