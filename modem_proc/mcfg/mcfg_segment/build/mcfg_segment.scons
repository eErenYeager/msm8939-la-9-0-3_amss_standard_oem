#===============================================================================
#
# MODEM_CFG_SEGMENT_LOADING Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2013 by QUALCOMM Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_segment/build/mcfg_segment.scons#3 $
#  $DateTime: 2016/03/31 23:27:01 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 06/10/14   hzhi    CR 670990: removing unnecessary dependency of mmu update on segment loading feature. 
# 09/26/13   hzhi    Updated public api list and private api list. 
# 02/15/13   ngeng   Created module
#===============================================================================

Import('env')
env = env.Clone()

mcfg_segment_public_api_list = [
        ('CORE',                'STORAGE'),
        ('CORE',                'KERNEL'),
        ('CORE',                'DAL'),
        ('CORE',                'DEBUGTOOLS'),
        ('CORE',                'SERVICES'),
        ('CORE',                'MEMORY'),
        ('MCFG',                'MCFG'),
        ('MMCP',                'MMCP'),
        ('MMCP',                'PUBLIC'),
        ]

for api_area,api_name in mcfg_segment_public_api_list:
    env.RequirePublicApi([api_name], area=api_area)
        
env.RequireRestrictedApi([
    'MCFG',
    ])

SRCPATH = "${BUILD_ROOT}/mcfg/mcfg_segment/src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

if env['PRODUCT_LINE'].startswith("MPSS.DPM.2"):
   env.Append(CPPDEFINES = ['FEATURE_BORROW_FROM_HLOS'])
   env.Append(CPPDEFINES = ['MCFG_MODEM_INITITIATED_GRACEFUL_RESET_SUPPORT'])
   env.Append(CPPDEFINES = ["BUF_FROM_AMSS_SIZE=0x300000"])

if env['PRODUCT_LINE'].startswith("MPSS.DPM.1"):
   env.Append(CPPDEFINES = ["BUF_FROM_AMSS_SIZE=0x200000"])

if env.has_key('USES_FEATURE_SEGMENT_LOADING'):
  MCFG_SEGMENT_SOURCES = [
    '${BUILDPATH}/mcfg_seg_load.c',
    '${BUILDPATH}/mcfg_mmu.c',
  ]

else:
  MCFG_SEGMENT_SOURCES = [
    '${BUILDPATH}/mcfg_mmu.c',
  ]

# Add our library to the MODEM_AMSS image
#if env.has_key('USES_FEATURE_SEGMENT_LOADING'):
env.AddLibrary(['MODEM_MODEM'], '${BUILDPATH}/mcfg_segment', MCFG_SEGMENT_SOURCES)

env.LoadSoftwareUnits()


#if 'USES_RCINIT' in env:
#   RCINIT_IMG = ['MODEM_MODEM']
#   env.AddRCInitFunc(           # Code Fragment in TMC: NO
#    RCINIT_IMG,                 # define TMC_RCINIT_INIT_DSM_INIT
#    {
#     'sequence_group'             : 'RCINIT_GROUP_1',                   # required
#     'init_name'                  : 'segmentloading',                   # required
#     'init_function'              : 'seg_load_init',                    # required
#     'dependencies'               : ['']
#    })