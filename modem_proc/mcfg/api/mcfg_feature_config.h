/*===========================================================================
                        mcfg_feature_config.h

DESCRIPTION

   Feature config supprot for the MCFG module
 
Copyright (c) 2012-2013 by QUALCOMM Technologies Incorporated.
===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/api/mcfg_feature_config.h#1 $ $DateTime: 2016/03/31 23:27:01 $ $Author: ravikira $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
1/18/12      sbt   Create

===========================================================================*/

#ifndef MODEM_CFG_FEATURE_CONFIG_H
#define MODEM_CFG_FEATURE_CONFIG_H

/* Mux support */
#define FEATURE_MODEM_CFG_MUX_SUPPORT

/* Debugging support */
//#define MCFG_DEBUG_EFS_LOG_SUPPORT
//#define FEATURE_MCFG_ACTIVATE_W_NO_RESET

/* For testing auth support */
// #define FEATURE_QDST_SECBOOT 

/* Disable default config when NV is not set */
// #define FEATURE_MCFG_DISABLE_DEFAULT_CONFIG_W_NV_INACTIVE

/* For remote storage support */
//#define FEATURE_MCFG_REMOTE_STORAGE 

/* For Multi-SIM support */
#define FEATURE_MCFG_MULTISIM_SUPPORT
#define MCFG_MODEM_INITITIATED_GRACEFUL_RESET_SUPPORT_W_REASON 
#endif /* MODEM_CFG_FEATURE_CONFIG_H */
