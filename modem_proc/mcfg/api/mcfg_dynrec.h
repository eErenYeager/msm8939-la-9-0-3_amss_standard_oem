/**
  @file mcfg_dynrec.h

  @brief  Definitions used in Dynamically Reclaiming.
*/
/*
  Copyright (c) 2014 Qualcomm Technologies Incorporated.
          All Rights Reserved.
    Qualcomm Confidential and Proprietary
*/

/*===========================================================================
                      EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/api/mcfg_dynrec.h#1 $
  $DateTime: 2016/03/31 23:27:01 $


  when        who     what, where, why
  ---------   ---     ---------------------------------------------------------------------------
 2014/08/19   hzhi    Created for Dynamic Reclaiming.
===========================================================================*/
#ifndef MCFG_DYNREC_H
#define MCFG_DYNREC_H

extern int mcfg_dynrec_load(char* list[], int numItems);
extern int mcfg_dynrec_reclaim(int* numused);
extern int mcfg_dynrec_init(void);
extern int mcfg_data_reclaim();

#endif /*MCFG_DYNREC_H*/