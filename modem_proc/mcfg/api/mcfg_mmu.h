/**
  @file mcfg_mmu.h

  @brief  Definitions used in mcfg_mmu API.
*/
/*
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
          All Rights Reserved.
    Qualcomm Confidential and Proprietary
*/

/*===========================================================================
                      EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/api/mcfg_mmu.h#3 $
  $DateTime: 2016/03/31 23:27:01 $


  when        who     what, where, why
  ---------   ---     ---------------------------------------------------------------------------
  07/25/14    hzhi    Updated for CR 699655: optimize MMU_table lookup API to fix MPPS performance hit.
  06/10/14    hzhi    Created for CR 670990: removing unnecessary dependency of mmu update on segment loading feature.

===========================================================================*/

#ifndef MCFG_MMU_H
#define MCFG_MMU_H

#include "ref_cnt_obj.h"
#include "comdef.h"
#include "IxErrno.h"

#include "qurt_memory.h"

/*------------------------------------------------------------------*
 * symbols defined to represent main memory region VA and PA start. * 
 *------------------------------------------------------------------*/
extern unsigned int image_pstart __attribute__((section(".data")));
extern unsigned int image_vstart __attribute__((section(".data")));

/*------------------------------------------------------------------*
 * local data types to manage mcfg MMU table to translate        *
 * heap section VA to PA upon query.                                *
 *------------------------------------------------------------------*/
#define MCFG_MMU_ENTRY_MAX_NUMBER 32
#define MCFG_MMU_VA_STEP_SIZE (256*1024)

/*-----------------------------------------------------------------------------
  Definition of return types for mcfg_mmu_update functions. 
-----------------------------------------------------------------------------*/
typedef enum
{
  MCFG_MMU_UPDATE_SUCCESS           = 0,
  MCFG_MMU_UPDATE_TRANSLATION_FAIL  = 1,
  MCFG_MMU_UPDATE_ENTRY_OVERFLOW_FAIL = 2,
  MCFG_MMU_UPDATE_MALLOC_FAIL = 3
}mcfg_mmu_update_status_t;

/*------------------------------------------------------------------*
 * mcfg_MMU_table entry data type.                               *
 *------------------------------------------------------------------*/
typedef struct
{
  qurt_addr_t va_start;
  qurt_addr_t va_end;
  qurt_paddr_t pa_start;
  qurt_paddr_t pa_end;
  unsigned int offset;
}mcfg_MMU_entry_t, *mcfg_MMU_entry_ptr_t;

/*------------------------------------------------------------------*
 * MMU table to manage address translation translate for heap       *
 * sections added from reclaimed memory region.                     *
 *------------------------------------------------------------------*/
extern mcfg_MMU_entry_t mcfg_MMU_table[MCFG_MMU_ENTRY_MAX_NUMBER];

/*------------------------------------------------------------------*
 * mcfg_mmu_table_last_entry points to last non-zero entry in MMU.  *
 *------------------------------------------------------------------*/
extern mcfg_MMU_entry_t *mcfg_mmu_table_last_entry;

/*===========================================================================

FUNCTION mcfg_VA_to_PA

DESCRIPTION
  This function returns accurate Physical Address of given Virtual Address 
  for heap sections added from reclaimed memory region. 
  *Usage is limited to address fails into heap and modem_mem region.*
  *Not including device I/O, smem, EFS simulation region.*

DEPENDENCIES
  None. 

RETURN VALUE
  Physical Address of input Virtual Address. 

SIDE EFFECTS
  None.

===========================================================================*/
static __inline qurt_paddr_t mcfg_VA_to_PA(qurt_addr_t va)
{
#ifndef T_WINNT
  mcfg_MMU_entry_ptr_t ptr = mcfg_mmu_table_last_entry;

  while(va < ptr->va_start || va > ptr->va_end)
  {
    ptr--;
  }
  return (qurt_paddr_t)(ptr->offset + va);
#else /*T_WINNT*/
  #error code not present
#endif /*T_WINNT*/
}


/*===========================================================================

FUNCTION mcfg_PA_to_VA

DESCRIPTION
  This function returns accurate Virtual Address of given Physical Address 
  for heap sections added from reclaimed memory region. 
  *Usage is limited to address fails into heap and modem_mem region.*
  *Not including device I/O, smem, EFS simulation region.*

DEPENDENCIES
  None. 

RETURN VALUE
  Virtual Address of input Physical Address. 

SIDE EFFECTS
  None.

===========================================================================*/
static __inline qurt_addr_t mcfg_PA_to_VA(qurt_paddr_t pa)
{
#ifndef T_WINNT
  mcfg_MMU_entry_ptr_t ptr = mcfg_mmu_table_last_entry;

  while(pa < ptr->pa_start || pa > ptr->pa_end)
  {
    ptr--;
  }
  return (qurt_addr_t)(pa - ptr->offset);
#else /*T_WINNT*/
  #error code not present
#endif /*T_WINNT*/
}


#endif /*MCFG_MMU_H*/