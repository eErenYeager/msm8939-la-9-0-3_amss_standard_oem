#ifndef MCFG_UTILS_I_H
#define MCFG_UTILS_I_H
/*==============================================================================

            M O D E M   C O N F I G   U T I L S   I N T E R N A L
 
GENERAL DESCRIPTION
  Internal header MCFG Utility library

Copyright (c) 2012-2013 by QUALCOMM Technologies Incorporated.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_utils/src/mcfg_utils_i.h#3 $ 
$DateTime: 2016/03/31 23:27:01 $ 
$Author: ravikira $ 
$Change: 10184892 $ 

when     who  what, where, why
-------- ---  ----------------------------------------------------------------
09/07/12 bd   Move condition check macros to mcfg_int.h
05/08/12 cy   Initial creation

==============================================================================*/

#include "mcfg_int.h"
#include "mcfg_common.h"
#include "mcfg_utils.h"

/*==============================================================================

                PRIVATE DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/
/*==============================================================================
  Constants and Macros
==============================================================================*/
/* The following defines are file offsets based on the trailer spec.
   Note: (GCI = Get Config Info) */
#define MCFG_GCI_PREFIX_TYPE_OFFSET     4
#define MCFG_GCI_TRAILER_TYPE_OFFSET    8
#define MCFG_GCI_TRAILER_LENGTH_OFFSET  10
#define MCFG_GCI_TRAILER_DESC_OFFSET    12
#define MCFG_GCI_MAGIC_PREFIX_TYPE      0xA
#define MCFG_GCI_MAGIC_TRAILER_TYPE     0xA1

/*==============================================================================
  Typedefs
==============================================================================*/

/*==============================================================================
  Variables
==============================================================================*/


/*============================================================================== 
 
                    PUBLIC FUNCTION DECLARATIONS FOR MODULE
 
==============================================================================*/
boolean mcfg_utils_parse_config_info
(
  mcfg_config_s_type      *config,
  mcfg_config_info_s_type *info
);

#endif /* MCFG_UTILS_I_H */

