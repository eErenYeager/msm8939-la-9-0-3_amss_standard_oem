#ifndef MCFG_UIM_H
#define MCFG_UIM_H
/*==============================================================================

                M O D E M   C O N F I G   U I M

GENERAL DESCRIPTION
  Module supporting accessing the UIM for the purposes of automatically
  selecting an associated configuration. This module is confined to reading the
  identifying parameters from the UIM - the actual configuration selection is
  done in mcfg_sel.

Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.DPM.1.0.1.c1/Main/modem_proc/mcfg/mcfg_sel/inc/mcfg_uim.h#3 $
$DateTime: 2016/03/31 23:27:01 $
$Author: ravikira $
$Change: 10184892 $

when     who  what, where, why
-------- ---  ----------------------------------------------------------------
09/07/12 bd   Created module

==============================================================================*/

#include "mcfg_task.h"

/*==============================================================================

                PUBLIC DEFINITIONS AND DECLARATIONS FOR MODULE

==============================================================================*/
/*==============================================================================
  Constants and Macros
==============================================================================*/

/* Length of EF-ICCID in bytes, per ETSI TS 102 221 (when encoded in BCD) */
#define MCFG_UIM_ICCID_LEN (10)

/* Minimum size, in bytes, of a buffer to fit the ICCID encoded as an ASCII
   string (includes space for null termination) */
#define MCFG_UIM_ICCID_STR_BUF_LEN (MCFG_UIM_ICCID_LEN * 2 + 1)

/* Minimum length of a valid ICCID string, in characters */
#define MCFG_UIM_ICCID_STR_MIN_LEN (19)

/*==============================================================================
  Typedefs
==============================================================================*/


/*==============================================================================
  Variables
==============================================================================*/


/*==============================================================================

                    PUBLIC FUNCTION DECLARATIONS FOR MODULE

==============================================================================*/
/*===========================================================================

  FUNCTION mcfg_uim_autoselect_enabled

  DESCRIPTION
    Checks whether UIM-based automatic MCFG_SW selection is enabled.

  DEPENDENCIES
    None

  PARAMETERS
    slot_index

  RETURN VALUE
    TRUE if enabled for specified slot, FALSE otherwise
    TRUE if enabled for any slot if slot_index is provided invalid

  SIDE EFFECTS
    None

===========================================================================*/
boolean mcfg_uim_autoselect_enabled
(
  mcfg_slot_index_type_e_type slot_index
);

/*===========================================================================

  FUNCTION mcfg_uim_cmd_proc

  DESCRIPTION
    Processes a mcfg_uim command in the context of the MCFG task.

  DEPENDENCIES
    None

  PARAMETERS
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None

===========================================================================*/
void mcfg_uim_cmd_proc
(
  mcfg_task_cmd_s_type *cmd
);

/*===========================================================================

  FUNCTION mcfg_uim_mmgsdi_init

  DESCRIPTION
    Starts the MCFG_UIM MMGSDI initialization procedure by requesting a
    client ID and registering for card events.

  DEPENDENCIES
    None

  PARAMETERS
    None

  RETURN VALUE
    None

  SIDE EFFECTS
    None

===========================================================================*/
void mcfg_uim_mmgsdi_init
(
  void
);

/*===========================================================================

  FUNCTION mcfg_uim_map_sub_id_to_slot_index

  DESCRIPTION

  DEPENDENCIES
    None

  PARAMETERS
    sub index

  RETURN VALUE
    slot index

  SIDE EFFECTS
    None

===========================================================================*/
mcfg_slot_index_type_e_type mcfg_uim_map_sub_id_to_slot_index
(
  mcfg_sub_id_type_e_type sub_id
);

#endif /* MCFG_UIM_H */
