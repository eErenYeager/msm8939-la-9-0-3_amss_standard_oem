#ifndef __APR_MISC_H__
#define __APR_MISC_H__

/*
  Copyright (C) 2009 Qualcomm Technologies Incorporated.
  All rights reserved.
  QUALCOMM Proprietary/GTDR.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/apr/osal/inc/apr_misc.h#1 $
  $Author: mplp4svc $
*/

#include "apr_comdef.h"

APR_INTERNAL int32_t apr_misc_sleep ( uint64_t time_ns );

#endif /* __APR_MISC_H__ */

