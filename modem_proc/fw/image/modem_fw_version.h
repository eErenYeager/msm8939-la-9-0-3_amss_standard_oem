/*!
  @file
  modem_fw_version.h

  @brief
  Modem firmware image version.

  @detail
  This is available from the FWS shared memory region.

*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/image/modem_fw_version.h#86 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

#ifndef MODEM_FW_VERSION_H
#define MODEM_FW_VERSION_H


/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#define MODEM_FW_IMAGE_VERSION      "MODEM_FW_MPSS_DPM_2_0_2_C1.10.03.240.48"


#endif /* MODEM_FW_VERSION_H */


