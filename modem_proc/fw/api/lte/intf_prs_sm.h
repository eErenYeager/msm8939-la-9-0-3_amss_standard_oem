/*!
  @file
  intf_prs_sm.h

  @brief
  REQUIRED brief one-sentence description of this C header file.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/09/13   cj      Adding COM adjustment value to PRS smem interface. 
04/17/12   cj      Add PRS S/(S+N) data to sharedmem interface.
11/01/11   cj      Add params for PRS start index and prune window to per-cell 
                   structure in shared mem.  
05/02/11   cj      Added PRS message router interface.
===========================================================================*/

#ifndef INTF_PRS_SM_H
#define INTF_PRS_SM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "intf_common.h"
#include "intf_prs.h"
#include "lte_LL1_log_prs.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief cell-specific PRS Meas descriptor.
*/
typedef struct
{
  /*! Physical Cell ID corresponding to the CER vector. 
      Range: 0~503
  */
  uint16  cer_cell_id;  
    
  /*! slot Offset */
  uint8 slot_offset;

  /*! Symbol Tap Offset */
  int8 symbol_offset;

  /*! Start Tap Index in Ts [0-2048] */
  uint16 start_idx; 

  /*! PRS Search Window Prune Size in Ts [0-2048]*/
  uint16 prune_size; 

  /*! Symbol Bitmap corresponding to CER */
  uint16 symbol_bitmap[LTE_LL1_LOG_PRS_NUMBER_OF_SUBFRAMES];

  /*! S/(S+N) data in Q31 format for Rx 0 */
  uint32 avg_s_sn_rx0;

  /*! S/(S+N) data in Q31 format for Rx 1 */
  uint32 avg_s_sn_rx1;
    
  /*! the number of the prs subframe per occasion */
  lte_LL1_prs_num_subframe_e n_prs;

  /*! Cell Center frequency */
  lte_earfcn_t    earfcn;
     
} lte_LL1_prs_cell_desc_s;

/*! @brief occasion-specific PRS Meas descriptor.
    This descriptor will be use for both messaging
    ML1, and to write to SharedMem.
*/
typedef struct
{
  /*! TimeStamp aligned to frame boundary of PRS Occasion,
      time read from 32-bit VSRC sample counter. 
      (alternatively 64-bit DL Link counter)
  */
  int64  timestamp; 

  /*! Accumulated TTL Adjustment */
  int16    ttl_adj;

  /*! PETL (Projected Energy Tracking Loop) Adjustment, in Ts units, Q10 format */
  int32    petl_adj_q10; 

  /*COM (Center of Mass) adj in Q6. Reintroduced after PETL to break dependency on GPS SW*/
  int32    com_adj;

  /*! Accumulated MSTMR adjustment, in Ts units, Q0 format. */
  int64    mstmr_adj;

  /*! cell ID of the serving sector in this PRS occasion */
  uint16  serv_cell_id; 
      
  /*! PRS Meas Result Sequence Number. This will be used 
    by GNSS SW to check if the data they read have been
    overwritten or not.
  */
  uint32  occasion_seq_num;

  /*! the number of the prs cell data in this prs occasion */
  uint8  num_cell; 
  
  /*! Header containing relevant info for PRS measurement vector */
  lte_LL1_prs_cell_desc_s cell_info[LTE_LL1_PRS_NUM_CELL_MAX]; 
  
} lte_LL1_prs_occasion_desc_s;

/*! @brief cell-specific PRS Meas Data 
*/
typedef struct
{
  /*! Vector containing PRS measurement of a single cell, 
    consisting of 2048 array of 12-bit data.
  */
  uint32 cer_vector[LTE_LL1_PRS_CELL_SIZE_MAX]; 
  
} lte_LL1_prs_cell_data_s;

/*! @brief PRS Meas descriptor and data for the 
    whole PRS occasion. This will be written to
    SharedMem.
*/
typedef struct
{
  
  /*! Individual Cell Data, up to 3 cells */
  uint32 cell_data[LTE_LL1_PRS_NUM_CELL_MAX][LTE_LL1_PRS_CELL_SIZE_MAX]; 

  /*! PRS Occasion Meas Descriptor */
  lte_LL1_prs_occasion_desc_s occasion_info;

} lte_LL1_prs_occasion_meas_s;


/*===========================================================================

                   OPCRS DEFINITIONS AND TYPES

===========================================================================*/
#define LTE_LL1_OPCRS_NUM_MEAS_SRV_CELL_MAX 3
#define LTE_LL1_OPCRS_NUM_MEAS_NBR_CELL_MAX 3

/*! @brief OpCRS Serving Cell measurement type.
*/
typedef struct
{
  uint16 seq_num;                      ///< sequence number.
  uint32 serving_cell_id;              ///< Range: 0...503
  uint32 sub_frame_number;             ///< Sub-frame number (Range 0..9)
  uint32 system_frame_number;          ///< System frame number (range 0..1023)
  uint32 cer_size;                   
  uint32 rx_antenna_number;            ///< Encoded as:
                                       ///<   0x0: antenna 0
                                       ///<   0x1: antenna 1
  uint32 energy_type;                  ///< Encoded as:
                                       ///<   0x0: signal energy
                                       ///<   0x1: signal + noise energy
  uint8  carrier_index;                ///< 0-PCC, 1-SCC  
  uint32 scell_num_records_cer_size;   ///< Range: 1..512
  int32  ttl_adj;
  int32  petl_adj_q10;
  int64  mstmr_adj;
  int32  bandwidth;
  int64  dl_subframe_ref_time;         ///< ref time at start of DL subframe
  int64  dl_subfn_ref_time_raw;        ///< ref time without mstmr correction
  int64  dl_subframe_ref_time_offline; ///< ref time at start of DL subframe until
                                       ///< sample recording is ON = dl_subframe_ref_time
                                       ///< Once sample recording is frozen, 
                                       ///< dl_subframe_ref_time_offline is also frozen.
  uint32 dl_subframe_cnt;              ///< subframe counter
  int32  dl_subframe_mstmr;            ///< O_STMR MSTMR count at start of DL subframe
  uint32 dl_sf_univ_stmr_cnt;          ///< UNIV_STMR MSTMR count at start of DL subframe
  int32  dl_subframe_sr_addr;          ///< SR addr at start of DL subframe
  int32  ul_subframe_mstmr;            ///< O_STMR MSTMR count at start of UL subframe
  uint32 ul_sf_univ_stmr_cnt;          ///< UNIV_STMR MSTMR count at start of UL subframe

  uint16 cer_per_rx_tx_pair[512];      ///< CER vector of maximum 512 taps. (#taps depend on LTE DL BW)
} lte_LL1_opcrs_srv_meas_s;

/*! @brief neighbor cell cer struct
 */
typedef struct
{
  uint16 per_tap_cer_for_rx0;         ///< Unitless energy metric. Range: 0-65535
  uint16 per_tap_cer_for_rx1;         ///< Unitless energy metric. Range: 0-65535
}lte_LL1_opcrs_ncell_cer_tap_s;

/*! @brief OpCRS Neighbor Cell measurement type.
*/
typedef struct
{
  uint16 seq_num;
  uint32 neighbor_cell_id;            ///< Range: 0...503
  uint32 sub_frame_number;            ///< Sub-frame number (Range 0..9)
  uint32 system_frame_number;         ///< System frame number (range 0..1023)
  uint32 number_of_tx_antennas_m;     ///< Encoded as:
                                      ///<   0x0: 1 antenna
                                      ///<   0x1: 2 antennas
                                      ///<   0x2: 4 antennas
  uint32 number_of_rx_antennas_n;     ///< Encoded as:
                                      ///<   0x0: 1 antenna
                                      ///<   0x1: 2 antennas 
  int64  scell_dl_subframe_ref_time;  ///< ref time at start of DL subframe
  int64  scell_dl_subfn_ref_time_raw; ///< ref time without mstmr correction
  int64  scell_dl_subframe_ref_time_offline; ///< ref time at start of DL subframe until
                                             ///< sample recording is ON = dl_subframe_ref_time
                                             ///< Once sample recording is frozen, 
                                             ///< dl_subframe_ref_time_offline is also frozen.
  int64  scell_dl_subframe_ext_mstmr; ///< unwrapped MSTMR count at DL subframe
  uint32 scell_dl_subframe_cnt;       ///< subframe counter
  int32  scell_dl_subframe_mstmr;     ///< O_STMR MSTMR count at start of DL subframe
  uint32 scell_dl_sf_univ_stmr_cnt;   ///< UNIV_STMR MSTMR count at start of DL subframe
  int32  scell_dl_subframe_sr_addr;   ///< SR addr at start of DL subframe
  int32  scell_ul_subframe_mstmr;     ///< O_STMR MSTMR count at start of UL subframe
  uint32 scell_ul_sf_univ_stmr_cnt;   ///< UNIV_STMR MSTMR count at start of UL subframe
  int16  scell_subframe_num;          ///< subframe number 
  int16  scell_frame_num;             ///< radio frame number
  int16  window_start_offset;
  uint32 frame_bndry_ref_time_rx0;
  uint32 frame_bndry_ref_time_rx1;
  int16  total_timing_adj_cir_rx0;    ///<  Total timing adjustment in CIR domain per Rx antenna to drive TTL
  int16  total_timing_adj_cir_rx1;    ///<  Total timing adjustment in CIR domain per Rx antenna to drive TTL
  int16  ttl_adj;
  int32  petl_adj_q10;
  int64  mstmr_adj;
             
  lte_LL1_opcrs_ncell_cer_tap_s ncell_cer[32];///< CER buffer storing rx0 and rx1 pairs. 
  uint32 ncell_taps_sum_num_records_m_x_n ;   ///< Range: 1-8 
  uint16 ncell_cer_sum_records_m_x_n[8];      /// [0] = per rx cer exponent for rx0
                                              /// [1] = per rx cer exponent for rx1
                                              /// [2] = High 16 bits of Rx0 raw EE value
                                              /// [3] = Low 16 bits of Rx0 raw EE value
                                              /// [4] = High 16 bits of Q24 Rx0 LNA adjustment (dB)
                                              /// [5] = High 16 bits of Rx1 raw EE value
                                              /// [6] = Low 16 bits of Rx1 raw EE value
                                              /// [7] = High 16 bits of Q24 Rx1 LNA adjustment (dB)

} lte_LL1_opcrs_nbr_meas_s;

/*! @brief Shared memory buffer to store Opportunistic CRS measurements.
*/
typedef struct
{
  /*! Current array index for OpCRS serving cell measurement */
  uint8 srv_cell_idx;

  /*! Current array index for OpCRS neighbor cell measurement */
  uint8 nbr_cell_idx;

  /*! Array of OpCRS serving cell measurement */
  lte_LL1_opcrs_srv_meas_s srv_meas[LTE_LL1_OPCRS_NUM_MEAS_SRV_CELL_MAX];

  /*! Array of OpCRS neighbor cell measurement */
  lte_LL1_opcrs_nbr_meas_s nbr_meas[LTE_LL1_OPCRS_NUM_MEAS_NBR_CELL_MAX];
 
} lte_LL1_opcrs_meas_db_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/


#endif /* INTF_PRS_SM_H */
