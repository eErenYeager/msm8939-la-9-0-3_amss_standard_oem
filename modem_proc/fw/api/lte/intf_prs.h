/*!
  @file
  intf_prs.h

  @brief
  File containing structures for PRS FW module interface.

  @detail

*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/02/11   cj      Added PRS message router interface.
===========================================================================*/

#ifndef INTF_PRS_H
#define INTF_PRS_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "intf_common.h"
#include "intf_dl_msg.h"
#include "lte_LL1_log_prs.h"

/*===========================================================================

                                 MACROS

===========================================================================*/
#define LTE_LL1_PRS_NUM_CELL_MAX               4
#define LTE_LL1_PRS_NUM_SF_MAX                 6
/* PRS CER results will be 2048 of 12UFL packed to 32S= 2048*12/32 = 768 */
#define LTE_LL1_PRS_CELL_SIZE_MAX           2048

/*===========================================================================

                      EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief Response Types for PRS Config and Meas Request.
*/
typedef enum
{
  /*! SUCCESS */
  LTE_LL1_PRS_RSP_SUCCESS = 0,   

  /*! KERNEL Load/Reload Fail */
  LTE_LL1_PRS_RSP_KERNEL_FAIL,

  /*! Invalid Parameters */
  LTE_LL1_PRS_RSP_INVALID_PARAMS,

  /*! Aborted */
  LTE_LL1_PRS_RSP_ABORT,

  /*! Unknown Error */
  LTE_LL1_PRS_RSP_UNKNOWN_ERROR,

  /*! MBSFN Collision */
  LTE_LL1_PRS_RSP_COLLISION_MBSFN,

  /*! Max enum for PRS Response Type */
  LTE_LL1_PRS_RSP_MAX     
} lte_LL1_prs_rsp_e;

/*! @brief Enum describing LTE PRS Port Configuration
*/
typedef enum
{
  /*! 1 or 2 PBCH antenna ports, Normal CP */
  LTE_LL1_PRS_PORT_1_2_PBCH = 0,   
  
  /*! 4 PBCH antenna ports, Normal CP */
  LTE_LL1_PRS_PORT_4_PBCH,   

  /*! Max enum value for PRS Port Map config. */
  LTE_LL1_PRS_PORT_MAX

} lte_LL1_prs_port_map_e;

/*! @brief Enum describing the LTE PRS Cycle Prefix configuraiton 
*/
typedef enum
{
  /*! Normal CP */
  LTE_LL1_PRS_NORMAL_CP = 0,

  /*! Extended CP */
  LTE_LL1_PRS_EXTENDED_CP,

  /*! MAX enum limit */
  LTE_LL1_PRS_CP_MAX

} lte_LL1_prs_cyclic_prefix_e;

/*! @brief REQUIRED one-sentence brief description of this enumeration typedef
*/
typedef enum
{
  /*!< 1 PRS Subframe in PRS Occasion */
  LTE_LL1_PRS_1_SF = 0,   

  /*!< 2 PRS Subframe in PRS Occasion */
  LTE_LL1_PRS_2_SF,   

  /*!< 4 PRS Subframe in PRS Occasion */
  LTE_LL1_PRS_4_SF,   

  /*!< 6 PRS Subframe in PRS Occasion */
  LTE_LL1_PRS_6_SF,   

  /*!< Maximum support to 6 Tx Antenna */
  LTE_LL1_PRS_TX_MAX = LTE_LL1_PRS_6_SF

} lte_LL1_prs_num_subframe_e;

/*! @brief REQUIRED one-sentence brief description of this enumeration typedef
*/
typedef enum
{
  /*!< OPCRS START */
  LTE_LL1_OPCRS_START = 0,   

  /*!< OPCRS STOP */
  LTE_LL1_OPCRS_STOP = 1,   

  /*!< MAX */
  LTE_LL1_OPCRS_MAX = LTE_LL1_OPCRS_STOP

} lte_LL1_opcrs_cmd_e;

/*! @brief Response Types for PRS Config and Meas Request.
*/
typedef enum
{
  /*! SUCCESS */
  LTE_LL1_OPCRS_CNF_SUCCESS = 0,   

  /*! KERNEL Load/Reload Fail */
  LTE_LL1_OPCRS_CNF_ERROR,   

  /*! Max */
  LTE_LL1_OPCRS_CNF_MAX     

} lte_LL1_opcrs_cnf_e;

typedef enum
{
  /*! SUCCESS */
  LTE_LL1_OPCRS_IND_SUCCESS = 0,   

  /*! KERNEL Load/Reload Fail */
  LTE_LL1_OPCRS_IND_ERROR,   

  /*! Max */
  LTE_LL1_OPCRS_IND_MAX     

} lte_LL1_opcrs_ind_e;

typedef enum
{
  LTE_LL1_OPCRS_CELLTYPE_SRV = 0,   
  LTE_LL1_OPCRS_CELLTYPE_NBR,
  LTE_LL1_OPCRS_CELLTYPE_MAX = LTE_LL1_OPCRS_CELLTYPE_NBR,   

} lte_LL1_opcrs_cell_type_e;


/*===========================================================================
                                  PRS_CONFIG
===========================================================================*/
/*! @brief PRS Config Info from ML1.
*/
typedef struct
{
  /*!  common req header */
  lte_LL1_req_header_struct req_hdr;

  /*! PRS BW: 1.4, 3, 5, 10, 15, 20 MHz */
  lte_bandwidth_enum bandwidth; 
    
} lte_LL1_dl_prs_config_req_struct;

// LTE_LL1_INTF_MSG( dl_prs_config_req );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_config_req_struct       msg_payload;
} lte_LL1_dl_prs_config_req_msg_struct;



/*! @brief PRS Config CNF to ML1.
*/
typedef struct
{
  /*!  common cnf header */
  lte_LL1_cnf_header_struct cnf_hdr;

  /*! PRS Config Status */
  lte_LL1_prs_rsp_e status;

  /*! Result is interfered by other tech */
  boolean  interf_active;
} lte_LL1_dl_prs_config_cnf_struct;

//LTE_LL1_INTF_MSG( dl_prs_config_cnf );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_config_cnf_struct       msg_payload;
} lte_LL1_dl_prs_config_cnf_msg_struct;

/*===========================================================================
                                  PRS_ABORT
===========================================================================*/

/*! @brief PRS Abort Request from ML1.
*/
typedef struct
{
  /*!  common req header */
  lte_LL1_req_header_struct req_hdr;
  
} lte_LL1_dl_prs_abort_req_struct;

//LTE_LL1_INTF_MSG( dl_prs_abort_req );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_abort_req_struct       msg_payload;
} lte_LL1_dl_prs_abort_req_msg_struct;

/*! @brief PRS Config CNF to ML1.
*/
typedef struct
{
  /*!  common cnf header */
  lte_LL1_cnf_header_struct cnf_hdr;

  /*! PRS Config Status */
  lte_LL1_prs_rsp_e status;

} lte_LL1_dl_prs_abort_cnf_struct;

// LTE_LL1_INTF_MSG( dl_prs_abort_cnf );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_abort_cnf_struct       msg_payload;
} lte_LL1_dl_prs_abort_cnf_msg_struct;

/*===========================================================================
                             PRS_MEAS: ML1-PRS FW
===========================================================================*/

/*! @brief PRS Cell Specific Info structure from ML1.
*/
typedef struct
{
  /*! physical cell ID, necessary for RS Tone split. */
  uint16 cell_id;  
  
  /*! PRS BW. One of 1.4, 3, 5, 10, 15, 20 MHz. 
      Needed for (prs BW requeted < prs BW loaded on uKernel) case,
      where FW should zero out the outer bands.
  */
  lte_bandwidth_enum bandwidth; 
  
  /*! 1 or 2 pbch ants, 4 pbch ports; do we need consider
      other mapping for the multiple timing hypos
  */
  lte_LL1_prs_port_map_e port_map; 
   
  /*! Start Tap Index in Ts [0-2047] */
  uint16 start_idx; 
   
  /*! Prune Size in Ts [0-2047] */  
  uint16 prune_size; 
     
  /*! slot offset */
  uint8    slot_offset;

  /*! symbol delay offset. [-4, 4] */
  int8 symbol_offset; 

  /*! the number of the prs subframe per occasion */
  lte_LL1_prs_num_subframe_e n_prs; 
       
  /*! Cell Center frequency */
  lte_earfcn_t    earfcn;
       
} lte_LL1_prs_cell_info_s;

/*! @brief PRS occasion info
*/
typedef struct
{
  /*!  common req header */
  lte_LL1_req_header_struct req_hdr;

  /*! next starting subframe number (SFN*10+subf). 
      This field is for sanity check only.
      Range: [0-10239]
  */
  uint16 frame_subframe; 
    
  /*! number of cells to be measured. 
      Range: [1-3]
  */
  uint8 num_cells;

  /*! carrier */
  lte_LL1_carrier_type_enum  carrier;

  /*! the OTDOA cells to measure */    
  lte_LL1_prs_cell_info_s cell_info[LTE_LL1_PRS_NUM_CELL_MAX]; 
    
} lte_LL1_dl_prs_meas_req_struct;

//LTE_LL1_INTF_MSG( dl_prs_meas_req );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_meas_req_struct       msg_payload;
} lte_LL1_dl_prs_meas_req_msg_struct;

/*! @brief Cnf msg sent to ML1 when PRS occasion processing is done.
*/
typedef struct
{
  /*!  common cnf header */
  lte_LL1_cnf_header_struct cnf_hdr;

  /*! Status of PRS Occasion processing. */
  lte_LL1_prs_rsp_e occasion_status;  

  /*! PRS Occasion Sequence Number, to be used 
      by Location SW for data validity check.
  */
  uint32 occasion_seq_num;  
  /*! Result is interfered by other tech */
  boolean  interf_active;
} lte_LL1_dl_prs_meas_cnf_struct;

//LTE_LL1_INTF_MSG( dl_prs_meas_cnf );
typedef struct {                                      
  msgr_hdr_struct_type                   msg_hdr;
  lte_LL1_dl_prs_meas_cnf_struct       msg_payload;
} lte_LL1_dl_prs_meas_cnf_msg_struct;

/* For reference 
 
typedef struct {                                      
  msgr_hdr_struct_type                       msg_hdr;
  lte_LL1_dl_opcrs_meas_req_struct       msg_payload;
} lte_LL1_dl_opcrs_meas_req_msg_struct;
 
typedef struct {                                      
  msgr_hdr_struct_type                       msg_hdr;
  lte_LL1_dl_opcrs_meas_cnf_struct             msg_payload;
} lte_LL1_dl_opcrs_meas_cnf_msg_struct; 
 
typedef struct {                                      
  msgr_hdr_struct_type                       msg_hdr;
  lte_LL1_dl_opcrs_meas_ind_struct             msg_payload;
} lte_LL1_dl_opcrs_meas_ind_msg_struct;
 
*/ 

typedef struct
{
  /*!  common req header */
  lte_LL1_req_header_struct req_hdr;

  lte_LL1_opcrs_cmd_e cmd;
    
} lte_LL1_dl_opcrs_meas_req_struct;

typedef struct
{
  /*!  common cnf header */
  lte_LL1_cnf_header_struct cnf_hdr;

  /*! Status of PRS Occasion processing. */
  lte_LL1_opcrs_cnf_e occasion_status;  

} lte_LL1_dl_opcrs_meas_cnf_struct;

typedef struct
{
  /*!  common ind header */
  lte_LL1_ind_header_struct ind_hdr;

  lte_LL1_opcrs_ind_e occasion_status;
  lte_LL1_opcrs_cell_type_e cell_type; 
  uint32 opcrs_seq_num;
  uint16 phy_cell_id;
  void* smem_data_ptr;

} lte_LL1_dl_opcrs_meas_ind_struct;

/* OPCRS Messages */ 
LTE_LL1_INTF_MSG( dl_opcrs_meas_req );
LTE_LL1_INTF_MSG( dl_opcrs_meas_cnf );
LTE_LL1_INTF_MSG( dl_opcrs_meas_ind );

#endif /* INTF_PRS_H */
