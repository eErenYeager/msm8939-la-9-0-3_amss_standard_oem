#===============================================================================
#
# Modem firmware API build script
#
# Copyright (c) 2011 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/api/build/fw_api.scons#1 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 10/16/12   sv      Clean-up as part of removing modem folder
# 03/31/11   rjg     Initial version
#===============================================================================

from glob import glob
from os.path import join, basename

# --------------------------------------------------------------------------- #
# Import SCons Environment                                                    #
# --------------------------------------------------------------------------- #
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Necessary Public & Restricted API's
#-------------------------------------------------------------------------------
env.RequirePublicApi([
    'DAL',
    'DEBUGTOOLS',
    'MPROC',
    'SERVICES',
    'SYSTEMDRIVERS',
    'MEMORY',
    'KERNEL',          # needs to be last
    ], area='core')

# Need to get access to Modem Restricted headers
env.RequireRestrictedApi(['FW', 'MCS', 'LTE'])

# Need to get access to Modem Public headers
env.RequirePublicApi(['FW'],  area='FW')
env.RequirePublicApi(['MCS'], area='MCS')
env.RequirePublicApi(['LTE'], area='LTE')
env.RequirePublicApi(['RF'],  area='FW')
env.RequirePublicApi(['COMMON'],  area='FW')

# Autodetect UMID files
for dirname in glob(join(env.subst('${FW_ROOT}/api/'), '*')):
  files = [ ]
  for fname in glob(join(dirname, '*_msg.h')):
    files.append('${FW_ROOT}/api/'+basename(dirname)+'/'+basename(fname))     

  if len(files) > 0:
    env.AddUMID('${BUILDPATH}/fw_'+basename(dirname)+'.umid', files)


