
#ifndef FW_RXLM_INTF_AG_H
#define FW_RXLM_INTF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Wed Jan 29 14:27:01 2014
Generated using: lm_autogen.pl
Generated from:  v5.2.XX of DimePM_RxFE_Register_Settings.xlsx
*/

/*=============================================================================

           R X L M    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rxlm.

Copyright (c) 2009, 2010, 2011, 2012, 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2015/02/01 23:26:13 $
$Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/common/drivers/api/fw_rxlm_intf_ag.h#1 $

=============================================================================*/


/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 



#define FW_RXLM_SUB_REV_1 5
#define FW_RXLM_SUB_REV_2 2

#define FW_RXLM_STATIC_MASK 0x1 	/* Used to configure the set of registers that will be configured by SW just once; Rest may be updated dynamically from SW and needs to be written as independent blocks*/ 
#define FW_RXLM_COMMON_NOTCH_01_MASK 0x2 /* This Control Word triggers programming of the URxFE registers to program the common notches.*/ 
#define FW_RXLM_COMMON_NOTCH_02_MASK 0x4 /* This Control Word triggers programming of the URxFE registers to program the common notches.*/ 
#define FW_RXLM_IQMC_MASK 0x8 /* This Control Word triggers programming of all notch related registers in a WB chain*/ 
#define FW_RXLM_NOTCH_MASK 0x10 /* This Control Word triggers programming of all notch related registers in a WB chain*/ 
#define FW_RXLM_CSR_FREQ_OFFSET_NB0_MASK 0x20 /* This Control Word triggers programming of the URxFE registers to program the second NB chain's cordic rotator frequency offset in a NB group.*/ 
#define FW_RXLM_CSR_FREQ_OFFSET_NB1_MASK 0x40 /* This Control Word triggers programming of the URxFE registers to program the second NB chain's cordic rotator frequency offset in a NB group.*/ 
#define FW_RXLM_CSR_FREQ_OFFSET_NB2_MASK 0x80 /* This Control Word triggers programming of the URxFE registers to program the third NB chain's cordic rotator frequency offset in a NB group.*/ 

#define FW_RXLM_FULL_CFG_MASK ( FW_RXLM_STATIC_MASK | FW_RXLM_IQMC_MASK | FW_RXLM_NOTCH_MASK | FW_RXLM_CSR_FREQ_OFFSET_NB0_MASK | FW_RXLM_CSR_FREQ_OFFSET_NB1_MASK | FW_RXLM_CSR_FREQ_OFFSET_NB2_MASK )	/* This control word triggers programming all static and dynamic regsiters in the buffer */
#define FW_RXLM_GSM_MASK FW_RXLM_STATIC_MASK	/* This control word triggers programming the GSM set of RxFE registers */

#define FW_RXLM_NUM_CMNNOTCH_BLOCKS 2 /* Macro defining the number of blocks of common notches available for allocation */



/* This struct captures the autogenerated header, for Revision information, etc. */
typedef struct
{
  uint8 major_rev;  /* Indicates Product Line */ 
  uint8 fw_intf_rev;  /* Indicates FW RXLM Intf Rev */ 
  uint8 sw_settings_rev;  /* Indicates SW Settings Rev for the given FW Intf */ 
}fw_rxlm_header_struct;



/* This struct captures the different indices used to index into the HWIO registers. */
typedef struct
{
  uint8 rxfe_cn_idx_0;
  uint8 a_idx_0;
  uint8 wbw_idx_0;
  uint8 nbn_idx_0;
  uint8 nbn_idx_1;
  uint8 nbn_idx_2;
}fw_rxlm_indices_struct;



/* This struct captures flags that control whether certain groups of registers are programmed for a given state. */
typedef struct
{
  boolean rxfe_enable_flag;/* This flag enables programming of the RXFE ENABLE*/
  boolean mtc_trig_flag;/* This flag enables programming of the MTC registers to program the STMR trigger for URxFE WB chain 0*/
  boolean adc_cfg_flag;/* This flag enables programming of the Bolt registers to program ADC sampling clock*/
  boolean rxfe_top_flag;/* This flag enables programming of the ADC RXFE DBG mode registers and ACI filters*/
  boolean common_notch_01_flag;/* This flag enables programming of the URxFE registers to program the common notches.*/
  boolean common_notch_02_flag;/* This flag enables programming of the URxFE registers to program the common notches.*/
  boolean urxfe_wb_flag;/* This flag enables programming of the URxFE registers to program a WB chain.*/
  boolean urxfe_nb0_flag;/* This flag enables programming of the URxFE registers to program the first NB chain in a NB group.*/
  boolean urxfe_nb1_flag;/* This flag enables programming of the URxFE registers to program the second NB chain*/
  boolean urxfe_nb2_flag;/* This flag enables programming of the URxFE registers to program the third NB chain in a NB group.*/
}fw_rxlm_block_valid_flags_struct;



 /* Group CSR_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_clk_hz_c0;
  uint32 csr_clk_hz_c1;
  uint32 csr_clk_hz_c2;
  uint32 csr_ideal_val_c0;
  uint32 csr_ideal_val_c1;
  uint32 csr_ideal_val_c2;
  uint32 f_lo_inv_24u19;
}fw_rxlm_csr_xo_vars_group_struct;



 /* Group XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_res_inv_c0;
  uint32 csr_res_inv_c1;
  uint32 csr_res_inv_c2;
}fw_rxlm_xo_vars_group_struct;



 /* Group VSRC_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 vsrc_t1byt2_ideal_val_q7;
  uint32 vsrc_t1byt2_scale;
  uint32 vsrc_t2byt1m1_ideal_val_q32;
  uint32 vsrc_t2byt1_ideal_scale;
}fw_rxlm_vsrc_xo_vars_group_struct;



 /* Group STMR_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 stmr_fcw_ideal_val;
  uint32 stmr_fcw_scale;
}fw_rxlm_stmr_xo_vars_group_struct;



 /* Group DELAY_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 rxfe_delay_nb0_in_sctr;
  uint32 rxfe_delay_nb1_in_sctr;
  uint32 rxfe_delay_nb2_in_sctr;
  uint32 adc_vsrc_delay_in_off_clk;
}fw_rxlm_delay_vars_group_struct;



 /* Group DVGA_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  int32 dvga_offset_c0;
  int32 dvga_offset_c1;
  int32 dvga_offset_c2;
}fw_rxlm_dvga_vars_group_struct;



 /* Group VSRC_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 vsrc_output_rate;
}fw_rxlm_vsrc_vars_group_struct;



 /* Group ADC_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 adc_insertion_loss_delta;
}fw_rxlm_adc_vars_group_struct;



 /* Group CSR_PHASE_COMP_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_phase_offset[6];
}fw_rxlm_csr_phase_comp_vars_group_struct;



 /* Group NOTCH_THRESH: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  int32 wb_notch_filt_threshold[3];
  int32 cmn_notch_filt01_threshold[2];
  int32 cmn_notch_filt23_threshold[2];
}fw_rxlm_notch_thresh_group_struct;



 /* Group SCALE_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 wbdc_scale;
  uint32 wbpwr_scale;
  uint32 nbpwr0_scale;
  uint32 nbpwr1_scale;
  uint32 nbpwr2_scale;
}fw_rxlm_scale_vars_group_struct;



 /* Group RXFE_ENABLE: */ 
/* This block consist of the RXFE ENABLE*/
typedef struct
{
  uint32 rxfe_enable;
}fw_rxlm_rxfe_enable_group_struct;



 /* Group MTC_TRIG: */ 
/* This block consist of the MTC registers to program the STMR trigger for URxFE WB chain 0*/
typedef struct
{
  uint32 rxfe_cn_trig_cmd;
  uint32 rxfe_cn_trig_val;
}fw_rxlm_mtc_trig_group_struct;



 /* Group ADC_CONTROL: */ 
/* This block consist of the Bolt registers to program ADC sampling clock*/
typedef struct
{
  uint32 rxfe_adc_control_reg_adca;
}fw_rxlm_adc_control_group_struct;



 /* Group ADC_CLK_CFG: */ 
/* This block consist of the Bolt registers to program ADC sampling clock*/
typedef struct
{
  uint32 mss_bbrxa_mux_sel;
  uint32 mss_bbrxa_misc;
}fw_rxlm_adc_clk_cfg_group_struct;



 /* Group ADC_CONFIG: */ 
/* This block consist of the Bolt registers to program ADC sampling clock*/
typedef struct
{
  uint32 rxfe_adc_test1_reg_adca;
  uint32 rxfe_adc_test2_reg_adca;
  uint32 rxfe_adc_test3_reg_adca;
  uint32 rxfe_adc_config1_reg_adca;
  uint32 rxfe_adc_config2_reg_adca;
  uint32 rxfe_adca_logic_cfg;
}fw_rxlm_adc_config_group_struct;



 /* Group TOP_DBG: */ 
/* This block consist of the ADC RXFE DBG mode registers and ACI filters*/
typedef struct
{
  uint32 rxfe_dbg_enables;
  uint32 rxfe_dbg_probe_ctl;
  uint32 rxfe_dbg_cmd;
}fw_rxlm_top_dbg_group_struct;



 /* Group PBS_CFG: */ 
/* This block consist of the ADC RXFE DBG mode registers and ACI filters*/
typedef struct
{
  uint32 rxfe_pbs_cfg;
}fw_rxlm_pbs_cfg_group_struct;



 /* Group RFIF_UPDATED: */ 
/* This block consist of the ADC RXFE DBG mode registers and ACI filters*/
typedef struct
{
  uint32 rxfe_rfif_update_cmd;
}fw_rxlm_rfif_updated_group_struct;



 /* Group COMMON_ACI: */ 
/* This block consist of the ADC RXFE DBG mode registers and ACI filters*/
typedef struct
{
  uint32 rxfe_aci_filt0_cfg;
  uint32 rxfe_aci_filt1_cfg;
}fw_rxlm_common_aci_group_struct;



 /* Group COMMON_NOTCH_01: */ 
/* This block consist of the URxFE registers to program the common notches.*/
typedef struct
{
  uint32 rxfe_cmnnotch_filt01_cfg;
  uint32 rxfe_cmnnotch_filtn_cfg0[2];
  uint32 rxfe_cmnnotch_filtn_cfg1[2];
}fw_rxlm_common_notch_01_group_struct;



 /* Group COMMON_NOTCH_23: */ 
/* This block consist of the URxFE registers to program the common notches.*/
typedef struct
{
  uint32 rxfe_cmnnotch_filt23_cfg;
  uint32 rxfe_cmnnotch_filtn_cfg0[2];
  uint32 rxfe_cmnnotch_filtn_cfg1[2];
}fw_rxlm_common_notch_23_group_struct;



 /* Group TOP_WB_v0: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_top_ctl_wbw;
}fw_rxlm_top_wb_v0_group_struct;



 /* Group WB_ADCMUX: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wbw_adcmux_cfg;
}fw_rxlm_wb_adcmux_group_struct;



 /* Group TOP_WB_CMD: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_stopgate_action_sample_wbw;
  uint32 rxfe_wb_top_cmd_wbw;
}fw_rxlm_top_wb_cmd_group_struct;



 /* Group WB_MISC: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_misc_cfg_wbw;
}fw_rxlm_wb_misc_group_struct;



 /* Group WB_PWR: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_wbpwr_start_mask_wbw;
  uint32 rxfe_wb_wbpwr_cfg_wbw;
  uint32 rxfe_wb_wbpwr_start_action_sample_wbw;
  uint32 rxfe_wb_wbpwr_stop_action_sample_wbw;
  uint32 rxfe_wb_wbpwr_cmd_wbw;
}fw_rxlm_wb_pwr_group_struct;



 /* Group WBDC: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_wbdc_cfg_wbw;
  uint32 rxfe_wb_wbdc_update_action_sample_wbw;
  uint32 rxfe_wb_wbdc_start_action_sample_wbw;
  uint32 rxfe_wb_wbdc_stop_action_sample_wbw;
  uint32 rxfe_wb_wbdc_cmd_wbw;
  uint32 rxfe_wb_wbdc_update_delay_wbw;
  uint32 rxfe_wb_wbdc_ovrd_i_wbw;
  uint32 rxfe_wb_wbdc_ovrd_q_wbw;
  uint32 rxfe_wb_wbdc_est1_load_i_wbw;
  uint32 rxfe_wb_wbdc_est1_load_q_wbw;
  uint32 rxfe_wb_wbdc_est2_load_i_wbw;
  uint32 rxfe_wb_wbdc_est2_load_q_wbw;
}fw_rxlm_wbdc_group_struct;



 /* Group VSRC: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_vsrc_cfg_wbw;
  uint32 rxfe_wb_vsrc_t2byt1m1_wbw;
  uint32 rxfe_wb_vsrc_updt_action_sample_wbw;
  uint32 rxfe_wb_vsrc_sctr_wa_wbw;
  uint32 rxfe_wb_vsrc_sctr_load_value_wbw;
  uint32 rxfe_wb_vsrc_cmd_wbw;
}fw_rxlm_vsrc_group_struct;



 /* Group WB_DEC: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_dec_filt_cfg_wbw;
}fw_rxlm_wb_dec_group_struct;



 /* Group IQMC_A_B_COEFFS: */ 
/* This block consists of all notch related registers in a WB chain*/
typedef struct
{
  uint32 rxfe_wb_iqmc_cfg1_wbw;
}fw_rxlm_iqmc_a_b_coeffs_group_struct;



 /* Group IQMC: */ 
/* This block consists of all notch related registers in a WB chain*/
typedef struct
{
  uint32 rxfe_wb_iqmc_cfg0_wbw;
  uint32 rxfe_wb_iqmc_updt_action_sample_wbw;
  uint32 rxfe_wb_iqmc_updt_cmd_wbw;
}fw_rxlm_iqmc_group_struct;



 /* Group NOTCH: */ 
/* This block consists of all notch related registers in a WB chain*/
typedef struct
{
  uint32 rxfe_wb_notch_filtn_cfg0_wbw[3];
  uint32 rxfe_wb_notch_filtn_cfg1_wbw[3];
  uint32 rxfe_wb_notch_filt012_cfg_wbw;
}fw_rxlm_notch_group_struct;



 /* Group NB_WBMUX_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nbn_wbmux_cfg;
}fw_rxlm_nb_wbmux_nb_group_struct;



 /* Group TOP_NB_v0: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_top_ctl_nbn;
}fw_rxlm_top_nb_v0_group_struct;



 /* Group NB_GATE_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_gate_action_sample_nbn;
  uint32 rxfe_nb_gate_cmd_nbn;
}fw_rxlm_nb_gate_nb_group_struct;



 /* Group NB_DEC_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_filters_cfg_nbn;
}fw_rxlm_nb_dec_nb_group_struct;



 /* Group ICIFIR_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_filters_cfg_nbn;
  uint32 rxfe_nb_ici_coefc_nbn[11];
}fw_rxlm_icifir_nb_group_struct;



 /* Group NB_DC_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_filters_cfg_nbn;
  uint32 rxfe_nb_nbdc_compval_i_nbn;
  uint32 rxfe_nb_nbdc_compval_q_nbn;
}fw_rxlm_nb_dc_nb_group_struct;



 /* Group NB_GDA_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_gda_nbn;
}fw_rxlm_nb_gda_nb_group_struct;



 /* Group NB_PWR_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_nbpwr_cfg_nbn;
  uint32 rxfe_nb_nbpwr_start_action_sample_nbn;
  uint32 rxfe_nb_nbpwr_start_mask_nbn;
  uint32 rxfe_nb_nbpwr_stop_action_sample_nbn;
  uint32 rxfe_nb_nbpwr_cmd_nbn;
}fw_rxlm_nb_pwr_nb_group_struct;



 /* Group DVGA_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_dvga_cfg_nbn;
  uint32 rxfe_nb_dvga_rxfe_gain_nbn;
  uint32 rxfe_nb_dvga_updt_action_sample_nbn;
  uint32 rxfe_nb_dvga_override_dvga_gain_nbn;
  uint32 rxfe_nb_dvga_updt_mask_nbn;
  uint32 rxfe_nb_dvga_cmd_nbn;
}fw_rxlm_dvga_nb_group_struct;



 /* Group FINAL_BITWIDTHS_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_final_bitwidths_cfg_nbn;
}fw_rxlm_final_bitwidths_nb_group_struct;



 /* Group WRITER_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_wtr_cfg_nbn;
  uint32 rxfe_nb_wtr_cmd_nbn;
}fw_rxlm_writer_nb_group_struct;



 /* Group DBG_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_wtr_start_action_sample_nbn;
  uint32 rxfe_nb_wtr_stop_action_sample_nbn;
  uint32 rxfe_nb_wtr_dbg_cfg0_nbn;
  uint32 rxfe_nb_wtr_dbg_cfg1_nbn;
}fw_rxlm_dbg_nb_group_struct;



 /* Group CSR_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_csr_cfg_nbn;
  uint32 rxfe_nb_csr_phaseupdt_action_sample_nbn;
  uint32 rxfe_nb_csr_cmd_nbn;
}fw_rxlm_csr_nb_group_struct;



 /* Group CSR_PH_OFFSET_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_csr_phase_offset_nbn;
}fw_rxlm_csr_ph_offset_nb_group_struct;



 /* Group WTR_FUNC_CFG_NB: */ 
/* This block consist of the URxFE registers to program the first NB chain in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_wtr_func_cfg_nbn;
}fw_rxlm_wtr_func_cfg_nb_group_struct;



 /* Group CSR_FREQ_OFFSET_NB: */ 
/* This block consist of the URxFE registers to program the second NB chain's cordic rotator frequency offset in a NB group.*/
typedef struct
{
  uint32 rxfe_nb_csr_freq_offset_nbn;
  uint32 rxfe_nb_csr_frequpdt_action_sample_nbn;
  uint32 rxfe_nb_csr_cmd_nbn;
}fw_rxlm_csr_freq_offset_nb_group_struct;



// SW FW Interface Buffer

typedef struct ALIGN(32)
{
  fw_rxlm_csr_xo_vars_group_struct csr_xo_vars_params;
  fw_rxlm_xo_vars_group_struct xo_vars_params;
  fw_rxlm_vsrc_xo_vars_group_struct vsrc_xo_vars_params;
  fw_rxlm_stmr_xo_vars_group_struct stmr_xo_vars_params;
  fw_rxlm_delay_vars_group_struct delay_vars_params;
  fw_rxlm_dvga_vars_group_struct dvga_vars_params;
  fw_rxlm_vsrc_vars_group_struct vsrc_vars_params;
  fw_rxlm_adc_vars_group_struct adc_vars_params;
  fw_rxlm_csr_phase_comp_vars_group_struct csr_phase_comp_vars_params;
  fw_rxlm_notch_thresh_group_struct notch_thresh_params;
  fw_rxlm_scale_vars_group_struct scale_vars_params;
  fw_rxlm_rxfe_enable_group_struct rxfe_enable_params;
  fw_rxlm_mtc_trig_group_struct mtc_trig_params;
  fw_rxlm_adc_control_group_struct adc_control_params;
  fw_rxlm_adc_clk_cfg_group_struct adc_clk_cfg_params;
  fw_rxlm_adc_config_group_struct adc_config_params;
  fw_rxlm_top_dbg_group_struct top_dbg_params;
  fw_rxlm_pbs_cfg_group_struct pbs_cfg_params;
  fw_rxlm_rfif_updated_group_struct rfif_updated_params;
  fw_rxlm_common_aci_group_struct common_aci_params;
  fw_rxlm_common_notch_01_group_struct common_notch_01_params;
  fw_rxlm_common_notch_23_group_struct common_notch_23_params;
  fw_rxlm_top_wb_v0_group_struct top_wb_v0_params;
  fw_rxlm_top_wb_v0_group_struct top_wb_v1_params;
  fw_rxlm_top_wb_v0_group_struct top_wb_v2_params;
  fw_rxlm_wb_adcmux_group_struct wb_adcmux_params;
  fw_rxlm_top_wb_v0_group_struct top_wb_v3_params;
  fw_rxlm_top_wb_cmd_group_struct top_wb_cmd_params;
  fw_rxlm_wb_misc_group_struct wb_misc_params;
  fw_rxlm_wb_pwr_group_struct wb_pwr_params;
  fw_rxlm_wbdc_group_struct wbdc_params;
  fw_rxlm_vsrc_group_struct vsrc_params;
  fw_rxlm_wb_dec_group_struct wb_dec_params;
  fw_rxlm_iqmc_a_b_coeffs_group_struct iqmc_a_b_coeffs_params;
  fw_rxlm_iqmc_group_struct iqmc_params;
  fw_rxlm_notch_group_struct notch_params;
  fw_rxlm_nb_wbmux_nb_group_struct nb_wbmux_nb0_params;
  fw_rxlm_top_nb_v0_group_struct top_nb0_v0_params;
  fw_rxlm_top_nb_v0_group_struct top_nb0_v1_params;
  fw_rxlm_top_nb_v0_group_struct top_nb0_v2_params;
  fw_rxlm_top_nb_v0_group_struct top_nb0_v3_params;
  fw_rxlm_nb_gate_nb_group_struct nb_gate_nb0_params;
  fw_rxlm_nb_dec_nb_group_struct nb_dec_nb0_params;
  fw_rxlm_nb_dec_nb_group_struct eq_nb0_params;
  fw_rxlm_icifir_nb_group_struct icifir_nb0_params;
  fw_rxlm_nb_dc_nb_group_struct nb_dc_nb0_params;
  fw_rxlm_nb_gda_nb_group_struct nb_gda_nb0_params;
  fw_rxlm_nb_pwr_nb_group_struct nb_pwr_nb0_params;
  fw_rxlm_dvga_nb_group_struct dvga_nb0_params;
  fw_rxlm_final_bitwidths_nb_group_struct final_bitwidths_nb0_params;
  fw_rxlm_writer_nb_group_struct writer_nb0_params;
  fw_rxlm_dbg_nb_group_struct dbg_nb0_params;
  fw_rxlm_csr_nb_group_struct csr_nb0_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb0_params;
  fw_rxlm_wtr_func_cfg_nb_group_struct wtr_func_cfg_nb0_params;
  fw_rxlm_csr_freq_offset_nb_group_struct csr_freq_offset_nb0_params;
  fw_rxlm_nb_wbmux_nb_group_struct nb_wbmux_nb1_params;
  fw_rxlm_top_nb_v0_group_struct top_nb1_v0_params;
  fw_rxlm_top_nb_v0_group_struct top_nb1_v1_params;
  fw_rxlm_top_nb_v0_group_struct top_nb1_v2_params;
  fw_rxlm_top_nb_v0_group_struct top_nb1_v3_params;
  fw_rxlm_nb_gate_nb_group_struct nb_gate_nb1_params;
  fw_rxlm_nb_dec_nb_group_struct nb_dec_nb1_params;
  fw_rxlm_nb_dec_nb_group_struct eq_nb1_params;
  fw_rxlm_icifir_nb_group_struct icifir_nb1_params;
  fw_rxlm_nb_dc_nb_group_struct nb_dc_nb1_params;
  fw_rxlm_nb_gda_nb_group_struct nb_gda_nb1_params;
  fw_rxlm_nb_pwr_nb_group_struct nb_pwr_nb1_params;
  fw_rxlm_dvga_nb_group_struct dvga_nb1_params;
  fw_rxlm_final_bitwidths_nb_group_struct final_bitwidths_nb1_params;
  fw_rxlm_writer_nb_group_struct writer_nb1_params;
  fw_rxlm_dbg_nb_group_struct dbg_nb1_params;
  fw_rxlm_csr_nb_group_struct csr_nb1_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb1_params;
  fw_rxlm_wtr_func_cfg_nb_group_struct wtr_func_cfg_nb1_params;
  fw_rxlm_csr_freq_offset_nb_group_struct csr_freq_offset_nb1_params;
  fw_rxlm_nb_wbmux_nb_group_struct nb_wbmux_nb2_params;
  fw_rxlm_top_nb_v0_group_struct top_nb2_v0_params;
  fw_rxlm_top_nb_v0_group_struct top_nb2_v1_params;
  fw_rxlm_top_nb_v0_group_struct top_nb2_v2_params;
  fw_rxlm_top_nb_v0_group_struct top_nb2_v3_params;
  fw_rxlm_nb_gate_nb_group_struct nb_gate_nb2_params;
  fw_rxlm_nb_dec_nb_group_struct nb_dec_nb2_params;
  fw_rxlm_nb_dec_nb_group_struct eq_nb2_params;
  fw_rxlm_icifir_nb_group_struct icifir_nb2_params;
  fw_rxlm_nb_dc_nb_group_struct nb_dc_nb2_params;
  fw_rxlm_nb_gda_nb_group_struct nb_gda_nb2_params;
  fw_rxlm_nb_pwr_nb_group_struct nb_pwr_nb2_params;
  fw_rxlm_dvga_nb_group_struct dvga_nb2_params;
  fw_rxlm_final_bitwidths_nb_group_struct final_bitwidths_nb2_params;
  fw_rxlm_writer_nb_group_struct writer_nb2_params;
  fw_rxlm_dbg_nb_group_struct dbg_nb2_params;
  fw_rxlm_csr_nb_group_struct csr_nb2_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb2_params;
  fw_rxlm_wtr_func_cfg_nb_group_struct wtr_func_cfg_nb2_params;
  fw_rxlm_csr_freq_offset_nb_group_struct csr_freq_offset_nb2_params;
  fw_rxlm_indices_struct rxlm_reg_indices;
  fw_rxlm_block_valid_flags_struct rxlm_block_valid;
  fw_rxlm_header_struct rxlm_header;
}fw_rxlm_settings_type_ag;



/* This struct captures the different indices used to index into the HWIO registers of the IFreq Delta Buffer. */
typedef struct
{
  uint8 wbw_idx_0;
  uint8 nbn_idx_0;
  uint8 nbn_idx_1;
  uint8 nbn_idx_2;
}fw_rxlm_ifreq_indices_struct;



/* This is the struct type for the IFreq Delta buffer, that will be used incremental to the full LM Neighbor buffer. */
typedef struct
{
  fw_rxlm_iqmc_a_b_coeffs_group_struct iqmc_a_b_coeffs_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb0_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb1_params;
  fw_rxlm_csr_ph_offset_nb_group_struct csr_ph_offset_nb2_params;
  fw_rxlm_ifreq_indices_struct rxlm_reg_indices;
}fw_rxlm_ifreq_delta_struct;


typedef struct
{
/* Structure type definition containing notch settings and index values for secondary notch settings */
  fw_rxlm_common_notch_01_group_struct common_notch_01_params;
  fw_rxlm_common_notch_23_group_struct common_notch_23_params;
  fw_rxlm_notch_group_struct notch_params;
  fw_rxlm_indices_struct rxlm_reg_indices;


}fw_rxlm_secondary_notch_settings_type;

#ifdef __cplusplus
}
#endif



#endif


