/*!
  @file cdmafw_msg.h

  @brief External message interface for the CDMA1x FW module

*/

/*===========================================================================

  Copyright (c) 2010-13 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/31/11   isalman Don't use q6 headers when compiling for UMPLT.

===========================================================================*/
/**
  @mainpage Nike-L CDMA1x Firmware Interface Document
  @authors Haobing Zhu, Vijay Ramasami
  @section intro Revision
  $Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/c2k/api/cdmafw_msg.h#2 $
  @section whatsnew What's New

*/


#ifndef CDMAFW_MSG_H
#define CDMAFW_MSG_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include "msgr.h"
#include "msgr_umid.h"
#include "cdmafw_txagc.h"
#include "cdmafw_logging.h"
#include "cfw_rf_intf.h"
#include "cdmafw_const.h"
#include "fw_rf_common_intf.h"

#ifdef __cplusplus
} // extern "C"
#endif



/*============================================================================
 *
 * DOXYGEN GROUPINGS
 *
 * ==========================================================================*/
/**
 * \defgroup capp App Configuration Messages
 * \defgroup csrch SRCH Finger SW Commands
 * \defgroup csrchafc SRCH AFC SW Commands
 * \defgroup cmux MUX SW Commands
 * \defgroup clrfa Legacy RFA SW Commands
 * \defgroup cirat IRAT Command and Response
 * \defgroup cresp FW Response Messages
 * \defgroup cind FW Indication Messages
 * \defgroup cunion FW Message Unions
 * \defgroup cshared Shared Memory Interface
 * \defgroup clog CDMA FW Logging Messages.
 */

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief The text string for the external CDMAFW Task Queue. */
#define CDMAFW_MSG_Q_NAME           "CDMAFW_Q"

/*! \brief This is the maximum number of fingers that can be assigned. */
#define CDMAFW_NUM_FINGERS             8


/*! \brief This is the walsh power log buffer length in QLIC. */
#define WALSH_LOG_BUF_LEN (66*4) //bytes

/*! \brief This is the maximum number of cells. */
#define CDMAFW_NUM_PWR_CTL_CELLS       6

/*! \brief Number of variables for FW to peek, poke (debug only) */
#define CFW_POKE_VAR_SIZE                    10
#define CFW_PEEK_VAR_SIZE                    10

/*-----------------------------------------------------------------------*/
/*     CDMAFW COMMAND & RESPONSE ID'S                                    */
/*-----------------------------------------------------------------------*/

#define MSGR_MODULE_FW              0x00
#define MSGR_CDMA_FW                MSGR_TECH_MODULE( MSGR_TECH_CDMA, MSGR_MODULE_FW)



/*-----------------------------------------------------------------------*/
/*     CDMAFW Supervisory                                                */
/*-----------------------------------------------------------------------*/

/*! @brief Enumeration of CDMA FW Supervisory commands
 */
enum
{
  /*! \brief Not used for now, sent from APPMGR */
  MSGR_DEFINE_UMID(CDMA, FW, SPR, LOOPBACK, MSGR_ID_LOOPBACK,
                   msgr_spr_loopback_struct_type),
  /*! \brief Not used for now, send to APPMGR */
  MSGR_DEFINE_UMID(CDMA, FW, SPR, LOOPBACK_REPLY, MSGR_ID_LOOPBACK_REPLY,
                   msgr_spr_loopback_reply_struct_type),

};


/*-----------------------------------------------------------------------*/

/*! @brief Supervisory Loopback  - This message is sent by the message router
           and is to be sent back to it in a cdmafw_spr_loopback_reply_s to
           confirm that the  message queues are operating correctly. */
typedef msgr_spr_loopback_struct_type       cdmafw_spr_loopback_s;

/*! @brief Supervisory Loopback Reply  - This reply is sent back to the
           message router in response to receiving a umbsrch_spr_loopback_s
           message to confirm that the message queues are operating
           correctly. */
typedef msgr_spr_loopback_reply_struct_type cdmafw_spr_loopback_reply_s;



/*-----------------------------------------------------------------------*/
/*     CDMAFW COMMAND, INDICATION and RESPONSE UMIDs                     */
/*-----------------------------------------------------------------------*/

/*! @brief Enumeration of CDMA FW Command UMIDs
 */
// Payload input for documentation only.
enum
{
  /*! \brief The first message in the enum */
  CDMA_FW_CMD_FIRST = MSGR_UMID_BASE(MSGR_CDMA_FW, MSGR_TYPE_CMD),

  /* APP and STATE configuration */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, STATE_CFG,                     0x00, cdmafw_state_cfg_msg_t),

  /* Finger Assignment and Configuration */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FINGER_ASSIGN,                 0x01, cdmafw_finger_assign_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, MULTIPLE_FINGERS_ASSIGN,       0x02, cdmafw_multiple_fingers_assign_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FINGER_CONFIG,                 0x03, cdmafw_finger_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, MULTIPLE_FINGERS_CONFIG,       0x04, cdmafw_multiple_fingers_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FINGER_COMMON_CONFIG,          0x05, cdmafw_finger_common_config_msg_t),

  /* AFC Configuration */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, AFC_MODE,                      0x06, cdmafw_afc_mode_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, XO_AFC_STATIC_CONFIG,          0x07, cdmafw_xo_afc_static_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, XO_AFC_DYNAMIC_CONFIG,         0x08, cdmafw_xo_afc_dynamic_config_msg_t),

  /* L1 Forward Link Configuration Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FL_SIGNALING,                  0x09, cdmafw_fl_signaling_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, APF_ENABLE,                    0x0A, cdmafw_apf_enable_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FPC_FREEZE_CONFIG,             0x0B, cdmafw_fpc_freeze_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FPC_OVERRIDE,                  0x0C, cdmafw_fpc_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, SCH_ACC_SCALE,                 0x0D, cdmafw_sch_acc_scale_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, FCH_TARGET_SETPOINT,           0x0E, cdmafw_fch_target_setpoint_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, SCH_TARGET_SETPOINT,           0x0F, cdmafw_sch_target_setpoint_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RDA_CONFIG,                    0x10, cdmafw_rda_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, QOF_NOISE_EST_FPC,             0x11, cdmafw_qof_noise_est_fpc_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, DEMBACK_MODE_CONFIG,           0x12, cdmafw_demback_mode_config_msg_t),

  /* L1 Reverse Link Configuration Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_SIGNALING,                  0x13, cdmafw_rl_signaling_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_LEGACY_RC_FRAME_CONFIG,     0x14, cdmafw_rl_legacy_rc_frame_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_RC8_FRAME_CONFIG,           0x15, cdmafw_rl_rc8_frame_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_MPP_CONFIG,                 0x16, cdmafw_rl_mpp_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_PWR_FILTER_CONFIG,          0x17, cfw_rl_pwr_est_filter_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RL_ACK_TEST_MODE_CONFIG,       0x18, cdmafw_rl_ack_test_mode_cfg_msg_t),

  /* Logging Configuration Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, QLIC_WALSH_PWR_LOGGING,        0x19, cdmafw_qlic_walsh_pwr_logging_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, ADV1X_LOG_CONFIG,              0x1A, cdmafw_adv1x_log_config_msg_t),

  /* RF SW TX Configuration Messages */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_AGC_CFG,                    0x1B, cfw_tx_agc_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_PA_PARAMS_CONFIG,           0x1C, cdmafw_tx_pa_params_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_LIMIT_CONFIG,               0x1D, cfw_tx_limit_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_HDET_CONFIG,                0x1E, cfw_tx_hdet_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, DAC_CAL_CONFIG,                0x1F, cfw_tx_dac_cal_config_msg_t),

  /* RF SW TX Override Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_OVERRIDE,                   0x20, cdmafw_tx_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_CLOSED_LOOP_OVERRIDE,       0x21, cdmafw_tx_closed_loop_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_OPEN_LOOP_OVERRIDE,         0x22, cdmafw_tx_open_loop_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_PA_STATE_OVERRIDE,          0x23, cdmafw_tx_pa_state_override_msg_t),

  /* RF SW TX Miscellanous Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_HDET_REQUEST,               0x24, cfw_tx_hdet_request_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RXLM_CONFIG_MASKED,            0x25, cdmafw_rxlm_config_masked_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, DYNAMIC_TXC_UPDATE,            0x26, cfw_dynamic_txc_update_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_HDET_RESET,                 0x27, cfw_tx_hdet_reset_msg_t),

  /* RX and TX Tuning Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_START_CFG,                  0x28, cfw_rx_start_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_START,                      0x29, cfw_rx_start_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_STOP,                       0x2A, cfw_rx_stop_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_START,                      0x2B, cfw_tx_start_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_STOP,                       0x2C, cfw_tx_stop_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_DAC_START,                  0x2D, cfw_tx_dac_start_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_DAC_STOP,                   0x2E, cfw_tx_dac_stop_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, INTELLICEIVER_UPDATE,          0x2F, cfw_intelliceiver_update_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, OQPCH_SEARCH_DONE,             0x30, cdmafw_oqpch_search_done_msg_t),

  /* IRAT Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, PILOT_MEAS_CFG_REQ,            0x31, cdmafw_pilot_meas_cfg_req_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, PILOT_MEAS_STOP_STREAM,        0x32, cdmafw_pilot_meas_stop_stream_msg_t),

  /* RF SW RX Configuration Messages */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_AGC_CFG,                    0x33, cfw_rx_agc_cfg_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, LNA_OVERRIDE,                  0x34, cfw_lna_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, LNA_GAIN_OFFSET,               0x35, cfw_lna_gain_offset_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_AGC_STOP,                   0x36, cfw_rx_agc_stop_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RX_SET_FREQ_OFFSET,            0x37, cdmafw_rx_set_freq_offset_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RXAGC_ACQ_MODE_CONFIG,         0x38, cdmafw_rxagc_acq_mode_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, XPT_CONFIG,                    0x39, cfw_xpt_config_msg_t),

  /*  Tx Sample Capture command */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, EPT_CAPTURE,                   0x3A, cdmafw_ept_capture_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, EPT_OVERRIDE,                  0x3B, cdmafw_ept_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, EPT_CONFIG,                    0x3C, cfw_ept_config_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, EPT_SAMPLE_BUFFER_CFG,         0x3D, cfw_ept_sample_buffer_cfg_msg_t),

  /*  QXDM Logging Messages */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, LOG_RX_IQ_SAMPLES,             0x3E, cdmafw_rx_iq_samples_logging_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, DIAG_LOG_CONFIG,               0x3F, cdmafw_diag_log_config_msg_t),

  /* To be deprecated */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, TDEC_BRDG_CONFIG,              0x40, cdmafw_tdec_brdg_config_msg_t),

  MSGR_DEFINE_UMID(CDMA, FW, CMD, XPT_CAPTURE,                   0x41, cdmafw_xpt_capture_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, STOP_TXAGC_UPDATE,             0x42, cdmafw_stop_txagc_update_msg_t),

  MSGR_DEFINE_UMID(CDMA, FW, CMD, TX_PA_RANGE_OVERRIDE,          0x43, cdmafw_tx_pa_range_override_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, RXAGC_LNA_CAL_OVERRIDE,        0x44, cdmafw_rxagc_lna_cal_override_msg_t),


  MSGR_DEFINE_UMID(CDMA, FW, CMD, JD_THRESHOLD_UPDATE,           0x45, cdmafw_jd_threshold_update_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, CMD, JD_MODE,                       0x46, cdmafw_jd_mode_msg_t),

  MSGR_DEFINE_UMID(CDMA, FW, CMD, XPT_DELAY_UPDATE,              0x47, cfw_xpt_delay_update_msg_t),

  MSGR_DEFINE_UMID(CDMA, FW, CMD, DEMBACK_SW_CONFIG,             0x48, cdmafw_demback_sw_config_msg_t),

  /* L1 DSDx priority Commands */
  MSGR_DEFINE_UMID(CDMA, FW, CMD, REGISTER_RX_TX_ACTIVITY,       0x49, cdmafw_dsda_priority_msg_t),

#ifdef FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY
  MSGR_DEFINE_UMID(CDMA, FW, CMD, ANT_SWITCH_ASDIV,              0x4A, cfw_rf_execute_ant_switch_msg_t),

  /*! \brief The last message in the enum */
  CDMA_FW_CMD_LAST = CDMA_FW_CMD_ANT_SWITCH_ASDIV
#else
  CDMA_FW_CMD_LAST = CDMA_FW_CMD_REGISTER_RX_TX_ACTIVITY

#endif

};


/*! @brief Enumeration of CDMA FW Indication UMIDs
 */
// Payload input for documentation only.
enum
{
  CDMA_FW_IND_FIRST = MSGR_UMID_BASE(MSGR_CDMA_FW, MSGR_TYPE_IND),
  MSGR_DEFINE_UMID(CDMA, FW, IND, RXTX_FRAME_STATS,              0x00, cdmafw_rxtx_frame_stats_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, QLIC_FRAME_STATS,              0x01, cdmafw_qlic_frame_stats_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, RC11_FCH_DECODER_DONE,         0x02, cdmafw_rc11_fch_decoder_done_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, ADV1X_LOG_BUFFER_RDY,          0x03, cdmafw_adv1x_log_buffer_rdy_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, VI_CTL,                        0x04, cdmafw_vi_ctl_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, RESET_REQ,                     0x05, cdmafw_reset_req_ind_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, EPT_PROC_DONE,                 0x06, cdmafw_ept_proc_done_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, IND, EPT_EXPANSION_DONE,            0x07, cdmafw_ept_expansion_done_msg_t),
  CDMA_FW_IND_LAST = CDMA_FW_IND_EPT_EXPANSION_DONE
};


/*! @brief Enumeration of CDMA FW Response UMIDs
 */
// Payload input for documentation only.
enum
{
  CDMA_FW_RSP_FIRST = MSGR_UMID_BASE(MSGR_CDMA_FW, MSGR_TYPE_RSP),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, QLIC_WALSH_PWR_LOGGING_READY,  0x00, cdmafw_qlic_walsh_pwr_logging_rdy_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, STATE_CFG,                     0x01, cdmafw_state_cfg_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, TX_HIGH_PWR_DET,               0x02, cdmafw_tx_high_pwr_det_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, TX_START,                      0x03, cfw_tx_start_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, TX_STOP,                       0x04, cfw_tx_stop_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, RX_START,                      0x05, cfw_rx_start_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, RX_STOP,                       0x06, cfw_rx_stop_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, TX_HDET_RESPONSE,              0x07, cfw_tx_hdet_response_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, PILOT_MEAS_CFG,                0x08, cdmafw_pilot_meas_cfg_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, PILOT_MEAS_STOP_STREAM,        0x09, cdmafw_pilot_meas_stop_stream_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, INTELLICEIVER_UPDATE,          0x0A, cfw_intelliceiver_update_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, DAC_CAL_CONFIG,                0x0B, cfw_dac_cal_config_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, IRAT_RX_START,                 0x0C, cfw_irat_rx_start_rsp_msg_t),

  MSGR_DEFINE_UMID(CDMA, FW, RSP, EPT_CAPTURE,                   0x0D, cdmafw_ept_capture_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, XPT_CAPTURE,                   0x0E, cdmafw_xpt_capture_rsp_msg_t),
//RK TODO PA_PARAMS_CONFIG to be removed after RFSW has no references to it
  MSGR_DEFINE_UMID(CDMA, FW, RSP, PA_PARAMS_CONFIG,              0x0F, cfw_pa_params_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, XPT_TRANS_COMPL,               0x10, cfw_xpt_trans_compl_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, DEMBACK_SW_CONFIG,             0x11, cdmafw_demback_sw_config_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, XPT_CONFIG,                    0x12, cfw_xpt_config_rsp_msg_t),
  MSGR_DEFINE_UMID(CDMA, FW, RSP, ASDIV_PRI_CHANGE,              0x13, cdmafw_asdiv_pri_change_rsp_msg_t),

  CDMA_FW_RSP_LAST = CDMA_FW_RSP_ASDIV_PRI_CHANGE
};

/*! @brief Enumeration of CDMA FW Internal Message UMIDs
 */
enum {
  MSGR_DEFINE_UMID(CDMA, FW, CMDI, MSGR_SHUT_DOWN,               0x00, NULL),
  /* internal RX_START message for VI use */
  MSGR_DEFINE_UMID(CDMA, FW, CMDI, RX_START,                     0x01, NULL)
};

/*-----------------------------------------------------------------------*/
/*     DATA STRUCTURES FOR CDMAFW COMMANDS                               */
/*-----------------------------------------------------------------------*/

/**
*   \addtogroup capp
*   @{
*/

/*! \brief  CDMA State Definitions */
typedef enum {
  CDMAFW_STATE_INACTIVE=0,
  CDMAFW_STATE_STANDBY,
  CDMAFW_STATE_ACTIVE,
  CDMAFW_STATE_MEAS,
  CDMAFW_STATE_SLEEP,
  CDMAFW_STATE_MEAS_IDLE
} cdmafw_state_t;

/*! \brief CDMA STATE Config message
 *
 * Change CDMA APP state to the requested state.
 *
 */
typedef struct {
  /*! \brief Message header */
  msgr_hdr_struct_type hdr;
  /*! \brief New state of CDMA1x APP */
  cdmafw_state_t state;
} cdmafw_state_cfg_msg_t;

/**@}*/

/**
   \addtogroup csrch
    */
/*@{*/

/*! @brief Single Finger Assignment Command
 *
 *   This is the message definition for the cdmafw Single Finger Assignment
 *   Command
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fingerIndex:3;
  /*!< Index of the finger to configure.
  */
  uint32 FingCfgWr_Update:1;
  /*!< Set to 1 to indicate FingCfgEnable, FingCfgPosition and
       FingPilotFilterState update.
  */
  uint32 FingCfgWr;
  /*!< Finger Configuration
    @verbatim
    Bit 31: Request to keep pilot filter state (0=clear pilot filter state, 1=keep pilot filter state)
    Bit 30-19: Sequence number (Increase the sequence number for each finger assignment)
    Bit 18-1: FingCfgPosition (PN position in cx8)
    Bit 0: Enable flag (0=Disable finger, 1=Enable finger)
    @endverbatim
    Note: SW shall set the ARM register DEM1X_RTF_CTL_SEL as 1 so that MDSP controls fingers and
          HW finger state machine is disabled.
  */
  uint32 PwrCtlCell_Update:1;
  /*!< Set to 1 to indicate PwrCtlCell update. \n
  	Note: When PwrCtlCell is updated, FingCfg is required to be updated.
  */
  uint16 PwrCtlCell;
  /*!< This indicates which Cell the finger is associated with.
    @verbatim
    Null Cell: 000
    Cell A:    001
    Cell B:    010
    Cell C:    011
    Cell D:    100
    Cell E:    101
    Cell F:    110
    @endverbatim
  */
  uint32 QLICSectorId_Update:1;
  /*!< Set to 1 to indicate QLICEnable and QLICSectorId update. \n
    Note: When QLICEnable is updated, FingCfg is required to be updated.
  */
  uint16 QLICSectorId;
  /*!< QLIC Sector ID for QOF set 0.
    @verbatim
    Valid ID: 0~9
    Not in any QLIC sector: 0xFFFF
    @endverbatim
	Note: Due to hardware limitation, the maximum delay spread supported in one
	      QLIC sector is 40 chips.
  */
  uint32 SectorId_Update:1;
  /*!< Set to 1 to indicate SectorId update.
  */
  uint16 SectorId;
  /*!< This indicates the fingers sector as configured in the
       DEM1X_COMMON_REV_PWR_CTL/CTL2 registers used for
	   F-CPCCH or F-CACKCH (1xAdv).
    @verbatim
    000: sector1 (offset specified by INIT_OFFSET1)
    001: sector2 (offset specified by INIT_OFFSET2)
    010: sector3 (offset specified by INIT_OFFSET3)
    011: sector4 (offset specified by INIT_OFFSET4)
    100: sector5 (offset specified by INIT_OFFSET5)
    101: sector6 (offset specified by INIT_OFFSET6)
	@endverbatim
  */

  uint32 QofSectorInfo_Update:1;
  /*!< Set to 1 to indicate QofSectorInfo Update
   */

  uint16 QofSectorInfo;
   /*!< QOF Sector Information

       @verbatim
       |15|                          |5   4|3         0|
       |-----------------------------------------------|
       |D | NOT USED                 |setId| sector Id |
       |-----------------------------------------------|
       3:0 - QOF sector Id.range is 0-9
       5:4 - Set Id.range is 0-3
        15 - QOF disable flag. set to 1 for disable.
       @endverbatim
  */
  uint32 fchQofUseQlicSectorERam_Update:1;
  /*!< Set to 1 to indicate fchQofUseQlicSectorERam Update
   */

  uint16 fchQofUseQlicSectorERam;
   /*!< Set to 0x10 for using QOF QLIC sector ERAM and 0 for not
     using it.
  */

  uint16 qlicCarrierSelect_Update;
  /*!< Set to 1 to indicate qlicCarrierSelect Update
   */

  uint16 qlicCarrierSelect;
  /*! QLIC Carrier Input Select.
   *
   * This field indicates the carrier from which QLIC fetches its finger data. Use the
   * following values:
   *
   * 0 => fetch from primary chain.
   * 3 => fetch from SV chain.
   *
   */
} cdmafw_finger_assign_msg_t;

/**
 * @brief SRCH SW Multiple Fingers Assignment Command
 * CDMA FW finger assignment command message structure
 */
typedef struct {
    uint32 FingCfgWr_Update:1;
    /*!< Set to 1 to indicate FingCfgEnable, FingCfgPosition and
         FingPilotFilterState update.
    */
    uint32 FingCfgWr;
    /*!< Finger Configuration
      @verbatim
      Bit 31: Request to keep pilot filter state (0=clear pilot filter state, 1=keep pilot filter state)
      Bit 30-19: Sequence number (Increase the sequence number for each finger assignment)
      Bit 18-1: FingCfgPosition (PN position in cx8)
      Bit 0: Enable flag (0=Disable finger, 1=Enable finger)
      @endverbatim
      Note: SW shall set the ARM register DEM1X_RTF_CTL_SEL as 1 so that MDSP controls fingers and
            HW finger state machine is disabled.
    */
    uint32 PwrCtlCell_Update:1;
    /*!< Set to 1 to indicate PwrCtlCell update. \n
      Note: When PwrCtlCell is updated, FingCfg is required to be updated.
    */
    uint16 PwrCtlCell;
    /*!< This indicates which Cell the finger is associated with.
      @verbatim
      Null Cell: 000
      Cell A:    001
      Cell B:    010
      Cell C:    011
      Cell D:    100
      Cell E:    101
      Cell F:    110
      @endverbatim
    */
    uint32 QLICSectorId_Update:1;
    /*!< Set to 1 to indicate QLICEnable and QLICSectorId update. \n
      Note: When QLICEnable is updated, FingCfg is required to be updated.
    */
    uint16 QLICSectorId;
    /*!< QLIC Sector ID for QOF set 0.
      @verbatim
      Valid ID: 0~9
      Not in any QLIC sector: 0xFFFF
      @endverbatim
      Note: Due to hardware limitation, the maximum delay spread supported in one
            QLIC sector is 40 chips.
    */
    uint32 SectorId_Update:1;
    /*!< Set to 1 to indicate SectorId update.
    */
    uint16 SectorId;
    /*!< This indicates the fingers sector as configured in the
         DEM1X_COMMON_REV_PWR_CTL/CTL2 registers used for
         F-CPCCH or F-CACKCH (1xAdv).
      @verbatim
      000: sector1 (offset specified by INIT_OFFSET1)
      001: sector2 (offset specified by INIT_OFFSET2)
      010: sector3 (offset specified by INIT_OFFSET3)
      011: sector4 (offset specified by INIT_OFFSET4)
      100: sector5 (offset specified by INIT_OFFSET5)
      101: sector6 (offset specified by INIT_OFFSET6)
      @endverbatim
    */

  uint32 QofSectorInfo_Update:1;
  /*!< Set to 1 to indicate QofSectorInfo Update
   */

  uint16 QofSectorInfo;
   /*!<
   |15|                          |5   4|3         0|
   |-----------------------------------------------|
   |D | NOT USED                 |setId| sector Id |
   |-----------------------------------------------|
   3:0 - QOF sector Id.range is 0-9
   5:4 - Set Id.range is 0-3
    15 - QOF disable flag. set to 1 for disable.

  */
  uint32 fchQofUseQlicSectorERam_Update:1;
  /*!< Set to 1 to indicate fchQofUseQlicSectorERam Update
   */

  uint16 fchQofUseQlicSectorERam;
   /*!< Set to 0x10 for using QOF QLIC sector ERAM and 0 for not
     using it.
  */

  uint16 qlicCarrierSelect_Update;
  /*!< Set to 1 to indicate qlicCarrierSelect Update
   *
   * \deprecated { No longer needed in Dime. Shall be ignored }
   */

  uint16 qlicCarrierSelect;
  /*! QLIC Carrier Input Select.
   *
   * \deprecated { No longer needed in Dime. Shall be ignored }
   *
   * This field indicates the carrier from which QLIC fetches its finger data. Use the
   * following values:
   *
   * 0 => fetch from primary chain.
   * 3 => fetch from SV chain.
   *
   */
} cdmafw_finger_assign_t;

/** @brief Multiple Fingers Assignment Command
 *
 *   This is the message definition for the cdmafw Multiple Fingers
 *   Assignment Command
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fingerMask;
  /*!< Finger Mask
    @verbatim
    Bit 0: set to 1 to indicate fingerAssign[0] update
    Bit 1: set to 1 to indicate fingerAssign[1] update
    Bit 2: set to 1 to indicate fingerAssign[2] update
    Bit 3: set to 1 to indicate fingerAssign[3] update
    Bit 4: set to 1 to indicate fingerAssign[4] update
    Bit 5: set to 1 to indicate fingerAssign[5] update
    Bit 6: set to 1 to indicate fingerAssign[6] update
    Bit 7: set to 1 to indicate fingerAssign[7] update
	@endverbatim
  */
  cdmafw_finger_assign_t fingerAssign[CDMAFW_NUM_FINGERS];
  /*!< finger assign array
  */
} cdmafw_multiple_fingers_assign_msg_t;

/** @brief SRCH SW Finger Configuration Command
 * CDMA FW finger configuration command message structure
 */

typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fingerIndex:3;
  /*!< Index of the finger to configure.
  */
  uint32 TTEnable:1;
  /*!< When it's 1 and the finger is in lock, the Time Tracking loop is enabled.
    When it's 0, advances or retards are not issued.
  */
  uint16 TTComELGain;
  /*!< TTComELGain is the scale factor used for normalizing the common
    early/late pilot symbols. This value is a function of the diversity
    mode.
  */
  uint16 TTDivELGain;
  /*!< TTDivELGain is the scale factor used for normalizing the diversity
    early/late pilot symbols. This value is a function of the diversity
    mode.
  */
  uint16 ComPilotGain;
  /*!< ComPilotGain is the scale factor used for normalizing the common pilot
    symbols. This value is a function of the diversity mode.
  */
  uint16 DivPilotGain;
  /*!< DivPilotGain is the scale factor used for normalizing the diversity
    pilot symbols. This value is a function of the diversity mode.
  */
  uint32 CombEnable:1;
  /*!< This register is used to mask the combiner lock state for the finger.
    @verbatim
    0: finger combining is disabled
    1: finger combining is enabled
    @endverbatim
	Note: If the finger is in the active set, this should be set to 1.
  */
  uint16 RSSIInit;
  /*!< This value is used to initialize the MSW of the finger's RSSI value
    during finger initialization/assignment. The LSW of finger's RSSI value
    is initialized to 0. This is applied to both antennas. \n
	The unit is (Ec/Io/18/2^11).
  */
  uint16 PowerLockThresh;
  /*!< This parameter is compared to the finger RSSI value (MSW) to determine
    whether a finger is in power lock. \n
    The unit is (Ec/Io/18/2^11).
  */
  uint32 ForceLock:1;
  /*!< This bit overrides Com_Finger_Lock and Div_Finger_Lock states of the finger.
    When it is 1, Com_Finger_Lock state and Div_Finger_Lock state are forced to the
	state specified in ForceLockState. When Lock_State_Override is 0, ForceLock is
    disabled and the finger lock state mechanism behaves normally for
    primary and diversity paths.
  */
  uint32 ForceLockState:1;
  /*!< This bit is used in conjunction with ForceLock to override Finger_Lock state.
    When ForceLock is 1, Finger_Lock state takes on ForceLockState value.
	A value of 1 forces finger in lock, a value of 0 forces finger out of lock.
	This is applied to both antennas.
  */
} cdmafw_finger_config_msg_t;

/** @brief SRCH SW Multiple Fingers Configuration Command
 * CDMA FW multiple finger configuration command message structure
 */
typedef struct {
  uint32 TTEnable:1;
  /*!< When it's 1 and the finger is in lock, the Time Tracking loop is enabled.
    When it's 0, advances or retards are not issued.
  */
  uint16 TTComELGain;
  /*!< TTComELGain is the scale factor used for normalizing the common
    early/late pilot symbols. This value is a function of the diversity
    mode.
  */
  uint16 TTDivELGain;
  /*!< TTDivELGain is the scale factor used for normalizing the diversity
    early/late pilot symbols. This value is a function of the diversity
    mode.
  */
  uint16 ComPilotGain;
  /*!< ComPilotGain is the scale factor used for normalizing the common pilot
    symbols. This value is a function of the diversity mode.
  */
  uint16 DivPilotGain;
  /*!< DivPilotGain is the scale factor used for normalizing the diversity
    pilot symbols. This value is a function of the diversity mode.
  */
  uint32 CombEnable:1;
  /*!< This register is used to mask the combiner lock state for the finger.
    @verbatim
    0: finger combining is disabled
    1: finger combining is enabled
    @endverbatim
	Note: If the finger is in the active set, this should be set to 1.
  */
  uint16 RSSIInit;
  /*!< This value is used to initialize the MSW of the finger's RSSI value
    during finger initialization/assignment. The LSW of finger's RSSI value
    is initialized to 0. This is applied to both antennas. \n
	The unit is (Ec/Io/18/2^11).
  */
  uint16 PowerLockThresh;
  /*!< This parameter is compared to the finger RSSI value (MSW) to determine
    whether a finger is in power lock. \n
    The unit is (Ec/Io/18/2^11).
  */
  uint32 ForceLock:1;
  /*!< This bit overrides Com_Finger_Lock and Div_Finger_Lock states of the finger.
    When it is 1, Com_Finger_Lock state and Div_Finger_Lock state are forced to the
	state specified in ForceLockState. When Lock_State_Override is 0, ForceLock is
    disabled and the finger lock state mechanism behaves normally for
    primary and diversity paths.
  */
  uint32 ForceLockState:1;
  /*!< This bit is used in conjunction with ForceLock to override Finger_Lock state.
    When ForceLock is 1, Finger_Lock state takes on ForceLockState value.
	A value of 1 forces finger in lock, a value of 0 forces finger out of lock.
	This is applied to both antennas.
  */
} cdmafw_finger_config_t;

/*! @brief Multiple Fingers Configuration Command
 *
 *   This is the message definition for the cdmafw Multiple Fingers
 *   Configuration Command
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fingerMask;
  /*!< Finger Mask
    @verbatim
    Bit 0: set to 1 to indicate fingerConfig[0] update
    Bit 1: set to 1 to indicate fingerConfig[1] update
    Bit 2: set to 1 to indicate fingerConfig[2] update
    Bit 3: set to 1 to indicate fingerConfig[3] update
    Bit 4: set to 1 to indicate fingerConfig[4] update
    Bit 5: set to 1 to indicate fingerConfig[5] update
    Bit 6: set to 1 to indicate fingerConfig[6] update
    Bit 7: set to 1 to indicate fingerConfig[7] update
	@endverbatim
  */
  cdmafw_finger_config_t fingerConfig[CDMAFW_NUM_FINGERS];
  /*!< finger assign array
  */
} cdmafw_multiple_fingers_config_msg_t;

/*! @brief Finger Common Parameters Configuration Command
 *
 *   This is the message definition for the CDMA FW Finger Common
 *   Config Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 ttK1Gain;
  /*!< This is the K1 Gain value used in the computation of the ttFreq and
    ttPhase parameters in the time tracking loop. \n This value is typically set
    to 2048 (0x0800).
  */
  uint16 ttK2Gain;
  /*!< This is the K2 Gain value used in the computation of the ttPhase
    parameter in the time tracking loop. \n This value is typically set to
    16384 (0x4000).
  */
  uint16 pilotFiltGain;
  /*!< This is the filter coefficient for the Pilot filter. \n The filter coefficient
    should be set to 4096 (0x1000) for Cellular, set it to 8192 (0x2000) for PCS.
    This value is in Q15.
  */
  uint16 fingLowerlockThresh;
  /*!< This parameter is compared to the finger com/div RSSI values to determine
    whether a finger is out of com/div lock. \n
	The unit is (Ec/Io/18/2^11). The default value is 74 (=> Ec/Io = -26.97 dB).
  */
} cdmafw_finger_common_config_msg_t;

/** @brief OQPCH Search Done
 *
 * This message must be sent after a OQPCH-based RX_STOP message, and it indicates that
 * OQPCH searches are done. On receiving this message, FW will free mempool lines.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

} cdmafw_oqpch_search_done_msg_t;

/**@}*/

/**
 * \addtogroup csrchafc
 * @{
 */

/** @brief AFC Mode Parameters
 *
 *   This is the message definition for the cdmafw AFC Mode Command
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 afcMode_Update:1 __attribute__((deprecated));
  /*!< Set to 1 to indicate afcMode update.
  */
  uint32 afcMode:1 __attribute__((deprecated));
  /*!< AFC mode configuration (Deprecated as XO AFC is used by default).
	@verbatim
    0: VCTCXO AFC
    1: XO AFC
    @endverbatim
  */
  uint32 afcCrossMode_Update:1;
  /*!< Set to 1 to indicate afcCrossMode update.
  */
  uint32 afcCrossMode:1;
  /*!< Cross mode configuration in VCTCXO and XO AFC
	@verbatim
    0: crossmode 0 (CM0), use scaled-filtered pilot to compute frequency error.
    1: crossmode 1 (CM1), use raw pilot to compute frequency error.
    @endverbatim
    Note : Typically, CM1 is used in acquisition and CM0 is used in tracking.
  */
  } cdmafw_afc_mode_msg_t;

/** @brief XO AFC Static Parameters
 *
 *   This is the message definition for the cdmafw XO AFC Static
 *   Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 afcTxRxFreqRatio;
  /*!< Ratio of Tx frequency / Rx frequency
    The formula is (Tx Freq/Rx Freq) x (2^14).
  */
  uint16 afcRxFreqGain;
  /*!< Gain used for MND update.
    The formula for SC2x is (0.125 x 1.2288 x 10^8) / (Rx Freq in MHz).
    The formula is different for different PLL setting used for CHIPXN.
  */
  uint32 afcXoInvRxFreq;
  /*!< Gain used for PPM computation.
   * It is defined as 2^33/f_DL(Hz) in Q19 format, where f_DL(Hz) is the
   * downlink frequency in Hz.
   */
  uint32 afcXoMndMBase;
  /*!< MND M base.
  */
  uint32 afcXoMndNBase;
  /*!< MND N base.
  */
  uint16 afcXoMndRndFactor;
  /*!< MND Rounding factor.
  */
  uint16 afcXoMndRndBits;
  /*!< MND number of rounding bits.
  */
  uint32 rpushParamsUpdate:1;
  /*!< Enable latching for GPS RPUSH filter parameters */
  int32 rpushEcIoFiltTcShift;
  /*!< Time constant shift value for Ecp/Io filter. Time constant would be
   * 1/2^shift in (64*4) chip time units */
} cdmafw_xo_afc_static_config_msg_t;

/** @brief XO AFC Dynamic Parameters
 *
 *   This is the message definition for the cdmafw XO AFC Dynamic
 *   Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 afcXoGain_Update:1;
  /*!< Set to 1 to indicate afcXoGain update.
  */
  uint16 afcXoGain;
  /*!< Rotator loop gain.
  */
  uint32 afcXoLoadStb_Update:1;
  /*!< Set to 1 to indicate afcXoLoadStb and afcXoLoadFreqAcc update.
  */
  uint32 afcXoLoadStb:1;
  /*!< Set to 1 to load afcLoadFreqAccVco and afcLoadFreqAccRot.
  */
  uint16 afcXoLoadFreqAcc;
  /*!< Initialization value, used in conjunction with afcXoLoadStb to initialize the
    state of the RX rotator frequency accumulator. If afcXoLoadStb is 1, then this
    value is loaded into the upper 16 bits of rotator accumulator.
    The unit is (4.6875/2^16) Hz.
  */
} cdmafw_xo_afc_dynamic_config_msg_t;
/**@}*/

/**
 * \addtogroup cmux
 * @{
 */
/** @brief FL Signaling Parameters
 *
 *   This is the message definition for the cdmafw FL Signaling
 *   Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fliRC11Enable_Update:1;
  /*!< Set to 1 to indicate fliRC11Enable update.
  */
  uint32 fliRC11Enable:1;
  /*!< FL RC11 enable flag
	@verbatim
    0: RC11 disabled
    1: RC11 enabled
	@endverbatim
  */
  uint32 fliFchAckMask_Update:1;
  /*!< Set to 1 to indicate fliFchAckMask update.
  */
  uint16 fliFchAckMask;
  /*!< ACK Mask for R-FCH early termination
    @verbatim
    Bit(n) is for PCG (15-n), n=0..15
    Bit(n)=0: ACK for R-FCH is not punctured in PCG (15-n) of F-FCH
    Bit(n)=1: ACK for R-FCH is punctured in PCG (15-n) of F-FCH
	@endverbatim
    Latched by FW at the frame boundary. The configuration of this variable should
	be from REV_FCH_ACK_MASK field of RCPM.
  */
  uint32 fliSchAckMask_Update:1;
  /*!< Set to 1 to indicate fliSchAckMask update.
  */
  uint16 fliSchAckMask;
  /*!< ACK Mask for R-SCH early termination
    @verbatim
    Bit(n) is for PCG (15-n), n=0..15
    Bit(n)=0: ACK for R-SCH is not punctured in PCG (15-n) of F-CACKCH
    Bit(n)=1: ACK for R-SCH is punctured in PCG (15-n) of F-CACKCH
	@endverbatim
    Latched by FW at the frame boundary. The configuration of this variable should
	be from REV_SCH_ACK_MASK field of RCPM.
  */
  uint32 fliBlankingPeriod_Update:1;
  /*!< Set to 1 to indicate fliBlankingPeriod update.
  */
  uint32 fliBlankingPeriod:3;
  /*!< FL blanking period
	@verbatim
    0: blanking period = 1 (blanking disabled)
    3: blanking period = 4 20-ms frames
    7: blanking period = 8 20-ms frames
	@endverbatim
    Latched by FW at the frame boundary. The configuration of this variable should
	be based on FOR_N field of RCPM.
  */
  uint32 fliFchEarlyDecodePCGMask_Update:1;
  /*!< Set to 1 to indicate fliFchEarlyDecodePCGMask update.
   */
  uint16 fliFchEarlyDecodePCGMask;
  /*!< FL FCH Early Decode PCG Mask

    @verbatim
    Bit(n) is for PCG (15-n), n=0..15
    Bit(n) = 0: no F-FCH early decoding attempt at PCG (15-n)
    Bit(n) = 1: F-FCH early decoding attempt at PCG (15-n)
    @endverbatim

    FW latches the configuration from SW at the frame boundary.

    The configuration of this variable should be derived from the value programmed in
    rliFchAckMask. If R-ACKCH can be transmitted at PCG n, FL early decoding can be
    triggered at PCG n-1. Since bit 15 (RL PCG0) and bit 0 (RL PCG15) of rliFchAckMask
    should always be 0, bit 0 (FL PCG15) and bit 1 (FL PCG14) for this variable should
    always be 0 too.  There is no early decoding for PCG0 and PCG1, so bit 15 (FL PCG0)
    and bit 14 (FL PCG1) should always be 0 too.
  */

} cdmafw_fl_signaling_msg_t;
/**@}*/

/**
 * \addtogroup csrch
 * @{
 */
/*! @brief APF Configuration Message
 *
 * This message configures the Adaptive Pilot Filter
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 apfEnable:1;
  /*!< APF Enable.
   * Set to 1 to enable APF, 0 otherwise (legacy pilot filter will be used).
   */

  uint32 apfStep;
  /*!< APF Step Parameter.
   * Should be in Q30 format. The default setting should be 16. This value should be eventually
   * determined by Modem Systems.
   */
} cdmafw_apf_enable_msg_t;
/**@}*/

/**
 * \addtogroup cmux
 * @{
 */
/*! @brief FPC Freeze limit
 *
 * This is the message definition for the FPC freeze command.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fliFchFpcFreezeEnable:1;
  /*!< FPC freeze enable */

  uint32 : 15;
  uint32  fliFchFpcFreezeLimit:16;
  /*!< FPC Freeze limit*/

} cdmafw_fpc_freeze_cfg_msg_t;

/*! @brief FPC override test pattern */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fliFPCOverride:1;
  /*!< FPC Override mode */

  uint32 : 15;

  uint32 fliFPCTestPattern:16;
  /*!< FPC Test pattern Mask if override mode is enabled*/

} cdmafw_fpc_override_msg_t;


/*! @brief XPT capture message. Carries info specific to xpt sample capture.
*/
typedef struct {
  /*! @brief Message header */
  msgr_hdr_struct_type hdr;

  /*! @ brief Specify sample capture parameters, including buffer ID's,
   * response sequence ID */
  fw_rf_capture_param_t captureParam;

}cdmafw_xpt_capture_msg_t;

/*! @brief XPT_CAPTURE_RSP Message Response
 *
 * \sa cdmafw_xpt_capture_rsp_msg_t
 */
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type hdr;

  /*! \brief DPDindex for which this capture was done */
  uint16  DPDindex;

  /*! \brief Sequence number for tracking request and response messages */
  uint32  sequenceNumber;

}cdmafw_xpt_capture_rsp_msg_t;

/**@}*/

/**
 * \addtogroup csrch
 * @{
 */

/*! @brief QLIC Walsh Power Logging
 *
 *   This is the message definition for the cdmafw QLIC Walsh Power
 *   Logging Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 walshPwrSector:4;
  /*!< QLIC Sector ID for logging
	@verbatim
    Valid ID: 0~9
	@endverbatim
  */
} cdmafw_qlic_walsh_pwr_logging_msg_t;
/**@}*/

/**
 * \addtogroup cmux
 * @{
 */

/*! @brief SCH IQ Accumulation Scale
 *
 *   This is the message definition for the cdmafw SCH IQ Acc Scale
 *   Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 schIqAccScale;
  /*!< Scaling applied to the IQ_ACC (Eb estimator) for the SCH FPC and DTX detection.
  */
} cdmafw_sch_acc_scale_msg_t;

/*! @brief FCH Target Setpoint
 *
 *   This is the message definition for the cdmafw FCH Target Setpoint
 *   Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 ebNtMagTargetFch;
  /*!< (Q10) This is the target threshold used in the fundamental channel forward power
    control decision. This is the scaled magnitude ratio (not energy), which is set to
    @verbatim
    sqrt(pi/2).sqrt((Eb(PCB)/Eb(FCH)).Eb(FCH)/Nt).2^10
    @endverbatim
    where "Eb(PCB)/Eb(FCH)" is the PCB to fundamental channel ratio derived from
    signaling
  */
} cdmafw_fch_target_setpoint_msg_t;

/*! @brief SCH Target Setpoint
 *
 *   This is the message definition for the cdmafw SCH Target Setpoint
 *   Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 ebntMagTargetSch;
  /*!< (Q10) This is the target threshold used in the supplemental channel forward
    power control decision. This is the scaled magnitude ratio (not energy), which is
    set to
    @verbatim
    sqrt(pi/2).sqrt(Eb(SCH)/Nt).2^10
    @endverbatim
    where Eb(SCH)/Nt is the desired SNR target.
 */
} cdmafw_sch_target_setpoint_msg_t;

/*! @brief TDEC Bridge Configuration
 *
 * This message configures (enables/disables) the TDEC bridge and SW mempool AHB access
 * to DEMBACK output data. It also registers mempool lines with CRM FW (required to keep
 * the mempool clock alive).
 */
typedef struct {

  /*!< MSGR header */
  msgr_hdr_struct_type hdr;

  /*!< Enable flag
   *
   * 1 => enable.
   * 0 => disable.
   */
  boolean tdecEnable;

} cdmafw_tdec_brdg_config_msg_t;

/*! @brief DEMBACK SW Configuration.

    This message allows SW to configure FW to either use VOICE mode in HW demback or buffer CH0
    symbols for SW decoding. If swDembackEn is set to TRUE, then FW does not touch demback HW, and
    instead buffers CH0 symbols in SMEM for SW decode usage. Otherwise, FW enables VOICE mode in HW
    demback. Enabling one mode will implicitly disable the other mode.

    Once FW is configured to use "SW mode" it is not expected for SW to enable "DATA" or "TDEC"
    modes using DEMBACK_MODE_CONFIG message. "SW mode" must be disabled prior to enabling these
    modes.

    To mimic legacy HW usage, it is best to send this message as early as possible (ex: right after
    FW goes to ACTIVE state).

*/
typedef struct {

  /** MSGR Header */
  msgr_hdr_struct_type hdr;

  /** Backdoor debug flag that enables both SW and HW demback modes (for debug).

      For DEBUG use only. Never enable on target. Only latched if swDembackEn
      is set to TRUE.

    */
  boolean debugModeEn;

  /** Set this to TRUE to update swDembackEn */
  boolean swDembackEn_Update;

  /** Demback SW Configuration.

      0 => Use VOICE mode in HW demback.
      1 => Use SW demback and enable CH0 SMEM buffer.

    */
  boolean swDembackEn;

  /** Set this to TRUE to update syncState */
  boolean syncStateEn_Update;

  /** Setup SYNC state (80ms or 20ms).

    0 => Ping Pong for CH0 SMEM happens every 20 ms.
    1 => Ping Pong for CH0 SMEM happens every 80 ms.

    Please note that this flag is checked at every 20ms frame boundary.

 */
  boolean syncStateEn;

} cdmafw_demback_sw_config_msg_t;


/*! @brief DEMBACK_MODE_SEL and TDEC Bridge Configuration
 *
 * This message is used to configure 1x Demback in 1X_VOICE/1X_DATA/1X_VOICE+1X_DATA modes.
 * Additionally, this message configures (enables/disables) the TDEC bridge and SW mempool AHB access
 * to DEMBACK output data. It also registers mempool lines with CRM FW (required to keep
 * the mempool clock alive).
 *
 * If SW demback is being used, then setting dataEnable or tdecEnable to TRUE will cause a FW
 * ERR fatal. Either SW or HW can be used for demback, but not both.
 */
typedef struct {

  /*!< MSGR header */
  msgr_hdr_struct_type hdr;

  /*!< Enable flag
   *
   * 1 => enable (configure 1x data mode in DEMBACK_MODE_SEL)
   * 0 => disable (disable 1x data mode in DEMBACK_MODE_SEL)
   */
  uint16 dataEnable;

  /*!< Set to 1 for tdecEnable update */
  uint16 tdecEnable_Update;
  /*!< Enable flag
   *
   * 1 => enable and config TDEC bridge
   * 0 => disable and free TDEC bridge
   */
  uint16 tdecEnable;

} cdmafw_demback_mode_config_msg_t;

/*! @brief RL_MPP_CONFIG Command
 *
 * This is the message definition for the cdmafw MPP_CONFIG
 * configuration command
 */
typedef struct {

  /*!< MSGR header */
  msgr_hdr_struct_type hdr;

  /*!< MPP Enable flag
    @verbatim
      0: MPP disabled.
      1: MPP enabled.
    @endverbatim
  */
  uint32 rliEnableMPP:1;

  /* Minimum R-FCHACKCH to R-PICH linear power ratio.
     The resolution is 1/(2048*2048) or Q22.
  */
  uint32 rliFchAckchT2PMin;

  /* Minimum R-SCH to R-PICK linear power ratio.
     The resolution is 1/(2048*2048) or Q22.
  */
  uint32 rliSchT2PMin;

} cdmafw_rl_mpp_cfg_msg_t;

/*! @brief RL_ACK_TEST_MODE_CONFIG Command
 *
 * This is the message definition for the cdmafw ACK_TEST_MODE_CONFIG
 * configuration command
 */
typedef struct {

  /*!< MSGR header */
  msgr_hdr_struct_type hdr;

  /*!< ACK Test mode*/
  uint16 rliAckTestMode;

  /*!< ACK test mode value*/
  uint16 rliAckTestModeValue;

} cdmafw_rl_ack_test_mode_cfg_msg_t;

/*! @brief RL Signaling Parameters
 *
 *   This is the message definition for the cdmafw RL Signaling Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fundFrameOffset_Update:1;
  /*!< Set to 1 to indicate fundFrameOffset update.
  */
  uint32 fundFrameOffset:4;
  /*!< Frame offset: 0~15
  */
  uint32 cTxGainAdjStepSize_Update:1;
  /*!< Set to 1 to indicate cTxGainAdjStepSize update.
  */
  uint16 cTxGainAdjStepSize;
  /*!< Tx closed loop adjust step size in Q9
	@verbatim
    0.25dB: 0x80
    0.5dB:  0x100
    1dB:    0x200
    2dB:    0x400
	@endverbatim
  */
  uint16 cTxMaskDelayWr_Update:1;
  /*!< Set to 1 to indicate cTxMaskDelayWr update.
    Every time software writes to TX_AGC_CTL[2:0] (MASK_DELAY), SW should write to this parameter
	with (to_HW_value+2). Both HW and FW need this information for processing.
	This parameter is configured base on RL RC and is latched by FW every frame.
  */
  uint16 cTxMaskDelayWr;
  /*!< Mask delay  write value.
    Every time software writes to TX_AGC_CTL[2:0] (MASK_DELAY), SW should write to this parameter
	with (to_HW_value+2). Both HW and FW need this information for processing.
	This parameter is configured base on RL RC and is latched by FW every frame.
  */
  uint32 rliRCConfig_Update:1;
  /*!< Set to 1 to indicate rliRCConfig update.
  */
  cdmafw_rc_config_type rliRCConfig;
  /*!< RL Configuration flag
	@verbatim
    0: Other RC Config
    1: RC8 Config
    2: IS-95 (RC1/2) Config
	@endverbatim

  \sa CDMAFW_RC_CONFIG_TYPE
  */
  uint32 rliAckMask_Update:1;
  /*!< Set to 1 to indicate rliFchAckMask and rliSchAckMask update.
  */
  uint16 rliFchAckMask;
  /*!< RL FCH ACK mask
	@verbatim
    Bit(n) is for PCG (15-n), n=0..15.
    Bit(n)=0: R-ACKCH for F-FCH is not allowed to be transmitted in PCG (15-n).
    Bit(n)=1: R-ACKCH for F-FCH is allowed to be transmitted in PCG (15-n).
	@endverbatim
    FW latches the configuration from SW at the frame boundary. \n The configuration of this
	variable should be from FOR_FCH_ACK_MASK_RL_BLANKING field or FOR_FCH_ACK_MASK_NO_RL_BLANKING
	field of Radio Configuration Parameter Message (RCPM). \n Mobile Station (MS) should not send
	ACK to Base station on PCG0 and PCG15, so bit 15 and bit 0 should always be 0.
  */
  uint16 rliSchAckMask;
  /*!< RL SCH ACK mask
   *
   * This is not yet supported !
  */
  uint32 rliFPCMask_Update:1;
  /*!< Set to 1 to indicate rliFPCMask update.
  */
  uint16 rliFPCMask;
  /*!< RL FPC Mask
    Bit(n) is for PCG (15-n), n=0..15.
  */
  uint32 rliDtxRfGatingEn_Update:1;
  /*!< Set to 1 to indicate rliDtxRFGatingEn update.
   */
  boolean rliDtxRfGatingEn;
  /*!< Allows 1xL1 to control PA/TX ON gating during DTX.
       0 => PA/TX ON gating disabled (only digital gains gating used).
       1 => PA/TX ON gating enabled.
   */

} cdmafw_rl_signaling_msg_t;

/*! @brief RL Legacy RC8 Frame Config Parameters
 *
 *   This is the message definition for the cdmafw RL Legacy RC
 *   Frame Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 MOD_PCH_GAIN;
  /*!< R-PICH digital channel gain.
    FW latches the configuration from SW at the frame boundary.
  */
  uint16 MOD_DCCH_GAIN;
  /*!< R-PICH digital channel gain.
    FW latches the configuration from SW at the frame boundary.
  */
  uint16 MOD_FCH_GAIN;
  /*!< R-PICH digital channel gain.
    FW latches the configuration from SW at the frame boundary.
  */
  uint16 MOD_SCH_GAIN;
  /*!< R-PICH digital channel gain.
    FW latches the configuration from SW at the frame boundary.
  */
  uint16 cTxRateAdjWr;
  /*!< TX_RATE_ADJ.
    FW latches the configuration from SW at the frame boundary. \n
    The unit is -1/640 dB.
  */
  boolean cTxFrameValid;
  /*!< Frame Valid Indicator
   FW uses this indicator to appropriately turn ON or OFF the
   TX_VALID and PA_VALID signals. This signal will be latched by FW
   at PCG15, symbol10 of a frame and will be used to control PA/TX
   valid signals from that point onwards.

   When TX/PA have to be turned OFF, SW must ensure that the digital
   gains that are sent in this message are also zeroed out.
  */
  uint16 rliTransmitPCGMask;
  /*!< Transmit PCG Mask for the next frame.

   FW will use this value to implement PCG masking for the next
   frame.

   BIT(15-i) = 1 => PCG(i) is transmitted.
   BIT(15-i) = 0 => PCG(i) is DTXed.

   - For non-DTX'ed frames, please set this to 0xFFFF.
   - For DTX'ed frames, please set this as per Systems Recommendation.

   This parameter will be latched by FW @ symbol-16 of the last PCG of
   the current frame, and shall be applied to the next frame.

   Please note cTxFrameValid = 0 setting *overrides* this parameter.
   Basically, when SW wishes to turn TX completely off (usually happens
   when tearing down TX), SW can set cTxFrameValid = 0, and FW will
   ignore any rliTransmitPCGMask setting and turns off TX.

   */

} cdmafw_rl_legacy_rc_frame_config_msg_t;

/*! @brief RL RC8 Frame Config Parameters
 *
 *   This is the message definition for the cdmafw
 *   RL RC8 Frame Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 rliAckchToPich;
  /*!< R-ACKCH to R-PICH linear gain ratio in Q11.
    FW latches the configuration from SW at the upcoming frame boundary.
  */
  uint16 rliFchToPich;
  /*!< R-FCH to R-PICH linear gain ratio in Q11.
    FW latches the configuration from SW at the upcoming frame boundary.
  */
  uint16 rliSchToPich;
  /*!< R-SCH to R-PICH linear gain ratio in Q11.
    FW latches the configuration from SW at the upcoming frame boundary.
  */
  uint16 rliDcchToPich;
  /*!< R-DCCH to R-PICH linear gain ratio in Q11.
    FW latches the configuration from SW at the upcoming frame boundary.
  */
  uint16 rliPCPattern;
  /*!< Power control pattern for the next transmitted frame.
    FW latches the configuration from SW at the upcoming frame boundary.
    - 200Hz: 0x4444
    - 400Hz: 0x5555
    - DTX: per systems recommendation.
  */
  uint32 rliBlankedFrame:1;
  /*!< Blanking flag for the next transmitted frame
	@verbatim
    0: not blanked
    1: blanked
	@endverbatim
    FW latches the configuration from SW at the frame boundary.
  @deprecated
    This parameter will be deprecated soon.
  */
  boolean cTxFrameValid;
  /*!< Frame Valid Indicator

   FW uses this indicator to appropriately turn ON or OFF the
   TX_VALID and PA_VALID signals. This signal will be latched by FW
   at PCG15, symbol10 of a frame and will be used to control PA/TX
   valid signals from that point onwards.

   When TX/PA have to be turned OFF, SW must ensure that the digital
   gains that are sent in this message are also zeroed out.

   When set to 0, this parameter overrides rliTransmitPCGMask.

  */
  uint16 rliTransmitPCGMask;
  /*!< Transmit PCG Mask for the next frame.

   FW will use this value to implement PCG masking for the next
   frame.

   BIT(15-i) = 1 => PCG(i) is transmitted.
   BIT(15-i) = 0 => PCG(i) is DTXed.

   - For non-DTX'ed and non-SB'ed frames, please set this to 0xFFFF.
   - For SB'ed frames, please set this to 0x9999.
   - For DTX'ed frames, please set this as per Systems Recommendation.

   This parameter will be latched by FW @ symbol-16 of the last PCG of
   the current frame, and shall be applied to the next frame.

   Please note cTxFrameValid = 0 setting *overrides* this parameter.
   Basically, when SW wishes to turn TX completely off (usually happens
   when tearing down TX), SW can set cTxFrameValid = 0, and FW will
   ignore any rliTransmitPCGMask setting and turns off TX.

   */

} cdmafw_rl_rc8_frame_config_msg_t;
/** DSDX State Definition. */
typedef enum {
  /*! CxM based DSDX is inactive.Single sim without CxM solution */
  DSDX_STATE_INACTIVE = 0,

  /*! CxM based DSDA is active */
  DSDA_STATE_ACTIVE,

  /*! CxM based DSDS is active */
  DSDS_STATE_ACTIVE,

} cdmafw_dsdx_state_t;


/*! @brief DSDA Priority Config Parameters
 *
 *   This is the message definition for the cdmafw
 *   dsda priority Configuration Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */
  cdmafw_dsdx_state_t dsdxMode;
  /*!< flag to select DSDx states */
  uint32 rxPriority;
  uint32 txPriority;
  /*!< Priority of activity. Higher value indicates higher priority.There is no
   * concept of Macro or Micro priority in FW. L1 resolves Macro/Micro priority
   * and passes one single priority value to Tech FW.
  */
  /*!< rxPriority and txPriority are respectively used by L1 to set the Rx and Tx
   * priority. A value of 0xFFFFFFFF indicate priority not valid.
  */
  /*!< asdiv priority used by L1 to set the priority during ASDIV event */
  uint32 asdivPriority;
  uint32 rliOffPCGPriority;
  /*!< DTx off PCG mask for DSDA.
   * FW will use this value to override the priority sent by L1 when FW detect
   * blanking.
  */
  uint32  rxFrequencyId;
  /*!< RX Frequency ID.
  */
  uint32  txFrequencyId;
  /*!< TX Frequency ID.
  */
  uint16  rxChannelId;
  /*!< RX Channel ID.
  */
  uint16  txChannelId;
  /*!< TX Channel ID.
  */

} cdmafw_dsda_priority_config_msg_t;
/**@}*/

/**
 * \addtogroup clrfa
 * @{
 */

/*! @brief PA Parameters Message
 *
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 paRangeMap;
  /*!< PA Range Map.
   *
   * Holds PA range mapping (see below):
   *
   \verbatim
     15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
    +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
    |\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\|  PAR0   |  PAR1   |  PAR2   |  PAR3   |
    +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
    PARi -> PA range for PA state i
   \endverbatim
   *
   * Latched immediately.
   */

  uint16 paHysteresisTimerValue;
  /*!< PA Hysteresis Timer Value
   *
   * Hysteresis is disabled when timer = 0 (Latched immediately).
   *
   * */

} cdmafw_tx_pa_params_config_msg_t;

/*! @brief TX Limit Parameters Message
 *
 * Defines TX limit parameters. Latched at 2 places:
 * - Each frame boundary.
 * - On TX init.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  int16 txTotalMax;
  /*!< MAX txTotal (FW units: -1/640 dB) */

  int16 txTotalMin;
  /*!< MIN txTotal (FW units: -1/640 dB) */

} cdmafw_tx_limit_config_msg_t;

/*! @brief Set PA Range Message
 *
 * Overrides PA range (works both in normal and override TxAGC mode). SW can use
 * this to bypass the PA state machine.
 *
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 paRangeOverride:1;
  /*!< PA Range Override flag */

  uint16 paRangeWr:2;
  /*!< PA Range Value (0-3) */

} cdmafw_tx_pa_range_override_msg_t;

/*! @brief TX Override Message
 *
 * Override TxAGC.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 txOverride:1;
  /*!< Override flag
   *
   * When this flag is set, FW enters override mode, where intial TxAGC
   * computations (OL, CL, RA) are skipped and FW will use RF SW provided
   * values for writing HW registers
   */

  uint16 paStateWr;
  /*!< PA State (for override)
   *
   * PA State (0-3) for override. FW will apply PA range map programmed by
   * SW.
   *
   */

  uint32 txAgcAdjWr;
  /*!< AVGA Gain Word (for override)
   *
   * 32-bit AVGA gain word (for override).
   */

  int16 txBetapGainWr;
  /*!< BETAP Gain (in dB units).
   *
   * BETAP Gain (-1/640 dB units) for override. FW will convert this into
   * linear units before writing to BETAP gain register.
   */

} cdmafw_tx_override_msg_t;

/*! @brief Override TX Closed Loop
 *
 * Override TX closed loop value. Both Normal and Trigger-Mode
 * overrides supported.
 *
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 txGainAdjOverride:1;
  /*!< Tx closed-loop power accumulator override request
	@verbatim
    0: normal mode, accumulator is updated by FW
    1: Override with the value specified in cTxGainAdjWr
	@endverbatim
  */

  uint16 txGainAdjClrOverride:1;
  /*!< Tx closed-loop power accumulator override clear request
	@verbatim
    0: stay in the override mode until cTxGainAdjOverride is set to 0
    1: go back to normal mode after taking one overridden value
	@endverbatim
  */

  int16 txGainAdjWr;
  /*!< Tx closed-loop power accumulator override value.
    The unit is -1/512 dB.
  */

} cdmafw_tx_closed_loop_override_msg_t;

/*! @brief Override TX Open Loop Message
 *
 *  Overrides TX open-loop value.
 *
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 txOpenLoopOverride:1;
  /*!< Open Loop Override flag.
   *
   * 0 -> no override (normal mode).
   * 1 -> override.
   */

  int16 txOpenLoopWr;
  /*!< Open Loop Override Value.
   *
   * 1/640 dB units.
   */

} cdmafw_tx_open_loop_override_msg_t;

/*! @brief Override PA state.
 *
 * Overrides PA state (in normal mode TXAGC operation) - in override mode
 * PA state is already overrriden.
 *
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 paStateOverride:1;
  /*!< PA State Override flag
   *
   * 0 -> no override.
   * 1 -> override.
   */

  uint16 paStateWr:2;
  /*!< PA State Override Value */

} cdmafw_tx_pa_state_override_msg_t;

/*! @brief Set Frequency Offset
 *
 * This commands is used to set a frequency offset for 1X. Pre-SVDO, the purpose
 * of this message is to implement the ULIF scheme required for RTR8600.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 rxFrequencyOffset;
  /*!< Specify RX rotator frequency offset for 1X*/

} cdmafw_rx_set_freq_offset_msg_t;

/*! @brief Stop txagc updates
 *
 * This command stops all FW TXAGC updates. (typically used for XPT setup)
 * **************************************************
 * NOTE: READ THE NOTE * ABOUT stop_txagc_gain_updates below.
 * **************************************************
 * */
typedef struct {

  /*!< MSGR header */
  msgr_hdr_struct_type hdr;

  /*!< Sequence number to track command response messages */
  uint32 sequence_number;

  /*!< Boolean flag that indicates FW to stop ALL TXAGC updates.  If set to
   * TRUE, FW will bypass all TX code execution. If this command is sent to
   * FW with this set to TRUE, and at a later point RFSW wants to send any TX
   * commands to FW, this command needs to be resent to FW with flag set to
   * FALSE. Basically, if you disable FW TXAGC updates, you have to re-enable
   * them by sending this command again before sending any other TX commands.
   * */
  boolean stop_txagc_gain_updates;

} cdmafw_stop_txagc_update_msg_t;

/*! @brief RF Notch Filters Configuration Message
 *
 * This message configures the RF Notch Filters. FW will always write to
 * notch registers as long as L1 gives us the corresponding RF chain. RF
 * SW can enable/disable notch filters at a higher level in HW.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 antenna;
  uint32 rxlm_buffer_idx;
  uint32 rxlm_config_mask;


} cdmafw_rxlm_config_masked_msg_t;

/*! @brief Tx High Power Detection
 *
 *   This is the message definition for the cdmafw Tx High Power
 *   Detection Command.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 cTxPowerDetEnable:1;
  /*!< TxAGC Override Flag
    @verbatim
    0: Disable
    1: Enable
    @endverbatim
  */
  uint16 cTxPowerDetThresh;
  /*!< The unit is in -1/640 dB resolution
  */
} cdmafw_tx_high_pwr_det_msg_t;
/*@}*/

/**@}*/

/**
 * \addtogroup cmux
 * @{
 */
/*! @brief 1xAdvance Half-PCG Logging Configuration
 *
 * Configure 1xAdvance half-PCG logging.
 */
typedef struct {

  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 logEnable:1;
  /*!< Enable logging features (1 = enable, 0 = disable) */

  uint16 logMsgEnable:1;
  /*!< When enabled (=1), FW will send log indication messages (every 8
   * PCGs). When disabled (=0), no log indication messages will be sent.
   */
} cdmafw_adv1x_log_config_msg_t;
/**@}*/


/*============================================================================
 *
 * INDICATION MESSAGES
 *
 * ==========================================================================*/
/**
 * \addtogroup cind
 * @{
 */

/**
 * @brief SRCH SW RxTx Frame Stats Indication
 * CDMA1x FW RxTx frame stats indication message structure
 */
typedef struct
{
  uint16 ComPilotRawI;
  /*!< Raw Pilot */
  uint16 ComPilotRawQ;
  /*!< Raw Pilot */
  uint16 DivPilotRawI;
  /*!< Raw Pilot */
  uint16 DivPilotRawQ;
  /*!< Raw Pilot */
  uint16 ComPilotFIltI;
  /*!< Filtered Pilot */
  uint16 ComPilotFiltQ;
  /*!< Filtered Pilot */
  uint16 DivPilotFiltI;
  /*!< Filtered Pilot */
  uint16 DivPilotFiltQ;
  /*!< Filtered Pilot */
  uint16 ComFingLock;
  /*!< Finger Lock */
  uint16 DivFingLock;
  /*!< Finger Lock */
  uint16 ComFiltRSSI;
  /*!< Finger estimated Ecp/Io */
  uint16 DivFiltRSSI;
  /*!< Finger estimated Ecp/Io */
  uint16 MmseFiltA11I;
  /*!< MMSE Correlation coefficients */
  uint16 MmseFiltA01I;
  /*!< MMSE Correlation coefficients */
  uint16 MmseFiltA01Q;
  /*!< MMSE Correlation coefficients */
  uint16 MmseFiltA00I;
  /*!< MMSE Correlation coefficients */
} cdmafw_fing_stats_t;

/*! @brief RxTx Frame Stats
 *
 *   This is the message definition for the cdmafw RxTx frame
 *   Stats Indication.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  cdmafw_fing_stats_t fingerStats[CDMAFW_NUM_FINGERS];
  /*!< finger stats */
  uint16 rpcHistory;
  /*!< This parameter contains the reverse power control decisions for the most recent frame.
    The MSB of rpcHistory is the oldest or first rcp decision of the frame, the LSB is the most
	recent or last rpc decision of the frame. rpcHistory is valid at the beginning of the next frame,
	however, software should wait for 1 symbol into the next frame before reading rpcHistory due to
	potential processing latencies of the combiner request.
  */
  uint16 rpcHistoryValid;
  /*!< This parameter indicates whether a particular RPC decision in rpcHistory was a valid decision.
    A one in the corresponding bit position of rpcHistory would indicate that the RPC decision was valid
	for that PC symbol. A zero would indicate that no fingers were in power lock for that PC symbol, and
	therefore no RPC decision was output to the AGC block. rpcHistoryValid is valid when rpcHistory is
	valid.
  */
  uint16 fpcHistory;
  /*!< This parameter contains the forward power control decisions for the most recent frame. The MSB
    of fpcHistory is the oldest or first FPC decision of the frame, the LSB is the most recent or last
	FPC decision of the frame. fpcHistory is valid at the beginning of the next frame, however, software
	should wait for 1 symbol into the next frame before reading fpcHistory due to potential processing
	latencies of the combiner request.
  */
  uint16 pcBitAccFrame;
  /*!< Power control sub-channel energy estimate accumulated over a frame
  */
  uint16 ch0AccFrame;
  /*!< Accumulation of symbol magnitudes (abs(I) + abs(Q)) over the frame for channel 0 (FCH/DCCH)
  */
  uint16 ch2AccFrame;
  /*!< Accumulation of symbol magnitudes (abs(I)+abs(Q)) over the frame for channel 2 (SCH)
  */
  uint16 fundWeightedNtMagAccFrame;
  /*!< This is the fundamental channel weighted Nt magnitude estimate accumulated over a frame
  */
  uint16 suppWeightedNtMagAccFrame;
  /*!< This is the supplemental channel weighted Nt magnitude estimate accumulated over a frame
  */
  uint16 fundWeightedPilotAccFrame;
  /*!< Accumulation of the weighted pilot magnitude for the FCH over a frame
  */
  uint16 suppWeightedPilotAccFrame;
  /*!<  Accumulation of the weighted pilot magnitude for the SCH over a frame
  */
  uint16 cTxGainAdjVal;
  /*!< Closed loop power accumulator in -1/512 dB resolution
  */
  uint16 cTxRateAdjVal;
  /*!< TX_RATE_ADJ in -1/640 dB resolution
  */
  uint16 cTxTotalGainVal;
  /*!< Tx Total in -1/640 dB resolution
  */
  uint16 rlMaxTxTotal;
  /*!< Max Tx limit in -1/640 dB resolution
  */
  uint16 rlMinTxTotal;
  /*!< Min Tx limit in -1/640 dB resolution
  */
} cdmafw_rxtx_frame_stats_ind_msg_t;

/*! @brief QLIC Frame Stats
 *
 *   This is the message definition for the cdmafw QLIC Frame
 *   Stats Indication.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

} cdmafw_qlic_frame_stats_ind_msg_t;

/*! @brief RC11 FCH Decoder Done
 *
 *   This is the message definition for the cdmafw RC11 FCH
 *   Decoder Done Indication.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 fliRC11FchRateInfo:5;
  /*!< RC 11 F-FCH rate determination result
    @verbatim
    0: Erasure
    1: Eighth rate
    2: Quarter rate
    3: Half rate
    4: Full rate
    @endverbatim
  */
  uint32 fliValidFPCFrame:1;
  /*!< Valid FPC frame flag
    @verbatim
    0: SW should not use this frame for channel supervision or adjusting FPC set point.
    1: SW should use this frame for channel supervision and adjusting FPC set point.
    @endverbatim
	FW does not determine whether its erasure or blank frame. Based on the frame counter,
	frame offset and the configuration of fliBlankingPeriod, FW determines whether its a
	guaranteed non-null-rate frame or not.
  */

  uint32 fliFrameIndex:3;
  /*!< Frame Counter
   * A 3-bit frame count for the decoded frame.
   */

  uint32 fliFchDecodePCGIndexMinusOne:4;
  /*!< Decoded PCG Indicator
     SW shall use (fliFchDecodePCGIndexMinusOne+1) for PCG index in MAC counter.

     If (fliFchDecodePCGIndexMinusOne != 14),  this means:
     - Early decoding happens at symbol 16 of Rx PCG (fliFchDecodePCGIndexMinusOne +1) for the current frame.
       There's no early (partial) decoding at symbol 16 of Rx PCG0, PCG1, PCG14, PCG15.
     - The number of symbols for decoding = 24*( fliFchDecodePCGIndexMinusOne +1)+16
     If (fliFchDecodePCGIndexMinusOne == 14), this means:
     - Full frame decoding happens at symbol 0 of Rx PCG 0 for the previous frame;
     - The number of symbols for decoding = 24*( fliFchDecodePCGIndexMinusOne +1)+24=384.
  */

} cdmafw_rc11_fch_decoder_done_ind_msg_t;

/*! @brief Adv1X Logging Buffer Ready
 *
 *   This is the message definition for the cdmafw ADV1X Logging
 *   Buffer Ready Indication.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  int16 logBufferReadIndex;
  /*<! Log Buffer Read Index
   *
   * SW must use this read index to read from the log buffer. The log buffer
   * is defined the SW readable SMEM region.
   *
   * \sa cdmafw_smem_read_intf_t.
   */

} cdmafw_adv1x_log_buffer_rdy_ind_msg_t;

/*! @brief VI mode control indication
 *
 * This message is used by CDMA FW to send indication messages
 * to VI thread.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
} cdmafw_vi_ctl_ind_msg_t;

/** @brief SSBI TQ Stuck Message
 *
 * \sa cdmafw_reset_req_ind_msg_t.
 */
typedef struct {
  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;
} cdmafw_reset_req_ind_msg_t;

/** @brief DEPRECATED. NOT USED ANYMORE.
 * Indicator message from FW after all the processing on all the
 * captures is done. At this point firmware has the compressed DPD tables in
 * its memory. Note the keyword "compressed". This means the DPD tables are
 * not in usable state by TxAGC code yet. Another indicator message will be
 * sent when the expanded tables are ready in firmware. *
 * \sa .
 *
 */
typedef struct {
  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;
} cdmafw_ept_proc_done_msg_t;

/** @brief Indicator message from FW after expanded DPD tables are available.
 * SW can start steps for sweep 3 on recieving this indicator because TxAGC
 * firmware has the DPD tables ready. In online mode, this might be used as
 * an indicator to firmware expansion finished within realtime deadline.
 * \sa .
 */
typedef struct {
  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;
  uint32                    sequenceNumber;
} cdmafw_ept_expansion_done_msg_t;

/**@}*/


/*============================================================================
 *
 * RESPONSE MESSAGES
 *
 * ==========================================================================*/
/**
 * \addtogroup cresp
 * @{
 */

/*!
 * @brief app_enable Search Response
 *
 * This is a message definition for the app_cgr response to be sent to SW to
 * indicate to SW that cdma APP has been enabled.
 *
 * @deprecated This message is deprecated in Nike-L.
 *
 */
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /** Current status of the CDMA app. */
  uint16                    statusEnabled:1;

  /*! \detailed If statusIgnored == 1, the previous app_cfg command is redundant.
      i.e cdma is already running (disabled) when firmware receives the enable (disable)
      spr message. */
  uint16                    statusIgnored:1;
  uint16                    cdmaVersion;

  /*! \brief CDMA shared mem start address and size */
  uint32                    cdmaSmemStartAddr;
  uint16                    cdmaSmemSize;
} cdmafw_app_cfg_rsp_msg_t;

/*! @brief Response to state config message.
 *
 * This message is sent in reponse to the STATE_CONFIG command.
 *
 * \sa cdmafw_state_cfg_msg_t
 */
typedef struct {
  /*! \brief Message Header */
  msgr_hdr_struct_type      hdr;
  /*! \brief present state of CDMA FW. */
  cdmafw_state_t             state;
  /*! \brief CDMA FW version. */
  uint16                    cdmaVersion;
  /*! \brief Physical address of the shared memory interface */
  uint32                    sharedMemoryAddr;
} cdmafw_state_cfg_rsp_msg_t;

/** @brief QLIC Walsh Power Logging Ready
 *
 *   This is the message definition for the cdmafw QLIC Walsh Power
 *   Logging Ready Response.
 *
 *   \sa cdmafw_qlic_walsh_pwr_logging_msg_t
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint32 sharedMemoryAddr;

} cdmafw_qlic_walsh_pwr_logging_rdy_rsp_msg_t;

/** DEMBACK_SW_CONFIG done response.

  FW sends this response once a DEMBACK_SW_CONFIG command has been processed.

  \sa cdmafw_demback_sw_config_msg_t

*/
typedef struct {

  /** MSGR header */
  msgr_hdr_struct_type hdr;

} cdmafw_demback_sw_config_rsp_msg_t;

/*! @brief 1x ASDIV Priority Change Response
 *
 *   This is the message definition for the cdmafw Priority Change
 *   after switch Response.
 */
typedef struct {

  /** MSGR header */
  msgr_hdr_struct_type hdr;

} cdmafw_asdiv_pri_change_rsp_msg_t;

/*! @brief Tx High Power Detection
 *
 *   This is the message definition for the cdmafw Tx High Power Detection
 *   Response.
 */
typedef struct {
  msgr_hdr_struct_type hdr;
  /*!< MSGR header */

  uint16 cTxPowerHighFlag;
  /*!< High Tx power flag */
} cdmafw_tx_high_pwr_det_rsp_msg_t;
/**@}*/

/*============================================================================
 *
 * IRAT COMMAND/RESPONSE
 *
 * ==========================================================================*/
/**
 * \addtogroup cirat
 * @{
 */

/**
    @brief Pilot Measurement Configuration Request Message.

    Software sends this message to configure for 1X pilot measurements.

    Pilot measurement configuration can be initiated immediately or based on
    start time in terms of 1X RTC (Cx1). The duration of the pilot measurement
    may be infinite or given in terms of 1X RTC (Cx1).

    Response: cdmafw_pilot_meas_cfg_rsp_msg_t.
    The response is generated based on either desired number of samples captured
    or end of the measurement window, whichever is earlier.
*/
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;
  /**   Bit(0) in half-word 0
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |                            startRtcCx1                                   |  I |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

      I : startImmediate. 1=Immediate - startRtcCx1 is ignored. 0=Start according
      to startRtcCx1.
      \endverbatim
  */
  uint16                    startImmediate:1;
  /**   Bit(15:1) in half-word 0
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |                            StartRtcCx1                                   |  I |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

      startRtcCx1 : Start time of pilot measurement configuration in terms of RTC Cx1.
      \endverbatim
  */
  uint16                    startRtcCx1:15;

  /**   Bit(0) in half-word 1
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |               durationRtcCx1                                             |  N |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

	N : durationInfinite. "Infinite" duration; durationRtcCx1 is ignored. FW will not
      release or disable any resources; FW will perform "tune" but not "tune back/away".
	Pilot measurement configuration response will be triggered solely based on
	desired numSamplesCaptured.

      durationRtcCx1 : Duration of pilot measurement gap in terms of RTC Cx1
	\endverbatim
  */
  uint16                    durationInfinite:1;

  /**   Bit(15:0) in half-word 1
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |               durationRtcCx1                                             |  N |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

      durationRtcCx1 : Duration of pilot measurement gap in terms of RTC Cx1
	\endverbatim
  */
  uint16                    durationRtcCx1:15;


  /**   Idle measurement search mode. Online search = 0, Offline search = 1. This flag is only used
   *    when durationInfinite is set. Regardless of search mode, rsp is sent after 4 half-slots worth
   *    of data is captured. In offline mode, SSI is stopped.
   */
  uint16                    idleOfflineSearch:1;


  uint16 :15;

  /**   Number of desired valid samples in sample RAM. Not used in idle mode.
   */
  uint16                    numSamplesDesired;

  /**   Initial value of RX rotator in FW units(same as rotAccum).
   */
  uint32                    rotError;


  /**   RF Tune Back Time Constant. Time before end of gap when RF tune back is started.
   */
  uint16                    rfTuneBackTimeConstant;


} cdmafw_pilot_meas_cfg_req_msg_t;

/** @brief Pilot Measurement Configuration Response Message

    Firmware generates this response to Pilot Measurement Configuration Request message:
    cdmafw_pilot_meas_cfg_rsp_msg_t.
 */
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /**   Number of samples captured after RX AGC acquisition.
  */
  uint16                    numSamplesCaptured;

  /**   Status/Error code (TBD)
   */
  uint16                    status;

  /**   Bit(15:0) in half-word 4
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |                        timeStampReqRtcCx2                                     |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

      timeStampReqRtcCx2 : 1X RTC timestamp when the Pilot Measurement Configureation
      request was received.
	\endverbatim
   */
  uint16                    timeStampReqRtcCx2;

  /**   Bit(15:0) in half-word 5
      \verbatim
        15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
      |                        timeStampRspRtcCx2                                     |
      +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

      timeStampRspRtcCx2 : 1X RTC timestamp when this Pilot Measurement Configuration
      response is sent.
	\endverbatim
   */
  uint16                    timeStampRspRtcCx2;

  /** RTC position in cx2 when streaming is stopped for connected mode
   * and idle offline mode, to be passed on to SRCH FW for offline
   * search*/
  uint16                    streamingStopRtcCx2;

  /**   Final value of RX AGC accumulator
   */
  uint32                    rxAgcAccum;

} cdmafw_pilot_meas_cfg_rsp_msg_t;

/** @brief Pilot Measurement Stop Stream Command

    At end of the idle mode measurement (in MEAS_CONNECTED state), to stop the sample
    streaming L1 must send this message before sending the state transition message
    to put firmware back to STANDBY state. This also results in firmware releasing
    the WB/NB chain allocated from FW CRM.

    Firmware assumes the chain to be stopped is the one started from PILOT_MEAS_CFG_REQ/RX_START_CFG
    sent eariler when the measurement was started.

    This message is not needed in connected mode measurement where stream is stopped automatically
    at gap end.

    Response: cdmafw_pilot_meas_stop_stream_rsp_t
    This is sent when the sample stream is stopped and RxFE chain released.
*/
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

} cdmafw_pilot_meas_stop_stream_msg_t;

/** @brief Pilot Measurement Stop Stream Response

    This is the response to the stop stream message. After receiving this message,
    software can put firmware to STANDBY state.
*/
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

} cdmafw_pilot_meas_stop_stream_rsp_msg_t;

/** @brief Message sent to RFSW after capturing a set of samples for EPT in
 * CAL mode.
 * \sa .  */
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /*! \brief DPDindex for which this capture was done */
  uint16  DPDindex;

  /*! \brief Sequence number for tracking request and response messages */
  uint32  sequenceNumber;

} cdmafw_ept_capture_rsp_msg_t;

/**@}*/

/**
 * \addtogroup cmux
 * @{
 */

/** @brief HW configuration for enabling MTVA viterbi decoder. Use this to
 * override FW default value for Yamamoto threshold and the number of trace
 * back paths in MTVA viterbi decoder HW block.  MTVA viterbi decoder.
 */
typedef struct {

  /* Yamamoto Threshold. Takes values 0 to 15 */
  uint8 mtva_ym_thresh:4;

  uint8 mtva_max_tracebacks:2;

} cdmafw_mtva_hw_config_t;

/** @brief Rate determination algorithm configuration message.

    Configures the RDA data tables and also enables MTVA and the
    corresponding MTVA RDA.
*/
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /*! \brief Enables Multi-trace viterbi algorithm and uses MTVA RDA for rate
   * determination. */
  boolean mtvaEnable;

  /*! \brief Loads the new RDA data tables from SW-writable shared mem . */
  boolean loadRdaDataTables;

  /*! \brief Loads the new HW config from mtvaHwConfig if TRUE. */
  boolean loadMtvaHwConfig;

  /*! \brief MTVA viterbi decoder HW configuration. see cdmafw_mtva_hw_config_t */
  cdmafw_mtva_hw_config_t mtvaHwConfig;

} cdmafw_rda_config_msg_t;

/** @brief Command to enable/disable QOF-based noise estimation for FPC.
*/
typedef struct {

  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /*! \brief Enables QOF-based noise estimation for FPC decision if this is
   * true.  Disables if false. */
  boolean qofFchNoiseEstimationEnable;

  /*! \brief Noise bias in Q15. Default is zero in FW. */
  int16 qlicNoiseBiasUint;

} cdmafw_qof_noise_est_fpc_config_msg_t;

/**@}*/

/**
 * \addtogroup cfwrxagc
 * @{
 */


/*!< LNA CAL Override
 *
 * This message is basically a combination of the legacy LNA_OVERRIDE, LNA_GAIN_OFFSET and RXAGC_ACQ_
 * MODE_CONFIG messages (to reduce messaging overhead). After a while, the legacy messages shall be
 * deprecated.
 *
 */
typedef struct {

  /*!< Message Header */
  msgr_hdr_struct_type hdr;

  /*!< Antenna to which this message applies */
  int32 antenna;

  /*!< Override Mode (1 => override, 0 => normal)
   *
   * LNA gain ofs and ACQ duration configs are ignored in normal mode.
   *
   */
  boolean overrideFlag;

  /*!< Override LNA state value. This state shall be applied at the next
   * RxAGC update */
  int32 overrideState;

  /*!< LNA CAL gain ofs override value in dB for the overriden state */
  int16 gainOfs;

  /*!< ACQ duration in 64-chip units. AGC shall switch to ACQ mode for this
   * duration starting from the next AGC update */
  uint32 acqDuration;

} cdmafw_rxagc_lna_cal_override_msg_t;


/*!
 * @brief RxAGC Acquisition Mode Configuration.
 *
 * This message is intended to be used in CAL to reduce the CAL segment duration. This
 * allows SW to configure AGC in ACQ mode briefly to allow a faster settling of AGC before
 * a measurement is taken.
 */
typedef struct {

  /*! @brief Message header */
  msgr_hdr_struct_type hdr;

  /*! @brief Antenna index to configure */
  uint32 antenna;

  /*! @brief ACQ duration to use */
  uint32 acqDuration;

} cdmafw_rxagc_acq_mode_config_msg_t;

/*!
 * @brief TxEPT Capture Command
 *
 * This message is intended to be issued by the Software with the
 * configurations of Tx/Rx Sample Capture process. One message per capture is
 * required. FW auto increments the capture index and returns it in
 * cdmafw_ept_capture_rsp_msg_t
 *
 */
typedef struct {
  /*! @brief Message header */
  msgr_hdr_struct_type hdr;

  /*! @ brief RxLM buffer index */
  uint16 rxlmBufferId;

  /*! @ brief WB chain index. Should be between [0,2] inclusive */
  uint16 rxChain;

  /*! @ brief TXC chain index. Should be 0 or 1*/
  uint16 txChain;

  /*! @ brief number of TXC/RX samples to capture.Must be a multiple of 1024(1K) */
  uint16 numSamples;

  /*! @ brief flag to do initialization.This must be set in 1st trigger*/
  boolean firstTrig;

  /*! @ brief flag to do de-initialization.This must be set in last trigger*/
  boolean lastTrig;

  /*! @ brief Indicate if firmware should do DPD estimation on the captured
   * samples.*/
  boolean doProcessing;

  /*! @ brief Sequence number to track request and response messages */
  uint32  sequenceNumber;
} cdmafw_ept_capture_msg_t;

/*!
 * @brief TxEPT Capture Command
 *
 * This message is intended to be issued by the Software for third sweep when
 * DPD should be enabled.
 *
 *
 */
typedef struct {
  /*! @brief Message header */
  msgr_hdr_struct_type hdr;

  /*! @ brief DPD row index corresponding to the current capture*/
  uint16  DPDindex;

  /*! @ brief Enable DPD override. */
  boolean dpdOverrideEnable;

} cdmafw_ept_override_msg_t;

/**@}**/

/**
 * \addtogroup clog
 * @{
 */

/*!
 *
 * @brief QXDM Logging Configuration.
 *
 * This message enables/disables a specified FW diag log packet.
 *
 */
typedef struct {


  /*!< Message header */
  msgr_hdr_struct_type hdr;


  /*!< QXDM Log packet ID */
  uint32 diagLogID;

  /*!< (0 => disable, 1 => enable) */
  boolean enable;

} cdmafw_diag_log_config_msg_t;

/*!
 * @brief RX mempool samples capture
 *
 * This message is intended to be issued by the Software
 * for capturing RX IQ samples
 */
typedef struct {

  /*! @brief Message header */
  msgr_hdr_struct_type hdr;

  /*! @ brief Antenna: Primary = 0, Diversity = 1 */
  uint16 ant;

} cdmafw_rx_iq_samples_logging_msg_t;

typedef struct {
   /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /*! \brief Threshold in linear scale*/
  uint32 jamDetThres[CFW_NUM_LNA_STATES-1];

  /*! \brief Threshold - Hysteresis in linear scale*/
  uint32 jamDetThresMinusHyst[CFW_NUM_LNA_STATES-1];

} cdmafw_jd_threshold_update_msg_t;

typedef struct {
  /*! \brief Message header */
  msgr_hdr_struct_type      hdr;

  /*! \brief Mode config for JD: 0 for enable/disable, 1 for freeze/unfreeze */
  uint16  jamFreezeMode;
  uint16  jdEnable;
} cdmafw_jd_mode_msg_t;


/**@}**/

/*============================================================================
 *
 * MESSAGE UNIONS
 *
 * ==========================================================================*/
/**
 * \addtogroup cunion
 * @{
 */

/*! \brief Union of all message received by firmware */
typedef union {
  /*! \brief Message header */
  msgr_hdr_struct_type                         hdr;
  cdmafw_finger_assign_msg_t                   finger_assign;
  cdmafw_multiple_fingers_assign_msg_t         multiple_fingers_assign;
  cdmafw_finger_config_msg_t                   finger_config;
  cdmafw_multiple_fingers_config_msg_t         multiple_fingers_config;
  cdmafw_finger_common_config_msg_t            finger_common_config;
  cdmafw_oqpch_search_done_msg_t               oqpch_search_done;
  cdmafw_afc_mode_msg_t                        afc_mode;
  cdmafw_xo_afc_static_config_msg_t            xo_afc_static_config;
  cdmafw_xo_afc_dynamic_config_msg_t           xo_afc_dynamic_config;
  cdmafw_fl_signaling_msg_t                    fl_signaling;
  cdmafw_apf_enable_msg_t                      apf_enable;
  cdmafw_fpc_freeze_cfg_msg_t                  fpc_freeze;
  cdmafw_fpc_override_msg_t                    fpc_override;
  cdmafw_qlic_walsh_pwr_logging_msg_t          qlic_walsh_pwr_logging;
  cdmafw_sch_acc_scale_msg_t                   sch_acc_scale;
  cdmafw_fch_target_setpoint_msg_t             fch_target_setpoint;
  cdmafw_sch_target_setpoint_msg_t             sch_target_setpoint;
  cdmafw_rl_mpp_cfg_msg_t                      rl_mpp_config;
  cfw_rl_pwr_est_filter_cfg_msg_t              rl_pwr_filter_config;
  cdmafw_rl_ack_test_mode_cfg_msg_t            rl_ack_test_mode_config;
  cdmafw_rl_signaling_msg_t                    rl_signaling;
  cdmafw_rl_legacy_rc_frame_config_msg_t       rl_legacy_rc_frame_config;
  cdmafw_rl_rc8_frame_config_msg_t             rl_rc8_frame_config;
  cdmafw_state_cfg_msg_t                       state_cfg;
  cdmafw_adv1x_log_config_msg_t                adv1x_log_config;
  cdmafw_pilot_meas_cfg_req_msg_t              pilot_meas_cfg_req;
  cdmafw_pilot_meas_stop_stream_msg_t          pilot_meas_stop_stream;
  cdmafw_tdec_brdg_config_msg_t                tdec_brdg_config;
  cdmafw_demback_mode_config_msg_t             demback_mode_config;
  cdmafw_demback_sw_config_msg_t               demback_sw_config;

  /*Rx AGC*/
  cfw_rx_agc_cfg_msg_t                         rx_agc_cfg;
  cfw_lna_override_msg_t                       lna_override;
  cfw_lna_gain_offset_msg_t                    lna_gain_offset;
  cfw_rx_agc_stop_msg_t                        rx_agc_stop_cfg;

  /* RFA commands */
  cdmafw_tx_pa_params_config_msg_t             tx_pa_params_config;
  cfw_tx_limit_config_msg_t                    tx_limit_config;
  cdmafw_tx_pa_range_override_msg_t            tx_pa_range_override;
  cdmafw_tx_override_msg_t                     tx_override;
  cdmafw_tx_closed_loop_override_msg_t         tx_closed_loop_override;
  cdmafw_tx_open_loop_override_msg_t           tx_open_loop_override;
  cdmafw_tx_pa_state_override_msg_t            tx_pa_state_override;
  cdmafw_rx_set_freq_offset_msg_t              rx_set_freq_offset;
  cdmafw_rxlm_config_masked_msg_t              rxlm_config_masked;
  /* RFA commands */

  /* RX/TX config commands */
  cfw_rx_start_cfg_msg_t                       rx_start_cfg;
  cfw_rx_start_msg_t                           rx_start;
  cfw_rx_stop_msg_t                            rx_stop;
  cfw_tx_start_msg_t                           tx_start;
  cfw_tx_stop_msg_t                            tx_stop;
  cfw_tx_dac_start_msg_t                       tx_dac_start;
  cfw_tx_dac_stop_msg_t                        tx_dac_stop;
  /* RX/TX config commands */

  /* TX AGC CFG*/
  cfw_tx_agc_cfg_msg_t                         tx_agc_cfg;

  /* HDET measurement request */
  cfw_tx_hdet_request_msg_t                    hdet_req;

  /* HDET configuration msg */
  cfw_tx_hdet_config_msg_t                     hdet_config;

  /* Intelliceiver update msg */
  cfw_intelliceiver_update_msg_t               intelliceiver_update;

  /* TXC dynamic update msg */
  cfw_dynamic_txc_update_msg_t                 dynamic_txc_update;

  /* RDA and MTVA config message */
  cdmafw_rda_config_msg_t                      rda_config;

  cdmafw_qof_noise_est_fpc_config_msg_t        qof_noise_est_fpc_cfg;

  cdmafw_rxagc_acq_mode_config_msg_t           rxagc_acq_mode_config;

  cdmafw_rxagc_lna_cal_override_msg_t          rxagc_lna_cal_override;

  cdmafw_xpt_capture_msg_t                     xpt_capture;

  cdmafw_stop_txagc_update_msg_t               stop_txagc_update;

  /* HDET reset MSG */
  cfw_tx_hdet_reset_msg_t                      hdet_reset;

  cfw_tx_dac_cal_config_msg_t                  dac_cal_config;

  /* EPT Capture MSG  */
  cdmafw_ept_capture_msg_t                     ept_capture;

  cdmafw_ept_override_msg_t                    ept_override;

  cfw_xpt_config_msg_t                         xpt_config;

  cfw_ept_config_msg_t                         ept_config;

  cfw_ept_sample_buffer_cfg_msg_t              ept_sample_buffer_cfg;

  /* RX IQ samples capture MSG*/
  cdmafw_rx_iq_samples_logging_msg_t           rx_iq_logging;

  /*!< QXDM diag logging configuration */
  cdmafw_diag_log_config_msg_t                 diag_log_config;

  cdmafw_jd_threshold_update_msg_t             jd_threshold_update;
  cdmafw_jd_mode_msg_t                         jd_mode;

  cfw_xpt_delay_update_msg_t                   xpt_delay_update;

  /*DSDA Priority command*/
  cdmafw_dsda_priority_config_msg_t            dsda_priority_config;
#ifdef FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY
  cfw_rf_execute_ant_switch_msg_t              rf_execute_ant_switch_msg;
#endif
 } cdmafw_msg_u;


/*! \brief Union of all indication messages recevied by software  */
typedef union {
  /*! \brief Message header */
  msgr_hdr_struct_type                         hdr;
  cdmafw_rxtx_frame_stats_ind_msg_t            rxtx_frame_stats;
  cdmafw_qlic_frame_stats_ind_msg_t            qlic_frame_stats;
  cdmafw_rc11_fch_decoder_done_ind_msg_t       rc11_fch_decoder_done;
  cdmafw_adv1x_log_buffer_rdy_ind_msg_t        adv1x_log_buffer_rdy;
  cdmafw_vi_ctl_ind_msg_t                      vi_ctl;
  cdmafw_reset_req_ind_msg_t                   reset_req;
  cdmafw_ept_proc_done_msg_t                   ept_proc_done;
  cdmafw_ept_expansion_done_msg_t              ept_expansion_done;
} cdmafw_msg_ind_u;


/*! \brief Union of all response messages recevied by software  */
typedef union {
  /*! \brief Message header */
  msgr_hdr_struct_type                         hdr;
  cdmafw_app_cfg_rsp_msg_t                     app_cfg;
  cdmafw_qlic_walsh_pwr_logging_rdy_rsp_msg_t  qlic_walsh_pwr_logging_rdy;
  cdmafw_state_cfg_rsp_msg_t                   state_cfg;
  cdmafw_tx_high_pwr_det_rsp_msg_t             tx_high_pwr_det;
  cdmafw_pilot_meas_cfg_rsp_msg_t              pilot_meas_cfg;
  cdmafw_pilot_meas_stop_stream_rsp_msg_t      pilot_meas_stop_stream_rsp;
  cfw_rx_start_rsp_msg_t                       rx_start_rsp;
  cfw_dac_cal_config_rsp_msg_t                 dac_cal_rsp;
  cdmafw_ept_capture_rsp_msg_t                 ept_capture_rsp;
  cdmafw_xpt_capture_rsp_msg_t                 xpt_capture_rsp;
  cfw_pa_params_rsp_msg_t                      pa_params_rsp;
  cfw_xpt_trans_compl_rsp_msg_t                xpt_trans_compl_rsp;
  cdmafw_demback_sw_config_rsp_msg_t           demback_sw_config_rsp;
  cfw_xpt_config_rsp_msg_t                     xpt_rsp;
  cdmafw_asdiv_pri_change_rsp_msg_t            asdiv_pri_change_rsp;


} cdmafw_msg_rsp_u;


#define CDMAFW_MAX_MSG_SIZE       sizeof(cdmafw_msg_u)
#define CDMAFW_MAX_IND_MSG_SIZE   sizeof(cdmafw_msg_ind_u)
#define CDMAFW_MAX_RSP_MSG_SIZE   sizeof(cdmafw_msg_rsp_u)

#define CDMAFW_NUM_CMD_MSG        (CDMA_FW_CMD_LAST - CDMA_FW_CMD_FIRST + 1)
#define CDMAFW_NUM_IND_MSG        (CDMA_FW_IND_LAST - CDMA_FW_IND_FIRST + 1)
#define CDMAFW_NUM_RSP_MSG        (CDMA_FW_RSP_LAST - CDMA_FW_RSP_FIRST + 1)

/**@}*/

/*============================================================================
 *
 * SHARED MEMORY INTERFACE
 *
 * ==========================================================================*/
/**
 * \addtogroup cshared
 * @{
 */

/** Finger Status Interface.  Content in this shared memory
    buffer is updated every 64 chips. */
typedef struct ALIGN(8)  {
    int16   ComPilotRawQ;
    int16   ComPilotRawI;
    int16   DivPilotRawQ;
    int16   DivPilotRawI;
    int32   ComFiltRSSI;
    int32   DivFiltRSSI;
    int16   ComPilotFiltQ;
    int16   ComPilotFiltI;
    int16   DivPilotFiltQ;
    int16   DivPilotFiltI;
    int16   MmseFiltA11I;
    int16   MmseFiltA01I;
    int16   MmseFiltA01Q;
    int16   MmseFiltA00I;
    boolean ComFingLock;
    boolean DivFingLock;
    boolean ComCombLock;
    boolean DivCombLock;
} cdmafw_finger_stats_interface_t;

/** AFC Status Interface.  Content in this shared memory
    buffer is updated every 64 chips. */
typedef struct {
    /*! VCO Frequency Accum */
    int32 afcFreqAccVco;
    /*! ROT Frequency Accum */
    int32 afcFreqAccRot;
    /*! Filtered AFC Frequency Error */
    int32 afcFreqErrorFilt;
    /*! Combined Pilot Ec/Io over locked fingers */
    int32 afcCmbPilotEcio;
} cdmafw_afc_stats_interface_t;

/** QPCH Status Interface. */
typedef struct {
    int16 qpIndI;
    int16 qpIndQ;
    int16 qpThresh1;
    int16 qpThresh2;
} cdmafw_online_qpch_stats_interface_t;

/*!< RPC Status Interface.
 *
 * Contents of this SMEM location are updated every Frame
 *
 */
typedef struct {
    /*!< RPC history
     *
     * RPC history for the previous frame (frame offset aligned). The format is:
     *
     * BIT(15-i) = 1 => RPC down command received in PCG i.
     * BIT(15-i) = 0 => Either RPC up command was received @ PCG i, or RPC bit is not
     *                  valid for this PCG (due to 1xA reduced rate RPC, RPC erasures etc.,)
     */
    uint16 rpcHistory;
    /*!< RPC History Valididy.
     *
     * RPC validity for the previous frame (frame offset aligned). The format is:
     *
     * BIT(15-i) = 1 => RPC command received in PCG(i) is valid.
     * BIT(15-i) = 0 => RPC command received in PCG(i) is invalid (due to 1xA reduced rate RPC,
     *                  RPC erasures etc.,)
     */
    uint16 rpcHistoryValid;
    /*!< FPC History
     *
     * Stores the past 16 FPC decisions.
     *
     * BIT(i) = 1 => FPC down command was transmitted.
     * BIT(i) = 0 => Either FPC up command was transmitted, or no FPC command was transmitted.
     *
     */
    uint16 fpcHistory;
    /*!< FPC History Validity
     *
     * Stores the validity for the past 16 FPC decisions.
     *
     * BIT(i) = 1 => FPC history for BIT(i) is valid.
     * BIT(i) = 0 => FPC hisotry for BIT(i) is invalid.
     */
    uint16 fpcHistoryValid;
    /*!< RPC History per Cell
     *
     * Same as rpcHistory, but has information for each PC cell.
     *
     */
    uint16 rpcHistoryCell[CDMAFW_NUM_PWR_CTL_CELLS];
    /*!< Frame Syncronized Closed Loop Adjust
     *
     * This gain adjust parameter is latched once per frame @ the frame boundary. This is expected
     * to be synchronous with RPC history.
     *
     * Units are -1/512 dB.
     *
     */
    int16  cTxGainAdjVal;
} cdmafw_power_control_stats_interface_t;

/** MUX Interface.  Content in this shared memory buffer
    is updated every frame. */
typedef struct {

  /* DO NOT change order of any variables in this structure */
  int16 ch0AccFrame;
  int16 pcBitAccFrame;
  int16 fundWeightedPilotAccFrame;

  int16 ch1AccFrame;
  int16 ch2AccFrame;
  int16 suppWeightedPilotAccFrame;
  int16 suppWeightedNtMagAccFrame;
  int16 fundWeightedNtMagAccFrame;

  /*!< Output of the SVDO HE filter (-1/640 dBm units). */
  int16 rliHEFilterOutput;

} cdmafw_mux_read_interface_t;

/*! @brief RC11 FCH Decoder Done Info.
 *
 * This is the SMEM structure that contains RC11 FCH decoder information. This
 * structure is populated sometime within PCG0 of a frame for the decode that
 * happened in the previous frame.
 *
 */
typedef struct {

  uint32 fliRC11FchRateInfo:5;
  /*!< RC 11 F-FCH rate determination result
    @verbatim
    0: Erasure
    1: Eighth rate
    2: Quarter rate
    3: Half rate
    4: Full rate
    @endverbatim
  */
  uint32 fliValidFPCFrame:1;
  /*!< Valid FPC frame flag
    @verbatim
    0: SW should not use this frame for channel supervision or adjusting FPC set point.
    1: SW should use this frame for channel supervision and adjusting FPC set point.
    @endverbatim
	FW does not determine whether its erasure or blank frame. Based on the frame counter,
	frame offset and the configuration of fliBlankingPeriod, FW determines whether its a
	guaranteed non-null-rate frame or not.
  */

  uint32 fliFrameIndex:3;
  /*!< Frame Counter
   * A 3-bit frame count for the decoded frame.
   */

  uint32 fliFchDecodePCGIndexMinusOne:4;
  /*!< Decoded PCG Indicator
     SW shall use (fliFchDecodePCGIndexMinusOne+1) for PCG index in MAC counter.

     If (fliFchDecodePCGIndexMinusOne != 14),  this means:
     - Early decoding happens at symbol 16 of Rx PCG (fliFchDecodePCGIndexMinusOne +1) for the current frame.
       There's no early (partial) decoding at symbol 16 of Rx PCG0, PCG1, PCG14, PCG15.
     - The number of symbols for decoding = 24*( fliFchDecodePCGIndexMinusOne +1)+16
     If (fliFchDecodePCGIndexMinusOne == 14), this means:
     - Full frame decoding happens at symbol 0 of Rx PCG 0 for the previous frame;
     - The number of symbols for decoding = 24*( fliFchDecodePCGIndexMinusOne +1)+24=384.
  */

} cdmafw_rc11_fch_decode_done_info_t;

/*! @brief FPC Freeze Info */
typedef struct {

  uint16   flFchFpcUpCnt:16;
  /*!< Count of FPC up decisions */

  uint16   fliFchFpcFreezeFlag:16;
  /*!< FPC Freeze Flag */

} cdmafw_fl_fpc_freeze_info_t;

/*! @brief RC11 ACK log buffer */
typedef struct {

  /*!< Index (0/1) of ACK log buffer presently updated by FW.
   *
   * SW must use (ackLogBufIndex^1) to index the ackLogBuf buffer as FW is using ackLogBufIndex
   * to index ackLogBuf.
   *
   */
  uint16 ackLogBufIndex;

  /** Log Buffer
   *
   * Each element below is a 16-bit quantity. There are two 32-bit words per cell in the ACK log
   * buffer.
   *
   * \verbatim
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |                               scaledFchAckSym (celln)                         |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |                               scaledPwrTh1  (cell n)                          |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |                 cellNAckDecision (cell n)  (0 => no ACK, 1 => ACK)            |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |                                  NtFilt (cell n)                              |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
	  \endverbatim
  */

  uint32 ackLogBuf[CDMAFW_NUM_ACK_LOG_BUFFERS][CDMAFW_ACK_FRAME_LOG_SIZE];

} cdmafw_fl_rc11_ack_log_buffer_t;


/*! @brief RDA data Tables
 *
 * SMEM interface definition for RDA data tables provided to L1 SW. These will
 * be latched whenever cdmafw_rda_config_msg_t message is recieved with the
 * loadRdaDataTables field set to TRUE.
 *
 * serFETThreshold table is special. This table is different between legacy
 * RDA and the newer MTVA-RDA. Therefore, if mtvaEnable field of
 * cdmafw_rda_config_msg_t is TRUE, then this table should contain valuse
 * specifc for MTVA-RDA. Otherwise, it should contain values for legacy RDA.
 *
 * @note MUST be cache-aligned.
 *
 */
typedef struct ALIGN(32) {

  int16 slopeThreshold[CDMAFW_NUM_RATE_HYPOTHESES*CDMAFW_NUM_DECODE_ATTEMPTS][CDMAFW_NUM_RATE_HYPOTHESES] ;
  int16 interceptThreshold[CDMAFW_NUM_RATE_HYPOTHESES*CDMAFW_NUM_DECODE_ATTEMPTS][CDMAFW_NUM_RATE_HYPOTHESES] ;
  int16 energySelfThreshold[CDMAFW_NUM_DECODE_ATTEMPTS][CDMAFW_NUM_RATE_HYPOTHESES] ;
  int16 serThreshold[CDMAFW_NUM_DECODE_ATTEMPTS][CDMAFW_NUM_RATE_HYPOTHESES] ;
  int16 serFETThreshold[CDMAFW_NUM_DECODE_ATTEMPTS][CDMAFW_NUM_RATE_HYPOTHESES] ;
  int16 serFrameEndThreshold[CDMAFW_NUM_RATE_HYPOTHESES] ;

} cdmafw_rda_data_tables_t;

/*! @brief CDMA TX SMEM Read Interface
 *
 * Read Interface for CDMA TxAGC.
 *
 */
typedef struct ALIGN(32) {

  /* TxAGC Logical Updates */

  /*!< TX Open Loop (-1/640 dB units) */
  int16 cTxOpenLoopVal;
  /*!< TX Rate Adjust  (-1/640 dB units) */
  int16 cTxRateAdjVal;
  /*!< TX Gain Adjust (-1/512 dB units) */
  int16 cTxGainAdjVal;
  /*!< TX total (-1/640 dB units) */
  int16 cTxTotalGainVal;
  /*!< Filtered TX total for SV headroom estimation (-1/640 dB units).
   *
   *  The HE filter must be configured first using RL_PWR_FILTER_CONFIG message.
   *
   *  \sa cfw_rl_pwr_est_filter_cfg_msg_t
   */
  int16 rliHEFilterOutput;

  /*!< Filtered TX total for C+G Coex TX filter (-1/640 dB units).
   *  See cfw_rl_pwr_est_filter_cfg_msg_t to configure this filter. See the same
   *  for FW's default filter time constant.
   */
  int16 CoexTxFiltOutput;

  /*!< TX MAX limiting flag */
  boolean cTxLimitingMin;
  /*!< TX MIN limiting flag */
  boolean cTxLimitingMax;

  /*!< Current PA State */
  int16 cPaStateValue;
  /*!< TX RFA LUT index */
  int16 cTxLutIndex;
  /*!< Analog Power */
  int16 cTxAnalogPwr;
  /*!< BetaP Gain (-1/640 dB) */
  int16 cTxBetapGain;

  /* TxAGC Physical Updates */

  /*!< PA Range */
  int16 cTxPaRangeValue;
  /*!< AVGA Gain Word written to SSBI interface */
  int32 cTxAgcAdjVal;
  /*!< BetaP Linear Multiplier value written to HW register*/
  int16 cTxBetapVal;
  /*!< TX SMPS APT PDM value written to HW register */
  int32 cTxAptValue;
  /*!< Filtered RX AGC (1/640 dBm units).
   *  Configure the filter gain in cfw_rx_agc_cfg_msg_t::agc_filt_gain.
   * DEPRICATED NOW, DONT READ THIS VARIABLE, USE rxAgcFiltPwr instead.
   */
  int16 rxFiltPwr;

  /*!< Filtered RX AGC per antenna (1/640 dBm units).
   *  Configure the filter gain in cfw_rx_agc_cfg_msg_t::agc_filt_gain.
   */
  int16 rxAgcFiltPwr[CDMAFW_NUM_ANTENNAS];

  cfw_xpt_rf_rd_intf_t  xptRfRdInfo;

} cdmafw_rf_read_interface_t;

/*! @brief Walsh Power Logging SMEM Interface
 */
typedef struct ALIGN(32)  {
    uint32 walshPower[WALSH_LOG_BUF_LEN/4];
} cdmafw_qlic_walsh_pwr_read_interface_t;

/*! @brief Rx AGC Report interface
 *
 *   This is the message definition for the cdmafw Rx AGC
 *   report interface.
 */
typedef struct ALIGN(32) {

  /*!< RxAGC(dBm) = -63.8 + Rx_AGC_Accum*102.4/65536 */
  uint16                  agcValue;
  /*!< LNA decision from LNA controller FSM */
  uint16                  lnaDecision;
  /*!< AGC acquisition down counter in units of 64 chips
   * The AGC moves to track mode when counter =0 */
  int32                   acqTimeDnCntr;


} cdmafw_rx_agc_report_interface_t;

/*!
 * @brief Difference between STMR time and Sample count.
 */
typedef struct {

  /** Frame component */
  uint32 frameCount;
  /** PCG component */
  uint32 pcgCount;
  /** Chip component */
  uint32 chipCount;

} cdmafw_tx_stmr_sample_cnt_diff_t;


/*!
 * @brief Real-time TX timing info.
 *
 * This structure disseminates useful real-time TX timing information.
 *
 */
typedef struct {

  /** TX Advance Counter */
  uint32 advanceCount;

  /** TX Retard Counter */
  uint32 retardCount;

  /** Difference between STMR and TX Sample Counter */
  cdmafw_tx_stmr_sample_cnt_diff_t stmrSampleCntDiff;

} cdmafw_tx_timing_info_t;

/*! \brief RX_STOP timed trigger status */
typedef enum {
  OQPCH_STOP_SUCCESS,
  OQPCH_STOP_FAILURE
} cmdafw_timed_rx_stop_status_e;

/*!
 * @brief Timed RX_STOP info
 */

typedef struct {
  /*! Validity of RX_STOP trigger time for timed trigger. Set to _FAILURE if RX_STOP trigger time
      has expired or is >13.33ms in the future */
  cmdafw_timed_rx_stop_status_e timed_rx_stop_status;

  /*! RX_STOP trigger RTC minus RTC when FW received RX_STOP in Cx1. Valid only when
      timed_stop_status = _FAILURE */
  int16                         stop_time_delta_cx1;

} cdmafw_timed_rx_stop_info_t;

/** CH0 Symbol Buffer Info.
  */
typedef struct {

  /** Ping-Pong Buffer of CH0 Symbols.

      Bits 5:0   => CH0 Q-Soft Decision.
      Bits 13:8  => CH0 I-Soft Decision.

    */
  uint16 symbolBuffer[2][CDMAFW_NUM_SYMBOLS_PER_FRAME];

  /** Current FW Ping-Pong Buffer Write Index (0, 1) */
  uint32 fwBufferWrIdx;

  /** Current FW Symbol Write Index (0, ..., CDMA1X_NUM_SYMBOLS_IN_FRAME-1) */
  uint32 fwSymbolWrIdx;

  /** Current FW Frame Index */
  uint32 fwFrameIdx;

} cdmafw_ch0_symbol_buffer_intf_t;

/** Software Readable Shared Memory Interface Structure. Firmware only writes to this structure. */
typedef struct ALIGN(32)  {

  /* SMEM parameters updated every 64 chips in combiner interrupt */
  cdmafw_finger_stats_interface_t         fingerStats[CDMAFW_NUM_FINGERS];
  cdmafw_afc_stats_interface_t            afcStats;
  cdmafw_rx_agc_report_interface_t        rxAgcReport[CDMAFW_NUM_ANTENNAS];

  /*! @brief RxAGC log information for L1
   * rxAgcLog [0][0..15] - RxAGC Value for primary antenna - PCGs->0 to 15
   * rxAgcLog [1][0..15] - RxAGC Value for secondary antenna - PCGs->0 to 15
   * For a disabled chain, RxAGC is always frozen from the last updated value.
   * */
  int16                                   rxAgcLog[CDMAFW_NUM_ANTENNAS][CDMAFW_NUM_PCG_PER_FRAME];

  /* MUX parameters updated every frame */
  cdmafw_mux_read_interface_t             muxRead;

  /* RPC information updated every frame */
  cdmafw_power_control_stats_interface_t  powerControlStats;

  /* FL FPC freeze information update very FPC interval */
  cdmafw_fl_fpc_freeze_info_t             flFpcFreezeInfo;

  /* SMEM parameters updated every 192 chips in TX interrupt */
  cdmafw_rf_read_interface_t              rf_rd;

  /* Walsh power data updated every time a walsh power measurement is requested */
  cdmafw_qlic_walsh_pwr_read_interface_t  walshPowerData;

  /* QPCH stats updated every time a QPCH bit is received */
  cdmafw_online_qpch_stats_interface_t    qpchStats;

  /* Decode done parameters updated at PCG0 for previous frame */
  cdmafw_rc11_fch_decode_done_info_t      adv1xDecodeDoneInfo;

  /* RC11 FCH ACK log buffer - updated every frame */
  cdmafw_fl_rc11_ack_log_buffer_t         ackLogBuffer;

  /* Difference in Cx8k between STMR & VSRC (updated every frame) */
  uint32                                  stmrVsrcDiffCx8k[CDMAFW_NUM_ANTENNAS];

  /*! \brief Number of HDET requests successfully serviced by FW from the
   * start of time. Will wrap-around to 0 after 2^31 -1 . SW should read this
   * value before sending a HDET request to FW and expect the incremented
   * value when FW finishes servicing the request. FW never resets this. So
   * the value is non-volatile between TX reinits. The only time this is
   * cleared is when the shared mem is cleared.*/
  uint32                                  numHdetServiced;

  /*! @brief Indicates if a HDET request is sucessfully serviced. SW should
   * first wait for the numHdetServiced to be incremented and then read this
   * variable.
   */
  boolean                                 hdetSuccess;

  /*! @brief The "frozen" tx total power value during the HDET measurement
   * process.
   */
  int16                                   txHdetTotalPwr;

  /*! @brief Log Buffer.
   *
   * FW stores 8 PCG worth of data in a single log buffer. There are two log buffers
   * that are used in a ping-pong fashion (FW writes to one log buffer while SW reads
   * from the other. The log buffer structure is shown below.
   *
   * \verbatim
   * Time Stamp and TxTotal.
   *
   *    15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |                                  txTotal                                      |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  | frame count  |   PCG count       |              4-chip count                  |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   * RL digital gains (incl BetaPGain), and Open Loop.
   *
   *   15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                         Pilot Channel Gain                                    |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                         ACK Channel Gain                                      |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   *   15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                         FCH Channel Gain                                      |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                         SCH Channel Gain                                      |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   *   15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                   TX Open Loop Power (1/640 dBm)                              |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                         txBetaPGain (log)                                     |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   * txRateAdj & Closed Loop Power.
   *
   *   15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                   TX Closed Loop Power (1/512 dB)                             |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   * |                             TxRateAdj                                         |
   * +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   * "The Others"
   *
   *    15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  |///////////////////////////////////////////////////////////| flRC11FchRateInfo |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *  | VF |   PA    | RPCmsk  | RPCraw  | RPCrat  | SA | FA |   FPC   | LN | LX | AS |
   *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
   *
   *  AS - ACK suppressed ?
   *  LX - txLimitingMax ?
   *  LN - txLimitingMin ?
   *  FPC - fpcDecision
   *  FA - FCH ACK received ?
   *  SA - SCH ACK received ?
   *  RPCrat - RPC ratcheted
   *  RPCraw - RPC raw
   *  RPCmsk - RPC masked
   *  PA - PA state
   *  VF - flValidFPCFrame
   *  \endverbatim
   */
  uint32 adv1xHalfPcgLogBuffer[CDMAFW_LOG_NUM_BUFFERS][CDMAFW_LOG_BUFFER_SIZE];

  /*!
   * @brief TX timing info when DAC_START message is received.
   *
   * This structure contains useful TX timing information when TX DAC_START message is
   * received.
   */
  cfw_tx_dac_start_timing_info_t dacStartTimingInfo;

  /*!
   * @brief Real-time TX timing info.
   *
   * This structure disseminates useful real-time TX timing information.
   *
   */
  cdmafw_tx_timing_info_t txTimingInfo;

  /*! @brief Timed RX_STOP info (status: _SUCCESS/_FAILURE)
   */
  cdmafw_timed_rx_stop_info_t timed_rx_stop_info;

  // status of of FW TxAGC . TRUE if TxAGC is stopped. FALSE if TxAGC is
  // running. This flag is set when the cdmafw_stop_txagc_update_msg_t from
  // RFSW takes affect in FW. SW can poll on this flag to wait on FW TxAGC
  // status change after sending a cdmafw_stop_txagc_update_msg_t MSG.
  boolean stopTxAgcUpdates;

  int16 fwRfJDAlgoStatus;   /* Jammer Detection HW status */
  int16 jamDetectedFlag;    /* Jammer Dtected Flag to be read by RFSW */

#ifdef FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY
  uint16 txLimitingMaxStat; /* AsDiv algo, counter for Tx pwr > Tx max */
#endif

  /** CH0 Symbol Buffer SMEM - see struct for more details */
  cdmafw_ch0_symbol_buffer_intf_t ch0SymBufferInfo;

}cdmafw_smem_read_intf_t;

/** Software Writable Shared Memory Interface Structure. Firmware only reads from this structure. */
typedef struct ALIGN(32)  {
    //XXX add real write interface later.
    uint16                                  channelConfig;

    /*!< 1X Debug Logging Interface */
    ALIGN(32)  uint8     logEnable;

    /*!< LNA interface */
    cfw_rf_LNA_interface_t                  cfw_lna_cfg[CDMAFW_NUM_ANTENNAS];

    /*!< RDA data tables FW uses in the Rate determination algorithm */
    cdmafw_rda_data_tables_t                rda_data;

#if 0
    /*!< Shared memory write interface for template group configuration */
    cfw_tx_tpl_grp_cfg_union_t              tplGroupCfgUnion;

    /*! RF event storage */
    cfw_rf_events_set_t                     rfEvents;

    /*!  @brief (IQ scale,dBOffset) for cdma1x.
     * cdma1x[0] - Pilot only
     * cdma1x[1] - RC1/2
     * cdma1x[2] - RC8, voice only
     * cdma1x[3] - RC8, data
     * cdma1x[4] - Others, voice only
     * cdma1x[5] - Others, data */
    ALIGN(32) cfw_tx_iq_scale_data_t iqScaleData[CFW_IQ_GAIN_DATA_SIZE_CDMA1X];

    /* ******************** */
    // RK:new Lin table

    /* Dynamic Linearizer data in packed format.  This is dynamic portion of
     * the linearizer table that is updated after Temp Comp or HDET updates.
     * FW latches this */
    cfw_rfa_lut_tables_packed_t             dynamic_lin_packed;

    /*! XPT bank size information */
    cfw_xpt_bank_size_info_t                xpt_bank_size_info;

    /*! Static portion of the linearizer data */
    cfw_static_lin_table_t                  static_lin_table;

    /*! Tx power column of the linearzer data */
    cfw_rfa_lin_tx_pwr_t                    lin_pwr_table;

    /*! Two sets of PA state thresholds. One for APT/EPT and one for ET
     * [0] is for APT & EPT modes. [1] is for ET mode */
    cfw_pa_state_thresholds_t               xpt_pa_thresh[2];

  // RK:new Lin table end
    /* ******************** */
#else
    /** CDMA TX Template configuration
        @deprecated No longer required for FED.
    */
    cfw_tx_tpl_grp_cfg_union_t              tplGroupCfgUnion __attribute__((deprecated));

    /** Storage for RF events.
        @deprecated No longer required for FED.
    */
    cfw_rf_events_set_t                     rfEvents __attribute__((deprecated));

    /** I/Q Scale/Offset Data for CDMA1x.
        @deprecated No longer required for FED.
    */
    ALIGN(32) cfw_tx_iq_scale_data_t iqScaleData[CFW_IQ_GAIN_DATA_SIZE_CDMA1X] __attribute__((deprecated));

    /** C2K Linearizer Table. @deprecated */
    cfw_rfa_lut_tables_packed_t             dynamic_lin_packed __attribute__((deprecated));

    /** XPT bank size information. @deprecated */
    cfw_xpt_bank_size_info_t                xpt_bank_size_info __attribute__((deprecated));

    /** Static portion of the linearizer data. @deprecated */
    cfw_static_lin_table_t                  static_lin_table __attribute__((deprecated));

    /** Tx power column of the linearzer data. @deprecated */
    cfw_rfa_lin_tx_pwr_t                    lin_pwr_table __attribute__((deprecated));

    /** APT/ET PA state thresholds. @deprecated */
    cfw_pa_state_thresholds_t               xpt_pa_thresh[2] __attribute__((deprecated));

    boolean forceUnlockDivFing;
#endif
} cdmafw_smem_write_intf_t;

/** Structures to peek and poke FW internal variables for
 *  debug purposes */
typedef struct {
  uint32 var_update;
  uint32 var;
} cfw_dbg_poke_format_t;

typedef struct ALIGN(32) {
  cfw_dbg_poke_format_t poke_var[CFW_POKE_VAR_SIZE];
} cfw_dbg_poke_intf_t;

typedef struct ALIGN(32) {
  uint32 var[CFW_PEEK_VAR_SIZE];
} cfw_dbg_peek_intf_t;

/** Shared Memory Interface Structure. The shared memory should be accessed using
    a pointer of this structure type. */
typedef struct {
    cdmafw_smem_read_intf_t                 read;
    cdmafw_smem_write_intf_t                write;
    cfw_dbg_peek_intf_t                     peek;
    cfw_dbg_poke_intf_t                     poke;
} cdmafw_shared_memory_interface_t;
/**@}*/

/*-----------------------------------------------------------------------*/
/*     REMOVED INTERFACE VARIABLES                                       */
/*-----------------------------------------------------------------------*/
/**
   \defgroup riv Removed Interface Variables
   CDMA1x FW removed interface variables */
/*@{*/

/*! @brief Removed Interface Variables
 *
 *   This is the list of removed interface variables.
 */
typedef struct {
  uint16 TdShift[CDMAFW_NUM_FINGERS];
  /*!< obsolete */
  uint16 LockInit[CDMAFW_NUM_FINGERS];
  /*!< un-used */
  uint16 mmseRyyFeedFwdGain;
  /*!< un-used */
  uint16 mmseRyyFeedBckGain;
  /*!< un-used */
  uint16 ttInit[CDMAFW_NUM_FINGERS];
  /*!< one configuration only */
  uint16 rssiFiltGain;
  /*!< one configuration only */
  uint16 fingUpperLockThresh;
  /*!< one configuration only */
  uint16 fingRelativeLockThresh;
  /*!< one configuration only */
  uint16 fingLockAlgorithm;
  /*!< one configuration only */
  uint16 cdma1xPassThruEnable;
  /*!< obsolete */
  uint16 txRateAdjLatchOffset;
  /*!< obsolete */
  uint16 ntIoFiltGain;
  /*!< one configuration only */
  uint16 pcFiltGain;
  /*!< one configuration only */
} cdmafw_removed_variables_t;
/*@}*/



#endif /* CDMAFW_MSG_H */

