/*!
  @file
  cfw_rf_templates.h

  @brief
  C2K Firmware RF Control Template Groups Definitions

  @detail

*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/c2k/api/cfw_rf_templates.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/
#include "ccs_rf_intf.h"

#ifndef _CFW_RF_TEMPLATES_H_
#define _CFW_RF_TEMPLATES_H_

/** \defgroup root RF TX Control Template Groups */ /*\{*/

/*! CFW RF TX Template Group Type Enum
 
    Terminology:
    "Template" - An array of CCS tasks. Usually created compile time statically
                 initialized.
    "Template Payload" An array of CCS task payloads corresponding to a particular
                 template. A payload structure by itself has no meaning.

    "Template Config" - A structure used by RF SW to configure a particular template.

    "(Template) Group" - A group of templates corresponding to one type of RF configuration
                         supported by FW

    "PA Change" - Groups of CCS tasks to be executed when PA state is updated. This includes RGI and
                  PA.

    "RGI Change" - Groups of CCS tasks to be executed when only RGI changes (without PA change).
                   This includes AVGA, APT etc

*/

/**
 * \defgroup cfwrftplconst RF Template Constants
 */

/** 
 * \addtogroup cfwrftplconst RF Template Constants 
 * @{
 */
#define CFW_RF_TX_TPL_AVGA_NUM_SSBI     4
#define CFW_RF_TX_TPL_PA_NUM_RFFE       8
#define CFW_RF_TX_TPL_APT_NUM_RFFE      8
#define CFW_RF_TX_TPL_PA_Q_CUR_NUM_RFFE 8
#define CFW_RF_TX_TPL_PA_NUM_SSBI       3

/**@}*/


typedef enum {

  /*! \brief Group 0 */
  CFW_RF_TX_TPL_GROUP_0 = 0,
  
  CFW_RF_TX_TPL_GROUP_1 = 1,
  
  CFW_RF_TX_TPL_GROUP_2 = 2, 
  
  CFW_RF_TX_TPL_GROUP_3 = 3,

  CFW_RF_TX_TPL_GROUP_4 = 4,
  
  CFW_RF_TX_TPL_GROUP_LAST = CFW_RF_TX_TPL_GROUP_4

} cfw_tx_tpl_grp_type_t;

  
/*! \brief Number of groups *defined*. Supported or not depends on #defs below. */
#define CFW_RF_TX_TPL_NUM_GROUPS ((int32)CFW_RF_TX_TPL_GROUP_LAST + 1)

typedef enum {

  CFW_RF_TX_TPL_SEQ_L2H = 0,
  
  CFW_RF_TX_TPL_SEQ_H2L

} cfw_tx_tpl_seq_type_t;

/*! Number of sequences supported = 2, 0 = L2H 1 = H2L */
#define CFW_RF_TX_TPL_NUM_SEQ_TYPE ((int32)CFW_RF_TX_TPL_SEQ_H2L + 1)

/*===========================================================================
  Notes regarding ccs_rf_task_t payload usage

  Since the templates are exclusively used for sequence generation, i.e. never
  intended to be submitted directly to ccs driver for enqueue, the payload
  (e.g. ssbi address/data pairs) are stored in Q6 memory and specified as 
  Q6 virtual address in .transaction_ptr fields

============================================================================*/

/*===========================================================================

  Feature definition for supported template groups
  At least 1 template group must be supported !!

============================================================================*/
#define CFW_RF_TEMPLATE_TX_GRP_0_SUPPORTED
#define CFW_RF_TEMPLATE_TX_GRP_1_SUPPORTED
#define CFW_RF_TEMPLATE_TX_GRP_2_SUPPORTED
#define CFW_RF_TEMPLATE_TX_GRP_3_SUPPORTED
#define CFW_RF_TEMPLATE_TX_GRP_4_SUPPORTED

/*===========================================================================

  Actual Template Group Headers

============================================================================*/

#include "cfw_tx_tpl_grp_0.h"

#include "cfw_tx_tpl_grp_1.h"

#include "cfw_tx_tpl_grp_2.h"

#include "cfw_tx_tpl_grp_3.h"

#include "cfw_tx_tpl_grp_4.h"


/*===========================================================================

                        Template Group configuration union

============================================================================*/
/*! Configuration structures for all template groups unioned. Instantiated in
    the shared memory */
typedef union {
 
  /*! Template Group Type */
  cfw_tx_tpl_grp_type_t  group_type;

  /*! Structures for group 0 */
  cfw_tx_tpl_grp_0_cfg_t    group0_cfg;

  /*! Structures for group 1 */
  cfw_tx_tpl_grp_1_cfg_t    group1_cfg;

  /*! Structures for group 2 */
  cfw_tx_tpl_grp_2_cfg_t    group2_cfg;

  /*! Structures for group 3 */
  cfw_tx_tpl_grp_3_cfg_t    group3_cfg;
  
  /*! Structures for group 4 */
  cfw_tx_tpl_grp_4_cfg_t    group4_cfg;
  

} cfw_tx_tpl_grp_cfg_union_t;

/*! Meta structure containing the current selected template group and payload.
    Used only internally in firmware */
typedef struct {

  union {
    /*! Structures for group 0 */
    cfw_tx_tpl_grp_0_t group0;
    /*! Structures for group 1 */
    cfw_tx_tpl_grp_1_t group1;
    /*! Structures for group 2 */
    cfw_tx_tpl_grp_2_t group2;
    /*! Structures for group 3 */
    cfw_tx_tpl_grp_3_t group3;
    /*! Structures for group 4 */
    cfw_tx_tpl_grp_4_t group4;

  } tpl_grp;

  union {
    cfw_tx_tpl_grp_0_payload_t group0;
    cfw_tx_tpl_grp_1_payload_t group1;
    cfw_tx_tpl_grp_2_payload_t group2;
    cfw_tx_tpl_grp_3_payload_t group3;
    cfw_tx_tpl_grp_4_payload_t group4;

  } tpl_payload;

} cfw_tx_meta_tpl_grp_t;

/*! Used only internally for template group static initialization*/

typedef struct {

  cfw_tx_tpl_grp_0_t group0;
  cfw_tx_tpl_grp_1_t group1;
  cfw_tx_tpl_grp_2_t group2;
  cfw_tx_tpl_grp_3_t group3;
  cfw_tx_tpl_grp_4_t group4;


} cfw_tx_tpl_grp_static_init_t;

/*\}*/
#endif
