/*!
  @file
  cfw_tx_tpl_grp_0.h

  @brief
  C2K TX Template Group 0 Definitions

  @detail

*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/c2k/api/cfw_tx_tpl_grp_0.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/
#include "ccs_rf_intf.h"
#include "cfw_rf_intf.h"

#ifndef _CFW_TX_TEMPLATE_GRP_0_H_
#define _CFW_TX_TEMPLATE_GRP_0_H_

/*===========================================================================

                    RF Control Group 0 Templates
        
        Name - "Dime BRING-UP"
        Description - Created for Dime bring-up


============================================================================*/
/** \defgroup tplgrp0 Group 0 */ /*\{*/

/*! Number of CCS tasks in PA_CHANGE of TPL 0 */
#define CFW_RF_TX_PA_CHANGE_TPL_0_NUM_TASK 4

/*! Template for PA state change (h2l,l2h) and RGI change (h2l,l2h) (AVGA gain word change) */
typedef union {

  /*! These fields are for direct access */
  struct {
    ccs_rf_task_ssbi_write_t      avga;
    ccs_rf_task_rffe_write_t      pa;
    ccs_rf_task_rffe_write_t      apt;
    ccs_rf_task_rffe_write_t      pa_q_current;
  };
  /*! For submitting to ccs APIs that require ccs_rf_task_t */
  ccs_rf_task_t tasks[CFW_RF_TX_PA_CHANGE_TPL_0_NUM_TASK];

} cfw_tx_pa_change_tpl_0_t;

/*! Defined here for setting up transaction_ptr during sequence generaton. Likely
    only the address part is populated at that time. We can also consider
    requesting RF software passing address directly in this format (skipping
    the data part). The rest is updated using the "fix-up" table on the
    generated sequence. */ 
typedef struct {
  
  /*! AVGA SSBIs */
  ccs_rf_transaction_ssbi_rw_t avga[CFW_RF_TX_TPL_AVGA_NUM_SSBI] ALIGN(4);
  /*! PA RFFEs */
  ccs_rf_transaction_rffe_rw_t pa[CFW_RF_TX_TPL_PA_NUM_RFFE]   ALIGN(4);
  /*! QPOET RFFEs */
  ccs_rf_transaction_rffe_rw_t apt[CFW_RF_TX_TPL_APT_NUM_RFFE] ALIGN(4);
  /*! PA Quiescent Current RFFEs */
  ccs_rf_transaction_rffe_rw_t
    pa_q_current[CFW_RF_TX_TPL_PA_Q_CUR_NUM_RFFE] ALIGN(4);
  
} cfw_tx_pa_change_tpl_0_payload_t;

#define CFW_RF_TX_RGI_CHANGE_TPL_0_NUM_TASK 3
/*! Template for RGI change - (AVGA gain word change) (h2l, l2h) */
typedef union {
  /*! These fields are for direct access */
  struct {
    ccs_rf_task_ssbi_write_t      avga;
    ccs_rf_task_rffe_write_t      apt;
    ccs_rf_task_rffe_write_t      pa_q_current;
  };
  /*! For submitting to ccs APIs that require ccs_rf_task_t */
  ccs_rf_task_t tasks[CFW_RF_TX_RGI_CHANGE_TPL_0_NUM_TASK];

} cfw_tx_rgi_change_tpl_0_t;

typedef struct {
  
  /*! AVGA SSBIs */
  ccs_rf_transaction_ssbi_rw_t avga[CFW_RF_TX_TPL_AVGA_NUM_SSBI] ALIGN(4);
  /*! QPOET RFFEs */
  ccs_rf_transaction_rffe_rw_t apt[CFW_RF_TX_TPL_APT_NUM_RFFE] ALIGN(4);
    /*! PA Q Current RFFEs */
  ccs_rf_transaction_rffe_rw_t
    pa_q_current[CFW_RF_TX_TPL_PA_Q_CUR_NUM_RFFE]   ALIGN(4);

} cfw_tx_rgi_change_tpl_0_payload_t;


/*! A structure that collects all template instances for this rf control group */

typedef struct {
  /*! Template Group Type */
  cfw_tx_tpl_grp_type_t  group_type;

  /*! \brief 0 = l2h, 1 = h2l */
  cfw_tx_pa_change_tpl_0_t pa_change[CFW_RF_TX_TPL_NUM_SEQ_TYPE];
  
  /*! \brief 0 = l2h, 1 = h2l */
  cfw_tx_rgi_change_tpl_0_t    rgi_change[CFW_RF_TX_TPL_NUM_SEQ_TYPE];

  // H2L, L2H shares the same payload. These are for configuration and
  // sequence generation. After sequences are generated the access
  // is through fix-up table which actually points to the sequence itself
  // instead of the payload here.
  // XXX - Unioned with other groups
  //cfw_tx_pa_change_tpl_0_payload_t pa_change_payload;
  //cfw_tx_rgi_change_tpl_0_payload_t rgi_change_payload;

} cfw_tx_tpl_grp_0_t;

/*! pa_change template configuration interface */
typedef struct {

  /*! Task config - 0 = L2H, - 1 = H2L */
  struct {
    /*! Index i is for the trig_time of task i in the templates */
    uint32 trig_time[CFW_RF_TX_PA_CHANGE_TPL_0_NUM_TASK];

    /*! Index i is for the channel (SSBI/RFFE) of task i in the templates */
    uint32 channel[CFW_RF_TX_PA_CHANGE_TPL_0_NUM_TASK];

    /*! Index i is for the slave_id (RFFE) of task i in the templates 
     * NOTE: if task-i is not RFFE task, then this paramter shall be ignored */
    uint32 slave_id[CFW_RF_TX_PA_CHANGE_TPL_0_NUM_TASK];

  } task_cfg[CFW_RF_TX_TPL_NUM_SEQ_TYPE];
  
  /*! Payload is shared between l2h, h2l cases hence only 1 copy */
  cfw_tx_pa_change_tpl_0_payload_t    payload_cfg;
  
} cfw_tx_pa_change_tpl_0_cfg_t;

/*! rgi_change template configuration interface */
typedef struct {

  /*! Task config - 0 = L2H, - 1 = H2L */
  struct {
    /*! Index i is for the trig_time of task i in the templates */
    uint32 trig_time[CFW_RF_TX_RGI_CHANGE_TPL_0_NUM_TASK];

    /*! Index i is for the channel (SSBI/RFFE) of task i in the templates */
    uint32 channel[CFW_RF_TX_RGI_CHANGE_TPL_0_NUM_TASK];

    /*! Index i is for the slave_id (RFFE) of task i in the templates
     * NOTE: if task-i is not a RFFE task, this parameter shall be ignored */
    uint32 slave_id[CFW_RF_TX_RGI_CHANGE_TPL_0_NUM_TASK];
  
  } task_cfg[CFW_RF_TX_TPL_NUM_SEQ_TYPE];
  
  cfw_tx_rgi_change_tpl_0_payload_t    payload_cfg;
  
} cfw_tx_rgi_change_tpl_0_cfg_t;


/*! A structure placed in shared memory for template configuration. RF SW
    is expected to only fill in the static parts. The dynamic part (SSBI values)
    is not used */
typedef struct {
  
  /*! Template Group Type */
  cfw_tx_tpl_grp_type_t  group_type;
  cfw_tx_pa_change_tpl_0_cfg_t pa_change;
  cfw_tx_rgi_change_tpl_0_cfg_t rgi_change;
  /*! Number of transactions for PA RFFEs */
  uint8 num_rffe_pa_transactions;
  /*! Number of transactions for QPOET RFFEs */
  uint8 num_rffe_qpoet_transactions;
  /*! Number of transactions for PA Q-Current RFFEs */
  uint8 num_rffe_pa_qcur_transactions;
  /*! Number of transactions for AVGA SSBIs */
  uint8 num_ssbi_avga_transactions;
} cfw_tx_tpl_grp_0_cfg_t;

typedef struct {

  cfw_tx_pa_change_tpl_0_payload_t pa_change;
  cfw_tx_rgi_change_tpl_0_payload_t rgi_change;
} cfw_tx_tpl_grp_0_payload_t;

/*\}*/
#endif
