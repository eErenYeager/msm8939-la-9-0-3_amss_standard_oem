#===============================================================================
#
# fw Subsystem build script
#
# Copyright (c) 2012 Qualcomm Technologies Incorporated.
#
# All Rights Reserved. Qualcomm Confidential and Proprietary
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/modem/fw/cpl/bagheera/MPSS.DPM.2.0.2.C1/build/fw.scons#1 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#===============================================================================

# --------------------------------------------------------------------------- #
# Import SCons Environment                                                    #
# --------------------------------------------------------------------------- #
Import('env')
import sys
import os
import glob
import re

if env.PathExists('${INC_ROOT}/fw'):
    env.Replace(FW_ROOT = '${INC_ROOT}/fw')
else:
    env.Replace(FW_ROOT = '${INC_ROOT}/modem/fw')

if 'USES_STANDALONE_FW' not in env:

    # Choose build variant
    if 'USES_EMULATION_PLATFORM' in env:
      fw_variant = "QRAFA"
    else:
      if 'USES_INTERNAL_BUILD' in env:
          fw_variant = "QSAFA"
      else:
          fw_variant = "QSAFA-external"

      # Load cleanpack script:
      if env.PathExists('../pack/fw_cleanpack.py'):
        env.LoadToolScript('../pack/fw_cleanpack.py')

    # Setup source PATH
    SRCPATH = "../image/"+fw_variant

    env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

    # Autodetect object and libraries
    OBJPATH = env.subst(SRCPATH)

    if 'USES_EXCLUDE_C2K_FW' in env:
      obj = [ '${BUILDPATH}' + re.sub('^'+OBJPATH,'',fname)
            for fname in (glob.glob(OBJPATH+'/*/build/fw/qdsp6/*/*/src/*.o')) if "c2k" not in fname]
      lib = [ '${BUILDPATH}' + re.sub('^'+OBJPATH,'',fname)
            for fname in (glob.glob(OBJPATH+'/*/build/fw/qdsp6/*/*.lib')) if "c2k" not in fname]       
    else:
      obj = [ '${BUILDPATH}' + re.sub('^'+OBJPATH,'',fname)
            for fname in (glob.glob(OBJPATH+'/*/build/fw/qdsp6/*/*/src/*.o'))]
      lib = [ '${BUILDPATH}' + re.sub('^'+OBJPATH,'',fname)
            for fname in (glob.glob(OBJPATH+'/*/build/fw/qdsp6/*/*.lib'))]  
    
    env.AddOEMObject(['MODEM_MODEM'], obj)
    env.AddOEMLibrary(['MODEM_MODEM'], lib)
    
    env.LoadSoftwareUnits()
    Return()





# --------------------------------------------------------------------------- #
# Create custom build options for FW tree                                     #
# --------------------------------------------------------------------------- #
env.Replace(HEXAGONCC_OPT = " -O2 ")
env.Replace(HEXAGON_LANIND = " -G0 ")
env.Append(CXXFLAGS = " -fno-exceptions ")
env.Append(ASFLAGS = " -DQDSP6_ASSEMBLER -Wa,--gdwarf2 -Wa,-mfalign-warn ")

env.Replace(HEXAGON_WARN =
   " -Wall -Wextra -Wundef -Wpointer-arith -Wno-unused-parameter -Wshadow"
   " -Wcast-align -Wcomments -Winline -Wno-unknown-pragmas ")
env.Replace(HEXAGONCC_WARN = " ${HEXAGON_WARN} -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations ")
env.Replace(HEXAGONCXX_WARN = " ${HEXAGON_WARN} ")
env.Replace(HEXAGONASM_WARN = " ${HEXAGON_WARN} -Wa,-mfalign-info ")

env.Append(CPPDEFINES = ['BLAST_TCM',
                         'FEATURE_USE_RTOS_BLAST',
                         'FEATURE_USE_RTOS_SOS',
                         'BLAST',
                         'FEATURE_BLAST',
                         'FEATURE_DAL_STUB',
                         'FEATURE_APPMGR_STUB',
                         'FEATURE_BLAST_V2',
                         'restrict=__restrict__',
                         'posix_fd_set=fd_set',
                         'posix_pselect=pselect',
                         'posix_select=select',
                         'FEATURE_POSIX',
                         'FEATURE_MCS',
                         'FEATURE_MSGR',
                         'DSM_MAJOR_VER_NO=3',
                         'ASSERT=ASSERT_FATAL',
                         'LTE_APP_DSPFW',
                         'FW_DIAG',
                         'T_QDSP6',
                         'ARCH_QDSP6',
                         'FEATURE_QUBE_SIM',
                         'CONVERGENT_ROUNDING',
                         'FEATURE_DIAG_PRINTF_ENABLED',
                         'THREAD_SAFE',
                         'FEATURE_ALIGNED_QWORD',
                         'FEATURE_MODEM_FIRMWARE',
                         'FEATURE_MODEM_STANDALONE',
                         'FEATURE_LTE_3GPP_REL8_MAR09',
                         ])

# XXX Remove
env.Append(CPPDEFINES = ['FEATURE_QDSP6', 'FEATURE_Q6SIM'])

if 'USES_SIM_PLATFORM' in env:
   env.Append(CPPDEFINES = ['SIM_PLATFORM'])

if 'USES_EMU_PLATFORM' in env:
   env.Append(CPPDEFINES = ['EMU_PLATFORM'])


if 'USES_LTE_FW' in env:
   env.Append(CPPDEFINES = ['FEATURE_LTE_ODYSSEY',
                            'LTE_APP_DSPFW',
                            'APPMGR_NUM_APPS=1',
                            'FEATURE_MODEM_FIRMWARE',
                            'FEATURE_MODEM_STANDALONE',
                            ])
   if 'USES_PLT_FW' in env:
      env.Append(CPPDEFINES = ['FEATURE_LMVT'])

if 'USES_HDR_FW' in env and 'USES_PLT_FW' in env:
   env.Append(CPPDEFINES = ['FEATURE_VI_MODE',
                            'FEATURE_HDR_VI'])


if 'USES_GERAN_FW' in env:
   env.Append(CPPDEFINES = ['PACKED_IK_BUFF' ])

   if 'USES_UMPLT_FW' in env:
      env.Append(CPPDEFINES = ['FEATURE_UMPLT_GERAN'])
   else:
      if 'USES_PLT_FW' in env:
         env.Append(CPPDEFINES = ['FW_PLT_PLATFORM'])

if 'USES_WCDMA_FW' in env:
   if 'USES_PLT_FW' in env:
      env.Append(CPPDEFINES = ['FEATURE_BUILD_WFW_TE'])

#Dime QSAFA builds have AsDiv feature enabled
if 'USES_MODEM_ANTENNA_SWITCH_DIVERSITY' in env:
    env.Append(CPPDEFINES = ['FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY'])

if 'USES_TDSCDMA_FW' in env and 'USES_PLT_FW' in env:
   env.Append(CPPDEFINES = ['FEATURE_TPLT_TDSCDMA'])

if 'USES_TDSCDMA_FW' in env and 'USES_UMPLT_FW' in env:
      env.Append(CPPDEFINES = ['FEATURE_UMPLT_TDSCDMA']) 

env.PublishProtectedApi('FW', ['${INC_ROOT}/modem/fw/api/lte',
                               '${INC_ROOT}/modem/fw/api/rf',
                               '${INC_ROOT}/modem/fw/api/common',
                               '${INC_ROOT}/modem/fw/ccs_app/swi_headers/dimeModem',
                               '${INC_ROOT}/modem/fw/ccs_app/swi_headers/ccsFirmware',
                               '${INC_ROOT}/modem/fw/ccs_app/fw/demback/include',
                               '${INC_ROOT}/modem/fw/ccs_app/fw/srch/include',
                               '${INC_ROOT}/modem/fw/ccs_app/fw/common/include',
                               '${INC_ROOT}/modem/fw/drivers/ccs/inc',
                               '${INC_ROOT}/modem/fw/drivers/fw_rf/inc',
                               '${INC_ROOT}/modem/fw/drivers/fw_rxlm/inc',
                               '${INC_ROOT}/modem/fw/drivers/fw_txlm/inc',
                               '${INC_ROOT}/modem/fw/drivers/fw_xo/inc',
                               '${INC_ROOT}/modem/fw/drivers/mcdma/inc',
                               '${INC_ROOT}/modem/fw/drivers/memss/inc',
                               '${INC_ROOT}/modem/fw/drivers/mtc/inc',
                               '${INC_ROOT}/modem/fw/drivers/vpe/inc',
                               '${INC_ROOT}/modem/fw/image',
                               '${INC_ROOT}/modem/fw/library/elf/inc',
                               '${INC_ROOT}/modem/fw/library/includes/inc',
                               '${INC_ROOT}/modem/fw/library/math/inc',
                               '${INC_ROOT}/modem/fw/library/mem/inc',
                               '${INC_ROOT}/modem/fw/library/rtos/inc',
                               '${INC_ROOT}/modem/fw/library/util/inc',
                               '${INC_ROOT}/modem/fw/services/app/inc',
                               '${INC_ROOT}/modem/fw/services/crm/inc',
                               '${INC_ROOT}/modem/fw/services/diag/inc',
                               '${INC_ROOT}/modem/fw/services/fws/inc',
                               '${INC_ROOT}/modem/fw/services/prof/inc',
                               '${INC_ROOT}/modem/fw/services/smem/inc',
                               '${INC_ROOT}/modem/fw/services/time/inc',
                               '${INC_ROOT}/modem/fw/target/inc',
                               '${INC_ROOT}/modem/fw/target/qurt/install/include',
                               '${INC_ROOT}/modem/fw/target/qurt/install/include/qube',
                               '${INC_ROOT}/modem/fw/target/qurt/install/include/posix',
                               '${INC_ROOT}/modem/fw/target/hwio',
                               '${INC_ROOT}/modem/fw/target/hwio/plat/phys',
                                ])


#========================================================================
# Function:  fw_normpath
# Description:
#   Return a normalized path, common for Windows and UNIX..
#   All backslashes are converted to forward slash, and any trailing slash
#   is removed.
# Return:
#   Normalized path
#========================================================================
def fw_normpath(path):
  path = os.path.normpath(path)
  path = re.sub('\\\\','/',path)
  return path


#========================================================================
# Function:  fw_get_base_path
# Description:
#   Return the base path for the current module tree.
#   Assume current directory is <dir>/build, then return <dir> in normalized
#   form with trailing '/'.
# Return:
#   Base directory.
#========================================================================
def fw_get_base_path():
   # Get base directory (remove last occurence of "/build" from absolute path)
   base_build_dir = os.getcwd()
   idx = base_build_dir.rfind('build')
   base_dir = base_build_dir[:idx-1]
   base_dir = fw_normpath(base_dir)+'/'

   return base_dir


#========================================================================
# Function:  fw_relative_glob
# Inputs :
#   pattern : Glob pattern
# Description:
#   Glob for filenames matching pattern relative to the current build tree,
#   which is assumed to be <tree>/build.
#   Return relative paths (relative to <tree>), in normalized (forward-slash) form.
#   e.g. from <tree>/build, pattern = src/*.c, return src/a.c, src/b.c
# Return:
#   List of relative paths.
#========================================================================
def fw_relative_glob(pattern):
   base_dir = fw_get_base_path()
   #print ("glob base dir: ", base_dir)
   #print ("glob pattern: ", pattern)
   paths = glob.glob(base_dir+pattern)
   #print ("glob found: ", paths)
   paths = [fw_normpath(p).replace(base_dir,'') for p in paths]
   #print ("glob returned: ", paths)

   return paths



#========================================================================
# Function:  fw_find_source
# Inputs :
#   src_dir : Base directory to find files under (relative path)
# Description:
#   Given a directory, search for all source files within that directory.
#   <dir>/*.c, <dir>/*.cc, <dir>/*.S
#   Depending on the setting of USES_NATIVE_SRC, also find
#   <dir>/native/*.c,cc or <dir>/asm/*.S
#   The directory should be a relative path, compatible with fw_relative_glob.
# Return:
#   List of source files, with relative paths
#========================================================================
def fw_find_source(src_dir):
   # Source Files pattern
   C_PATTERN = '*.c'
   CC_PATTERN = '*.cc'
   ASM_PATTERN = '*.[sS]'

   src_dir = fw_normpath(src_dir)+'/'

   src_files = []
   src_files += fw_relative_glob(src_dir+C_PATTERN)
   src_files += fw_relative_glob(src_dir+CC_PATTERN)
   src_files += fw_relative_glob(src_dir+ASM_PATTERN)

   if 'USES_NATIVE_SRC' in env:
      # Use C versions of assembly functions
      src_files += fw_relative_glob(src_dir+'native/'+C_PATTERN)
      src_files += fw_relative_glob(src_dir+'native/'+CC_PATTERN)
   else:
      # Use assembly versions
      src_files += fw_relative_glob(src_dir+'asm/'+ASM_PATTERN)

   #print ("src found: ", src_files)
   return src_files



#========================================================================
# Function:  fw_get_inc_src
# Inputs :
#   exclude_pat : Patterns of directories/files to exclude.
#   base_path : Base path to start search
# Description:
#   Return (inc_dirs, src_files) tuple with a list of
#   all source/include directories, and source files.
#   These can be published as builder arguments (see FwAddLibrary).
#   Search starts from <tree>/ assuming current dir is <tree>/build, and
#   base_path and exclude_list are relative to <tree>.
#   exclude_list is a list of patterns to exclude from the build.
#   Any path matching an exclude item will be excluded - to exclude an entire
#   tree, use a '/' suffix on the dir name.
#   Use only forward slashes for directory separators.
#   If base_path is specified, any exclude_list items must contain base_path also.
#========================================================================
def fw_get_inc_src(env,exclude_pat=[],base_path=''):
   # Subdirectories to search
   SRC_DIR_PATTERN = ['/src/','/*/src/','/*/*/src/']
   INC_DIR_PATTERN = ['/inc/','/*/inc/','/*/*/inc/']

   # Find all include directories
   inc_dirs = []
   for inc_pat in INC_DIR_PATTERN:
     inc_dirs += fw_relative_glob(base_path+inc_pat)

   # Find all source directories
   src_dirs = []
   for src_pat in SRC_DIR_PATTERN:
     src_dirs += fw_relative_glob(base_path+src_pat)

   # Find all source files
   src_files = []
   for src_dir in src_dirs:
      src_files += fw_find_source(src_dir)

   # Include directories contain source directories also
   inc_dirs += src_dirs

   #print ("INC dirs: ", inc_dirs)
   #print ("SRC files: ", src_files)

   # Find all include/source which match exclude pattern
   exclude_list = []
   for pat in exclude_pat:
      exclude_list += [d for d in inc_dirs if (re.search(pat,d) != None)]
      exclude_list += [f for f in src_files if (re.search(pat,f) != None)]

   #print ("Exclude list: ", exclude_list)

   # Filter out excluded directories/files
   inc_dirs = [d for d in inc_dirs if d not in exclude_list]
   src_files = [f for f in src_files if f not in exclude_list]

   #print ("Pruned inc dirs: ", inc_dirs)
   #print ("Pruned src files: ", src_files)

   return (inc_dirs, src_files)


#========================================================================
# Function:  fw_build_library
# Inputs :
#   lib_name : Name of output library
#   inc_dirs : Private include directories
#   src_files : Files to include in library
#   exclude_pat : Pattern of directories/files to exclude.
#   base_path : Base path of files
# Description:
#   Add a library to build targets.
#   This can be used two ways:
#   1. If src_files is not specified, then a default list of inc/src are
#      scanned using FwGetIncSrc(exclude_pat,base_path), and used for library.
#   2. If src_files is not empty, then inc_dirs/src_files are used directly
#      for the library, and exclude_pat/base_path are ignored.
#      Presumably, FwGetIncSrc was called previously.
#   All inputs use relative paths. This function adds the appropriate prefix.
#========================================================================
def fw_build_library(env,lib_name,exclude_pat=[],base_path='',inc_dirs=[],src_files=[]):

   if not src_files:
      (inc_dirs,src_files) = fw_get_inc_src(env,exclude_pat,base_path)

   base_path = fw_get_base_path()
   inc_dirs = [base_path+d for d in inc_dirs]
   src_files = ['${BUILDPATH}/'+f for f in src_files]

   #print("inc_dirs:", inc_dirs)
   #print("src_files:", src_files)

   # Publish the inc and src directories as private
   env.PublishPrivateApi('ALL_INCS', inc_dirs)

   # Build the file and create a library
   env.VariantDir('${BUILDPATH}', base_path, duplicate=0)
   env.AddLibrary(['MODEM_MODEM'], '${BUILDPATH}/'+lib_name, src_files)


#========================================================================
# Function:  fw_add_object
# Inputs :
#   src_files : List of source files
# Description:
#   Adds an individual object file to the build.
#   This is required for resolving weak symbols.
#   The source file should be excluded from its corresponding library.
#   Source files use relative paths, this function adds required prefix.
#========================================================================
def fw_add_object(env,src_files):

   base_path = fw_get_base_path()
   src_files = ['${BUILDPATH}/'+f for f in src_files]

   env.AddObject(['MODEM_MODEM'], src_files)


#========================================================================
# Function:  find_files  ** DEPRECATED **
# Inputs :
# base_dir : Base direcotry to build
# build_src_dir : Dir with-in base dir to find the files
# Description: 1)Given a directory name, this function find all
#              src (*.c,*.cc,*.S)  files
#           2) Based on NATIVE or ASM uses flag, this function will also
#              look for src (*.c, *.cc or *.S) files either in native
#               or asm direcotry within build_src_dir
#           3) Using base_dir, it modifies the src_files list such that
#           the obj files are created in proper dir within $BUILDPATH
# Return:
#  SRC_FILES: List of source files
#========================================================================
def find_files (base_dir, build_src_dir):
   # Source Files pattern
   C_PATTERN = '*.c'
   CC_PATTERN = '*.cc'
   ASM_PATTERN = '*.[sS]'
   # List to store source files
   SRC_FILES=[]
   ASM_OR_NATIVE=''

   #=================================================
   # Find C files
   #=================================================
   for name in glob.glob(build_src_dir + C_PATTERN):
      name = "${BUILDPATH}" + name.replace(base_dir,'')
      SRC_FILES.append(name)

   #=================================================
   # Find CC files
   #=================================================
   for name in glob.glob(build_src_dir + CC_PATTERN):
      name = "${BUILDPATH}" + name.replace(base_dir,'')
      SRC_FILES.append(name)

   #=================================================
   # Find ASM files
   #=================================================
   for name in glob.glob(build_src_dir + ASM_PATTERN):
      name = "${BUILDPATH}" + name.replace(base_dir,'')
      SRC_FILES.append(name)
   #================================================================
   # Check uses flag, based on which find files in asm or native dir
   #================================================================
   if 'USES_NATIVE_SRC'in env:
      ASM_OR_NATIVE = '/native/'

      #=================================================
      # Find C files in Native directory
      #=================================================
      for name in glob.glob(build_src_dir + ASM_OR_NATIVE + C_PATTERN):
         name = "${BUILDPATH}" + name.replace(base_dir,'')
         SRC_FILES.append(name)

      #=================================================
      # Find CC files in Native directory
      #=================================================
      for name in glob.glob(build_src_dir + ASM_OR_NATIVE + CC_PATTERN):
         name = "${BUILDPATH}" + name.replace(base_dir,'')
         SRC_FILES.append(name)

   else:
      ASM_OR_NATIVE = '/asm/'

      #=================================================
      # Find ASM files in ASM directory
      #=================================================
      for name in glob.glob(build_src_dir + ASM_OR_NATIVE+ ASM_PATTERN):
         name = "${BUILDPATH}" + name.replace(base_dir,'')
         SRC_FILES.append(name)

   return SRC_FILES

#========================================================================
# Function:  build_dir  ** DEPRECATED **
# Inputs :
# base_build_dir : Base direcotry to build
# exclude_list : Dir's list which are to be exculded from the build
# lib_name: Name of the library to be created
# Description: 1) This function finds the source files recursively in the
#           dir "base_build_dir" excluding files from "exlude_list" dir
#        2) It then publishes all inc and src dirs as private, so that
#         they are visible to the build(INC_DIRS)
#        3) It then creates a library with the name lib_name
#        Files and lib will be created under the dir $BUILDPATH within
#        base_build_dir
#========================================================================
def fw_build_dir(env,exclude_list,lib_name):
   # Source Types to build
   SRC_DIR_PATTERN = ['/src/','/*/src/','/*/*/src/']
   INC_DIR_PATTERN = ['/inc/','/*/inc/','/*/*/inc/']
   SRC_AND_INC_DIRS =[]
   ALL_SRC_FILES=[]

   base_build_dir = os.getcwd()

   # Get base directory (remove last occurance of "/build" from absolute path)
   idx = base_build_dir.rfind('build')
   base_dir = base_build_dir[:idx-1]

   #Find all the include directories
   for inc_pat in INC_DIR_PATTERN:
     for inc_dir in glob.glob(base_dir+inc_pat):
        SRC_AND_INC_DIRS.append(inc_dir)

   #Find the source directories to build
   for src_pat in SRC_DIR_PATTERN:
     for src_dir in glob.glob(base_dir+src_pat):
        SRC_AND_INC_DIRS.append(src_dir)
        # Flag to check if directory is to be ignored
        ignore_dir = 0
        for exclude_dir in exclude_list:
           # Build the string to find the exclude directories
           exclude_str =  base_dir + os.sep + exclude_dir

           #=====================================================
           # If the dir matches the exclude list, ignore this dir
           #=====================================================
           if ((src_dir.find(exclude_str)) == 0):
              ignore_dir=1

        if(ignore_dir==0):
           ALL_SRC_FILES = ALL_SRC_FILES + find_files(base_dir,src_dir)
        else:
           env.PrintDebugInfo('fw', "Ignoring the dir %s"%base_dir+src_dir)

   # Publish the inc and src directories as private
   env.PublishPrivateApi('ALL_INCS', SRC_AND_INC_DIRS)

   #print ("SRC_AND_INC: ", SRC_AND_INC_DIRS)
   #print ("ALL_SRC: ", ALL_SRC_FILES)

   # Build the file and create a library
   env.VariantDir('${BUILDPATH}', base_dir, duplicate=0)
   env.AddLibrary(['MODEM_MODEM'], '${BUILDPATH}/'+lib_name, ALL_SRC_FILES)

   return

#=========================================
# Add the defined function to the env
#=========================================
env.AddMethod(fw_build_library, "FwBuildLibrary")
env.AddMethod(fw_get_inc_src, "FwGetIncSrc")
env.AddMethod(fw_add_object, "FwAddObject")
env.AddMethod(fw_build_dir, "FwBuildDir")


# --------------------------------------------------------------------------- #
# Search for additional Units                                                 #
# --------------------------------------------------------------------------- #
env.LoadSoftwareUnits()

