#===============================================================================
#
# FDS (Flash Driver Service) QMI APIs
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#                      EDIT HISTORY FOR FILE
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/qmimsgs/fds/build/qmi_fds.scons#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 07/26/13   pm      Baseline version
#===============================================================================
Import('env')
env = env.Clone()
from glob import glob
from os.path import join, basename, exists

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

# ------------------------------------------------------------------------------
# Include Paths
#---------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# APIs needed to build the DSD APIs
#-------------------------------------------------------------------------------

env.RequirePublicApi(['FDS'])

env.RequirePublicApi(['MPROC'], area='CORE')

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
FDS_C_SOURCES = ['${BUILDPATH}/' + basename(fname)
                 for fname in glob(join(env.subst(SRCPATH), '*.c'))]

# Add our library to the following build tags:
env.AddLibrary (['QMIMSGS_MPSS'], '${BUILDPATH}/qmimsgs_fdsd', [FDS_C_SOURCES])

# Load test units
env.LoadSoftwareUnits()
