#ifndef COEX_SERVICE_02_H
#define COEX_SERVICE_02_H
/**
  @file coexistence_service_v02.h

  @brief This is the public header file which defines the coex service Data structures.

  This header file defines the types and structures that were defined in
  coex. It contains the constant values defined, enums, structures,
  messages, and service message IDs (in that order) Structures that were
  defined in the IDL as messages contain mandatory elements, optional
  elements, a combination of mandatory and optional elements (mandatory
  always come before optionals in the structure), or nothing (null message)

  An optional element in a message is preceded by a uint8_t value that must be
  set to true if the element is going to be included. When decoding a received
  message, the uint8_t values will be set to true or false by the decode
  routine, and should be checked before accessing the values that they
  correspond to.

  Variable sized arrays are defined as static sized arrays with an unsigned
  integer (32 bit) preceding it that must be set to the number of elements
  in the array that are valid. For Example:

  uint32_t test_opaque_len;
  uint8_t test_opaque[16];

  If only 4 elements are added to test_opaque[] then test_opaque_len must be
  set to 4 before sending the message.  When decoding, the _len value is set 
  by the decode routine and should be checked so that the correct number of
  elements in the array will be accessed.

*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.


  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/qmimsgs/coex/api/coexistence_service_v02.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====* 
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.7 
   It was generated on: Tue Apr 29 2014 (Spin 0)
   From IDL File: coexistence_service_v02.idl */

/** @defgroup coex_qmi_consts Constant values defined in the IDL */
/** @defgroup coex_qmi_msg_ids Constant values for QMI message IDs */
/** @defgroup coex_qmi_enums Enumerated types used in QMI messages */
/** @defgroup coex_qmi_messages Structures sent as QMI messages */
/** @defgroup coex_qmi_aggregates Aggregate types used in QMI messages */
/** @defgroup coex_qmi_accessor Accessor for QMI service object */
/** @defgroup coex_qmi_version Constant values for versioning information */

#include <stdint.h>
#include "qmi_idl_lib.h"
#include "common_v01.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup coex_qmi_version
    @{
  */
/** Major Version Number of the IDL used to generate this file */
#define COEX_V02_IDL_MAJOR_VERS 0x02
/** Revision Number of the IDL used to generate this file */
#define COEX_V02_IDL_MINOR_VERS 0x02
/** Major Version Number of the qmi_idl_compiler used to generate this file */
#define COEX_V02_IDL_TOOL_VERS 0x06
/** Maximum Defined Message ID */
#define COEX_V02_MAX_MESSAGE_ID 0x0032
/**
    @}
  */


/** @addtogroup coex_qmi_consts 
    @{ 
  */

/**  Maximum supported unique radio access technologies (RATs)  */
#define COEX_MAX_TECHS_SUPPORTED_V02 24

/**  Maximum unique operating bands/channels supported for WWAN GSM RAT  */
#define COEX_WWAN_GSM_MAX_BANDS_SUPPORTED_V02 16

/**  Maximum unique operating bands/channels supported for all other WWAN RATs  */
#define COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02 8

/**  Maximum unique events supported for WLAN WiFi RAT  */
#define COEX_WLAN_WIFI_MAX_BANDS_SUPPORTED_V02 4

/**  Maximum unique WLAN WiFi high priority events supported  */
#define COEX_WLAN_WIFI_MAX_EVENTS_SUPPORTED_V02 8

/**  Maximum unique WLAN WiFi active Connections supported  */
#define COEX_WLAN_WIFI_MAX_CONNS_SUPPORTED_V02 4
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_TECH_TYPE_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_TECH_TYPE_UNINIT_V02 = 0, /**<  RAT type uninitialized  */
  COEX_TECH_TYPE_WWAN_LTE_V02 = 1, /**<  RAT type: WWAN LTE  */
  COEX_TECH_TYPE_WWAN_TDSCDMA_V02 = 2, /**<  RAT type: WWAN TD-SCDMA  */
  COEX_TECH_TYPE_WWAN_GSM_V02 = 3, /**<  RAT type: WWAN GSM [Instance 1] */
  COEX_TECH_TYPE_WWAN_ONEX_V02 = 4, /**<  RAT type: WWAN CDMA2000\textsuperscript{\textregistered} 1X  */
  COEX_TECH_TYPE_WWAN_HDR_V02 = 5, /**<  RAT type: WWAN HDR  */
  COEX_TECH_TYPE_WWAN_WCDMA_V02 = 6, /**<  RAT type: WWAN WCDMA  */
  COEX_TECH_TYPE_WLAN_WIFI_V02 = 7, /**<  RAT type: WLAN WiFi  */
  COEX_TECH_TYPE_WLAN_BT_V02 = 8, /**<  RAT type: WLAN BlueTooth  */
  COEX_TECH_TYPE_SPECIAL_DIVERSITY_ANTENNA_V02 = 9, /**<  Special type to represent WWAN(MDM) diversity antenna chain  */
  COEX_TECH_TYPE_WWAN_GSM2_V02 = 10, /**<  RAT type: WWAN GSM [Instance 2] */
  COEX_TECH_TYPE_WWAN_GSM3_V02 = 11, /**<  RAT type: WWAN GSM [Instance 3] */
  COEX_TECH_TYPE_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_tech_type_v02;
/**
    @}
  */

typedef uint8_t coex_tech_state_v02;
#define COEX_TECH_STATE_OFF_V02 ((coex_tech_state_v02)0x00) /**<  Represents technology's state as is OFF  */
#define COEX_TECH_STATE_IDLE_V02 ((coex_tech_state_v02)0x01) /**<  Represents technology's state is in IDLE or not  */
#define COEX_TECH_STATE_CONN_V02 ((coex_tech_state_v02)0x02) /**<  Represents technology's state is CONNECTED or not  */
#define COEX_TECH_STATE_ACTIVE_V02 ((coex_tech_state_v02)0x04) /**<  Represents technology's state is ACTIVE or not  */
/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_TECH_OPERATING_DIMENSION_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_TECH_OPERATING_DIMENSION_FDD_V02 = 0x0, /**<  Technology's operating in Frequency Division Duplex dimension  */
  COEX_TECH_OPERATING_DIMENSION_TDD_V02 = 0x1, /**<  Technology's operating in Time Division Duplex dimension  */
  COEX_TECH_OPERATING_DIMENSION_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_tech_operating_dimension_v02;
/**
    @}
  */

typedef uint8_t coex_tech_band_direction_v02;
#define COEX_TECH_BAND_DIRECTION_DONT_CARE_V02 ((coex_tech_band_direction_v02)0x00) /**<  Technology's band direction is don't care  */
#define COEX_TECH_BAND_DIRECTION_DOWNLINK_V02 ((coex_tech_band_direction_v02)0x01) /**<  Indicates that the band information is for downlink.  */
#define COEX_TECH_BAND_DIRECTION_UPLINK_V02 ((coex_tech_band_direction_v02)0x02) /**<  Indicates that the band information is for uplink.  */
/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint16_t frequency;
  /**<   Radio access technology's operating center frequency in MHz (times 10).
       - NOTE: This is represented in x10 to provide fractional precision of 
               1 decimal point. 
       - Valid range is 0.0 MHz to 6553.5 MHz
       - Values are 0 to 65535 (2^16-1)
  */

  uint16_t bandwidth_and_direction;
  /**<   Radio access technology's operating bandwidth in MHz (times 10) bit-packed 
       with the link direction.
       - bit[15:14] - coex_tech_band_direction - Uplink/Downlink/Both/Don't Care
                      NOTE: a band can be specified as both UL and DL by combining 
                      the mask values of UL and DL.
       - bit[13:0]  - indicates the bandwidth in x10 to provide fractional precision 
                      of 1 decimal point. 
                      Valid range is 0.0 MHz to 1638.3 MHz
                      Values are 0 to 16383 (2^14-1)
  */
}coex_tech_band_type_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_ANTENNA_CHAIN_STATE_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_ANTENNA_CHAIN_STATE_INACTIVE_V02 = 0, /**<  Indicates that the antenna chain is inactive (not in use by modem)  */
  COEX_ANTENNA_CHAIN_STATE_ACTIVE_V02 = 1, /**<  Indicates that the antenna chain is active (in use by modem)  */
  COEX_ANTENNA_CHAIN_STATE_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_antenna_chain_state_v02;
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint16_t adv_notice;
  /**<    Indicates delta time (in future) in milli-seconds when the specified 
        coex_antenna_chain_state will take into effect. */

  uint16_t duration;
  /**<    Indicates delta time in milli-seconds beyond the adv_notice that the 
        coex_antenna_chain_state will remain in effect. */
}coex_antenna_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint8_t id;
  /**<   Bit-packed to represent,
       - bit[7:6] - reserved for future use
       - bit[5:0] - coex_tech_type - identifies the COEX technology type
  */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed.
                    Valid ranges from 0 to 31.
  */
}coex_tech_sync_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Request Message; Request to synchronize COEX state(s) between modules */
typedef struct {

  /* Optional */
  /*  WWAN/WLAN Technology(s) Sync State */
  uint8_t tech_valid;  /**< Must be set to true if tech is being passed */
  uint32_t tech_len;  /**< Must be set to # of elements in tech */
  coex_tech_sync_state_v02 tech[COEX_MAX_TECHS_SUPPORTED_V02];
  /**<   Used to share current COEX sync state for all valid/current technologies
       It is an array of the following elements,
          id - identifies the technology, bit-packed to represent,
            bit[7:6] - reserved for future use
            bit[5:0] - coex_tech_type - identifies the COEX technology type
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed. 
                       Valid ranges from 0 to 31
  */
}qmi_coex_tech_sync_req_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Response Message; Response to synchronize COEX state(s) between modules */
typedef struct {

  /* Optional */
  /*  WWAN/WLAN Technology(s) Sync State */
  uint8_t tech_valid;  /**< Must be set to true if tech is being passed */
  uint32_t tech_len;  /**< Must be set to # of elements in tech */
  coex_tech_sync_state_v02 tech[COEX_MAX_TECHS_SUPPORTED_V02];
  /**<   Used to share current COEX sync state for all valid/current techs
       It is an array of the following elements,
          id - identifies the technology, bit-packed to represent,
            bit[7:6] - reserved for future use
            bit[5:0] - coex_tech_type - identifies the COEX technology type
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed. 
                       Valid ranges from 0 to 31
  */
}qmi_coex_tech_sync_resp_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Indication to update specific COEX state between modules */
typedef struct {

  /* Optional */
  /*  WWAN/WLAN Technology(s) Sync State */
  uint8_t tech_valid;  /**< Must be set to true if tech is being passed */
  coex_tech_sync_state_v02 tech;
  /**<   Used to share current COEX SYNC state for current technology
        id - identifies the technology, bit-packed to represent,
          bit[7:6] - reserved for future use
          bit[5:0] - coex_tech_type - identifies the COEX technology type
        sync_state_token - contains technology's sync state & token, bit-packed to represent,
          bit[7:5] - coex_tech_state - informs of technology's current state
          bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}qmi_coex_tech_sync_update_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint8_t id;
  /**<   Bit-packed to represent,
       - bit[7:6] - reserved for future use
       - bit[5:0] - coex_tech_type - identifies the COEX technology type */
}coex_tech_state_update_req_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Request Message; Request for update of COEX state of various technology(s) */
typedef struct {

  /* Optional */
  /*  WWAN/WLAN Technology(s) Sync State */
  uint8_t tech_ids_valid;  /**< Must be set to true if tech_ids is being passed */
  uint32_t tech_ids_len;  /**< Must be set to # of elements in tech_ids */
  coex_tech_state_update_req_v02 tech_ids[COEX_MAX_TECHS_SUPPORTED_V02];
  /**<   Used to request current COEX state for selected technology(s)
       It is an array of the following elements,
          id - identifies the technology, bit-packed to represent,
            bit[7:6] - reserved for future use
            bit[5:0] - coex_tech_type - identifies the COEX technology type
  */
}qmi_coex_tech_state_update_req_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_LTE_TDD_CONFIG_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_LTE_TDD_CONFIG_UNDEF_V02 = 0, /**<  Unintialized LTE TDD config  */
  COEX_LTE_TDD_CONFIG_0_V02 = 1, /**<  LTE TDD config 0  */
  COEX_LTE_TDD_CONFIG_1_V02 = 2, /**<  LTE TDD config 1  */
  COEX_LTE_TDD_CONFIG_2_V02 = 3, /**<  LTE TDD config 2  */
  COEX_LTE_TDD_CONFIG_3_V02 = 4, /**<  LTE TDD config 3  */
  COEX_LTE_TDD_CONFIG_4_V02 = 5, /**<  LTE TDD config 4  */
  COEX_LTE_TDD_CONFIG_5_V02 = 6, /**<  LTE TDD config 5  */
  COEX_LTE_TDD_CONFIG_6_V02 = 7, /**<  LTE TDD config 6  */
  COEX_LTE_TDD_CONFIG_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_lte_tdd_config_v02;
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_LTE_TDD_SUBFRAME_CONFIG_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_LTE_TDD_SUBFRAME_CONFIG_UNDEF_V02 = 0, /**<  Unintialized LTE TDD config  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_0_V02 = 1, /**<  LTE TDD special subframe config 0  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_1_V02 = 2, /**<  LTE TDD special subframe config 1  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_2_V02 = 3, /**<  LTE TDD special subframe config 2  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_3_V02 = 4, /**<  LTE TDD special subframe config 3  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_4_V02 = 5, /**<  LTE TDD special subframe config 4  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_5_V02 = 6, /**<  LTE TDD special subframe config 5  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_6_V02 = 7, /**<  LTE TDD special subframe config 6  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_7_V02 = 8, /**<  LTE TDD special subframe config 7  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_8_V02 = 9, /**<  LTE TDD special subframe config 8  */
  COEX_LTE_TDD_SUBFRAME_CONFIG_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_lte_tdd_subframe_config_v02;
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_LTE_TDD_LINK_CONFIG_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_LTE_TDD_LINK_UNDEF_V02 = 0, /**<  Uninitialized LTE TDD Link  */
  COEX_LTE_TDD_LINK_NORMAL_V02 = 1, /**<  Normal cyclic prefix.  */
  COEX_LTE_TDD_LINK_EXTENDED_V02 = 2, /**<   Extended cyclic prefix.  */
  COEX_LTE_TDD_LINK_CONFIG_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_lte_tdd_link_config_v02;
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN LTE's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */

  uint16_t data;
  /**<   Bit-packed to represent, 
       - bit[15]    - coex_tech_operating_dimension - indicates LTE's dimension
       - bit[14:10] - reserved for future use
       - bit[9:0]   - frame offset (timing advance) in micro-seconds valid range is from 0 to 1023 micro-seconds
  */

  uint16_t tdd_info;
  /**<   Contains LTE TDD parameters bit-packed to represent, 
       - bit[15:12] - Reserved for future use
       - bit[11:10] - Uplink coex_lte_tdd_link_config
       - bit[9:8]   - Downlink coex_lte_tdd_link_config
       - bit[7:4]   - coex_lte_tdd_config
       - bit[3:0]   - coex_lte_tdd_subframe_config
  */
}coex_wwan_lte_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN LTE technology */
typedef struct {

  /* Optional */
  /*  WWAN LTE Technology's State Information for COEX */
  uint8_t lte_band_info_valid;  /**< Must be set to true if lte_band_info is being passed */
  coex_wwan_lte_state_v02 lte_band_info;
  /**<   Contains LTE state information needed for COEX 
       It comprises of,
          an array of WWAN LTE's current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
          data - WWAN LTE technology data, bit-packed to represent, 
            bit[15]    - coex_tech_operating_dimension - indicates LTE's dimension
            bit[14:10] - reserved for future use
            bit[9:0]   - frame offset (timing advance) in micro-seconds
                         valid range is from 0 to 1023 micro-seconds
          tdd_info - Contains LTE TDD parameters bit-packed to represent, 
            bit[15:12] - Reserved for future use
            bit[11:10] - Uplink coex_lte_tdd_link_config
            bit[9:8]   - Downlink coex_lte_tdd_link_config
            bit[7:4]   - coex_lte_tdd_config
            bit[3:0]   - coex_lte_tdd_subframe_config
  */
}qmi_coex_wwan_lte_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN TDSCDMA's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wwan_tdscdma_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN TDSCDMA technology */
typedef struct {

  /* Optional */
  /*  WWAN TDSCDMA Technology's State Information for COEX */
  uint8_t tdscdma_band_info_valid;  /**< Must be set to true if tdscdma_band_info is being passed */
  coex_wwan_tdscdma_state_v02 tdscdma_band_info;
  /**<   Contains TDSCDMA state information needed for COEX
       It comprises of,
          an array of WWAN TDSCDMA's current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_tdscdma_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_GSM_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN GSM's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wwan_gsm_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN GSM [Instance 1] technology */
typedef struct {

  /* Optional */
  /*  WWAN GSM [Instance 1] Technology's State Information for COEX */
  uint8_t gsm_band_info_valid;  /**< Must be set to true if gsm_band_info is being passed */
  coex_wwan_gsm_state_v02 gsm_band_info;
  /**<   Contains GSM [Instance 1] state information needed for COEX
       It comprises of,
          an array of WWAN GSM [Instance 1]'s current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_gsm_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN GSM [Instance 2] technology */
typedef struct {

  /* Optional */
  /*  WWAN GSM [Instance 2] Technology's State Information for COEX */
  uint8_t gsm2_band_info_valid;  /**< Must be set to true if gsm2_band_info is being passed */
  coex_wwan_gsm_state_v02 gsm2_band_info;
  /**<   Contains GSM [Instance 2] state information needed for COEX
       It comprises of,
          an array of WWAN GSM [Instance 2]'s current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_gsm2_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN GSM [Instance 3] technology */
typedef struct {

  /* Optional */
  /*  WWAN GSM [Instance 3] Technology's State Information for COEX */
  uint8_t gsm3_band_info_valid;  /**< Must be set to true if gsm3_band_info is being passed */
  coex_wwan_gsm_state_v02 gsm3_band_info;
  /**<   Contains GSM [Instance 3] state information needed for COEX
       It comprises of,
          an array of WWAN GSM [Instance 3]'s current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_gsm3_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN ONEX's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wwan_onex_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN ONEX technology */
typedef struct {

  /* Optional */
  /*  WWAN ONEX technology's state information for COEX */
  uint8_t onex_band_info_valid;  /**< Must be set to true if onex_band_info is being passed */
  coex_wwan_onex_state_v02 onex_band_info;
  /**<   Contains ONEX state information needed for COEX
       It comprises of,
          an array of WWAN ONEX's current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_onex_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN HDR's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wwan_hdr_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN HDR technology */
typedef struct {

  /* Optional */
  /*  WWAN HDR technology's state information for COEX */
  uint8_t hdr_band_info_valid;  /**< Must be set to true if hdr_band_info is being passed */
  coex_wwan_hdr_state_v02 hdr_band_info;
  /**<   Contains HDR state information needed for COEX
       It comprises of,
          an array of WWAN HDR's current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_hdr_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WWAN_TECH_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents WWAN WCDMA's current operating frequencies */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wwan_wcdma_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WWAN WCDMA technology */
typedef struct {

  /* Optional */
  /*  WWAN WCDMA technology's state information for COEX */
  uint8_t wcdma_band_info_valid;  /**< Must be set to true if wcdma_band_info is being passed */
  coex_wwan_wcdma_state_v02 wcdma_band_info;
  /**<   Contains WCDMA state information needed for COEX
       It comprises of,
          an array of WWAN WCDMA's current operating frequency & bandwidth information(s)
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wwan_wcdma_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t band_info_len;  /**< Must be set to # of elements in band_info */
  coex_tech_band_type_v02 band_info[COEX_WLAN_WIFI_MAX_BANDS_SUPPORTED_V02];
  /**<   Represents current WLAN Wifi's CONNECTION operating frequencies */
}coex_wlan_wifi_connection_info_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint32_t high_priority_events_list_len;  /**< Must be set to # of elements in high_priority_events_list */
  coex_tech_band_type_v02 high_priority_events_list[COEX_WLAN_WIFI_MAX_EVENTS_SUPPORTED_V02];
  /**<   List of active WLAN-WiFi frequencies with on-going high priority events */

  uint32_t connections_list_len;  /**< Must be set to # of elements in connections_list */
  coex_wlan_wifi_connection_info_v02 connections_list[COEX_WLAN_WIFI_MAX_CONNS_SUPPORTED_V02];
  /**<   List of active WLAN-WiFi on-going connections */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_wlan_wifi_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of COEX state for WLAN WiFi technology */
typedef struct {

  /* Optional */
  /*  WLAN WiFi technology's state information for COEX */
  uint8_t wifi_state_info_valid;  /**< Must be set to true if wifi_state_info is being passed */
  coex_wlan_wifi_state_v02 wifi_state_info;
  /**<   Contains WLAN WiFi state information needed for COEX
       It comprises of,
          list of active WLAN-WiFi frequencies with on-going high priority events
          list of active WLAN-WiFi on-going connections
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_wlan_wifi_state_ind_msg_v02;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of 
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}qmi_coex_wlan_bt_state_ind_msg_v02;

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  coex_antenna_state_v02 data;
  /**<   Represent state of the diversity antenna state
       It comprises of,
         adv_notice - time (in future) in milli-seconds when the above mentioned 
                      coex_antenna_chain_state will take into effect
         duration - time in milli-seconds beyond the adv_notice that the 
                    above mentioned coex_antenna_chain_state will remain 
                    in effect
  */

  uint8_t sync_state_token;
  /**<   Bit-packed to represent,
       - bit[7:5] - coex_tech_state - informs of technology's current state
       -- NOTE: only bit[7] applies & represented by coex_antenna_chain_state, bit[6:5] are don't cares
       - bit[4:0] - 5-bit revolving 'token' to indicate if state changed valid ranges from 0 to 31
  */
}coex_diversity_antenna_state_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Informs of the state of diversity antenna chain */
typedef struct {

  /* Optional */
  /*  Antenna Chain State Information for COEX */
  uint8_t state_valid;  /**< Must be set to true if state is being passed */
  coex_diversity_antenna_state_v02 state;
  /**<   Contains current state of diversity antenna chain needed for COEX
       It comprises of,
          data - contains state of the diversity antenna state and it comprises of,
            adv_notice - time (in future) in milli-seconds when the above mentioned 
                         coex_antenna_chain_state will take into effect
            duration - time in milli-seconds beyond the adv_notice that the 
                       above mentioned coex_antenna_chain_state will remain 
                       in effect
          sync_state_token - contains technology's sync state & token, bit-packed to represent,
            bit[7:5] - coex_tech_state - informs of technology's current state
                       NOTE
                         -- only bit[7] applies to antenna chain state
                             and is represented by coex_antenna_chain_state,
                             bit[6:5] are don't cares
            bit[4:0] - 5-bit revolving 'token' to indicate if state changed
                       valid ranges from 0 to 31
  */
}qmi_coex_diversity_antenna_state_ind_msg_v02;  /* Message */
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint8_t filter_alpha;
  /**<   The "history" parameter for the 1st order LTE SINR metrics filter 
       Valid range: 0 to 1 with 1/100th precision
	   Represented as x100 (times 100): e.g. 0.1 -> 10, 0.89 -> 89
  */
}coex_metrics_lte_sinr_params_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Indication Message; Request to start collecting/collating the LTE Signal-to-Interface
              Plus Noise Ratio (SINR) metric. */
typedef struct {

  /* Optional */
  /*  Filter Parameters */
  uint8_t data_valid;  /**< Must be set to true if data is being passed */
  coex_metrics_lte_sinr_params_v02 data;
  /**<   Filter parameter(s) for LTE SINR metric.
       It comprises of,
       filter_alpha - The "history" parameter for the 1st order LTE SINR metrics filter 
       Valid range: 0.00 to 1.00 with 1/100th precision
       Represented as x100 (times 100): e.g. 0.10 -> 10, 0.89 -> 89
  */
}qmi_coex_metrics_lte_sinr_start_ind_msg_v02;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of 
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}qmi_coex_metrics_lte_sinr_read_req_msg_v02;

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  int8_t sinr_system;
  /**<   Filter output for the total (overall system)
       LTE SINR metrics in dBM 
       Likely range: -10 to +30 (with 1dBM resolution)
  */

  int8_t sinr_bt_only;
  /**<   Filter output for the LTE SINR metrics in dBM
       when Bluetooth is active
       Likely range: -10 to +30 (with 1dBM resolution)
  */

  int8_t sinr_wifi_only;
  /**<   Filter output for the LTE SINR metrics in dBM
       when Wi-Fi is active
       Likely range: -10 to +30 (with 1dBM resolution)
  */

  int8_t sinr_bt_and_wifi;
  /**<   Filter output for the LTE SINR metrics in dBM
       when both Bluetooth & Wi-Fi are active
       Likely range: -10 to +30 (with 1dBM resolution)
  */

  int8_t sinr_lte_only;
  /**<   Filter output for the LTE SINR metrics in dBM
       when only LTE is active (both Bluetooth & Wi-Fi are inactive)
       Likely range: -10 to +30 (with 1dBM resolution)
  */
}coex_metrics_lte_sinr_stats_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_enums
    @{
  */
typedef enum {
  COEX_METRICS_ESTATUS_MIN_ENUM_VAL_V02 = -2147483647, /**< To force a 32 bit signed enum.  Do not change or use*/
  COEX_METRICS_E_SUCCESS_V02 = 0x00, /**<  Represents metrics collected/reported successfully  */
  COEX_METRICS_E_NOT_STARTED_V02 = 0x01, /**<  Represents metric collection not started (request never made)  */
  COEX_METRICS_E_TECH_NOT_ACTIVE_V02 = 0x02, /**<  Represents metrics technology not present/active  */
  COEX_METRICS_ESTATUS_MAX_ENUM_VAL_V02 = 2147483647 /**< To force a 32 bit signed enum.  Do not change or use*/
}coex_metrics_estatus_v02;
/**
    @}
  */

/** @addtogroup coex_qmi_aggregates
    @{
  */
typedef struct {

  uint8_t estatus;
  /**<   Validates the enclosed LTE SINR metrics collected
  */

  uint8_t collection_duration;
  /**<   Represents the duration for which the metric collection 
       has been running
       Units: 10s of milli-seconds
       Range: 0x00 -> 0 milli-seconds to 0xFE -> 254*10=2540 milli-seconds
       Special values:
       - 0x00: either just started or not started (see estatus above)
       - 0xFF: greater than 2.54 seconds has passed
  */

  coex_metrics_lte_sinr_stats_v02 stats;
  /**<   Summary report of all the LTE SINR metrics collected thus far
  */
}coex_metrics_lte_sinr_report_v02;  /* Type */
/**
    @}
  */

/** @addtogroup coex_qmi_messages
    @{
  */
/** Response Message; Response to provide requested filter output for the LTE SINR metric. */
typedef struct {

  /* Optional */
  /*  LTE SINR metrics report */
  uint8_t report_valid;  /**< Must be set to true if report is being passed */
  coex_metrics_lte_sinr_report_v02 report;
  /**<   Represents current report of LTE SINR stats as requested
       It comprises of,
       estatus - coex_metrics_estatus - validates current status of these metrics
       stats - coex_metrics_lte_sinr_stats - summarizes all collected SINR stats
  */
}qmi_coex_metrics_lte_sinr_read_resp_msg_v02;  /* Message */
/**
    @}
  */

typedef struct {
  /* This element is a placeholder to prevent the declaration of 
     an empty struct.  DO NOT USE THIS FIELD UNDER ANY CIRCUMSTANCE */
  char __placeholder;
}qmi_coex_metrics_lte_sinr_stop_ind_msg_v02;

/*Service Message Definition*/
/** @addtogroup coex_qmi_msg_ids
    @{
  */
#define QMI_COEX_TECH_SYNC_REQ_V02 0x0020
#define QMI_COEX_TECH_SYNC_RESP_V02 0x0021
#define QMI_COEX_TECH_SYNC_UPDATE_IND_V02 0x0022
#define QMI_COEX_TECH_STATE_UPDATE_REQ_V02 0x0023
#define QMI_COEX_WWAN_LTE_STATE_IND_V02 0x0024
#define QMI_COEX_WWAN_TDSCDMA_STATE_IND_V02 0x0025
#define QMI_COEX_WWAN_GSM_STATE_IND_V02 0x0026
#define QMI_COEX_WWAN_ONEX_STATE_IND_V02 0x0027
#define QMI_COEX_WWAN_HDR_STATE_IND_V02 0x0028
#define QMI_COEX_WWAN_WCDMA_STATE_IND_V02 0x0029
#define QMI_COEX_WLAN_WIFI_STATE_IND_V02 0x002A
#define QMI_COEX_WLAN_BT_STATE_IND_V02 0x002B
#define QMI_COEX_DIVERSITY_ANTENNA_STATE_IND_V02 0x002C
#define QMI_COEX_METRICS_LTE_SINR_START_IND_V02 0x002D
#define QMI_COEX_METRICS_LTE_SINR_READ_REQ_V02 0x002E
#define QMI_COEX_METRICS_LTE_SINR_READ_RESP_V02 0x002F
#define QMI_COEX_METRICS_LTE_SINR_STOP_IND_V02 0x0030
#define QMI_COEX_WWAN_GSM2_STATE_IND_V02 0x0031
#define QMI_COEX_WWAN_GSM3_STATE_IND_V02 0x0032
/**
    @}
  */

/* Service Object Accessor */
/** @addtogroup wms_qmi_accessor 
    @{
  */
/** This function is used internally by the autogenerated code.  Clients should use the
   macro coex_get_service_object_v02( ) that takes in no arguments. */
qmi_idl_service_object_type coex_get_service_object_internal_v02
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version );
 
/** This macro should be used to get the service object */ 
#define coex_get_service_object_v02( ) \
          coex_get_service_object_internal_v02( \
            COEX_V02_IDL_MAJOR_VERS, COEX_V02_IDL_MINOR_VERS, \
            COEX_V02_IDL_TOOL_VERS )
/** 
    @} 
  */


#ifdef __cplusplus
}
#endif
#endif

