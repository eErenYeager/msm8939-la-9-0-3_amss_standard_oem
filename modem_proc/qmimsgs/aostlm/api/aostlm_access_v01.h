#ifndef AOSTLM_SERVICE_01_H
#define AOSTLM_SERVICE_01_H
/**
  @file aostlm_access_v01.h

  @brief This is the public header file which defines the aostlm service Data structures.

  This header file defines the types and structures that were defined in
  aostlm. It contains the constant values defined, enums, structures,
  messages, and service message IDs (in that order) Structures that were
  defined in the IDL as messages contain mandatory elements, optional
  elements, a combination of mandatory and optional elements (mandatory
  always come before optionals in the structure), or nothing (null message)

  An optional element in a message is preceded by a uint8_t value that must be
  set to true if the element is going to be included. When decoding a received
  message, the uint8_t values will be set to true or false by the decode
  routine, and should be checked before accessing the values that they
  correspond to.

  Variable sized arrays are defined as static sized arrays with an unsigned
  integer (32 bit) preceding it that must be set to the number of elements
  in the array that are valid. For Example:

  uint32_t test_opaque_len;
  uint8_t test_opaque[16];

  If only 4 elements are added to test_opaque[] then test_opaque_len must be
  set to 4 before sending the message.  When decoding, the _len value is set 
  by the decode routine and should be checked so that the correct number of
  elements in the array will be accessed.

*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2014 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.



  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/qmimsgs/aostlm/api/aostlm_access_v01.h#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====* 
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.10 
   It was generated on: Wed Jul 30 2014 (Spin 0)
   From IDL File: aostlm_access_v01.idl */

/** @defgroup aostlm_qmi_consts Constant values defined in the IDL */
/** @defgroup aostlm_qmi_msg_ids Constant values for QMI message IDs */
/** @defgroup aostlm_qmi_enums Enumerated types used in QMI messages */
/** @defgroup aostlm_qmi_messages Structures sent as QMI messages */
/** @defgroup aostlm_qmi_aggregates Aggregate types used in QMI messages */
/** @defgroup aostlm_qmi_accessor Accessor for QMI service object */
/** @defgroup aostlm_qmi_version Constant values for versioning information */

#include <stdint.h>
#include "qmi_idl_lib.h"


#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup aostlm_qmi_version
    @{
  */
/** Major Version Number of the IDL used to generate this file */
#define AOSTLM_V01_IDL_MAJOR_VERS 0x01
/** Revision Number of the IDL used to generate this file */
#define AOSTLM_V01_IDL_MINOR_VERS 0x00
/** Major Version Number of the qmi_idl_compiler used to generate this file */
#define AOSTLM_V01_IDL_TOOL_VERS 0x06
/** Maximum Defined Message ID */
#define AOSTLM_V01_MAX_MESSAGE_ID 0x0020
/**
    @}
  */


/** @addtogroup aostlm_qmi_consts 
    @{ 
  */

/** 
 AOSTLM QMI Service ID  */
#define AOSTLM_QMI_SVC_ID_V01 0x3B

/**  Maximum length of the AOSTLM QMI message  */
#define AOSTLM_MAX_QMI_MSG_LENGTH_V01 3072
#define AOSTLM_RESULT_SUCCESS_V01 0
#define AOSTLM_RESULT_FAILURE_V01 1
/**
    @}
  */

/** @addtogroup aostlm_qmi_aggregates
    @{
  */
typedef struct {

  /*  This structure shall be the first element of every response message  */
  uint8_t aostlm_result_t;

  /* 0 == SUCCESS; 1 == FAILURE
     A result of FAILURE indicates that that any data contained in the response
     should not be used  */
  uint8_t aostlm_err_t;
}aostlm_common_resp_s_v01;  /* Type */
/**
    @}
  */

/** @addtogroup aostlm_qmi_messages
    @{
  */
/** Request Message; This command carries the access request from the AOSTLM client to
           the AOSTLM License Manager QMI Server. */
typedef struct {

  /* Mandatory */
  /*  Length */
  uint16_t length;

  /* Mandatory */
  /*  Blob */
  uint8_t blob[AOSTLM_MAX_QMI_MSG_LENGTH_V01];
}aostlm_access_client_req_msg_v01;  /* Message */
/**
    @}
  */

/** @addtogroup aostlm_qmi_messages
    @{
  */
/** Response Message; This command carries the access request from the AOSTLM client to
           the AOSTLM License Manager QMI Server. */
typedef struct {

  /* Mandatory */
  /*  Response */
  aostlm_common_resp_s_v01 resp;

  /* Optional */
  /*  Length */
  uint8_t length_valid;  /**< Must be set to true if length is being passed */
  uint16_t length;

  /* Optional */
  /*  Blob */
  uint8_t blob_valid;  /**< Must be set to true if blob is being passed */
  uint8_t blob[AOSTLM_MAX_QMI_MSG_LENGTH_V01];
}aostlm_access_client_resp_msg_v01;  /* Message */
/**
    @}
  */

/* Conditional compilation tags for message removal */ 
//#define REMOVE_AOSTLM_ACCESS_CLIENT_REQ_V01 

/*Service Message Definition*/
/** @addtogroup aostlm_qmi_msg_ids
    @{
  */
#define AOSTLM_ACCESS_CLIENT_REQ_V01 0x0020
#define AOSTLM_ACCESS_CLIENT_RESP_V01 0x0020
/**
    @}
  */

/* Service Object Accessor */
/** @addtogroup wms_qmi_accessor 
    @{
  */
/** This function is used internally by the autogenerated code.  Clients should use the
   macro aostlm_get_service_object_v01( ) that takes in no arguments. */
qmi_idl_service_object_type aostlm_get_service_object_internal_v01
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version );
 
/** This macro should be used to get the service object */ 
#define aostlm_get_service_object_v01( ) \
          aostlm_get_service_object_internal_v01( \
            AOSTLM_V01_IDL_MAJOR_VERS, AOSTLM_V01_IDL_MINOR_VERS, \
            AOSTLM_V01_IDL_TOOL_VERS )
/** 
    @} 
  */


#ifdef __cplusplus
}
#endif
#endif

