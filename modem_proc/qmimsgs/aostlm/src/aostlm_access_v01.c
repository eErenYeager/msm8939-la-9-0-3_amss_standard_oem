/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        A O S T L M _ A C C E S S _ V 0 1  . C

GENERAL DESCRIPTION
  This is the file which defines the aostlm service Data structures.

  Copyright (c) 2014 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.



  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/qmimsgs/aostlm/src/aostlm_access_v01.c#1 $
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====* 
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY 
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 6.10 
   It was generated on: Wed Jul 30 2014 (Spin 0)
   From IDL File: aostlm_access_v01.idl */

#include "stdint.h"
#include "qmi_idl_lib_internal.h"
#include "aostlm_access_v01.h"


/*Type Definitions*/
static const uint8_t aostlm_common_resp_s_data_v01[] = {
  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(aostlm_common_resp_s_v01, aostlm_result_t),

  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(aostlm_common_resp_s_v01, aostlm_err_t),

  QMI_IDL_FLAG_END_VALUE
};

/*Message Definitions*/
static const uint8_t aostlm_access_client_req_msg_data_v01[] = {
  0x01,
   QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(aostlm_access_client_req_msg_v01, length),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x02,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(aostlm_access_client_req_msg_v01, blob),
  ((AOSTLM_MAX_QMI_MSG_LENGTH_V01) & 0xFF), ((AOSTLM_MAX_QMI_MSG_LENGTH_V01) >> 8)
};

static const uint8_t aostlm_access_client_resp_msg_data_v01[] = {
  2,
   QMI_IDL_AGGREGATE,
  QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, resp),
  QMI_IDL_TYPE88(0, 0),

  QMI_IDL_TLV_FLAGS_OPTIONAL | (QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, length) - QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, length_valid)),
  0x10,
   QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, length),

  QMI_IDL_TLV_FLAGS_LAST_TLV | QMI_IDL_TLV_FLAGS_OPTIONAL | (QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, blob) - QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, blob_valid)),
  0x11,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(aostlm_access_client_resp_msg_v01, blob),
  ((AOSTLM_MAX_QMI_MSG_LENGTH_V01) & 0xFF), ((AOSTLM_MAX_QMI_MSG_LENGTH_V01) >> 8)
};

/* Type Table */
static const qmi_idl_type_table_entry  aostlm_type_table_v01[] = {
  {sizeof(aostlm_common_resp_s_v01), aostlm_common_resp_s_data_v01}
};

/* Message Table */
static const qmi_idl_message_table_entry aostlm_message_table_v01[] = {
  {sizeof(aostlm_access_client_req_msg_v01), aostlm_access_client_req_msg_data_v01},
  {sizeof(aostlm_access_client_resp_msg_v01), aostlm_access_client_resp_msg_data_v01}
};

/* Range Table */
/* Predefine the Type Table Object */
static const qmi_idl_type_table_object aostlm_qmi_idl_type_table_object_v01;

/*Referenced Tables Array*/
static const qmi_idl_type_table_object *aostlm_qmi_idl_type_table_object_referenced_tables_v01[] =
{&aostlm_qmi_idl_type_table_object_v01};

/*Type Table Object*/
static const qmi_idl_type_table_object aostlm_qmi_idl_type_table_object_v01 = {
  sizeof(aostlm_type_table_v01)/sizeof(qmi_idl_type_table_entry ),
  sizeof(aostlm_message_table_v01)/sizeof(qmi_idl_message_table_entry),
  1,
  aostlm_type_table_v01,
  aostlm_message_table_v01,
  aostlm_qmi_idl_type_table_object_referenced_tables_v01,
  NULL
};

/*Arrays of service_message_table_entries for commands, responses and indications*/
static const qmi_idl_service_message_table_entry aostlm_service_command_messages_v01[] = {
  {AOSTLM_ACCESS_CLIENT_REQ_V01, QMI_IDL_TYPE16(0, 0), 3080}
};

static const qmi_idl_service_message_table_entry aostlm_service_response_messages_v01[] = {
  {AOSTLM_ACCESS_CLIENT_RESP_V01, QMI_IDL_TYPE16(0, 1), 3085}
};

/*Service Object*/
struct qmi_idl_service_object aostlm_qmi_idl_service_object_v01 = {
  0x06,
  0x01,
  AOSTLM_QMI_SVC_ID_V01,
  3085,
  { sizeof(aostlm_service_command_messages_v01)/sizeof(qmi_idl_service_message_table_entry),
    sizeof(aostlm_service_response_messages_v01)/sizeof(qmi_idl_service_message_table_entry),
    0 },
  { aostlm_service_command_messages_v01, aostlm_service_response_messages_v01, NULL},
  &aostlm_qmi_idl_type_table_object_v01,
  0x00,
  NULL
};

/* Service Object Accessor */
qmi_idl_service_object_type aostlm_get_service_object_internal_v01
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version ){
  if ( AOSTLM_V01_IDL_MAJOR_VERS != idl_maj_version || AOSTLM_V01_IDL_MINOR_VERS != idl_min_version 
       || AOSTLM_V01_IDL_TOOL_VERS != library_version) 
  {
    return NULL;
  } 
  return (qmi_idl_service_object_type)&aostlm_qmi_idl_service_object_v01;
}

