#===============================================================================
#
# IMSDCM (IP Multimedia Subsystem Data Connection Management) QMI APIs
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2012-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#                      EDIT HISTORY FOR FILE
#
#  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/qmimsgs/imsdcm/build/qmi_imsdcm.scons#1 $
#  $DateTime: 2015/01/27 06:42:19 $
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 04/22/13   pm      Change from AddLibrary to AddBinaryLibrary
# 11/30/12   mpa     Baseline version
#===============================================================================
Import('env')
env = env.Clone()
from glob import glob
from os.path import join, basename

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

# ------------------------------------------------------------------------------
# Include Paths
#---------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# APIs needed to build the IMSDCM APIs
#-------------------------------------------------------------------------------

# Should be first
env.RequirePublicApi([
  'IMSDCM',
  ])

env.RequirePublicApi([
  'MPROC',
  ], area='CORE')

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c
IMSDCM_C_SOURCES = ['${BUILDPATH}/' + basename(fname)
                 for fname in glob(join(env.subst(SRCPATH), '*.c'))]

# Add our library to the following build tags:
#   QMIMSGS_MPSS for the MPSS image
env.AddBinaryLibrary (['QMIMSGS_MPSS'], 
                '${BUILDPATH}/qmimsgs_imsdcm', [IMSDCM_C_SOURCES])

# Load test units
env.LoadSoftwareUnits()
