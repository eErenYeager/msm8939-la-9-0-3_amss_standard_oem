#ifndef QFE2520_CALIBRATION_DATA_H
#define QFE2520_CALIBRATION_DATA_H
/*!
  @file
  qfe2520_calibration_data.h 

  @brief
  Calibrated data of QFE2520 physical device.
*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_qfe2520/api/qfe2520_calibration_data.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
04/04/14   ndb    Added TDET support
12/25/13   yb     Inital version

===========================================================================*/

#include "rfdevice_physical_device.h"

#include "qfe2520_tuner_typedef_ag.h"
 
typedef struct
{
 /* R tuner cal data */
  boolean r_tuner_data_valid;  
  uint8 r_tuner_val[QFE2520_NUM_R];
 
 /* C tuner cal data */
  boolean c_tuner_data_valid;  
  uint8 c_tuner_val[QFE2520_NUM_C];
 
}qfe2520_rc_tuner_cal_data_type;


/* K sensor cal data */
typedef struct
{
  boolean data_valid;
  uint16 k0_value;
}qfe2520_k_sens_cal_data_type;


/*Calibrated data */ 
typedef struct qfe2520_cal_data
{
  /* Device cfg info */
  rfc_device_cfg_info_type cfg;

  /* RC tuner cal data */
  qfe2520_rc_tuner_cal_data_type rc_tuner_data;
   
  /*K Sensor Cal data*/
  qfe2520_k_sens_cal_data_type k_sensor_data;
   
}qfe2520_cal_data_type_declaration;

#endif