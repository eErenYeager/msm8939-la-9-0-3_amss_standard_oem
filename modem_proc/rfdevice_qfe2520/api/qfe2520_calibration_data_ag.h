
#ifndef QFE2520_CALIBRATION_DATA_H
#define QFE2520_CALIBRATION_DATA_H
/*
WARNING: This QFE2520 physical device driver is auto-generated.

Generated using: qfe_physical_device_autogen.pl 
Generated from-  

	File: QFE2520_RFFE_Settings.xlsx 
	Released: 
	Author: 
	Revision: 
	Change Note: 

*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of QFE2520 CHIP.

Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.

$Header: //source/qcom/qct/modem/rfdevice/qfe2520/main/latest/etc/qfe_physical_device_autogen.pl#1 : dnatesan : 2013/12/19 02:36:21 60============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/


#include "rfdevice_physical_device.h"

#include "qfe2520_tuner_typedef_ag.h"
 
typedef struct
{
 /* R tuner cal data */
  boolean r_tuner_data_valid;  
  uint8 r_tuner_val[QFE2520_NUM_R];
 
 /* C tuner cal data */
  boolean c_tuner_data_valid;  
  uint8 c_tuner_val[QFE2520_NUM_C];
 
}qfe2520_rc_tuner_cal_data_type;

/*Calibrated data */ 
typedef struct qfe2520_cal_data
{
  /* Device cfg info */
  rfc_device_cfg_info_type cfg;

  /* RC tuner cal data */
  qfe2520_rc_tuner_cal_data_type rc_tuner_data;
   
}qfe2520_cal_data_type_declaration;

#endif