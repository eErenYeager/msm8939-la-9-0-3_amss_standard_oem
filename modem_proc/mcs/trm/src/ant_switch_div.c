
/*!
  @file
  ant_switch_div.c
 
  @brief
 
  Ant Switch Diversity apis are used for antenna arbitration and switch state
  maintenence. For non-SV(ATnT), TRM is used to just maintain switch state
  over L1 sleep cycles. For SV, TRM acts like a switch arbiter.
 
  */

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/trm/src/ant_switch_div.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/23/15   sk      Send switch start indication for 1x acqusition switching
					scenario(810864)
11/10/14   sk      Allow 1x acquisition switching if there is another tech in
					traffic(700858)
11/02/14   sk      update preferred config for all switching request(748320)
09/04/14   sk      change the sequence of passive switching- 
		   first update switch to transition and then call callback 
07/07/14   sk      fix KW issues 
06/19/14   sk      Consider all higher priority techs before making swithcing
					permission (682228)
06/19/14   sk      Update DSDS mode in DSDS fallback scenario(CR:681600)
05/27/14   sk      update preferred config based on NV default value 
04/08/14   sk      Fixed mob compilation error
04/08/14   sk      Correct logic to add/remove clients in active tech list
03/20/14   sk      Add SVLTE+G DSDA ASDiv support, new api to block switching
		    		block switching if there is another tech in acq or idle
02/28/14   sk      Check switch permission only if current config is different
					for idlewakeup case
02/06/14   sk      report switch initiation to passive tx tech in dual tx
01/15/14   sk      kw error fix
01/15/14   sk      DSDA ASDiv support
05/24/13   sk      Initial Rev

===========================================================================*/


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include <msg.h>
#include <err.h>
#include <event.h>

#include "modem_mcs_defs.h"

#include "fs_sys_types.h"
#include "fs_public.h"
#include "fs_errno.h"

#include "sys.h"
#include "trm.h"
#include "trmi.h"
#include "subs_prio.h"


/*===========================================================================

                         CONSTANTS, MACRO DEFINITIONS

===========================================================================*/

typedef uint8		client_mode_val_type;
typedef	uint8		agg_client_mode_type;

/*! BIT Manipulation macros */
/*set specific bit of a 32 bit unsigned flag using bit position*/
#define  SET_BIT_POS(flag, pos) (flag |= ((uint32)(0x1<<pos)))

/*clear specific bit of a 32 bit unsigned flag using bit position*/
#define  CLR_BIT_POS(flag, pos) ( flag &= ( ~((uint32)(0x1<<pos)) ))

/*check specific bit of a 32 bit unsigned flag using bit position*/
#define  CHECK_BIT_POS(flag, pos) ( (flag) & ((uint32)(0x1<<pos)))

#define ASDIV_MAX_CLIENT		TRM_LAST_CLIENT+1

/*!BIT position definitions of CONFIGURATION REGISTER, 
  These are set/reset at the time of initialization */

#define	BIT_DEFAULT_KEEP_IN_IDLE		0
#define BIT_DEFAULT_KEEP_IN_SVDO		1
#define BIT_DEFAULT_KEEP_IN_SVLTE		2
#define BIT_DEFAULT_KEEP_IN_SGLTE		3
#define BIT_DEFAULT_KEEP_IN_TXACQ		4
#define BIT_DEFAULT_KEEP_IN_ACQTX		5

#define BIT_ALLOW_SWITCHING_IN_SV		16
#define BIT_SWITCH_BY_TX_CL_ONLY		17

#define BIT_FEATURE_SUPPORTED			31


/*! BIT position definitions of STATUS flag,
   These bits keep changing */
/* switch is in privilege mode */
#define BIT_SWITCH_PRIVILEGE_CTRL		0
/* switching can be disabled by three main reason-
    1) when switch is in progress- transition
    2) when switch is in privilege mode
    3) when simultaneou/enforced mode is active
   following bits specify if switching is disabled by any 
   corresponding reason */
#define BIT_SW_DISABLED_PRIVILEGE_CTRL	BIT_SWITCH_PRIVILEGE_CTRL
#define BIT_SW_DISABLED_TRANSITION		1
#define BIT_SW_DISABLED_AGG_MODE		2

#define BIT_TX_BLANKING_NEEDED			8
/* represents if SGLTE mode is active */
#define BIT_SGLTE_ACTIVE				9
/* indicates whether or not set mode can be granted if 
   switching gets cancelled */
#define BIT_GRANT_MODE_IF_CANCELLED		10

#define SWITCH_IN_TRANSITION	TRM_ANT_SWITCH_DIV_CONFIG_IN_TRANSITION

/*! Client's mode is categorised into three different types-
        1) that have switch locking ability,
        2) that have switching abilities
        3) other types
    Numerical values are assigned to each type in the increasing order of
    power of 10. So Aggregate mode can be calculated summing up numerical
    mode value of all the clients */
/* idle type, value assigned is 0*/
#define ASDIV_MODE_TYPE_0			0
/* enforced/locking type, value assigned is 10^0=1*/
#define ASDIV_MODE_TYPE_1			1
/* switching capable type, value assigned is 10^1=10*/
#define ASDIV_MODE_TYPE_2			10

/* aggregate modes on the basis of above types 
    All clients idle */
#define ASDIV_MODE_TX_NONE			ASDIV_MODE_TYPE_0
/* aggregate mode in enforced/locked state */
#define ASDIV_MODE_ENF_ONLY			ASDIV_MODE_TYPE_1
/* aggregate mode in switching capable state */
#define ASDIV_MODE_TX_ONLY			ASDIV_MODE_TYPE_2
/* Simultaneous modes, summation of above modes */
#define ASDIV_MODE_TX_SIMUL			ASDIV_MODE_TX_ONLY + ASDIV_MODE_TX_ONLY
#define ASDIV_MODE_ENF_TX			ASDIV_MODE_ENF_ONLY + ASDIV_MODE_TX_ONLY

#define ASDIV_INVALID_DWELLING_TIME 0

/*! Other utility Macros */

/* Macros for initializing, entering and exiting critcal section */
#define ASDIV_INIT_CRIT_SECT(lock)    rex_init_crit_sect(&(lock))
#define ASDIV_ENTER_CRIT_SECT(lock)   rex_enter_crit_sect(&(lock))
#define ASDIV_EXIT_CRIT_SECT(lock)    rex_leave_crit_sect(&(lock))

/* Check if a client has privilege access */
#define IS_PRIVILEGED_CLIENT(client) (client==TRM_GSM1)
/* Check if the privilege mode is active */
#define IS_PRIVILEGED_MODE	( CHECK_BIT_POS(as_div.stat_flag, BIT_SWITCH_PRIVILEGE_CTRL))

/* Check if Switching is disabled for non-privileged clients */
#define IS_SWITCHING_DISABLED		(  (CHECK_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_PRIVILEGE_CTRL) ||\
										CHECK_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_TRANSITION) ||\
										CHECK_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_AGG_MODE)) )

/* check if Switching is disabled by client's aggregate mode */
#define IS_SW_DISABLED_BY_MODE(agg_mode) ( 	(agg_mode == ASDIV_MODE_ENF_ONLY)|| \
											(agg_mode == ASDIV_MODE_TX_SIMUL)|| \
											(agg_mode == ASDIV_MODE_ENF_TX) )

/* check if antenna switch feature is supported by the HW */
#define ASD_IS_FEATURE_SUPPORTED		(CHECK_BIT_POS(as_div.config_reg, BIT_FEATURE_SUPPORTED))
/* check if switch should be defaulted in idle*/
#define ASD_KEEP_IN_IDLE			(CHECK_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_IDLE))
#define ASD_CURR_EQUALS_INIT_CFG	( as_div.init_setting.configuration ==\
									  as_div.curr_setting.configuration )
#define ASD_SWITCH_IN_TRANSITION 	( as_div.curr_setting.configuration ==\
									  SWITCH_IN_TRANSITION )

/* This Macro should be used to null check interfaces.  If a Void return
   value is desired, then pass in the RETURN_VOID macro */
#define ASD_NULL_PTR_CHK(ptr, retval)                                \
  if (ptr == NULL)                                                       \
  {                                                                      \
    MSG(MSG_SSID_DFLT, MSG_LEGACY_ERROR, "Unexpected Null Ptr"); \
    { return retval; }        \
  }

#define RETURN_VOID /* null macro */

/* efs file paths */
#define TRM_CONF_EFS_FILE         "/nv/item_files/conf/mcs_as_div.conf"
#define ANT_SWITCH_DIV_EFS_PATH   "/nv/item_files/mcs/trm/ant_div_switch_init"

/* DSDA changes */
#define TRM_ASDIV_DEFAULT_COL_LIMIT 2

#define TRM_ASDIV_SUB_STATE_SHIFT   24
#define TRM_ASDIV_TECH_MODE_SHIFT   16
#define TRM_ASDIV_TECH_TYPE_SHIFT   8
#define TRM_ASDIV_SUB_PRIO_SHIFT    0
#define ASDIV_RST_COL_CNT_MASK      ( ~((uint32) ( 1<<TRM_CLIENT_MODE_IDLE_WAKEUP)|\
                                        ( 1<<TRM_CLIENT_MODE_SLEEP) ) )

#define ASDIV_IS_SINGLE_TECH_ID(tech_id)  ( (tech_id == TRM_GSM2) || (tech_id == TRM_GSM3) )

#define TRM_ASDIV_SIMUL_SUPPORT_MASK ((uint8)((TRM_SVDO_IS_ENABLED) |\
                                              (TRM_SVLTE_IS_ENABLED) |\
                                              (TRM_DSDA_IS_ENABLED) |\
                                              (TRM_SGLTE_SGTDS_IS_ENABLED)) )

#define TRM_ASDIV_IS_TECH_VALID(tech_id) ( tech_id < ASDIV_MAX_CLIENT )
#define TRM_ASDIV_IS_SUBID_VALID(sub_id) ( sub_id < MAX_NUM_OF_SUBSCRIPTIONS && \
                                           sub_id > SYS_MODEM_AS_ID_NONE )

#define TRM_ASDIV_IS_DATA_TECH(tech_id) ( tech_id == TRM_LTE || \
                                          tech_id == TRM_TDSCDMA )

#define TRM_ASDIV_IS_TRAFFIC_MODE(mode) ( mode == TRM_CLIENT_MODE_RXTX || \
                                          mode == TRM_CLIENT_MODE_RXTX_LOW )

#define TRM_ASDIV_IS_IDLE_MODE(mode) ( mode == TRM_CLIENT_MODE_IDLE_WAKEUP || \
                                       mode == TRM_CLIENT_MODE_RXACQ )

/* subscription priority module call back function declaration */
void trm_asdiv_subs_prio_cb(
   sp_subscription_priority_list    subs_prio_list,
   sp_client_enum_t                 client_id
   );

/*----------------------------------------------------------------------------
  Asdiv switch info change type. 
----------------------------------------------------------------------------*/
typedef enum
{
  /* no information has cahnged */
  ASDIV_SW_NONE_CHANGED,
  /* Switch permission has changed */
  ASDIV_SW_PERM_CHANGED,
  /* switch configuratioh has changed */
  ASDIV_SW_CONFIG_CHANGED,
  /* switch permission and configuration both changed */
  ASDIV_SW_BOTH_CHANGED,

}asdiv_sw_info_change_enum;


/*----------------------------------------------------------------------------
  Ant Switch SV Mode type. 
----------------------------------------------------------------------------*/
typedef enum
{
  /* no simultaneous mode*/
  CLIENT_IDLE_MODE,
  /* 1x and DO */
  CLIENT_SVDO_MODE,
  /* 1x and LTE */
  CLIENT_SVLTE_MODE,
  /* G and LTE/W/*/
  CLIENT_SGLTE_MODE,
  /* 1x acq and LTE/DO */
  CLIENT_TXACQ_MODE,
  /* LTE/DO and 1x */
  CLIENT_ACQTX_MODE,
  /* invalid */
  CLIENT_INVALID_SV_MODE
}client_sv_mode;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode Action type. Internal to set mode to decide action
----------------------------------------------------------------------------*/
typedef enum
{
  /* Set Mode Granted, no change to switching permission */
  TRM_MODE_ACT_GRANT,
  /* grant mode and enable switching */
  TRM_MODE_ACT_GRANT_EN_SW,
  /* grant mode and disable switching */
  TRM_MODE_ACT_GRANT_DIS_SW,
  /* needs switching to default before granting mode */
  TRM_MODE_ACT_SWITCH_AND_CONTINUE,
  /* mode status is simultaneous or enforced RX*/
  TRM_MODE_ACT_HANDLE_SV_ENFORCED,
  /* switch is in transition and needs to wait */
  TRM_MODE_ACT_WAIT_SWITCH_COMPLETE,
  /* set mode should be denied */
  TRM_MODE_ACT_DENY

} trm_ant_switch_mode_action_type;

/*----------------------------------------------------------------------------
  TRM ASDiv Event Type: Switching events type
----------------------------------------------------------------------------*/
typedef enum
{
  /* Event due to switch cancel */
  TRM_ASDIV_EVENT_SWITCHING_CANCEL,

  /* Event due to switching in idle wakeup */
  TRM_ASDIV_EVENT_SWITCHING_IDLEWAKEUP,

  /* Event due to switching in other modes */
  TRM_ASDIV_EVENT_SWITCHING_OTHERS

}trm_asdiv_event_type;

/*----------------------------------------------------------------------------
  TRM ASDiv Event Info Type: Switching events information structure
----------------------------------------------------------------------------*/
typedef struct
{
   uint8                   client_id;
   uint8                   old_switch_config;
   uint8                   new_switch_config;
   trm_asdiv_event_type    event_type;
   uint8    reserved1;
   uint8    reserved2;
   uint16   reserved3;
   uint32   reserved4;
}trm_asdiv_event_info_type;

/*----------------------------------------------------------------------------
  TRM Asdiv idle idle col info. Idle/Idle Collision information
----------------------------------------------------------------------------*/
typedef struct {

  /* idle idle collision limit */
  uint8                            idle_idle_col_limit;
  /* flag to indicate if limit has reached */
  boolean                          limit_reached;
  /* flag to indicate if collision was for idle/idle case */
  boolean                          is_idl_idl_col;
  /* client that was denied pref config due to colision */
  trm_client_enum_t                denied_cl;
  /* preferred config of denied client */
  trm_ant_switch_div_config_type   denied_config;
  /* count for successiv idle/idle collsion */
  int                              col_count;

}trm_asdiv_idl_idl_col_info;


/*----------------------------------------------------------------------------
  Ant Switch active Client list. List of active clients
----------------------------------------------------------------------------*/
typedef struct {

  /* client id */
  trm_client_enum_t          cl_id[ASDIV_MAX_CLIENT];

  /* number of active clients in the list */
  uint32                     num_clients;

  /* Bit mask for modes of all active tech */
  uint32                     active_mode_mask;

  /* active client that has highest priority */
  trm_client_enum_t          high_prio_cl;

  /* highest priority mask */
  uint32                     high_prio_mask;

}trm_asdiv_active_cl_list;

/*----------------------------------------------------------------------------
  Ant Switch Client Structure. Client specific attributes
----------------------------------------------------------------------------*/
typedef struct {

  /* anonymous client's data that is echoed back when mode is granted */
  trm_request_tag_t       				tag;

  /* Client's id */
  trm_client_enum_t     				id;

  /* the mode in which the client it is in */
  trm_client_mode_type                	curr_mode;

  /* requested mode */
  trm_client_mode_type                	req_mode;

  /* last requested configuration by the client */
  trm_ant_switch_div_config_type      	req_config;

  /* NOT USED */
  trm_ant_switch_subcribe_switch_info 	subs_sw_info;

  /* for bw compatibility, can be removed once L1s use 
    subscribing for swithc info*/
  boolean 								ntfy_switch_change;

  /* inform the client if mode of any tech changes */
  boolean   							ntfy_mode_change;

  /* indicates if ant switch is enabled or not */
  trm_ant_switch_state_type         	ant_switch_state;

  /* set mode callback: called to grant the mode request */
  trm_ant_switch_set_mode_callback_t  	set_mode_cb;

  /* ant switch call-back..called when we want the client to
     actively switch */
  trm_ant_switch_div_callback_t      	ant_switch_cb;

  /* ant switch call-back: called when the mode chane occurs */
  trm_ant_switch_mode_notification_callback_t  mode_change_cb;

  /* DSDA ASDiv changes */

  /* preferred switch configuration of client */
  trm_ant_switch_div_config_type          pref_config;

  /* Switch control: held if not in safe mode
                     release in safe mode */
  trm_ant_switch_set_ctrl_enum            sw_control;

  /* call back for set mode and config call */
  trm_asd_set_mode_and_config_cb_t        set_mode_cfg_cb;

  /* corresponding sys mode id */
  sys_sys_mode_e_type      sys_tech_id;

    /* subscription sys id */
  sys_modem_as_id_e_type  sub_id;

  /* priority of subscription */
  uint8                   sub_order_idx;

  /* priority mask for the client */
  uint32                  priority_mask;

  uint32                  switch_lock_mask;

  boolean                 switch_start_reported;

}ant_switch_client_struct;


/*----------------------------------------------------------------------------
  Ant Switch Diversity Structure. Overall ASdiv attributes
----------------------------------------------------------------------------*/
typedef struct
{
  /* Configuration Register, holds initial setting */
  uint32							config_reg;
  /* Status REgister, holds current information */
  uint32              				stat_flag;
  /* Critical section - for antenna arbitration. */
  rex_crit_sect_type              	crit_sect;
  /* asdiv initial settings from NV */
  trm_ant_switch_div_data_type    	init_setting;
  /* asdiv current switch related attribute */
  trm_ant_switch_div_data_type    	curr_setting;
  /* aggregate client mode */
  agg_client_mode_type            	aggregate_client_mode;
  /* stores the switch config before switch is moved to transition */
  trm_ant_switch_div_config_type	prev_config;
  /* stores expected switch config while switching */
  trm_ant_switch_div_config_type	next_config;
  /* Client array depending upon the number of clients */
  ant_switch_client_struct        	client[ASDIV_MAX_CLIENT];

  /* DSDA changes */
  /* subscription priority info sent by subs prio module */
  sp_subscription_priority_list         subs_prio_list;
  /* list of active clients */
  trm_asdiv_active_cl_list              active_list;
  /* idle idle collision data */
  trm_asdiv_idl_idl_col_info            idl_idl_col_data;

  uint32               lock_mask;
  /* flag to indicate if DSDA is supported */
  boolean              is_standalone_mode;

  /* tech that is doing the switching */
  trm_client_enum_t    switching_tech;

  /* tech that is requesting for switching,
     it is same as switching tech in most case except for passive switching */
  trm_client_enum_t    requesting_tech;
  
  /* diag event info for switching events */
  trm_asdiv_event_info_type      event_info;
  
}ant_switch_div_struct;

/* global ant switch structure */
ant_switch_div_struct     as_div;



/*===========================================================================

                         FUNCTIONS

===========================================================================*/

/* Funtion Declarations */

void trm_asdiv_update_actv_list_tech_info(
   trm_client_enum_t    cl
   );

void trm_asdiv_update_actv_list_prio_info(void);

boolean trm_asdiv_chk_switch_perm(trm_client_enum_t cl_id);



/*============================================================================
FUNCTION TRM_EFS_CREATE_FOLDER

DESCRIPTION
  Creates EFS Path for the conf file it is doesn't exist.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
static void trm_efs_create_folders(void)
{
  int ret;

/*--------------------------------------------------------------------------*/
  /* Create mcs folder if it doesnt already exist */
  ret = efs_mkdir("/nv/item_files/mcs", 0x777);
  if (ret != 0 && efs_errno != EEXIST)
  {
    MSG_1(MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error %d creating mcs folder in efs", efs_errno);
    return ;
  }

  /* Create trm folder if it doesnt already exist */
  ret = efs_mkdir("/nv/item_files/mcs/trm", 0x777);
  if (ret != 0 && efs_errno != EEXIST)
  {
    MSG_1(MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error %d creating trm folder in efs", efs_errno);
    return ;
  }  
}


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_UPDATE_CONF_FILE

DESCRIPTION
  Update the conf file with nv file location.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_update_conf_file(void)
{

  /* File Descriptor */
  int32                 fd;
  struct 	fs_stat 	conf_file_det;
  size_t         		data_size;
  int            		file_stat_result;

  char str[] = ANT_SWITCH_DIV_EFS_PATH "\n";

/*--------------------------------------------------------------------------*/


  file_stat_result = efs_stat(TRM_CONF_EFS_FILE, &conf_file_det);
  data_size = (size_t)strlen(str);

  /* If the conf file does not exist or if the size of the existing conf file is
     not what we expect, create a new conf file */
  if (file_stat_result != 0 || data_size != conf_file_det.st_size)
  {

    trm_efs_create_folders();

    /* Open the TRM CONF EFS file. If its not present then create it. */
    fd = efs_open(TRM_CONF_EFS_FILE, O_CREAT | O_AUTODIR | O_WRONLY | O_TRUNC, ALLPERMS);

	  if (fd < 0)
	  {
		  MSG_1(MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error opening EFS file %d", fd);
		  return;
	  }

    /* Write the conf file */
    (void)efs_write(fd, str, (size_t)strlen(str));

    efs_close(fd);
  }

}


/*============================================================================

FUNCTION TRM_VALID_ANT_SWITCH_DIV_ITEM_READ

DESCRIPTION
  Validates the data read from the tx div switch item file.
  
DEPENDENCIES
  None

RETURN VALUE
  True : Valid.
  False: Invalid.

SIDE EFFECTS
  None

============================================================================*/
boolean trm_valid_ant_switch_div_item_read
(
   const trm_ant_switch_div_data_type ant_switch_div_data
   )
{

  if (ant_switch_div_data.configuration >= 0 &&
      ant_switch_div_data.configuration <= 1
      )
  {
    return TRUE;
  }

  return FALSE;
}


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_READ_EFS

DESCRIPTION
  Read the EFS File to get NV settings.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_read_efs(void)
{

  int result = 0;

/*--------------------------------------------------------------------------*/

  /* Open the EFS file. If its not present then create it els read it. */
  result = efs_get(ANT_SWITCH_DIV_EFS_PATH, (void *)&as_div.init_setting,
                   sizeof(trm_ant_switch_div_data_type));

  /* If read passed, check if we read the expected number of bytes */
  if (result != sizeof(trm_ant_switch_div_data_type))
  {
    MSG_3(MSG_SSID_DFLT, MSG_LEGACY_FATAL,
	  "Could not read settings from EFS. result: %d, efs_errno %d, size %d", 
          result,efs_errno,sizeof(trm_ant_switch_div_data_type));
    return;
  }

  /* if efs read values are not valid, initialize to default values */
  if (!trm_valid_ant_switch_div_item_read(as_div.init_setting))
  {
	  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "Invalid settings read from EFS: configuration - %d ", 
             as_div.init_setting.configuration);
	  as_div.init_setting.dwelling_time  = 0;
	  as_div.init_setting.test_mode_ctrl = TYPICAL;
	  as_div.init_setting.configuration  = TRM_ANT_SWITCH_DIV_CONFIG_0;
	  as_div.init_setting.sv_default_configuration = TRM_ANT_SWITCH_DIV_CONFIG_0;
	  as_div.init_setting.sv_behavior    = TRM_ANT_SWITCH_SV_KEEP_IDLE_DEF;
  }

  /* transfer the init values to the current variables */
  as_div.curr_setting.configuration  = as_div.init_setting.configuration;
  as_div.curr_setting.test_mode_ctrl = as_div.init_setting.test_mode_ctrl;
  as_div.curr_setting.dwelling_time  = as_div.init_setting.dwelling_time;


  /* initialize the keep/default behaviour on idle/SV mode on the basis
     of NV data read */
  if (as_div.init_setting.sv_behavior == TRM_ANT_SWITCH_SV_DEF_IDLE_DEF ||
      as_div.init_setting.sv_behavior == TRM_ANT_SWITCH_SV_DEF_IDLE_KEEP)
  {
    //trm.ant_switch_use_nv_in_sv = TRUE;
    SET_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SGLTE);
    SET_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SVLTE);
    SET_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SVDO);
  }
  else
  {
    CLR_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SGLTE);
    CLR_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SVLTE);
    CLR_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_SVDO);
  }

  if (as_div.init_setting.sv_behavior == TRM_ANT_SWITCH_SV_KEEP_IDLE_DEF ||
      as_div.init_setting.sv_behavior == TRM_ANT_SWITCH_SV_DEF_IDLE_DEF)
  {
    SET_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_IDLE);
  }
  else
  {
    CLR_BIT_POS(as_div.config_reg, BIT_DEFAULT_KEEP_IN_IDLE);
  }

}

/*============================================================================

FUNCTION ANT_SWITCH_DIV_MUTEX_INIT

DESCRIPTION
  Initializes the asdiv mutex, TRM needs to call this api first to initialize
  the asdiv mutex
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void ant_switch_div_mutex_init()
{
  /* mutex should be initialized before L1s are active, so before
      initialization of other variables, this api is called by
      TRM to initialize the mutex */
  ASDIV_INIT_CRIT_SECT(as_div.crit_sect);

  /* reset configuration register */
  as_div.config_reg = 0;

}

/*============================================================================

FUNCTION TRM_ASDIV_GET_SYS_MODE_TYPE

DESCRIPTION
  Gets the sys id for trm client id
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

sys_sys_mode_e_type  trm_asdiv_get_sys_mode_type(trm_client_enum_t cl_id)
{
  switch (cl_id)
  {

  case TRM_1X:
    return SYS_SYS_MODE_CDMA;
  case TRM_HDR:
    return SYS_SYS_MODE_HDR;
  case TRM_WCDMA:
    return SYS_SYS_MODE_WCDMA;
  case TRM_GSM1:
  case TRM_GSM2:
  case TRM_GSM3:
    return SYS_SYS_MODE_GSM;
  case TRM_TDSCDMA:
    return SYS_SYS_MODE_TDS;
  case TRM_LTE:
    return SYS_SYS_MODE_LTE;
  default:
    return SYS_SYS_MODE_MAX;

  }

}
/*============================================================================

FUNCTION ANT_SWITCH_DIV_INIT

DESCRIPTION
  Initializes the asdiv variables to default, reads efs item to get the
  default values
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void ant_switch_div_init(
   boolean          feature_supported,
   uint8            trm_modes
   )
{

  uint8   index;

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  //memset(&as_div, 0, sizeof(ant_switch_div_struct));

  /* assign feature supported bit with passed argument */
  (feature_supported) ?
     SET_BIT_POS(as_div.config_reg, BIT_FEATURE_SUPPORTED) :
     CLR_BIT_POS(as_div.config_reg, BIT_FEATURE_SUPPORTED);


  /* initialize the ant switch div variables
      if h/w is supported */
  if (ASD_IS_FEATURE_SUPPORTED)
  {
    as_div.stat_flag = 0;

    as_div.init_setting.dwelling_time  = 0;
    as_div.init_setting.test_mode_ctrl = TYPICAL;
    as_div.init_setting.configuration  = TRM_ANT_SWITCH_DIV_CONFIG_0;
    as_div.init_setting.sv_default_configuration = TRM_ANT_SWITCH_DIV_CONFIG_0;
    as_div.init_setting.sv_behavior    = TRM_ANT_SWITCH_SV_KEEP_IDLE_KEEP;

    as_div.aggregate_client_mode         = 0;
    as_div.curr_setting.configuration    = TRM_ANT_SWITCH_DIV_CONFIG_0;

    as_div.lock_mask = 0;

    as_div.is_standalone_mode = TRUE;

    if( trm_modes & TRM_ASDIV_SIMUL_SUPPORT_MASK )
    {
      as_div.is_standalone_mode = FALSE;
    }
   
    /* initialize client specific data */
    for (index = 0; index <= TRM_LAST_CLIENT; index++)
    {
      as_div.client[index].id			   = (trm_client_enum_t)index;
      as_div.client[index].curr_mode       = TRM_CLIENT_MODE_INACTIVE;
      as_div.client[index].req_mode        = TRM_CLIENT_MODE_INACTIVE;
      as_div.client[index].req_config      = TRM_ANT_SWITCH_DIV_CONFIG_MAX;

      as_div.client[index].ant_switch_state    = TRM_ANT_SWITCH_DIV_ENABLED;

      as_div.client[index].subs_sw_info		  = TRM_SUBS_NONE;
      as_div.client[index].ntfy_mode_change   = FALSE;
      as_div.client[index].ntfy_switch_change  = FALSE;

      as_div.client[index].set_mode_cb         = NULL;
      as_div.client[index].ant_switch_cb       = NULL;
      as_div.client[index].mode_change_cb      = NULL;

      as_div.client[index].sys_tech_id = trm_asdiv_get_sys_mode_type(as_div.client[index].id);

      as_div.client[index].sw_control = TRM_ANT_SWITCH_CTRL_RELEASE;

      as_div.client[index].switch_lock_mask = 0;

      as_div.client[index].switch_start_reported = FALSE;


    }

    as_div.active_list.num_clients = 0;
    as_div.idl_idl_col_data.denied_cl = TRM_NO_CLIENT;
    as_div.idl_idl_col_data.idle_idle_col_limit = TRM_ASDIV_DEFAULT_COL_LIMIT;
    as_div.subs_prio_list.num = 0;

    as_div.switching_tech = TRM_NO_CLIENT;
    as_div.requesting_tech = TRM_NO_CLIENT;

#ifndef TEST_FRAMEWORK

    /*Register with Subscription Priority for priority list */
    sp_register_client(trm_asdiv_subs_prio_cb, SP_TRM);
    /* read and initialize from EFS file */
    trm_ant_switch_div_update_conf_file();
    trm_ant_switch_div_read_efs();

#endif

    /* update the preferred configuration of tech with default configuration */
     for (index = 0; index <= TRM_LAST_CLIENT; index++)
     {
       as_div.client[index].pref_config = as_div.init_setting.configuration;
     }
    

  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

}

/*============================================================================

FUNCTION TRM_IS_ANT_SWITCH_DIV_ENABLED

DESCRIPTION
  Indicate if the tc diversity switch enable functionality is enabled or not.
  
DEPENDENCIES
  None

RETURN VALUE
  True  : if the functionality is enabled. 
  False : if the functionality is disabled. 

SIDE EFFECTS
  None

============================================================================*/
boolean ant_switch_div_supported(void)
{
  if (ASD_IS_FEATURE_SUPPORTED)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}


/*============================================================================

FUNCTION ASDIV_INFORM_SWITCH_INFO_CHANGE

DESCRIPTION
  if any client is subscribed to switch information (configuration, permission)
  change, this api will call the subscribed clients registered call back
  function to inform about the change.
  Currently, switch information includes- 
    antenna switch configuration change
    antenna switching permission change

  AI: currently, notifying switch complete is done via different method. It is
  left as it is for backward compatibility. But this feature should also be 
  integrated to this api
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void asdiv_inform_switch_info_change(
   trm_client_enum_t			client_curr,
   asdiv_sw_info_change_enum   	change_info
   )
{

  uint8 		cnt;
  boolean 	subscribed = FALSE;
  trm_ant_switch_cb_info_type 	ant_cfg_info;

  /* populate the structure to be passed */
  ant_cfg_info.config = as_div.curr_setting.configuration;
  ant_cfg_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;
  ant_cfg_info.client  = client_curr;

  for (cnt = 0; cnt <= TRM_LAST_CLIENT; cnt++)
  {

    subscribed = FALSE;

    /* check for each client if they have subscribed for TRM 
        switch info change feature */
    if (as_div.client[cnt].subs_sw_info == TRM_SUBS_SW_COMPLETE &&
        as_div.client[cnt].id != client_curr &&
        change_info != ASDIV_SW_PERM_CHANGED)
    {
      /* switching is complete and the client is subscribed to 
          switch complet information */
      ant_cfg_info.action = TRM_ANT_SWITCH_SET_CONFIG_DONE;
      subscribed = TRUE;
    }
    else if (as_div.client[cnt].subs_sw_info == TRM_SUBS_SW_PERM_CHANGE &&
             change_info != ASDIV_SW_CONFIG_CHANGED)
    {
      /* switch permission has changed and the client is subscribed
          to switch permission change */
      if (IS_SWITCHING_DISABLED)
      {
        ant_cfg_info.action = TRM_ANT_SWITCH_DISABLED;
      }
      else
      {
        ant_cfg_info.action = TRM_ANT_SWITCH_ENABLED;
      }
      subscribed = TRUE;
    }

    /* if a client is subscribed and has a valid call back registered
        call the call back function */
    if (subscribed &&
        as_div.client[cnt].ant_switch_cb != NULL)
    {
      as_div.client[cnt].ant_switch_cb(ant_cfg_info);
    }

  }

} /* asdiv_inform_switch_info_change */

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_CONFIG

DESCRIPTION
  Get the current tx diversity switch configuration.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity switch configuration:
    config_0 or config_1 or in_transition

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_config_type trm_ant_switch_div_get_config(
   trm_ant_switch_get_input_type input_type)
{

  trm_ant_switch_div_config_type return_val;

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  /* return invalid config if feature is not supported */
  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
    return_val = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
  }
  else if (input_type == TRM_ANT_SWITCH_CONFIG_DEFAULT)
  {
    /* Default config should be provided always */
    return_val = as_div.init_setting.configuration;
  }
  else
  {
    return_val = as_div.curr_setting.configuration;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ADiv: get config; req:%d cfg_ret:%d",
         input_type, return_val);

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
  return return_val;

} /* trm_get_ant_switch_div_config */


/*============================================================================

FUNCTION TRM_CLIENT_TRANSMITTING

DESCRIPTION
  Returns the first client which is under transmit.
  
DEPENDENCIES
  None

RETURN VALUE
  Client which is under RxTx; if there is one.

SIDE EFFECTS
  None

============================================================================*/
trm_client_enum_t trm_client_transmitting(
   trm_client_enum_t requesting_client
   )
{
  trm_client_enum_t client = TRM_NO_CLIENT;
  int32 loopcnt = 0;
/*--------------------------------------------------------------------------*/

  for (loopcnt = 0; loopcnt <= TRM_LAST_CLIENT; ++loopcnt)
  {
    ant_switch_client_struct client1 = as_div.client[loopcnt];
    /* ignore the current client */
    if (requesting_client == loopcnt)
    {
      continue;
    }
    /* Client mode is rxtx and hence is the client switching */
    if ( TRM_ASDIV_IS_TRAFFIC_MODE(client1.curr_mode) )
    {
      client = client1.id;
    }
  }
  return client;
}

/*============================================================================

FUNCTION TRM_CLIENT_ACQUIRING

DESCRIPTION
  Returns the first client which is under acquiring mode.
  NOTE: 1x while acquiring needs to do antenna switching, so new client mode
  is added (RXACQ). 
  
DEPENDENCIES
  None

RETURN VALUE
  Client which is under RxACQ; if there is one.

SIDE EFFECTS
  None

============================================================================*/
trm_client_enum_t trm_client_acquiring(
   trm_client_enum_t requesting_client
   )
{
  trm_client_enum_t client = TRM_NO_CLIENT;
  int32 loopcnt = 0;
/*--------------------------------------------------------------------------*/

  for (loopcnt = 0; loopcnt <= TRM_LAST_CLIENT; ++loopcnt)
  {
    ant_switch_client_struct client1 = as_div.client[loopcnt];
    /* ignore the current client */
    if (requesting_client == loopcnt)
    {
      continue;
    }
    /* Client mode is rxtx and hence is the client switching */
    if (client1.curr_mode == TRM_CLIENT_MODE_RXACQ)
    {
      client = client1.id;
    }
  }
  return client;
}


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_TEST_MODE_CTRL

DESCRIPTION
  Get the current tx diversity test mode control.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity test mode control

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_test_mode_ctrl_type trm_ant_switch_div_get_test_mode_ctrl()
{
  return as_div.init_setting.test_mode_ctrl;
} /* trm_ant_switch_div_get_test_mode_ctrl */



/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_DWELLING_TIME

DESCRIPTION
  Get the current tx diversity dwelling time.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity test mode control

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_dwelling_time_type trm_get_ant_switch_div_dwelling_time()
{
  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported");
    return ASDIV_INVALID_DWELLING_TIME;
  }
  else
  {
    return as_div.init_setting.dwelling_time;
  }
} /* trm_get_ant_switch_div_dwelling_time */



/*============================================================================

FUNCTION TRM_RESET_ANT_SWITCH_CONFIG_INFO

DESCRIPTION
  Reset the switching info of the client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_reset_ant_switch_config_info(trm_client_enum_t client)
{
  /* Reset all the ant switch attributes of the client */
  as_div.client[client].set_mode_cb   	= NULL;
  as_div.client[client].ant_switch_cb     = NULL;
  as_div.client[client].ntfy_switch_change  = FALSE;
  as_div.client[client].subs_sw_info	= TRM_SUBS_NONE;
  //as_div.client[client].req_config    = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
  as_div.client[client].curr_mode     = TRM_CLIENT_MODE_INACTIVE;
  as_div.client[client].req_mode  	= TRM_CLIENT_MODE_INACTIVE;

}


/*============================================================================

FUNCTION TRM_VALIDATE_SET_MODE_INPUT

DESCRIPTION
  Validate the input provided for set mode.
  
DEPENDENCIES
  None

RETURN VALUE
  True -  Valid.
  False - Invalid.

SIDE EFFECTS
  None

============================================================================*/
boolean 	trm_validate_set_mode_input
(
   trm_set_mode_input_type       set_mode_input
   )
{
  boolean 						  ret_val = TRUE;
  trm_ant_switch_set_mode_type      set_mode_info;
  ant_switch_client_struct	 *req_client = NULL;

  if ( TRM_ASDIV_IS_TECH_VALID(set_mode_input.client)) 
  {
     req_client		= &as_div.client[set_mode_input.client];
  }

  /* check for NULL pointer */
  ASD_NULL_PTR_CHK(req_client, FALSE);

  /* check for invalid case, call the client's cb with app. reason
      if invalid parameters */
  if (set_mode_input.client > TRM_LAST_CLIENT ||
      set_mode_input.set_mode_cb == NULL ||
      set_mode_input.mode >= TRM_CLIENT_MODE_MAX
      )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "Invalid input provided for set mode. Client: %d, Mode: %d",
           set_mode_input.client, set_mode_input.mode);

    set_mode_info.set_mode_grant = TRM_SET_MODE_DENIED_INVALID_PARAMETERS;
    set_mode_info.tag = set_mode_input.tag;

    if (req_client->set_mode_cb != NULL)
    {
      req_client->set_mode_cb(set_mode_info);
    }
    else if (set_mode_input.set_mode_cb != NULL)
    {
      set_mode_input.set_mode_cb(set_mode_info);
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "ASD Set_mode: Invalid parameters with no cb, client: %d",
             set_mode_input.client);
    }
    ret_val = FALSE;
  }

  return ret_val;
}


/*============================================================================

FUNCTION TRM_GET_OPPOSITE_CONFIG

DESCRIPTION
  Returns the comfig which is opposite to the passed config. If switch is in 
  transition, the opposite of next_config is returned.
  
DEPENDENCIES
  None

RETURN VALUE
  Returns the comfig which is opposite to the passed config

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_config_type trm_get_opposite_config
(
   trm_ant_switch_div_config_type 	config
   )
{
  trm_ant_switch_div_config_type  opposite_config = TRM_ANT_SWITCH_DIV_CONFIG_0;

  if (config == SWITCH_IN_TRANSITION)
  {
    config = as_div.next_config;
  }

  if (config == TRM_ANT_SWITCH_DIV_CONFIG_0)
  {
    opposite_config = TRM_ANT_SWITCH_DIV_CONFIG_1;
  }

  return opposite_config;

}

/*============================================================================

FUNCTION TRM_ANT_SWITCH_INFORM_MODE_CHANGE

DESCRIPTION
  Informs the registered clients about mode changes of other clients.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_inform_mode_change(
   trm_client_enum_t switching_client,
   trm_client_mode_type mode_switched
   )
{
  uint32 loopcnt = 0;
  trm_ant_switch_mode_notification_info_type set_mode_notify;
/*--------------------------------------------------------------------------*/
  set_mode_notify.client = switching_client;
  set_mode_notify.mode   = mode_switched;

  for (loopcnt = 0; loopcnt <= TRM_LAST_CLIENT; ++loopcnt)
  {
    if (as_div.client[loopcnt].ntfy_mode_change &&
        (as_div.client[loopcnt].mode_change_cb != NULL) &&
        switching_client != (trm_client_enum_t)loopcnt)
    {
      as_div.client[loopcnt].mode_change_cb(set_mode_notify);
    }
  }
}


/*============================================================================

FUNCTION ASDIV_GET_SIMUL_MODE

DESCRIPTION
  figures out the current simultaneous mode of operation
    1x+LTE	- SVLTE
    1x+DO	- SVDO
    G+X 	- SGLTE
  
DEPENDENCIES
  None

RETURN VALUE
  Client_sv_mode

SIDE EFFECTS
  None

============================================================================*/
client_sv_mode asdiv_get_simul_mode
(
   trm_set_mode_input_type *set_mode_input
)
{
  boolean					in_acq = FALSE;
  trm_client_enum_t		tx_client;
  client_sv_mode			sv_mode = CLIENT_INVALID_SV_MODE;

  /* check for NULL pointer */
  ASD_NULL_PTR_CHK(set_mode_input, CLIENT_INVALID_SV_MODE);

  tx_client	= trm_client_transmitting(set_mode_input->client);

  /* As a requirement for 1x to do asdiv during acquisition, simulatanoues
      modes- TXACQ and ACQTX are introduced. In acq it cannot be requested
      to do the switching, so there is a need to identify if simulateuos mode
      consists of 1x being in acquisition. If active client is 1x Acq, then
      the limitation exists */
  /* if no client is in traffic, check to see if any client is in acquisition*/
  if (tx_client == TRM_NO_CLIENT)
  {
    tx_client = trm_client_acquiring(set_mode_input->client);
    in_acq 		= TRUE;
  }
  if (tx_client == TRM_NO_CLIENT)
  {
    sv_mode	= CLIENT_INVALID_SV_MODE;
  }
  else if (in_acq &&
           set_mode_input->mode == TRM_CLIENT_MODE_RXTX)
  {
    sv_mode	= CLIENT_ACQTX_MODE;
  }
  else if (set_mode_input->mode == TRM_CLIENT_MODE_RXACQ)
  {
    sv_mode = CLIENT_TXACQ_MODE;
  }
  /* all other modes are general */
  else if (tx_client == TRM_1X)
  {
    if (set_mode_input->client == TRM_UMTS)
    {
      sv_mode = CLIENT_SVLTE_MODE;
    }
    else if (set_mode_input->client == TRM_HDR)
    {
      sv_mode = CLIENT_SVDO_MODE;
    }
  }
  else if (set_mode_input->client == TRM_1X)
  {
    if (tx_client == TRM_UMTS)
    {
      sv_mode = CLIENT_SVLTE_MODE;
    }
    else if (tx_client == TRM_HDR)
    {
      sv_mode = CLIENT_SVDO_MODE;
    }
  }
  else if (tx_client == TRM_GSM1 ||
           set_mode_input->client == TRM_GSM1)
  {
    sv_mode 	= CLIENT_SGLTE_MODE;
  }
  else
  {
    sv_mode = CLIENT_INVALID_SV_MODE;
  }

  return sv_mode;
}

/*============================================================================

FUNCTION ASDIV_MAP_MODE_VALUE

DESCRIPTION
  Calculates the mode value of passed client on the basis of current
  client's mode
    Traffic mode is assigned mode value 10
    Enforced rx mode is assigned mode value 15
 
    So, SV equals to aggregate mode value 20
        Enforced RX equals to aggregate mode value 25
        Single Traffic mode value is 10
        0 other wise
  
DEPENDENCIES
  None

RETURN VALUE
  client_mode_val_type: 

SIDE EFFECTS
  None

============================================================================*/

client_mode_val_type asdiv_map_mode_value(
   trm_client_mode_type cl_mode
   )
{
  client_mode_val_type	mode_val;

  switch (cl_mode)
  {
    /* switching capable: */
  case TRM_CLIENT_MODE_RXTX:
  case TRM_CLIENT_MODE_RXACQ:
    mode_val	= ASDIV_MODE_TYPE_2;
    break;

    /* locking capable: */
  case TRM_CLIENT_MODE_DEF_ENFORCED_RX:
  case TRM_CLIENT_MODE_LOCK_SWITCH:
    mode_val		= ASDIV_MODE_TYPE_1;
    break;

    /* others- non switching and non-locking: */
  default:
    mode_val	= ASDIV_MODE_TYPE_0;

  }

  return mode_val;
}

/*============================================================================

FUNCTION ASDIV_GRANT_MODE

DESCRIPTION
  This api simply calls the requested client's setmode callback api to
  grant the set mode request
  
DEPENDENCIES
  None

RETURN VALUE
  none: 

SIDE EFFECTS
  None

============================================================================*/

void asdiv_grant_mode(ant_switch_client_struct *req_client)
{
  trm_ant_switch_set_mode_type      set_mode_info;

  ASD_NULL_PTR_CHK(req_client, RETURN_VOID);

  /* populate the structure with required info */
  set_mode_info.set_mode_grant = TRM_SET_MODE_GRANTED;
  set_mode_info.config	= as_div.init_setting.configuration;
  set_mode_info.tag		= req_client->tag;

  /* call the registered call back if it is not NULL */
  if (req_client->set_mode_cb != NULL)
  {
    req_client->set_mode_cb(set_mode_info);

  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDiv; mode GRANT no call back, cl:%d Mode:%d",
           req_client->id, req_client->req_mode );
  }

  /* if the requested mode was sleep, then reset the attributes
     of the client */
  if (req_client->req_mode == TRM_CLIENT_MODE_INACTIVE)
  {
    trm_reset_ant_switch_config_info(req_client->id);
  }

  /* change the client's current mode to requested mode */
  req_client->curr_mode	= req_client->req_mode;

  /* Mode granted */
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "TRM_ASDiv: mode granted- Client:%d Mode:%d ",
         req_client->id, req_client->curr_mode );


}


/*============================================================================

FUNCTION ASDIV_HANDLE_SET_MODE_REQ

DESCRIPTION
  Performs appropriate operation on the basis of current mode. 
  TRM_ANT_SWITCH_SET_MODE identifies what the resulting mode is for the request
  made. And this api performs the necessary operation based on the resulting
  mode.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void asdiv_handle_set_mode_req(
   trm_set_mode_input_type	     *set_mode_input,
   ant_switch_client_struct	     *req_client,
   trm_ant_switch_mode_action_type     mode_act
   )
{

  trm_ant_switch_set_mode_type      set_mode_info;
  trm_ant_switch_cb_info_type       ant_cfg_info;
  trm_client_enum_t                 tx_client;

  /* check for NULL pointers */
  ASD_NULL_PTR_CHK(set_mode_input, RETURN_VOID);
  ASD_NULL_PTR_CHK(req_client, RETURN_VOID);

  /* update requested client's attributes */
  if (mode_act != TRM_MODE_ACT_DENY)
  {
    req_client->req_mode 			= set_mode_input->mode;

    if (set_mode_input->inform_switch_change == TRUE)
    {
      req_client->subs_sw_info = TRM_SUBS_SW_COMPLETE;
    }
    else if (set_mode_input->subscribe_switch_info < TRM_SUBS_SW_INVALID)
    {
      req_client->subs_sw_info = set_mode_input->subscribe_switch_info;
    }

    req_client->tag			        = set_mode_input->tag;
    req_client->ant_switch_cb       = set_mode_input->ant_switch_cb;
    req_client->set_mode_cb  		= set_mode_input->set_mode_cb;
  }


  switch (mode_act)
  {
    /* mode has to be granted */
  case TRM_MODE_ACT_GRANT:

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           " ASD set mode: Granted CL:%d Mode:%d ",
           set_mode_input->client, set_mode_input->mode );

    /* inform subscribed client if mode has changed */
    if (req_client->curr_mode != req_client->req_mode)
    {
      trm_ant_switch_inform_mode_change(set_mode_input->client,
                                        set_mode_input->mode);
    }

    /* grant the mode */
    asdiv_grant_mode(req_client);

    break;

  case TRM_MODE_ACT_SWITCH_AND_CONTINUE:
    /* Request for switching has to be made to current active client */

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           " ASD set mode: Switch and Continue CL:%d Mode:%d ",
           set_mode_input->client, set_mode_input->mode );

    /* if switch is not in transition, put it to transition */
    if (!ASD_SWITCH_IN_TRANSITION)
    {
      as_div.prev_config	=	as_div.curr_setting.configuration;
      as_div.curr_setting.configuration = SWITCH_IN_TRANSITION;
    }
    as_div.next_config		= as_div.init_setting.configuration;

    /* switching is now disabled by transition reason */
    SET_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_TRANSITION);

    /* populate the structure to send */
    set_mode_info.set_mode_grant 	= TRM_SET_SWITCH_AND_CONTINUE;
    set_mode_info.config         	= as_div.init_setting.configuration;
    set_mode_info.tag				= req_client->tag;

    req_client->req_config 	= set_mode_info.config;

    /* call registered call back function to do the switching */
    if (req_client->set_mode_cb != NULL)
    {
      req_client->set_mode_cb(set_mode_info);
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "Invalid set mode call-back: %d",
             set_mode_input->client );
    }

    break;

  case TRM_MODE_ACT_HANDLE_SV_ENFORCED:
    /* set mode request has resulted in SV or enforced RX mode */

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           " ASD set mode: SV/Enforced mode: CL:%d Mode:%d ",
           set_mode_input->client, set_mode_input->mode );

    ant_cfg_info.config = as_div.init_setting.sv_default_configuration;

    if (as_div.aggregate_client_mode == ASDIV_MODE_ENF_TX)
    {
      /* In enforced default; config should be opposite of current */
      ant_cfg_info.config = trm_get_opposite_config(
         as_div.curr_setting.configuration);

    }

    /* store the current config if switch is not in transition */
    if (!ASD_SWITCH_IN_TRANSITION)
    {
      as_div.prev_config	=	as_div.curr_setting.configuration;
      as_div.curr_setting.configuration = SWITCH_IN_TRANSITION;
    }
    as_div.next_config	= ant_cfg_info.config;

    /* find transmitting client to request for switching */
    tx_client = trm_client_transmitting(set_mode_input->client);

    /* for KW issue: make sure tx_client is less than last client */
    if ( !TRM_ASDIV_IS_TECH_VALID(tx_client) )
    {

      ERR_FATAL("TRM_ASDiv: Invalid TX client %d for SV/enf case", 
                tx_client, 0, 0);
    }

    /* indicate switchi disabled by transition reason */
    SET_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_TRANSITION);

    /* populate the request structure */
    ant_cfg_info.action           = TRM_ANT_SWITCH_INITIATE_SET_CONFIG;
    ant_cfg_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;
    ant_cfg_info.client           = TRM_NO_CLIENT;

    /* put clients in switch complete waiting mode */
    req_client->req_config 				= ant_cfg_info.config;
    as_div.client[tx_client].req_config = ant_cfg_info.config;
    /* call registered call back if it is not null */
    if (as_div.client[tx_client].ant_switch_cb != NULL)
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TRM_ASDiv: set mode: Switching requested to CL:%d ",
             tx_client );
      as_div.client[tx_client].ant_switch_cb(ant_cfg_info);
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "TRM_ASDiv SV_enforced set switch call-back NULL client: %d",
             tx_client );
    }
    break;

  case TRM_MODE_ACT_WAIT_SWITCH_COMPLETE:
    /* switching is in progress, so wait till the switching is complete */

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDiv: set mode: Wait Sw Complete CL:%d Mode:%d ",
           set_mode_input->client, set_mode_input->mode );

    req_client->req_mode 	= set_mode_input->mode;
    req_client->req_config	= as_div.next_config;

    break;



  default:
    /* set mode is to be denied */

    set_mode_info.set_mode_grant = TRM_SET_MODE_DENIED;
    set_mode_info.config         = as_div.init_setting.configuration;
    set_mode_info.tag			= req_client->tag;
    if (set_mode_input->set_mode_cb != NULL)
    {
      set_mode_input->set_mode_cb(set_mode_info);
    }
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDiv: set mode: Cl:%d mode req:%d set mode denied:%d",
           set_mode_input->client, set_mode_input->mode, 
           set_mode_info.set_mode_grant);

  }
}

/*============================================================================

FUNCTION ASDIV_GET_SET_MODE_ACTION

DESCRIPTION
  Depending upon the request made by client and the current status, this
  function calculates the action that needs to be performed and returns.
  
DEPENDENCIES
  None

RETURN VALUE
  Action to perform

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_mode_action_type asdiv_get_set_mode_action(
   trm_set_mode_input_type			*set_mode_input,
   ant_switch_client_struct			*req_client
   )
{
  uint8								keep_default_in_sv;
  trm_ant_switch_mode_action_type		mode_act;
  agg_client_mode_type				old_agg_mode;
  agg_client_mode_type				new_agg_mode;

  /* check for NULL pointers */
  ASD_NULL_PTR_CHK(set_mode_input, TRM_MODE_ACT_DENY);
  ASD_NULL_PTR_CHK(req_client, TRM_MODE_ACT_DENY);

  /* calculate the new aggregate mode considering the requested mode */
  old_agg_mode	= as_div.aggregate_client_mode;
  new_agg_mode	= old_agg_mode -
     asdiv_map_mode_value(req_client->curr_mode) +
     asdiv_map_mode_value(set_mode_input->mode);
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         " ASD set mode- new agg:%d prev agg mode:%d ",
         new_agg_mode, old_agg_mode );

  CLR_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);

  switch (new_agg_mode)
  {
  case ASDIV_MODE_TX_NONE:
    /* resulting mode is no client in traffic */

    if (old_agg_mode == ASDIV_MODE_TX_NONE ||
        old_agg_mode == ASDIV_MODE_ENF_ONLY)
    {
      mode_act = TRM_MODE_ACT_GRANT;

    }
    else if (old_agg_mode == ASDIV_MODE_TX_ONLY)
    {
      /* Req client is the last client coming out of traffic, so check
         to see if it has to default in idle state */
      if (ASD_KEEP_IN_IDLE &&
          !ASD_CURR_EQUALS_INIT_CFG &&
          req_client->curr_mode != TRM_CLIENT_MODE_RXACQ)
      {
        /* reset bit to indicate that mode should be denied if
           switching gets cancelled */
        SET_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);
        mode_act = TRM_MODE_ACT_SWITCH_AND_CONTINUE;

      }
      else
      {
        mode_act = TRM_MODE_ACT_GRANT;
      }
    }
    else
    {
      /* unexpected */
      mode_act	= TRM_MODE_ACT_DENY;

    }

    break;


  case ASDIV_MODE_TX_ONLY:
    /* Req client is going to traffic mode, grant the mode but
       wait till switch complete if it is transition */

    if (old_agg_mode == ASDIV_MODE_TX_SIMUL ||
        old_agg_mode == ASDIV_MODE_ENF_TX ||
        old_agg_mode == ASDIV_MODE_TX_ONLY)
    {
      mode_act = TRM_MODE_ACT_GRANT;

    }
    else if (old_agg_mode == ASDIV_MODE_TX_NONE)
    {
      /* grant mode if switch is not in transisiton else wait
         for the switch to complete */
      if (as_div.curr_setting.configuration != SWITCH_IN_TRANSITION)
      {

        mode_act = TRM_MODE_ACT_GRANT;
      }
      else
      {
        SET_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);
        mode_act = TRM_MODE_ACT_WAIT_SWITCH_COMPLETE;
      }

    }
    else
    {
      /* unexpected */
      mode_act	= TRM_MODE_ACT_DENY;

    }
    break;

  case ASDIV_MODE_ENF_ONLY:

    if (old_agg_mode == ASDIV_MODE_ENF_TX)
    {
      if (ASD_KEEP_IN_IDLE &&
          !ASD_CURR_EQUALS_INIT_CFG)
      {
        /* no client is in tx, so default in idle if enabled */
        SET_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);
        mode_act = TRM_MODE_ACT_SWITCH_AND_CONTINUE;
      }
      else
      {
        mode_act = TRM_MODE_ACT_GRANT;
      }

    }
    else if (set_mode_input->mode == TRM_CLIENT_MODE_LOCK_SWITCH)
    {
      mode_act = TRM_MODE_ACT_GRANT;
    }
    else
    {
      /* unexpected */
      mode_act	= TRM_MODE_ACT_DENY;

    }

    break;

  case ASDIV_MODE_TX_SIMUL:

    /* figure out the SV mode */
    keep_default_in_sv = (uint8)asdiv_get_simul_mode(set_mode_input);

    /* we need to know if simultaneous mode is SGLTE active
               for deciding switching request */
    if (keep_default_in_sv == ((uint8)CLIENT_SGLTE_MODE))
    {
      SET_BIT_POS(as_div.stat_flag, BIT_SGLTE_ACTIVE);
    }

    if (old_agg_mode == ASDIV_MODE_TX_ONLY)
    {
      if (CHECK_BIT_POS(as_div.config_reg, keep_default_in_sv) &&
          as_div.init_setting.sv_default_configuration != as_div.curr_setting.configuration)
      {
        /* default in SV enabled and current config is differnt than default
           so go to default */
        SET_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);
        mode_act = TRM_MODE_ACT_HANDLE_SV_ENFORCED;
      }
      else if (ASD_SWITCH_IN_TRANSITION)
      {
        /* if no need to default, wait if switch is in transition
           grant otherwise */
        SET_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);

        mode_act = TRM_MODE_ACT_WAIT_SWITCH_COMPLETE;

      }
      else
      {
        mode_act = TRM_MODE_ACT_GRANT;
      }

    }
    else if (old_agg_mode == ASDIV_MODE_TX_SIMUL)
    {
      mode_act = TRM_MODE_ACT_GRANT;
    }
    else
    {
      /* unexpected */
      mode_act	= TRM_MODE_ACT_DENY;
    }

    break;

  case ASDIV_MODE_ENF_TX:

    if ((old_agg_mode == ASDIV_MODE_TX_ONLY ||
         old_agg_mode == ASDIV_MODE_TX_SIMUL) &&
        set_mode_input->mode != TRM_CLIENT_MODE_LOCK_SWITCH)
    {
      /* resulting mode is enforced rx tx, so needs to switch
         to opposite config */
      CLR_BIT_POS(as_div.stat_flag, BIT_GRANT_MODE_IF_CANCELLED);
      if (ASD_SWITCH_IN_TRANSITION)
      {
        mode_act = TRM_MODE_ACT_WAIT_SWITCH_COMPLETE;
      }
      else
      {
        mode_act = TRM_MODE_ACT_HANDLE_SV_ENFORCED;
      }
    }
    else if (old_agg_mode == ASDIV_MODE_ENF_ONLY ||
             old_agg_mode == ASDIV_MODE_ENF_TX ||
             set_mode_input->mode == TRM_CLIENT_MODE_LOCK_SWITCH)
    {

      mode_act = TRM_MODE_ACT_GRANT;
    }
    else
    {
      /* unexpected */
      mode_act = TRM_MODE_ACT_DENY;

    }

    break;


  default:
    /* unexpected */
    mode_act	= TRM_MODE_ACT_DENY;

  }

  /* if current mode is not denied then update the aggregate mode */
  if (mode_act != TRM_MODE_ACT_DENY)
  {
    as_div.aggregate_client_mode = new_agg_mode;
  }

  return mode_act;

} /* asdiv_get_set_mode_action */

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SET_MODE

DESCRIPTION
  L1s update the mode which they are entering: SLEEP, RX or TX. Depending on
  the mode, set mode logic could initiate call-back to other techs.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_set_mode(
   trm_set_mode_input_type 	set_mode_input
   )
{

  boolean			          	sw_perm_store;
  ant_switch_client_struct		*req_client = NULL;
  trm_ant_switch_mode_action_type		mode_act;
  trm_ant_switch_set_mode_type		set_mode_info;

  /* get the lock */
  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  /* deny request if feature is not supported */
  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           " ASD set mode: Feature unsupported!! CL;%d Mode:%d ",
           set_mode_input.client,set_mode_input.mode );

    set_mode_info.set_mode_grant = TRM_SET_MODE_DENIED_FEATURE_UNSUPPORTED;
    set_mode_info.config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
    set_mode_info.tag = set_mode_input.tag;

    if (set_mode_input.set_mode_cb != NULL)
    {
      set_mode_input.set_mode_cb(set_mode_info);
    }
    ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
    return;
  }


  mode_act 		= TRM_MODE_ACT_GRANT;
  /* back up current switching permission*/
  sw_perm_store = IS_SWITCHING_DISABLED;

  /* validate the input values, deny if invalid request is made */
  if ( !trm_validate_set_mode_input(set_mode_input) )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           " ASD set mode: Invalid params!! CL;%d Mode:%d ",
           set_mode_input.client, set_mode_input.mode );
    ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
    return;
  }

      /* initialization */
  if ( TRM_ASDIV_IS_TECH_VALID(set_mode_input.client)) 
  {
     req_client		= &as_div.client[set_mode_input.client];
  }
  else
  {
     ERR_FATAL("TRM_ASDiv: set mode, Invalid client %d ", 
               set_mode_input.client, 0, 0);
  }

    /* request is valid, so process further */
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         " ASD set mode- Client:%d Mode:%d ",
         set_mode_input.client, set_mode_input.mode );

  /* run new algorithm for all cases */
  if (TRUE)
  {

    /* change rxtx mode to rxtx_low for data tech */
    if( TRM_ASDIV_IS_DATA_TECH(req_client->id) &&
        set_mode_input.mode == TRM_CLIENT_MODE_RXTX )
    {
      set_mode_input.mode = TRM_CLIENT_MODE_RXTX_LOW;
    }

    req_client->req_mode = set_mode_input.mode;
    req_client->ant_switch_cb = set_mode_input.ant_switch_cb;
    req_client->ntfy_switch_change = set_mode_input.inform_switch_change;
    req_client->set_mode_cb = set_mode_input.set_mode_cb;
    req_client->set_mode_cfg_cb = NULL;
    req_client->tag = set_mode_input.tag;
    req_client->ntfy_switch_change = set_mode_input.inform_switch_change;

    /* update tech info */
    trm_asdiv_update_actv_list_tech_info(req_client->id);

    /* update active list clients' priority */
    trm_asdiv_update_actv_list_prio_info();

    /* if mode request is for tx and switch is in transition keep the
       request in pending till switch is complete */
    if ( TRM_ASDIV_IS_TRAFFIC_MODE(req_client->req_mode) &&
        req_client->req_mode != req_client->curr_mode &&
        ASD_SWITCH_IN_TRANSITION)
    {
      req_client->req_config = as_div.next_config;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "TRM_ASDIV: req pending, switching in progress by:%d req_tech:%d",
             as_div.switching_tech, as_div.requesting_tech );
    }
    else
    {
      asdiv_grant_mode(req_client);

    }

    ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

    return;

  }


  /* we need to know if SGLTE mode is active,
     So we disable the SGLTE ACTIVE bit initially, and if the 
     resulting mode is SGLTE we set the bit */
  CLR_BIT_POS(as_div.stat_flag, BIT_SGLTE_ACTIVE);

  /* figure out what action needs to be performed as per the current request */
  mode_act = asdiv_get_set_mode_action(&set_mode_input,
                                       req_client);


  /* Necessary action is identified, so hanlde it accordingly */
  asdiv_handle_set_mode_req(&set_mode_input,
                            req_client,
                            mode_act);

  /* check if resulting mode can disable the switch */
  if (IS_SW_DISABLED_BY_MODE(as_div.aggregate_client_mode))
  {
    SET_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_AGG_MODE);
  }
  else
  {
    CLR_BIT_POS(as_div.stat_flag, BIT_SW_DISABLED_AGG_MODE);
  }

  /* check if switching permission has changed after the mode request,
      inform subscribed clients if changed */
  if (sw_perm_store != IS_SWITCHING_DISABLED)
  {
    asdiv_inform_switch_info_change(req_client->id,
                                    ASDIV_SW_PERM_CHANGED);
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

  return;

} /* trm_ant_switch_div_set_mode */



/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SWITCH_COMPLETE

DESCRIPTION
  L1s update trm about switch complete once they have done the switch following
  set_config or set_mode.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_switch_complete(
   trm_switch_complete_input_type switch_input
   )
{

  int32 						loopcnt;
  trm_ant_switch_cb_info_type		cb_info;

/*--------------------------------------------------------------------------*/

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv: Feature unsupported!! cl:%d config:%d",
           switch_input.client, switch_input.config );
    return;
  }

  /* make sure arguments are expected */
  if ( !TRM_ASDIV_IS_TECH_VALID(switch_input.client) ||
       switch_input.config > TRM_ANT_SWITCH_DIV_CONFIG_1 ) 
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv: sw complete unexp Cl:%d config:%d",
           switch_input.client, switch_input.config );
    return;
  }

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDiv: Sw complete: exp cfg:%d Cl:%d cfg:%d",
         as_div.next_config, switch_input.client, switch_input.config);

  /* get the default config */
  if (switch_input.config == TRM_ANT_SWITCH_DIV_CONFIG_DEFAULT)
  {
    switch_input.config = as_div.init_setting.configuration;
  }

  /* if the switch changed is the last requested switching */
  if (ASD_SWITCH_IN_TRANSITION &&
      as_div.switching_tech == switch_input.client &&
      switch_input.config == as_div.next_config)
  {
    /* populate the call back info */
    cb_info.action = TRM_ANT_SWITCH_SET_CONFIG_DONE;
    cb_info.config = as_div.next_config;
    cb_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;

    for (loopcnt = 0; loopcnt <= TRM_LAST_CLIENT; ++loopcnt)
    {

      /* inform subscribed clients about switch completion */
      if ( (as_div.client[loopcnt].ntfy_switch_change == TRUE &&
            as_div.client[loopcnt].id != switch_input.client) ||
           as_div.client[loopcnt].switch_start_reported == TRUE )
      {

        cb_info.client = as_div.client[loopcnt].id;

        if (as_div.client[loopcnt].ant_switch_cb != NULL)
        {
          as_div.client[loopcnt].ant_switch_cb(cb_info);
        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "TRM_ASDiv: sw complete NULL cb; cl:%d ",
                 switch_input.client );
        }

        as_div.client[loopcnt].switch_start_reported = FALSE;

      }

      /* Grant pending mode request */
      if (as_div.client[loopcnt].req_config == switch_input.config)
      {
        as_div.client[loopcnt].req_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;

        if (as_div.client[loopcnt].req_mode != as_div.client[loopcnt].curr_mode)
        {

          if (loopcnt != switch_input.client)
          {
            asdiv_grant_mode(&as_div.client[loopcnt]);
          }

          trm_ant_switch_inform_mode_change(as_div.client[loopcnt].id,
                                            as_div.client[loopcnt].req_mode);

        }
      }
    }

 	/* send the switch event */
    as_div.event_info.client_id = (uint8) switch_input.client;
    as_div.event_info.old_switch_config = (uint8) as_div.prev_config;
    as_div.event_info.new_switch_config = (uint8) as_div.next_config;
    as_div.event_info.event_type = TRM_ASDIV_EVENT_SWITCHING_OTHERS;

    event_report_payload((event_id_enum_type) EVENT_MCS_TRM_ASDIV,
                         sizeof(trm_asdiv_event_info_type),
                         &as_div.event_info );


    /* update the current switch configuration */
    as_div.curr_setting.configuration	= as_div.next_config;

    /* update switching and requesting techs */
    as_div.switching_tech = TRM_NO_CLIENT;
    as_div.requesting_tech = TRM_NO_CLIENT;

    /* change previous and next config to max after switch complet */
    as_div.prev_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
    as_div.next_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;

  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

} /* trm_ant_switch_div_switch_complete */


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SWITCH_CANCEL

DESCRIPTION
  L1s update trm about switch incomplete if for any reason the switching
  couldnt go through.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_ant_switch_div_switch_cancel(trm_switch_complete_input_type req_data)
{
  uint8 cnt;
  trm_ant_switch_cb_info_type		cb_info;

  /* check if feature is supported */
  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv: Feature unsupported!! cl:%d config:%d",
           req_data.client, req_data.config );
    return;
  }

  /* make sure arguments are expected */
  if ( !TRM_ASDIV_IS_TECH_VALID(req_data.client) ||
       req_data.config > TRM_ANT_SWITCH_DIV_CONFIG_1 ) 
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv: sw cancel unexp Cl:%d config:%d",
           req_data.client, req_data.config );
    return;
  }

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  /* check if the caller is the legitimate client in switch control */
  if (ASD_SWITCH_IN_TRANSITION &&
       as_div.switching_tech == req_data.client &&
      as_div.next_config == req_data.config)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "ASD Switch Req Cancelled, Cl:%d, cf:%d",
           req_data.client, req_data.config );

    /* populate the call back info */
    cb_info.action = TRM_ANT_SWITCH_SET_CONFIG_DONE;
    cb_info.config = as_div.prev_config;
    cb_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;

    as_div.curr_setting.configuration = as_div.prev_config;
    as_div.client[req_data.client].req_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;

    /* retrieve waiting client's mode */
    for (cnt = 0; cnt <= TRM_LAST_CLIENT; cnt++)
    {
      if (as_div.client[cnt].req_config == req_data.config)
      {
        as_div.client[cnt].req_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
      }
      if (as_div.client[cnt].curr_mode != as_div.client[cnt].req_mode)
      {

        /* mode granting if switching cancelled enabled, so grant */
        asdiv_grant_mode(&as_div.client[cnt]);
      }

      /* inform switch initiation reported techs about switch process is done */
      if(as_div.client[cnt].switch_start_reported == TRUE)
      {
        as_div.client[cnt].switch_start_reported = FALSE;
        cb_info.client = as_div.client[cnt].id;

        if (as_div.client[cnt].ant_switch_cb != NULL)
        {
          as_div.client[cnt].ant_switch_cb(cb_info);
        }

     }

    }

	/* send the switch event */
    as_div.event_info.client_id = (uint8) req_data.client;
    as_div.event_info.old_switch_config = (uint8) as_div.prev_config;
    as_div.event_info.new_switch_config = (uint8) as_div.prev_config;
    as_div.event_info.event_type = TRM_ASDIV_EVENT_SWITCHING_CANCEL;

    event_report_payload((event_id_enum_type) EVENT_MCS_TRM_ASDIV,
                         sizeof(trm_asdiv_event_info_type),
                         &as_div.event_info );

    /*update the switching and requesting techs */
    as_div.switching_tech = TRM_NO_CLIENT;
    as_div.requesting_tech = TRM_NO_CLIENT;

    /* change previous and next config to max after switch complet */
    as_div.prev_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;
    as_div.next_config = TRM_ANT_SWITCH_DIV_CONFIG_MAX;

  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv: Invalid Switch cancel request, cl:%d cf: %d",
           req_data.client, req_data.config );
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

} /* trm_ant_switch_div_switch_cancel*/


/*============================================================================

FUNCTION TRM_ANT_SWITCH_SET_CONTROL

DESCRIPTION
  Updates the ant switch diversity switch control, each tech can hold or
  release the switch control form its side. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_set_control(trm_ant_switch_set_ctrl_type 	ctrl_req)
{

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv Feature not supported" );
  }
  else if (ctrl_req.req_type >= TRM_ANT_SWITCH_CTRL_INVALID ||
           ctrl_req.client > TRM_LAST_CLIENT)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDiv; invalid values cl:%d req:%d",
           ctrl_req.client, ctrl_req.req_type );
  }
  else
  {

    as_div.client[ctrl_req.client].sw_control = ctrl_req.req_type;

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDiv; control set by cl:%d to %d",
           ctrl_req.client, ctrl_req.req_type );

  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

} /* trm_ant_switch_set_control */


/*============================================================================

FUNCTION TRM_GET_CLIENT_ANT_SWITCH_MODE

DESCRIPTION
  Returns the mode of the requested client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
trm_client_mode_type trm_get_client_ant_switch_mode(trm_client_enum_t clientid)
{
  trm_client_mode_type ret_val = TRM_CLIENT_MODE_MAX;

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
  }
  else if (clientid <= TRM_LAST_CLIENT)
  {
    ret_val = as_div.client[clientid].curr_mode;
    if (as_div.client[clientid].curr_mode != as_div.client[clientid].req_mode)
    {
      ret_val = as_div.client[clientid].req_mode;
    }
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

  return ret_val;
}


/*============================================================================

FUNCTION TRM_SET_ANT_SWITCH_DIV_STATE

DESCRIPTION
  Updates the ant switch diversity state(enable or disable) for a given client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_ant_switch_div_state(trm_set_asd_state_input_type input)
{

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
  }
  else if (input.client <= TRM_LAST_CLIENT)
  {
    as_div.client[input.client].ant_switch_state = input.state;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "Client %d ant switch div state changed to %d",
           input.client, input.state );
  }
  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
}


/*============================================================================

FUNCTION TRM_SET_ANT_SWITCH_DIV_STATE

DESCRIPTION
  Updates the ant switch diversity state(enable or disable) for a given client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_ant_switch_mode_change_notify(
   trm_ant_switch_mode_notification_input_type input
   )
{

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
  }
  else if (input.client <= TRM_LAST_CLIENT)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "Mode change notify request, Client: %d, notify: %d",
           input.client, input.should_notify );
    as_div.client[input.client].ntfy_mode_change = input.should_notify;
    as_div.client[input.client].mode_change_cb   = input.mode_change_cb;
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "Invalid client info for mode change notify: %d", 
           input.client );
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
}



/*============================================================================

FUNCTION TRM_ANT_SWITCH_IS_SWITCHING_ALLOWED

DESCRIPTION
  Returns whether switching is allowed or not.
  
DEPENDENCIES
  None

RETURN VALUE
  boolean

SIDE EFFECTS
  None

============================================================================*/
boolean trm_ant_switch_is_switching_allowed(void)
{
  boolean retval = TRUE;

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
    retval = FALSE;
  }
  /* check if switching is disabled */
  else if (IS_SWITCHING_DISABLED)
  {
    retval = FALSE;
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

  return retval;

}


/* DSDA ASDiv changes */

/*============================================================================

FUNCTION TRM_ASDIV_REPORT_SWITCH_INITIATION

DESCRIPTION
   Updates the switch initiation to the passive tech. In dual tx scenario of
   techs X+Y, if X requests for switching that can be granted, Y is notified
   about the starting of the switching process.
 
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_report_switch_initiation
(
   trm_client_enum_t                switching_client,
   trm_ant_switch_div_config_type   requested_config
)
{
  uint8                           i, client_id;
  uint8                           tx_tech_count = 0;
  trm_ant_switch_cb_info_type     call_back_info;

  /* check for dual tx scenario by getting tech count in tx */
  for(i=0; i<as_div.active_list.num_clients; i++)
  {
    client_id = (uint8) as_div.active_list.cl_id[i];
    if( TRM_ASDIV_IS_TECH_VALID(client_id) &&
        ( as_div.client[client_id].curr_mode == TRM_CLIENT_MODE_RXTX ||
          as_div.client[client_id].curr_mode == TRM_CLIENT_MODE_RXTX_LOW ) )
    {
      tx_tech_count++;
    }

  }

  /* if tech count is greater than 1, report switch initiation */
  if( tx_tech_count > 1 ||
      ( tx_tech_count == 1 &&
        as_div.client[switching_client].curr_mode == TRM_CLIENT_MODE_RXACQ ) )
  {
    call_back_info.action = TRM_ANT_SWITCH_SET_CONFIG_STARTED;
    
    call_back_info.config = requested_config;
    call_back_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;

    for(i=0; i<as_div.active_list.num_clients; i++)
    {
      /* skip reporting if this is the requesting client */
      if( as_div.active_list.cl_id[i] == switching_client )
      {
        continue;
      }

      call_back_info.client = as_div.active_list.cl_id[i];
      client_id = (uint8) as_div.active_list.cl_id[i];

      if( TRM_ASDIV_IS_TECH_VALID(client_id) &&
          ( as_div.client[client_id].curr_mode == TRM_CLIENT_MODE_RXTX ||
            as_div.client[client_id].curr_mode == TRM_CLIENT_MODE_RXTX_LOW ) &&
          as_div.client[client_id].ant_switch_cb != NULL )
      {
        as_div.client[client_id].ant_switch_cb( call_back_info );

        as_div.client[client_id].switch_start_reported = TRUE;

        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "TRM_ASDIV: rep_sw_initiate; cl:%d, reported switch initiation",
               client_id );
        
      }
    }
  }

}


/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_PREF_CONFIG

DESCRIPTION
   Updates the preferred switch configuration of the client, it is triggered
   when a tech requests for switching
 
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_update_pref_config(
   trm_client_enum_t                cl,
   trm_ant_switch_div_config_type   config
   )
{
  trm_ant_switch_div_config_type new_pref_cfg;

  new_pref_cfg = config;

  /* Check if configuration is invalid */
  if (config >= TRM_ANT_SWITCH_DIV_CONFIG_INVALID)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: pref_cfg_upd; Invalid sw config cl:%d, cfg:%d",
           cl, config );
    new_pref_cfg = as_div.client[cl].pref_config;
  }
  /* if config is PREFERRED then no update is required */
  else if (config == TRM_ANT_SWITCH_DIV_CONFIG_PREFERRED)
  {
    return;
  }
  /* change the new config to current config if it is not in transition*/
  else if (config == TRM_ANT_SWITCH_DIV_CONFIG_CURRENT)
  {
    if(as_div.curr_setting.configuration != SWITCH_IN_TRANSITION)
    {
      new_pref_cfg = as_div.curr_setting.configuration;
    }
    else
    {
      /* no update required if switch is in progress and tech is 
         asking for the current config */
      return;
    }
  }
  /* if requested for default config, get the default configuration */
  else if (config == TRM_ANT_SWITCH_DIV_CONFIG_DEFAULT)
  {
    new_pref_cfg = as_div.init_setting.configuration;
  }
  /* update teh pref config if it has changed */
  if (as_div.client[cl].pref_config != new_pref_cfg)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: pref_cfg_upd; prev_cfg:%d, new_cfg:%d",
           as_div.client[cl].pref_config, new_pref_cfg );
    as_div.client[cl].pref_config = new_pref_cfg;
  }

}

/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_SWITCH_LOCK_MASK

DESCRIPTION
   Update the switch lock mask with the request made by the clients.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_update_switch_lock_mask(void)
{
  uint8 i;
  uint8 client_id;

  /* start with clearing the mask*/
  as_div.lock_mask = 0;

  /* check for each active client */
  for (i = 0; i < as_div.active_list.num_clients; i++)
  {
    client_id = (uint8) as_div.active_list.cl_id[i];
    as_div.lock_mask |= as_div.client[client_id].switch_lock_mask;
  }

}

/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_IDLE_COL_DATA

DESCRIPTION
   Updates the collision count if a client is denied its preferred chain
   due to idle_wakup + idle_wakeup collision
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_update_idle_col_data(
   trm_client_enum_t       cl
   )
{


  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "TRM_ASDIV: col_upd; BEFORE cl:%d cnt:%d cfg:%d",
         as_div.idl_idl_col_data.denied_cl,
         as_div.idl_idl_col_data.col_count,
         as_div.idl_idl_col_data.denied_config);

  /* Before updating the colision count, make sure
        a) the client id is same, otherwise reset
        b) the preferred config is same, otherwise reset */
  if (as_div.idl_idl_col_data.denied_cl != cl ||
      as_div.idl_idl_col_data.denied_config != as_div.client[cl].pref_config)
  {
    as_div.idl_idl_col_data.col_count = 0;
  }

  /* update the collision data */
  as_div.idl_idl_col_data.denied_cl = cl;
  as_div.idl_idl_col_data.denied_config = as_div.client[cl].pref_config;
  as_div.idl_idl_col_data.col_count++;

  /* if the limit has been reached, set limit_reach flag */
  if (as_div.idl_idl_col_data.col_count >= as_div.idl_idl_col_data.idle_idle_col_limit)
  {
    as_div.idl_idl_col_data.limit_reached = TRUE;
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDIV: col_upd; col_limit reached cl:%d cnt:%d cfg:%d",
           cl, as_div.idl_idl_col_data.col_count,
           as_div.idl_idl_col_data.denied_config);
  }

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDIV: col_upd; AFTER cl:%d cnt:%d cfg:%d",
         cl, as_div.idl_idl_col_data.col_count,
         as_div.idl_idl_col_data.denied_config);
}

/*============================================================================

FUNCTION TRM_ASDIV_CHK_MODE_COMB_RESTRICTION

DESCRIPTION
  Called internally to see if active mode combination restricts switching
  restricting combined modes:
      idle_wakeup+idle_wakeup
      idle_wakeup+traffic
      acq + traffic
      sleep + non-traffic
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE: if current combined mode is not restricting
  FALSE: if current combined mode is restricting

SIDE EFFECTS
  None

============================================================================*/
boolean trm_asdiv_chk_mode_comb_restriction(
   trm_client_enum_t    this_cl
   )
{
  uint8               i;
  boolean              reiterate;
  boolean              mode_comb_alowd = TRUE;
  trm_client_enum_t    other_cl;


  /* check for invalid tech in the list */
  if ( !TRM_ASDIV_IS_TECH_VALID(this_cl) ) 
  {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "TRM_ASDIV: mode_comb_chk, Invalid client id: %d", this_cl );
     return FALSE;
  }

  /* check with the client in active list */
  for (i = 0; i < as_div.active_list.num_clients; i++)
  {
    other_cl = as_div.active_list.cl_id[i];

    if (other_cl == this_cl)
    {
      continue;
    }

    do{
     /* check for invalid tech in the list */
      reiterate = FALSE;

    if ( !TRM_ASDIV_IS_TECH_VALID(other_cl) ) 
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "TRM_ASDIV: mode_comb_chk, Invalid client id: %d", other_cl );
       mode_comb_alowd = FALSE;
       break;
    }

      if( TRM_ASDIV_IS_IDLE_MODE(as_div.client[other_cl].req_mode) )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "TRM_ASDIV: sw_perm-DENIED for client:%d as client:%d is in idle mode: %d",
               this_cl, other_cl, as_div.client[other_cl].req_mode );
        mode_comb_alowd = FALSE;

        /* check if it is idle/idle collision to set collision count */
      if (as_div.client[this_cl].req_mode == TRM_CLIENT_MODE_IDLE_WAKEUP &&
          as_div.client[other_cl].req_mode == TRM_CLIENT_MODE_IDLE_WAKEUP)
      {
        /* restricting mode idle_wakeup + idle_wakeup */
        as_div.idl_idl_col_data.is_idl_idl_col = TRUE;
      }

        break;

      }

      if( (TRM_ASDIV_IS_IDLE_MODE(as_div.client[this_cl].req_mode)||
           as_div.client[this_cl].req_mode == TRM_CLIENT_MODE_RX_ONLY ) &&
          TRM_ASDIV_IS_TRAFFIC_MODE(as_div.client[other_cl].req_mode) )
      {
         if ( !( (this_cl == TRM_1X) &&
                 (as_div.client[this_cl].curr_mode == TRM_CLIENT_MODE_RXACQ) ) )
         {

           MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "TRM_ASDIV: sw_perm-DENIED for client:%d as client:%d is in traffic mode: %d",
                 this_cl, other_cl, as_div.client[other_cl].req_mode );
           mode_comb_alowd = FALSE;
           break;
         }

      }
      reiterate = TRUE;
    }while(0);

    if( reiterate == FALSE ) 
    {
      break;
    }
  }

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "TRM_ASDIV: mode_comb_alowd, %d ", mode_comb_alowd );

  return mode_comb_alowd;

}

/*============================================================================

FUNCTION TRM_ASDIV_CHK_SWITCH_PERM

DESCRIPTION
  Called internally to see if switching can be done by the passed client
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE: if switching can be done
  FALSE: if switching cannot be done

SIDE EFFECTS
  None

============================================================================*/
boolean trm_asdiv_chk_switch_perm(trm_client_enum_t cl_id)
{
  int i;
  uint8 client;
  boolean  perm_granted = FALSE;
  boolean  mode_comb_alowd;

  do
  {
  
    if ( as_div.curr_setting.configuration == SWITCH_IN_TRANSITION )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TRM_ASDIV: sw_perm-DENIED, sw in TRANSITION sw_tech:%d req_tech:%d",
             as_div.switching_tech,
             as_div.requesting_tech );

      break;
    }

    if( as_div.lock_mask != 0 )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TRM_ASDIV: sw_perm-DENIED, sw locked mask: 0x%x",
             as_div.lock_mask );
      for(i=0; i<as_div.active_list.num_clients; i++)
      {
        client = (uint8) as_div.active_list.cl_id[i];
        if(as_div.client[client].switch_lock_mask != 0)
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "TRM_ASDIV: Sw locking tech:%d mask: 0x%x",
                 client, as_div.client[client].switch_lock_mask );
        }
      }

      break;
    }

    /* check if current active mode combination restrict switching */
    mode_comb_alowd = trm_asdiv_chk_mode_comb_restriction(cl_id);

    if (mode_comb_alowd == TRUE)
    {
      /* mode combination allows switching,
         if requesting cl is high priority client, grant
         else check if the high priority client is in safe situation */
      if ( cl_id == as_div.active_list.high_prio_cl ||
           as_div.is_standalone_mode == TRUE )
      {
        /* it is high priority client, so grant */
        perm_granted = TRUE;
      }
      else
      {
        /* neither standalone nor highest priority client */
        /* start with grant */
        perm_granted = TRUE;

        /* check in the active list it it has high priority 
           client holding the control */
        for(i=0; i<as_div.active_list.num_clients; i++)
        {
          /* no need to check with itself */
          if(cl_id == as_div.active_list.cl_id[i])
          {
            continue;
      }

          client = (uint8) as_div.active_list.cl_id[i];
          
          if( as_div.client[client].priority_mask > as_div.client[cl_id].priority_mask &&
              as_div.client[client].sw_control != TRM_ANT_SWITCH_CTRL_RELEASE )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                   "TRM_ASDIV: SW perm denied: High priority cl:%d ctrl held:%d",
                   client, as_div.client[client].sw_control );
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                   "TRM_ASDIV: Priority Mask High:0x%x Low: 0x%x",
                   as_div.client[client].priority_mask, 
                   as_div.client[cl_id].priority_mask );

            perm_granted = FALSE;
            break;
          }
        }
      }
    }
  }while(0);

  return perm_granted;

}

/*============================================================================

FUNCTION TRM_ASDIV_MAP_TRM_SP_ID

DESCRIPTION
  Maps TRM client id to subscription priority client id

DEPENDENCIES
  None

RETURN VALUE
  Subscription priority client id

SIDE EFFECTS
  None

============================================================================*/
sp_client_enum_t trm_asdiv_map_trm_sp_id(
   trm_client_enum_t  trm_id
)
{

  switch(trm_id)
  {
    
    case TRM_GSM2:
      return SP_GSM2;

    case TRM_GSM3:
      return SP_GSM3;

  default:
    return SP_MAX_CLIENT;

  }

}
/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_ACTV_LIST_SUB_INFO

DESCRIPTION
  Updates the subs info i.e. subscription id and subscription state/priority
  of the active clients. It is called when subs prio module calls the callback
  and when a new client is added to the list

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_update_actv_list_sub_info(void)
{

  uint8                     i, j;
  sys_modem_as_id_e_type  sub_id = SYS_MODEM_AS_ID_1;
  trm_client_enum_t       cl_id;
  sp_client_enum_t        sp_id;


  /* for active client get the sub id */
  for (i = 0; i < as_div.active_list.num_clients; i++)
  {
    cl_id = as_div.active_list.cl_id[i];

     /* check for invalid tech in the list */
    if ( !TRM_ASDIV_IS_TECH_VALID(cl_id) ) 
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "TRM_ASDIV: lst_sub, Invalid client id: %d", cl_id );
       break;
    }


    /* if single sim device, set sub id to ID_NONE */
    if (as_div.subs_prio_list.num == 0)
    {
      as_div.client[cl_id].sub_id = SYS_MODEM_AS_ID_NONE;

      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: lst_sub, single sim case" );

    }
    else
    {
      /* if multisim case, get sub id from sub prio data */
      /* If GSM2 || GSM3, look in the gsm_id list for sub id */
      if (ASDIV_IS_SINGLE_TECH_ID(cl_id))
      {
        sp_id = trm_asdiv_map_trm_sp_id(cl_id);

        for(j=0; j< MAX_NUM_OF_SUBSCRIPTIONS; j++)
        {
          if(as_div.subs_prio_list.tech_asid_map.gsm_id_list[j] == sp_id)
          {
            sub_id = j;
            break;
          }
        }
      }
      else
      {
        /* if multimode clients, look in the multimode sim */
        sub_id = as_div.subs_prio_list.tech_asid_map.multimode_as_id;

        if ( TRM_ASDIV_IS_SUBID_VALID(sub_id) &&
             !( as_div.subs_prio_list.active_tech_info.tech_supported_bitmask[sub_id] &
                 (1 << as_div.client[cl_id].sys_tech_id)))
        {
          /* Unexpected case, tech not supported on multisim sub */
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "TRM_ASDIV: tech %d not supported in multisim sub %d",
                 cl_id, sub_id );

        }


      }

      /* sub_id is determined, find out the sub priority */
      for (j = 0; j < as_div.subs_prio_list.num; j++)
      {
        if (sub_id == as_div.subs_prio_list.order[j].sub)
        {
          as_div.client[cl_id].sub_order_idx = j;
          break;
        }
      }
      /* update client's sub id */
      as_div.client[cl_id].sub_id = sub_id;

    }
  }

}

/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_ACTV_LIST_PRIO_INFO

DESCRIPTION
  This api updates the priority mask of the active clients. Also finds
  out the high priority client in the list.
    prioirty mask = 32 bit
      31-24 -> sub state   
      23-16 -> TRM modes
      8-15  -> reserved for tech id
      7-0   -> SIM priority  
  
  
DEPENDENCIES
  None

RETURN VALUE
  NONE

SIDE EFFECTS
  Idle\idle collision count could get reset if the combined mode of active
  clients is other than idle & sleep combination

============================================================================*/

void trm_asdiv_update_actv_list_prio_info()
{

  uint8    i;
  uint8    sub_state, sub_ord_idx;
  uint32   prio_mask;
  trm_client_enum_t    cl_id;


  /* start with combined mode mask as 0, it will be updated with
     active list clients' mode */
  as_div.active_list.active_mode_mask = 0;
  /* set initial high priority mask to be 0 */
  as_div.active_list.high_prio_mask = 0;

  for (i = 0; i < as_div.active_list.num_clients; i++)
  {
    cl_id = as_div.active_list.cl_id[i];

    /* check for invalid tech in the list */
    if ( !TRM_ASDIV_IS_TECH_VALID(cl_id) ) 
    {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "TRM_ASDIV: lst_prio, Invalid client id: %d", cl_id );
       break;
    }

    /* if mode id is invalid (single sub case),
       assume sub state and prio order to be 0*/
    if (as_div.client[cl_id].sub_id == SYS_MODEM_AS_ID_NONE)
    {
      sub_state = 0;
      sub_ord_idx = 0;
    }
    else
    {
      sub_ord_idx = as_div.client[cl_id].sub_order_idx;
      sub_state = as_div.subs_prio_list.order[sub_ord_idx].state;

    }


    /* calculate the priority mask */
    prio_mask = ((1 << sub_state) << TRM_ASDIV_SUB_STATE_SHIFT) |\
       ((1 << (as_div.client[cl_id].req_mode)) << TRM_ASDIV_TECH_MODE_SHIFT) |\
       ((1 << (MAX_NUM_OF_SUBSCRIPTIONS - sub_ord_idx)) << TRM_ASDIV_SUB_PRIO_SHIFT);

    /* find out the high priority client and update */
    if (as_div.active_list.high_prio_mask < prio_mask)
    {
      as_div.active_list.high_prio_mask = prio_mask;
      as_div.active_list.high_prio_cl = cl_id;
    }

    /* assign caluclated priority mask */
    as_div.client[cl_id].priority_mask = prio_mask;

    /* update the aggregate active mode mask, it contains the bit mask for active mode */
    as_div.active_list.active_mode_mask |= ((uint32)(1 << as_div.client[cl_id].req_mode));

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: lst_prio, Cl: %d, sub_state: %d prio: 0x%x", 
           cl_id, sub_state, prio_mask);
  }

  /* if a tech is in mode other than sleep/idlewakeup, reset col count */
  if (as_div.active_list.active_mode_mask & ASDIV_RST_COL_CNT_MASK)
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: lst_prio, Col cnt rst due to Comb mode: 0x%x",
           as_div.active_list.active_mode_mask );
    as_div.idl_idl_col_data.denied_cl = TRM_NO_CLIENT;
  }
}


/*============================================================================

FUNCTION TRM_ASDIV_SUBS_PRIO_CB

DESCRIPTION
  This fucntion is called by subscription priority module whenever there is
  change in SIM activity in Multi sim case.
  
DEPENDENCIES
  None

RETURN VALUE
  NONE

SIDE EFFECTS
  Priorities are updated in the active list 

============================================================================*/

void trm_asdiv_subs_prio_cb(
   sp_subscription_priority_list    subs_prio_list,
   sp_client_enum_t                 client_id
   )
{
  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  /* check if switching is disabled */
  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV: Subs_prio_cb: Feature not supported" );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDIV: Subs_prio_cb called" );

    /* assign the structure passed */
    as_div.subs_prio_list = subs_prio_list;

    /* update the sub info/state */
    trm_asdiv_update_actv_list_sub_info();

    /* update active list client priority */
    trm_asdiv_update_actv_list_prio_info();

        /* check if it is DSDS mode */
    if( as_div.subs_prio_list.active_tech_info.multi_sim_config.max_standby_subscriptions == 2 &&
        as_div.subs_prio_list.active_tech_info.multi_sim_config.max_concurrent_active_subscriptions == 1 )
    {
      as_div.is_standalone_mode = TRUE;
    }

  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
}


/*============================================================================

FUNCTION TRM_ASDIV_UPDATE_ACTIVE_LIST_TECH_INFO

DESCRIPTION
  Whenever a new tech becomes active, it is added to the active list and tech
  becoming inactive is removed
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_update_actv_list_tech_info(
   trm_client_enum_t    cl
   )
{

  int ind, num;

  num = as_div.active_list.num_clients;

  if ( !TRM_ASDIV_IS_TECH_VALID (cl) ) 
  {
     return;
  }

  /* if requesting client is new, add to the active list */
  if ( as_div.client[cl].curr_mode == TRM_CLIENT_MODE_INACTIVE &&
       as_div.client[cl].req_mode != TRM_CLIENT_MODE_INACTIVE )
  {
    as_div.active_list.cl_id[num] = cl;
    as_div.active_list.num_clients++;

    /* need to find the sub_id, order_index, sub_prio */
    trm_asdiv_update_actv_list_sub_info();

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: upd_act_lst: Client: %d, added; cnt:%d",
           cl, as_div.active_list.num_clients );
  }
  /* if requesting client is being inactive, remove from the list */
  else if ( as_div.client[cl].req_mode == TRM_CLIENT_MODE_INACTIVE &&
            as_div.client[cl].curr_mode != TRM_CLIENT_MODE_INACTIVE)
  {
    for (ind = 0; ind < num; ind++)
    {
      if (as_div.active_list.cl_id[ind] == cl)
      {
        /* there is a match, reduce the active client number and shift */
        num = --as_div.active_list.num_clients;

        while (ind < num)
        {
          as_div.active_list.cl_id[ind] = as_div.active_list.cl_id[ind + 1];
          ind++;
        }
        break;
      }
    }

    /* need to find the sub_id, order_index, sub_prio */
    trm_asdiv_update_actv_list_sub_info();

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "TRM_ASDIV: upd_act_lst: Client: %d, removed, cnt:%d",
           cl, as_div.active_list.num_clients );
  }
  else
  {
    // as_div.client[cl].curr_mode =
  }


}

/*============================================================================

FUNCTION TRM_ASDIV_VALIDATE_CLIENT_INFO

DESCRIPTION
  
  
DEPENDENCIES
  None

RETURN VALUE
  NONE

SIDE EFFECTS
 

============================================================================*/
boolean trm_asdiv_validate_client_info(
   const trm_set_mode_and_config_input_t 	*input_data
   )
{
  boolean                             info_valid = TRUE;
  trm_client_enum_t                   cl;
  trm_asd_set_mode_and_config_type    cb_info;

  cl = input_data->client;

  cb_info.config = as_div.curr_setting.configuration;
  cb_info.tag = input_data->tag;
  cb_info.set_config_grant = TRM_ANT_SWITCH_DIV_SET_CFG_DENIED;

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    info_valid = FALSE;
    cb_info.set_mode_grant = TRM_SET_MODE_DENIED_FEATURE_UNSUPPORTED;
    
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV: Feature not supported!" );

  }
  /* Validate the client info */
  else if (cl > TRM_LAST_CLIENT ||
           input_data->set_mode_cb == NULL ||
           input_data->config >= TRM_ANT_SWITCH_DIV_CONFIG_INVALID ||
           input_data->mode >= TRM_CLIENT_MODE_MAX)
  {
    info_valid = FALSE;
    cb_info.set_mode_grant = TRM_SET_MODE_DENIED_INVALID_PARAMETERS;

    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV: set_mod_cfg: Invalid client info");
  }

  if (info_valid == FALSE)
  {
    /* deny the request if validation fails */
    if (input_data->set_mode_cb != NULL)
    {
      input_data->set_mode_cb(cb_info);
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "TRM_ASDiv: Set_mode: Invalid param with no cb, client: %d",
             input_data->client );
    }

  }
  else
  {
    /* update the client info if validation passes */
    as_div.client[cl].req_mode = input_data->mode;
    as_div.client[cl].ant_switch_cb = input_data->ant_switch_cb;
    as_div.client[cl].set_mode_cfg_cb = input_data->set_mode_cb;
    as_div.client[cl].set_mode_cb = NULL;
    as_div.client[cl].ntfy_switch_change = input_data->inform_switch_change;
    as_div.client[cl].tag = input_data->tag;
  }

  return info_valid;

}

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SET_CONFIG

DESCRIPTION
  Sets the tx diversity switch configuration to given configuration.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
trm_set_ant_switch_return_type trm_ant_switch_div_set_config(
   trm_ant_switch_set_input_type new_setting
   )
{
  trm_set_ant_switch_return_type 	return_val;

  if (!ASD_IS_FEATURE_SUPPORTED)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDiv: Feature not supported" );
    return TRM_ANT_SWITCH_DIV_SET_FAIL_FEAT_DISABLED;
  }

  if ( !TRM_ASDIV_IS_TECH_VALID(new_setting.client) ||
       new_setting.config >= TRM_ANT_SWITCH_DIV_CONFIG_INVALID )
  {
     MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "TRM_ASDiv: Set Config Invalid input Cl: %d, cfg: %d",
            new_setting.client, new_setting.config );
     return TRM_ANT_SWITCH_DIV_SET_FAIL_INVALID_INPUT;
  }

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDiv: Set Config: Cl: %d, cfg: %d",
         new_setting.client, new_setting.config );

  /* convert 'default' config to init config */
  if (new_setting.config  ==  TRM_ANT_SWITCH_DIV_CONFIG_DEFAULT)
  {
    new_setting.config = as_div.init_setting.configuration;
  }

  /* Always run the new algorithm irrespective of
     standalone, DSDS or DSDA */
  if (TRUE)
  {

    trm_asdiv_update_pref_config(new_setting.client, 
                                 new_setting.config);

    if (trm_asdiv_chk_switch_perm(new_setting.client))
    {
      return_val = TRM_ANT_SWITCH_DIV_SET_SUCCESS;

      /* check if it is dual tx scenario and inform the passive client
         about the switch initiation*/
      trm_asdiv_report_switch_initiation( new_setting.client,
                                          new_setting.config );
    }
    else
    {
      return_val = TRM_ANT_SWITCH_DIV_SET_CFG_DENIED;
    }
  }

  /* deny if switch is in transition */
  else if (as_div.curr_setting.configuration == SWITCH_IN_TRANSITION)
  {
    return_val = TRM_ANT_SWITCH_DIV_SET_FAIL_IN_TRANSITION;
  }
  /* In S G\X active case, G can switch whenever it wants and other techs
      can switch when allowed. G first sets the privilege mode before any
      switching request */

  else if (CHECK_BIT_POS(as_div.stat_flag, BIT_SGLTE_ACTIVE))
  {
    //Changes required here for SGLTE new requirement
    return_val = TRM_ANT_SWITCH_DIV_SET_SUCCESS;
    SET_BIT_POS(as_div.stat_flag, BIT_TX_BLANKING_NEEDED);

  }

  /* deny request if it is simultaneous mode and switching is not
      allowed in this mode */
  else if (as_div.aggregate_client_mode == ASDIV_MODE_TX_SIMUL &&
           !CHECK_BIT_POS(as_div.config_reg, BIT_ALLOW_SWITCHING_IN_SV))
  {
    return_val = TRM_ANT_SWITCH_DIV_SET_FAIL_IN_SV_TRANSMIT;
  }
  /* deny request if enforced RX mode is active */
  else if (as_div.aggregate_client_mode == ASDIV_MODE_ENF_ONLY ||
           as_div.aggregate_client_mode == ASDIV_MODE_ENF_TX)
  {
    return_val = TRM_ANT_SWITCH_DIV_SET_FAIL_IN_ENFORCED_DEFAULT;
  }
  /* if current client mode value is not switching mode and the setting
      requires the client to be in switching mode then deny */
  else if (as_div.aggregate_client_mode == ASDIV_MODE_TX_NONE &&
           CHECK_BIT_POS(as_div.config_reg, BIT_SWITCH_BY_TX_CL_ONLY))
  {
    return_val = TRM_ANT_SWITCH_SET_FAIL_CURR_MODE;
  }
  /* if no denying case satisfies, grant the request */
  else
  {
    return_val = TRM_ANT_SWITCH_DIV_SET_SUCCESS;
  }

  /* if switching is being granted change the current status to reflect
      the switch in transition status */
  if (return_val == TRM_ANT_SWITCH_DIV_SET_SUCCESS)
  {
    /* save the current config before switching, change current status to
     in transition and assign next_config with requested config */
    as_div.prev_config  = as_div.curr_setting.configuration;
    as_div.curr_setting.configuration = SWITCH_IN_TRANSITION;
    as_div.next_config	= new_setting.config;
    
    /* change requesting client requested config attribute */
    as_div.client[new_setting.client].req_config = new_setting.config;

    /* update switching tech and requesting tech */
    as_div.switching_tech = new_setting.client;
    as_div.requesting_tech = new_setting.client;


    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TRM_ASDiv: Set Config Success: Cl: %d, cfg: %d",
           new_setting.client, new_setting.config );
  }

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

  return return_val;
}

/*============================================================================

FUNCTION TRM_ASD_SET_MODE_AND_CONFIG

DESCRIPTION
  L1s update the mode which they are entering: SLEEP, RX or TX. Depending on
  the mode, set mode logic could initiate call-back to other techs.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_asd_set_mode_and_config(
   const trm_set_mode_and_config_input_t 	*input_data
   )
{
  trm_client_enum_t                   cl, tx_cl;
  trm_asd_set_mode_and_config_type    cb_info;
  trm_ant_switch_cb_info_type         sw_cb_info;

  /* check for null pointer */
  if (input_data == NULL)
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV: set_mod_cfg: NULL pointer" );
    return;
  }

  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  /* validate info */
  if (!trm_asdiv_validate_client_info(input_data))
  {
    ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);
    return;
  }

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDIV: set mode config; Cl: %d, mode:%d config:%d",
         input_data->client, input_data->mode, input_data->config);
  /* validation successful */
  cl = input_data->client;
  cb_info.set_config_grant = TRM_ANT_SWITCH_DIV_SET_SUCCESS;
  cb_info.set_mode_grant = TRM_SET_MODE_GRANTED;
  cb_info.config = as_div.curr_setting.configuration;
  cb_info.tag = input_data->tag;

  /* update the preferred config of the client */
  trm_asdiv_update_pref_config(cl, input_data->config);

  /* update the active list */
  trm_asdiv_update_actv_list_tech_info(input_data->client);

  /* update the priority mask in the list */
  trm_asdiv_update_actv_list_prio_info();

  /* check if switching is required*/
  if( as_div.curr_setting.configuration == as_div.client[cl].pref_config &&
      !as_div.idl_idl_col_data.limit_reached )
  {

     /* set_config grant is always DENIED for sleep unless passive switching
        is involved*/
    if (input_data->mode == TRM_CLIENT_MODE_SLEEP)
    {
      cb_info.set_config_grant = TRM_ANT_SWITCH_DIV_SET_CFG_DENIED;
    }

  }
  /* check if switchin is allowed */
  else if (trm_asdiv_chk_switch_perm(cl))
  {
    /* check if mode is sleep */
    if (input_data->mode == TRM_CLIENT_MODE_IDLE_WAKEUP)
    {
      /* client can be given its preferred antenna */
      cb_info.config = as_div.client[cl].pref_config;

      //populate the config with clients preferred config
      if (as_div.idl_idl_col_data.limit_reached == TRUE)
      {
        /* idle/idle colision limit had reached,
           so update the config with denied client's pref config*/
        cb_info.config = as_div.idl_idl_col_data.denied_config;

        /* reset idle collision data */
        as_div.idl_idl_col_data.limit_reached = FALSE;
        as_div.idl_idl_col_data.denied_cl = TRM_NO_CLIENT;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "TRM_ASDIV: denied cl:%d's cfg:%d being granted due to denial limit",
               as_div.idl_idl_col_data.denied_cl, cb_info.config );

      }
      else if (as_div.idl_idl_col_data.denied_cl == cl)
      {
        /* current colision data contains this client,
           since it is granted its pref config, reset col data */
        as_div.idl_idl_col_data.denied_cl = TRM_NO_CLIENT;
      }

      /* send the switch event */
      if ( cb_info.config != as_div.curr_setting.configuration) 
      {

         as_div.event_info.client_id = (uint8) cl;
         as_div.event_info.old_switch_config = (uint8) as_div.curr_setting.configuration;
         as_div.event_info.new_switch_config = (uint8) cb_info.config;
         as_div.event_info.event_type = TRM_ASDIV_EVENT_SWITCHING_IDLEWAKEUP;

         event_report_payload((event_id_enum_type) EVENT_MCS_TRM_ASDIV,
                              sizeof(trm_asdiv_event_info_type),
                              &as_div.event_info );

      }

      /* update current switch configuration */
      as_div.curr_setting.configuration = cb_info.config;

    }
    else if (input_data->mode == TRM_CLIENT_MODE_SLEEP)
    {
      cb_info.set_config_grant = TRM_ANT_SWITCH_DIV_SET_CFG_DENIED;

      if ( as_div.client[cl].pref_config != as_div.curr_setting.configuration &&
           as_div.is_standalone_mode != TRUE )
      {
        /* get tech in tx and request for switching */
        tx_cl = trm_client_transmitting(cl);
        if ( TRM_ASDIV_IS_TECH_VALID(tx_cl) &&
            as_div.client[tx_cl].ant_switch_cb != NULL)
        {
          /* update switching and requesting tech */
          as_div.switching_tech = tx_cl;
          as_div.requesting_tech = cl;

          sw_cb_info.action = TRM_ANT_SWITCH_INITIATE_SET_CONFIG;
          sw_cb_info.config = as_div.client[cl].pref_config;
          sw_cb_info.feature_override = TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET;
          sw_cb_info.client = tx_cl;

          /* move switch to transition */
          as_div.prev_config  = as_div.curr_setting.configuration;
          as_div.curr_setting.configuration = SWITCH_IN_TRANSITION;
          as_div.next_config	= sw_cb_info.config;

          as_div.client[tx_cl].req_config = as_div.next_config;

          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "TRM_ASDIV: Passive switch req to Cl: %d, for cl:%d",
                 tx_cl, input_data->client );

          as_div.client[tx_cl].ant_switch_cb(sw_cb_info);

          cb_info.set_config_grant = TRM_ANT_SWITCH_SET_CFG_BY_TX_TECH;

        }
        else
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "TRM_ASDIV: set_mod_cfg: NULL cb cl:%d", tx_cl);
        }
      }
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "TRM_ASDIV: set_mod_cfg: Unexpected mode cl:%d mode:%d",
             cl, input_data->mode );
    }
  }
  else
  {
    /* Switching is not allowed, so give current config */
    cb_info.config = as_div.curr_setting.configuration;

    /* check if idle/idle collision and update collision data*/
    if (as_div.idl_idl_col_data.is_idl_idl_col == TRUE)
    {
      as_div.idl_idl_col_data.is_idl_idl_col = FALSE;
      trm_asdiv_update_idle_col_data(cl);
      /* Reset col data if the denied client's preferred
         config is being given */
      if (as_div.client[cl].pref_config == cb_info.config)
      {
        as_div.idl_idl_col_data.denied_cl = TRM_NO_CLIENT;
      }
    }

    /* set_config is always SUCCESS for idle_wakeup */
    if (input_data->mode == TRM_CLIENT_MODE_SLEEP)
    {
      cb_info.set_config_grant = TRM_ANT_SWITCH_DIV_SET_CFG_DENIED;
    }
    /* change the switch config to current config*/

  }

  /* update tech's current mode */
  as_div.client[cl].curr_mode = as_div.client[cl].req_mode;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDIV: set mode cfg; Cl: %d, pref_cfg:%d cfg_givn:%d",
         input_data->client, as_div.client[cl].pref_config, cb_info.config);

  /* call Grant call back */
  as_div.client[cl].set_mode_cfg_cb(cb_info);

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

}


/*============================================================================

FUNCTION TRM_ASDIV_REQUEST_SWITCH_STATE_UPDATE

DESCRIPTION
  L1s can request for switch state update. Currently supported switch states:
    LOCK STATE: if successful, TRM denies following switching requests
                irrespective of the priority till it is updated back to
                UNLOCK STATE
 
    UNLOCK STATE: switching decisions are now based on priority and modes
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/


trm_asdiv_switch_state_update_return_info_t trm_asdiv_request_switch_state_update
(
   trm_asdiv_switch_state_update_request_info_t* update_info
)
{
  trm_asdiv_switch_state_update_return_info_t ret_info;

  trm_client_enum_t     client_id;
  trm_asdiv_switch_state_update_reason_t  reason;

  /* check for the NULL pointer */
  if( update_info == NULL )
  {
    ret_info.update_result = TRM_ASDIV_SWITCH_STATE_UPDATE_FAILED;
    ret_info.fail_reason = TRM_ASDIV_STATE_UPDATE_FAIL_NONE;
    return ret_info;
  }
  
  ASDIV_ENTER_CRIT_SECT(as_div.crit_sect);

  do
  {

    client_id = update_info->client;
    reason = update_info->update_reason;

    ret_info.update_result = TRM_ASDIV_SWITCH_STATE_UPDATE_FAILED;
    
    /* check if ASDiv feature is supported */
    if (!ASD_IS_FEATURE_SUPPORTED)
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TRM_ASDIV: Feature not supported!" );
      ret_info.fail_reason = TRM_ASDIV_STATE_UPDATE_FAIL_FEAT_NOT_SUPPORTED;
      break;

    }

    /* check for the valid info */
    if( !TRM_ASDIV_IS_TECH_VALID(update_info->client) ||
        update_info->request_type >= TRM_ASDIV_UPDATE_REQUEST_INVALID ||
        update_info->update_reason >= TRM_ASDIV_STATE_UPDATE_REASON_MAX )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "TRM_ASDIV: updt sw state invalid info; client: %d, req:%d reason:%d",
             update_info->client, update_info->request_type, update_info->update_reason);
      ret_info.fail_reason = TRM_ASDIV_STATE_UPDATE_FAIL_INVALID_INFO;
      break;
    }

    /* update return info with success */
    ret_info.update_result = TRM_ASDIV_SWITCH_STATE_UPDATE_SUCCESS;
    ret_info.fail_reason = TRM_ASDIV_STATE_UPDATE_FAIL_NONE;

    switch(update_info->request_type)
    {

    case TRM_ASDIV_SWITCH_LOCK_REQUEST:

      /* if request is to lock the switch, then check if the update
         can be done when switch is in transition.
         if it can be simply update the state
         else update only if switch is not in transition */
      if( update_info->update_if_switch_in_progress == TRUE ||
          ( update_info->update_if_switch_in_progress == FALSE &&
            !ASD_SWITCH_IN_TRANSITION ))
      {
        TRM_SET_BIT_ON(as_div.client[client_id].switch_lock_mask, reason);
        trm_asdiv_update_switch_lock_mask();
      }
      else
      {
        ret_info.update_result = TRM_ASDIV_SWITCH_STATE_UPDATE_FAILED;
        ret_info.fail_reason = TRM_ASDIV_STATE_UPDATE_FAIL_SWITCH_IN_PROGRESS;
      }
      break;


    case TRM_ASDIV_SWITCH_UNLOCK_REQUEST:
      /* for unlock state update simply clear the lock state for the reason */
      TRM_SET_BIT_OFF(as_div.client[client_id].switch_lock_mask, reason);
      trm_asdiv_update_switch_lock_mask();
      break;

    default:
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV: updt sw state unexpected" );
      break;

    }

  }while(0);

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "TRM_ASDIV: update sw state; client: %d, upd_req:%d reason:%d",
         client_id, update_info->request_type, reason);
  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
         "Update_result: %d, fail_reason:%d",
         ret_info.update_result, ret_info.fail_reason);

  ASDIV_EXIT_CRIT_SECT(as_div.crit_sect);

  return ret_info;
}

/*============================================================================

FUNCTION TRM_ASDIV_SET_PREF_CONFIG_TEST

DESCRIPTION
  OFFTARGET API: is used to set the preferred config of a client
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_set_pref_config_test
(
   trm_client_enum_t                cl, 
   trm_ant_switch_div_config_type   cfg
)
{

#ifdef TEST_FRAMEWORK
   #error code not present
#else
  MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV SET_CFG_TEST: unexpected call to offtarget test api" );

#endif
}


/*============================================================================

FUNCTION TRM_ASDIV_SET_SUB_PRIO_DATA_TEST

DESCRIPTION
  OFFTARGET API: is used to set the sub priority info.. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_set_sub_prio_data_test
(
   uint8    state_sub0,
   uint8    state_sub1
)
{

#ifdef TEST_FRAMEWORK
#error code not present
#else
  MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "TRM_ASDIV TEST: unexpected call to offtarget test api" );

#endif
}
