#ifndef TRMLOG_H
#define TRMLOG_H
/*===========================================================================

                   T R M    L O G   H E A D E R    F I L E

DESCRIPTION
   This file contains log types used internally by the TRM.
   It should NOT be included by any source file outside the TRM!

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/trm/src/trmlog.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/03/14   mn      Adding support for connected mode WTR hopping.
05/10/13   mn      Adding support for TSTS.
12/17/12   rj      Logging change to log Set Client Prio.
03/22/12   sr      Logging change to log RF Capability and NV data.
07/26/11   sg      Added TRM V2 logging
10/18/10   ag      Merged DSDS functionality.
07/31/09   ag      Fixed lint warnings 
08/28/08   adw     Added FEATURE_MCS_TRM to featurize TRM for ULC.
06/16/05   ajn     Re-implemented logging as on "ostream" look-a-like.
05/26/05   ajn     Code review comment changes
05/06/05   ajn     Added official LOG_TRANSCEIVER_RESOURCE_MGR_C log code.
02/23/05   ajn     TRMLogClient now takes client id
02/22/05   ajn     Implemented logging structures using inheritance.
02/18/05   ajn     Added more log structures
02/11/05   ajn     Initial revision.

===========================================================================*/

#include "customer.h"

extern "C"
{
  #include "trm.h"
}

#include "trmi.h"

#include <stddef.h>


/*===========================================================================

                              HEADER CONSTANTS

  Numeric values for the identifiers in this section should not be changed.

===========================================================================*/


/*-------------------------------------------------------------------------
  Version IDs 
-------------------------------------------------------------------------*/

#define TRMLOG_VERSION_1              1
#define TRMLOG_VERSION_2              2
#define TRMLOG_VERSION_3              3
#define TRMLOG_VERSION_4              4
#define TRMLOG_VERSION_5              5

/*-------------------------------------------------------------------------
  Packet Type IDs 
-------------------------------------------------------------------------*/

typedef enum
{                 
  TRMLOG_ID_GRANT                     = 0,
  TRMLOG_ID_RESERVE_AT                = 1,
  TRMLOG_ID_REQUEST_N_NOTIFY          = 2,
  TRMLOG_ID_REQUEST                   = 3,
  TRMLOG_ID_CANCEL_REQUEST            = 4,
  TRMLOG_ID_RELEASE                   = 5,
  TRMLOG_ID_RETAIN_LOCK               = 6,
  TRMLOG_ID_CHANGE_PRIORITY           = 7,
  TRMLOG_ID_EXTEND_DURATION           = 8,
  TRMLOG_ID_CHANGE_DURATION           = 9,
  TRMLOG_ID_EXCHANGE                  = 10,
  TRMLOG_ID_UNLOCK_NOTIFY             = 11,
  TRMLOG_ID_REQUEST_N_NOTIFY_ENHANCED = 12,
  TRMLOG_ID_GRANT_ENHANCED            = 13,
  TRMLOG_ID_INIT                      = 14,
  TRMLOG_ID_REQUEST_BAND_TUNE         = 15,
  TRMLOG_ID_CONFIRM_BAND_TUNE         = 16,
  TRMLOG_ID_CANCEL_BAND_TUNE          = 17,
  TRMLOG_ID_BAND_GRANT                = 18,
  TRMLOG_ID_CHANGE_REASON             = 19,
  TRMLOG_ID_SET_CLIENT_PRIO           = 20,
  TRMLOG_ID_RETAIN_LOCK_ADVANCED      = 21,
  TRMLOG_ID_UNLOCK_NOTIFY_ADVANCED    = 22,
  TRMLOG_ID_ASYNC_EVENT_CALLBACK      = 23,
  TRMLOG_ID_ASYNC_RESPONSE            = 24
}
trmlog_id_type;



/*----------------------------------------------------------------------------
  Log Support Data
    This structure is packed so that TRM log does not have to
    account for any padding and to make it ABI independent.
----------------------------------------------------------------------------*/

typedef PACK( struct )
{
  /* SHDR / Hybrid mode indication */
  uint32                     rf_type;

  /* RF chain information          */
  uint32                     rf_mask;

  /* PAM/SVDO/SVLTE information    */
  uint32                     trm_config;

  /* RF Capability */
  uint32                     rf_capability;
 
  /* NV capabilities */
  uint32                    nv_rf_type;
  uint32                    nv_config_mask;
      
  uint32                    drx_cycle;
           
  uint32                    pbr_count;
 
  uint32                    pbr_bias;
           
  uint32                    pbr_priority;
} trm_log_struct_type;



/*============================================================================

Class: TRMLog

Description:
  Allocates a buffer from diag's log pool, using TRM's log code, and
  accumulates data into the buffer.  Automatically commits the log buffer
  when the TRMLog object goes out of scope.

  If logging of TRM packets is not enabled, or if there is no availiable 
  memory for the buffer, logging requests using this object are silently
  discarded.
  
============================================================================*/

class TRMLog
{
  /*------------------------------------------------------------------------
    Private data members
  ------------------------------------------------------------------------*/
  private:

    /* Pointer to log buffer - if one can be allocated */
    uint8 * packet;

    /* Size of the log buffer */
    uint32  size;

    /* Amount of log buffer that has been used */
    uint32  used;


  /*------------------------------------------------------------------------
    Private Member functions
  ------------------------------------------------------------------------*/

    /* Allocate memory from diag's log buffer */
    void allocate( uint32 length );


  /*------------------------------------------------------------------------
    Public Member functions
  ------------------------------------------------------------------------*/
  public:

    /* Constructor.  Begin a log packet with the given log id and version
       This is a Version 1 type log packet */
    TRMLog( trmlog_id_type id, uint8 ver=TRMLOG_VERSION_1, uint32 length=0 );

    /* Constructor.  Begin a log packet with the given log id and version 
       This is a Version 3 type log packet */
    TRMLog( trmlog_id_type id, trm_log_struct_type *trm_cfg_info, 
               uint8 ver = TRMLOG_VERSION_5, uint32 length = 0);

    /* Constructor.  length=0 --> uses default length */
    TRMLog( uint32 length=0 );

    /* Destructor.  Will commit the log packet, if necessary. */
    ~TRMLog();

    /* Commit the log packet */
    void commit();

    /* Allocate memory inside the log packet */
    void *alloc( uint32 length );

    /* Allocate memory inside the log packet, and copy in the given data */
    void log( const void *data, uint32 length );

    /* Set log packet id */
    void update_log_id(trmlog_id_type id);

    /* Set current band */
    void update_current_band (trm_rf_mode_map_type *rf_map);
    
    /* Log the async event callback */
    void log_async_event_cb (trm_async_event_callback_data_type* data);
    
    /* Log the async response*/
    void log_async_response (trm_async_event_response_data_type* data);

    /* Returns "true" if the log packet can accept more data */
    operator int () const { return size > used; }


  /*------------------------------------------------------------------------
    "ostream" style logging functions.
  ------------------------------------------------------------------------*/

    TRMLog& operator << (uint8  u8);
    TRMLog& operator << (uint16 u16);
    TRMLog& operator << (uint32 u32);

    TRMLog& operator << (int8   i8);
    TRMLog& operator << (int16  i16);
    TRMLog& operator << (int32  i32);

    TRMLog& operator << (trm_client_enum_t client_id);
    TRMLog& operator << (const trm_client_state &state);
    TRMLog& operator << (const trm_bc_info_type &bcstate);
    TRMLog& operator << (const trm_client_info_t &info);

  /*------------------------------------------------------------------------
    Additional stream output operators can be declared outside the class,
    via: "TRMLog& operator << (TRMLog& log, sometype& t);"
  ------------------------------------------------------------------------*/
};

#endif /* TRMLOG_H */

