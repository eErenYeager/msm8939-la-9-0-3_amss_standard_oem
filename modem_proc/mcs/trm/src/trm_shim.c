/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

                 Transceiver Resource Manager Shim Source File

GENERAL DESCRIPTION

  This file provides API for the GSM clients to use TRM for accessing RF
  resources in a Dual SIM scenario.


EXTERNALIZED FUNCTIONS

  trm_shim_set_client_priority() - Notifies TRM about the client priority. 
    Must be called each time the client priority is changed.

  trm_shim_get_client_priority() - Retrives the shim client priority.

  trm_shim_reserve_at() - Registers a client's need to access the RF at a 
    specific moment in the future for the indicated duration, such as for a
    paging slot.  trm_shim_request() must be used at the specified moment, to
    actually obtain the RF resource.  Implicitly performs a trm_release(). 
    It looks up the client priority and internally calls trm_reserve_at()
    with appropriate reason as argument.

  trm_shim_request() - Request access to an RF chain immediately, for the 
    indicated duration. Implicitly performs a trm_release(). It looks up 
    the client priority and internally calls trm_request() with appropriate 
    reason as argument. 

  trm_shim_request_and_notify() - Request access to an RF chain, when it 
    becomes available.  Implicitly performs a trm_release(). It looks up 
    the client priority and internally calls trm_request_and_notify() with 
    appropriate reason as argument.

  trm_shim_change_priority() - Specifies a change in the reason (and 
    consequently priority) for a client's hold on an RF resource. It looks 
    up the client priority and internally calls trm_change_priority() with 
    appropriate reason as argument. 

  trm_shim_retain_lock() - Informs TRM that the client wants to hold the 
    resource indefinitely. TRM may inform the client that it must give up 
    the lock through the supplied unlock callback. It looks up the client 
    priority and internally calls trm_retain_lock() with appropriate reason 
    as argument.

  trm_shim_release() - Releases the clients hold on an RF resource.  If no
    client is waiting for the RF resource, the RF will be turned off. It 
    will call corresponding trm_release().

  trm_shim_request_and_notify_enhanced() - Requests and holds on to the
    resource until the next resource collision. TRM will call the grant
    callback when it can satisfy the client request. It will also provide a 
    time value that will indicate the amount of time the client can hold the
    resource without a collision.
    
  trm_shim_get_reason() - Returns nominal last reason with which the client 
    requested lock. If the client is not holding the chain nor has made any 
    request/reservation, then TRM_NUM_REASONS will be returned.  

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  trm_init() must be called before any other function.

  Before using any RF functions, a client must be granted permission via
  trm_shim_request( ) or trm_shim_request_and_notify().

  When the client is finished using the RF, it must release its hold on the
  RF chain either directly via a call to trm_shim_release(), or indirectly by
  calling trm_shim_reserve_at(), trm_shim_request_and_notify(), or 
  trm_shim_request().


  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=



==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/trm/src/trm_shim.c#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     --------------------------------------------------------- 
09/20/2012   mn      NikeL DSDS merge
01/10/2012   ag      TRM Feature Cleanup  
12/21/2010   hh      Added new api to get shim client priority. Add Q6 INTLOCK
                     changes.
07/26/2010   hh      Added new inv and inv hi reasons.
09/28/2009   ag      Fixed lint errors.
09/18/2009   ag      Fixed some compiler warnings. 
09/18/2009   ag      Added err.h and msg.h
05/26/2009   ag      Initial version.

============================================================================*/



/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "trm_shim.h"
#include "err.h"
#include "msg.h"


/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

/* Returns the client sitting on the other stack */
#define OPPOSITE_STACK_CLIENT(client_id) \
	(((client_id)==TRM_GSM1) ? TRM_GSM2 : TRM_GSM1)

/* Returns the opposite polarity */
#define OPPOSITE_POLARITY(prio) \
	(((prio)==TRM_PRIO_HI) ? TRM_PRIO_LO : TRM_PRIO_HI)

/*----------------------------------------------------------------------------
  TRM SHIM Clients
----------------------------------------------------------------------------*/

/* Enum that identifies all TRM SHIM clients */
typedef enum trm_shim_client_enum_t
{
  TRM_SHIM_GSM1,
  TRM_SHIM_GSM2,

  TRM_SHIM_MAX_CLIENTS

} trm_shim_client_type;


static boolean trm_shim_invalid_reason
(
  trm_reason_enum_t       reason
);

static int trm_shim_get_index 
(
  trm_client_enum_t       client
);

static void trm_shim_switch_reason_hi_lo
(
  trm_reason_enum_t                  *reason,

  trm_shim_client_priority_enum_t    hi_lo
);

/* Structure to hold the trm_shim lock and client status */
typedef struct trm_shim_struct
{
  rex_crit_sect_type crit_sect;
  /* Array to hold the current priority of the TRM SHIM clients */
  trm_shim_client_priority_type trm_shim_cli_prio_array[TRM_SHIM_MAX_CLIENTS];  
}trm_shim_struct_type;

/* Variable to hold the trm shim client status */
trm_shim_struct_type trm_shim;

/* MACROS for using rex based critical sections on Q6 platforms */

/* Critical section macros */
#define TRM_SHIM_INIT_CRIT_SECT(lock)    rex_init_crit_sect(&(lock))
#define TRM_SHIM_ENTER_CRIT_SECT(lock)   rex_enter_crit_sect(&(lock))
#define TRM_SHIM_LEAVE_CRIT_SECT(lock)   rex_leave_crit_sect(&(lock))

/*============================================================================

                            FUNCTION DEFINITIONS

============================================================================*/
/*============================================================================

FUNCTION TRM_SHIM_INIT

DESCRIPTION
  Initializes the priority for all TRM Shim clients to TRM_PRIO_UNDEFINED.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_shim_init( void )
{
  int i;

  /* Set the client id */
  trm_shim.trm_shim_cli_prio_array[0].client_id = TRM_GSM1;
  trm_shim.trm_shim_cli_prio_array[1].client_id = TRM_GSM2;

  /* Set the priority to Undefined */
  for (i=0; i<(int)TRM_SHIM_MAX_CLIENTS; i++)
  {
    trm_shim.trm_shim_cli_prio_array[i].client_prio = TRM_PRIO_UNDEFINED;
  }

  /* Initialize the shim critical section */
  TRM_SHIM_INIT_CRIT_SECT(trm_shim.crit_sect);

  return ;
}

/*============================================================================

FUNCTION TRM_SHIM_INVALID_REASON

DESCRIPTION
  Checks whether the reason specified is one of the HI reasons.
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE if the reason is one of the HI ones.
  FALSE otherwise.

SIDE EFFECTS
  None

============================================================================*/

static boolean trm_shim_invalid_reason
(
  trm_reason_enum_t reason
)
{
  /* Return value */
  boolean ret;

  /* Check if the reason is any of the high ones. */
  switch (reason)
  {
    case TRM_DEMOD_PAGE_HI:
    case TRM_DEMOD_BROADCAST_HI:
    case TRM_ACQUISITION_HI:
    case TRM_DEMOD_PAGE_MAX_SENS_HI:
    case TRM_RESELECT_HI:
    case TRM_CHANNEL_MAINTENANCE_HI:
    case TRM_DEMOD_CBCH_HI:
    case TRM_ACQUISITION_INV_HI:
    case TRM_SMALL_SCI_PAGE_HI:
    case TRM_DEMOD_PAGE_INV_HI:
    case TRM_DEMOD_PAGE_CONTINUATION_HI:
      /* True - Reason is Invalid */
      ret = TRUE;
      break;
    default:
      /* False - Reason is not invalid */
      ret = FALSE;
      break;
  }

  return ret;

} /* trm_shim_invalid_reason */

/*============================================================================

FUNCTION TRM_SHIM_GET_INDEX

DESCRIPTION
  Returns the index into the internal trm shim client priority array.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None 

============================================================================*/
static int trm_shim_get_index 
(
  trm_client_enum_t       client
)
{
  /* Index value to be returned */
  int index = -1;

  switch (client)
  {
    case TRM_GSM1: 
    {
      index = 0;
      break;
    }
    case TRM_GSM2: 
    {
      index = 1;
      break;
    }
    default:
    {
      ERR_FATAL("Client %d is not a TRM SHIM client. Invalid clientid.",(int)client,0,0);
    }
  }

  return index;

} /* trm_shim_get_index */

/*============================================================================

FUNCTION TRM_SHIM_SWITCH_REASON_HI_LO

DESCRIPTION
  Changes the reason to the corresponding HI reason if the hi_lo is TRM_PRIO_HI
  else changes it to LO if hi_lo is TRM_PRIO_LO.
  This is an internal function, called when the TRM shim clients change their 
  client priority and whenever the higher priority stack makes TRM requests.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
static void trm_shim_switch_reason_hi_lo
(
  trm_reason_enum_t                  *reason,

  trm_shim_client_priority_enum_t    hi_lo
)
{

  if (hi_lo == TRM_PRIO_HI)
  {
    /* Change reason to HI */
    switch (*reason)
    {
      case TRM_DEMOD_PAGE: 
        *reason = TRM_DEMOD_PAGE_HI;
        break;
  
      case TRM_DEMOD_BROADCAST:
        *reason = TRM_DEMOD_BROADCAST_HI;
        break;

      case TRM_ACQUISITION:
        *reason = TRM_ACQUISITION_HI;
        break;

      case TRM_DEMOD_PAGE_MAX_SENS:
        *reason = TRM_DEMOD_PAGE_MAX_SENS_HI;
        break;

      case TRM_RESELECT:
        *reason = TRM_RESELECT_HI;
        break;

      case TRM_CHANNEL_MAINTENANCE:
        *reason = TRM_CHANNEL_MAINTENANCE_HI;
        break;

      case TRM_DEMOD_CBCH:
        *reason = TRM_DEMOD_CBCH_HI;
        break;
        
      case TRM_ACQUISITION_INV:
        *reason = TRM_ACQUISITION_INV_HI;
        break;
        
      case TRM_SMALL_SCI_PAGE:
        *reason = TRM_SMALL_SCI_PAGE_HI;
        break;

      case TRM_DEMOD_PAGE_INV:
        *reason = TRM_DEMOD_PAGE_INV_HI;
        break;

      case TRM_DEMOD_PAGE_CONTINUATION:
        *reason = TRM_DEMOD_PAGE_CONTINUATION_HI;
        break;

      default:
        break;
    }
  }
  else if (hi_lo == TRM_PRIO_LO)
  {
    /* Change reason to normal/low i.e not HI*/
    switch (*reason)
    {
      case TRM_DEMOD_PAGE_HI: 
        *reason = TRM_DEMOD_PAGE;
        break;
  
      case TRM_DEMOD_BROADCAST_HI:
        *reason = TRM_DEMOD_BROADCAST;
        break;

      case TRM_ACQUISITION_HI:
        *reason = TRM_ACQUISITION;
        break;

      case TRM_DEMOD_PAGE_MAX_SENS_HI:
        *reason = TRM_DEMOD_PAGE_MAX_SENS;
        break;

      case TRM_RESELECT_HI:
        *reason = TRM_RESELECT;
        break;

      case TRM_CHANNEL_MAINTENANCE_HI:
        *reason = TRM_CHANNEL_MAINTENANCE;
        break;

      case TRM_DEMOD_CBCH_HI:
        *reason = TRM_DEMOD_CBCH;
        break;
        
      case TRM_ACQUISITION_INV_HI:
        *reason = TRM_ACQUISITION_INV;
        break;

      case TRM_SMALL_SCI_PAGE_HI:
        *reason = TRM_SMALL_SCI_PAGE;
        break;
        
      case TRM_DEMOD_PAGE_INV_HI:
        *reason = TRM_DEMOD_PAGE_INV;
        break;

      case TRM_DEMOD_PAGE_CONTINUATION_HI:
        *reason = TRM_DEMOD_PAGE_CONTINUATION;
        break;

      default:
        break;
    }

  }

} /* trm_shim_switch_reason_hi_lo */

/*============================================================================

FUNCTION TRM_SHIM_SWITCH_CLIENT_STATE

DESCRIPTION
  Checks if the client priority (Shim priority) needs to be changed. 
  If yes, then changes it also updates the reason in the internal TRM
  structures.
  Internal function to trm_shim_set_client_priority() and called within an 
  INTLOCK context.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_switch_client_state
(
  /* Structure variable specifying client_id, priority of the client */
  trm_shim_client_priority_type        id_prio
)
{
  /* Index into the global client priority array */
  uint32              index = 0;

  /* Current reason with which the client is holding the lock */
  trm_reason_enum_t   curr_reason;

  /* new reason to replace the current reason. */
  trm_reason_enum_t  new_reason; 
 
  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(id_prio.client_id);

  /* If the priority is same as in the array,  we don't need to do anything,
     just print a message in log. */
  if (trm_shim.trm_shim_cli_prio_array[index].client_prio != id_prio.client_prio)
  {
    /* Update the array with new priority */
    trm_shim.trm_shim_cli_prio_array[index].client_prio = id_prio.client_prio;
    
    /* Get the reason */
    curr_reason = trm_get_reason(id_prio.client_id);

    new_reason = curr_reason;

    /* Switch the reason to id_prio.client_prio */
    trm_shim_switch_reason_hi_lo(&new_reason, id_prio.client_prio);

    /* If both reasons are same, that means there is no corresponding HI 
       so no need to call change_priority */
    if (curr_reason != new_reason)
    {
      /* Change priority */
      trm_change_priority(id_prio.client_id, new_reason);
    }
    MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
          "TRM SHIM::Client %d priority changed to %d",
          id_prio.client_id, id_prio.client_prio);
  }
  else
  {
    MSG_1(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
          "TRM SHIM::No change in priority. Current Priority - %d",
          (int)id_prio.client_prio);
  }
}




/*============================================================================

FUNCTION TRM_SHIM_SET_CLIENT_PRIORITY

DESCRIPTION
  Sets the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  The client priority will be changed after this function call. Any subsequent
  trm requests and reservations will be made with the new priority.  

============================================================================*/

void trm_shim_set_client_priority
(

  /* Structure variable specifying client_id, priority of the client */
  trm_shim_client_priority_type        id_prio

)
{
  trm_shim_client_priority_type        opp_stack_id_prio;

  /* Check whether the client priority is correct */
  if (id_prio.client_prio >= TRM_PRIO_UNDEFINED)
  {
    ERR_FATAL("TRM SHIM::Incorrect Client Priority: %d", 
              (int)id_prio.client_prio, 0, 0);
  }
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_set_client_priority: client - %d, priority - %d",
        id_prio.client_id, id_prio.client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  /* Switch the requested client's state */
  trm_switch_client_state(id_prio);

  /* Get the opposite index and polarity for client on the opposite stack. */
  opp_stack_id_prio.client_id   = OPPOSITE_STACK_CLIENT(id_prio.client_id);
  opp_stack_id_prio.client_prio = OPPOSITE_POLARITY(id_prio.client_prio);

  /* Switch the other client's state */
  trm_switch_client_state(opp_stack_id_prio);

  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect);
  
} /* trm_shim_set_client_priority */

/*============================================================================

FUNCTION TRM_SHIM_GET_CLIENT_PRIORITY

DESCRIPTION
  Gets the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  TRM shim client priority.

SIDE EFFECTS
 None.  

============================================================================*/

trm_shim_client_priority_enum_t trm_shim_get_client_priority
(
  /* client_id */
  trm_client_enum_t                client_id
)
{
  /* Index into the global client priority array */
  uint32              index = 0;
  trm_shim_client_priority_enum_t client_prio;
  
  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);
  
  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  /* Get the corresponding priority */
  client_prio = trm_shim.trm_shim_cli_prio_array[index].client_prio;
  
  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 
  
  return client_prio;
}

/*============================================================================

FUNCTION TRM_SHIM_RESERVE_AT

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason. This function calls
  the corresponding trm_reserve_at() after checking the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_shim_reserve_at 
(

  /* The client which needs the RF resource */
  trm_client_enum_t               client_id,

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason

)
{
  /* Index into the global client priority array */
  uint32 index = 0;

  /* Error Value set in case of undefined priority */
  int32 err_val = -1;
  
  /* Ensure the reason specified is not one of the HI ones. */
  if (trm_shim_invalid_reason(reason) == TRUE)
  {
    ERR_FATAL("TRM SHIM::Invalid reason specified - %d",
              (int)reason,0,0);
  }

  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_reserve_at: client - %d, priority - %d",
        client_id, trm_shim.trm_shim_cli_prio_array[index].client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  switch (trm_shim.trm_shim_cli_prio_array[index].client_prio)
  {
    case TRM_PRIO_HI:
    {
      /* For high priority client, use HI reason*/
      trm_shim_switch_reason_hi_lo(&reason, TRM_PRIO_HI);

      /* Call trm_reserve_at */
      trm_reserve_at (client_id, TRM_RXTX_BEST, when, duration, reason);

      err_val = 0;
      break;
    }    
    case TRM_PRIO_LO:
    {
      /* Call trm_reserve_at */
      trm_reserve_at (client_id, TRM_RXTX_BEST, when, duration, reason);

      err_val = 0;
      break;
    }
    default:
      break;
  }
  
  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 

  if (err_val < 0)
  {
    ERR_FATAL("TRM SHIM:Client Priority is invalid", 0, 0, 0);
  }

} /* trm_shim_reserve_at */


/*============================================================================

FUNCTION TRM_SHIM_REQUEST_AND_NOTIFY

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for the
  given duration, for the supplied reason.

  When the resource can be granted to the client, the event callback for the
  client will be called with the result of the lock request.

  This function implicitly calls trm_reserve_and_notify after identifying the
  client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_shim_request_and_notify
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_t            grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag

)
{
  /* Index into the global client priority array */
  uint32 index = 0;

  /* Error Value set in case of undefined priority */
  int32 err_val = -1;
  
  /* Ensure the reason specified is not one of the HI ones. */
  if (trm_shim_invalid_reason(reason) == TRUE)
  {
    ERR_FATAL("TRM SHIM::Invalid reason specified - %d",
              (int)reason,0,0);
  }

  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_request_and_notify: client - %d, priority - %d",
        client_id, trm_shim.trm_shim_cli_prio_array[index].client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  switch (trm_shim.trm_shim_cli_prio_array[index].client_prio)
  {
    case TRM_PRIO_HI:
    {
      /* For high priority client, use HI reason*/
      trm_shim_switch_reason_hi_lo(&reason, TRM_PRIO_HI);

      /* Call trm_request_and_notify */
      trm_request_and_notify (client_id, TRM_RXTX_BEST, duration, 
                              reason, grant_callback, tag);

      err_val = 0;
      break;
    }    
    case TRM_PRIO_LO:
    {
      /* Call trm_request_and_notify */
      trm_request_and_notify (client_id, TRM_RXTX_BEST, duration, 
                              reason, grant_callback, tag);

      err_val = 0;
      break;
    }
    default:
      break;
  }
  
  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 

  if (err_val < 0)
  {
    ERR_FATAL("TRM SHIM:Client Priority is invalid", 0, 0, 0);
  }

} /* trm_shim_request_and_notify */

/*============================================================================

FUNCTION TRM_SHIM_REQUEST

DESCRIPTION
  Specifies the given client needs the given transciever resource, for the
  given duration, for the supplied reason.

  The resource request is immediately evaluated, and the result returned.

  This function implicitly calls trm_request() after the client priority is
  correctly identified and the new reason is determined.

  This may be used in conjunction with trm_shim_reserve_at().
  
DEPENDENCIES
  None

RETURN VALUE
  TRM_DENIED          - the client was denied the transceiver resources.
  TRM_GRANTED_CHAIN0  - the client can now use the primary RF chain.
  
SIDE EFFECTS
  If the client currently holds an RF chain, that chain is released before
  the request is evaluated.

============================================================================*/

trm_grant_event_enum_t trm_shim_request
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason

)
{
  /* Index into the global client priority array */
  uint32 index = 0;

  /* Error Value set in case of undefined priority */
  int32 err_val = -1;

  /* Return value */
  trm_grant_event_enum_t ret = TRM_DENIED;
  
  /* Ensure the reason specified is not one of the HI ones. */
  if (trm_shim_invalid_reason(reason) == TRUE)
  {
    ERR_FATAL("TRM SHIM::Invalid reason specified - %d",
              (int)reason,0,0);
  }

  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_request: client - %d, priority - %d",
        client_id, trm_shim.trm_shim_cli_prio_array[index].client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  switch (trm_shim.trm_shim_cli_prio_array[index].client_prio)
  {
    case TRM_PRIO_HI:
    {
      /* For high priority client, use HI reason*/
      trm_shim_switch_reason_hi_lo(&reason, TRM_PRIO_HI);

      /* Call trm_request */
      ret = trm_request(client_id, TRM_RXTX_BEST, duration, reason);

      err_val = 0;
      break;
    }    
    case TRM_PRIO_LO:
    {
      /* Call trm_request */
      ret = trm_request(client_id, TRM_RXTX_BEST, duration, reason);

      err_val = 0;
      break;
    }
    default:
      break;
  }
  
  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 

  if (err_val < 0)
  {
    ERR_FATAL("TRM SHIM:Client Priority is invalid", 0, 0, 0);
  }

  return ret;

} /* trm_shim_request() */


/*============================================================================

FUNCTION TRM_SHIM_CHANGE_PRIORITY

DESCRIPTION
  When a client changes what it is doing, it should change the advertised
  reason for holding the transceiver resource, so its priority will change.

  Eg) A client request the transceiver resource for listening for a PAGE. If
  it receives one, it would change its priority to PAGE_RESPONSE and attempt
  to respond to the page, and eventually change its priority to TRAFFIC.

  This function implicitly calls trm_change_priority() after evaluating the
  new reason by looking at client priority.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_shim_change_priority
(

  /* The client whose priority is to be changed */
  trm_client_enum_t               client_id,

  /* The new reason why the RF lock is held (used for priority decisions) */
  trm_reason_enum_t               reason

)
{

  /* Index into the global client priority array */
  uint32 index = 0;

  /* Error Value set in case of undefined priority */
  int32 err_val = -1;
  
  /* Ensure the reason specified is not one of the HI ones. */
  if (trm_shim_invalid_reason(reason) == TRUE)
  {
    ERR_FATAL("TRM SHIM::Invalid reason specified - %d",
              (int)reason,0,0);
  }

  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_change_priority: client - %d, priority - %d",
        client_id, trm_shim.trm_shim_cli_prio_array[index].client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  switch (trm_shim.trm_shim_cli_prio_array[index].client_prio)
  {
    case TRM_PRIO_HI:
    {
      /* For high priority client, use HI reason*/
      trm_shim_switch_reason_hi_lo(&reason, TRM_PRIO_HI);

      /* Call trm_change_priority */
      trm_change_priority(client_id, reason);

      /* Set err_val to 0 */
      err_val = 0;
      break;
    }    
    case TRM_PRIO_LO:
    {
      /* Call trm_change_priority */
      trm_change_priority(client_id, reason);
    
      /* Set err_val to 0 */
      err_val = 0;
      break;
    }
    default:
      break;
  }
  
  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 

  if (err_val < 0)
  {
    ERR_FATAL("TRM SHIM:Client Priority is invalid", 0, 0, 0);
  }

} /* trm_shim_change_priority */


/*============================================================================

FUNCTION TRM_SHIM_RETAIN_LOCK

DESCRIPTION
  Informs the Transceiver Resource Manager that the client wants to hold
  the resource indefinitely.  The TRM may inform the client that it must
  give up the lock through the supplied unlock callback.
  
  This function implicitly calls trm_retain_lock() after evaluating the new
  reason by looking at client priority.

DEPENDENCIES
  The client must be holding a transceiver resource lock.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_shim_retain_lock
(

  /* The client which is attempting to extend the lock duration */
  trm_client_enum_t               client_id,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback

)
{
  /* Obtain the index from the argument client id.
     Just to check if the client is a TRM shim client. */
  (void)trm_shim_get_index(client_id);

  /* Call trm_retain_lock */
  trm_retain_lock (client_id, unlock_callback);

} /* trm_shim_retain_lock */


/*============================================================================

FUNCTION TRM_SHIM_RELEASE

DESCRIPTION
  Release the transceiver resource currently held by a client.
  Calls the corresponding trm_release().
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If no client is waiting for the resource, the RF chain will be turned off.

============================================================================*/

void trm_shim_release
(

  /* The client which is releasing the resource */
  trm_client_enum_t               client

)
{
  /* Obtain the index from the argument client id.
     Just to check if the client is a TRM shim client */
  (void)trm_shim_get_index(client);

  /* Call trm_release */
  trm_release (client);  

} /* trm_shim_release */

/*============================================================================

FUNCTION TRM_SHIM_REQUEST_AND_NOTIFY_ENHANCED

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for a 
  minimum of the given duration, for the supplied reason. At the time of the
  grant, the duration is extended to the maximium possible time that the lock
  can be made available to the client without colliding with any other client.

  This function implicitly calls trm_request_and_notify_enhanced() after
  evaluating the new reason by looking at client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.
  Also the time value returned via a grant_enh_callback is subject to change
  depending on the nature and timing of other requests from other clients
  after this function call.

============================================================================*/

void trm_shim_request_and_notify_enhanced
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* Minimum duration that the client requests the resource for.(sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted and till when the
   * client can hold on to the resource. */
  trm_grant_enh_callback_t        grant_enh_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback

)
{

  /* Index into the global client priority array */
  uint32 index = 0;

  /* Error Value set in case of undefined priority */
  int32 err_val = -1;

  /* Ensure the reason specified is not one of the HI ones. */
  if (trm_shim_invalid_reason(reason) == TRUE)
  {
    ERR_FATAL("TRM SHIM::Invalid reason specified - %d",
              (int)reason,0,0);
  }

  /* Obtain the index from the argument client id. */
  index = trm_shim_get_index(client_id);
  MSG_2(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
        "TRM SHIM::trm_shim_request_and_notify_enhanced: client - %d, priority - %d",
        client_id, trm_shim.trm_shim_cli_prio_array[index].client_prio);

  TRM_SHIM_ENTER_CRIT_SECT(trm_shim.crit_sect);

  switch (trm_shim.trm_shim_cli_prio_array[index].client_prio)
  {
    case TRM_PRIO_HI:
    {
      /* For high priority client, use HI reason*/
      trm_shim_switch_reason_hi_lo(&reason, TRM_PRIO_HI);
  
      /* Call trm_request_and_notify_enhanced */
      trm_request_and_notify_enhanced(client_id, TRM_RXTX_BEST, duration,
                                     reason, grant_enh_callback, tag, 
                                     unlock_callback);
      err_val = 0;
      break;
    }
    case TRM_PRIO_LO:
    {
      /* Call trm_request_and_notify_enhanced */
      trm_request_and_notify_enhanced(client_id, TRM_RXTX_BEST, duration,
                                     reason, grant_enh_callback, tag, 
                                     unlock_callback);
      err_val = 0;
      break;
    }
    default:
      break;
  }

  TRM_SHIM_LEAVE_CRIT_SECT(trm_shim.crit_sect); 

  /* Error Fatal if priority is not defined */
  if (err_val < 0)
  {
    ERR_FATAL("TRM SHIM:Client Priority is invalid", 0, 0, 0);
  }

} /* trm_shim_request_and_notify_enhanced */

/*============================================================================

FUNCTION TRM_SHIM_GET_REASON

DESCRIPTION
  This function returns the reason from the internal TRM structure table which
  will be the last reason with which the client requested lock. If the client 
  is not holding the chain nor has made any request/reservation, then 
  TRM_NUM_REASONS will be returned.  
  
DEPENDENCIES
  None

RETURN VALUE
  trm_reason_enum_t

SIDE EFFECTS
  None.

============================================================================*/

trm_reason_enum_t trm_shim_get_reason
(
  trm_client_enum_t       client_id
)
{
  /* Client reason to be returned. Assume initially client doesnt have a 
     valid reason */
  trm_reason_enum_t reason = TRM_NUM_REASONS;
  
  /* Obtain the index from the argument client id.
     Just to check if the client is a TRM shim client */
  (void)trm_shim_get_index(client_id);
  
  /* the last reason with which the client requested lock */
  reason = trm_get_reason( client_id );
  
  /* Convert the _HI to nominal reason */
  trm_shim_switch_reason_hi_lo( &reason, TRM_PRIO_LO );
  
  return reason;
}

