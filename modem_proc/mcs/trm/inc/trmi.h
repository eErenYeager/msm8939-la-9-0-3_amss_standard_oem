#ifndef TRMI_H
#define TRMI_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

              Transceiver Resource Manager Internal Header File

GENERAL DESCRIPTION

  This file provides some common definitions for trm.cpp & trmlog.cpp


EXTERNALIZED FUNCTIONS

  None


REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/trm/inc/trmi.h#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
06/20/2014   sk      Changes to support single SIM scenario for TRM
						  mode changes(679494)
05/15/2014   sk      Changes to check G band support before mapping G sub2 to
		     			to device 2 in DSDa roaming case for SVLTE+G(659015)
04/03/2014   mn      Support for SLTE + CA (CR: 616403).
03/28/2014   sr      Changes for QTA gap issue in SGLTE+G (CR:640167)
02/27/2014   sr      SLTE Changes.
03/12/2014   mn      Adding support for connected mode WTR hopping.
02/08/2014   sk      Changes for reporting switch initiation
02/08/2014   sk      Changes enable\disable Idle hopping using NV
01/10/2014   sk      Changes for SGLTE+G DSDA wtr hopping requirement
11/06/2013   mn      Adding support for Opp SGLTE/SGTDS/SVLTE + G.
05/10/2013   mn      Adding support for TSTS.
03/19/2013   mn      Fix for trying to give 1x secondary chain when GPS is
                     holding a chain and resource is RX_ANY.
12/06/2012   sr      Changes for Band Tune logging issue.
11/22/2012   mn      Triton DSDA changes.
11/05/2012   sr      CD fix(CR: 390912) and Band Tune fix(418118).
10/21/2012   sr      Logging Fix (CR: 412848).
01/10/2012   ag      TRM Feature Cleanup
07/07/2011   ag      Support for wakeup manager.
10/18/2010   ag      Merged DSDS functionality.
11/13/2009   ag      Added modem_mcs_defs.h for GENESIS modem.
07/21/2009   ag      Merged INTLOCK and other Q6 changes from //depot
08/28/2008   adw     Added FEATURE_MCS_TRM to featurize TRM for ULC.
11/02/2005   cab     Added BEST_POSSIBLE resource option - pre-empts lower
                     pri clients from chain 0, but takes chain 1 if higher
                     pri clients have chain 0
05/26/2005   ajn     Code review comment changes
04/08/2004   ajn     Initial AMSS version

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "customer.h"
#include "rfm.h"
#include "modem_mcs_defs.h"
#include "trm.h"
#include "trm_wmgr.h"

/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/

/* MACROS for using rex based critical sections on Q6 platforms */

/* Atomic section macros for initialization of critical section */
#define TRM_ENTER_ATOMIC_SECT()     REX_DISABLE_PREMPTION()
#define TRM_LEAVE_ATOMIC_SECT()     REX_ENABLE_PREMPTION()

/* Atomic section macros for section of code under TASKLOCK */
#define TRM_ENTER_TASKLOCK_SECT()   REX_DISABLE_PREMPTION()
#define TRM_LEAVE_TASKLOCK_SECT()   REX_ENABLE_PREMPTION()

/* Critical section macros */
#define TRM_INIT_CRIT_SECT(lock)    rex_init_crit_sect(&(lock))
#define TRM_ENTER_CRIT_SECT(lock)   rex_enter_crit_sect(&(lock))
#define TRM_LEAVE_CRIT_SECT(lock)   rex_leave_crit_sect(&(lock))

/* Macro to get the opposite RXTX Chain */
#define TRM_OPP_RXTX_CHAIN(chain)      ((chain == TRM_CHAIN_0) ? TRM_CHAIN_2 : TRM_CHAIN_0)
#define TRM_ASSOCIATED_RX_CHAIN(chain) ((chain == TRM_CHAIN_0) ? TRM_CHAIN_1 : TRM_CHAIN_3)

/* macro to prevent compiler warnings when the payload parameter is not used
   by state entry, exit, and transition functions, or when a variable is only
   used in MSG_XXXX calls that may be compiled out */
#define TRM_NOTUSED(i) if(i){}

#define TRM_ARR_SIZE(a) sizeof(a)/sizeof(a[0])

/* SRLTE and DSDS mode bits */
#define TRM_DSDS_MODE_BIT     (uint8)0x1
#define TRM_SRLTE_MODE_BIT    (uint8)0x2


/* Macros for handling of bitmasks */
#define TRM_SET_BIT(cfg,data)                 ((cfg) |= (data))
#define TRM_RESET_BIT(cfg,data)               ((cfg) &= ~(data))
#define TRM_IS_BIT_ENABLED(bitmask, bit_val)  ((bitmask) & (bit_val))
#define TRM_MSK_BIT_ON(mask, bit)          ( (mask) & ( 1 << bit ) )
#define TRM_SET_BIT_ON(mask, bit)          ( (mask) |= ( 1 << bit ) )
#define TRM_SET_BIT_OFF(mask, bit)         ( (mask) &= ~( 1 << bit ) )

/* Priorities are on the range 1..255 */
typedef uint8                     trm_pri_t;

/* Invalid client/reason pairs are indicated by a priority of BAD */
#define BAD                       0


/* Client/reason compatibility mask */
typedef uint64                    trm_compatible_mask_t;

/*-----------------------------------------------------------------------------
  *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE ***

  Enumerations in this file are 8-bit values that are used directly by the TRM
  log.  Changing enumerator values and adding new enumerator values will
  necessitate a new TRM log parser.
-----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
  Client lock state

    A TRM client may be inactive, waiting for an RF resource, or holding one.
    When waiting for an RF resource, it may need it at a particular time, or
    merely ASAP.  When holding an RF resource, it may need it for a specific
    length of time, or as long as possible.

    When TRM is processing a client request, it may temporarily change a
    client's state during processing, and restore the state when done, or
    change the client to a new state.
-----------------------------------------------------------------------------*/

typedef enum
{
  /* Client does not have an RF chain, nor does it want one */
  TRM_INACTIVE,

  /* Client has a reservation for an RF chain at a particular time */
  TRM_RESERVED_AT,

  /* Client is requesting an RF chain immediately */
  TRM_REQUEST,

  /* Client requests to be notified when it can be granted an RF chain */
  TRM_REQUEST_AND_NOTIFY,

  /* Client has been given the RF chain, & will keep it until told not to */
  TRM_LOCK_RETAINED,

  /* Client has been given an RF chain until a specific time */
  TRM_LOCKED_UNTIL,


  /* The following states are TRANSIENT states.  TRM will change the client
     state to one of the above states before returning */


  /* Client is trying to extend the "locked until" duration */
  TRM_EXTENDING,

  /* Client has been assigned an RF chain, but client hasn't been told yet */
  TRM_GRANTING,

  /* Client has been assigned an RF chain and the maximum preemptible
     extension has been applied but the client hasn't been told yet */
  TRM_GRANTING_ENH,

  /* Client is requesting for a maximum possible extension of the lock after
     the minimum duration has been granted */
  TRM_REQUEST_AND_NOTIFY_ENH,

  /* Client has been given a RF chain for a minimum non premptible duration
     plus a preemptible maximum duration possible. */
  TRM_LOCK_RETAINED_ENH,

  /* Client has been given the RF chain, & will keep it until told not to */
  TRM_LOCK_RETAINED_ADVANCED
} trm_client_lock_state_enum;




/*-----------------------------------------------------------------------------
  RF Chains
    The clients request resources with certain capabilites, which need to be
    converted to more specific RF chain requests.  [The second RF chain may
    not exist.  If it does exist, it may not have the same sensitivity as the
    first, and probably doesn't have an associated transmit chain.]
-----------------------------------------------------------------------------*/

typedef enum
{
  /* The primary RF chain */
  TRM_CHAIN_0,

  /* The secondary RF chain */
  TRM_CHAIN_1,

  TRM_CHAIN_DIVERSITY = TRM_CHAIN_1,


  /* Any RF chain */
  TRM_CHAIN_ANY,

  /* The best possible RF chain */
  TRM_CHAIN_BEST_POSSIBLE,

  /* No RF chain */
  TRM_CHAIN_NONE,

  /* The third RF chain */
  TRM_CHAIN_2,

  /* The fourth RF chain */
  TRM_CHAIN_3,

  /* Best Possible 2 Chain */
  TRM_CHAIN_BEST_POSSIBLE_2,

  TRM_CHAIN_4,

  TRM_CHAIN_REVERSE_BEST_POSSIBLE,

  TRM_CHAIN_REVERSE_RX_ANY,

  TRM_CHAIN_BEST_POSSIBLE_MOD,

  TRM_CHAIN_BEST_POSSIBLE_MOD_2,

  TRM_CHAIN_SECOND_PREFERRED,

  /* This needs to be the last value */
  TRM_CHAIN_LAST = TRM_CHAIN_SECOND_PREFERRED,

  TRM_CHAIN_MAX
} trm_chain_enum;

/*----------------------------------------------------------------------------
   Band Class State Enum definitions
 ----------------------------------------------------------------------------*/
typedef enum
{
   /* No band class activity going on or band tuning is completed */
   TRM_BC_STATE_INACTIVE,
   /* A new band class has been requested */
   TRM_BC_STATE_REQUEST,
   /* Waiting for a client to release its lock */
   TRM_BC_STATE_WAITING,
   /* Tuning to the requested band */
   TRM_BC_STATE_TUNING,
   /* Wait complete and the band can be granted */
   TRM_BC_STATE_GRANTED

} trm_bc_enum_t;


/*-----------------------------------------------------------------------------
  RF Chain Groups

    TRM will assign clients to "RF Chain Groups", or simply "groups", in the
    following manner:

      -1: Client has not yet been assigned to an RF chain, or
          client cannot be assigned to an RF chain.
       0: Client is assigned to the primary RF chain.
       1: Client is assigned to the secondary RF chain.
      2+: Client will be assigned to a chain, but which is not yet determined.

    For group "g" (g >= 0), "g xor 1" represents the group assigned to the
    other RF chain.  For example, if clients in group 2 end up being assigned
    to the secondary RF chain, group 3 (2 ^ 1) will be assigned to the primary.

-----------------------------------------------------------------------------*/

typedef int8                      trm_group;


/* Custom Data Type for SLTE Bitmask */
typedef uint8 trm_slte_bitmask;

/*----------------------------------------------------------------------------
  SLTE Type information.
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_SLTE_DISABLED = 0,
  TRM_SLTE_1X       = 1,
  TRM_SLTE_GSM      = 2
} trm_slte_info_type;

/*----------------------------------------------------------------------------
  TRM client and chain mapping information.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_chain_enum first_config_chain[TRM_MAX_CLIENTS];
  trm_chain_enum second_config_chain[TRM_MAX_CLIENTS];
} trm_client_chain_info_type;

/*-----------------------------------------------------------------------------
  Client state information

    A client may be inactive, requesting a lock, or holding one.
    If it is requesting a lock, it requests a lock on a resource, which maps
    to an RF chain (primary, secondary, or any).
    It will request or hold a lock for a specific reason, which will map to
    a particular priority.
    TRM will assign the request into a group, based on priority and conflicts.
    A client may be granted an lock on an RF chain (primary, or secondary),
    which correspond to an RF device.
    If holding a lock, it may report to the client when it must release it.
-----------------------------------------------------------------------------*/

/*
  *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE *** NOTE ***

  This structure is a hand-packed 8-byte long record, and is used for logging.
  Changes to the size, order, or content will require an updated log parser. */

struct trm_client_state
{
  /* Lock state (inactive, requesting a lock, holding a lock, ... ) */
  trm_client_lock_state_enum      lock_state;

  /* Resource the client wants (RX_ANY, RXTX, RX_GPS, ...) */
  trm_resource_enum_t             resource;

  /* Reason the client wants the resource (Acq, Traffic, QPCH, ...) */
  trm_reason_enum_t               reason;

  /* Relative priority (based on client and reason) */
  trm_pri_t                       priority;

  /* RF Chain client wants (based on resource & h/w capability) */
  trm_chain_enum                  chain;

  /* What the client currently holds (Nothing, Chain 0, or Chain 1) */
  trm_grant_event_enum_t          granted;

  /* Internal TRM group assignment, for conflict resolution */
  trm_group                       group;

  /* When the client must give up a lock (By a certain time, now, never) */
  trm_unlock_event_enum_t         unlock_state;


};

/*----------------------------------------------------------------------------
  Band class information - 8 bit struct
----------------------------------------------------------------------------*/
typedef struct
{
  trm_band_t              bc_current;    /* Current Band          */
  trm_band_t              bc_requested;  /* Requested Band        */
  trm_band_t              bc_previous;   /* Previous Band         */
  trm_bc_enum_t           bc_state;      /* Band Class State Enum */
  trm_client_enum_t       bc_waiting_on; /* Client waiting on ... */
  boolean                 bc_registered;
  boolean                 reserved;      /* Reserved */
  /* Until this, logging have been modified to work as 16 bytes.
     The modification was needed to keep it backward compatible
     with NikeL where trm_band_t was 32 bits while it is 16 bits
     on Dime. Any future modifications would need either 8 byte
     boundary rule to work seamlessly w.r.t. logging. */
} trm_bc_info_type;

/*----------------------------------------------------------------------------
  Transceiver RF mode mapping information
----------------------------------------------------------------------------*/
typedef struct
{
  /* RF Mode for this object */
  rfm_mode_enum_type   rfmode;

  /* Band class information */
  trm_bc_info_type   bc_info;

}trm_rf_mode_map_type;


/*----------------------------------------------------------------------------
  TRM Internal structure to hold all the wakeup clients information
----------------------------------------------------------------------------*/
typedef struct
{
  /* Is client registered */
  boolean                   registered;

  /* Client ID */
  trm_wakeup_client_enum_t  client;

  /* CLient Result */
  trm_wakeup_info_type      *result;

  /* Call back pointer */
  trm_wmgr_cb_type          cb;

} trm_wakeup_client_type;

/*============================================================================

FUNCTION TRM_GET_CHAIN_FOR_DEVICE

DESCRIPTION
  Returns the trm chain number for corresponding rf device.

DEPENDENCIES
  None

RETURN VALUE
  rfm_device_enum_type

SIDE EFFECTS
  None

============================================================================*/
trm_chain_enum trm_get_chain_for_device
(
  rfm_device_enum_type device
);

/*============================================================================

FUNCTION TRM_VALIDATE_MODE

DESCRIPTION
  This is an internal API that can be used to validate a given mode against
  HW capability.

DEPENDENCIES
  trm_init() should have been called by tmc prior to using this api

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
boolean trm_validate_mode( uint8 mode );

/*============================================================================

FUNCTION TRM_IS_IDLE_HOPPING_ENABLED

DESCRIPTION
  This is an internal API that can be used to check if idle hopping is
  enabled

DEPENDENCIES
  None

RETURN VALUE
  TRUE if idle hopping is enabled
  FALSE if idle hopping is disabled

SIDE EFFECTS
  None

============================================================================*/
boolean trm_is_idle_hopping_enabled(void);






/*============================================================================

FUNCTION ANT_SWITCH_DIV_INIT

DESCRIPTION
  Initializes the asdiv related variables, called from TRM during TRM
  initialization
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void ant_switch_div_init(boolean en, uint8 modes);

/*============================================================================

FUNCTION ANT_SWITCH_DIV_MUTEX_INIT

DESCRIPTION
  Initializes the asdiv mutex, TRM needs to call this api first to initialize
  the asdiv mutex
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void ant_switch_div_mutex_init( void );

/*============================================================================
FUNCTION TRM_GET_GRANT_EVENT_FROM_CHAIN

DESCRIPTION
  Converts the trm_chain_enum to trm_grant_event_enum_t

DEPENDENCIES
  None

RETURN VALUE
  Grant event corresponding to the input chain.
  TRM_DENIED when conversion is not possible.

SIDE EFFECTS
  None
============================================================================*/

trm_grant_event_enum_t trm_get_grant_event_from_chain
( 
  trm_chain_enum chain
);

#endif /* TRMI_H */

