
/*!
  @file
  wwcoex_conflict_table.c

  @brief
  This file implements the conflict table management functions of LIMTSMGR COEX module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/limitsmgr/wwan_coex/src/wwcoex_conflict_table.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
01/14/14   jm      Ensure freqID is 16-bit value
11/23/13   rj      Handle Tech Rx Power Ind after Rx OFF indication
11/22/13   jm      Resolution of LLVM Warnings
07/29/13   ag      Fixed query highest action returning UNKNOWN for hybrid mode
03/23/13   ag      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "comdef.h"
#include <stringl/stringl.h>
#include "wwcoex_action_iface.h"
#include "wwcoex_conflict_table.h"

/*=============================================================================

                            TYPEDEFS

=============================================================================*/


/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/
/*! conflict tables */
wwcoex_conflict_tbls_type  wwcoex_conflict_tbls[COEX_MAX_CONCURRENT_SCENARIOS];

/*! number of active/standby stacks */
uint32 wwcoex_num_standby_stacks = 0;
uint32 wwcoex_num_active_stacks = 0;

/*=============================================================================

                                FUNCTIONS

=============================================================================*/
/*===========================================================================
FUNCTION wwcoex_set_sub_state

DESCRIPTION
  This API will update the subscription state

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_sub_state
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
)
{
  wwcoex_num_standby_stacks = num_standby_stacks;
  wwcoex_num_active_stacks = num_active_stacks; 
}

/*===========================================================================
FUNCTION wwcoex_init_tables

DESCRIPTION
  This API will init the wwcoex conflict tables

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_init_tables
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
)
{
  uint32 i;

  /* Allocate memory for conflict tables */
  for (i=0; i <COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    memset(&wwcoex_conflict_tbls[i], 0, sizeof(wwcoex_conflict_tbls_type));

    wwcoex_conflict_tbls[i].current_tbl = WWCOEX_TBL_INVALID;
  }
  
  wwcoex_num_active_stacks = num_active_stacks;
  wwcoex_num_standby_stacks = num_standby_stacks;
}

/*=============================================================================

  FUNCTION:  wwcoex_find_match_node

=============================================================================*/
/*!
    @brief
    Find a node with matching techs
 
    @return
    index to the matching node node, MAX otherwise

*/
/*===========================================================================*/
uint32 wwcoex_find_match_node
(
  uint8 tech1,
  uint8 tech2
)
{
  uint32 index;

  /* Check if this tech combination already has a node */
  for (index=0; index<COEX_MAX_CONCURRENT_SCENARIOS; index++)
  {
    if ( wwcoex_conflict_tbls[index].current_tbl == WWCOEX_TBL_INVALID )
    {
      continue;
    }

    if ( ( (wwcoex_conflict_tbls[index].tech1 == tech1) && 
           (wwcoex_conflict_tbls[index].tech2 == tech2) ) ||
         ( (wwcoex_conflict_tbls[index].tech1 == tech2) &&
           (wwcoex_conflict_tbls[index].tech2 == tech1) ) )
    {
      /* Found a match */
      break;
    }
  }

  return index;
}

/*=============================================================================

  FUNCTION:  wwcoex_get_table_node

=============================================================================*/
/*!
    @brief
    Get the conflict table node corresponding to the two tech pair. If there
    is no table for the pair, then return an unused node.
 
    @return
    conflict table node

*/
/*===========================================================================*/
wwcoex_conflict_tbls_type* wwcoex_get_table_node
(
  uint8   tech1,
  uint8   tech2
)
{
  uint32 index;
  wwcoex_conflict_tbls_type* tbl_node=NULL;

  /* Check if there already exists a node for this tech combination */
  index = wwcoex_find_match_node(tech1, tech2);

  if (index >= COEX_MAX_CONCURRENT_SCENARIOS)
  {
    /* Could not find a match */

    /* Check if there is anything unused */
    for (index = 0; index < COEX_MAX_CONCURRENT_SCENARIOS; index++ )
    {
      if ( wwcoex_conflict_tbls[index].current_tbl == WWCOEX_TBL_INVALID )
      {
        tbl_node = &wwcoex_conflict_tbls[index];
        tbl_node->tech1 = tech1;
        tbl_node->tech2 = tech2;
        break; 
      }
    }
  }
  else
  {
    tbl_node = &wwcoex_conflict_tbls[index];
  }

  return tbl_node;
}

/*=============================================================================

  FUNCTION:  wwcoex_invalidate_tables

=============================================================================*/
/*!
    @brief
    Invalidates the conflict tables containing the specified tech
 
    @return
    NONE

*/
/*===========================================================================*/
void wwcoex_invalidate_tables
(
  uint8  tech
)
{
  uint8 i,j;

  /* Find nodes that have this tech and invalidate them */
  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    if (wwcoex_conflict_tbls[i].tech1 == tech || 
        wwcoex_conflict_tbls[i].tech2 == tech)
    {
      wwcoex_conflict_tbls[i].current_tbl = WWCOEX_TBL_INVALID;

      for (j=0; j<WWCOEX_MAX_TABLES_PER_SCENARIO; j++)
      {
        wwcoex_conflict_tbls[i].tables[j].tech1_num_entries = 0;
        wwcoex_conflict_tbls[i].tables[j].tech2_num_entries = 0;
      }
    }
  }
}

/*=============================================================================

  FUNCTION:  wwcoex_get_oldest_tbl_index

=============================================================================*/
/*!
    @brief
    Returns index to the oldest table
 
    @return
    NONE

*/
/*===========================================================================*/
int8 wwcoex_get_oldest_tbl_index
(
  wwcoex_conflict_tbls_type*  conflict_tbl
)
{
  int8 index;

  if (conflict_tbl->current_tbl < 0)
  {
    /* Table node is not valid... so use the first one */
    index = 0;
  }
  else
  {
    index = (conflict_tbl->current_tbl + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO ;
  }

  return index;
}

/*=============================================================================

  FUNCTION:  wwcoex_tbl_freqid_pair_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the conflict tables and return the action
 
    @return
    NONE

*/
/*===========================================================================*/
cxm_action_type wwcoex_tbl_freqid_pair_lookup
(
  uint32 freqid1,
  uint32 freqid2
)
{
  cxm_action_type action = ACTION_UNKNOWN;
  int index1, index2;
  uint32 i,j;
  wwcoex_tbl_type* tbl_ptr;
  boolean match_found = FALSE;

  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    /* Check if node is valid */
    if (wwcoex_conflict_tbls[i].current_tbl == WWCOEX_TBL_INVALID)
    {
      continue;
    }

    /* Node is valid so scan both tables. Start for current table first since
       that has more chances of finding a match */
    j = wwcoex_conflict_tbls[i].current_tbl;
    do
    {
      tbl_ptr =  &wwcoex_conflict_tbls[i].tables[j];

      /* Is the table valid ? */
      if ( (tbl_ptr->tech1_num_entries > 0) &&
           (tbl_ptr->tech2_num_entries > 0) )
      {
        index1 = freqid1 - tbl_ptr->tech1_fid_offset;
        index2 = freqid2 - tbl_ptr->tech2_fid_offset;

        if ( (index1 < 0) || (index1 >= tbl_ptr->tech1_num_entries) || 
             (index2 < 0) || (index2 >= tbl_ptr->tech2_num_entries)  )
        {
          /* Try the other way around */
          index1 = freqid2 - tbl_ptr->tech1_fid_offset;
          index2 = freqid1 - tbl_ptr->tech2_fid_offset;

          if ( (index1 >= 0) && (index1 < tbl_ptr->tech1_num_entries) &&
               (index2 >= 0) && (index2 < tbl_ptr->tech2_num_entries)  )
          {
            /* Found a match... */
            action = tbl_ptr->arr[index1][index2].action;
            match_found = TRUE;
            break;
          }
        }
        else
        {
          /* Found a match... */
          action = tbl_ptr->arr[index1][index2].action;
          match_found = TRUE;
          break;
        }
      }
      j = (j + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO;
    }while (j != wwcoex_conflict_tbls[i].current_tbl);

    if (match_found == TRUE)
    {
      break;
    }
  }

  return action;
}

/*=============================================================================

  FUNCTION:  wwcoex_tbl_freqid_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id in the conflict tables and return the MAX action
 
    @return
    NONE

*/
/*===========================================================================*/
cxm_action_type wwcoex_tbl_freqid_lookup
(
  uint32 freqid1
)
{
  cxm_action_type action = ACTION_NONE;
  int index1, index2;
  uint32 i,j,num_scenarios=COEX_MAX_CONCURRENT_SCENARIOS;
  wwcoex_tbl_type* tbl_ptr;
  boolean found = FALSE;

  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    /* Check if node is valid */
    if (wwcoex_conflict_tbls[i].current_tbl == WWCOEX_TBL_INVALID)
    {
      num_scenarios--;
      continue;
    }

    /* Node is valid so scan both tables. Start for current table first since
       that has more chances of finding a match */
    j = wwcoex_conflict_tbls[i].current_tbl;
    do
    {
      tbl_ptr =  &wwcoex_conflict_tbls[i].tables[j];

      /* Is the table valid ? */
      if ( (tbl_ptr->tech1_num_entries > 0) &&
           (tbl_ptr->tech2_num_entries > 0) )
      {
        /* Check first which columns does the index map to */
        index1 = freqid1 - tbl_ptr->tech1_fid_offset;
        if ( (index1 < 0) || (index1 >= tbl_ptr->tech1_num_entries) )
        {
          /* Try the other way around */
          index2 = freqid1 - tbl_ptr->tech2_fid_offset;

          if ( (index2 >= 0) && (index2 < tbl_ptr->tech2_num_entries) )
          {
            /* Its the secondary index.. So go through all the entries in first column. */
            for (index1 = 0; index1 < tbl_ptr->tech1_num_entries; index1++)
            {
              action = MAX((uint8)action, (uint8)(tbl_ptr->arr[index1][index2].action));
            }

            /* found id in this table so no need to check other table */
            found = TRUE;
            break;
          }
          //else cannot find the freqId in this table.. do nothing.
        }
        else
        {
          /* Its the primary index.. So go through all the entries in second column. */
          for (index2 = 0; index2 < tbl_ptr->tech2_num_entries; index2++)
          {
            action = MAX((uint8)action, (uint8)(tbl_ptr->arr[index1][index2].action));
          }

          /* found id in this table so no need to check other table */
          found = TRUE;
          break;
        }
      }
      j = (j + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO;
    }while (j != wwcoex_conflict_tbls[i].current_tbl);
  }

  /* if not found in any tables and there is table for atleast one scenario, then return UNKNOWN... */
  if ((found==FALSE) && (num_scenarios > 0))
  {
    action = ACTION_UNKNOWN;
  }

  return action;
}

/*============================================================================

FUNCTION CXM_QUERY_HIGHEST_ACTION

DESCRIPTION
  Query the highest mitigation action for one freqId
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_highest_action
(
  cxm_highest_action_query_s* query_st
)
{
  int i;

  /* Check input... */
  if ( query_st == NULL || query_st->actions == NULL ||
       query_st->num_requesting_ids == 0 || query_st->requesting_ids == NULL || 
       query_st->num_requesting_ids > WWCOEX_MAX_REQUESTING_IDS )
  {
    return FALSE;
  }

  /* SW Coex mitigation is disabled */
  if (wwcoex_num_standby_stacks == 0)
  {
    memset((void *)(query_st->actions), 0, 
           query_st->num_requesting_ids * sizeof(cxm_action_type));
    return TRUE;
  }

  /* Scan through each entry in the table */
  for (i=0; i < query_st->num_requesting_ids; i++)
  {
    /* If any of the requesting Ids are unknown then return action as unknown */
    if ( (query_st->requesting_ids[i]) == WWCOEX_UNKNOWN_FREQID )
    {
      query_st->actions[i] = ACTION_UNKNOWN;
    }
    else
    {
       /* Lookup conflict table for freqids*/
       query_st->actions[i] = wwcoex_tbl_freqid_lookup(query_st->requesting_ids[i]);
    }
  }

  return TRUE;
}

/*============================================================================

FUNCTION CXM_QUERY_ACTION

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_action
(
  cxm_action_query_s* query_st
)
{
  int i,j;
  cxm_action_type action = ACTION_NONE;
  cxm_action_type action_func = ACTION_NONE;

  /* Check input... */
  if ( query_st == NULL || query_st->actions == NULL ||
       query_st->num_conflicting_ids == 0 || query_st->conflicting_ids == NULL ||
       query_st->num_requesting_ids == 0 || query_st->requesting_ids == NULL || 
       query_st->num_requesting_ids > WWCOEX_MAX_REQUESTING_IDS )
  {
    return FALSE;
  }
  
  /*Reset the mask */
  *(query_st->actions) = 0;

  /* If SW mitigation is disabled then num standby tasks will be 0; 
     Also if its DSDA, then Rx-Rx conflict should have action as NONE */
  if ( (wwcoex_num_standby_stacks == 0) ||
       ( (wwcoex_num_standby_stacks == wwcoex_num_active_stacks) &&
         (query_st->requesting_activity == ACTIVITY_RX) && 
	       (query_st->conflicting_activity == ACTIVITY_RX) ) ) 
  {
    return TRUE;
  }

  /* Scan through each entry in the table */
  for (i=0; i < query_st->num_requesting_ids; i++)
  {
    if (query_st->requesting_ids[i] != WWCOEX_UNKNOWN_FREQID)
    {
	  /* Start with no action */
	  action = ACTION_NONE;
	
      for (j=0; j < query_st->num_conflicting_ids; j++)
      {
        /* If any of the Ids are unknown then return action as unknown */
        if ( query_st->conflicting_ids[j] != WWCOEX_UNKNOWN_FREQID )
        {
          /* Lookup conflict table for freqids*/
		      action_func =  wwcoex_tbl_freqid_pair_lookup( 
                                            query_st->requesting_ids[i],
                                            query_st->conflicting_ids[j]);
		 	   
          action = MAX(action, action_func);
        }
		    else
		    {
		      action = MAX(action, ACTION_UNKNOWN);
		    }
      }
    }
	  else
    {
	    /* Use unknown... */
      action = ACTION_UNKNOWN;	
	  }

    /* Store the action into the mask */
    *(query_st->actions) |= (action << (WWCOEX_ACTION_BIT_SIZE*i));
  }

  return TRUE;
}
