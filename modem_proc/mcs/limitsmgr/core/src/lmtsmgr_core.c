
/*!
  @file
  lmtsmgr_core.c

  @brief
  This file implements the core functions of LIMTS MGR module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/limitsmgr/core/src/lmtsmgr_core.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
12/23/14   rj      Validate conflict_tbl index from wwcoex_get_table_node
12/10/14   ag      For G+G data+mms, floor all tiers except tier 5 
09/23/14   ag      For IRAT handover, reset lprio tech only if it was prev MAX
06/23/14   sg      desense indication mechanism
06/28/14   rj      Disable SVLTE+G Idle Mode Hopping
05/09/14   rj      KW fixes 
05/06/14   jm      KW Warning Fixes
05/04/14   rj      Filter Path support to send GSM Tx specific flag info to RF
05/02/14   rj      KW fixes
05/01/14   rj      Update Coex power limits to MAX, when moved into filter path
04/28/14   jm      L+G Band Avoidance support
04/19/14   rj      1X-SLTE support added 
04/17/14   ag      Priorities for techs on same sub are fixed
04/17/14   rj      Check blanking EFS only if list returns action = BLANK
04/07/14   ag      Resetting the inactivity timer before sending dereg msg to GSM
03/27/14   jm      Proper error handling for get table node
03/21/14   rj      Only memscpy action array and don't re-write freqID_offsets
03/19/14   jm      WWAN coex based off of RF device assignment
03/19/14   ag      Reset power_change flag only in the outer tech loop
03/17/14   rj      WWcoex Action info packet not getting captured
03/17/14   rj      Fix for action table not updated in logging
03/17/14   jm      Fix for action table when mult. freq are given on same band
02/05/14   jm      Adding slot level backoff support for SVLTE/SGTDS
01/14/14   jm      Ensure freqID is 16-bit value
12/19/13   jm      GPS support for DDR coex
12/19/13   jm      DDR management phase 2 support (diversity power + GPS)
11/27/13   jm      Include min/max channel ranges as part of desensed channels
11/23/13   ag      Ignore freq/power comparisons for DSDS/TSTS mode
11/22/13   jm      Resolution of LLVM Warnings
11/21/13   jm      Adding DDR management support
11/16/13   rj      Adding macro prio callback support
11/06/13   rj      Adding prob_aggr_mask support
10/22/13   ag      Rescheduling searches based on power thresholds
09/19/13   rj      Adding support for GSM to ASID mapping
08/22/13   ag      KW fix and use high prio tech power for IM power backoff
08/09/13   ag      Added NPA notification for BL
07/24/13   ag      Support for conveying macro priority to techs
07/19/13   ag      Set action as unknown for DSDS/TSTS
07/01/13   rj      KW error fixes
06/28/13   ag      Support for EFS item to disable all SW mitigation
06/17/13   ag      Avoid memset of action table during power updates
06/13/13   rj      SVLTE/SVDO Coex for Triton/DI2.0
04/23/13   rj      Fix for sending pwr limits to GSM and WCDMA
04/06/13   ag      Support for DSDA WWAN Coexistence
02/20/13   rj      Fix CW issues
09/24/12   ag      Modified to handle single processor and msgr
04/22/12   ag      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "mcs_variation.h"
#include "comdef.h"
#include "stringl.h"
#include "cm.h"
#include "sys.h"

#include "cxm.h"
#include "lmtsmgr_task.h"
#include "lmtsmgr_i.h"
#include "lmtsmgr_efs.h"
#include "lmtsmgr_msgr.h"

#include "sar_efs.h"
#include "sar_dsi.h"
#include "wwan_coex.h"
#include "batt_efs.h"
#include "subs_prio.h"
#include "wwcoex_conflict_table.h"
#include "wwcoex_inactivity.h"
#include "lmtsmgr_diag.h"
#include "wwcoex_action_iface.h"
#include "batt_npa.h"
#include "trm.h"
#include "ddr_coex_npa.h"
#include "wwcoex_ba.h"

/*=============================================================================

                            TYPEDEFS

=============================================================================*/
/* Maximum number of entires possible in a single priority tier */
#define LMTSMGR_MAX_TIER_TECHS  1

/* Max concurrent UL techs is 2 */
#define LMTSMGR_MAX_CONCURRENT_UL_TECHS 2

#define LMTSMGR_COMPILE_ASSERT(a)  switch(0){case 0: case (a):;}

#define LMTSMGR_CHECK_DIFF_SUB(tech1, tech2) (((tech1)==CXM_TECH_GSM2) || ((tech2)==CXM_TECH_GSM2) || ((tech1)==CXM_TECH_GSM3) || ((tech2)==CXM_TECH_GSM3))

/* Get the other GSM tech */
#define GET_OTHER_GSM_TECH(tech) (((tech)==CXM_TECH_GSM1)?CXM_TECH_GSM2:CXM_TECH_GSM1)

typedef enum 
{
  LMTSMGR_TIER_1,     /* Highest priority */
  LMTSMGR_TIER_2,     /* Second highest priority */
  LMTSMGR_TIER_MAX    /* Max or invalid */
}lmtsmgr_priority_type;

typedef struct {
  /* Technology identifier */
  cxm_tech_type           tech;
  /* Which RF Device is used for Tx? */
  cxm_rfdev_type          tx_rfdev;
  /* Tx'ing on which band? */
  uint32                  band;
  /* Priority of this transmitter */
  lmtsmgr_priority_type       pri;
} lmtsmgr_core_prio_table_type;

/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/

/* Priority table: The content shall be aligned in ascending order of pri at 
   all times */
typedef struct {
  lmtsmgr_core_prio_table_type tier1[LMTSMGR_MAX_TIER_TECHS];
  uint32  num_tier1_entries;
  lmtsmgr_core_prio_table_type tier2[LMTSMGR_MAX_TIER_TECHS];
  uint32 num_tier2_entries;
} lmtsmgr_core_prio_tier_type;

lmtsmgr_core_prio_tier_type lmtsmgr_core_tiers;

/* Global LMTSMGR state variables */
lmtsmgr_struct lmtsmgr;

/* Temp variable to hold the special ind mask value for each coex_input run */
static uint32 lmgr_temp_ind_mask = 0;

/* Temp result table for storing actions */
wwcoex_tbl_type temp_result_tbl;

/* Variables to store the current SUB and tech which are low priority */
sys_modem_as_id_e_type      lprio_data_sub = MAX_NUM_OF_SUBSCRIPTIONS;
cxm_tech_type               lprio_data_tech = CXM_TECH_MAX;
wwcoex_prio_table_upd_type  lprio_data_tbl_action = PRIO_TABLE_SET_MIN;

/*=============================================================================

                                FUNCTIONS

=============================================================================*/

/*=======================================================================
FUNCTION LMTSMGR_IS_ON_SEPERATE_DEV
    
DESCRIPTION
  This function will determine if the two techs are on the same TRM device chain

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/
static boolean lmtsmgr_is_on_seperate_dev(cxm_tech_type tech1, cxm_tech_type tech2)
{
  int i;
  int found_tech1 = -1;
  int found_tech2 = -1;
  for (i=0; i<TRM_MAX_CHAINS; i++)
  {
    if (lmtsmgr.chain_owner[i] == tech1)
    {
      found_tech1 = i;
    }
    else if (lmtsmgr.chain_owner[i] == tech2)
    {
      found_tech2 = i;
    }
  }

  return ((found_tech1 != -1) && (found_tech2 != -1) &&
          (found_tech1 != found_tech2));
}

/*=======================================================================
FUNCTION LMTSMGR_UPDATE_TECH_ASID_MAP
    
DESCRIPTION
  This function will Update tech_asid_map for GSM clients with ASID based on multimode subscription.

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/

static void lmtsmgr_update_tech_asid_map ( void )
{
  uint8 index = 0;

  /* Update default */
  for (index = 0; index < MAX_NUM_OF_SUBSCRIPTIONS; index++)
  {
    switch (lmtsmgr.subs_prio_list.tech_asid_map.gsm_id_list[index])
    {
      case SP_GSM1:
          lmtsmgr.gsm_id_list[index] = CXM_TECH_GSM1;
        break;

      case SP_GSM2:
          lmtsmgr.gsm_id_list[index] = CXM_TECH_GSM2;
        break;

      case SP_GSM3:
          lmtsmgr.gsm_id_list[index] = CXM_TECH_GSM3;
        break;

      default:
          MSG_HIGH("lmtsmgr_update_tech_asid_map wrong num_subs %d ", index, 0, 0);
        break;
    }
  }

}

/*=======================================================================
FUNCTION LMTSMGR_GET_SUB_ID
    
DESCRIPTION
  This function will retrieve the SUB ID for a given tech

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/
static sys_modem_as_id_e_type lmtsmgr_get_sub_id
(
  cxm_tech_type  tech
)
{
  sys_modem_as_id_e_type sub_id;

  if (IS_GSM_TECH(tech))
  {
    for (sub_id=0; sub_id < MAX_NUM_OF_SUBSCRIPTIONS; sub_id++)
    {
      if (tech == lmtsmgr.gsm_id_list[sub_id])
      {
        /* Found the SUB id... */
        break;
      }
    }
  }
  else
  {
    /* Return multimode sub id */
    sub_id = lmtsmgr.subs_prio_list.tech_asid_map.multimode_as_id;
  }

  return sub_id;
}

/*=======================================================================
FUNCTION LMTSMGR_GET_DATA_TBL_ACTION
    
DESCRIPTION
  This function will return the prio table action that needs to be applied
  to the data tech. For G+G, there is a special action that needs to be 
  taken.

DEPENDENCIES
  This api should be called only when the criteria to rail the priorities
  is met.. for example when data + mms is hit.
    
RETURN VALUE
  Priority table action to be taken.

SIDE EFFECTS
  None
========================================================================*/
wwcoex_prio_table_upd_type lmtsmgr_get_data_tbl_action 
(
  cxm_tech_type  lprio_tech 
)
{
  cxm_tech_type hprio_tech;
  
  /* init to the default efs based global action */
  wwcoex_prio_table_upd_type action = lprio_data_tbl_action;
  
  /* If its a G+G scenario, then change it... */
  if (IS_GSM_TECH(lprio_tech))
  {
    hprio_tech = GET_OTHER_GSM_TECH(lprio_tech);
    if (lmtsmgr.tech_state[hprio_tech].currList.num_ul_entries > 0)
    {
      action = PRIO_TABLE_SET_MIN_EXCLUDE_SUPER_CRIT;
    }
  }
  
  return action;
}

/*=======================================================================
FUNCTION LMTSMGR_UPDATE_CLIENT_PRIO_TABLE
    
DESCRIPTION
  This function will update client prio table of the registered GSM client.

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/

static void lmtsmgr_update_client_prio_table ( void )
{
  sp_subscription_priority_list  sp_prio_list;
  uint8                          index = 0;
  sys_modem_as_id_e_type         sub;
  cxm_tech_type                  tech;
  wwcoex_prio_table_upd_type     data_tbl_action;

  sp_prio_list = lmtsmgr.subs_prio_list;

  /* Check if we are in data + data High combination */
  if ( (sp_prio_list.num > 1) && 
       (sp_prio_list.order[0].state == SP_DATA_HIGH_ONLY_STATE) &&
       (sp_prio_list.order[1].state == SP_DATA_ONLY_STATE) )
  {
    lprio_data_sub = sp_prio_list.order[1].sub;

    /* Check for array overflow */
    if ((lprio_data_sub < 0) ||
        (lprio_data_sub >= MAX_NUM_OF_SUBSCRIPTIONS))
    {
      /* Should not happen... */
      LMTSMGR_MSG_1(FATAL, "Invalid SUB ID %d", lprio_data_sub);
      lprio_data_tech = CXM_TECH_MAX;
      lprio_data_sub = MAX_NUM_OF_SUBSCRIPTIONS;
      return;
    }

    /* if data SUB is multimode, then need to find the tech */
    if (lprio_data_sub == sp_prio_list.tech_asid_map.multimode_as_id)
    {
      /* go through all clients to find the one on low priority SUB */
      for (tech=0; tech<CXM_TECH_MAX; tech++)
      {
        /* Only interested in techs with UL entries... if its GSM tech then
           it should match the entry in gsm_id_list for corresponding SUB */
        if (lmtsmgr.tech_state[tech].currList.num_ul_entries > 0)
        {
          /* If its GSM tech, also check if the id_list matches for the corr SUB*/
          if (IS_GSM_TECH(tech))
          {
            if (lmtsmgr.gsm_id_list[lprio_data_sub] != tech)
            {
              /* This is not the GSM tech on the lprio SUB */
              continue;
            }
          }

          /* Found the tech */
          break;
        }
      }
      lprio_data_tech = tech;
    }
    else /* lprio SUB is not multimode*/
    {
      /* We know the GSM techid using the SUB as index */
      lprio_data_tech = lmtsmgr.gsm_id_list[lprio_data_sub];
    }

    if (lprio_data_tech < CXM_TECH_MAX)
    {
      data_tbl_action = lmtsmgr_get_data_tbl_action(lprio_data_tech);
      cxm_update_prio_table(lprio_data_tech, 1, 0, data_tbl_action);
    }
    else
    {
      /* Should not happen... */
      LMTSMGR_MSG_1(FATAL, "Could not find UL tech for multimode data SUB %d", lprio_data_sub);
    }
  }
  /* if there is a lprio_data_tech and the d+dh cond is not met, reset the prio 
     if lprio_data_tech is GSM, then instead of reset, set the prio acco to macro prio */
  else if ( (lprio_data_tech < CXM_TECH_MAX) && (!IS_GSM_TECH(lprio_data_tech)) )
  {
    cxm_update_prio_table(lprio_data_tech, 1, 0, PRIO_TABLE_RESET_PREV);

    /* Reset the lprio_data fields */
    lprio_data_tech = CXM_TECH_MAX;
    lprio_data_sub = MAX_NUM_OF_SUBSCRIPTIONS;
  }
  else /* We are not in data+MMS and the prio has been reset to original...
          just need to ensure GSM stack prios are updated as per macro prio */
  {
    /* GSM1 will always be mapped to multimode sub */
    /* GSM2/GSM3 will always be mapped to non-multimode subscription from gsm_id_list */
    lmtsmgr.curr_prio_offset++;
    lmtsmgr.curr_prio_offset %= LMTSMGR_NUM_GSM_PRIO_SETS;
    for (index = 0; index < sp_prio_list.num; index++)
    {
      sub = sp_prio_list.order[index].sub;
      if ((sub < (sys_modem_as_id_e_type)0) || (sub >= MAX_NUM_OF_SUBSCRIPTIONS))
      {
        LMTSMGR_MSG_2(FATAL,"Incorrect Sub# %d received on index %d",
                             sub, index);
        continue;
      }

      cxm_update_prio_table(lmtsmgr.gsm_id_list[sub], index, 
                            lmtsmgr.curr_prio_offset, PRIO_TABLE_MACRO_OFFSET_BASED);

      lprio_data_tech = CXM_TECH_MAX;
      lprio_data_sub = MAX_NUM_OF_SUBSCRIPTIONS;
    }
  }
}


/*=======================================================================
FUNCTION LMTSGR_UPD_PRIO_TBL_ON_TX_STATE
    
DESCRIPTION
  This function will update client prio table when there is a change in
  Tx state. This is required to take care of IRAT handovers during an 
  active data call

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/
void lmtsmgr_upd_prio_tbl_on_tx_state
(
  cxm_tech_type   tech,
  boolean         is_tx_on
)
{
  sys_modem_as_id_e_type         sub;
  wwcoex_prio_table_upd_type data_tbl_action;

  /* No need to update prio table if there is no low prio data sub active */
  if (lprio_data_sub == MAX_NUM_OF_SUBSCRIPTIONS)
  {
    return;
  }

  if (is_tx_on == TRUE)
  {
    /* Tx is turning ON... check if its on the same sub as lprio data */
    sub = lmtsmgr_get_sub_id(tech);
    if ( (sub == lprio_data_sub) && (lprio_data_tech == CXM_TECH_MAX) )
    {
      lprio_data_tech = tech;
      data_tbl_action = lmtsmgr_get_data_tbl_action(lprio_data_tech);
      cxm_update_prio_table(lprio_data_tech, 1, 0, data_tbl_action );
    }
  }
  /* If Tx is turning OFF, check if it is the lprio tech is the one turning OFF*/
  else if (lprio_data_tech == tech)
  {
    /* Reset the table to original */
    cxm_update_prio_table(lprio_data_tech, 1, 0, PRIO_TABLE_RESET_PREV);

    /* Reset the lprio_tech field... keep the lprio_sub field as is since we can have 
       a RAT handover */
    lprio_data_tech = CXM_TECH_MAX;
  }

  return;
}

void lmtsmgr_subs_prio_callback(sp_subscription_priority_list  subs_prio_list, 
                                                   sp_client_enum_t client_id)
{
  if (client_id < SP_MAX_CLIENT)
  {
    /* Enter Critical Section */
    LMTSMGR_ENTER_CRIT_SECT(lmtsmgr.crit_sect);

    lmtsmgr.subs_prio_list = subs_prio_list;

    lmtsmgr_set_sigs(LMTSMGR_SAR_CALL_BACK_SIG);

    /* Leave Critical Section */
    LMTSMGR_LEAVE_CRIT_SECT(lmtsmgr.crit_sect);
  }
  else
  {
    LMTSMGR_MSG_1(ERROR, "lmtsmgr_subs_prio_callback: client_id (%d) not valid ", client_id);
  }

}

void lmtsmgr_sp_callback_process_input(void)
{
  uint8 index = 0;

  LMTSMGR_MSG_2(HIGH, "Subs-prio->SAR: num: %d, activeMask: %d", 
                       lmtsmgr.subs_prio_list.num, 
                       lmtsmgr.subs_prio_list.active_tech_info.active_subscription_mask);
  for (index = 0; index < lmtsmgr.subs_prio_list.num; index++)
  {
    LMTSMGR_MSG_3(HIGH, "Subs-prio->SAR: num: %d, techMask[%d] %d", 
                       lmtsmgr.subs_prio_list.order[index].sub, index, 
                       lmtsmgr.subs_prio_list.active_tech_info.tech_supported_bitmask[index]);
  }

  /* Update Tech ASID mapping from Subs-Prio client to CXM clients */
  lmtsmgr_update_tech_asid_map();

  /* Update client priority tables of registered GSM clients */
  lmtsmgr_update_client_prio_table();
  
  /* Process Subs-Prio CallBack function inputs */
  lmtsmgr_process_sar_input();

}

void lmtsmgr_max_pwr_backoff(int16 *sar_plimit, uint8* num_valid_sar, cxm_tech_type tech)
{
  uint8 index = 0;

  /* If tech is GSM, SAR pwr limit should be sent for array of 10 */
  if (IS_GSM_TECH(tech))
  {
    for (index = 0; index < SAR_GSM_MAX_LIMIT_TYPE; index++)
    {
      sar_plimit[index] = DEFAULT_MAX_PLIMIT;
    }
    *num_valid_sar = SAR_GSM_MAX_LIMIT_TYPE;
  }
  else
  {
    sar_plimit[index] = DEFAULT_MAX_PLIMIT;
    *num_valid_sar = 1;
  }

}

/*=======================================================================
FUNCTION LMTSMGR_SET_TECH_BACKOFF_LIMITS_MAX
    
DESCRIPTION
  This function will set MAX limits for the Backoff for a tech

DEPENDENCIES
  None
    
RETURN VALUE
  None

SIDE EFFECTS
  None
========================================================================*/

static void lmtsmgr_set_tech_backoff_limits_max(cxm_tech_type tech)
{
  uint8 index = 0;
  lmtsmgr_power_info_type *pwr_info_t1;

  pwr_info_t1 = &lmtsmgr.tech_state[tech].pwr_info;

  lmtsmgr_max_pwr_backoff(pwr_info_t1->new_limits.sar_plimit,
                            &pwr_info_t1->new_limits.num_valid_sar, tech);
  for (index=0; index<CXM_TECH_MAX; index++)
  {
    pwr_info_t1->new_limits.coex_plimit[index] = DEFAULT_MAX_PLIMIT;
    pwr_info_t1->new_limits.coex_slot_plimit[index] = DEFAULT_MAX_PLIMIT;
  }
  pwr_info_t1->new_limits.batt_plimit = DEFAULT_MAX_PLIMIT;
  lmtsmgr_notify_tech_plimit(tech);
}


static sys_sys_mode_e_type lmtsmgr_core_map_tech_id 
(
  cxm_tech_type tech
)
{

  switch (tech)
  {
    case CXM_TECH_ONEX:
        return SYS_SYS_MODE_CDMA;

    case CXM_TECH_GSM1:
    case CXM_TECH_GSM2:
    case CXM_TECH_GSM3:
        return SYS_SYS_MODE_GSM;

    case CXM_TECH_LTE:
        return SYS_SYS_MODE_LTE;

    case CXM_TECH_TDSCDMA:
        return SYS_SYS_MODE_TDS;

    case CXM_TECH_HDR:
        return SYS_SYS_MODE_HDR;
    
    case CXM_TECH_WCDMA:
        return SYS_SYS_MODE_WCDMA;

    default:
        LMTSMGR_MSG_1(ERROR, "Tech id %d not valid", tech);
        return SYS_SYS_MODE_NONE;
  }

}

static void lmtsmgr_reset_order_list (void)
{
#ifdef FEATURE_MODEM_DDR_MGMT
  uint8 index;

  lmtsmgr.num_order = 0;
  for (index = 0; index < (uint8)CXM_TECH_MAX; index++)
  {
    lmtsmgr.order[index] = CXM_TECH_DFLT_INVLD;
  }
#endif
}

static cxm_tech_type lmtsmgr_trm_client_mapping 
(
  trm_client_enum_t client    /* Client's whose state update is sent */
)
{
  switch (client)
  {
    case TRM_LTE:
        return CXM_TECH_LTE;

    case TRM_TDSCDMA:
        return CXM_TECH_TDSCDMA;

    case TRM_1X:
        return CXM_TECH_ONEX;

    case TRM_HDR:
        return CXM_TECH_HDR;

    case TRM_GSM1:
        return CXM_TECH_GSM1;

    case TRM_UMTS:
    case TRM_WCDMA:
        return CXM_TECH_WCDMA;

    case TRM_GSM2:
        return CXM_TECH_GSM2;

    case TRM_GSM3:
        return CXM_TECH_GSM3;

    default:
        return CXM_TECH_DFLT_INVLD;
  }

}

#ifdef FEATURE_MODEM_DDR_MGMT
static cxm_tech_type lmtsmgr_trm_client_div_mapping
(
  trm_client_enum_t client    /* Client's whose state update is sent */
)
{
  switch (client)
  {
    case TRM_LTE_SECONDARY:
        return CXM_TECH_LTE;

    case TRM_TDSCDMA_SECONDARY:
        return CXM_TECH_TDSCDMA;

    case TRM_1X_SECONDARY:
        return CXM_TECH_ONEX;

    case TRM_HDR_SECONDARY:
        return CXM_TECH_HDR;

    case TRM_GSM_SECONDARY:
                /* TODO: GSM2/GSM3 */
        return CXM_TECH_GSM1;

    case TRM_UMTS_SECONDARY:
        return CXM_TECH_WCDMA;

    default:
        return CXM_TECH_DFLT_INVLD;
  }
}

void lmtsmgr_insert_in_order_list
(
  /* The Lmtsmgr client id */
  cxm_tech_type               client_id,
  uint8 priority          /* Priority of client request */
)
{
  /* Order loop position index */
  uint32                          pos;

  /* Client just before position "pos" */
  cxm_tech_type               c;


/*--------------------------------------------------------------------------*/

  /* If Priority is Valid INSERT */
  if ( priority != 0 )
  {

    /* Find client in the ordered list.  Remove it, if present */
    for( pos = 0;  pos < lmtsmgr.num_order; pos++ )
    {
      if ( lmtsmgr.order[pos] == client_id )
      {
        /* It is present!  Remove it from the list ... */
        lmtsmgr.num_order--;;
  
        /* ... by shifting all entries after it one position forward */
        while ( pos < lmtsmgr.num_order )
        {
          lmtsmgr.order[ pos ] = lmtsmgr.order[ pos+1 ];
          pos++;
        }
        break;
      }
    }

    /* Move clients with a lower priority one position backwards */
    for ( pos = lmtsmgr.num_order;  pos > 0 ;  pos-- )
    {
      /* Get the client just before the current position */
      c = lmtsmgr.order[ pos-1 ];

      /* When we find a higher priority client ... */
      if ((LMTSMGR_IS_TECH_VALID(c))&&( lmtsmgr.tech_state[ c ].priority >= priority ))
      {
        /* ... stop shifting */
        break;
      }

      /* Shift backwards one position in list, to current position */
      lmtsmgr.order[ pos ] = c;
    }

    /* Insert client in the newly created hole in priority order list */
    lmtsmgr.order[ pos ] = client_id;
    lmtsmgr.num_order++;
  }
  /* If Priority is NOT Valid than REMOVE */
  else
  {
    /* Find client in the ordered list.  Remove it, if present */
    for( pos = 0;  pos < lmtsmgr.num_order; pos++ )
    {
      if ( lmtsmgr.order[ pos ] == client_id )
      {
        /* It is present!  Remove it from the list ... */
        lmtsmgr.num_order--;
  
        /* ... by shifting all entries after it one position forward */
        while ( pos < lmtsmgr.num_order )
        {
          lmtsmgr.order[ pos ] = lmtsmgr.order[ pos+1 ];
          pos++;
        }
        break;
      }
    }
  }
} /* TRMClientOrderArray::insert( trm_client_enum_t ) */

#endif  /* DDR FEATURE */

void lmtsmgr_trm_state_update_callback 
(
  trm_state_info_type trm_state_info
)
{
#ifdef FEATURE_MODEM_DDR_MGMT
  cxm_tech_type client_id;
  lmtsmgr_tech_state_type *tech_st;
#endif

  cxm_chain_owner_msg_s chain_own_msg;
  int i;
  
  /* Check input parameters */
  if (trm_state_info.client < TRM_1X && 
      trm_state_info.client > TRM_LAST_CLIENT)
  {
    return;
  }

  /* If the chain owner has changed, send a msgr message to Limitsmgr task */
  if (trm_state_info.chain_owner_changed == TRUE)
  {
    /* Remap TRM client to CXM client */
    for (i=0; i<TRM_MAX_CHAINS; i++)
    {
      chain_own_msg.owner.chain[i] = lmtsmgr_trm_client_mapping(
                                               trm_state_info.chain_owner[i] );
    }

    lmtsmgr_msgr_send_msg(&chain_own_msg.msg_hdr, MCS_CXM_CHAIN_OWNER_UPDATE_IND, sizeof(chain_own_msg));
  }

#ifdef FEATURE_MODEM_DDR_MGMT
  /* Enter Critical Section */
  LMTSMGR_ENTER_CRIT_SECT(lmtsmgr.crit_sect);

  client_id = lmtsmgr_trm_client_mapping(trm_state_info.client);
  if (LMTSMGR_IS_TECH_VALID(client_id))
  {
    tech_st = &lmtsmgr.tech_state[client_id];
    tech_st->reason = trm_state_info.reason;
    lmtsmgr_update_tech_band(client_id, trm_state_info.band);
    tech_st->priority = trm_state_info.priority;
    lmtsmgr_insert_in_order_list(client_id, trm_state_info.priority);

    LMTSMGR_MSG_3(LOW, "reason %d band %d priority %d ", 
                        trm_state_info.reason, trm_state_info.band, trm_state_info.priority);

    /* Call DDR coex process function */
    lmtsmgr_process_ddr_coex_input();
  }
  else
  {
     /* Check for Diversity */
     client_id = lmtsmgr_trm_client_div_mapping(trm_state_info.client);
     if (LMTSMGR_IS_TECH_VALID(client_id))
     {
        tech_st = &lmtsmgr.tech_state[client_id];
        tech_st->div_hold_prev = tech_st->div_hold_cur;
        if (trm_state_info.reason == TRM_NUM_REASONS)
        {
          tech_st->div_hold_cur = FALSE;
        }
        else
        {
          tech_st->div_hold_cur = TRUE;
        }

        LMTSMGR_MSG_2(MED, "CXM Client: %d Diversity On: %d", 
                      client_id, tech_st->div_hold_cur);

        /* Call DDR coex process function */
        lmtsmgr_process_ddr_coex_input();
     }

     /* Check for GPS */
     if (trm_state_info.client == TRM_GPS)
     {
        if (trm_state_info.reason == TRM_NUM_REASONS)
        {
           lmtsmgr.gps_tech_state_on = FALSE;
        }
        else
        {
           lmtsmgr.gps_tech_state_on = TRUE;
        }

        LMTSMGR_MSG_1(MED, "GPS On: %d", lmtsmgr.gps_tech_state_on);

         /* Call DDR coex process function */
        lmtsmgr_process_ddr_coex_input();
     }
  }

  /* Leave Critical Section */
  LMTSMGR_LEAVE_CRIT_SECT(lmtsmgr.crit_sect);
#endif
}

static boolean lmtsmgr_validate_hopping_para_changed 
(
  trm_modify_hop_behavior_input_type* hop
)
{
  if ((hop != NULL) && ((hop->disable_hop != lmtsmgr.hop_input.disable_hop) ||
      (hop->hop_type != lmtsmgr.hop_input.hop_type)))
  {
    return TRUE;
  }

  return FALSE;
}

/*=============================================================================

  FUNCTION:  lmtsmgr_update_hopping_behavior

=============================================================================*/
/*!
    @brief
    Checks Techs behavior and Calls TRM API to update 
    whether Idle Mode Hopping is to be enabled
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_update_hopping_behavior (void)
{
  uint8 index, dlindex;
  /* By Default always disable Hopping */
  trm_modify_hop_behavior_input_type temp = {FALSE , TRM_HOPPING_TYPE_IDLE_MODE};
  lmtsmgr_tech_state_type *gsm_st;
  uint32 gsm_num_dlfreqs;
  cxm_tech_type tech1;

  /* Whether LTE is in Data Call and SVLTE is active */
  if (LMTSMGR_IS_TECH_UL_ACTIVE(CXM_TECH_LTE) && LMTSMGR_IS_SVLTE_FEATURE_MODE())
  {
    for (tech1 = (CXM_TECH_MAX-1); tech1 >= 0; tech1--)
    {
      /* Check it's GSM tech and GSM in IDLE mode */
      if (IS_GSM_TECH(tech1) &&
          (!LMTSMGR_IS_TECH_UL_ACTIVE(tech1) && LMTSMGR_IS_TECH_ACTIVE(tech1)))
      {
        gsm_st = &lmtsmgr.tech_state[tech1];
        gsm_num_dlfreqs = gsm_st->currList.num_dl_entries;
        /*LMTSMGR_MSG_1(MED, "lmtsmgr_update_hopping_behavior: pwr lmt %d", 
                   lmtsmgr.idle_mode_hopping_rxpwr_lmt);*/
        for (index = 0; index < gsm_num_dlfreqs; index++)
        {
          dlindex = gsm_st->currList.dl_freqList[index];
          LMTSMGR_MSG_3(HIGH, "lmtsmgr_update_hopping_behavior: pwr lmt %d pwr %d, type %d", 
                   lmtsmgr.idle_mode_hopping_rxpwr_lmt, gsm_st->currList.links[dlindex].powerInfo.rx_power,
                   gsm_st->currList.links[dlindex].freqInfo.link_info.type);
          /* Determine if dl entry is NOT diveristy/PM  AND Rx Power is < -85 dBm (or EFS updated)  AND
                       Rx Power should only be applicable if There is Power update from GSM, else we will ignore Hopping check */
          if ((gsm_st->currList.links[dlindex].freqInfo.link_info.type != CXM_LNK_TYPE_DIVERSITY) && 
              (gsm_st->currList.links[dlindex].freqInfo.link_info.type != CXM_LNK_TYPE_POWER_MONITOR) &&
              (gsm_st->currList.links[dlindex].powerInfo.rx_power != CXM_UNKNOWN_POWER) &&
              (gsm_st->currList.links[dlindex].powerInfo.rx_power < lmtsmgr.idle_mode_hopping_rxpwr_lmt))
          {
            /* Disable Hopping */
            temp.disable_hop = TRUE;
            temp.hop_type = TRM_HOPPING_TYPE_IDLE_MODE;
            break;
          }
        }
        if (temp.disable_hop) // Break from loop once we Found GSM in Idle mode hopping scenario 
        break;
      }
    }
  }

  if (lmtsmgr_validate_hopping_para_changed(&temp))
  {
    LMTSMGR_MSG_1(HIGH, "lmtsmgr_update_hopping_behavior: disable_hop %d", temp.disable_hop);
    trm_modify_hopping_behavior(&temp);
    lmtsmgr.hop_input.disable_hop = temp.disable_hop;
    lmtsmgr.hop_input.hop_type = temp.hop_type;
  }
}


/*=============================================================================

  FUNCTION:  lmtsmgr_core_get_sub_based_priority

=============================================================================*/
/*!
    @brief
    Returns priority based on the SUB priority order list from subs_prio module
    This shall be used when the 2 concurrent active techs are on different SUBs
 
    @return
    Priority tier
*/
/*===========================================================================*/
lmtsmgr_priority_type lmtsmgr_core_get_sub_based_priority
(
  cxm_tech_type tech
)
{
  sp_subscription_priority_list  sp_prio_list;
  sp_active_subscription_info*   active;
  uint8                          index = 0;
  lmtsmgr_priority_type          tier_ret = LMTSMGR_TIER_MAX;
  sys_modem_as_id_e_type         sub;

  /* Enter Critical Section */
  LMTSMGR_ENTER_CRIT_SECT(lmtsmgr.crit_sect);

  sp_prio_list = lmtsmgr.subs_prio_list;
  active = &sp_prio_list.active_tech_info;

  LMTSMGR_DBG_MSG_1(LOW, "sp_prio_list.num: %d", sp_prio_list.num);

    switch (tech)
    {
      case CXM_TECH_ONEX:
      case CXM_TECH_WCDMA:
      case CXM_TECH_LTE:
      case CXM_TECH_TDSCDMA:
      case CXM_TECH_HDR:
        /* Check if it is NOT GSM only sub as other techs shold be mapped to multimode subscription */
        for (index = 0; index < sp_prio_list.num; index++)
        {
          sub = sp_prio_list.order[index].sub;
          if ((sub < (sys_modem_as_id_e_type)0) || (sub >= MAX_AS_IDS))
          {
             LMTSMGR_MSG_2(FATAL,"Incorrect Sub# %d received on index %d",
                           sub, index);
             continue;
          }

          if ((sub == sp_prio_list.tech_asid_map.multimode_as_id) && 
               (SP_CONV_ENUM_TO_BIT_MASK(lmtsmgr_core_map_tech_id(tech)) &
                                   active->tech_supported_bitmask[sub]))
          {
            tier_ret = (lmtsmgr_priority_type)index;
      
            LMTSMGR_MSG_2(LOW, "get_priority: tech: %d, tier_ret: %d ", tech, tier_ret);
            break;
          }
        }
        break;
      
      case CXM_TECH_GSM1:
      case CXM_TECH_GSM2:
    #ifdef FEATURE_TRIPLE_SIM
      case CXM_TECH_GSM3:
    #endif
        /* GSM1 will always be mapped to multimode sub */
        /* GSM2/GSM3 will always be mapped to non-multimode subscription from gsm_id_list */
        for (index = 0; index < sp_prio_list.num; index++)
        {
          sub = sp_prio_list.order[index].sub;
          if ((sub < (sys_modem_as_id_e_type)0) || (sub >= MAX_AS_IDS))
          {
             LMTSMGR_MSG_2(FATAL,"Incorrect Sub# %d received on index %d",
                           sub, index);
             continue;
          }
          
          if (lmtsmgr.gsm_id_list[sub] == tech)
          {
            tier_ret = (lmtsmgr_priority_type)index;
            if (LMTSMGR_TIER_MAX == tier_ret)
            {
              tier_ret = LMTSMGR_TIER_2;
            }
            break;
          }
        }
        break;

      default:
        LMTSMGR_MSG_1(ERROR, "Tech id %d not valid", tech);
    }

  /* Leave Critical Section */
  LMTSMGR_LEAVE_CRIT_SECT(lmtsmgr.crit_sect);

  return tier_ret;
  }

/*=============================================================================

  FUNCTION:  lmtsmgr_core_get_same_sub_priority

=============================================================================*/
/*!
    @brief
    Returns a fixed priority for a tech on the basis that it is on the same
    SUB as the other concurrent active tech
 
    @return
    Priority tier
*/
/*===========================================================================*/
lmtsmgr_priority_type lmtsmgr_core_get_same_sub_priority
(
  cxm_tech_type tech
)
  {
  lmtsmgr_priority_type tier_ret;

    /* Determine priority for this transmitter 
       For now, priority is based on only tech. So GSM > LTE and GSM > TDSCDMA.
       But in the future we might have to look at the entire tech state or 
       extract priority from upper layers or APSS.
    */
    switch (tech)
    {
      case CXM_TECH_ONEX:
      case CXM_TECH_GSM1:
      case CXM_TECH_GSM2:
      case CXM_TECH_GSM3:
       tier_ret = LMTSMGR_TIER_1;
       break;
    
      case CXM_TECH_LTE:
      case CXM_TECH_TDSCDMA:
      case CXM_TECH_HDR:
       tier_ret = LMTSMGR_TIER_2;
       break;
      
      case CXM_TECH_WCDMA:
      default:
       tier_ret = LMTSMGR_TIER_MAX;
       break;
    }

  return tier_ret;
  }

/*=============================================================================
  
  FUNCTION:  lmtsmgr_core_get_priority

=============================================================================*/
/*!
    @brief
    Updates the priorities for the two simultaneously active techs
 
    @return
    None
*/
/*===========================================================================*/
boolean lmtsmgr_core_get_priority 
(
  lmtsmgr_core_prio_table_type *tech1_prio,
  lmtsmgr_core_prio_table_type *tech2_prio
)
{
  if ( (tech1_prio == NULL) || (tech1_prio == NULL) )
  {
    LMTSMGR_MSG_2(FATAL, "Incorrect args: ptr1 0x%x, ptr2 0x%x", 
                  tech1_prio, tech1_prio);
    return FALSE;
  }

  /* Are the 2 techs on diff SUB ? */
  if (LMTSMGR_CHECK_DIFF_SUB(tech1_prio->tech, tech2_prio->tech))
  { 
    tech1_prio->pri = lmtsmgr_core_get_sub_based_priority(tech1_prio->tech);
    tech2_prio->pri = lmtsmgr_core_get_sub_based_priority(tech2_prio->tech);
  }
  else
  {
    tech1_prio->pri = lmtsmgr_core_get_same_sub_priority(tech1_prio->tech);
    tech2_prio->pri = lmtsmgr_core_get_same_sub_priority(tech2_prio->tech);
  }
  
  return TRUE;
}

static void lmtsmgr_insert_rec_in_tier
(
  lmtsmgr_core_prio_table_type tier[],
  uint32 *num_entries,
  lmtsmgr_core_prio_table_type* new_rec  
)
{
  uint32 pos;

  /* Move clients with a lower priority one position backwards */
  for ( pos = *num_entries ;  pos > 0 ;  pos-- )
  {
    /* Shift backwards one position in list, to current position */
    tier[ pos ] = tier[pos - 1];
  }

  /* Insert client in the newly created hole in priority order list */
  memscpy( (void *)&tier[pos], 
           sizeof(lmtsmgr_core_prio_table_type),
           (void *)new_rec, 
           sizeof(lmtsmgr_core_prio_table_type) );

  (*num_entries)++;

}


void lmtsmgr_insert_in_table
(
  lmtsmgr_core_prio_table_type* new_rec
)
{
  switch (new_rec->pri)
  {
    case LMTSMGR_TIER_1:
      /* This should go in the high tier */
      
      // Check if it has some space and if the tech is the same 
      if ( (lmtsmgr_core_tiers.num_tier1_entries < LMTSMGR_MAX_TIER_TECHS) &&
           ( (lmtsmgr_core_tiers.tier1[0].tech == CXM_TECH_MAX) ||
             (lmtsmgr_core_tiers.tier1[0].tech == new_rec->tech) ) )
      {
        //Insert this new record
        lmtsmgr_insert_rec_in_tier( lmtsmgr_core_tiers.tier1, 
                                &lmtsmgr_core_tiers.num_tier1_entries,
                                new_rec);
      }

      break;

    case LMTSMGR_TIER_2:
      /* This should go in the high tier */
      
      // Check if it has some space and if the tech is the same 
      if ( (lmtsmgr_core_tiers.num_tier2_entries < LMTSMGR_MAX_TIER_TECHS) &&
           ( (lmtsmgr_core_tiers.tier2[0].tech == CXM_TECH_MAX) ||
             (lmtsmgr_core_tiers.tier2[0].tech == new_rec->tech) ) )
      {
        //Insert this new record
        lmtsmgr_insert_rec_in_tier( lmtsmgr_core_tiers.tier2, 
                                &lmtsmgr_core_tiers.num_tier2_entries,
                                new_rec);
      }

      break;

    case LMTSMGR_TIER_MAX:
      break;
  }
}

static void lmtsmgr_clear_table(void)
{
  int i;

  for (i=0; i<LMTSMGR_MAX_TIER_TECHS; i++)
  {
    lmtsmgr_core_tiers.tier1[i].tech = CXM_TECH_MAX;
    lmtsmgr_core_tiers.tier2[i].tech = CXM_TECH_MAX;
  }
  lmtsmgr_core_tiers.num_tier1_entries = 0;
  lmtsmgr_core_tiers.num_tier2_entries = 0;
}

/*=============================================================================

  FUNCTION:  lmtsmgr_map_sys_bands_to_rf_bands

=============================================================================*/

uint32 lmtsmgr_map_sys_bands_to_rf_bands 
(
  uint32 tech_band,
  cxm_tech_type tech_id
)
{

  switch (tech_id)
  {
    case CXM_TECH_LTE:
      if ((tech_band >= SYS_BAND_LTE_EUTRAN_BAND1) &&
            (tech_band <= SYS_BAND_LTE_EUTRAN_BAND43))
      {
        return ((uint32)(tech_band-SYS_BAND_LTE_EUTRAN_BAND1));
      }
      else
      {
        LMTSMGR_MSG_1(ERROR, "LTE band %d not present in SYS_BAND", tech_band);
        return (uint32)RFCOM_BAND_LTE_INVALID;
      }
    break;

    case CXM_TECH_TDSCDMA:
      switch ((sys_band_class_e_type)tech_band)
      {
        case SYS_BAND_TDS_BANDA:
          return ((uint32)RFCOM_BAND_TDSCDMA_B34);
        case SYS_BAND_TDS_BANDF:
          return ((uint32)RFCOM_BAND_TDSCDMA_B39);
        case SYS_BAND_TDS_BANDE:
          return ((uint32)RFCOM_BAND_TDSCDMA_B40);
                  
        default:
          LMTSMGR_MSG_1(ERROR, "TDSCDMA band %d not present in SYS_BAND", tech_band);
          return (uint32)RFCOM_BAND_TDSCDMA_INVALID;
      }
    break;

    case CXM_TECH_GSM1:
    case CXM_TECH_GSM2:
    case CXM_TECH_GSM3:
      switch ((sys_band_class_e_type)tech_band)
      {
        case SYS_BAND_GSM_850:
          return ((uint32)RFCOM_BAND_GSM850);
        case SYS_BAND_GSM_EGSM_900:
          return ((uint32)RFCOM_BAND_GSM900);
        case SYS_BAND_GSM_DCS_1800:
          return ((uint32)RFCOM_BAND_GSM1800);
        case SYS_BAND_GSM_PCS_1900:
          return ((uint32)RFCOM_BAND_GSM1900);
                  
        default:
          LMTSMGR_MSG_1(ERROR, "GSM band %d not present in SYS_BAND", tech_band);
          return (uint32)RFCOM_NUM_GSM_BANDS;
      }
    break;

    case CXM_TECH_ONEX:
    case CXM_TECH_HDR:
      if ((tech_band >= SYS_BAND_BC0) &&
           (tech_band <= SYS_BAND_BC19))
      {
        return ((uint32)(tech_band-SYS_BAND_BC0));
      }
      else
      {
        LMTSMGR_MSG_2(ERROR, "Tech %d band %d not present in SYS_BAND", 
                      tech_id, tech_band);

        return (uint32)RFCOM_NUM_1X_BANDS;
      }
    break;

    case CXM_TECH_WCDMA:
      switch ((sys_band_class_e_type)tech_band)
      {
        case SYS_BAND_WCDMA_I_IMT_2000:
          return ((uint32)RFCOM_BAND_IMT);
        case SYS_BAND_WCDMA_II_PCS_1900:
          return ((uint32)RFCOM_BAND_1900);
        case SYS_BAND_WCDMA_III_1700:
          return ((uint32)RFCOM_BAND_BC3);
        case SYS_BAND_WCDMA_IV_1700:
          return ((uint32)RFCOM_BAND_BC4);
        case SYS_BAND_WCDMA_V_850:
          return ((uint32)RFCOM_BAND_800);
        case SYS_BAND_WCDMA_VIII_900:
          return ((uint32)RFCOM_BAND_BC8);
        case SYS_BAND_WCDMA_IX_1700:
          return ((uint32)RFCOM_BAND_BC9);
        case SYS_BAND_WCDMA_XI_1500:
          return ((uint32)RFCOM_BAND_BC11);
        case SYS_BAND_WCDMA_XIX_850:
          return ((uint32)RFCOM_BAND_BC19);
                  
        default:
          LMTSMGR_MSG_2(ERROR, "Tech %d band %d not present in SYS_BAND", 
                        tech_id, tech_band);
          return (uint32)RFCOM_BAND_INVALID;
      }
    break;

    default:
      LMTSMGR_MSG_1(ERROR, "Tech %d Invalid", tech_id);
      return (uint32)RFCOM_BAND_LTE_INVALID;
    break;
  }
}


/*=============================================================================

  FUNCTION:  lmtsmgr_map_tech_to_rf_mode

=============================================================================*/
/*!
    @brief
    Maps tech type to RF mode
 
    @return
    None
*/
rfm_mode_enum_type lmtsmgr_map_tech_to_rf_mode(cxm_tech_type tech_id)
{
  switch (tech_id) 
  {
    case CXM_TECH_GSM1:
    case CXM_TECH_GSM2:
      return RFCOM_GSM_MODE;

    case CXM_TECH_LTE:
      return RFM_LTE_MODE;

    //TODO: Add support for all techs
    default:
      LMTSMGR_MSG_1(ERROR, "Unsupported tech %d", tech_id);
      return RFM_INVALID_MODE;

  }
}

/*=============================================================================

  FUNCTION:  wwcoex_check_special_inds

=============================================================================*/
/*!
    @brief
    Checks if any special indications need to be sent
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_check_special_inds(void)
{
#ifndef FEATURE_BOLT_MODEM
  lmtsmgr_filter_path_type send_ind;

  send_ind.filtered_path_flag = FALSE;
  send_ind.gsm_aggr_filter_path_flag = FALSE;
#endif

  /* Has anything changed ? */
  if (lmtsmgr.special_ind_mask != lmgr_temp_ind_mask)
  {
    /* Save it as the current final state */
    lmtsmgr.special_ind_mask = lmgr_temp_ind_mask;

#ifdef FEATURE_BOLT_MODEM
    /* Has the filter path flag set ? */
    if (lmtsmgr.special_ind_mask & (1<<FILTERED_PATH))
    {
      lmtsmgr_send_filter_path_ind(TRUE);
    }
    else
    {
      lmtsmgr_send_filter_path_ind(FALSE);
    }
#else
    /* Has the filter path flag set ? */
    if (lmtsmgr.special_ind_mask & (1<<FILTERED_PATH))
    {
      send_ind.filtered_path_flag = TRUE;
    }
    if (lmtsmgr.special_ind_mask & (1<<FILTERED_PATH_GSM_TX_ONLY))
    {
      send_ind.gsm_aggr_filter_path_flag = TRUE;
    }
    lmtsmgr_send_filter_path_ind(send_ind);
#endif
  }
}

/*=============================================================================

  FUNCTION:  lmtsmgr_process_txstate_change

=============================================================================*/
/*!
    @brief
    Processes any change in the Tx on/off state for a tech
 
    @return
    None
*/
/*===========================================================================*/

void lmtsmgr_process_txstate_change
(
  cxm_tech_type   tech_id,
  uint32          prev_num_ul,
  uint32          new_num_ul
)
{
  int i;

  /* If the tech is invalid, return */
  if (tech_id >= CXM_TECH_MAX)
  {
    return;
  }

  /* Has the state changed ? */
  if ( ((prev_num_ul == 0) && (new_num_ul == 0)) ||
       ((prev_num_ul > 0) && (new_num_ul > 0)) )
  {
    return;
  }

  if (new_num_ul > 0)
  {
    // Tech just turning TX ON
    LMTSMGR_MSG_1(HIGH, "Received Tech %d Tx On", tech_id); 

    /* Reset backoff limits to MAX */
    lmtsmgr_max_pwr_backoff(lmtsmgr.tech_state[tech_id].pwr_info.new_limits.sar_plimit,
                            &lmtsmgr.tech_state[tech_id].pwr_info.new_limits.num_valid_sar, 
                            tech_id);
    lmtsmgr.tech_state[tech_id].pwr_info.new_limits.batt_plimit=DEFAULT_MAX_PLIMIT;
    for (i=0; i < CXM_TECH_MAX; i++)
    {
      lmtsmgr.tech_state[tech_id].pwr_info.new_limits.coex_plimit[i]=DEFAULT_MAX_PLIMIT;
      lmtsmgr.tech_state[tech_id].pwr_info.new_limits.coex_slot_plimit[i]=DEFAULT_MAX_PLIMIT;
    }


    if (!IS_WWAN_MITIGATION_DISABLED(lmtsmgr.mitigation_mask))
    {
      /* Request inactivity timeline registration */
      /* If tech has already registered, no need to send a request again */
      lmtsmgr_send_activity_info_req(tech_id, CXM_LNK_DRCTN_UL, TRUE);
    }

    /* update any prio table if required... */
    lmtsmgr_upd_prio_tbl_on_tx_state(tech_id, TRUE);
  }
  else
  {
    LMTSMGR_MSG_1(HIGH, "Received Tech %d Tx Off", tech_id); 

    /* Reset the state */
    lmtsmgr.tech_state[tech_id].pwr_info.tx_pwr_valid = FALSE;
       
    if (!IS_WWAN_MITIGATION_DISABLED(lmtsmgr.mitigation_mask))
    {
#ifdef FEATURE_MODEM_COEXISTENCE_FW

      /* Deregister inactivity timer for this tech */
      wwcoex_deregister_activity(tech_id);
#endif

      /* Request inactivity timeline deregistration */
      lmtsmgr_send_activity_info_req(tech_id, CXM_LNK_DRCTN_UL, FALSE);
    }

    /* update any prio table if required... */
    lmtsmgr_upd_prio_tbl_on_tx_state(tech_id, FALSE);
  }

  /* Notify limits to tech */
  lmtsmgr_notify_tech_plimit( tech_id );
}


/*=============================================================================

  FUNCTION:  lmtsmgr_process_sar_input

=============================================================================*/
/*!
    @brief
    Processes any change in the input variables for SAR/Batt
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_sar_input(void)
{
  lmtsmgr_core_prio_table_type pri_state[LMTSMGR_MAX_CONCURRENT_UL_TECHS]; 
  cxm_tech_type hprio_tech = CXM_TECH_MAX, lprio_tech = CXM_TECH_MAX;
  int i, cnt = 0;
  lmtsmgr_tech_state_type *tier1_tech, *tier2_tech, *tech_st;
  coex_freqid_link_s  tulinfo;

  sar_efs_lookup_in sar_input;
  sar_efs_lookup_out sar_output;

  batt_efs_lookup_in batt_input;
  batt_efs_lookup_out batt_output;

  uint8 subs_in_call = 0;
  memset((void*)sar_output.hplimits, DEFAULT_MAX_PLIMIT, sizeof(sar_output.hplimits));

  // Reset the priority table 
  lmtsmgr_clear_table();
  
  // Get the techs which are in UL state
  for (i=0; i<CXM_TECH_MAX; i++)
  {
    tech_st = &lmtsmgr.tech_state[i];
    if(tech_st->currList.num_ul_entries > 0)
    {
      pri_state[cnt].tech = i;
      tulinfo = tech_st->currList.links[tech_st->currList.ul_freqList[0]].freqInfo;
      pri_state[cnt].tx_rfdev = tulinfo.link_info.rf_device;
      pri_state[cnt].band = lmtsmgr_map_sys_bands_to_rf_bands(tulinfo.link_info.band, i);
      cnt++;
      if (cnt >= LMTSMGR_MAX_CONCURRENT_UL_TECHS)
      {
        break;
      }
    }
  }

  if (cnt >= LMTSMGR_MAX_CONCURRENT_UL_TECHS)
  {
    // Get priority of the two techs and insert in the table 
    (void)lmtsmgr_core_get_priority(&pri_state[0], &pri_state[1]);
    lmtsmgr_insert_in_table(&pri_state[0]);
    lmtsmgr_insert_in_table(&pri_state[1]);
  }
  else if (cnt == 1)
  {
    /* There is only 1 tech active so assume tier 1 */
    pri_state[0].pri = LMTSMGR_TIER_1;
    lmtsmgr_insert_in_table(&pri_state[0]);
  }
  else if (cnt == 0)
  {
    return ;
  }

      LMTSMGR_MSG_4(LOW, "tier1-tech: %d, pri: %d, tier2-tech: %d, pri: %d ",
                       lmtsmgr_core_tiers.tier1[0].tech,
                             lmtsmgr_core_tiers.tier1[0].pri,
                             lmtsmgr_core_tiers.tier2[0].tech,
                             lmtsmgr_core_tiers.tier2[0].pri);
      
      LMTSMGR_MSG_2(LOW, "num_tier1_entries: %d, num_tier2_entries: %d ",
                             lmtsmgr_core_tiers.num_tier1_entries,
                             lmtsmgr_core_tiers.num_tier2_entries);

  /* If there are entries in tier 1 but none in tier 2, provide a max backoff 
     to all tier 1 entries */
  if ( (lmtsmgr_core_tiers.num_tier1_entries > 0) && 
       (lmtsmgr_core_tiers.num_tier2_entries == 0) )
  {
    /* New limits for hprio tech will be MAX */
    hprio_tech = lmtsmgr_core_tiers.tier1[0].tech;

    /* Check if tech_id is valid */
    if (( hprio_tech <= CXM_TECH_DFLT_INVLD ) || ( hprio_tech >= CXM_TECH_MAX ))
    {
      LMTSMGR_MSG_1(ERROR, "Invalid tech %d", hprio_tech);
      return;
    }

    /* Send notification via NPA to therm service for BCL... */
    batt_npa_send_notification(hprio_tech, CXM_TECH_MAX);

    tier1_tech = &lmtsmgr.tech_state[hprio_tech];
    lmtsmgr_max_pwr_backoff(tier1_tech->pwr_info.new_limits.sar_plimit,
                            &tier1_tech->pwr_info.new_limits.num_valid_sar, hprio_tech);
    tier1_tech->pwr_info.new_limits.batt_plimit = DEFAULT_MAX_PLIMIT;
    lmtsmgr_notify_tech_plimit(hprio_tech);
  }
  else if ( (lmtsmgr_core_tiers.num_tier1_entries == 0) && 
            (lmtsmgr_core_tiers.num_tier2_entries > 0) )
  {
    /* New limits for lprio tech will be MAX */
    lprio_tech = lmtsmgr_core_tiers.tier2[0].tech;

    /* Check if tech_id is valid */
    if (( lprio_tech <= CXM_TECH_DFLT_INVLD ) || ( lprio_tech >= CXM_TECH_MAX ))
    {
      LMTSMGR_MSG_1(ERROR, "Invalid tech %d", lprio_tech);
      return;
    }

    /* Send notification via NPA to therm service for BCL... */
    batt_npa_send_notification(CXM_TECH_MAX, lprio_tech);

    tier2_tech = &lmtsmgr.tech_state[lprio_tech];
    lmtsmgr_max_pwr_backoff(tier2_tech->pwr_info.new_limits.sar_plimit,
                            &tier2_tech->pwr_info.new_limits.num_valid_sar, lprio_tech);
    tier2_tech->pwr_info.new_limits.batt_plimit = DEFAULT_MAX_PLIMIT;
    lmtsmgr_notify_tech_plimit(lprio_tech);
  }
  else if ( (lmtsmgr_core_tiers.num_tier1_entries > 0) && 
            (lmtsmgr_core_tiers.num_tier2_entries > 0) )
  {
    hprio_tech = lmtsmgr_core_tiers.tier1[0].tech;
    lprio_tech = lmtsmgr_core_tiers.tier2[0].tech;

    /* Check if tech_id is valid */
    if ((( hprio_tech <= CXM_TECH_DFLT_INVLD ) || ( hprio_tech >= CXM_TECH_MAX )) ||
         (( lprio_tech <= CXM_TECH_DFLT_INVLD ) || ( lprio_tech >= CXM_TECH_MAX )))
    {
      LMTSMGR_MSG_2(ERROR, "Invalid tech hprio: %d, lprio: %d", hprio_tech, lprio_tech);
      return;
    }

    /* Send notification via NPA to therm service for BCL only if both SUBs are in 
       CALL state ... */
    subs_in_call = 0;
    for (i = 0; i < lmtsmgr.subs_prio_list.num; i++)
    {
      if (IS_SUB_IN_CALL(lmtsmgr.subs_prio_list.order[i].state))
      {
        subs_in_call++;
      }
    }

    if ((lmtsmgr.subs_prio_list.num != 2)|| (subs_in_call == 2))
    {
      batt_npa_send_notification(hprio_tech, lprio_tech);
    }

    tier1_tech = &lmtsmgr.tech_state[hprio_tech];
    tier2_tech = &lmtsmgr.tech_state[lprio_tech];
    
    /* Do we have Tx power from hprio tech? */
    if (tier1_tech->pwr_info.tx_pwr_valid)
    {
      sar_input.hptech = hprio_tech;
      sar_input.hpband = lmtsmgr_core_tiers.tier1[0].band;
      sar_input.lptech = lprio_tech;
      sar_input.lpband = lmtsmgr_core_tiers.tier2[0].band;
      sar_input.tx_power = tier1_tech->pwr_info.tx_power;
      sar_input.dsi = sar_get_current_dsi();
      if (TRUE == sar_efs_lookup(&sar_input,&sar_output))
      {
        if(sar_output.num_hplimits > 0)
        {
          /* SAR limit for high priority tech */
          memscpy( (void *)&tier1_tech->pwr_info.new_limits.sar_plimit, 
                   sizeof(tier1_tech->pwr_info.new_limits.sar_plimit),
                   (void *)&sar_output.hplimits,
                   sizeof(sar_output.hplimits) );

          tier1_tech->pwr_info.new_limits.num_valid_sar = sar_output.num_hplimits;
        }
        else
        {
          /* No limit defined, set it to MAX */
          lmtsmgr_max_pwr_backoff(tier1_tech->pwr_info.new_limits.sar_plimit,
                                  &tier1_tech->pwr_info.new_limits.num_valid_sar, hprio_tech);
        }

        if(sar_output.lplimit_valid)
        {
          /* SAR limit for low priority tech */

          if (sar_output.num_lplimits == (SAR_GSM_MAX_LIMIT_TYPE))
          {
            for (i = 0; i < (SAR_GSM_MAX_LIMIT_TYPE); i++)
            {
                  tier2_tech->pwr_info.new_limits.sar_plimit[i] = sar_output.lplimit[i];
            }
                  tier2_tech->pwr_info.new_limits.num_valid_sar = SAR_GSM_MAX_LIMIT_TYPE;
          }
              else
              {
                tier2_tech->pwr_info.new_limits.sar_plimit[0] = sar_output.lplimit[0];
                  tier2_tech->pwr_info.new_limits.num_valid_sar = 1;
              }
        }
        else
        {
          /* No limit defined, set it to MAX */
          lmtsmgr_max_pwr_backoff(tier2_tech->pwr_info.new_limits.sar_plimit,
                                  &tier2_tech->pwr_info.new_limits.num_valid_sar, lprio_tech);
        }
      }
      else
      {
        /* No SAR limits defined for this band combination, set limit to MAX */
        lmtsmgr_max_pwr_backoff(tier1_tech->pwr_info.new_limits.sar_plimit,
                                &tier1_tech->pwr_info.new_limits.num_valid_sar, hprio_tech);
        lmtsmgr_max_pwr_backoff(tier2_tech->pwr_info.new_limits.sar_plimit,
                                &tier2_tech->pwr_info.new_limits.num_valid_sar, lprio_tech);
      }

      /* Check if there is a backoff to hprio tech due for limiting battery 
         current */
      batt_input.hptech = hprio_tech;
      batt_input.hpband = lmtsmgr_core_tiers.tier1[0].band;
      batt_input.lptech = lprio_tech;
      batt_input.lpband = lmtsmgr_core_tiers.tier2[0].band;
      if (TRUE == batt_efs_lookup(&batt_input,&batt_output))
      {
        if(batt_output.hplimit_valid)
        {
          /* Batt limit for high priority tech */
          tier1_tech->pwr_info.new_limits.batt_plimit = batt_output.hplimit;
        }
        else
        {
          /* No limit defined, set it to MAX */
          tier1_tech->pwr_info.new_limits.batt_plimit = DEFAULT_MAX_PLIMIT;
        }
      }
      
      /* Notify techs of the new pwr limits */
      lmtsmgr_notify_tech_plimit(lprio_tech);
      lmtsmgr_notify_tech_plimit(hprio_tech);
    }
    else
    {
      LMTSMGR_MSG_1(MED, "Have not recieved Txpower from tech %d yet...", hprio_tech);
    }
  }
  else /* Error case */
  {
    LMTSMGR_MSG_0(LOW, "No entries in either tiers");
  }  
}

/*=============================================================================

  FUNCTION:  lmtsmgr_coex_init

=============================================================================*/
/*!
    @brief
    Initialize coex variables
 
    @return
    None
*/
/*===========================================================================*/
static void lmtsmgr_coex_init(void)
{
  uint32 i;

  /* Check if the two sizes are same */
  LMTSMGR_COMPILE_ASSERT(CXM_MAX_SUPPORTED_LINK_SETS == WWCOEX_MAX_SUPPORTED_LINKS);
  LMTSMGR_COMPILE_ASSERT(CXM_UNKNOWN_FREQID == WWCOEX_UNKNOWN_FREQID);

#ifdef FEATURE_MODEM_DDR_MGMT
  /* If this is ASSERTed means update new tech in ddr_default_freq_plan_of_tech_type 
        in LimtsMgrNvDefinition.xml */
  LMTSMGR_COMPILE_ASSERT(CXM_TECH_MAX == 8);
#endif
  /* Set the freqId range for each tech... */
  for (i=0; i<CXM_TECH_MAX; i++)
  {
    lmtsmgr.tech_state[i].freqId_range_start = i * LMTSMGR_MAX_FREQIDS_PER_TECH + 1;
    lmtsmgr.tech_state[i].freqId_range_end = 
       lmtsmgr.tech_state[i].freqId_range_start + LMTSMGR_MAX_FREQIDS_PER_TECH;
  }

  /*! Initially assume we are in DSDA mode */
  wwcoex_init_tables(2,2);
  
  /* Read the mitigation mask */
  lmtsmgr.mitigation_mask = wwcoex_mitigation_mask_efs_lookup();
  lmtsmgr.en_mitigation_mask = wwcoex_mitigation_en_mask_efs_lookup();

  /* Read the Idle Mode Hopping Rx Power Limit */
  lmtsmgr.idle_mode_hopping_rxpwr_lmt = wwcoex_idle_mode_rxpwr_efs_lookup();

  LMTSMGR_MSG_2(HIGH, "Mitigation mask from EFS (disabled/enabled) is 0x%x 0x%x",
                lmtsmgr.mitigation_mask,
                lmtsmgr.en_mitigation_mask);

  wwcoex_init_inactivity_st();

  wwcoex_init_desense_stat();
}

/*=============================================================================

  FUNCTION:  lmtsmgr_init

=============================================================================*/
/*!
    @brief
    Initializes LMTSMGR and its global variables
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_init(void)
{
  cxm_tech_type i;
  uint32 efs_content;

  lmtsmgr_efs_init();

  wwan_coex_list_init();

  /* Reset the global struct */
  memset((void *)&lmtsmgr, 0, sizeof(lmtsmgr_struct));

  /* Creating a critial section *IS* a critical section */
  LMTSMGR_ENTER_ATOMIC_SECT();
    
  /* Initialize the Subs-Prio's critical section */
  LMTSMGR_INIT_CRIT_SECT(lmtsmgr.crit_sect);
  
  /* We have a critical section which can be used instead of INTLOCK'ing */
  LMTSMGR_LEAVE_ATOMIC_SECT();

  /* Reset the power limits for all technologies to TXSTOP */
  for (i=0; i < CXM_TECH_MAX; i++)
  {
    lmtsmgr_notify_tech_plimit(i);
  }

  /* Initialize the coex variables... */
  lmtsmgr_coex_init();

  /* Initialize the DSI service */
  sar_dsi_service_init();

  /*Register with Subscription Priority for priority list */
  sp_register_client(lmtsmgr_subs_prio_callback, SP_SAR);

  /* Initialize batt npa */
  batt_npa_init();
  ddr_coex_npa_init();
  lmtsmgr_reset_order_list();

  /* Update CXM Prio Table */
  cxm_update_prio_table_init();

  /* Store the action for prio table update from efs file */
  efs_content = wwan_coex_read_prio_update_efs();
  if (efs_content > 0)
  {
    lprio_data_tbl_action = PRIO_TABLE_SET_MIN_EXCLUDE_SUPER_CRIT;
  }
  else
  {
    lprio_data_tbl_action = PRIO_TABLE_SET_MIN;
  }

}

#ifdef FEATURE_MODEM_COEXISTENCE_SW

/*=============================================================================

  FUNCTION:  wwcoex_check_coex_with_dl

=============================================================================*/
/*!
    @brief
    Check a victim and/or aggressor UL frequency with all the victim DL
    frequencies. Also lookup into the efs tables and store the final action
    in the result table.
 
    @return
    TRUE
*/
/*===========================================================================*/
boolean wwcoex_check_coex_with_dl
(
  cxm_tech_type  tech1,
  cxm_tech_type  tech2,
  cxm_tech_type  vtech,
  cxm_tech_type  vtulindex,
  cxm_tech_type  atulindex,
  boolean        skip_im,
  wwcoex_tbl_type*  result_tbl,
  wwan_coex_efs_lookup_out* coex_output
)
{
  uint32 vtdlindex, tech1_index, tech2_index;
  lmtsmgr_tech_state_type* vt_st, *at_st;
  lmtsmgr_tech_link_type vtdl, atul, *vtul = NULL;
  cxm_tech_type atech;
  cxm_action_type action;
  lmtsmgr_core_prio_table_type tech1_prio_tbl,tech2_prio_tbl;

  /* Non-IM Freq compare argument structures */
  wwcoex_nonim_freqcompare_in  nonim_comp_in;
  wwcoex_nonim_freqcompare_out  nonim_comp_out;

   /* IM Freq compare argument structures */
  wwcoex_im_freqcompare_in      im_comp_in;
  wwcoex_im_freqcompare_out     im_comp_out;

  /* EFS look up input structures */
  wwcoex_blank_efs_lookup_in  lookup_in;

  wwan_coex_efs_lookup_in coex_input;
  cxm_tech_type hprio_tech;
  cxm_tech_type lprio_tech;

  if (coex_output == NULL)
  {
    LMTSMGR_MSG_0(FATAL, "ERROR:wwcoex_check_coex_with_dl coex_output NULL");
    return FALSE;
  }

  if (!LMTSMGR_IS_TECH_VALID(tech1) ||
     !LMTSMGR_IS_TECH_VALID(tech2) ||
     !LMTSMGR_IS_TECH_VALID(vtech))
  {
    LMTSMGR_MSG_3(FATAL, "ERROR:wwcoex_check_coex_with_dl tech input invalid %d %d %d",
                        tech1, tech2, vtech);
    return FALSE;
  }

  tech1_prio_tbl.tech = tech1;
  tech2_prio_tbl.tech = tech2;
  (void)lmtsmgr_core_get_priority (&tech1_prio_tbl, &tech2_prio_tbl);
  if (LMTSMGR_TIER_1 == tech1_prio_tbl.pri)
  {
    hprio_tech = tech1;
    lprio_tech = tech2;
  }
  else if (LMTSMGR_TIER_2 == tech1_prio_tbl.pri)
  {
    hprio_tech = tech2;
    lprio_tech = tech1;
  }
  else
  {
    LMTSMGR_MSG_3(FATAL,"Tech %d has invalid tier %d.. ignoring coex with tech %d",
                   tech1, tech1_prio_tbl.pri, tech2);
    return FALSE;
  }
  
  /* Initialize coex output/input */
  coex_output->aplimit_valid = FALSE;
  coex_output->aplimit = DEFAULT_MAX_PLIMIT;  
  coex_input.input_power = CXM_UNKNOWN_POWER;
  nonim_comp_out.action = ACTION_NONE;
  nonim_comp_out.desense = NO_DESENSE;
  nonim_comp_out.enh_action_mask = 0;
  im_comp_out.action = ACTION_NONE;
  im_comp_out.desense = NO_DESENSE;

  if (vtech == tech1)
  {
    atech = tech2;
  }
  else
  {
    atech = tech1;
  }

  vt_st = &lmtsmgr.tech_state[vtech];
  at_st = &lmtsmgr.tech_state[atech];

  /* Get pointers to UL freq and power info node */
  atul = at_st->currList.links[at_st->currList.ul_freqList[atulindex]];

  /* Dont need victim UL if IM needs to be skipped */
  if (skip_im == FALSE)
  {
    vtul = &vt_st->currList.links[vt_st->currList.ul_freqList[vtulindex]];
  }

  for (vtdlindex = 0; vtdlindex < vt_st->currList.num_dl_entries; vtdlindex++)
  {
    /* DL freq arr has ids into the currList freqinfo */
    vtdl = vt_st->currList.links[vt_st->currList.dl_freqList[vtdlindex]];

    /* for SS, ignore the div and PM frequencies */
    if ( (lmtsmgr.device_mode == SYS_MODEM_DEVICE_MODE_SINGLE_SIM) &&
         (vtdl.freqInfo.link_info.type > 0 ) )
    {
      continue;
    }

    /* Check for IM only if there is no emissions issue */
    im_comp_out.desense = NO_DESENSE;
    if (skip_im == FALSE)
    {
      /* Check IM with tech1 DL so tech1 as the victim */
      im_comp_in.vtech = vtech;
      im_comp_in.atech = atech;
      im_comp_in.vtulfreqinfo = vtul->freqInfo;
      im_comp_in.atulfreqinfo = atul.freqInfo;
      im_comp_in.vtdlfreqinfo = vtdl.freqInfo;

      LMTSMGR_DBG_MSG_3(LOW, "Debug:Checking IM for freqIds %u %u %u", 
                       atul.freqInfo.freqid, vtul->freqInfo.freqid, 
                       vtdl.freqInfo.freqid );

      (void)wwcoex_im_compare_freqs(&im_comp_in, &im_comp_out);
    }

    if ( (im_comp_out.desense != IM_TYPE1_DESENSE) &&
         (im_comp_out.desense != IM_TYPE2_DESENSE) &&
             (im_comp_out.desense != IM_DESENSE_GPS) )
    {
      /* There is no IM desense, check for any non-IM issue */

      LMTSMGR_DBG_MSG_4(LOW, "Debug:Checking Non-IM for tech %d, tech %d with freqIds %u %u", 
                      tech1, tech2, vtdl.freqInfo.freqid, atul.freqInfo.freqid );

      nonim_comp_in.vtech = vtech;
      nonim_comp_in.atech = atech;
      nonim_comp_in.vtdlfreqinfo = vtdl.freqInfo;
      nonim_comp_in.atulfreqinfo = atul.freqInfo;
      (void)wwcoex_nonim_compare_freqs(&nonim_comp_in, &nonim_comp_out);

      /* Check for any special indications*/
#ifdef FEATURE_BOLT_MODEM
      if ((nonim_comp_out.desense != NO_DESENSE) && lmtsmgr_is_on_seperate_dev(vtech, atech))
#else
      if (lmtsmgr_is_on_seperate_dev(vtech, atech))
#endif
      {
        lmgr_temp_ind_mask = MAX(lmgr_temp_ind_mask, 
                                 nonim_comp_out.special_ind_mask);
      }

      if ( nonim_comp_out.action != ACTION_BACKOFF )
      {
        if (( nonim_comp_out.desense != NO_DESENSE ) && ( nonim_comp_out.action != ACTION_NONE ))
        {
          lookup_in.vtech = vtech;
          lookup_in.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                          vtdl.freqInfo.link_info.band, 
                                          vtech);
          lookup_in.atech = atech;
          lookup_in.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                          atul.freqInfo.link_info.band,
                                          atech);
          lookup_in.victim_rxpower = vtdl.powerInfo.rx_power;
          lookup_in.victim_txpower = CXM_UNKNOWN_POWER;
          lookup_in.aggr_txpower = atul.powerInfo.tx_power;

          lookup_in.desense = nonim_comp_out.desense;
          action = wwcoex_blank_efs_lookup(&lookup_in);
        }
        else
        {
          /* There is no non-IM desense */
          action = ACTION_NONE;
          if (nonim_comp_out.special_ind_mask & (1<<FILTERED_PATH))
          {
            /* No desense in this combination, so no backoff required */
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
          }
        }

        /* Store the action in the result table...  */
        if (vtech == tech1)
        {
          tech2_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech1_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;     
        }
        else
        {
          tech1_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech2_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;
        }

        /* Store action/desense pair if action is required (ie do not 
           overwrite BLANKING with NONE */
        if (action == MAX(action, result_tbl->arr[tech1_index][tech2_index].action))
        {
          result_tbl->arr[tech1_index][tech2_index].action = action;
          result_tbl->arr[tech1_index][tech2_index].desense = nonim_comp_out.desense;
          result_tbl->is_valid = TRUE;

          LMTSMGR_DBG_MSG_4(MED, "Debug:Non-IM result stored for index %d %d action %d, desense %d", 
                    tech1_index, tech2_index, action, nonim_comp_out.desense );
        }
        else
        {
          LMTSMGR_DBG_MSG_4(MED, "Debug:Non-IM result IGNORED for index %d %d action %d, desense %d", 
                    tech1_index, tech2_index, action, nonim_comp_out.desense );
        }
      
      }
      else
      {
        if (NO_DESENSE == nonim_comp_out.desense)
        {
          /* No desense in this combination, so no backoff required */
          lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
          lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
        }
        else
        {
          /* Noise Desense */
          coex_input.desense = nonim_comp_out.desense;
          coex_input.input_power = vtdl.powerInfo.rx_power;
          coex_input.vtech = hprio_tech;
          coex_input.atech = lprio_tech;
          if (hprio_tech == vtech)
          {
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            vtdl.freqInfo.link_info.band,
                                            hprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            atul.freqInfo.link_info.band,
                                            lprio_tech);
          }
          else
          {
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            atul.freqInfo.link_info.band,
                                            hprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            vtdl.freqInfo.link_info.band,
                                            lprio_tech);
          }
          coex_input.vpriority = 2;
          coex_input.apriority = 1;
        }
      }
    }
    else if (vtul != NULL)
    {
      if ( im_comp_out.action != ACTION_BACKOFF )
      {
        /* There is IM in tech1 DL freq */
        lookup_in.vtech = vtech;
        lookup_in.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                        vtdl.freqInfo.link_info.band,
                                        vtech);
        lookup_in.atech = atech;
        lookup_in.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                        atul.freqInfo.link_info.band,
                                        atech);
        lookup_in.victim_rxpower = vtdl.powerInfo.rx_power;
        lookup_in.victim_txpower = vtul->powerInfo.tx_power;
        lookup_in.aggr_txpower = atul.powerInfo.tx_power;
        lookup_in.desense = im_comp_out.desense;
        action = wwcoex_blank_efs_lookup(&lookup_in);

        /* Store the action in the result table... */
        if (vtech == tech1)
        {
          tech2_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech1_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;     
        }
        else
        {
          tech1_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech2_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;
        }

         /* Store action/desense pair if action is required (ie do not 
           overwrite BLANKING with NONE */
        if (action == MAX(action, result_tbl->arr[tech1_index][tech2_index].action))
        {
          result_tbl->arr[tech1_index][tech2_index].action = action;
          result_tbl->arr[tech1_index][tech2_index].desense = im_comp_out.desense;
          result_tbl->is_valid = TRUE;

          LMTSMGR_DBG_MSG_4(MED, "Debug:IM result stored for index %d %d action %d, desense %d", 
                      tech1_index, tech2_index, action, im_comp_out.desense );
        }
        else
        {
          LMTSMGR_DBG_MSG_4(MED, "Debug:IM result IGNORED for index %d %d action %d, desense %d", 
                    tech1_index, tech2_index, action, im_comp_out.desense );
        }


        /* Also need to update the result for Tx-Tx conflict... */
        if (vtech == tech1)
        {
          tech2_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech1_index = vtul->freqInfo.freqid - vt_st->currList.freqId_offset;
        }
        else
        {
          tech1_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
          tech2_index = vtul->freqInfo.freqid - vt_st->currList.freqId_offset;
        }    

        /* For Tx - Tx result, if there is already a desense flag, 
          then dont change it */
        if (result_tbl->arr[tech1_index][tech2_index].desense == NO_DESENSE)
        {
          /* Action should be none for Tx-Tx conflict */
          result_tbl->arr[tech1_index][tech2_index].action = ACTION_NONE;
          result_tbl->arr[tech1_index][tech2_index].desense = im_comp_out.desense;
          result_tbl->is_valid = TRUE;

          LMTSMGR_DBG_MSG_4(LOW, "Debug:IM result stored for index %d %d action %d, desense %d", 
                      tech1_index, tech2_index, action, im_comp_out.desense );
        }
      }
      else
      {
        coex_input.desense = im_comp_out.desense;
        if (im_comp_out.desense == IM_TYPE2_DESENSE) /* Low prio tech is Victim */
        {
          /* IM Desense Low Prio tech */
          coex_input.vtech = lprio_tech;
          coex_input.atech = hprio_tech;
          if (lprio_tech == vtech)
          {
            coex_input.input_power = atul.powerInfo.tx_power;
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                           vtul->freqInfo.link_info.band,
                                           lprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                           atul.freqInfo.link_info.band,
                                           hprio_tech);
          }
          else
          {
            coex_input.input_power = vtul->powerInfo.tx_power;
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                           atul.freqInfo.link_info.band,
                                           lprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                           vtul->freqInfo.link_info.band,
                                           hprio_tech);
          }
          coex_input.vpriority = 1;
          coex_input.apriority = 2;
        }
        else if ( (im_comp_out.desense == IM_TYPE1_DESENSE) /* High prio tech is Victim */ ||
                      (im_comp_out.desense == IM_DESENSE_GPS) )
        {
          /* IM Desense High prio tech */
          coex_input.vtech = hprio_tech;
          coex_input.atech = lprio_tech;
          if (hprio_tech == vtech)
          {
            coex_input.input_power = vtul->powerInfo.tx_power;
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            vtul->freqInfo.link_info.band,
                                            hprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            atul.freqInfo.link_info.band,
                                            lprio_tech);
          }
          else
          {
            coex_input.input_power = atul.powerInfo.tx_power;
            coex_input.vband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            atul.freqInfo.link_info.band,
                                            hprio_tech);
            coex_input.aband = lmtsmgr_map_sys_bands_to_rf_bands(
                                            vtul->freqInfo.link_info.band,
                                            lprio_tech);
          }
          coex_input.vpriority = 2;
          coex_input.apriority = 1;
        }
      }
    }
    
    /* If power is unknown, then dont trigger backoffs... */
    if ( (coex_input.input_power != CXM_UNKNOWN_POWER ) &&
         ( ((im_comp_out.action == ACTION_BACKOFF) && IS_IM_DESENSE(im_comp_out.desense))||
           ((nonim_comp_out.action == ACTION_BACKOFF) && IS_NON_IM_DESENSE(nonim_comp_out.desense)) ) )
    {
      /* Set power limits if techs are on seperate chains & thresholds are crossed */
      if (lmtsmgr_is_on_seperate_dev(lprio_tech, hprio_tech) &&
          (TRUE == wwan_coex_efs_lookup(&coex_input, coex_output)) )
      {
        if(coex_output->aplimit_valid)
        {
          /* Check if slot level backoff is required for this combo */
          if (nonim_comp_out.enh_action_mask & (1<<ENH_CXM_SLOT_LEVEL))
          {
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = coex_output->aplimit;
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;

            /* Store the action in the result table... */
            if (vtech == tech1)
            {
              tech2_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
              tech1_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;     
            }
            else
            {
              tech1_index = atul.freqInfo.freqid - at_st->currList.freqId_offset;
              tech2_index = vtdl.freqInfo.freqid - vt_st->currList.freqId_offset;
            }

            result_tbl->arr[tech1_index][tech2_index].action = (coex_output->aplimit==DEFAULT_MAX_PLIMIT) ? 
                                                                ACTION_NONE : nonim_comp_out.action;
            result_tbl->arr[tech1_index][tech2_index].desense = nonim_comp_out.desense;
            result_tbl->is_valid = TRUE;

            LMTSMGR_DBG_MSG_4(MED, "Debug:Non-IM result stored for index %d %d action %d, desense %d", 
                      tech1_index, tech2_index, nonim_comp_out.action, nonim_comp_out.desense );
          }
          else
          {
            /* Slot level is not needed; just set power limits to RF. */
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = coex_output->aplimit;
            lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
          }

#ifndef FEATURE_BOLT_MODEM
          while (vtdlindex < vt_st->currList.num_dl_entries)
          {
            /* DL freq arr has ids into the currList freqinfo */
            vtdl = vt_st->currList.links[vt_st->currList.dl_freqList[vtdlindex]];

            lmgr_temp_ind_mask = MAX(lmgr_temp_ind_mask, 
                              wwcoex_is_filtered_path_combo(&(atul.freqInfo.link_info), &(vtdl.freqInfo.link_info)));
            vtdlindex++;
          };
 #endif   
          break;
        }
        else
        {
          /* No Coex limit defined, set it to MAX */
          lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
          lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
        }
      }
    }
    else if (coex_input.input_power == CXM_UNKNOWN_POWER)
    {
       /* No Coex limit defined, set it to MAX */
       lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
       lmtsmgr.tech_state[lprio_tech].pwr_info.new_limits.coex_slot_plimit[hprio_tech] = DEFAULT_MAX_PLIMIT;
    }
  }

  return TRUE;
}

/*=============================================================================

  FUNCTION:  wwcoex_compare_freq_lists

=============================================================================*/
/*!
    @brief
    Compare freq lists of two technologies and store the actions in the
    result table.
 
    @return
    TRUE
*/
/*===========================================================================*/
cxm_wwcoex_state_info_s wwcoex_compare_freq_lists
(
  cxm_tech_type tech1,
  cxm_tech_type tech2,
  wwcoex_tbl_type* result_tbl
)
{
  uint32 tech1_num_ulfreqs, tech2_num_ulfreqs, tech1_num_dlfreqs, tech2_num_dlfreqs;
  lmtsmgr_tech_state_type* tech1_st, *tech2_st;
  lmtsmgr_tech_link_type t1ul, t2ul;
  uint32 t1ulindex, t2ulindex, tech1_index, tech2_index;
  cxm_action_type action;
  cxm_wwcoex_state_info_s state;
  uint32 t1index, t2index, total_table_enries, entries_with_desense;

  /* Emissions lut arguments */
  wwcoex_sem_lookup_in sem_lookup_in;
  /* SVLTE/SVDO Coex Output */
  wwan_coex_efs_lookup_out coex_output;
  
  tech1_st = &lmtsmgr.tech_state[tech1];
  tech2_st = &lmtsmgr.tech_state[tech2];

  tech1_num_ulfreqs = tech1_st->currList.num_ul_entries;
  tech2_num_ulfreqs = tech2_st->currList.num_ul_entries;

  tech1_num_dlfreqs = tech1_st->currList.num_dl_entries;
  tech2_num_dlfreqs = tech2_st->currList.num_dl_entries;

  /* Start with blank table but keep tech offset/entry info */
  memset(&temp_result_tbl, 0, sizeof(wwcoex_tbl_type));
  temp_result_tbl.tech1_fid_offset = result_tbl->tech1_fid_offset;
  temp_result_tbl.tech1_num_entries = result_tbl->tech1_num_entries;
  temp_result_tbl.tech2_fid_offset = result_tbl->tech2_fid_offset;
  temp_result_tbl.tech2_num_entries = result_tbl->tech2_num_entries;
  temp_result_tbl.is_valid = TRUE;

  /* Shouldn't get here with techs not even in Rx state */
  LMTSMGR_ASSERT((tech1_num_dlfreqs > 0) && (tech2_num_dlfreqs > 0));

  if (tech1_num_ulfreqs > 0 && tech2_num_ulfreqs > 0)
  {
    // Both techs are transmitting
    state.coex_st = COEX_DUAL_ACTIVE_ST;
    state.prob_victim_mask = (1<<tech1) | (1<<tech2);
    state.prob_aggr_mask = (1<<tech1) | (1<<tech2);

    /* check if im desense exists for the tech pair */
    if ( wwcoex_check_tech_pair_im(tech1, tech2) ) 
    {
      LMTSMGR_DBG_MSG_2(MED, "IM involved for tech %d & tech %d pair", 
                            tech1, tech2 );

      /* IM desense exists */
      for (t1ulindex = 0; t1ulindex < tech1_num_ulfreqs; t1ulindex++)
      {
        /* UL freq arr has ids into the currList freqinfo */
        t1ul = tech1_st->currList.links[tech1_st->currList.ul_freqList[t1ulindex]];

        for (t2ulindex = 0; t2ulindex < tech2_num_ulfreqs; t2ulindex++)
        {
          /* UL freq arr has ids into the currList freqinfo */
          t2ul = tech2_st->currList.links[tech2_st->currList.ul_freqList[t2ulindex]];

          /* Check Emissions threshold in LUT */
          sem_lookup_in.tech1 = tech1;
          sem_lookup_in.t1band = lmtsmgr_map_sys_bands_to_rf_bands(
                                                      t1ul.freqInfo.link_info.band, 
                                                      tech1);
          sem_lookup_in.tech2 = tech2;
          sem_lookup_in.t2band = lmtsmgr_map_sys_bands_to_rf_bands(
                                                      t2ul.freqInfo.link_info.band,
                                                      tech2);
          sem_lookup_in.t1_txpwr = t1ul.powerInfo.tx_power;
          sem_lookup_in.t2_txpwr = t2ul.powerInfo.tx_power;

          LMTSMGR_DBG_MSG_4(MED, "Debug:Checking SEM for tech %d, tech %d with powers 0x%x 0x%x", 
                      tech1, tech2, sem_lookup_in.t1_txpwr, sem_lookup_in.t2_txpwr );

          action = wwcoex_emission_efs_lookup(sem_lookup_in);

          /* Store the action */
          tech1_index = t1ul.freqInfo.freqid - tech1_st->currList.freqId_offset;
          tech2_index = t2ul.freqInfo.freqid - tech2_st->currList.freqId_offset;
        
          temp_result_tbl.arr[tech1_index][tech2_index].action = action;
          if (action != ACTION_NONE )
          {
            temp_result_tbl.arr[tech1_index][tech2_index].desense = SPURIOUS_EMISSIONS;
          }
          else
          {
            temp_result_tbl.arr[tech1_index][tech2_index].desense = NO_DESENSE;
          }
          LMTSMGR_DBG_MSG_3(LOW, "Debug:SEM result stored for index %d %d action %d", 
                      tech1_index, tech2_index, action );

          /* Check coex of Tech1 DL with Tech1 UL and Tech2 UL */
          wwcoex_check_coex_with_dl(tech1, tech2, 
                                    tech1,      // Victim tech
                                    t1ulindex,  // Victim UL index
                                    t2ulindex,  // Aggr UL index 
                                    (action != ACTION_NONE),
                                    &temp_result_tbl,
                                    &coex_output);

          if (coex_output.aplimit_valid == FALSE)
          {
            /* Check coex of tech2 DL with tech1 UL and tech 2 UL */
            wwcoex_check_coex_with_dl(tech1, tech2, 
                                      tech2,       // Victim tech
                                      t2ulindex,   // Victim UL index
                                      t1ulindex,   // Aggr UL index 
                                      (action != ACTION_NONE),
                                      &temp_result_tbl,
                                      &coex_output);
          }
        
          if (coex_output.aplimit_valid == TRUE)
            break;
        } /* End For */

        if (coex_output.aplimit_valid == TRUE)
          break;
      } /* End For */
    }
    else
    {
        LMTSMGR_DBG_MSG_2(MED, "No IM involved for tech %d,& tech %d pair", 
                            tech1, tech2 );
        /* IM desense doesnt exist for tech pair */
        for (t1ulindex = 0; t1ulindex < tech1_num_ulfreqs; t1ulindex++)
        {
           /* Check coex of Tech2 DL with Tech1 UL and skip IM */
           wwcoex_check_coex_with_dl(tech1, tech2, 
                                     tech2,   /* Victim tech */
                                     0,       /* No UL for victim */
                                     t1ulindex, 
                                     TRUE,    /* Skip IM checks */
                                   &temp_result_tbl,
                                     &coex_output);
           if (coex_output.aplimit_valid == TRUE)
              break;
        }

        for (t2ulindex = 0; t2ulindex < tech2_num_ulfreqs; t2ulindex++)
        {
           /* Check coex of Tech1 DL with Tech2 UL and skip IM */
           wwcoex_check_coex_with_dl(tech1, tech2, 
                                     tech1, /* Victim Tech */
                                     0, /* No UL index needed for victim */
                                     t2ulindex, 
                                     TRUE,  /* Skip IM checks */
                                   &temp_result_tbl,
                                     &coex_output);
           if (coex_output.aplimit_valid == TRUE)
              break;
        }
     }

    /* Log the state */
    wwcoex_log_state(tech1, tech2, &temp_result_tbl);

    /* Calculations for desense detection are done here.
       Desense detection is done only when both the techs have UL frequencies 
         - Parse through the conflcit table created for the 2 techs
         - Mark the number of entries in the table
         - Mark the number of entries that have valid desense action  
         - Average this value over a given period to detect desense */
    
    total_table_enries   = temp_result_tbl.tech1_num_entries * temp_result_tbl.tech2_num_entries;
    entries_with_desense = 0;

    for (t1index=0; t1index < temp_result_tbl.tech1_num_entries; t1index++)
    {
      for (t2index=0; t2index < temp_result_tbl.tech2_num_entries; t2index++)
      {
        if(temp_result_tbl.arr[t1index][t2index].action != ACTION_NONE)
        {
          entries_with_desense++;
        }
      }
    }

    /* Maintain a history of statistics to detect desense over a period */
    lmtsmgr.desense_stats.cxm_report_count++;
    lmtsmgr.desense_stats.cxm_entries_count += total_table_enries;
    lmtsmgr.desense_stats.cxm_desense_count += entries_with_desense;
    
  }
  else if (tech1_num_ulfreqs > 0)
  {
    state.coex_st = COEX_SINGLE_ACTIVE_ST;
    state.prob_victim_mask = 1<<tech2;
    state.prob_aggr_mask = 1<<tech1;

    for (t1ulindex = 0; t1ulindex < tech1_num_ulfreqs; t1ulindex++)
    {
      /* Check coex of Tech2 DL with Tech1 UL and skip IM */
      wwcoex_check_coex_with_dl(tech1, tech2, 
                                tech2,   /* Victim tech */
                                0,       /* No UL for victim */
                                t1ulindex, 
                                TRUE,    /* Skip IM checks */
                                &temp_result_tbl,
                                &coex_output);
      if (coex_output.aplimit_valid == TRUE)
        break;
    }

    /* Log the state */
    wwcoex_log_state(tech1, tech2, &temp_result_tbl);
  }
  else if (tech2_num_ulfreqs > 0)
  {
    state.coex_st = COEX_SINGLE_ACTIVE_ST;
    state.prob_victim_mask = (1<<tech1);
    state.prob_aggr_mask = 1<<tech2;

    for (t2ulindex = 0; t2ulindex < tech2_num_ulfreqs; t2ulindex++)
    {
      /* Check coex of Tech1 DL with Tech2 UL and skip IM */
      wwcoex_check_coex_with_dl(tech1, tech2, 
                                tech1, /* Victim Tech */
                                0, /* No UL index needed for victim */
                                t2ulindex, 
                                TRUE,  /* Skip IM checks */
                                &temp_result_tbl,
                                &coex_output);
      if (coex_output.aplimit_valid == TRUE)
        break;
    }

    /* Log the state */
    wwcoex_log_state(tech1, tech2, &temp_result_tbl);
  }
  else
  {
    state.coex_st = COEX_INACTIVE_ST;
    state.prob_victim_mask = 0;
    state.prob_aggr_mask = 0;
  }

  /* Copy results to original table */
  memscpy((void *)(result_tbl->arr), 
          WWCOEX_MAX_SUPPORTED_LINKS * WWCOEX_MAX_SUPPORTED_LINKS *sizeof(coex_result_type),
          (void *)(temp_result_tbl.arr),
          WWCOEX_MAX_SUPPORTED_LINKS * WWCOEX_MAX_SUPPORTED_LINKS *sizeof(coex_result_type)); 

  return state;
}
#endif /* FEATURE_MODEM_COEXISTENCE_SW */

uint32 wwcoex_query_max_action
(
  cxm_tech_type aggr_tech,
  cxm_tech_link_direction  aggr_dir,
  cxm_tech_type victim_tech,
  cxm_tech_link_direction  victim_dir
)
{
  lmtsmgr_tech_state_type *aggr_st, *victim_st;
  cxm_action_query_s query_st;
  uint32 requesting_ids[10];
  uint32 conflicting_ids[10];
  uint32 index, action_mask, i;

  aggr_st = &lmtsmgr.tech_state[aggr_tech];
  victim_st = &lmtsmgr.tech_state[victim_tech];

  /* Reset all the fields */
  query_st.num_requesting_ids = 0;
  query_st.num_conflicting_ids = 0;

  if (aggr_dir == CXM_LNK_DRCTN_UL)
  {
    query_st.num_requesting_ids = aggr_st->currList.num_ul_entries;

    for (i=0; i < query_st.num_requesting_ids; i++)
    {
      index = aggr_st->currList.ul_freqList[i];
      requesting_ids[i] = aggr_st->currList.links[index].freqInfo.freqid;
    }
  }


  if (victim_dir == CXM_LNK_DRCTN_DL)
  {
    query_st.num_conflicting_ids = victim_st->currList.num_dl_entries;

    for (i=0; i < query_st.num_conflicting_ids; i++)
    {
      index = victim_st->currList.dl_freqList[i];
      conflicting_ids[i] = victim_st->currList.links[index].freqInfo.freqid;
    }
  }

  query_st.requesting_ids = requesting_ids;
  query_st.conflicting_ids = conflicting_ids;
  query_st.actions = &action_mask;

  if ( FALSE == cxm_query_action(&query_st)) 
  {
    action_mask = 0;
  }
 
  return action_mask;
}

/*=============================================================================

  FUNCTION:  lmtsmgr_update_lmtsmgr_coex_state

=============================================================================*/
/*!
    @brief
    Updates Coex state for the tech combination
 
    @return
    None
*/
/*===========================================================================*/
static void lmtsmgr_update_lmtsmgr_coex_state
(
  cxm_tech_type tech1,
  cxm_tech_type tech2,
  cxm_wwcoex_state_info_s* old_st
)
{
  lmtsmgr_tech_state_type* tech1_st, *tech2_st;
  cxm_wwcoex_state_info_s temp_state;

  /* Initial state */
  temp_state.coex_st = COEX_INACTIVE_ST;
  temp_state.prob_victim_mask = 0;
  temp_state.prob_aggr_mask = 0;

  if (!LMTSMGR_IS_TECH_VALID(tech1) ||
     !LMTSMGR_IS_TECH_VALID(tech2) ||
     old_st == NULL)
  {
    LMTSMGR_MSG_2(FATAL, "ERROR:lmtsmgr_update_lmtsmgr_coex_state tech input invalid %d %d",
                        tech1, tech2);
    return;
  }

  tech1_st = &lmtsmgr.tech_state[tech1];
  tech2_st = &lmtsmgr.tech_state[tech2];

  /* Update Coex state for the techs which are active */
  if (tech1_st->currList.num_ul_entries > 0 && 
       tech2_st->currList.num_ul_entries > 0)
  {
    // Both techs are transmitting
    temp_state.coex_st = COEX_DUAL_ACTIVE_ST;
    temp_state.prob_victim_mask = (1<<tech1) | (1<<tech2);
    temp_state.prob_aggr_mask = (1<<tech1) | (1<<tech2);
  }
  else if (tech1_st->currList.num_ul_entries > 0)
  {
    temp_state.coex_st = COEX_SINGLE_ACTIVE_ST;
    temp_state.prob_victim_mask = 1<<tech2;
    temp_state.prob_aggr_mask = 1<<tech1;
  }
  else if (tech2_st->currList.num_ul_entries > 0)
  {
    temp_state.coex_st = COEX_SINGLE_ACTIVE_ST;
    temp_state.prob_victim_mask = 1<<tech1;
    temp_state.prob_aggr_mask = 1<<tech2;
  }

  /* Set temp state as the new state only if it is higher... */
  if (temp_state.coex_st > old_st->coex_st)
  {
    old_st->coex_st = temp_state.coex_st;
  }

  /* Or the victim mask with the overall mask */
  old_st->prob_victim_mask |= temp_state.prob_victim_mask;
  old_st->prob_aggr_mask |= temp_state.prob_aggr_mask;

  return;
}

#ifndef FEATURE_BOLT_MODEM
/*=============================================================================

  FUNCTION:  lmtsmgr_tech_combo_update_filter_path

=============================================================================*/
/*!
    @brief
    Updates Coex Filter Path for the tech combination for which there is no update
 
    @return
    None
*/
/*===========================================================================*/
static void lmtsmgr_tech_combo_update_filter_path
(
  cxm_tech_type tech1,
  cxm_tech_type tech2
)
{
  lmtsmgr_tech_state_type* tech1_st, *tech2_st;
  uint32 tech1_num_ulfreqs, tech2_num_ulfreqs, tech1_num_dlfreqs, tech2_num_dlfreqs;
  lmtsmgr_tech_link_type ul, dl;
  uint32 ulindex, dlindex;
  uint32 final_special_ind_mask;

  if (!LMTSMGR_IS_TECH_VALID(tech1) ||
     !LMTSMGR_IS_TECH_VALID(tech2))
  {
    LMTSMGR_MSG_2(FATAL, "ERROR:lmtsmgr_tech_combo_update_filter_path tech input invalid %d %d",
                        tech1, tech2);
    return;
  }


  if (lmtsmgr_is_on_seperate_dev(tech1, tech2))
  {
    /* Initial state */
    final_special_ind_mask = 0;
    tech1_st = &lmtsmgr.tech_state[tech1];
    tech2_st = &lmtsmgr.tech_state[tech2];

    tech1_num_ulfreqs = tech1_st->currList.num_ul_entries;
    tech2_num_ulfreqs = tech2_st->currList.num_ul_entries;
  
    tech1_num_dlfreqs = tech1_st->currList.num_dl_entries;
    tech2_num_dlfreqs = tech2_st->currList.num_dl_entries;
  
    /* Tech1 UL as aggressor and Tech2 DL as Victim */
    for (ulindex = 0; ulindex < tech1_num_ulfreqs; ulindex++)
    {
      /* UL freq arr has ids into the currList freqinfo */
      ul = tech1_st->currList.links[tech1_st->currList.ul_freqList[ulindex]];
      for (dlindex = 0; dlindex < tech2_num_dlfreqs; dlindex++)
      {
        dl = tech2_st->currList.links[tech2_st->currList.dl_freqList[dlindex]];

        final_special_ind_mask = MAX(final_special_ind_mask, 
                         wwcoex_is_filtered_path_combo(&(ul.freqInfo.link_info), &(dl.freqInfo.link_info)));
      }
    }

    /* Tech2 UL as aggressor and Tech1 DL as Victim */
    for (ulindex = 0; ulindex < tech2_num_ulfreqs; ulindex++)
    {
      /* UL freq arr has ids into the currList freqinfo */
      ul = tech2_st->currList.links[tech2_st->currList.ul_freqList[ulindex]];
      for (dlindex = 0; dlindex < tech1_num_dlfreqs; dlindex++)
      {
        dl = tech1_st->currList.links[tech1_st->currList.dl_freqList[dlindex]];
        final_special_ind_mask = MAX(final_special_ind_mask, 
                          wwcoex_is_filtered_path_combo(&(ul.freqInfo.link_info), &(dl.freqInfo.link_info)));
      }
    }

    lmgr_temp_ind_mask = MAX(lmgr_temp_ind_mask,final_special_ind_mask);
  }


  return;
}
#endif

/*=============================================================================

  FUNCTION:  lmtsmgr_process_coex_input

=============================================================================*/
/*!
    @brief
    Processes any change in the coex input variables
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_coex_input(void)
{
#ifdef FEATURE_MODEM_COEXISTENCE_SW
  uint32 tbl_index;
  cxm_tech_type tech1, tech2;
  lmtsmgr_tech_state_type* tech1_st, *tech2_st;
  wwcoex_conflict_tbls_type* conflict_tbl;
  cxm_wwcoex_state_info_s new_state, temp_state;
  sys_modem_device_mode_e_type device_mode;
  uint32 onex_action_mask = 0, hdr_action_mask = 0;

  /* Initial state */
  new_state.coex_st = COEX_INACTIVE_ST;
  new_state.prob_victim_mask = 0;
  new_state.prob_aggr_mask = 0;
  temp_state = new_state;

  /* Set the mask to 0 */
  lmgr_temp_ind_mask = 0;

  /* Check if CM mode has changed... */
  device_mode = cm_get_device_mode();
  
  /* check if device mode NV has changed... */
  if (lmtsmgr.device_mode != (uint32)device_mode)
  {
    /* Set the device mode in limitsmgr global space... */
    LMTSMGR_MSG_3(HIGH, "Device mode changed from %d to %d, Mitigation mask 0x%x",
                  lmtsmgr.device_mode, device_mode, lmtsmgr.mitigation_mask);
    lmtsmgr.device_mode = (uint32)device_mode;

    if (device_mode == SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_ACTIVE)
    {
      if (IS_WWAN_MITIGATION_DISABLED(lmtsmgr.mitigation_mask))
      {
        LMTSMGR_MSG_1(HIGH, "WWAN Mitigation is disabled 0x%x",
                      lmtsmgr.mitigation_mask);
        /* Mitigation is disabled... */
        wwcoex_set_sub_state(0,0);
      }
      else
      {
        wwcoex_set_sub_state(2,2);
      }
    }
    else /* Behave as if DSDS */
    {
      /* behave as if DSDS but run freq comparisions for SG*, SV* cases... */
      wwcoex_set_sub_state(2,1);
    }  
  }

  /* For DSDS, TSTS, no need to run any freq comparisons */
  if ( (lmtsmgr.device_mode != SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_ACTIVE) &&
       (lmtsmgr.device_mode != SYS_MODEM_DEVICE_MODE_SINGLE_SIM) )
  {
    /* Reset the flag for all techs */
    for (tech1 = (CXM_TECH_MAX-1); tech1 >= 0; tech1--)
    {
      tech1_st = &lmtsmgr.tech_state[tech1];
      tech1_st->freq_change = FALSE;
      tech1_st->power_change = FALSE;
    }
     
    /* No need to do anything else... */
    return;
  }  
  

  for (tech1 = (CXM_TECH_MAX-1); tech1 >= 0; tech1--)
  {
    tech1_st = &lmtsmgr.tech_state[tech1];
     
    /* Ignore the techs that are inactive */
    if (tech1_st->currList.num_entries == 0)
    {
      continue;
    }

    LMTSMGR_MSG_1(LOW, "proc_coex: Found tech %d active", tech1);

    tech2 = tech1 - 1;
    while (tech2 >= 0)
    {
      tech2_st = &lmtsmgr.tech_state[tech2];

      /* Only consider techs that are inactive */
      if (tech2_st->currList.num_entries > 0)
      {
        /* Tech1 and Tech2 are active i.e. atleast in rx state */
        
        // Get the table node for this concurrent scenario
        conflict_tbl = wwcoex_get_table_node((uint8)tech1, (uint8)tech2);
        if (conflict_tbl == NULL)
        {
          /* Should never come here */
          LMTSMGR_MSG_2(FATAL, "Could not find a table node for techs %d %d",
                        tech1, tech2);

          tech2--;

          continue;
        }

        if ( (tech1_st->power_change == FALSE) && 
             (tech2_st->power_change == FALSE) &&
             (tech1_st->freq_change == FALSE) && 
             (tech2_st->freq_change == FALSE) )
        {
          lmtsmgr_update_lmtsmgr_coex_state(tech1, tech2, &new_state);

#ifdef FEATURE_BOLT_MODEM
          LMTSMGR_MSG_5(MED, "Processing Coex for tech pair %d, %d nothing changed, st: %d victim: %d, aggr: %d",
                        tech1, tech2, new_state.coex_st, new_state.prob_victim_mask, new_state.prob_aggr_mask);
#else
          lmtsmgr_tech_combo_update_filter_path(tech1, tech2);

          LMTSMGR_MSG_6(MED, "Processing Coex for tech pair %d, %d nothing changed, st: %d victim: %d, aggr: %d, filter path: %d",
                        tech1, tech2, new_state.coex_st, new_state.prob_victim_mask, new_state.prob_aggr_mask, lmgr_temp_ind_mask);
#endif
          tech2--;

          continue;
        }

        /* Is there a power update */
        if ((tech1_st->power_change == TRUE) || (tech2_st->power_change == TRUE))
        {
          /* just compare the current tables */
          tbl_index = conflict_tbl->current_tbl;

          if ((conflict_tbl->current_tbl >= 0) && (tbl_index < WWCOEX_MAX_TABLES_PER_SCENARIO))
          {
            /* Perform comparisons and store in this current tbl */
            temp_state = wwcoex_compare_freq_lists(tech1, tech2, 
                                                &conflict_tbl->tables[tbl_index]);
          }
          else
          {
            LMTSMGR_MSG_3(FATAL,"Tbl index %d incorrect for techs %d %d",
                                 conflict_tbl->current_tbl, tech1, tech2 );
          }

        }
        /* Is there a frequency update on both techs ? */
        else if (tech1_st->freq_change == TRUE && tech2_st->freq_change == TRUE)
        {
          /* Should not happen */
          LMTSMGR_MSG_2(ERROR, "Both techs %d and %d have freq_change set", 
                        tech1, tech2);

          /* Get the least used table index */
          tbl_index = wwcoex_get_oldest_tbl_index(conflict_tbl);
          if (tbl_index < WWCOEX_MAX_TABLES_PER_SCENARIO)
          {
            conflict_tbl->current_tbl = tbl_index;

              /* Update the freq offsets */
            conflict_tbl->tables[tbl_index].tech1_fid_offset = 
                                       tech1_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech2_fid_offset = 
                                       tech2_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech1_num_entries =
                                       tech1_st->currList.num_entries;
            conflict_tbl->tables[tbl_index].tech2_num_entries =
                                       tech2_st->currList.num_entries;
                                       
            /* Reset the table */
            memset((void*)conflict_tbl->tables[tbl_index].arr, 0, 
                         sizeof(conflict_tbl->tables[tbl_index].arr));

            /* Perform comparisons and store in this current tbl */
            temp_state = wwcoex_compare_freq_lists(tech1, tech2, 
                                                  &conflict_tbl->tables[tbl_index]);
          }
          else
          {
            LMTSMGR_MSG_3(FATAL,"Tbl index %d incorrect for techs %d %d",
                             tbl_index, tech1, tech2 );
          }
        }
        /* Tech1 freq has changed */
        else if (tech1_st->freq_change == TRUE)
        {
          /* Get the least used table index */
          tbl_index = wwcoex_get_oldest_tbl_index(conflict_tbl);
          if (tbl_index < WWCOEX_MAX_TABLES_PER_SCENARIO)
          {
            conflict_tbl->current_tbl = tbl_index;

            LMTSMGR_DBG_MSG_2(MED, "Debug: Tech %d freq change is set, current tbl %d",
                          tech1, tbl_index);
                          
            /* Update the freq offsets */
            conflict_tbl->tables[tbl_index].tech1_fid_offset = 
                                       tech1_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech2_fid_offset = 
                                       tech2_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech1_num_entries =
                                       tech1_st->currList.num_entries;
            conflict_tbl->tables[tbl_index].tech2_num_entries =
                                       tech2_st->currList.num_entries;

            /* Reset the table */
            memset((void *)conflict_tbl->tables[tbl_index].arr, 0, 
                         sizeof(conflict_tbl->tables[tbl_index].arr));

            /* Perform comparisons and store in this current tbl */
            temp_state = wwcoex_compare_freq_lists(tech1, tech2, 
                                                  &conflict_tbl->tables[tbl_index]);
          }
          else
          {
            LMTSMGR_MSG_3(FATAL,"Tbl index %d incorrect for techs %d %d",
                             tbl_index, tech1, tech2 );
          }

        }
        /* Tech2 frequency has changed */
        else 
        {
          /* Get the least used table index */
          tbl_index = wwcoex_get_oldest_tbl_index(conflict_tbl);
          if (tbl_index < WWCOEX_MAX_TABLES_PER_SCENARIO)
          {
            conflict_tbl->current_tbl = tbl_index;

            LMTSMGR_DBG_MSG_2(MED, "Debug: Tech %d freq change is set, current tbl %d",
                          tech2, tbl_index);

                /* Update the freq offsets */
            conflict_tbl->tables[tbl_index].tech1_fid_offset = 
                                       tech1_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech2_fid_offset = 
                                       tech2_st->currList.freqId_offset;
            conflict_tbl->tables[tbl_index].tech1_num_entries =
                                       tech1_st->currList.num_entries;
            conflict_tbl->tables[tbl_index].tech2_num_entries =
                                       tech2_st->currList.num_entries;

            /* Reset the table */
            memset((void *)conflict_tbl->tables[tbl_index].arr, 0, 
                       sizeof(conflict_tbl->tables[tbl_index].arr));
                                     
            /* Perform comparisons and store in this current tbl */
            temp_state = wwcoex_compare_freq_lists(tech1, tech2, 
                                                &conflict_tbl->tables[tbl_index]);
          }
          else
          {
            LMTSMGR_MSG_3(FATAL,"Tbl index %d incorrect for techs %d %d",
                             tbl_index, tech1, tech2 );
          }
        }

        /* Set temp state as the new state only if it is higher... */
        if (temp_state.coex_st > new_state.coex_st)
        {
          new_state.coex_st = temp_state.coex_st;
        }
        /* Or the victim mask with the overall mask */
        new_state.prob_victim_mask |= temp_state.prob_victim_mask;
        new_state.prob_aggr_mask |= temp_state.prob_aggr_mask;
      }
    
      /* move to the next tech id */
      tech2--;
    }

    /* Reset the flag for tech 1 */
    tech1_st->freq_change = FALSE;
    tech1_st->power_change = FALSE;

    /* Notify techs of the new pwr limits */
    lmtsmgr_notify_tech_plimit(tech1);
  }

  /* Check if G Tx interference with C2K Rx is possible to request G for 
     Tx Activity indication */
  onex_action_mask = wwcoex_query_max_action(CXM_TECH_GSM2, CXM_LNK_DRCTN_UL, 
                                             CXM_TECH_ONEX, CXM_LNK_DRCTN_DL);

  hdr_action_mask = wwcoex_query_max_action(CXM_TECH_GSM2, CXM_LNK_DRCTN_UL, 
                                              CXM_TECH_HDR, CXM_LNK_DRCTN_DL);
  
  LMTSMGR_MSG_2(MED, "Debug: Max query actions 1x: %d, HDR: %d", 
                     onex_action_mask,
                     hdr_action_mask);

  /* If either 1x or HDR has interference, send activity request to GSM */
  if ( (onex_action_mask > 0) || (hdr_action_mask > 0))
  {
    wwcoex_set_send_flag(CXM_TECH_GSM2, TRUE);
  }
  else
  {
    wwcoex_set_send_flag(CXM_TECH_GSM2, FALSE);
  }

  /* check if special ind mask has changed */
  wwcoex_check_special_inds();

  /* Check if coex state has changed and send the broadcast message */
  lmtsmgr_update_wwcoex_state(new_state);

#endif /* FEATURE_MODEM_COEXISTENCE_SW */
}

#ifdef FEATURE_MODEM_DDR_MGMT
static int16 lmtsmgr_get_tech_rx_power
(
  cxm_tech_type techid,
  wwan_ddr_coex_table_entry carrier,
  wwcoex_ddr_threshold_table_val chain
)
{
  lmtsmgr_tech_state_type *tech_st;
  uint32 num_dl_entries, i, dl_index;

  /* Used to determined LTE CA RF */
  trm_get_device_mapping_input_type input;

  /* default power to start with... */
  int16 rx_power = CXM_UNKNOWN_POWER;

  tech_st = &lmtsmgr.tech_state[techid];
  num_dl_entries = tech_st->currList.num_dl_entries;


  for (i=0; i<num_dl_entries; i++)
  {
    dl_index = tech_st->currList.dl_freqList[i];

    if (chain == COEX_DDR_TABLE_ENTRY_DIV)
    {
      /* Determine if dl entry is diveristy */
      if (tech_st->currList.links[dl_index].freqInfo.link_info.type == CXM_LNK_TYPE_DIVERSITY)
      {
         /* Select the entry with valid Rx power.. */
         rx_power = tech_st->currList.links[dl_index].powerInfo.rx_power;
         if ( rx_power != CXM_UNKNOWN_POWER)
         {
           break;
         }
      }
    }
    else /* Assume chain == COEX_DDR_TABLE_ENTRY_PRIMARY */
    {
       if (carrier == DDR_LUT_ENTRY_SECONDARY && techid == CXM_TECH_LTE)
       {
         /* Special Case: LTE CA */
         input.client = TRM_LTE_CA;
         input.resource = TRM_RX_CA_PRIMARY;
         input.band = tech_st->currList.links[dl_index].freqInfo.link_info.band;

         /* Get the RX power specific to LTE Secondary */
         if(tech_st->currList.links[dl_index].freqInfo.link_info.rf_device == trm_get_device_mapping(input))
         {
            rx_power = tech_st->currList.links[dl_index].powerInfo.rx_power;
            break;
         }
       }
       else
       {
         if (tech_st->currList.links[dl_index].freqInfo.link_info.type != CXM_LNK_TYPE_DIVERSITY)
         {
           /* Select the entry with valid Rx power... */
           rx_power = tech_st->currList.links[dl_index].powerInfo.rx_power;
           if ( rx_power != CXM_UNKNOWN_POWER)
           {
             break;
           }
         }
       }
    }
  } // End for

  return rx_power;
}
#endif /* DDR */

/*=============================================================================

  FUNCTION:  lmtsmgr_process_ddr_coex_input

=============================================================================*/
/*!
    @brief
    Processes any change in the DDR coex input variables
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_ddr_coex_input(void)
{
#ifdef FEATURE_MODEM_DDR_MGMT
  lmtsmgr_tech_state_type *tech_st, *save_tech_st = NULL;
  cxm_tech_type   hprio_tech;
  boolean is_TxON = FALSE;
  boolean is_anyTxON = FALSE;
  boolean is_anyInChannel = FALSE;
  boolean is_anyDesense = FALSE;
  uint32  pwr_update_duration, i;
  wwan_ddr_coex_table_entry carrier_idx;
  wwcoex_ddr_threshold_table_val chain_idx;
  int16 rx_power;
  int16 save_rx_power = CXM_UNKNOWN_POWER;

  wwan_ddr_efs_lookup_in ddr_input;
  wwan_ddr_efs_lookup_out ddr_output, save_ddr_info;
  uint32 gps_ddr_freq = 0;

  memset(&save_ddr_info, 0, sizeof(wwan_ddr_efs_lookup_out));

  if(IS_DDR_MITIGATION_DISABLED(lmtsmgr.mitigation_mask))
  {
     LMTSMGR_MSG_0(MED, "DDR Mitigation is diasbled.");
     return;
  }

  /* Priority: Tech1 Primary, T2 Primary, T1 Secondary, T2 Secondary, T1 Diversity, T2 Diversity */
  for (chain_idx = 0; chain_idx<COEX_DDR_TABLE_ENTRY_MAX; chain_idx++) 
  {
     for (carrier_idx = 0; carrier_idx<DDR_LUT_ENTRY_MAX; carrier_idx++)
     {
        for (i = 0; i<lmtsmgr.num_order; i++)
        {
          hprio_tech = lmtsmgr.order[i];
          if (!LMTSMGR_IS_TECH_VALID(hprio_tech))
          {
             continue;
          }

          tech_st = &lmtsmgr.tech_state[hprio_tech];

          if (tech_st->band >= SYS_BAND_CLASS_MAX)
          {
             continue;
          }

          /* Update input to the DDR EFS */
          ddr_input.tech = hprio_tech;
          ddr_input.carrier = carrier_idx;
          ddr_input.chain = chain_idx;

          /* Convert to RF band */
          ddr_input.band = lmtsmgr_map_sys_bands_to_rf_bands(tech_st->band, hprio_tech);

          if (FALSE == wwan_ddr_efs_lookup(&ddr_input, &ddr_output))
          {
             /* If there was a failure in reading EFS entry for a particular tech */
             continue;
          }

          if (!ddr_output.band_spec_plan_valid)
          {
             /* If tech does not have a valid EFS entry*/
             continue;
          }

          if ((!IS_GSM_TECH(hprio_tech) && (carrier_idx == DDR_LUT_ENTRY_PRIMARY) &&
               !IS_IN_CHANNEL_RANGE(tech_st->channel, ddr_output.channel_min, ddr_output.channel_max)) ||
              ((hprio_tech == CXM_TECH_LTE) && (carrier_idx == DDR_LUT_ENTRY_SECONDARY) &&
               !IS_IN_CHANNEL_RANGE(tech_st->lte_ca_channel, ddr_output.channel_min, ddr_output.channel_max)))
          {
             /* If non-GSM tech is out of channel desense range */
             continue;
          }
          
          if (IS_GSM_TECH(hprio_tech) ||
              (!IS_GSM_TECH(hprio_tech) && 
               IS_IN_CHANNEL_RANGE(tech_st->channel, ddr_output.channel_min, ddr_output.channel_max)))
          {
            /* Set if any tech is within channel range (non-GSM) or is GSM */
             is_anyInChannel = TRUE;
          }

          if (tech_st->currList.num_ul_entries > 0 && (chain_idx == COEX_DDR_TABLE_ENTRY_PRIMARY))
          {
             is_TxON = TRUE;
             is_anyTxON = TRUE;
          }
          else
          {
             is_TxON = FALSE;
          }

          /* 
             Mitigate DDR if the following is true:
               - Acquision Mode (non-Diversity)
               - Band change (GSM only) or Channel within range (non-GSM)
          */
          if (!is_TxON && (chain_idx == COEX_DDR_TABLE_ENTRY_PRIMARY) &&
              (((tech_st->band_changed == TRUE) && IS_GSM_TECH(hprio_tech)) ||
               (!IS_GSM_TECH(hprio_tech) && IS_IN_CHANNEL_RANGE(tech_st->channel, ddr_output.channel_min, ddr_output.channel_max))
             ))
          {
            if (IS_GSM_TECH(hprio_tech))
            {
              LMTSMGR_MSG_5(MED, "tech %d, chain: %d, is_TxON %d, band %d, band change %d", 
                           hprio_tech, chain_idx, is_TxON, tech_st->band, tech_st->band_changed);
            }
            else
            {
              LMTSMGR_MSG_5(MED, "tech %d, chain: %d, is_TxON %d, channel %d, channel change %d", 
                            hprio_tech, chain_idx, is_TxON, tech_st->channel, tech_st->channel_changed);
            }
            ddr_coex_npa_update_freq_plan(ddr_output.ddr_freq);
            goto process_ddr_coex_cleanup;
            return;
          }

          rx_power = lmtsmgr_get_tech_rx_power(hprio_tech, ddr_input.carrier, ddr_input.chain);

          LMTSMGR_MSG_6(MED, "tech %d, carrier: %d, chain: %d, power change: %d, Rx power %d, rx_thrshd %d", 
                               hprio_tech,
                               carrier_idx,
                               chain_idx,
                               tech_st->power_change,
                               rx_power, 
                               ddr_output.rxpower_threshold);

          if (rx_power == CXM_UNKNOWN_POWER)
          {
             continue;
          }

          /* If power has changed, perform time hystersis. */
          if (tech_st->power_change == TRUE)
          { 
             if (rx_power < ddr_output.rxpower_threshold)
             {
                 is_anyDesense = TRUE;
                 /* Neutral is required when the Power threshold is crossed 1st time, 
                              it should be updated as per */
                 if ((tech_st->thrshd_switchover_sign == LMTSMGR_DDR_RX_THRSHD_NEUTRAL) ||
                               (tech_st->thrshd_switchover_sign == LMTSMGR_DDR_RX_THRSHD_POSITIVE))
                 {
                   tech_st->pwr_thrshd_switchover_time = tech_st->curr_pwr_update_time;
                   tech_st->thrshd_switchover_sign = LMTSMGR_DDR_RX_THRSHD_NEGATIVE;
                 }
                 else if (tech_st->curr_pwr_update_time < tech_st->pwr_thrshd_switchover_time)
                 {
                   /*There is a rollover happended in time hence updating switchover time accordingly */
                   tech_st->pwr_thrshd_switchover_time = tech_st->curr_pwr_update_time;
                 }

                 pwr_update_duration = 
                       (uint32)((tech_st->curr_pwr_update_time - tech_st->pwr_thrshd_switchover_time) 
                                & 0xFFFFFFFFuL);

                 LMTSMGR_MSG_4(MED, "time Rx thrshd NOT crossed duration %x (%d) current 0x%llx old 0x%llx", 
                               pwr_update_duration, pwr_update_duration, 
                               tech_st->curr_pwr_update_time, 
                               tech_st->pwr_thrshd_switchover_time);

                 if (pwr_update_duration > ddr_output.lower_threshold)
                 {
                   ddr_coex_npa_update_freq_plan(ddr_output.ddr_freq);
                   goto process_ddr_coex_cleanup;
                 }
             }
             else
             {
                /* Store Rx and LUT info */
                save_tech_st = tech_st;
                save_rx_power = rx_power;
                save_ddr_info = ddr_output;
             }
          }

        } // End For (i)
     } // End For (carrier_idx)
  } // End for (chain_idx);

  LMTSMGR_MSG_3(LOW, "anyTxOn: %d, anyInChannel %d, anyDesense %d",
                is_anyTxON, is_anyInChannel, is_anyDesense);

  if ((lmtsmgr.gps_tech_state_on == TRUE) && (wwan_ddr_efs_lookup_gps(&gps_ddr_freq) == TRUE))
  {
     /* If GPS is ON and EFS entry is valid, utilize associated DDR Frequency Plan. */
     LMTSMGR_MSG_1(MED, "GPS is enabled. Set DDR Frequency to %d", gps_ddr_freq);
     ddr_coex_npa_update_freq_plan(gps_ddr_freq);
  }
  /* If no tech is active or any tech is within channel range, set to default */
  else if(!is_anyTxON || !is_anyInChannel)
  {
     ddr_coex_npa_update_freq_plan(0);
  }
  else if (!is_anyDesense && (save_rx_power != CXM_UNKNOWN_POWER) && (save_tech_st != NULL))
  {
     /* All techs do not require desense. Switch back to default freq plan if passes time hystersis */
     if (save_rx_power >= save_ddr_info.rxpower_threshold)
     {
         if ((save_tech_st->thrshd_switchover_sign == LMTSMGR_DDR_RX_THRSHD_NEUTRAL) ||
             (save_tech_st->thrshd_switchover_sign == LMTSMGR_DDR_RX_THRSHD_NEGATIVE))
         {
           save_tech_st->pwr_thrshd_switchover_time = save_tech_st->curr_pwr_update_time;
           save_tech_st->thrshd_switchover_sign = LMTSMGR_DDR_RX_THRSHD_POSITIVE;
         }
         else if (save_tech_st->curr_pwr_update_time <save_tech_st->pwr_thrshd_switchover_time)
         {
           /*There is a rollover happended in time hence updating switchover time accordingly */
           save_tech_st->pwr_thrshd_switchover_time = save_tech_st->curr_pwr_update_time;
         }
         
         pwr_update_duration = 
               (uint32)((save_tech_st->curr_pwr_update_time - save_tech_st->pwr_thrshd_switchover_time) 
                       & 0xFFFFFFFFuL);
         
         LMTSMGR_MSG_4(MED, "time Rx thrshd crossed duration %x (%d) current 0x%llx old 0x%llx", 
                      pwr_update_duration, pwr_update_duration, 
                      save_tech_st->curr_pwr_update_time, 
                      save_tech_st->pwr_thrshd_switchover_time);
         
         if (pwr_update_duration > save_ddr_info.upper_threshold)
         {
           /* If the power update is too long, set frequency plan to default */
           ddr_coex_npa_update_freq_plan(ddr_output.default_ddr_freq);
         }
     }
  }

process_ddr_coex_cleanup:

  /* Reset the flag for all techs */
  for (i = 0; i<lmtsmgr.num_order; i++)
  {
    hprio_tech = lmtsmgr.order[i];
    if (!LMTSMGR_IS_TECH_VALID(hprio_tech))
    {
      continue;
    }

    tech_st = &lmtsmgr.tech_state[hprio_tech];
#ifndef FEATURE_MODEM_COEXISTENCE_SW
    tech_st->freq_change = FALSE;
    tech_st->power_change = FALSE;
#endif
    tech_st->band_changed = FALSE;
    tech_st->channel_changed = FALSE;
  }

#endif /* DDR */
}


/*=============================================================================

  FUNCTION:  lmtsmgr_process_chain_owner_input

=============================================================================*/
/*!
    @brief
    Processes any change in the chain owner inputs
 
    @details
    This should only be called when chain owner is updated in TRM
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_chain_owner_input()
{
  wwcoex_conflict_tbls_type* conflict_tbl;
  lmtsmgr_power_info_type *pwr_info_t1, *pwr_info_t2;
  uint32 tech1_num_dlfreqs, tech2_num_dlfreqs, tbl_index;
  cxm_tech_type tech1 = CXM_TECH_DFLT_INVLD, tech_temp;
  cxm_tech_type tech2;
  lmtsmgr_tech_state_type* tech1_st, *tech2_st;
  int i, j;
  boolean multi_chain_active = FALSE;

  for (i=0; i<TRM_MAX_CHAINS; i++)
  {
     tech_temp = lmtsmgr.chain_owner[i];
     if (!LMTSMGR_IS_TECH_VALID(tech_temp))
     {
        continue;
     }
     else
     {
       tech1 = tech_temp;
       tech1_st = &lmtsmgr.tech_state[tech_temp];
       tech1_num_dlfreqs = tech1_st->currList.num_dl_entries;
     }

     for (j=i+1; j<TRM_MAX_CHAINS; j++)
     {
        tech2 = lmtsmgr.chain_owner[j];
        LMTSMGR_DBG_MSG_4(MED, "Comparing index %x (CXM_TECH: %x) with index %x (CXM_TECH: %x)",
                          i, tech1, j, tech2);

        /* If the techs are on seperate chains, compare the frequencies */
        if (LMTSMGR_IS_TECH_VALID(tech2) && (tech1 != tech2))
        {
           tech2_st = &lmtsmgr.tech_state[tech2];
           tech2_num_dlfreqs = tech2_st->currList.num_dl_entries;

           LMTSMGR_MSG_4(LOW, "Comparing Freqs for Techs %x (num_dl: %x) %x (num_dl: %x)",
                         tech1, tech1_num_dlfreqs, tech2, tech2_num_dlfreqs);

           if (tech1_num_dlfreqs > 0 && tech2_num_dlfreqs > 0 &&
               (ACTION_BACKOFF == wwcoex_get_mitigation_action(tech1, tech2)))
           {
             multi_chain_active = TRUE;
             conflict_tbl = wwcoex_get_table_node(tech1, tech2);
             if((conflict_tbl == NULL) || 
                (conflict_tbl->current_tbl == WWCOEX_TBL_INVALID))
             {
               /* Should never come here */
               LMTSMGR_MSG_2(FATAL, "Could not find a table node for techs %d %d",
                                     tech1, tech2);
               continue;
             }
             tbl_index = conflict_tbl->current_tbl;

             /* 1st arg should be > 2nd arg in compare_freq_techs */
             if (tech1 > tech2)
             {
               wwcoex_compare_freq_lists(tech1, tech2, &conflict_tbl->tables[tbl_index]);
             }
             else
             {
               wwcoex_compare_freq_lists(tech2, tech1, &conflict_tbl->tables[tbl_index]);
             }

             /* Ensure that power limits are only set for active techs*/
             pwr_info_t1 = &lmtsmgr.tech_state[tech1].pwr_info;
             pwr_info_t2 = &lmtsmgr.tech_state[tech2].pwr_info;
             for (i=0; i<CXM_TECH_MAX; i++)
             {
               if (i != tech2)
               {
                 pwr_info_t1->new_limits.coex_plimit[i] = DEFAULT_MAX_PLIMIT;
                 pwr_info_t1->new_limits.coex_slot_plimit[i] = DEFAULT_MAX_PLIMIT;
               }
               if (i != tech1)
               {
                 pwr_info_t2->new_limits.coex_plimit[i] = DEFAULT_MAX_PLIMIT;
                 pwr_info_t2->new_limits.coex_slot_plimit[i] = DEFAULT_MAX_PLIMIT;
               }
             }
             wwcoex_check_special_inds();
             lmtsmgr_notify_tech_plimit(tech1);
             lmtsmgr_notify_tech_plimit(tech2);
           }
        }
     }
  }

  /* If single tech is on, set limits to max and ensure filter path is not used. */
  if (!multi_chain_active && (tech1 != CXM_TECH_DFLT_INVLD))
  {
    LMTSMGR_MSG_1(MED, "Resetting pwr limits for Tech: %d", tech1);

    lmtsmgr_set_tech_backoff_limits_max(tech1);

    lmgr_temp_ind_mask = 0;
    wwcoex_check_special_inds();
  }
  
}

/*=============================================================================

  FUNCTION:  lmtsmgr_process_band_avoidance_input

=============================================================================*/
/*!
    @brief
    Processes any change in the band avoidance inputs (serving frequency,
    serving power, blacklist response)
 
    @details
    This should only be called when serving frequency, serving power,
    or a blacklist response has been sent from Tech L1
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_band_avoidance_input(void)
{

  cxm_tech_type                tech1 = CXM_TECH_DFLT_INVLD, tech2 = CXM_TECH_DFLT_INVLD;
  cxm_tech_type                hprio_tech, lprio_tech, tech_pos;
  lmtsmgr_core_prio_table_type tech1_prio_tbl,tech2_prio_tbl;
  uint32                       num_active_techs = 0, blist_id;
  lmtsmgr_ba_info_type         *ba_state;
  lmtsmgr_tech_ba_type         *hprio_ba_info, *lprio_ba_info;
  coex_ba_blist_data           new_blacklist;

  #ifdef TEST_FRAMEWORK
  #error code not present
#endif

  ba_state = &lmtsmgr.band_avoidance;
  memset(&new_blacklist, 0, sizeof(coex_ba_blist_data));

  /* Only enable feature if in DSDA mode and enable mask is set */
  if (!IS_WWAN_BAND_AVOIDANCE_ENABLED(lmtsmgr.en_mitigation_mask) || 
      LMTSMGR_IS_SGLTE_FEATURE_MODE())
  {
    LMTSMGR_MSG_2(MED, "Band Avoidance is disabled 0x%x (Device Mode: 0x%x)",
                  lmtsmgr.en_mitigation_mask, lmtsmgr.device_mode);

    //TODO: Corner case when BA is disabled but tech received BList
    return;
  }

  /* Only two techs should be in connected mode */
  for (tech_pos=CXM_TECH_LTE; tech_pos<CXM_TECH_MAX; tech_pos++)
  {
    if(ba_state->tech_info[tech_pos].num_entries > 0)
    {
      if (tech1 == CXM_TECH_DFLT_INVLD)
      {
        tech1 = tech_pos;
      }
      else //tech2 == CXM_TECH_DFLT_INVLD
      {
        tech2 = tech_pos;
      }
      num_active_techs++;
    }
  }

  if (num_active_techs > 2)
  {
    LMTSMGR_MSG_1(ERROR, "ERROR: Only two techs should be registered for Band Avoidance!",
                  num_active_techs);
    return;
  }
  else if (num_active_techs == 1) 
  {
    /* send empty blacklist to only active tech */
    if (FALSE == wwcoex_ba_comp_blacklists(&ba_state->currBlacklist, &new_blacklist))
    {
      /* Keep blacklist ID */
      blist_id = ba_state->currBlacklist.blacklist_id+1;
      memset(&ba_state->currBlacklist, 0, sizeof(coex_ba_blist_data));
      ba_state->currBlacklist.blacklist_id = blist_id;

      wwcoex_log_blacklist(tech1);
      lmtsmgr_send_blacklist_info(tech1, TRUE); /* Don't care but setting TRUE
                                                   will save memset call */
    }
    return;
  }
  else if ((tech1 == CXM_TECH_DFLT_INVLD) || (tech2 == CXM_TECH_DFLT_INVLD))
  {
    LMTSMGR_MSG_0(ERROR, "ERROR: Two techs required for Band Avoidance!");
    return;
  }

  /* Determine priority of two techs */
  tech1_prio_tbl.tech = tech1;
  tech2_prio_tbl.tech = tech2;
  (void)lmtsmgr_core_get_priority (&tech1_prio_tbl, &tech2_prio_tbl);
  if (LMTSMGR_TIER_1 == tech1_prio_tbl.pri)
  {
    hprio_tech = tech1;
    lprio_tech = tech2;
  }
  else if (LMTSMGR_TIER_2 == tech1_prio_tbl.pri)
  {
    hprio_tech = tech2;
    lprio_tech = tech1;
  }
  else
  {
    LMTSMGR_MSG_3(FATAL,"Tech %d has invalid tier %d.. ignoring band avoidace with tech %d",
                   tech1, tech1_prio_tbl.pri, tech2);
    return;
  }

  hprio_ba_info = &ba_state->tech_info[hprio_tech];
  lprio_ba_info = &ba_state->tech_info[lprio_tech];

  /* Do not send blacklist under the following cirumstances:
        - No problem is received from either tech
        - If blacklist response is old
  */
  if (((lprio_ba_info->ba_rsp_changed) &&
      ((lprio_ba_info->blist_rsp.bl_id != ba_state->currBlacklist.blacklist_id) ||
      (lprio_ba_info->blist_rsp.response == FREQ_AVOID_NO_PROBLEM))) ||
      ((hprio_ba_info->ba_rsp_changed) &&
      ((hprio_ba_info->blist_rsp.bl_id != ba_state->currBlacklist.blacklist_id) ||
      (hprio_ba_info->blist_rsp.response == FREQ_AVOID_NO_PROBLEM))))
  {
    /* Do nothing */
  }
  /* 
     Send blacklist to high priority tech under following circumstances:
        - When low priority tech NACK'd
        - Blacklist has been already sent to hprio tech, low priority frequency
          has not been updated, and there is power update by either tech 
  */
  else if ( ( (lprio_ba_info->blist_rsp.response == FREQ_AVOID_NACK) &&
              (lprio_ba_info->blist_rsp.bl_id == ba_state->currBlacklist.blacklist_id)) ||
            ( (lmtsmgr.band_avoidance.currBlTech == hprio_tech) && 
              (lprio_ba_info->freq_changed != TRUE) &&
              ( (hprio_ba_info->power_changed == TRUE) || 
                (lprio_ba_info->power_changed == TRUE) ) ) )
  {
    /* Send blacklist to higher priority tech */
    wwcoex_ba_gen_blacklist(hprio_tech, hprio_ba_info, lprio_tech, lprio_ba_info,
                            &new_blacklist);

    /* Force the high priority tech to change serving frequency */
    new_blacklist.serv_freq_state = SERV_FREQ_AVOID_REQ;

    /* Mode should be set to opposite of what lprio tech was sent
       (regardless of threshold crossing) */
    new_blacklist.serv_freq_mode = 
      (lprio_ba_info->serv_desense_mode == DESENSE_AGGR) ? DESENSE_VICTIM :
      (lprio_ba_info->serv_desense_mode == DESENSE_VICTIM) ? DESENSE_AGGR :
      lprio_ba_info->serv_desense_mode;

    /* If the new and current blacklist entries are different, copy
       new blacklist to data and send to appropriate tech */
    if(FALSE == wwcoex_ba_comp_blacklists(&ba_state->currBlacklist, &new_blacklist))
    {
      /* Increment blacklist id */
      new_blacklist.blacklist_id = ba_state->currBlacklist.blacklist_id+1;
      memscpy(&ba_state->currBlacklist, sizeof(coex_ba_blist_data),
              &new_blacklist, sizeof(coex_ba_blist_data));

      /* Empty blacklist NOT sent to lprio_tech as it should attempt to 
         perform BA regardless */

      wwcoex_log_blacklist(hprio_tech);
      lmtsmgr_send_blacklist_info(hprio_tech, FALSE);
    }

    lmtsmgr.band_avoidance.currBlTech = hprio_tech;
  }
  else
  {
    /* Send blacklist to lower priority tech */
    wwcoex_ba_gen_blacklist(lprio_tech, lprio_ba_info, hprio_tech, hprio_ba_info,
                            &new_blacklist);

    /* If the new and current blacklist entries are different, copy
       new blacklist to data and send to appropriate tech */
    if(FALSE == wwcoex_ba_comp_blacklists(&ba_state->currBlacklist, &new_blacklist))
    {
      /* Increment blacklist id */
      new_blacklist.blacklist_id = ba_state->currBlacklist.blacklist_id+1;

      memscpy(&ba_state->currBlacklist, sizeof(coex_ba_blist_data),
              &new_blacklist, sizeof(coex_ba_blist_data));

      /* Store desense mode */
      lprio_ba_info->serv_desense_mode = new_blacklist.serv_freq_mode;

      /* Send empty blacklist to hprio_tech if NACK'd */
      if (hprio_ba_info->blist_rsp.response == FREQ_AVOID_NACK)
      {
        LMTSMGR_MSG_2(MED, "Sending empty blacklist to Tech %d (ID: %d)", 
                       hprio_tech, new_blacklist.blacklist_id);
        lmtsmgr_send_blacklist_info(hprio_tech, TRUE);

        /* Increment blacklist ID again */
        new_blacklist.blacklist_id++;
      }

      wwcoex_log_blacklist(lprio_tech);
      lmtsmgr_send_blacklist_info(lprio_tech, FALSE);
    }
 
    lmtsmgr.band_avoidance.currBlTech = lprio_tech;
  }

  /* Reset flags */
  lprio_ba_info->ba_rsp_changed = FALSE;
  hprio_ba_info->ba_rsp_changed = FALSE;
}

/*=============================================================================

  FUNCTION:  lmtsmgr_update_tech_band

=============================================================================*/
/*!
    @brief
    Updates the tech's band only if the band has changed. Also sets the
    band changed flag.
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_update_tech_band
(
  cxm_tech_type techid,
  uint32 band
)
{
#ifdef FEATURE_MODEM_DDR_MGMT
  lmtsmgr_tech_state_type *tech_st;

  tech_st = &lmtsmgr.tech_state[techid];

  /* Has the band changed ? */
  if (tech_st->band != band)
  {
    LMTSMGR_MSG_2(MED, "band changed from %d->%d", tech_st->band, band);
    tech_st->band = band;
    tech_st->band_changed = TRUE;
  }
#endif
}

/*=============================================================================

  FUNCTION:  lmtsmgr_update_tech_channel

=============================================================================*/
/*!
    @brief
    Updates the tech's channel only if the channel has changed. Also sets the
    channel changed flag.
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_update_tech_channel
(
  cxm_tech_type techid,
  uint32 channel
)
{
#ifdef FEATURE_MODEM_DDR_MGMT
  lmtsmgr_tech_state_type *tech_st;

  tech_st = &lmtsmgr.tech_state[techid];

  /* Has the band changed ? */
  if (tech_st->channel != channel)
  {
    LMTSMGR_MSG_2(MED, "channel changed from %d->%d", tech_st->channel, channel);
    tech_st->channel = channel;
    tech_st->channel_changed = TRUE;
  }
#endif
}

/*=============================================================================

  FUNCTION:  lmtsmgr_core_deinit

=============================================================================*/
/*!
    @brief
    De-inits SP and any other memory allocations.
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_core_deinit(void)
{
  /* Deregister SP client */
  sp_deregister_client(SP_SAR);

  /* Stop QMI service */
  sar_dsi_service_deinit();

  /* Deinit sar efs*/
  sar_efs_deinit();

  /* Deinit wwcoex efs*/
  wwcoex_efs_deinit();
}
