/*!
  @file
  hwio_cap.c

  @brief

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "mcs_variation.h"
#include <customer.h>
#include "msmhwioreg.h"
#include <DDIChipInfo.h>
#include "hwio_cap.h"
#include <msg_diag_service.h>

#ifdef TEST_FRAMEWORK
  #error code not present
#else
  #ifdef FEATURE_BOLT_MODEM
    #define MCSHWIO_READ_CAP() HWIO_IN(MSS_SW_FEATURE_FUSES)
  #else
    #ifdef FEATURE_DIMEPM_MODEM
      #define MCSHWIO_READ_CAP() HWIO_IN(MODEM_FEATURE_EN)
	#else
	  #define MCSHWIO_READ_CAP() HWIO_IN(MODEM_OPTION_EN)
	#endif
  #endif
#endif /* TEST_FRAMEWORK */

 
  
/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*----------------------------------------------------------------------------
  Modem capability: 0xFFFE and 0xFFFF in each modem_cap_* enum below are 
  special values assigned to default values SUPPORTED and UNSUPPORTED 
  respectively. Having a common values for defaults across chips helps return 
  default values for feature query irrespective of the hardware bits.
----------------------------------------------------------------------------*/
typedef enum
{
  FRODO_DSDA,
  FRODO_RXD,
  FRODO_UMTS_UL_ABOVE_CAT6,
  FRODO_MCDO,
  FRODO_NAV,
  FRODO_MODEM_GLOBAL,
  FRODO_UMTS_DL_ABOVE_CAT10,
  FRODO_UMTS_DL_ABOVE_CAT8,
  FRODO_TDSCDMA,
  FRODO_HSPA,
  FRODO_WCDMA,
  FRODO_HDR,
  FRODO_1X,
  FRODO_GSM,
  FRODO_SUPPORTED = 0xFFFE,
  FRODO_UNSUPPORTED = 0xFFFF
}modem_cap_frodo;

typedef enum
{
  ARAGORN_GSM,
  ARAGORN_1X,
  ARAGORN_HDR,
  ARAGORN_WCDMA,
  ARAGORN_HSPA,
  ARAGORN_LTE,
  ARAGORN_MODEM_GLOBAL,
  ARAGORN_TDSCDMA,
  ARAGORN_NAV,
  ARAGORN_HSPA_MIMO,
  ARAGORN_HSPA_DC,
  ARAGORN_LTE_ABOVE_CAT1,
  ARAGORN_LTE_ABOVE_CAT2,
  ARAGORN_BBRX2,
  ARAGORN_BBRX3,
  ARAGORN_SUPPORTED = 0xFFFE,
  ARAGORN_UNSUPPORTED = 0xFFFF
}modem_cap_aragorn;

typedef enum
{
  GIMLI_GSM,
  GIMLI_1X,
  GIMLI_HDR,
  GIMLI_WCDMA,
  GIMLI_HSPA,
  GIMLI_LTE,
  GIMLI_MODEM_GLOBAL,
  GIMLI_TDSCDMA,
  GIMLI_NAV,
  GIMLI_HSPA_MIMO,
  GIMLI_HSPA_DC,
  GIMLI_LTE_ABOVE_CAT1,
  GIMLI_LTE_ABOVE_CAT2,
  GIMLI_DSDA,
  GIMLI_SUPPORTED = 0xFFFE,
  GIMLI_UNSUPPORTED = 0xFFFF
}modem_cap_gimli;

typedef enum
{
  TORINO_1X = 0,
  TORINO_DO = 2,
  TORINO_WCDMA = 4,
  TORINO_HSDPA = 6,
  TORINO_HSDPA_MIMO = 8,
  TORINO_HSDPA_DC = 10,
  TORINO_LTE = 12,
  TORINO_LTE_ABOVE_CAT2 = 14,
  TORINO_LTE_ABOVE_CAT1 = 16,
  TORINO_TDSCDMA = 18,
  TORINO_GERAN = 20,
  TORINO_LTE_40MHZ = 22,
  TORINO_LTE_60MHZ = 23,
  TORINO_LTE_UL_CA = 24,
  TORINO_ADC2 = 25,
  TORINO_ADC3 = 26,
  TORINO_MODEM = 27,
  TORINO_DSDA = 28,
  TORINO_SUPPORTED = 0xFFFE,
  TORINO_UNSUPPORTED = 0xFFFF
}modem_cap_torino;

/*===========================================================================

                         INTERNAL VARIABLES

===========================================================================*/
modem_cap_aragorn aragorn_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  ARAGORN_GSM,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   ARAGORN_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   ARAGORN_HDR,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 ARAGORN_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 ARAGORN_HSPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  ARAGORN_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              ARAGORN_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                ARAGORN_MODEM_GLOBAL,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     ARAGORN_HSPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  ARAGORN_NAV,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            ARAGORN_HSPA_MIMO,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       ARAGORN_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       ARAGORN_LTE_ABOVE_CAT2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            ARAGORN_BBRX2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            ARAGORN_BBRX3,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 ARAGORN_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 ARAGORN_UNSUPPORTED,
};

modem_cap_frodo frodo_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  FRODO_GSM,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   FRODO_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   FRODO_HDR,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 FRODO_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 FRODO_HSPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              FRODO_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                FRODO_MODEM_GLOBAL,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  FRODO_NAV,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   FRODO_UMTS_DL_ABOVE_CAT8,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  FRODO_UMTS_DL_ABOVE_CAT10,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 FRODO_MCDO,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   FRODO_UMTS_UL_ABOVE_CAT6,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  FRODO_RXD,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 FRODO_DSDA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 FRODO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 FRODO_UNSUPPORTED,
};

modem_cap_gimli gimli_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  GIMLI_GSM,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   GIMLI_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   GIMLI_HDR,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 GIMLI_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 GIMLI_HSPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  GIMLI_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              GIMLI_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                GIMLI_MODEM_GLOBAL,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     GIMLI_HSPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  GIMLI_NAV,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            GIMLI_HSPA_MIMO,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       GIMLI_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       GIMLI_LTE_ABOVE_CAT2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 GIMLI_DSDA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 GIMLI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 GIMLI_UNSUPPORTED,
};

modem_cap_torino torino_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  TORINO_GERAN,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   TORINO_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   TORINO_DO,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 TORINO_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 TORINO_HSDPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  TORINO_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              TORINO_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                TORINO_MODEM,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  TORINO_SUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            TORINO_HSDPA_MIMO,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       TORINO_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       TORINO_LTE_ABOVE_CAT2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  TORINO_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 TORINO_DSDA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             TORINO_HSDPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            TORINO_LTE_40MHZ,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            TORINO_LTE_60MHZ,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            TORINO_LTE_UL_CA,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 TORINO_ADC2,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 TORINO_ADC3,
};

typedef struct
{
  uint32 data;
  DalChipInfoFamilyType chipType;
}modem_capability_type;

/*! Global state for modem capability */
static modem_capability_type modem_cap;

/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/


/*===========================================================================

                                FUNCTIONS

===========================================================================*/
/*============================================================================

FUNCTION HWIO_CAP_INIT

DESCRIPTION
  Reads the modem capability information and caches it.
  
DEPENDENCIES
  Must be called by RC INIT before any other functions in this
  file are called.

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
void hwio_cap_init(void)
{

  /* Get chip type */
  modem_cap.chipType = DalChipInfo_ChipFamily();

  #ifdef FEATURE_GNSS_SA
  /* Not applicable for GPS-only builds */
  modem_cap.data = 0;
  #else
  /* Get all hw capabilities */
  modem_cap.data = MCSHWIO_READ_CAP();
  #endif
}


/*============================================================================

FUNCTION MCS_MODEM_HAS_CAPABILITY

DESCRIPTION
  Returns if modem is capable of supporting queried modem technology.
  
DEPENDENCIES
  None

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
mcs_modem_cap_return_enum mcs_modem_has_capability
(
  mcs_modem_capability_enum capability /* capability to check */
)
{

  mcs_modem_cap_return_enum return_value = MCS_MODEM_CAP_UNKNOWN;
  uint8 bitposition;

  /* Get bit position based on the chip type */
  switch (modem_cap.chipType)
  {
    case DALCHIPINFO_FAMILY_MDM9x35:
    case DALCHIPINFO_FAMILY_MSM8994:
      {
        bitposition = (uint8)torino_bitpositions[(uint8)capability];
      }
      break;

    case DALCHIPINFO_FAMILY_MSM8x10:
    case DALCHIPINFO_FAMILY_MSM8x26:
      {
        bitposition = (uint8)frodo_bitpositions[(uint8)capability];
      }
      break;

    case DALCHIPINFO_FAMILY_MSM8974:
    case DALCHIPINFO_FAMILY_MDM9x25:
      {
        bitposition = (uint8)aragorn_bitpositions[(uint8)capability];
      }
      break;

    case DALCHIPINFO_FAMILY_MSM8926:
    case DALCHIPINFO_FAMILY_MSM8x62:
    case DALCHIPINFO_FAMILY_MSM8916:
    case DALCHIPINFO_FAMILY_MSM8974_PRO:
    case DALCHIPINFO_FAMILY_MSM8936:
    case DALCHIPINFO_FAMILY_MSM8929:
      {
        bitposition = (uint8)gimli_bitpositions[(uint8)capability];
      }
      break;

    default:
      bitposition = (uint8)ARAGORN_UNSUPPORTED;
      break;
  }

  /* Not all features exist in every chip. If there is no actual hardware bit
     to query the feature availability return unavailable. */
  if (bitposition == (uint8)ARAGORN_UNSUPPORTED)
  {
    MSG_3(MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "Returning Unsupported capability %d for requested bit %d on chip %d",
          (uint8)MCS_MODEM_CAP_UNKNOWN, 
          (uint8)capability,
          modem_cap.chipType);
    return_value = MCS_MODEM_CAP_UNKNOWN;
  }
  /* Default to Available (added for torino because NAV fuse is set, but no
     hardware bit to check */
  else if (bitposition == (uint8)ARAGORN_SUPPORTED)
  {
    return_value = MCS_MODEM_CAP_AVAILABLE;
  }
  else
  {
    /* Check if the required bit is set */
    if (modem_cap.data & (1<<bitposition))
    {
      return_value = MCS_MODEM_CAP_AVAILABLE;
    }
    else
    {
      return_value = MCS_MODEM_CAP_UNAVAILABLE;
      MSG_3(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "Modem Option Register 0x%x; Capability Requested %d, Chip %d", 
            modem_cap.data, capability, modem_cap.chipType);
    }
  }

  return return_value;
}
