/*!
  @file   msgr_rex.h

  @brief  REX specific message router header file

  @detail
  Defines REX-specific Message Router related types and interfaces.

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/api/msgr_rex.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/09/10   zm      Moved file into mcs/api folder - SU API effort.
04/02/10   ck      Add MSGR_NO_QUEUE support for legacy REX clients
03/24/09   ejv     Add msgr_client_add_rex_q.
12/08/08   awj     Message router cleanup pass
10/31/08   ejv     Initial revision

===========================================================================*/

#ifndef MSGR_REX_H
#define MSGR_REX_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "msgr.h"
#include "msgr_types.h"
#include <rex.h>      /* REX */
#include <queue.h>    /* Queue services */
#include <IxErrno.h>

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

errno_enum_type msgr_client_set_rex_q
(
  msgr_client_t             *msgr_client_id,   //!< Message Client Handle
  rex_tcb_type              *tcb,              //!< REX TCB pointer
  rex_sigs_type              sig,              //!< REX signal to set
  q_type                    *msg_queue,        //!< queue to place the msg
  q_type                    *free_msg_queue,   //!< queue to get empty buffer
  uint16                     msgr_hdr_offset,  //!< offset to msgr_hdr
  uint16                     cmd_type_offset,  //!< offset to legacy cmd_type
  uint16                     max_msg_size      //!< max size of msgs
);

errno_enum_type msgr_client_add_rex_q
(
  msgr_client_t             *msgr_client_id,   //!< Message Client Handle
  rex_tcb_type              *tcb,              //!< REX TCB pointer
  rex_sigs_type              sig,              //!< REX signal to set
  q_type                    *msg_queue,        //!< queue to place the msg
  q_type                    *free_msg_queue,   //!< queue to get empty buffer
  uint16                     msgr_hdr_offset,  //!< offset to msgr_hdr
  uint16                     cmd_type_offset,  //!< offset to legacy cmd_type
  uint16                     max_msg_size,     //!< max size of msgs
  msgr_id_t                 *queue_id          //!< queue identifier
);

void msgr_rex_free_msg_buf
(
  q_link_type               *rex_msg_link_ptr  //!< pointer to msg buffer
);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif /* MSGR_REX_H */
