#ifndef __WWCOEX_ACTION_IFACE_H__
#define __WWCOEX_ACTION_IFACE_H__
/*!
  @file wwcoex_fw_iface.h

  @brief
   APIs exposed by the CXM for WWAN-WWAN COEX for the inner loop (FW/G Arbitrator)

*/

/*=============================================================================

  Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/api/wwcoex_action_iface.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
02/05/14   jm      Added new ACTION for slot level backoff
11/21/13   jm      Added DDR Freq as desense type
05/05/13   ag      Moved init function
04/06/13   ag      WWAN Coex Mgr interface version 1

=============================================================================*/
/*=============================================================================

                           INCLUDE FILES

=============================================================================*/

/* Keep this below all the includes */
#ifdef __cplusplus
extern "C" {
#endif
/*=============================================================================

                       COMMON TYPES AND DEFINES

=============================================================================*/
#define WWCOEX_MAX_REQUESTING_IDS 10
#define WWCOEX_ACTION_BIT_SIZE 3

/*! Enum for activity types - DL/UL */
typedef enum
{
  ACTIVITY_INVALID,
  ACTIVITY_RX,
  ACTIVITY_TX
}wwan_coex_activity_type;

/*! @brief Enum for identifying type of interference or desense issue */
typedef enum
{
  NO_DESENSE,
  NOISE_DESENSE,
  HARMONIC_DESENSE,
  BLOCKING_DESENSE,
  IM_TYPE1_DESENSE,
  IM_TYPE2_DESENSE,
  SPURIOUS_EMISSIONS,
  DDR_FREQ_DESENSE,
  IM_DESENSE_GPS,
  MAX_DESENSE_TYPES
}wwan_coex_desense_type;

/*! @brief Enum for Mitigation Action for WWAN coex
    This should be in the order of severity. FW-CXM depends on ordering 
    of this enum */
typedef enum
{ 
  /*! Action is None i.e there is no desense */
  ACTION_NONE,

  /*! Action is subframe level backoff at the FW conflict check level 
      i.e desense is expected */
  ACTION_BACKOFF,

  /*! Action is Blank i.e desense is expected */
  ACTION_BLANK,

  /*! Action is Unknown i.e either power or frequency information is 
    unavailable to accurately determine desense action */
  ACTION_UNKNOWN,

  /*! Action can only be 3 bits max */
  ACTION_MAX = ACTION_UNKNOWN
}cxm_action_type;

/*! @brief Action Table for querying action per freqId pair */
typedef struct 
{
  /* FreqId of the tech that is requesting for a conflict check */
  uint32 requesting_freqid;

  /* FreqId of the tech that is conflicting with the requesting tech */
  uint32 conflicting_freqid;

  /* Mitigation action to be taken by the requesting tech if its low priority */
  cxm_action_type action;

}cxm_action_table_s;

typedef struct
{
  /* FreqId of the requesting techs... */
  uint32 *requesting_ids;
  uint32 num_requesting_ids;

  /* FreqId of the conflicting techs... */
  uint32 *conflicting_ids;
  uint32 num_conflicting_ids;

  /* Action Mask... */
  uint32 *actions;
  
  /* Activity Types ... */
  wwan_coex_activity_type requesting_activity;
  wwan_coex_activity_type conflicting_activity;
}cxm_action_query_s;

typedef struct
{
  /* FreqId array of the requesting techs... */
  uint32 *requesting_ids;

  /* Action array for the corresponding requesting ids */
  cxm_action_type *actions;

  uint32 num_requesting_ids;
}cxm_highest_action_query_s;

/*=============================================================================

                        Function Declarations

=============================================================================*/

/*============================================================================

FUNCTION CXM_QUERY_ACTION

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_action
(
  cxm_action_query_s* query_st
);

/*============================================================================

FUNCTION CXM_QUERY_HIGHEST_ACTION

DESCRIPTION
  Query the highest mitigation action for one freqId
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_highest_action
(
  cxm_highest_action_query_s* query_st
);

/*===========================================================================
FUNCTION wwcoex_init_tables

DESCRIPTION
  This API will init the wwcoex conflict tables.
  num_standby_stacks to be 2 for Dual Sim. this is number of concurrent standby 
  stacks.
  num_active_stacks to be 1 for DSDS and 2 for DSDA... this is number of 
  concurrent active stacks.

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_init_tables
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
);

#ifdef __cplusplus
}
#endif

#endif /* __WWCOEX_ACTION_IFACE_H__ */
