#ifndef __WWAN_COEX_MGR_H__
#define __WWAN_COEX_MGR_H__
/*!
  @file wwan_coex_mgr.h

  @brief
   APIs exposed by the CXM for WWAN-WWAN COEX

*/

/*=============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/api/wwan_coex_mgr.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/23/14   sg      desense indication mechanism
06/23/14   jm      Add support for GSM Slot Level Activity Messaging
04/28/14   jm      L+G Band Avoidance support
03/27/14   jm      Added API to query UL/DL status of all techs
03/19/14   jm      WWAN coex based off of RF device assignment
03/19/14   ag      Added TIER_3 priority for ASDIV
01/15/14   jm      Ensure freqID is 16-bit value
01/14/14   rj      Adding support for SGLTE+G channel ID info 
12/07/13   ag      Added new activity tiers for T+G
11/06/13   rj      Adding prob_aggr_mask support
04/05/13   rj      GPIO Based SAR Control added
04/03/13   ag      WWAN Coex Mgr interface version 1
01/16/13   ckk     Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "cxm.h"
#include "trm.h"

/* DAL APIs */
#include "DDITlmm.h"

/*=============================================================================

                       COMMON TYPES AND DEFINES

=============================================================================*/
/*! Number of inactivity slots that will be communicated at a time */
#define CXM_NUM_INACTVITY_SETS  15

/*! Unknown Power indicator: To be used when the power information is not 
  available. */
#define CXM_UNKNOWN_POWER  (int32)(-32765)

/*! Unknown freqId: To be used by tech to register with FW for very short time 
  period activities */
#define CXM_UNKNOWN_FREQID  0x0000FFF0

/*! VALID Channel ID: To be used to check whether channel ID is valid */
#define CXM_CHANNELID_VALID  0x80

/*! Maximum number of entries supported in the Band Avoidance Blacklist */
#define CXM_MAX_SUPP_BL_ENTRIES  24

/*! Maximum number of serving frequencies supported in Band Avoidance */
#define CXM_MAX_SUPP_FREQ_LINK_BA 64

/*! Utilizing this flag indicates that there is not a 1:1 mapping with frequency list.
    Tx/Rx entry in the power list shall be applied to all frequencies */
#define BA_PWR_LIST_FILL_ALL 0x1

/*! Maximum number of entries supported in slot timing info list. 
    4 frame look-ahead * 8 slots per frame */
#define CXM_MAX_NUM_TIMING_SLOTS 32


/*! @brief Enum for Coex State */
typedef enum
{
  /*! None of the technology is in Tx state so coex module is in inactive 
      state */
  COEX_INACTIVE_ST,
  
  /*! Single Tech is in Tx state */
  COEX_SINGLE_ACTIVE_ST,

  /*! Two technologies are in active Tx state  */
  COEX_DUAL_ACTIVE_ST
}wwcoex_state_type;

/*! @brief Enum for Activity Tiers 
           This enum alongwith the techId determine the priority value
 */
typedef enum
{
  /*! Highest Priority Tier: To be used only for ASDIV activity */
  ACTIVITY_TIER_3,

  /*! Second highest Priority Tier: Should be used for activities which needs to be 
    protected from the other tech
    For Example: used by TDSCDMA and GSM.. for other techs, priority is same as
    TIER_10 */
  ACTIVITY_TIER_5,

  /*! Third highest Priority Tier: Should be used for activities which needs to be 
    protected from the other tech
    For Example: PLL Tuning, AGC Acquisition, W PICH Decode, Any G Tx for 2
      slots etc */
  ACTIVITY_TIER_10,

  /*! Fourth highest Priority Tier: Used for W2G IRAT Rx measurements */
  ACTIVITY_TIER_15,
  
  /*! Second lowest Tier: Second highest priority for activities that can be 
    blanked when it conflicts with other tech's higher priority tier.
    e.g. G PCH, G BCCH, Any W Tx*/
  ACTIVITY_TIER_20,

  /*! Lowest Tier: Third highest priority for activities that can be 
    blanked when it conflicts with other tech's higher priority tier. */
  ACTIVITY_TIER_25,

  /* End of the list indicator */
  MAX_ACTIVITY_TIERS
}cxm_activity_type;

/*! @brief Enum for FA Response*/
typedef enum{
  FREQ_AVOID_INVALID,   /*! Not to be used */
  FREQ_AVOID_NACK,      /*! Unable to change serv freq */
  FREQ_AVOID_NO_PROBLEM /*! Current serv freq is acceptable - only used when serv freq is victim */
}wwcoex_fa_res_type; 

/*! @brief Serving Frequency State */
typedef enum{
  SERV_FREQ_OK,         /*! Serving frequency has no desense issue with other active tech */
  SERV_FREQ_AVOID_REQ,  /*! Serving frequency has issue with other active tech - response expected by MCS */
  SERV_FREQ_MAX         /*! End of list indicator */
}wwcoex_serv_freq_state_type;

/*! @brief Blacklist desense type */
typedef enum{
  DESENSE_NONE,           /*! No desense detected */
  DESENSE_AGGR,           /*! Desense is caused by aggressor (Tx) */
  DESENSE_VICTIM,         /*! Desense is caused by victim (Rx) */
  DESENSE_AGGR_VICTIM,    /*! Both Aggressor and Victim */
  DESENSE_MAX             /*! End of list indicator */
}wwan_coex_desense_mode;


/*! @brief Frequency Link information */
typedef struct
{
  /* Frequency ID */
  uint32  freqid;

  /* Frequency information corresponding to the freqId */
  cxm_tech_link_info_s link_info;
}coex_freqid_link_s;

/*! @brief  Frequency ID List */
typedef struct
{
  uint8 num_link_info_sets;
  coex_freqid_link_s  link_list[CXM_MAX_SUPPORTED_LINK_SETS];
}coex_freqid_list_s;


/*! @brief Power Link information */
typedef struct
{
  /* Frequency ID for which power is being updated */
  uint32  freqid;

  /* Direction: UL if Tx Power is updated, DL if Rx is updated 
     or UL_AND_DL if both are updated */
  cxm_tech_link_direction direction;

  /* Tx Power in dBm*10 format. 
     Will be accessed only if direction = UL or UL_AND_DL */
  int32   tx_power;

  /* Rx Power in dBm*10 format. 
     Will be accessed only if direction = DL or UL_AND_DL */
  int32   rx_power;
}coex_power_link_s;


/*! @brief  Power List */
typedef struct
{
  uint8 num_link_info_sets;
  coex_power_link_s  link_list[CXM_MAX_SUPPORTED_LINK_SETS];
}coex_power_list_s;

/*! @brief Activity Table for querying priority corr to an activity */
typedef struct
{
  /* Activity for which priority is desired */
  cxm_activity_type  activity;

  /* Priority value */
  uint32             priority;
}cxm_activity_table_s;

/*! @brief  Inactivity Slot Timing Info */
typedef struct
{
  /*! Start time (in USTMR) of the inactivity slot */
  uint32         start_time;

  /*! Duration (in USTMR) of the inactivity slot */
  uint32         duration;
}cxm_inactivity_slot_list_s;

/*! @brief  WWCoex State Info */
typedef struct
{
  /*! WWCoex State */
  wwcoex_state_type    coex_st;

  /*! Bitmask of probable victim techs */
  uint32               prob_victim_mask;

  /*! Bitmask of probable aggr techs */
  uint32               prob_aggr_mask;

}cxm_wwcoex_state_info_s;

/*! @brief Band Avoidance - Serving Frequency Entry */
typedef struct{
  uint32 frequency;                  /*!< Tech operating frequency in KHz */
  uint32 bandwidth;                  /*!< Tech operating bandwidth in Hz */
  uint32 band;                       /*!< Tech operating band (sys_band_class_e_type) */
  uint32 channel;                    /*!< Tech operating channel */
  cxm_tech_link_direction direction; /*!< Direction: UL/DL; if UL & DL is selected, 
                                          offset will be used to generate DL frequency */
}coex_ba_serv_freq_list_s;

/*! @brief Band Avoidance - Serving Frequency Info */
typedef struct{
  cxm_tech_type               tech_id;
  uint16                      num_entries;                           /*! Number of serving frequencies in list */
  coex_ba_serv_freq_list_s    freq_list[CXM_MAX_SUPP_FREQ_LINK_BA];  /*! List of serving frequencies */
}coex_ba_freq_data;

typedef struct{
  cxm_tech_link_direction   direction; /*! UL if Tx, DL if RX */
  int32                     tx_pwr;    /*! Filled when direction is Tx */
  int32                     rx_pwr;    /*! Filled when direction is Rx */
}coex_ba_serv_pwr_list_s;

/*! @brief Band Avoidance - Serving Power Info */
typedef struct{
  cxm_tech_type              tech_id;
  uint16                     num_entries;                         /*! Number of serving power entries in list */
  uint16                     handle_flag;                         /*! Options to handle pwr_list (Bitwise-OR) */
  coex_ba_serv_pwr_list_s    pwr_list[CXM_MAX_SUPP_FREQ_LINK_BA]; /*! Power provided in same order as Serving
                                                                      Frequency Info (unless handle flag is set) */
}coex_ba_pwr_data;

/*! @brief Band Avoidance - Blacklist Response */
typedef struct{
  cxm_tech_type             tech_id;
  uint16                    blacklist_id;   /*! Correlates w/ Blacklist data */
  wwcoex_fa_res_type        freq_avoid_res; /*! Blacklist response from L1 if serving
                                                frequency has not changed */
}coex_ba_blist_res_data;

/*! @brief Band Avoidance - Blacklist Entry */
typedef struct{
  uint32                    band;         /*!< sys_band_class_e_type */
  uint32                    freq_lower;   /*!< Lower bounds (KHz) - any freq beyond the band should
                                               be considered as MIN(band) */
  uint32                    freq_upper;   /*!< Upper bounds (KHz) - any freq beyond the band should 
                                               be considered as MAX(band) */
  wwan_coex_desense_mode    desense_mode; /*!< Victim (DL) or Aggressor (UL) */
  int16                     rxpwr_thresh; /*!< Rx Power in dBm*10 format; filled when desense mode is Victim;
                                               CXM_UNKNOWN_POWER if no threshold */
  int16                     txpwr_thresh; /*!< Tx Power in dBm*10 format; filled when desense mode is Aggr;
                                               CXM_UNKNOWN_POWER if no threshold */
}coex_ba_list_s;

/*! @brief Band Avoidance - Blacklist Data */
typedef struct{
  uint16                      blacklist_id;                       /*! Correlates w/ blacklist response */
  wwcoex_serv_freq_state_type serv_freq_state;                    /*! Indicates if serving frequency must be changed */
  wwan_coex_desense_mode      serv_freq_mode;                     /*! Desense type of serving frequency (if any) */
  uint16                      num_entries;                        /*! Number of entries in blacklist */
  coex_ba_list_s              freq_list[CXM_MAX_SUPP_BL_ENTRIES]; /*! List of frequency ranges which 
                                                                  have desense issues with other tech*/
}coex_ba_blist_data;

/*! @brief Coex desense indication */
typedef struct{
  boolean                     is_reg;    /*! is the client registered for desense indication  */
  msgr_umid_type              umid;      /*! UMID used for sending indication to clients. 
                                             Clients define their own UMID and pass it to CXM */
}coex_desense_ind_reg_s;

/*! @brief Coex desense indication */
typedef struct{
  boolean                     is_desense;    /*! TRUE indicates Desense. FALSE indicates No Desense  */
}coex_desense_ind_s;


/*=============================================================================

                       MESSAGE DATA TYPES/STRUCTURES

=============================================================================*/

/*! @brief Message for WWAN Coexistence Frequency ID Indication sent to tech L1*/
typedef struct
{
  msgr_hdr_struct_type      msg_hdr; /*!< msgr header contianing msg_id */
  coex_freqid_list_s        freq_data;
}cxm_freqid_info_ind_s;

/*! @brief Message for WWAN Coexistence Power Indication sent to MCS */
typedef struct
{
  msgr_hdr_struct_type      msg_hdr; /*!< msgr header contianing msg_id */
  cxm_tech_type             tech_id;
  coex_power_list_s         power_data;
}cxm_coex_power_ind_s;

/*! @brief Message for Activity Timeline Indication sent to MCS */
typedef struct
{
  msgr_hdr_struct_type      msg_hdr;

  /*! Technology Identifier */
  cxm_tech_type             tech_id;

  /*! Type of activity being registered, Uplink/Downlink/Both */
  cxm_tech_link_direction   direction;

  /*! Start time (in USTMR units) of the activity in the next frame */
  uint32                    start_time;

  /*! End time (in USTMR units) of the activity in the next frame */
  uint32                    end_time;

  /*! Period (USTMR units) with which the activity repeats */
  uint32                    period;
}cxm_activity_timeline_s;

/*! @brief Message for Client Registration for GSM Tx inactivity sent to MCS */
typedef struct
{
  msgr_hdr_struct_type      msg_hdr;

  /*! Technology Identifier of the client that is registering */
  cxm_tech_type             tech_id;

  /*! Registration flag should be set to TRUE when interested in the Tx 
      inactivity information. For deregistration, this flag should be set to
      FALSE */
  boolean                   reg_flag;
}cxm_inactivity_client_reg_s;

/*! @brief GSM Tx Inactivity information for 1X/HDR L1 */
typedef struct
{
  msgr_hdr_struct_type        msg_hdr;

  /*! This flag will be set to FALSE if the inactivity is not slotted and TRUE 
    otherwise. e.g if GSM Tx is off, then this flag will be FALSE. */
  boolean                     is_slotted;

  /*! List of inactivity slots. This should be accessed only if is_slotted 
    is TRUE */
  cxm_inactivity_slot_list_s  slot_list[CXM_NUM_INACTVITY_SETS];

}cxm_inactivity_report_s;

/*! @brief Message for registering/deregistering with GSM for Activity 
           information */
typedef struct
{
  msgr_hdr_struct_type        msg_hdr;

  /*! Registration flag should be set to TRUE when interested in the Tx 
      activity information. For deregistration, this flag should be set to
      FALSE */
  boolean                     reg_flag;

  /*! Type of activity for which timeline is required (e.g Uplink,Downlink) */
  cxm_tech_link_direction     direction;

}cxm_request_activity_info_s;

/*! @brief Timing Slot Info */
typedef struct {
  /*! Frequency ID */
  uint32 freqid; 

  /*! Frequency (kHz) */
  uint32 frequency;
  
  /*!< Tech operating bandwidth in Hz */
  uint32 bandwidth;

  /*!< Tech operating band (sys_band_class_e_type) */
  uint32 band;

  /*! Type of activity being registered, Uplink/Downlink - Both is considered invalid */ 
  cxm_tech_link_direction direction;

  /*! Link Type of slot (Normal/Diversity/PowerMonitor) */
  cxm_tech_link_type link_type;

  /*! Start time (in USTMR units) of the activity in the next  slot (unpadded)*/ 
  uint32 start_time;

  /*! End time (in USTMR units) of the activity in the next  slot  (unpadded)*/ 
  uint32 end_time; 

  /*! Micro priority which maps to ACTIVITY_TIER */ 
  uint32 micro_prio;

  /*! Relative priority of activity */
  cxm_slot_prio_e wlan_prio;

  /*! Power for slot (dBm * 10) */ 
  int32 pwr; 

}cxm_timing_slot_entry; 

/*! @brief Message for Slot Level Timing Indication sent to MCS */ 
typedef struct { 

  msgr_hdr_struct_type msg_hdr; 

  /*! Technology identifier */ 
  cxm_tech_type tech_id; 

  /*!  Number of activity slots in list */ 
  uint8 num_entries;

  /*! List of slots - DTx activity should not be included */
  cxm_timing_slot_entry slot_list[CXM_MAX_NUM_TIMING_SLOTS]; 

}cxm_timing_info_s; 


/*! @brief Message for sending the WWCOEX state information */
typedef struct
{
  msgr_hdr_struct_type        msg_hdr;

  /*! Wwcoex state information... */
  cxm_wwcoex_state_info_s     st_info;

}cxm_wwcoex_state_update_s;

/*! @brief structure for setting DSI value
     this structure will be passed as payload to msgr */
typedef struct
{
  msgr_hdr_struct_type        msg_hdr; /*!< msgr header contianing msg_id */
  cxm_sar_dsi_type            dsi_val; /*!< DSI value */
  boolean                     is_gpio_valid;/*!< GPIO value valid */
  DALGpioValueType            gpio_value; /*!< GPIO value */
}cxm_sar_set_dsi_s;

/*! @brief structure for getting RF Device from Tech L1
     this structure will be passed as input argument to get channel_ID */
typedef struct
{
    cxm_tech_type techid;
    uint8   rf_device; /* RF device (rfm_device_enum_type) returned by TRM */
}cxm_channel_in_type;

/*! @brief structure for channel ID corresponding to RF Device
     this structure will be returned from API call     */
typedef struct
{
   uint8  rx_channel_id;   /* Channel Id for Rx operation */
   uint8  tx_channel_id;   /* Channel Id for Tx operation..ignore it for div device */
}cxm_channel_out_type;


typedef struct 
{
  cxm_tech_type  chain[TRM_MAX_CHAINS];
}cxm_chain_owner_info_type;

/*! @brief structure for chain owner message     */
typedef struct
{
  msgr_hdr_struct_type        msg_hdr; /*!< msgr header contianing msg_id */ 
  cxm_chain_owner_info_type   owner;
}cxm_chain_owner_msg_s;

/*! @brief structure for UL/DL status for a single tech  */
typedef struct
{
  boolean                     is_ul_active; /*!< Has UL info been reported by tech */
  boolean                     is_dl_active; /*!< Has DL info been reported by tech */
}cxm_conn_tbl_entry;

/*! @brief structure for UL/DL status for all techs 
     this structure will be returned from API call     */
typedef struct
{
  cxm_conn_tbl_entry    conn_state[CXM_TECH_MAX]; /*! Array of all techs */
}cxm_conn_tbl_type;

/*! @brief structure for serving frequency indication message (Band Avoidance) */
typedef struct{
  msgr_hdr_struct_type      msg_hdr;
  coex_ba_freq_data         serv_freq_data;
}cxm_coex_ba_freq_ind_s;

/*! @brief structure for serving power indication message (Band Avoidance) */
typedef struct{
  msgr_hdr_struct_type      msg_hdr;
  coex_ba_pwr_data          serv_pwr_data;
}cxm_coex_ba_pwr_ind_s;

/*! @brief structure for Blacklist response message from L1 (Band Avoidance) */
typedef struct{
  msgr_hdr_struct_type       msg_hdr;
  coex_ba_blist_res_data     blist_res_data;
}cxm_coex_ba_blist_res_s;

/*! @brief structure for Blacklist info message to L1 (Band Avoidance) */
typedef struct{
  msgr_hdr_struct_type        msg_hdr;
  coex_ba_blist_data          blacklist_data;
}cxm_coex_ba_ind_s; 

/*! @brief structure for requesting desense indication from MCS */
typedef struct{
  msgr_hdr_struct_type        msg_hdr;
  coex_desense_ind_reg_s      desense_reg;
}cxm_coex_desense_ind_req_s; 


/*! @brief structure for returning desense indication to clients */
typedef struct{
  msgr_hdr_struct_type        msg_hdr;
  coex_desense_ind_s          desense_ind;
}cxm_coex_desense_ind_s; 


/*=============================================================================

                        MSGR UMID Definitions

=============================================================================*/

/*! @brief These indications are used for WWAN Coexistence mitigation
 */
enum 
{
  /*! Tech Power Indication ( Tech L1 to MCS ) for updating Tx/Rx power */
  MSGR_DEFINE_UMID( MCS, CXM, IND, COEX_POWER,
                    COEX_POWER_ID, cxm_coex_power_ind_s ),  

  /*! Tech Tx activity indication (GSM L1 to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, SET_ACTIVITY_TIMELINE,
                    SET_ACTIVITY_TL_ID, cxm_activity_timeline_s),

  /*! Tech interested in inactivity information can register/deregister 
    using this. (1x/HDR L1 to MCS)*/
  MSGR_DEFINE_UMID( MCS, CXM, IND, INACTIVITY_CLIENT_REGISTRATION,
                    INACTIVITY_CLIENT_REG_ID, cxm_inactivity_client_reg_s),

  /*! Tech Tx/Rx slot level activity indication (GSM L1 to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, SET_SLOT_ACTIVITY_TL,
                    SET_SLOT_ACT_TL_ID, cxm_timing_info_s),

  /* FreqId Indications sent by MCS to various Tech L1s */

  /*! GSM1 FreqId Indication ( MCS to GSM L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_GSM1,
                    FREQID_LIST_GSM1_ID, cxm_freqid_info_ind_s ),

  /*! GSM2 FreqId Indication ( MCS to GSM L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_GSM2,
                    FREQID_LIST_GSM2_ID, cxm_freqid_info_ind_s ),

  /*! WCDMA FreqId Indication ( MCS to WCDMA L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_WCDMA,
                    FREQID_LIST_WCDMA_ID, cxm_freqid_info_ind_s ),

  /*! 1X FreqId Indication ( MCS to 1x L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_ONEX,
                    FREQID_LIST_ONEX_ID, cxm_freqid_info_ind_s ),

  /*! HDR FreqId Indication ( MCS to HDR L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_HDR,
                    FREQID_LIST_HDR_ID, cxm_freqid_info_ind_s ),

  /*! TDSCDMA FreqId Indication ( MCS to TDS L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_TDSCDMA,
                    FREQID_LIST_TDSCDMA_ID, cxm_freqid_info_ind_s ),

  /*! LTE FreqId Indication ( MCS to LTE L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_LTE,
                    FREQID_LIST_LTE_ID, cxm_freqid_info_ind_s ),

  /*! GSM3 FreqId Indication ( MCS to GSM L1 ) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, FREQID_LIST_GSM3,
                    FREQID_LIST_GSM3_ID, cxm_freqid_info_ind_s ),

  /* Messages to register for and publish GSM Tx inactivity */

  /*! GSM Tx Inactivity information (MCS to 1X L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, REPORT_INACTIVITY_INFO_ONEX,
                    INACTIVITY_REPORT_ONEX_ID, cxm_inactivity_report_s),

  /*! GSM Tx Inactivity information (MCS to HDR L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, REPORT_INACTIVITY_INFO_HDR,
                    INACTIVITY_REPORT_HDR_ID, cxm_inactivity_report_s),

  /*! Register/deregister for GSM1 Tx Activity information (MCS to GSM L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, REQUEST_ACTIVITY_INFO_GSM1,
                    REQUEST_ACTIVITY_INFO_GSM1_ID, cxm_request_activity_info_s),

  /*! Register/deregister for GSM2 Tx Activity information (MCS to GSM L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, REQUEST_ACTIVITY_INFO_GSM2,
                    REQUEST_ACTIVITY_INFO_GSM2_ID, cxm_request_activity_info_s),

  /*! Register/deregister for GSM3 Tx Activity information (MCS to GSM L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, REQUEST_ACTIVITY_INFO_GSM3,
                    REQUEST_ACTIVITY_INFO_GSM3_ID, cxm_request_activity_info_s),

  /*! WWAN Coex state update indication (MCS to tech L1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, WWCOEX_STATE_UPDATE,
                    WWCOEX_STATE_UPDATE_ID, cxm_wwcoex_state_update_s),

  /*! @brief It is used for SAR DSI value manipulation via diag command */
  MSGR_DEFINE_UMID( MCS, CXM, IND, SAR_SET_DSI,
                    SAR_SET_DSI_ID, cxm_sar_set_dsi_s ),

  /*! @brief This is used for TRM chain owner update */
  MSGR_DEFINE_UMID( MCS, CXM, IND, CHAIN_OWNER_UPDATE,
                    CHAIN_OWNER_UPDATE_ID, cxm_chain_owner_msg_s ),

  /*! Band Avoidance serving frequency information (Tech L1 to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_FREQ,
                    BAND_AVOID_FREQ_ID, cxm_coex_ba_freq_ind_s ),

  /*! Band Avoidance serving power information (Tech L1 to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_PWR,
                    BAND_AVOID_PWR_ID, cxm_coex_ba_pwr_ind_s ),

  /*! Band Avoidance Blacklist response (Tech L1 to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, RSP, BAND_AVOID_BLIST,
                    BAND_AVOID_BLIST_RSP_ID, cxm_coex_ba_blist_res_s ),

  /*! Band Avoidance Blacklist indication (MCS to GSM1) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_BLIST_GSM1,
                    BAND_AVOID_BLIST_IND_GSM1_ID, cxm_coex_ba_ind_s ),

  /*! Band Avoidance Blacklist indication (MCS to GSM2) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_BLIST_GSM2,
                    BAND_AVOID_BLIST_IND_GSM2_ID, cxm_coex_ba_ind_s ),

  /*! Band Avoidance Blacklist indication (MCS to GSM3) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_BLIST_GSM3,
                    BAND_AVOID_BLIST_IND_GSM3_ID, cxm_coex_ba_ind_s ),

  /*! Band Avoidance Blacklist indication (MCS to LTE) */
  MSGR_DEFINE_UMID( MCS, CXM, IND, BAND_AVOID_BLIST_LTE,
                    BAND_AVOID_BLIST_IND_LTE_ID, cxm_coex_ba_ind_s ),

  /*! Desende indication requset (Data Services to MCS) */
  MSGR_DEFINE_UMID( MCS, CXM, REQ, DESENSE_IND,
                    DESENSE_IND_REQ_ID, cxm_coex_desense_ind_req_s ),
};


/*=============================================================================

                        Function Declarations

=============================================================================*/


/*============================================================================

CALLBACK CXM_PRIO_TABLE_CB_T

DESCRIPTION
  Callback that techs can register at bootup to get its complete list of
  priorities. This callback will be called whenever there is a change in
  priorities due to user input like voice call put on hold or priority of
  subscription is changed.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
typedef void (*cxm_prio_table_cb_t)
(
  cxm_tech_type  tech_id,
  cxm_activity_table_s activity_tbl[MAX_ACTIVITY_TIERS]
);

/*============================================================================

FUNCTION CXM_REGISTER_PRIO_TABLE_CB

DESCRIPTION
  Function used by tech L1 to register its priority table callback.
  To deregister, NULL can be passed.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_register_prio_table_cb
(
  cxm_tech_type       tech_id,
  cxm_prio_table_cb_t  prio_cb
);

/*============================================================================

FUNCTION CXM_GET_CHANNEL_ID

DESCRIPTION
  Function used by tech L1 to get channel ID correcponding to RF device.
  
DEPENDENCIES
  None

RETURN VALUE
  Channel ID for both Rx and Tx chain for RF device 

SIDE EFFECTS
  None

============================================================================*/
cxm_channel_out_type cxm_get_channel_id
(
  cxm_channel_in_type input
);

/*============================================================================

FUNCTION CXM_GET_CONN_STATE_TBL

DESCRIPTION
  Function used by tech L1 to retrieve the UL/DL status of all techs
  
DEPENDENCIES
  None

RETURN VALUE 
  Table indicating UL/DL status of each tech 

SIDE EFFECTS
  This function is not mutex protected so results may not reflect
  the latest status

============================================================================*/
cxm_conn_tbl_type cxm_get_conn_state_tbl(void);


#endif /* __WWAN_COEX_MGR_H__ */
