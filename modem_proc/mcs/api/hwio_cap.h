#ifndef HWIO_CAP_H
#define HWIO_CAP_H

/*!
  @file   msgr.h

  @brief  Message Router header file

  @detail
  Defines Message Router related types and interfaces.

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/22/13   GR      Initial release

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include <comdef.h>      /* Definition of basic types and macros */
#include <IxErrno.h>

/*===========================================================================

                    EXTERNAL DEFINES

===========================================================================*/

/*----------------------------------------------------------------------------
  Modem Capability
----------------------------------------------------------------------------*/
typedef enum {

  /* GSM Feature Capability */
  MCS_MODEM_CAPABILITY_FEATURE_GSM,
  
   /* 1X Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_1X,
  
   /* HDR Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_DO,
  
   /* WCDMA Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_WCDMA,
  
   /* HSPA Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_HSPA,
  
   /* LTE Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_LTE,
  
   /* TDSCDMA Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA,
  
   /* Modem Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_MODEM,
  
   /* HSPA above CAT14 Feature Capability.. Also covers HSPA_DC */ 
  MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14,
  
   /* NAV Feature Capability */ 
  MCS_MODEM_CAPABILITY_FEATURE_NAV,

  MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_AVOVE_CAT1,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_AVOVE_CAT2,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3,

  MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8,

  MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABBOVE_CAT10,

  MCS_MODEM_CAPABILITY_FEATURE_MCDO,

  MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABBOVE_CAT6,

  MCS_MODEM_CAPABILITY_FEATURE_RXD,

  MCS_MODEM_CAPABILITY_FEATURE_DSDA,

  MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ,

  MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA,

  MCS_MODEM_CAPABILITY_FEATURE_ADC2,

  MCS_MODEM_CAPABILITY_FEATURE_ADC3,

  MCS_MODEM_NUM_CAPABILITY

} 
mcs_modem_capability_enum;

/*----------------------------------------------------------------------------
  Modem Capability Return Values
----------------------------------------------------------------------------*/
typedef enum
{
  /* modem is not capable */
  MCS_MODEM_CAP_UNAVAILABLE,

    /* modem is capable */
  MCS_MODEM_CAP_AVAILABLE,

    /* we could not deduce the capability due to some failures */
  MCS_MODEM_CAP_UNKNOWN = 0xFF,
}
mcs_modem_cap_return_enum;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*============================================================================

FUNCTION HWIO_CAP_INIT

DESCRIPTION
  Reads the modem capability information and caches it.
  
DEPENDENCIES
  None

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
void hwio_cap_init(void);

/*============================================================================

FUNCTION MCS_MODEM_HAS_CAPABILITY

DESCRIPTION
  Returns if modem is capable of supporting queried modem technology.
  
DEPENDENCIES
  None

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
mcs_modem_cap_return_enum mcs_modem_has_capability
(
  mcs_modem_capability_enum capability /* capability to check for */
);

#endif /* HWIO_CAP_H */
