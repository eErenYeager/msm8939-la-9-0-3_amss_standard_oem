#ifndef TRM_H
#define TRM_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

                   Transceiver Resource Manager Header File

GENERAL DESCRIPTION

  This file provides management of the RF Rx/Tx resources


EXTERNALIZED FUNCTIONS

  trm_reserve_at() - Registers a client's need to access the RF at a specific
    moment in the future for the indicated duration, such as for a paging
    slot.  trm_request() must be used at the specified moment, to actually
    obtain the RF resource.  Implicitly performs a trm_release().

  trm_request() - Request access to an RF chain immediately, for the indicated
    duration.  Implicitly performs a trm_release().

  trm_request_and_notify() - Request access to an RF chain, when it becomes
    available.  Implicitly performs a trm_release().


  trm_extend_duration() - Requests an increase in the duration the RF is
    held for by the client.  If the increase cannot be granted, the original
    grant duration remains in effect.

  trm_change_duration() - Requests a change in the duration the RF is held for
    by the client.  If the full extension cannot be granted, a partial
    extension may result.

  trm_change_priority() - Specifies a change in the reason (and consequently
    priority) for a client's hold on an RF resource.

  trm_exchange() - Swaps the resources that two clients hold.  This is useful
    when the primary client holds the secondary RF chain, and the secondary
    client acquires the primary RF chain.  After this function is called, the
    primary client will hold the primary RF chain, and the secondary client
    will hold the secondary RF chain.

  trm_release() - Releases the clients hold on an RF resource.  If no
    client is waiting for the RF resource, the RF will be turned off.


  trm_get_granted() - This function indicates if a client holds an RF
    resource.  Ideally, the client should already know and maintain knowledge
    of whether or not it holds an RF resource.

  trm_get_rf_device() - Returns the identifier associated with the granted
    RF chain.

  trm_get_1x_mode() - Return the 1X mode

  trm_get_device_mapping() - Returns the device mapping for certain (band
     resource and client) combination.
 
  trm_get_device_mapping_multiple() - Returns the device mapping for certain
    (band resource and client) combination for multiple requests.

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  trm_task() must be called before any other function.

  Before using any RF functions, a client must be granted permission via
  trm_request( ) or trm_request_and_notify().

  When the client is finished using the RF, it must release its hold on the
  RF chain either directly via a call to trm_release(), or indirectly by
  calling trm_reserve_at(), trm_request_and_notify(), or trm_request().


  Copyright (c) 2004-2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.


=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=



==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/api/trm.h#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ---------------------------------------------------------
10/13/2014   sk      API to clear PBR priority for G (714132)
05/21/2014   sr      Changes for supporting DP transition in future while in
                     Paging.
06/21/2014   sk      API to enable disable hopping for L+G coex issue(678827)
06/11/2014   sr      Expose a reserve at api to distinguish redundant vs
                     non-redundant reservations (678054)
04/18/2014   sk      TRM support for Adaptive PAge Skip
04/07/2014   sk      TRM support for QTA
04/03/2014   sk      New Reason for LTE emergency SIB Reads
03/25/2014   sk      New Reason for LTE high priority signalling(653561)
03/28/2014   sr      Changes for QTA gap issue in SGLTE+G (CR:640167)
03/20/2014   sk      SVLTE support for ASDiv, new api to block switching
03/19/14   jm      WWAN coex based off of RF device assignment
02/27/2014   sr      SLTE Changes
03/12/2014   mn      Adding support for connected mode WTR hopping.
02/13/2014   sk      Added channel_maintenance_inv reason 
02/10/2014   sk      Changes for Bolt SHDR 
01/10/2014   sk      Changes for SGLTE+G WTR hopping requirement
01/05/2014   sr      In PBR, increments within 100 ms are redundant(CR:591479)
11/26/2013   mn      Adding support for Opp SGLTE/SGTDS/SVLTE + G.
11/25/2013   sr      PBR Algorithm Implementation
11/21/2013   jm      trm_update_tech_state, update G/W/TDS state in single SIM
10/02/2013   jm      Adding API for querying multiple device mappings  
09/19/2013   rj      Adding TRM support GSM to ASID mapping
09/19/2013   rj      Adding TRM support for G2W TRM_LTA_SUPPORTED
09/12/2013   sk      L+G DSDS support
06/13/2013   sk      ASDiv Enhancement Changes
05/21/2013   mn      Adding support for TSTS.
05/31/2013   rj      trm_get_device_mapping API support 
05/16/2013   sk      ASDiv feature porting
05/14/2013   rj      Added support for SGLTE in TRM
02/04/2012   sr      Add a new priority TRM_ACQ_DIVERSITY for HDR(CR:439864)
05/14/2012   sr      Changes to REVERSE_ANY and REVERSE_BP.
12/07/2012   ag      Remove trm_init header
11/22/2012   mn      Triton DSDA changes.
11/19/2012   rj      Added a new client TRM_CM and new reason TRM_SUBS_CAP_CHANGE.
10/21/2012   sr      TRM Logging fix changes (CR: 412848).
10/03/2012   mn      NikeL DSDS merge
08/13/2012   ag      Added new reason for non-urgent 1x rude wakeup
08/09/2012   sr      Modified the order of trm clients to avoid parser change.
08/07/2012   sr      Dime Changes: Added new clients and api for Dime.
04/20/2012   ag      Added new api - trm_change_reason
02/10/2012   ag      Support for SVLTE+noSVDO mode.
02/08/2012   sr      new methods for rc-init task initialization and startup.
01/10/2012   ag      Adding two new diversity reasons.
09/19/2011   sr      Adding API for querying supported modem technologies.
08/15/2011   sg      Added support for TDSCDMA Layer1
07/07/2011   ag      Support for wakeup manager.
04/22/2011   ag      Single Api that returns SV and SHDR capability
04/21/2011   ag      Api that checks whether shdr mode is enabled.
02/10/2011   sg      Added new reasons for ET and HDR IRAT measurement
11/22/2010   ag      Added new reason SMALL_SCI_CONTINUATION for HDR. 
10/22/2010   sg      Fixed a compilation issue.
10/18/2010   ag      Merged DSDS functionality.
08/23/2010   ag      Merged GSM/WCDMA reasons.
06/17/2010   ag      Added new reason TRM_MC_TRAFFIC for HDR to do a 'hybrid 
                     like' traffic in SHDR mode. 
06/01/2010   hh      Added new reason TRM_SYSTEM_MEASUREMENT for 1x system 
                     measurement changes.
03/09/2010   ag      Removed trm_init() function declaration.
10/28/2009   ag      Moved TRM_NO_CLIENT to trm_client_enum_t
08/31/09     ag      Removing customer.h
02/27/09     ns      Enable trm_get_rf_device for Off Target builds 
02/17/09     cpk     Added support for Off Target builds
09/18/2008   rkc     Added TRM_DEMOD_PAGE_CONTINUATION to trm_reason_enum_t
08/28/2008   adw     Added FEATURE_MCS_TRM to featurize TRM for ULC.
06/06/2008   sg      Added TRM_HP_BROADCAST in trm_reason_enum_t
05/20/2008   sg      Consolidated new NV items into new 1x_mode enum/API
03/18/2008   sg      Remove FTS-3 Featurization from num_antennas/1x SecondaryOnly 
                     functions
02/11/2008   sg      Added NV fields for num antennas and 1x SecondaryOnly mode 
01/24/2008   sg      Added Full Time SHDR-3 support
07/11/2007   cab     Added trm_timestamp_t
10/17/2006   cab     Added TRM_DEMOD_SMALL_SCI_PAGE reason.
09/20/2006   cab     Integrated UMTS
09/20/2006   cab     Added include for customer.h
07/28/2006   jwob    Added #error for !FEATURE_TRM_API_V2
07/13/2006   jwob    Tag parameter to trm_request_and_notify and callback
07/12/2006   jwob    Removed trm_cancel_request_and_notify
05/26/2006   cab     Updated trm_extend_duration comment
05/02/2006   cab     Added ifdef's for offline compilation and testing
12/16/2005   cab     Added some comments 
12/12/2005   grl     Corrected enum to preserve backward compatibility with 
                     TRM log parsers.
11/02/2005   cab     Added BEST_POSSIBLE resource option - pre-empts lower
                     pri clients from chain 0, but takes chain 1 if higher 
                     pri clients have chain 0
09/20/2005   grl     Made the last supported reason TRM_LOW_LAT_TRAFFIC.
09/10/2005   grl     Added broadcast access/traffic reasons.
08/29/2005   grl     Added support for dynamic compatibility masks and unlock
                     immediate in the unlock callback.
06/29/2005   ajn     Added typedef for trm_init's wait callback.
05/26/2005   ajn     Code review comment changes
03/28/2005   ajn     Added GPS_TDM reason.
03/16/2005   ajn     Added get_rf_config.
03/01/2005   ajn     Timestamp to unlock callback.
01/07/2005   ajn     Removed GRANTED_0_GIVE_1.  Use trm_exchange() instead.
12/29/2004   ajn     Moved RFM_* helpers to trm_rf.h
11/12/2004   ajn     Initial AMSS version

============================================================================*/


/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include <comdef.h>
#include <timetick.h>
#include "hwio_cap.h"
#include "rfm_device_types.h"
#include "sys.h"


/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/


/*----------------------------------------------------------------------------
  Transceiver resource clients (who?)
    These represent the possible owners of the Rx/Tx chains.
 
    TRM_xxx_SECONDARY clients are intended for use with the secondary RF chain
    for diversity or for off-frequency searching without tuning the primary
    RF chain away from the active frequency.

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

----------------------------------------------------------------------------*/

typedef enum
{           
  TRM_1X,
  TRM_1X_SECONDARY,

  TRM_HDR,
  TRM_HDR_SECONDARY,

  TRM_GPS,

  TRM_UMTS,
  TRM_UMTS_SECONDARY,

  TRM_GSM1,

  TRM_GSM2,

  TRM_WCDMA,

  TRM_TDSCDMA,
  TRM_TDSCDMA_SECONDARY,

  TRM_UMTS_CA,
  TRM_UMTS_CA_SECONDARY,

  TRM_LTE,
  TRM_LTE_SECONDARY,
  TRM_LTE_CA,
  TRM_LTE_CA_SECONDARY,


  TRM_CM,

  TRM_GSM3,
  
  TRM_GSM_SECONDARY,


  TRM_RF,
  
  /* Dummy client for L+G DSDS */
  TRM_IRAT,

  /* WLAN client */
  TRM_WLAN,
  
  TRM_GPRS1,

  TRM_GPRS2,
  
  /* For internal bound-checking and array-sizing */
  TRM_LAST_CLIENT = TRM_GPRS2,
  
  /* Should always be TRM_LAST_CLIENT + 1 */
  TRM_MAX_CLIENTS,

  TRM_NO_CLIENT    = 0xFF
}
trm_client_enum_t;

/* This driver can only support 5 RF chains */
#define TRM_MAX_CHAINS            5

#define TRM_MAX_EVENT_REG_NUM     10

/*----------------------------------------------------------------------------
  Transceiver resource requests (what?)
    These represent what the clients can ask for.

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

    Note: Always update trm_chains array when modifying this enum

----------------------------------------------------------------------------*/

typedef enum
{
  /* Any available Rx chain */
  TRM_RX_ANY,

  /* ... with the highest sensitivity, nothing if not available */
  TRM_RX_BEST,

  /* The secondary receive chain */
  TRM_RX_SECONDARY,

  /* The receive chain associated with GPS antenna */
  TRM_RX_GPS,

  /* Any RF chain with both Rx and Tx capability */
  TRM_RXTX_ANY,

  /* The RXTX RF chain the best sensitivity, nothing if not available */
  TRM_RXTX_BEST,

  /* ... with the highest sensitivity, or lower if not available */
  TRM_RX_BEST_POSSIBLE,

  /* The RXTX RF chain the best sensitivity, or lower if not available */
  TRM_RXTX_BEST_POSSIBLE,

  /* The RXTX SV RF chain */
  TRM_RXTX_SV,

  /* ... with the highest sensitivity, or lower if its already locked */
  TRM_RX_BEST_POSSIBLE_2,
  
  
  TRM_RX_CA_PRIMARY,

  TRM_RX_CA_SECONDARY,

  /* Preferred resource for a single mode subscription under DSDA context */
  TRM_RXTX_BEST_SMODE,

  TRM_REVERSE_RX_BEST_POSSIBLE,

  TRM_REVERSE_RX_ANY,

  TRM_RX_BEST_POSSIBLE_MOD,

  TRM_RX_BEST_POSSIBLE_MOD_2,

  TRM_RX_SECOND_PREFERRED,

  /* For internal bound-checking */
  TRM_LAST_RESOURCE = TRM_RX_SECOND_PREFERRED,
}
trm_resource_enum_t;



/*----------------------------------------------------------------------------
  Transceiver resource request times (when?)
    When a client needs a resource is composed of two parts:
    1) A start time, when the resource is needed (could be ASAP).
    2) The duration the resource is needed for.
----------------------------------------------------------------------------*/

/* Timestamp type used by TRM (sclks)  See: timetick_get() */
typedef timetick_type             trm_time_t;


/* Time duration type used by TRM (sclks) */
typedef timetick_type             trm_duration_t;

/* 64 bit timestamp for increased range*/
typedef uint64                    trm_timestamp_t;

/*----------------------------------------------------------------------------
  Transceiver resource request reasons (why?)
    Is used, with the requesting client, to determine the priority of the
    request.

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

----------------------------------------------------------------------------*/

typedef enum
{
  /* Perform general access attempt */
  TRM_ACCESS,

  /* Perform access as soon as possible. TRM asks retain lock holders to
     release the lock immediately. */
  TRM_ACCESS_URGENT,

  /* Access attempt while in broadcast */
  TRM_BROADCAST_ACCESS,

  /* Determining the mobile location */
  TRM_LOCATION_FIX,

  /* In a traffic call */
  TRM_TRAFFIC,

  /* Monitoring paging channel */
  TRM_DEMOD_PAGE,

  /* Monitoring Broadcast page/data  */
  TRM_DEMOD_BROADCAST,

  /* Monitoring QPCH bits */
  TRM_DEMOD_QPCH,

  /* Demodulation of the GPS signal */
  TRM_DEMOD_GPS,

  /* Acquiring or reacquiring the system  */
  TRM_ACQUISITION,

  /* Off Frequency Search */
  TRM_OFS,

  /* Diversity */
  TRM_DIVERSITY,

  /* GPS in time-diversity-multiplex mode */
  TRM_GPS_TDM,

  /* In a traffic call during with broadcast on */
  TRM_BROADCAST_TRAFFIC,

  /* Traffic which requires low latency such as IP based video telephony */
  TRM_LOW_LAT_TRAFFIC,

  /* UMTS requests and holds the primary chain indefinitely */
  TRM_TOTAL_CONTROL,

  /* Monitoring paging channel with a small SCI */
  TRM_SMALL_SCI_PAGE,

  /* Monitoring with Max Sensitivity */
  TRM_DEMOD_PAGE_MAX_SENS,

  /* High priority BCMCS */
  TRM_HP_BROADCAST,

  /* Continuation of monitoring paging channel */
  TRM_DEMOD_PAGE_CONTINUATION,

  /* Monitoring page channel. For high priority stack */
  TRM_DEMOD_PAGE_HI,

  /* Monitoring Broadcast page/data. For high priority stack */
  TRM_DEMOD_BROADCAST_HI,

  /* Acquiring or reacquiring the system. For high priority stack */
  TRM_ACQUISITION_HI,

  /* Monitoring with Max Sensitivity. For high priority stack */
  TRM_DEMOD_PAGE_MAX_SENS_HI,

  /**/
  TRM_RESELECT,

  /* For high priority stack */
  TRM_RESELECT_HI,

  /**/
  TRM_CHANNEL_MAINTENANCE,

  /* For high priority stack */
  TRM_CHANNEL_MAINTENANCE_HI,

  /* For reading CBCH bits */
  TRM_DEMOD_CBCH,

  /* For higher priority stack to read CBCH bits */
  TRM_DEMOD_CBCH_HI,
		
  /* For priority inversion  */
  TRM_ACQUISITION_INV,		
  
  /* For high priority stack  */
  TRM_ACQUISITION_INV_HI,

  /* For high priority stack */
  TRM_SMALL_SCI_PAGE_HI,

  /* For Demod Page priority inversion */
  TRM_DEMOD_PAGE_INV,
  
  /* For high priority stack */
  TRM_DEMOD_PAGE_INV_HI,
  
  /* For Background Traffic */
  TRM_BG_TRAFFIC,
  
  /* Performing a system measurement */
  TRM_SYSTEM_MEASUREMENT,

  /* Multi Carrier Traffic */
  TRM_MC_TRAFFIC,
  
  /* Small SCI Page Continuation */
  TRM_SMALL_SCI_PAGE_CONTINUATION,

  /* For IRAT measurements */
  TRM_IRAT_MEASUREMENT,

  /* For Envelope Tracking */
  TRM_ENVELOPE_TRACKING,

  /* For secondary lock */
  TRM_DIVERSITY_INTERLOCK,

  /* Diversity reason for low CM threshold */
  TRM_DIVERSITY_LOWCMTHRESH,

  /* Diversity reason while performing access */
  TRM_ACCESS_DIVERSITY,

  /* Diversity reason while performing idle wakeup */
  TRM_IDLE_DIVERSITY,

  /* Non-urgent Rude wakeup like for GPS search */
  TRM_DEMOD_PAGE_NONURGENT,

  /*  Demod Page Continuation for HI client */
  TRM_DEMOD_PAGE_CONTINUATION_HI,

  /*  For Subscription Capability change */
  TRM_SUBS_CAP_CHANGE,

  TRM_ACQ_DIVERSITY,

  /* For Low priority Acquisition  */
  TRM_ACQUISITION_LOW,	

  /* Sleep */
  TRM_SLEEP,
  
  /* For high priority acquisition */
  TRM_ACQUISITION_MAX, 	
  
  /* inverted priority for channel maintenance */
  TRM_CHANNEL_MAINTENANCE_INV,
  
    /* SIB READ EMERGENCY NOTIFICATION */
  TRM_SIB_EMERGENCY_NOTIFICATION,
  
  /* High Priority Signalling */
  TRM_HIGH_PRIORITY_SIGNALLING,
  
  /* For internal bound-checking and array-sizing */
  TRM_NUM_REASONS
}
trm_reason_enum_t;



/*----------------------------------------------------------------------------
  Request results (what happened?)
    Returned by trm_request or passed as an argument to the
    trm_request_and_notify callback.

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

----------------------------------------------------------------------------*/

typedef enum
{
  /* Sorry, you can't have the requested resource. */
  TRM_DENIED,

  /* You have been granted usage of the primary RF chain */
  TRM_GRANTED_CHAIN0,

  /* You have been granted usage of the secondary RF chain */
  TRM_GRANTED_CHAIN1,
  
  /* You have been granted usage of the tertiary RF chain */
  TRM_GRANTED_CHAIN2,

  /* You have been granted usage of the fourth RF chain */
  TRM_GRANTED_CHAIN3,

  /* You have been granted usage of the fourth RF chain */
  TRM_GRANTED_CHAIN4
}
trm_grant_event_enum_t;



/*----------------------------------------------------------------------------
  Unlock request events
    A higher priority client needs the resource you hold ...

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

----------------------------------------------------------------------------*/

typedef enum
{
  /* Unlock request cancelled.  You no-longer need to unlock. */
  TRM_UNLOCK_CANCELLED,

  /* ... at the indicated time.  You can hold the lock until then. */
  TRM_UNLOCK_BY,

  /* ... as soon as possible.  Release the lock when convenient, but soon. */
  TRM_UNLOCK_REQUIRED,

  /* ... immediately for a page response!  Give it up ASAP! */
  TRM_UNLOCK_IMMEDIATELY,

  /*. . . as soon as possible for a pending band retune or handoff */
  TRM_UNLOCK_BAND_INCOMPATIBLE
}
trm_unlock_event_enum_t;



/*----------------------------------------------------------------------------
  RF device corresponding to RF chain owned by TRM lock
----------------------------------------------------------------------------*/

/* Identifer representing "no RF chain is owned" */
#define TRM_NO_DEVICE             RFM_MAX_DEVICES



/*----------------------------------------------------------------------------
  RF configurations

    Note: Always add to the end in order to preserve backward compatibility
          with the TRM log parsers.

----------------------------------------------------------------------------*/

typedef enum
{
  /* Single RF chain */
  TRM_RF_SINGLE,

  /* Dual RF chain - both must tune to the same frequency */
  TRM_RF_DIVERSITY,

  /* Dual RF chain - independently tunable - unequal sensitivity */
  TRM_RF_UNEQUAL_SENSITIVITY,

  /* Dual RF chain - independently tunable - equal sensitivity */
  TRM_RF_EQUAL_SENSITIVITY,

  /* Two Tx chains with an exclusive chain for 1x */
  TRM_RF_DUAL_TX
}
trm_rf_enum_t;



/*----------------------------------------------------------------------------
  TRM Modes which can be enabled/disabled
----------------------------------------------------------------------------*/

typedef enum
{
  /* Simultaneous 1x idle with HDR traffic mode */
  TRM_MODE_SIMUL_1XIDLE_HDRTC
}
trm_mode_enum_t;

#define TRM_SHDR_IS_ENABLED  (0x1)
#define TRM_SVDO_IS_ENABLED  (0x2)
#define TRM_SVLTE_IS_ENABLED (0x4)
#define TRM_DSDA_IS_ENABLED (0x10)
#define TRM_SGLTE_SGTDS_IS_ENABLED (0x20)
#define TRM_1X_SLTE_IS_ENABLED (0x40)
#define TRM_GSM_SLTE_IS_ENABLED (0x80)

/* Below macros are used by TRM and its clients for tune away */
/* Extension flag bitmaps and masks */
#define TRM_EXTENSION_FLAG_DEFAULT (0x00)

/* these masks should correspond to single bit positions in an 8-bit map */
#define TRM_PRIORITY_INV_ENABLED   (0x01)
#define TRM_QUICK_TA_SUPPORTED     (0x02)
#define TRM_ALTERNATE_PAGE_ENABLED (0x04)
#define TRM_LTA_SUPPORTED          (0x08)

/* Number of subscriptions that are possible & supported */
#define MAX_NUM_OF_SUBSCRIPTIONS       MAX_AS_IDS

/* rfm device capability */
#define TRM_RF_DEVICE_RX_CAPABLE      RFM_DEVICE_RX_SUPPORTED
#define TRM_RF_DEVICE_TX_CAPABLE      RFM_DEVICE_TX_SUPPORTED
#define TRM_RF_DEVICE_DR_CAPABLE      0x04

/*----------------------------------------------------------------------------
  TRM 1X Modes
----------------------------------------------------------------------------*/

typedef enum
{
  /* Standard 1X operation */
  TRM_1X_MODE_NORMAL,

  /* Require 1x to operate on primary antenna in SOODA operation mode  */
  TRM_1X_MODE_FORCE_SOODA,

  /* 1X should only run on the secondary chain */
  TRM_1X_MODE_SECONDARY_ONLY,

  /* Undefined operation */
  TRM_1X_MODE_UNDEFINED
}
trm_1x_mode_enum_t;

/*  Band Request Grant Type  */
typedef enum {
  
  /* Band tune or cancel request is denied */
  TRM_BAND_REQUEST_DENIED,

  /* Band tune request is pending */
  TRM_BAND_REQUEST_PENDING,

  /* Band tune request is granted */
  TRM_BAND_REQUEST_GRANTED

} trm_band_request_grant_type;


/*----------------------------------------------------------------------------
  Result of a change request e.g request to change reason for holding chain
----------------------------------------------------------------------------*/

typedef enum
{
  /* Change request is denied */
  TRM_CHANGE_DENIED,

  /* Change request is accepted. This can still result in an unlock callback
     to the requestor */
  TRM_CHANGE_ACCEPTED,

  /* Change request is pending. expect a callback... */
  TRM_CHANGE_PENDING

} trm_change_result_enum_t;

/*----------------------------------------------------------------------------
  TRM G Client Priority
----------------------------------------------------------------------------*/
typedef enum
{
  /* Low Priority */
  TRM_PRIO_LOW,

  /* High Priority */
  TRM_PRIO_HIGH,

  /* Undefined Priority */
  TRM_PRIO_UNDEFINE
}
trm_client_priority_enum_t;

/* Size of band-chain mapping information */
#define TRM_DEVICE_MAPPING_INPUT_SIZE	10


/*----------------------------------------------------------------------------
  Payload for trm_request_and_notify() / (*trm_grant_callback_t)() pair
----------------------------------------------------------------------------*/

typedef uint32                    trm_request_tag_t;

/*----------------------------------------------------------------------------
  Band type used to communicate with tech and RF.
----------------------------------------------------------------------------*/

typedef int32                    trm_band_type;

/*----------------------------------------------------------------------------
  Band type
----------------------------------------------------------------------------*/
typedef  sys_band_class_e_type trm_band_t;

/*----------------------------------------------------------------------------
  Channel Type
----------------------------------------------------------------------------*/
typedef  uint32 trm_channel_t;

/*----------------------------------------------------------------------------
  Frequency Type
----------------------------------------------------------------------------*/
typedef struct
{
  trm_band_t band;
  uint32     num_channels;
  trm_channel_t* channels;
} trm_frequency_type_t;


/*----------------------------------------------------------------------------
  Grant Return Type
----------------------------------------------------------------------------*/
typedef struct
{
  trm_grant_event_enum_t grant;
} trm_chain_grant_return_type;

typedef struct
{
  trm_band_request_grant_type grant;
} trm_band_grant_return_type;

/*----------------------------------------------------------------------------
  Structure for Multimode AS-ID mapping with tech clients 
----------------------------------------------------------------------------*/

typedef struct
{
  sys_modem_as_id_e_type multimode_as_id;
  trm_client_enum_t	     gsm_id_list[MAX_NUM_OF_SUBSCRIPTIONS];
} trm_tech_map_table_t;

/*----------------------------------------------------------------------------
  Structure to hold the current priority of the TRM GSM clients.
----------------------------------------------------------------------------*/
typedef struct trm_client_priority_struct
{
  /* Client Id */
  trm_client_enum_t           client_id;
  
  /* Priority of the client */
  trm_client_priority_enum_t  client_prio;

  /* Subscription capability tech mapping as per multimode subs */
  trm_tech_map_table_t         tech_asid_map;
} trm_client_priority_type;

/*----------------------------------------------------------------------------
  Structure to capture client data
----------------------------------------------------------------------------*/
typedef struct
{
  /* Client Id */
  trm_client_enum_t       client_id;
  
  /* Reason for which the client is requesting for chain */
  trm_reason_enum_t       reason;

} trm_client_info_t;

/*----------------------------------------------------------------------------
  Data for unlock callback advanced. To be used with trm_retain_lock_advanced.
----------------------------------------------------------------------------*/
typedef struct 
{
  /* Client Id of the client being 
     asked to unlock the chain */
  trm_client_enum_t       unlock_client;
  
  /* The event being sent to the client */
  trm_unlock_event_enum_t event;

  /* Sclk timestamp for TRM_UNLOCK_BY */
  uint32                  unlock_by_sclk;

  /* Information about the winning client.
     Higher priority client who's request is 
     causing the unlock notification */
  trm_client_info_t       winning_client;
} trm_unlock_cb_advanced_data_t;

/*----------------------------------------------------------------------------
  Struct for DRX Update.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t client; /* client setting drx input */
  uint32            drx_cycle; /* drx cycle in milli-seconds */

} trm_drx_cycle_input_type;




/*----------------------------------------------------------------------------
  Struct for PBR Mode Registration.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t client;    /* client setting Page Block Rate Modes */
  uint64            pbr_modes; /* Modes for which PBR modes are set */

} trm_pbr_modes_input_type;

/*----------------------------------------------------------------------------
  EFS Struct for PBR.
----------------------------------------------------------------------------*/
typedef PACK(struct) 
{
  uint8 pbr_bias_1x;
  uint8 pbr_bias_hdr;
  uint8 pbr_bias_gsm;
  uint8 pbr_bias_wcdma;
  uint8 pbr_bias_lte;
  uint8 pbr_bias_tdscdma;
  uint8 pbr_bias_gsm2;
  uint8 reserved1;
  uint8 reserved2;
  uint8 reserved3;
  uint8 reserved4;

} trm_pbr_efs_data_type;

/*----------------------------------------------------------------------------
  Types of Async events supported through async callbacks
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_ASYNC_EVENT_DEVICE_HOP_REQ,

  /* Adaptive page skipping state update */
  TRM_ASYNC_EVENT_APS_STATE_UPDATE,

  /* Quick tune away state update */
  TRM_ASYNC_EVENT_QTA_STATE_UPDATE,

  TRM_ASYNC_EVENT_MAX
} trm_async_event_enum_type;

/*----------------------------------------------------------------------------
  Denotes the result of an ASYNC event.
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_ASYNC_EVENT_RESPONSE_SUCCESS,

  TRM_ASYNC_EVENT_RESPONSE_FAIL_NOT_READY,

  TRM_ASYNC_EVENT_RESPONSE_FAIL_NO_RETRY
} trm_async_event_response_result_enum_type;

/*----------------------------------------------------------------------------
  Data for the Device hopping request event.
----------------------------------------------------------------------------*/
typedef struct
{
  /* The grant enum type for the device to hop to*/
  trm_grant_event_enum_t    grant;

  /* RF device for the device to hop to */
  rfm_device_enum_type      rf_device;
} trm_async_event_device_hop_req_struct_type;

/*----------------------------------------------------------------------------
  Union which contains the data for different async events.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_async_event_device_hop_req_struct_type dev_hop_req;
} trm_async_event_union_type;

/*----------------------------------------------------------------------------
  Data for the Device hopping request event.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_async_event_response_result_enum_type result;
} trm_async_event_response_device_hop_req_struct_type;

/*----------------------------------------------------------------------------
  Union which contains the data for different async responses.
----------------------------------------------------------------------------*/
typedef struct
{
  trm_async_event_response_device_hop_req_struct_type dev_hop_req;
} trm_async_event_response_union_type;

/*----------------------------------------------------------------------------
  Data type passed as input to the ASYNC event callback
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t          client_id;

  trm_async_event_enum_type  event;

  trm_async_event_union_type data;

} trm_async_event_callback_data_type;

/*----------------------------------------------------------------------------
  Data type passed as input to the ASYNC response API
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t                   client_id;

  trm_async_event_enum_type           event;

  trm_async_event_response_union_type data;

} trm_async_event_response_data_type;

/*----------------------------------------------------------------------------
  Callback function for Async events.
----------------------------------------------------------------------------*/
typedef void (* trm_async_event_callback_type)
(
  trm_async_event_callback_data_type* event_data
);

/*----------------------------------------------------------------------------
  Data type passed as input to the register ASYNC callback API
----------------------------------------------------------------------------*/
typedef struct
{
  /* Identifier of the client registering for the event callbacks */
  trm_client_enum_t             client_id;

  /* Bitmask of the events the client is registering for. Masked using
     the enum trm_async_event_enum_type */
  uint32                        events_bitmask;

  /* Function pointer of the function to be called to nofify the
     client of the event */
  trm_async_event_callback_type callback_ptr;
} trm_register_async_event_cb_data_type;

/*----------------------------------------------------------------------------
  Data to denote the HW capability
----------------------------------------------------------------------------*/
typedef struct 
{
  /*  This is an array of bitmasks to be indexed using sys_sys_mode_e_type. It denotes the 
      technologies on a multimode subscription that are compatible with each other. That is,
      they can hold different chains simultaneously.

      Each bit in the bitmask corresponds to a tech the index tech is simultaneously
      compatible with. 

      Also, the values of the bitmasks should show symmetry. That is, if 1x is compatible
      with LTE, LTE should be made compatible with 1x.
      
      For Example, to enable SVLTE and SGLTE: 
      simult_cap[SYS_SYS_MODE_LTE] =  SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_HDR)  | 
                                              SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_CDMA) | 
                                              SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_GSM); 
      simult_cap[SYS_SYS_MODE_CDMA] = SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_LTE);
      simult_cap[SYS_SYS_MODE_HDR]  = SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_LTE);
      simult_cap[SYS_SYS_MODE_GSM]  = SP_CONV_ENUM_TO_BIT_MASK(SYS_SYS_MODE_LTE); */
  uint32 simult_cap[SYS_SYS_MODE_MAX];

  /* Denotes the maximum number of concurrently active subscriptions supported. 
     It is also the number of subscriptions that can transmit at the same time
     For example: For SSSS, max_concurrent_active_subscriptions = 1,
                  For DSDS, max_concurrent_active_subscriptions = 1,
                  For DSDA, max_concurrent_active_subscriptions = 2,
                  For TSTS, max_concurrent_active_subscriptions = 1 and so on. */
  uint32 max_concurrent_active_subscriptions;
}trm_hw_cap_info;


/* Chain owner and request information callback for clients interested in
   state update for particular client */
typedef struct
{
  /* Chain owner information */
  trm_client_enum_t chain_owner[TRM_MAX_CHAINS];
  trm_client_enum_t client;    /* Client's whose state update is sent */
  trm_reason_enum_t reason;   /* Reason for which client is holding chain */
  trm_band_type band;          /* Band class used */
  uint8 priority;
  boolean  chain_owner_changed;

} trm_state_info_type;

/*============================================================================

CALLBACK TRM_GRANT_CALLBACK_TYPE

DESCRIPTION
  The prototype for event callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_grant_callback_type)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_chain_grant_return_type          grant,

  /* Anonymous payload echoed from trm_request_and_notify() */
  trm_request_tag_t               tag
);


/*============================================================================

CALLBACK TRM_GRANT_CALLBACK_T

DESCRIPTION
  The prototype for event callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_grant_callback_t)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_grant_event_enum_t          event,

  /* Anonymous payload echoed from trm_request_and_notify() */
  trm_request_tag_t               tag
);



/*============================================================================

CALLBACK TRM_GRANT_ENH_CALLBACK_TYPE

DESCRIPTION
  The prototype for event callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_grant_enh_callback_type)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_chain_grant_return_type           grant,

  /* Anonymous payload echoed from trm_request_and_notify() */
  trm_request_tag_t               tag,

  /* Duration by which the lock is extended */
  trm_duration_t                  duration

);


/*============================================================================

CALLBACK TRM_GRANT_ENH_CALLBACK_T

DESCRIPTION
  The prototype for event callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_grant_enh_callback_t)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_grant_event_enum_t          event,

  /* Anonymous payload echoed from trm_request_and_notify() */
  trm_request_tag_t               tag,

  /* Duration by which the lock is extended */
  trm_duration_t                  duration

);



/*============================================================================

CALLBACK TRM_UNLOCK_CALLBACK_T

DESCRIPTION
  The prototype for unlock event callback functions, used by the Transceiver
  Resource Manager to inform the clients of when it should unlock a resource
  it holds.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_unlock_callback_t)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The event being sent to the client */
  trm_unlock_event_enum_t         event,

  /* Sclk timestamp for TRM_UNLOCK_BY */
  uint32                          unlock_by_sclk
);



/*============================================================================
CALLBACK TRM_BAND_GRANT_CALLBACK_T

DESCRIPTION
  The prototype for band callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
typedef void (* trm_band_grant_callback_type)
(
  trm_client_enum_t client,    /* Client requested for the band change */
  trm_band_type band,          /* Band class */
  trm_band_grant_return_type granted,   /* Status of band grant */
  trm_request_tag_t tag
);

/*============================================================================
CALLBACK TRM_TECH_STATE_UPDATE_CALLBACK_T

DESCRIPTION
  The prototype for callback functions, used by the Transceiver manager
  to inform the clients of client's state for each tech.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
typedef void (* trm_tech_state_update_callback_type)
(
  trm_state_info_type trm_state
);

/*============================================================================
CALLBACK TRM_ASYNC_RESEPONSE_TYPE

DESCRIPTION
  The prototype for callback functions, used by the Transceiver manager
  to inform the clients of a particular event.

DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_async_event_response
(
  trm_async_event_response_data_type* resp_data
);

/*----------------------------------------------------------------------------
  Antenna Switch Configuration Enum Type.
----------------------------------------------------------------------------*/
typedef enum {

  /* PRx --> Ant0; DRx --> Ant1 */
  TRM_ANT_SWITCH_DIV_CONFIG_0,

  /* PRx --> Ant1; DRx --> Ant0 */
  TRM_ANT_SWITCH_DIV_CONFIG_1,

    /* Default configuration */
  TRM_ANT_SWITCH_DIV_CONFIG_DEFAULT,

  /* Current Settings */
  TRM_ANT_SWITCH_DIV_CONFIG_CURRENT,

  /* Preferred Config*/
  TRM_ANT_SWITCH_DIV_CONFIG_PREFERRED,

  /* The switch is in transition */
  TRM_ANT_SWITCH_DIV_CONFIG_IN_TRANSITION,
  
  /* Invalid configuration */
  TRM_ANT_SWITCH_DIV_CONFIG_INVALID = TRM_ANT_SWITCH_DIV_CONFIG_IN_TRANSITION,

  /* Maximum configuration, can be cleaned */
  TRM_ANT_SWITCH_DIV_CONFIG_MAX,

} trm_ant_switch_div_config_type;


/*----------------------------------------------------------------------------
  Antenna Switch Configuration Input Enum Type.
----------------------------------------------------------------------------*/
typedef enum
{
  /* return the default antenna configuration */
  TRM_ANT_SWITCH_CONFIG_DEFAULT,

  /* return the current antenna configuration */
  TRM_ANT_SWITCH_CONFIG_CURRENT,
  
  TRM_ANT_SWITCH_CONFIG_MAX,
} trm_ant_switch_get_input_type;



typedef uint64 trm_ant_switch_div_dwelling_time_type;

/*----------------------------------------------------------------------------
  Feature Override Enum Type. This is used to indicate if the feature On/Off
  settings in NV has been explicitly over ridden or not through QMI
----------------------------------------------------------------------------*/
typedef enum
{
  /* Feature Override is On */
  TRM_ANT_SWITCH_FEATURE_OVERRIDE_ON,

  /* Feature Override is Off */
  TRM_ANT_SWITCH_FEATURE_OVERRIDE_OFF,

  /* Feature Override is Unset */
  TRM_ANT_SWITCH_FEATURE_OVERRIDE_UNSET,
} trm_ant_switch_feature_override_type;

/*----------------------------------------------------------------------------
  Antenna Switch Div Test Mode Ctrl Type.
----------------------------------------------------------------------------*/
typedef enum
{
  TYPICAL,
  ALTERNATING_ON_OFF,
  ALTERNATING_OFF_FORCED_SWITCH,
  ALTERNATING_ON_OFF_FORCED_SWITCH,
  ANT_SWITCH_DIV_TEST_MODE_CTRL_MAX,
} trm_ant_switch_div_test_mode_ctrl_type;


/*----------------------------------------------------------------------------
  Antenna Switch Client Mode Types, In Ascending order of priority
----------------------------------------------------------------------------*/
typedef enum
{

  /* INACTIVE: Default/OOS */
  TRM_CLIENT_MODE_INACTIVE,

  /* client is entering receive mode */
  TRM_CLIENT_MODE_RX_ONLY,

  /* INTERNAL */
  TRM_CLIENT_MODE_RXTX_LOW,

  /* RXACQ: acquisition mode */
  TRM_CLIENT_MODE_RXACQ,

  /* SLEEP: IDLE sleep */
  TRM_CLIENT_MODE_SLEEP,

  /* IDLE WAKEUP: Idle wakeup */
  TRM_CLIENT_MODE_IDLE_WAKEUP,

  /* client is entering Transmit mode */
  TRM_CLIENT_MODE_RXTX,


  /*! following modes are decprecated */


  /* client is entering receive mode */
  TRM_CLIENT_MODE_DEF_ENFORCED_RX,

  TRM_CLIENT_MODE_LOCK_SWITCH,

  TRM_CLIENT_MODE_LISTEN,

  TRM_CLIENT_MODE_MAX

} trm_client_mode_type;



typedef enum
{
  /* sv keep, idle default */
  TRM_ANT_SWITCH_SV_KEEP_IDLE_DEF,
  /* default in SV and idle mode */
  TRM_ANT_SWITCH_SV_DEF_IDLE_DEF,
  /* keep in SV and idle mode */
  TRM_ANT_SWITCH_SV_KEEP_IDLE_KEEP,
  /* default in SV and keep in idle */
  TRM_ANT_SWITCH_SV_DEF_IDLE_KEEP

} trm_ant_switch_div_sv_idle_behavior;

/*----------------------------------------------------------------------------
  Antenna Switch Configuration Data Type. This data is initialized in NV.
----------------------------------------------------------------------------*/
typedef PACK(struct) 
{

  /* default/current configuration */
  trm_ant_switch_div_config_type  configuration;

  /* test mode control */
  trm_ant_switch_div_test_mode_ctrl_type test_mode_ctrl;
  
  /* dwelling time */
  trm_ant_switch_div_dwelling_time_type dwelling_time;

  /* default configuration when in SV transmit mode */
  trm_ant_switch_div_config_type      sv_default_configuration;
  
  /* ant switch diversity behavior
     0: SV KEEP, Idle Default
     1: SV Default, Idle Default
     2: SV KEEP, Idle KEEP
     3: SV Default, Idle KEEP */
  // To Do: rename this variable 
  trm_ant_switch_div_sv_idle_behavior     sv_behavior;

  /* limit for idle_idle collision before the denied client
     is given the preferred antenna */
  uint8 idle_idle_col_limit;

  /* reserved for future use */
  uint16 reserved1;
  
} trm_ant_switch_div_data_type;


/*----------------------------------------------------------------------------
  Antenna Switch callback info type. This enum is sent from TRM to the L1s.
  This indicates if any config completes occurred or if any config action has
  to be initiated from L1s.
----------------------------------------------------------------------------*/
typedef enum
{
  /* Represents action:
      TECH needs to do switching to config specified by config parm,
      TRM will pass this action in case of PASSIVE SWITCHING */
  TRM_ANT_SWITCH_INITIATE_SET_CONFIG,

  /* Represents info:
      Switching was complete. When a tech subscribes to switch
      change during registering mode with TRM, TRM will call cb
      with this action everytime the switching is completed */
  TRM_ANT_SWITCH_SET_CONFIG_DONE,

   /* Represents info:
      Switching has started by other tech in tx. When a tech subscribes
      to switch change during registering mode with TRM, TRM will call cb
      with this action everytime the switching is initiated */
  TRM_ANT_SWITCH_SET_CONFIG_STARTED,

  /* Represents info: NOT USED */
  TRM_ANT_SWITCH_ENABLED,

  /* represents info: NOT USED */
  TRM_ANT_SWITCH_DISABLED,

  /* no action/info: INTERNAL */
  TRM_ANT_SWITCH_ACTION_NONE

} trm_ant_switch_action_type;


/*----------------------------------------------------------------------------
  Redundant Reservation Info Type.
----------------------------------------------------------------------------*/
typedef enum
{
  /* Redundant Call */
  TRM_RES_AT_REDUNDANT_CALL,

  /* Non Redundant Call */
  TRM_RES_AT_NON_REDUNDANT_CALL,

   /* Max */
  TRM_RES_AT_REDUNDANT_MAX,

} trm_redundant_res_at_enum_type;

/*----------------------------------------------------------------------------
  Input struct for fall-back to default config cb.
----------------------------------------------------------------------------*/
typedef struct 
{
  /* feedback of action taken or to be taken */
  trm_ant_switch_action_type            action;

  /* configuration which was set or needs to be set */
  trm_ant_switch_div_config_type       config;

  /* feature override info */
  trm_ant_switch_feature_override_type feature_override;

  /* Client_id: used by G to diff betwn G1, G2 or G3 */
  trm_client_enum_t                    client;

} trm_ant_switch_cb_info_type;


/*----------------------------------------------------------------------------
  Callback input for calling to force fallback to default config.
----------------------------------------------------------------------------*/
typedef void (*trm_ant_switch_div_callback_t)
(
  /* ant config info type */
  trm_ant_switch_cb_info_type ant_cfg_info
);


/*----------------------------------------------------------------------------
  Ant Switch Set Mode Return Type.
----------------------------------------------------------------------------*/
typedef enum
{
  /* Invalid client provided as input */
  TRM_SET_MODE_DENIED_INVALID_PARAMETERS,

  /* Set Mode Granted */
  TRM_SET_MODE_GRANTED,
  
  /* Set the switch to the mentioned state in config and continue */
  TRM_SET_SWITCH_AND_CONTINUE,

  /* feature is not supported */
  TRM_SET_MODE_DENIED_FEATURE_UNSUPPORTED,
  
  /* set mode request denied */
  TRM_SET_MODE_DENIED    

} trm_ant_switch_set_mode_return_type;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode Return Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* set mode return type */
  trm_ant_switch_set_mode_return_type set_mode_grant;

  /* configuration to be set if feedback is set rf and continue */
  trm_ant_switch_div_config_type       config;

  /* Anonymous payload to be echoed through set mode grant callback */
  trm_request_tag_t                     tag;

} trm_ant_switch_set_mode_type;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode Call Back Type.
----------------------------------------------------------------------------*/
typedef void (*trm_ant_switch_set_mode_callback_t)
(
  /* set mode call-back info */
  trm_ant_switch_set_mode_type set_mode_info
);

/*----------------------------------------------------------------------------
  Ant Switch subscribe switch info
----------------------------------------------------------------------------*/
typedef enum{
  /* subscribe to none */
  TRM_SUBS_NONE=0x00,
  /* subscribe to switch change */
  TRM_SUBS_SW_COMPLETE,
  /* subscribe to permission change */
  TRM_SUBS_SW_PERM_CHANGE,
  /* invalid subscription */
  TRM_SUBS_SW_INVALID
}trm_ant_switch_subcribe_switch_info;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode Input Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* client requesting the mode set*/
  trm_client_enum_t client;

  /* Mode to be set */
  trm_client_mode_type mode;

  /* set mode call-back info. This is called when set mode request is
     granted or denied. */
  trm_ant_switch_set_mode_callback_t set_mode_cb;

  /* ant switch call-back info. This is used when an explicit set swith
     has to be initiated or if feed-back has to be provided about any other
     set info*/
  trm_ant_switch_div_callback_t      ant_switch_cb;

  /* to subscribe to switch completion, below value should be set to TRUE
     TRM will call the callback function with actiong: SET_CONFIG_DONE to
     inform the client that switching is complete */
  boolean                            inform_switch_change;

  /* Deprecated... NOT USED*/
  trm_ant_switch_subcribe_switch_info   subscribe_switch_info;

  /* Anonymous payload to be echoed through set mode grant callback */
  trm_request_tag_t                     tag;

} trm_set_mode_input_type;

/*----------------------------------------------------------------------------
  Antenna  Switch Return Type.
----------------------------------------------------------------------------*/
typedef enum
{
  /* Set Switch completed successfully */
  TRM_ANT_SWITCH_DIV_SET_SUCCESS,

  /* set switch failed */
  TRM_ANT_SWITCH_DIV_SET_CFG_DENIED,

  /* Set Switch failed. Feature Disabled */
  TRM_ANT_SWITCH_DIV_SET_FAIL_FEAT_DISABLED,

  /* Failed: Modem in SV transmit mode */
  TRM_ANT_SWITCH_DIV_SET_FAIL_IN_SV_TRANSMIT,

  /* Failed: In Enforced Default mode */
  TRM_ANT_SWITCH_DIV_SET_FAIL_IN_ENFORCED_DEFAULT,

  /* Failed: Invalid input provided */
  TRM_ANT_SWITCH_DIV_SET_FAIL_INVALID_INPUT,

  /* Failed: In transition */
  TRM_ANT_SWITCH_DIV_SET_FAIL_IN_TRANSITION,

  /* Failed: Switch is locked */
  TRM_ANT_SWITCH_DIV_SET_FAIL_IN_LOCK,
  /* failed: switch is in privilege mode */
  TRM_ANT_SWITCH_IN_PRIVILEGE_CONTROL,
  /* failed: swithcing not allowed for current mode */
  TRM_ANT_SWITCH_SET_FAIL_CURR_MODE,
  /* success: switching requested to LWT */
  TRM_ANT_SWITCH_DIV_SET_DONE_BY_LWT,

  /* success: PASSIVE SWITCHING done by tech in tx */
  TRM_ANT_SWITCH_SET_CFG_BY_TX_TECH

}trm_set_ant_switch_return_type;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode and Config Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* set mode return type */
  trm_ant_switch_set_mode_return_type   set_mode_grant;

  /* set config return type */
  trm_set_ant_switch_return_type  set_config_grant;

  /* configuration to be set if feedback is set rf and continue */
  trm_ant_switch_div_config_type       config;

  /* Anonymous payload to be echoed through set mode grant callback */
  trm_request_tag_t                     tag;

} trm_asd_set_mode_and_config_type;

/*----------------------------------------------------------------------------
  Ant Switch Set Mode and Config Call Back Type.
----------------------------------------------------------------------------*/
typedef void (*trm_asd_set_mode_and_config_cb_t)
   (
   /* set mode call-back info */
   trm_asd_set_mode_and_config_type  set_mode_cfg_info
   );

/*----------------------------------------------------------------------------
  Ant Switch Set Mode and Config Input Type.
  This structure should be used in set mode and switch api
----------------------------------------------------------------------------*/
typedef struct
{
  /* client requesting the mode set*/
  trm_client_enum_t client;

  /* Mode to be set */
  trm_client_mode_type mode;

  /* configuration tech want to switch to */
  trm_ant_switch_div_config_type  config;

  /* set mode call-back info. This is called when set mode request is
     granted or denied. */
  trm_asd_set_mode_and_config_cb_t   set_mode_cb;

  /* ant switch call-back info. This is used when an explicit set swith
     has to be initiated or if feed-back has to be provided about any other
     set info*/
  trm_ant_switch_div_callback_t      ant_switch_cb;

  /* to subscribe to switch completion, below value should be set to TRUE
    TRM will call the callback function with actiong: SET_CONFIG_DONE to
    inform the client that switching is complete */
  boolean                            inform_switch_change;

  /* Deprecated.. NOT USED. */
  trm_ant_switch_subcribe_switch_info   subscribe_switch_info;

  /* Anonymous payload to be echoed through set mode grant callback */
  trm_request_tag_t                     tag;

} trm_set_mode_and_config_input_t;

/*----------------------------------------------------------------------------
  Ant Switch Input Data for Settng Configuration
----------------------------------------------------------------------------*/
typedef struct
{
  /* client setting the switch configuration */
  trm_client_enum_t              client;

  /* configuration to which the switch is being set */
  trm_ant_switch_div_config_type config;

} trm_ant_switch_set_input_type;


/*----------------------------------------------------------------------------
  Ant Switch Complete input type
----------------------------------------------------------------------------*/
typedef struct
{
  /* client completing the switch */
  trm_client_enum_t client;

  /* configuration to switch to */
  trm_ant_switch_div_config_type config;
} trm_switch_complete_input_type;


/*----------------------------------------------------------------------------
  Ant Switch State Enum Type
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_ANT_SWITCH_DIV_ENABLED,

  TRM_ANT_SWITCH_DIV_DISABLED,
} trm_ant_switch_state_type;


/*----------------------------------------------------------------------------
  Set Ant Switch Diversity State Input Struct Type
----------------------------------------------------------------------------*/
typedef struct
{
  trm_client_enum_t         client; /* client */
  trm_ant_switch_state_type state;  /* state */
} trm_set_asd_state_input_type;

/*----------------------------------------------------------------------------
  Ant Switch Mode Notification Info Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* client completing the mode set*/
  trm_client_enum_t client;

  /* Mode to which it transitioned */
  trm_client_mode_type mode;
} trm_ant_switch_mode_notification_info_type;

/*----------------------------------------------------------------------------
  Ant Switch Mode Notification call-back type.
----------------------------------------------------------------------------*/
typedef void (*trm_ant_switch_mode_notification_callback_t)
(
  /* set mode call-back info */
  trm_ant_switch_mode_notification_info_type set_mode_info
);

/*----------------------------------------------------------------------------
  Ant Switch Mode Notification Input Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* client requesting the mode set*/
  trm_client_enum_t client;

  /* indicates whether notification should be turned on or not */
  boolean should_notify;

  /* if notification is turned on, the call-back to call for notification */
  trm_ant_switch_mode_notification_callback_t mode_change_cb;
} trm_ant_switch_mode_notification_input_type;

/*----------------------------------------------------------------------------
  Ant Switch Set Control Type enum.
----------------------------------------------------------------------------*/
typedef enum{
  /* release the switch control */
  TRM_ANT_SWITCH_CTRL_RELEASE,
  /* request for switch control */
  TRM_ANT_SWITCH_CTRL_REQUEST,
  /* invalid request */
  TRM_ANT_SWITCH_CTRL_INVALID

}trm_ant_switch_set_ctrl_enum;

/*----------------------------------------------------------------------------
  Ant Switch Set Control Type.
----------------------------------------------------------------------------*/
typedef struct{
  /* client */
  trm_client_enum_t               client;
  /* control request/release */
  trm_ant_switch_set_ctrl_enum    req_type;
}trm_ant_switch_set_ctrl_type;

/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Result Type
----------------------------------------------------------------------------*/
typedef enum{

  /* SWITCH STATE UPDATE LOCK */
  TRM_ASDIV_SWITCH_STATE_UPDATE_FAILED,

  /* SWITCH STATE UPDATE UNLOCK */
  TRM_ASDIV_SWITCH_STATE_UPDATE_SUCCESS,

}trm_asdiv_switch_state_update_result_t;

/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Fail Reason Type
----------------------------------------------------------------------------*/
typedef enum{

  /* SWITCH STATE UPDATE LOCK */
  TRM_ASDIV_STATE_UPDATE_FAIL_NONE,

    /* SWITCH STATE UPDATE LOCK */
  TRM_ASDIV_STATE_UPDATE_FAIL_FEAT_NOT_SUPPORTED,

    /* SWITCH STATE UPDATE LOCK */
  TRM_ASDIV_STATE_UPDATE_FAIL_INVALID_INFO,

  /* SWITCH STATE UPDATE UNLOCK */
  TRM_ASDIV_STATE_UPDATE_FAIL_SWITCH_IN_PROGRESS,

}trm_asdiv_switch_state_update_fail_reason_t;

/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Return Info Type
----------------------------------------------------------------------------*/
typedef struct{

  /* SWITCH STATE UPDATE LOCK */
  trm_asdiv_switch_state_update_result_t  update_result;

  /* SWITCH STATE UPDATE UNLOCK */
  trm_asdiv_switch_state_update_fail_reason_t  fail_reason;

}trm_asdiv_switch_state_update_return_info_t;


/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Request Type
----------------------------------------------------------------------------*/
typedef enum{

  /* SWITCH STATE UPDATE LOCK */
  TRM_ASDIV_SWITCH_UNLOCK_REQUEST,

  /* SWITCH STATE UPDATE UNLOCK */
  TRM_ASDIV_SWITCH_LOCK_REQUEST,

  /* invalid request */
  TRM_ASDIV_UPDATE_REQUEST_INVALID

}trm_asdiv_switch_state_update_request_t;

/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Reason Type
----------------------------------------------------------------------------*/
typedef enum{

  /* QTA PROCESS */
  TRM_ASDIV_STATE_UPDATE_REASON_QTA,

  /* IRAT PROCESS */
  TRM_ASDIV_STATE_UPDATE_REASON_IRAT,

    /* RF INTERACTION */
  TRM_ASDIV_STATE_UPDATE_REASON_RF_INTERACT,

  /* OTHER CRITICAL PROCESS */
  TRM_ASDIV_STATE_UPDATE_REASON_CRIT_PROCESS,

  /* INVALID REASON */
  TRM_ASDIV_STATE_UPDATE_REASON_MAX

}trm_asdiv_switch_state_update_reason_t;

/*----------------------------------------------------------------------------
  TRM Asdiv Switch State Update Request Info
----------------------------------------------------------------------------*/
typedef struct{

  /* CLIENT ID */
  trm_client_enum_t   client;

  /* REQUEST TYPE */
  trm_asdiv_switch_state_update_request_t    request_type;

  /* REQUEST REASON */
  trm_asdiv_switch_state_update_reason_t     update_reason;

  /* SWITCH CONFIGURATION- for future purpose */
  trm_ant_switch_div_config_type             switch_config;

  /* If, TRUE- update can happen even if switch is in progress
               and update result is always returned SUCCESS. 
         FALSE- update cannot happen if switch is in progress
               and update result is returned as FAILED */
  boolean     update_if_switch_in_progress;

}trm_asdiv_switch_state_update_request_info_t;




/*----------------------------------------------------------------------------
  trm_get_device_mapping Input Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* client requesting band info */
  trm_client_enum_t     client;

  /* resource type being requested */
  trm_resource_enum_t   resource;

  /* Band information */
  trm_band_t            band;
} trm_get_device_mapping_input_type;


/*----------------------------------------------------------------------------
  trm_get_device_mapping_multiple Input Type.
----------------------------------------------------------------------------*/

typedef struct
{
  /* client requesting band info */
  trm_client_enum_t     client;

  /* resource type being requested */
  trm_resource_enum_t   resource;

  /* Band information */
  trm_band_t            band[TRM_DEVICE_MAPPING_INPUT_SIZE];

  /* Device type */
  rfm_device_enum_type  device[TRM_DEVICE_MAPPING_INPUT_SIZE];

} trm_get_multiple_band_dev_map_input_type;

/*----------------------------------------------------------------------------
  Trm Rfm Device Info Type.
----------------------------------------------------------------------------*/

typedef struct
{

  /* Device type */
  rfm_device_enum_type  device;

  /* capability bitmask */
  uint32                 capability;

} trm_rfm_device_info_type;

/*----------------------------------------------------------------------------
  trm Rfm Device Support Input Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* client id */
  trm_client_enum_t         client;

  /* band info */
  trm_band_t                band;

  /* number of devices supporting the band */
  uint8                     num_of_device;

  /* rfm device info supporting the band requested */
  trm_rfm_device_info_type  rfm_device[TRM_MAX_CHAINS];

}trm_rfm_device_support_input_type;


/*----------------------------------------------------------------------------
  Input type for reservation with information about redundancy.
----------------------------------------------------------------------------*/
typedef struct
{
  /* The client which needs the RF resource */
  trm_client_enum_t               client_id;

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource;

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when;

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration;

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason;

  /* Frequency Information */
  trm_frequency_type_t            freq_info;

  /* Redundancy Information */
  trm_redundant_res_at_enum_type  redundant_info;


} trm_freq_reserve_at_adv_input_type;

/*============================================================================

                            FUNCTION DECLARATIONS

============================================================================*/



/*============================================================================

FUNCTION TRM_GET_RF_CONFIG

DESCRIPTION
  Return the RF configuration
  
DEPENDENCIES
  trm_init( ) must have been called.

RETURN VALUE
  The RF configuration in use

SIDE EFFECTS
  None

============================================================================*/

trm_rf_enum_t trm_get_rf_config( void );




/*============================================================================

FUNCTION TRM_FREQ_RESERVE_AT

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason. This is different to
  old api since it takes band as an argument. This is used to check if the
  band is valid and also if components mapped to the band don't have any
  concurrency issues.

  This would be used with the "trm_request()" function, below.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_freq_reserve_at
(
  /* The client which needs the RF resource */
  trm_client_enum_t               client,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  trm_frequency_type_t            freq_info
);





/*============================================================================

FUNCTION TRM_FREQ_REQUEST_AND_NOTIFY

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for the
  given duration, for the supplied reason.

  When the resource can be granted to the client, the event callback for the
  client will be called with the result of the lock request.This is different to
  old api since it takes band as an argument. This is used to check if the
  band is valid and also if components mapped to the band don't have any
  concurrency issues.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_freq_request_and_notify
(
  /* The client which needs the transceiver resource */
  trm_client_enum_t               client,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_type         grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag,

  trm_frequency_type_t            freq_info
);


/*============================================================================

FUNCTION TRM_REQUEST_AND_NOTIFY_ENHANCED

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for a 
  minimum of the given duration, for the supplied reason. At the time of the
  grant, the duration is extended to the maximium possible time that the lock
  can be made available to the client without colliding with any other client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.
  Also the time value returned via a grant_enh_callback is subject to change
  depending on the nature and timing of other requests from other clients
  after this function call.

============================================================================*/
void trm_freq_request_and_notify_enhanced
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* Desired resource */
  trm_resource_enum_t             resource,

  /* Minimum duration that the client requests the resource for.(sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted and till when the
   * client can hold on to the resource. */
  trm_grant_enh_callback_type        grant_enh_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback,

  trm_frequency_type_t            freq_info

);



/*============================================================================

FUNCTION TRM_FREQ_REQUEST

DESCRIPTION
  Specifies the given client needs the given transciever resource, for the
  given duration, for the supplied reason.

  The resource request is immediately evaluated, and the result returned.

  This may be used in conjunction with trm_reserve_at(). This is different to
  old api since it takes band as an argument. This is used to check if the
  band is valid and also if components mapped to the band don't have any
  concurrency issues.
  
DEPENDENCIES
  None

RETURN VALUE
  TRM_DENIED          - the client was denied the transceiver resources.
  TRM_GRANTED_CHAIN0  - the client can now use the primary RF chain.
  TRM_GRANTED_CHAIN1  - the client can now use the secondary RF chain.

SIDE EFFECTS
  If the client currently holds an RF chain, that chain is released before
  the request is evaluated.

============================================================================*/

trm_chain_grant_return_type trm_freq_request
(
  /* The client which needs the transceiver resource */
  trm_client_enum_t               client,

  /* The transceiver resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

    trm_frequency_type_t            freq_info
);


/*============================================================================
FUNCTION TRM_REQUEST_FREQUENCY_TUNE

DESCRIPTION
  Request for a new band tune before retuning RF.
  
DEPENDENCIES
  NONE

RETURN VALUE
  TRM_BAND_REQUEST_GRANTED - Client can go through with the RF tune
  TRM_BAND_REQUEST_PENDING - Client needs to wait for the band grant cb

SIDE EFFECTS
  Sets the client's band class in TRM

============================================================================*/
trm_band_grant_return_type trm_request_frequency_tune
(
  trm_client_enum_t          client,      /* Client requesting a band change */
  trm_frequency_type_t       freq_info,         /* New band that the client wants to use */
  trm_band_grant_callback_type band_grant_cb,  /* Band Grant Callback */
  trm_request_tag_t tag
);


/*============================================================================

FUNCTION TRM_RESERVE_AT

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason.

  This would be used with the "trm_request()" function, below.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_reserve_at
(
  /* The client which needs the RF resource */
  trm_client_enum_t               client,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason
);



/*============================================================================

FUNCTION TRM_REQUEST_AND_NOTIFY

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for the
  given duration, for the supplied reason.

  When the resource can be granted to the client, the event callback for the
  client will be called with the result of the lock request.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_request_and_notify
(
  /* The client which needs the transceiver resource */
  trm_client_enum_t               client,

  /* The RF resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_t            grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag
);



/*============================================================================

FUNCTION TRM_REQUEST

DESCRIPTION
  Specifies the given client needs the given transciever resource, for the
  given duration, for the supplied reason.

  The resource request is immediately evaluated, and the result returned.

  This may be used in conjunction with trm_reserve_at().
  
DEPENDENCIES
  None

RETURN VALUE
  TRM_DENIED          - the client was denied the transceiver resources.
  TRM_GRANTED_CHAIN0  - the client can now use the primary RF chain.
  TRM_GRANTED_CHAIN1  - the client can now use the secondary RF chain.

SIDE EFFECTS
  If the client currently holds an RF chain, that chain is released before
  the request is evaluated.

============================================================================*/

trm_grant_event_enum_t trm_request
(
  /* The client which needs the transceiver resource */
  trm_client_enum_t               client,

  /* The transceiver resource which is being requested */
  trm_resource_enum_t             resource,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason
);



/*============================================================================

FUNCTION TRM_RELEASE

DESCRIPTION
  Release the transceiver resource currently held by a client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If no client is waiting for the resource, the RF chain will be turned off.

============================================================================*/

void trm_release
(
  /* The client which is releasing the resource */
  trm_client_enum_t               client
);



/*============================================================================

FUNCTION TRM_EXCHANGE

DESCRIPTION
  Swaps the resources held by two clients.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_exchange
(
  /* The first client */
  trm_client_enum_t               client_1,

  /* The second client */
  trm_client_enum_t               client_2
);



/*============================================================================

FUNCTION TRM_RETAIN_LOCK

DESCRIPTION
  Informs the Transceiver Resource Manager that the client wants to hold
  the resource indefinitely.  The TRM may inform the client that it must
  give up the lock through the supplied unlock callback.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_retain_lock
(
  /* The client which is attempting to extend the lock duration */
  trm_client_enum_t               client,

  /* The required sclk extension, from "now" */
  trm_unlock_callback_t           unlock_callback
);



/*============================================================================

FUNCTION TRM_EXTEND_DURATION

DESCRIPTION
  Attempts to extend the duration a transceiver resource lock is held for.
  If the entire extension can be granted, it will be granted.
  If the entire extension cannot be granted, the lock duration remains
  unchanged, and the client should release the lock at the original lock
  expiry point.

  If the client had originally locked the resource for longer than the
  required extension, the lock will remain the original length
  and the extension will be INDICATED AS BEING GRANTED.

  The extension is all or nothing.  If a partial extension is desired, the
  trm_change_duration( ) function should be used.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock

RETURN VALUE
  TRUE if the lock duration extends from "now" to "now + duration".
  FALSE if the lock duration is unchanged.

SIDE EFFECTS
  None

============================================================================*/

boolean trm_extend_duration
(
  /* The client which is attempting to extend the lock duration */
  trm_client_enum_t               client,

  /* The required sclk extension, from "now" */
  trm_duration_t                  duration
);



/*============================================================================

FUNCTION TRM_CHANGE_DURATION

DESCRIPTION
  Attempts to extend the duration a transceiver resource lock is held for.
  The lock will be extended as long as possible, but not greater than the
  given maximum.  If minimum is not TRM_DONT_SHORTEN, the lock duration
  may actually be shortened if a higher priority client is waiting for
  the resource.
  
DEPENDENCIES
  The client must be holding an transceiver resource lock

RETURN VALUE
  The new lock duration, from "now", in sclks.

SIDE EFFECTS
  None

============================================================================*/

/* Don't shorten the lock duration - only extend it, if possible */
#define TRM_DONT_SHORTEN          0xFFFFFFFFuL

trm_duration_t trm_change_duration
(
  /* The client which is attempting to alter the lock duration */
  trm_client_enum_t               client,

  /* The minimum time, from now, that is acceptable (or TRM_DONT_SHORTEN) */
  trm_duration_t                  minimum,

  /* The maximum duration, from now, the lock may be extended for. */
  trm_duration_t                  maximum
);



/*============================================================================

FUNCTION TRM_CHANGE_PRIORITY

DESCRIPTION
  When a client changes what it is doing, it should change the advertised
  reason for holding the transceiver resource, so its priority will change.

  Eg) A client request the transceiver resource for listening for a PAGE.  If
  it receives one, it would change its priority to PAGE_RESPONSE and attempt
  to respond to the page, and eventually change its priority to TRAFFIC.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_change_priority
(
  /* The client whose priority is to be changed */
  trm_client_enum_t               client,

  /* The new reason why the RF lock is held (used for priority decisions) */
  trm_reason_enum_t               reason
);



/*============================================================================

FUNCTION TRM_GET_GRANTED

DESCRIPTION
  Get the RF chain which the client holds a lock on.
  
DEPENDENCIES
  None
  
RETURN VALUE
  TRM_GRANTED_CHAIN0
  TRM_GRANTED_CHAIN1
  TRM_DENIED

SIDE EFFECTS
  None

============================================================================*/

trm_grant_event_enum_t trm_get_granted
(
  /* The client who's RF chain ownership is desired */
  trm_client_enum_t               client
);


/*============================================================================

FUNCTION TRM_GET_RF_DEVICE

DESCRIPTION
  Get the RF device identifier for the given client
  
DEPENDENCIES
  None
  
RETURN VALUE
  RFM_DEVICE_0 - Owns primary RF chain
  RFM_DEVICE_1 - Owns secondary RF chain
  
  TRM_NO_DEVICE       - No RF chain owned

SIDE EFFECTS
  None

============================================================================*/
rfm_device_enum_type trm_get_rf_device
(
  /* The client who's RF chain ownership is desired */
  trm_client_enum_t               client
);


/*============================================================================

FUNCTION TRM_ENABLE_MODE

DESCRIPTION
  Enable the given TRM mode if possible.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_enable_mode
( 
  trm_mode_enum_t mode
);



/*============================================================================

FUNCTION TRM_DISABLE_MODE

DESCRIPTION
  Disable the given TRM mode if possible.
  
DEPENDENCIES
  None
  
RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_disable_mode
( 
  trm_mode_enum_t mode
);

/*============================================================================

FUNCTION TRM_GET_1X_MODE

DESCRIPTION 
  Get the current 1x mode.  This is defined by the NV_ITEM
  
DEPENDENCIES
  None
  
RETURN VALUE
  The 1X mode

SIDE EFFECTS
  None

============================================================================*/

trm_1x_mode_enum_t  trm_get_1x_mode( void );

/*============================================================================

FUNCTION TRM_REQUEST_AND_NOTIFY_ENHANCED

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for a 
  minimum of the given duration, for the supplied reason. At the time of the
  grant, the duration is extended to the maximium possible time that the lock
  can be made available to the client without colliding with any other client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.
  Also the time value returned via a grant_enh_callback is subject to change
  depending on the nature and timing of other requests from other clients
  after this function call.

============================================================================*/

void trm_request_and_notify_enhanced
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* Desired resource */
  trm_resource_enum_t             resource,

  /* Minimum duration that the client requests the resource for.(sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted and till when the
   * client can hold on to the resource. */
  trm_grant_enh_callback_t        grant_enh_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback

);

/*============================================================================

FUNCTION TRM_GET_REASON

DESCRIPTION
  This function returns the reason from the internal TRM structure table which
  will be the last reason with which the client requested lock. If the client 
  is not holding the chain nor has made any request/reservation, then 
  TRM_NUM_REASONS will be returned.  
  
DEPENDENCIES
  None

RETURN VALUE
  trm_reason_enum_t

SIDE EFFECTS
  None

============================================================================*/

trm_reason_enum_t trm_get_reason
(
  trm_client_enum_t       client_id
);

/*============================================================================

FUNCTION TRM_COMPARE_PRIORITIES

DESCRIPTION
  This function compares the priority of the (client, reason1) and 
  (client, reason2) pairs.
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE if prio(client, reason1) > prio(client, reason2)
  FALSE otherwise

SIDE EFFECTS
  This function does not check if the client is holding any lock before 
  comparing the priorities.

============================================================================*/

boolean trm_compare_priorities 
(
  /* Cleint Id */
  trm_client_enum_t       client,

  /* First reason to compare */
  trm_reason_enum_t       reason1,

  /* Second reason to compare */
  trm_reason_enum_t       reason2
);

/*============================================================================

FUNCTION TRM_GET_PRIORITY_INV_ENABLED

DESCRIPTION
  This function returns the boolean value indicating the status of priority 
  inversion for the reason passed as an argument. 
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE if priority inversion is set for the reason.
  FALSE otherwise

SIDE EFFECTS
  This function does not check if the client is holding any lock before 
  comparing the priorities.

============================================================================*/

boolean trm_get_priority_inv_enabled 
(
  /* Cleint Id */
  trm_client_enum_t       client,

  /* Reason for which the inversion status to be checked */
  trm_reason_enum_t       reason
);

/*============================================================================

FUNCTION TRM_GET_EXTENSION_FLAG

DESCRIPTION
  This function returns a boolean value indicating the status of the specified
  extension flag bit for the given TRM reason code
  
DEPENDENCIES
  None

RETURN VALUE
  TRUE if the extension flag is enabled for the specified reason.
  FALSE otherwise

SIDE EFFECTS
  None whatsoever.

============================================================================*/

boolean trm_get_extension_flag 
(
  /* Cleint Id */
  trm_client_enum_t       client,

  /* Reason for which the inversion status to be checked */
  trm_reason_enum_t       reason,

  /* extension flag mask to be checked */
  uint8                   ext_flag_mask
);

/*============================================================================

FUNCTION TRM_SET_EXTENSION_FLAG

DESCRIPTION
  This function sets the specified extension flag for the given reason
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_set_extension_flag 
(
  /* Client Id */
  trm_client_enum_t       client,

  /* Reason for which the extension flag is to be set */
  trm_reason_enum_t       reason,

  /* extension flag mask to be updated */
  uint8                   ext_flag_mask,

  /* value to be set against the extension flags */
  boolean                 status
);

/*============================================================================

FUNCTION TRM_GET_EXTENSION_MASK

DESCRIPTION
  This function returns full mask information set against a given TRM reason 
  code.
  
DEPENDENCIES
  None

RETURN VALUE
  Full mask information set against a given reason code.

SIDE EFFECTS
  None

============================================================================*/
uint8 trm_get_extension_mask
(
  /* Client Id */
  trm_client_enum_t       client,

  /* Reason whose complete mask value needs to be read */
  trm_reason_enum_t       reason
);

/*============================================================================

FUNCTION TRM_SET_EXTENSION_MASK

DESCRIPTION
  This function sets full mask information against a given TRM reason code
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_extension_mask
(
  /* Client Id */
  trm_client_enum_t       client,

  /* Reason for which the mask needs to be set */
  trm_reason_enum_t       reason,
  
  /* Mask having combined set of flags */
  uint8                   mask
);

/*============================================================================

FUNCTION TRM_SET_PRIORITY_INV_ENABLED

DESCRIPTION
  This function sets the passed boolean value against the reason.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  This function does not check if the client is holding any lock before 
  comparing the priorities.

============================================================================*/

void trm_set_priority_inv_enabled 
(
  /* Cleint Id */
  trm_client_enum_t       client,

  /* Reason for which the inversion status to be set */
  trm_reason_enum_t       reason,

  /* Boolean value to be set against the reason */
  boolean status
);

/*============================================================================
 

FUNCTION TRM_GET_HW_CAP

DESCRIPTION
  This API will return the HW capability based on information provided
  by RF at boot up.

DEPENDENCIES
  trm_init() should have been called by tmc prior to using this api
 

RETURN VALUE
  None
 
SIDE EFFECTS
  None

============================================================================*/
void trm_get_hw_cap( trm_hw_cap_info* info );

/*============================================================================
 

FUNCTION TRM_GET_SIMULT_CAP
 TRM get simultaneous capability

DESCRIPTION
  This api will return a 8 bit value where
  Bit 0 = 1 indicates SHDR is enabled.  
  Bit 1 = 1 indicates SVDO is enabled.
  Bit 2 = 1 indicates SVLTE is enabled 
  Bit 3 = 1 is reserved.
  Bit 4 = 1 indicates DSDA is enabled 
  Bit 5 = 1 indicates SGLTE and SGTDS are enabled 

  If SVDO bit(bit 1) is set, then the SHDR bit(bit 0) should be treated as
  �don�t care�. Following macros can be used to identify the bits :
   TRM_SHDR_IS_ENABLED        (0x01)
   TRM_SVDO_IS_ENABLED        (0x02)
   TRM_SVLTE_IS_ENABLED       (0x04)
   TRM_SVLTE_IS_DSDA          (0x10)
   TRM_SGLTE_SGTDS_IS_ENABLED (0x20)
   
  Note: Value 0x8 is reserved.
 

DEPENDENCIES
  trm_init() should have been called by tmc prior to using this api
 

RETURN VALUE
 
SIDE EFFECTS
 

============================================================================*/
uint8 trm_get_simult_cap( void );


/*============================================================================
CALLBACK TRM_BAND_GRANT_CALLBACK_T

DESCRIPTION
  The prototype for band callback functions, used by the Transceiver manager
  to inform the clients of transceiver management events.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
typedef void (* trm_band_grant_callback_t)
(
  trm_client_enum_t client,    /* Client requested for the band change */
  trm_band_type band,          /* Band class */
  trm_band_request_grant_type granted,   /* Status of band grant */
  trm_request_tag_t tag
);

/*============================================================================
FUNCTION TRM_REQUEST_BAND_TUNE

DESCRIPTION
  Request for a new band tune before retuning RF.
  
DEPENDENCIES
  NONE

RETURN VALUE
  TRM_BAND_REQUEST_GRANTED - Client can go through with the RF tune
  TRM_BAND_REQUEST_PENDING - Client needs to wait for the band grant cb

SIDE EFFECTS
  Sets the client's band class in TRM

============================================================================*/
trm_band_request_grant_type trm_request_band_tune
(
  trm_client_enum_t client,      /* Client requesting a band change */
  trm_band_type newBand,         /* New band that the client wants to use */
  trm_band_grant_callback_t  band_grant_cb,  /* Band Grant Callback */
  trm_request_tag_t tag
);

/*============================================================================
FUNCTION TRM_CONFIRM_BAND_TUNE_COMPLETE

DESCRIPTION
  Confirm that a RF tune to a new band is complete.
  
DEPENDENCIES
  NONE

RETURN VALUE
  NONE

SIDE EFFECTS
  Sets the client's band class in TRM

============================================================================*/
void trm_confirm_band_tune_complete
( 
   trm_client_enum_t   client /* Client confirming it's band tune is complete*/
);

/*============================================================================
FUNCTION TRM_CANCEL_BAND_TUNE

DESCRIPTION
  Cancel a previously requested band tune.
  
DEPENDENCIES
  NONE

RETURN VALUE
  TRM_BAND_REQUEST_DENIED - Cancellation is denied.
  TRM_BAND_REQUEST_GRANTED - Cancellation is granted.

SIDE EFFECTS
  Sets the client's band class in TRM

============================================================================*/
trm_band_request_grant_type trm_cancel_band_tune 
(
   trm_client_enum_t     client      /* Client cancelling the request */
);


/*============================================================================

FUNCTION TRM_TASKINIT

DESCRIPTION
  Initializes the data required for task start-up. Please note that no
  NV initialization should happen in task init.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_taskinit (void);


/*============================================================================

FUNCTION TRM_TASK

DESCRIPTION
  Task Start-up function where all the NV initialization amongst others is 
  done.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_task ( dword dummy );

/*============================================================================

CALLBACK TRM_CHANGE_CALLBACK_T

DESCRIPTION
  The prototype for change callback functions, used by the Transceiver
  Resource Manager to inform the clients when it's change request is
  satisfied.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_change_callback_t)
(
  /* The client which is being informed of an event */
  trm_client_enum_t               client,

  /* The new changed reason */
  trm_reason_enum_t               reason,

  /* The result being sent to the client */
  trm_change_result_enum_t        result,

  /* Anonymous payload echoed from trm_change_reason() */
  trm_request_tag_t               tag
);

/*============================================================================

FUNCTION TRM_CHANGE_REASON

DESCRIPTION
  When a client changes what it is doing, it should change the advertised
  reason for holding the RF resource, so its priority will change.

  Eg) A client request the RF resource for listening for a PAGE.  If it
  receives one, it would change its priority to PAGE_RESPONSE and attempt
  to respond to the page, and eventually to TRAFFIC.

  There is a possibility that the reason cannot be changed for cases such as
  the new reason is incompatible with the other chain owner. In this case,
  the change reason request is denied. 

  In case of a change reason request being denied, the client still has the 
  lock on the chain but with its original reason.
  
DEPENDENCIES
  The client must be holding an RF resource lock

RETURN VALUE
  TRM_CHANGE_GRANTED - if the reason is changed
  TRM_CHANGE_DENIED - if the reason could not be changed 

SIDE EFFECTS
  None

============================================================================*/
trm_change_result_enum_t trm_change_reason
(
  /* The client whose priority is to be changed */
  trm_client_enum_t               client_id,

  /* The new resource why the RF lock is held (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback for informing the client in case this request is kept waiting */
  trm_change_callback_t           change_cb,

  /* User defined arg to pass onto change callback */
  trm_request_tag_t               tag
);

/*============================================================================

FUNCTION TRM_SET_CLIENT_PRIORITY

DESCRIPTION
  Sets the client priority.
  Client priority is used in case of multiple subscriptions.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  The client priority will be changed after this function call. Any subsequent
  trm requests and reservations will be made with the new priority.  

============================================================================*/

void trm_set_client_priority
(

  /* Structure variable specifying client_id, priority of the client */
  trm_client_priority_type        id_prio

);  /* trm_set_client_priority */

/*============================================================================

FUNCTION TRM_GET_CLIENT_PRIORITY

DESCRIPTION
  Gets the client priority.
  Client priority is used in case of multiple subscriptions.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_client_priority_enum_t

SIDE EFFECTS
 None.  

============================================================================*/

trm_client_priority_enum_t trm_get_client_priority
(

  /* client_id */
  trm_client_enum_t                client_id

);  /* trm_get_client_priority */

/*============================================================================

FUNCTION TRM_GET_CLIENT_ASID_MAPPING

DESCRIPTION
  Gets the GSM client ASID mapping.
  TRM_GSM1 will always map to Multi-mode subscription
  
DEPENDENCIES
  None

RETURN VALUE
  ASID corresponding to a client, currently this is being handled only for GSM client

SIDE EFFECTS
 None.  

============================================================================*/

sys_modem_as_id_e_type trm_get_client_asid_mapping
(
  /* client_id */
  trm_client_enum_t                client_id
);

/*============================================================================

FUNCTION TRM_GET_CHAIN_HOLDER

DESCRIPTION
  Returns the TRM client currently holding the chain
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  This function does not check if the client is holding any lock before 
  comparing the priorities.

============================================================================*/

trm_client_enum_t trm_get_chain_holder
(
  trm_grant_event_enum_t chain,
  trm_reason_enum_t*     reason
);

/*============================================================================

CALLBACK trm_unlock_callback_advanced_t

DESCRIPTION
  The prototype for unlock event callback functions, used by the Transceiver
  Resource Manager to inform the clients of when it should unlock a resource
  it holds.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

typedef void (*trm_unlock_callback_advanced_t)
(
  trm_unlock_cb_advanced_data_t* unlock_data
);

/*============================================================================

FUNCTION TRM_RETAIN_LOCK_ADVANCED

DESCRIPTION
  Informs the Transceiver Resource Manager that the client wants to hold
  the resource indefinitely.  The TRM may inform the client that it must
  give up the lock through the supplied unlock callback.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_retain_lock_advanced
(
  /* The client which is attempting to extend the lock duration */
  trm_client_enum_t               client_id,

  /* Unlock callback function which is called to request client to 
     unlock the chain it is holding. */
  trm_unlock_callback_advanced_t        unlock_callback
);

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SET_CONFIG

DESCRIPTION
  Sets the tx diversity switch configuration to given configuration.
  
DEPENDENCIES
  None

RETURN VALUE
  Set Config Success or Error.

SIDE EFFECTS
  None

============================================================================*/
trm_set_ant_switch_return_type trm_ant_switch_div_set_config(
   trm_ant_switch_set_input_type newsettings /* new settings info */
);


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_CONFIG

DESCRIPTION
  Get the current tx diversity switch configuration.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity switch configuration or error due to switching.

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_config_type trm_ant_switch_div_get_config(
  trm_ant_switch_get_input_type input_type /* get config input type */
);


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_TEST_MODE_CTRL

DESCRIPTION
  Get the current tx diversity test mode control.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity test mode control

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_test_mode_ctrl_type trm_ant_switch_div_get_test_mode_ctrl(void);

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_GET_DWELLING_TIME

DESCRIPTION
  Get the current tx diversity dwelling time.
  
DEPENDENCIES
  None

RETURN VALUE
  Current tx diversity dwelling time.

SIDE EFFECTS
  None

============================================================================*/
trm_ant_switch_div_dwelling_time_type trm_get_ant_switch_div_dwelling_time(void);


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SET_MODE

DESCRIPTION
  L1s update the mode which they are entering: SLEEP, RX or TX. Depending on
  the mode, set mode logic could initiate call-back to other techs.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_set_mode(
  trm_set_mode_input_type set_mode_data /* Set mode Input type */
);

/*============================================================================

FUNCTION TRM_ASD_SET_MODE_AND_CONFIG

DESCRIPTION
  L1s use this function to register modes and request switching permission
  This should be used for idle wakeup/sleep modes only
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asd_set_mode_and_config(
   const trm_set_mode_and_config_input_t  *input_data
   );


/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SWITCH_COMPLETE

DESCRIPTION
  L1s update trm about switch complete once they have done the switch following
  set_config or set_mode.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_switch_complete(
  trm_switch_complete_input_type switch_input /* switch complete input type */
);

/*============================================================================

FUNCTION TRM_ANT_SWITCH_DIV_SWITCH_CANCEL

DESCRIPTION
  L1s update trm about switch incomplete if for any reason the switching
  couldnt go through.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_div_switch_cancel
(
   trm_switch_complete_input_type req_data 
);

/*============================================================================

FUNCTION TRM_GET_CLIENT_ANT_SWITCH_MODE

DESCRIPTION
  Returns the mode of the requested client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
trm_client_mode_type trm_get_client_ant_switch_mode(
  trm_client_enum_t clientid
);

/*============================================================================

FUNCTION TRM_SET_ANT_SWITCH_DIV_STATE

DESCRIPTION
  Updates the ant switch diversity state(enable or disable) for a given client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_ant_switch_div_state(trm_set_asd_state_input_type input);


/*============================================================================

FUNCTION TRM_SET_ANT_SWITCH_DIV_STATE

DESCRIPTION
  Updates the ant switch diversity state(enable or disable) for a given client.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_ant_switch_mode_change_notify(
  trm_ant_switch_mode_notification_input_type input
);



/*============================================================================

FUNCTION TRM_ANT_SWITCH_IS_SWITCHING_ALLOWED

DESCRIPTION
  Returns whether switching is allowed or not.
  
DEPENDENCIES
  None

RETURN VALUE
  boolean

SIDE EFFECTS
  None

============================================================================*/
boolean trm_ant_switch_is_switching_allowed(void);

/*============================================================================

FUNCTION TRM_GET_DEVICE_MAPPING

DESCRIPTION
  Specifies the device that supports the given band.
  
RETURN VALUE
  device that supports the given band
  RFM_MAX_DEVICES in case no valid device

SIDE EFFECTS
  None

============================================================================*/
rfm_device_enum_type trm_get_device_mapping(
   trm_get_device_mapping_input_type input
);

/*============================================================================

FUNCTION TRM_GET_DEVICE_MAPPING_MULTIPLE

DESCRIPTION
  Specifies the device that supports the given band

RETURN VALUE
  Device that supports the given band will be filled for num_of_bands
  (up to TRM_DEVICE_MAPPING_INPUT_SIZE). RFM_MAX_DEVICES in case no valid device.

SIDE EFFECTS
  device field in info struct will be modified.

============================================================================*/
void trm_get_device_mapping_multiple(
   /* Number of entries to determine mapping */
   uint32 num_of_bands,

   /* Input information for the API */
   trm_get_multiple_band_dev_map_input_type *info
);


/*============================================================================

FUNCTION TRM_ANT_SWITCH_SET_CONTROL

DESCRIPTION
  Updates the ant switch diversity switch control to privilege mode,
  it is called when privileged clients request/release the switch
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_ant_switch_set_control
( 
   trm_ant_switch_set_ctrl_type 	ctrl_req 
);

/*============================================================================

FUNCTION TRM_SET_DRX_CYCLE

DESCRIPTION
  This function sets the new DRx Cycle for the client and resets the PBR
  session.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_drx_cycle(trm_drx_cycle_input_type inputinfo);

/*============================================================================

FUNCTION TRM_SET_PBR_MODES

DESCRIPTION
  This function sets the PBR modes for which the client wants PBR resolution.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_set_pbr_modes(trm_pbr_modes_input_type inputinfo);


#ifdef TEST_FRAMEWORK
#error code not present
#endif

/*============================================================================
FUNCTION TRM_UPDATE_TECH_STATE

DESCRIPTION
  Techs (GSM/WCDMA/TDSCDMA) will call this API during Single SIM scenario 
  to update the state. This will NOT be used by TRM for scheduling or arbitrating
  RF devices or any kind of chain management.

DEPENDENCIES
  Device should be in Single SIM mode so chain should not be held by any other tech

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/

void trm_update_tech_state
(
  /* The client which needs the RF resource */
  trm_client_enum_t               client,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Band which is currently used by tech */
  trm_band_t                      band
);

/*============================================================================
FUNCTION TRM_REGISTER_LMTSMGR_TECH_STATE

DESCRIPTION
  This function registers Limitsmgr callback to TRM for getting tech's state info

DEPENDENCIES
  TRM should be intialized

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/

void trm_register_lmtsmgr_client
(
  /* The Limitsmgr callback */
  trm_tech_state_update_callback_type   callback
);

/*============================================================================

FUNCTION TRM_GET_DEVICE_SUPPORT_INFO

DESCRIPTION
  Checks the RFM devices that support the band provided

RETURN VALUE
  List of rfm devices that support the band provided and the capability of
  each device.

SIDE EFFECTS
  following fields in the passed structure gets modified
    num_of_devices- indicates how many device support the band
    rfm_device- represents the list of device with capability

============================================================================*/
void trm_get_device_support_info(

  trm_rfm_device_support_input_type *info
);

/*============================================================================
FUNCTION TRM_REGISTER_ASYNC_EVENT_CB

DESCRIPTION
  This function registers an asynchronous event callback function with TRM.
  TRM will call the callback function when a particular even occurs.

DEPENDENCIES
  TRM should be intialized

RETURN VALUE
  TRUE when successful.
  FALSE when failed.

SIDE EFFECTS
  None
============================================================================*/

boolean trm_register_async_event_cb
(
  trm_register_async_event_cb_data_type* input
);

/*----------------------------------------------------------------------------
  Enum to indicate the QTA feature state
----------------------------------------------------------------------------*/
typedef enum
{
   /* QTA gap start */
  TRM_QTA_STATE_START,

   /* QTA gap end */
  TRM_QTA_STATE_END,

   /* QTA state invalid */
  TRM_QTA_STATE_MAX

}trm_qta_state_enum_t;

/*----------------------------------------------------------------------------
  Enum to indicate the APS feature state
----------------------------------------------------------------------------*/
typedef enum
{
  /* Default, APS not expected/supported*/
  TRM_APS_STATE_NONE,

  /* APS expected
     DATA tech specific state to indicate it expects page skipping when
     allowed */
  TRM_APS_STATE_SKIP_EXPECTED,

   /* APS supported
      VOICE tech specific state to indicate it can allow page skipping */
  TRM_APS_STATE_SKIP_SUPPORTED,

   /* APS state invalid */
  TRM_APS_STATE_MAX

}trm_aps_state_enum_t;

/*----------------------------------------------------------------------------
  Data type indicating APS state update
----------------------------------------------------------------------------*/
typedef struct
{
   /* new APS state  */
  trm_aps_state_enum_t  new_state;

}trm_aps_state_update_data_t;

/*----------------------------------------------------------------------------
  Data type indicating QTA state update
----------------------------------------------------------------------------*/
typedef struct
{
   /* new QTA gap state update- START or END */
  trm_qta_state_enum_t  new_state;

  /* idle tech id that data tech is tuning away to
     eg: in L->G QTA, this is G id */
  trm_client_enum_t     idle_tech_id;

}trm_qta_state_update_data_t;

/*----------------------------------------------------------------------------
  Data type to be passed to update event info api
----------------------------------------------------------------------------*/
typedef struct
{
   /* TRM client id of requesting tech
      eg: in case of L->G qta, this is LTE id */
  trm_client_enum_t   client;

  /* Event related to the update
     for QTA: event is TRM_ASYNC_EVENT_QTA_STATE_UPDATE
     for APS: event is TRM_ASYNC_EVENT_APS_STATE_UPDATE */
  trm_async_event_enum_type   event;

  /* Data specific to the event update */
  union
  {
    /* data structure for QTA state update */
    trm_qta_state_update_data_t  qta_data;

    /* data structure for APS state update */
    trm_aps_state_update_data_t  aps_data;

  }data;

}trm_update_event_info_t;

/*----------------------------------------------------------------------------
  Enum to represent the status of update info request
----------------------------------------------------------------------------*/
typedef enum
{
   /* Update request state- SUCCESS */
  TRM_UPDATE_EVENT_INFO_SUCCESS,

   /* update request state- FAILED */
  TRM_UPDATE_EVENT_INFO_FAILED

}trm_update_event_info_result_enum_t;

/*----------------------------------------------------------------------------
  Enum to represent the reason of update info request failure
----------------------------------------------------------------------------*/
typedef enum
{
   /* no failure */
  TRM_UPDATE_FAIL_REASON_NONE,

   /* update was unexpected */
  TRM_UPDATE_FAIL_UPDATE_UNEXPECTED

}trm_update_event_info_fail_reason_enum_t;

/*----------------------------------------------------------------------------
  Data type returned by TRM from update event info api call
----------------------------------------------------------------------------*/
typedef struct
{
   /* Indicates state of the update request - successful or not*/
 trm_update_event_info_result_enum_t        update_result;

   /* Reason of failure if the update failed. In success scenario,
      it should be FAIL_REASON_NONE*/
 trm_update_event_info_fail_reason_enum_t   fail_reason;

}trm_update_event_info_ret_t;

/*============================================================================
FUNCTION TRM ASDIV REQUEST SWITCH_STATE UPDATE 

DESCRIPTION
  This function allows tech to update the switch state to LOCK or UNLOCK.
 
  If updated to LOCK state, TRM will deny any request for switching irrespective
  of priority, till the tech updates it to UNLOCK state.
  Reason used for locking should be used to unlock as well.

DEPENDENCIES
  Switch denials if this api is called to lock the switch

RETURN VALUE
  None

SIDE EFFECTS
  None
============================================================================*/
trm_asdiv_switch_state_update_return_info_t trm_asdiv_request_switch_state_update
(
   trm_asdiv_switch_state_update_request_info_t*  update_info
);

/*----------------------------------------------------------------------------
  trm hopping type.
----------------------------------------------------------------------------*/
typedef enum
{
  /* TRM idle mode hopping type */
  TRM_HOPPING_TYPE_IDLE_MODE,

  /* TRM connected mode hopping type */
  TRM_HOPPING_TYPE_CONNECTED_MODE,

  /* TRM all hopping types */
  TRM_HOPPING_TYPE_ALL

}trm_hopping_type;

/*----------------------------------------------------------------------------
  trm update hop behavior input Type.
----------------------------------------------------------------------------*/
typedef struct
{
  /* flag to disable/enable hopping */
  boolean   disable_hop;

  /* specifies which type of hopping to disable */
  trm_hopping_type  hop_type;

}trm_modify_hop_behavior_input_type;

/*============================================================================

FUNCTION TRM_UPDATE_HOPPING_BEHAVIOR

DESCRIPTION
  This will enable/disable the hopping behavior
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
boolean trm_modify_hopping_behavior
( 
   const trm_modify_hop_behavior_input_type*  input 
);

/*============================================================================
FUNCTION TRM_UPDATE_EVENT_INFO

DESCRIPTION
  This API will be used by L1s to update the info related to any events,
  eg: In case of QTA, L1s can call this api to update
      the QTA gap start and end 

DEPENDENCIES
  TRM should be intialized. Techs should have registered for the event/feature
  in the beginning

RETURN VALUE
  Indicates if the update was successful. Also the reason for update failure
  if any
 
SIDE EFFECTS
  None
============================================================================*/
trm_update_event_info_ret_t trm_update_event_info(trm_update_event_info_t* event_info);


/*----------------------------------------------------------------------------
  Data type to be passed to register reasons for adaptive pageskipping
----------------------------------------------------------------------------*/
typedef struct
{

   /* bitmask containing the reasons to be registered for Adaptive
      Page skipping
      Bit should be set if corresponding reason has to be registered for APS */
  uint64    reason_mask;

}trm_adaptive_page_skip_reg_info;

/*----------------------------------------------------------------------------
  Data type to be passed to register events at the beginning
----------------------------------------------------------------------------*/
typedef struct
{
  /* Bitmask of the events the client is registering for. Masked using
     the enum trm_async_event_enum_type */
  uint32                        events;

  /* data associated with the event in events bitmask */
  union
  {
    trm_adaptive_page_skip_reg_info   aps_info;
    };

} trm_event_info_t;

/*----------------------------------------------------------------------------
  Data type to be passed to register events at the beginning
----------------------------------------------------------------------------*/
typedef struct
{
    /* Client Id */
  trm_client_enum_t               client;

  /* Function pointer of the function to be called to nofify the
     client of the event,
     it can be null for tech if it is only registering events for which
     callback is not needed */
  trm_async_event_callback_type   callback_ptr;

  /* Array of all the events and data related to the events,
     techs can register callback and provide data using a single call
     by populating all the events and data in this array */
  trm_event_info_t                event_info[TRM_MAX_EVENT_REG_NUM];

  /* Number of valid events in the event_info array */
  uint32                          num_of_events;


}trm_async_event_info_registration_t;


/*============================================================================
FUNCTION TRM_REGISTER_ASYNC_EVENT_INFO

DESCRIPTION
  This API will be used by L1s to register async callbacks and provide
  data specific to the event, Techs can register multiple events in a
  single call and each event can be associated with a separate event
  specific data.
  eg: In case of Adaptive Page Skipping, L1s can call this api to register the
      reasons associated with Page Skipping feature. In this case, callback
      is not really used. If tech is just registering for APS and no other
      event registration is needed callback can be NULL as well.

DEPENDENCIES
  TRM should be intialized.

RETURN VALUE
  TRUE- If all event registration was successful
  FALSE- if event registration failed due to unexpected information
 
SIDE EFFECTS
  None
============================================================================*/
boolean trm_register_async_event_info
(
   trm_async_event_info_registration_t* reg_info
);


/*============================================================================

FUNCTION TRM_ASDIV_SET_PREF_CONFIG_TEST

DESCRIPTION
  OFFTARGET USE ONLY API:
      to set the preferred config of a client
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_set_pref_config_test
(
   trm_client_enum_t                cl, 
   trm_ant_switch_div_config_type   cfg
);


/*============================================================================

FUNCTION TRM_ASDIV_SET_SUB_PRIO_DATA_TEST

DESCRIPTION
  OFFTARGET USE ONLY API:
      is used to set the sub priority info.. 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_asdiv_set_sub_prio_data_test
(
   uint8    state_sub0,
   uint8    state_sub1
);


/*============================================================================

FUNCTION TRM_FREQ_RESERVE_AT_ADV

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason. It also indicates if
  the reservation is redundant or not.

  This would be used with the "trm_request()" function, below.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_freq_reserve_at_advanced
(
  trm_freq_reserve_at_adv_input_type *res_at_info
);


/*============================================================================

FUNCTION TRM_GET_DRX_CYCLE

DESCRIPTION
  returns the current DRx cycle for the querried tec
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
uint32 trm_get_drx_cycle
(
  trm_client_enum_t		client_id
);

/*============================================================================

FUNCTION TRM_CLEAR_PBR_PRIORITY_INFO

DESCRIPTION
  clears the PBR priority flags. 
  
DEPENDENCIES
  should be used only by GSM when it is in data call listening to its own 
  page.

RETURN VALUE
  None

SIDE EFFECTS
  Clears the PBR priorities and does not go through scheduler.

============================================================================*/
void trm_clear_pbr_priority_info(trm_client_enum_t client_id);

#endif /* TRM_H */

