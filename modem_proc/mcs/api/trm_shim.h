#ifndef TRM_SHIM_H
#define TRM_SHIM_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

                 Transceiver Resource Manager Shim Header File

GENERAL DESCRIPTION

  This file provides API for the GSM clients to use TRM for accessing RF
  resources in a Dual SIM scenario.


EXTERNALIZED FUNCTIONS

  trm_shim_set_client_priority() - Notifies TRM about the client priority. 
    Must be called each time the client priority is changed.

  trm_shim_get_client_priority() - Retrives the shim client priority.

  trm_shim_reserve_at() - Registers a client's need to access the RF at a 
    specific moment in the future for the indicated duration, such as for a
    paging slot.  trm_shim_request() must be used at the specified moment, to
    actually obtain the RF resource.  Implicitly performs a trm_release(). 
    It looks up the client priority and internally calls trm_reserve_at()
    with appropriate reason as argument.

  trm_shim_request() - Request access to an RF chain immediately, for the 
    indicated duration. Implicitly performs a trm_release(). It looks up 
    the client priority and internally calls trm_request() with appropriate 
    reason as argument. 

  trm_shim_request_and_notify() - Request access to an RF chain, when it 
    becomes available.  Implicitly performs a trm_release(). It looks up 
    the client priority and internally calls trm_request_and_notify() with 
    appropriate reason as argument.

  trm_shim_change_priority() - Specifies a change in the reason (and 
    consequently priority) for a client's hold on an RF resource. It looks 
    up the client priority and internally calls trm_change_priority() with 
    appropriate reason as argument. 

  trm_shim_retain_lock() - Informs TRM that the client wants to hold the 
    resource indefinitely. TRM may inform the client that it must give up 
    the lock through the supplied unlock callback. It looks up the client 
    priority and internally calls trm_retain_lock() with appropriate reason 
    as argument.

  trm_shim_release() - Releases the clients hold on an RF resource.  If no
    client is waiting for the RF resource, the RF will be turned off. It 
    will call corresponding trm_release().

  trm_shim_request_and_notify_enhanced() - Requests and holds on to the
    resource until the next resource collision. TRM will call the grant
    callback when it can satisfy the client request. It will also provide a 
    time value that will indicate the amount of time the client can hold the
    resource without a collision.
    
  trm_shim_get_reason() - Returns nominal last reason with which the client 
    requested lock. If the client is not holding the chain nor has made any 
    request/reservation, then TRM_NUM_REASONS will be returned. 

REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  trm_init() must be called before any other function.

  Before using any RF functions, a client must be granted permission via
  trm_shim_request( ) or trm_shim_request_and_notify().

  When the client is finished using the RF, it must release its hold on the
  RF chain either directly via a call to trm_shim_release(), or indirectly by
  calling trm_shim_reserve_at(), trm_shim_request_and_notify(), or 
  trm_shim_request().


  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=



==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/api/trm_shim.h#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ---------------------------------------------------------
10/03/2012   mn      NikeL DSDS merge
05/15/2012   hh      Adding trm_shim_get_reason function
12/21/2010   hh      Added new api to get shim client priority.
05/14/2009   ag      Initial version

============================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "trm.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/
/*----------------------------------------------------------------------------
  TRM SHIM Client Priority
----------------------------------------------------------------------------*/
typedef enum
{
  /* Low Priority */
  TRM_PRIO_LO,

  /* High Priority */
  TRM_PRIO_HI,

  /* Undefined Priority */
  TRM_PRIO_UNDEFINED
}
trm_shim_client_priority_enum_t;


/*----------------------------------------------------------------------------
  Structure to hold the current priority of the TRM SHIM clients.
----------------------------------------------------------------------------*/
typedef struct trm_shim_client_priority_struct
{
  /* Client Id */
  trm_client_enum_t                client_id;
  
  /* Priority of the client */
  trm_shim_client_priority_enum_t  client_prio;
}
trm_shim_client_priority_type;


/*============================================================================

                            FUNCTION DECLARATIONS

============================================================================*/

/*============================================================================

FUNCTION TRM_SHIM_SET_CLIENT_PRIORITY

DESCRIPTION
  Sets the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  The client priority will be changed after this function call. Any subsequent
  trm requests and reservations will be made with the new priority.  

============================================================================*/

void trm_shim_set_client_priority
(

  /* Structure variable specifying client_id, priority of the client */
  trm_shim_client_priority_type        id_prio

);  /* trm_shim_set_client_priority */

/*============================================================================

FUNCTION TRM_SHIM_GET_CLIENT_PRIORITY

DESCRIPTION
  Gets the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_shim_client_priority_enum_t

SIDE EFFECTS
 None.  

============================================================================*/

trm_shim_client_priority_enum_t trm_shim_get_client_priority
(

  /* client_id */
  trm_client_enum_t                client_id

);  /* trm_shim_get_client_priority */

/*============================================================================

FUNCTION TRM_SHIM_RESERVE_AT

DESCRIPTION
  Specifies the given client needs the given transceiver resource at the given
  time, for the given duration, for the supplied reason. This function calls
  the corresponding trm_reserve_at() after checking the client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_shim_reserve_at 
(

  /* The client which needs the RF resource */
  trm_client_enum_t               client_id,

  /* When the resource will be needed (sclks timestamp) */
  trm_time_t                      when,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason

);  /* trm_shim_reserve_at */


/*============================================================================

FUNCTION TRM_SHIM_REQUEST_AND_NOTIFY

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for the
  given duration, for the supplied reason.

  When the resource can be granted to the client, the event callback for the
  client will be called with the result of the lock request.

  This function implicitly calls trm_reserve_and_notify after identifying the
  client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.

============================================================================*/

void trm_shim_request_and_notify
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted */
  trm_grant_callback_t            grant_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag

);  /* trm_shim_request_and_notify */


/*============================================================================

FUNCTION TRM_SHIM_REQUEST

DESCRIPTION
  Specifies the given client needs the given transciever resource, for the
  given duration, for the supplied reason.

  The resource request is immediately evaluated, and the result returned.

  This function implicitly calls trm_request() after the client priority is
  correctly identified and the new reason is determined.

  This may be used in conjunction with trm_shim_reserve_at().
  
DEPENDENCIES
  None

RETURN VALUE
  TRM_DENIED          - the client was denied the transceiver resources.
  TRM_GRANTED_CHAIN0  - the client can now use the primary RF chain.
  
SIDE EFFECTS
  If the client currently holds an RF chain, that chain is released before
  the request is evaluated.

============================================================================*/

trm_grant_event_enum_t trm_shim_request
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* How long the resource will be needed for (in sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason

);  /* trm_shim_request */

/*============================================================================

FUNCTION TRM_SHIM_CHANGE_PRIORITY

DESCRIPTION
  When a client changes what it is doing, it should change the advertised
  reason for holding the transceiver resource, so its priority will change.

  Eg) A client request the transceiver resource for listening for a PAGE. If
  it receives one, it would change its priority to PAGE_RESPONSE and attempt
  to respond to the page, and eventually change its priority to TRAFFIC.

  This function implicitly calls trm_change_priority() after evaluating the
  new reason by looking at client priority.
  
DEPENDENCIES
  The client must be holding a transceiver resource lock.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_shim_change_priority
(

  /* The client whose priority is to be changed */
  trm_client_enum_t               client_id,

  /* The new reason why the RF lock is held (used for priority decisions) */
  trm_reason_enum_t               reason

);  /* trm_shim_change_priority */

/*============================================================================

FUNCTION TRM_SHIM_RETAIN_LOCK

DESCRIPTION
  Informs the Transceiver Resource Manager that the client wants to hold
  the resource indefinitely.  The TRM may inform the client that it must
  give up the lock through the supplied unlock callback.
  
  This function implicitly calls trm_retain_lock() after evaluating the new
  reason by looking at client priority.

DEPENDENCIES
  The client must be holding a transceiver resource lock.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void trm_shim_retain_lock
(

  /* The client which is attempting to extend the lock duration */
  trm_client_enum_t               client_id,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback

);  /* trm_shim_retain_lock */


/*============================================================================

FUNCTION TRM_SHIM_RELEASE

DESCRIPTION
  Release the transceiver resource currently held by a client.
  Calls the corresponding trm_release().
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If no client is waiting for the resource, the RF chain will be turned off.

============================================================================*/

void trm_shim_release
(

  /* The client which is releasing the resource */
  trm_client_enum_t               client

);  /* trm_shim_release */

/*============================================================================

FUNCTION TRM_SHIM_REQUEST_AND_NOTIFY_ENHANCED

DESCRIPTION
  Specifies the given client needs the given transceiver resource, for a 
  minimum of the given duration, for the supplied reason. At the time of the
  grant, the duration is extended to the maximium possible time that the lock
  can be made available to the client without colliding with any other client.

  This function implicitly calls trm_request_and_notify_enhanced() after
  evaluating the new reason by looking at client priority.
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  If the client currently holds an RF chain, the chain is released.
  Also the time value returned via a grant_enh_callback is subject to change
  depending on the nature and timing of other requests from other clients
  after this function call.

============================================================================*/

void trm_shim_request_and_notify_enhanced
(

  /* The client which needs the transceiver resource */
  trm_client_enum_t               client_id,

  /* Minimum duration that the client requests the resource for.(sclks) */
  trm_duration_t                  duration,

  /* Why the resource is needed (used for priority decisions) */
  trm_reason_enum_t               reason,

  /* Callback to notify client when resource is granted and till when the
   * client can hold on to the resource. */
  trm_grant_enh_callback_t        grant_enh_callback,

  /* Anonymous payload to be echoed through grant_callback() */
  trm_request_tag_t               tag,

  /* Callback to notify client to release the resource */
  trm_unlock_callback_t           unlock_callback

);  /* trm_shim_request_and_notify_enhanced */

/*============================================================================

FUNCTION TRM_SHIM_GET_REASON

DESCRIPTION
  This function returns the reason from the internal TRM structure table which
  will be the last reason with which the client requested lock. If the client 
  is not holding the chain nor has made any request/reservation, then 
  TRM_NUM_REASONS will be returned.  
  
DEPENDENCIES
  None

RETURN VALUE
  trm_reason_enum_t

SIDE EFFECTS
  None.

============================================================================*/

trm_reason_enum_t trm_shim_get_reason
(
  trm_client_enum_t       client_id
); /* trm_shim_get_reason */

#endif  /* TRM_SHIM_H */
