/*!
  @file
  wci2_core.c

  @brief
  Service for sending WCI-2 messages over UART

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/src/wci2_core.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
02/26/14   btl     Add type 7
10/15/13   ckk     Moving to WCI2
06/27/13   btl     Initial version

==========================================================================*/

/*=============================================================================

                      INCLUDE FILES

=============================================================================*/
#include "wci2_uart.h"
#include <comdef.h>
#include "wci2_core.h"
#include "wci2_utils.h"
#include "wci2_trace.h"

#include <qmi_mb_xport.h>

#include "coex_interface.h" /* handle type 6 recv */
#include "wci2_hdlc.h"

/*=============================================================================

                      DEFINES, CONSTANTS, GLOBALS

=============================================================================*/
#define WCI2_TYPE2_BITS_TO_SHIFT_DATA 4
#define WCI2_TYPE2_LO_NIBBLE_MASK 0x02
#define WCI2_TYPE2_HI_NIBBLE_MASK 0x0a

/* set data 5th bit in wci2 type 6 message if this is a TX subframe */
#define WCI2_TYPE6_SF_TX_MASK 0x80

#define COEX_QMI_SVC_ID 0x22
/* TODO: add statistics, counters, errors */

/* client registration table. use client enum to index into table */
#define WCI2_TYPE2_MAX_NUM_CLIENTS 2
typedef struct
{
  boolean                 is_initialized;
  wci2_type2_recv_cb_type callback;
  void*                   cb_param;
  uint8                   address_map;
} wci2_type2_client_reg_entry_s;

typedef struct
{
  uint8  num_clients;
  wci2_type2_client_reg_entry_s entry[WCI2_TYPE2_MAX_NUM_CLIENTS];
} wci2_type2_client_reg_table_s;

static wci2_type2_client_reg_table_s type2_client_reg_table;


/* qmb transport layer data structures & callbacks */
static void* wci2_qmb_open_cb( void*, void*, uint32_t, uint32_t, uint32_t, uint32_t );
static qmi_mb_error_type wci2_qmb_reg_cb( void* );
static qmi_mb_error_type wci2_qmb_send_cb( void*, void*, void*, uint32_t );
static void wci2_qmb_close_cb( void* );
static uint32_t wci2_qmb_addr_len_cb( void );
static void wci2_recv_qmb_msg_cb( uint8, void*, uint16, void* );

static qmi_mb_xport_ops_type xport_ops = 
{
  wci2_qmb_open_cb,     /* open callback from QMB infrastructure */
  wci2_qmb_reg_cb,      /* register callback */
  wci2_qmb_reg_cb,      /* unregister cb */
  wci2_qmb_send_cb,     /* send cb */
  wci2_qmb_close_cb,    /* close callback */
  wci2_qmb_addr_len_cb  /* return address length */
};

static void* wci2_qmb_xport_handle;

/*=============================================================================

                      INTERNAL FUNCTION DEFINITIONS

=============================================================================*/

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_open_fn_type
=============================================================================*/
/*!
@brief
  This callback function is called by the QMB infrastructure to open a new
  transport

@param[in]   xport_data          Opaque parameter to the xport (e.g. port ID)
@param[in]   xport               Pointer to infrastructure's transport struct.
                                 (Must be used for up calls)
@param[in]   service_id          Service ID of the bus
@param[in]   version             Version of the service
@param[in]   subgroup            Subgroup of the bus. Usually 0.
@param[in]   max_rx_len          Maximum length of messages that can be
                                 received. Used by xport to allocate a buffer
                                 if the underlying transport cannot pass the
                                 message through a callback.

@retval      Opaque handle to the transport. NULL on failure.

*/
/*=========================================================================*/
static void* wci2_qmb_open_cb
(
  void *xport_data,
  void *xport,
  uint32_t service_id,
  uint32_t version,
  uint32_t subgroup,
  uint32_t max_rx_len
)
{
  /* confirm client has permission to connect. For now only the COEX
   * service_id is valid. Once confirmed, pass a valid handle */
  if( COEX_QMI_SVC_ID == service_id )
  {
    /* internally, treat them as a regular WCI-2 client and register QMB 
     * automatically */
    wci2_type2_client_create( WCI2_COEX_QMB_CLIENT, wci2_recv_qmb_msg_cb, 0 );
    wci2_type2_register_for_msg( WCI2_COEX_QMB_CLIENT, 
                                 WCI2_TYPE2_ADDRESS_QMB_COEX, TRUE );

    /* save qmb infrastructure data. for now ignore everything else */
    wci2_qmb_xport_handle = xport;

    return (void*)WCI2_COEX_QMB_CLIENT;
  }
  else
  {
    /* invalid client/srvc_id */
    return NULL;
  }
}

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_reg_fn_type
=============================================================================*/
/*!
@brief
  This callback function is called by the QMB infrastructure to register
  or unregister with a message bus.

@param[in]   handle              Opaque handle returned by the open call

@retval      QMI_MB_NO_ERR       Success

*/
/*=========================================================================*/
static qmi_mb_error_type wci2_qmb_reg_cb
(
  void *handle
)
{
  /* called when QMB client (e.g. COEX) joins or leaves. */
  /* TODO: can hook into this to do an NPA vote to power on/off the UART? */
  return QMI_MB_NO_ERR;
}

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_send_fn_type
=============================================================================*/
/*!
@brief
  This callback function is called by the QMB infrastructure to send data to
  an end-point.

@param[in]   handle              Opaque handle returned by the open call
@param[in]   addr                Opaque address sent to the infrastructure
                                 through the join or recv calls. If NULL,
                                 broadcast to (sub)group.
@param[in]   msg                 Pointer to message to be sent
@param[in]   msg_len             Length of the message

@retval      QMI_MB_NO_ERR       Success

*/
/*=========================================================================*/
static qmi_mb_error_type wci2_qmb_send_cb
(
  void *handle,
  void *addr,
  void *msg,
  uint32_t msg_len
)
{
  errno_enum_type retval;

  /* QMB has already encoded the message, now we just need to 
   * put it in frame format and send over WCI2 as a type 2 */
  if( WCI2_COEX_QMB_CLIENT == (wci2_client_e)handle )
  {
    retval = wci2_type2_frame_and_send_msg( msg, (uint16) msg_len,
                                            WCI2_TYPE2_ADDRESS_QMB_COEX );
    WCI2_ASSERT( E_SUCCESS == retval );
  }

  return QMI_MB_NO_ERR;
}

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_close_fn_type
=============================================================================*/
/*!
@brief
  This callback function is called by the QMB infrastructure to close the
  transport usually when the user releases the handle. It is crucial that the
  xport synchronize the deallocation of memory and its callback functions
  before calling qmi_mb_xport_closed() to free up the rest of the data
  associated with the service.

@param[in]   handle              Opaque handle returned by the open call

*/
/*=========================================================================*/
static void wci2_qmb_close_cb
(
  void *handle
)
{
  /* this is the last thing called by the framework when 
   * either the whether the xport or client disconnect. */

  if( WCI2_COEX_QMB_CLIENT == (wci2_client_e)handle )
  {
    /* deregister the wci2 client */
    wci2_type2_client_delete( WCI2_COEX_QMB_CLIENT );
  }

  /* tell the QMB infrastructure that this transport has been fully closed,
   * and they can deallocate all info relating to us */
  qmi_mb_xport_closed( wci2_qmb_xport_handle );
}

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_addr_len_fn_type
=============================================================================*/
/*!
@brief
  This callback function is called by the QMB infrastructure to retrieve the
  length of the (source) address of the xport.

@retval         Length of address

*/
/*=========================================================================*/
static uint32_t wci2_qmb_addr_len_cb( void )
{
  /* we don't support addressing from qmb clients at this time */
  return 0;
}

/*=============================================================================

  CALLBACK FUNCTION: wci2_recv_qmb_msg_cb

=============================================================================*/
/*!
    @brief
    "client" callback with WCI2 - register QMB to receive a wci2 type2 frame

    @return
    void
*/
/*===========================================================================*/
static void wci2_recv_qmb_msg_cb
(
  uint8 address, 
  void* msg, 
  uint16 size, 
  void* param 
)
{
  qmi_mb_error_type retval;

  /* call QMB's receive function to signal the infrastructure to process this
   * incoming message */
  retval = qmi_mb_xport_recv( wci2_qmb_xport_handle, NULL, msg, (uint32_t)size );
  WCI2_ASSERT( QMI_MB_NO_ERR == retval );
}

/*=============================================================================

  FUNCTION:  wci2_type2_route_received_msg

=============================================================================*/
/*!
    @brief
    If a framed message was received over WCI-2 type 2, route the message
    to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_type2_route_received_msg( void* msg, uint8 address, uint16 size )
{
  uint8 i;
  uint8 address_mask;

  /*-----------------------------------------------------------------------*/

  /* make sure address is recognized */
  if( WCI2_TYPE2_ADDRESS_MAX <= address )
  {
    /* unrecognized. drop message */
    return;
  }

  /* calculate address mask, used with the client's address map to determine 
   * if the client registered for this address. */
  address_mask = (uint8) (1 << address);

  /* search client reg table pass message to anyone registered */
  for( i = 0; i < WCI2_MAX_CLIENT; i++ )
  {
    if( type2_client_reg_table.entry[i].is_initialized &&
        (type2_client_reg_table.entry[i].address_map & address_mask) )
    {
      /* match: client registered for address. call its callback */
      type2_client_reg_table.entry[i].callback(
              address, (void*) msg, size,
              type2_client_reg_table.entry[i].cb_param );
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  cxm_wci2_type2_nibblize

=============================================================================*/
/*!
    @brief
    Splits input byte stream into two nibbles per byte

    @return
    void
*/
/*===========================================================================*/
void cxm_wci2_type2_nibblize( const uint8 *in_buffer, uint16 size )
{
  uint8  out_buffer[WCI2_TYPE2_MAX_BUFFER_SIZE];
  uint16 out_size;
  uint16 in_pos, out_pos = 0;

  /*-----------------------------------------------------------------------*/
  /* determine the needed size (2x the input array size) */
  out_size = size << 1;

  /* verify inputs */
  WCI2_ASSERT( NULL != in_buffer && out_size <= WCI2_TYPE2_MAX_BUFFER_SIZE );

  /* split bytes into nibbles and pack in the wci-2 type 2 bits */
  for( in_pos = 0; in_pos < size; in_pos++ )
  {
    /* low nibble goes first */
    out_buffer[out_pos++] = (uint8) (((in_buffer[in_pos] & 0x0F) << 
                            WCI2_TYPE2_BITS_TO_SHIFT_LSN) | WCI2_TYPE2);
    /* next goes upper nibble */
    out_buffer[out_pos++] = (uint8) (((in_buffer[in_pos] & 0xF0) <<
                            WCI2_TYPE2_BITS_TO_SHIFT_MSN) | 
                            WCI2_TYPE2 | WCI2_TYPE2_HSN_BIT);
  }

  wci2_uart_send( out_buffer, out_size );

  return;
}

/*=============================================================================

  FUNCTION:  cxm_wci2_type2_denibblize

=============================================================================*/
/*!
    @brief
    Process one type-2 byte at a time. After two are received, if the 
    type-2 format is acceptible, will combine them into one byte and pass
    it on.

    @return
    void
*/
/*===========================================================================*/
inline void cxm_wci2_type2_denibblize( uint8 type2_byte )
{
  static uint8   whole_byte;
  static boolean new_byte = TRUE;

  /*-----------------------------------------------------------------------*/

  if( 0 == (type2_byte & WCI2_TYPE2_HSN_BIT) )
  {
    /* we've received a low nibble */
    if( new_byte )
    {
      /* we're expecting a new type-2 message, so everything looks good */
      whole_byte = (type2_byte & WCI2_TYPE2_DATA_MASK) >> WCI2_TYPE2_BITS_TO_SHIFT_LSN;
      new_byte = FALSE;
    }
    else
    {
      /* something's wrong... expected a least-significant nibble. Drop
       * nibble and reset */
      new_byte = TRUE;
    }
  }
  else
  {
    /* we've received a most-significant nibble */
    if( !new_byte )
    {
      /* we were expecting that, so finish building the byte and process it */
      whole_byte |= (type2_byte & WCI2_TYPE2_DATA_MASK) >> WCI2_TYPE2_BITS_TO_SHIFT_MSN;
      wci2_hdlc_buffer_and_unescape( whole_byte );
      new_byte = TRUE;
    }
  }
}

/*=============================================================================

                      EXTERNAL FUNCTION DEFINITIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  wci2_transport_init

=============================================================================*/
/*!
    @brief
    Initialize the WCI-2 service resources

    @return
    void
*/
/*===========================================================================*/
void wci2_transport_init( void )
{
  memset( &type2_client_reg_table, 0, sizeof(type2_client_reg_table) );

  /* register with QMB infrastructure */
  qmi_mb_xport_start( &xport_ops, 0 );
}

/*=============================================================================

  FUNCTION:  wci2_transport_deinit

=============================================================================*/
/*!
    @brief
    Release and clean up the WCI-2 service resources

    @return
    void
*/
/*===========================================================================*/
void wci2_transport_deinit( void )
{
  /* deregister with QMB infrastructure */
  qmi_mb_xport_stop( &xport_ops, 0 );
}

/*=============================================================================

  FUNCTION:  wci2_type2_client_create

=============================================================================*/
/*!
    @brief
    Create a client to receive WCI2 messages.
    Also need to call wci2_type2_register_for_msg() to receive any messages

    @return
    void
*/
/*===========================================================================*/
errno_enum_type wci2_type2_client_create(
  wci2_client_e           client,
  wci2_type2_recv_cb_type callback,
  void*                   cb_data
)
{
  /* make sure client is valid */
  WCI2_ASSERT( WCI2_TYPE2_MAX_NUM_CLIENTS > client &&
              NULL != callback );

  type2_client_reg_table.entry[client].is_initialized = TRUE;
  type2_client_reg_table.entry[client].callback = callback;
  type2_client_reg_table.entry[client].cb_param = cb_data;
  type2_client_reg_table.entry[client].address_map = 0;

  return E_SUCCESS;
}

/*=============================================================================

  FUNCTION:  wci2_type2_client_delete

=============================================================================*/
/*!
    @brief
    Delete a WCI2 client, so it can no longer receive messages.

    @return
    void
*/
/*===========================================================================*/
errno_enum_type wci2_type2_client_delete(
  wci2_client_e           client
)
{
  /* make sure client is valid */
  WCI2_ASSERT( WCI2_TYPE2_MAX_NUM_CLIENTS > client );

  type2_client_reg_table.entry[client].is_initialized = FALSE;

  return E_SUCCESS;
}


/*=============================================================================

  FUNCTION:  wci2_type2_register_for_msg

=============================================================================*/
/*!
    @brief
    Register or deregister for a client to receive WCI2 type2 messages 
    sent to a particular address

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type wci2_type2_register_for_msg( 
  wci2_client_e        client,
  wci2_type2_address_e address,
  boolean              want_message
)
{
  uint8 new_map;

  /* confirm parameters */
  if( WCI2_TYPE2_ADDRESS_MAX <= address || WCI2_MAX_CLIENT <= client )
  {
    return E_INVALID_ARG;
  }

  /* find in table and add/remove registration appropriately */
  if( type2_client_reg_table.entry[client].is_initialized )
  {
    /* use address enum as bit mask and save bit in mapping field */
    new_map = type2_client_reg_table.entry[client].address_map;
    if( want_message )
    {
      new_map |= (1 << address);
    }
    else
    {
      new_map &= ~(1 << address);
    }
    type2_client_reg_table.entry[client].address_map = new_map;
  }

  return E_SUCCESS;
}

/*=============================================================================

  FUNCTION:  wci2_type2_frame_and_send

=============================================================================*/
/*!
    @brief
    Frame the message into an hdlc-type frame and transmit over the UART in
    wci-2 type 2 format.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type wci2_type2_frame_and_send_msg(
  const void*          msg,
  uint16               msg_size,
  wci2_type2_address_e address
)
{
  uint8*            frame_ptr = NULL;
  uint16            frame_size = 0;
  wci2_msg_type_s wci2_msg;
  errno_enum_type   retval = E_SUCCESS;

  /*-----------------------------------------------------------------------*/

  /* validate client ID and translate to frame address */
  if( WCI2_TYPE2_ADDRESS_MAX <= address ||
      NULL == msg )
  {
    /* invalid args, return error */
    return E_INVALID_ARG;
  }

  frame_size = wci2_hdlc_frame_and_escape( msg, msg_size, address, 
                                           &frame_ptr, 0 );

  if( frame_size )
  {
    /* if frame_size is non-zero, we have something to send! */
    wci2_msg.type = WCI2_TYPE2;
    wci2_msg.data.type2_msg.length = frame_size;
    wci2_msg.data.type2_msg.buffer = frame_ptr;
    wci2_send_msg( &wci2_msg );

    WCI2_MEM_FREE( frame_ptr );
  }
  else
  {
    retval = E_FAILURE;
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  wci2_send_msg

=============================================================================*/
/*!
    @brief
    send a message over CXM_UART in WCI2 format

    @return
    void
*/
/*===========================================================================*/
void wci2_send_msg( const wci2_msg_type_s* msg )
{
  uint8 byte_to_send = 0x00;

  /*-----------------------------------------------------------------------*/

  WCI2_ASSERT( WCI2_TYPE0 <= msg->type && WCI2_TYPE7 >= msg->type );


  switch( msg->type )
  {
    /* prep the byte in WCI-2 format, then transmit the data over UART */
    case WCI2_TYPE1:
      byte_to_send = (uint8) ((WCI2_TYPE1_RESEND_REAL_TIME<<WCI2_BITS_TO_SHIFT_DATA) | WCI2_TYPE1);
      wci2_uart_send( &byte_to_send, 1 );
      wci2_counter_event( WCI2_CNT_UART_TX_WCI2_TYPE1, byte_to_send );
      break;

    case WCI2_TYPE2:
      #ifdef WCI2_FEATURE_ADV_UART_HW
      wci2_uart_send( msg->data.type2_msg.buffer, msg->data.type2_msg.length );
      #else
      cxm_wci2_type2_nibblize( msg->data.type2_msg.buffer, msg->data.type2_msg.length );
      #endif
      break;

    case WCI2_TYPE3:
      if( WCI2_TYPE3_MAX_INACT_DURN_MS <= msg->data.type3_inact_durn )
      {
        /* indicates WCI2 reserved infinite time of 0b11111 */
        byte_to_send = (uint8) (31<<WCI2_BITS_TO_SHIFT_DATA | WCI2_TYPE3);
      }
      else
      {
        byte_to_send = (uint8) (((msg->data.type3_inact_durn/5)<<WCI2_BITS_TO_SHIFT_DATA) | WCI2_TYPE3);
      }

      wci2_uart_send( &byte_to_send, 1 );
      wci2_counter_event( WCI2_CNT_UART_TX_WCI2_TYPE3, byte_to_send );
      break;

    case WCI2_TYPE6:
      byte_to_send = (uint8)(((msg->data.type6_adv_tx_sfn.sfn%10)<<WCI2_BITS_TO_SHIFT_DATA) | WCI2_TYPE6);
      if( msg->data.type6_adv_tx_sfn.tx )
      {
        byte_to_send |= WCI2_TYPE6_SF_TX_MASK;
      }
      wci2_uart_send_dir_char( byte_to_send );
      wci2_counter_event( WCI2_CNT_UART_TX_WCI2_TYPE6, byte_to_send );
      break;

    case WCI2_TYPE7:
      byte_to_send = (uint8)(msg->data.type7_wwan_tx_active);
      byte_to_send = (byte_to_send << WCI2_BITS_TO_SHIFT_DATA) | WCI2_TYPE7;
      wci2_uart_send_dir_char( byte_to_send );
      break;

    case WCI2_TYPE0:
    case WCI2_TYPE4:
    case WCI2_TYPE5:
      WCI2_MSG_1( ERROR, "UNSUPPORTED WCI2 message msg type = %x", msg->type );
      wci2_counter_event( WCI2_CNT_UART_TX_WCI2_TYPE_UNSUPPORTED, msg->type );
      break;

    default:
      WCI2_MSG_1( ERROR, "INVALID WCI2 message msg type = %x", msg->type );
      break;
  }

  WCI2_MSG_2( MED, "Sending WCI2 message (type=%d byte=%x)", 
              msg->type, byte_to_send );

  /*-----------------------------------------------------------------------*/

  return;
}

/*=============================================================================

  FUNCTION:  wci2_process_rx_data

=============================================================================*/
/*!
    @brief
    Strip the type and route the data to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_process_rx_data( uint8 byte )
{
  #ifndef WCI2_FEATURE_ADV_UART_HW
  uint8 type, data;
  #endif

  #ifdef WCI2_FEATURE_ADV_UART_HW
  wci2_hdlc_buffer_and_unescape( byte );
  #else
  type = byte & WCI2_TYPE_BITS_MASK;
  data = byte >> WCI2_BITS_TO_SHIFT_DATA;

  switch( (wci2_data_type_e) type )
  {
    case WCI2_TYPE1: /* type 1 message - handled in hardware */
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE1, byte );
      break;

    case WCI2_TYPE2:
      cxm_wci2_type2_denibblize( byte );
      break;

    case WCI2_TYPE6: /* type 6 message */
      coex_handle_recv_wci2_type6( data );
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE6, byte );
      break;

    default:
      WCI2_MSG_2( ERROR, "dropped type %d msg = (%x)", type, byte );
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE_UNSUPPORTED, byte );
      break;
  }
  #endif

  return;
}


/*=============================================================================

  FUNCTION:  wci2_process_rx_dir_read_data

=============================================================================*/
/*!
    @brief
    Strip the type and route the data to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_process_rx_dir_read_data( uint8 byte )
{

  #ifdef WCI2_FEATURE_ADV_UART_HW
  uint8 type, data;

  type = byte & WCI2_TYPE_BITS_MASK;
  data = byte >> WCI2_BITS_TO_SHIFT_DATA;

  switch( (wci2_data_type_e) type )
  {
    case WCI2_TYPE1: /* type 1 message - handled in hardware */
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE1, byte );
      break;

    case WCI2_TYPE6: /* type 6 message */
      coex_handle_recv_wci2_type6( data );
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE6, byte );
      break;

    default:
      WCI2_MSG_2( ERROR, "dropped type %d msg = (%x)", type, byte );
      wci2_counter_event( WCI2_CNT_UART_RX_WCI2_TYPE_UNSUPPORTED, byte );
      break;
  }
  #endif

}

