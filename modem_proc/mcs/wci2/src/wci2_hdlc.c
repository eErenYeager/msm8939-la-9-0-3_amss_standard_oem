/*!
  @file
  wci2_hdlc.c

  @brief
  Service for framing/unframing messages in HDLC-like format for sending
  over uart as a WCI-2 type 2 message

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/src/wci2_hdlc.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
07/15/13   btl     Initial version

==========================================================================*/

/*=============================================================================

                      INCLUDE FILES

=============================================================================*/
#include <comdef.h>
#include <crc.h>
#include "wci2_hdlc.h"
#include "wci2_core.h"
#include "wci2_utils.h"

/*=============================================================================

                      DEFINES, CONSTANTS, GLOBALS

=============================================================================*/
#define FRAME_START_FLAG      0x7e
#define FRAME_ESCAPE_FLAG     0x7d
#define FRAME_ESCAPE_XOR_MASK 0x20

#define SHOULD_ESCAPE_BYTE(byte) \
    ( FRAME_START_FLAG == (byte) || FRAME_ESCAPE_FLAG == (byte) )

/*=============================================================================

  FUNCTION:  wci2_hdlc_frame_and_escape

=============================================================================*/
/*!
    @brief
    Frame the given raw byte buffer using HDLC-like framing. To be used for
    transporting messages over UART.

    @detail
    * The maximum message size is defined by WCI2_TYPE2_MAX_MSG_SIZE
    Frame Structure:
      +----------+----------+----------+----------+----------+
      |   Flag   | Address  | Payload  |   CRC    |   Flag   |
      |   0x7e   |    *     |    *     | 16 bits  |   0x7e   |
      +----------+----------+----------+----------+----------+
       - only one flag sequence needed btw two frames
       - CRC transmitted least-significant octet first (highest term coefficients)
       - CRC does not include Flags or CRC fields. i.e. escape after CRC calculation
       - closing flag is used to locate CRC field
       - recommended that transmitters send a new open flag sequence when an 
         appreciable time has elapsed (1sec?)

    @return
    size of frame in bytes
*/
/*===========================================================================*/
uint16 wci2_hdlc_frame_and_escape ( 
  const void* msg,             /**< In - frame payload */
  uint16      msg_size,        /**< In - size of msg payload in bytes */
  uint8       address,         /**< In - address field of frame */
  uint8**     frame_buff,      /**< In/Out - address of buffer to write frame
                                             to. If NULL, will malloc */
  uint16      frame_buff_size  /**< In - size of frame buffer in bytes if 
                                         passed in */
)
{
  uint8  *in_buff, *out_buff;
  uint16 crc = (uint16) CRC_16_STEP_SEED;
  uint8  out_pos = 0;
  uint8  in_pos = 0;
  uint8  byte_to_write;

  /*-----------------------------------------------------------------------*/

  /* prepare output buffer to write to. If none was passed in, malloc one
   * with the max frame size. If one was passed in, use that. */
  if( NULL != *frame_buff )
  {
    out_buff = *frame_buff;
  }
  else
  {
    out_buff = WCI2_MEM_ALLOC( WCI2_TYPE2_MAX_FRAME_SIZE );
    *frame_buff = out_buff;
  }

  /* Validate inputs. To simplify, assume a max message size for now. */
  WCI2_ASSERT( msg_size <= WCI2_TYPE2_MAX_MSG_SIZE &&
              NULL != msg && 
              NULL != out_buff );

  /* treat msg as a byte stream */
  in_buff = (uint8*) msg;

  /* fill in frame header - start flag & address */
  out_buff[out_pos++] = FRAME_START_FLAG;

  /* compute the CRC (address + msg, before escaping) */
  crc = crc_16_step( crc, &address, 1 );
  crc = crc_16_step( crc, in_buff, msg_size );

  /* write (escaped) address */
  if( SHOULD_ESCAPE_BYTE( address ) )
  {
    out_buff[out_pos++] = FRAME_ESCAPE_FLAG;
    out_buff[out_pos++] = address ^ FRAME_ESCAPE_XOR_MASK;
  }
  else
  {
    out_buff[out_pos++] = address;
  }

  /* Copy over message from msg buffer passed in to new buffer. Also
   * escape any reserved bytes if encountered in msg buffer. */
  /* TODO: safer, more accurate bounds checks? */
  for( in_pos = 0; in_pos < msg_size; in_pos++ )
  {
    if( out_pos + 7 < WCI2_TYPE2_MAX_FRAME_SIZE )
    {
      /* test if character needs to be escaped */
      if( SHOULD_ESCAPE_BYTE( in_buff[in_pos] ) )
      {
        out_buff[out_pos++] = FRAME_ESCAPE_FLAG;
        out_buff[out_pos++] = in_buff[in_pos] ^ FRAME_ESCAPE_XOR_MASK;
      }
      else
      {
        out_buff[out_pos++] = in_buff[in_pos];
      }
    }
    else
    {
      /* out of space. should not happen if we sized eveything correctly. */
      return 0;
    }
  }

  /* write (escaped) LSB of CRC first */
  byte_to_write = (uint8) crc & 0x00ff;
  if( SHOULD_ESCAPE_BYTE( byte_to_write ) )
  {
    out_buff[out_pos++] = FRAME_ESCAPE_FLAG;
    out_buff[out_pos++] = byte_to_write ^ FRAME_ESCAPE_XOR_MASK;
  }
  else
  {
    out_buff[out_pos++] = byte_to_write;
  }

  /* now write (escaped) MSB of CRC */
  byte_to_write = (uint8) ((crc & 0xff00) >> 8);
  if( SHOULD_ESCAPE_BYTE( byte_to_write ) )
  {
    out_buff[out_pos++] = FRAME_ESCAPE_FLAG;
    out_buff[out_pos++] = byte_to_write ^ FRAME_ESCAPE_XOR_MASK;
  }
  else
  {
    out_buff[out_pos++] = byte_to_write;
  }

  /* append ending flag */
  out_buff[out_pos++] = FRAME_START_FLAG;

  WCI2_MSG_2( MED, "WCI2: Built frame, CRC=%d size=%d", crc, out_pos );

  return out_pos;
}

/*=============================================================================

  FUNCTION:  wci2_hdlc_unframe

=============================================================================*/
/*!
    @brief
    Take the WCI-2 HDLC-like frame and strip out all the frame info (while
    validating the frame), returning a buffer containing the original raw
    bytestream. If frame is invalid, will silently drop.

    @detail
    Start/Stop flags and escape characters should already be stripped out

    @return
    boolean frame_valid
*/
/*===========================================================================*/
boolean wci2_hdlc_unframe( 
  uint8* buffer, /**< Input - buffer containing framed msg */
  uint16 size    /**< Input - size of framed msg */
)
{
  uint16  crc_calcd = CRC_16_SEED;
  uint16  crc_recvd = 0;

  /*-----------------------------------------------------------------------*/

  /* validate frame size first. must be at least 3 bytes (address + CRC) */
  WCI2_ASSERT( NULL != buffer );
  if( size < 3 ) return FALSE;

  /* assume first byte is address and last 2 bytes are the CRC. 
   * The rest are the message. Loop through and make sure the frame
   * is not corrupted (check CRC-16) */
  crc_calcd = crc_16_calc( buffer, (size-2)<<3 );

  /* Compare calculated CRC with the transmitted one */
  crc_recvd = (buffer[size - 1] << 8) | (buffer[size - 2]);
  if( crc_recvd == crc_calcd )
  {
    /* frame error-free! process it */
    /* now that the frame is validated, strip off the frame overhead
     * and pass along the message */
    wci2_type2_route_received_msg( &buffer[1], buffer[0], size - 3 );
    WCI2_MSG_2( MED, "WCI2 type2 frame received: addr=%d size=%d", 
               buffer[0], size );
    return TRUE;
  }
  else
  {
    /* there was an error - CRC didn't check out */
    /* TODO: increment counters? */
    WCI2_MSG_1( ERROR, "WCI2 type2 frame RX CRC error, size=%d", size );
    return FALSE;
  }
}

/*=============================================================================

  FUNCTION:  wci2_hdlc_buffer_and_unescape

=============================================================================*/
/*!
    @brief
    Take one byte at a time and construct a complete frame out of it. When
    we have a complete frame, pass it along to be processed.

    @detail
    If any characters are reserved and need to be stripped out, this is
    the place to do it.

    @return
    void
*/
/*===========================================================================*/
void wci2_hdlc_buffer_and_unescape( uint8 byte )
{
  static uint8   buffer[WCI2_TYPE2_MAX_FRAME_SIZE];
  static uint8   index;
  static boolean frame_in_progress = FALSE;
  static boolean escaped_character = FALSE;

  /*-----------------------------------------------------------------------*/

  if( !frame_in_progress )
  {
    /* wait for a frame start, drop until then */
    if( FRAME_START_FLAG == byte )
    {
      /* starting a new frame! reset everything */
      index = 0;
      frame_in_progress = TRUE;
      escaped_character = FALSE;
    }
  }
  else
  {
    /* frame in progress - keep building the frame */

    if( FRAME_START_FLAG == byte )
    {
      /* end of this frame/start of the next! process what we have */
      wci2_hdlc_unframe( buffer, index );

      /* reset in case that was a start */
      index = 0;
      frame_in_progress = TRUE;
      escaped_character = FALSE;
    }
    else if( FRAME_ESCAPE_FLAG == byte )
    {
      /* character was escaped. un-escape it: */
      /* skip the escape char and un-mask the following one */
      escaped_character = TRUE;
    }
    else
    {
      if( index >= WCI2_TYPE2_MAX_FRAME_SIZE )
      {
        /* if buffer overflow imminent, reset */
        frame_in_progress = FALSE;
      }
      else if( escaped_character )
      {
        /* previous character was escape character. Need to uncode this one */
        buffer[index++] = byte ^ FRAME_ESCAPE_XOR_MASK;
        escaped_character = FALSE;
      }
      else
      {
        /* end not found yet and character normal; store byte in buffer */
        buffer[index++] = byte;
      }
    }
  }

  return;
}

