
/*!
  @file
  wci2_task.c

  @brief
  This file implements the thread/task level of the WCI-2 task.

  The main-loop of the task is wci2_task() .

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/src/wci2_task.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
           ckk     Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "mcs_variation.h"
#include <dog_hb_rex.h>
#include <rcinit.h>
#include <rcevt_rex.h>        /* RCEVT Signal Delivery by REX */

#include "wci2_utils.h"
#include "wci2_uart.h"
#include "wci2_trace.h"
#include "wci2_core.h"

/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/
#ifdef FEATURE_WDOG_DYNAMIC
STATIC dog_report_type wci2_wdog_id = 0; /*!< handle for WDOG reporting */
#endif /* FEATURE_WDOG_DYNAMIC */

/*=============================================================================

                                FUNCTIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  wci2_task_init

=============================================================================*/
/*!
    @brief
    Calls alls init functions needed for WCI2 task
 
    @return
    None
*/
/*===========================================================================*/
void wci2_task_init(void)
{
  /* Setup task stop signal */
  RCEVT_SIGEX_SIGREX sig;
  RCINIT_GROUP my_group;
  

  /*-----------------------------------------------------------------------*/

  /* Initialize WCI2's internal trace logging setup */
  wci2_trace_init();

  #ifdef FEATURE_WDOG_DYNAMIC
  /* Initialize CXM Watchdog (should be at the end of cxm_task_init()
      - Register using WDOG's Heartbeat APIs (no need of task timers) */
  wci2_wdog_id = dog_hb_register_rex( WCI2_WDOG_SIG );
  #endif /* FEATURE_WDOG_DYNAMIC */

  /* Setup the TASK STOP signal with rcinit... */
  sig.signal = rex_self();      
  sig.mask = WCI2_TASK_STOP_SIG; 
  my_group = rcinit_lookup_group_rextask(sig.signal);
  rcinit_register_term_group(my_group, RCEVT_SIGEX_TYPE_SIGREX, &sig);

  WCI2_TRACE( WCI2_TRC_TSK_INIT, 0, 0, 0 );
  /*-----------------------------------------------------------------------*/

  return;
} /* wci2_task_init */

/*=============================================================================

  FUNCTION:  wci2_task_deinit

=============================================================================*/
/*!
    @brief
    Tears down the WCI2 task

    @return
    None
*/
/*===========================================================================*/
STATIC void wci2_task_deinit(void)
{

  /*-----------------------------------------------------------------------*/

  #ifdef FEATURE_WDOG_DYNAMIC
  /* Deinitialize CXM Watchdog (should be at the beginning of cxm_task_deinit()
      - Deregister CXM from Watchdog's task monitoring */
  dog_hb_deregister( wci2_wdog_id );
  #endif /* FEATURE_WDOG_DYNAMIC */
  
  WCI2_TRACE( WCI2_TRC_TSK_DEINIT, 0, 0, 0 );

  /*-----------------------------------------------------------------------*/

  return;

} /* wci2_task_deinit */

/*=============================================================================

  FUNCTION:  wci2_clr_sigs

=============================================================================*/
/*!
    @brief
    Clears the required signals

    @return
    None
*/
/*===========================================================================*/
void wci2_clr_sigs ( wci2_signal_mask_t sigs )
{
  /* Make sure there's at least one signal needs clearing. */
  if(sigs)
  {
    /* Clear the signal, (REX locks interrupts while clearing.) */
    (void) rex_clr_sigs( &wci2_tcb, sigs );
  }
} /* wci2_clr_sigs */

/*=============================================================================

  FUNCTION:  wci2_task

=============================================================================*/
/*!
    @brief
    REX main function used for WCI2 task
*/
/*===========================================================================*/
void wci2_task
(
  dword dummy
)
{
  static wci2_signal_mask_t sig_mask_to_wait_for;
  wci2_signal_result_t      curr_sigs_set = 0;

  /*-----------------------------------------------------------------------*/

  (void)dummy;

  /* Initialize CXM task functionalities */
  wci2_task_init();

  /* Set REX signal mask to wait for */
  sig_mask_to_wait_for = WCI2_SIGS;

  /* Synchronize thread at startup with rcinit */
  rcinit_handshake_startup();

  WCI2_MSG_0( HIGH, "task active" );

  /* WCI2 task's main loop */
  while (1)
  {
    curr_sigs_set = rex_wait( sig_mask_to_wait_for );
    wci2_clr_sigs(curr_sigs_set & sig_mask_to_wait_for);

    if ( curr_sigs_set & WCI2_WDOG_SIG )
    {
      #ifdef FEATURE_WDOG_DYNAMIC
      /* Pet the dog */
      dog_hb_report( wci2_wdog_id );
      #endif /* FEATURE_WDOG_DYNAMIC */
    }

    /* Process received RX data signal */
    if ( curr_sigs_set & WCI2_UART_RX_SIG )
    {
      wci2_uart_process_rx_data();
    }

    if ( curr_sigs_set & WCI2_TASK_STOP_SIG )
    {
      /*  Deinitialize everything... */
      wci2_task_deinit();

      /* ACK rcinit that the task deinit is done.. */
      rcinit_handshake_term();
      
      /* Done with the loop */
      break; 
    }
  }

  /*-----------------------------------------------------------------------*/

  return;
}
