/*!
  @file
  wci2_uart.c

  @brief
  Implementation of WCI-2's UART interface APIs

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/
/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/src/wci2_uart.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/12/14   ckk     Address return status for UART powerdown request
02/12/14   btl     Mutex + states to prevent UART on/off race condition
11/22/13   btl     Save dsm_item_type* in RX FIFO
03/21/13   btl     Fix RX FIFO concurrency problems
01/29/13   btl     DCVS vote when UART active
11/05/12   ckk     Add WCI2's event trace logging
10/19/12   btl     Add additional F3 logging
09/20/12   btl     Added RX support
09/13/12   cab     Remove tx flush from init sequence
09/13/12   cab     Added power down and wakeup modes for power savings
           ckk     Initial implementation

===========================================================================*/


/*===========================================================================

                      INCLUDE FILES FOR MODULE

===========================================================================*/

/* Common */
#include <IxErrno.h>
#include <timer.h>
#include <dsm.h>
#include <sio.h>
#include <atomic_ops.h>
#include <DALSys.h>
#include <npa_resource.h>

#include <cxm_utils.h>
#include <cxm_diag.h>
#include <coex_interface.h>

#include "wci2_utils.h"
#include "wci2_core.h"
#include "wci2_uart.h"
#include "wci2_trace.h"

/*===========================================================================

                   INTERNAL TYPES

===========================================================================*/
#define WCI2_UART_FIFO_SIZE 64
#define WCI2_UART_DIR_READ_MSG_BUF_SIZE 8
#define WCI2_UART_TYPE0_INTR_BIT 0x01
#define WCI2_UART_TYPE1_INTR_BIT 0x02
#define WCI2_UART_TYPE2_INTR_BIT 0x04
#define WCI2_UART_TYPE3_INTR_BIT 0x08
#define WCI2_UART_TYPE4_INTR_BIT 0x10
#define WCI2_UART_TYPE5_INTR_BIT 0x20
#define WCI2_UART_TYPE6_INTR_BIT 0x40
#define WCI2_UART_TYPE7_INTR_BIT 0x80

typedef struct
{
  atomic_word_t   put_index; /* next slot to put data */
  atomic_word_t   get_index; /* next data to take */
  dsm_item_type*  array[WCI2_UART_FIFO_SIZE];
  uint32          overflow_count;
}wci2_uart_fifo_type_s;

typedef enum
{
  WCI2_UART_OFF,
  WCI2_UART_FLUSHING,
  WCI2_UART_ON
} wci2_uart_state_type;

typedef struct {
  wci2_uart_state_type previous_wci2_uart_state;
  uint32 count_consecutive_flush_powerdown_reqs;
  uint32 count_all_flush_powerdown_reqs;
} wci2_uart_monitors_type;

typedef struct
{
  /* enable bits for type0-7 on direct read */
  uint32 intr_enable;

  /* status bits for type0-7 on direct read */
  uint32 intr_status;

  /* incoming direct read message buffer, one byte per type0-7 */
  uint8 msg_buf[WCI2_UART_DIR_READ_MSG_BUF_SIZE];
} wci2_uart_dir_read_type;

static wci2_uart_state_type wci2_uart_state = WCI2_UART_OFF;
static wci2_uart_monitors_type wci2_uart_monitors;

static DALSYSSyncObj cxm_uart_sync_obj;
static DALSYSSyncHandle cxm_uart_sync_handle;
#define INIT_WCI2_UART_MUTEX() \
  if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, \
                     &cxm_uart_sync_handle, \
                     &cxm_uart_sync_obj)) \
  { \
    ERR_FATAL("cxm_uart_sync_obj create failed!", 0, 0, 0); \
  } 
#define ACQUIRE_WCI2_UART_MUTEX() DALSYS_SyncEnter(cxm_uart_sync_handle)
#define RELEASE_WCI2_UART_MUTEX() DALSYS_SyncLeave(cxm_uart_sync_handle)
#define WCI2_UART_CONSEC_FLUSH_POWERDOWN_REQS_LIMIT 5

/*===========================================================================

                    INTERNAL FUNCTION DECLARATIONS

===========================================================================*/
STATIC void wci2_uart_sio_process_rx_packet( dsm_item_type ** wci2_rx_dsm_ptr );


/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/
#ifdef FEATURE_DSM_WM_CB
   STATIC void wci2_uart_lowater_cb(struct dsm_watermark_type_s *wm_ptr, void *v_ptr);
   STATIC void wci2_uart_hiwater_cb(struct dsm_watermark_type_s *wm_ptr, void *v_ptr);
#else /* FEATURE_DSM_WM_CB */
   STATIC void wci2_uart_lowater_cb(void);
   STATIC void wci2_uart_hiwater_cb(void);
#endif /* FEATURE_DSM_WM_CB */
STATIC dsm_watermark_type wci2_uart_wmq;
STATIC q_type wci2_uart_echo_q;
STATIC volatile boolean wci2_tx_uart_high;
STATIC boolean wci2_uart_initialized = FALSE;
#ifdef WCI2_FEATURE_ADV_UART_HW
STATIC wci2_uart_dir_read_type wci2_uart_dir_read = 
  { WCI2_UART_TYPE1_INTR_BIT | WCI2_UART_TYPE6_INTR_BIT };
#endif

STATIC sio_open_type wci2_sio_open_struct = {
  SIO_NO_STREAM_ID,    /**< Stream ID Type.  Filled in
                            by SIO for internal use.    */
  SIO_STREAM_RXTX,     /**< Type of stream being opened.
                            Filled in by SIO for 
                            internal use only.          */
  SIO_DS_PKT_MODE,     /**< Stream Mode Type.           */
  NULL,                /**< Received Data Queue.         */
  &wci2_uart_wmq,       /**< Transmit Data Queue.         */
  SIO_BITRATE_3000000, /**< Bit-rate for reception.      */
  SIO_BITRATE_3000000, /**< Bit-rate for transmission.   */
  SIO_PORT_UART_CXM,  /**< Port which needs to be used. */
  FALSE,               /**< True, if tail character is used*/
  '\0',                /**< If tail character is used,
                            this is the tail character. */
  wci2_uart_sio_process_rx_packet, /**< If non-NULL, this function 
                                       will be called for each. 
                            packet which is received.   */
  SIO_FCTL_OFF, /**< TX flow control method.      */
  SIO_FCTL_OFF, /**< RX flow control method.      */
};

STATIC wci2_uart_fifo_type_s wci2_uart_rx_fifo;

/* NPA node data */
STATIC npa_resource_state wci2_uart_npa_driver ( npa_resource *resource,
                                                npa_client *client,
                                                npa_resource_state state );

STATIC npa_resource_definition wci2_uart_resource[] =
{
  {"/modem/mcs/cxm_uart",     /**< Resource Name */
    "",                       /**< Resource Units - Informational only */
    1,                        /**< Resource Max  */
    &npa_binary_plugin,       /**< Aggregate with binary style */
    NPA_RESOURCE_DEFAULT,     /**< Default resource behavior */
    NULL}                     /**< User Data */
};

STATIC npa_node_definition wci2_uart_node = 
{ 
  "/node/modem/mcs/cxm_uart",       /**< Node name        */
  wci2_uart_npa_driver,              /**< Node driver function */
  NPA_NODE_DEFAULT,                 /**< Attributes */
  NULL,                             /**< User data */
  NPA_EMPTY_ARRAY,                  /**< No dependencies */
  NPA_ARRAY(wci2_uart_resource)      /**< Node resource from above */
};

/* Dynamic Clock and Voltage Scaling - needed to meet uart bandwidth req */
STATIC npa_client_handle wci2_uart_dcvs_client;

/*===========================================================================

                    INTERNAL FUNCTION DEFINITIONS

===========================================================================*/

#ifdef FEATURE_DSM_WM_CB
   STATIC void wci2_uart_hiwater_cb(struct dsm_watermark_type_s *wm_ptr, void *v_ptr)
#else /* FEATURE_DSM_WM_CB */
   STATIC void wci2_uart_hiwater_cb()
#endif /* FEATURE_DSM_WM_CB */
{

  /*-----------------------------------------------------------------------*/

  wci2_tx_uart_high = TRUE;

  WCI2_MSG_0( MED, "UART high watermark callback" );

  /*-----------------------------------------------------------------------*/

  return;
}

#ifdef FEATURE_DSM_WM_CB
   STATIC void wci2_uart_lowater_cb(struct dsm_watermark_type_s *wm_ptr, void *v_ptr)
#else /* FEATURE_DSM_WM_CB */
   STATIC void wci2_uart_lowater_cb()
#endif /* FEATURE_DSM_WM_CB */
{

  /*-----------------------------------------------------------------------*/

  wci2_tx_uart_high = FALSE;

  WCI2_MSG_0( MED, "UART low watermark callback" );

  /*-----------------------------------------------------------------------*/

  return;
}

STATIC void wci2_uart_init_dsm_wmq (
  void
)
{

  /*-----------------------------------------------------------------------*/

  /* Setup queue */
  wci2_uart_wmq.q_ptr = &wci2_uart_echo_q;
  dsm_queue_init( &wci2_uart_wmq,    /* watermark queue */
                  1280,            /* dont_exceed_cnt */
                  &wci2_uart_echo_q  /* queue */
                );

  /* Setup port setting variable */
  wci2_uart_wmq.current_cnt = 0;
  wci2_uart_wmq.dont_exceed_cnt = 1280;
  wci2_uart_wmq.hi_watermark = 1024;
  wci2_uart_wmq.lo_watermark = 256;
  wci2_uart_wmq.hiwater_func_ptr = wci2_uart_hiwater_cb;
  wci2_uart_wmq.lowater_func_ptr = wci2_uart_lowater_cb;

  wci2_tx_uart_high = FALSE;

  /*-----------------------------------------------------------------------*/

  return;
}

#ifdef WCI2_FEATURE_ADV_UART_HW
/*===========================================================================

  FUNCTION:  wci2_uart_dir_read_cb

===========================================================================*/
/*!
  @brief
    Called when a direct read of type0-7 is ready from uart

  @return
    
*/
/*=========================================================================*/
STATIC void wci2_uart_dir_read_cb(
  void
)
{
  /* signal cxm_task that uart data needs to be processed */
  rex_set_sigs( &cxm_tcb, CXM_UART_RX_DIRECT_SIG );

} /* wci2_uart_dir_read_cb */

/*===========================================================================

  FUNCTION:  wci2_uart_type2_misalign_cb

===========================================================================*/
/*!
  @brief
    Called when a direct read of type0-7 is ready from uart

  @return
    
*/
/*=========================================================================*/
STATIC void wci2_uart_type2_misalign_cb(
  void
)
{
  WCI2_MSG_0( ERROR, "Detected misaligned byte in type2 reception" );
} /* wci2_uart_type2_misalign_cb */
#endif

/*===========================================================================

  FUNCTION:  wci2_uart_recv

===========================================================================*/
/*!
  @brief
    Grabs one received dsm packet from wci2_uart_rx_fifo

  @return
    returns pointer to a pointer to a DSM item if successful, 
    else returns NULL
*/
/*=========================================================================*/
STATIC dsm_item_type* wci2_uart_recv(
  void
)
{
  dsm_item_type *     wci2_rx_dsm_ptr;
  atomic_plain_word_t get_index;
  atomic_plain_word_t put_index;
  atomic_plain_word_t new_get_index;

  /*-----------------------------------------------------------------------*/

  get_index = atomic_read( &wci2_uart_rx_fifo.get_index );
  put_index = atomic_read( &wci2_uart_rx_fifo.put_index );

  if( get_index == put_index )
  {
    /* FIFO empty */
    return NULL;
  }
  else
  {
    do
    {
      /* update local copy of get_index in case it changed */
      get_index = atomic_read( &wci2_uart_rx_fifo.get_index );
      wci2_rx_dsm_ptr = wci2_uart_rx_fifo.array[get_index];
      new_get_index = get_index + 1;
      /* wrap index */
      if(new_get_index >= WCI2_UART_FIFO_SIZE)
      {
        new_get_index = 0;
      }
      /* if get_index changed out from under us, data is invalid. Try again */
    } while( !atomic_compare_and_set( &wci2_uart_rx_fifo.get_index, 
                                      get_index, new_get_index )
           );
    wci2_counter_event( WCI2_CNT_UART_RX, (uint32)wci2_rx_dsm_ptr );

    return wci2_rx_dsm_ptr;
  }
}

/*===========================================================================

  FUNCTION:  wci2_uart_sio_process_rx_packet

===========================================================================*/
/*!
  @brief
    Handler for the CxM's SIO stream to read incoming data on UART
    NOTE: lockless, overwrites when FIFO full, not reentrant

  @return
    void
*/
/*=========================================================================*/
STATIC void wci2_uart_sio_process_rx_packet(
  dsm_item_type ** wci2_rx_dsm_ptr
)
{
  atomic_plain_word_t get_index;
  atomic_plain_word_t put_index;
  atomic_plain_word_t new_get_index;
  atomic_plain_word_t new_put_index;

  /*-----------------------------------------------------------------------*/

  /* TODO: SIO/DSM can automatically enque in an RX queue for us...
   * the only problem is what to do if that queue fills up? 
   *  - maybe make the queue big enough that this should never happen,
   *    then ASSERT if it does? */

  /* save pointer to the DSM packet to process later in our context */
  get_index = atomic_read( &wci2_uart_rx_fifo.get_index );
  put_index = atomic_read( &wci2_uart_rx_fifo.put_index );

  /* push received packet into FIFO */
  new_put_index = put_index + 1;
  if( new_put_index >= WCI2_UART_FIFO_SIZE )
  {
    /* wrap index */
    new_put_index = 0;
  }
  if( new_put_index == get_index )
  {
    new_get_index = get_index + 1;
    if( new_get_index >= WCI2_UART_FIFO_SIZE )
    {
      /* wrap index */
      new_get_index = 0;
    }

    /* use atomic operation on get_index to avoid race condition with fifo_get */
    /* If set fails here, means FIFO wasn't full after all. */
    if(atomic_compare_and_set( &wci2_uart_rx_fifo.get_index, get_index, new_get_index ))
    {
      /* overflow happened, did lose data! Free the packet whose
       * pointer we're overwriting */
      dsm_free_packet( &wci2_uart_rx_fifo.array[get_index] );
      wci2_uart_rx_fifo.overflow_count++;
      WCI2_MSG_1( ERROR, "UART RX FIFO full - dropped packet (%x)",
                 wci2_uart_rx_fifo.array[get_index]);
    }
  }
  wci2_uart_rx_fifo.array[put_index] = *wci2_rx_dsm_ptr;
  atomic_set( &wci2_uart_rx_fifo.put_index, new_put_index );

  /* signal cxm_task that uart data needs to be processed */
  rex_set_sigs( &cxm_tcb, CXM_UART_RX_SIG );

  /*-----------------------------------------------------------------------*/

  return;
} /* wci2_uart_sio_process_rx_packet */

/*===========================================================================

                    EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  wci2_uart_init

===========================================================================*/
/*!
  @brief
    To initialize the UART driver

  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type wci2_uart_init (
  wci2_uart_baud_type_e baud
)
{
  errno_enum_type retval = E_SUCCESS;
  sio_stream_id_type stream_id = SIO_NO_STREAM_ID;
  sio_ioctl_param_type param;
  npa_resource_state npa_initial_state[] = { WCI2_UART_NPA_REQ_OFF };
#ifdef WCI2_FEATURE_ADV_UART_HW
  sio_status_type ioctl_status = SIO_DONE_S;
#endif

  /*-----------------------------------------------------------------------*/

  /* initialize the WM DSM queue for SIO */
  wci2_uart_init_dsm_wmq();

  INIT_WCI2_UART_MUTEX();

#ifdef T_RUMI_EMULATION
  /* RUMI has slower clocks, only supports max of 115200 */
  baud = WCI2_UART_BAUD_115200;
  wci2_sio_open_struct.rx_bitrate = SIO_BITRATE_115200;
  wci2_sio_open_struct.tx_bitrate = SIO_BITRATE_115200;
#endif

  /* Open UART using SIO interface */
  stream_id = sio_open( &wci2_sio_open_struct );
  WCI2_ASSERT( SIO_NO_STREAM_ID != wci2_sio_open_struct.stream_id );
  WCI2_ASSERT( SIO_NO_STREAM_ID != stream_id );

  /* (if needed) setup additional parameters for the UART using sio_ioctl() */
  param.set_cxm.enable_cxm = TRUE;
  param.set_cxm.sam = 0;
  sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_SET_CXM, &param);  

#ifdef WCI2_FEATURE_ADV_UART_HW
  /* enable direct read operation, type2 nib/denib in fifo */
  param.uart_cxm_type2_misalign_param.cxm_type2_misalign_cb = 
    (sio_vpu_func_ptr_type) wci2_uart_type2_misalign_cb;
  param.uart_cxm_type2_misalign_param.status_ptr = 
    &ioctl_status;
  sio_ioctl(wci2_sio_open_struct.stream_id, 
            SIO_IOCTL_ENABLE_CXM_TYPE2, &param);
  WCI2_ASSERT( ioctl_status == SIO_DONE_S );

  /* setup direct receive and associated message buffer */
  param.uart_cxm_type2_wci2_msg_param.cxm_type2_wci2_msg_cb = 
    (sio_vv_func_ptr_type) wci2_uart_dir_read_cb;
  param.uart_cxm_type2_wci2_msg_param.wci2_msg_ptr = wci2_uart_dir_read.msg_buf;
  param.uart_cxm_type2_wci2_msg_param.wci2_msg_length = WCI2_UART_DIR_READ_MSG_BUF_SIZE;
  param.uart_cxm_type2_wci2_msg_param.wci2_msg_intr_status_ptr = 
    &wci2_uart_dir_read.intr_status;
  param.uart_cxm_type2_wci2_msg_param.status_ptr = 
    &ioctl_status;
  sio_ioctl(wci2_sio_open_struct.stream_id, 
            SIO_IOCTL_ENABLE_CXM_TYPE2_WCI2_MSG_EVENT, &param);  
  WCI2_ASSERT( ioctl_status == SIO_DONE_S );

  /* restrict direct receive to only types of interest */
  param.cxm_type2_wci2_msg_intr_mask = ~(wci2_uart_dir_read.intr_enable);
  sio_ioctl(wci2_sio_open_struct.stream_id, 
            SIO_IOCTL_SET_TYPE2_WCI2_MSG_INTR_MASK, &param);  
#endif

  /*set the baud rate from NV*/
  wci2_uart_set_baud( baud );

  /* until a coex policy comes, power down uart */
  wci2_uart_state = WCI2_UART_OFF;
  sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_POWERDOWN, NULL);  
  WCI2_MSG_0( HIGH, "Powering down UART" );

  /* create client for adjusting clock when UART needed */
  wci2_uart_dcvs_client = npa_create_sync_client( "/core/cpu",
                                                 "wci2_dcvs",
                                                 NPA_CLIENT_REQUIRED );
  WCI2_ASSERT( wci2_uart_dcvs_client != NULL );

  /* define npa node, set initial state to reflect powerdown state */
  npa_define_node( &wci2_uart_node, npa_initial_state, NULL);

  /* Initialize RX FIFO */
  atomic_set( &wci2_uart_rx_fifo.get_index, 0 );
  atomic_set( &wci2_uart_rx_fifo.put_index, 0 );
  wci2_uart_rx_fifo.overflow_count = 0;

  /* Initialize UART monitors */
  wci2_uart_monitors.previous_wci2_uart_state = WCI2_UART_OFF;
  wci2_uart_monitors.count_consecutive_flush_powerdown_reqs = 0;
  wci2_uart_monitors.count_all_flush_powerdown_reqs = 0;

  /*-----------------------------------------------------------------------*/

  wci2_uart_initialized = TRUE;

  return retval;
} /* wci2_uart_init */


/*===========================================================================

  FUNCTION:  wci2_uart_deinit

===========================================================================*/
/*!
  @brief
    To de-initialize the UART driver

  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type wci2_uart_deinit (
  void
)
{
  errno_enum_type  retval = E_SUCCESS;

  /*-----------------------------------------------------------------------*/

  /* Close the UART using SIO interface */
  sio_close( wci2_sio_open_struct.stream_id, NULL );

  /* close NPA clients */
  npa_destroy_client( wci2_uart_dcvs_client );

  /*-----------------------------------------------------------------------*/

  return retval;
} /* wci2_uart_deinit */

/*===========================================================================

  FUNCTION:  wci2_uart_set_baud

===========================================================================*/
/*!
  @brief
    Sets the baud rate of the uart driver

  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type wci2_uart_set_baud(
  wci2_uart_baud_type_e baud
)
{
  errno_enum_type  retval = E_SUCCESS;
  sio_ioctl_param_type param;

  /*-----------------------------------------------------------------------*/

  param.bitrate = SIO_BITRATE_3000000;

  /* convert from wci2 baud data type to sio baud data type */
  WCI2_MSG_1( MED, "baud rate change: wci2_uart_baud_type_e %d", baud );
  if ( baud == WCI2_UART_BAUD_115200 )
  {
    param.bitrate = SIO_BITRATE_115200;
  }
  else if ( baud == WCI2_UART_BAUD_2000000 )
  {
    param.bitrate = SIO_BITRATE_2000000;
  }
  else if ( baud == WCI2_UART_BAUD_3000000 )
  {
    param.bitrate = SIO_BITRATE_3000000;
  }
  else
  {
    retval = E_FAILURE;
  }

  /* set new rate in init struct, to be sent at init */
  wci2_sio_open_struct.tx_bitrate = param.bitrate;
  wci2_sio_open_struct.rx_bitrate = param.bitrate;

  if ( wci2_uart_initialized )
  {
    /* if we are initialized, send new rate direct to driver */
    sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_CHANGE_BAUD_NOW, 
              &param);  
  }

  /*-----------------------------------------------------------------------*/

  return retval;
}

/*===========================================================================

  FUNCTION:  wci2_uart_get_baud

===========================================================================*/
/*!
  @brief
    Gets the baud rate of the uart driver

  @return
    wci2_uart_baud_type_enum 
*/
/*=========================================================================*/
wci2_uart_baud_type_e wci2_uart_get_baud(
  void
)
{
  if( wci2_sio_open_struct.tx_bitrate == SIO_BITRATE_115200 )
  {
    return WCI2_UART_BAUD_115200;
  }
  else if( wci2_sio_open_struct.tx_bitrate == SIO_BITRATE_2000000 )
  {
    return WCI2_UART_BAUD_2000000;
  }
  else
  {
    return WCI2_UART_BAUD_3000000;
  }
}

/*===========================================================================

  FUNCTION:  wci2_uart_flush_complete

===========================================================================*/
/*!
  @brief
    Finishes placing UART in powerdown state, allowing low power modes

  @return
    none
*/
/*=========================================================================*/
void wci2_uart_flush_complete( void )
{
  sio_ioctl_param_type param1, param2, param3;
  sio_status_type ioctl_status = SIO_DONE_S;
  uint8                clear_byte = 0x00;
  uint16               dsm_return = 0;
  dsm_item_type*       wci2_dsm_ptr = NULL;

  /*-----------------------------------------------------------------------*/

  ACQUIRE_WCI2_UART_MUTEX();
  
  /* if we are ON or OFF, then flush was cancelled or duplicate, do nothing */
  if ( wci2_uart_state == WCI2_UART_FLUSHING )
  {
    WCI2_MSG_0( HIGH, "uart flush request complete callback being processed" );
    /* for now hard-coding to sio interface instead of local, for optimization */
    /* tx buffer flushed, now place UART in powerdown mode */
    param1.enable_loopback_mode = TRUE;
    sio_ioctl( wci2_sio_open_struct.stream_id, SIO_IOCTL_SET_LOOPBACK_MODE, &param1 );
    DALSYS_BusyWait( 4 );
    /* allocate the DSM packet and pack in the data */
    dsm_return = dsm_pushdown( &wci2_dsm_ptr, (void *)&clear_byte,
                               sizeof(clear_byte), DSM_DS_SMALL_ITEM_POOL );
    WCI2_ASSERT( sizeof(clear_byte) == dsm_return );
    sio_transmit( wci2_sio_open_struct.stream_id, wci2_dsm_ptr );
    DALSYS_BusyWait( 10 );
    param2.cxm_tx.tx_sticky = 0;
    param2.cxm_tx.clear = TRUE;
    sio_ioctl( wci2_sio_open_struct.stream_id, SIO_IOCTL_GET_CXM_TX, &param2 );
    param2.cxm_tx.clear = FALSE;
    sio_ioctl( wci2_sio_open_struct.stream_id, SIO_IOCTL_GET_CXM_TX, &param2 );

    /* turn loopback off */
    param1.enable_loopback_mode = FALSE;
    sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_SET_LOOPBACK_MODE, &param1);

    /* attempt to power down the uart */
    param3.status_ptr = (void *)&ioctl_status;
    /* keeping a count of overall system flush-powerdown requests */
    wci2_uart_monitors.count_all_flush_powerdown_reqs++;
    sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_POWERDOWN, &param3);
    /* Check whether uart powerdown request went through */
    if( ioctl_status == SIO_DONE_S )
    {
      /* UART powered down */
      wci2_uart_monitors.previous_wci2_uart_state = wci2_uart_state;
      wci2_uart_state = WCI2_UART_OFF;
      WCI2_MSG_1( HIGH, "uart powered down, loopback setting: %d",
                  param1.enable_loopback_mode );  
      wci2_uart_monitors.count_consecutive_flush_powerdown_reqs = 0;

      /* npa vote - higher dynamic clock no longer needed */
      WCI2_ASSERT( wci2_uart_dcvs_client != NULL );
      npa_complete_request( wci2_uart_dcvs_client );
    }
    else /* ioctl_status == SIO_BADP_S */
    {
      /* UART power down request failed due to potential pending data
         re-start flushing tx buffer */
      WCI2_MSG_0( HIGH, "uart power down stalled, resending flushing request" );
      sio_flush_tx(wci2_sio_open_struct.stream_id, wci2_uart_flush_complete);
      wci2_uart_monitors.previous_wci2_uart_state = wci2_uart_state;
      wci2_uart_state = WCI2_UART_FLUSHING;

      /* Check to see if the UART ever went through a successful
          powering "down & on" cycle */
      if( wci2_uart_monitors.previous_wci2_uart_state == wci2_uart_state )
      {
        wci2_uart_monitors.count_consecutive_flush_powerdown_reqs++;
        WCI2_MSG_1( HIGH, "Consecutive uart flush-powerdown request count: %d",
                   wci2_uart_monitors.count_consecutive_flush_powerdown_reqs );

        if( wci2_uart_monitors.count_consecutive_flush_powerdown_reqs >= WCI2_UART_CONSEC_FLUSH_POWERDOWN_REQS_LIMIT )
        {
          WCI2_ERR_FATAL( "Too many consecutive uart flush-powerdown requests: %d, threshold set to %d",
                         wci2_uart_monitors.count_consecutive_flush_powerdown_reqs, 
                         WCI2_UART_CONSEC_FLUSH_POWERDOWN_REQS_LIMIT, 0 );
        }
      }
      else
      {
        /* UART has previously gone through a successful powering "down & on" cycle */
        wci2_uart_monitors.count_consecutive_flush_powerdown_reqs = 0;
        WCI2_MSG_2( HIGH, "Resetting consecutive uart flush-powerdown request count,  previous state %d != current state %d",
                   wci2_uart_monitors.previous_wci2_uart_state, wci2_uart_state );
      }
    }
  }
  else
  {
    WCI2_MSG_0( HIGH, "uart flush request was previously cancelled" );
  }

  RELEASE_WCI2_UART_MUTEX();

  /*-----------------------------------------------------------------------*/

}

/*===========================================================================

  FUNCTION:  wci2_uart_send

===========================================================================*/
/*!
  @brief
    Handler for sending data over the CxM UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_send (
  const uint8* buffer,
  uint16       size
)
{
  uint16 dsm_return = 0;
  dsm_item_type* wci2_dsm_ptr = NULL;

  /*-----------------------------------------------------------------------*/

  WCI2_ASSERT(  NULL != buffer );

  /* allocate a DSM memory buffer, copy the data into the new buffer,
   * and hand the buffer over to SIO to transmit */
  dsm_return = dsm_pushdown( &wci2_dsm_ptr, (void *)buffer,
                             size, DSM_DS_SMALL_ITEM_POOL );
  WCI2_ASSERT( size == dsm_return );

  /* transmit the data over UART */
  sio_transmit( wci2_sio_open_struct.stream_id, wci2_dsm_ptr );
  WCI2_MSG_1( LOW, "Sent raw data array size %d B over uart", size );
  WCI2_TRACE( WCI2_TRC_UART_SND_MSG, size, 0, 0 );

  /* count all UART transmit events
     NOTE: Depending on type of message, multiple bytes could be sent across */
  wci2_counter_event( WCI2_CNT_UART_TX, size );

  /*-----------------------------------------------------------------------*/

  return;
} /* wci2_uart_send */

/*===========================================================================

  FUNCTION:  wci2_uart_enable_loopback_mode

===========================================================================*/
/*!
  @brief
    Enable loopback mode on CxM's SIO stream on UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_enable_loopback_mode(
  void
)
{
  sio_ioctl_param_type param;

  /* send command to enable loopback mode */
  param.enable_loopback_mode = TRUE;
  sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_SET_LOOPBACK_MODE, &param);

  return;
}

/*===========================================================================

  FUNCTION:  wci2_uart_disable_loopback_mode

===========================================================================*/
/*!
  @brief
    Disable loopback mode on CxM's SIO stream on UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_disable_loopback_mode(
  void
)
{
  sio_ioctl_param_type param;

  /* send command to disable loopback mode */
  param.enable_loopback_mode = FALSE;
  sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_SET_LOOPBACK_MODE, &param);

  return;
}

/*===========================================================================

  FUNCTION:  wci2_uart_send_dir_char

===========================================================================*/
/*!
  @brief
    Send a byte through the direct uart register

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_send_dir_char(uint8 out_byte)
{
   sio_ioctl_param_type param;

   param.cxm_tx_direct_char.character = out_byte;
   sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_CXM_TX_DIRECT_CHAR, &param); 

   return;
}

/*===========================================================================

  FUNCTION:  wci2_is_uart_tx_full

===========================================================================*/
/*!
  @brief
    Use to check if there's room in the wci2_uart TX queue

  @return
    boolean FALSE if room
*/
/*=========================================================================*/
boolean wci2_is_uart_tx_full (
  void
)
{
  return wci2_tx_uart_high;
}

/*===========================================================================

  FUNCTION:  wci2_uart_process_rx_direct_read_data

===========================================================================*/
/*!
  @brief
    Route available wci2_uart RX direct read data to the appropriate place

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_process_rx_direct_read_data (
  void
)
{

  #ifdef WCI2_FEATURE_ADV_UART_HW
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE0_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[0]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE1_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[1]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE2_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[2]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE3_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[3]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE4_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[4]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE5_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[5]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE6_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[6]);
  }
  if ( wci2_uart_dir_read.intr_enable & wci2_uart_dir_read.intr_status &
       WCI2_UART_TYPE7_INTR_BIT )
  {
    wci2_process_rx_dir_read_data(wci2_uart_dir_read.msg_buf[7]);
  }
  #endif

} /* wci2_uart_process_rx_direct_read_data */

/*===========================================================================

  FUNCTION:  wci2_uart_process_rx_data

===========================================================================*/
/*!
  @brief
    Route available wci2_uart RX data to the appropriate place

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_process_rx_data (
  void
)
{
  dsm_item_type * wci2_rx_dsm_ptr;
  int16 dsm_return;
  uint8 byte;
  boolean used = TRUE;

  /*-----------------------------------------------------------------------*/

  /* Process all available DSM data packets recieved over CXM_UART */
  wci2_rx_dsm_ptr = wci2_uart_recv();
  while( wci2_rx_dsm_ptr )
  {
    /* pull bytes off packet one at a time until empty */
    dsm_return = dsm_pull8(&wci2_rx_dsm_ptr);

    while( -1 != dsm_return )
    {
      byte = (uint8) dsm_return;

      /* route each byte appropriately */
      WCI2_MSG_1( LOW, "UART received byte 0x%x", byte );
      /* TODO: what to do with UART_RECV trace? */

      /* Diag tests preempt normal UART operaton */
      /* TODO: instead of this, have a flag turning on/off WCI-2 operation */
      used = cxm_diag_process_uart_rx_data(byte);
      if( !used )
      {
        wci2_process_rx_data(byte);
      }

      /* try to get next byte in prep for next loop iteration */
      dsm_return = dsm_pull8(&wci2_rx_dsm_ptr);
    } /* end process dsm packet loop */

    /* get next packet for next loop iteration */
    wci2_rx_dsm_ptr = wci2_uart_recv();
  } /* end uart recv loop */

  return;
}


/*===========================================================================

  FUNCTION:  wci2_uart_npa_driver

===========================================================================*/
/*!
  @brief
    Used to deal with state transitions for wci2 uart npa resource.  When
    client voting causes a change in resource state, this function is called
    to impose the new state on hardware.

  @return
    npa_resource_state new npa state for uart resource
*/
/*=========================================================================*/
STATIC npa_resource_state wci2_uart_npa_driver ( npa_resource *resource,
                                                npa_client *client,
                                                npa_resource_state state )
{

  wci2_msg_type_s wci2_msg;

  /*-----------------------------------------------------------------------*/

  ACQUIRE_WCI2_UART_MUTEX();

  WCI2_ASSERT( (state == WCI2_UART_NPA_REQ_OFF) || 
              (state == WCI2_UART_NPA_REQ_ON) );

  if ( state == (npa_resource_state) WCI2_UART_NPA_REQ_OFF )
  {
    /* start flushing tx buffer in prep for power down */
    sio_flush_tx(wci2_sio_open_struct.stream_id, wci2_uart_flush_complete);
    wci2_uart_monitors.previous_wci2_uart_state = wci2_uart_state;
    wci2_uart_state = WCI2_UART_FLUSHING;

    /* processing continues in wci2_uart_flush_complete once flush is done */  
    WCI2_MSG_0( HIGH, "Triggering UART powering down, sending flushing request" );
  }
  else
  {
    if ( wci2_uart_state == WCI2_UART_FLUSHING )
    {
      /* if we are currently flushing, cancel and return to ON */
      wci2_uart_monitors.previous_wci2_uart_state = wci2_uart_state;
      wci2_uart_state = WCI2_UART_ON;
      sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_CANCEL_FLUSH_TX, NULL);
    }
    else
    {
      wci2_uart_monitors.previous_wci2_uart_state = wci2_uart_state;
      wci2_uart_state = WCI2_UART_ON;
    }

    /* npa vote for higher dynamic clock - 192MHz */
    WCI2_ASSERT( wci2_uart_dcvs_client != NULL );
    npa_issue_required_request( wci2_uart_dcvs_client, 192 );

    /* policy is now enforced, wakeup UART if it was powered down */
    sio_ioctl(wci2_sio_open_struct.stream_id, SIO_IOCTL_WAKEUP, NULL);

    /* send out type1 to get updated type0 information from remote end */
    wci2_msg.type = WCI2_TYPE1;
    wci2_send_msg( &wci2_msg );

    WCI2_MSG_0( HIGH, "Powering up UART" );
  }

  RELEASE_WCI2_UART_MUTEX();

  return state;
}

