#ifndef WCI2_HDLC_H
#define WCI2_HDLC_H
/*!
  @file
  wci2_hdlc.h

  @brief
  functions relating to framing & unframing using HDLC-sytle frames
  for transmission over WCI-2 type 2 messages.

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/inc/wci2_hdlc.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
07/16/13   btl     Initial version

==========================================================================*/
/*=============================================================================

                      DEFINES, CONSTANTS

=============================================================================*/
#define WCI2_TYPE2_MAX_BUFFER_SIZE 256 /* size of a DSM item */
#define WCI2_TYPE2_MAX_FRAME_SIZE WCI2_TYPE2_MAX_BUFFER_SIZE/2
/* 2 bytes flag overhead, then need to half in case every remaining byte needs
 * to be escaped, then 3 more overhead for CRC-16 and address */
/* ( FRAME_SIZE - 2(flags overhead) )/2 - 3 = 60 */
#define WCI2_TYPE2_MAX_MSG_SIZE   60

/*=============================================================================

  FUNCTION:  wci2_hdlc_frame_and_escape

=============================================================================*/
/*!
    @brief
    Frame the given raw byte buffer using HDLC-like framing. To be used for
    transporting messages over UART.

    @detail
    * The maximum message size is defined by WCI2_TYPE2_MAX_MSG_SIZE
    Frame Structure:
      +----------+----------+----------+----------+
      |   Flag   | Payload  |   CRC    |   Flag   |
      |   0x7e   |    *     | 16 bits  |   0x7e   |
      +----------+----------+----------+----------+
       - only one flag sequence needed btw two frames
       - CRC transmitted least-significant octet first (highest term coefficients)
       - CRC does not include Flags or CRC fields. i.e. escape after CRC calculation
       - closing flag is used to locate CRC field
       - recommended that transmitters send a new open flag sequence when an 
         appreciable time has elapsed (1sec?)
       - payload must be at least 4 bytes?? for CRC?

    @return
    size of frame in bytes
*/
/*===========================================================================*/
uint16 wci2_hdlc_frame_and_escape ( 
  const void* msg,             /**< In - frame payload */
  uint16      msg_size,        /**< In - size of msg payload in bytes */
  uint8       address,         /**< In - address field of frame */
  uint8**     frame_buff,      /**< In/Out - address of buffer to write frame
                                             to. If NULL, will malloc */
  uint16      frame_buff_size  /**< In - size of frame buffer in bytes if 
                                         passed in */
);

/*=============================================================================

  FUNCTION:  wci2_hdlc_unframe

=============================================================================*/
/*!
    @brief
    Take the WCI-2 HDLC-like frame and strip out all the frame info (while
    validating the frame), returning a buffer containing the original raw
    bytestream. If frame is invalid, will silently drop.

    @detail
    Start/Stop flags and escape characters should already be stripped out

    @return
    boolean frame_valid
*/
/*===========================================================================*/
boolean wci2_hdlc_unframe( 
  uint8* buffer, /**< Input - buffer containing framed msg */
  uint16 size    /**< Input - size of framed msg */
);

/*=============================================================================

  FUNCTION:  wci2_hdlc_buffer_and_unescape

=============================================================================*/
/*!
    @brief
    Take one byte at a time and construct a complete frame out of it. When
    we have a complete frame, pass it along to be processed.

    @detail
    If any characters are reserved and need to be stripped out, this is
    the place to do it.

    @return
    void
*/
/*===========================================================================*/
void wci2_hdlc_buffer_and_unescape( uint8 byte );

#endif /* WCI2_HDLC_H */
