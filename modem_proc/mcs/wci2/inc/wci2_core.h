#ifndef WCI2_CORE_H
#define WCI2_CORE_H
/*!
  @file
  wci2_core.h

  @brief
  Service for packing and sending messages/data in WCI-2 format

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/inc/wci2_core.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
02/26/14   btl     Add type 7
10/15/13   ckk     Moving over to wci2
07/01/13   btl     Initial version

==========================================================================*/
/*=============================================================================

                      INCLUDE FILES

=============================================================================*/
#include <IxErrno.h>

/*===========================================================================

                        DATA TYPES AND MACROS

===========================================================================*/
#define WCI2_BITS_TO_SHIFT_DATA       3
#define WCI2_TYPE1_RESEND_REAL_TIME   0x01 //0b00001
#define WCI2_TYPE2_HSN_BIT            0x08 //0b01000
#define WCI2_TYPE2_DATA_MASK          0xf0
#define WCI2_TYPE2_BITS_TO_SHIFT_DATA 4
#define WCI2_TYPE2_BITS_TO_SHIFT_LSN  4
#define WCI2_TYPE2_BITS_TO_SHIFT_MSN  0
#define WCI2_TYPE3_MAX_INACT_DURN_MS  155
#define WCI2_TYPE_BITS_MASK           0x07 //0b00000111

/* addresses used to mux type-2 messages */
typedef enum {
  WCI2_TYPE2_ADDRESS_QMB_COEX = 0x01,
  WCI2_TYPE2_ADDRESS_MAX
}wci2_type2_address_e;

typedef enum {
  WCI2_RESERVED_CLIENT,
  WCI2_COEX_QMB_CLIENT,
  WCI2_MAX_CLIENT
}wci2_client_e;

typedef enum {
  WCI2_TYPE0,
  WCI2_TYPE1,
  WCI2_TYPE2,
  WCI2_TYPE3,
  WCI2_TYPE4,
  WCI2_TYPE5,
  WCI2_TYPE6,
  WCI2_TYPE7,
}wci2_data_type_e;

typedef struct {
  uint16       length;
  const uint8* buffer;
}wci2_type2_type_s;

typedef struct {
  uint8   sfn;
  boolean tx;
} wci2_type6_type_s;

typedef union {
  uint8              unused;
  uint8              type1_byte;
  wci2_type2_type_s  type2_msg;
  uint16             type3_inact_durn;
  wci2_type6_type_s  type6_adv_tx_sfn;
  boolean            type7_wwan_tx_active;
}wci2_supported_data_type_u;

typedef struct {
  wci2_data_type_e type;
  wci2_supported_data_type_u data;
} wci2_msg_type_s;

/* callback to be called when client receives a type2 hdlc-style message.
 * Note: will be executed in wci2 context, so recommend just copying the message
 * to somewhere else, not processing it. */
typedef void (*wci2_type2_recv_cb_type)( uint8 address, void* msg, 
                                          uint16 size, void* param );

/*=============================================================================

                      EXTERNAL FUNCTION DEFINITIONS

=============================================================================*/
/*=============================================================================

  FUNCTION:  wci2_transport_init

=============================================================================*/
/*!
    @brief
    Initialize the WCI-2 service resources

    @return
    void
*/
/*===========================================================================*/
void wci2_transport_init( void );

/*=============================================================================

  FUNCTION:  wci2_transport_deinit

=============================================================================*/
/*!
    @brief
    Release and clean up the WCI-2 service resources

    @return
    void
*/
/*===========================================================================*/
void wci2_transport_deinit( void );

/*=============================================================================

  FUNCTION:  wci2_send_msg

=============================================================================*/
/*!
    @brief
    send a message over CXM_UART in WCI2 format

    @return
    void
*/
/*===========================================================================*/
void wci2_send_msg( 
  const wci2_msg_type_s* msg
);

/*=============================================================================

  FUNCTION:  wci2_type2_frame_and_send

=============================================================================*/
/*!
    @brief
    Frame the message into an hdlc-type frame and transmit over the UART in
    wci-2 type 2 format.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type wci2_type2_frame_and_send_msg(
  const void*          msg,
  uint16               msg_size,
  wci2_type2_address_e address
);

/*=============================================================================

  FUNCTION:  wci2_type2_client_create

=============================================================================*/
/*!
    @brief
    Create a client to receive WCI2 messages.
    Also need to call wci2_type2_register_for_msg() to receive any messages

    @return
    void
*/
/*===========================================================================*/
errno_enum_type wci2_type2_client_create(
  wci2_client_e           client,
  wci2_type2_recv_cb_type callback,
  void*                   cb_data
);

/*=============================================================================

  FUNCTION:  wci2_type2_client_delete

=============================================================================*/
/*!
    @brief
    Delete a WCI2 client, so it can no longer receive messages.

    @return
    void
*/
/*===========================================================================*/
errno_enum_type wci2_type2_client_delete(
  wci2_client_e           client
);

/*=============================================================================

  FUNCTION:  wci2_type2_register_client

=============================================================================*/
/*!
    @brief
    Register a client to receive WCI2 type2 messages sent to a particular
    address. Can call multiple times with same client and different addresses

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type wci2_type2_register_for_msg( 
  wci2_client_e        client,
  wci2_type2_address_e address,
  boolean              want_message
);

/*=============================================================================

  FUNCTION:  wci2_type2_route_received_msg

=============================================================================*/
/*!
    @brief
    If a framed message was received over WCI-2 type 2, route the message
    to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_type2_route_received_msg( void* msg, uint8 address, uint16 size );

/*=============================================================================

  FUNCTION:  wci2_process_rx_data

=============================================================================*/
/*!
    @brief
    Strip the type and route the data to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_process_rx_data( uint8 byte );

/*=============================================================================

  FUNCTION:  wci2_process_rx_dir_read_data

=============================================================================*/
/*!
    @brief
    Strip the type and route the data to the appropriate place

    @return
    void
*/
/*===========================================================================*/
void wci2_process_rx_dir_read_data( uint8 byte );

#endif /* WCI2_CORE_H */

