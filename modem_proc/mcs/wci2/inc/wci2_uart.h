#ifndef WCI2_UART_H
#define WCI2_UART_H
/*!
  @file
  wci2_uart.h

  @brief
  APIs for WCI-2's UART interface

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/wci2/inc/wci2_uart.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
           ckk     Initial implementation

==========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <comdef.h>
#include <IxErrno.h>
#include <atomic_ops.h>

/*===========================================================================

                        DATA TYPES AND MACROS

===========================================================================*/
typedef enum {
  WCI2_UART_BAUD_115200,
  WCI2_UART_BAUD_2000000,
  WCI2_UART_BAUD_3000000
}wci2_uart_baud_type_e;

typedef enum
{
  WCI2_UART_NPA_REQ_OFF,
  WCI2_UART_NPA_REQ_ON
} wci2_uart_npa_req_type_e;


/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  wci2_uart_init

===========================================================================*/
/*!
  @brief
    To initialize the UART driver

  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type wci2_uart_init(
  wci2_uart_baud_type_e baud
);

/*===========================================================================

  FUNCTION:  wci2_uart_set_baud

===========================================================================*/
/*!
  @brief
    Sets the baud rate of the uart driver

  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type wci2_uart_set_baud(
  wci2_uart_baud_type_e baud
);

/*===========================================================================

  FUNCTION:  wci2_uart_get_baud

===========================================================================*/
/*!
  @brief
    Gets the baud rate of the uart driver

  @return
    wci2_uart_baud_type_e 
*/
/*=========================================================================*/
wci2_uart_baud_type_e wci2_uart_get_baud(
  void
);

/*===========================================================================

  FUNCTION:  wci2_uart_deinit

===========================================================================*/
/*!
  @brief
    To de-initialize the UART driver

  @return
    errno_enum_type
*/

/*=========================================================================*/
errno_enum_type wci2_uart_deinit(
  void
);

/*===========================================================================

  FUNCTION:  wci2_uart_send

===========================================================================*/
/*!
  @brief
    Handler for sending data over the CxM UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_send( 
 const uint8* buffer,
 uint16       size
);

/*===========================================================================

  FUNCTION:  wci2_uart_enable_loopback_mode

===========================================================================*/
/*!
  @brief
    Enable loopback mode on CxM's SIO stream on UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_enable_loopback_mode(
  void
);

/*===========================================================================

  FUNCTION:  wci2_uart_disable_loopback_mode

===========================================================================*/
/*!
  @brief
    Disable loopback mode on CxM's SIO stream on UART

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_disable_loopback_mode(
  void
);

/*===========================================================================

  FUNCTION:  wci2_is_uart_tx_full

===========================================================================*/
/*!
  @brief
    Use to check if there's room in the wci2_uart TX queue

  @return
    boolean False if room
*/
/*=========================================================================*/
boolean wci2_is_uart_tx_full(
  void
);

/*===========================================================================

  FUNCTION:  wci2_uart_process_rx_data

===========================================================================*/
/*!
  @brief
    Route available wci2_uart RX data to the appropriate place

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_process_rx_data(
  void
);

/*===========================================================================

  FUNCTION:  wci2_uart_send_dir_char

===========================================================================*/
/*!
  @brief
    Send char through direct uart register

  @return
    void
*/
/*=========================================================================*/

void wci2_uart_send_dir_char(
  uint8 byte_to_send
);

/*===========================================================================

  FUNCTION:  wci2_uart_process_rx_direct_read_data

===========================================================================*/
/*!
  @brief
    Route available wci2 direct read RX data to the appropriate place

  @return
    void
*/
/*=========================================================================*/
void wci2_uart_process_rx_direct_read_data (
  void
);

#endif /* WCI2_UART_H */
