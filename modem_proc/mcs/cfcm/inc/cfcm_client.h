/*!
  @file
  cfcm_client.h

  @brief
  REQUIRED brief one-sentence description of this C header file.

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

  @author
  rohitj

*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cfcm/inc/cfcm_client.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
04/07/14   rj      initial version
==============================================================================*/

#ifndef CFCM_CLIENT_H
#define CFCM_CLIENT_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include "cfcm.h"

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

extern void cfcm_client_init( void );

extern void cfcm_client_update_fc_cmd
(
  cfcm_client_e          client
);

extern void cfcm_client_proc_reg
(
  const cfcm_reg_req_msg_type_s*     msg_ptr
);

extern void cfcm_client_proc_dereg
(
  const cfcm_dereg_req_msg_type_s*   msg_ptr
);

#endif /* CFCM_CLIENT_H */
