/*!
  @file
  cfcm_monitor.h

  @brief
  CFCM monitor related header file .

  @detail
  OPTIONAL detailed description of this C header file.
  - DELETE this section if unused.

  @author
  rohitj

*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cfcm/inc/cfcm_monitor.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
07/03/14   rj      BUS BW changes
04/07/14   rj      initial version
==============================================================================*/

#ifndef CFCM_MONITOR_H
#define CFCM_MONITOR_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include "cfcm.h"

/* QMI Files */
#include "mqcsi_conn_mgr.h"
#include "qmi_si.h"
#include "qmi_idl_lib.h"
#include "common_flow_control_management_v01.h"

/*==============================================================================

                   INTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief whether Bus Bandwidth Level is Valid
*/
#define CFCM_IS_BUS_BW_LEVEL_VALID(l)  (((l) >= QMI_CFCM_BUS_BW_NO_THROTTLE_V01) && \
                                       ((l) <= QMI_CFCM_BUS_BW_CRITICAL_THROTTLE_V01))


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

extern void cfcm_monitor_init( void );

/*=============================================================================

  FUNCTION:  cfcm_monitor_deinit

=============================================================================*/
/*!
    @brief
        De-Initializes CFCM QMI layer's functionalities
 
    @return
        None
*/
/*===========================================================================*/
void cfcm_monitor_deinit 
(
  void
);

extern void cfcm_monitor_proc_reg
(
  const cfcm_reg_req_type_s*    msg_ptr
);

extern void cfcm_monitor_proc_dereg
(
  const cfcm_dereg_req_type_s*   msg_ptr,
  uint32                       monitor_mask
);

extern void cfcm_monitor_compute_fc_cmd
(
  cfcm_cmd_type_s*             fc_cmd_ptr,
  uint32                       monitor_mask,
  cfcm_client_e client   /*!< the client id */
);

extern void cfcm_monitor_proc_update
(
  cfcm_monitor_ind_msg_s*       msg_ptr
);

extern boolean cfcm_monitor_registered
( 
  cfcm_monitor_e monitor
);

/*=============================================================================

  FUNCTION:  cfcm_qmi_process_req

=============================================================================*/
/*!
    @brief
        Callback function called by QCSI infrastructure when a REQ message to
        CFCM is received
 
    @note
    	QCSI infrastructure decodes the data before forwarding it to this layer
 
    @return
   	 qmi_csi_cb_error
*/
/*===========================================================================*/

qmi_csi_cb_error cfcm_qmi_process_req (
  void           *connection_handle,
  qmi_req_handle  req_handle,
  unsigned int    msg_id,
  void           *req_c_struct,
  unsigned int    req_c_struct_len,
  void           *service_cookie
);

/*===========================================================================

                                UNIT TEST

===========================================================================*/


void cfcm_test_thermal_monitor_via_diag
(
  uint8 input 
);

void cfcm_test_dsm_monitor_via_diag
(
  uint8 input 
);

#endif /* CFCM_MONITOR_H */

