/*!
  @file
  cfcm_cpu_monitor.c

  @brief
  CPU monitor implementation.

*/

/*==============================================================================

  Copyright (c) 2014-15 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cfcm/src/cfcm_cpu_monitor.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
01/20/15   rj      Add Current Q6 clk Freq in an existing F3
10/27/14   rj      CFCM changes for CLM API update and logging
10/03/14   rj      CFCM changes to Process monitor's input in its task context
04/11/14   rj      KW error fix
04/07/14   rj      initial version
==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#ifndef T_WINNT
#include <qurt.h>
#endif
#include <msgr.h>
#include <mcs_timer.h>
#include <fs_public.h>
#include "comdef.h"
#include "cfcm_cfg.h"
#include "cfcm_msg.h"
#include "cfcmi.h"
#include "cfcm_cpu_monitor.h"
#include "cfcm.h"
#include "cfcm_monitor.h"
#include "cfcm_msgr.h"
#ifdef CFCM_ON_TARGET
#include <npa.h>
#endif

/*==============================================================================

                   INTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief CPU Monitor timer frequency in ms
*/
#define CFCM_CPU_MONITOR_TIMER_PERIODICITY                                50

/*! @brief history buffer size 
*/
#define CFCM_CPU_MONITOR_HIST_BUF_SZ                                     200

/*! @brief statistics related to CPU monitor
*/
typedef struct
{
  uint32 last_load_index;  /*!< Last index filled in history buffer */
  uint32 cpu_load[CFCM_CPU_MONITOR_HIST_BUF_SZ];

} cfcm_cpu_monitor_stats_s;


/*! @brief CPU monitor related data structure
*/
typedef struct
{
  cfcm_cpu_monitor_client_cmd_s   status[CFCM_CLIENT_MAX]; /*!< current state of CPU monitor per client */

  cfcm_cpu_monitor_stats_s   stats;         /*!< statistics */

#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
} cfcm_cpu_monitor_s;

/*==============================================================================

                         LOCAL VARIABLES

==============================================================================*/

STATIC cfcm_cpu_monitor_s  cfcm_cpu_monitor;

STATIC cfcm_cpu_monitor_s* const cfcm_cpu_monitor_ptr = &cfcm_cpu_monitor;

/*==============================================================================

                    INTERNAL FUNCTION PROTOTYPES

==============================================================================*/

static void cfcm_register_with_CLM(
                                    void        *pContext,
                                    unsigned int nEventType,
                                    void        *pNodeName,
                                    unsigned int nNodeNameSize
                                  )
{
  CLM_HandleType cfcm_clm_handle;

  CFCM_ASSERT( cfcm_clm_handle =
               CLM_RegisterPeriodicClient( "CFCM", CLM_CLIENT_BASIC_CPUUTIL, (CFCM_CPU_MONITOR_TIMER_PERIODICITY*1000), 
                                           CLM_ATTRIBUTE_DEFAULT, (CLM_CallbackFuncPtr)cfcm_cb_from_clm, NULL) );

}



/*==============================================================================

                                FUNCTIONS

==============================================================================*/

/*==============================================================================

  FUNCTION:  cfcm_cpu_monitor_init

==============================================================================*/
/*!
    @brief
    initiation at task start up.

    @return
    None
*/
/*============================================================================*/
void cfcm_cpu_monitor_init( void )
{
  uint32 i;
  /*--------------------------------------------------------------------------*/


  /*--------------------------------------------------------------------------*/

  npa_resource_available_cb( "/clm/enabled", (npa_callback)cfcm_register_with_CLM, NULL );



  for(i = 0; i < CFCM_CLIENT_MAX; i++)
  {
    cfcm_cpu_monitor_ptr->status[i].state = CFCM_CPU_MONITOR_STATE_NORMAL;
    cfcm_cpu_monitor_ptr->status[i].cmd = CFCM_CMD_FC_OFF;
  }

  cfcm_cpu_monitor_ptr->stats.last_load_index = 0;
  memset(&cfcm_cpu_monitor_ptr->stats.cpu_load, 0, CFCM_CPU_MONITOR_HIST_BUF_SZ*sizeof(uint32));

#ifndef FEATURE_MODEM_CFCM_DIAG_TEST
  cfcm_test_cpu_monitor_via_diag(0);
#else
  #error code not present
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
} /* cfcm_cpu_monitor_init() */


/*==============================================================================

  FUNCTION:  cfcm_cb_from_clm

==============================================================================*/
/*!
    @brief
    Callback function for CLM to check Threshold for various clients.

    @return
    None
*/
/*============================================================================*/
void cfcm_cb_from_clm(CLM_LoadInfoBasicStructType * clmInfo, void * clientData)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /* Check Inputs params */
  if(clmInfo == NULL)  
  {
    CFCM_MSG_0( ERROR, "cfcm_cb_from_clm: NULL input parameter ");
    return;
  }

  /*--------------------------------------------------------------------------*/

#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
    if( clmInfo->updateReason == CLM_UPDATE_REASON_PERIODIC )
    {
      CFCM_MSG_2(HIGH, "CFCM using CLM: CPU percentage utilization is %d, Current Q6 Clock : %d KHz", 
                                                             clmInfo->utilPctAtMaxClk, clmInfo->currentClkKhz);
  
      msg.data_info = CFCM_MONITOR_CPU_INPUT;
      msg.monitor_data.cpu_info.cpu_load = clmInfo->utilPctAtMaxClk;
    
      // Store CPU load value 
      cfcm_cpu_monitor_ptr->stats.cpu_load[cfcm_cpu_monitor_ptr->stats.last_load_index] = clmInfo->utilPctAtMaxClk;
      cfcm_cpu_monitor_ptr->stats.last_load_index++;
      cfcm_cpu_monitor_ptr->stats.last_load_index %= CFCM_CPU_MONITOR_HIST_BUF_SZ;
	  
      send_status = cfcm_msgr_send_msg(&msg.hdr,
                                 MCS_CFCM_MONITOR_IND,
                                 sizeof(cfcm_monitor_ind_msg_s));
      CFCM_ASSERT(send_status == E_SUCCESS);
    }
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
} /* cfcm_cb_from_clm() */

/*==============================================================================

  FUNCTION:  cfcm_cpu_monitor_process_load_percentage

==============================================================================*/
/*!
    @brief
    Callback function for CLM to check Threshold for various clients.

    @return
    TRUE/FALSE
*/
/*============================================================================*/
boolean cfcm_cpu_monitor_process_load_percentage
(
  cfcm_client_e client_id,
  uint32        cpu_pct,
  cfcm_cmd_e*   cmd
)
{
  boolean state_updated = FALSE;
  cfcm_cpu_monitor_client_cmd_s   status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  status.state = CFCM_CPU_MONITOR_STATE_NORMAL;
  if (cfcm_cfg_cpu_monitor_client_cmd_status(client_id, cpu_pct, &status))
  {
    /* if the state is updated/changed then we should run FC for all clients */
    if (status.state != cfcm_cpu_monitor_ptr->status[client_id].state)
    {
      state_updated = TRUE;
      cfcm_cpu_monitor_ptr->status[client_id].cmd = status.cmd;
      *cmd = status.cmd;
      cfcm_cpu_monitor_ptr->status[client_id].state = status.state;
    }
  }

  return state_updated;
} /* cfcm_cpu_monitor_process_load_percentage() */

/*==============================================================================

                                UNIT TEST

==============================================================================*/

void cfcm_test_cpu_monitor_via_diag
(
  uint8 input 
)
{

#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#else
  CFCM_UNUSED(input);
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
  return;
} /* cfcm_test_thermal_monitor_via_diag() */


