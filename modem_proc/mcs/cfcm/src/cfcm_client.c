/*!
  @file
  cfcm_client.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

  @author
  rohitj

*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cfcm/src/cfcm_client.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
09/06/14   rj      Added monitor mask to F3
04/07/14   rj      initial version
==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <comdef.h>
#include <msg.h>
#include "cfcm_cfg.h"
#include "cfcm_msg.h"
#include "cfcmi.h"
#include "cfcm_client.h"
#include "cfcm_monitor.h"
#include "cfcm.h"
#include "cfcm_msgr.h"


/*==============================================================================

                   INTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/*! @brief history buffer size
*/
#define CFCM_CLIENT_HIST_BUF_SZ                                         (1 << 4)

/*! @brief history buffer index mask
*/
#define CFCM_CLIENT_HIST_BUF_IDX_MASK                (CFCM_CLIENT_HIST_BUF_SZ - 1)

/*! @brief statistics about a client
*/
typedef struct
{
  uint32  num_cmd_sent;      /*!< total number of commands sent to the client */
  uint32  num_down_cmd_sent; /*!< total number of down commands sent */
} cfcm_client_stats_s;

/*! @brief cfcm client information structure
*/
typedef struct
{
  cfcm_client_e    client_id;  /*!< for ease of debugging */
  cfcm_fc_evt_cb_type fc_cb_ptr; /*!< flow control event callback function */
  msgr_umid_type  fc_req;     /*!<flow control REQ umid unique for the client */
  uint32          monitor_mask; /*!<mask of monitors client respond to */
  cfcm_cmd_type_s    last_cmd;      /*!<  last cmd sent to this client */
  uint32          last_down_step_timer; /*!< step timer of the last down,
                                             set_min command*/
  cfcm_client_stats_s stats;      /*!< statistics */
  uint32        latest_cmd_hist_idx;/*!< latest command history buffer index */ 
  cfcm_cmd_type_s       cmd_hist[CFCM_CLIENT_HIST_BUF_SZ]; /*!< command history */
} cfcm_client_info_s;             

/*! @brief top level structure for cfcm_client
*/
typedef struct
{
  uint32             num_errors;               /*!< total errors */
  cfcm_client_info_s  clients[CFCM_CLIENT_MAX];  /*!< info for each know client */
} cfcm_client_s;

/*==============================================================================

                         LOCAL VARIABLES

==============================================================================*/

STATIC cfcm_client_s cfcm_client;
STATIC cfcm_client_s* const cfcm_client_ptr = &cfcm_client;
/*==============================================================================

                    INTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  cfcm_client_init_client

==============================================================================*/
/*!
    @brief
    initialized one client.

    @return
    None
*/
/*============================================================================*/
static void cfcm_client_init_client
(
  cfcm_client_e client   /*!< client id */
)
{
  uint32 j;
  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(client < CFCM_CLIENT_MAX);
  /*--------------------------------------------------------------------------*/
  cfcm_client_ptr->clients[(uint8)client].client_id           = CFCM_CLIENT_MAX;
  cfcm_client_ptr->clients[(uint8)client].fc_req              = 0;
  cfcm_client_ptr->clients[(uint8)client].monitor_mask        = 0;
  cfcm_client_ptr->clients[(uint8)client].fc_cb_ptr           = NULL;
  cfcm_client_ptr->clients[(uint8)client].last_cmd.cmd        = CFCM_CMD_FC_OFF;
  cfcm_client_ptr->clients[(uint8)client].last_cmd.step_timer = 
    CFCM_MONITOR_DEFAULT_STEP_TIMER;
  cfcm_client_ptr->clients[(uint8)client].last_down_step_timer = 
    CFCM_MONITOR_DEFAULT_STEP_TIMER;
  cfcm_client_ptr->clients[(uint8)client].stats.num_cmd_sent   = 0;
  cfcm_client_ptr->clients[(uint8)client].stats.num_down_cmd_sent = 0;

  for(j = 0; j < CFCM_CLIENT_HIST_BUF_SZ; j++)
  {
    cfcm_client_ptr->clients[(uint8)client].cmd_hist[j].cmd = CFCM_CMD_FC_OFF;
    cfcm_client_ptr->clients[(uint8)client].cmd_hist[j].step_timer = 
      CFCM_MONITOR_DEFAULT_STEP_TIMER;
  }
  cfcm_client_ptr->clients[(uint8)client].latest_cmd_hist_idx = 
    CFCM_CLIENT_HIST_BUF_IDX_MASK;
} /* cfcm_client_init_client() */




/*==============================================================================

                                FUNCTIONS

==============================================================================*/

/*==============================================================================

  FUNCTION:  cfcm_client_init

==============================================================================*/
/*!
    @brief
    initialized internal data structure at start-up.

    @return
    None

    @retval value
    OPTIONAL brief description of a particular return value goes here.
    This tag may be repeated to document a number of specific return values.
    - DELETE this section if unused

    @note
    OPTIONAL 'notes' about a function go here.
    - DELETE this section if unused

    @see related_function()
    OPTIONAL, this tag may be repeated to document a number related entities.
    - DELETE this section if unused

    @deprecated
    OPTIONAL Deprecation of usage can go here.
    - DELETE this section if unused
*/
/*============================================================================*/
EXTERN void cfcm_client_init( void )
{
  uint32 i;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cfcm_client_ptr->num_errors = 0;
  for(i = 0; i < (uint32)CFCM_CLIENT_MAX; i++)
  {
    cfcm_client_init_client((cfcm_client_e)i);
  }
} /* cfcm_client_init() */


/*==============================================================================

  FUNCTION:  cfcm_client_proc_reg

==============================================================================*/
/*!
    @brief
    process client registration message.

    @detail
    An OPTIONAL detailed description of cfcm_client_proc_reg() goes here.

    And can span multiple paragraphs, if necessary.
    - Bulleted lists are OK
      - Nesting of bullets is OK
        -# Numbered lists work, too
        -# That's it!

    DELETE this section if unused

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_client_proc_reg
(
  const cfcm_reg_req_msg_type_s* msg_ptr   /*!< pointer to the registration message */
)
{
  uint32 client_idx;
  const cfcm_reg_req_type_s*    cfcm_req = NULL;
  /*--------------------------------------------------------------------------*/
  if (IS_CFCM_DISABLED(cfcm.cfcm_disable))
  {
    CFCM_MSG_1( ERROR, "cfcm_client_proc_reg: CFCM Disabled %d ", cfcm.cfcm_disable );
    return;
  }

  CFCM_ASSERT(msg_ptr != NULL);

  cfcm_req = &msg_ptr->cfcm_req;

  CFCM_ASSERT(cfcm_req != NULL);
  CFCM_ASSERT(CFCM_IS_CLIENT_VALID(cfcm_req->client_id));
  /*--------------------------------------------------------------------------*/

  client_idx = (uint32)cfcm_req->client_id;
  /* update the client data structure */
  if(cfcm_client_ptr->clients[client_idx].client_id == CFCM_CLIENT_MAX)
  {
    CFCM_MSG_4(HIGH, "CFCM proc registration from client=%d, cb_ptr = 0x%x, umid=0x%x, mask=0x%x",
              client_idx,
              cfcm_req->req_cb,
              cfcm_req->req_umid,
              cfcm_req->monitor_mask);
  }
  else
  {
    CFCM_MSG_1(ERROR, "CFCM Registration for a client=%d already registed: Ignored",
              client_idx);
    cfcm_client_ptr->num_errors++;
    return;
  }

  cfcm_client_ptr->clients[client_idx].client_id    = cfcm_req->client_id;
  cfcm_client_ptr->clients[client_idx].fc_cb_ptr    = cfcm_req->req_cb;
  cfcm_client_ptr->clients[client_idx].fc_req       = cfcm_req->req_umid;
  cfcm_client_ptr->clients[client_idx].monitor_mask = cfcm_req->monitor_mask;

  /* update monitor(s) about this new client */
  cfcm_monitor_proc_reg(cfcm_req);

  /* update flow control command if necessary */
  cfcm_client_update_fc_cmd(cfcm_req->client_id);
} /* cfcm_client_proc_reg() */

/*==============================================================================

  FUNCTION:  cfcm_client_proc_dereg

==============================================================================*/
/*!
    @brief
    process client deregistration message.

    @detail
    An OPTIONAL detailed description of cfcm_client_proc_reg() goes here.

    And can span multiple paragraphs, if necessary.
    - Bulleted lists are OK
      - Nesting of bullets is OK
        -# Numbered lists work, too
        -# That's it!

    DELETE this section if unused

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_client_proc_dereg
(
  const cfcm_dereg_req_msg_type_s* msg_ptr/*!< pointer to the registration message */
)
{
  uint32 client_idx;
  const cfcm_dereg_req_type_s*  dereg_req = NULL;

  /*--------------------------------------------------------------------------*/
  if (IS_CFCM_DISABLED(cfcm.cfcm_disable))
  {
    CFCM_MSG_1( ERROR, "cfcm_client_proc_dereg: CFCM Disabled %d ", cfcm.cfcm_disable );
    return;
  }

  CFCM_ASSERT(msg_ptr != NULL);

  dereg_req = &msg_ptr->client;

  CFCM_ASSERT(dereg_req != NULL);
  CFCM_ASSERT(CFCM_IS_CLIENT_VALID(dereg_req->client_id));
  /*--------------------------------------------------------------------------*/

  client_idx = (uint32)dereg_req->client_id;
  /* update the client data structure */
  if(cfcm_client_ptr->clients[client_idx].client_id != CFCM_CLIENT_MAX)
  {
    CFCM_MSG_1(HIGH, "CFCM proc deregistration from client=%d", client_idx);
  }
  else
  {
    CFCM_MSG_1(ERROR, "CFCM Deregistration for a client=%d not registed: Ignored",
              client_idx);
    cfcm_client_ptr->num_errors++;
    return;
  }

  cfcm_monitor_proc_dereg(dereg_req,
                         cfcm_client_ptr->clients[client_idx].monitor_mask);

  cfcm_client_init_client(dereg_req->client_id);
} /* cfcm_client_proc_reg() */


/*==============================================================================

  FUNCTION:  cfcm_client_compute_fc_cmd

==============================================================================*/
/*!
    @brief
    compute the flow control command based on current monitor states.

    @detail
    Also send flow control command if it is different from the last command sent

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_client_update_fc_cmd
(
  cfcm_client_e          client   /*!< the client id */
)
{
  cfcm_cmd_msg_type_s     fc_cmd_msg;
  cfcm_cmd_type_s         fc_cmd;
  cfcm_client_info_s*   client_ptr;
  errno_enum_type      send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  client_ptr = &cfcm_client_ptr->clients[(uint32)client];

  cfcm_monitor_compute_fc_cmd(&fc_cmd, client_ptr->monitor_mask, client);

  if(fc_cmd.cmd == CFCM_CMD_UP)
  {
    /* Use the step_timer corresponding to the monitor that resulted the last 
    a DOWN or SET_MIN command */
    fc_cmd.step_timer = client_ptr->last_down_step_timer;
  }

  fc_cmd.client_id = client;

  if((fc_cmd.cmd != client_ptr->last_cmd.cmd) ||
     (fc_cmd.step_timer != client_ptr->last_cmd.step_timer)||
     (fc_cmd.monitors_active != client_ptr->last_cmd.monitors_active))
  {
    /* update the last cmd */
    client_ptr->last_cmd.client_id = client;
    client_ptr->last_cmd.cmd = fc_cmd.cmd;
    client_ptr->last_cmd.step_timer = fc_cmd.step_timer;
    client_ptr->last_cmd.monitors_active = fc_cmd.monitors_active;

    /* update statistics */
    client_ptr->stats.num_cmd_sent++;

    if(client_ptr->last_cmd.cmd >= CFCM_CMD_DOWN)
    {
      client_ptr->last_down_step_timer = client_ptr->last_cmd.step_timer;
      client_ptr->stats.num_down_cmd_sent++;
    }

    /* send new flow control command to the client */
    if(cfcm_cfg_client_enabled(client))
    {
      client_ptr->latest_cmd_hist_idx++;
      client_ptr->latest_cmd_hist_idx &= CFCM_CLIENT_HIST_BUF_IDX_MASK;
      client_ptr->cmd_hist[client_ptr->latest_cmd_hist_idx].cmd = fc_cmd.cmd;
      client_ptr->cmd_hist[client_ptr->latest_cmd_hist_idx].client_id = client;
      client_ptr->cmd_hist[client_ptr->latest_cmd_hist_idx].step_timer = 
        fc_cmd.step_timer;
      client_ptr->cmd_hist[client_ptr->latest_cmd_hist_idx].monitors_active = fc_cmd.monitors_active;

      if(client_ptr->fc_cb_ptr == NULL)
      {
        /* notification by message via msgr */
        fc_cmd_msg.fc_cmd = fc_cmd;

        send_status = cfcm_msgr_send_msg(&fc_cmd_msg.hdr,
                                   client_ptr->fc_req,
                                   sizeof(cfcm_cmd_msg_type_s));
        CFCM_ASSERT(send_status == E_SUCCESS);
      }
      else
      {
        (client_ptr->fc_cb_ptr)(&fc_cmd);
      }
      CFCM_MSG_4(HIGH, "CFCM issued FC command to client=%d, cmd=%d, step_timer=%d, monitors=%d",
                (uint32)client,
                (uint32)fc_cmd.cmd,
                fc_cmd.step_timer, fc_cmd.monitors_active);
    }
    else
    {
      CFCM_MSG_4(HIGH, "CFCM un-issued FC command to client=%d, cmd=%d, step_timer=%d, monitors=%d",
                (uint32)client,
                (uint32)fc_cmd.cmd,
                fc_cmd.step_timer, fc_cmd.monitors_active);
    }
  }
} /* cfcm_client_compute_fc_cmd() */


/*==============================================================================

  FUNCTION:  cfcm_cap_ul_data_rate

==============================================================================*/
/*!
    @brief
    Cap the UL data rate for the specified modem radio access technoloy

    @detail
    - this API is intended for external entity to control the UL data rate for
    the specified radio access technology. The external entity is expected to
    assume all responsibility for thermal mitigation on the modem for the RAT. 

    - When this API is called, it is expected that LTE internal thermal 
    mitigation mechanism is not enabled. Otherwise, data_rate will not be 
    applied and the functino will return FALSE. 

    - The data rate convergence will be a best effort appoximation over a
    period of time in hundreds ms. SRB and RLC control PDUs are excluded, 
    in other words, only application level data is accounted for.

    - The data rate is sticky. At power UP, CFCM_MAX_DATA_RATE is used, it is 
    going to be changed with this API call only. 

    - setting data rate to 0 will effectively prevent application data from
    being transmitted. Use with caution.

    @return
    TRUE if data rate is successfully set. FALSE otherwise. 
*/
/*============================================================================*/
EXTERN boolean cfcm_cap_ul_data_rate
(
  cfcm_modem_rat_e rat, /*!< the radio access technology rate is applied to */
  uint32          data_rate/*!< maximum data rate in number of byte per ms.  
                   Using CFCM_MAX_DATA_RATE  will turn off flow control */
)
{
  cfcm_cmd_msg_type_s     fc_cmd_msg;
  errno_enum_type      send_status;
  cfcm_client_info_s*   client_ptr;
  boolean              status = TRUE;
  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(rat == CFCM_MODEM_RAT_LTE);
  /*--------------------------------------------------------------------------*/
  client_ptr = &cfcm_client_ptr->clients[(uint32)CFCM_CLIENT_LTE_UL];

  if(client_ptr->client_id == CFCM_CLIENT_LTE_UL)
  {
    /* LTE MAC has registered */
    if(!cfcm_monitor_registered(CFCM_MONITOR_THERMAL_PA))
    {
      /* proceed only if internal TM is not intended to be used */
      
      fc_cmd_msg.fc_cmd.cmd = CFCM_CMD_SET_VALUE;
      fc_cmd_msg.fc_cmd.data_rate = data_rate;

      send_status = cfcm_msgr_send_msg(&fc_cmd_msg.hdr,
                                   client_ptr->fc_req,
                                   sizeof(cfcm_cmd_msg_type_s));
      CFCM_ASSERT(send_status == E_SUCCESS);

      CFCM_MSG_1(HIGH, "cfcm_cap_ul_data_rate data rate = %d", data_rate);
    }
    else
    {
      status = FALSE;
      cfcm_client_ptr->num_errors++;
      CFCM_ERR_FATAL("cfcm_cap_ul_data_rate failure: internal TM enabled", 0, 0, 0);
    }
  }
  else
  {
    status = FALSE;
    cfcm_client_ptr->num_errors++;
    CFCM_MSG_0(HIGH, "cfcm_cap_ul_data_rate failure: LTE MAC task not ready");
  }
  return status;
} /* cfcm_cap_ul_data_rate() */



/*==============================================================================

                                UNIT TEST

==============================================================================*/

