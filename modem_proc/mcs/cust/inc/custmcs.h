#ifndef CUSTMCS_H
#define CUSTMCS_H
/*===========================================================================

DESCRIPTION
  Configuration for MCS.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cust/inc/custmcs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/19/13   sk      asdiv feature flag for enabled pls
07/19/13   tak     Coex feature to remove NV 
11/19/12   ag      Added Triton specific SRCH4 feature
10/17/12   ag      Define bringup feature for rumi
02/10/12   sr      TCXOMGR Feature Cleanup
02/08/12   sr      removing the feature to be removed from rc-init
01/18/12   sr      SRCH4 and STM Feature Cleanup
01/10/12   ag      TRM & WMGR Feature Cleanup 
09/01/11   ns      Disable Tcxomgr bringup flag for 9x15 builds
08/18/11   ns      Enable Tcxomgr bringup for 9x15 builds
07/07/11   ag      Enable Wakeup manager
06/01/11   sm      Enable Dyn RPUSH
04/25/11   rj      Enable FEATURE_DSM_POOL_OPTIMIZED_CB for DSM Memory 
                   Level event handling optimization
04/26/11   ag      Cleanup unwanted featurizations.
04/20/11   ag      Remove trm sv feature.
04/19/11   vks     Cleanup featurization - enable AFLT feature for Nikel.
03/08/11   ag      DSM CMI features
02/02/11   sm      Enable PAM for  MDM9K 3.0 PL
01/26/11   cjb     Enable AFLT support for MDM9K 3.0 PL.
11/30/10   ag      Featurized TRM dual sim features.
11/23/10   cjb     Added SRCH4 FEATURE for Offline Search support.
11/23/10   cjb     Corrected the 2.4 PL target FEATURE.
11/17/10   cjb     Added SRCH4 FEATURE for carrierSelect support from FW.
11/10/10   cjb     Enable AFLT for MDM9K 2.2 PL ONLY.
06/19/10   vks     Enable HPQ1 for SRCH4.
02/26/10   ns      Define polarity related features in mcs (central place) 
05/07/09   ns      Enable the XO calibration for SCMM 
27/03/09   ns      Clean up to support SCMM and 8650 builds only 
10/31/08   bb      Added additional check (!T_QSC6085) for SC2x mid tier builds.
10/31/08   bb      Added check for SC2x mid tier builds.
10/20/08   adw     Replaced FEATURE_XO_DIAG w/ FEATURE_TCXOMGR_DIAG and
                   modified feature trigger to be based on no gps & no hdr.
10/10/08   adw     Stop cdma includes from being pulled into UMTS builds.
10/09/08   adw     Added check for SC2x low tier builds.
10/03/08   adw     Added support for varrying targets and technologies.
                   Pulled back in previously removed cust*.h files.
09/30/08   adw     Only define FEATURE_MCS_REDUCED_MEMORY for ULC targets.
09/26/08   mca     Removed custcdma2000.h, custhdr.h to be compatible with
                   UMTS only builds
09/16/08   adw     Corrected conditional check for including custhdr.h.
09/05/08   adw     Cleaned up featurization and reorganized each subsystem.
                   Modified includes to correctly determine GPS features.
09/02/08   adw     Added features to enable XO Cal for Rev1.1+ only.
08/28/08   adw     Initial revision.
===========================================================================*/

 

/* ------------------------------------------------------------------------
** Search 4 Features
** ------------------------------------------------------------------------ */

  #define SRCH4_HDR_ENABLED
 
  /* Enable Search 4 Low Priority Queue */
  #define SRCH4_USE_LPQ
 
  /* Define Search 4 debug message mask */
  #define MSG_DBG_SRCH4        MSG_MASK_20
  

  


/* ------------------------------------------------------------------------
** State Machine Features
** ------------------------------------------------------------------------ */

/* Add state machine related features here ... */


/* ------------------------------------------------------------------------
** TCXO Manager Features
** ------------------------------------------------------------------------ */

  /* Turn on initialization and shutdown of the tcxomgr service within TMC */
//top level feature removed:   #define FEATURE_TMC_TCXOMGR

#ifndef FEATURE_XO
    /* For VCTCXO targets, enable this feature always. Makes sure RGS is
     consistent across various variants */
  #define FEATURE_UMTS_NEGATE_ALL_TCXOMGR_VALUES
  /* For VCTCXO targets, enable this feature always. Makes sure polairty
     is consistent across variants */
//top level feature removed:   #define FEATURE_INVERTED_TRK_LO_POLARITY 
  
#else /* !FEATURE_XO */
  /* For Xo targets, enable this feature always. Makes sure RGS is
     consistent across various variants */
//top level feature removed:   #define FEATURE_TCXOMGR_NEGATE_FREQ_ERROR
  
  /*Enable dynamic rpush for L1s */
//top level feature removed:   #define FEATURE_TCXOMGR_DYN_RPUSH 
  
  /* Calibration Features */
//top level feature removed:   #define FEATURE_XO_TASK
//top level feature removed:   #define FEATURE_XO_FACTORY_CAL

#endif

#ifdef T_RUMI_EMULATION
  /* Define features for RUMI bringup */
  
  /* For disabling temp Reads via ADC DAL */
  #define FEATURE_TCXOMGR_BRINGUP
  
  /* For disabling XO Trim PMIC read/writes */
  #define FEATURE_TCXOMGR_PMIC_BRINGUP
#endif

/* ------------------------------------------------------------------------
** Transceiver Resource Manager Features
** ------------------------------------------------------------------------ */
  #ifndef FEATURE_MODEM_RCINIT 
  /* Turn on initialization of the TRM service within TMC */
  #define FEATURE_TMC_TRM
  #endif

  /* Enable Transceiver Resource Manager */
  #define FEATURE_MCS_TRM

  /* Configure TRM for API Version 2 */
//top level feature removed:   #define FEATURE_TRM_API_V2

/* ------------------------------------------------------------------------
** Coexistence Manager Features
** ------------------------------------------------------------------------ */

#ifdef FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY
  #define TRM_ASDIV_FEATURE_ENABLED  
#endif

#endif /* CUSTMCS_H */
