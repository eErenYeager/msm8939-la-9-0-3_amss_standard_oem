#ifndef CXM_QMIL_H
#define CXM_QMIL_H
/*!
  @file
  cxm_qmil.h

  @brief
  APIs for CXM's QMI Lite interface

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/inc/cxm_qmil.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/14/13   cab     Initial implementation

==========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_handle_msg

===========================================================================*/
/*!
  @brief
    Read and service incoming SMD message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_handle_msg (
  void
);

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_init

===========================================================================*/
/*!
  @brief
    To initialize SMDL port and interface

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_init (
  void
);

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_deinit

===========================================================================*/
/*!
  @brief
    To deinitialize SMDL port and interface

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_deinit (
  void
);

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_state_init_ind

===========================================================================*/
/*!
  @brief
    Handle LTE state init info MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_state_init_ind (
  void              *msgr_msg
);

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_frame_timing_ind

===========================================================================*/
/*!
  @brief
    Handle LTE frame timing MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_frame_timing_ind (
  void              *msgr_msg
);

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_notify_event_ind

===========================================================================*/
/*!
  @brief
    Handle LTE notify event MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_notify_event_ind (
  void              *msgr_msg
);

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_read_counter_cnf

===========================================================================*/
/*!
  @brief
    Handle LTE read counter conf MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_read_counter_cnf (
  void              *msgr_msg
);

#endif /* CXM_QMIL_H */
