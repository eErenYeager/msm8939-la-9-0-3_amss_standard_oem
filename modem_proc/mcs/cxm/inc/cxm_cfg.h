#ifndef __CXM_CFG_H__
#define __CXM_CFG_H__
/*!
  @file cxm_cfg.h

  @brief
   Header file used to configure the CXM (Co-Existence Manager) Layer.

   This file can be used to configure all parameters of cxm.

*/

/*=============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/inc/cxm_cfg.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/08/12   ckk     Initial revision

=============================================================================*/
#include "comdef.h"
#include "customer.h"

/* CXM's COEX service */
  #define FEATURE_MCS_CXM_COEX

#endif /* __CXM_CFG_H__ */
