#ifndef CXM_DIAG_H
#define CXM_DIAG_H
/*!
  @file
  cxm_diag.h

  @brief
  APIs for CXM's DIAG interface

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/inc/cxm_diag.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/20/12   btl     Update diag commands, add UART RX capability
           ckk     Initial implementation

==========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <IxErrno.h>

/*===========================================================================

                       DEFINITIONS & DATA TYPES

===========================================================================*/
#define FRAME_SYNC_GRFC_ENGINE  11
#define FRAME_SYNC_GRFC_NUM     38
#define RX_PRIORITY_GRFC_ENGINE 12
#define RX_PRIORITY_GRFC_NUM    39
#define TX_ACTIVE_GRFC_ENGINE   22
#define TX_ACTIVE_GRFC_NUM      37

#define CXM_UART_BUFF_LEN       128

#define CXM_DIAG_BURST_LENGTH   4

#define CXM_NUM_NOTIFI_SUBFRS   5
#define CXM_USTMR_TIME_INVALID  0xFFFFFFFF

/* list of cxm diag request indexes */
/* gaps indicate commands present on other branches, but not here */
typedef enum
{
  ENABLE_CXM                    = 0x00,
  TRIGGER_WCI2_TYPE0_MSG        = 0x01,
  SEND_ARBITRARY_WCI2_MSG_SEQ   = 0x02,
  SEND_8_ARBITRARY_WCI2_MSGS    = 0x03,
  STOP_CURRENT_TEST             = 0x04,
  ENABLE_CXM_UART_LOOPBACK_MODE = 0x05,
  START_ORIGINATOR_MODE         = 0x06,
  GET_ORIGINATOR_MODE_RESULT    = 0x07,
  START_RECEIVER_MODE           = 0x08,
  SET_CXM_UART_BAUD_RATE        = 0x09,
  SET_WWAN_TECH_STATE           = 0x0A,
  TRIGGER_WCN_PRIO              = 0x0B,
  LOG_FW_COUNTERS               = 0x0C,  
  ENABLE_RF_COMMAND             = 0x0D,
  SEND_SUBFR_NOTIFICATION       = 0x0E,
  SEND_POLICY_MSG               = 0x0F,
  SET_WCI2_TYPE7_FILTER_TIME    = 0x10
} cxm_diag_table_id_type;

typedef struct
{
  unsigned int length;
  const uint8 *bytes;
} cxm_diag_send_arb_msg_entry;

typedef enum
{
  CXM_DIAG_IND_BYTE,
  CXM_DIAG_FLOOD,
  CXM_DIAG_ORIGINATOR,
  CXM_DIAG_RECEIVER,
  CXM_DIAG_SUBFR_NOTIFI
} cxm_diag_test_type;

typedef struct
{
  unsigned int       count;
  unsigned int       length;
  unsigned int       time_apart;
  unsigned int       num_times;
  const uint8       *msg_ptr;
  cxm_diag_test_type test;
} cxm_diag_arb_msg_info_type;

typedef struct
{
  unsigned int num_failed;
  unsigned int num_run;
  unsigned int index;
  uint8        result[CXM_DIAG_BURST_LENGTH];
} cxm_diag_test_result_type;

typedef struct
{
  uint8 index;
  uint32 timing_ustmr[CXM_NUM_NOTIFI_SUBFRS];
  boolean transmit[CXM_NUM_NOTIFI_SUBFRS];
} cxm_diag_subfr_notifi_type;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  cxm_diag_init

===========================================================================*/
/*!
  @brief
    To initialize CXM's DIAG interface

  @return
    None
*/
/*=========================================================================*/
void cxm_diag_init (
  void
);

/*===========================================================================

  FUNCTION:  cxm_diag_deinit

===========================================================================*/
/*!
  @brief
    To de-initialize CXM's DIAG interface

  @return
    None
*/

/*=========================================================================*/
void cxm_diag_deinit (
  void
);

/*===========================================================================

  FUNCTION:  cxm_diag_send_arb_msgs_timer_handler

===========================================================================*/
/*!
  @brief
    timer handler to periodically send bytes for cxm_diag_send*()

  @return
    void
*/

/*=========================================================================*/
void cxm_diag_send_arb_msgs_timer_handler (
  void
);

/*===========================================================================

  FUNCTION:  cxm_diag_process_uart_rx_data

===========================================================================*/
/*!
  @brief
    routes data appropriately within cxm_diag

  @return
    TRUE if data was processed, FALSE if cxm_diag didn't want it
*/

/*=========================================================================*/
boolean cxm_diag_process_uart_rx_data (
  uint8
);

#endif /* CXM_DIAG_H */
