/*!
  @file
  coex_qmb.c

  @brief
  Coexistance service using COEX IDL v2 over QMB (with transport layer 
  WCI-2 type 2)

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/src/coex_qmb.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
03/18/14   btl     Add LTE SINR metrics for QMB, deferred push/pop
07/25/13   btl     Initial version

==========================================================================*/

/*=============================================================================

                      INCLUDES

=============================================================================*/
#include <stringl.h>
#include <queue.h>

#include <qmi_mb.h>
#include "cxm_utils.h"
#include <wci2_core.h>
#include "coex_algos_v2.h"
#include <coexistence_service_v02.h>
#include "cxm_diag.h"

/* union of all possible messages we should receive. Used to determine
 * max msg size for rcv queue */
typedef union
{
  qmi_coex_tech_sync_req_msg_v02              sync_req;
  qmi_coex_tech_sync_resp_msg_v02             sync_resp;
  qmi_coex_tech_state_update_req_msg_v02      update_req;
  qmi_coex_tech_sync_update_ind_msg_v02       update_ind;
  qmi_coex_wlan_wifi_state_ind_msg_v02        wifi_state;
  qmi_coex_metrics_lte_sinr_start_ind_msg_v02 lte_sinr_start;
  qmi_coex_metrics_lte_sinr_read_req_msg_v02  lte_sinr_read_req;
  qmi_coex_metrics_lte_sinr_stop_ind_msg_v02  lte_sinr_stop;

} coex_qmb_msg_u;

/*=============================================================================

                      DEFINES, TYPES, & CONSTANTS

=============================================================================*/
/* Maximum number of outstanding delayed/deferred QMB requests */
#define COEX_QMB_MAX_DEFERRED_REQS_ALLOWED   10

static qmi_mb_type coex_qmb_handle = NULL;

/* qmb message recv queue */
q_type coex_qmb_rx_q;

/* COEX QMB msg type */
typedef struct
{
  q_link_type qlink;
  unsigned int msg_id;
  unsigned int msg_len;
  uint8 msg[sizeof(coex_qmb_msg_u)];
} coex_qmb_msg_t;

/*!< CxM Connection Manager's outstanding deferred/delayed REQ structure */
typedef struct {
  boolean      is_valid; /* Validity check */
  unsigned int msg_id;   /* Message Id (QMB message ID) */       
} coex_qmb_deferred_req_s;

coex_qmb_deferred_req_s coex_qmb_deferred_reqs[COEX_QMB_MAX_DEFERRED_REQS_ALLOWED];

/*=============================================================================

                      FUNCTION DEFINITIONS

=============================================================================*/

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_msg_cb
=============================================================================*/
/*!
@brief
  This callback function is called by the infrastructure when a
  message is received. This callback is registered at initialization time.

@param[in]   user_handle        Handle used by the infrastructure to
                                identify different clients.
@param[in]   ep_handle          Handle for the end-point
@param[in]   msg_id             Message ID
@param[in]   msg_buf            Pointer to the message*
@param[in]   msg_buf_len        Length of the message
@param[in]   msg_cb_data        User-data

@Notes
  * If the raw flag was set on this handle (See QMI_MB_OPTIONS_SET_RAW_FLAG)
    then the user is responsible to decode the message. Else, this is the
    pointer to the C structure of the decoded message.

*/
/*=========================================================================*/
void coex_qmb_msg_cb
(
 qmi_mb_type    user_handle,
 qmi_mb_ep_type ep_handle,
 unsigned int   msg_id,
 void           *msg_buf,
 unsigned int   msg_buf_len,
 void           *msg_cb_data
)
{
  coex_qmb_msg_t* msg_ptr = NULL;

  if( msg_buf_len > sizeof(coex_qmb_msg_u) )
  {
    CXM_MSG_1( ERROR, "COEX_QMB dropped RCV msg too big: %d", msg_buf_len );
    return;
  }

  /* get a new queue item and copy the message over */
  msg_ptr = (coex_qmb_msg_t*) CXM_MEM_ALLOC( sizeof(coex_qmb_msg_t) );
  CXM_ASSERT( (NULL != msg_ptr) && (NULL != msg_buf) );
  q_link( msg_ptr, &msg_ptr->qlink );

  /* now that we have a pointer to the msg, fill it in & queue it up */
  msg_ptr->msg_id = msg_id;
  msg_ptr->msg_len = msg_buf_len;
  memscpy( msg_ptr->msg, sizeof(coex_qmb_msg_u), msg_buf, msg_buf_len );
  q_put( &coex_qmb_rx_q, &msg_ptr->qlink );

  /* signal task for processing */
  (void) rex_set_sigs( &cxm_tcb, CXM_COEX_QMB_SIG );

  return;
}

/*=============================================================================
  CALLBACK FUNCTION qmi_mb_release_cb
=============================================================================*/
/*!
@brief
  This callback function is called by the infrastructure when a connection
  is fully released.


@param[in]   release_cb_data    cookie provided in qmi_mb_release()

*/
/*=========================================================================*/
void coex_qmb_release_cb( void *release_cb_data )
{
  /* no transports remaining open */
  return;
}

/*=============================================================================

  FUNCTION:  coex_qmb_init

=============================================================================*/
/*!
    @brief
    Register this service as a client with QMB to open a connection to 
    the WCI2 UART transport

    @return
    void
*/
/*===========================================================================*/
void coex_qmb_init( void )
{
  qmi_mb_error_type retval;
  qmi_idl_service_object_type coex_service_object;

  /*-----------------------------------------------------------------------*/

  /* get IDL service object from COEX IDL */
  coex_service_object = coex_get_service_object_v02();

  /* get a handle to the QMB. Since we aren't using any options pass NULL */
  /* NOTE: if there are multiple active transports, this will connect us
   * to all of them. */
  retval = qmi_mb_init(
                        coex_service_object, //qmi_idl_service_object_type 
                        NULL,               // qmi_mb_options *options,
                        coex_qmb_msg_cb,    // qmi_mb_msg_cb msg_cb,
                        0,                  // void *msg_cb_data,
                        &coex_qmb_handle     // qmi_mb_type *user_handle
                      );
  CXM_ASSERT( QMI_MB_NO_ERR == retval );

  /* establish connection to message bus */
  /* TODO: for now join at init, in the future, can join only when 
   * LTE up and leave when LTE down to save power */
  retval = qmi_mb_join( coex_qmb_handle );
  CXM_ASSERT( QMI_MB_NO_ERR == retval );
  /* we're now connected to the bus and can begin sending messages */

  /* initialize queue for passing msgs from QMB to COEX context */
  q_init( &coex_qmb_rx_q );

  return;
}

/*=============================================================================

  FUNCTION:  coex_qmb_deinit

=============================================================================*/
/*!
    @brief
    Close connection to QMI MB and release handle

    @return
    void
*/
/*===========================================================================*/
void coex_qmb_deinit( void )
{
  /* TODO: see _join(). can use to possibly turn on/off uart later */
  qmi_mb_leave( coex_qmb_handle );
  /* will call release callback when transport is done cleaning up */
  qmi_mb_release( coex_qmb_handle, coex_qmb_release_cb, 0 );
  q_destroy( &coex_qmb_rx_q );

  return;
}

/*=============================================================================

  FUNCTION:  coex_qmb_send_msg

=============================================================================*/
/*!
    @brief
    Send a message over QMB (and by extension UART over WCI2 type2

    @detail
    Broadcasts to the bus. At this point the bus is a UART with one endpoint,
    so this is the same as sending a message to one recipient (doesn't 
    support addressing ). QMI will encode for us.

    @return
    void
*/
/*===========================================================================*/
void coex_qmb_publish_msg( unsigned int msg_id, void* msg, unsigned int len )
{
  qmi_mb_error_type retval;

  /*-----------------------------------------------------------------------*/

  CXM_MSG_1( LOW, "QMB: Sending msg id=0x%x", msg_id );

  retval = qmi_mb_publish( coex_qmb_handle, msg_id, msg, len );
  CXM_ASSERT( QMI_MB_NO_ERR == retval );

  return;
}

/*=============================================================================

  FUNCTION:  coex_qmb_push_deferred_req

=============================================================================*/
/*!
    @brief
    Helper API to push delayed/deferred QMB request onto the publisher's stack

    @return
    void
*/
/*===========================================================================*/
errno_enum_type coex_qmb_push_deferred_req (
  uint32           *deferred_req_id,   /*!< Deferred Request ID*/
  unsigned int      msg_id             /*!< Message Id */
)
{
  errno_enum_type retval = E_SUCCESS;
  uint32          loop_counter = 0;

  /*-----------------------------------------------------------------------*/

  if( NULL == deferred_req_id )
  {
    retval = E_INVALID_ARG;
  }
  else
  {
    /* Look for empty/available slot in the service's outstanding deferred/delayed
       QMI request table */
    for( loop_counter = 0; loop_counter < COEX_QMB_MAX_DEFERRED_REQS_ALLOWED; ++loop_counter )
    {
      if( FALSE == coex_qmb_deferred_reqs[loop_counter].is_valid )
      {
        break;
      }
    }

    /* Check to see if space is available in the table */
    ASSERT( COEX_QMB_MAX_DEFERRED_REQS_ALLOWED > loop_counter );

    /* Save details of the current deferred/delayed QMI request */
    coex_qmb_deferred_reqs[loop_counter].is_valid = TRUE;
    coex_qmb_deferred_reqs[loop_counter].msg_id = msg_id;

    /* Update requester with the 'ID' */
    *deferred_req_id = loop_counter;
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_qmb_pop_deferred_req

=============================================================================*/
/*!
    @brief
    Helper API to pop delayed/deferred QMB request from the publisher's stack

    @return
    void
*/
/*===========================================================================*/
errno_enum_type coex_qmb_pop_deferred_req (
  uint32            deferred_req_id,   /*!< Deferred Request ID*/
  unsigned int      msg_id             /*!< Message Id */
)
{
  errno_enum_type retval = E_SUCCESS;

  /*-----------------------------------------------------------------------*/

  if( COEX_QMB_MAX_DEFERRED_REQS_ALLOWED <= deferred_req_id )
  {
    retval = E_INVALID_ARG;
  }
  else
  {
    /* Validate the requested 'deferred_req_id' from the table */
    if( TRUE == coex_qmb_deferred_reqs[deferred_req_id].is_valid &&
        msg_id == coex_qmb_deferred_reqs[deferred_req_id].msg_id )
    {
      /* Invalidate/Empty the slot in the table for reuse later */
      coex_qmb_deferred_reqs[deferred_req_id].is_valid = FALSE;
    }
    else
    {
      /* No entry found for the deferred/delayed REQ requested here */
      retval = E_FAILURE;
    }
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_qmb_process_sig

=============================================================================*/
/*!
    @brief
    Get a message off the queue/buffer and process it/route it appropriately

    @return
    void
*/
/*===========================================================================*/
void coex_qmb_process_sig( void )
{
  coex_qmb_msg_t* msg_ptr;

  /*-----------------------------------------------------------------------*/

  /* pull received messages off queue and process them */
  while( NULL != (msg_ptr = (coex_qmb_msg_t*) q_get( &coex_qmb_rx_q )) )
  {
    CXM_MSG_1( LOW, "QMB: received MSG ID 0x%x", msg_ptr->msg_id );

    switch( msg_ptr->msg_id )
    {
      case QMI_COEX_TECH_SYNC_UPDATE_IND_V02:
        coex_handle_sync_update_ind_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_TECH_SYNC_REQ_V02:
        coex_handle_sync_state_req_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_TECH_SYNC_RESP_V02:
        coex_handle_sync_state_resp_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_TECH_STATE_UPDATE_REQ_V02:
        coex_handle_update_req_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_WLAN_WIFI_STATE_IND_V02:
        coex_handle_wlan_wifi_state_ind_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_METRICS_LTE_SINR_START_IND_V02:
        coex_handle_metrics_lte_sinr_start_ind_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_METRICS_LTE_SINR_READ_REQ_V02:
        coex_handle_metrics_lte_sinr_read_req_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      case QMI_COEX_METRICS_LTE_SINR_STOP_IND_V02:
        coex_handle_metrics_lte_sinr_stop_ind_v02( (void*)msg_ptr->msg, msg_ptr->msg_len );
        break;

      default:
        CXM_MSG_1( ERROR, "QMB: unrecognized msg id 0x%x", msg_ptr->msg_id );
        break;
    }

    /* free the message when done processing */
    CXM_MEM_FREE( msg_ptr );
  }

  return;
}

