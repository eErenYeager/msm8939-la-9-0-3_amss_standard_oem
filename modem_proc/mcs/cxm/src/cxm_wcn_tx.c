/*!
  @file
  cxm_wcn_tx.c

  @brief
  Implementation of TX sticky bit manager

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/
/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/25/13   tak     Initial implementation

===========================================================================*/


/*===========================================================================

                      INCLUDE FILES FOR MODULE

===========================================================================*/

#include <IxErrno.h>
#include <atomic_ops.h>
#include "cxm_utils.h"
#include "wcn_coex_mgr.h"
#include "cxm_wcn_tx.h"
#include "mcs_hwio.h"
#include "wci2_uart.h"
#include "npa.h"
#include <DDITimetick.h>

/*===========================================================================

                         LOCAL VARIABLES AND MACROS

===========================================================================*/

#ifndef HWIO_MSS_CXM_RX_STAT_WLAN_TX_STICKY_BMSK
  #define HWIO_MSS_CXM_RX_STAT_WLAN_TX_STICKY_BMSK 0
#endif

#ifndef HWIO_MSS_CXM_RX_STAT_IN
  #define HWIO_MSS_CXM_RX_STAT_IN 0
#endif

#if !defined( HWIO_MSS_CXM_RX_STAT_OUT )
  #define HWIO_MSS_CXM_RX_STAT_OUT( v )
#endif

#define CXM_TX_STICKY_CLEAR_VAL   1
#define CXM_WCN_TX_GET_TIME( time_var ) \
         DalTimetick_GetTimetick64( cxm_wcn_tx_DAL_handle, \
                 (DalTimetickTime64Type *) time_var )
         
#define CXM_WCN_TX_ENTER_ATOMIC_SECT() \
         while( !atomic_compare_and_set( &cxm_wcn_tx_ops_active, 0, 1 ) ); \
         CXM_WCN_TX_GET_TIME( &cxm_wcn_prev_start )

#define CXM_WCN_TX_EXIT_ATOMIC_SECT(clnt, cmd) \
         CXM_WCN_TX_GET_TIME( &cxm_wcn_prev_stop ); \
         cxm_wcn_prev_dur = cxm_wcn_prev_stop - \
                                       cxm_wcn_prev_start; \
         if ( cxm_wcn_prev_dur > cxm_wcn_tx_clnt[clnt].stats[cmd].longest_exec_period ) \
         { \
           cxm_wcn_tx_clnt[clnt].stats[cmd].longest_exec_period = cxm_wcn_prev_dur; \
           cxm_wcn_tx_clnt[clnt].stats[cmd].longest_exec_time = cxm_wcn_prev_start; \
         }\
         atomic_set( &cxm_wcn_tx_ops_active, 0 )

typedef enum
{
  CXM_WCN_TX_ENABLED,
  CXM_WCN_TX_DISABLED,
  CXM_WCN_TX_ACTIVE
} cxm_wcn_tx_status_e;
typedef enum
{
  CXM_WCN_TX_CMD_ENABLE,
  CXM_WCN_TX_CMD_DISABLE,
  CXM_WCN_TX_CMD_START,
  CXM_WCN_TX_CMD_STOP,
  CXM_WCN_TX_CMD_MAX
} cxm_wcn_tx_cmd_e;

typedef struct
{
  /*last time this function was called*/
  uint64 last_time;
  /*time when this function took longest to complete execution*/           
  uint64 longest_exec_time;
  /*longest time taken for this function to execute*/
  uint64 longest_exec_period;
  /*number of times this function is called. Note that CXM_WCN_TX_CMD_START
  and CXM_WCN_TX_CMD_STOP are reset when the client is disabled*/
  uint32 count;
} cxm_wcn_tx_client_stats_s;

typedef struct
{
  /*client's state*/
  cxm_wcn_tx_status_e state;
  /*tx state for this client*/
  cxm_wcn_tx_state_e tx_on;
  /* npa client for voting uart power state on/off */
  npa_client_handle uart_client;
  /*stats for this client*/
  cxm_wcn_tx_client_stats_s stats[CXM_WCN_TX_CMD_MAX];
} cxm_client_wcn_tx_info_s;

STATIC cxm_client_wcn_tx_info_s cxm_wcn_tx_clnt[CXM_WCN_TX_CLIENT_MAX];
STATIC atomic_word_t cxm_wcn_tx_ops_active = ATOMIC_INIT(0);
STATIC boolean cxm_wcn_tx_initialized = FALSE;
STATIC uint64 cxm_wcn_prev_start;
STATIC uint64 cxm_wcn_prev_stop;
STATIC uint64 cxm_wcn_prev_dur;

/*handle for checking Qtimer*/
STATIC DalDeviceHandle *cxm_wcn_tx_DAL_handle = NULL;

/*===========================================================================

                    EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  cxm_wcn_tx_init

===========================================================================*/
/*!
  @brief
    To initialize the WLAN TX structures

  @return
    void
*/
/*=========================================================================*/
void cxm_wcn_tx_init (
  void
)
{
  DALResult DAL_retval = DAL_SUCCESS;
  uint8 i;
  /*-----------------------------------------------------------------------*/

  if ( cxm_wcn_tx_initialized )
  {
    return;
  }
  memset( &cxm_wcn_tx_clnt, 0, sizeof( cxm_client_wcn_tx_info_s ) 
                                            * CXM_WCN_TX_CLIENT_MAX );  
  /* create npa clients for turning on/off uart for each wcn_tx client*/
  for ( i = 0; i < CXM_WCN_TX_CLIENT_MAX; i++ )
  {
    cxm_wcn_tx_clnt[i].state = CXM_WCN_TX_DISABLED;
    cxm_wcn_tx_clnt[i].uart_client = npa_create_sync_client(
                                            "/modem/mcs/cxm_uart",
                                            "coex_wcn_tx",
                                            NPA_CLIENT_REQUIRED );
  }

  /*open handle for getting Qtimer*/
  DAL_retval = DalTimetick_Attach("SystemTimer", &cxm_wcn_tx_DAL_handle);
  CXM_ASSERT( (DAL_SUCCESS == DAL_retval) && (NULL != cxm_wcn_tx_DAL_handle) );
  cxm_wcn_tx_initialized = TRUE;
  
} /* cxm_wcn_tx_init */

/*===========================================================================

  FUNCTION:  cxm_wcn_tx_enable

===========================================================================*/
/*!
  @brief
    Power on the uart block. This must be called by clients
    before using cxm_wcn_tx_start and cxm_wcn_tx_stop. When WLAN TX is not
    needed for some time, cxm_wcn_tx_disable must be called.

  @return
    cxm_wcn_tx_rsp_codes_e
*/
/*=========================================================================*/
cxm_wcn_tx_rsp_codes_e cxm_wcn_tx_enable (
  cxm_wcn_tx_client_e client
)
{
  cxm_wcn_tx_rsp_codes_e ret_val = CXM_WCN_TX_RSP_SUCCESS;
  /*-----------------------------------------------------------------------*/

  if( cxm_wcn_tx_initialized == FALSE )
  {
    ret_val = CXM_WCN_TX_RSP_NOT_SUPPORTED;
  }
  else if ( client >= CXM_WCN_TX_CLIENT_MAX )
  {
    ret_val = CXM_WCN_TX_RSP_INVALID_CLIENT;
  }
  else
  {
    CXM_WCN_TX_ENTER_ATOMIC_SECT();
    if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_ENABLED )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_ALREADY_ENABLED;
    }
    else
    {
      cxm_wcn_tx_clnt[client].state = CXM_WCN_TX_ENABLED;
      npa_issue_required_request( cxm_wcn_tx_clnt[client].uart_client, WCI2_UART_NPA_REQ_ON );
      /*get the enable time for this client*/
      CXM_WCN_TX_GET_TIME( &cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_ENABLE].last_time ); 
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_ENABLE].count++;
    }
    CXM_WCN_TX_EXIT_ATOMIC_SECT(client, CXM_WCN_TX_CMD_ENABLE);
  }
  return ret_val;
  
} /* cxm_wcn_tx_enable */

/*===========================================================================

  FUNCTION:  cxm_wcn_tx_disable

===========================================================================*/
/*!
  @brief
    Power off the uart block. 

  @return
    cxm_wcn_tx_rsp_codes_e
*/
/*=========================================================================*/
cxm_wcn_tx_rsp_codes_e cxm_wcn_tx_disable (
  cxm_wcn_tx_client_e client
)
{
  cxm_wcn_tx_rsp_codes_e ret_val = CXM_WCN_TX_RSP_SUCCESS;
  /*-----------------------------------------------------------------------*/
  
  if(cxm_wcn_tx_initialized == FALSE)
  {
    ret_val = CXM_WCN_TX_RSP_NOT_SUPPORTED;
  }
  else if ( client >= CXM_WCN_TX_CLIENT_MAX )
  {
    ret_val = CXM_WCN_TX_RSP_INVALID_CLIENT;
  }
  else
  {
    CXM_WCN_TX_ENTER_ATOMIC_SECT();  
    if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_DISABLED )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_ALREADY_DISABLED;
    }
    else if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_ACTIVE )
    {
      ret_val = CXM_WCN_TX_RSP_DISABLE_FAIL_TX_CHECK_ACTIVE;
    }
    else
    {
      cxm_wcn_tx_clnt[client].state = CXM_WCN_TX_DISABLED;
      npa_issue_required_request( cxm_wcn_tx_clnt[client].uart_client, WCI2_UART_NPA_REQ_OFF );
      /*get the enable time for this client*/
      CXM_WCN_TX_GET_TIME( &cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_DISABLE].last_time );
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_DISABLE].count++;
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_START].count = 0;
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_STOP].count = 0;
    }
    CXM_WCN_TX_EXIT_ATOMIC_SECT(client, CXM_WCN_TX_CMD_DISABLE);
  }
  return ret_val;

} /* cxm_wcn_tx_disable */

/*===========================================================================

  FUNCTION:  cxm_wcn_tx_start

===========================================================================*/
/*!
  @brief
    To start collecting WLAN tx state for this tech

  @return
    void
*/
/*=========================================================================*/
cxm_wcn_tx_rsp_codes_e cxm_wcn_tx_start (
  cxm_wcn_tx_client_e client
)
{
  cxm_wcn_tx_state_e sticky_state = CXM_WCN_TX_STATE_INACTIVE;
  uint8 i = 0;
  cxm_wcn_tx_rsp_codes_e ret_val = CXM_WCN_TX_RSP_SUCCESS;
  /*-----------------------------------------------------------------------*/

  if( client >= CXM_WCN_TX_CLIENT_MAX )
  {
    ret_val = CXM_WCN_TX_RSP_INVALID_CLIENT;
  }
  else
  {
    CXM_WCN_TX_ENTER_ATOMIC_SECT();
    if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_DISABLED )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_NOT_ENABLED;
    }
    else if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_ACTIVE )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_ALREADY_STARTED;
    }
    else
    {
      /*update any active client with the current state of the bit 
        before clearing it*/
      if ( HWIO_MSS_CXM_RX_STAT_WLAN_TX_STICKY_BMSK & HWIO_MSS_CXM_RX_STAT_IN )
      {
        sticky_state = CXM_WCN_TX_STATE_ACTIVE;
      }
      else
      {
        sticky_state = CXM_WCN_TX_STATE_INACTIVE;
      }
      
      for ( i=0; i < CXM_WCN_TX_CLIENT_MAX; i++ )
      {
        if ( cxm_wcn_tx_clnt[i].state == CXM_WCN_TX_ACTIVE )
        {
          cxm_wcn_tx_clnt[i].tx_on |= sticky_state;
        }
      }

      /*clear the tx sticky bit*/
      HWIO_MSS_CXM_RX_STAT_OUT(CXM_TX_STICKY_CLEAR_VAL);

      /*set requesting client to active*/
      cxm_wcn_tx_clnt[client].state = CXM_WCN_TX_ACTIVE;
      cxm_wcn_tx_clnt[client].tx_on = CXM_WCN_TX_STATE_INACTIVE;

      /*update stats for this command*/
      CXM_WCN_TX_GET_TIME( &cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_START].last_time );
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_START].count++;
      
    }
    CXM_WCN_TX_EXIT_ATOMIC_SECT(client, CXM_WCN_TX_CMD_START);
  }
  return ret_val;

} /* cxm_wcn_tx_start */


/*===========================================================================

  FUNCTION:  cxm_wcn_tx_stop

===========================================================================*/
/*!
  @brief
    To stop collecting WLAN tx state for this client and return the tx state for
    the period. cxm_wcn_tx_start must be called before calling this.

  @return
    cxm_wcn_tx_rsp_codes_e
*/
/*=========================================================================*/
cxm_wcn_tx_rsp_codes_e cxm_wcn_tx_stop (
  cxm_wcn_tx_client_e client,
  cxm_wcn_tx_state_e* tx_state
)
{
  cxm_wcn_tx_state_e sticky_state = CXM_WCN_TX_STATE_INACTIVE;
  cxm_wcn_tx_rsp_codes_e ret_val = CXM_WCN_TX_RSP_SUCCESS;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT(tx_state != NULL);
  if( client >= CXM_WCN_TX_CLIENT_MAX )
  {
    ret_val = CXM_WCN_TX_RSP_INVALID_CLIENT;
  }
  else
  {
    CXM_WCN_TX_ENTER_ATOMIC_SECT();
    if ( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_DISABLED )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_NOT_ENABLED;
    }
    else if( cxm_wcn_tx_clnt[client].state == CXM_WCN_TX_ENABLED )
    {
      ret_val = CXM_WCN_TX_RSP_CLNT_ALREADY_STOPPED;
    }
    else
    {
      /*reset the client*/
      cxm_wcn_tx_clnt[client].state = CXM_WCN_TX_ENABLED;

      /*wlan tx may have been ON during this client's collection period but cleared by 
        another client so we OR stored state with the current state of the sticky bit*/
      if (HWIO_MSS_CXM_RX_STAT_WLAN_TX_STICKY_BMSK & HWIO_MSS_CXM_RX_STAT_IN )
      {
        sticky_state = CXM_WCN_TX_STATE_ACTIVE;
      }
      else
      {
        sticky_state = CXM_WCN_TX_STATE_INACTIVE;
      }
      (*tx_state) = cxm_wcn_tx_clnt[client].tx_on | sticky_state;

      /*update stats for this command*/
      CXM_WCN_TX_GET_TIME( &cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_STOP].last_time );
      cxm_wcn_tx_clnt[client].stats[CXM_WCN_TX_CMD_STOP].count++;
    }
    CXM_WCN_TX_EXIT_ATOMIC_SECT(client, CXM_WCN_TX_CMD_STOP);
  }
  return ret_val;

} /* cxm_wcn_tx_stop */




