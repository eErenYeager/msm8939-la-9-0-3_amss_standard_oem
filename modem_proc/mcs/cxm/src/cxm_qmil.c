/*!
  @file
  cxm_qmil.c

  @brief
  Implementation of CXM's QMIL (QMI Lite) APIs

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/
/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/src/cxm_qmil.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/11/13   cab     Initial implementation

===========================================================================*/


/*===========================================================================

                      INCLUDE FILES FOR MODULE

===========================================================================*/
/* Common */
#include "mcs_variation.h"
#include <IxErrno.h>

#include <msgr_rex.h>
#include <smd_lite.h>
#include <smsm.h>
#include <stringl.h>
#include "lte_cxm_msg.h"
#include "cxm_msgr.h"
#include "wcn_coex_mgr.h"
#include "cxm_utils.h"
#include "coex_interface.h"
#include "cxm_trace.h"
#include "cxm_qmil_intf_v01.c"

/*===========================================================================

                   INTERNAL TYPES

===========================================================================*/

#define CXM_QMIL_QMI_HEADER_SIZE   7
#define CXM_QMIL_SMDL_FIFO_SIZE    4096
#define CXM_QMIL_MAX_SMD_MSG_SIZE  256
#define CXM_QMIL_MAX_QMI_MSG_SIZE \
  ( CXM_QMIL_MAX_SMD_MSG_SIZE - CXM_QMIL_QMI_HEADER_SIZE )

typedef struct
{
  uint8  msg_type;
  uint16 txn_id;
  uint16 msg_id;
  uint16 msg_len;
} cxm_qmil_qmi_hdr_s;

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/

LOCAL smdl_handle_type cxm_qmil_smdl_port; 
LOCAL cxm_qmil_qmi_hdr_s cxm_qmil_qmi_hdr;
LOCAL uint8 cxm_qmil_rx_msg_buf[CXM_QMIL_MAX_SMD_MSG_SIZE];
LOCAL uint8 cxm_qmil_tx_msg_buf[CXM_QMIL_MAX_SMD_MSG_SIZE];

/*===========================================================================

                    INTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_rx_cb

===========================================================================*/
/*!
  @brief
    Callback for receiving data from SMDL layer

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_smdl_rx_cb(
  smdl_handle_type smdlHandleObj, 
  smdl_event_type event,
  void *pCbData)
{

  /*-----------------------------------------------------------------------*/

  if ( event == SMDL_EVENT_READ )
  {
    /* signal task for processing */
    (void) rex_set_sigs( &cxm_tcb, CXM_COEX_SMDL_SIG );
  }
  CXM_TRACE( CXM_TRC_SMDL_RCV_NOT, event, 0, 0, CXM_TRC_EN );

} /* cxm_qmil_smdl_rx_cb */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_dec_hdr

===========================================================================*/
/*!
  @brief
    Decode QMI Lite header

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_smdl_dec_hdr (
  uint8 *buf,
  cxm_qmil_qmi_hdr_s *hdr
)
{

  /*-----------------------------------------------------------------------*/

  CXM_ASSERT( buf != NULL );
  CXM_ASSERT( hdr != NULL );
  memscpy(&hdr->msg_type, 1, buf, 1);
  memscpy(&hdr->txn_id, 2, buf + 1, 2);
  memscpy(&hdr->msg_id, 2, buf + 3, 2);
  memscpy(&hdr->msg_len, 2, buf + 5, 2);

} /* cxm_qmil_smdl_dec_hdr */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_enc_hdr

===========================================================================*/
/*!
  @brief
    Encode QMI Lite header

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_smdl_enc_hdr (
  uint8 *buf,
  cxm_qmil_qmi_hdr_s *hdr
)
{

  /*-----------------------------------------------------------------------*/

  CXM_ASSERT( buf != NULL );
  CXM_ASSERT( hdr != NULL );
  memscpy(buf, 1, &hdr->msg_type, 1);
  memscpy(buf + 1, 2, &hdr->txn_id, 2);
  memscpy(buf + 3, 2, &hdr->msg_id, 2);
  memscpy(buf + 5, 2, &hdr->msg_len, 2);

} /* cxm_qmil_smdl_enc_hdr */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_snd_msg

===========================================================================*/
/*!
  @brief
    Send a message via SMDL

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_smdl_snd_msg (
  uint8 *buf,
  cxm_qmil_qmi_hdr_s *hdr
)
{

  /*-----------------------------------------------------------------------*/

  /* QMIL header encode */
  cxm_qmil_smdl_enc_hdr ( buf, hdr );

  /* send it off */
  smdl_write( cxm_qmil_smdl_port, hdr->msg_len + CXM_QMIL_QMI_HEADER_SIZE, 
              buf, SMDL_WRITE_FLAGS_NONE );
  CXM_TRACE( CXM_TRC_SMDL_SND_MSG, hdr->msg_id, hdr->msg_len, 0, CXM_TRC_EN );

  CXM_MSG_1( HIGH, "Sent message %d to smdl", hdr->msg_id );
} /* cxm_qmil_smdl_snd_msg */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_rcv_msg

===========================================================================*/
/*!
  @brief
    Process received message from SMDL

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_smdl_rcv_msg (
  void *buf
)
{
  int32 peek_size = smdl_rx_peek( cxm_qmil_smdl_port );
  int32 bytes_read = 0;

  /*-----------------------------------------------------------------------*/

  while( peek_size > 0 )
  {
    CXM_ASSERT( peek_size <= (int32) CXM_QMIL_MAX_SMD_MSG_SIZE );

    /* read the data */
    bytes_read = smdl_read( cxm_qmil_smdl_port,
                            (uint32) peek_size,
                            buf,
                            (uint32) SMDL_READ_FLAGS_NONE );
    CXM_TRACE( CXM_TRC_SMDL_RCV_MSG, bytes_read, peek_size, 0, CXM_TRC_EN );
    CXM_ASSERT( bytes_read == peek_size );

    /* check for more data */
    peek_size = smdl_rx_peek( cxm_qmil_smdl_port );
  }

} /* cxm_qmil_smdl_rcv_msg */

/*===========================================================================

  FUNCTION:  cxm_qmil_msgr_snd_msg

===========================================================================*/
/*!
  @brief
    Send and log message router message

  @return
    none
*/
/*=========================================================================*/
STATIC errno_enum_type cxm_qmil_msgr_snd_msg (
  msgr_hdr_struct_type *msg_hdr_ptr,  /*!< Pointer to MSG Header  */
  msgr_umid_type        umid,         /*!< UMID of the message    */
  uint32                size          /*!< Size of the message    */
)
{
  errno_enum_type                msgr_result;

  /*-----------------------------------------------------------------------*/

  /* Initialize the message header */
  msgr_init_hdr( msg_hdr_ptr, MSGR_LTE_ML1_COEX, umid );

  /* Send the message */
  msgr_result = msgr_send( msg_hdr_ptr, size );

  CXM_MSG_1( HIGH, "Sent umid %d to msgr", umid );
  CXM_TRACE( CXM_TRC_MSGR_SND_MSG, msgr_result, 0, umid, CXM_TRC_EN );

  return msgr_result;

} /* cxm_qmil_msgr_snd_msg */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_activate_req

===========================================================================*/
/*!
  @brief
    Handle activate request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_activate_req (
  uint8              *smdl_msg,
  cxm_qmil_qmi_hdr_s *smdl_hdr
)
{
  cxm_activate_req_msg_v01     cxm_activate_req;
  qmi_idl_service_object_type  cxm_object = cxm_get_service_object_v01();
  int32_t                      qmi_result;
  lte_ml1_cxm_activation_req_s coex_activation;
  errno_enum_type              msgr_result;

  /*-----------------------------------------------------------------------*/

  qmi_result = qmi_idl_message_decode ( cxm_object,
                                        smdl_hdr->msg_type,
                                        smdl_hdr->msg_id,
                                        smdl_msg,
                                        smdl_hdr->msg_len,
                                        (uint8 *) &cxm_activate_req,
                                        sizeof(cxm_activate_req_msg_v01) );
  CXM_ASSERT( qmi_result == 0 );

  coex_activation.data_plane_enable = 
    ( (cxm_activate_req.data_plane_enable == 0) ? FALSE:TRUE );

  msgr_result = cxm_qmil_msgr_snd_msg( &coex_activation.msg_hdr, 
                                       LTE_ML1_CXM_ACTIVATION_REQ, 
                                       sizeof(lte_ml1_cxm_activation_req_s) );

  if ( msgr_result != E_SUCCESS )
  {
    CXM_MSG_1( ERROR, "Activation req send failed: %d", msgr_result );
  }

} /* cxm_qmil_handle_activate_req */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_deactivate_req

===========================================================================*/
/*!
  @brief
    Handle deactivate request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_deactivate_req (
  void
)
{
  lte_ml1_cxm_deactivation_req_s coex_deactivation;
  errno_enum_type                msgr_result;

  /*-----------------------------------------------------------------------*/

  msgr_result = cxm_qmil_msgr_snd_msg( &coex_deactivation.msg_hdr, 
                                       LTE_ML1_CXM_DEACTIVATION_REQ, 
                                       sizeof(lte_ml1_cxm_deactivation_req_s) );

  if ( msgr_result != E_SUCCESS )
  {
    CXM_MSG_1( ERROR, "Deactivation req send failed: %d", msgr_result );
  }

} /* cxm_qmil_handle_deactivate_req */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_state_req

===========================================================================*/
/*!
  @brief
    Handle state request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_state_req (
  void
)
{
  lte_ml1_cxm_state_update_req_s coex_state_req;
  errno_enum_type                msgr_result;

  /*-----------------------------------------------------------------------*/

  msgr_result = cxm_qmil_msgr_snd_msg( &coex_state_req.msg_hdr, 
                                       LTE_ML1_CXM_STATE_UPDATE_REQ, 
                                       sizeof(lte_ml1_cxm_state_update_req_s) );

  if ( msgr_result != E_SUCCESS )
  {
    CXM_MSG_1( ERROR, "State req send failed: %d", msgr_result );
  }

} /* cxm_qmil_handle_state_req */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_stmr_read_req

===========================================================================*/
/*!
  @brief
    Handle STMR (LTE System Timer) read request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_stmr_read_req (
  void
)
{
  lte_ml1_cxm_reading_count_req_s coex_reading_counter;
  errno_enum_type                 msgr_result;

  /*-----------------------------------------------------------------------*/

  msgr_result = cxm_qmil_msgr_snd_msg( &coex_reading_counter.msg_hdr, 
                                       LTE_ML1_CXM_READING_COUNTER_REQ, 
                                       sizeof(lte_ml1_cxm_reading_count_req_s) );

  if ( msgr_result != E_SUCCESS )
  {
    CXM_MSG_1( ERROR, "STMR read req send failed: %d", msgr_result );
  }

} /* cxm_qmil_handle_stmr_read_req */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_stmr_read_compl_req

===========================================================================*/
/*!
  @brief
    Handle STMR read complete request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_stmr_read_compl_req (
  void
)
{
  lte_ml1_cxm_rc_complete_ind_s coex_reading_counter_done;
  errno_enum_type               msgr_result;

  /*-----------------------------------------------------------------------*/

  msgr_result = cxm_qmil_msgr_snd_msg( &coex_reading_counter_done.msg_hdr, 
                                       LTE_ML1_CXM_RC_COMPLETE_IND, 
                                       sizeof(lte_ml1_cxm_rc_complete_ind_s) );

  if ( msgr_result != E_SUCCESS )
  {
    CXM_MSG_1( ERROR, "STMR read compl send failed: %d", msgr_result );
  }

} /* cxm_qmil_handle_stmr_read_compl_req */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_phr_backoff_req

===========================================================================*/
/*!
  @brief
    Handle PHR backoff request from SMDL interface

  @return
    none
*/
/*=========================================================================*/
STATIC void cxm_qmil_handle_phr_backoff_req (
  uint8              *smdl_msg,
  cxm_qmil_qmi_hdr_s *smdl_hdr
)
{
  cxm_phr_backoff_req_msg_v01    cxm_backoff_req;
  qmi_idl_service_object_type    cxm_object = cxm_get_service_object_v01();
  int32_t                        qmi_result;
  lte_ml1_cxm_phr_backoff_req_s        phr_bo;
  lte_ml1_cxm_phr_less_backoff_req_s   phrl_bo;
  errno_enum_type                msgr_result;

  /*-----------------------------------------------------------------------*/

  qmi_result = qmi_idl_message_decode ( cxm_object,
                                        smdl_hdr->msg_type,
                                        smdl_hdr->msg_id,
                                        smdl_msg,
                                        smdl_hdr->msg_len,
                                        (uint8 *) &cxm_backoff_req,
                                        sizeof(cxm_phr_backoff_req_msg_v01) );
  CXM_ASSERT( qmi_result == 0 );

  if( cxm_backoff_req.phr_backoff_valid != 0 )
  {
    phr_bo.backoff_mtpl = cxm_backoff_req.phr_backoff.backoff_mtpl;
    phr_bo.priority_threshold = cxm_backoff_req.phr_backoff.priority_threshold;
    msgr_result = cxm_qmil_msgr_snd_msg( &phr_bo.msg_hdr, 
                                         LTE_ML1_CXM_PHR_BACKOFF_REQ, 
                                         sizeof(lte_ml1_cxm_phr_backoff_req_s) );
    if( msgr_result != E_SUCCESS )
    {
      CXM_MSG_1( ERROR, "PHR backoff send failed: %d", msgr_result );
    }
  }
  else if ( cxm_backoff_req.phr_less_backoff_valid != 0 )
  {
    phrl_bo.backoff_mtpl = cxm_backoff_req.phr_less_backoff.backoff_mtpl;
    phrl_bo.priority_threshold = cxm_backoff_req.phr_less_backoff.priority_threshold;
    phrl_bo.starting_time = cxm_backoff_req.phr_less_backoff.starting_time;
    phrl_bo.num_of_subframes = cxm_backoff_req.phr_less_backoff.num_of_subframes;
    msgr_result = cxm_qmil_msgr_snd_msg( &phrl_bo.msg_hdr, 
                                         LTE_ML1_CXM_PHR_LESS_BACKOFF_REQ, 
                                         sizeof(lte_ml1_cxm_phr_less_backoff_req_s) );
    if ( msgr_result != E_SUCCESS )
    {
      CXM_MSG_1( ERROR, "PHRless backoff send failed: %d", msgr_result );
    }
  }
  else
  {
    CXM_MSG_2( ERROR, "PHR backoff req invalid: %d %d", 
               cxm_backoff_req.phr_backoff_valid, 
               cxm_backoff_req.phr_less_backoff_valid );
  }

} /* cxm_qmil_handle_phr_backoff_req */

/*===========================================================================

                    EXTERNAL FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_handle_msg

===========================================================================*/
/*!
  @brief
    Read and service incoming SMD message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_handle_msg (
  void
)
{
  /*-----------------------------------------------------------------------*/

  cxm_qmil_smdl_rcv_msg( cxm_qmil_rx_msg_buf );
  cxm_qmil_smdl_dec_hdr( cxm_qmil_rx_msg_buf, &cxm_qmil_qmi_hdr );

  CXM_MSG_1( HIGH, "Received message %d from smdl", cxm_qmil_qmi_hdr.msg_id );

  if ( cxm_qmil_qmi_hdr.msg_type >= QMI_IDL_NUM_MSG_TYPES )
  {
    CXM_MSG_1( ERROR, "Invalid qmi msg type: %d", cxm_qmil_qmi_hdr.msg_type );
    return;
  }

  if ( cxm_qmil_qmi_hdr.msg_len >= CXM_QMIL_MAX_QMI_MSG_SIZE )
  {
    CXM_MSG_1( ERROR, "Invalid qmi msg length: %d", cxm_qmil_qmi_hdr.msg_len );
    return;
  }

  switch( cxm_qmil_qmi_hdr.msg_id )
  {
    case QMI_CXM_ACTIVATE_REQ_MSG_V01:
      cxm_qmil_handle_activate_req( cxm_qmil_rx_msg_buf + CXM_QMIL_QMI_HEADER_SIZE, 
                                    &cxm_qmil_qmi_hdr );
      break;
    case QMI_CXM_DEACTIVATE_REQ_MSG_V01:
      cxm_qmil_handle_deactivate_req();
      break;
    case QMI_CXM_STATE_REQ_MSG_V01:
      cxm_qmil_handle_state_req();
      break;
    case QMI_CXM_STMR_READ_REQ_MSG_V01:
      cxm_qmil_handle_stmr_read_req();
      break;
    case QMI_CXM_STMR_READ_COMPLETE_REQ_MSG_V01:
      cxm_qmil_handle_stmr_read_compl_req();
      break;
    case QMI_CXM_PHR_BACKOFF_REQ_MSG_V01:
      cxm_qmil_handle_phr_backoff_req( cxm_qmil_rx_msg_buf + CXM_QMIL_QMI_HEADER_SIZE, 
                                       &cxm_qmil_qmi_hdr );
      break;
    default:
      ERR_FATAL( "Invalid qmi msg id received: %d", 
                 cxm_qmil_qmi_hdr.msg_id, 0, 0 );
      break;
  }

} /* cxm_qmil_smdl_handle_msg */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_init

===========================================================================*/
/*!
  @brief
    To initialize SMDL port and interface

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_init (
  void
)
{

  /*-----------------------------------------------------------------------*/

  cxm_qmil_smdl_port = smdl_open( "CXM_QMI_PORT", 
                            SMD_MODEM_RIVA,
                            SMDL_OPEN_FLAGS_PRIORITY_IST,
                            CXM_QMIL_SMDL_FIFO_SIZE,
                            cxm_qmil_smdl_rx_cb, NULL );

} /* cxm_qmil_smdl_init */

/*===========================================================================

  FUNCTION:  cxm_qmil_smdl_deinit

===========================================================================*/
/*!
  @brief
    To deinitialize SMDL port and interface

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_smdl_deinit (
  void
)
{

  /*-----------------------------------------------------------------------*/

  smdl_close( cxm_qmil_smdl_port );

} /* cxm_qmil_smdl_deinit */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_state_init_ind

===========================================================================*/
/*!
  @brief
    Handle LTE state init info MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_state_init_ind (
  void              *msgr_msg
)
{
  lte_ml1_cxm_state_init_ind_s *ci_ind = 
    (lte_ml1_cxm_state_init_ind_s *) msgr_msg;

  /*-----------------------------------------------------------------------*/

  if ( ci_ind->initialized == FALSE ) 
  {
    /* LTE is going down */
    smsm_state_clr( SMSM_MODEM_STATE, SMSM_LTE_COEX_AWAKE );
  }
  else
  {
    /* LTE is coming up*/
    if ( !( smsm_state_get(SMSM_MODEM_STATE) & SMSM_LTE_COEX_AWAKE ) )
    {
      /* flush SMD msgs, none expected when SMSM state not set */ 
      cxm_qmil_smdl_rcv_msg( cxm_qmil_rx_msg_buf );
      smsm_state_set( SMSM_MODEM_STATE, SMSM_LTE_COEX_AWAKE );
    }
  }

} /* cxm_qmil_handle_lte_state_init_ind */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_frame_timing_ind

===========================================================================*/
/*!
  @brief
    Handle LTE frame timing MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_frame_timing_ind (
  void              *msgr_msg
)
{
  qmi_idl_service_object_type cxm_object;
  lte_ml1_cxm_frame_timing_ind_s *ft_ptr = 
    (lte_ml1_cxm_frame_timing_ind_s *) msgr_msg;
  cxm_frame_timing_ind_msg_v01 ft_msg;
  int32_t err_code = 0;
  uint32_t msg_len = 0;
  uint8 i = 0;
  cxm_qmil_qmi_hdr_s qmi_hdr = 
    { QMI_IDL_INDICATION, 0, QMI_CXM_FRAME_TIMING_IND_V01, 0 };

  /*-----------------------------------------------------------------------*/

  if ( !( smsm_state_get(SMSM_WCNSS_STATE) & SMSM_LTE_COEX_AWAKE ) )
  {
    CXM_MSG_0( ERROR, "WCN in sleep when sending frame timing ind" );
    return;
  }

  /* copy MSGR fields to QMIL structure */
  ft_msg.frame_timing_report_list_len = ft_ptr->num_of_reports;
  for( i = 0; i < ft_ptr->num_of_reports; i++ )
  {
    ft_msg.frame_timing_report_list[i].report_type = 
      (cxm_lte_ml1_coex_report_type_e_v01) ft_ptr->report[i].report_type;
    ft_msg.frame_timing_report_list[i].timing.dl_subframe = 
      ft_ptr->report[i].timing.dl_sub_frame;
    ft_msg.frame_timing_report_list[i].timing.ul_subframe = 
      ft_ptr->report[i].timing.ul_sub_frame;
    ft_msg.frame_timing_report_list[i].timing.dl_frame_time = 
      ft_ptr->report[i].timing.dl_frame_time;
    ft_msg.frame_timing_report_list[i].timing.ul_frame_time = 
      ft_ptr->report[i].timing.ul_frame_time;
  }

  /* QMIL message encode */
  cxm_object = cxm_get_service_object_v01();
  err_code = qmi_idl_message_encode( cxm_object,
                                     QMI_IDL_INDICATION,
                                     QMI_CXM_FRAME_TIMING_IND_V01,
                                     &ft_msg,
                                     sizeof(cxm_frame_timing_ind_msg_v01),
                                     cxm_qmil_tx_msg_buf + CXM_QMIL_QMI_HEADER_SIZE,
                                     CXM_QMIL_MAX_QMI_MSG_SIZE,
                                     &msg_len );
  qmi_hdr.msg_len = (uint16) msg_len;

  CXM_ASSERT( err_code == QMI_IDL_LIB_NO_ERR );

  /* send it off */
  cxm_qmil_smdl_snd_msg( cxm_qmil_tx_msg_buf, &qmi_hdr );

} /* cxm_qmil_handle_lte_frame_timing_ind */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_notify_event_ind

===========================================================================*/
/*!
  @brief
    Handle LTE notify event MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_notify_event_ind (
  void              *msgr_msg
)
{
  qmi_idl_service_object_type cxm_object;
  lte_ml1_cxm_notify_event_ind_s* ne_ptr = 
    (lte_ml1_cxm_notify_event_ind_s *) msgr_msg;
  cxm_notify_event_ind_msg_v01 ne_msg;
  int32_t err_code = 0;
  uint32_t msg_len = 0;
  cxm_qmil_qmi_hdr_s qmi_hdr = 
    { QMI_IDL_INDICATION, 0, QMI_CXM_NOTIFY_EVENT_IND_V01, 0 };

  /*-----------------------------------------------------------------------*/

  if ( !( smsm_state_get(SMSM_WCNSS_STATE) & SMSM_LTE_COEX_AWAKE ) )
  {
    CXM_MSG_0( ERROR, "WCN in sleep when sending notify event ind" );
    return;
  }

  /* copy MSGR fields to QMIL structure */
  ne_msg.lte_ml1_tx_event_info_valid  = TRUE;
  ne_msg.lte_ml1_tx_event_info.channel_type = 
    (cxm_lte_ml1_coex_channel_e_v01) ne_ptr->tx_event_info.channel_type;
  ne_msg.lte_ml1_tx_event_info.subframe = ne_ptr->tx_event_info.sub_frame;
  ne_msg.lte_ml1_tx_event_info.tx_is_scheduled = ne_ptr->tx_event_info.tx_flag;
  ne_msg.lte_ml1_tx_event_info.priority = ne_ptr->tx_event_info.priority;
  ne_msg.lte_ml1_tx_event_info.slot0_power = ne_ptr->tx_event_info.slot0_power;
  ne_msg.lte_ml1_tx_event_info.slot0_first_rb = 
    ne_ptr->tx_event_info.slot0_rb.first_rb;
  ne_msg.lte_ml1_tx_event_info.slot0_last_rb = 
    ne_ptr->tx_event_info.slot0_rb.last_rb;
  ne_msg.lte_ml1_tx_event_info.slot1_power = ne_ptr->tx_event_info.slot1_power;
  ne_msg.lte_ml1_tx_event_info.slot1_first_rb = 
    ne_ptr->tx_event_info.slot1_rb.first_rb;
  ne_msg.lte_ml1_tx_event_info.slot1_last_rb = 
    ne_ptr->tx_event_info.slot1_rb.last_rb;
  ne_msg.lte_ml1_tx_event_info.srs_power = ne_ptr->tx_event_info.srs_power;
  ne_msg.lte_ml1_tx_event_info.srs_first_rb = 
    ne_ptr->tx_event_info.srs_rb.first_rb;
  ne_msg.lte_ml1_tx_event_info.srs_last_rb = 
    ne_ptr->tx_event_info.srs_rb.last_rb;
  ne_msg.lte_ml1_snr_info_valid = TRUE;
  ne_msg.lte_ml1_snr_info.snr = ne_ptr->snr_info.snr;
  ne_msg.lte_ml1_snr_info.snr_is_valid = ne_ptr->snr_info.valid;
  ne_msg.lte_ml1_snr_info.subframe= ne_ptr->snr_info.subframe;

  /* QMIL message encode */
  cxm_object = cxm_get_service_object_v01();
  err_code = qmi_idl_message_encode( cxm_object,
                                     QMI_IDL_INDICATION,
                                     QMI_CXM_NOTIFY_EVENT_IND_V01,
                                     &ne_msg,
                                     sizeof(cxm_notify_event_ind_msg_v01),
                                     cxm_qmil_tx_msg_buf + CXM_QMIL_QMI_HEADER_SIZE,
                                     CXM_QMIL_MAX_QMI_MSG_SIZE,
                                     &msg_len );
  qmi_hdr.msg_len = (uint16) msg_len;

  CXM_ASSERT( err_code == QMI_IDL_LIB_NO_ERR );

  /* send it off */
  cxm_qmil_smdl_snd_msg( cxm_qmil_tx_msg_buf, &qmi_hdr );

} /* cxm_qmil_handle_lte_notify_event_ind */

/*===========================================================================

  FUNCTION:  cxm_qmil_handle_lte_read_counter_cnf

===========================================================================*/
/*!
  @brief
    Handle LTE read counter conf MSGR message

  @return
    none
*/
/*=========================================================================*/
void cxm_qmil_handle_lte_read_counter_cnf (
  void              *msgr_msg
)
{
  cxm_qmil_qmi_hdr_s qmi_hdr = 
    { QMI_IDL_INDICATION, 0, QMI_CXM_STMR_READ_RESP_MSG_V01, 0 };

  /*-----------------------------------------------------------------------*/

  if ( !( smsm_state_get(SMSM_WCNSS_STATE) & SMSM_LTE_COEX_AWAKE ) )
  {
    CXM_MSG_0( ERROR, "WCN in sleep when sending read counter cnf" );
    return;
  }

  /* send it off */
  cxm_qmil_smdl_snd_msg( cxm_qmil_tx_msg_buf, &qmi_hdr );

} /* cxm_qmil_handle_lte_read_counter_cnf */





