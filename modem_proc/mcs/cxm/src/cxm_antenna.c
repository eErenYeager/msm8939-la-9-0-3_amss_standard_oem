/*!
  @file
  cxm_antenna.c

  @brief
  This file contains the message handlers and algorithms for sharing
  the diversity antenna with WLAN using WCI-2 Control Plane messaging

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/cxm/src/cxm_antenna.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
02/02/14   ckk     Adding antenna grant duration
11/17/13   btl     Initial version

==========================================================================*/
/*=============================================================================

                      INCLUDE FILES

=============================================================================*/
#include <rcevt_rex.h> /* RCEVT Signal Delivery by REX */
#include <trm.h>
#include <timer.h>
#include <npa.h>
#include <coexistence_service_v02.h>
#include <wci2_uart.h> /* UART NPA vote */
#include "cxm_utils.h" /* rex & task signals, MSG */
#include "cxm_trace.h"
#include "coex_algos_v2.h"
#include "cxm_antenna.h"

#define CXM_SCLK_PER_MS 32
/* minimum duration for requesting antenna in 32kHz ticks = 100ms */
#define CXM_ANTENNA_REQ_MIN_DUR 100*CXM_SCLK_PER_MS
/* give a 2ms buffer before release must happen (in 32kHz ticks) */
#define CXM_ANTENNA_RELEASE_BUFFER 2*CXM_SCLK_PER_MS
#define CXM_ANTENNA_MAX_DUR 0xFFFF

/* set fields within sync_state_token */
#define COEX_TECH_TOKEN_MASK  0x1F
#define COEX_TECH_TOKEN_SHIFT 0
#define COEX_ANT_STATE_MASK  0x01
#define COEX_ANT_STATE_SHIFT 7
#define COEX_BITFLD_VAL(var,field) ( \
  ((var) & (field##_MASK << field##_SHIFT)) >> field##_SHIFT )
/* take a numerical value and shift and mask it into field position */
#define COEX_BITFLD_FVAL(var,field) ( \
  ((var) & field##_MASK) << field##_SHIFT )
/* read, mask in supplied data, then write back */
#define COEX_BITFLD_SET(var,val,field) (var)=    \
  ( ((var) & ~(field##_MASK << field##_SHIFT)) | \
    (((val) & field##_MASK) << field##_SHIFT) )

/* current valid tokens for antenna sharing */
#define COEX_ANTENNA_TOKEN_INVALID 0x0
#define COEX_ANTENNA_TOKEN_VALID 0x1

static cxm_antenna_state_s cxm_antenna_state = 
{
  CXM_ANTENNA_UNUSED, NULL, NULL, NULL, 0
};

/* @brief Antenna npa client for voting uart power state on/off */
static npa_client_handle cxm_antenna_uart_client;

/*! @brief Timer to track antenna duration grants */
static timer_type cxm_antenna_timer;

/* trm callback declarations */
void cxm_antenna_grant_callback(trm_client_enum_t           client,
                                trm_chain_grant_return_type grant,
                                trm_request_tag_t           tag,
                                trm_duration_t              duration);
void cxm_antenna_unlock_callback(trm_client_enum_t       client,
                                 trm_unlock_event_enum_t event,
                                 uint32                  unlock_by_sclk);


/*=============================================================================

                      INTERNAL FUNCTION DEFINITIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  cxm_get_antenna_state_ptr

=============================================================================*/
/*!
    @brief
    Returns a pointer to the global cxm_antenna_state struct for diag logging.

    @return
    cxm_antenna_state_s *
*/
/*===========================================================================*/
cxm_antenna_state_s *cxm_get_antenna_state_ptr( void )
{
  return &cxm_antenna_state;
}

/*=============================================================================

  FUNCTION:  cxm_antenna_update_state_info

=============================================================================*/
/*!
    @brief
    Update stored antenna sharing state info and send a new state notification
    if anything changed

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_update_state_info( uint16 adv_notice, uint16 duration )
{
  if(( cxm_antenna_state.info->adv_notice == adv_notice ) &&
     ( cxm_antenna_state.info->duration == duration ))
  {
    /* nothing changed, nothing to do. */
    return;
  }
  else
  {
    /* save new state */
    cxm_antenna_state.info->adv_notice = adv_notice;
    cxm_antenna_state.info->duration = duration;

    #if 0 /* Disabling token updates for antenna sharing */
    /* increment token to show this is a new state, then send to WCN */
    token = COEX_BITFLD_VAL(
                    cxm_antenna_state.sync->sync_state_token,
                    COEX_TECH_TOKEN) + 1;
    /* if token wrapped, wrap it and set wrap flag */
    if( token > COEX_TECH_TOKEN_MASK )
    {
      token = 0;
      *cxm_antenna_state.token_wrap = TRUE;
    }
    COEX_BITFLD_SET(cxm_antenna_state.sync->sync_state_token,
                    token, COEX_TECH_TOKEN );
    #endif /* Disabling token updates for antenna sharing */

    coex_send_diversity_antenna_state_ind_v02();
  }
}

/*=============================================================================

  FUNCTION:  cxm_antenna_request_and_notify

=============================================================================*/
/*!
    @brief
    Request antenna on behalf of WLAN

    @return
    void
*/
/*===========================================================================*/
inline void cxm_antenna_request_and_notify( void )
{
  trm_frequency_type_t freq;

  CXM_MSG_1( LOW, "Antenna: Released, new req for %d", CXM_ANTENNA_REQ_MIN_DUR );

  /* make request to TRM for antenna */
  freq.band = SYS_BAND_CLASS_MAX;
  freq.num_channels = 0;
  freq.channels = 0;
  trm_freq_request_and_notify_enhanced(
    TRM_WLAN,
    TRM_RX_SECONDARY,
    CXM_ANTENNA_REQ_MIN_DUR,
    TRM_ACCESS_DIVERSITY,
    cxm_antenna_grant_callback, 0,
    cxm_antenna_unlock_callback,
    freq
  );

  cxm_antenna_state.state = CXM_ANTENNA_PENDING;

  CXM_TRACE( CXM_ANT_RELEASE_AND_REQUEST, TRM_WLAN, 
             TRM_RX_SECONDARY, CXM_ANTENNA_REQ_MIN_DUR,
             CXM_TRC_AND_PKT_EN, CXM_LOG_ANTENNA_STATE );

  return;
}

/*=============================================================================

  FUNCTION:  cxm_antenna_grant_callback

=============================================================================*/
/*!
CALLBACK TRM_GRANT_ENH_CALLBACK_TYPE

  @brief
  Tell us of the antenna chain availablity

DESCRIPTION
  Used by the Transceiver manager to inform the clients of transceiver 
  management events.

DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None
*/
/*===========================================================================*/
void cxm_antenna_grant_callback
(
  /* The client which is being informed of an event */
  trm_client_enum_t           client,
  /* The event being sent to the client */
  trm_chain_grant_return_type grant,
  /* Anonymous payload echoed from trm_request_and_notify() */
  trm_request_tag_t           tag,
  /* Duration by which the lock is extended */
  trm_duration_t              duration
)
{
  /* TODO: right now, only access when no other techs are active. If we
   * do access while other techs active, may need a specific antenna chain
   * to know that the correct diversity antenna is available? */
  CXM_ASSERT( TRM_WLAN == client );

  CXM_TRACE( CXM_ANT_GRANT_CB, 0, grant.grant, duration, CXM_TRC_AND_PKT_EN,
             CXM_LOG_ANTENNA_STATE );

  /* check if we were granted the chain */
  if( TRM_DENIED == grant.grant )
  {
    /* request denied, request again */
    /* Should not happen with request_and-notify */
    CXM_MSG_0( ERROR, "TRM request denied" );
    cxm_antenna_request_and_notify();
  }
  else
  {
    CXM_MSG_2( MED, "Antenna: granted chain %d for %d", grant.grant, duration );
    cxm_antenna_state.grant_dur = duration;
    /* signal task for processing */
    (void) rex_set_sigs( &cxm_tcb, CXM_ANTENNA_GRANT_SIG );
  }

  return;
}

/*============================================================================
FUNCTION: cxm_antenna_unlock_callback

CALLBACK TRM_UNLOCK_CALLBACK_T

DESCRIPTION
  The prototype for unlock event callback functions, used by the Transceiver
  Resource Manager to inform the clients of when it should unlock a resource
  it holds.
  
DEPENDENCIES
  The callback will be called by the Transceiver Manager.  It may be called
  from a task context of another client, or from interrupt context.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/

void cxm_antenna_unlock_callback
(
  /* The client which is being informed of an event */
  trm_client_enum_t       client,
  /* The event being sent to the client */
  trm_unlock_event_enum_t event,
  /* Sclk timestamp for TRM_UNLOCK_BY */
  uint32                  unlock_by_sclk
)
{
  timetick_type ts;

  CXM_ASSERT( TRM_WLAN == client );

  ts = unlock_by_sclk - timetick_get_safe();
  CXM_TRACE( CXM_ANT_UNLOCK_CB, event, ts, unlock_by_sclk, CXM_TRC_AND_PKT_EN,
             CXM_LOG_ANTENNA_STATE );
  CXM_MSG_2( MED, "Antenna: unlock req event=%d unlock_in=%d sclk",
             event, ts );
  if( TRM_UNLOCK_BY == event )
  {
    /* don't need to unlock now, but need to set timer and unlock
     * before the given timestamp */
    timer_set( &cxm_antenna_timer, 
               ts - CXM_ANTENNA_RELEASE_BUFFER, 
               0, T_TICK );
  }
  else if( TRM_UNLOCK_CANCELLED == event )
  {
    /* don't need to unlock. clear any existing unlock timer */
    (void) timer_clr( &cxm_antenna_timer, T_TICK );
  }
  else
  {
    /* unlock now. signal task to do dirty work */
    (void) rex_set_sigs( &cxm_tcb, CXM_ANTENNA_RELEASE_SIG );
    /* clear timer since we just signalled to release. don't
     * need timer to do it a second time. */
    (void) timer_clr( &cxm_antenna_timer, T_TICK );
  }

  return;
}

/*=============================================================================

                      EXTERNAL FUNCTION DEFINITIONS

=============================================================================*/
/*=============================================================================

  FUNCTION:  cxm_antenna_init

=============================================================================*/
/*!
    @brief
    Initialize resources and start diversity antenna resource request

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_init
(
  coex_tech_sync_state_v02 *sync_state_p,
  coex_antenna_state_v02   *state_info_p,
  boolean                  *token_wrap_flag_p
)
{
  /* get relevant pointers to global coex_algos_v2 data structures 
   * for local use */
  CXM_ASSERT( sync_state_p != NULL && 
              state_info_p != NULL &&
              token_wrap_flag_p != NULL );
  cxm_antenna_state.info = state_info_p;
  cxm_antenna_state.sync = sync_state_p;
  cxm_antenna_state.token_wrap = token_wrap_flag_p;

  /* initialize state information */
  cxm_antenna_state.state = CXM_ANTENNA_UNUSED;
  cxm_antenna_update_state_info( 0, CXM_ANTENNA_MAX_DUR );
  COEX_BITFLD_SET(cxm_antenna_state.sync->sync_state_token,
                  COEX_ANTENNA_TOKEN_VALID, COEX_TECH_TOKEN );

  /* create npa client for turning on/off uart */
  cxm_antenna_uart_client = npa_create_sync_client(
                              "/modem/mcs/cxm_uart",
                              "cxm_antenna",
                              NPA_CLIENT_REQUIRED );

  /* define timer to keep track of grant duration */
  timer_def( &cxm_antenna_timer, NULL, &cxm_tcb, 
             CXM_ANTENNA_RELEASE_SIG, NULL, 0 );

  /* wait for TRM to enter system before starting antenna sharing */
  rcevt_wait_name("TRM_INIT_COMPLETE");

  /* begin antenna sharing request */
  cxm_antenna_request_and_notify();

  return;
}

/*=============================================================================

  FUNCTION:  cxm_antenna_deinit

=============================================================================*/
/*!
    @brief
    Release resources associated with WWAN/WLAN antenna sharing

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_deinit( void )
{
  /* clear all timers (if any pending) */
  (void) timer_clr( &cxm_antenna_timer, T_TICK );

  trm_release( TRM_WLAN );
  cxm_antenna_state.state = CXM_ANTENNA_UNUSED;
  return;
}

/*=============================================================================

  FUNCTION:  cxm_antenna_handle_grant

=============================================================================*/
/*!
    @brief
    We have been granted the WLAN antenna; execute appropriate actions and
    send message to WLAN

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_handle_grant( void )
{
  /* You have been granted usage an RF chain, assume antenna is available.*/
  cxm_antenna_state.state = CXM_ANTENNA_GRANTED;
  if( CXM_ANTENNA_REQ_MIN_DUR <= cxm_antenna_state.grant_dur )
  {
    npa_issue_required_request( cxm_antenna_uart_client, WCI2_UART_NPA_REQ_ON );

    /* tell WCN the good news */
    COEX_BITFLD_SET(
      cxm_antenna_state.sync->sync_state_token,
      COEX_ANTENNA_CHAIN_STATE_ACTIVE_V02, COEX_ANT_STATE);
    cxm_antenna_update_state_info( 0, (uint16)(cxm_antenna_state.grant_dur/CXM_SCLK_PER_MS) );
    coex_send_diversity_antenna_state_ind_v02();

    npa_issue_required_request( cxm_antenna_uart_client, WCI2_UART_NPA_REQ_OFF );
  }
  else
  {
    /* not granted enough time. Release and don't mention this incident 
     * to WLAN */
    CXM_MSG_2( ERROR, "Antenna: Not enough time (%d), wanted %d ticks", 
               cxm_antenna_state.grant_dur, CXM_ANTENNA_REQ_MIN_DUR );
    cxm_antenna_request_and_notify();
  }

  return;
}

/*=============================================================================

  FUNCTION:  cxm_antenna_handle_release

=============================================================================*/
/*!
    @brief
    We have been asked us to release Antenna. Can be caused two ways:
      1. TRM asked us to release through callback
      2. Our grant duration is up and our timer fired

    @detail
    First try to re-acquire antenna. If that fails, submit a new request
    and notify WLAN that we lost it.

    @return
    void
*/
/*===========================================================================*/
void cxm_antenna_handle_release( void )
{
  trm_duration_t dur = 0;

  /* try to get extension for antenna */
  dur = trm_change_duration( TRM_WLAN,
                             TRM_DONT_SHORTEN,
                             TRM_DONT_SHORTEN );

  CXM_MSG_2( MED, "Antenna: Req extension, dur given=%d thresh=%d", 
             dur, CXM_ANTENNA_RELEASE_BUFFER );

  /* check if extension granted and long enough to be useful */
  if( dur > CXM_ANTENNA_RELEASE_BUFFER )
  {
    /* set timer for duration given. Make new request when timer expires */
    timer_set( &cxm_antenna_timer, dur - CXM_ANTENNA_RELEASE_BUFFER, 
               0, T_TICK );
  }
  else
  {
    /* need to unlock the antenna chain now:
     * make new request. will implicitly release currently-held chain */
    cxm_antenna_request_and_notify();

    npa_issue_required_request( cxm_antenna_uart_client, WCI2_UART_NPA_REQ_ON );

    /* tell WLAN they lost the antenna */
    COEX_BITFLD_SET(
      cxm_antenna_state.sync->sync_state_token,
      COEX_ANTENNA_CHAIN_STATE_INACTIVE_V02, COEX_ANT_STATE);
    cxm_antenna_update_state_info( 0, CXM_ANTENNA_MAX_DUR );
    coex_send_diversity_antenna_state_ind_v02();

    npa_issue_required_request( cxm_antenna_uart_client, WCI2_UART_NPA_REQ_OFF );
  }

  return;
}

