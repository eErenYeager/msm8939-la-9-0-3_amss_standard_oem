/*!
  @file   __msgr_global_umids.h

  @brief Auto-generated file containing UMID mapping tables

  @detail
  This file is AUTO-GENERATED.  Do not modify!
  This auto-generated header file declares the message router table mapping
  UMIDs to their string equivalent

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#ifndef MSGR_UMID_GEN
#error "__msgr_global_umids.h may only be included from msgr_umid.c"
#endif /* MSGR_UMID_GEN */

/* @brief Populate table entries based on flags set */
#ifdef TF_MSCGEN
  #ifndef TF_UMID_NAMES
    #error "Incorrect configuration - TF_UMID_NAMES must be defined"
  #else
    /* TF_MSCGEN & TF_UMID_NAMES is defined, so define complete table */
    #define MSGR_UMID_TABLE_ENTRY(hex_id, name, func_ptr) { hex_id, name, func_ptr }
  #endif
#elif defined TF_UMID_NAMES
  /* Only TF_UMID_NAMES is defined, so define UMID names only */
   #define MSGR_UMID_TABLE_ENTRY(hex_id, name, func_ptr) { hex_id, name, NULL }
#else
  /* TF_MSCGEN/TF_UMID_NAMES are not defined, so no definitions required
     This case is for final release*/
#endif

#ifdef TF_MSCGEN
#endif /* TF_MSCGEN */

/*! @brief Table mapping UMID to strings */
static const msgr_umid_name_entry_s msgr_umid_name_table[] = {
#if ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES))
  /* TF_MSCGEN/TF_UMID_NAMES are not defined, so no definitions */
  { 0x00000000, "", NULL },
#else
  /* MCS_MSGR_SNOOP_SPR, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00010001, "MCS_MSGR_SNOOP_SPR", NULL ),

  /* MCS_MSGR_ALL_MQS_BLOCKED_SPR, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00010002, "MCS_MSGR_ALL_MQS_BLOCKED_SPR", NULL ),

  /* MCS_MSGR_SMDL_INIT_INDI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00011403, "MCS_MSGR_SMDL_INIT_INDI", NULL ),

  /* MCS_APPMGR_THREAD_RUN_IND, payload: appmgr_ind_thread_run_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00020400, "MCS_APPMGR_THREAD_RUN_IND", NULL ),

  /* MCS_FWS_SHUTDOWN_SPR, payload: fws_shutdown_spr_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070000, "MCS_FWS_SHUTDOWN_SPR", NULL ),

  /* MCS_FWS_APP_CONFIG_CMD, payload: fws_app_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070100, "MCS_FWS_APP_CONFIG_CMD", NULL ),

  /* MCS_FWS_SUSPEND_CMD, payload: fws_suspend_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070101, "MCS_FWS_SUSPEND_CMD", NULL ),

  /* MCS_FWS_WAKEUP_CMD, payload: fws_wakeup_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070102, "MCS_FWS_WAKEUP_CMD", NULL ),

  /* MCS_FWS_APP_CONFIG_RSP, payload: fws_app_config_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070300, "MCS_FWS_APP_CONFIG_RSP", NULL ),

  /* MCS_FWS_SUSPEND_RSP, payload: fws_suspend_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070301, "MCS_FWS_SUSPEND_RSP", NULL ),

  /* MCS_FWS_WAKEUP_RSP, payload: fws_wakeup_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070302, "MCS_FWS_WAKEUP_RSP", NULL ),

  /* MCS_FWS_DIAG_MSG_IND, payload: fws_diag_msg_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070400, "MCS_FWS_DIAG_MSG_IND", NULL ),

  /* MCS_FWS_DIAG_LOG_IND, payload: fws_diag_log_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070401, "MCS_FWS_DIAG_LOG_IND", NULL ),

  /* MCS_FWS_WDOG_REPORT_TMRI, payload: fws_wdog_report_tmri_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00071600, "MCS_FWS_WDOG_REPORT_TMRI", NULL ),

  /* MCS_CXM_WWAN_TECH_STATE_UPDATE_REQ, payload: cxm_wwan_tech_state_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0201, "MCS_CXM_WWAN_TECH_STATE_UPDATE_REQ", NULL ),

  /* MCS_CXM_COEX_BOOT_PARAMS_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0220, "MCS_CXM_COEX_BOOT_PARAMS_REQ", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_LTE_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0221, "MCS_CXM_COEX_ACTIVE_POLICY_LTE_REQ", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0226, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_BLER_REQ, payload: cxm_coex_metrics_lte_bler_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0227, "MCS_CXM_COEX_METRICS_LTE_BLER_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_SINR_REQ, payload: cxm_coex_metrics_lte_sinr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0228, "MCS_CXM_COEX_METRICS_LTE_SINR_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_TDSCDMA_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b022e, "MCS_CXM_COEX_METRICS_TDSCDMA_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM1_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b022f, "MCS_CXM_COEX_METRICS_GSM1_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM2_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0230, "MCS_CXM_COEX_METRICS_GSM2_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM3_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0231, "MCS_CXM_COEX_METRICS_GSM3_REQ", NULL ),

  /* MCS_CXM_DESENSE_IND_REQ, payload: cxm_coex_desense_ind_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0272, "MCS_CXM_DESENSE_IND_REQ", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_RSP, payload: cxm_coex_tx_pwr_lmt_lte_cndtns_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0326, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_RSP", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_SINR_RSP, payload: cxm_coex_metrics_lte_sinr_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0328, "MCS_CXM_COEX_METRICS_LTE_SINR_RSP", NULL ),

  /* MCS_CXM_COEX_TECH_METRICS_RSP, payload: cxm_coex_metrics_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0332, "MCS_CXM_COEX_TECH_METRICS_RSP", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_RSP, payload: cxm_coex_ba_blist_res_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0366, "MCS_CXM_BAND_AVOID_BLIST_RSP", NULL ),

  /* MCS_CXM_WWAN_TECH_STATE_IND, payload: cxm_wwan_tech_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0400, "MCS_CXM_WWAN_TECH_STATE_IND", NULL ),

  /* MCS_CXM_COEX_BOOT_PARAMS_IND, payload: cxm_coex_boot_params_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0420, "MCS_CXM_COEX_BOOT_PARAMS_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_LTE_IND, payload: cxm_coex_active_policy_lte_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0421, "MCS_CXM_COEX_ACTIVE_POLICY_LTE_IND", NULL ),

  /* MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND, payload: cxm_coex_tech_sleep_wakeup_duration_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0422, "MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND", NULL ),

  /* MCS_CXM_COEX_TECH_TX_ADV_NTC_IND, payload: cxm_coex_tech_tx_adv_ntc_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0423, "MCS_CXM_COEX_TECH_TX_ADV_NTC_IND", NULL ),

  /* MCS_CXM_COEX_WCN_TXFRMDNL_REPORT_IND, payload: cxm_coex_wcn_txfrndnl_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0424, "MCS_CXM_COEX_WCN_TXFRMDNL_REPORT_IND", NULL ),

  /* MCS_CXM_COEX_TECH_TX_FRM_DNL_REPORT_IND, payload: cxm_coex_tech_tx_frm_dnl_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0425, "MCS_CXM_COEX_TECH_TX_FRM_DNL_REPORT_IND", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_IND, payload: cxm_coex_tx_pwr_lmt_lte_cndtns_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0426, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_IND", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_BLER_IND, payload: cxm_coex_metrics_lte_bler_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0427, "MCS_CXM_COEX_METRICS_LTE_BLER_IND", NULL ),

  /* MCS_CXM_COEX_FW_COUNTERS_RESET_IND, payload: cxm_reset_fw_counters_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0429, "MCS_CXM_COEX_FW_COUNTERS_RESET_IND", NULL ),

  /* MCS_CXM_COEX_TRIGGER_WCN_PRIO_IND, payload: cxm_trigger_wcn_prio_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042a, "MCS_CXM_COEX_TRIGGER_WCN_PRIO_IND", NULL ),

  /* MCS_CXM_COEX_TECH_FRAME_TIMING_IND, payload: cxm_coex_frame_timing_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042b, "MCS_CXM_COEX_TECH_FRAME_TIMING_IND", NULL ),

  /* MCS_CXM_COEX_CONFIG_PARAMS_IND, payload: cxm_config_params_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042c, "MCS_CXM_COEX_CONFIG_PARAMS_IND", NULL ),

  /* MCS_CXM_COEX_TECH_ACTIVITY_ADV_NTC_IND, payload: cxm_coex_tech_activity_adv_ntc_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042d, "MCS_CXM_COEX_TECH_ACTIVITY_ADV_NTC_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_TDSCDMA_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0433, "MCS_CXM_COEX_ACTIVE_POLICY_TDSCDMA_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM1_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0434, "MCS_CXM_COEX_ACTIVE_POLICY_GSM1_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM2_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0435, "MCS_CXM_COEX_ACTIVE_POLICY_GSM2_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM3_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0436, "MCS_CXM_COEX_ACTIVE_POLICY_GSM3_IND", NULL ),

  /* MCS_CXM_COEX_HIGH_PRIORITY_IND, payload: cxm_coex_high_prio_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0437, "MCS_CXM_COEX_HIGH_PRIORITY_IND", NULL ),

  /* MCS_CXM_COEX_LOG_ASYNC_IND, payload: cxm_coex_log_async_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0438, "MCS_CXM_COEX_LOG_ASYNC_IND", NULL ),

  /* MCS_CXM_STX_SET_POWER_IND, payload: cxm_stx_set_pwr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0440, "MCS_CXM_STX_SET_POWER_IND", NULL ),

  /* MCS_CXM_STX_SET_TX_STATE_IND, payload: cxm_stx_tech_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0441, "MCS_CXM_STX_SET_TX_STATE_IND", NULL ),

  /* MCS_CXM_STX_CLR_TX_STATE_IND, payload: cxm_stx_tech_txoff_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0442, "MCS_CXM_STX_CLR_TX_STATE_IND", NULL ),

  /* MCS_CXM_STX_SET_TX_STATE_WITH_CHS_IND, payload: cxm_stx_tech_state_chs_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0443, "MCS_CXM_STX_SET_TX_STATE_WITH_CHS_IND", NULL ),

  /* MCS_CXM_COEX_POWER_IND, payload: cxm_coex_power_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0444, "MCS_CXM_COEX_POWER_IND", NULL ),

  /* MCS_CXM_SET_ACTIVITY_TIMELINE_IND, payload: cxm_activity_timeline_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0445, "MCS_CXM_SET_ACTIVITY_TIMELINE_IND", NULL ),

  /* MCS_CXM_INACTIVITY_CLIENT_REGISTRATION_IND, payload: cxm_inactivity_client_reg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0446, "MCS_CXM_INACTIVITY_CLIENT_REGISTRATION_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM1_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0447, "MCS_CXM_FREQID_LIST_GSM1_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM2_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0448, "MCS_CXM_FREQID_LIST_GSM2_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_WCDMA_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0449, "MCS_CXM_FREQID_LIST_WCDMA_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_ONEX_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0450, "MCS_CXM_FREQID_LIST_ONEX_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_HDR_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0451, "MCS_CXM_FREQID_LIST_HDR_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_TDSCDMA_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0452, "MCS_CXM_FREQID_LIST_TDSCDMA_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_LTE_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0453, "MCS_CXM_FREQID_LIST_LTE_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM3_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0454, "MCS_CXM_FREQID_LIST_GSM3_IND", NULL ),

  /* MCS_CXM_REPORT_INACTIVITY_INFO_ONEX_IND, payload: cxm_inactivity_report_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0455, "MCS_CXM_REPORT_INACTIVITY_INFO_ONEX_IND", NULL ),

  /* MCS_CXM_REPORT_INACTIVITY_INFO_HDR_IND, payload: cxm_inactivity_report_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0456, "MCS_CXM_REPORT_INACTIVITY_INFO_HDR_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM1_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0457, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM1_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM2_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0458, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM2_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM3_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0459, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM3_IND", NULL ),

  /* MCS_CXM_WWCOEX_STATE_UPDATE_IND, payload: cxm_wwcoex_state_update_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0460, "MCS_CXM_WWCOEX_STATE_UPDATE_IND", NULL ),

  /* MCS_CXM_SAR_SET_DSI_IND, payload: cxm_sar_set_dsi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0461, "MCS_CXM_SAR_SET_DSI_IND", NULL ),

  /* MCS_CXM_CHAIN_OWNER_UPDATE_IND, payload: cxm_chain_owner_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0463, "MCS_CXM_CHAIN_OWNER_UPDATE_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_FREQ_IND, payload: cxm_coex_ba_freq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0464, "MCS_CXM_BAND_AVOID_FREQ_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_PWR_IND, payload: cxm_coex_ba_pwr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0465, "MCS_CXM_BAND_AVOID_PWR_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM1_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0467, "MCS_CXM_BAND_AVOID_BLIST_GSM1_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM2_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0468, "MCS_CXM_BAND_AVOID_BLIST_GSM2_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM3_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0469, "MCS_CXM_BAND_AVOID_BLIST_GSM3_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_LTE_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0470, "MCS_CXM_BAND_AVOID_BLIST_LTE_IND", NULL ),

  /* MCS_CXM_SET_SLOT_ACTIVITY_TL_IND, payload: cxm_timing_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0471, "MCS_CXM_SET_SLOT_ACTIVITY_TL_IND", NULL ),

  /* MCS_CFCM_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0000, "MCS_CFCM_LOOPBACK_SPR", NULL ),

  /* MCS_CFCM_CLIENT_REGISTER_REQ, payload: cfcm_reg_req_msg_type_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0200, "MCS_CFCM_CLIENT_REGISTER_REQ", NULL ),

  /* MCS_CFCM_CLIENT_DEREGISTER_REQ, payload: cfcm_dereg_req_msg_type_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0201, "MCS_CFCM_CLIENT_DEREGISTER_REQ", NULL ),

  /* MCS_CFCM_MONITOR_IND, payload: cfcm_monitor_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0402, "MCS_CFCM_MONITOR_IND", NULL ),

  /* MCS_APP_DSPSW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00100000, "MCS_APP_DSPSW_LOOPBACK_SPR", NULL ),

  /* MCS_APP_DSPSW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00100001, "MCS_APP_DSPSW_LOOPBACK_REPLY_SPR", NULL ),

  /* MCS_APP_DSPSW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00100002, "MCS_APP_DSPSW_THREAD_READY_SPR", NULL ),

  /* MCS_APP_DSPSW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00100003, "MCS_APP_DSPSW_THREAD_KILL_SPR", NULL ),

  /* MCS_APP_DSPSW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100101, "MCS_APP_DSPSW_SPAWN_THREADS_CMD", NULL ),

  /* MCS_APP_DSPSW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100102, "MCS_APP_DSPSW_SYNC_CMD", NULL ),

  /* MCS_APP_DSPSW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100301, "MCS_APP_DSPSW_SPAWN_THREADS_RSP", NULL ),

  /* MCS_APP_DSPSW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100302, "MCS_APP_DSPSW_SYNC_RSP", NULL ),

  /* MCS_APP_DSPFW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00110000, "MCS_APP_DSPFW_LOOPBACK_SPR", NULL ),

  /* MCS_APP_DSPFW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00110001, "MCS_APP_DSPFW_LOOPBACK_REPLY_SPR", NULL ),

  /* MCS_APP_DSPFW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00110002, "MCS_APP_DSPFW_THREAD_READY_SPR", NULL ),

  /* MCS_APP_DSPFW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00110003, "MCS_APP_DSPFW_THREAD_KILL_SPR", NULL ),

  /* MCS_APP_DSPFW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110101, "MCS_APP_DSPFW_SPAWN_THREADS_CMD", NULL ),

  /* MCS_APP_DSPFW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110102, "MCS_APP_DSPFW_SYNC_CMD", NULL ),

  /* MCS_APP_DSPFW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110301, "MCS_APP_DSPFW_SPAWN_THREADS_RSP", NULL ),

  /* MCS_APP_DSPFW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110302, "MCS_APP_DSPFW_SYNC_RSP", NULL ),

  /* LTE_APP_DSPSW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0000, "LTE_APP_DSPSW_LOOPBACK_SPR", NULL ),

  /* LTE_APP_DSPSW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0001, "LTE_APP_DSPSW_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_APP_DSPSW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0002, "LTE_APP_DSPSW_THREAD_READY_SPR", NULL ),

  /* LTE_APP_DSPSW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0003, "LTE_APP_DSPSW_THREAD_KILL_SPR", NULL ),

  /* LTE_APP_DSPSW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0101, "LTE_APP_DSPSW_SPAWN_THREADS_CMD", NULL ),

  /* LTE_APP_DSPSW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0102, "LTE_APP_DSPSW_SYNC_CMD", NULL ),

  /* LTE_APP_DSPSW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0301, "LTE_APP_DSPSW_SPAWN_THREADS_RSP", NULL ),

  /* LTE_APP_DSPSW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0302, "LTE_APP_DSPSW_SYNC_RSP", NULL ),

  /* LTE_APP_DSPFW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0000, "LTE_APP_DSPFW_LOOPBACK_SPR", NULL ),

  /* LTE_APP_DSPFW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0001, "LTE_APP_DSPFW_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_APP_DSPFW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0002, "LTE_APP_DSPFW_THREAD_READY_SPR", NULL ),

  /* LTE_APP_DSPFW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0003, "LTE_APP_DSPFW_THREAD_KILL_SPR", NULL ),

  /* LTE_APP_DSPFW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0101, "LTE_APP_DSPFW_SPAWN_THREADS_CMD", NULL ),

  /* LTE_APP_DSPFW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0102, "LTE_APP_DSPFW_SYNC_CMD", NULL ),

  /* LTE_APP_DSPFW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0301, "LTE_APP_DSPFW_SPAWN_THREADS_RSP", NULL ),

  /* LTE_APP_DSPFW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0302, "LTE_APP_DSPFW_SYNC_RSP", NULL ),

  /* GPS_PGI_INIT_COMPLETE_CMD, payload: gnss_PgiInitComplete */
  MSGR_UMID_TABLE_ENTRY ( 0x0a010100, "GPS_PGI_INIT_COMPLETE_CMD", NULL ),

  /* GPS_PGI_INIT_COMPLETE_RSP, payload: gnss_PgiInitComplete */
  MSGR_UMID_TABLE_ENTRY ( 0x0a010301, "GPS_PGI_INIT_COMPLETE_RSP", NULL ),

  /* UTILS_CFM_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010000, "UTILS_CFM_LOOPBACK_SPR", NULL ),

  /* UTILS_CFM_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010001, "UTILS_CFM_LOOPBACK_REPLY_SPR", NULL ),

  /* UTILS_CFM_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x11010002, "UTILS_CFM_THREAD_READY_SPR", NULL ),

  /* UTILS_CFM_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x11010003, "UTILS_CFM_THREAD_KILL_SPR", NULL ),

  /* UTILS_CFM_FC_CMD, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010100, "UTILS_CFM_FC_CMD", NULL ),

  /* UTILS_CFM_REG_IND, payload: cfm_reg_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010400, "UTILS_CFM_REG_IND", NULL ),

  /* UTILS_CFM_DEREG_IND, payload: cfm_dereg_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010401, "UTILS_CFM_DEREG_IND", NULL ),

  /* UTILS_CFM_MONITOR_IND, payload: cfm_monitor_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010402, "UTILS_CFM_MONITOR_IND", NULL ),

  /* UTILS_CFM_EXPIRE_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11011600, "UTILS_CFM_EXPIRE_TMRI", NULL ),

#endif /* ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES)) */
};

/*! @brief Table mapping TECH/MODULE to strings */
static const msgr_module_name_entry_s  msgr_module_name_table[] = {
#if ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES))
  /* TF_MSCGEN & TF_UMID_NAMES are not defined, so no definitions */
  { 0x0000, "" },
#else
  /* MCS_MSGR */
  { 0x0001, "MSGR_MCS_MSGR" },

  /* MCS_APPMGR */
  { 0x0002, "MSGR_MCS_APPMGR" },

  /* MCS_FWS */
  { 0x0007, "MSGR_MCS_FWS" },

  /* MCS_CXM */
  { 0x000b, "MSGR_MCS_CXM" },

  /* MCS_CFCM */
  { 0x000c, "MSGR_MCS_CFCM" },

  /* MCS_APP_DSPSW */
  { 0x0010, "MSGR_MCS_APP_DSPSW" },

  /* MCS_APP_DSPFW */
  { 0x0011, "MSGR_MCS_APP_DSPFW" },

  /* LTE_APP_DSPSW */
  { 0x041e, "MSGR_LTE_APP_DSPSW" },

  /* LTE_APP_DSPFW */
  { 0x041f, "MSGR_LTE_APP_DSPFW" },

  /* GPS_PGI */
  { 0x0a01, "MSGR_GPS_PGI" },

  /* UTILS_CFM */
  { 0x1101, "MSGR_UTILS_CFM" },

#endif /* ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES)) */
};


#define MSGR_MAX_MCS_MODULES 20
static uint16 msgr_mcs_jump_table[MSGR_MAX_MCS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MSGR (0x0001) */
  { 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0003, 0xffff, 0xffff, 0xffff, },
  /* APPMGR (0x0002) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FWS (0x0007) */
  { 0x0000, 0x0002, 0xffff, 0x0002, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CXM (0x000b) */
  { 0xffff, 0xffff, 0x0072, 0x0066, 0x0071, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CFCM (0x000c) */
  { 0x0000, 0xffff, 0x0001, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPSW (0x0010) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPFW (0x0011) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_LTE_MODULES 34
static uint16 msgr_lte_jump_table[MSGR_MAX_LTE_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPSW (0x001e) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPFW (0x001f) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0020)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_GPS_MODULES 4
static uint16 msgr_gps_jump_table[MSGR_MAX_GPS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* PGI (0x0001) */
  { 0xffff, 0x0000, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_UTILS_MODULES 4
static uint16 msgr_utils_jump_table[MSGR_MAX_UTILS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CFM (0x0001) */
  { 0x0003, 0x0000, 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_NUM_TECHS 27
static msgr_tech_jump_entry_s msgr_gen_jump_table[MSGR_NUM_TECHS] = 
{
  { (uint16*)msgr_mcs_jump_table, 20 },
  { NULL, 0 },/* UMB (no messages) */
  { NULL, 0 },/* MM (no messages) */
  { NULL, 0 },/* WMX (no messages) */
  { (uint16*)msgr_lte_jump_table, 34 },
  { NULL, 0 },/* FTM (no messages) */
  { NULL, 0 },/* RFA (no messages) */
  { NULL, 0 },/* CDMA (no messages) */
  { NULL, 0 },/* HDR (no messages) */
  { NULL, 0 },/* GERAN (no messages) */
  { (uint16*)msgr_gps_jump_table, 4 },
  { NULL, 0 },/* WCDMA (no messages) */
  { NULL, 0 },/* DS (no messages) */
  { NULL, 0 },/* ONEX (no messages) */
  { NULL, 0 },/* C2K (no messages) */
  { NULL, 0 },/* NAS (no messages) */
  { NULL, 0 },/* UIM (no messages) */
  { (uint16*)msgr_utils_jump_table, 4 },
  { NULL, 0 },/* TDSCDMA (no messages) */
  { NULL, 0 },/* WMS (no messages) */
  { NULL, 0 },/* CNE (no messages) */
  { NULL, 0 },/* IMS (no messages) */
  { NULL, 0 },/* QMI (no messages) */
  { NULL, 0 },/* ECALL (no messages) */
  { NULL, 0 },/* POLICYMAN (no messages) */
  { NULL, 0 },/* CORE (no messages) */
  { NULL, 0 },/* RFLM (no messages) */
};

#define MSGR_NUM_TOTAL_MSGS 126


/* Defines Number of Unique Combinations of Tech/Mod/Type in the system 
   when MSGR gets compiled */

#define MSGR_NUM_TECH_MOD_TYPE 32

/* Defines Number of Reg Nodes Slot Needed for Known UMIDS 
   when MSGR gets compiled */

#define MSGR_NUM_REGULAR_SLOTS 43

/* The size of the top-level jump table,
   which points to the correct tech-specific table.*/
#define MSGR_JUMP_TABLE_SIZE  (MSGR_NUM_TECHS * sizeof(msgr_tech_jump_entry_s))

/* The total size of all the tech-specific tables. For each TECH, one row for 
   each module. Each jump table is a 2D array of max module rows and num type 
   columns each of which is an uint16 */
#define MSGR_TECH_JUMP_TABLE_SIZE  (62 * MSGR_NUM_TYPES * sizeof(uint16))