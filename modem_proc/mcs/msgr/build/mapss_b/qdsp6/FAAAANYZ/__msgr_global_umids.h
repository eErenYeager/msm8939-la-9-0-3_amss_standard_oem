/*!
  @file   __msgr_global_umids.h

  @brief Auto-generated file containing UMID mapping tables

  @detail
  This file is AUTO-GENERATED.  Do not modify!
  This auto-generated header file declares the message router table mapping
  UMIDs to their string equivalent

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#ifndef MSGR_UMID_GEN
#error "__msgr_global_umids.h may only be included from msgr_umid.c"
#endif /* MSGR_UMID_GEN */

/* @brief Populate table entries based on flags set */
#ifdef TF_MSCGEN
  #ifndef TF_UMID_NAMES
    #error "Incorrect configuration - TF_UMID_NAMES must be defined"
  #else
    /* TF_MSCGEN & TF_UMID_NAMES is defined, so define complete table */
    #define MSGR_UMID_TABLE_ENTRY(hex_id, name, func_ptr) { hex_id, name, func_ptr }
  #endif
#elif defined TF_UMID_NAMES
  /* Only TF_UMID_NAMES is defined, so define UMID names only */
   #define MSGR_UMID_TABLE_ENTRY(hex_id, name, func_ptr) { hex_id, name, NULL }
#else
  /* TF_MSCGEN/TF_UMID_NAMES are not defined, so no definitions required
     This case is for final release*/
#endif

#ifdef TF_MSCGEN
#endif /* TF_MSCGEN */

/*! @brief Table mapping UMID to strings */
static const msgr_umid_name_entry_s msgr_umid_name_table[] = {
#if ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES))
  /* TF_MSCGEN/TF_UMID_NAMES are not defined, so no definitions */
  { 0x00000000, "", NULL },
#else
  /* MCS_MSGR_SNOOP_SPR, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00010001, "MCS_MSGR_SNOOP_SPR", NULL ),

  /* MCS_MSGR_ALL_MQS_BLOCKED_SPR, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00010002, "MCS_MSGR_ALL_MQS_BLOCKED_SPR", NULL ),

  /* MCS_MSGR_SMDL_INIT_INDI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00011403, "MCS_MSGR_SMDL_INIT_INDI", NULL ),

  /* MCS_APPMGR_THREAD_RUN_IND, payload: appmgr_ind_thread_run_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00020400, "MCS_APPMGR_THREAD_RUN_IND", NULL ),

  /* MCS_FWS_SHUTDOWN_SPR, payload: fws_shutdown_spr_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070000, "MCS_FWS_SHUTDOWN_SPR", NULL ),

  /* MCS_FWS_APP_CONFIG_CMD, payload: fws_app_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070100, "MCS_FWS_APP_CONFIG_CMD", NULL ),

  /* MCS_FWS_SUSPEND_CMD, payload: fws_suspend_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070101, "MCS_FWS_SUSPEND_CMD", NULL ),

  /* MCS_FWS_WAKEUP_CMD, payload: fws_wakeup_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070102, "MCS_FWS_WAKEUP_CMD", NULL ),

  /* MCS_FWS_APP_CONFIG_RSP, payload: fws_app_config_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070300, "MCS_FWS_APP_CONFIG_RSP", NULL ),

  /* MCS_FWS_SUSPEND_RSP, payload: fws_suspend_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070301, "MCS_FWS_SUSPEND_RSP", NULL ),

  /* MCS_FWS_WAKEUP_RSP, payload: fws_wakeup_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070302, "MCS_FWS_WAKEUP_RSP", NULL ),

  /* MCS_FWS_DIAG_MSG_IND, payload: fws_diag_msg_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070400, "MCS_FWS_DIAG_MSG_IND", NULL ),

  /* MCS_FWS_DIAG_LOG_IND, payload: fws_diag_log_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00070401, "MCS_FWS_DIAG_LOG_IND", NULL ),

  /* MCS_FWS_WDOG_REPORT_TMRI, payload: fws_wdog_report_tmri_t */
  MSGR_UMID_TABLE_ENTRY ( 0x00071600, "MCS_FWS_WDOG_REPORT_TMRI", NULL ),

  /* MCS_CXM_WWAN_TECH_STATE_UPDATE_REQ, payload: cxm_wwan_tech_state_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0201, "MCS_CXM_WWAN_TECH_STATE_UPDATE_REQ", NULL ),

  /* MCS_CXM_COEX_BOOT_PARAMS_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0220, "MCS_CXM_COEX_BOOT_PARAMS_REQ", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_LTE_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0221, "MCS_CXM_COEX_ACTIVE_POLICY_LTE_REQ", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_REQ, payload: cxm_coex_generic_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0226, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_BLER_REQ, payload: cxm_coex_metrics_lte_bler_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0227, "MCS_CXM_COEX_METRICS_LTE_BLER_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_SINR_REQ, payload: cxm_coex_metrics_lte_sinr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0228, "MCS_CXM_COEX_METRICS_LTE_SINR_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_TDSCDMA_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b022e, "MCS_CXM_COEX_METRICS_TDSCDMA_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM1_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b022f, "MCS_CXM_COEX_METRICS_GSM1_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM2_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0230, "MCS_CXM_COEX_METRICS_GSM2_REQ", NULL ),

  /* MCS_CXM_COEX_METRICS_GSM3_REQ, payload: cxm_coex_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0231, "MCS_CXM_COEX_METRICS_GSM3_REQ", NULL ),

  /* MCS_CXM_DESENSE_IND_REQ, payload: cxm_coex_desense_ind_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0272, "MCS_CXM_DESENSE_IND_REQ", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_RSP, payload: cxm_coex_tx_pwr_lmt_lte_cndtns_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0326, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_RSP", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_SINR_RSP, payload: cxm_coex_metrics_lte_sinr_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0328, "MCS_CXM_COEX_METRICS_LTE_SINR_RSP", NULL ),

  /* MCS_CXM_COEX_TECH_METRICS_RSP, payload: cxm_coex_metrics_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0332, "MCS_CXM_COEX_TECH_METRICS_RSP", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_RSP, payload: cxm_coex_ba_blist_res_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0366, "MCS_CXM_BAND_AVOID_BLIST_RSP", NULL ),

  /* MCS_CXM_WWAN_TECH_STATE_IND, payload: cxm_wwan_tech_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0400, "MCS_CXM_WWAN_TECH_STATE_IND", NULL ),

  /* MCS_CXM_COEX_BOOT_PARAMS_IND, payload: cxm_coex_boot_params_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0420, "MCS_CXM_COEX_BOOT_PARAMS_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_LTE_IND, payload: cxm_coex_active_policy_lte_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0421, "MCS_CXM_COEX_ACTIVE_POLICY_LTE_IND", NULL ),

  /* MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND, payload: cxm_coex_tech_sleep_wakeup_duration_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0422, "MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND", NULL ),

  /* MCS_CXM_COEX_TECH_TX_ADV_NTC_IND, payload: cxm_coex_tech_tx_adv_ntc_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0423, "MCS_CXM_COEX_TECH_TX_ADV_NTC_IND", NULL ),

  /* MCS_CXM_COEX_WCN_TXFRMDNL_REPORT_IND, payload: cxm_coex_wcn_txfrndnl_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0424, "MCS_CXM_COEX_WCN_TXFRMDNL_REPORT_IND", NULL ),

  /* MCS_CXM_COEX_TECH_TX_FRM_DNL_REPORT_IND, payload: cxm_coex_tech_tx_frm_dnl_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0425, "MCS_CXM_COEX_TECH_TX_FRM_DNL_REPORT_IND", NULL ),

  /* MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_IND, payload: cxm_coex_tx_pwr_lmt_lte_cndtns_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0426, "MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_IND", NULL ),

  /* MCS_CXM_COEX_METRICS_LTE_BLER_IND, payload: cxm_coex_metrics_lte_bler_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0427, "MCS_CXM_COEX_METRICS_LTE_BLER_IND", NULL ),

  /* MCS_CXM_COEX_FW_COUNTERS_RESET_IND, payload: cxm_reset_fw_counters_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0429, "MCS_CXM_COEX_FW_COUNTERS_RESET_IND", NULL ),

  /* MCS_CXM_COEX_TRIGGER_WCN_PRIO_IND, payload: cxm_trigger_wcn_prio_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042a, "MCS_CXM_COEX_TRIGGER_WCN_PRIO_IND", NULL ),

  /* MCS_CXM_COEX_TECH_FRAME_TIMING_IND, payload: cxm_coex_frame_timing_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042b, "MCS_CXM_COEX_TECH_FRAME_TIMING_IND", NULL ),

  /* MCS_CXM_COEX_CONFIG_PARAMS_IND, payload: cxm_config_params_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042c, "MCS_CXM_COEX_CONFIG_PARAMS_IND", NULL ),

  /* MCS_CXM_COEX_TECH_ACTIVITY_ADV_NTC_IND, payload: cxm_coex_tech_activity_adv_ntc_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b042d, "MCS_CXM_COEX_TECH_ACTIVITY_ADV_NTC_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_TDSCDMA_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0433, "MCS_CXM_COEX_ACTIVE_POLICY_TDSCDMA_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM1_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0434, "MCS_CXM_COEX_ACTIVE_POLICY_GSM1_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM2_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0435, "MCS_CXM_COEX_ACTIVE_POLICY_GSM2_IND", NULL ),

  /* MCS_CXM_COEX_ACTIVE_POLICY_GSM3_IND, payload: cxm_coex_active_policy_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0436, "MCS_CXM_COEX_ACTIVE_POLICY_GSM3_IND", NULL ),

  /* MCS_CXM_COEX_HIGH_PRIORITY_IND, payload: cxm_coex_high_prio_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0437, "MCS_CXM_COEX_HIGH_PRIORITY_IND", NULL ),

  /* MCS_CXM_COEX_LOG_ASYNC_IND, payload: cxm_coex_log_async_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0438, "MCS_CXM_COEX_LOG_ASYNC_IND", NULL ),

  /* MCS_CXM_STX_SET_POWER_IND, payload: cxm_stx_set_pwr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0440, "MCS_CXM_STX_SET_POWER_IND", NULL ),

  /* MCS_CXM_STX_SET_TX_STATE_IND, payload: cxm_stx_tech_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0441, "MCS_CXM_STX_SET_TX_STATE_IND", NULL ),

  /* MCS_CXM_STX_CLR_TX_STATE_IND, payload: cxm_stx_tech_txoff_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0442, "MCS_CXM_STX_CLR_TX_STATE_IND", NULL ),

  /* MCS_CXM_STX_SET_TX_STATE_WITH_CHS_IND, payload: cxm_stx_tech_state_chs_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0443, "MCS_CXM_STX_SET_TX_STATE_WITH_CHS_IND", NULL ),

  /* MCS_CXM_COEX_POWER_IND, payload: cxm_coex_power_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0444, "MCS_CXM_COEX_POWER_IND", NULL ),

  /* MCS_CXM_SET_ACTIVITY_TIMELINE_IND, payload: cxm_activity_timeline_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0445, "MCS_CXM_SET_ACTIVITY_TIMELINE_IND", NULL ),

  /* MCS_CXM_INACTIVITY_CLIENT_REGISTRATION_IND, payload: cxm_inactivity_client_reg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0446, "MCS_CXM_INACTIVITY_CLIENT_REGISTRATION_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM1_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0447, "MCS_CXM_FREQID_LIST_GSM1_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM2_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0448, "MCS_CXM_FREQID_LIST_GSM2_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_WCDMA_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0449, "MCS_CXM_FREQID_LIST_WCDMA_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_ONEX_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0450, "MCS_CXM_FREQID_LIST_ONEX_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_HDR_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0451, "MCS_CXM_FREQID_LIST_HDR_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_TDSCDMA_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0452, "MCS_CXM_FREQID_LIST_TDSCDMA_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_LTE_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0453, "MCS_CXM_FREQID_LIST_LTE_IND", NULL ),

  /* MCS_CXM_FREQID_LIST_GSM3_IND, payload: cxm_freqid_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0454, "MCS_CXM_FREQID_LIST_GSM3_IND", NULL ),

  /* MCS_CXM_REPORT_INACTIVITY_INFO_ONEX_IND, payload: cxm_inactivity_report_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0455, "MCS_CXM_REPORT_INACTIVITY_INFO_ONEX_IND", NULL ),

  /* MCS_CXM_REPORT_INACTIVITY_INFO_HDR_IND, payload: cxm_inactivity_report_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0456, "MCS_CXM_REPORT_INACTIVITY_INFO_HDR_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM1_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0457, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM1_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM2_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0458, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM2_IND", NULL ),

  /* MCS_CXM_REQUEST_ACTIVITY_INFO_GSM3_IND, payload: cxm_request_activity_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0459, "MCS_CXM_REQUEST_ACTIVITY_INFO_GSM3_IND", NULL ),

  /* MCS_CXM_WWCOEX_STATE_UPDATE_IND, payload: cxm_wwcoex_state_update_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0460, "MCS_CXM_WWCOEX_STATE_UPDATE_IND", NULL ),

  /* MCS_CXM_SAR_SET_DSI_IND, payload: cxm_sar_set_dsi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0461, "MCS_CXM_SAR_SET_DSI_IND", NULL ),

  /* MCS_CXM_CHAIN_OWNER_UPDATE_IND, payload: cxm_chain_owner_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0463, "MCS_CXM_CHAIN_OWNER_UPDATE_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_FREQ_IND, payload: cxm_coex_ba_freq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0464, "MCS_CXM_BAND_AVOID_FREQ_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_PWR_IND, payload: cxm_coex_ba_pwr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0465, "MCS_CXM_BAND_AVOID_PWR_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM1_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0467, "MCS_CXM_BAND_AVOID_BLIST_GSM1_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM2_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0468, "MCS_CXM_BAND_AVOID_BLIST_GSM2_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_GSM3_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0469, "MCS_CXM_BAND_AVOID_BLIST_GSM3_IND", NULL ),

  /* MCS_CXM_BAND_AVOID_BLIST_LTE_IND, payload: cxm_coex_ba_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0470, "MCS_CXM_BAND_AVOID_BLIST_LTE_IND", NULL ),

  /* MCS_CXM_SET_SLOT_ACTIVITY_TL_IND, payload: cxm_timing_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000b0471, "MCS_CXM_SET_SLOT_ACTIVITY_TL_IND", NULL ),

  /* MCS_CFCM_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0000, "MCS_CFCM_LOOPBACK_SPR", NULL ),

  /* MCS_CFCM_CLIENT_REGISTER_REQ, payload: cfcm_reg_req_msg_type_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0200, "MCS_CFCM_CLIENT_REGISTER_REQ", NULL ),

  /* MCS_CFCM_CLIENT_DEREGISTER_REQ, payload: cfcm_dereg_req_msg_type_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0201, "MCS_CFCM_CLIENT_DEREGISTER_REQ", NULL ),

  /* MCS_CFCM_MONITOR_IND, payload: cfcm_monitor_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x000c0402, "MCS_CFCM_MONITOR_IND", NULL ),

  /* MCS_APP_DSPSW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00100000, "MCS_APP_DSPSW_LOOPBACK_SPR", NULL ),

  /* MCS_APP_DSPSW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00100001, "MCS_APP_DSPSW_LOOPBACK_REPLY_SPR", NULL ),

  /* MCS_APP_DSPSW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00100002, "MCS_APP_DSPSW_THREAD_READY_SPR", NULL ),

  /* MCS_APP_DSPSW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00100003, "MCS_APP_DSPSW_THREAD_KILL_SPR", NULL ),

  /* MCS_APP_DSPSW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100101, "MCS_APP_DSPSW_SPAWN_THREADS_CMD", NULL ),

  /* MCS_APP_DSPSW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100102, "MCS_APP_DSPSW_SYNC_CMD", NULL ),

  /* MCS_APP_DSPSW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100301, "MCS_APP_DSPSW_SPAWN_THREADS_RSP", NULL ),

  /* MCS_APP_DSPSW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00100302, "MCS_APP_DSPSW_SYNC_RSP", NULL ),

  /* MCS_APP_DSPFW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00110000, "MCS_APP_DSPFW_LOOPBACK_SPR", NULL ),

  /* MCS_APP_DSPFW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x00110001, "MCS_APP_DSPFW_LOOPBACK_REPLY_SPR", NULL ),

  /* MCS_APP_DSPFW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00110002, "MCS_APP_DSPFW_THREAD_READY_SPR", NULL ),

  /* MCS_APP_DSPFW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x00110003, "MCS_APP_DSPFW_THREAD_KILL_SPR", NULL ),

  /* MCS_APP_DSPFW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110101, "MCS_APP_DSPFW_SPAWN_THREADS_CMD", NULL ),

  /* MCS_APP_DSPFW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110102, "MCS_APP_DSPFW_SYNC_CMD", NULL ),

  /* MCS_APP_DSPFW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110301, "MCS_APP_DSPFW_SPAWN_THREADS_RSP", NULL ),

  /* MCS_APP_DSPFW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x00110302, "MCS_APP_DSPFW_SYNC_RSP", NULL ),

  /* MM_CM_AC_EMERGENCY_ENTER_REQ, payload: \cm_ac_emergency_enter_req_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010217, "MM_CM_AC_EMERGENCY_ENTER_REQ", NULL ),

  /* MM_CM_AC_EMERGENCY_EXIT_REQ, payload: \cm_ac_emergency_exit_req_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010220, "MM_CM_AC_EMERGENCY_EXIT_REQ", NULL ),

  /* MM_CM_1XCSFB_ABORT_RSP, payload: \cm_1xCSFB_abort_rsp */
  MSGR_UMID_TABLE_ENTRY ( 0x02010312, "MM_CM_1XCSFB_ABORT_RSP", NULL ),

  /* MM_CM_1XCSFB_CALL_RSP, payload: \cm_1xCSFB_call_rsp */
  MSGR_UMID_TABLE_ENTRY ( 0x02010313, "MM_CM_1XCSFB_CALL_RSP", NULL ),

  /* MM_CM_SRVCC_CONTEXT_RSP, payload: cm_srvcc_call_context_rsp_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010322, "MM_CM_SRVCC_CONTEXT_RSP", NULL ),

  /* MM_CM_SUBSC_CHGND_RSP, payload: cm_subsc_chgnd_rsp_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010325, "MM_CM_SUBSC_CHGND_RSP", NULL ),

  /* MM_CM_ACT_DEFAULT_BEARER_CONTEXT_REQUEST_IND, payload: \cm_act_default_bearer_context_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010401, "MM_CM_ACT_DEFAULT_BEARER_CONTEXT_REQUEST_IND", NULL ),

  /* MM_CM_ACT_DEDICATED_BEARER_CONTEXT_REQUEST_IND, payload: \cm_act_dedicated_bearer_context_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010402, "MM_CM_ACT_DEDICATED_BEARER_CONTEXT_REQUEST_IND", NULL ),

  /* MM_CM_DEACT_BEARER_CONTEXT_REQUEST_IND, payload: \cm_deact_bearer_context_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010403, "MM_CM_DEACT_BEARER_CONTEXT_REQUEST_IND", NULL ),

  /* MM_CM_BEARER_RESOURCE_ALLOC_REJECT_IND, payload: \cm_bearer_resource_alloc_reject_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010404, "MM_CM_BEARER_RESOURCE_ALLOC_REJECT_IND", NULL ),

  /* MM_CM_MODIFY_BEARER_CONTEXT_REQUEST_IND, payload: \cm_modify_bearer_context_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010405, "MM_CM_MODIFY_BEARER_CONTEXT_REQUEST_IND", NULL ),

  /* MM_CM_PDN_CONNECTIVITY_REJECT_IND, payload: \cm_pdn_connectivity_reject_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010406, "MM_CM_PDN_CONNECTIVITY_REJECT_IND", NULL ),

  /* MM_CM_PDN_DISCONNECT_REJECT_IND, payload: \cm_pdn_disconnect_reject_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010407, "MM_CM_PDN_DISCONNECT_REJECT_IND", NULL ),

  /* MM_CM_ACT_DRB_RELEASED_IND, payload: cm_drb_released_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010408, "MM_CM_ACT_DRB_RELEASED_IND", NULL ),

  /* MM_CM_DRB_SETUP_IND, payload: cm_drb_setup_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010409, "MM_CM_DRB_SETUP_IND", NULL ),

  /* MM_CM_DRB_REESTABLISH_FAILED_IND, payload: \cm_drb_reestablish_failed_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040a, "MM_CM_DRB_REESTABLISH_FAILED_IND", NULL ),

  /* MM_CM_PDN_CONNECTIVITY_FAILURE_IND, payload: \cm_pdn_connectivity_failure_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040b, "MM_CM_PDN_CONNECTIVITY_FAILURE_IND", NULL ),

  /* MM_CM_BEARER_RESOURCE_ALLOC_FAILURE_IND, payload: \cm_bearer_resource_alloc_failure_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040c, "MM_CM_BEARER_RESOURCE_ALLOC_FAILURE_IND", NULL ),

  /* MM_CM_DRB_REESTABLISH_REJECT_IND, payload: \cm_drb_reestablish_reject_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040d, "MM_CM_DRB_REESTABLISH_REJECT_IND", NULL ),

  /* MM_CM_GET_PDN_CONNECTIVITY_REQUEST_IND, payload: \cm_get_pdn_connectivity_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040e, "MM_CM_GET_PDN_CONNECTIVITY_REQUEST_IND", NULL ),

  /* MM_CM_BEARER_CONTEXT_MODIFY_REJECT_IND, payload: \cm_bearer_context_modify_reject_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0201040f, "MM_CM_BEARER_CONTEXT_MODIFY_REJECT_IND", NULL ),

  /* MM_CM_PDN_CONNECTIVITY_PACKED_REQUEST_IND, payload: \cm_pdn_connectivity_packed_request_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010410, "MM_CM_PDN_CONNECTIVITY_PACKED_REQUEST_IND", NULL ),

  /* MM_CM_ESM_NOTIFICATION_IND, payload: \cm_esm_notification_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010411, "MM_CM_ESM_NOTIFICATION_IND", NULL ),

  /* MM_CM_CFCM_MONITOR_THERMAL_PA_EM_IND, payload: \cm_cfcm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x02010414, "MM_CM_CFCM_MONITOR_THERMAL_PA_EM_IND", NULL ),

  /* MM_CM_AC_1XPPP_CLEAR_START_IND, payload: \cm_ac_info_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010415, "MM_CM_AC_1XPPP_CLEAR_START_IND", NULL ),

  /* MM_CM_AC_1XPPP_CLEAR_END_IND, payload: \cm_ac_info_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010416, "MM_CM_AC_1XPPP_CLEAR_END_IND", NULL ),

  /* MM_CM_AC_EMERGENCY_ENTER_IND, payload: \cm_ac_info_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010418, "MM_CM_AC_EMERGENCY_ENTER_IND", NULL ),

  /* MM_CM_AC_EMERGENCY_READY_IND, payload: \cm_ac_info_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010419, "MM_CM_AC_EMERGENCY_READY_IND", NULL ),

  /* MM_CM_AC_EMERGENCY_EXIT_IND, payload: \cm_ac_info_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010421, "MM_CM_AC_EMERGENCY_EXIT_IND", NULL ),

  /* MM_CM_SRVCC_HO_COMPLETE_IND, payload: cm_mid_srvcc_ho_comp_list_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010423, "MM_CM_SRVCC_HO_COMPLETE_IND", NULL ),

  /* MM_CM_SUBSC_CHGND_IND, payload: cm_subsc_chgnd_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010424, "MM_CM_SUBSC_CHGND_IND", NULL ),

  /* MM_CM_IMS_EMERG_CAP_SUPP_IND, payload: \cm_ims_emerg_cap_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010428, "MM_CM_IMS_EMERG_CAP_SUPP_IND", NULL ),

  /* MM_CM_CSG_SEARCH_TRIGGER_IND, payload: \cm_csg_srch_trigger_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010429, "MM_CM_CSG_SEARCH_TRIGGER_IND", NULL ),

  /* MM_CM_EMERGENCY_MODE_STATUS_IND, payload: \cm_emerg_mode_status_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010430, "MM_CM_EMERGENCY_MODE_STATUS_IND", NULL ),

  /* MM_CM_VOLTE_STATUS_IND, payload: \cm_volte_call_status_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010431, "MM_CM_VOLTE_STATUS_IND", NULL ),

  /* MM_CM_VOICE_CALL_STATUS_IND, payload: \cm_voice_call_status_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010432, "MM_CM_VOICE_CALL_STATUS_IND", NULL ),

  /* MM_CM_AUDIO_RELEASE_IND, payload: \cm_audio_session_rel_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010433, "MM_CM_AUDIO_RELEASE_IND", NULL ),

  /* MM_CM_LTE_ACTIVE_STATUS_IND, payload: \cm_lte_active_status_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010434, "MM_CM_LTE_ACTIVE_STATUS_IND", NULL ),

  /* MM_CM_MANUAL_SCAN_FAIL_IND, payload: \cm_manual_scan_fail_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010435, "MM_CM_MANUAL_SCAN_FAIL_IND", NULL ),

  /* MM_CM_TRM_PRIORITY_IND, payload: \cm_trm_priority_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010436, "MM_CM_TRM_PRIORITY_IND", NULL ),

  /* MM_CM_TAU_COMPLETE_IND, payload: \cm_tau_complete_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02010437, "MM_CM_TAU_COMPLETE_IND", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_GET_REQ, payload: cm_domain_sel_domain_get_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020201, "MM_DOM_SEL_DOMAIN_SELECTED_GET_REQ", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_LTE_IMS_PREF_GET_REQ, payload: cm_domain_sel_ims_pref_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020205, "MM_DOM_SEL_DOMAIN_SELECTED_LTE_IMS_PREF_GET_REQ", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_GET_EXT_REQ, payload: cm_domain_sel_get_ims_pref_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020210, "MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_GET_EXT_REQ", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_GET_RSP, payload: cm_domain_sel_domain_get_rsp_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020302, "MM_DOM_SEL_DOMAIN_SELECTED_GET_RSP", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_LTE_IMS_PREF_GET_RSP, payload: cm_domain_sel_ims_pref_rsp_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020306, "MM_DOM_SEL_DOMAIN_SELECTED_LTE_IMS_PREF_GET_RSP", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_GET_EXT_RSP, payload: cm_domain_sel_ims_pref_rsp_msg_type_ext */
  MSGR_UMID_TABLE_ENTRY ( 0x02020311, "MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_GET_EXT_RSP", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_CHANGED_IND, payload: cm_domain_sel_domain_chg_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020403, "MM_DOM_SEL_DOMAIN_SELECTED_CHANGED_IND", NULL ),

  /* MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_CHANGED_IND, payload: cm_domain_sel_ims_pref_chg_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020404, "MM_DOM_SEL_DOMAIN_SELECTED_IMS_PREF_CHANGED_IND", NULL ),

  /* MM_DOM_SEL_UPDATE_IMS_REG_STATUS_IND, payload: cm_domain_sel_update_ims_reg_status_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02020407, "MM_DOM_SEL_UPDATE_IMS_REG_STATUS_IND", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_GET_REQ, payload: cm_domain_sel_get_config_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02030208, "MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_GET_REQ", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_GET_REQ, payload: cm_domain_sel_get_config_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02030209, "MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_GET_REQ", NULL ),

  /* MM_DOM_SEL_CONFIG_UPDATE_LTE_VOICE_DOMAIN_PREF_REQ, payload: cm_domain_sel_config_update_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203020c, "MM_DOM_SEL_CONFIG_UPDATE_LTE_VOICE_DOMAIN_PREF_REQ", NULL ),

  /* MM_DOM_SEL_CONFIG_UPDATE_LTE_SMS_DOMAIN_PREF_REQ, payload: cm_domain_sel_config_update_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203020d, "MM_DOM_SEL_CONFIG_UPDATE_LTE_SMS_DOMAIN_PREF_REQ", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_GET_RSP, payload: cm_domain_sel_get_config_rsp_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203030a, "MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_GET_RSP", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_GET_RSP, payload: cm_domain_sel_get_config_rsp_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203030b, "MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_GET_RSP", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_CHANGED_IND, payload: cm_domain_sel_config_chg_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203040e, "MM_DOM_SEL_CONFIG_LTE_VOICE_DOMAIN_PREF_CHANGED_IND", NULL ),

  /* MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_CHANGED_IND, payload: cm_domain_sel_config_chg_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0203040f, "MM_DOM_SEL_CONFIG_LTE_SMS_DOMAIN_PREF_CHANGED_IND", NULL ),

  /* MM_TUI_CSG_SELECTION_CMD, payload: ui_base_msgr_msg_csg_select_config_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x020a0101, "MM_TUI_CSG_SELECTION_CMD", NULL ),

  /* MM_MMOC_OFFLINE_IND, payload: mmoc_offline_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x02510401, "MM_MMOC_OFFLINE_IND", NULL ),

  /* LTE_ML1_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04010000, "LTE_ML1_LOOPBACK_SPR", NULL ),

  /* LTE_ML1_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04010001, "LTE_ML1_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_ML1_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04010002, "LTE_ML1_THREAD_READY_SPR", NULL ),

  /* LTE_ML1_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04010003, "LTE_ML1_THREAD_KILL_SPR", NULL ),

  /* LTE_ML1_SM_SCHDLR_OBJ_REQ, payload: lte_ml1_sm_idle_sch_obj_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020200, "LTE_ML1_SM_SCHDLR_OBJ_REQ", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402023a, "LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_ABORT_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402023b, "LTE_ML1_SM_IRAT_SCHED_ABORT_REQ", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402023f, "LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020401, "LTE_ML1_SM_CONN_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_FINAL_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020402, "LTE_ML1_SM_CONN_MEAS_FINAL_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_START_SWRP_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020404, "LTE_ML1_SM_CONN_MEAS_INTRA_START_SWRP_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_END_SWRP_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020405, "LTE_ML1_SM_CONN_MEAS_INTRA_END_SWRP_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_FINAL_START_SWRP_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020406, "LTE_ML1_SM_CONN_MEAS_INTRA_FINAL_START_SWRP_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_FINAL_END_SWRP_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020407, "LTE_ML1_SM_CONN_MEAS_INTRA_FINAL_END_SWRP_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_OCC_IND, payload: lte_ml1_sm_cgi_meas_occ_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020409, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_OCC_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_CFG_IND, payload: lte_ml1_sm_conn_meas_cgi_meas_cfg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040a, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_CFG_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_CSWITCH_IND, payload: lte_ml1_sm_cswitch_done */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040b, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_CSWITCH_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_ABORT_IND, payload: lte_ml1_sm_son_obj_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040c, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_ABORT_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_SI_DONE_IND, payload: lte_ml1_sm_cgi_meas_si_done_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040d, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_SI_DONE_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_IRAT_EVAL_TICK_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040e, "LTE_ML1_SM_CONN_MEAS_IRAT_EVAL_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_IRAT_FINAL_EVAL_TICK_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402040f, "LTE_ML1_SM_CONN_MEAS_IRAT_FINAL_EVAL_TICK_IND", NULL ),

  /* LTE_ML1_SM_TIME_SLAM_DONE_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020411, "LTE_ML1_SM_TIME_SLAM_DONE_IND", NULL ),

  /* LTE_ML1_SM_TTRANS_DONE_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020412, "LTE_ML1_SM_TTRANS_DONE_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_NBPBCH_DEC_IND, payload: lte_ml1_sm_conn_meas_cgi_pbch_result_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020413, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_NBPBCH_DEC_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_CDRX_OFF_IND, payload: lte_ml1_sm_conn_meas_cgi_cdrx_off_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020414, "LTE_ML1_SM_CONN_MEAS_CGI_CDRX_OFF_IND", NULL ),

  /* LTE_ML1_SM_CONN_SCHDLR_OBJ_DEREG_IND, payload: lte_ml1_sm_con_sch_obj_dereg_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020415, "LTE_ML1_SM_CONN_SCHDLR_OBJ_DEREG_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_IRAT_CGI_MEAS_ABORT_IND, payload: lte_ml1_sm_son_obj_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020417, "LTE_ML1_SM_CONN_MEAS_IRAT_CGI_MEAS_ABORT_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_NGBR_SRCH_TICK_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020418, "LTE_ML1_SM_CONN_MEAS_NGBR_SRCH_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_SRCHR_MEAS_ENV_ABORT_IND, payload: lte_ml1_sm_abort_intra_meas_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020419, "LTE_ML1_SM_CONN_SRCHR_MEAS_ENV_ABORT_IND", NULL ),

  /* LTE_ML1_SM_CONN_SRCHR_MEAS_ENV_DOWN_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402041a, "LTE_ML1_SM_CONN_SRCHR_MEAS_ENV_DOWN_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_DELAYED_NGBR_SRCH_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402041b, "LTE_ML1_SM_CONN_MEAS_DELAYED_NGBR_SRCH_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_NGBR_SRCH_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402041c, "LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_NGBR_SRCH_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_GSM_SHUTDOWN_DONE_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402041f, "LTE_ML1_SM_CONN_GSM_SHUTDOWN_DONE_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_START_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020421, "LTE_ML1_SM_CONN_MEAS_START_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_START_OBJ_DEREG_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020422, "LTE_ML1_SM_CONN_MEAS_START_OBJ_DEREG_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_MEAS_NBPBCH_STOP_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020423, "LTE_ML1_SM_CONN_MEAS_CGI_MEAS_NBPBCH_STOP_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_WAKEUP_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020424, "LTE_ML1_SM_CONN_MEAS_WAKEUP_IND", NULL ),

  /* LTE_ML1_SM_OFFLINE_GOTO_SLEEP_IND, payload: lte_ml1_sm_offline_goto_sleep_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020425, "LTE_ML1_SM_OFFLINE_GOTO_SLEEP_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_DELAYED_NGBR_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020426, "LTE_ML1_SM_CONN_MEAS_DELAYED_NGBR_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_NGBR_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020427, "LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_NGBR_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_DELAYED_SRV_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020428, "LTE_ML1_SM_CONN_MEAS_DELAYED_SRV_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_SRV_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020429, "LTE_ML1_SM_CONN_MEAS_FINAL_DELAYED_SRV_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_DISABLE_FOR_OFFLINE_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402042a, "LTE_ML1_SM_CONN_MEAS_DISABLE_FOR_OFFLINE_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_FINAL_DISABLE_FOR_OFFLINE_MEAS_TICK_IND, payload: lte_ml1_sm_conn_meas_final_tick_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402042b, "LTE_ML1_SM_CONN_MEAS_FINAL_DISABLE_FOR_OFFLINE_MEAS_TICK_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_START_SWRP_ABORT_IND, payload: lte_ml1_sm_obj_abort_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020430, "LTE_ML1_SM_CONN_MEAS_INTRA_START_SWRP_ABORT_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_INTRA_END_SWRP_ABORT_IND, payload: lte_ml1_sm_obj_abort_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020431, "LTE_ML1_SM_CONN_MEAS_INTRA_END_SWRP_ABORT_IND", NULL ),

  /* LTE_ML1_SM_ACQ_FSCAN_OBJ_SCHEDULE_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020432, "LTE_ML1_SM_ACQ_FSCAN_OBJ_SCHEDULE_IND", NULL ),

  /* LTE_ML1_SM_ACQ_FSCAN_OBJ_ABORT_IND, payload: lte_ml1_sm_obj_abort_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020433, "LTE_ML1_SM_ACQ_FSCAN_OBJ_ABORT_IND", NULL ),

  /* LTE_ML1_SM_MCVS_Q6_DONE_IND, payload: lte_ml1_sm_irat_meas_mcvs_q6_done_ind */
  MSGR_UMID_TABLE_ENTRY ( 0x04020434, "LTE_ML1_SM_MCVS_Q6_DONE_IND", NULL ),

  /* LTE_ML1_SM_CON_AUTO_PBCH_START_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020435, "LTE_ML1_SM_CON_AUTO_PBCH_START_IND", NULL ),

  /* LTE_ML1_SM_CON_AUTO_PBCH_ABORT_IND, payload: lte_ml1_sm_obj_abort_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020436, "LTE_ML1_SM_CON_AUTO_PBCH_ABORT_IND", NULL ),

  /* LTE_ML1_SM_CONN_MEAS_CGI_DELAYED_PBCH_TICK_IND, payload: lte_ml1_sm_conn_meas_tick_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020437, "LTE_ML1_SM_CONN_MEAS_CGI_DELAYED_PBCH_TICK_IND", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND, payload: lte_ml1_sm_sched_complete_ind_payload_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402043c, "LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND", NULL ),

  /* LTE_ML1_SM_ACQ_TIMER_EXPIRY_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402043d, "LTE_ML1_SM_ACQ_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_TICK_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0402043e, "LTE_ML1_SM_IRAT_SCHED_TICK_IND", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020440, "LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020441, "LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND", NULL ),

  /* LTE_ML1_SM_IRAT_SCHED_OFFLINE_OBJ_END_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020442, "LTE_ML1_SM_IRAT_SCHED_OFFLINE_OBJ_END_IND", NULL ),

  /* LTE_ML1_SM_PBCH_TIMER_EXPIRY_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020443, "LTE_ML1_SM_PBCH_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_SM_CONN_HST_PBCH_OCC_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020444, "LTE_ML1_SM_CONN_HST_PBCH_OCC_IND", NULL ),

  /* LTE_ML1_SM_CONN_HST_PBCH_ABORT_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020445, "LTE_ML1_SM_CONN_HST_PBCH_ABORT_IND", NULL ),

  /* LTE_ML1_SM_RF_TUNE_CNF, payload: lte_ml1_sm_rf_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020803, "LTE_ML1_SM_RF_TUNE_CNF", NULL ),

  /* LTE_ML1_SM_RX_CFG_CNF, payload: lte_ml1_sm_rx_cfg_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020808, "LTE_ML1_SM_RX_CFG_CNF", NULL ),

  /* LTE_ML1_SM_RF_TUNE_SCRIPT_CNF, payload: lte_ml1_sm_rf_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020816, "LTE_ML1_SM_RF_TUNE_SCRIPT_CNF", NULL ),

  /* LTE_ML1_SM_SCHEDULE_CANCEL_CNF, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020838, "LTE_ML1_SM_SCHEDULE_CANCEL_CNF", NULL ),

  /* LTE_ML1_SM_RAT_SHUTDOWN_CNF, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04020839, "LTE_ML1_SM_RAT_SHUTDOWN_CNF", NULL ),

  /* LTE_ML1_DLM_RX_CFG_REQ, payload: lte_ml1_dlm_rx_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030240, "LTE_ML1_DLM_RX_CFG_REQ", NULL ),

  /* LTE_ML1_DLM_CDRX_REQ, payload: lte_ml1_dlm_cdrx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030243, "LTE_ML1_DLM_CDRX_REQ", NULL ),

  /* LTE_ML1_DLM_PEND_RX_CFG_REQ, payload: lte_ml1_dlm_pend_rx_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030254, "LTE_ML1_DLM_PEND_RX_CFG_REQ", NULL ),

  /* LTE_ML1_DLM_CONN_RELEASE_REQ, payload: lte_ml1_dlm_conn_rel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030258, "LTE_ML1_DLM_CONN_RELEASE_REQ", NULL ),

  /* LTE_ML1_DLM_CA_ABORT_REQ, payload: lte_ml1_dlm_ca_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403026b, "LTE_ML1_DLM_CA_ABORT_REQ", NULL ),

  /* LTE_ML1_DLM_ANTENNA_SWITCH_REQ, payload: lte_ml1_dlm_antenna_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403026c, "LTE_ML1_DLM_ANTENNA_SWITCH_REQ", NULL ),

  /* LTE_ML1_DLM_OBJ_DL_RX_ABORT_REQ, payload: lte_ml1_dlm_obj_dl_rx_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403026e, "LTE_ML1_DLM_OBJ_DL_RX_ABORT_REQ", NULL ),

  /* LTE_ML1_DLM_ACQ2_START_REQ, payload: lte_ml1_dlm_stm_acq2_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030273, "LTE_ML1_DLM_ACQ2_START_REQ", NULL ),

  /* LTE_ML1_DLM_RACH_MSG2_EXPIRY_IND, payload: lte_ml1_dlm_rach_msg2_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030430, "LTE_ML1_DLM_RACH_MSG2_EXPIRY_IND", NULL ),

  /* LTE_ML1_DLM_SI_MSG_WIN_END_IND, payload: lte_ml1_dlm_si_msg_win_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030431, "LTE_ML1_DLM_SI_MSG_WIN_END_IND", NULL ),

  /* LTE_ML1_DLM_SIB1_WIN_END_IND, payload: lte_ml1_dlm_sib1_win_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030432, "LTE_ML1_DLM_SIB1_WIN_END_IND", NULL ),

  /* LTE_ML1_DLM_MOD_PRD_BND_IND, payload: lte_ml1_dlm_mod_prd_bnd_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030433, "LTE_ML1_DLM_MOD_PRD_BND_IND", NULL ),

  /* LTE_ML1_DLM_PO_WIN_END_IND, payload: lte_ml1_dlm_po_win_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030434, "LTE_ML1_DLM_PO_WIN_END_IND", NULL ),

  /* LTE_ML1_DLM_MIB_DECODE_COMP_IND, payload: lte_ml1_dlm_mib_decode_comp_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030435, "LTE_ML1_DLM_MIB_DECODE_COMP_IND", NULL ),

  /* LTE_ML1_DLM_BLOCK_OBJ_NS_TO_S_IND, payload: lte_ml1_dlm_block_obj_ns_to_s_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030436, "LTE_ML1_DLM_BLOCK_OBJ_NS_TO_S_IND", NULL ),

  /* LTE_ML1_DLM_BLOCK_OBJ_S_OR_NS_TO_NS_IND, payload: lte_ml1_dlm_block_obj_s_to_ns_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030437, "LTE_ML1_DLM_BLOCK_OBJ_S_OR_NS_TO_NS_IND", NULL ),

  /* LTE_ML1_DLM_INT_SIB1_WIN_END_IND, payload: lte_ml1_dlm_int_sib1_win_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030438, "LTE_ML1_DLM_INT_SIB1_WIN_END_IND", NULL ),

  /* LTE_ML1_DLM_EXT_SIB1_DECODE_DONE_IND, payload: lte_ml1_dlm_ext_sib1_decode_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030439, "LTE_ML1_DLM_EXT_SIB1_DECODE_DONE_IND", NULL ),

  /* LTE_ML1_DLM_SIB1_PUNCTURE_WIN_END_IND, payload: lte_ml1_dlm_sib1_puncture_win_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403043a, "LTE_ML1_DLM_SIB1_PUNCTURE_WIN_END_IND", NULL ),

  /* LTE_ML1_DLM_PO_ENV_END_IND, payload: lte_ml1_dlm_po_env_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403043c, "LTE_ML1_DLM_PO_ENV_END_IND", NULL ),

  /* LTE_ML1_DLM_INT_RX_CFG_DONE_IND, payload: lte_ml1_dlm_int_rx_cfg_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403043d, "LTE_ML1_DLM_INT_RX_CFG_DONE_IND", NULL ),

  /* LTE_ML1_DLM_SIB_ENV_END_IND, payload: lte_ml1_dlm_sib_env_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403043f, "LTE_ML1_DLM_SIB_ENV_END_IND", NULL ),

  /* LTE_ML1_DLM_TIMER_EXPIRY_IND, payload: lte_ml1_dlm_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030442, "LTE_ML1_DLM_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_DLM_RA_TIMER_ABORT_IND, payload: lte_ml1_dlm_ra_timer_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030445, "LTE_ML1_DLM_RA_TIMER_ABORT_IND", NULL ),

  /* LTE_ML1_DLM_PMCH_EVNT_START_IND, payload: lte_ml1_dlm_pmch_evnt_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030447, "LTE_ML1_DLM_PMCH_EVNT_START_IND", NULL ),

  /* LTE_ML1_DLM_PMCH_EVNT_END_IND, payload: lte_ml1_dlm_pmch_evnt_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030448, "LTE_ML1_DLM_PMCH_EVNT_END_IND", NULL ),

  /* LTE_ML1_DLM_PMCH_EVNT_ABORT_IND, payload: lte_ml1_dlm_pmch_evnt_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030449, "LTE_ML1_DLM_PMCH_EVNT_ABORT_IND", NULL ),

  /* LTE_ML1_DLM_MIB_EVNT_START_IND, payload: lte_ml1_dlm_mib_evnt_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044a, "LTE_ML1_DLM_MIB_EVNT_START_IND", NULL ),

  /* LTE_ML1_DLM_MIB_EVNT_ABORT_IND, payload: lte_ml1_dlm_mib_evnt_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044b, "LTE_ML1_DLM_MIB_EVNT_ABORT_IND", NULL ),

  /* LTE_ML1_DLM_P_RNTI_FAILURE_IND, payload: lte_ml1_dlm_p_rnti_failure_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044c, "LTE_ML1_DLM_P_RNTI_FAILURE_IND", NULL ),

  /* LTE_ML1_DLM_PMCH_INTRA_CSWITCH_IND, payload: lte_ml1_dlm_pmch_intra_cswitch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044d, "LTE_ML1_DLM_PMCH_INTRA_CSWITCH_IND", NULL ),

  /* LTE_ML1_DLM_CA_CFG_DONE_IND, payload: lte_ml1_dlm_ca_cfg_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044e, "LTE_ML1_DLM_CA_CFG_DONE_IND", NULL ),

  /* LTE_ML1_DLM_CA_SCC_ADD_IND, payload: lte_ml1_dlm_ca_scc_add_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403044f, "LTE_ML1_DLM_CA_SCC_ADD_IND", NULL ),

  /* LTE_ML1_DLM_CA_SCC_ACT_IND, payload: lte_ml1_dlm_ca_scc_act_deact_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030452, "LTE_ML1_DLM_CA_SCC_ACT_IND", NULL ),

  /* LTE_ML1_DLM_CA_ACT_DEACT_DONE_IND, payload: lte_ml1_dlm_ca_act_deact_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030453, "LTE_ML1_DLM_CA_ACT_DEACT_DONE_IND", NULL ),

  /* LTE_ML1_DLM_CA_SCC_DEACT_IND, payload: lte_ml1_dlm_ca_scc_act_deact_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030455, "LTE_ML1_DLM_CA_SCC_DEACT_IND", NULL ),

  /* LTE_ML1_DLM_CA_DEACT_DL_IND, payload: lte_ml1_dlm_ca_deact_dl_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030456, "LTE_ML1_DLM_CA_DEACT_DL_IND", NULL ),

  /* LTE_ML1_DLM_CA_SCC_DEL_IND, payload: lte_ml1_dlm_ca_scc_del_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030457, "LTE_ML1_DLM_CA_SCC_DEL_IND", NULL ),

  /* LTE_ML1_DLM_CA_PQ_IND, payload: lte_ml1_dlm_ca_pq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030460, "LTE_ML1_DLM_CA_PQ_IND", NULL ),

  /* LTE_ML1_DLM_INIT_ACQ_DONE_IND, payload: lte_ml1_dlm_init_acq_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030461, "LTE_ML1_DLM_INIT_ACQ_DONE_IND", NULL ),

  /* LTE_ML1_DLM_LIGHT_SLEEP_ISSUE_IND, payload: lte_ml1_dlm_light_sleep_issue_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030462, "LTE_ML1_DLM_LIGHT_SLEEP_ISSUE_IND", NULL ),

  /* LTE_ML1_DLM_CA_PEND_ACTION_TRIGGER_IND, payload: lte_ml1_dlm_ca_pend_action_trigger_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030464, "LTE_ML1_DLM_CA_PEND_ACTION_TRIGGER_IND", NULL ),

  /* LTE_ML1_DLM_SCC_VRLF_IND, payload: lte_ml1_dlm_scc_vrlf_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030465, "LTE_ML1_DLM_SCC_VRLF_IND", NULL ),

  /* LTE_ML1_DLM_SCC_VRLF_RESTORE_IND, payload: lte_ml1_dlm_scc_vrlf_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030466, "LTE_ML1_DLM_SCC_VRLF_RESTORE_IND", NULL ),

  /* LTE_ML1_DLM_CA_MCPM_PROC_DONE_IND, payload: lte_ml1_dlm_ca_mcpm_proc_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030468, "LTE_ML1_DLM_CA_MCPM_PROC_DONE_IND", NULL ),

  /* LTE_ML1_DLM_CA_PROC_START_IND, payload: lte_ml1_dlm_ca_proc_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030469, "LTE_ML1_DLM_CA_PROC_START_IND", NULL ),

  /* LTE_ML1_DLM_CA_OBJ_ABORT_IND, payload: lte_ml1_dlm_ca_obj_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403046a, "LTE_ML1_DLM_CA_OBJ_ABORT_IND", NULL ),

  /* LTE_ML1_DLM_TRM_ACQ_DONE_IND, payload: lte_ml1_dlm_trm_acq_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403046f, "LTE_ML1_DLM_TRM_ACQ_DONE_IND", NULL ),

  /* LTE_ML1_DLM_BIMC_SWITCH_OBJ_START_IND, payload: lte_ml1_dlm_stm_bimc_switch_obj_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030470, "LTE_ML1_DLM_BIMC_SWITCH_OBJ_START_IND", NULL ),

  /* LTE_ML1_DLM_BIMC_SWITCH_OBJ_ABORT_IND, payload: lte_ml1_dlm_stm_bimc_switch_obj_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030471, "LTE_ML1_DLM_BIMC_SWITCH_OBJ_ABORT_IND", NULL ),

  /* LTE_ML1_DLM_RESEL_CANCEL_TRIGGER_IND, payload: lte_ml1_dlm_resel_cancel_trigger_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030475, "LTE_ML1_DLM_RESEL_CANCEL_TRIGGER_IND", NULL ),

  /* LTE_ML1_DLM_CA_DTA_END_IND, payload: lte_ml1_dlm_ca_dta_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030476, "LTE_ML1_DLM_CA_DTA_END_IND", NULL ),

  /* LTE_ML1_DLM_CA_TRM_ACQ_IND, payload: lte_ml1_mgr_trm_acq_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030477, "LTE_ML1_DLM_CA_TRM_ACQ_IND", NULL ),

  /* LTE_ML1_DLM_RX_TUNE_CNF, payload: lte_ml1_dlm_rx_tune_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403083e, "LTE_ML1_DLM_RX_TUNE_CNF", NULL ),

  /* LTE_ML1_DLM_RX_CFG_CNF, payload: lte_ml1_dlm_rx_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030841, "LTE_ML1_DLM_RX_CFG_CNF", NULL ),

  /* LTE_ML1_DLM_CDRX_CNF, payload: lte_ml1_dlm_cdrx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030844, "LTE_ML1_DLM_CDRX_CNF", NULL ),

  /* LTE_ML1_DLM_EMBMS_SUSPEND_CNF, payload: lte_ml1_dlm_embms_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030846, "LTE_ML1_DLM_EMBMS_SUSPEND_CNF", NULL ),

  /* LTE_ML1_DLM_CA_SCC_INIT_SRCH_CNF, payload: lte_ml1_dlm_ca_scc_init_srch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030850, "LTE_ML1_DLM_CA_SCC_INIT_SRCH_CNF", NULL ),

  /* LTE_ML1_DLM_SCC_STATUS_UPDATE_CNF, payload: lte_ml1_dlm_scc_status_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030851, "LTE_ML1_DLM_SCC_STATUS_UPDATE_CNF", NULL ),

  /* LTE_ML1_DLM_RF_EXIT_CNF, payload: lte_ml1_dlm_rf_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030859, "LTE_ML1_DLM_RF_EXIT_CNF", NULL ),

  /* LTE_ML1_DLM_CA_PCC_CSWITCH_CNF, payload: lte_ml1_dlm_ca_pcc_cswitch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030863, "LTE_ML1_DLM_CA_PCC_CSWITCH_CNF", NULL ),

  /* LTE_ML1_DLM_MCVS_NOTIFY_CNF, payload: lte_ml1_dlm_mcvs_notify_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030867, "LTE_ML1_DLM_MCVS_NOTIFY_CNF", NULL ),

  /* LTE_ML1_DLM_ANTENNA_SWITCH_CNF, payload: lte_ml1_dlm_antenna_switch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0403086d, "LTE_ML1_DLM_ANTENNA_SWITCH_CNF", NULL ),

  /* LTE_ML1_DLM_CA_ANT_SWITCH_DISABLE_CNF, payload: lte_ml1_dlm_ca_ant_switch_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030874, "LTE_ML1_DLM_CA_ANT_SWITCH_DISABLE_CNF", NULL ),

  /* LTE_ML1_DLM_SM_RECFG_SUSPEND_CNF, payload: lte_ml1_dlm_stm_recfg_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030878, "LTE_ML1_DLM_SM_RECFG_SUSPEND_CNF", NULL ),

  /* LTE_ML1_DLM_POS_PRS_RECFG_SUSPEND_CNF, payload: lte_ml1_dlm_stm_recfg_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04030879, "LTE_ML1_DLM_POS_PRS_RECFG_SUSPEND_CNF", NULL ),

  /* LTE_ML1_ULM_SCC_STATUS_UPDATE_REQ, payload: lte_ml1_ulm_scc_status_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0404020d, "LTE_ML1_ULM_SCC_STATUS_UPDATE_REQ", NULL ),

  /* LTE_ML1_ULM_RACH_MSG12_SCHDLR_REG_IND, payload: lte_ml1_ulm_rach_msg12_schdlr_reg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040400, "LTE_ML1_ULM_RACH_MSG12_SCHDLR_REG_IND", NULL ),

  /* LTE_ML1_ULM_RACH_ABORTED_IND, payload: lte_ml1_ulm_rach_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040401, "LTE_ML1_ULM_RACH_ABORTED_IND", NULL ),

  /* LTE_ML1_ULM_RACH_CALLBACK_IND, payload: lte_ml1_ulm_rach_callback_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040402, "LTE_ML1_ULM_RACH_CALLBACK_IND", NULL ),

  /* LTE_ML1_ULM_RACH_ENV_START_IND, payload: lte_ml1_ulm_rach_env_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040403, "LTE_ML1_ULM_RACH_ENV_START_IND", NULL ),

  /* LTE_ML1_ULM_PRACH_TRIGGERED_IND, payload: lte_ml1_ulm_prach_triggered_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040404, "LTE_ML1_ULM_PRACH_TRIGGERED_IND", NULL ),

  /* LTE_ML1_ULM_RACH_ENV_ABORTED_IND, payload: lte_ml1_ulm_rach_env_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040405, "LTE_ML1_ULM_RACH_ENV_ABORTED_IND", NULL ),

  /* LTE_ML1_ULM_RACH_MSG3_GRANT_IND, payload: lte_ml1_ulm_rach_msg3_grant_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040406, "LTE_ML1_ULM_RACH_MSG3_GRANT_IND", NULL ),

  /* LTE_ML1_ULM_TA_RCVD_IND, payload: lte_ml1_ulm_ta_rcvd_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040408, "LTE_ML1_ULM_TA_RCVD_IND", NULL ),

  /* LTE_ML1_ULM_TX_DISABLE_CALLBACK_IND, payload: lte_ml1_ulm_tx_disable_callback_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04040409, "LTE_ML1_ULM_TX_DISABLE_CALLBACK_IND", NULL ),

  /* LTE_ML1_ULM_TX_DISABLE_ABORTED_IND, payload: lte_ml1_ulm_tx_disable_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0404040a, "LTE_ML1_ULM_TX_DISABLE_ABORTED_IND", NULL ),

  /* LTE_ML1_ULM_RACH_ENV_END_IND, payload: lte_ml1_ulm_rach_env_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0404040b, "LTE_ML1_ULM_RACH_ENV_END_IND", NULL ),

  /* LTE_ML1_ULM_PROC_PENDING_SCC_UPDATE_IND, payload: lte_ml1_ulm_proc_pending_scc_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0404040c, "LTE_ML1_ULM_PROC_PENDING_SCC_UPDATE_IND", NULL ),

  /* LTE_ML1_MGR_IMP_SCHDLR_OBJ_REQ, payload: lte_ml1_mgr_process_pq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050210, "LTE_ML1_MGR_IMP_SCHDLR_OBJ_REQ", NULL ),

  /* LTE_ML1_MGR_SM_SCHDLR_OBJ_REQ, payload: lte_ml1_mgr_process_pq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050211, "LTE_ML1_MGR_SM_SCHDLR_OBJ_REQ", NULL ),

  /* LTE_ML1_MGR_PLMN_SUSPEND_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050232, "LTE_ML1_MGR_PLMN_SUSPEND_REQ", NULL ),

  /* LTE_ML1_MGR_DIAG_F3_TOGGLE_REQ, payload: lte_ml1_mgr_diag_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050236, "LTE_ML1_MGR_DIAG_F3_TOGGLE_REQ", NULL ),

  /* LTE_ML1_MGR_DIAG_RSSI_IND_REQ, payload: lte_ml1_mgr_diag_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050238, "LTE_ML1_MGR_DIAG_RSSI_IND_REQ", NULL ),

  /* LTE_ML1_MGR_CM_RSSI_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0405023b, "LTE_ML1_MGR_CM_RSSI_REQ", NULL ),

  /* LTE_ML1_MGR_CM_NBR_RSSI_REQ, payload: lte_ml1_common_cm_nbr_rssi_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405023d, "LTE_ML1_MGR_CM_NBR_RSSI_REQ", NULL ),

  /* LTE_ML1_MGR_CM_ML1_MGR_INFO_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050244, "LTE_ML1_MGR_CM_ML1_MGR_INFO_REQ", NULL ),

  /* LTE_ML1_MGR_TEMP_TRM_CC_ACQ_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050248, "LTE_ML1_MGR_TEMP_TRM_CC_ACQ_REQ", NULL ),

  /* LTE_ML1_MGR_TEMP_TRM_CC_REL_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050249, "LTE_ML1_MGR_TEMP_TRM_CC_REL_REQ", NULL ),

  /* LTE_ML1_MGR_TAM_SUSPEND_REQ, payload: lte_cphy_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405024f, "LTE_ML1_MGR_TAM_SUSPEND_REQ", NULL ),

  /* LTE_ML1_MGR_TAM_RESUME_REQ, payload: lte_cphy_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050250, "LTE_ML1_MGR_TAM_RESUME_REQ", NULL ),

  /* LTE_ML1_MGR_CM_PMCH_STATS_REQ, payload: lte_ml1_common_cm_pmch_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050261, "LTE_ML1_MGR_CM_PMCH_STATS_REQ", NULL ),

  /* LTE_ML1_MGR_RRC_PROCESS_PQ_IND, payload: lte_ml1_mgr_process_pq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050400, "LTE_ML1_MGR_RRC_PROCESS_PQ_IND", NULL ),

  /* LTE_ML1_MGR_MAC_PROCESS_PQ_IND, payload: lte_ml1_mgr_process_pq_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050401, "LTE_ML1_MGR_MAC_PROCESS_PQ_IND", NULL ),

  /* LTE_ML1_MGR_TRIGGER_RSSI_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050425, "LTE_ML1_MGR_TRIGGER_RSSI_IND", NULL ),

  /* LTE_ML1_MGR_RESUME_PROC_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050426, "LTE_ML1_MGR_RESUME_PROC_IND", NULL ),

  /* LTE_ML1_MGR_CFG_START_IND, payload: lte_ml1_mgr_cfg_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050427, "LTE_ML1_MGR_CFG_START_IND", NULL ),

  /* LTE_ML1_MGR_RX_CFG_DONE_IND, payload: lte_ml1_mgr_rx_cfg_done_ind */
  MSGR_UMID_TABLE_ENTRY ( 0x04050428, "LTE_ML1_MGR_RX_CFG_DONE_IND", NULL ),

  /* LTE_ML1_MGR_DEFER_HANDLER_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050429, "LTE_ML1_MGR_DEFER_HANDLER_IND", NULL ),

  /* LTE_ML1_MGR_IDLE2CONN_TRIGGER_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0405042d, "LTE_ML1_MGR_IDLE2CONN_TRIGGER_IND", NULL ),

  /* LTE_ML1_MGR_DOG_TIMER_EXPIRY_IND, payload: lte_ml1_mgr_dog_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405042f, "LTE_ML1_MGR_DOG_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_MGR_HO_COMN_CFG_DONE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050430, "LTE_ML1_MGR_HO_COMN_CFG_DONE_IND", NULL ),

  /* LTE_ML1_MGR_CON_REL_DONE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050431, "LTE_ML1_MGR_CON_REL_DONE_IND", NULL ),

  /* LTE_ML1_MGR_UPDATE_PMIC_IND, payload: lte_ml1_mgr_update_pmic_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050434, "LTE_ML1_MGR_UPDATE_PMIC_IND", NULL ),

  /* LTE_ML1_MGR_FW_LOG_MASK_IND, payload: lte_ml1_mgr_fw_log_timer_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405043a, "LTE_ML1_MGR_FW_LOG_MASK_IND", NULL ),

  /* LTE_ML1_MGR_CM_SCAN_CAPTURE_START_IND, payload: lte_ml1_common_cm_scan_capture_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050440, "LTE_ML1_MGR_CM_SCAN_CAPTURE_START_IND", NULL ),

  /* LTE_ML1_MGR_CM_SCAN_CAPTURE_STOP_IND, payload: lte_ml1_common_cm_scan_capture_stop_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050441, "LTE_ML1_MGR_CM_SCAN_CAPTURE_STOP_IND", NULL ),

  /* LTE_ML1_MGR_HO_RX_TUNE_DONE_IND, payload: lte_ml1_mgr_ho_rx_tune_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050445, "LTE_ML1_MGR_HO_RX_TUNE_DONE_IND", NULL ),

  /* LTE_ML1_MGR_HO_TX_TUNE_DONE_IND, payload: lte_ml1_mgr_ho_tx_tune_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050446, "LTE_ML1_MGR_HO_TX_TUNE_DONE_IND", NULL ),

  /* LTE_ML1_MGR_RESEL_TRIGGER_IND, payload: lte_ml1_mgr_resel_trigger_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405044e, "LTE_ML1_MGR_RESEL_TRIGGER_IND", NULL ),

  /* LTE_ML1_MGR_TAM_SPV_TIMER_EXPIRY_IND, payload: lte_ml1_mgr_spv_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050451, "LTE_ML1_MGR_TAM_SPV_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_MGR_TAM_TAO_START_CB_IND, payload: lte_ml1_mgr_tam_tao_start_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050452, "LTE_ML1_MGR_TAM_TAO_START_CB_IND", NULL ),

  /* LTE_ML1_MGR_TAM_TAO_SCHEDULE_IND, payload: lte_ml1_mgr_tam_tao_schedule_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050453, "LTE_ML1_MGR_TAM_TAO_SCHEDULE_IND", NULL ),

  /* LTE_ML1_MGR_TTL_SETUP_DONE_IND, payload: lte_ml1_mgr_ttl_setup_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050454, "LTE_ML1_MGR_TTL_SETUP_DONE_IND", NULL ),

  /* LTE_ML1_MGR_TAM_NO_RF_IND, payload: lte_ml1_mgr_tam_no_rf_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050456, "LTE_ML1_MGR_TAM_NO_RF_IND", NULL ),

  /* LTE_ML1_MGR_TAM_RF_LESS_WAKEUP_IND, payload: lte_ml1_mgr_tam_rf_less_wakeup_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050457, "LTE_ML1_MGR_TAM_RF_LESS_WAKEUP_IND", NULL ),

  /* LTE_ML1_MGR_TAM_TAO_ABORT_IND, payload: lte_ml1_mgr_tam_tao_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050458, "LTE_ML1_MGR_TAM_TAO_ABORT_IND", NULL ),

  /* LTE_ML1_MGR_QTA_TO_LTA_SWITCH_IND, payload: lte_ml1_mgr_qta_to_lta_switch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050459, "LTE_ML1_MGR_QTA_TO_LTA_SWITCH_IND", NULL ),

  /* LTE_ML1_MGR_TRM_ACQ_DONE_IND, payload: lte_ml1_mgr_trm_acq_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050460, "LTE_ML1_MGR_TRM_ACQ_DONE_IND", NULL ),

  /* LTE_ML1_MGR_ACQ_STAGE1_COMPLETE_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050800, "LTE_ML1_MGR_ACQ_STAGE1_COMPLETE_CNF", NULL ),

  /* LTE_ML1_MGR_ACQ_STAGE2_COMPLETE_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050801, "LTE_ML1_MGR_ACQ_STAGE2_COMPLETE_CNF", NULL ),

  /* LTE_ML1_MGR_SM_ABORT_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050802, "LTE_ML1_MGR_SM_ABORT_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_ABORT_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050803, "LTE_ML1_MGR_DLM_ABORT_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_COMN_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050804, "LTE_ML1_MGR_DLM_COMN_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_DED_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050805, "LTE_ML1_MGR_DLM_DED_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_ABORT_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050807, "LTE_ML1_MGR_ULM_ABORT_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_COMN_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050808, "LTE_ML1_MGR_ULM_COMN_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_DED_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050809, "LTE_ML1_MGR_ULM_DED_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_CELL_SELECT_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080a, "LTE_ML1_MGR_CELL_SELECT_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_SUSPEND_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080b, "LTE_ML1_MGR_DLM_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_HO_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080c, "LTE_ML1_MGR_DLM_HO_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_HO_CFG_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080d, "LTE_ML1_MGR_ULM_HO_CFG_CNF", NULL ),

  /* LTE_ML1_MGR_SM_SUSPEND_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080e, "LTE_ML1_MGR_SM_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_SM_HO_ACQ_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405080f, "LTE_ML1_MGR_SM_HO_ACQ_CNF", NULL ),

  /* LTE_ML1_MGR_RESEL_COMP_CNF, payload: lte_ml1_mgr_resel_comp_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050810, "LTE_ML1_MGR_RESEL_COMP_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_HO_CSWITCH_CNF, payload: lte_ml1_mgr_ho_cswitch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050811, "LTE_ML1_MGR_DLM_HO_CSWITCH_CNF", NULL ),

  /* LTE_ML1_MGR_SCHDLR_SUSPEND_CNF, payload: lte_ml1_mgr_schdlr_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050812, "LTE_ML1_MGR_SCHDLR_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_SCHDLR_RESUME_CNF, payload: lte_ml1_mgr_schdlr_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050813, "LTE_ML1_MGR_SCHDLR_RESUME_CNF", NULL ),

  /* LTE_ML1_MGR_SCHDLR_DEACTIVATE_CNF, payload: lte_ml1_mgr_schdlr_deactivate_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050814, "LTE_ML1_MGR_SCHDLR_DEACTIVATE_CNF", NULL ),

  /* LTE_ML1_MGR_HO_CFG_DONE_CNF, payload: lte_ml1_mgr_ho_cfg_done_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050815, "LTE_ML1_MGR_HO_CFG_DONE_CNF", NULL ),

  /* LTE_ML1_MGR_ACQ_ODY_PLT_COMPLETE_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050816, "LTE_ML1_MGR_ACQ_ODY_PLT_COMPLETE_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_RACH_ABORT_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050817, "LTE_ML1_MGR_ULM_RACH_ABORT_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_START_RACH_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050818, "LTE_ML1_MGR_ULM_START_RACH_CNF", NULL ),

  /* LTE_ML1_MGR_SM_HO_PBCH_DONE_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050819, "LTE_ML1_MGR_SM_HO_PBCH_DONE_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_SFN_UPDATE_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081a, "LTE_ML1_MGR_DLM_SFN_UPDATE_CNF", NULL ),

  /* LTE_ML1_MGR_SM_CONN_REL_CNF, payload: lte_ml1_mgr_sm_conn_rel_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081b, "LTE_ML1_MGR_SM_CONN_REL_CNF", NULL ),

  /* LTE_ML1_MGR_RFM_CNF, payload: lte_ml1_mgr_rfm_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081c, "LTE_ML1_MGR_RFM_CNF", NULL ),

  /* LTE_ML1_MGR_SM_BS_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081d, "LTE_ML1_MGR_SM_BS_CNF", NULL ),

  /* LTE_ML1_MGR_SM_SS_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081e, "LTE_ML1_MGR_SM_SS_CNF", NULL ),

  /* LTE_ML1_MGR_PLMN_CELL_CNF, payload: lte_ml1_mgr_plmn_cell_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405081f, "LTE_ML1_MGR_PLMN_CELL_CNF", NULL ),

  /* LTE_ML1_MGR_PLMN_STOP_CNF, payload: lte_ml1_mgr_plmn_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050820, "LTE_ML1_MGR_PLMN_STOP_CNF", NULL ),

  /* LTE_ML1_MGR_PLMN_SUSPEND_CNF, payload: lte_ml1_mgr_plmn_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050821, "LTE_ML1_MGR_PLMN_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_RESUME_CNF, payload: lte_ml1_mgr_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050822, "LTE_ML1_MGR_RESUME_CNF", NULL ),

  /* LTE_ML1_MGR_SM_RESUME_ACQ_CNF, payload: lte_ml1_mgr_sm_resume_acq_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050824, "LTE_ML1_MGR_SM_RESUME_ACQ_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_CONN_REL_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405082a, "LTE_ML1_MGR_ULM_CONN_REL_CNF", NULL ),

  /* LTE_ML1_MGR_CFG_DONE_CNF, payload: lte_ml1_mgr_cfg_done_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405082b, "LTE_ML1_MGR_CFG_DONE_CNF", NULL ),

  /* LTE_ML1_MGR_SM_IDLE_EXIT_CNF, payload: lte_ml1_mgr_sm_idle_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405082c, "LTE_ML1_MGR_SM_IDLE_EXIT_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_RESEL_CANCEL_CNF, payload: lte_ml1_mgr_dlm_resel_cancel_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405082e, "LTE_ML1_MGR_DLM_RESEL_CANCEL_CNF", NULL ),

  /* LTE_ML1_MGR_RECOVERY_CLEANUP_CNF, payload: lte_ml1_mgr_recovery_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050833, "LTE_ML1_MGR_RECOVERY_CLEANUP_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_HO_SUSPEND_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050835, "LTE_ML1_MGR_ULM_HO_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_DIAG_F3_TOGGLE_CNF, payload: lte_ml1_mgr_diag_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050837, "LTE_ML1_MGR_DIAG_F3_TOGGLE_CNF", NULL ),

  /* LTE_ML1_MGR_DIAG_RSSI_IND_CNF, payload: lte_ml1_mgr_diag_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050839, "LTE_ML1_MGR_DIAG_RSSI_IND_CNF", NULL ),

  /* LTE_ML1_MGR_CM_RSSI_CNF, payload: lte_api_signal_info_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0405083c, "LTE_ML1_MGR_CM_RSSI_CNF", NULL ),

  /* LTE_ML1_MGR_CM_NBR_RSSI_CNF, payload: lte_ml1_common_cm_nbr_rssi_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405083e, "LTE_ML1_MGR_CM_NBR_RSSI_CNF", NULL ),

  /* LTE_ML1_MGR_X2L_HO_PREP_CNF, payload: lte_ml1_mgr_x2l_ho_prep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405083f, "LTE_ML1_MGR_X2L_HO_PREP_CNF", NULL ),

  /* LTE_ML1_MGR_CM_SCAN_CAPTURE_STOP_CNF, payload: lte_ml1_common_cm_scan_capture_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050842, "LTE_ML1_MGR_CM_SCAN_CAPTURE_STOP_CNF", NULL ),

  /* LTE_ML1_MGR_ULM_SUSPEND_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04050843, "LTE_ML1_MGR_ULM_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_CM_ML1_MGR_INFO_CNF, payload: lte_ml1_mgr_cm_ml1_mgr_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050844, "LTE_ML1_MGR_CM_ML1_MGR_INFO_CNF", NULL ),

  /* LTE_ML1_MGR_TRM_STATUS_UPDATE_CNF, payload: lte_ml1_mgr_trm_status_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050847, "LTE_ML1_MGR_TRM_STATUS_UPDATE_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_CONN_REL_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405084a, "LTE_ML1_MGR_DLM_CONN_REL_CNF", NULL ),

  /* LTE_ML1_MGR_PLMN_CPHY_SUSPEND_CNF, payload: lte_ml1_mgr_plmn_cphy_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405084b, "LTE_ML1_MGR_PLMN_CPHY_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_CA_RESOURCE_REL_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405084c, "LTE_ML1_MGR_DLM_CA_RESOURCE_REL_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_CA_RESOURCE_ACQ_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0405084d, "LTE_ML1_MGR_DLM_CA_RESOURCE_ACQ_CNF", NULL ),

  /* LTE_ML1_MGR_TAM_SUSPEND_CNF, payload: lte_cphy_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0405084f, "LTE_ML1_MGR_TAM_SUSPEND_CNF", NULL ),

  /* LTE_ML1_MGR_TAM_RESUME_CNF, payload: lte_cphy_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050850, "LTE_ML1_MGR_TAM_RESUME_CNF", NULL ),

  /* LTE_ML1_MGR_ASDIV_DISABLE_CNF, payload: lte_ml1_mgr_asdiv_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050851, "LTE_ML1_MGR_ASDIV_DISABLE_CNF", NULL ),

  /* LTE_ML1_MGR_DLM_CA_PROC_TRIGGER_CNF, payload: lte_ml1_mgr_ca_proc_trigger_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04050855, "LTE_ML1_MGR_DLM_CA_PROC_TRIGGER_CNF", NULL ),

  /* LTE_ML1_MGR_CM_PMCH_STATS_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04050861, "LTE_ML1_MGR_CM_PMCH_STATS_CNF", NULL ),

  /* LTE_LL1_SRCH_FRONT_END_CONFIG_REQ, payload: lte_LL1_srch_front_end_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070200, "LTE_LL1_SRCH_FRONT_END_CONFIG_REQ", NULL ),

  /* LTE_LL1_SRCH_INIT_ACQ_REQ, payload: lte_LL1_srch_init_acq_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070201, "LTE_LL1_SRCH_INIT_ACQ_REQ", NULL ),

  /* LTE_LL1_SRCH_SERVING_CELL_SRCH_REQ, payload: lte_LL1_srch_serving_cell_srch_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070202, "LTE_LL1_SRCH_SERVING_CELL_SRCH_REQ", NULL ),

  /* LTE_LL1_SRCH_NEIGHBOR_CELL_SRCH_REQ, payload: lte_LL1_srch_neighbor_cell_srch_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070203, "LTE_LL1_SRCH_NEIGHBOR_CELL_SRCH_REQ", NULL ),

  /* LTE_LL1_SRCH_PBCH_DECODE_REQ, payload: lte_LL1_srch_pbch_decode_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070204, "LTE_LL1_SRCH_PBCH_DECODE_REQ", NULL ),

  /* LTE_LL1_SRCH_STOP_PBCH_DECODE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04070205, "LTE_LL1_SRCH_STOP_PBCH_DECODE_REQ", NULL ),

  /* LTE_LL1_SRCH_ABORT_REQ, payload: lte_LL1_srch_init_acq_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070206, "LTE_LL1_SRCH_ABORT_REQ", NULL ),

  /* LTE_LL1_SRCH_TEST_PARAM_CFG_REQ, payload: lte_LL1_srch_test_param_cfg_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070207, "LTE_LL1_SRCH_TEST_PARAM_CFG_REQ", NULL ),

  /* LTE_LL1_SRCH_ABORT_NCELL_SRCH_REQ, payload: lte_LL1_srch_ncell_srch_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070208, "LTE_LL1_SRCH_ABORT_NCELL_SRCH_REQ", NULL ),

  /* LTE_LL1_SRCH_PROC_CONFIG_REQ, payload: lte_LL1_srch_proc_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070209, "LTE_LL1_SRCH_PROC_CONFIG_REQ", NULL ),

  /* LTE_LL1_SRCH_SRCH_BLACKLIST_REQ, payload: lte_LL1_srch_black_list_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020a, "LTE_LL1_SRCH_SRCH_BLACKLIST_REQ", NULL ),

  /* LTE_LL1_SRCH_LIST_FREQ_SCAN_REQ, payload: lte_LL1_srch_list_freq_scan_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020b, "LTE_LL1_SRCH_LIST_FREQ_SCAN_REQ", NULL ),

  /* LTE_LL1_SRCH_FULL_FREQ_SCAN_REQ, payload: lte_LL1_srch_full_freq_scan_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020c, "LTE_LL1_SRCH_FULL_FREQ_SCAN_REQ", NULL ),

  /* LTE_LL1_SRCH_ABORT_FREQ_SCAN_REQ, payload: lte_LL1_srch_freq_scan_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020d, "LTE_LL1_SRCH_ABORT_FREQ_SCAN_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_CONN_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_conn_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020e, "LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_CONN_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_DRX_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_drx_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407020f, "LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_DRX_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_SCELL_REQ, payload: lte_LL1_meas_scell_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070210, "LTE_LL1_SRCH_MEAS_SCELL_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_ABORT_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070211, "LTE_LL1_SRCH_MEAS_TTL_FTL_NCELL_ABORT_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_TRACKING_NCELL_REQ, payload: lte_LL1_meas_tracking_ncell_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070212, "LTE_LL1_SRCH_MEAS_TRACKING_NCELL_REQ", NULL ),

  /* LTE_LL1_SRCH_MEAS_TRACKING_NCELL_ABORT_REQ, payload: lte_LL1_meas_tracking_ncell_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070213, "LTE_LL1_SRCH_MEAS_TRACKING_NCELL_ABORT_REQ", NULL ),

  /* LTE_LL1_SRCH_FRONT_END_CONFIG_CNF, payload: lte_LL1_srch_front_end_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070800, "LTE_LL1_SRCH_FRONT_END_CONFIG_CNF", NULL ),

  /* LTE_LL1_SRCH_INIT_ACQ_CNF, payload: lte_LL1_srch_init_acq_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070801, "LTE_LL1_SRCH_INIT_ACQ_CNF", NULL ),

  /* LTE_LL1_SRCH_SERVING_CELL_SRCH_CNF, payload: lte_LL1_srch_serving_cell_srch_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070802, "LTE_LL1_SRCH_SERVING_CELL_SRCH_CNF", NULL ),

  /* LTE_LL1_SRCH_NEIGHBOR_CELL_SRCH_CNF, payload: lte_LL1_srch_neighbor_cell_srch_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070803, "LTE_LL1_SRCH_NEIGHBOR_CELL_SRCH_CNF", NULL ),

  /* LTE_LL1_SRCH_PBCH_DECODE_CNF, payload: lte_LL1_srch_pbch_decode_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070804, "LTE_LL1_SRCH_PBCH_DECODE_CNF", NULL ),

  /* LTE_LL1_SRCH_STOP_PBCH_DECODE_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04070805, "LTE_LL1_SRCH_STOP_PBCH_DECODE_CNF", NULL ),

  /* LTE_LL1_SRCH_ABORT_CNF, payload: lte_LL1_srch_init_acq_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070806, "LTE_LL1_SRCH_ABORT_CNF", NULL ),

  /* LTE_LL1_SRCH_ABORT_NCELL_SRCH_CNF, payload: lte_LL1_srch_ncell_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070807, "LTE_LL1_SRCH_ABORT_NCELL_SRCH_CNF", NULL ),

  /* LTE_LL1_SRCH_PROC_CONFIG_CNF, payload: lte_LL1_srch_proc_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070808, "LTE_LL1_SRCH_PROC_CONFIG_CNF", NULL ),

  /* LTE_LL1_SRCH_SRCH_BLACKLIST_CNF, payload: lte_LL1_srch_black_list_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04070809, "LTE_LL1_SRCH_SRCH_BLACKLIST_CNF", NULL ),

  /* LTE_LL1_SRCH_LIST_FREQ_SCAN_CNF, payload: lte_LL1_srch_list_freq_scan_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407080a, "LTE_LL1_SRCH_LIST_FREQ_SCAN_CNF", NULL ),

  /* LTE_LL1_SRCH_FULL_FREQ_SCAN_CNF, payload: lte_LL1_srch_full_freq_scan_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407080b, "LTE_LL1_SRCH_FULL_FREQ_SCAN_CNF", NULL ),

  /* LTE_LL1_SRCH_ABORT_FREQ_SCAN_CNF, payload: lte_LL1_srch_freq_scan_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0407080c, "LTE_LL1_SRCH_ABORT_FREQ_SCAN_CNF", NULL ),

  /* LTE_LL1_DL_CH_CONFIG_REQ, payload: lte_LL1_dl_ch_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080200, "LTE_LL1_DL_CH_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_CH_STATE_CHANGE_REQ, payload: lte_LL1_dl_ch_state_change_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080201, "LTE_LL1_DL_CH_STATE_CHANGE_REQ", NULL ),

  /* LTE_LL1_DL_PDCCH_RNTI_CHANGE_REQ, payload: lte_LL1_dl_pdcch_rnti_change_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080202, "LTE_LL1_DL_PDCCH_RNTI_CHANGE_REQ", NULL ),

  /* LTE_LL1_DL_PBCH_STATE_CHANGE_REQ, payload: lte_LL1_dl_pbch_state_change_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080203, "LTE_LL1_DL_PBCH_STATE_CHANGE_REQ", NULL ),

  /* LTE_LL1_DL_ENABLE_DISABLE_REQ, payload: lte_LL1_dl_enable_disable_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080204, "LTE_LL1_DL_ENABLE_DISABLE_REQ", NULL ),

  /* LTE_LL1_DL_TEST_DDE_REQ, payload: lte_LL1_dl_test_dde_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080205, "LTE_LL1_DL_TEST_DDE_REQ", NULL ),

  /* LTE_LL1_DL_TEST_DDE_SF_REQ, payload: lte_LL1_dl_test_dde_sf_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080206, "LTE_LL1_DL_TEST_DDE_SF_REQ", NULL ),

  /* LTE_LL1_DL_TEST_CHEST_REQ, payload: lte_LL1_dl_test_chest_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080207, "LTE_LL1_DL_TEST_CHEST_REQ", NULL ),

  /* LTE_LL1_DL_TEST_FTL_REQ, payload: lte_LL1_dl_test_ftl_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080208, "LTE_LL1_DL_TEST_FTL_REQ", NULL ),

  /* LTE_LL1_DL_TEST_TTL_REQ, payload: lte_LL1_dl_test_ttl_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080209, "LTE_LL1_DL_TEST_TTL_REQ", NULL ),

  /* LTE_LL1_DL_TTL_CONFIG_REQ, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020a, "LTE_LL1_DL_TTL_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_FTL_CONFIG_REQ, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020b, "LTE_LL1_DL_FTL_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_CONFIG_REQ, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020c, "LTE_LL1_DL_MEAS_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_SCELL_REQ, payload: lte_LL1_meas_scell_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020d, "LTE_LL1_DL_MEAS_SCELL_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TRACKING_NCELL_REQ, payload: lte_LL1_meas_tracking_ncell_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020e, "LTE_LL1_DL_MEAS_TRACKING_NCELL_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TRACKING_NCELL_ABORT_REQ, payload: lte_LL1_meas_tracking_ncell_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408020f, "LTE_LL1_DL_MEAS_TRACKING_NCELL_ABORT_REQ", NULL ),

  /* LTE_LL1_DL_SI_START_COLLECT_REQ, payload: lte_LL1_dl_si_start_collect_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080210, "LTE_LL1_DL_SI_START_COLLECT_REQ", NULL ),

  /* LTE_LL1_DL_SI_STOP_COLLECT_REQ, payload: lte_LL1_dl_si_stop_collect_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080211, "LTE_LL1_DL_SI_STOP_COLLECT_REQ", NULL ),

  /* LTE_LL1_DL_RLM_REQ, payload: lte_LL1_dl_rlm_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080212, "LTE_LL1_DL_RLM_REQ", NULL ),

  /* LTE_LL1_DL_CONFIG_CE_GENERAL_REQ, payload: lte_LL1_dl_config_ce_general_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080213, "LTE_LL1_DL_CONFIG_CE_GENERAL_REQ", NULL ),

  /* LTE_LL1_DL_CONFIG_CE_IIR_REQ, payload: lte_LL1_dl_config_ce_iir_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080214, "LTE_LL1_DL_CONFIG_CE_IIR_REQ", NULL ),

  /* LTE_LL1_DL_PDSCH_TEST_REQ, payload: lte_LL1_dl_pdsch_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080215, "LTE_LL1_DL_PDSCH_TEST_REQ", NULL ),

  /* LTE_LL1_DL_PDSCH_CONFIG_REQ, payload: lte_LL1_dl_pdsch_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080216, "LTE_LL1_DL_PDSCH_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_PDCCH_TEST_REQ, payload: lte_LL1_dl_pdcch_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080217, "LTE_LL1_DL_PDCCH_TEST_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_SCELL_CONFIG_REQ, payload: lte_LL1_meas_scell_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080218, "LTE_LL1_DL_MEAS_SCELL_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_SCELL_ODYSSEY_REQ, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x04080219, "LTE_LL1_DL_MEAS_SCELL_ODYSSEY_REQ", NULL ),

  /* LTE_LL1_DL_TTL_SCELL_CONFIG_REQ, payload: lte_LL1_ttl_scell_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021a, "LTE_LL1_DL_TTL_SCELL_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_FTL_SCELL_CONFIG_REQ, payload: lte_LL1_ftl_scell_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021b, "LTE_LL1_DL_FTL_SCELL_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_CSF_CONFIG_REQ, payload: lte_LL1_dl_csf_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021c, "LTE_LL1_DL_CSF_CONFIG_REQ", NULL ),

  /* LTE_LL1_DL_CSF_SCHED_REQ, payload: lte_LL1_dl_csf_sched_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021d, "LTE_LL1_DL_CSF_SCHED_REQ", NULL ),

  /* LTE_LL1_DL_CSF_DECONFIG_REQ, payload: lte_LL1_dl_csf_deconfig_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021e, "LTE_LL1_DL_CSF_DECONFIG_REQ", NULL ),

  /* LTE_LL1_DL_PBCH_DECODE_REQ, payload: lte_LL1_dl_pbch_decode_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408021f, "LTE_LL1_DL_PBCH_DECODE_REQ", NULL ),

  /* LTE_LL1_DL_PBCH_STOP_DECODE_REQ, payload: lte_LL1_dl_pbch_stop_decode_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080220, "LTE_LL1_DL_PBCH_STOP_DECODE_REQ", NULL ),

  /* LTE_LL1_DL_RXAGC_CFG_PARAM_REQ, payload: lte_LL1_rxagc_cfg_param_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080221, "LTE_LL1_DL_RXAGC_CFG_PARAM_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CONN_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_conn_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080222, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CONN_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_DRX_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_drx_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080223, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_DRX_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_ABORT_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080224, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_ABORT_REQ", NULL ),

  /* LTE_LL1_DL_RVAFC_GET_FREQ_CORR_REQ, payload: lte_LL1_rvafc_get_freq_corr_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080225, "LTE_LL1_DL_RVAFC_GET_FREQ_CORR_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CDRX_SCHED_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_cdrx_sched_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080226, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CDRX_SCHED_REQ", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CDRX_CONT_REQ, payload: lte_LL1_meas_ttl_ftl_ncell_cdrx_cont_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080227, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CDRX_CONT_REQ", NULL ),

  /* LTE_LL1_DL_ANT_CORR_ENABLE_DISABLE_REQ, payload: lte_LL1_ant_corr_enable_disable_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080228, "LTE_LL1_DL_ANT_CORR_ENABLE_DISABLE_REQ", NULL ),

  /* LTE_LL1_DL_AP_CSF_SCHED_REQ, payload: lte_LL1_dl_csf_sched_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080229, "LTE_LL1_DL_AP_CSF_SCHED_REQ", NULL ),

  /* LTE_LL1_DL_PRS_MEAS_REQ, payload: lte_LL1_dl_prs_meas_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022a, "LTE_LL1_DL_PRS_MEAS_REQ", NULL ),

  /* LTE_LL1_DL_PRS_ABORT_REQ, payload: lte_LL1_dl_prs_abort_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022b, "LTE_LL1_DL_PRS_ABORT_REQ", NULL ),

  /* LTE_LL1_DL_PMCH_DECODE_REQ, payload: lte_LL1_dl_pmch_decode_req_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022c, "LTE_LL1_DL_PMCH_DECODE_REQ", NULL ),

  /* LTE_LL1_DL_PMCH_DECODE_STOP_REQ, payload: lte_LL1_dl_pmch_decode_stop_req_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022d, "LTE_LL1_DL_PMCH_DECODE_STOP_REQ", NULL ),

  /* LTE_LL1_DL_IQ_SAMPLE_CAPTURE_REQ, payload: lte_LL1_dl_iq_sample_capture_req_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022e, "LTE_LL1_DL_IQ_SAMPLE_CAPTURE_REQ", NULL ),

  /* LTE_LL1_DL_PDCCH_MONITOR_CANCEL_REQ, payload: lte_LL1_dl_pdcch_monitor_cancel_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408022f, "LTE_LL1_DL_PDCCH_MONITOR_CANCEL_REQ", NULL ),

  /* LTE_LL1_DL_OPCRS_MEAS_REQ, payload: lte_LL1_dl_opcrs_meas_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080230, "LTE_LL1_DL_OPCRS_MEAS_REQ", NULL ),

  /* LTE_LL1_DL_PDCCH_PHICH_IND, payload: lte_LL1_dl_pdcch_phich_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080400, "LTE_LL1_DL_PDCCH_PHICH_IND", NULL ),

  /* LTE_LL1_DL_PBCH_RESULT_IND, payload: lte_LL1_dl_pbch_result_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080401, "LTE_LL1_DL_PBCH_RESULT_IND", NULL ),

  /* LTE_LL1_DL_RLM_BLER_IND, payload: lte_LL1_dl_rlm_bler_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080402, "LTE_LL1_DL_RLM_BLER_IND", NULL ),

  /* LTE_LL1_DL_PDSCH_STAT_IND, payload: lte_LL1_dl_pdsch_stat_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080403, "LTE_LL1_DL_PDSCH_STAT_IND", NULL ),

  /* LTE_LL1_DL_ANT_CORR_RESULT_IND, payload: lte_LL1_ant_corr_result_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080404, "LTE_LL1_DL_ANT_CORR_RESULT_IND", NULL ),

  /* LTE_LL1_DL_EMBMS_WHITENED_MATRICES_IND, payload: lte_LL1_dl_embms_whitened_matrices_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080405, "LTE_LL1_DL_EMBMS_WHITENED_MATRICES_IND", NULL ),

  /* LTE_LL1_DL_OPCRS_MEAS_IND, payload: lte_LL1_dl_opcrs_meas_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080406, "LTE_LL1_DL_OPCRS_MEAS_IND", NULL ),

  /* LTE_LL1_DL_CH_CONFIG_CNF, payload: lte_LL1_dl_ch_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080800, "LTE_LL1_DL_CH_CONFIG_CNF", NULL ),

  /* LTE_LL1_DL_CH_STATE_CHANGE_CNF, payload: lte_LL1_dl_ch_state_change_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080801, "LTE_LL1_DL_CH_STATE_CHANGE_CNF", NULL ),

  /* LTE_LL1_DL_PDCCH_RNTI_CHANGE_CNF, payload: lte_LL1_dl_pdcch_rnti_change_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080802, "LTE_LL1_DL_PDCCH_RNTI_CHANGE_CNF", NULL ),

  /* LTE_LL1_DL_PBCH_STATE_CHANGE_CNF, payload: lte_LL1_dl_pbch_state_change_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080803, "LTE_LL1_DL_PBCH_STATE_CHANGE_CNF", NULL ),

  /* LTE_LL1_DL_ENABLE_DISABLE_CNF, payload: lte_LL1_dl_enable_disable_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080804, "LTE_LL1_DL_ENABLE_DISABLE_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_SCELL_CNF, payload: lte_LL1_meas_scell_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080805, "LTE_LL1_DL_MEAS_SCELL_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_TRACKING_NCELL_CNF, payload: lte_LL1_meas_tracking_ncell_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080806, "LTE_LL1_DL_MEAS_TRACKING_NCELL_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_TRACKING_NCELL_ABORT_CNF, payload: lte_LL1_meas_tracking_ncell_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080807, "LTE_LL1_DL_MEAS_TRACKING_NCELL_ABORT_CNF", NULL ),

  /* LTE_LL1_DL_SI_START_COLLECT_CNF, payload: lte_LL1_dl_si_start_collect_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080808, "LTE_LL1_DL_SI_START_COLLECT_CNF", NULL ),

  /* LTE_LL1_DL_SI_STOP_COLLECT_CNF, payload: lte_LL1_dl_si_stop_collect_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080809, "LTE_LL1_DL_SI_STOP_COLLECT_CNF", NULL ),

  /* LTE_LL1_DL_PDSCH_TEST_CNF, payload: lte_LL1_dl_pdsch_test_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080a, "LTE_LL1_DL_PDSCH_TEST_CNF", NULL ),

  /* LTE_LL1_DL_PDSCH_CONFIG_CNF, payload: lte_LL1_dl_pdsch_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080b, "LTE_LL1_DL_PDSCH_CONFIG_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_SCELL_ODYSSEY_CNF, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080c, "LTE_LL1_DL_MEAS_SCELL_ODYSSEY_CNF", NULL ),

  /* LTE_LL1_DL_CSF_CONFIG_CNF, payload: lte_LL1_dl_csf_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080d, "LTE_LL1_DL_CSF_CONFIG_CNF", NULL ),

  /* LTE_LL1_DL_CSF_DECONFIG_CNF, payload: lte_LL1_dl_csf_deconfig_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080e, "LTE_LL1_DL_CSF_DECONFIG_CNF", NULL ),

  /* LTE_LL1_DL_PBCH_DECODE_CNF, payload: lte_LL1_dl_pbch_decode_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408080f, "LTE_LL1_DL_PBCH_DECODE_CNF", NULL ),

  /* LTE_LL1_DL_PBCH_STOP_DECODE_CNF, payload: lte_LL1_dl_pbch_stop_decode_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080810, "LTE_LL1_DL_PBCH_STOP_DECODE_CNF", NULL ),

  /* LTE_LL1_DL_RXAGC_CFG_PARAM_CNF, payload: lte_LL1_rxagc_cfg_param_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080811, "LTE_LL1_DL_RXAGC_CFG_PARAM_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CNF, payload: lte_LL1_meas_ttl_ftl_ncell_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080812, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_CNF", NULL ),

  /* LTE_LL1_DL_MEAS_TTL_FTL_NCELL_ABORT_CNF, payload: lte_LL1_meas_ttl_ftl_ncell_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080813, "LTE_LL1_DL_MEAS_TTL_FTL_NCELL_ABORT_CNF", NULL ),

  /* LTE_LL1_DL_RLM_CNF, payload: lte_LL1_rlm_req_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080814, "LTE_LL1_DL_RLM_CNF", NULL ),

  /* LTE_LL1_DL_RVAFC_FREQ_CORR_CNF, payload: lte_LL1_rvafc_freq_corr_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080815, "LTE_LL1_DL_RVAFC_FREQ_CORR_CNF", NULL ),

  /* LTE_LL1_DL_PRS_MEAS_CNF, payload: lte_LL1_dl_prs_meas_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080816, "LTE_LL1_DL_PRS_MEAS_CNF", NULL ),

  /* LTE_LL1_DL_PRS_ABORT_CNF, payload: lte_LL1_dl_prs_abort_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080817, "LTE_LL1_DL_PRS_ABORT_CNF", NULL ),

  /* LTE_LL1_DL_PMCH_DECODE_STOP_CNF, payload: lte_LL1_dl_pmch_decode_stop_cnf_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080818, "LTE_LL1_DL_PMCH_DECODE_STOP_CNF", NULL ),

  /* LTE_LL1_DL_IQ_SAMPLE_CAPTURE_CNF, payload: lte_LL1_dl_iq_sample_capture_cnf_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04080819, "LTE_LL1_DL_IQ_SAMPLE_CAPTURE_CNF", NULL ),

  /* LTE_LL1_DL_OPCRS_MEAS_CNF, payload: lte_LL1_dl_opcrs_meas_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0408081a, "LTE_LL1_DL_OPCRS_MEAS_CNF", NULL ),

  /* LTE_LL1_UL_PHYCHAN_CONFIG_REQ, payload: lte_LL1_ul_phychan_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090200, "LTE_LL1_UL_PHYCHAN_CONFIG_REQ", NULL ),

  /* LTE_LL1_UL_PHYCHAN_DECONFIG_REQ, payload: lte_LL1_ul_phychan_deconfig_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090201, "LTE_LL1_UL_PHYCHAN_DECONFIG_REQ", NULL ),

  /* LTE_LL1_UL_PHYCHAN_SCHEDULE_REQ, payload: lte_LL1_ul_phychan_schedule_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090202, "LTE_LL1_UL_PHYCHAN_SCHEDULE_REQ", NULL ),

  /* LTE_LL1_UL_IFFT_SCALING_CHANGE_REQ, payload: lte_LL1_ul_ifft_scaling_change_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090203, "LTE_LL1_UL_IFFT_SCALING_CHANGE_REQ", NULL ),

  /* LTE_LL1_UL_TX_WINDOW_LENGTH_CHANGE_REQ, payload: lte_LL1_ul_tx_window_length_change_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090204, "LTE_LL1_UL_TX_WINDOW_LENGTH_CHANGE_REQ", NULL ),

  /* LTE_LL1_UL_CNTL_INFO_TEST_REQ, payload: lte_LL1_ul_cntl_info_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090205, "LTE_LL1_UL_CNTL_INFO_TEST_REQ", NULL ),

  /* LTE_LL1_UL_CTRL_INFO_BUF_TEST_REQ, payload: lte_LL1_ul_ctrl_info_buf_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090206, "LTE_LL1_UL_CTRL_INFO_BUF_TEST_REQ", NULL ),

  /* LTE_LL1_UL_CSF_CTRL_INFO_TEST_REQ, payload: lte_LL1_ul_csf_ctrl_info_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090207, "LTE_LL1_UL_CSF_CTRL_INFO_TEST_REQ", NULL ),

  /* LTE_LL1_UL_SIM_UL_DL_TEST_REQ, payload: lte_LL1_ul_sim_ul_dl_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090208, "LTE_LL1_UL_SIM_UL_DL_TEST_REQ", NULL ),

  /* LTE_LL1_UL_CAL_CONT_TX_REQ, payload: lte_LL1_ul_cal_cont_tx_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090209, "LTE_LL1_UL_CAL_CONT_TX_REQ", NULL ),

  /* LTE_LL1_UL_TX_LM_CONFIG_REQ, payload: lte_LL1_ul_tx_lm_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0409020a, "LTE_LL1_UL_TX_LM_CONFIG_REQ", NULL ),

  /* LTE_LL1_UL_IQ_SAMP_CAPTURE_REQ, payload: lte_LL1_ul_iq_samp_capture_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0409020b, "LTE_LL1_UL_IQ_SAMP_CAPTURE_REQ", NULL ),

  /* LTE_LL1_UL_ET_RESOURCE_ENABLE_REQ, payload: lte_LL1_ul_et_resource_enable_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x0409020c, "LTE_LL1_UL_ET_RESOURCE_ENABLE_REQ", NULL ),

  /* LTE_LL1_UL_HDET_IND, payload: lte_LL1_ul_hdet_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090400, "LTE_LL1_UL_HDET_IND", NULL ),

  /* LTE_LL1_UL_HDET_FW_RD_IND, payload: lte_LL1_ul_hdet_fw_rd_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090401, "LTE_LL1_UL_HDET_FW_RD_IND", NULL ),

  /* LTE_LL1_UL_THERM_FW_RD_IND, payload: lte_LL1_ul_therm_fw_rd_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090402, "LTE_LL1_UL_THERM_FW_RD_IND", NULL ),

  /* LTE_LL1_UL_PUSCH_TX_STATUS_IND, payload: lte_LL1_ul_pusch_tx_status_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090403, "LTE_LL1_UL_PUSCH_TX_STATUS_IND", NULL ),

  /* LTE_LL1_UL_TDET_TEMP_COMP_FW_RD_IND, payload: lte_LL1_ul_tdet_temp_comp_fw_rd_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090404, "LTE_LL1_UL_TDET_TEMP_COMP_FW_RD_IND", NULL ),

  /* LTE_LL1_UL_PHYCHAN_CONFIG_CNF, payload: lte_LL1_ul_phychan_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090800, "LTE_LL1_UL_PHYCHAN_CONFIG_CNF", NULL ),

  /* LTE_LL1_UL_PHYCHAN_DECONFIG_CNF, payload: lte_LL1_ul_phychan_deconfig_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090801, "LTE_LL1_UL_PHYCHAN_DECONFIG_CNF", NULL ),

  /* LTE_LL1_UL_IFFT_SCALING_CHANGE_CNF, payload: lte_LL1_ul_ifft_scaling_change_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090802, "LTE_LL1_UL_IFFT_SCALING_CHANGE_CNF", NULL ),

  /* LTE_LL1_UL_TX_WINDOW_LENGTH_CHANGE_CNF, payload: lte_LL1_ul_tx_window_length_change_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090803, "LTE_LL1_UL_TX_WINDOW_LENGTH_CHANGE_CNF", NULL ),

  /* LTE_LL1_UL_TX_LM_CONFIG_CNF, payload: lte_LL1_ul_tx_lm_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090804, "LTE_LL1_UL_TX_LM_CONFIG_CNF", NULL ),

  /* LTE_LL1_UL_ASYNC_PHYCHAN_REQUEST_CNF, payload: lte_LL1_ul_aysnc_phychan_request_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090805, "LTE_LL1_UL_ASYNC_PHYCHAN_REQUEST_CNF", NULL ),

  /* LTE_LL1_UL_IQ_SAMP_CAPTURE_CNF, payload: lte_LL1_ul_iq_samp_capture_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090806, "LTE_LL1_UL_IQ_SAMP_CAPTURE_CNF", NULL ),

  /* LTE_LL1_UL_ET_RESOURCE_ENABLE_CNF, payload: lte_LL1_ul_et_resource_enable_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x04090807, "LTE_LL1_UL_ET_RESOURCE_ENABLE_CNF", NULL ),

  /* LTE_LL1_SYS_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0000, "LTE_LL1_SYS_LOOPBACK_SPR", NULL ),

  /* LTE_LL1_SYS_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0001, "LTE_LL1_SYS_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_LL1_SYS_THREAD_READY_SPR, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0002, "LTE_LL1_SYS_THREAD_READY_SPR", NULL ),

  /* LTE_LL1_SYS_THREAD_KILL_SPR, payload: void */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0003, "LTE_LL1_SYS_THREAD_KILL_SPR", NULL ),

  /* LTE_LL1_SYS_CONFIG_SERVING_CELL_REQ, payload: lte_LL1_sys_config_serving_cell_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0200, "LTE_LL1_SYS_CONFIG_SERVING_CELL_REQ", NULL ),

  /* LTE_LL1_SYS_CONFIG_UE_REQ, payload: lte_LL1_sys_config_ue_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0201, "LTE_LL1_SYS_CONFIG_UE_REQ", NULL ),

  /* LTE_LL1_SYS_SLAM_TIMING_REQ, payload: lte_LL1_sys_slam_timing_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0202, "LTE_LL1_SYS_SLAM_TIMING_REQ", NULL ),

  /* LTE_LL1_SYS_ADJ_TIMING_REQ, payload: lte_LL1_sys_adj_timing_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0203, "LTE_LL1_SYS_ADJ_TIMING_REQ", NULL ),

  /* LTE_LL1_SYS_TA_ADJ_REQ, payload: lte_LL1_sys_ta_adj_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0204, "LTE_LL1_SYS_TA_ADJ_REQ", NULL ),

  /* LTE_LL1_SYS_PARAM_CONFIG_REQ, payload: lte_LL1_sys_param_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0205, "LTE_LL1_SYS_PARAM_CONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_STOP_SYS_TIME_REQ, payload: lte_LL1_sys_stop_sys_time_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0206, "LTE_LL1_SYS_STOP_SYS_TIME_REQ", NULL ),

  /* LTE_LL1_SYS_LOG_MASK_CONFIG_REQ, payload: lte_LL1_sys_log_mask_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0207, "LTE_LL1_SYS_LOG_MASK_CONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_CONFIG_APP_REQ, payload: lte_LL1_sys_config_app_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0208, "LTE_LL1_SYS_CONFIG_APP_REQ", NULL ),

  /* LTE_LL1_SYS_SLEEP_REQ, payload: lte_LL1_sys_sleep_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0209, "LTE_LL1_SYS_SLEEP_REQ", NULL ),

  /* LTE_LL1_SYS_RUN_RF_SCRIPT_REQ, payload: lte_LL1_sys_run_rf_script_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020a, "LTE_LL1_SYS_RUN_RF_SCRIPT_REQ", NULL ),

  /* LTE_LL1_SYS_SCELL_STAT_CONFIG_REQ, payload: lte_LL1_sys_scell_stat_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020b, "LTE_LL1_SYS_SCELL_STAT_CONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_SCELL_SEMI_STAT_CONFIG_REQ, payload: lte_LL1_sys_scell_semi_stat_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020c, "LTE_LL1_SYS_SCELL_SEMI_STAT_CONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_GPS_TIMETAG_REQ, payload: lte_LL1_sys_gps_timetag_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020d, "LTE_LL1_SYS_GPS_TIMETAG_REQ", NULL ),

  /* LTE_LL1_SYS_SYNC_STMR_DUMP_REQ, payload: lte_LL1_sys_sync_stmr_dump_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020e, "LTE_LL1_SYS_SYNC_STMR_DUMP_REQ", NULL ),

  /* LTE_LL1_SYS_RUN_RXLM_RF_SCRIPT_REQ, payload: lte_LL1_sys_run_rxlm_rf_script_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a020f, "LTE_LL1_SYS_RUN_RXLM_RF_SCRIPT_REQ", NULL ),

  /* LTE_LL1_SYS_NB_CARRIER_SWITCH_REQ, payload: lte_LL1_sys_nb_carrier_switch_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0210, "LTE_LL1_SYS_NB_CARRIER_SWITCH_REQ", NULL ),

  /* LTE_LL1_SYS_MEMPOOL_BUF_DETAILS_REQ, payload: lte_LL1_sys_mempool_buf_details_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0211, "LTE_LL1_SYS_MEMPOOL_BUF_DETAILS_REQ", NULL ),

  /* LTE_LL1_SYS_SCELL_STAT_DECONFIG_REQ, payload: lte_LL1_sys_scell_stat_deconfig_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0212, "LTE_LL1_SYS_SCELL_STAT_DECONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_CXM_BOOT_PARAMS_REQ, payload: lte_LL1_sys_cxm_boot_params_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0213, "LTE_LL1_SYS_CXM_BOOT_PARAMS_REQ", NULL ),

  /* LTE_LL1_SYS_CXM_ACTIVE_REQ, payload: lte_LL1_sys_cxm_active_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0214, "LTE_LL1_SYS_CXM_ACTIVE_REQ", NULL ),

  /* LTE_LL1_SYS_LIGHT_SLEEP_MOD_CTRL_REQ, payload: lte_LL1_sys_light_sleep_mod_ctrl_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0215, "LTE_LL1_SYS_LIGHT_SLEEP_MOD_CTRL_REQ", NULL ),

  /* LTE_LL1_SYS_RX_TOOL_CHAIN_CONFIG_REQ, payload: lte_LL1_sys_rx_tool_chain_config_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0216, "LTE_LL1_SYS_RX_TOOL_CHAIN_CONFIG_REQ", NULL ),

  /* LTE_LL1_SYS_IQMC_UPDATE_REQ, payload: lte_LL1_sys_iqmc_update_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0217, "LTE_LL1_SYS_IQMC_UPDATE_REQ", NULL ),

  /* LTE_LL1_SYS_CONFLICT_CHECK_REQ, payload: lte_LL1_sys_conflict_check_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0218, "LTE_LL1_SYS_CONFLICT_CHECK_REQ", NULL ),

  /* LTE_LL1_SYS_DRX_ON_DURATION_REQ, payload: lte_LL1_sys_drx_on_duration_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0219, "LTE_LL1_SYS_DRX_ON_DURATION_REQ", NULL ),

  /* LTE_LL1_SYS_DSDA_INTF_TEST_REQ, payload: lte_LL1_sys_dsda_intf_test_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a021a, "LTE_LL1_SYS_DSDA_INTF_TEST_REQ", NULL ),

  /* LTE_LL1_SYS_MULTI_RAT_CHANGE_CXM_PARAMS_REQ, payload: lte_LL1_sys_multi_rat_change_cxm_params_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a021b, "LTE_LL1_SYS_MULTI_RAT_CHANGE_CXM_PARAMS_REQ", NULL ),

  /* LTE_LL1_SYS_RX_ANT_MODE_CHG_REQ, payload: lte_LL1_sys_rx_ant_mode_chg_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a021c, "LTE_LL1_SYS_RX_ANT_MODE_CHG_REQ", NULL ),

  /* LTE_LL1_SYS_ANT_SWITCH_REQ, payload: lte_LL1_sys_ant_switch_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a021d, "LTE_LL1_SYS_ANT_SWITCH_REQ", NULL ),

  /* LTE_LL1_SYS_SUBFRAME_IND, payload: lte_LL1_sys_subframe_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0400, "LTE_LL1_SYS_SUBFRAME_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_PSS_RESULT_LOG_C_IND, payload: lte_LL1_log_srch_pss_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0401, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_PSS_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SSS_RESULT_LOG_C_IND, payload: lte_LL1_log_srch_sss_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0402, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SSS_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UL_PUSCH_TX_REPORT_LOG_C_IND, payload: lte_LL1_log_ul_pusch_tx_report_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0403, "LTE_LL1_SYS_LOG_LTE_LL1_UL_PUSCH_TX_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UL_PUCCH_TX_REPORT_LOG_C_IND, payload: lte_LL1_log_ul_pucch_tx_report_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0404, "LTE_LL1_SYS_LOG_LTE_LL1_UL_PUCCH_TX_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UL_SRS_TX_REPORT_LOG_C_IND, payload: lte_LL1_log_ul_srs_tx_report_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0405, "LTE_LL1_SYS_LOG_LTE_LL1_UL_SRS_TX_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UL_PRACH_TX_REPORT_LOG_C_IND, payload: lte_LL1_log_ul_rach_tx_report_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0406, "LTE_LL1_SYS_LOG_LTE_LL1_UL_PRACH_TX_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_SERVING_CELL_CER_LOG_C_IND, payload: lte_LL1_log_demfront_serving_cell_cer_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0407, "LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_SERVING_CELL_CER_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_PDSCH_DEMAPPER_CONFIGURATION_LOG_C_IND, payload: log_demfront_pdsch_demapper_configuration_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0408, "LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_PDSCH_DEMAPPER_CONFIGURATION_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE127_3R1_RESULT_LOG_C_IND, payload: (lte_LL1_log_csf_spectral_efficiency_for_txmode_1_2_7_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0409, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE127_3R1_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE456_R1_RESULT_LOG_C_IND, payload: (lte_LL1_log_csf_spectral_efficiency_for_txmode_4_5_6_rank_1_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040a, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE456_R1_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE4_R2_RESULT_LOG_C_IND, payload: (lte_LL1_log_csf_spectral_efficiency_for_txmode_4_rank_2_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040b, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE4_R2_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_WHITENED_MATRICES_LOG_C_IND, payload: (lte_LL1_log_csf_whitened_matrices_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040c, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_WHITENED_MATRICES_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_PUCCH_PERIODIC_REPORT_LOG_C_IND, payload: (lte_LL1_log_csf_pucch_report_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040d, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_PUCCH_PERIODIC_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_PUSCH_APERIODIC_REPORT_LOG_C_IND, payload: (lte_LL1_log_csf_pusch_report_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040e, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_PUSCH_APERIODIC_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_MEASUREMENT_RESULT_INT_LOG_C_IND, payload: lte_LL1_log_srch_serving_cell_measurement_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a040f, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_MEASUREMENT_RESULT_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UL_AGC_TX_REPORT_LOG_C_IND, payload: lte_LL1_log_ul_agc_tx_report_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0410, "LTE_LL1_SYS_LOG_LTE_LL1_UL_AGC_TX_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_RXFE_AGC_LOG_C_IND, payload: lte_LL1_log_rxfe_agc_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0411, "LTE_LL1_SYS_LOG_LTE_LL1_RXFE_AGC_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_RXFE_AGC_INT_LOG_C_IND, payload: lte_LL1_log_rxfe_agc_int_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0412, "LTE_LL1_SYS_LOG_LTE_LL1_RXFE_AGC_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_FTL_RESULT_INT_LOG_C_IND, payload: lte_LL1_log_srch_serving_cell_ftl_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0413, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_FTL_RESULT_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_TTL_RESULT_INT_LOG_C_IND, payload: lte_LL1_log_srch_serving_cell_ttl_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0414, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_SERVING_CELL_TTL_RESULT_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_RLM_RESULT_INT_LOG_C_IND, payload: lte_LL1_log_rlm_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0415, "LTE_LL1_SYS_LOG_LTE_LL1_RLM_RESULT_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SRCH_NCELL_MEASUREMENT_AND_TRACKING_INT_LOG_C_IND, payload: lte_LL1_log_srch_neighbor_cell_measurements_and_tracking_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0416, "LTE_LL1_SYS_LOG_LTE_LL1_SRCH_NCELL_MEASUREMENT_AND_TRACKING_INT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_ANT_CORR_RESULT_LOG_C_IND, payload: lte_LL1_log_demfront_antenna_correlation_results_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0417, "LTE_LL1_SYS_LOG_LTE_LL1_ANT_CORR_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_SERVING_CELL_RS_LOG_C_IND, payload: lte_LL1_log_demfront_serving_cell_rs_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0418, "LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_SERVING_CELL_RS_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_RECOVERY_IND, payload: lte_LL1_sys_recovery_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0419, "LTE_LL1_SYS_RECOVERY_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_UERS_TONES_LOG_C_IND, payload: lte_LL1_log_uers_tones_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041a, "LTE_LL1_SYS_LOG_LTE_LL1_UERS_TONES_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_SAMPLE_REC_DONE_IND, payload: lte_LL1_sys_sample_rec_done_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041b, "LTE_LL1_SYS_SAMPLE_REC_DONE_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_NEIGHBOR_CELL_CER_LOG_C_IND, payload: lte_LL1_log_demfront_neighbor_cell_cer_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041c, "LTE_LL1_SYS_LOG_LTE_LL1_DEMFRONT_NEIGHBOR_CELL_CER_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_PRS_FW_LOG_C_IND, payload: lte_LL1_log_prs_fw_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041d, "LTE_LL1_SYS_LOG_LTE_LL1_PRS_FW_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_MBSFN_EMBMS_CER_LOG_C_IND, payload: lte_LL1_log_mbsfn_embms_cer_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041e, "LTE_LL1_SYS_LOG_LTE_LL1_MBSFN_EMBMS_CER_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_MBSFN_WHITENED_MATRICES_LOG_C_IND, payload: lte_LL1_log_mbsfn_whitened_matrices_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a041f, "LTE_LL1_SYS_LOG_LTE_LL1_MBSFN_WHITENED_MATRICES_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_SERVING_CELL_PETL_LOG_C_IND, payload: lte_LL1_log_serving_cell_petl_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0420, "LTE_LL1_SYS_LOG_LTE_LL1_SERVING_CELL_PETL_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LTE_LL1_CXM_WCN_THRESHOLD_EXCEEDED_IND, payload: lte_LL1_sys_cxm_wcn_threshold_exceeded_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0421, "LTE_LL1_SYS_LTE_LL1_CXM_WCN_THRESHOLD_EXCEEDED_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE9_RESULT_LOG_C_IND, payload: (lte_LL1_log_csf_spectral_efficiency_for_txmode_9_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0422, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_SE_TXMODE9_RESULT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_CSF_CSIRS_CHEST_TXMODE9_LOG_C_IND, payload: (lte_LL1_log_csf_csirs_chest_for_txmode_9_ind_msg_struct*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0423, "LTE_LL1_SYS_LOG_LTE_LL1_CSF_CSIRS_CHEST_TXMODE9_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_CONFLICT_CHECK_IND, payload: (lte_LL1_sys_conflict_check_ind_msg_struct) */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0424, "LTE_LL1_SYS_CONFLICT_CHECK_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_MULTI_RAT_DL_REPORT_LOG_C_IND, payload: lte_LL1_log_multi_rat_conflict_dl_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0425, "LTE_LL1_SYS_LOG_LTE_LL1_MULTI_RAT_DL_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_LOG_LTE_LL1_MULTI_RAT_UL_REPORT_LOG_C_IND, payload: lte_LL1_log_multi_rat_conflict_ul_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0426, "LTE_LL1_SYS_LOG_LTE_LL1_MULTI_RAT_UL_REPORT_LOG_C_IND", NULL ),

  /* LTE_LL1_SYS_FTL_FREEZE_IND, payload: lte_LL1_sys_ftl_freeze_ind_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0427, "LTE_LL1_SYS_FTL_FREEZE_IND", NULL ),

  /* LTE_LL1_SYS_CONFIG_SERVING_CELL_CNF, payload: lte_LL1_sys_config_serving_cell_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0800, "LTE_LL1_SYS_CONFIG_SERVING_CELL_CNF", NULL ),

  /* LTE_LL1_SYS_CONFIG_UE_CNF, payload: lte_LL1_sys_config_ue_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0801, "LTE_LL1_SYS_CONFIG_UE_CNF", NULL ),

  /* LTE_LL1_SYS_SLAM_TIMING_CNF, payload: lte_LL1_sys_slam_timing_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0802, "LTE_LL1_SYS_SLAM_TIMING_CNF", NULL ),

  /* LTE_LL1_SYS_ADJ_TIMING_CNF, payload: lte_LL1_sys_adj_timing_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0803, "LTE_LL1_SYS_ADJ_TIMING_CNF", NULL ),

  /* LTE_LL1_SYS_TA_ADJ_CNF, payload: lte_LL1_sys_ta_adj_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0804, "LTE_LL1_SYS_TA_ADJ_CNF", NULL ),

  /* LTE_LL1_SYS_PARAM_CONFIG_CNF, payload: lte_LL1_sys_param_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0805, "LTE_LL1_SYS_PARAM_CONFIG_CNF", NULL ),

  /* LTE_LL1_SYS_STOP_SYS_TIME_CNF, payload: lte_LL1_sys_stop_sys_time_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0806, "LTE_LL1_SYS_STOP_SYS_TIME_CNF", NULL ),

  /* LTE_LL1_SYS_CONFIG_APP_CNF, payload: lte_LL1_sys_config_app_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0807, "LTE_LL1_SYS_CONFIG_APP_CNF", NULL ),

  /* LTE_LL1_SYS_SLEEP_CNF, payload: lte_LL1_sys_sleep_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0808, "LTE_LL1_SYS_SLEEP_CNF", NULL ),

  /* LTE_LL1_SYS_RUN_RF_SCRIPT_CNF, payload: lte_LL1_sys_run_rf_script_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0809, "LTE_LL1_SYS_RUN_RF_SCRIPT_CNF", NULL ),

  /* LTE_LL1_SYS_SCELL_STAT_CONFIG_CNF, payload: lte_LL1_sys_scell_stat_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080a, "LTE_LL1_SYS_SCELL_STAT_CONFIG_CNF", NULL ),

  /* LTE_LL1_SYS_SCELL_SEMI_STAT_CONFIG_CNF, payload: lte_LL1_sys_scell_semi_stat_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080b, "LTE_LL1_SYS_SCELL_SEMI_STAT_CONFIG_CNF", NULL ),

  /* LTE_LL1_SYS_GPS_TIMETAG_CNF, payload: lte_LL1_sys_gps_timetag_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080c, "LTE_LL1_SYS_GPS_TIMETAG_CNF", NULL ),

  /* LTE_LL1_SYS_SYNC_STMR_DUMP_CNF, payload: lte_LL1_sys_sync_stmr_dump_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080d, "LTE_LL1_SYS_SYNC_STMR_DUMP_CNF", NULL ),

  /* LTE_LL1_SYS_RUN_RXLM_RF_SCRIPT_CNF, payload: lte_LL1_sys_run_rxlm_rf_script_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080e, "LTE_LL1_SYS_RUN_RXLM_RF_SCRIPT_CNF", NULL ),

  /* LTE_LL1_SYS_NB_CARRIER_SWITCH_CNF, payload: lte_LL1_sys_nb_carrier_switch_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a080f, "LTE_LL1_SYS_NB_CARRIER_SWITCH_CNF", NULL ),

  /* LTE_LL1_SYS_MEMPOOL_BUF_DETAILS_CNF, payload: lte_LL1_sys_mempool_buf_details_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0810, "LTE_LL1_SYS_MEMPOOL_BUF_DETAILS_CNF", NULL ),

  /* LTE_LL1_SYS_SCELL_STAT_DECONFIG_CNF, payload: lte_LL1_sys_scell_stat_deconfig_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0811, "LTE_LL1_SYS_SCELL_STAT_DECONFIG_CNF", NULL ),

  /* LTE_LL1_SYS_CXM_BOOT_PARAMS_CNF, payload: lte_LL1_sys_cxm_boot_params_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0812, "LTE_LL1_SYS_CXM_BOOT_PARAMS_CNF", NULL ),

  /* LTE_LL1_SYS_CXM_ACTIVE_CNF, payload: lte_LL1_sys_cxm_active_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0813, "LTE_LL1_SYS_CXM_ACTIVE_CNF", NULL ),

  /* LTE_LL1_SYS_LIGHT_SLEEP_MOD_CTRL_CNF, payload: lte_LL1_sys_light_sleep_mod_ctrl_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0814, "LTE_LL1_SYS_LIGHT_SLEEP_MOD_CTRL_CNF", NULL ),

  /* LTE_LL1_SYS_RX_TOOL_CHAIN_CONFIG_CNF, payload: lte_LL1_sys_rx_tool_chain_config_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0815, "LTE_LL1_SYS_RX_TOOL_CHAIN_CONFIG_CNF", NULL ),

  /* LTE_LL1_SYS_CONFLICT_CHECK_CNF, payload: lte_LL1_sys_conflict_check_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0816, "LTE_LL1_SYS_CONFLICT_CHECK_CNF", NULL ),

  /* LTE_LL1_SYS_RX_ANT_MODE_CHG_CNF, payload: lte_LL1_sys_rx_ant_mode_chg_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0817, "LTE_LL1_SYS_RX_ANT_MODE_CHG_CNF", NULL ),

  /* LTE_LL1_SYS_ANT_SWITCH_CNF, payload: lte_LL1_sys_ant_switch_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040a0818, "LTE_LL1_SYS_ANT_SWITCH_CNF", NULL ),

  /* LTE_LL1_ASYNC_START_SYS_TIME_REQ, payload: lte_LL1_async_start_sys_time_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0200, "LTE_LL1_ASYNC_START_SYS_TIME_REQ", NULL ),

  /* LTE_LL1_ASYNC_READ_HW_MEM_REQ, payload: lte_LL1_async_read_hw_mem_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0201, "LTE_LL1_ASYNC_READ_HW_MEM_REQ", NULL ),

  /* LTE_LL1_ASYNC_WRITE_HW_MEM_REQ, payload: lte_LL1_async_write_hw_mem_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0202, "LTE_LL1_ASYNC_WRITE_HW_MEM_REQ", NULL ),

  /* LTE_LL1_ASYNC_RESET_REQ, payload: lte_LL1_async_reset_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0203, "LTE_LL1_ASYNC_RESET_REQ", NULL ),

  /* LTE_LL1_ASYNC_STOP_REQ, payload: lte_LL1_async_stop_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0204, "LTE_LL1_ASYNC_STOP_REQ", NULL ),

  /* LTE_LL1_ASYNC_CONFIG_APP_REQ, payload: lte_LL1_async_config_app_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0205, "LTE_LL1_ASYNC_CONFIG_APP_REQ", NULL ),

  /* LTE_LL1_ASYNC_WAKEUP_REQ, payload: lte_LL1_async_config_app_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0206, "LTE_LL1_ASYNC_WAKEUP_REQ", NULL ),

  /* LTE_LL1_ASYNC_DOG_TIMEOUT_REQ, payload: lte_LL1_async_dog_timeout_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0207, "LTE_LL1_ASYNC_DOG_TIMEOUT_REQ", NULL ),

  /* LTE_LL1_ASYNC_TTRANS_WAKEUP_REQ, payload: lte_LL1_async_ttrans_wakeup_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0208, "LTE_LL1_ASYNC_TTRANS_WAKEUP_REQ", NULL ),

  /* LTE_LL1_ASYNC_PHYCHAN_SCHEDULE_REQ, payload: lte_LL1_ul_phychan_schedule_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0209, "LTE_LL1_ASYNC_PHYCHAN_SCHEDULE_REQ", NULL ),

  /* LTE_LL1_ASYNC_DAC_CAL_REQ, payload: lte_LL1_async_dac_cal_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b020a, "LTE_LL1_ASYNC_DAC_CAL_REQ", NULL ),

  /* LTE_LL1_ASYNC_MULTI_RAT_RAISE_CXM_PRIO_REQ, payload: lte_LL1_async_multi_rat_raise_cxm_prio_req_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b020b, "LTE_LL1_ASYNC_MULTI_RAT_RAISE_CXM_PRIO_REQ", NULL ),

  /* LTE_LL1_ASYNC_START_SYS_TIME_CNF, payload: lte_LL1_async_start_sys_time_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0800, "LTE_LL1_ASYNC_START_SYS_TIME_CNF", NULL ),

  /* LTE_LL1_ASYNC_READ_HW_MEM_CNF, payload: lte_LL1_async_read_hw_mem_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0801, "LTE_LL1_ASYNC_READ_HW_MEM_CNF", NULL ),

  /* LTE_LL1_ASYNC_WRITE_HW_MEM_CNF, payload: lte_LL1_async_write_hw_mem_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0802, "LTE_LL1_ASYNC_WRITE_HW_MEM_CNF", NULL ),

  /* LTE_LL1_ASYNC_RESET_CNF, payload: lte_LL1_async_reset_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0803, "LTE_LL1_ASYNC_RESET_CNF", NULL ),

  /* LTE_LL1_ASYNC_STOP_CNF, payload: lte_LL1_async_stop_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0804, "LTE_LL1_ASYNC_STOP_CNF", NULL ),

  /* LTE_LL1_ASYNC_CONFIG_APP_CNF, payload: lte_LL1_async_config_app_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0805, "LTE_LL1_ASYNC_CONFIG_APP_CNF", NULL ),

  /* LTE_LL1_ASYNC_WAKEUP_CNF, payload: lte_LL1_async_config_app_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0806, "LTE_LL1_ASYNC_WAKEUP_CNF", NULL ),

  /* LTE_LL1_ASYNC_TTRANS_WAKEUP_CNF, payload: lte_LL1_async_ttrans_wakeup_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0807, "LTE_LL1_ASYNC_TTRANS_WAKEUP_CNF", NULL ),

  /* LTE_LL1_ASYNC_DAC_CAL_CNF, payload: lte_LL1_async_dac_cal_cnf_msg_struct */
  MSGR_UMID_TABLE_ENTRY ( 0x040b0808, "LTE_LL1_ASYNC_DAC_CAL_CNF", NULL ),

  /* LTE_CPHY_ACQ_REQ, payload: lte_cphy_acq_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0200, "LTE_CPHY_ACQ_REQ", NULL ),

  /* LTE_CPHY_START_REQ, payload: lte_cphy_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0201, "LTE_CPHY_START_REQ", NULL ),

  /* LTE_CPHY_STOP_REQ, payload: lte_cphy_stop_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0202, "LTE_CPHY_STOP_REQ", NULL ),

  /* LTE_CPHY_ABORT_REQ, payload: lte_cphy_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0203, "LTE_CPHY_ABORT_REQ", NULL ),

  /* LTE_CPHY_COMMON_CFG_REQ, payload: lte_cphy_common_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0204, "LTE_CPHY_COMMON_CFG_REQ", NULL ),

  /* LTE_CPHY_DEDICATED_CFG_REQ, payload: lte_cphy_dedicated_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0205, "LTE_CPHY_DEDICATED_CFG_REQ", NULL ),

  /* LTE_CPHY_CON_RELEASE_REQ, payload: lte_cphy_con_release_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0206, "LTE_CPHY_CON_RELEASE_REQ", NULL ),

  /* LTE_CPHY_HANDOVER_REQ, payload: lte_cphy_handover_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0207, "LTE_CPHY_HANDOVER_REQ", NULL ),

  /* LTE_CPHY_BAND_SCAN_REQ, payload: lte_cphy_band_scan_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0208, "LTE_CPHY_BAND_SCAN_REQ", NULL ),

  /* LTE_CPHY_SYSTEM_SCAN_REQ, payload: lte_cphy_system_scan_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0209, "LTE_CPHY_SYSTEM_SCAN_REQ", NULL ),

  /* LTE_CPHY_CELL_SELECT_REQ, payload: lte_cphy_cell_select_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020a, "LTE_CPHY_CELL_SELECT_REQ", NULL ),

  /* LTE_CPHY_START_RACH_REQ, payload: lte_cphy_start_rach_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020b, "LTE_CPHY_START_RACH_REQ", NULL ),

  /* LTE_CPHY_RACH_RC_REQ, payload: lte_cphy_rach_rc_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020c, "LTE_CPHY_RACH_RC_REQ", NULL ),

  /* LTE_CPHY_RAR_PARAMS_REQ, payload: lte_cphy_rar_params_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020d, "LTE_CPHY_RAR_PARAMS_REQ", NULL ),

  /* LTE_CPHY_RACH_ABORT_REQ, payload: lte_cphy_rach_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020e, "LTE_CPHY_RACH_ABORT_REQ", NULL ),

  /* LTE_CPHY_RACH_CFG_REQ, payload: lte_cphy_rach_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c020f, "LTE_CPHY_RACH_CFG_REQ", NULL ),

  /* LTE_CPHY_OUT_OF_SYNC_REQ, payload: lte_cphy_out_of_sync_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0210, "LTE_CPHY_OUT_OF_SYNC_REQ", NULL ),

  /* LTE_CPHY_APPLY_TA_PARAMS_REQ, payload: lte_cphy_apply_ta_params_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0211, "LTE_CPHY_APPLY_TA_PARAMS_REQ", NULL ),

  /* LTE_CPHY_SIB_SCHED_REQ, payload: lte_cphy_sib_sched_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0212, "LTE_CPHY_SIB_SCHED_REQ", NULL ),

  /* LTE_CPHY_CELL_BAR_REQ, payload: lte_cphy_cell_bar_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0213, "LTE_CPHY_CELL_BAR_REQ", NULL ),

  /* LTE_CPHY_IDLE_DRX_CFG_REQ, payload: lte_cphy_idle_drx_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0214, "LTE_CPHY_IDLE_DRX_CFG_REQ", NULL ),

  /* LTE_CPHY_IDLE_MEAS_CFG_REQ, payload: lte_cphy_idle_meas_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0215, "LTE_CPHY_IDLE_MEAS_CFG_REQ", NULL ),

  /* LTE_CPHY_CONN_MEAS_CFG_REQ, payload: lte_cphy_conn_meas_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0216, "LTE_CPHY_CONN_MEAS_CFG_REQ", NULL ),

  /* LTE_CPHY_CELL_RESEL_CANCEL_REQ, payload: lte_cphy_cell_resel_cancel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0217, "LTE_CPHY_CELL_RESEL_CANCEL_REQ", NULL ),

  /* LTE_CPHY_CANCEL_CONN_REQ, payload: lte_cphy_cancel_conn_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0218, "LTE_CPHY_CANCEL_CONN_REQ", NULL ),

  /* LTE_CPHY_SUSPEND_REQ, payload: lte_cphy_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0219, "LTE_CPHY_SUSPEND_REQ", NULL ),

  /* LTE_CPHY_RESUME_REQ, payload: lte_cphy_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021a, "LTE_CPHY_RESUME_REQ", NULL ),

  /* LTE_CPHY_IRAT_CDMA_SYSTEM_TIME_CHG_REQ, payload: lte_cphy_irat_cdma_system_time_chg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021b, "LTE_CPHY_IRAT_CDMA_SYSTEM_TIME_CHG_REQ", NULL ),

  /* LTE_CPHY_BPLMN_START_REQ, payload: lte_cphy_msg_bplmn_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021c, "LTE_CPHY_BPLMN_START_REQ", NULL ),

  /* LTE_CPHY_BPLMN_STOP_REQ, payload: lte_cphy_msg_bplmn_stop_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021d, "LTE_CPHY_BPLMN_STOP_REQ", NULL ),

  /* LTE_CPHY_BPLMN_CELL_REQ, payload: lte_cphy_msg_bplmn_cell_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021e, "LTE_CPHY_BPLMN_CELL_REQ", NULL ),

  /* LTE_CPHY_BPLMN_SKIP_REQ, payload: lte_cphy_msg_bplmn_skip_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c021f, "LTE_CPHY_BPLMN_SKIP_REQ", NULL ),

  /* LTE_CPHY_SEND_UL_SR_REQ, payload: lte_cphy_send_ul_sr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0220, "LTE_CPHY_SEND_UL_SR_REQ", NULL ),

  /* LTE_CPHY_DEACTIVATE_SPS_REQ, payload: lte_cphy_deactivate_sps_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0221, "LTE_CPHY_DEACTIVATE_SPS_REQ", NULL ),

  /* LTE_CPHY_UL_TO_OOS_REQ, payload: lte_cphy_ul_ta_oos_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0222, "LTE_CPHY_UL_TO_OOS_REQ", NULL ),

  /* LTE_CPHY_TDD_CFG_REQ, payload: lte_cphy_tdd_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0223, "LTE_CPHY_TDD_CFG_REQ", NULL ),

  /* LTE_CPHY_GPS_TIMETAG_REQ, payload: lte_cphy_gps_timetag_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0224, "LTE_CPHY_GPS_TIMETAG_REQ", NULL ),

  /* LTE_CPHY_NMR_INFO_REQ, payload: lte_cphy_nmr_info_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0225, "LTE_CPHY_NMR_INFO_REQ", NULL ),

  /* LTE_CPHY_MAC_DRX_REQ, payload: lte_cphy_mac_drx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0226, "LTE_CPHY_MAC_DRX_REQ", NULL ),

  /* LTE_CPHY_SERV_CELL_MEAS_REQ, payload: lte_cphy_serv_cell_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0227, "LTE_CPHY_SERV_CELL_MEAS_REQ", NULL ),

  /* LTE_CPHY_GPS_MRL_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0228, "LTE_CPHY_GPS_MRL_REQ", NULL ),

  /* LTE_CPHY_GPS_EVENT_CB_REQ, payload: lte_cphy_gps_event_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0229, "LTE_CPHY_GPS_EVENT_CB_REQ", NULL ),

  /* LTE_CPHY_RFCHAIN_REQ, payload: lte_cphy_rfchain_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c022a, "LTE_CPHY_RFCHAIN_REQ", NULL ),

  /* LTE_CPHY_PRS_START_REQ, payload: lte_cphy_prs_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c022d, "LTE_CPHY_PRS_START_REQ", NULL ),

  /* LTE_CPHY_PRS_UPDATE_CELLS_REQ, payload: lte_cphy_prs_update_cells_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c022e, "LTE_CPHY_PRS_UPDATE_CELLS_REQ", NULL ),

  /* LTE_CPHY_PRS_STOP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x040c022f, "LTE_CPHY_PRS_STOP_REQ", NULL ),

  /* LTE_CPHY_ABORT_IRAT_CGI_REQ, payload: lte_cphy_abort_irat_cgi_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0230, "LTE_CPHY_ABORT_IRAT_CGI_REQ", NULL ),

  /* LTE_CPHY_MSI_REQ, payload: lte_cphy_msi_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0232, "LTE_CPHY_MSI_REQ", NULL ),

  /* LTE_CPHY_CDMA_SYSTIME_REQ, payload: lte_cphy_cdma_systime_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0233, "LTE_CPHY_CDMA_SYSTIME_REQ", NULL ),

  /* LTE_CPHY_HRPD_MEAS_RESULTS_REQ, payload: lte_cphy_hrpd_meas_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0234, "LTE_CPHY_HRPD_MEAS_RESULTS_REQ", NULL ),

  /* LTE_CPHY_DRX_OPT_REQ, payload: lte_cphy_drx_opt_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0235, "LTE_CPHY_DRX_OPT_REQ", NULL ),

  /* LTE_CPHY_GPS_TIMEXFER_REQ, payload: lte_cphy_gps_timexfer_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0236, "LTE_CPHY_GPS_TIMEXFER_REQ", NULL ),

  /* LTE_CPHY_GPS_TIMEXFER_ABORT_REQ, payload: lte_cphy_gps_timexfer_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0237, "LTE_CPHY_GPS_TIMEXFER_ABORT_REQ", NULL ),

  /* LTE_CPHY_BPLMN_SUSPEND_REQ, payload: lte_cphy_msg_bplmn_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0238, "LTE_CPHY_BPLMN_SUSPEND_REQ", NULL ),

  /* LTE_CPHY_MBSFN_SIGNAL_STRENGTH_REQ, payload: lte_cphy_mbsfn_signal_strength_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0239, "LTE_CPHY_MBSFN_SIGNAL_STRENGTH_REQ", NULL ),

  /* LTE_CPHY_UEINFO_RPT_REQ, payload: lte_cphy_ueinfo_rpt_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c023a, "LTE_CPHY_UEINFO_RPT_REQ", NULL ),

  /* LTE_CPHY_GPS_RXTX_REQ, payload: lte_cphy_gps_rxtx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c023b, "LTE_CPHY_GPS_RXTX_REQ", NULL ),

  /* LTE_CPHY_UE_INFO_MDT_SESSION_REQ, payload: lte_cphy_ueinfo_mdt_session_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c023d, "LTE_CPHY_UE_INFO_MDT_SESSION_REQ", NULL ),

  /* LTE_CPHY_UTC_TIME_UPDATE_REQ, payload: lte_cphy_utc_time_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c023e, "LTE_CPHY_UTC_TIME_UPDATE_REQ", NULL ),

  /* LTE_CPHY_CELL_UNBAR_REQ, payload: lte_cphy_cell_unbar_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0240, "LTE_CPHY_CELL_UNBAR_REQ", NULL ),

  /* LTE_CPHY_BEST_MBMS_NEIGH_FREQ_REQ, payload: lte_cphy_best_mbms_neigh_freq_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0241, "LTE_CPHY_BEST_MBMS_NEIGH_FREQ_REQ", NULL ),

  /* LTE_CPHY_TRM_PRIORITY_REQ, payload: lte_cphy_trm_priority_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0242, "LTE_CPHY_TRM_PRIORITY_REQ", NULL ),

  /* LTE_CPHY_RELEASE_TRM_REQ, payload: lte_cphy_release_trm_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0243, "LTE_CPHY_RELEASE_TRM_REQ", NULL ),

  /* LTE_CPHY_CDMA_CHANNEL_BAR_REQ, payload: lte_cphy_cdma_channel_bar_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0244, "LTE_CPHY_CDMA_CHANNEL_BAR_REQ", NULL ),

  /* LTE_CPHY_DEBUG_DEADLOCK_TMR_EXP_REQ, payload: lte_cphy_debug_deadlock_tmr_exp_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0245, "LTE_CPHY_DEBUG_DEADLOCK_TMR_EXP_REQ", NULL ),

  /* LTE_CPHY_PDCCH_ORDER_IND, payload: lte_cphy_pdcch_order_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0400, "LTE_CPHY_PDCCH_ORDER_IND", NULL ),

  /* LTE_CPHY_RA_TIMER_EXPIRED_IND, payload: lte_cphy_ra_timer_expired_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0401, "LTE_CPHY_RA_TIMER_EXPIRED_IND", NULL ),

  /* LTE_CPHY_MIB_IND, payload: lte_cphy_mib_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0402, "LTE_CPHY_MIB_IND", NULL ),

  /* LTE_CPHY_MOD_PRD_BND_IND, payload: lte_cphy_mod_prd_boundary_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0403, "LTE_CPHY_MOD_PRD_BND_IND", NULL ),

  /* LTE_CPHY_DL_WEAK_IND, payload: lte_cphy_dl_weak_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0404, "LTE_CPHY_DL_WEAK_IND", NULL ),

  /* LTE_CPHY_CELL_RESEL_IND, payload: lte_cphy_cell_resel_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0405, "LTE_CPHY_CELL_RESEL_IND", NULL ),

  /* LTE_CPHY_OOS_IND, payload: lte_cphy_oos_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0406, "LTE_CPHY_OOS_IND", NULL ),

  /* LTE_CPHY_RL_FAILURE_IND, payload: lte_cphy_rl_failure_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0407, "LTE_CPHY_RL_FAILURE_IND", NULL ),

  /* LTE_CPHY_MEAS_REPORT_IND, payload: lte_cphy_conn_meas_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0408, "LTE_CPHY_MEAS_REPORT_IND", NULL ),

  /* LTE_CPHY_MSG3_TRANSMISSION_IND, payload: (lte_cphy_msg3_transmission_ind_s) */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0409, "LTE_CPHY_MSG3_TRANSMISSION_IND", NULL ),

  /* LTE_CPHY_BPLMN_TIME_AVAIL_IND, payload: lte_cphy_msg_bplmn_time_avail_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040a, "LTE_CPHY_BPLMN_TIME_AVAIL_IND", NULL ),

  /* LTE_CPHY_BPLMN_COMPLETE_IND, payload: lte_cphy_msg_bplmn_complete_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040b, "LTE_CPHY_BPLMN_COMPLETE_IND", NULL ),

  /* LTE_CPHY_UL_PKT_BUILD_IND, payload: lte_cphy_ul_pkt_build_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040c, "LTE_CPHY_UL_PKT_BUILD_IND", NULL ),

  /* LTE_CPHY_UL_PKT_FAILED_IND, payload: lte_cphy_ul_pkt_failed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040d, "LTE_CPHY_UL_PKT_FAILED_IND", NULL ),

  /* LTE_CPHY_PDCCH_ORDER_RACH_SUCCESS_IND, payload: lte_cphy_pdcch_order_rach_sucess_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040e, "LTE_CPHY_PDCCH_ORDER_RACH_SUCCESS_IND", NULL ),

  /* LTE_CPHY_RSSI_IND, payload: lte_cphy_rssi_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c040f, "LTE_CPHY_RSSI_IND", NULL ),

  /* LTE_CPHY_SIGNAL_REPORT_CFG_IND, payload: lte_cphy_signal_report_cfg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0410, "LTE_CPHY_SIGNAL_REPORT_CFG_IND", NULL ),

  /* LTE_CPHY_MAC_RAA_IND, payload: lte_cphy_raa_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0411, "LTE_CPHY_MAC_RAA_IND", NULL ),

  /* LTE_CPHY_T310_STATUS_IND, payload: lte_cphy_t310_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0412, "LTE_CPHY_T310_STATUS_IND", NULL ),

  /* LTE_CPHY_SCELL_STATUS_IND, payload: lte_cphy_scell_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0413, "LTE_CPHY_SCELL_STATUS_IND", NULL ),

  /* LTE_CPHY_RA_ONE_OFF_IND, payload: lte_cphy_ra_one_off_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0414, "LTE_CPHY_RA_ONE_OFF_IND", NULL ),

  /* LTE_CPHY_UE_INFO_MDT_REPORT_IND, payload: lte_cphy_ueinfo_mdt_report_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0415, "LTE_CPHY_UE_INFO_MDT_REPORT_IND", NULL ),

  /* LTE_CPHY_CA_EVENT_IND, payload: lte_cphy_ca_event_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0416, "LTE_CPHY_CA_EVENT_IND", NULL ),

  /* LTE_CPHY_IRAT_ASF_NEEDED_IND, payload: lte_cphy_irat_asf_needed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0417, "LTE_CPHY_IRAT_ASF_NEEDED_IND", NULL ),

  /* LTE_CPHY_RF_AVAILABLE_IND, payload: lte_cphy_rf_available_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0418, "LTE_CPHY_RF_AVAILABLE_IND", NULL ),

  /* LTE_CPHY_RF_UNAVAILABLE_IND, payload: lte_cphy_rf_unavailable_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0419, "LTE_CPHY_RF_UNAVAILABLE_IND", NULL ),

  /* LTE_CPHY_IFREQ_OTDOA_IND, payload: lte_cphy_ifreq_otdoa_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c041a, "LTE_CPHY_IFREQ_OTDOA_IND", NULL ),

  /* LTE_CPHY_BPLMN_SIB_RECEIVED_IND, payload: lte_cphy_msg_bplmn_sib_received_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c041b, "LTE_CPHY_BPLMN_SIB_RECEIVED_IND", NULL ),

  /* LTE_CPHY_BLACKLISTED_CSG_PCI_RANGE_IND, payload: lte_cphy_blacklisted_csg_pci_range_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c041c, "LTE_CPHY_BLACKLISTED_CSG_PCI_RANGE_IND", NULL ),

  /* LTE_CPHY_UE_MODE_CHANGE_IND, payload: lte_cphy_ue_mode_change_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c041d, "LTE_CPHY_UE_MODE_CHANGE_IND", NULL ),

  /* LTE_CPHY_IRAT_CGI_START_IND, payload: lte_cphy_irat_cgi_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c042b, "LTE_CPHY_IRAT_CGI_START_IND", NULL ),

  /* LTE_CPHY_IRAT_CGI_END_IND, payload: lte_cphy_irat_cgi_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c042c, "LTE_CPHY_IRAT_CGI_END_IND", NULL ),

  /* LTE_CPHY_MCCH_CHANGE_NOTIFICATION_IND, payload: lte_cphy_mcch_change_notification_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0431, "LTE_CPHY_MCCH_CHANGE_NOTIFICATION_IND", NULL ),

  /* LTE_CPHY_HANDOVER_SFN_STATUS_IND, payload: lte_cphy_handover_sfn_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0448, "LTE_CPHY_HANDOVER_SFN_STATUS_IND", NULL ),

  /* LTE_CPHY_ACQ_CNF, payload: lte_cphy_acq_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0800, "LTE_CPHY_ACQ_CNF", NULL ),

  /* LTE_CPHY_START_CNF, payload: lte_cphy_start_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0801, "LTE_CPHY_START_CNF", NULL ),

  /* LTE_CPHY_STOP_CNF, payload: lte_cphy_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0802, "LTE_CPHY_STOP_CNF", NULL ),

  /* LTE_CPHY_ABORT_CNF, payload: lte_cphy_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0803, "LTE_CPHY_ABORT_CNF", NULL ),

  /* LTE_CPHY_COMMON_CFG_CNF, payload: lte_cphy_common_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0804, "LTE_CPHY_COMMON_CFG_CNF", NULL ),

  /* LTE_CPHY_DEDICATED_CFG_CNF, payload: lte_cphy_dedicated_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0805, "LTE_CPHY_DEDICATED_CFG_CNF", NULL ),

  /* LTE_CPHY_CON_RELEASE_CNF, payload: lte_cphy_con_release_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0806, "LTE_CPHY_CON_RELEASE_CNF", NULL ),

  /* LTE_CPHY_HANDOVER_CNF, payload: lte_cphy_handover_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0807, "LTE_CPHY_HANDOVER_CNF", NULL ),

  /* LTE_CPHY_BAND_SCAN_CNF, payload: lte_cphy_band_scan_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0808, "LTE_CPHY_BAND_SCAN_CNF", NULL ),

  /* LTE_CPHY_SYSTEM_SCAN_CNF, payload: lte_cphy_system_scan_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0809, "LTE_CPHY_SYSTEM_SCAN_CNF", NULL ),

  /* LTE_CPHY_CELL_SELECT_CNF, payload: lte_cphy_cell_select_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c080a, "LTE_CPHY_CELL_SELECT_CNF", NULL ),

  /* LTE_CPHY_START_RACH_CNF, payload: lte_cphy_start_rach_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c080b, "LTE_CPHY_START_RACH_CNF", NULL ),

  /* LTE_CPHY_RACH_RC_CNF, payload: lte_cphy_rach_rc_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c080c, "LTE_CPHY_RACH_RC_CNF", NULL ),

  /* LTE_CPHY_RACH_ABORT_CNF, payload: lte_cphy_rach_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c080e, "LTE_CPHY_RACH_ABORT_CNF", NULL ),

  /* LTE_CPHY_RACH_CFG_CNF, payload: lte_cphy_rach_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c080f, "LTE_CPHY_RACH_CFG_CNF", NULL ),

  /* LTE_CPHY_IDLE_MEAS_CFG_CNF, payload: lte_cphy_idle_meas_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0815, "LTE_CPHY_IDLE_MEAS_CFG_CNF", NULL ),

  /* LTE_CPHY_CONN_MEAS_CFG_CNF, payload: lte_cphy_conn_meas_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0816, "LTE_CPHY_CONN_MEAS_CFG_CNF", NULL ),

  /* LTE_CPHY_SUSPEND_CNF, payload: lte_cphy_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0819, "LTE_CPHY_SUSPEND_CNF", NULL ),

  /* LTE_CPHY_RESUME_CNF, payload: lte_cphy_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c081a, "LTE_CPHY_RESUME_CNF", NULL ),

  /* LTE_CPHY_BPLMN_STOP_CNF, payload: lte_cphy_msg_bplmn_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c081d, "LTE_CPHY_BPLMN_STOP_CNF", NULL ),

  /* LTE_CPHY_BPLMN_CELL_CNF, payload: lte_cphy_msg_bplmn_cell_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c081e, "LTE_CPHY_BPLMN_CELL_CNF", NULL ),

  /* LTE_CPHY_SEND_UL_SR_CNF, payload: lte_cphy_send_ul_sr_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0820, "LTE_CPHY_SEND_UL_SR_CNF", NULL ),

  /* LTE_CPHY_DEACTIVATE_SPS_CNF, payload: lte_cphy_deactivate_sps_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0821, "LTE_CPHY_DEACTIVATE_SPS_CNF", NULL ),

  /* LTE_CPHY_TDD_CFG_CNF, payload: lte_cphy_tdd_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0823, "LTE_CPHY_TDD_CFG_CNF", NULL ),

  /* LTE_CPHY_NMR_INFO_CNF, payload: lte_cphy_nmr_info_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0825, "LTE_CPHY_NMR_INFO_CNF", NULL ),

  /* LTE_CPHY_SERV_CELL_MEAS_CNF, payload: lte_cphy_serv_cell_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0827, "LTE_CPHY_SERV_CELL_MEAS_CNF", NULL ),

  /* LTE_CPHY_RFCHAIN_CNF, payload: lte_cphy_rfchain_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c082a, "LTE_CPHY_RFCHAIN_CNF", NULL ),

  /* LTE_CPHY_CDMA_SYSTIME_CNF, payload: lte_cphy_cdma_systime_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0833, "LTE_CPHY_CDMA_SYSTIME_CNF", NULL ),

  /* LTE_CPHY_HRPD_MEAS_RESULTS_CNF, payload: lte_cphy_hrpd_meas_results_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0834, "LTE_CPHY_HRPD_MEAS_RESULTS_CNF", NULL ),

  /* LTE_CPHY_BPLMN_SUSPEND_CNF, payload: lte_cphy_msg_bplmn_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0838, "LTE_CPHY_BPLMN_SUSPEND_CNF", NULL ),

  /* LTE_CPHY_MBSFN_SIGNAL_STRENGTH_CNF, payload: lte_cphy_mbsfn_signal_strength_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0839, "LTE_CPHY_MBSFN_SIGNAL_STRENGTH_CNF", NULL ),

  /* LTE_CPHY_UEINFO_RPT_CNF, payload: lte_cphy_ueinfo_rpt_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c083a, "LTE_CPHY_UEINFO_RPT_CNF", NULL ),

  /* LTE_CPHY_GPS_RXTX_CNF, payload: lte_cphy_gps_rxtx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c083c, "LTE_CPHY_GPS_RXTX_CNF", NULL ),

  /* LTE_CPHY_UTC_TIME_UPDATE_CNF, payload: lte_cphy_utc_time_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c083f, "LTE_CPHY_UTC_TIME_UPDATE_CNF", NULL ),

  /* LTE_CPHY_BEST_MBMS_NEIGH_FREQ_CNF, payload: lte_cphy_best_mbms_neigh_freq_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0841, "LTE_CPHY_BEST_MBMS_NEIGH_FREQ_CNF", NULL ),

  /* LTE_CPHY_RELEASE_TRM_CNF, payload: lte_cphy_release_trm_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040c0843, "LTE_CPHY_RELEASE_TRM_CNF", NULL ),

  /* LTE_RRC_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0000, "LTE_RRC_LOOPBACK_SPR", NULL ),

  /* LTE_RRC_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0001, "LTE_RRC_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_RRC_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0002, "LTE_RRC_THREAD_READY_SPR", NULL ),

  /* LTE_RRC_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0003, "LTE_RRC_THREAD_KILL_SPR", NULL ),

  /* LTE_RRC_INT_MSG_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0005, "LTE_RRC_INT_MSG_SPR", NULL ),

  /* LTE_RRC_EXT_MSG_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0006, "LTE_RRC_EXT_MSG_SPR", NULL ),

  /* LTE_RRC_STIMULI_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0007, "LTE_RRC_STIMULI_SPR", NULL ),

  /* LTE_RRC_CONN_EST_REQ, payload: lte_rrc_conn_est_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0200, "LTE_RRC_CONN_EST_REQ", NULL ),

  /* LTE_RRC_UL_DATA_REQ, payload: lte_rrc_ul_data_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0201, "LTE_RRC_UL_DATA_REQ", NULL ),

  /* LTE_RRC_DEACTIVATE_REQ, payload: lte_rrc_deactivate_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0202, "LTE_RRC_DEACTIVATE_REQ", NULL ),

  /* LTE_RRC_CONN_ABORT_REQ, payload: lte_rrc_conn_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0203, "LTE_RRC_CONN_ABORT_REQ", NULL ),

  /* LTE_RRC_SERVICE_REQ, payload: lte_rrc_service_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0204, "LTE_RRC_SERVICE_REQ", NULL ),

  /* LTE_RRC_SYSTEM_UPDATE_REQ, payload: lte_rrc_system_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0205, "LTE_RRC_SYSTEM_UPDATE_REQ", NULL ),

  /* LTE_RRC_SIM_UPDATE_REQ, payload: lte_rrc_sim_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0206, "LTE_RRC_SIM_UPDATE_REQ", NULL ),

  /* LTE_RRC_DRX_INFO_REQ, payload: lte_rrc_drx_info_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0207, "LTE_RRC_DRX_INFO_REQ", NULL ),

  /* LTE_RRC_PLMN_SEARCH_REQ, payload: lte_rrc_plmn_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0208, "LTE_RRC_PLMN_SEARCH_REQ", NULL ),

  /* LTE_RRC_PLMN_SEARCH_ABORT_REQ, payload: lte_rrc_plmn_search_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0209, "LTE_RRC_PLMN_SEARCH_ABORT_REQ", NULL ),

  /* LTE_RRC_NW_SEL_MODE_RESET_REQ, payload: lte_rrc_nw_sel_mode_reset_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020a, "LTE_RRC_NW_SEL_MODE_RESET_REQ", NULL ),

  /* LTE_RRC_NMR_INFO_REQ, payload: lte_rrc_nmr_info_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020b, "LTE_RRC_NMR_INFO_REQ", NULL ),

  /* LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ, payload: lte_rrc_irat_tunnel_ul_msg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020c, "LTE_RRC_IRAT_TUNNEL_UL_MSG_REQ", NULL ),

  /* LTE_RRC_ETWS_MSG_ID_LIST_REQ, payload: lte_rrc_etws_msg_id_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020d, "LTE_RRC_ETWS_MSG_ID_LIST_REQ", NULL ),

  /* LTE_RRC_MSG_ID_LIST_REQ, payload: lte_rrc_msg_id_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020e, "LTE_RRC_MSG_ID_LIST_REQ", NULL ),

  /* LTE_RRC_IM3_BACKOFF_APPLIED_REQ, payload: lte_rrc_im3_backoff_applied_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d020f, "LTE_RRC_IM3_BACKOFF_APPLIED_REQ", NULL ),

  /* LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ, payload: lte_irat_cdma_tunnel_ul_msg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0210, "LTE_RRC_IRAT_HDR_UL_TUNNEL_MSG_REQ", NULL ),

  /* LTE_RRC_EMBMS_ACT_TMGI_REQ, payload: lte_rrc_embms_act_tmgi_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0211, "LTE_RRC_EMBMS_ACT_TMGI_REQ", NULL ),

  /* LTE_RRC_EMBMS_DEACT_TMGI_REQ, payload: lte_rrc_embms_deact_tmgi_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0212, "LTE_RRC_EMBMS_DEACT_TMGI_REQ", NULL ),

  /* LTE_RRC_EMBMS_AVAIL_TMGI_LIST_REQ, payload: lte_rrc_embms_avail_tmgi_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0213, "LTE_RRC_EMBMS_AVAIL_TMGI_LIST_REQ", NULL ),

  /* LTE_RRC_EMBMS_ACT_TMGI_LIST_REQ, payload: lte_rrc_embms_act_tmgi_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0214, "LTE_RRC_EMBMS_ACT_TMGI_LIST_REQ", NULL ),

  /* LTE_RRC_EMBMS_ENABLE_REQ, payload: lte_rrc_embms_enable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0215, "LTE_RRC_EMBMS_ENABLE_REQ", NULL ),

  /* LTE_RRC_EMBMS_DISABLE_REQ, payload: lte_rrc_embms_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0216, "LTE_RRC_EMBMS_DISABLE_REQ", NULL ),

  /* LTE_RRC_EMBMS_COVERAGE_STATE_REQ, payload: lte_rrc_embms_coverage_state_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0217, "LTE_RRC_EMBMS_COVERAGE_STATE_REQ", NULL ),

  /* LTE_RRC_EMBMS_ACT_DEACT_TMGI_REQ, payload: lte_rrc_embms_act_deact_tmgi_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0218, "LTE_RRC_EMBMS_ACT_DEACT_TMGI_REQ", NULL ),

  /* LTE_RRC_PROCEDURE_STATUS_REQ, payload: lte_rrc_procedure_status_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0219, "LTE_RRC_PROCEDURE_STATUS_REQ", NULL ),

  /* LTE_RRC_CELL_BAR_REQ, payload: lte_rrc_cell_bar_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021a, "LTE_RRC_CELL_BAR_REQ", NULL ),

  /* LTE_RRC_FORBIDDEN_TA_LIST_RESET_REQ, payload: lte_rrc_forbidden_ta_list_reset_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021b, "LTE_RRC_FORBIDDEN_TA_LIST_RESET_REQ", NULL ),

  /* LTE_RRC_CAP_FGI_CHANGE_REQ, payload: lte_rrc_cap_fgi_change_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021c, "LTE_RRC_CAP_FGI_CHANGE_REQ", NULL ),

  /* LTE_RRC_EMBMS_SIGNAL_STRENGTH_REQ, payload: lte_rrc_embms_signal_strength_report_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021d, "LTE_RRC_EMBMS_SIGNAL_STRENGTH_REQ", NULL ),

  /* LTE_RRC_BAND_PRI_CHANGE_REQ, payload: lte_rrc_band_pri_change_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021e, "LTE_RRC_BAND_PRI_CHANGE_REQ", NULL ),

  /* LTE_RRC_EMBMS_AVAIL_SAI_LIST_REQ, payload: lte_rrc_embms_avail_sai_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d021f, "LTE_RRC_EMBMS_AVAIL_SAI_LIST_REQ", NULL ),

  /* LTE_RRC_GET_BAND_PRI_LIST_REQ, payload: lte_rrc_get_band_pri_list_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0220, "LTE_RRC_GET_BAND_PRI_LIST_REQ", NULL ),

  /* LTE_RRC_DSDS_TRM_PRIORITY_REQ, payload: lte_dsds_trm_priority_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0221, "LTE_RRC_DSDS_TRM_PRIORITY_REQ", NULL ),

  /* LTE_RRC_CSP_SET_PCI_LOCK_REQ, payload: lte_rrc_csp_set_pci_lock_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0222, "LTE_RRC_CSP_SET_PCI_LOCK_REQ", NULL ),

  /* LTE_RRC_CSP_SET_EARFCN_LOCK_REQ, payload: lte_rrc_csp_set_earfcn_lock_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0223, "LTE_RRC_CSP_SET_EARFCN_LOCK_REQ", NULL ),

  /* LTE_RRC_DS_STATUS_CHANGE_REQ, payload: lte_rrc_ds_status_change_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0224, "LTE_RRC_DS_STATUS_CHANGE_REQ", NULL ),

  /* LTE_RRC_AVOIDANCE_REQ, payload: lte_rrc_avoidance_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0225, "LTE_RRC_AVOIDANCE_REQ", NULL ),

  /* LTE_RRC_CMAPI_EMBMS_COV_STATUS_REQ, payload: lte_rrc_cmapi_embms_cov_status_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0226, "LTE_RRC_CMAPI_EMBMS_COV_STATUS_REQ", NULL ),

  /* LTE_RRC_CMAPI_EMBMS_DATAMCS_REQ, payload: lte_rrc_cmapi_embms_datamcs_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0227, "LTE_RRC_CMAPI_EMBMS_DATAMCS_REQ", NULL ),

  /* LTE_RRC_GET_SERV_CELL_SIB_REQ, payload: lte_rrc_get_serv_cell_sib_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0228, "LTE_RRC_GET_SERV_CELL_SIB_REQ", NULL ),

  /* LTE_RRC_WCDMA_RESEL_REQ, payload: lte_rrc_wcdma_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0280, "LTE_RRC_WCDMA_RESEL_REQ", NULL ),

  /* LTE_RRC_WCDMA_REDIR_REQ, payload: lte_rrc_wcdma_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0281, "LTE_RRC_WCDMA_REDIR_REQ", NULL ),

  /* LTE_RRC_WCDMA_ABORT_RESEL_REQ, payload: lte_rrc_wcdma_abort_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0282, "LTE_RRC_WCDMA_ABORT_RESEL_REQ", NULL ),

  /* LTE_RRC_WCDMA_ABORT_REDIR_REQ, payload: lte_rrc_wcdma_abort_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0283, "LTE_RRC_WCDMA_ABORT_REDIR_REQ", NULL ),

  /* LTE_RRC_G_RESEL_REQ, payload: lte_rrc_G_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0284, "LTE_RRC_G_RESEL_REQ", NULL ),

  /* LTE_RRC_G_ABORT_RESEL_REQ, payload: lte_rrc_G_abort_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0285, "LTE_RRC_G_ABORT_RESEL_REQ", NULL ),

  /* LTE_RRC_G_REDIR_REQ, payload: lte_rrc_G_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0286, "LTE_RRC_G_REDIR_REQ", NULL ),

  /* LTE_RRC_G_ABORT_REDIR_REQ, payload: lte_rrc_G_abort_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0287, "LTE_RRC_G_ABORT_REDIR_REQ", NULL ),

  /* LTE_RRC_EUTRA_CAPABILITIES_REQ, payload: lte_rrc_eutra_capabilities_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0288, "LTE_RRC_EUTRA_CAPABILITIES_REQ", NULL ),

  /* LTE_RRC_WCDMA_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0289, "LTE_RRC_WCDMA_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_WCDMA_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028a, "LTE_RRC_WCDMA_ABORT_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_G_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028b, "LTE_RRC_G_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_G_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028c, "LTE_RRC_G_ABORT_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_GET_DEDICATED_PRI_REQ, payload: lte_rrc_get_dedicated_pri_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028d, "LTE_RRC_GET_DEDICATED_PRI_REQ", NULL ),

  /* LTE_RRC_eHRPD_RESEL_REQ, payload: lte_rrc_eHRPD_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028e, "LTE_RRC_eHRPD_RESEL_REQ", NULL ),

  /* LTE_RRC_eHRPD_ABORT_RESEL_REQ, payload: lte_rrc_eHRPD_abort_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d028f, "LTE_RRC_eHRPD_ABORT_RESEL_REQ", NULL ),

  /* LTE_RRC_1X_RESEL_REQ, payload: lte_rrc_1x_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0290, "LTE_RRC_1X_RESEL_REQ", NULL ),

  /* LTE_RRC_1X_ABORT_RESEL_REQ, payload: lte_rrc_1x_abort_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0291, "LTE_RRC_1X_ABORT_RESEL_REQ", NULL ),

  /* LTE_RRC_WCDMA_PSHO_REQ, payload: lte_rrc_wcdma_psho_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0292, "LTE_RRC_WCDMA_PSHO_REQ", NULL ),

  /* LTE_RRC_WCDMA_ABORT_PSHO_REQ, payload: lte_rrc_wcdma_abort_psho_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0293, "LTE_RRC_WCDMA_ABORT_PSHO_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_RESEL_REQ, payload: lte_rrc_tdscdma_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0294, "LTE_RRC_TDSCDMA_RESEL_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_REDIR_REQ, payload: lte_rrc_tdscdma_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0295, "LTE_RRC_TDSCDMA_REDIR_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_RESEL_REQ, payload: lte_rrc_tdscdma_abort_resel_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0296, "LTE_RRC_TDSCDMA_ABORT_RESEL_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_REDIR_REQ, payload: lte_rrc_tdscdma_abort_redir_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0297, "LTE_RRC_TDSCDMA_ABORT_REDIR_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0298, "LTE_RRC_TDSCDMA_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0299, "LTE_RRC_TDSCDMA_ABORT_PLMN_SRCH_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_PSHO_REQ, payload: lte_rrc_tdscdma_psho_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d029a, "LTE_RRC_TDSCDMA_PSHO_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_PSHO_REQ, payload: lte_rrc_tdscdma_abort_psho_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d029b, "LTE_RRC_TDSCDMA_ABORT_PSHO_REQ", NULL ),

  /* LTE_RRC_WCDMA_GET_PLMN_PRTL_RESULTS_REQ, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d029c, "LTE_RRC_WCDMA_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* LTE_RRC_TDSCDMA_GET_PLMN_PRTL_RESULTS_REQ, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d029d, "LTE_RRC_TDSCDMA_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* LTE_RRC_G_GET_PLMN_PRTL_RESULTS_REQ, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d029e, "LTE_RRC_G_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* LTE_RRC_NAS_UMTS_KEY_RSP, payload: lte_rrc_nas_umts_key_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0301, "LTE_RRC_NAS_UMTS_KEY_RSP", NULL ),

  /* LTE_RRC_NAS_LTE_KEY_RSP, payload: lte_rrc_nas_lte_key_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0302, "LTE_RRC_NAS_LTE_KEY_RSP", NULL ),

  /* LTE_RRC_NAS_GSM_KEY_RSP, payload: lte_rrc_nas_umts_key_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0303, "LTE_RRC_NAS_GSM_KEY_RSP", NULL ),

  /* LTE_RRC_ACTIVATION_RSP, payload: lte_rrc_act_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0380, "LTE_RRC_ACTIVATION_RSP", NULL ),

  /* LTE_RRC_WCDMA_RESEL_FAILED_RSP, payload: lte_rrc_wcdma_resel_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0381, "LTE_RRC_WCDMA_RESEL_FAILED_RSP", NULL ),

  /* LTE_RRC_WCDMA_REDIR_FAILED_RSP, payload: lte_rrc_wcdma_redir_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0382, "LTE_RRC_WCDMA_REDIR_FAILED_RSP", NULL ),

  /* LTE_RRC_WCDMA_ABORT_RESEL_RSP, payload: lte_rrc_wcdma_abort_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0383, "LTE_RRC_WCDMA_ABORT_RESEL_RSP", NULL ),

  /* LTE_RRC_WCDMA_ABORT_REDIR_RSP, payload: lte_rrc_wcdma_abort_redir_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0384, "LTE_RRC_WCDMA_ABORT_REDIR_RSP", NULL ),

  /* LTE_RRC_G_RESEL_FAILED_RSP, payload: lte_rrc_G_resel_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0385, "LTE_RRC_G_RESEL_FAILED_RSP", NULL ),

  /* LTE_RRC_G_REDIR_FAILED_RSP, payload: lte_rrc_G_redir_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0386, "LTE_RRC_G_REDIR_FAILED_RSP", NULL ),

  /* LTE_RRC_G_ABORT_RESEL_RSP, payload: lte_rrc_G_abort_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0387, "LTE_RRC_G_ABORT_RESEL_RSP", NULL ),

  /* LTE_RRC_G_ABORT_REDIR_RSP, payload: lte_rrc_G_abort_redir_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0388, "LTE_RRC_G_ABORT_REDIR_RSP", NULL ),

  /* LTE_RRC_EUTRA_CAPABILITIES_RSP, payload: lte_rrc_eutra_capabilities_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0389, "LTE_RRC_EUTRA_CAPABILITIES_RSP", NULL ),

  /* LTE_RRC_WCDMA_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038a, "LTE_RRC_WCDMA_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_WCDMA_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038b, "LTE_RRC_WCDMA_ABORT_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_G_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038c, "LTE_RRC_G_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_G_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038d, "LTE_RRC_G_ABORT_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_GET_DEDICATED_PRI_RSP, payload: lte_rrc_get_dedicated_pri_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038e, "LTE_RRC_GET_DEDICATED_PRI_RSP", NULL ),

  /* LTE_RRC_eHRPD_RESEL_FAILED_RSP, payload: lte_rrc_eHRPD_resel_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d038f, "LTE_RRC_eHRPD_RESEL_FAILED_RSP", NULL ),

  /* LTE_RRC_eHRPD_ABORT_RESEL_RSP, payload: lte_rrc_eHRPD_abort_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0390, "LTE_RRC_eHRPD_ABORT_RESEL_RSP", NULL ),

  /* LTE_RRC_FROM_CM_ACTIVATION_RSP, payload: lte_rrc_from_CM_act_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0391, "LTE_RRC_FROM_CM_ACTIVATION_RSP", NULL ),

  /* LTE_RRC_1X_RESEL_FAILED_RSP, payload: lte_rrc_1x_resel_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0392, "LTE_RRC_1X_RESEL_FAILED_RSP", NULL ),

  /* LTE_RRC_1X_ABORT_RESEL_RSP, payload: lte_rrc_1x_abort_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0393, "LTE_RRC_1X_ABORT_RESEL_RSP", NULL ),

  /* LTE_RRC_WCDMA_PSHO_RSP, payload: lte_rrc_wcdma_psho_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0394, "LTE_RRC_WCDMA_PSHO_RSP", NULL ),

  /* LTE_RRC_WCDMA_ABORT_PSHO_RSP, payload: lte_rrc_wcdma_abort_psho_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0395, "LTE_RRC_WCDMA_ABORT_PSHO_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_RESEL_FAILED_RSP, payload: lte_rrc_tdscdma_resel_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0396, "LTE_RRC_TDSCDMA_RESEL_FAILED_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_REDIR_FAILED_RSP, payload: lte_rrc_tdscdma_redir_failed_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0397, "LTE_RRC_TDSCDMA_REDIR_FAILED_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_RESEL_RSP, payload: lte_rrc_tdscdma_abort_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0398, "LTE_RRC_TDSCDMA_ABORT_RESEL_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_REDIR_RSP, payload: lte_rrc_tdscdma_abort_redir_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0399, "LTE_RRC_TDSCDMA_ABORT_REDIR_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d039a, "LTE_RRC_TDSCDMA_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d039b, "LTE_RRC_TDSCDMA_ABORT_PLMN_SRCH_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_PSHO_RSP, payload: lte_rrc_tdscdma_psho_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d039c, "LTE_RRC_TDSCDMA_PSHO_RSP", NULL ),

  /* LTE_RRC_TDSCDMA_ABORT_PSHO_RSP, payload: lte_rrc_tdscdma_abort_psho_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d039d, "LTE_RRC_TDSCDMA_ABORT_PSHO_RSP", NULL ),

  /* LTE_RRC_WCDMA_RESEL_RSP, payload: lte_rrc_wcdma_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d039e, "LTE_RRC_WCDMA_RESEL_RSP", NULL ),

  /* LTE_RRC_ACTIVATION_IND, payload: lte_rrc_act_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0400, "LTE_RRC_ACTIVATION_IND", NULL ),

  /* LTE_RRC_SERVICE_IND, payload: lte_rrc_service_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0401, "LTE_RRC_SERVICE_IND", NULL ),

  /* LTE_RRC_CONN_REL_IND, payload: lte_rrc_conn_rel_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0403, "LTE_RRC_CONN_REL_IND", NULL ),

  /* LTE_RRC_DL_DATA_IND, payload: lte_rrc_dl_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0404, "LTE_RRC_DL_DATA_IND", NULL ),

  /* LTE_RRC_PAGE_IND, payload: lte_rrc_page_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0405, "LTE_RRC_PAGE_IND", NULL ),

  /* LTE_RRC_ACTIVE_EPS_BEARER_UPDATE_IND, payload: lte_rrc_active_eps_bearer_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0406, "LTE_RRC_ACTIVE_EPS_BEARER_UPDATE_IND", NULL ),

  /* LTE_RRC_CONN_EST_TIMER_UPDATE_IND, payload: lte_rrc_conn_est_timer_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0407, "LTE_RRC_CONN_EST_TIMER_UPDATE_IND", NULL ),

  /* LTE_RRC_HANDOVER_COMPLETED_IND, payload: lte_rrc_handover_completed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0408, "LTE_RRC_HANDOVER_COMPLETED_IND", NULL ),

  /* LTE_RRC_CONFIG_COMPLETED_IND, payload: lte_rrc_config_completed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0409, "LTE_RRC_CONFIG_COMPLETED_IND", NULL ),

  /* LTE_RRC_ETWS_PRIM_IND, payload: lte_rrc_etws_prim_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d040b, "LTE_RRC_ETWS_PRIM_IND", NULL ),

  /* LTE_RRC_ETWS_SEC_IND, payload: lte_rrc_etws_sec_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d040c, "LTE_RRC_ETWS_SEC_IND", NULL ),

  /* LTE_RRC_IRAT_TUNNEL_DL_MSG_IND, payload: lte_rrc_irat_tunnel_dl_msg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d040d, "LTE_RRC_IRAT_TUNNEL_DL_MSG_IND", NULL ),

  /* LTE_RRC_1XCSFB_HO_STARTED_IND, payload: lte_rrc_1xcsfb_ho_started_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d040e, "LTE_RRC_1XCSFB_HO_STARTED_IND", NULL ),

  /* LTE_RRC_1XCSFB_HO_FAILED_IND, payload: lte_rrc_1xcsfb_ho_failed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d040f, "LTE_RRC_1XCSFB_HO_FAILED_IND", NULL ),

  /* LTE_RRC_NAS_UMTS_KEY_IND, payload: lte_rrc_nas_umts_key_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0410, "LTE_RRC_NAS_UMTS_KEY_IND", NULL ),

  /* LTE_RRC_CMAS_IND, payload: lte_rrc_cmas_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0411, "LTE_RRC_CMAS_IND", NULL ),

  /* LTE_RRC_NAS_LTE_KEY_IND, payload: lte_rrc_nas_lte_key_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0412, "LTE_RRC_NAS_LTE_KEY_IND", NULL ),

  /* LTE_RRC_IRAT_HDR_TUNNEL_DL_MSG_IND, payload: lte_irat_cdma_tunnel_dl_msg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0413, "LTE_RRC_IRAT_HDR_TUNNEL_DL_MSG_IND", NULL ),

  /* LTE_RRC_EMBMS_DEACT_TMGI_IND, payload: lte_rrc_embms_deact_tmgi_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0414, "LTE_RRC_EMBMS_DEACT_TMGI_IND", NULL ),

  /* LTE_RRC_EMBMS_AVAIL_TMGI_LIST_IND, payload: lte_rrc_embms_avail_tmgi_list_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0415, "LTE_RRC_EMBMS_AVAIL_TMGI_LIST_IND", NULL ),

  /* LTE_RRC_EMBMS_ACT_TMGI_LIST_IND, payload: lte_rrc_embms_act_tmgi_list_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0416, "LTE_RRC_EMBMS_ACT_TMGI_LIST_IND", NULL ),

  /* LTE_RRC_EMBMS_COVERAGE_STATE_IND, payload: lte_rrc_embms_coverage_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0417, "LTE_RRC_EMBMS_COVERAGE_STATE_IND", NULL ),

  /* LTE_RRC_EMBMS_STATUS_CHANGE_IND, payload: lte_rrc_embms_status_change_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0418, "LTE_RRC_EMBMS_STATUS_CHANGE_IND", NULL ),

  /* LTE_RRC_EMBMS_OOS_WARN_IND, payload: lte_rrc_embms_oos_warn_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0419, "LTE_RRC_EMBMS_OOS_WARN_IND", NULL ),

  /* LTE_RRC_NAS_GSM_KEY_IND, payload: lte_rrc_nas_umts_key_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041a, "LTE_RRC_NAS_GSM_KEY_IND", NULL ),

  /* LTE_RRC_PROCEDURE_STATUS_IND, payload: lte_rrc_procedure_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041b, "LTE_RRC_PROCEDURE_STATUS_IND", NULL ),

  /* LTE_RRC_UTC_TIME_UPDATE_IND, payload: lte_rrc_utc_time_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041c, "LTE_RRC_UTC_TIME_UPDATE_IND", NULL ),

  /* LTE_RRC_CSG_INFO_UPDATE_IND, payload: lte_rrc_csg_info_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041d, "LTE_RRC_CSG_INFO_UPDATE_IND", NULL ),

  /* LTE_RRC_CSFB_CALL_STATUS_IND, payload: lte_rrc_csfb_call_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041e, "LTE_RRC_CSFB_CALL_STATUS_IND", NULL ),

  /* LTE_RRC_EMBMS_AVAIL_SAI_LIST_IND, payload: lte_rrc_embms_avail_sai_list_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d041f, "LTE_RRC_EMBMS_AVAIL_SAI_LIST_IND", NULL ),

  /* LTE_RRC_UE_MODE_IND, payload: lte_rrc_ue_mode_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0420, "LTE_RRC_UE_MODE_IND", NULL ),

  /* LTE_RRC_SSAC_UPDATE_IND, payload: lte_rrc_ssac_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0421, "LTE_RRC_SSAC_UPDATE_IND", NULL ),

  /* LTE_RRC_1XSRVCC_HO_FAILED_IND, payload: lte_rrc_1xsrvcc_ho_failed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0422, "LTE_RRC_1XSRVCC_HO_FAILED_IND", NULL ),

  /* LTE_RRC_E911_CALL_STATUS_IND, payload: lte_rrc_e911_call_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0423, "LTE_RRC_E911_CALL_STATUS_IND", NULL ),

  /* LTE_RRC_1XSRVCC_HO_STARTED_IND, payload: lte_rrc_1xsrvcc_ho_started_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0424, "LTE_RRC_1XSRVCC_HO_STARTED_IND", NULL ),

  /* LTE_RRC_1X_SIB8_INFO_IND, payload: lte_rrc_1x_sib8_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0481, "LTE_RRC_1X_SIB8_INFO_IND", NULL ),

  /* LTE_RRC_CLEAR_DEDICATED_PRI_IND, payload: lte_rrc_clear_dedicated_pri_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0482, "LTE_RRC_CLEAR_DEDICATED_PRI_IND", NULL ),

  /* LTE_RRC_TO_CM_ACTIVATION_IND, payload: lte_rrc_to_CM_act_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0483, "LTE_RRC_TO_CM_ACTIVATION_IND", NULL ),

  /* LTE_RRC_HDR_SIB8_INFO_IND, payload: lte_rrc_hdr_sib8_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0484, "LTE_RRC_HDR_SIB8_INFO_IND", NULL ),

  /* LTE_RRC_HDR_PRE_REG_INFO_IND, payload: lte_rrc_hdr_pre_reg_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0485, "LTE_RRC_HDR_PRE_REG_INFO_IND", NULL ),

  /* LTE_RRC_CCO_NACC_COMPLETED_IND, payload: lte_rrc_cco_nacc_completed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0486, "LTE_RRC_CCO_NACC_COMPLETED_IND", NULL ),

  /* LTE_RRC_WCDMA_SUSPEND_PLMN_SRCH_IND, payload: lte_irat_suspend_plmn_srch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0487, "LTE_RRC_WCDMA_SUSPEND_PLMN_SRCH_IND", NULL ),

  /* LTE_RRC_G_SUSPEND_PLMN_SRCH_IND, payload: lte_irat_suspend_plmn_srch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0488, "LTE_RRC_G_SUSPEND_PLMN_SRCH_IND", NULL ),

  /* LTE_RRC_TDSCDMA_SUSPEND_PLMN_SRCH_IND, payload: lte_irat_suspend_plmn_srch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0489, "LTE_RRC_TDSCDMA_SUSPEND_PLMN_SRCH_IND", NULL ),

  /* LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0700, "LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_DLM", NULL ),

  /* LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_REJECT_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0701, "LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_REJECT_DLM", NULL ),

  /* LTE_RRC_RRC_CONNECTION_REJECT_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0702, "LTE_RRC_RRC_CONNECTION_REJECT_DLM", NULL ),

  /* LTE_RRC_RRC_CONNECTION_SETUP_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0703, "LTE_RRC_RRC_CONNECTION_SETUP_DLM", NULL ),

  /* LTE_RRC_CSFB_PARAMETERS_RESPONSE_CDMA2000_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0704, "LTE_RRC_CSFB_PARAMETERS_RESPONSE_CDMA2000_DLM", NULL ),

  /* LTE_RRC_DL_INFORMATION_TRANSFER_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0705, "LTE_RRC_DL_INFORMATION_TRANSFER_DLM", NULL ),

  /* LTE_RRC_HANDOVER_FROM_EUTRA_PREPARATION_REQUEST_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0706, "LTE_RRC_HANDOVER_FROM_EUTRA_PREPARATION_REQUEST_DLM", NULL ),

  /* LTE_RRC_MOBILITY_FROM_EUTRA_COMMAND_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0707, "LTE_RRC_MOBILITY_FROM_EUTRA_COMMAND_DLM", NULL ),

  /* LTE_RRC_RRC_CONNECTION_RECONFIGURATION_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0708, "LTE_RRC_RRC_CONNECTION_RECONFIGURATION_DLM", NULL ),

  /* LTE_RRC_RRC_CONNECTION_RELEASE_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0709, "LTE_RRC_RRC_CONNECTION_RELEASE_DLM", NULL ),

  /* LTE_RRC_SECURITY_MODE_COMMAND_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070a, "LTE_RRC_SECURITY_MODE_COMMAND_DLM", NULL ),

  /* LTE_RRC_UE_CAPABILITY_ENQUIRY_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070b, "LTE_RRC_UE_CAPABILITY_ENQUIRY_DLM", NULL ),

  /* LTE_RRC_COUNTER_CHECK_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070c, "LTE_RRC_COUNTER_CHECK_DLM", NULL ),

  /* LTE_RRC_REL9_UE_INFORMATION_REQUEST_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070d, "LTE_RRC_REL9_UE_INFORMATION_REQUEST_DLM", NULL ),

  /* LTE_RRC_REL10_LOGGED_MEAS_CFG_REQUEST_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070e, "LTE_RRC_REL10_LOGGED_MEAS_CFG_REQUEST_DLM", NULL ),

  /* LTE_RRC_REL9_PROXIMITY_INDICATION_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d070f, "LTE_RRC_REL9_PROXIMITY_INDICATION_DLM", NULL ),

  /* LTE_RRC_PAGING_DLM, payload: lte_rrc_mh_dlm_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0710, "LTE_RRC_PAGING_DLM", NULL ),

  /* LTE_RRC_CONN_EST_CNF, payload: lte_rrc_conn_est_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0800, "LTE_RRC_CONN_EST_CNF", NULL ),

  /* LTE_RRC_UL_DATA_CNF, payload: lte_rrc_ul_data_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0801, "LTE_RRC_UL_DATA_CNF", NULL ),

  /* LTE_RRC_DEACTIVATE_CNF, payload: lte_rrc_deactivate_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0802, "LTE_RRC_DEACTIVATE_CNF", NULL ),

  /* LTE_RRC_PLMN_SEARCH_CNF, payload: lte_rrc_plmn_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0803, "LTE_RRC_PLMN_SEARCH_CNF", NULL ),

  /* LTE_RRC_NMR_INFO_CNF, payload: lte_rrc_nmr_info_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0804, "LTE_RRC_NMR_INFO_CNF", NULL ),

  /* LTE_RRC_IRAT_TUNNEL_UL_MSG_CNF, payload: lte_rrc_irat_tunnel_ul_msg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0805, "LTE_RRC_IRAT_TUNNEL_UL_MSG_CNF", NULL ),

  /* LTE_RRC_IRAT_HDR_TUNNEL_UL_MSG_CNF, payload: lte_rrc_irat_tunnel_ul_msg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0806, "LTE_RRC_IRAT_HDR_TUNNEL_UL_MSG_CNF", NULL ),

  /* LTE_RRC_EMBMS_ACT_TMGI_CNF, payload: lte_rrc_embms_act_tmgi_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0807, "LTE_RRC_EMBMS_ACT_TMGI_CNF", NULL ),

  /* LTE_RRC_EMBMS_DEACT_TMGI_CNF, payload: lte_rrc_embms_deact_tmgi_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0808, "LTE_RRC_EMBMS_DEACT_TMGI_CNF", NULL ),

  /* LTE_RRC_EMBMS_ACT_DEACT_TMGI_CNF, payload: lte_rrc_embms_act_deact_tmgi_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0809, "LTE_RRC_EMBMS_ACT_DEACT_TMGI_CNF", NULL ),

  /* LTE_RRC_CAP_FGI_CHANGE_CNF, payload: lte_rrc_cap_fgi_change_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080a, "LTE_RRC_CAP_FGI_CHANGE_CNF", NULL ),

  /* LTE_RRC_EMBMS_SIGNAL_STRENGTH_CNF, payload: lte_rrc_embms_signal_strength_report_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080b, "LTE_RRC_EMBMS_SIGNAL_STRENGTH_CNF", NULL ),

  /* LTE_RRC_BAND_PRI_CHANGE_CNF, payload: lte_rrc_band_pri_change_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080c, "LTE_RRC_BAND_PRI_CHANGE_CNF", NULL ),

  /* LTE_RRC_GET_BAND_PRI_LIST_CNF, payload: lte_rrc_get_band_pri_list_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080d, "LTE_RRC_GET_BAND_PRI_LIST_CNF", NULL ),

  /* LTE_RRC_CSP_SET_PCI_LOCK_CNF, payload: lte_rrc_csp_set_pci_lock_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080e, "LTE_RRC_CSP_SET_PCI_LOCK_CNF", NULL ),

  /* LTE_RRC_CSP_SET_EARFCN_LOCK_CNF, payload: lte_rrc_csp_set_earfcn_lock_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d080f, "LTE_RRC_CSP_SET_EARFCN_LOCK_CNF", NULL ),

  /* LTE_RRC_CMAPI_EMBMS_COV_STATUS_CNF, payload: lte_rrc_cmapi_embms_cov_status_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0810, "LTE_RRC_CMAPI_EMBMS_COV_STATUS_CNF", NULL ),

  /* LTE_RRC_CMAPI_EMBMS_DATAMCS_CNF, payload: lte_rrc_cmapi_embms_datamcs_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0811, "LTE_RRC_CMAPI_EMBMS_DATAMCS_CNF", NULL ),

  /* LTE_RRC_GET_SERV_CELL_SIB_CNF, payload: lte_rrc_get_serv_cell_sib_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d0812, "LTE_RRC_GET_SERV_CELL_SIB_CNF", NULL ),

  /* LTE_RRC_SEND_UL_MSG_REQI, payload: lte_rrc_send_ul_msg_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1200, "LTE_RRC_SEND_UL_MSG_REQI", NULL ),

  /* LTE_RRC_MODE_CHANGE_REQI, payload: lte_rrc_mode_change_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1201, "LTE_RRC_MODE_CHANGE_REQI", NULL ),

  /* LTE_RRC_GET_SIBS_REQI, payload: lte_rrc_get_sibs_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1202, "LTE_RRC_GET_SIBS_REQI", NULL ),

  /* LTE_RRC_SIB_ABORT_REQI, payload: lte_rrc_sib_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1203, "LTE_RRC_SIB_ABORT_REQI", NULL ),

  /* LTE_RRC_INITIATE_CELL_SEL_REQI, payload: lte_rrc_initiate_cell_sel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1204, "LTE_RRC_INITIATE_CELL_SEL_REQI", NULL ),

  /* LTE_RRC_CSP_CELL_SELECT_ABORT_REQI, payload: lte_rrc_csp_cell_select_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1205, "LTE_RRC_CSP_CELL_SELECT_ABORT_REQI", NULL ),

  /* LTE_RRC_CFG_REQI, payload: lte_rrc_cfg_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1206, "LTE_RRC_CFG_REQI", NULL ),

  /* LTE_RRC_MEAS_CFG_REQI, payload: lte_rrc_meas_cfg_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1207, "LTE_RRC_MEAS_CFG_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_ABORT_REQI, payload: lte_rrc_irat_from_lte_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1208, "LTE_RRC_IRAT_FROM_LTE_ABORT_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_REDIR_REQI, payload: lte_rrc_irat_from_lte_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1209, "LTE_RRC_IRAT_FROM_LTE_TO_1X_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_RESEL_REQI, payload: lte_rrc_irat_from_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120a, "LTE_RRC_IRAT_FROM_LTE_TO_1X_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_REDIR_REQI, payload: lte_rrc_irat_from_lte_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120b, "LTE_RRC_IRAT_FROM_LTE_TO_DO_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_RESEL_REQI, payload: lte_rrc_irat_from_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120c, "LTE_RRC_IRAT_FROM_LTE_TO_DO_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_REDIR_REQI, payload: lte_rrc_irat_from_lte_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120d, "LTE_RRC_IRAT_FROM_LTE_TO_G_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_RESEL_REQI, payload: lte_rrc_irat_from_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120e, "LTE_RRC_IRAT_FROM_LTE_TO_G_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_RESEL_REQI, payload: lte_rrc_irat_from_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d120f, "LTE_RRC_IRAT_FROM_LTE_TO_W_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_REDIR_REQI, payload: lte_rrc_irat_from_lte_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1210, "LTE_RRC_IRAT_FROM_LTE_TO_W_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_CAPABILITIES_REQI, payload: lte_rrc_irat_from_lte_to_do_capabilities_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1211, "LTE_RRC_IRAT_FROM_LTE_TO_DO_CAPABILITIES_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_CAPABILITIES_REQI, payload: lte_rrc_irat_from_lte_to_w_capabilities_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1212, "LTE_RRC_IRAT_FROM_LTE_TO_W_CAPABILITIES_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_CS_CAPABILITIES_REQI, payload: lte_rrc_irat_from_lte_to_g_cs_capabilities_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1213, "LTE_RRC_IRAT_FROM_LTE_TO_G_CS_CAPABILITIES_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_PS_CAPABILITIES_REQI, payload: lte_rrc_irat_from_lte_to_g_ps_capabilities_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1214, "LTE_RRC_IRAT_FROM_LTE_TO_G_PS_CAPABILITIES_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_RESEL_REQI, payload: lte_rrc_irat_to_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1215, "LTE_RRC_IRAT_TO_LTE_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_RESEL_ABORT_REQI, payload: lte_rrc_irat_to_lte_resel_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1216, "LTE_RRC_IRAT_TO_LTE_RESEL_ABORT_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_LIST_REQI, payload: lte_rrc_irat_to_lte_redir_list_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1217, "LTE_RRC_IRAT_TO_LTE_REDIR_LIST_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_FULL_REQI, payload: lte_rrc_irat_to_lte_redir_full_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1218, "LTE_RRC_IRAT_TO_LTE_REDIR_FULL_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_ABORT_REQI, payload: lte_rrc_irat_to_lte_redir_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1219, "LTE_RRC_IRAT_TO_LTE_REDIR_ABORT_REQI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_ABORT_REQI, payload: lte_rrc_plmn_search_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121a, "LTE_RRC_PLMN_SEARCH_ABORT_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121b, "LTE_RRC_IRAT_TO_LTE_FROM_G_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_ABORT_PLMN_SRCH_REQI, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121c, "LTE_RRC_IRAT_TO_LTE_FROM_G_ABORT_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121d, "LTE_RRC_IRAT_TO_LTE_FROM_W_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_ABORT_PLMN_SRCH_REQI, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121e, "LTE_RRC_IRAT_TO_LTE_FROM_W_ABORT_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d121f, "LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1220, "LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_ML1_REVOKE_REQI, payload: lte_rrc_ml1_revoke_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1221, "LTE_RRC_ML1_REVOKE_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_CCO_REQI, payload: lte_rrc_irat_from_lte_to_g_cco_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1222, "LTE_RRC_IRAT_FROM_LTE_TO_G_CCO_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_INTERNAL_REDIR_REQI, payload: lte_rrc_irat_from_lte_internal_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1223, "LTE_RRC_IRAT_FROM_LTE_TO_G_INTERNAL_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_GET_CGI_REQI, payload: lte_rrc_irat_from_lte_to_w_get_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1224, "LTE_RRC_IRAT_FROM_LTE_TO_W_GET_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_ABORT_CGI_REQI, payload: lte_rrc_irat_from_lte_to_w_abort_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1225, "LTE_RRC_IRAT_FROM_LTE_TO_W_ABORT_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_GET_CGI_REQI, payload: lte_rrc_irat_from_lte_to_g_get_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1226, "LTE_RRC_IRAT_FROM_LTE_TO_G_GET_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_ABORT_CGI_REQI, payload: lte_rrc_irat_from_lte_to_g_abort_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1227, "LTE_RRC_IRAT_FROM_LTE_TO_G_ABORT_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_REQI, payload: lte_rrc_irat_to_lte_psho_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1228, "LTE_RRC_IRAT_TO_LTE_PSHO_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_REQI, payload: lte_rrc_irat_to_lte_psho_abort_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1229, "LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_REQI", NULL ),

  /* LTE_RRC_ESMGR_ACT_TMGI_REQI, payload: lte_rrc_esmgr_act_tmgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122a, "LTE_RRC_ESMGR_ACT_TMGI_REQI", NULL ),

  /* LTE_RRC_ESMGR_DEACT_TMGI_REQI, payload: lte_rrc_esmgr_deact_tmgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122b, "LTE_RRC_ESMGR_DEACT_TMGI_REQI", NULL ),

  /* LTE_RRC_ESMGR_AVAIL_TMGI_LIST_REQI, payload: lte_rrc_esmgr_avail_tmgi_list_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122c, "LTE_RRC_ESMGR_AVAIL_TMGI_LIST_REQI", NULL ),

  /* LTE_RRC_PENDED_CFG_REQI, payload: lte_rrc_pended_cfg_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122d, "LTE_RRC_PENDED_CFG_REQI", NULL ),

  /* LTE_RRC_START_INTERNAL_CELL_SELECTION_REQI, payload: lte_rrc_start_internal_cell_selection_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122e, "LTE_RRC_START_INTERNAL_CELL_SELECTION_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_CAPABILITIES_REQI, payload: lte_rrc_irat_from_lte_to_tds_capabilities_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d122f, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_CAPABILITIES_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_CGI_REQI, payload: lte_rrc_irat_from_lte_to_tds_get_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1230, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_ABORT_CGI_REQI, payload: lte_rrc_irat_from_lte_to_tds_abort_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1231, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_ABORT_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1232, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_RESEL_REQI, payload: lte_rrc_irat_from_lte_resel_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1233, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_RESEL_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_REDIR_REQI, payload: lte_rrc_irat_from_lte_redir_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1234, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_REDIR_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_PLMN_SRCH_REQI, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1235, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_ABORT_PLMN_SRCH_REQI, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1236, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_ABORT_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_GET_CGI_REQI, payload: lte_rrc_irat_from_lte_to_do_get_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1237, "LTE_RRC_IRAT_FROM_LTE_TO_DO_GET_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_ABORT_CGI_REQI, payload: lte_rrc_irat_from_lte_to_do_abort_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1238, "LTE_RRC_IRAT_FROM_LTE_TO_DO_ABORT_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_GET_CGI_REQI, payload: lte_rrc_irat_from_lte_to_1x_get_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1239, "LTE_RRC_IRAT_FROM_LTE_TO_1X_GET_CGI_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_ABORT_CGI_REQI, payload: lte_rrc_irat_from_lte_to_1x_abort_cgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123a, "LTE_RRC_IRAT_FROM_LTE_TO_1X_ABORT_CGI_REQI", NULL ),

  /* LTE_RRC_ESMGR_ACT_DEACT_TMGI_REQI, payload: lte_rrc_esmgr_act_deact_tmgi_reqi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123b, "LTE_RRC_ESMGR_ACT_DEACT_TMGI_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_SUSPEND_PLMN_SRCH_REQI, payload: lte_irat_suspend_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123c, "LTE_RRC_IRAT_TO_LTE_FROM_G_SUSPEND_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_SUSPEND_PLMN_SRCH_REQI, payload: lte_irat_suspend_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123d, "LTE_RRC_IRAT_TO_LTE_FROM_W_SUSPEND_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_SUSPEND_PLMN_SRCH_REQI, payload: lte_irat_suspend_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123e, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_SUSPEND_PLMN_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d123f, "LTE_RRC_IRAT_FROM_LTE_TO_G_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1240, "LTE_RRC_IRAT_FROM_LTE_TO_W_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1241, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1242, "LTE_RRC_IRAT_TO_LTE_FROM_G_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1243, "LTE_RRC_IRAT_TO_LTE_FROM_W_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_GET_PLMN_PRTL_SRCH_REQI, payload: lte_irat_get_plmn_prtl_results_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1244, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_GET_PLMN_PRTL_SRCH_REQI", NULL ),

  /* LTE_RRC_STOPPED_INDI, payload: lte_rrc_stopped_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1400, "LTE_RRC_STOPPED_INDI", NULL ),

  /* LTE_RRC_UNSOLICITED_SIB_INDI, payload: lte_rrc_unsolicited_sib_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1401, "LTE_RRC_UNSOLICITED_SIB_INDI", NULL ),

  /* LTE_RRC_SIB_UPDATED_INDI, payload: lte_rrc_sib_updated_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1402, "LTE_RRC_SIB_UPDATED_INDI", NULL ),

  /* LTE_RRC_SIB_READ_ERROR_INDI, payload: lte_rrc_sib_read_error_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1403, "LTE_RRC_SIB_READ_ERROR_INDI", NULL ),

  /* LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI, payload: lte_rrc_conn_establishment_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1404, "LTE_RRC_CONN_ESTABLISHMENT_STARTED_INDI", NULL ),

  /* LTE_RRC_CONNECTED_INDI, payload: lte_rrc_connected_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1405, "LTE_RRC_CONNECTED_INDI", NULL ),

  /* LTE_RRC_CONN_ESTABLISHMENT_FAILURE_INDI, payload: lte_rrc_conn_establishment_failure_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1406, "LTE_RRC_CONN_ESTABLISHMENT_FAILURE_INDI", NULL ),

  /* LTE_RRC_STOP_CELL_RESEL_INDI, payload: lte_rrc_stop_cell_resel_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1407, "LTE_RRC_STOP_CELL_RESEL_INDI", NULL ),

  /* LTE_RRC_PROCEED_WITH_RESEL_INDI, payload: lte_rrc_proceed_with_resel_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1408, "LTE_RRC_PROCEED_WITH_RESEL_INDI", NULL ),

  /* LTE_RRC_CONN_REL_STARTED_INDI, payload: lte_rrc_conn_rel_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1409, "LTE_RRC_CONN_REL_STARTED_INDI", NULL ),

  /* LTE_RRC_CONN_RELEASED_INDI, payload: lte_rrc_conn_released_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140a, "LTE_RRC_CONN_RELEASED_INDI", NULL ),

  /* LTE_RRC_NOT_CAMPED_INDI, payload: lte_rrc_not_camped_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140b, "LTE_RRC_NOT_CAMPED_INDI", NULL ),

  /* LTE_RRC_CAMPED_INDI, payload: lte_rrc_camped_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140c, "LTE_RRC_CAMPED_INDI", NULL ),

  /* LTE_RRC_CELL_RESEL_STARTED_INDI, payload: lte_rrc_cell_resel_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140d, "LTE_RRC_CELL_RESEL_STARTED_INDI", NULL ),

  /* LTE_RRC_CELL_RESEL_CANCELED_INDI, payload: lte_rrc_cell_resel_canceled_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140e, "LTE_RRC_CELL_RESEL_CANCELED_INDI", NULL ),

  /* LTE_RRC_CONN_MODE_FAILURE_INDI, payload: lte_rrc_conn_mode_failure_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d140f, "LTE_RRC_CONN_MODE_FAILURE_INDI", NULL ),

  /* LTE_RRC_PENDING_MSG_INDI, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1410, "LTE_RRC_PENDING_MSG_INDI", NULL ),

  /* LTE_RRC_HANDOVER_STARTED_INDI, payload: lte_rrc_handover_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1411, "LTE_RRC_HANDOVER_STARTED_INDI", NULL ),

  /* LTE_RRC_CRE_STARTED_INDI, payload: lte_rrc_cre_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1412, "LTE_RRC_CRE_STARTED_INDI", NULL ),

  /* LTE_RRC_CRE_COMPLETED_INDI, payload: lte_rrc_cre_completed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1413, "LTE_RRC_CRE_COMPLETED_INDI", NULL ),

  /* LTE_RRC_INITIATE_CONN_REL_INDI, payload: lte_rrc_initiate_conn_rel_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1414, "LTE_RRC_INITIATE_CONN_REL_INDI", NULL ),

  /* LTE_RRC_MEAS_PENDING_SIB_UPDATED_INDI, payload: lte_rrc_sib_updated_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1415, "LTE_RRC_MEAS_PENDING_SIB_UPDATED_INDI", NULL ),

  /* LTE_RRC_SMC_FAILURE_INDI, payload: lte_rrc_smc_failure_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1416, "LTE_RRC_SMC_FAILURE_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_REDIR_FAILED_INDI, payload: lte_rrc_irat_from_lte_redir_failed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1417, "LTE_RRC_IRAT_FROM_LTE_REDIR_FAILED_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_RESEL_FAILED_INDI, payload: lte_rrc_irat_from_lte_resel_failed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1418, "LTE_RRC_IRAT_FROM_LTE_RESEL_FAILED_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_RESEL_COMPLETED_INDI, payload: lte_rrc_irat_to_lte_resel_completed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1419, "LTE_RRC_IRAT_TO_LTE_RESEL_COMPLETED_INDI", NULL ),

  /* LTE_RRC_DED_PRIORITY_LIST_INDI, payload: lte_rrc_ded_priority_list_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141a, "LTE_RRC_DED_PRIORITY_LIST_INDI", NULL ),

  /* LTE_RRC_IRAT_UTRA_RESEL_WAIT_TIME_INDI, payload: lte_rrc_irat_utra_resel_wait_time_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141b, "LTE_RRC_IRAT_UTRA_RESEL_WAIT_TIME_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_COMPLETED_INDI, payload: lte_rrc_irat_to_lte_redir_completed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141c, "LTE_RRC_IRAT_TO_LTE_REDIR_COMPLETED_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_PLMN_SRCH_RESUME_FAILED_INDI, payload: lte_rrc_irat_from_lte_plmn_srch_resume_failed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141d, "LTE_RRC_IRAT_FROM_LTE_PLMN_SRCH_RESUME_FAILED_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_MOBILITY_INDI, payload: lte_rrc_irat_from_lte_to_g_mobility_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141e, "LTE_RRC_IRAT_FROM_LTE_TO_G_MOBILITY_INDI", NULL ),

  /* LTE_RRC_DLM_PROCESSED_INDI, payload: lte_rrc_dlm_processed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d141f, "LTE_RRC_DLM_PROCESSED_INDI", NULL ),

  /* LTE_RRC_SERVING_CELL_CHANGED_INDI, payload: lte_rrc_serving_cell_changed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1420, "LTE_RRC_SERVING_CELL_CHANGED_INDI", NULL ),

  /* LTE_RRC_OOS_INDI, payload: lte_rrc_oos_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1421, "LTE_RRC_OOS_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_MOBILITY_INDI, payload: lte_rrc_irat_from_lte_to_1x_mobility_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1422, "LTE_RRC_IRAT_FROM_LTE_TO_1X_MOBILITY_INDI", NULL ),

  /* LTE_RRC_NEW_CELL_INFO_INDI, payload: lte_rrc_new_cell_info_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1423, "LTE_RRC_NEW_CELL_INFO_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_MOBILITY_INDI, payload: lte_rrc_irat_from_lte_to_w_mobility_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1424, "LTE_RRC_IRAT_FROM_LTE_TO_W_MOBILITY_INDI", NULL ),

  /* LTE_RRC_SRB2_RESUMED_INDI, payload: lte_rrc_srb2_resumed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1425, "LTE_RRC_SRB2_RESUMED_INDI", NULL ),

  /* LTE_RRC_IRAT_HRPD_PRE_REG_INFO_INDI, payload: lte_rrc_irat_hrpd_pre_reg_info_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1426, "LTE_RRC_IRAT_HRPD_PRE_REG_INFO_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI, payload: lte_rrc_irat_to_lte_psho_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1427, "LTE_RRC_IRAT_TO_LTE_PSHO_STARTED_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_KEY_GEN_INDI, payload: lte_rrc_irat_to_lte_psho_key_gen_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1428, "LTE_RRC_IRAT_TO_LTE_PSHO_KEY_GEN_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_SUCCESS_INDI, payload: lte_rrc_irat_to_lte_psho_success_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1429, "LTE_RRC_IRAT_TO_LTE_PSHO_SUCCESS_INDI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_COMPLETED_INDI, payload: lte_rrc_irat_to_lte_psho_completed_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142a, "LTE_RRC_IRAT_TO_LTE_PSHO_COMPLETED_INDI", NULL ),

  /* LTE_RRC_EMP_DEACT_TMGI_INDI, payload: lte_rrc_emp_deact_tmgi_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142b, "LTE_RRC_EMP_DEACT_TMGI_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_HDR_MOBILITY_INDI, payload: lte_rrc_irat_from_lte_to_hdr_mobility_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142c, "LTE_RRC_IRAT_FROM_LTE_TO_HDR_MOBILITY_INDI", NULL ),

  /* LTE_RRC_EMP_OOS_WARN_INDI, payload: lte_rrc_emp_oos_warn_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142d, "LTE_RRC_EMP_OOS_WARN_INDI", NULL ),

  /* LTE_RRC_EMP_PENDED_SERVICE_INDI, payload: lte_rrc_emp_pended_service_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142e, "LTE_RRC_EMP_PENDED_SERVICE_INDI", NULL ),

  /* LTE_RRC_ESMGR_AVAIL_TMGI_LIST_INDI, payload: lte_rrc_esmgr_avail_tmgi_list_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d142f, "LTE_RRC_ESMGR_AVAIL_TMGI_LIST_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_MOBILITY_INDI, payload: lte_rrc_irat_from_lte_to_tds_mobility_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1430, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_MOBILITY_INDI", NULL ),

  /* LTE_RRC_IRAT_UTRA_TDD_RESEL_WAIT_TIME_INDI, payload: lte_rrc_irat_utra_resel_tdd_wait_time_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1431, "LTE_RRC_IRAT_UTRA_TDD_RESEL_WAIT_TIME_INDI", NULL ),

  /* LTE_RRC_CSG_CFG_INDI, payload: lte_rrc_csg_cfg_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1432, "LTE_RRC_CSG_CFG_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_SUSPEND_INDI, payload: lte_irat_plmn_srch_suspend_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1433, "LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_SUSPEND_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_SUSPEND_INDI, payload: lte_irat_plmn_srch_suspend_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1434, "LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_SUSPEND_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_SUSPEND_INDI, payload: lte_irat_plmn_srch_suspend_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1435, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_SUSPEND_INDI", NULL ),

  /* LTE_RRC_RAT_PRIORITY_UPDATE_INDI, payload: lte_rrc_rat_priority_update_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1436, "LTE_RRC_RAT_PRIORITY_UPDATE_INDI", NULL ),

  /* LTE_RRC_INTERFREQ_LIST_UPDATE_INDI, payload: lte_rrc_interfreq_list_update_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1437, "LTE_RRC_INTERFREQ_LIST_UPDATE_INDI", NULL ),

  /* LTE_RRC_LLC_MBSFN_CFG_CHANGED_INDI, payload: lte_rrc_llc_mbsfn_cfg_chng_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1438, "LTE_RRC_LLC_MBSFN_CFG_CHANGED_INDI", NULL ),

  /* LTE_RRC_DEPRI_FREQ_INDI, payload: lte_rrc_depri_freq_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1439, "LTE_RRC_DEPRI_FREQ_INDI", NULL ),

  /* LTE_RRC_IRAT_LTE_DEPRI_INDI, payload: lte_rrc_irat_lte_depri_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143a, "LTE_RRC_IRAT_LTE_DEPRI_INDI", NULL ),

  /* LTE_RRC_ESMGR_RESEL_PRIO_CHANGE_INDI, payload: lte_rrc_esmgr_resel_prio_change_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143b, "LTE_RRC_ESMGR_RESEL_PRIO_CHANGE_INDI", NULL ),

  /* LTE_RRC_CGI_SUCCESS_INDI, payload: lte_rrc_cgi_success_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143c, "LTE_RRC_CGI_SUCCESS_INDI", NULL ),

  /* LTE_RRC_TRM_PRIORITY_CHANGE_INDI, payload: lte_rrc_trm_priority_change_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143d, "LTE_RRC_TRM_PRIORITY_CHANGE_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_RESEL_SUCCESS_INDI, payload: lte_rrc_irat_from_lte_resel_success_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143e, "LTE_RRC_IRAT_FROM_LTE_RESEL_SUCCESS_INDI", NULL ),

  /* LTE_RRC_PROX_CFG_INDI, payload: lte_rrc_prox_cfg_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d143f, "LTE_RRC_PROX_CFG_INDI", NULL ),

  /* LTE_RRC_SIB_NOT_RCVD_INDI, payload: lte_rrc_sib_not_rcvd_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1440, "LTE_RRC_SIB_NOT_RCVD_INDI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_MOBILITY_STARTED_INDI, payload: lte_rrc_irat_from_lte_mobility_started_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1441, "LTE_RRC_IRAT_FROM_LTE_MOBILITY_STARTED_INDI", NULL ),

  /* LTE_RRC_CLEAR_DEPRI_FREQ_INDI, payload: lte_rrc_clear_depri_freq_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1442, "LTE_RRC_CLEAR_DEPRI_FREQ_INDI", NULL ),

  /* LTE_RRC_MBSFN_SNR_INDI, payload: lte_rrc_mbsfn_snr_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1443, "LTE_RRC_MBSFN_SNR_INDI", NULL ),

  /* LTE_RRC_SEC_ACTIVE_INDI, payload: lte_rrc_sec_active_indi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1444, "LTE_RRC_SEC_ACTIVE_INDI", NULL ),

  /* LTE_RRC_CSFB_PARAMETERS_REQUEST_CDMA2000_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1500, "LTE_RRC_CSFB_PARAMETERS_REQUEST_CDMA2000_CNFI", NULL ),

  /* LTE_RRC_MEASUREMENT_REPORT_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1501, "LTE_RRC_MEASUREMENT_REPORT_CNFI", NULL ),

  /* LTE_RRC_RRC_CONNECTION_RECONFIGURATION_COMPLETE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1502, "LTE_RRC_RRC_CONNECTION_RECONFIGURATION_COMPLETE_CNFI", NULL ),

  /* LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_COMPLETE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1503, "LTE_RRC_RRC_CONNECTION_REESTABLISHMENT_COMPLETE_CNFI", NULL ),

  /* LTE_RRC_RRC_CONNECTION_SETUP_COMPLETE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1504, "LTE_RRC_RRC_CONNECTION_SETUP_COMPLETE_CNFI", NULL ),

  /* LTE_RRC_SECURITY_MODE_COMPLETE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1505, "LTE_RRC_SECURITY_MODE_COMPLETE_CNFI", NULL ),

  /* LTE_RRC_SECURITY_MODE_FAILURE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1506, "LTE_RRC_SECURITY_MODE_FAILURE_CNFI", NULL ),

  /* LTE_RRC_UE_CAPABILITY_INFORMATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1507, "LTE_RRC_UE_CAPABILITY_INFORMATION_CNFI", NULL ),

  /* LTE_RRC_UL_HANDOVER_PREPARATION_TRANSFER_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1508, "LTE_RRC_UL_HANDOVER_PREPARATION_TRANSFER_CNFI", NULL ),

  /* LTE_RRC_UL_INFORMATION_TRANSFER_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1509, "LTE_RRC_UL_INFORMATION_TRANSFER_CNFI", NULL ),

  /* LTE_RRC_COUNTER_CHECK_RESPONSE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150a, "LTE_RRC_COUNTER_CHECK_RESPONSE_CNFI", NULL ),

  /* LTE_RRC_UE_INFORMATION_RESPONSE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150b, "LTE_RRC_UE_INFORMATION_RESPONSE_CNFI", NULL ),

  /* LTE_RRC_PROXIMITY_INDICATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150c, "LTE_RRC_PROXIMITY_INDICATION_CNFI", NULL ),

  /* LTE_RRC_RN_RECONFIGURATION_COMPLETE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150d, "LTE_RRC_RN_RECONFIGURATION_COMPLETE_CNFI", NULL ),

  /* LTE_RRC_MBMS_COUNTING_RESPONSE_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150e, "LTE_RRC_MBMS_COUNTING_RESPONSE_CNFI", NULL ),

  /* LTE_RRC_INTER_FREQ_RSTD_MEASUREMENT_INDICATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d150f, "LTE_RRC_INTER_FREQ_RSTD_MEASUREMENT_INDICATION_CNFI", NULL ),

  /* LTE_RRC_UE_ASSISTANCE_INFORMATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1510, "LTE_RRC_UE_ASSISTANCE_INFORMATION_CNFI", NULL ),

  /* LTE_RRC_IN_DEVICE_COEX_INDICATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1511, "LTE_RRC_IN_DEVICE_COEX_INDICATION_CNFI", NULL ),

  /* LTE_RRC_MBMS_INTEREST_INDICATION_CNFI, payload: lte_rrc_ul_msg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1512, "LTE_RRC_MBMS_INTEREST_INDICATION_CNFI", NULL ),

  /* LTE_RRC_MODE_CHANGE_CNFI, payload: lte_rrc_mode_change_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1513, "LTE_RRC_MODE_CHANGE_CNFI", NULL ),

  /* LTE_RRC_GET_SIBS_CNFI, payload: lte_rrc_get_sibs_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1514, "LTE_RRC_GET_SIBS_CNFI", NULL ),

  /* LTE_RRC_CSP_CELL_SELECT_ABORT_CNFI, payload: lte_rrc_csp_cell_select_abort_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1515, "LTE_RRC_CSP_CELL_SELECT_ABORT_CNFI", NULL ),

  /* LTE_RRC_CFG_CNFI, payload: lte_rrc_cfg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1516, "LTE_RRC_CFG_CNFI", NULL ),

  /* LTE_RRC_MEAS_CFG_CNFI, payload: lte_rrc_meas_cfg_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1517, "LTE_RRC_MEAS_CFG_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_RESEL_CNFI, payload: lte_rrc_irat_to_lte_resel_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1518, "LTE_RRC_IRAT_TO_LTE_RESEL_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_RESEL_ABORT_CNFI, payload: lte_rrc_irat_to_lte_resel_abort_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1519, "LTE_RRC_IRAT_TO_LTE_RESEL_ABORT_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_DO_TO_LTE_CAPABILITIES_CNFI, payload: lte_rrc_irat_from_do_to_lte_capabilities_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151a, "LTE_RRC_IRAT_FROM_DO_TO_LTE_CAPABILITIES_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_W_TO_LTE_CAPABILITIES_CNFI, payload: lte_rrc_irat_from_w_to_lte_capabilities_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151b, "LTE_RRC_IRAT_FROM_W_TO_LTE_CAPABILITIES_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_G_CS_TO_LTE_CAPABILITIES_CNFI, payload: lte_rrc_irat_from_g_cs_to_lte_capabilities_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151c, "LTE_RRC_IRAT_FROM_G_CS_TO_LTE_CAPABILITIES_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_G_PS_TO_LTE_CAPABILITIES_CNFI, payload: lte_rrc_irat_from_g_ps_to_lte_capabilities_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151d, "LTE_RRC_IRAT_FROM_G_PS_TO_LTE_CAPABILITIES_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_CNFI, payload: lte_rrc_irat_to_lte_redir_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151e, "LTE_RRC_IRAT_TO_LTE_REDIR_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_REDIR_ABORT_CNFI, payload: lte_rrc_irat_to_lte_redir_abort_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d151f, "LTE_RRC_IRAT_TO_LTE_REDIR_ABORT_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1520, "LTE_RRC_IRAT_TO_LTE_FROM_G_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_G_ABORT_PLMN_SRCH_CNFI, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1521, "LTE_RRC_IRAT_TO_LTE_FROM_G_ABORT_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1522, "LTE_RRC_IRAT_TO_LTE_FROM_W_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_W_ABORT_PLMN_SRCH_CNFI, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1523, "LTE_RRC_IRAT_TO_LTE_FROM_W_ABORT_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1524, "LTE_RRC_IRAT_FROM_LTE_TO_G_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1525, "LTE_RRC_IRAT_FROM_LTE_TO_W_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_ABORT_CNFI, payload: lte_rrc_irat_from_lte_abort_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1526, "LTE_RRC_IRAT_FROM_LTE_ABORT_CNFI", NULL ),

  /* LTE_RRC_ML1_REVOKE_CNFI, payload: lte_rrc_ml1_revoke_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1527, "LTE_RRC_ML1_REVOKE_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_W_GET_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_w_get_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1528, "LTE_RRC_IRAT_FROM_LTE_TO_W_GET_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_GET_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_g_get_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152a, "LTE_RRC_IRAT_FROM_LTE_TO_G_GET_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_G_ABORT_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_g_abort_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152b, "LTE_RRC_IRAT_FROM_LTE_TO_G_ABORT_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_DO_GET_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_do_get_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152c, "LTE_RRC_IRAT_FROM_LTE_TO_DO_GET_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_1X_GET_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_1x_get_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152d, "LTE_RRC_IRAT_FROM_LTE_TO_1X_GET_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_CNFI, payload: lte_rrc_irat_to_lte_psho_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152e, "LTE_RRC_IRAT_TO_LTE_PSHO_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_CNFI, payload: lte_rrc_irat_to_lte_psho_abort_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d152f, "LTE_RRC_IRAT_TO_LTE_PSHO_ABORT_CNFI", NULL ),

  /* LTE_RRC_ESMGR_ACT_TMGI_CNFI, payload: lte_rrc_esmgr_act_tmgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1530, "LTE_RRC_ESMGR_ACT_TMGI_CNFI", NULL ),

  /* LTE_RRC_ESMGR_DEACT_TMGI_CNFI, payload: lte_rrc_esmgr_deact_tmgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1531, "LTE_RRC_ESMGR_DEACT_TMGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1532, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_CGI_CNFI, payload: lte_rrc_irat_from_lte_to_tds_get_cgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1533, "LTE_RRC_IRAT_FROM_LTE_TO_TDS_GET_CGI_CNFI", NULL ),

  /* LTE_RRC_IRAT_FROM_TDS_TO_LTE_CAPABILITIES_CNFI, payload: lte_rrc_irat_from_tds_to_lte_capabilities_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1535, "LTE_RRC_IRAT_FROM_TDS_TO_LTE_CAPABILITIES_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_PLMN_SRCH_CNFI, payload: lte_irat_plmn_srch_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1536, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_IRAT_TO_LTE_FROM_TDS_ABORT_PLMN_SRCH_CNFI, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1537, "LTE_RRC_IRAT_TO_LTE_FROM_TDS_ABORT_PLMN_SRCH_CNFI", NULL ),

  /* LTE_RRC_ESMGR_ACT_DEACT_TMGI_CNFI, payload: lte_rrc_esmgr_act_deact_tmgi_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1538, "LTE_RRC_ESMGR_ACT_DEACT_TMGI_CNFI", NULL ),

  /* LTE_RRC_CSG_ASF_SEARCH_CNFI, payload: lte_rrc_csg_asf_search_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1539, "LTE_RRC_CSG_ASF_SEARCH_CNFI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_CNFI, payload: lte_rrc_plmn_search_cnfi_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d153a, "LTE_RRC_PLMN_SEARCH_CNFI", NULL ),

  /* LTE_RRC_SIB_EVENT1_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1600, "LTE_RRC_SIB_EVENT1_WT_TMRI", NULL ),

  /* LTE_RRC_SIB_EVENT2_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1601, "LTE_RRC_SIB_EVENT2_WT_TMRI", NULL ),

  /* LTE_RRC_SIB_3HR_CLOCK_TICK_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1602, "LTE_RRC_SIB_3HR_CLOCK_TICK_TMRI", NULL ),

  /* LTE_RRC_T300_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1603, "LTE_RRC_T300_TMRI", NULL ),

  /* LTE_RRC_T301_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1604, "LTE_RRC_T301_TMRI", NULL ),

  /* LTE_RRC_T302_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1605, "LTE_RRC_T302_TMRI", NULL ),

  /* LTE_RRC_T303_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1606, "LTE_RRC_T303_TMRI", NULL ),

  /* LTE_RRC_T305_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1607, "LTE_RRC_T305_TMRI", NULL ),

  /* LTE_RRC_OOS_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1609, "LTE_RRC_OOS_TMRI", NULL ),

  /* LTE_RRC_T304_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d160b, "LTE_RRC_T304_TMRI", NULL ),

  /* LTE_RRC_CONN_REL_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d160c, "LTE_RRC_CONN_REL_TMRI", NULL ),

  /* LTE_RRC_DEADLOCK_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d160d, "LTE_RRC_DEADLOCK_TMRI", NULL ),

  /* LTE_RRC_CEP_RESEL_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d160e, "LTE_RRC_CEP_RESEL_TMRI", NULL ),

  /* LTE_RRC_T311_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d160f, "LTE_RRC_T311_TMRI", NULL ),

  /* LTE_RRC_SMC_FAIL_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1610, "LTE_RRC_SMC_FAIL_TMRI", NULL ),

  /* LTE_RRC_T320_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1611, "LTE_RRC_T320_TMRI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_GUARD_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1612, "LTE_RRC_PLMN_SEARCH_GUARD_TMRI", NULL ),

  /* LTE_RRC_IRAT_REDIR_LIST_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1613, "LTE_RRC_IRAT_REDIR_LIST_TMRI", NULL ),

  /* LTE_RRC_IRAT_REDIR_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1614, "LTE_RRC_IRAT_REDIR_WAIT_TMRI", NULL ),

  /* LTE_RRC_FROM_G_REDIR_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1615, "LTE_RRC_FROM_G_REDIR_TMRI", NULL ),

  /* LTE_RRC_IRAT_W_RESEL_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1616, "LTE_RRC_IRAT_W_RESEL_WAIT_TMRI", NULL ),

  /* LTE_RRC_PERIODIC_SIB8_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1617, "LTE_RRC_PERIODIC_SIB8_TMRI", NULL ),

  /* LTE_RRC_T321_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1618, "LTE_RRC_T321_TMRI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_ACQ_DB_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1619, "LTE_RRC_PLMN_SEARCH_ACQ_DB_TMRI", NULL ),

  /* LTE_RRC_OOS_SYSTEM_SCAN_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161a, "LTE_RRC_OOS_SYSTEM_SCAN_TMRI", NULL ),

  /* LTE_RRC_IRAT_CGI_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161b, "LTE_RRC_IRAT_CGI_TMRI", NULL ),

  /* LTE_RRC_FROM_G_BLIND_REDIR_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161c, "LTE_RRC_FROM_G_BLIND_REDIR_TMRI", NULL ),

  /* LTE_RRC_FROM_W_BLIND_REDIR_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161d, "LTE_RRC_FROM_W_BLIND_REDIR_TMRI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_MSEARCH_BSCAN_SEP_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161e, "LTE_RRC_PLMN_SEARCH_MSEARCH_BSCAN_SEP_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH0_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d161f, "LTE_RRC_EMP_MCCH0_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH1_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1620, "LTE_RRC_EMP_MCCH1_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH2_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1621, "LTE_RRC_EMP_MCCH2_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH3_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1622, "LTE_RRC_EMP_MCCH3_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH4_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1623, "LTE_RRC_EMP_MCCH4_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH5_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1624, "LTE_RRC_EMP_MCCH5_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH6_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1625, "LTE_RRC_EMP_MCCH6_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_MCCH7_WT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1626, "LTE_RRC_EMP_MCCH7_WT_TMRI", NULL ),

  /* LTE_RRC_EMP_OOS_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1627, "LTE_RRC_EMP_OOS_TMRI", NULL ),

  /* LTE_RRC_FROM_TDS_BLIND_REDIR_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1628, "LTE_RRC_FROM_TDS_BLIND_REDIR_TMRI", NULL ),

  /* LTE_RRC_IRAT_TDS_RESEL_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1629, "LTE_RRC_IRAT_TDS_RESEL_WAIT_TMRI", NULL ),

  /* LTE_RRC_CTLR_DEADLOCK_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162a, "LTE_RRC_CTLR_DEADLOCK_TMRI", NULL ),

  /* LTE_RRC_RECONFIG_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162b, "LTE_RRC_RECONFIG_WAIT_TMRI", NULL ),

  /* LTE_RRC_CSG_ASF_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162c, "LTE_RRC_CSG_ASF_TMRI", NULL ),

  /* LTE_RRC_T306_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162d, "LTE_RRC_T306_TMRI", NULL ),

  /* LTE_RRC_UEINFO_RPT_VALID_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162e, "LTE_RRC_UEINFO_RPT_VALID_TMRI", NULL ),

  /* LTE_RRC_UEINFO_CONN_FAILURE_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d162f, "LTE_RRC_UEINFO_CONN_FAILURE_TMRI", NULL ),

  /* LTE_RRC_MDT_RPT_VALID_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1630, "LTE_RRC_MDT_RPT_VALID_TMRI", NULL ),

  /* LTE_RRC_T330_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1631, "LTE_RRC_T330_TMRI", NULL ),

  /* LTE_RRC_T325_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1632, "LTE_RRC_T325_TMRI", NULL ),

  /* LTE_RRC_RLF_SYSTEM_SCAN_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1633, "LTE_RRC_RLF_SYSTEM_SCAN_TMRI", NULL ),

  /* LTE_RRC_SYSTEM_SCAN_PRIOR_T311_EXPIRY_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1634, "LTE_RRC_SYSTEM_SCAN_PRIOR_T311_EXPIRY_TMRI", NULL ),

  /* LTE_RRC_PERIODIC_SIB16_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1635, "LTE_RRC_PERIODIC_SIB16_TMRI", NULL ),

  /* LTE_RRC_ACT_TMGI_GUARD_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1636, "LTE_RRC_ACT_TMGI_GUARD_TMRI", NULL ),

  /* LTE_RRC_MBMS_INTEREST_IND_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1637, "LTE_RRC_MBMS_INTEREST_IND_TMRI", NULL ),

  /* LTE_RRC_CEP_RF_UNAVAILABLE_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1638, "LTE_RRC_CEP_RF_UNAVAILABLE_TMRI", NULL ),

  /* LTE_RRC_ESMGR_CLEAR_CFL_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d1639, "LTE_RRC_ESMGR_CLEAR_CFL_WAIT_TMRI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_FIRSTP_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d163a, "LTE_RRC_PLMN_SEARCH_FIRSTP_TMRI", NULL ),

  /* LTE_RRC_PLMN_SEARCH_MOREP_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d163b, "LTE_RRC_PLMN_SEARCH_MOREP_TMRI", NULL ),

  /* LTE_RRC_ESMGR_SLTE_IN_COV_WAIT_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d163c, "LTE_RRC_ESMGR_SLTE_IN_COV_WAIT_TMRI", NULL ),

  /* LTE_RRC_TRM_HIGH_PRIORITY_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d163d, "LTE_RRC_TRM_HIGH_PRIORITY_TMRI", NULL ),

  /* LTE_RRC_CEP_ACB_DELAY_RACH_TMRI, payload: lte_rrc_tmri_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040d163e, "LTE_RRC_CEP_ACB_DELAY_RACH_TMRI", NULL ),

  /* LTE_MAC_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0000, "LTE_MAC_LOOPBACK_SPR", NULL ),

  /* LTE_MAC_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0001, "LTE_MAC_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_MAC_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0002, "LTE_MAC_THREAD_READY_SPR", NULL ),

  /* LTE_MAC_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0003, "LTE_MAC_THREAD_KILL_SPR", NULL ),

  /* LTE_MAC_ACCESS_REQ, payload: lte_mac_access_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0200, "LTE_MAC_ACCESS_REQ", NULL ),

  /* LTE_MAC_CFG_REQ, payload: lte_mac_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0201, "LTE_MAC_CFG_REQ", NULL ),

  /* LTE_MAC_ACCESS_ABORT_REQ, payload: lte_mac_access_abort_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0202, "LTE_MAC_ACCESS_ABORT_REQ", NULL ),

  /* LTE_MAC_CANCEL_CONN_REQ, payload: lte_mac_cancel_conn_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0203, "LTE_MAC_CANCEL_CONN_REQ", NULL ),

  /* LTE_MAC_FC_UL_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0204, "LTE_MAC_FC_UL_REQ", NULL ),

  /* LTE_MAC_START_REQ, payload: lte_mac_start_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0205, "LTE_MAC_START_REQ", NULL ),

  /* LTE_MAC_STOP_REQ, payload: lte_mac_stop_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0206, "LTE_MAC_STOP_REQ", NULL ),

  /* LTE_MAC_STATS_UPDATE_REQ, payload: lte_mac_stats_update_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0207, "LTE_MAC_STATS_UPDATE_REQ", NULL ),

  /* LTE_MAC_RACH_RPT_REQ, payload: lte_mac_rach_rpt_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0208, "LTE_MAC_RACH_RPT_REQ", NULL ),

  /* LTE_MAC_CONTENTION_RESULT_IND, payload: lte_mac_contention_result_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0400, "LTE_MAC_CONTENTION_RESULT_IND", NULL ),

  /* LTE_MAC_SEND_PHR_FAILED_IND, payload: lte_mac_send_phr_failed_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0401, "LTE_MAC_SEND_PHR_FAILED_IND", NULL ),

  /* LTE_MAC_RANDOM_ACCESS_PROBLEM_IND, payload: lte_mac_random_access_problem_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0402, "LTE_MAC_RANDOM_ACCESS_PROBLEM_IND", NULL ),

  /* LTE_MAC_RLC_DL_DATA_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0403, "LTE_MAC_RLC_DL_DATA_IND", NULL ),

  /* LTE_MAC_RRC_PCCH_DL_DATA_IND, payload: lte_mac_rrc_dl_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0404, "LTE_MAC_RRC_PCCH_DL_DATA_IND", NULL ),

  /* LTE_MAC_RRC_CCCH_DL_DATA_IND, payload: lte_mac_rrc_dl_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0405, "LTE_MAC_RRC_CCCH_DL_DATA_IND", NULL ),

  /* LTE_MAC_RRC_BCCH_DL_DATA_IND, payload: lte_mac_rrc_dl_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0406, "LTE_MAC_RRC_BCCH_DL_DATA_IND", NULL ),

  /* LTE_MAC_A2_DL_PHY_DATA_IND, payload: lte_mac_a2_dl_phy_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0407, "LTE_MAC_A2_DL_PHY_DATA_IND", NULL ),

  /* LTE_MAC_RELEASE_RESOURCES_IND, payload: lte_mac_release_resources_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0408, "LTE_MAC_RELEASE_RESOURCES_IND", NULL ),

  /* LTE_MAC_BCAST_DATA_IND, payload: lte_mac_bcast_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0409, "LTE_MAC_BCAST_DATA_IND", NULL ),

  /* LTE_MAC_DUAL_DATA_IND, payload: lte_mac_dual_data_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f040a, "LTE_MAC_DUAL_DATA_IND", NULL ),

  /* LTE_MAC_LCID_PRIORITY_IND, payload: lte_mac_lcid_priority_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f040b, "LTE_MAC_LCID_PRIORITY_IND", NULL ),

  /* LTE_MAC_MAX_HARQ_RETX_IND, payload: lte_mac_max_harq_retx_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f040c, "LTE_MAC_MAX_HARQ_RETX_IND", NULL ),

  /* LTE_MAC_ACCESS_CNF, payload: lte_mac_access_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0800, "LTE_MAC_ACCESS_CNF", NULL ),

  /* LTE_MAC_CFG_CNF, payload: lte_mac_cfg_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0801, "LTE_MAC_CFG_CNF", NULL ),

  /* LTE_MAC_ACCESS_ABORT_CNF, payload: lte_mac_access_abort_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0802, "LTE_MAC_ACCESS_ABORT_CNF", NULL ),

  /* LTE_MAC_START_CNF, payload: lte_mac_start_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0803, "LTE_MAC_START_CNF", NULL ),

  /* LTE_MAC_STOP_CNF, payload: lte_mac_stop_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0804, "LTE_MAC_STOP_CNF", NULL ),

  /* LTE_MAC_STATS_UPDATE_CNF, payload: lte_mac_stats_update_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0805, "LTE_MAC_STATS_UPDATE_CNF", NULL ),

  /* LTE_MAC_RACH_RPT_CNF, payload: lte_mac_rach_rpt_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x040f0806, "LTE_MAC_RACH_RPT_CNF", NULL ),

  /* LTE_MAC_DL_CONFIG_REQI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1200, "LTE_MAC_DL_CONFIG_REQI", NULL ),

  /* LTE_MAC_RANDOM_ACCESS_REQI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1201, "LTE_MAC_RANDOM_ACCESS_REQI", NULL ),

  /* LTE_MAC_ACCESS_ABORT_REQI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1202, "LTE_MAC_ACCESS_ABORT_REQI", NULL ),

  /* LTE_MAC_EMBMS_CONFIG_REQI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1203, "LTE_MAC_EMBMS_CONFIG_REQI", NULL ),

  /* LTE_MAC_QSH_ANALYSIS_REQI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1204, "LTE_MAC_QSH_ANALYSIS_REQI", NULL ),

  /* LTE_MAC_MSG4_RESULT_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1400, "LTE_MAC_MSG4_RESULT_INDI", NULL ),

  /* LTE_MAC_MONITOR_RAR_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1401, "LTE_MAC_MONITOR_RAR_INDI", NULL ),

  /* LTE_MAC_PRACH_RESP_RESULT_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1402, "LTE_MAC_PRACH_RESP_RESULT_INDI", NULL ),

  /* LTE_MAC_INFORM_ACTION_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1403, "LTE_MAC_INFORM_ACTION_INDI", NULL ),

  /* LTE_MAC_FLUSH_UL_TB_LOG_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1404, "LTE_MAC_FLUSH_UL_TB_LOG_INDI", NULL ),

  /* LTE_MAC_EXT_HIGH_DATA_ARRIVAL_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1405, "LTE_MAC_EXT_HIGH_DATA_ARRIVAL_INDI", NULL ),

  /* LTE_MAC_UL_CONFIG_INDI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1406, "LTE_MAC_UL_CONFIG_INDI", NULL ),

  /* LTE_MAC_DL_CONFIG_CNFI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1500, "LTE_MAC_DL_CONFIG_CNFI", NULL ),

  /* LTE_MAC_RANDOM_ACCESS_CNFI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1501, "LTE_MAC_RANDOM_ACCESS_CNFI", NULL ),

  /* LTE_MAC_ACCESS_ABORT_CNFI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1502, "LTE_MAC_ACCESS_ABORT_CNFI", NULL ),

  /* LTE_MAC_EMBMS_CONFIG_CNFI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1503, "LTE_MAC_EMBMS_CONFIG_CNFI", NULL ),

  /* LTE_MAC_UL_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1600, "LTE_MAC_UL_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_DL_TA_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1601, "LTE_MAC_DL_TA_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_DL_LOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1602, "LTE_MAC_DL_LOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_CTRL_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1603, "LTE_MAC_CTRL_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_TEST_UTIL_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1604, "LTE_MAC_TEST_UTIL_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_DL_WDOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1605, "LTE_MAC_DL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_UL_WDOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1606, "LTE_MAC_UL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_MAC_CTRL_WDOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x040f1607, "LTE_MAC_CTRL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_RLCDL_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110000, "LTE_RLCDL_LOOPBACK_SPR", NULL ),

  /* LTE_RLCDL_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110001, "LTE_RLCDL_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_RLCDL_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04110002, "LTE_RLCDL_THREAD_READY_SPR", NULL ),

  /* LTE_RLCDL_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04110003, "LTE_RLCDL_THREAD_KILL_SPR", NULL ),

  /* LTE_RLCDL_CFG_REQ, payload: lte_rlcdl_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110200, "LTE_RLCDL_CFG_REQ", NULL ),

  /* LTE_RLCDL_START_REQ, payload: lte_rlc_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110201, "LTE_RLCDL_START_REQ", NULL ),

  /* LTE_RLCDL_STOP_REQ, payload: lte_rlc_stop_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110202, "LTE_RLCDL_STOP_REQ", NULL ),

  /* LTE_RLCDL_FC_REQ, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110203, "LTE_RLCDL_FC_REQ", NULL ),

  /* LTE_RLCDL_STATS_REQ, payload: lte_rlcdl_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110204, "LTE_RLCDL_STATS_REQ", NULL ),

  /* LTE_RLCDL_QSH_REQ, payload: lte_rlcdl_qsh_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110205, "LTE_RLCDL_QSH_REQ", NULL ),

  /* LTE_RLCDL_DATA_IND, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110400, "LTE_RLCDL_DATA_IND", NULL ),

  /* LTE_RLCDL_CTRL_PDU_IND, payload: lte_rlcdl_ctrl_pdu_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110401, "LTE_RLCDL_CTRL_PDU_IND", NULL ),

  /* LTE_RLCDL_MCCH_PDU_IND, payload: lte_rlcdl_mcch_pdu_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110402, "LTE_RLCDL_MCCH_PDU_IND", NULL ),

  /* LTE_RLCDL_CFG_CNF, payload: lte_rlc_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110800, "LTE_RLCDL_CFG_CNF", NULL ),

  /* LTE_RLCDL_START_CNF, payload: lte_rlc_start_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110801, "LTE_RLCDL_START_CNF", NULL ),

  /* LTE_RLCDL_STOP_CNF, payload: lte_rlc_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110802, "LTE_RLCDL_STOP_CNF", NULL ),

  /* LTE_RLCDL_STATS_CNF, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04110803, "LTE_RLCDL_STATS_CNF", NULL ),

  /* LTE_RLCDL_EXPIRE_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04111600, "LTE_RLCDL_EXPIRE_TMRI", NULL ),

  /* LTE_RLCDL_WDOG_TIMER_EXPIRED_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04111601, "LTE_RLCDL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_RLCUL_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120000, "LTE_RLCUL_LOOPBACK_SPR", NULL ),

  /* LTE_RLCUL_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120001, "LTE_RLCUL_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_RLCUL_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04120002, "LTE_RLCUL_THREAD_READY_SPR", NULL ),

  /* LTE_RLCUL_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04120003, "LTE_RLCUL_THREAD_KILL_SPR", NULL ),

  /* LTE_RLCUL_CFG_REQ, payload: lte_rlcul_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120200, "LTE_RLCUL_CFG_REQ", NULL ),

  /* LTE_RLCUL_START_REQ, payload: lte_rlc_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120201, "LTE_RLCUL_START_REQ", NULL ),

  /* LTE_RLCUL_STOP_REQ, payload: lte_rlc_stop_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120202, "LTE_RLCUL_STOP_REQ", NULL ),

  /* LTE_RLCUL_STATS_REQ, payload: lte_rlcul_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120203, "LTE_RLCUL_STATS_REQ", NULL ),

  /* LTE_RLCUL_QSH_REQ, payload: lte_rlcul_qsh_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120204, "LTE_RLCUL_QSH_REQ", NULL ),

  /* LTE_RLCUL_ACK_IND, payload: lte_rlcul_ack_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120400, "LTE_RLCUL_ACK_IND", NULL ),

  /* LTE_RLCUL_FREE_PDU_IND, payload: lte_rlcul_free_pdu_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120401, "LTE_RLCUL_FREE_PDU_IND", NULL ),

  /* LTE_RLCUL_RE_EST_STATUS_IND, payload: lte_rlcul_re_est_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120402, "LTE_RLCUL_RE_EST_STATUS_IND", NULL ),

  /* LTE_RLCUL_MAX_RETX_IND, payload: lte_rlcul_max_retx_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120403, "LTE_RLCUL_MAX_RETX_IND", NULL ),

  /* LTE_RLCUL_FLUSH_RLCUL_LOG_BUF_IND, payload: lte_rlcul_flush_rlcul_log_buf_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120404, "LTE_RLCUL_FLUSH_RLCUL_LOG_BUF_IND", NULL ),

  /* LTE_RLCUL_FLUSH_PDCPUL_LOG_BUF_IND, payload: lte_rlcul_flush_pdcpul_log_buf_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120405, "LTE_RLCUL_FLUSH_PDCPUL_LOG_BUF_IND", NULL ),

  /* LTE_RLCUL_STOP_STAT_PROHIBIT_IND, payload: lte_rlcul_am_stop_stat_prohibit_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120406, "LTE_RLCUL_STOP_STAT_PROHIBIT_IND", NULL ),

  /* LTE_RLCUL_CFG_CNF, payload: lte_rlc_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120800, "LTE_RLCUL_CFG_CNF", NULL ),

  /* LTE_RLCUL_START_CNF, payload: lte_rlc_start_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120801, "LTE_RLCUL_START_CNF", NULL ),

  /* LTE_RLCUL_STOP_CNF, payload: lte_rlc_stop_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120802, "LTE_RLCUL_STOP_CNF", NULL ),

  /* LTE_RLCUL_STATS_CNF, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04120803, "LTE_RLCUL_STATS_CNF", NULL ),

  /* LTE_RLCUL_PERIODIC_TIMER_EXP_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04121600, "LTE_RLCUL_PERIODIC_TIMER_EXP_TMRI", NULL ),

  /* LTE_RLCUL_WDOG_TIMER_EXPIRED_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04121601, "LTE_RLCUL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_PDCPDL_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130000, "LTE_PDCPDL_LOOPBACK_SPR", NULL ),

  /* LTE_PDCPDL_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130001, "LTE_PDCPDL_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_PDCPDL_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04130002, "LTE_PDCPDL_THREAD_READY_SPR", NULL ),

  /* LTE_PDCPDL_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04130003, "LTE_PDCPDL_THREAD_KILL_SPR", NULL ),

  /* LTE_PDCPDL_CFG_REQ, payload: lte_pdcpdl_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130200, "LTE_PDCPDL_CFG_REQ", NULL ),

  /* LTE_PDCPDL_RECFG_PREP_REQ, payload: lte_pdcpdl_recfg_prep_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130201, "LTE_PDCPDL_RECFG_PREP_REQ", NULL ),

  /* LTE_PDCPDL_RAB_REGISTER_REQ, payload: lte_pdcpdl_rab_register_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130202, "LTE_PDCPDL_RAB_REGISTER_REQ", NULL ),

  /* LTE_PDCPDL_COUNTER_REQ, payload: lte_pdcpdl_counter_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130203, "LTE_PDCPDL_COUNTER_REQ", NULL ),

  /* LTE_PDCPDL_ENTER_FTM_REQ, payload: lte_pdcpdl_enter_ftm_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130204, "LTE_PDCPDL_ENTER_FTM_REQ", NULL ),

  /* LTE_PDCPDL_EXIT_FTM_REQ, payload: lte_pdcpdl_exit_ftm_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130205, "LTE_PDCPDL_EXIT_FTM_REQ", NULL ),

  /* LTE_PDCPDL_START_REQ, payload: lte_pdcp_start_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130206, "LTE_PDCPDL_START_REQ", NULL ),

  /* LTE_PDCPDL_STOP_REQ, payload: lte_pdcp_stop_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130207, "LTE_PDCPDL_STOP_REQ", NULL ),

  /* LTE_PDCPDL_FC_REQ, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130208, "LTE_PDCPDL_FC_REQ", NULL ),

  /* LTE_PDCPDL_SEC_RESET_REQ, payload: lte_pdcpdl_sec_reset_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130209, "LTE_PDCPDL_SEC_RESET_REQ", NULL ),

  /* LTE_PDCPDL_DECOMP_ALLOC_REQ, payload: lte_pdcpdl_decomp_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020a, "LTE_PDCPDL_DECOMP_ALLOC_REQ", NULL ),

  /* LTE_PDCPDL_DECOMP_FREE_REQ, payload: lte_pdcpdl_decomp_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020b, "LTE_PDCPDL_DECOMP_FREE_REQ", NULL ),

  /* LTE_PDCPDL_DECOMP_PKT_REQ, payload: lte_pdcpdl_decomp_pkt_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020c, "LTE_PDCPDL_DECOMP_PKT_REQ", NULL ),

  /* LTE_PDCPDL_A2_RAB_DEREGISTER_REQ, payload: lte_pdcpdl_a2_rab_deregister_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020d, "LTE_PDCPDL_A2_RAB_DEREGISTER_REQ", NULL ),

  /* LTE_PDCPDL_RAB_DEREGISTER_REQ, payload: lte_pdcp_rab_deregister_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020e, "LTE_PDCPDL_RAB_DEREGISTER_REQ", NULL ),

  /* LTE_PDCPDL_CIPH_RESET_REQ, payload: lte_pdcpdl_ciph_reset_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0413020f, "LTE_PDCPDL_CIPH_RESET_REQ", NULL ),

  /* LTE_PDCPDL_QSH_ANALYSIS_REQ, payload: lte_pdcp_qsh_analysis_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130210, "LTE_PDCPDL_QSH_ANALYSIS_REQ", NULL ),

  /* LTE_PDCPDL_SDU_IND, payload: lte_pdcpdl_sdu_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130400, "LTE_PDCPDL_SDU_IND", NULL ),

  /* LTE_PDCPDL_PEER_CTRL_IND, payload: lte_pdcpdl_peer_ctrl_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130401, "LTE_PDCPDL_PEER_CTRL_IND", NULL ),

  /* LTE_PDCPDL_TX_STATUS_IND, payload: lte_pdcpdl_peer_ctrl_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130402, "LTE_PDCPDL_TX_STATUS_IND", NULL ),

  /* LTE_PDCPDL_TX_ROHC_FB_IND, payload: lte_pdcpdl_peer_ctrl_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130403, "LTE_PDCPDL_TX_ROHC_FB_IND", NULL ),

  /* LTE_PDCPDL_WM_LOW_IND, payload: lte_pdcpdl_wm_low_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130404, "LTE_PDCPDL_WM_LOW_IND", NULL ),

  /* LTE_PDCPDL_PEER_ROHC_FB_IND, payload: lte_pdcpdl_peer_ctrl_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130405, "LTE_PDCPDL_PEER_ROHC_FB_IND", NULL ),

  /* LTE_PDCPDL_CFG_CNF, payload: lte_pdcpdl_cfg_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130800, "LTE_PDCPDL_CFG_CNF", NULL ),

  /* LTE_PDCPDL_RECFG_PREP_CNF, payload: lte_pdcpdl_recfg_prep_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130801, "LTE_PDCPDL_RECFG_PREP_CNF", NULL ),

  /* LTE_PDCPDL_RAB_REGISTER_CNF, payload: lte_pdcpdl_rab_register_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130802, "LTE_PDCPDL_RAB_REGISTER_CNF", NULL ),

  /* LTE_PDCPDL_COUNTER_CNF, payload: lte_pdcpdl_counter_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130803, "LTE_PDCPDL_COUNTER_CNF", NULL ),

  /* LTE_PDCPDL_ENTER_FTM_CNF, payload: lte_pdcpdl_enter_ftm_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130804, "LTE_PDCPDL_ENTER_FTM_CNF", NULL ),

  /* LTE_PDCPDL_EXIT_FTM_CNF, payload: lte_pdcpdl_exit_ftm_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130805, "LTE_PDCPDL_EXIT_FTM_CNF", NULL ),

  /* LTE_PDCPDL_START_CNF, payload: lte_pdcp_start_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130806, "LTE_PDCPDL_START_CNF", NULL ),

  /* LTE_PDCPDL_STOP_CNF, payload: lte_pdcp_stop_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130807, "LTE_PDCPDL_STOP_CNF", NULL ),

  /* LTE_PDCPDL_RAB_DEREGISTER_CNF, payload: lte_pdcp_rab_deregister_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04130808, "LTE_PDCPDL_RAB_DEREGISTER_CNF", NULL ),

  /* LTE_PDCPDL_LOG_FLUSH_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x04131600, "LTE_PDCPDL_LOG_FLUSH_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_PDCPDL_WDOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x04131601, "LTE_PDCPDL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_PDCPUL_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140000, "LTE_PDCPUL_LOOPBACK_SPR", NULL ),

  /* LTE_PDCPUL_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140001, "LTE_PDCPUL_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_PDCPUL_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04140002, "LTE_PDCPUL_THREAD_READY_SPR", NULL ),

  /* LTE_PDCPUL_CFG_REQ, payload: lte_pdcpul_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140200, "LTE_PDCPUL_CFG_REQ", NULL ),

  /* LTE_PDCPUL_SDU_REQ, payload: lte_pdcpul_sdu_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140201, "LTE_PDCPUL_SDU_REQ", NULL ),

  /* LTE_PDCPUL_RECFG_PREP_REQ, payload: lte_pdcpul_recfg_prep_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140202, "LTE_PDCPUL_RECFG_PREP_REQ", NULL ),

  /* LTE_PDCPUL_RAB_REGISTER_REQ, payload: lte_pdcpul_rab_register_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140203, "LTE_PDCPUL_RAB_REGISTER_REQ", NULL ),

  /* LTE_PDCPUL_COUNTER_REQ, payload: lte_pdcpul_counter_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140204, "LTE_PDCPUL_COUNTER_REQ", NULL ),

  /* LTE_PDCPUL_ENTER_FTM_REQ, payload: lte_pdcpul_enter_ftm_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140205, "LTE_PDCPUL_ENTER_FTM_REQ", NULL ),

  /* LTE_PDCPUL_EXIT_FTM_REQ, payload: lte_pdcpul_exit_ftm_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140206, "LTE_PDCPUL_EXIT_FTM_REQ", NULL ),

  /* LTE_PDCPUL_START_REQ, payload: lte_pdcp_start_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140207, "LTE_PDCPUL_START_REQ", NULL ),

  /* LTE_PDCPUL_STOP_REQ, payload: lte_pdcp_stop_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140208, "LTE_PDCPUL_STOP_REQ", NULL ),

  /* LTE_PDCPUL_COMP_ALLOC_REQ, payload: lte_pdcpul_comp_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140209, "LTE_PDCPUL_COMP_ALLOC_REQ", NULL ),

  /* LTE_PDCPUL_COMP_FREE_REQ, payload: lte_pdcpul_comp_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020a, "LTE_PDCPUL_COMP_FREE_REQ", NULL ),

  /* LTE_PDCPUL_COMP_PKT_REQ, payload: lte_pdcpul_comp_pkt_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020b, "LTE_PDCPUL_COMP_PKT_REQ", NULL ),

  /* LTE_PDCPUL_ROHC_RESET_REQ, payload: lte_pdcpul_comp_cfg_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020c, "LTE_PDCPUL_ROHC_RESET_REQ", NULL ),

  /* LTE_PDCPUL_RAB_DEREGISTER_REQ, payload: lte_pdcp_rab_deregister_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020d, "LTE_PDCPUL_RAB_DEREGISTER_REQ", NULL ),

  /* LTE_PDCPUL_RB_STATS_REQ, payload: lte_pdcpul_rb_stats_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020e, "LTE_PDCPUL_RB_STATS_REQ", NULL ),

  /* LTE_PDCPUL_OFFLOAD_RAB_DEREGISTER_REQ, payload: lte_pdcp_offload_rab_deregister_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414020f, "LTE_PDCPUL_OFFLOAD_RAB_DEREGISTER_REQ", NULL ),

  /* LTE_PDCPUL_CFG_CNF, payload: lte_pdcpul_cfg_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140800, "LTE_PDCPUL_CFG_CNF", NULL ),

  /* LTE_PDCPUL_RECFG_PREP_CNF, payload: lte_pdcpul_recfg_prep_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140801, "LTE_PDCPUL_RECFG_PREP_CNF", NULL ),

  /* LTE_PDCPUL_SDU_CNF, payload: lte_pdcpul_sdu_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140802, "LTE_PDCPUL_SDU_CNF", NULL ),

  /* LTE_PDCPUL_RAB_REGISTER_CNF, payload: lte_pdcpul_rab_register_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140803, "LTE_PDCPUL_RAB_REGISTER_CNF", NULL ),

  /* LTE_PDCPUL_COUNTER_CNF, payload: lte_pdcpul_counter_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140804, "LTE_PDCPUL_COUNTER_CNF", NULL ),

  /* LTE_PDCPUL_ENTER_FTM_CNF, payload: lte_pdcpul_enter_ftm_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140805, "LTE_PDCPUL_ENTER_FTM_CNF", NULL ),

  /* LTE_PDCPUL_EXIT_FTM_CNF, payload: lte_pdcpul_exit_ftm_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140806, "LTE_PDCPUL_EXIT_FTM_CNF", NULL ),

  /* LTE_PDCPUL_START_CNF, payload: lte_pdcp_start_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140807, "LTE_PDCPUL_START_CNF", NULL ),

  /* LTE_PDCPUL_STOP_CNF, payload: lte_pdcp_stop_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140808, "LTE_PDCPUL_STOP_CNF", NULL ),

  /* LTE_PDCPUL_RAB_DEREGISTER_CNF, payload: lte_pdcp_rab_deregister_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04140809, "LTE_PDCPUL_RAB_DEREGISTER_CNF", NULL ),

  /* LTE_PDCPUL_RB_STATS_CNF, payload: lte_pdcpul_rb_stats_cnf_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0414080a, "LTE_PDCPUL_RB_STATS_CNF", NULL ),

  /* LTE_PDCPUL_DISCARD_TIMER_TICK_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x04141600, "LTE_PDCPUL_DISCARD_TIMER_TICK_EXPIRED_TMRI", NULL ),

  /* LTE_PDCPUL_LOG_STATS_FLUSH_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x04141601, "LTE_PDCPUL_LOG_STATS_FLUSH_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_PDCPUL_WDOG_TIMER_EXPIRED_TMRI, payload: (void*) */
  MSGR_UMID_TABLE_ENTRY ( 0x04141602, "LTE_PDCPUL_WDOG_TIMER_EXPIRED_TMRI", NULL ),

  /* LTE_CPHY_TEST_GM_SKIP_RACH_REQ, payload: lte_cphy_test_gm_skip_rach_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0200, "LTE_CPHY_TEST_GM_SKIP_RACH_REQ", NULL ),

  /* LTE_CPHY_TEST_SKIP_DBCH_PRE_IDLE_REQ, payload: lte_cphy_test_skip_dbch_pre_idle_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0201, "LTE_CPHY_TEST_SKIP_DBCH_PRE_IDLE_REQ", NULL ),

  /* LTE_CPHY_TEST_ML1_SKIP_RACH_REQ, payload: lte_cphy_test_ml1_skip_rach_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0202, "LTE_CPHY_TEST_ML1_SKIP_RACH_REQ", NULL ),

  /* LTE_CPHY_TEST_TX_ENABLE_REQ, payload: lte_cphy_test_tx_enable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0203, "LTE_CPHY_TEST_TX_ENABLE_REQ", NULL ),

  /* LTE_CPHY_TEST_L1_MODE_CHANGE_REQ, payload: lte_cphy_test_l1_mode_change_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0204, "LTE_CPHY_TEST_L1_MODE_CHANGE_REQ", NULL ),

  /* LTE_CPHY_TEST_OVERRIDE_UL_GRANT_REQ, payload: lte_cphy_test_override_ul_grant_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0205, "LTE_CPHY_TEST_OVERRIDE_UL_GRANT_REQ", NULL ),

  /* LTE_CPHY_TEST_RESET_PDSCH_STATS_REQ, payload: lte_cphy_test_reset_pdsch_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0206, "LTE_CPHY_TEST_RESET_PDSCH_STATS_REQ", NULL ),

  /* LTE_CPHY_TEST_GET_PDSCH_STATS_REQ, payload: lte_cphy_test_get_pdsch_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0207, "LTE_CPHY_TEST_GET_PDSCH_STATS_REQ", NULL ),

  /* LTE_CPHY_TEST_GET_ALL_CARR_PDSCH_STATS_REQ, payload: lte_cphy_test_get_all_carr_pdsch_stats_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0208, "LTE_CPHY_TEST_GET_ALL_CARR_PDSCH_STATS_REQ", NULL ),

  /* LTE_CPHY_TEST_UL_TTI_CFG_REQ, payload: lte_cphy_test_ul_tti_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0210, "LTE_CPHY_TEST_UL_TTI_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_CFG_REQ, payload: lte_cphy_test_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0211, "LTE_CPHY_TEST_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_SKIP_ACQ_REQ, payload: lte_cphy_test_skip_acq_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0212, "LTE_CPHY_TEST_SKIP_ACQ_REQ", NULL ),

  /* LTE_CPHY_TEST_SEARCH_CFG_REQ, payload: lte_cphy_test_search_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0213, "LTE_CPHY_TEST_SEARCH_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_DL_TTI_CFG_REQ, payload: lte_cphy_test_dl_tti_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0214, "LTE_CPHY_TEST_DL_TTI_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_DL_CFG_REQ, payload: lte_cphy_test_dl_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0215, "LTE_CPHY_TEST_DL_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_SKIP_ULPC_REQ, payload: lte_cphy_test_skip_ulpc_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0216, "LTE_CPHY_TEST_SKIP_ULPC_REQ", NULL ),

  /* LTE_CPHY_TEST_ODY_PLT_L1M_CFG_REQ, payload: lte_cphy_test_ody_plt_l1m_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0217, "LTE_CPHY_TEST_ODY_PLT_L1M_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_SEARCH_CFG_ODY_REQ, payload: lte_cphy_test_search_cfg_ody_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0218, "LTE_CPHY_TEST_SEARCH_CFG_ODY_REQ", NULL ),

  /* LTE_CPHY_TEST_PRACH_MSG1_REQ, payload: lte_cphy_test_prach_msg1_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0219, "LTE_CPHY_TEST_PRACH_MSG1_REQ", NULL ),

  /* LTE_CPHY_TEST_CONN_MEAS_CFG_REQ, payload: lte_cphy_test_conn_meas_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a021a, "LTE_CPHY_TEST_CONN_MEAS_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_IDLE_MEAS_CFG_REQ, payload: lte_cphy_test_idle_meas_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a021b, "LTE_CPHY_TEST_IDLE_MEAS_CFG_REQ", NULL ),

  /* LTE_CPHY_TEST_COMMON_CFG_MBSFN_REQ, payload: lte_cphy_test_common_cfg_mbsfn_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a021f, "LTE_CPHY_TEST_COMMON_CFG_MBSFN_REQ", NULL ),

  /* LTE_CPHY_TEST_PRS_COMPLETE_IND, payload: lte_cphy_test_prs_complete_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a041e, "LTE_CPHY_TEST_PRS_COMPLETE_IND", NULL ),

  /* LTE_CPHY_TEST_COMPLETE_IND, payload: lte_cphy_test_complete_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0420, "LTE_CPHY_TEST_COMPLETE_IND", NULL ),

  /* LTE_CPHY_TEST_PRS_ERROR_IND, payload: lte_cphy_test_prs_error_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0424, "LTE_CPHY_TEST_PRS_ERROR_IND", NULL ),

  /* LTE_CPHY_TEST_TX_ENABLE_CNF, payload: lte_cphy_test_tx_enable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0803, "LTE_CPHY_TEST_TX_ENABLE_CNF", NULL ),

  /* LTE_CPHY_TEST_L1_MODE_CHANGE_CNF, payload: lte_cphy_test_l1_mode_change_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0804, "LTE_CPHY_TEST_L1_MODE_CHANGE_CNF", NULL ),

  /* LTE_CPHY_TEST_GET_PDSCH_STATS_CNF, payload: lte_cphy_test_get_pdsch_stats_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0807, "LTE_CPHY_TEST_GET_PDSCH_STATS_CNF", NULL ),

  /* LTE_CPHY_TEST_GET_ALL_CARR_PDSCH_STATS_CNF, payload: lte_cphy_test_get_all_carr_pdsch_stats_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0808, "LTE_CPHY_TEST_GET_ALL_CARR_PDSCH_STATS_CNF", NULL ),

  /* LTE_CPHY_TEST_UL_TTI_CFG_CNF, payload: lte_cphy_test_ul_tti_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0810, "LTE_CPHY_TEST_UL_TTI_CFG_CNF", NULL ),

  /* LTE_CPHY_TEST_CFG_CNF, payload: lte_cphy_test_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0811, "LTE_CPHY_TEST_CFG_CNF", NULL ),

  /* LTE_CPHY_TEST_SKIP_ACQ_CNF, payload: lte_cphy_test_skip_acq_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0812, "LTE_CPHY_TEST_SKIP_ACQ_CNF", NULL ),

  /* LTE_CPHY_TEST_SEARCH_CFG_CNF, payload: lte_cphy_test_search_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0813, "LTE_CPHY_TEST_SEARCH_CFG_CNF", NULL ),

  /* LTE_CPHY_TEST_SEARCH_CFG_ODY_CNF, payload: lte_cphy_test_search_cfg_ody_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041a0818, "LTE_CPHY_TEST_SEARCH_CFG_ODY_CNF", NULL ),

  /* LTE_ML1_GM_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0000, "LTE_ML1_GM_LOOPBACK_SPR", NULL ),

  /* LTE_ML1_GM_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0001, "LTE_ML1_GM_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_ML1_GM_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0002, "LTE_ML1_GM_THREAD_READY_SPR", NULL ),

  /* LTE_ML1_GM_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0003, "LTE_ML1_GM_THREAD_KILL_SPR", NULL ),

  /* LTE_ML1_GM_COMMON_CFG_REQ, payload: lte_ml1_gm_common_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0202, "LTE_ML1_GM_COMMON_CFG_REQ", NULL ),

  /* LTE_ML1_GM_DEDICATED_CFG_REQ, payload: lte_ml1_gm_dedicated_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0203, "LTE_ML1_GM_DEDICATED_CFG_REQ", NULL ),

  /* LTE_ML1_GM_ABORT_REQ, payload: lte_ml1_gm_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0207, "LTE_ML1_GM_ABORT_REQ", NULL ),

  /* LTE_ML1_GM_HANDOVER_SUSPEND_REQ, payload: lte_ml1_gm_handover_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0209, "LTE_ML1_GM_HANDOVER_SUSPEND_REQ", NULL ),

  /* LTE_ML1_GM_SFN_UPDATED_REQ, payload: lte_ml1_gm_sfn_updated_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b020a, "LTE_ML1_GM_SFN_UPDATED_REQ", NULL ),

  /* LTE_ML1_GM_RACH_CFG_REQ, payload: lte_ml1_gm_rach_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b020e, "LTE_ML1_GM_RACH_CFG_REQ", NULL ),

  /* LTE_ML1_GM_RACH_ABORT_REQ, payload: lte_ml1_gm_rach_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b020f, "LTE_ML1_GM_RACH_ABORT_REQ", NULL ),

  /* LTE_ML1_GM_DL_BANDWIDTH_CFG_REQ, payload: lte_ml1_gm_dl_bandwidth_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0210, "LTE_ML1_GM_DL_BANDWIDTH_CFG_REQ", NULL ),

  /* LTE_ML1_GM_CON_RELEASE_REQ, payload: lte_ml1_gm_con_release_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0211, "LTE_ML1_GM_CON_RELEASE_REQ", NULL ),

  /* LTE_ML1_GM_RECFG_SUSPEND_REQ, payload: lte_ml1_gm_recfg_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0212, "LTE_ML1_GM_RECFG_SUSPEND_REQ", NULL ),

  /* LTE_ML1_GM_RECFG_RESUME_REQ, payload: lte_ml1_gm_recfg_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0213, "LTE_ML1_GM_RECFG_RESUME_REQ", NULL ),

  /* LTE_ML1_GM_HANDOVER_RESUME_REQ, payload: lte_ml1_gm_handover_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0214, "LTE_ML1_GM_HANDOVER_RESUME_REQ", NULL ),

  /* LTE_ML1_GM_SUSPEND_REQ, payload: lte_ml1_gm_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0217, "LTE_ML1_GM_SUSPEND_REQ", NULL ),

  /* LTE_ML1_GM_RESUME_REQ, payload: lte_ml1_gm_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0218, "LTE_ML1_GM_RESUME_REQ", NULL ),

  /* LTE_ML1_GM_TDD_CFG_REQ, payload: lte_ml1_gm_tdd_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0219, "LTE_ML1_GM_TDD_CFG_REQ", NULL ),

  /* LTE_ML1_GM_PMAX_CFG_REQ, payload: lte_ml1_gm_pmax_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b021c, "LTE_ML1_GM_PMAX_CFG_REQ", NULL ),

  /* LTE_ML1_GM_RECOVERY_CLEANUP_REQ, payload: lte_ml1_gm_recovery_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b021f, "LTE_ML1_GM_RECOVERY_CLEANUP_REQ", NULL ),

  /* LTE_ML1_GM_X2L_HANDOVER_PREP_REQ, payload: lte_ml1_gm_x2l_handover_prep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0228, "LTE_ML1_GM_X2L_HANDOVER_PREP_REQ", NULL ),

  /* LTE_ML1_GM_CM_ML1_GM_INFO_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0230, "LTE_ML1_GM_CM_ML1_GM_INFO_REQ", NULL ),

  /* LTE_ML1_GM_SCC_STATUS_UPDATE_REQ, payload: lte_ml1_gm_scc_status_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0231, "LTE_ML1_GM_SCC_STATUS_UPDATE_REQ", NULL ),

  /* LTE_ML1_GM_ANTENNA_SWITCH_REQ, payload: lte_ml1_gm_antenna_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0235, "LTE_ML1_GM_ANTENNA_SWITCH_REQ", NULL ),

  /* LTE_ML1_GM_PDCCH_ORDER_IND, payload: lte_ml1_gm_pdcch_order_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b040b, "LTE_ML1_GM_PDCCH_ORDER_IND", NULL ),

  /* LTE_ML1_GM_RACH_MSG3_SENT_IND, payload: lte_ml1_gm_rach_msg3_sent_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b040d, "LTE_ML1_GM_RACH_MSG3_SENT_IND", NULL ),

  /* LTE_ML1_GM_RL_FAIL_IND, payload: lte_ml1_gm_rl_fail_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0415, "LTE_ML1_GM_RL_FAIL_IND", NULL ),

  /* LTE_ML1_GM_TIMER_EXPIRY_IND, payload: lte_ml1_gm_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b041a, "LTE_ML1_GM_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_GM_TEST_CDRX_CYCLE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041b041b, "LTE_ML1_GM_TEST_CDRX_CYCLE_IND", NULL ),

  /* LTE_ML1_GM_SR_ABORTED_IND, payload: lte_ml1_gm_sr_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b041d, "LTE_ML1_GM_SR_ABORTED_IND", NULL ),

  /* LTE_ML1_GM_RECOVERY_TRIGGER_IND, payload: lte_ml1_gm_recovery_trigger_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b041e, "LTE_ML1_GM_RECOVERY_TRIGGER_IND", NULL ),

  /* LTE_ML1_GM_ENTER_CONN_MODE_IND, payload: lte_ml1_gm_enter_conn_mode_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0420, "LTE_ML1_GM_ENTER_CONN_MODE_IND", NULL ),

  /* LTE_ML1_GM_TEST_CDRX_ON_DURATION_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0421, "LTE_ML1_GM_TEST_CDRX_ON_DURATION_IND", NULL ),

  /* LTE_ML1_GM_SCHDLR_OBJ_ABORTED_IND, payload: lte_ml1_gm_schdlr_obj_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0422, "LTE_ML1_GM_SCHDLR_OBJ_ABORTED_IND", NULL ),

  /* LTE_ML1_GM_RA_TIMER_STARTED_IND, payload: lte_ml1_gm_ra_timer_started_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0423, "LTE_ML1_GM_RA_TIMER_STARTED_IND", NULL ),

  /* LTE_ML1_GM_CDRX_EVT_IND, payload: lte_ml1_gm_cdrx_evt_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0424, "LTE_ML1_GM_CDRX_EVT_IND", NULL ),

  /* LTE_ML1_GM_T310_EXPIRED_IND, payload: lte_ml1_gm_t310_expired_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0425, "LTE_ML1_GM_T310_EXPIRED_IND", NULL ),

  /* LTE_ML1_GM_SR_ENV_STARTED_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0426, "LTE_ML1_GM_SR_ENV_STARTED_IND", NULL ),

  /* LTE_ML1_GM_TX_SUSPEND_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0427, "LTE_ML1_GM_TX_SUSPEND_IND", NULL ),

  /* LTE_ML1_GM_CDRX_EVNT_END_IND, payload: lte_ml1_gm_cdrx_evt_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0429, "LTE_ML1_GM_CDRX_EVNT_END_IND", NULL ),

  /* LTE_ML1_GM_SCC_DEACTIVATE_IND, payload: lte_ml1_gm_scc_deactivate_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0432, "LTE_ML1_GM_SCC_DEACTIVATE_IND", NULL ),

  /* LTE_ML1_GM_ANTENNA_SWITCH_START_IND, payload: lte_ml1_gm_antenna_switch_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0435, "LTE_ML1_GM_ANTENNA_SWITCH_START_IND", NULL ),

  /* LTE_ML1_GM_RESET_TXAGC_MTPL_CNTR_IND, payload: lte_ml1_gm_reset_txagc_mtpl_cntr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0436, "LTE_ML1_GM_RESET_TXAGC_MTPL_CNTR_IND", NULL ),

  /* LTE_ML1_GM_QTA_INFO_IND, payload: lte_ml1_gm_qta_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0437, "LTE_ML1_GM_QTA_INFO_IND", NULL ),

  /* LTE_ML1_GM_COMMON_CFG_CNF, payload: lte_ml1_gm_common_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0802, "LTE_ML1_GM_COMMON_CFG_CNF", NULL ),

  /* LTE_ML1_GM_DEDICATED_CFG_CNF, payload: lte_ml1_gm_dedicated_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0803, "LTE_ML1_GM_DEDICATED_CFG_CNF", NULL ),

  /* LTE_ML1_GM_ABORT_CNF, payload: lte_ml1_gm_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0807, "LTE_ML1_GM_ABORT_CNF", NULL ),

  /* LTE_ML1_GM_HANDOVER_SUSPEND_CNF, payload: lte_ml1_gm_handover_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0809, "LTE_ML1_GM_HANDOVER_SUSPEND_CNF", NULL ),

  /* LTE_ML1_GM_SFN_UPDATED_CNF, payload: lte_ml1_gm_sfn_updated_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b080a, "LTE_ML1_GM_SFN_UPDATED_CNF", NULL ),

  /* LTE_ML1_GM_RACH_ABORT_CNF, payload: lte_ml1_gm_rach_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b080f, "LTE_ML1_GM_RACH_ABORT_CNF", NULL ),

  /* LTE_ML1_GM_CON_RELEASE_CNF, payload: lte_ml1_gm_con_release_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0811, "LTE_ML1_GM_CON_RELEASE_CNF", NULL ),

  /* LTE_ML1_GM_RECFG_SUSPEND_CNF, payload: lte_ml1_gm_recfg_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0812, "LTE_ML1_GM_RECFG_SUSPEND_CNF", NULL ),

  /* LTE_ML1_GM_HANDOVER_RESUME_CNF, payload: lte_ml1_gm_handover_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0814, "LTE_ML1_GM_HANDOVER_RESUME_CNF", NULL ),

  /* LTE_ML1_GM_TEST_TX_ENABLE_CNF, payload: lte_ml1_gm_test_tx_enable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0816, "LTE_ML1_GM_TEST_TX_ENABLE_CNF", NULL ),

  /* LTE_ML1_GM_SUSPEND_CNF, payload: lte_ml1_gm_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0817, "LTE_ML1_GM_SUSPEND_CNF", NULL ),

  /* LTE_ML1_GM_RESUME_CNF, payload: lte_ml1_gm_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0818, "LTE_ML1_GM_RESUME_CNF", NULL ),

  /* LTE_ML1_GM_TDD_CFG_CNF, payload: lte_ml1_gm_tdd_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0819, "LTE_ML1_GM_TDD_CFG_CNF", NULL ),

  /* LTE_ML1_GM_CM_ML1_GM_INFO_CNF, payload: lte_ml1_gm_cm_ml1_gm_info_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0830, "LTE_ML1_GM_CM_ML1_GM_INFO_CNF", NULL ),

  /* LTE_ML1_GM_SCC_STATUS_UPDATE_CNF, payload: lte_ml1_gm_scc_status_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0831, "LTE_ML1_GM_SCC_STATUS_UPDATE_CNF", NULL ),

  /* LTE_ML1_GM_MCPM_CLOCK_CFG_CNF, payload: lte_ml1_gm_mcpm_clock_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0833, "LTE_ML1_GM_MCPM_CLOCK_CFG_CNF", NULL ),

  /* LTE_ML1_GM_ANTENNA_SWITCH_CNF, payload: lte_ml1_gm_antenna_switch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041b0835, "LTE_ML1_GM_ANTENNA_SWITCH_CNF", NULL ),

  /* LTE_APP_DSPSW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0000, "LTE_APP_DSPSW_LOOPBACK_SPR", NULL ),

  /* LTE_APP_DSPSW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0001, "LTE_APP_DSPSW_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_APP_DSPSW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0002, "LTE_APP_DSPSW_THREAD_READY_SPR", NULL ),

  /* LTE_APP_DSPSW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0003, "LTE_APP_DSPSW_THREAD_KILL_SPR", NULL ),

  /* LTE_APP_DSPSW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0101, "LTE_APP_DSPSW_SPAWN_THREADS_CMD", NULL ),

  /* LTE_APP_DSPSW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0102, "LTE_APP_DSPSW_SYNC_CMD", NULL ),

  /* LTE_APP_DSPSW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0301, "LTE_APP_DSPSW_SPAWN_THREADS_RSP", NULL ),

  /* LTE_APP_DSPSW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041e0302, "LTE_APP_DSPSW_SYNC_RSP", NULL ),

  /* LTE_APP_DSPFW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0000, "LTE_APP_DSPFW_LOOPBACK_SPR", NULL ),

  /* LTE_APP_DSPFW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0001, "LTE_APP_DSPFW_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_APP_DSPFW_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0002, "LTE_APP_DSPFW_THREAD_READY_SPR", NULL ),

  /* LTE_APP_DSPFW_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0003, "LTE_APP_DSPFW_THREAD_KILL_SPR", NULL ),

  /* LTE_APP_DSPFW_SPAWN_THREADS_CMD, payload: appmgr_cmd_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0101, "LTE_APP_DSPFW_SPAWN_THREADS_CMD", NULL ),

  /* LTE_APP_DSPFW_SYNC_CMD, payload: appmgr_cmd_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0102, "LTE_APP_DSPFW_SYNC_CMD", NULL ),

  /* LTE_APP_DSPFW_SPAWN_THREADS_RSP, payload: appmgr_rsp_spawn_threads_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0301, "LTE_APP_DSPFW_SPAWN_THREADS_RSP", NULL ),

  /* LTE_APP_DSPFW_SYNC_RSP, payload: appmgr_rsp_sync_s */
  MSGR_UMID_TABLE_ENTRY ( 0x041f0302, "LTE_APP_DSPFW_SYNC_RSP", NULL ),

  /* LTE_ML1_SCHDLR_ACTIVATE_REQ, payload: lte_ml1_schdlr_msg_activate_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200200, "LTE_ML1_SCHDLR_ACTIVATE_REQ", NULL ),

  /* LTE_ML1_SCHDLR_DEACT_REQ, payload: lte_ml1_schdlr_msg_deactivate_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200201, "LTE_ML1_SCHDLR_DEACT_REQ", NULL ),

  /* LTE_ML1_SCHDLR_SUSPEND_REQ, payload: lte_ml1_schdlr_msg_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200202, "LTE_ML1_SCHDLR_SUSPEND_REQ", NULL ),

  /* LTE_ML1_SCHDLR_RESUME_REQ, payload: lte_ml1_schdlr_msg_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200203, "LTE_ML1_SCHDLR_RESUME_REQ", NULL ),

  /* LTE_ML1_SCHDLR_SCHED_REQ, payload: lte_ml1_schdlr_msg_sched_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200204, "LTE_ML1_SCHDLR_SCHED_REQ", NULL ),

  /* LTE_ML1_SCHDLR_OBJREG_REQ, payload: lte_ml1_schdlr_msg_obj_reg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200205, "LTE_ML1_SCHDLR_OBJREG_REQ", NULL ),

  /* LTE_ML1_SCHDLR_OBJDEREG_REQ, payload: lte_ml1_schdlr_msg_obj_dereg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200206, "LTE_ML1_SCHDLR_OBJDEREG_REQ", NULL ),

  /* LTE_ML1_SCHDLR_SETGROUP_REQ, payload: lte_ml1_schdlr_msg_setgroup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200208, "LTE_ML1_SCHDLR_SETGROUP_REQ", NULL ),

  /* LTE_ML1_SCHDLR_DOG_TIMER_ENABLE_DISABLE_REQ, payload: lte_ml1_schdlr_dog_timer_enable_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0420020a, "LTE_ML1_SCHDLR_DOG_TIMER_ENABLE_DISABLE_REQ", NULL ),

  /* LTE_ML1_SCHDLR_X2L_HO_PREP_REQ, payload: lte_ml1_schdlr_x2l_ho_prep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0420020b, "LTE_ML1_SCHDLR_X2L_HO_PREP_REQ", NULL ),

  /* LTE_ML1_SCHDLR_HI_PRI_SCHED_REQ, payload: lte_ml1_schdlr_msg_sched_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0420020d, "LTE_ML1_SCHDLR_HI_PRI_SCHED_REQ", NULL ),

  /* LTE_ML1_SCHDLR_CSWITCHDONE_IND, payload: lte_ml1_schdlr_msg_cswitch_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200407, "LTE_ML1_SCHDLR_CSWITCHDONE_IND", NULL ),

  /* LTE_ML1_SCHDLR_DOG_TIMER_IND, payload: lte_ml1_schdlr_dog_msg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04200409, "LTE_ML1_SCHDLR_DOG_TIMER_IND", NULL ),

  /* LTE_ML1_SCHDLR_ACTIVATED_IND, payload: lte_ml1_schdlr_activated_msg_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0420040c, "LTE_ML1_SCHDLR_ACTIVATED_IND", NULL ),

  /* LTE_TLB_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270000, "LTE_TLB_LOOPBACK_SPR", NULL ),

  /* LTE_TLB_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270001, "LTE_TLB_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_TLB_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04270002, "LTE_TLB_THREAD_READY_SPR", NULL ),

  /* LTE_TLB_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04270003, "LTE_TLB_THREAD_KILL_SPR", NULL ),

  /* LTE_TLB_UL_INFO_REQ, payload: lte_tlb_ul_info_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270200, "LTE_TLB_UL_INFO_REQ", NULL ),

  /* LTE_TLB_RRC_CFG_REQ, payload: lte_tlb_rrc_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270201, "LTE_TLB_RRC_CFG_REQ", NULL ),

  /* LTE_TLB_LB_ACT_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04270400, "LTE_TLB_LB_ACT_IND", NULL ),

  /* LTE_TLB_LB_CLOSE_TL_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04270401, "LTE_TLB_LB_CLOSE_TL_IND", NULL ),

  /* LTE_TLB_LB_DEACT_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04270402, "LTE_TLB_LB_DEACT_IND", NULL ),

  /* LTE_TLB_DL_INFO_IND, payload: lte_tlb_dl_info_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270403, "LTE_TLB_DL_INFO_IND", NULL ),

  /* LTE_TLB_LB_OPEN_IND, payload: lte_tlb_lb_open_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270404, "LTE_TLB_LB_OPEN_IND", NULL ),

  /* LTE_TLB_DS_CTL_READY_IND, payload: lte_tlb_ds_close_tl_ready_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270405, "LTE_TLB_DS_CTL_READY_IND", NULL ),

  /* LTE_TLB_RRC_CFG_CNF, payload: lte_tlb_rrc_cfg_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04270800, "LTE_TLB_RRC_CFG_CNF", NULL ),

  /* LTE_ML1_RFMGR_START_REQ, payload: lte_ml1_rfmgr_msg_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290200, "LTE_ML1_RFMGR_START_REQ", NULL ),

  /* LTE_ML1_RFMGR_TUNE_REQ, payload: lte_ml1_rfmgr_msg_tune_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290201, "LTE_ML1_RFMGR_TUNE_REQ", NULL ),

  /* LTE_ML1_RFMGR_SLEEP_REQ, payload: lte_ml1_rfmgr_msg_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290202, "LTE_ML1_RFMGR_SLEEP_REQ", NULL ),

  /* LTE_ML1_RFMGR_WAKEUP_REQ, payload: lte_ml1_rfmgr_msg_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290203, "LTE_ML1_RFMGR_WAKEUP_REQ", NULL ),

  /* LTE_ML1_RFMGR_STOP_REQ, payload: lte_ml1_rfmgr_msg_stop_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290204, "LTE_ML1_RFMGR_STOP_REQ", NULL ),

  /* LTE_ML1_RFMGR_SEND_TUNE_SCRIPT_REQ, payload: lte_ml1_rfmgr_msg_tune_script_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290205, "LTE_ML1_RFMGR_SEND_TUNE_SCRIPT_REQ", NULL ),

  /* LTE_ML1_RFMGR_BUILD_SCRIPT_REQ, payload: lte_ml1_rfmgr_msg_tune_script_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290206, "LTE_ML1_RFMGR_BUILD_SCRIPT_REQ", NULL ),

  /* LTE_ML1_RFMGR_INT_CNF_REQ, payload: lte_ml1_rfmgr_msg_int_cnf_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0429020c, "LTE_ML1_RFMGR_INT_CNF_REQ", NULL ),

  /* LTE_ML1_RFMGR_ANT_SW_REQ, payload: lte_ml1_rfmgr_msg_ant_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290211, "LTE_ML1_RFMGR_ANT_SW_REQ", NULL ),

  /* LTE_ML1_RFMGR_DIV_CFG_REQ, payload: lte_ml1_rfmgr_msg_div_cfg_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290213, "LTE_ML1_RFMGR_DIV_CFG_REQ", NULL ),

  /* LTE_ML1_RFMGR_SUSPEND_REQ, payload: lte_ml1_rfmgr_msg_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290214, "LTE_ML1_RFMGR_SUSPEND_REQ", NULL ),

  /* LTE_ML1_RFMGR_RESUME_REQ, payload: lte_ml1_rfmgr_msg_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290215, "LTE_ML1_RFMGR_RESUME_REQ", NULL ),

  /* LTE_ML1_RFMGR_ABORT_ANT_SW_REQ, payload: lte_ml1_rfmgr_msg_ant_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290217, "LTE_ML1_RFMGR_ABORT_ANT_SW_REQ", NULL ),

  /* LTE_ML1_RFMGR_OFFLINE_COMPLETE_IND, payload: lte_ml1_rfmgr_msg_offline_complete_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0429040b, "LTE_ML1_RFMGR_OFFLINE_COMPLETE_IND", NULL ),

  /* LTE_ML1_RFMGR_ANT_TUNER_TIMER_EXPIRY_IND, payload: lte_ml1_rfmgr_ant_tuner_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290410, "LTE_ML1_RFMGR_ANT_TUNER_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_RFMGR_APPLY_BUF_RX_DIV_IND, payload: lte_ml1_rfmgr_apply_buf_rx_div_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290412, "LTE_ML1_RFMGR_APPLY_BUF_RX_DIV_IND", NULL ),

  /* LTE_ML1_RFMGR_RESUME_DONE_IND, payload: lte_ml1_rfmgr_resume_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290416, "LTE_ML1_RFMGR_RESUME_DONE_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_CHAIN_GRANT_IND, payload: lte_ml1_rfmgr_msg_trm_chain_grant_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290421, "LTE_ML1_RFMGR_TRM_CHAIN_GRANT_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_UNLOCK_IND, payload: lte_ml1_rfmgr_msg_trm_unlock_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290422, "LTE_ML1_RFMGR_TRM_UNLOCK_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_UNLOCK_CANCEL_IND, payload: lte_ml1_rfmgr_msg_trm_unlock_cancel_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290423, "LTE_ML1_RFMGR_TRM_UNLOCK_CANCEL_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_CHAIN_TIMER_EXPIRY_IND, payload: lte_ml1_rfmgr_msg_trm_int_chain_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290430, "LTE_ML1_RFMGR_TRM_INT_CHAIN_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_CB_IND, payload: lte_ml1_rfmgr_msg_trm_int_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290431, "LTE_ML1_RFMGR_TRM_INT_CB_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_BAND_NOTIFICATION_CB_IND, payload: lte_ml1_rfmgr_msg_trm_int_band_notification_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290432, "LTE_ML1_RFMGR_TRM_INT_BAND_NOTIFICATION_CB_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_UNLOCK_BY_IND, payload: lte_ml1_rfmgr_msg_trm_int_unlock_by_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290433, "LTE_ML1_RFMGR_TRM_INT_UNLOCK_BY_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_UNLOCK_IMM_IND, payload: lte_ml1_rfmgr_msg_trm_int_unlock_imm_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290434, "LTE_ML1_RFMGR_TRM_INT_UNLOCK_IMM_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_UNLOCK_CANCEL_IND, payload: lte_ml1_rfmgr_msg_trm_int_unlock_cancel_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290435, "LTE_ML1_RFMGR_TRM_INT_UNLOCK_CANCEL_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_GET_CB_IND, payload: lte_ml1_rfmgr_msg_trm_int_get_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290437, "LTE_ML1_RFMGR_TRM_INT_GET_CB_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_CC_TIMEOUT_IND, payload: lte_ml1_rfmgr_msg_trm_int_cc_timeout_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290438, "LTE_ML1_RFMGR_TRM_INT_CC_TIMEOUT_IND", NULL ),

  /* LTE_ML1_RFMGR_TRM_INT_PI_TIMEOUT_IND, payload: lte_ml1_rfmgr_msg_trm_int_pi_timeout_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04290439, "LTE_ML1_RFMGR_TRM_INT_PI_TIMEOUT_IND", NULL ),

  /* LTE_ML1_BPLMN_SCHDLR_START_CB_REQ, payload: lte_ml1_bplmn_msg_schdlr_start_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0200, "LTE_ML1_BPLMN_SCHDLR_START_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SCHDLR_SUSPEND_CB_REQ, payload: lte_ml1_bplmn_msg_schdlr_suspend_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0201, "LTE_ML1_BPLMN_SCHDLR_SUSPEND_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SCHDLR_ABORT_CB_REQ, payload: lte_ml1_bplmn_msg_schdlr_abort_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0202, "LTE_ML1_BPLMN_SCHDLR_ABORT_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SCHDLR_TIMER_CB_REQ, payload: lte_ml1_bplmn_msg_schdlr_timer_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0203, "LTE_ML1_BPLMN_SCHDLR_TIMER_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_RFMGR_TUNE_CB_REQ, payload: lte_ml1_bplmn_msg_rfmgr_tune_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0204, "LTE_ML1_BPLMN_RFMGR_TUNE_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_RFMGR_CONFIG_CB_REQ, payload: lte_ml1_bplmn_msg_rfmgr_config_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0205, "LTE_ML1_BPLMN_RFMGR_CONFIG_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SRCH_BANDSCAN_CB_REQ, payload: lte_ml1_bplmn_msg_srch_bandscan_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0206, "LTE_ML1_BPLMN_SRCH_BANDSCAN_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SRCH_SYSSCAN_CB_REQ, payload: lte_ml1_bplmn_msg_srch_sysscan_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0207, "LTE_ML1_BPLMN_SRCH_SYSSCAN_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SRCH_PBCH_CB_REQ, payload: lte_ml1_bplmn_msg_srch_pbch_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0208, "LTE_ML1_BPLMN_SRCH_PBCH_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SRCH_CELL_MEAS_CB_REQ, payload: lte_ml1_bplmn_msg_srch_cell_meas_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020a, "LTE_ML1_BPLMN_SRCH_CELL_MEAS_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_DLM_SIB_CB_REQ, payload: lte_ml1_bplmn_msg_dlm_sib_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020b, "LTE_ML1_BPLMN_DLM_SIB_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_DLM_CELL_SWITCH_CB_REQ, payload: lte_ml1_bplmn_msg_dlm_cell_switch_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020c, "LTE_ML1_BPLMN_DLM_CELL_SWITCH_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_RXCFG_CB_REQ, payload: lte_ml1_bplmn_msg_rxcfg_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020d, "LTE_ML1_BPLMN_RXCFG_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_GEN_CB_REQ, payload: lte_ml1_bplmn_msg_gen_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020e, "LTE_ML1_BPLMN_GEN_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_SRCH_ACQ_CB_REQ, payload: lte_ml1_bplmn_msg_srch_acq_cb_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a020f, "LTE_ML1_BPLMN_SRCH_ACQ_CB_REQ", NULL ),

  /* LTE_ML1_BPLMN_PROC_NEXT_IND, payload: lte_ml1_bplmn_msg_proc_next_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0400, "LTE_ML1_BPLMN_PROC_NEXT_IND", NULL ),

  /* LTE_ML1_BPLMN_PROC_ABORT_IND, payload: lte_ml1_bplmn_msg_proc_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0401, "LTE_ML1_BPLMN_PROC_ABORT_IND", NULL ),

  /* LTE_ML1_BPLMN_PROC_STOP_IND, payload: lte_ml1_bplmn_msg_proc_stop_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0402, "LTE_ML1_BPLMN_PROC_STOP_IND", NULL ),

  /* LTE_ML1_BPLMN_PROC_SUSPEND_IND, payload: lte_ml1_bplmn_msg_proc_suspend_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0403, "LTE_ML1_BPLMN_PROC_SUSPEND_IND", NULL ),

  /* LTE_ML1_BPLMN_PROC_CPHY_SUSPEND_IND, payload: lte_ml1_bplmn_msg_proc_cphy_suspend_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0404, "LTE_ML1_BPLMN_PROC_CPHY_SUSPEND_IND", NULL ),

  /* LTE_ML1_BPLMN_TIMER_EXPIRY_IND, payload: lte_ml1_bplmn_msg_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042a0405, "LTE_ML1_BPLMN_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_ENABLE_SLEEP_REQ, payload: lte_ml1_sleepmgr_enable_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0200, "LTE_ML1_SLEEPMGR_ENABLE_SLEEP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_DISABLE_SLEEP_REQ, payload: lte_ml1_sleepmgr_disable_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0201, "LTE_ML1_SLEEPMGR_DISABLE_SLEEP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_OBJ_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0202, "LTE_ML1_SLEEPMGR_OBJ_START_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_OBJ_END_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0203, "LTE_ML1_SLEEPMGR_OBJ_END_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_OBJ_ABORT_REQ, payload: lte_ml1_sleepmgr_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0204, "LTE_ML1_SLEEPMGR_OBJ_ABORT_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_WAKEUP_REQ, payload: lte_ml1_sleepmgr_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0205, "LTE_ML1_SLEEPMGR_WAKEUP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_STMR_ON_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0206, "LTE_ML1_SLEEPMGR_STMR_ON_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_SCTL_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0207, "LTE_ML1_SLEEPMGR_SCTL_START_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_UPDATE_SCLK_ERR_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0208, "LTE_ML1_SLEEPMGR_UPDATE_SCLK_ERR_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_GO_TO_SLEEP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0209, "LTE_ML1_SLEEPMGR_GO_TO_SLEEP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_ENABLE_FEE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020a, "LTE_ML1_SLEEPMGR_ENABLE_FEE_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_DISABLE_FEE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020b, "LTE_ML1_SLEEPMGR_DISABLE_FEE_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_LIGHT_SLEEP_WAKEUP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020c, "LTE_ML1_SLEEPMGR_LIGHT_SLEEP_WAKEUP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_LIGHT_SLEEP_ENABLE_FW_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020d, "LTE_ML1_SLEEPMGR_LIGHT_SLEEP_ENABLE_FW_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_RF_LESS_WAKEUP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020e, "LTE_ML1_SLEEPMGR_RF_LESS_WAKEUP_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_OFFLINE_ENABLE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b020f, "LTE_ML1_SLEEPMGR_OFFLINE_ENABLE_REQ", NULL ),

  /* LTE_ML1_SLEEPMGR_FEE_DONE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0401, "LTE_ML1_SLEEPMGR_FEE_DONE_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_FEE_TIMEOUT_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0402, "LTE_ML1_SLEEPMGR_FEE_TIMEOUT_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_WAKEUP_TIMING_ERR_IND, payload: lte_ml1_sleepmgr_wakeup_timing_err_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0403, "LTE_ML1_SLEEPMGR_WAKEUP_TIMING_ERR_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_SLEEP_START_IND, payload: lte_ml1_sleepmgr_sleep_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0404, "LTE_ML1_SLEEPMGR_SLEEP_START_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_WAKEUP_ISR_IND, payload: lte_ml1_sleepmgr_wakeup_isr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0405, "LTE_ML1_SLEEPMGR_WAKEUP_ISR_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_STMR_ON_IND, payload: lte_ml1_sleepmgr_stmr_on_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0406, "LTE_ML1_SLEEPMGR_STMR_ON_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_OFFLINE_ABORTED_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0407, "LTE_ML1_SLEEPMGR_OFFLINE_ABORTED_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_LIGHT_SLEEP_OBJ_END_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0408, "LTE_ML1_SLEEPMGR_LIGHT_SLEEP_OBJ_END_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_DELAYED_CFG_APP_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0409, "LTE_ML1_SLEEPMGR_DELAYED_CFG_APP_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_DLS_EXIT_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040a, "LTE_ML1_SLEEPMGR_DLS_EXIT_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_WMGR_RESULT_IND, payload: lte_ml1_sleepmgr_wmgr_result_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040b, "LTE_ML1_SLEEPMGR_WMGR_RESULT_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_WMGR_TIMER_EXPIRY_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040c, "LTE_ML1_SLEEPMGR_WMGR_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_TRM_GRANT_CB_IND, payload: lte_ml1_sleepmgr_grant_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040d, "LTE_ML1_SLEEPMGR_TRM_GRANT_CB_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_FAKE_TICK_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040e, "LTE_ML1_SLEEPMGR_FAKE_TICK_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_TRM_GRANT_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b040f, "LTE_ML1_SLEEPMGR_TRM_GRANT_IND", NULL ),

  /* LTE_ML1_SLEEPMGR_RF_WAKEUP_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0801, "LTE_ML1_SLEEPMGR_RF_WAKEUP_CNF", NULL ),

  /* LTE_ML1_SLEEPMGR_RF_SLEEP_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0802, "LTE_ML1_SLEEPMGR_RF_SLEEP_CNF", NULL ),

  /* LTE_ML1_SLEEPMGR_RF_ENTER_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0803, "LTE_ML1_SLEEPMGR_RF_ENTER_CNF", NULL ),

  /* LTE_ML1_SLEEPMGR_RF_EXIT_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042b0804, "LTE_ML1_SLEEPMGR_RF_EXIT_CNF", NULL ),

  /* LTE_ML1_MD_GSM_CLEANUP_DONE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0404, "LTE_ML1_MD_GSM_CLEANUP_DONE_IND", NULL ),

  /* LTE_ML1_MD_GSM_STARTUP_CNF, payload: lte_ml1_md_gsm_startup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0800, "LTE_ML1_MD_GSM_STARTUP_CNF", NULL ),

  /* LTE_ML1_MD_GSM_MEAS_CNF, payload: lte_ml1_md_gsm_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0801, "LTE_ML1_MD_GSM_MEAS_CNF", NULL ),

  /* LTE_ML1_MD_GSM_TD_CNF, payload: lte_ml1_md_gsm_td_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0802, "LTE_ML1_MD_GSM_TD_CNF", NULL ),

  /* LTE_ML1_MD_GSM_BSIC_CNF, payload: lte_ml1_md_gsm_bsic_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0803, "LTE_ML1_MD_GSM_BSIC_CNF", NULL ),

  /* LTE_ML1_MD_GSM_INIT_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0805, "LTE_ML1_MD_GSM_INIT_CNF", NULL ),

  /* LTE_ML1_MD_GSM_TT_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0806, "LTE_ML1_MD_GSM_TT_CNF", NULL ),

  /* LTE_ML1_MD_GSM_TIME_SYNC_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x042c0807, "LTE_ML1_MD_GSM_TIME_SYNC_CNF", NULL ),

  /* LTE_ML1_GAPMGR_SUSPEND_REQ, payload: lte_ml1_gapmgr_msg_suspend_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0200, "LTE_ML1_GAPMGR_SUSPEND_REQ", NULL ),

  /* LTE_ML1_GAPMGR_RESUME_REQ, payload: lte_ml1_gapmgr_msg_resume_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0202, "LTE_ML1_GAPMGR_RESUME_REQ", NULL ),

  /* LTE_ML1_GAPMGR_ALLOCATION_REQ, payload: lte_ml1_gapmgr_msg_allocation_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0203, "LTE_ML1_GAPMGR_ALLOCATION_REQ", NULL ),

  /* LTE_ML1_GAPMGR_GAP_START_REQ, payload: lte_ml1_gapmgr_msg_gap_start_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0204, "LTE_ML1_GAPMGR_GAP_START_REQ", NULL ),

  /* LTE_ML1_GAPMGR_GAP_END_REQ, payload: lte_ml1_gapmgr_msg_gap_end_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0205, "LTE_ML1_GAPMGR_GAP_END_REQ", NULL ),

  /* LTE_ML1_GAPMGR_ABORT_REQ, payload: lte_ml1_gapmgr_msg_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0206, "LTE_ML1_GAPMGR_ABORT_REQ", NULL ),

  /* LTE_ML1_GAPMGR_TIMED_ACTION_REQ, payload: lte_ml1_gapmgr_timed_action_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0207, "LTE_ML1_GAPMGR_TIMED_ACTION_REQ", NULL ),

  /* LTE_ML1_GAPMGR_ALLOC_ABORT_REQ, payload: lte_ml1_gapmgr_alloc_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0208, "LTE_ML1_GAPMGR_ALLOC_ABORT_REQ", NULL ),

  /* LTE_ML1_GAPMGR_PRE_GAP_REQ, payload: lte_ml1_gapmgr_msg_pre_gap_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0210, "LTE_ML1_GAPMGR_PRE_GAP_REQ", NULL ),

  /* LTE_ML1_GAPMGR_STATUS_IND, payload: lte_ml1_gapmgr_msg_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0402, "LTE_ML1_GAPMGR_STATUS_IND", NULL ),

  /* LTE_ML1_GAPMGR_CDRX_RATS_COMPLETED_IND, payload: lte_ml1_gapmgr_msg_cdrx_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0403, "LTE_ML1_GAPMGR_CDRX_RATS_COMPLETED_IND", NULL ),

  /* LTE_ML1_GAPMGR_RAT_COMPLETED_IND, payload: lte_ml1_gapmgr_msg_rat_status_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0404, "LTE_ML1_GAPMGR_RAT_COMPLETED_IND", NULL ),

  /* LTE_ML1_GAPMGR_SUSPEND_CNF, payload: lte_ml1_gapmgr_msg_suspend_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0800, "LTE_ML1_GAPMGR_SUSPEND_CNF", NULL ),

  /* LTE_ML1_GAPMGR_RESUME_CNF, payload: lte_ml1_gapmgr_msg_resume_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042d0801, "LTE_ML1_GAPMGR_RESUME_CNF", NULL ),

  /* LTE_ML1_MD_WCDMA_SRCH_CNF, payload: lte_ml1_md_wcdma_msg_srch_cnf_t */
  MSGR_UMID_TABLE_ENTRY ( 0x042e0801, "LTE_ML1_MD_WCDMA_SRCH_CNF", NULL ),

  /* LTE_ML1_MD_WCDMA_MEAS_CNF, payload: lte_ml1_md_wcdma_msg_meas_cnf_t */
  MSGR_UMID_TABLE_ENTRY ( 0x042e0802, "LTE_ML1_MD_WCDMA_MEAS_CNF", NULL ),

  /* LTE_ML1_MD_WCDMA_ABORT_DONE_CNF, payload: lte_ml1_md_wcdma_msg_abort_cnf_t */
  MSGR_UMID_TABLE_ENTRY ( 0x042e0803, "LTE_ML1_MD_WCDMA_ABORT_DONE_CNF", NULL ),

  /* LTE_ML1_MD_WCDMA_ONLINE_PROC_DONE_CNF, payload: lte_ml1_md_wcdma_msg_online_proc_done_t */
  MSGR_UMID_TABLE_ENTRY ( 0x042e0804, "LTE_ML1_MD_WCDMA_ONLINE_PROC_DONE_CNF", NULL ),

  /* LTE_ML1_MD_WCDMA_CLEANUP_DONE_CNF, payload: lte_ml1_md_wcdma_msg_cleanup_cnf_t */
  MSGR_UMID_TABLE_ENTRY ( 0x042e0805, "LTE_ML1_MD_WCDMA_CLEANUP_DONE_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0102, "LTE_CPHY_IRAT_MEAS_W2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0103, "LTE_CPHY_IRAT_MEAS_W2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0200, "LTE_CPHY_IRAT_MEAS_W2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0201, "LTE_CPHY_IRAT_MEAS_W2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0204, "LTE_CPHY_IRAT_MEAS_W2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0205, "LTE_CPHY_IRAT_MEAS_W2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0206, "LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0207, "LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0208, "LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0800, "LTE_CPHY_IRAT_MEAS_W2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0801, "LTE_CPHY_IRAT_MEAS_W2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0804, "LTE_CPHY_IRAT_MEAS_W2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0805, "LTE_CPHY_IRAT_MEAS_W2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0806, "LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0807, "LTE_CPHY_IRAT_MEAS_W2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x042f0808, "LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300102, "LTE_CPHY_IRAT_MEAS_G2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300103, "LTE_CPHY_IRAT_MEAS_G2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300200, "LTE_CPHY_IRAT_MEAS_G2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300201, "LTE_CPHY_IRAT_MEAS_G2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300204, "LTE_CPHY_IRAT_MEAS_G2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300205, "LTE_CPHY_IRAT_MEAS_G2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300206, "LTE_CPHY_IRAT_MEAS_G2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300207, "LTE_CPHY_IRAT_MEAS_G2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300208, "LTE_CPHY_IRAT_MEAS_G2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300800, "LTE_CPHY_IRAT_MEAS_G2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300801, "LTE_CPHY_IRAT_MEAS_G2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300804, "LTE_CPHY_IRAT_MEAS_G2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300805, "LTE_CPHY_IRAT_MEAS_G2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300806, "LTE_CPHY_IRAT_MEAS_G2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300807, "LTE_CPHY_IRAT_MEAS_G2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_G2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04300808, "LTE_CPHY_IRAT_MEAS_G2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_ML1_POS_PRS_OCCASION_OBJ_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320201, "LTE_ML1_POS_PRS_OCCASION_OBJ_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_OCCASION_OBJ_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320202, "LTE_ML1_POS_PRS_OCCASION_OBJ_ABORT_REQ", NULL ),

  /* LTE_ML1_POS_PRS_PBCH_OBJ_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320203, "LTE_ML1_POS_PRS_PBCH_OBJ_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_PBCH_OBJ_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320204, "LTE_ML1_POS_PRS_PBCH_OBJ_ABORT_REQ", NULL ),

  /* LTE_ML1_POS_PRS_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320205, "LTE_ML1_POS_PRS_ABORT_REQ", NULL ),

  /* LTE_ML1_POS_PRS_SUSPEND_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320206, "LTE_ML1_POS_PRS_SUSPEND_REQ", NULL ),

  /* LTE_ML1_POS_PRS_RESUME_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320207, "LTE_ML1_POS_PRS_RESUME_REQ", NULL ),

  /* LTE_ML1_POS_PRS_SERV_CELL_CHANGE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320208, "LTE_ML1_POS_PRS_SERV_CELL_CHANGE_REQ", NULL ),

  /* LTE_ML1_POS_PRS_CSWITCH_TICK_OBJ_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432020b, "LTE_ML1_POS_PRS_CSWITCH_TICK_OBJ_ABORT_REQ", NULL ),

  /* LTE_ML1_POS_PRS_CSWITCH_TICK_OBJ_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432020c, "LTE_ML1_POS_PRS_CSWITCH_TICK_OBJ_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_CSWITCH_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432020d, "LTE_ML1_POS_PRS_CSWITCH_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_CSWITCH_END_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432020e, "LTE_ML1_POS_PRS_CSWITCH_END_REQ", NULL ),

  /* LTE_ML1_POS_PRS_HO_SUSPEND_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432020f, "LTE_ML1_POS_PRS_HO_SUSPEND_REQ", NULL ),

  /* LTE_ML1_POS_PRS_OCCASION_OBJ_FW_PROCESS_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320210, "LTE_ML1_POS_PRS_OCCASION_OBJ_FW_PROCESS_START_REQ", NULL ),

  /* LTE_ML1_POS_TIMETAG_FLUSH_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320220, "LTE_ML1_POS_TIMETAG_FLUSH_REQ", NULL ),

  /* LTE_ML1_POS_ECID_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320223, "LTE_ML1_POS_ECID_REQ", NULL ),

  /* LTE_ML1_POS_MRL_UPDATE_TA_REQ, payload: lte_ml1_pos_mrl_update_ta_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04320230, "LTE_ML1_POS_MRL_UPDATE_TA_REQ", NULL ),

  /* LTE_ML1_POS_TEST_MRL_RXED_REQ, payload: lte_ml1_pos_mrl_update_ta_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04320252, "LTE_ML1_POS_TEST_MRL_RXED_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320260, "LTE_ML1_POS_PRS_INTER_FREQ_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_LNA_GAP_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320261, "LTE_ML1_POS_PRS_INTER_FREQ_LNA_GAP_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_MEAS_GAP_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320262, "LTE_ML1_POS_PRS_INTER_FREQ_MEAS_GAP_START_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_GAP_END_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320264, "LTE_ML1_POS_PRS_INTER_FREQ_GAP_END_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_STOP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320265, "LTE_ML1_POS_PRS_INTER_FREQ_STOP_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320266, "LTE_ML1_POS_PRS_INTER_FREQ_ABORT_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_NEW_GAP_CFG_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320267, "LTE_ML1_POS_PRS_INTER_FREQ_NEW_GAP_CFG_REQ", NULL ),

  /* LTE_ML1_POS_PRS_GAP_CFG_TIMER_EXPIRY_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320268, "LTE_ML1_POS_PRS_GAP_CFG_TIMER_EXPIRY_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_SCC_GAP_RECONFIGURATION_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320269, "LTE_ML1_POS_PRS_INTER_FREQ_SCC_GAP_RECONFIGURATION_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_INTRA_FREQ_DONE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432026a, "LTE_ML1_POS_PRS_INTER_FREQ_INTRA_FREQ_DONE_REQ", NULL ),

  /* LTE_ML1_POS_PRS_INTER_FREQ_INTER_FREQ_DONE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432026b, "LTE_ML1_POS_PRS_INTER_FREQ_INTER_FREQ_DONE_REQ", NULL ),

  /* LTE_ML1_POS_OPCRS_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320270, "LTE_ML1_POS_OPCRS_START_REQ", NULL ),

  /* LTE_ML1_POS_OPCRS_STOP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320271, "LTE_ML1_POS_OPCRS_STOP_REQ", NULL ),

  /* LTE_ML1_POS_OPCRS_FW_AWAKE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320272, "LTE_ML1_POS_OPCRS_FW_AWAKE_REQ", NULL ),

  /* LTE_ML1_POS_PRS_SM_SEARCH_IND, payload: lte_pos_prs_sm_search_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04320409, "LTE_ML1_POS_PRS_SM_SEARCH_IND", NULL ),

  /* LTE_ML1_POS_TIMEXFER_WAKEUP_COMPLETE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320421, "LTE_ML1_POS_TIMEXFER_WAKEUP_COMPLETE_IND", NULL ),

  /* LTE_ML1_POS_TIMEXFER_SIB8_VALID_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320422, "LTE_ML1_POS_TIMEXFER_SIB8_VALID_IND", NULL ),

  /* LTE_ML1_POS_RXTX_WAKEUP_COMPLETE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320424, "LTE_ML1_POS_RXTX_WAKEUP_COMPLETE_IND", NULL ),

  /* LTE_ML1_POS_TIMEXFER_SIB16_VALID_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320425, "LTE_ML1_POS_TIMEXFER_SIB16_VALID_IND", NULL ),

  /* LTE_ML1_POS_TEST_PRS_MEAS_ERROR_IND, payload: lte_ml1_pos_prs_meas_error_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04320450, "LTE_ML1_POS_TEST_PRS_MEAS_ERROR_IND", NULL ),

  /* LTE_ML1_POS_TEST_PRS_MEAS_RESULTS_AVAIL_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320451, "LTE_ML1_POS_TEST_PRS_MEAS_RESULTS_AVAIL_IND", NULL ),

  /* LTE_ML1_POS_PRS_ABORT_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320805, "LTE_ML1_POS_PRS_ABORT_CNF", NULL ),

  /* LTE_ML1_POS_PRS_SUSPEND_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04320806, "LTE_ML1_POS_PRS_SUSPEND_CNF", NULL ),

  /* LTE_ML1_POS_PRS_SM_PBCH_DECODE_CNF, payload: lte_pos_prs_sm_pbch_decode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0432080a, "LTE_ML1_POS_PRS_SM_PBCH_DECODE_CNF", NULL ),

  /* LTE_ML1_POS_PRS_HO_SUSPEND_CNF, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0432080f, "LTE_ML1_POS_PRS_HO_SUSPEND_CNF", NULL ),

  /* LTE_FC_SHUTDOWN_IND, payload: lte_fc_shutdown_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04330400, "LTE_FC_SHUTDOWN_IND", NULL ),

  /* LTE_ML1_AFC_RELEASE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340201, "LTE_ML1_AFC_RELEASE_REQ", NULL ),

  /* LTE_ML1_AFC_RGS_TIMER_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340202, "LTE_ML1_AFC_RGS_TIMER_REQ", NULL ),

  /* LTE_ML1_AFC_RPUSH_FLAG_REQ, payload: lte_ml1_afc_rpush_flag_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x04340203, "LTE_ML1_AFC_RPUSH_FLAG_REQ", NULL ),

  /* LTE_ML1_AFC_RPUSH_SAMPLE_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340204, "LTE_ML1_AFC_RPUSH_SAMPLE_REQ", NULL ),

  /* LTE_ML1_AFC_RV_XO_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340205, "LTE_ML1_AFC_RV_XO_REQ", NULL ),

  /* LTE_ML1_AFC_LOG_TIMER_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340206, "LTE_ML1_AFC_LOG_TIMER_REQ", NULL ),

  /* LTE_ML1_AFC_SLEEP_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x04340207, "LTE_ML1_AFC_SLEEP_REQ", NULL ),

  /* LTE_ML1_AFC_TCXOMGR_GRANT_REQ, payload: lte_ml1_afc_tcxomgr_restriction_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x04340208, "LTE_ML1_AFC_TCXOMGR_GRANT_REQ", NULL ),

  /* LTE_ML1_AFC_TCXOMGR_CHANGE_REQ, payload: lte_ml1_afc_tcxomgr_restriction_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x04340209, "LTE_ML1_AFC_TCXOMGR_CHANGE_REQ", NULL ),

  /* LTE_ML1_AFC_FLUSH_RPUSH_REQ, payload: lte_ml1_afc_flush_rpush_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0434020a, "LTE_ML1_AFC_FLUSH_RPUSH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350102, "LTE_CPHY_IRAT_MEAS_D2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350103, "LTE_CPHY_IRAT_MEAS_D2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350200, "LTE_CPHY_IRAT_MEAS_D2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350201, "LTE_CPHY_IRAT_MEAS_D2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350204, "LTE_CPHY_IRAT_MEAS_D2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350205, "LTE_CPHY_IRAT_MEAS_D2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350206, "LTE_CPHY_IRAT_MEAS_D2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350207, "LTE_CPHY_IRAT_MEAS_D2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350208, "LTE_CPHY_IRAT_MEAS_D2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350800, "LTE_CPHY_IRAT_MEAS_D2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350801, "LTE_CPHY_IRAT_MEAS_D2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350804, "LTE_CPHY_IRAT_MEAS_D2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350805, "LTE_CPHY_IRAT_MEAS_D2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350806, "LTE_CPHY_IRAT_MEAS_D2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350807, "LTE_CPHY_IRAT_MEAS_D2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_D2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04350808, "LTE_CPHY_IRAT_MEAS_D2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360102, "LTE_CPHY_IRAT_MEAS_C2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360103, "LTE_CPHY_IRAT_MEAS_C2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360200, "LTE_CPHY_IRAT_MEAS_C2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360201, "LTE_CPHY_IRAT_MEAS_C2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360204, "LTE_CPHY_IRAT_MEAS_C2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360205, "LTE_CPHY_IRAT_MEAS_C2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360206, "LTE_CPHY_IRAT_MEAS_C2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360207, "LTE_CPHY_IRAT_MEAS_C2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360208, "LTE_CPHY_IRAT_MEAS_C2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360800, "LTE_CPHY_IRAT_MEAS_C2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360801, "LTE_CPHY_IRAT_MEAS_C2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360804, "LTE_CPHY_IRAT_MEAS_C2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360805, "LTE_CPHY_IRAT_MEAS_C2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360806, "LTE_CPHY_IRAT_MEAS_C2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360807, "LTE_CPHY_IRAT_MEAS_C2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_C2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04360808, "LTE_CPHY_IRAT_MEAS_C2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370102, "LTE_CPHY_IRAT_MEAS_F2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370103, "LTE_CPHY_IRAT_MEAS_F2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370200, "LTE_CPHY_IRAT_MEAS_F2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370201, "LTE_CPHY_IRAT_MEAS_F2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370204, "LTE_CPHY_IRAT_MEAS_F2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370205, "LTE_CPHY_IRAT_MEAS_F2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370206, "LTE_CPHY_IRAT_MEAS_F2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370207, "LTE_CPHY_IRAT_MEAS_F2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370208, "LTE_CPHY_IRAT_MEAS_F2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370800, "LTE_CPHY_IRAT_MEAS_F2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370801, "LTE_CPHY_IRAT_MEAS_F2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370804, "LTE_CPHY_IRAT_MEAS_F2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370805, "LTE_CPHY_IRAT_MEAS_F2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370806, "LTE_CPHY_IRAT_MEAS_F2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370807, "LTE_CPHY_IRAT_MEAS_F2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_F2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04370808, "LTE_CPHY_IRAT_MEAS_F2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_ML1_COEX_STATE_UPDATE_AND_NOTIFY_REQ, payload: lte_ml1_coex_state_update_and_notify_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380200, "LTE_ML1_COEX_STATE_UPDATE_AND_NOTIFY_REQ", NULL ),

  /* LTE_ML1_COEX_FRAME_TIMING_REQ, payload: lte_ml1_coex_frame_timing_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380201, "LTE_ML1_COEX_FRAME_TIMING_REQ", NULL ),

  /* LTE_ML1_COEX_GM_BACKOFF_REQ, payload: lte_ml1_coex_gm_backoff_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380202, "LTE_ML1_COEX_GM_BACKOFF_REQ", NULL ),

  /* LTE_ML1_COEX_GM_BACKOFF_IND, payload: lte_ml1_coex_gm_backoff_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380403, "LTE_ML1_COEX_GM_BACKOFF_IND", NULL ),

  /* LTE_ML1_COEX_PENDING_TA_EXPIRY_IND, payload: lte_ml1_coex_pending_ta_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380404, "LTE_ML1_COEX_PENDING_TA_EXPIRY_IND", NULL ),

  /* LTE_ML1_COEX_LOG_TIMER_EXPIRY_IND, payload: lte_ml1_coex_log_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380405, "LTE_ML1_COEX_LOG_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_COEX_SEND_POWER_IND, payload: lte_ml1_coex_log_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04380406, "LTE_ML1_COEX_SEND_POWER_IND", NULL ),

  /* LTE_ML1_CXM_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390000, "LTE_ML1_CXM_LOOPBACK_SPR", NULL ),

  /* LTE_ML1_CXM_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390001, "LTE_ML1_CXM_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_ML1_CXM_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04390002, "LTE_ML1_CXM_THREAD_READY_SPR", NULL ),

  /* LTE_ML1_CXM_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x04390003, "LTE_ML1_CXM_THREAD_KILL_SPR", NULL ),

  /* LTE_ML1_CXM_STATE_UPDATE_REQ, payload: lte_ml1_cxm_state_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390200, "LTE_ML1_CXM_STATE_UPDATE_REQ", NULL ),

  /* LTE_ML1_CXM_ACTIVATION_REQ, payload: lte_ml1_cxm_activation_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390202, "LTE_ML1_CXM_ACTIVATION_REQ", NULL ),

  /* LTE_ML1_CXM_DEACTIVATION_REQ, payload: lte_ml1_cxm_deactivation_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390203, "LTE_ML1_CXM_DEACTIVATION_REQ", NULL ),

  /* LTE_ML1_CXM_READING_COUNTER_REQ, payload: lte_ml1_cxm_reading_count_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390206, "LTE_ML1_CXM_READING_COUNTER_REQ", NULL ),

  /* LTE_ML1_CXM_PHR_LESS_BACKOFF_REQ, payload: lte_ml1_cxm_phr_less_backoff_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390209, "LTE_ML1_CXM_PHR_LESS_BACKOFF_REQ", NULL ),

  /* LTE_ML1_CXM_PHR_BACKOFF_REQ, payload: lte_ml1_cxm_phr_backoff_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0439020a, "LTE_ML1_CXM_PHR_BACKOFF_REQ", NULL ),

  /* LTE_ML1_CXM_STATE_IND, payload: lte_ml1_cxm_state_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390401, "LTE_ML1_CXM_STATE_IND", NULL ),

  /* LTE_ML1_CXM_NOTIFY_EVENT_IND, payload: lte_ml1_cxm_notify_event_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390404, "LTE_ML1_CXM_NOTIFY_EVENT_IND", NULL ),

  /* LTE_ML1_CXM_FRAME_TIMING_IND, payload: lte_ml1_cxm_frame_timing_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390405, "LTE_ML1_CXM_FRAME_TIMING_IND", NULL ),

  /* LTE_ML1_CXM_RC_COMPLETE_IND, payload: lte_ml1_cxm_rc_complete_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390408, "LTE_ML1_CXM_RC_COMPLETE_IND", NULL ),

  /* LTE_ML1_CXM_QMI_SMD_RX_IND, payload: lte_ml1_cxm_qmi_smd_rx_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0439040b, "LTE_ML1_CXM_QMI_SMD_RX_IND", NULL ),

  /* LTE_ML1_CXM_BAND_FILTER_IND, payload: lte_ml1_cxm_band_filter_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0439040c, "LTE_ML1_CXM_BAND_FILTER_IND", NULL ),

  /* LTE_ML1_CXM_STATE_INIT_IND, payload: lte_ml1_cxm_state_init_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0439040d, "LTE_ML1_CXM_STATE_INIT_IND", NULL ),

  /* LTE_ML1_CXM_READING_COUNTER_CNF, payload: lte_ml1_cxm_reading_count_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x04390807, "LTE_ML1_CXM_READING_COUNTER_CNF", NULL ),

  /* LTE_ML1_COMMON_ANT_CORR_RPT_START_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0200, "LTE_ML1_COMMON_ANT_CORR_RPT_START_REQ", NULL ),

  /* LTE_ML1_COMMON_ANT_CORR_RPT_ABORT_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0201, "LTE_ML1_COMMON_ANT_CORR_RPT_ABORT_REQ", NULL ),

  /* LTE_ML1_COMMON_ANT_CORR_RPT_END_REQ, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0202, "LTE_ML1_COMMON_ANT_CORR_RPT_END_REQ", NULL ),

  /* LTE_ML1_COMMON_FC_THERMAL_IND, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0403, "LTE_ML1_COMMON_FC_THERMAL_IND", NULL ),

  /* LTE_ML1_COMMON_FC_TIMER_EXPIRY_IND, payload: lte_ml1_common_fc_timer_expiry_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0404, "LTE_ML1_COMMON_FC_TIMER_EXPIRY_IND", NULL ),

  /* LTE_ML1_COMMON_FC_CPU_IND, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0405, "LTE_ML1_COMMON_FC_CPU_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_TRIGGER_VALUE_IND, payload: switch_trigger_value_ind */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0406, "LTE_ML1_COMMON_ANT_SWITCH_TRIGGER_VALUE_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_TX_SET_MODE_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0407, "LTE_ML1_COMMON_ANT_SWITCH_TX_SET_MODE_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_TRIGGER_IND, payload: lte_ml1_common_ant_switch_trigger_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0408, "LTE_ML1_COMMON_ANT_SWITCH_TRIGGER_IND", NULL ),

  /* LTE_ML1_COMMON_ASDIV_START_CB_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0410, "LTE_ML1_COMMON_ASDIV_START_CB_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_START_TX_BLANK_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0411, "LTE_ML1_COMMON_ANT_SWITCH_START_TX_BLANK_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_STOP_TX_BLANK_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0412, "LTE_ML1_COMMON_ANT_SWITCH_STOP_TX_BLANK_IND", NULL ),

  /* LTE_ML1_COMMON_ANT_SWITCH_RESCHED_ENV_IND, payload: msgr_hdr_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x043a0413, "LTE_ML1_COMMON_ANT_SWITCH_RESCHED_ENV_IND", NULL ),

  /* LTE_PDCPOFFLOAD_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0000, "LTE_PDCPOFFLOAD_LOOPBACK_SPR", NULL ),

  /* LTE_PDCPOFFLOAD_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0001, "LTE_PDCPOFFLOAD_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_PDCPOFFLOAD_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0002, "LTE_PDCPOFFLOAD_THREAD_READY_SPR", NULL ),

  /* LTE_PDCPOFFLOAD_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0003, "LTE_PDCPOFFLOAD_THREAD_KILL_SPR", NULL ),

  /* LTE_PDCPOFFLOAD_START_REQ, payload: lte_pdcp_offload_start_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0201, "LTE_PDCPOFFLOAD_START_REQ", NULL ),

  /* LTE_PDCPOFFLOAD_STOP_REQ, payload: lte_pdcp_offload_stop_req_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0202, "LTE_PDCPOFFLOAD_STOP_REQ", NULL ),

  /* LTE_PDCPOFFLOAD_WM_ENQUEUE_IND, payload: lte_pdcp_offload_wm_enqueue_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0400, "LTE_PDCPOFFLOAD_WM_ENQUEUE_IND", NULL ),

  /* LTE_PDCPOFFLOAD_WM_LOW_IND, payload: lte_pdcp_offload_wm_low_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0401, "LTE_PDCPOFFLOAD_WM_LOW_IND", NULL ),

  /* LTE_PDCPOFFLOAD_COMP_DONE_IND, payload: lte_pdcp_offload_comp_done_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043b0402, "LTE_PDCPOFFLOAD_COMP_DONE_IND", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_STARTUP_CMD, payload: lte_cphy_irat_meas_startup_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0102, "LTE_CPHY_IRAT_MEAS_T2L_STARTUP_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_BLACKLIST_CMD, payload: lte_cphy_irat_meas_blacklist_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0103, "LTE_CPHY_IRAT_MEAS_T2L_BLACKLIST_CMD", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_INIT_REQ, payload: lte_cphy_irat_meas_init_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0200, "LTE_CPHY_IRAT_MEAS_T2L_INIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_DEINIT_REQ, payload: lte_cphy_irat_meas_deinit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0201, "LTE_CPHY_IRAT_MEAS_T2L_DEINIT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_SEARCH_REQ, payload: lte_cphy_irat_meas_search_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0204, "LTE_CPHY_IRAT_MEAS_T2L_SEARCH_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_MEAS_REQ, payload: lte_cphy_irat_meas_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0205, "LTE_CPHY_IRAT_MEAS_T2L_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_CLEANUP_REQ, payload: lte_cphy_irat_meas_cleanup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0206, "LTE_CPHY_IRAT_MEAS_T2L_CLEANUP_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_ABORT_REQ, payload: lte_cphy_irat_meas_abort_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0207, "LTE_CPHY_IRAT_MEAS_T2L_ABORT_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_TIMED_SRCH_MEAS_REQ, payload: lte_cphy_irat_meas_timed_srch_meas_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0208, "LTE_CPHY_IRAT_MEAS_T2L_TIMED_SRCH_MEAS_REQ", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_INIT_CNF, payload: lte_cphy_irat_meas_init_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0800, "LTE_CPHY_IRAT_MEAS_T2L_INIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_DEINIT_CNF, payload: lte_cphy_irat_meas_deinit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0801, "LTE_CPHY_IRAT_MEAS_T2L_DEINIT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_SEARCH_CNF, payload: lte_cphy_irat_meas_search_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0804, "LTE_CPHY_IRAT_MEAS_T2L_SEARCH_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_MEAS_CNF, payload: lte_cphy_irat_meas_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0805, "LTE_CPHY_IRAT_MEAS_T2L_MEAS_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_CLEANUP_CNF, payload: lte_cphy_irat_meas_cleanup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0806, "LTE_CPHY_IRAT_MEAS_T2L_CLEANUP_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_ABORT_CNF, payload: lte_cphy_irat_meas_abort_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0807, "LTE_CPHY_IRAT_MEAS_T2L_ABORT_CNF", NULL ),

  /* LTE_CPHY_IRAT_MEAS_T2L_TIMED_SRCH_MEAS_CNF, payload: lte_cphy_irat_meas_timed_srch_meas_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043c0808, "LTE_CPHY_IRAT_MEAS_T2L_TIMED_SRCH_MEAS_CNF", NULL ),

  /* LTE_ML1_MCLK_MCVS_START_IND, payload: lte_ml1_mcvs_obj_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0400, "LTE_ML1_MCLK_MCVS_START_IND", NULL ),

  /* LTE_ML1_MCLK_MCVS_ABORTED_IND, payload: lte_ml1_mcvs_obj_aborted_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0401, "LTE_ML1_MCLK_MCVS_ABORTED_IND", NULL ),

  /* LTE_ML1_MCLK_MCVS_END_IND, payload: lte_ml1_mcvs_obj_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0402, "LTE_ML1_MCLK_MCVS_END_IND", NULL ),

  /* LTE_ML1_MCLK_SIB_OBJ_START_IND, payload: lte_ml1_mcvs_sib_obj_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0403, "LTE_ML1_MCLK_SIB_OBJ_START_IND", NULL ),

  /* LTE_ML1_MCLK_SIB_OBJ_END_IND, payload: lte_ml1_mcvs_sib_obj_end_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0404, "LTE_ML1_MCLK_SIB_OBJ_END_IND", NULL ),

  /* LTE_ML1_MCLK_PAGE_OBJ_START_IND, payload: lte_ml1_mcvs_page_obj_start_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0405, "LTE_ML1_MCLK_PAGE_OBJ_START_IND", NULL ),

  /* LTE_ML1_MCLK_MCPM_DONE_IND, payload: lte_ml1_mcpm_done_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043d0410, "LTE_ML1_MCLK_MCPM_DONE_IND", NULL ),

  /* LTE_ML1_OFFLOAD_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0000, "LTE_ML1_OFFLOAD_LOOPBACK_SPR", NULL ),

  /* LTE_ML1_OFFLOAD_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0001, "LTE_ML1_OFFLOAD_LOOPBACK_REPLY_SPR", NULL ),

  /* LTE_ML1_OFFLOAD_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0002, "LTE_ML1_OFFLOAD_THREAD_READY_SPR", NULL ),

  /* LTE_ML1_OFFLOAD_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0003, "LTE_ML1_OFFLOAD_THREAD_KILL_SPR", NULL ),

  /* LTE_ML1_OFFLOAD_MCPM_UPDATE_REQ, payload: lte_ml1_offload_mcpm_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0201, "LTE_ML1_OFFLOAD_MCPM_UPDATE_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_MCVS_PRESCALE_REQ, payload: lte_ml1_offload_mcvs_prescale_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0202, "LTE_ML1_OFFLOAD_MCVS_PRESCALE_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_MCVS_UPDATE_REQ, payload: lte_ml1_offload_mcvs_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0203, "LTE_ML1_OFFLOAD_MCVS_UPDATE_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_MCVS_RELEASE_REQ, payload: lte_ml1_offload_mcvs_release_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0204, "LTE_ML1_OFFLOAD_MCVS_RELEASE_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_MCPM_IRAT_UPDATE_REQ, payload: lte_ml1_offload_mcpm_irat_update_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0205, "LTE_ML1_OFFLOAD_MCPM_IRAT_UPDATE_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_BUILD_RF_SCRIPT_REQ, payload: lte_ml1_offload_build_rf_script_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0206, "LTE_ML1_OFFLOAD_BUILD_RF_SCRIPT_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_GL1_HW_QTA_REQ, payload: lte_ml1_offload_gl1_hw_qta_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0207, "LTE_ML1_OFFLOAD_GL1_HW_QTA_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_L2G_PREBUILD_RF_SCRIPT_REQ, payload: lte_ml1_offload_l2g_prebuild_rf_script_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0208, "LTE_ML1_OFFLOAD_L2G_PREBUILD_RF_SCRIPT_REQ", NULL ),

  /* LTE_ML1_OFFLOAD_MCPM_UPDATE_CNF, payload: lte_ml1_offload_mcpm_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0801, "LTE_ML1_OFFLOAD_MCPM_UPDATE_CNF", NULL ),

  /* LTE_ML1_OFFLOAD_MCVS_PRESCALE_CNF, payload: lte_ml1_offload_mcvs_prescale_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0802, "LTE_ML1_OFFLOAD_MCVS_PRESCALE_CNF", NULL ),

  /* LTE_ML1_OFFLOAD_MCVS_UPDATE_CNF, payload: lte_ml1_offload_mcvs_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0803, "LTE_ML1_OFFLOAD_MCVS_UPDATE_CNF", NULL ),

  /* LTE_ML1_OFFLOAD_BUILD_RF_SCRIPT_CNF, payload: lte_ml1_offload_build_rf_script_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0804, "LTE_ML1_OFFLOAD_BUILD_RF_SCRIPT_CNF", NULL ),

  /* LTE_ML1_OFFLOAD_L2G_PREBUILD_RF_SCRIPT_CNF, payload: lte_ml1_offload_l2g_prebuild_rf_script_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043e0805, "LTE_ML1_OFFLOAD_L2G_PREBUILD_RF_SCRIPT_CNF", NULL ),

  /* LTE_ML1_COEX_DSDA_PRIO_SWITCH_REQ, payload: lte_ml1_coex_dsda_prio_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0201, "LTE_ML1_COEX_DSDA_PRIO_SWITCH_REQ", NULL ),

  /* LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_REQ, payload: lte_ml1_coex_dsda_cancel_prio_switch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0203, "LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_REQ", NULL ),

  /* LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_REQ, payload: lte_ml1_coex_dsda_prio_switch_handover_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0205, "LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_REQ", NULL ),

  /* LTE_ML1_COEX_DSDA_ACTIVITY_PRIO_IND, payload: lte_ml1_coex_dsda_prio_switch_handover_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0407, "LTE_ML1_COEX_DSDA_ACTIVITY_PRIO_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_ARB_IND, payload: lte_ml1_coex_dsda_arb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0408, "LTE_ML1_COEX_DSDA_ARB_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_SCHDLR_IND, payload: lte_ml1_coex_dsda_schdlr_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0409, "LTE_ML1_COEX_DSDA_SCHDLR_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_BA_TIMEOUT_IND, payload: lte_ml1_coex_dsda_ba_timeout_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f040a, "LTE_ML1_COEX_DSDA_BA_TIMEOUT_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_PRIO_SW_CB_IND, payload: lte_ml1_coex_dsda_prio_sw_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f040b, "LTE_ML1_COEX_DSDA_PRIO_SW_CB_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_ASAP_PRIO_SW_CB_IND, payload: lte_ml1_coex_dsda_prio_sw_cb_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f040c, "LTE_ML1_COEX_DSDA_ASAP_PRIO_SW_CB_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_DISABLE_CONFLICT_IND, payload: lte_ml1_coex_dsda_disable_conflict_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f040d, "LTE_ML1_COEX_DSDA_DISABLE_CONFLICT_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_RES_MODE_CHANGE_IND, payload: lte_ml1_coex_dsda_res_mode_change_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f040e, "LTE_ML1_COEX_DSDA_RES_MODE_CHANGE_IND", NULL ),

  /* LTE_ML1_COEX_DSDA_PRIO_SWITCH_CNF, payload: lte_ml1_coex_dsda_prio_switch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0802, "LTE_ML1_COEX_DSDA_PRIO_SWITCH_CNF", NULL ),

  /* LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_CNF, payload: lte_ml1_coex_dsda_cancel_prio_switch_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0804, "LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_CNF", NULL ),

  /* LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_CNF, payload: lte_ml1_coex_dsda_prio_switch_handover_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x043f0806, "LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_CNF", NULL ),

  /* FTM_LTE_NS_START_LTE_MODE_REQ, payload: ftm_lte_ns_start_mode_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020200, "FTM_LTE_NS_START_LTE_MODE_REQ", NULL ),

  /* FTM_LTE_NS_STOP_LTE_MODE_REQ, payload: ftm_lte_ns_stop_mode_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020201, "FTM_LTE_NS_STOP_LTE_MODE_REQ", NULL ),

  /* FTM_LTE_NS_IDLE_REQ, payload: ftm_lte_ns_idle_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020202, "FTM_LTE_NS_IDLE_REQ", NULL ),

  /* FTM_LTE_NS_ACQ_REQ, payload: ftm_lte_ns_acq_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020203, "FTM_LTE_NS_ACQ_REQ", NULL ),

  /* FTM_LTE_NS_START_DP_REQ, payload: ftm_lte_ns_start_dp_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020204, "FTM_LTE_NS_START_DP_REQ", NULL ),

  /* FTM_LTE_NS_IS_CONN_REQ, payload: ftm_lte_ns_is_conn_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020206, "FTM_LTE_NS_IS_CONN_REQ", NULL ),

  /* FTM_LTE_NS_CONFIG_UL_WAVEFORM_REQ, payload: ftm_lte_ns_config_ul_waveform_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020207, "FTM_LTE_NS_CONFIG_UL_WAVEFORM_REQ", NULL ),

  /* FTM_LTE_NS_CONFIG_UL_POWER_REQ, payload: ftm_lte_ns_config_ul_power_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020208, "FTM_LTE_NS_CONFIG_UL_POWER_REQ", NULL ),

  /* FTM_LTE_NS_GET_DL_LEVEL_REQ, payload: ftm_lte_ns_get_dl_level_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020209, "FTM_LTE_NS_GET_DL_LEVEL_REQ", NULL ),

  /* FTM_LTE_NS_RESET_DL_BLER_REPORT_REQ, payload: ftm_lte_ns_reset_dl_bler_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020a, "FTM_LTE_NS_RESET_DL_BLER_REPORT_REQ", NULL ),

  /* FTM_LTE_NS_GET_DL_BLER_REPORT_REQ, payload: ftm_lte_ns_get_dl_bler_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020b, "FTM_LTE_NS_GET_DL_BLER_REPORT_REQ", NULL ),

  /* FTM_LTE_NS_HANDOVER_REQ, payload: ftm_lte_ns_handover_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020c, "FTM_LTE_NS_HANDOVER_REQ", NULL ),

  /* FTM_LTE_NS_TDD_CONFIG_REQ, payload: ftm_lte_ns_tdd_config_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020d, "FTM_LTE_NS_TDD_CONFIG_REQ", NULL ),

  /* FTM_LTE_NS_ENABLE_SCELL_REQ, payload: ftm_lte_ns_enable_scell_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020e, "FTM_LTE_NS_ENABLE_SCELL_REQ", NULL ),

  /* FTM_LTE_NS_DISABLE_SCELL_REQ, payload: ftm_lte_ns_disable_scell_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502020f, "FTM_LTE_NS_DISABLE_SCELL_REQ", NULL ),

  /* FTM_LTE_NS_GET_ALL_CARR_DL_BLER_REQ, payload: ftm_lte_ns_get_all_carr_dl_bler_req_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020210, "FTM_LTE_NS_GET_ALL_CARR_DL_BLER_REQ", NULL ),

  /* FTM_LTE_NS_DL_DATA_IND, payload: ftm_lte_ns_dl_data_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020405, "FTM_LTE_NS_DL_DATA_IND", NULL ),

  /* FTM_LTE_NS_START_LTE_MODE_CNF, payload: ftm_lte_ns_start_mode_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020800, "FTM_LTE_NS_START_LTE_MODE_CNF", NULL ),

  /* FTM_LTE_NS_STOP_LTE_MODE_CNF, payload: ftm_lte_ns_stop_mode_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020801, "FTM_LTE_NS_STOP_LTE_MODE_CNF", NULL ),

  /* FTM_LTE_NS_IDLE_CNF, payload: ftm_lte_ns_idle_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020802, "FTM_LTE_NS_IDLE_CNF", NULL ),

  /* FTM_LTE_NS_ACQ_CNF, payload: ftm_lte_ns_acq_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020803, "FTM_LTE_NS_ACQ_CNF", NULL ),

  /* FTM_LTE_NS_START_DP_CNF, payload: ftm_lte_ns_start_dp_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020804, "FTM_LTE_NS_START_DP_CNF", NULL ),

  /* FTM_LTE_NS_IS_CONN_CNF, payload: ftm_lte_ns_is_conn_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020806, "FTM_LTE_NS_IS_CONN_CNF", NULL ),

  /* FTM_LTE_NS_CONFIG_UL_WAVEFORM_CNF, payload: ftm_lte_ns_config_ul_waveform_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020807, "FTM_LTE_NS_CONFIG_UL_WAVEFORM_CNF", NULL ),

  /* FTM_LTE_NS_CONFIG_UL_POWER_CNF, payload: ftm_lte_ns_config_ul_power_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020808, "FTM_LTE_NS_CONFIG_UL_POWER_CNF", NULL ),

  /* FTM_LTE_NS_GET_DL_LEVEL_CNF, payload: ftm_lte_ns_get_dl_level_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020809, "FTM_LTE_NS_GET_DL_LEVEL_CNF", NULL ),

  /* FTM_LTE_NS_RESET_DL_BLER_REPORT_CNF, payload: ftm_lte_ns_reset_dl_bler_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080a, "FTM_LTE_NS_RESET_DL_BLER_REPORT_CNF", NULL ),

  /* FTM_LTE_NS_GET_DL_BLER_REPORT_CNF, payload: ftm_lte_ns_get_dl_bler_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080b, "FTM_LTE_NS_GET_DL_BLER_REPORT_CNF", NULL ),

  /* FTM_LTE_NS_HANDOVER_CNF, payload: ftm_lte_ns_handover_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080c, "FTM_LTE_NS_HANDOVER_CNF", NULL ),

  /* FTM_LTE_NS_TDD_CONFIG_CNF, payload: ftm_lte_ns_tdd_config_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080d, "FTM_LTE_NS_TDD_CONFIG_CNF", NULL ),

  /* FTM_LTE_NS_ENABLE_SCELL_CNF, payload: ftm_lte_ns_enable_scell_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080e, "FTM_LTE_NS_ENABLE_SCELL_CNF", NULL ),

  /* FTM_LTE_NS_DISABLE_SCELL_CNF, payload: ftm_lte_ns_disable_scell_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0502080f, "FTM_LTE_NS_DISABLE_SCELL_CNF", NULL ),

  /* FTM_LTE_NS_GET_ALL_CARR_DL_BLER_CNF, payload: ftm_lte_ns_get_all_carr_dl_bler_cnf_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x05020810, "FTM_LTE_NS_GET_ALL_CARR_DL_BLER_CNF", NULL ),

  /* RFA_RF_CONTROL_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06010000, "RFA_RF_CONTROL_LOOPBACK_SPR", NULL ),

  /* RFA_RF_CONTROL_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06010001, "RFA_RF_CONTROL_LOOPBACK_REPLY_SPR", NULL ),

  /* RFA_RF_CONTROL_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x06010002, "RFA_RF_CONTROL_THREAD_READY_SPR", NULL ),

  /* RFA_RF_CONTROL_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x06010003, "RFA_RF_CONTROL_THREAD_KILL_SPR", NULL ),

  /* RFA_RF_CONTROL_INIT_COMPLETE_CMD, payload: uint32 */
  MSGR_UMID_TABLE_ENTRY ( 0x06010100, "RFA_RF_CONTROL_INIT_COMPLETE_CMD", NULL ),

  /* RFA_RF_CONTROL_INIT_COMPLETE_RSP, payload: rf_init_comp_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x06010300, "RFA_RF_CONTROL_INIT_COMPLETE_RSP", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_REQ, payload: rfa_rf_gsm_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030201, "RFA_RF_GSM_ENTER_MODE_REQ", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_REQ, payload: rfa_rf_gsm_rx_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030202, "RFA_RF_GSM_RX_ENTER_MODE_REQ", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_REQ, payload: rfa_rf_gsm_gta_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030203, "RFA_RF_GSM_GTA_ENTER_MODE_REQ", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_REQ, payload: rfa_rf_gsm_gta_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030204, "RFA_RF_GSM_GTA_EXIT_MODE_REQ", NULL ),

  /* RFA_RF_GSM_WAKEUP_REQ, payload: rfa_rf_gsm_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030205, "RFA_RF_GSM_WAKEUP_REQ", NULL ),

  /* RFA_RF_GSM_SLEEP_REQ, payload: rfa_rf_gsm_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030206, "RFA_RF_GSM_SLEEP_REQ", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_REQ, payload: rfa_rf_gsm_tx_enable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030207, "RFA_RF_GSM_TX_ENABLE_REQ", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_REQ, payload: rfa_rf_gsm_tx_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030208, "RFA_RF_GSM_TX_DISABLE_REQ", NULL ),

  /* RFA_RF_GSM_RX_BURST_REQ, payload: rfa_rf_gsm_rx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030209, "RFA_RF_GSM_RX_BURST_REQ", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_REQ, payload: rfa_rf_gsm_tx_band_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020a, "RFA_RF_GSM_SET_TX_BAND_REQ", NULL ),

  /* RFA_RF_GSM_TX_BURST_REQ, payload: rfa_rf_gsm_tx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020b, "RFA_RF_GSM_TX_BURST_REQ", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_REQ, payload: rfa_rf_gsm_get_timing_infor_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020c, "RFA_RF_GSM_GET_TIMING_INFOR_REQ", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_REQ, payload: rfa_rf_gsm_idle_frame_processing_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_REQ", NULL ),

  /* RFA_RF_GSM_VREG_ON_REQ, payload: rfa_rf_gsm_vreg_on_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020e, "RFA_RF_GSM_VREG_ON_REQ", NULL ),

  /* RFA_RF_GSM_VREG_OFF_REQ, payload: rfa_rf_gsm_vreg_off_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603020f, "RFA_RF_GSM_VREG_OFF_REQ", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_REQ, payload: rfa_rf_gsm_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030210, "RFA_RF_GSM_EXIT_MODE_REQ", NULL ),

  /* RFA_RF_GSM_CM_ENTER_REQ, payload: rfa_rf_gsm_cm_enter_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030212, "RFA_RF_GSM_CM_ENTER_REQ", NULL ),

  /* RFA_RF_GSM_CM_EXIT_REQ, payload: rfa_rf_gsm_cm_exit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030213, "RFA_RF_GSM_CM_EXIT_REQ", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_REQ, payload: rfa_rf_gsm_update_temp_comp_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030214, "RFA_RF_GSM_UPDATE_TEMP_COMP_REQ", NULL ),

  /* RFA_RF_GSM_SET_ANTENNA_REQ, payload: rfa_rf_gsm_set_antenna_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030215, "RFA_RF_GSM_SET_ANTENNA_REQ", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_REQ, payload: rfa_rf_gsm_hal_vote_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030216, "RFA_RF_GSM_HAL_VOTE_REQ", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_REQ, payload: rfa_rf_gsm_start_ip2_cal_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030217, "RFA_RF_GSM_START_IP2_CAL_REQ", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_REQ, payload: rfa_rf_gsm_ip2_cal_msm_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030218, "RFA_RF_GSM_INIT_MSM_IP2_CAL_REQ", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_REQ, payload: rfa_rf_gsm_process_burst_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030219, "RFA_RF_GSM_PROCESS_BURST_METRICS_REQ", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_SUB2_REQ, payload: rfa_rf_gsm_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030221, "RFA_RF_GSM_ENTER_MODE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_SUB2_REQ, payload: rfa_rf_gsm_rx_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030222, "RFA_RF_GSM_RX_ENTER_MODE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_SUB2_REQ, payload: rfa_rf_gsm_gta_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030223, "RFA_RF_GSM_GTA_ENTER_MODE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_SUB2_REQ, payload: rfa_rf_gsm_gta_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030224, "RFA_RF_GSM_GTA_EXIT_MODE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_WAKEUP_SUB2_REQ, payload: rfa_rf_gsm_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030225, "RFA_RF_GSM_WAKEUP_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_SLEEP_SUB2_REQ, payload: rfa_rf_gsm_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030226, "RFA_RF_GSM_SLEEP_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_SUB2_REQ, payload: rfa_rf_gsm_tx_enable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030227, "RFA_RF_GSM_TX_ENABLE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_SUB2_REQ, payload: rfa_rf_gsm_tx_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030228, "RFA_RF_GSM_TX_DISABLE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_RX_BURST_SUB2_REQ, payload: rfa_rf_gsm_rx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030229, "RFA_RF_GSM_RX_BURST_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_SUB2_REQ, payload: rfa_rf_gsm_tx_band_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022a, "RFA_RF_GSM_SET_TX_BAND_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_TX_BURST_SUB2_REQ, payload: rfa_rf_gsm_tx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022b, "RFA_RF_GSM_TX_BURST_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_SUB2_REQ, payload: rfa_rf_gsm_get_timing_infor_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022c, "RFA_RF_GSM_GET_TIMING_INFOR_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB2_REQ, payload: rfa_rf_gsm_idle_frame_processing_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_VREG_ON_SUB2_REQ, payload: rfa_rf_gsm_vreg_on_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022e, "RFA_RF_GSM_VREG_ON_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_VREG_OFF_SUB2_REQ, payload: rfa_rf_gsm_vreg_off_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603022f, "RFA_RF_GSM_VREG_OFF_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_SUB2_REQ, payload: rfa_rf_gsm_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030230, "RFA_RF_GSM_EXIT_MODE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_CM_ENTER_SUB2_REQ, payload: rfa_rf_gsm_cm_enter_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030232, "RFA_RF_GSM_CM_ENTER_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_CM_EXIT_SUB2_REQ, payload: rfa_rf_gsm_cm_exit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030233, "RFA_RF_GSM_CM_EXIT_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_SUB2_REQ, payload: rfa_rf_gsm_update_temp_comp_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030234, "RFA_RF_GSM_UPDATE_TEMP_COMP_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_SET_ANTENNA_SUB2_REQ, payload: rfa_rf_gsm_set_antenna_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030235, "RFA_RF_GSM_SET_ANTENNA_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_SUB2_REQ, payload: rfa_rf_gsm_hal_vote_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030236, "RFA_RF_GSM_HAL_VOTE_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_SUB2_REQ, payload: rfa_rf_gsm_start_ip2_cal_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030237, "RFA_RF_GSM_START_IP2_CAL_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB2_REQ, payload: rfa_rf_gsm_ip2_cal_msm_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030238, "RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_SUB2_REQ, payload: rfa_rf_gsm_process_burst_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030239, "RFA_RF_GSM_PROCESS_BURST_METRICS_SUB2_REQ", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_SUB3_REQ, payload: rfa_rf_gsm_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030241, "RFA_RF_GSM_ENTER_MODE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_SUB3_REQ, payload: rfa_rf_gsm_rx_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030242, "RFA_RF_GSM_RX_ENTER_MODE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_SUB3_REQ, payload: rfa_rf_gsm_gta_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030243, "RFA_RF_GSM_GTA_ENTER_MODE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_SUB3_REQ, payload: rfa_rf_gsm_gta_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030244, "RFA_RF_GSM_GTA_EXIT_MODE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_WAKEUP_SUB3_REQ, payload: rfa_rf_gsm_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030245, "RFA_RF_GSM_WAKEUP_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_SLEEP_SUB3_REQ, payload: rfa_rf_gsm_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030246, "RFA_RF_GSM_SLEEP_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_SUB3_REQ, payload: rfa_rf_gsm_tx_enable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030247, "RFA_RF_GSM_TX_ENABLE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_SUB3_REQ, payload: rfa_rf_gsm_tx_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030248, "RFA_RF_GSM_TX_DISABLE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_RX_BURST_SUB3_REQ, payload: rfa_rf_gsm_rx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030249, "RFA_RF_GSM_RX_BURST_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_SUB3_REQ, payload: rfa_rf_gsm_tx_band_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024a, "RFA_RF_GSM_SET_TX_BAND_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_TX_BURST_SUB3_REQ, payload: rfa_rf_gsm_tx_burst_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024b, "RFA_RF_GSM_TX_BURST_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_SUB3_REQ, payload: rfa_rf_gsm_get_timing_infor_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024c, "RFA_RF_GSM_GET_TIMING_INFOR_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB3_REQ, payload: rfa_rf_gsm_idle_frame_processing_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_VREG_ON_SUB3_REQ, payload: rfa_rf_gsm_vreg_on_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024e, "RFA_RF_GSM_VREG_ON_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_VREG_OFF_SUB3_REQ, payload: rfa_rf_gsm_vreg_off_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603024f, "RFA_RF_GSM_VREG_OFF_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_SUB3_REQ, payload: rfa_rf_gsm_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030250, "RFA_RF_GSM_EXIT_MODE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_CM_ENTER_SUB3_REQ, payload: rfa_rf_gsm_cm_enter_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030252, "RFA_RF_GSM_CM_ENTER_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_CM_EXIT_SUB3_REQ, payload: rfa_rf_gsm_cm_exit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030253, "RFA_RF_GSM_CM_EXIT_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_SUB3_REQ, payload: rfa_rf_gsm_update_temp_comp_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030254, "RFA_RF_GSM_UPDATE_TEMP_COMP_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_SUB3_REQ, payload: rfa_rf_gsm_hal_vote_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030255, "RFA_RF_GSM_HAL_VOTE_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_SUB3_REQ, payload: rfa_rf_gsm_start_ip2_cal_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030256, "RFA_RF_GSM_START_IP2_CAL_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB3_REQ, payload: rfa_rf_gsm_ip2_cal_msm_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030257, "RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_SUB3_REQ, payload: rfa_rf_gsm_process_burst_metrics_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030258, "RFA_RF_GSM_PROCESS_BURST_METRICS_SUB3_REQ", NULL ),

  /* RFA_RF_GSM_SET_SAR_LIMIT_IND, payload: rfa_rf_gsm_set_sar_limit_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030411, "RFA_RF_GSM_SET_SAR_LIMIT_IND", NULL ),

  /* RFA_RF_GSM_SET_SAR_LIMIT_SUB2_IND, payload: rfa_rf_gsm_set_sar_limit_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030431, "RFA_RF_GSM_SET_SAR_LIMIT_SUB2_IND", NULL ),

  /* RFA_RF_GSM_SET_SAR_LIMIT_SUB3_IND, payload: rfa_rf_gsm_set_sar_limit_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030451, "RFA_RF_GSM_SET_SAR_LIMIT_SUB3_IND", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_CNF, payload: rfa_rf_gsm_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030801, "RFA_RF_GSM_ENTER_MODE_CNF", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_CNF, payload: rfa_rf_gsm_rx_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030802, "RFA_RF_GSM_RX_ENTER_MODE_CNF", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_CNF, payload: rfa_rf_gsm_gta_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030803, "RFA_RF_GSM_GTA_ENTER_MODE_CNF", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_CNF, payload: rfa_rf_gsm_gta_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030804, "RFA_RF_GSM_GTA_EXIT_MODE_CNF", NULL ),

  /* RFA_RF_GSM_WAKEUP_CNF, payload: rfa_rf_gsm_wakeup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030805, "RFA_RF_GSM_WAKEUP_CNF", NULL ),

  /* RFA_RF_GSM_SLEEP_CNF, payload: rfa_rf_gsm_sleep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030806, "RFA_RF_GSM_SLEEP_CNF", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_CNF, payload: rfa_rf_gsm_tx_enable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030807, "RFA_RF_GSM_TX_ENABLE_CNF", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_CNF, payload: rfa_rf_gsm_tx_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030808, "RFA_RF_GSM_TX_DISABLE_CNF", NULL ),

  /* RFA_RF_GSM_RX_BURST_CNF, payload: rfa_rf_gsm_rx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030809, "RFA_RF_GSM_RX_BURST_CNF", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_CNF, payload: rfa_rf_gsm_tx_band_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080a, "RFA_RF_GSM_SET_TX_BAND_CNF", NULL ),

  /* RFA_RF_GSM_TX_BURST_CNF, payload: rfa_rf_gsm_tx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080b, "RFA_RF_GSM_TX_BURST_CNF", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_CNF, payload: rfa_rf_gsm_get_timing_infor_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080c, "RFA_RF_GSM_GET_TIMING_INFOR_CNF", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_CNF, payload: rfa_rf_gsm_idle_frame_processing_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_CNF", NULL ),

  /* RFA_RF_GSM_VREG_ON_CNF, payload: rfa_rf_gsm_vreg_on_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080e, "RFA_RF_GSM_VREG_ON_CNF", NULL ),

  /* RFA_RF_GSM_VREG_OFF_CNF, payload: rfa_rf_gsm_vreg_off_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603080f, "RFA_RF_GSM_VREG_OFF_CNF", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_CNF, payload: rfa_rf_gsm_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030810, "RFA_RF_GSM_EXIT_MODE_CNF", NULL ),

  /* RFA_RF_GSM_CM_ENTER_CNF, payload: rfa_rf_gsm_cm_enter_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030812, "RFA_RF_GSM_CM_ENTER_CNF", NULL ),

  /* RFA_RF_GSM_CM_EXIT_CNF, payload: rfa_rf_gsm_cm_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030813, "RFA_RF_GSM_CM_EXIT_CNF", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_CNF, payload: rfa_rf_gsm_temp_comp_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030814, "RFA_RF_GSM_UPDATE_TEMP_COMP_CNF", NULL ),

  /* RFA_RF_GSM_SET_ANTENNA_CNF, payload: rfa_rf_gsm_set_antenna_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030815, "RFA_RF_GSM_SET_ANTENNA_CNF", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_CNF, payload: rfa_rf_gsm_hal_vote_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030816, "RFA_RF_GSM_HAL_VOTE_CNF", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_CNF, payload: rfa_rf_gsm_start_ip2_cal_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030817, "RFA_RF_GSM_START_IP2_CAL_CNF", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_CNF, payload: rfa_rf_gsm_ip2_cal_msm_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030818, "RFA_RF_GSM_INIT_MSM_IP2_CAL_CNF", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_CNF, payload: rfa_rf_gsm_process_burst_metrics_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030819, "RFA_RF_GSM_PROCESS_BURST_METRICS_CNF", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_SUB2_CNF, payload: rfa_rf_gsm_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030821, "RFA_RF_GSM_ENTER_MODE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_SUB2_CNF, payload: rfa_rf_gsm_rx_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030822, "RFA_RF_GSM_RX_ENTER_MODE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_SUB2_CNF, payload: rfa_rf_gsm_gta_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030823, "RFA_RF_GSM_GTA_ENTER_MODE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_SUB2_CNF, payload: rfa_rf_gsm_gta_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030824, "RFA_RF_GSM_GTA_EXIT_MODE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_WAKEUP_SUB2_CNF, payload: rfa_rf_gsm_wakeup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030825, "RFA_RF_GSM_WAKEUP_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_SLEEP_SUB2_CNF, payload: rfa_rf_gsm_sleep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030826, "RFA_RF_GSM_SLEEP_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_SUB2_CNF, payload: rfa_rf_gsm_tx_enable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030827, "RFA_RF_GSM_TX_ENABLE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_SUB2_CNF, payload: rfa_rf_gsm_tx_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030828, "RFA_RF_GSM_TX_DISABLE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_RX_BURST_SUB2_CNF, payload: rfa_rf_gsm_rx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030829, "RFA_RF_GSM_RX_BURST_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_SUB2_CNF, payload: rfa_rf_gsm_tx_band_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082a, "RFA_RF_GSM_SET_TX_BAND_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_TX_BURST_SUB2_CNF, payload: rfa_rf_gsm_tx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082b, "RFA_RF_GSM_TX_BURST_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_SUB2_CNF, payload: rfa_rf_gsm_get_timing_infor_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082c, "RFA_RF_GSM_GET_TIMING_INFOR_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB2_CNF, payload: rfa_rf_gsm_idle_frame_processing_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_VREG_ON_SUB2_CNF, payload: rfa_rf_gsm_vreg_on_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082e, "RFA_RF_GSM_VREG_ON_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_VREG_OFF_SUB2_CNF, payload: rfa_rf_gsm_vreg_off_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603082f, "RFA_RF_GSM_VREG_OFF_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_SUB2_CNF, payload: rfa_rf_gsm_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030830, "RFA_RF_GSM_EXIT_MODE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_CM_ENTER_SUB2_CNF, payload: rfa_rf_gsm_cm_enter_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030832, "RFA_RF_GSM_CM_ENTER_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_CM_EXIT_SUB2_CNF, payload: rfa_rf_gsm_cm_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030833, "RFA_RF_GSM_CM_EXIT_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_SUB2_CNF, payload: rfa_rf_gsm_temp_comp_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030834, "RFA_RF_GSM_UPDATE_TEMP_COMP_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_SET_ANTENNA_SUB2_CNF, payload: rfa_rf_gsm_set_antenna_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030835, "RFA_RF_GSM_SET_ANTENNA_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_SUB2_CNF, payload: rfa_rf_gsm_hal_vote_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030836, "RFA_RF_GSM_HAL_VOTE_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_SUB2_CNF, payload: rfa_rf_gsm_start_ip2_cal_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030837, "RFA_RF_GSM_START_IP2_CAL_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB2_CNF, payload: rfa_rf_gsm_ip2_cal_msm_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030838, "RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_SUB2_CNF, payload: rfa_rf_gsm_process_burst_metrics_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030839, "RFA_RF_GSM_PROCESS_BURST_METRICS_SUB2_CNF", NULL ),

  /* RFA_RF_GSM_ENTER_MODE_SUB3_CNF, payload: rfa_rf_gsm_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030841, "RFA_RF_GSM_ENTER_MODE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_RX_ENTER_MODE_SUB3_CNF, payload: rfa_rf_gsm_rx_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030842, "RFA_RF_GSM_RX_ENTER_MODE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_GTA_ENTER_MODE_SUB3_CNF, payload: rfa_rf_gsm_gta_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030843, "RFA_RF_GSM_GTA_ENTER_MODE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_GTA_EXIT_MODE_SUB3_CNF, payload: rfa_rf_gsm_gta_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030844, "RFA_RF_GSM_GTA_EXIT_MODE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_WAKEUP_SUB3_CNF, payload: rfa_rf_gsm_wakeup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030845, "RFA_RF_GSM_WAKEUP_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_SLEEP_SUB3_CNF, payload: rfa_rf_gsm_sleep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030846, "RFA_RF_GSM_SLEEP_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_TX_ENABLE_SUB3_CNF, payload: rfa_rf_gsm_tx_enable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030847, "RFA_RF_GSM_TX_ENABLE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_TX_DISABLE_SUB3_CNF, payload: rfa_rf_gsm_tx_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030848, "RFA_RF_GSM_TX_DISABLE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_RX_BURST_SUB3_CNF, payload: rfa_rf_gsm_rx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030849, "RFA_RF_GSM_RX_BURST_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_SET_TX_BAND_SUB3_CNF, payload: rfa_rf_gsm_tx_band_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084a, "RFA_RF_GSM_SET_TX_BAND_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_TX_BURST_SUB3_CNF, payload: rfa_rf_gsm_tx_burst_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084b, "RFA_RF_GSM_TX_BURST_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_GET_TIMING_INFOR_SUB3_CNF, payload: rfa_rf_gsm_get_timing_infor_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084c, "RFA_RF_GSM_GET_TIMING_INFOR_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB3_CNF, payload: rfa_rf_gsm_idle_frame_processing_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084d, "RFA_RF_GSM_IDLE_FRAME_PROCESSING_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_VREG_ON_SUB3_CNF, payload: rfa_rf_gsm_vreg_on_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084e, "RFA_RF_GSM_VREG_ON_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_VREG_OFF_SUB3_CNF, payload: rfa_rf_gsm_vreg_off_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0603084f, "RFA_RF_GSM_VREG_OFF_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_EXIT_MODE_SUB3_CNF, payload: rfa_rf_gsm_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030850, "RFA_RF_GSM_EXIT_MODE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_CM_ENTER_SUB3_CNF, payload: rfa_rf_gsm_cm_enter_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030852, "RFA_RF_GSM_CM_ENTER_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_CM_EXIT_SUB3_CNF, payload: rfa_rf_gsm_cm_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030853, "RFA_RF_GSM_CM_EXIT_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_UPDATE_TEMP_COMP_SUB3_CNF, payload: rfa_rf_gsm_temp_comp_update_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030854, "RFA_RF_GSM_UPDATE_TEMP_COMP_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_HAL_VOTE_SUB3_CNF, payload: rfa_rf_gsm_hal_vote_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030855, "RFA_RF_GSM_HAL_VOTE_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_START_IP2_CAL_SUB3_CNF, payload: rfa_rf_gsm_start_ip2_cal_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030856, "RFA_RF_GSM_START_IP2_CAL_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB3_CNF, payload: rfa_rf_gsm_ip2_cal_msm_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030857, "RFA_RF_GSM_INIT_MSM_IP2_CAL_SUB3_CNF", NULL ),

  /* RFA_RF_GSM_PROCESS_BURST_METRICS_SUB3_CNF, payload: rfa_rf_gsm_process_burst_metrics_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06030858, "RFA_RF_GSM_PROCESS_BURST_METRICS_SUB3_CNF", NULL ),

  /* RFA_RF_1X_TXRX_AGC_RELOAD_CMD, payload: rfa_1x_txrx_agc_reload_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06040101, "RFA_RF_1X_TXRX_AGC_RELOAD_CMD", NULL ),

  /* RFA_RF_1X_SET_COEX_TX_PWR_LIMIT_REQ, payload: rfa_1x_set_coex_tx_pwr_limit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06040202, "RFA_RF_1X_SET_COEX_TX_PWR_LIMIT_REQ", NULL ),

  /* RFA_RF_HDR_SET_COEX_TX_PWR_LIMIT_REQ, payload: rfa_hdr_set_coex_tx_pwr_limit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06050201, "RFA_RF_HDR_SET_COEX_TX_PWR_LIMIT_REQ", NULL ),

  /* RFA_RF_GSM_FTM_SET_MODE_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060101, "RFA_RF_GSM_FTM_SET_MODE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_ON_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060102, "RFA_RF_GSM_FTM_SET_TX_ON_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_OFF_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060103, "RFA_RF_GSM_FTM_SET_TX_OFF_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_LNA_RANGE_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060104, "RFA_RF_GSM_FTM_SET_LNA_RANGE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_PDM_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060105, "RFA_RF_GSM_FTM_SET_PDM_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_BAND_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060106, "RFA_RF_GSM_FTM_SET_BAND_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TRANSMIT_CONT_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060107, "RFA_RF_GSM_FTM_SET_TRANSMIT_CONT_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TRANSMIT_BURST_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060108, "RFA_RF_GSM_FTM_SET_TRANSMIT_BURST_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_BURST_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060109, "RFA_RF_GSM_FTM_SET_RX_BURST_CMD", NULL ),

  /* RFA_RF_GSM_FTM_GET_RSSI_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010a, "RFA_RF_GSM_FTM_GET_RSSI_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_START_DELTA_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010b, "RFA_RF_GSM_FTM_SET_PA_START_DELTA_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_STOP_DELTA_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010c, "RFA_RF_GSM_FTM_SET_PA_STOP_DELTA_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_DAC_INPUT_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010d, "RFA_RF_GSM_FTM_SET_PA_DAC_INPUT_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_CONTINUOUS_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010e, "RFA_RF_GSM_FTM_SET_RX_CONTINUOUS_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_PATH_DELAY_CMD, payload: rfa_rf_gsm_ftm_set_path_delay_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606010f, "RFA_RF_GSM_FTM_SET_PATH_DELAY_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_SLOT_OVERRIDE_FLAG_CMD, payload: rfa_rf_gsm_ftm_set_slot_override_flag_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060110, "RFA_RF_GSM_FTM_SET_SLOT_OVERRIDE_FLAG_CMD", NULL ),

  /* RFA_RF_GSM_FTM_RX_GAIN_RANGE_CAL_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060111, "RFA_RF_GSM_FTM_RX_GAIN_RANGE_CAL_CMD", NULL ),

  /* RFA_RF_GSM_FTM_TX_KV_CAL_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060112, "RFA_RF_GSM_FTM_TX_KV_CAL_CMD", NULL ),

  /* RFA_RF_GSM_FTM_TX_KV_CAL_V2_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060113, "RFA_RF_GSM_FTM_TX_KV_CAL_V2_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_LINEAR_PA_RANGE_CMD, payload: rfa_rf_gsm_ftm_set_pa_range_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060114, "RFA_RF_GSM_FTM_SET_LINEAR_PA_RANGE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_LINEAR_RGI_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060115, "RFA_RF_GSM_FTM_SET_LINEAR_RGI_CMD", NULL ),

  /* RFA_RF_GSM_FTM_CAPTURE_IQ_CMD, payload: rfa_rf_gsm_ftm_capture_iq_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060116, "RFA_RF_GSM_FTM_CAPTURE_IQ_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_TIMING_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060117, "RFA_RF_GSM_FTM_SET_RX_TIMING_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_TIMING_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060118, "RFA_RF_GSM_FTM_SET_TX_TIMING_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_GAIN_SWEEP_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060119, "RFA_RF_GSM_FTM_SET_TX_GAIN_SWEEP_CMD", NULL ),

  /* RFA_RF_GSM_FTM_TX_ENVDC_CS_SWEEP_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011a, "RFA_RF_GSM_FTM_TX_ENVDC_CS_SWEEP_CMD", NULL ),

  /* RFA_RF_GSM_FTM_CFG2_AMAM_SWEEP_V2_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011b, "RFA_RF_GSM_FTM_CFG2_AMAM_SWEEP_V2_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_MULTISLOT_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011c, "RFA_RF_GSM_FTM_SET_RX_MULTISLOT_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_BURST_FOR_EXPECTED_PWR_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011d, "RFA_RF_GSM_FTM_SET_RX_BURST_FOR_EXPECTED_PWR_CMD", NULL ),

  /* RFA_RF_GSM_FTM_GET_MEAS_PWR_CMD, payload: rfa_rf_gsm_ftm_set_mode_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011e, "RFA_RF_GSM_FTM_GET_MEAS_PWR_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_POW_DBM_CMD, payload: rfa_rf_gsm_ftm_set_tx_pow_dbm_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606011f, "RFA_RF_GSM_FTM_SET_TX_POW_DBM_CMD", NULL ),

  /* RFA_RF_GSM_FTM_TUNE_TX_CMD, payload: rfa_rf_gsm_ftm_tune_tx_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060120, "RFA_RF_GSM_FTM_TUNE_TX_CMD", NULL ),

  /* RFA_RF_GSM_FTM_TUNER_TUNE_CODE_OVERRIDE_CMD, payload: rfa_rf_gsm_ftm_tuner_override_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060121, "RFA_RF_GSM_FTM_TUNER_TUNE_CODE_OVERRIDE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_IP2_CAL_PARAMS_CMD, payload: rfa_rf_gsm_ftm_ip2_cal_type_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060122, "RFA_RF_GSM_FTM_SET_IP2_CAL_PARAMS_CMD", NULL ),

  /* RFA_RF_GSM_FTM_GET_IP2_CAL_PARAMS_CMD, payload: rfa_rf_gsm_ftm_get_ip2_cal_info_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060123, "RFA_RF_GSM_FTM_GET_IP2_CAL_PARAMS_CMD", NULL ),

  /* RFA_RF_GSM_FTM_GET_IP2_CAL_RESULTS_CMD, payload: rfa_rf_gsm_ftm_get_ip2_cal_results_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060124, "RFA_RF_GSM_FTM_GET_IP2_CAL_RESULTS_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_SAWLESS_LIN_MODE_CMD, payload: rfa_rf_gsm_ftm_set_sawless_lin_mode_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060125, "RFA_RF_GSM_FTM_SET_SAWLESS_LIN_MODE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_GET_SAWLESS_LIN_MODE_CMD, payload: rfa_rf_gsm_ftm_get_sawless_lin_mode_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060126, "RFA_RF_GSM_FTM_GET_SAWLESS_LIN_MODE_CMD", NULL ),

  /* RFA_RF_GSM_FTM_SET_MODE_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060301, "RFA_RF_GSM_FTM_SET_MODE_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_ON_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060302, "RFA_RF_GSM_FTM_SET_TX_ON_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_OFF_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060303, "RFA_RF_GSM_FTM_SET_TX_OFF_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_LNA_RANGE_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060304, "RFA_RF_GSM_FTM_SET_LNA_RANGE_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_PDM_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060305, "RFA_RF_GSM_FTM_SET_PDM_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_BAND_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060306, "RFA_RF_GSM_FTM_SET_BAND_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TRANSMIT_CONT_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060307, "RFA_RF_GSM_FTM_SET_TRANSMIT_CONT_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TRANSMIT_BURST_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060308, "RFA_RF_GSM_FTM_SET_TRANSMIT_BURST_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_BURST_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060309, "RFA_RF_GSM_FTM_SET_RX_BURST_RSP", NULL ),

  /* RFA_RF_GSM_FTM_GET_RSSI_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030a, "RFA_RF_GSM_FTM_GET_RSSI_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_START_DELTA_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030b, "RFA_RF_GSM_FTM_SET_PA_START_DELTA_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_STOP_DELTA_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030c, "RFA_RF_GSM_FTM_SET_PA_STOP_DELTA_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_PA_DAC_INPUT_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030d, "RFA_RF_GSM_FTM_SET_PA_DAC_INPUT_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_CONTINUOUS_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030e, "RFA_RF_GSM_FTM_SET_RX_CONTINUOUS_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_PATH_DELAY_RSP, payload: rfa_rf_gsm_ftm_set_path_delay_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606030f, "RFA_RF_GSM_FTM_SET_PATH_DELAY_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_SLOT_OVERRIDE_FLAG_RSP, payload: rfa_rf_gsm_ftm_set_slot_override_flag_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060310, "RFA_RF_GSM_FTM_SET_SLOT_OVERRIDE_FLAG_RSP", NULL ),

  /* RFA_RF_GSM_FTM_RX_GAIN_RANGE_CAL_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060311, "RFA_RF_GSM_FTM_RX_GAIN_RANGE_CAL_RSP", NULL ),

  /* RFA_RF_GSM_FTM_TX_KV_CAL_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060312, "RFA_RF_GSM_FTM_TX_KV_CAL_RSP", NULL ),

  /* RFA_RF_GSM_FTM_TX_KV_CAL_V2_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060313, "RFA_RF_GSM_FTM_TX_KV_CAL_V2_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_LINEAR_PA_RANGE_RSP, payload: rfa_rf_gsm_ftm_set_pa_range_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060314, "RFA_RF_GSM_FTM_SET_LINEAR_PA_RANGE_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_LINEAR_RGI_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060315, "RFA_RF_GSM_FTM_SET_LINEAR_RGI_RSP", NULL ),

  /* RFA_RF_GSM_FTM_CAPTURE_IQ_RSP, payload: rfa_rf_gsm_ftm_capture_iq_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060316, "RFA_RF_GSM_FTM_CAPTURE_IQ_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_TIMING_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060317, "RFA_RF_GSM_FTM_SET_RX_TIMING_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_TIMING_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060318, "RFA_RF_GSM_FTM_SET_TX_TIMING_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_GAIN_SWEEP_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060319, "RFA_RF_GSM_FTM_SET_TX_GAIN_SWEEP_RSP", NULL ),

  /* RFA_RF_GSM_FTM_TX_ENVDC_CS_SWEEP_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031a, "RFA_RF_GSM_FTM_TX_ENVDC_CS_SWEEP_RSP", NULL ),

  /* RFA_RF_GSM_FTM_CFG2_AMAM_SWEEP_V2_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031b, "RFA_RF_GSM_FTM_CFG2_AMAM_SWEEP_V2_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_MULTISLOT_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031c, "RFA_RF_GSM_FTM_SET_RX_MULTISLOT_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_RX_BURST_FOR_EXPECTED_PWR_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031d, "RFA_RF_GSM_FTM_SET_RX_BURST_FOR_EXPECTED_PWR_RSP", NULL ),

  /* RFA_RF_GSM_FTM_GET_MEAS_PWR_RSP, payload: rfa_rf_gsm_ftm_set_mode_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031e, "RFA_RF_GSM_FTM_GET_MEAS_PWR_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_TX_POW_DBM_RSP, payload: rfa_rf_gsm_ftm_set_tx_pow_dbm_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0606031f, "RFA_RF_GSM_FTM_SET_TX_POW_DBM_RSP", NULL ),

  /* RFA_RF_GSM_FTM_TUNE_TX_RSP, payload: rfa_rf_gsm_ftm_tune_tx_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060320, "RFA_RF_GSM_FTM_TUNE_TX_RSP", NULL ),

  /* RFA_RF_GSM_FTM_TUNER_TUNE_CODE_OVERRIDE_RSP, payload: rfa_rf_gsm_ftm_tuner_override_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060321, "RFA_RF_GSM_FTM_TUNER_TUNE_CODE_OVERRIDE_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_IP2_CAL_PARAMS_RSP, payload: rfa_rf_gsm_ftm_ip2_cal_type_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060322, "RFA_RF_GSM_FTM_SET_IP2_CAL_PARAMS_RSP", NULL ),

  /* RFA_RF_GSM_FTM_GET_IP2_CAL_PARAMS_RSP, payload: rfa_rf_gsm_ftm_get_ip2_cal_info_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060323, "RFA_RF_GSM_FTM_GET_IP2_CAL_PARAMS_RSP", NULL ),

  /* RFA_RF_GSM_FTM_GET_IP2_CAL_RESULTS_RSP, payload: rfa_rf_gsm_ftm_get_ip2_cal_results_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060324, "RFA_RF_GSM_FTM_GET_IP2_CAL_RESULTS_RSP", NULL ),

  /* RFA_RF_GSM_FTM_SET_SAWLESS_LIN_MODE_RSP, payload: rfa_rf_gsm_ftm_set_sawless_lin_mode_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060325, "RFA_RF_GSM_FTM_SET_SAWLESS_LIN_MODE_RSP", NULL ),

  /* RFA_RF_GSM_FTM_GET_SAWLESS_LIN_MODE_RSP, payload: rfa_rf_gsm_ftm_get_sawless_lin_mode_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06060326, "RFA_RF_GSM_FTM_GET_SAWLESS_LIN_MODE_RSP", NULL ),

  /* RFA_RF_LTE_ENTER_MODE_REQ, payload: rfa_rf_lte_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a1, "RFA_RF_LTE_ENTER_MODE_REQ", NULL ),

  /* RFA_RF_LTE_EXIT_MODE_REQ, payload: rfa_rf_lte_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a2, "RFA_RF_LTE_EXIT_MODE_REQ", NULL ),

  /* RFA_RF_LTE_FDD_RX_CONFIG_REQ, payload: rfa_rf_lte_fdd_rx_config_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a3, "RFA_RF_LTE_FDD_RX_CONFIG_REQ", NULL ),

  /* RFA_RF_LTE_FDD_TX_CONFIG_REQ, payload: rfa_rf_lte_fdd_tx_config_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a4, "RFA_RF_LTE_FDD_TX_CONFIG_REQ", NULL ),

  /* RFA_RF_LTE_TDD_RX_CONFIG_REQ, payload: rfa_rf_lte_tdd_rx_config_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a5, "RFA_RF_LTE_TDD_RX_CONFIG_REQ", NULL ),

  /* RFA_RF_LTE_TDD_TX_CONFIG_REQ, payload: rfa_rf_lte_tdd_tx_config_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a6, "RFA_RF_LTE_TDD_TX_CONFIG_REQ", NULL ),

  /* RFA_RF_LTE_FDD_TX_DISABLE_REQ, payload: rfa_rf_lte_fdd_tx_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a7, "RFA_RF_LTE_FDD_TX_DISABLE_REQ", NULL ),

  /* RFA_RF_LTE_TDD_TX_DISABLE_REQ, payload: rfa_rf_lte_tdd_tx_disable_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a8, "RFA_RF_LTE_TDD_TX_DISABLE_REQ", NULL ),

  /* RFA_RF_LTE_SLEEP_REQ, payload: rfa_rf_lte_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702a9, "RFA_RF_LTE_SLEEP_REQ", NULL ),

  /* RFA_RF_LTE_WAKEUP_REQ, payload: rfa_rf_lte_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702aa, "RFA_RF_LTE_WAKEUP_REQ", NULL ),

  /* RFA_RF_LTE_RF_IS_SLEEPING_REQ, payload: rfa_rf_lte_rf_is_sleeping_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702ab, "RFA_RF_LTE_RF_IS_SLEEPING_REQ", NULL ),

  /* RFA_RF_LTE_L2L_BUILD_SCRIPTS_REQ, payload: rfa_rf_lte_l2l_build_scripts_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702ae, "RFA_RF_LTE_L2L_BUILD_SCRIPTS_REQ", NULL ),

  /* RFA_RF_LTE_PLL_ADJUST_REQ, payload: rfa_rf_lte_pll_adjust_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702af, "RFA_RF_LTE_PLL_ADJUST_REQ", NULL ),

  /* RFA_RF_LTE_GET_UE_MIN_POWER_REQ, payload: rfa_rf_lte_get_ue_min_power_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702b3, "RFA_RF_LTE_GET_UE_MIN_POWER_REQ", NULL ),

  /* RFA_RF_LTE_ANT_SW_REQ, payload: rfa_rf_lte_ant_sw_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702b5, "RFA_RF_LTE_ANT_SW_REQ", NULL ),

  /* RFA_RF_LTE_RX1_CHAIN_CONFIG_REQ, payload: rfa_rf_lte_rx1_chain_config_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060702bf, "RFA_RF_LTE_RX1_CHAIN_CONFIG_REQ", NULL ),

  /* RFA_RF_LTE_TX_ON_IND, payload: rfa_rf_lte_tx_on_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704ad, "RFA_RF_LTE_TX_ON_IND", NULL ),

  /* RFA_RF_LTE_CONNECT_MODE_IND, payload: rfa_rf_lte_connect_mode_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b0, "RFA_RF_LTE_CONNECT_MODE_IND", NULL ),

  /* RFA_RF_LTE_TXPL_UPDATE_IND, payload: rfa_rf_lte_txpl_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b1, "RFA_RF_LTE_TXPL_UPDATE_IND", NULL ),

  /* RFA_RF_LTE_SET_TX_PLIMIT_IND, payload: rfa_rf_lte_set_tx_plimit_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b2, "RFA_RF_LTE_SET_TX_PLIMIT_IND", NULL ),

  /* RFA_RF_LTE_ANT_TUNER_TICK_IND, payload: rfa_rf_lte_ant_tuner_tick_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b4, "RFA_RF_LTE_ANT_TUNER_TICK_IND", NULL ),

  /* RFA_RF_LTE_ANT_SW_ABORT_IND, payload: rfa_rf_lte_ant_sw_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b6, "RFA_RF_LTE_ANT_SW_ABORT_IND", NULL ),

  /* RFA_RF_LTE_TDD_ASM_SCRIPT_UPDATE_IND, payload: rfa_rf_lte_dynamic_asm_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b7, "RFA_RF_LTE_TDD_ASM_SCRIPT_UPDATE_IND", NULL ),

  /* RFA_RF_LTE_FED_SET_TIMER_IND, payload: rfa_rf_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704b8, "RFA_RF_LTE_FED_SET_TIMER_IND", NULL ),

  /* RFA_RF_LTE_FED_SCHEDULER_WAKEUP_IND, payload: rfa_rf_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704ba, "RFA_RF_LTE_FED_SCHEDULER_WAKEUP_IND", NULL ),

  /* RFA_RF_LTE_FED_PRACH_WAKEUP_IND, payload: rfa_rf_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704bb, "RFA_RF_LTE_FED_PRACH_WAKEUP_IND", NULL ),

  /* RFA_RF_LTE_P_CMAX_UPDATE_IND, payload: rfa_rf_lte_p_cmax_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704bc, "RFA_RF_LTE_P_CMAX_UPDATE_IND", NULL ),

  /* RFA_RF_LTE_UL_POWER_UPDATE_IND, payload: rfa_rf_lte_ul_power_update_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704bd, "RFA_RF_LTE_UL_POWER_UPDATE_IND", NULL ),

  /* RFA_RF_LTE_PATH_SEL_OVERRIDE_IND, payload: rfa_rf_lte_path_sel_override_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704be, "RFA_RF_LTE_PATH_SEL_OVERRIDE_IND", NULL ),

  /* RFA_RF_LTE_TXPLL_SCRIPTS_UPDATE_IND, payload: rfa_rf_lte_txpll_scripts_update_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060704c0, "RFA_RF_LTE_TXPLL_SCRIPTS_UPDATE_IND", NULL ),

  /* RFA_RF_LTE_ENTER_MODE_CNF, payload: rfa_rf_lte_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a1, "RFA_RF_LTE_ENTER_MODE_CNF", NULL ),

  /* RFA_RF_LTE_EXIT_MODE_CNF, payload: rfa_rf_lte_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a2, "RFA_RF_LTE_EXIT_MODE_CNF", NULL ),

  /* RFA_RF_LTE_FDD_RX_CONFIG_CNF, payload: rfa_rf_lte_fdd_rx_config_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a3, "RFA_RF_LTE_FDD_RX_CONFIG_CNF", NULL ),

  /* RFA_RF_LTE_FDD_TX_CONFIG_CNF, payload: rfa_rf_lte_fdd_tx_config_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a4, "RFA_RF_LTE_FDD_TX_CONFIG_CNF", NULL ),

  /* RFA_RF_LTE_TDD_RX_CONFIG_CNF, payload: rfa_rf_lte_tdd_rx_config_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a5, "RFA_RF_LTE_TDD_RX_CONFIG_CNF", NULL ),

  /* RFA_RF_LTE_TDD_TX_CONFIG_CNF, payload: rfa_rf_lte_tdd_tx_config_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a6, "RFA_RF_LTE_TDD_TX_CONFIG_CNF", NULL ),

  /* RFA_RF_LTE_FDD_TX_DISABLE_CNF, payload: rfa_rf_lte_fdd_tx_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a7, "RFA_RF_LTE_FDD_TX_DISABLE_CNF", NULL ),

  /* RFA_RF_LTE_TDD_TX_DISABLE_CNF, payload: rfa_rf_lte_tdd_tx_disable_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a8, "RFA_RF_LTE_TDD_TX_DISABLE_CNF", NULL ),

  /* RFA_RF_LTE_SLEEP_CNF, payload: rfa_rf_lte_sleep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708a9, "RFA_RF_LTE_SLEEP_CNF", NULL ),

  /* RFA_RF_LTE_WAKEUP_CNF, payload: rfa_rf_lte_wakeup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708aa, "RFA_RF_LTE_WAKEUP_CNF", NULL ),

  /* RFA_RF_LTE_RF_IS_SLEEPING_CNF, payload: rfa_rf_lte_rf_is_sleeping_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708ab, "RFA_RF_LTE_RF_IS_SLEEPING_CNF", NULL ),

  /* RFA_RF_LTE_TX_ON_CNF, payload: rfa_rf_lte_tx_on_cfn_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708ad, "RFA_RF_LTE_TX_ON_CNF", NULL ),

  /* RFA_RF_LTE_L2L_BUILD_SCRIPTS_CNF, payload: rfa_rf_lte_l2l_build_scripts_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708ae, "RFA_RF_LTE_L2L_BUILD_SCRIPTS_CNF", NULL ),

  /* RFA_RF_LTE_PLL_ADJUST_CNF, payload: rfa_rf_lte_pll_adjust_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708af, "RFA_RF_LTE_PLL_ADJUST_CNF", NULL ),

  /* RFA_RF_LTE_GET_UE_MIN_POWER_CNF, payload: rfa_rf_lte_get_ue_min_power_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708b3, "RFA_RF_LTE_GET_UE_MIN_POWER_CNF", NULL ),

  /* RFA_RF_LTE_ANT_SW_CNF, payload: rfa_rf_lte_ant_sw_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708b5, "RFA_RF_LTE_ANT_SW_CNF", NULL ),

  /* RFA_RF_LTE_RX1_CHAIN_CONFIG_CNF, payload: rfa_rf_lte_rx1_chain_config_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060708bf, "RFA_RF_LTE_RX1_CHAIN_CONFIG_CNF", NULL ),

  /* RFA_RF_LTE_FTM_CONFIGURE_TX_SWEEP_CMD, payload: rfa_rf_lte_ftm_configure_tx_sweep_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0608010d, "RFA_RF_LTE_FTM_CONFIGURE_TX_SWEEP_CMD", NULL ),

  /* RFA_RF_LTE_FTM_SET_RX_IQ_MISMATCH_COMP_COEFFTS_CMD, payload: rfa_rf_lte_ftm_set_rx_iq_mismatch_coeft_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080110, "RFA_RF_LTE_FTM_SET_RX_IQ_MISMATCH_COMP_COEFFTS_CMD", NULL ),

  /* RFA_RF_LTE_FTM_GET_HDET_FROM_TX_SWEEP_CMD, payload: rfa_rf_lte_ftm_get_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080116, "RFA_RF_LTE_FTM_GET_HDET_FROM_TX_SWEEP_CMD", NULL ),

  /* RFA_RF_LTE_FTM_GET_ALL_HDET_FROM_TX_SWEEP_CMD, payload: rfa_rf_lte_ftm_get_all_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080117, "RFA_RF_LTE_FTM_GET_ALL_HDET_FROM_TX_SWEEP_CMD", NULL ),

  /* RFA_RF_LTE_FTM_GET_LPM_HDET_FROM_TX_SWEEP_CMD, payload: rfa_rf_lte_ftm_get_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0608011f, "RFA_RF_LTE_FTM_GET_LPM_HDET_FROM_TX_SWEEP_CMD", NULL ),

  /* RFA_RF_LTE_FTM_GET_ALL_LPM_HDET_FROM_TX_SWEEP_CMD, payload: rfa_rf_lte_ftm_get_all_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080120, "RFA_RF_LTE_FTM_GET_ALL_LPM_HDET_FROM_TX_SWEEP_CMD", NULL ),

  /* RFA_RF_LTE_FTM_OVERRIDE_TXAGC_OUTPUT_POWER_CMD, payload: rfa_rf_lte_ftm_oride_txagc_output_power_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080121, "RFA_RF_LTE_FTM_OVERRIDE_TXAGC_OUTPUT_POWER_CMD", NULL ),

  /* RFA_RF_LTE_FTM_OVERRIDE_TXAGC_BACKOFF_CMD, payload: rfa_rf_lte_ftm_oride_txagc_backoff_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080122, "RFA_RF_LTE_FTM_OVERRIDE_TXAGC_BACKOFF_CMD", NULL ),

  /* RFA_RF_LTE_FTM_FBRX_UPDATE_CMD, payload: rfa_rf_lte_ftm_fbrx_update_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080123, "RFA_RF_LTE_FTM_FBRX_UPDATE_CMD", NULL ),

  /* RFA_RF_LTE_FTM_CONFIGURE_TX_SWEEP_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0608030d, "RFA_RF_LTE_FTM_CONFIGURE_TX_SWEEP_RSP", NULL ),

  /* RFA_RF_LTE_FTM_SET_RX_IQ_MISMATCH_COMP_COEFFTS_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080310, "RFA_RF_LTE_FTM_SET_RX_IQ_MISMATCH_COMP_COEFFTS_RSP", NULL ),

  /* RFA_RF_LTE_FTM_GET_HDET_FROM_TX_SWEEP_RSP, payload: rfa_rf_lte_ftm_get_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080316, "RFA_RF_LTE_FTM_GET_HDET_FROM_TX_SWEEP_RSP", NULL ),

  /* RFA_RF_LTE_FTM_GET_ALL_HDET_FROM_TX_SWEEP_RSP, payload: rfa_rf_lte_ftm_get_all_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080317, "RFA_RF_LTE_FTM_GET_ALL_HDET_FROM_TX_SWEEP_RSP", NULL ),

  /* RFA_RF_LTE_FTM_GET_LPM_HDET_FROM_TX_SWEEP_RSP, payload: rfa_rf_lte_ftm_get_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0608031f, "RFA_RF_LTE_FTM_GET_LPM_HDET_FROM_TX_SWEEP_RSP", NULL ),

  /* RFA_RF_LTE_FTM_GET_ALL_LPM_HDET_FROM_TX_SWEEP_RSP, payload: rfa_rf_lte_ftm_get_all_hdet_from_sweep_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080320, "RFA_RF_LTE_FTM_GET_ALL_LPM_HDET_FROM_TX_SWEEP_RSP", NULL ),

  /* RFA_RF_LTE_FTM_OVERRIDE_TXAGC_OUTPUT_POWER_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080321, "RFA_RF_LTE_FTM_OVERRIDE_TXAGC_OUTPUT_POWER_RSP", NULL ),

  /* RFA_RF_LTE_FTM_OVERRIDE_TXAGC_BACKOFF_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080322, "RFA_RF_LTE_FTM_OVERRIDE_TXAGC_BACKOFF_RSP", NULL ),

  /* RFA_RF_LTE_FTM_FBRX_UPDATE_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x06080323, "RFA_RF_LTE_FTM_FBRX_UPDATE_RSP", NULL ),

  /* RFA_RF_LTE_FTM_GENERIC_MESSAGE_RSP, payload: rfa_rf_lte_ftm_generic_message_cmd_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060803ff, "RFA_RF_LTE_FTM_GENERIC_MESSAGE_RSP", NULL ),

  /* RFA_RF_TDSCDMA_ENTER_MODE_REQ, payload: rfa_tdscdma_enter_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0201, "RFA_RF_TDSCDMA_ENTER_MODE_REQ", NULL ),

  /* RFA_RF_TDSCDMA_EXIT_MODE_REQ, payload: rfa_tdscdma_exit_mode_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0202, "RFA_RF_TDSCDMA_EXIT_MODE_REQ", NULL ),

  /* RFA_RF_TDSCDMA_ENABLE_RX_REQ, payload: rfa_tdscdma_enable_rx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0203, "RFA_RF_TDSCDMA_ENABLE_RX_REQ", NULL ),

  /* RFA_RF_TDSCDMA_ENABLE_TX_REQ, payload: rfa_tdscdma_enable_tx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0204, "RFA_RF_TDSCDMA_ENABLE_TX_REQ", NULL ),

  /* RFA_RF_TDSCDMA_DISABLE_TX_REQ, payload: rfa_tdscdma_disable_tx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0205, "RFA_RF_TDSCDMA_DISABLE_TX_REQ", NULL ),

  /* RFA_RF_TDSCDMA_SLEEP_REQ, payload: rfa_tdscdma_sleep_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0206, "RFA_RF_TDSCDMA_SLEEP_REQ", NULL ),

  /* RFA_RF_TDSCDMA_WAKEUP_REQ, payload: rfa_tdscdma_wakeup_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0207, "RFA_RF_TDSCDMA_WAKEUP_REQ", NULL ),

  /* RFA_RF_TDSCDMA_SET_TX_PWR_LIMIT_REQ, payload: rfa_tdscdma_set_tx_pwr_limit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0208, "RFA_RF_TDSCDMA_SET_TX_PWR_LIMIT_REQ", NULL ),

  /* RFA_RF_TDSCDMA_GET_RF_WARMUP_TIME_REQ, payload: rfa_tdscdma_get_rf_warmup_time_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0209, "RFA_RF_TDSCDMA_GET_RF_WARMUP_TIME_REQ", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_BHO_REQ, payload: rfa_tdscdma_update_bho_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020a, "RFA_RF_TDSCDMA_UPDATE_BHO_REQ", NULL ),

  /* RFA_RF_TDSCDMA_SWITCH_BUF_REQ, payload: rfa_tdscdma_switch_buf_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020b, "RFA_RF_TDSCDMA_SWITCH_BUF_REQ", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_TDS_NBR_REQ, payload: rfa_tdscdma_update_tds_nbr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020c, "RFA_RF_TDSCDMA_UPDATE_TDS_NBR_REQ", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_GSM_NBR_REQ, payload: rfa_tdscdma_update_gsm_nbr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020d, "RFA_RF_TDSCDMA_UPDATE_GSM_NBR_REQ", NULL ),

  /* RFA_RF_TDSCDMA_GET_MAX_TX_PWR_REQ, payload: rfa_tdscdma_get_max_tx_pwr_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020e, "RFA_RF_TDSCDMA_GET_MAX_TX_PWR_REQ", NULL ),

  /* RFA_RF_TDSCDMA_THERM_BACKOFF_REQ, payload: rfa_tdscdma_therm_backoff_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a020f, "RFA_RF_TDSCDMA_THERM_BACKOFF_REQ", NULL ),

  /* RFA_RF_TDSCDMA_INIT_TX_REQ, payload: rfa_tdscdma_init_tx_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0210, "RFA_RF_TDSCDMA_INIT_TX_REQ", NULL ),

  /* RFA_RF_TDSCDMA_SET_COEX_TX_PWR_LIMIT_REQ, payload: rfa_tdscdma_set_coex_tx_pwr_limit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0211, "RFA_RF_TDSCDMA_SET_COEX_TX_PWR_LIMIT_REQ", NULL ),

  /* RFA_RF_TDSCDMA_GET_RXAGC_VAL_REQ, payload: rfa_tdscdma_get_rxagc_val_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0212, "RFA_RF_TDSCDMA_GET_RXAGC_VAL_REQ", NULL ),

  /* RFA_RF_TDSCDMA_GET_AGC_LOG_REQ, payload: rfa_tdscdma_get_agc_log_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0213, "RFA_RF_TDSCDMA_GET_AGC_LOG_REQ", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_TUNER_REQ, payload: rfa_tdscdma_update_tuner_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a021d, "RFA_RF_TDSCDMA_UPDATE_TUNER_REQ", NULL ),

  /* RFA_RF_TDSCDMA_ANT_SW_REQ, payload: rfa_tdscdma_get_agc_log_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a021e, "RFA_RF_TDSCDMA_ANT_SW_REQ", NULL ),

  /* RFA_RF_TDSCDMA_GET_RF_TXCHAIN_NO_REQ, payload: rfa_tdscdma_get_rf_txchain_no_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0220, "RFA_RF_TDSCDMA_GET_RF_TXCHAIN_NO_REQ", NULL ),

  /* RFA_RF_TDSCDMA_DYNAMIC_SCRIPT_REQ, payload: rfa_tdscdma_dynamic_script_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0221, "RFA_RF_TDSCDMA_DYNAMIC_SCRIPT_REQ", NULL ),

  /* RFA_RF_TDSCDMA_ANT_SW_ABORT_IND, payload: rfa_tdscdma_ant_sw_abort_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a041f, "RFA_RF_TDSCDMA_ANT_SW_ABORT_IND", NULL ),

  /* RFA_RF_TDSCDMA_ENTER_MODE_CNF, payload: rfa_tdscdma_enter_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0801, "RFA_RF_TDSCDMA_ENTER_MODE_CNF", NULL ),

  /* RFA_RF_TDSCDMA_EXIT_MODE_CNF, payload: rfa_tdscdma_exit_mode_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0802, "RFA_RF_TDSCDMA_EXIT_MODE_CNF", NULL ),

  /* RFA_RF_TDSCDMA_ENABLE_RX_CNF, payload: rfa_tdscdma_enable_rx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0803, "RFA_RF_TDSCDMA_ENABLE_RX_CNF", NULL ),

  /* RFA_RF_TDSCDMA_ENABLE_TX_CNF, payload: rfa_tdscdma_enable_tx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0804, "RFA_RF_TDSCDMA_ENABLE_TX_CNF", NULL ),

  /* RFA_RF_TDSCDMA_DISABLE_TX_CNF, payload: rfa_tdscdma_disable_tx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0805, "RFA_RF_TDSCDMA_DISABLE_TX_CNF", NULL ),

  /* RFA_RF_TDSCDMA_SLEEP_CNF, payload: rfa_tdscdma_sleep_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0806, "RFA_RF_TDSCDMA_SLEEP_CNF", NULL ),

  /* RFA_RF_TDSCDMA_WAKEUP_CNF, payload: rfa_tdscdma_wakeup_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0807, "RFA_RF_TDSCDMA_WAKEUP_CNF", NULL ),

  /* RFA_RF_TDSCDMA_SET_TX_PWR_LIMIT_CNF, payload: rfa_tdscdma_set_tx_pwr_limit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0808, "RFA_RF_TDSCDMA_SET_TX_PWR_LIMIT_CNF", NULL ),

  /* RFA_RF_TDSCDMA_GET_RF_WARMUP_TIME_CNF, payload: rfa_tdscdma_get_rf_warmup_time_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0809, "RFA_RF_TDSCDMA_GET_RF_WARMUP_TIME_CNF", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_BHO_CNF, payload: rfa_tdscdma_update_bho_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080a, "RFA_RF_TDSCDMA_UPDATE_BHO_CNF", NULL ),

  /* RFA_RF_TDSCDMA_SWITCH_BUF_CNF, payload: rfa_tdscdma_switch_buf_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080b, "RFA_RF_TDSCDMA_SWITCH_BUF_CNF", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_TDS_NBR_CNF, payload: rfa_tdscdma_update_tds_nbr_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080c, "RFA_RF_TDSCDMA_UPDATE_TDS_NBR_CNF", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_GSM_NBR_CNF, payload: rfa_tdscdma_update_gsm_nbr_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080d, "RFA_RF_TDSCDMA_UPDATE_GSM_NBR_CNF", NULL ),

  /* RFA_RF_TDSCDMA_GET_MAX_TX_PWR_CNF, payload: rfa_tdscdma_get_max_tx_pwr_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080e, "RFA_RF_TDSCDMA_GET_MAX_TX_PWR_CNF", NULL ),

  /* RFA_RF_TDSCDMA_THERM_BACKOFF_CNF, payload: rfa_tdscdma_therm_backoff_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a080f, "RFA_RF_TDSCDMA_THERM_BACKOFF_CNF", NULL ),

  /* RFA_RF_TDSCDMA_INIT_TX_CNF, payload: rfa_tdscdma_init_tx_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0810, "RFA_RF_TDSCDMA_INIT_TX_CNF", NULL ),

  /* RFA_RF_TDSCDMA_GET_RXAGC_VAL_CNF, payload: rfa_tdscdma_get_rxagc_val_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0812, "RFA_RF_TDSCDMA_GET_RXAGC_VAL_CNF", NULL ),

  /* RFA_RF_TDSCDMA_GET_AGC_LOG_CNF, payload: rfa_tdscdma_get_agc_log_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0813, "RFA_RF_TDSCDMA_GET_AGC_LOG_CNF", NULL ),

  /* RFA_RF_TDSCDMA_UPDATE_TUNER_CNF, payload: rfa_tdscdma_update_tuner_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a081d, "RFA_RF_TDSCDMA_UPDATE_TUNER_CNF", NULL ),

  /* RFA_RF_TDSCDMA_ANT_SW_CNF, payload: rfa_tdscdma_ant_sw_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a081e, "RFA_RF_TDSCDMA_ANT_SW_CNF", NULL ),

  /* RFA_RF_TDSCDMA_GET_RF_TXCHAIN_NO_CNF, payload: rfa_tdscdma_get_rf_txchain_no_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0820, "RFA_RF_TDSCDMA_GET_RF_TXCHAIN_NO_CNF", NULL ),

  /* RFA_RF_TDSCDMA_DYNAMIC_SCRIPT_CNF, payload: rfa_tdscdma_dynamic_script_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060a0821, "RFA_RF_TDSCDMA_DYNAMIC_SCRIPT_CNF", NULL ),

  /* RFA_RF_TUNER_SET_SCENARIO_CMD, payload: uint32 */
  MSGR_UMID_TABLE_ENTRY ( 0x060b0101, "RFA_RF_TUNER_SET_SCENARIO_CMD", NULL ),

  /* RFA_RF_TUNER_GET_SCENARIO_CMD, payload: uint32 */
  MSGR_UMID_TABLE_ENTRY ( 0x060b0102, "RFA_RF_TUNER_GET_SCENARIO_CMD", NULL ),

  /* RFA_RF_TUNER_GET_REVISION_CMD, payload: uint32 */
  MSGR_UMID_TABLE_ENTRY ( 0x060b0103, "RFA_RF_TUNER_GET_REVISION_CMD", NULL ),

  /* RFA_RF_MEAS_GET_IRAT_INFO_PARAM_REQ, payload: rfa_rf_meas_get_irat_info_param_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0201, "RFA_RF_MEAS_GET_IRAT_INFO_PARAM_REQ", NULL ),

  /* RFA_RF_MEAS_GET_TIME_CONSTANT_REQ, payload: rfa_rf_meas_get_time_constant_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0202, "RFA_RF_MEAS_GET_TIME_CONSTANT_REQ", NULL ),

  /* RFA_RF_MEAS_SCRIPT_ENTER_REQ, payload: rfa_rf_meas_script_enter_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0203, "RFA_RF_MEAS_SCRIPT_ENTER_REQ", NULL ),

  /* RFA_RF_MEAS_SCRIPT_BUILD_SCRIPTS_REQ, payload: rfa_rf_meas_script_build_scripts_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0204, "RFA_RF_MEAS_SCRIPT_BUILD_SCRIPTS_REQ", NULL ),

  /* RFA_RF_MEAS_SCRIPT_EXIT_REQ, payload: rfa_rf_meas_script_exit_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0205, "RFA_RF_MEAS_SCRIPT_EXIT_REQ", NULL ),

  /* RFA_RF_MEAS_GET_IRAT_INFO_PARAM_CNF, payload: rfa_rf_meas_get_irat_info_param_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0801, "RFA_RF_MEAS_GET_IRAT_INFO_PARAM_CNF", NULL ),

  /* RFA_RF_MEAS_GET_TIME_CONSTANT_CNF, payload: rfa_rf_meas_get_time_constant_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0802, "RFA_RF_MEAS_GET_TIME_CONSTANT_CNF", NULL ),

  /* RFA_RF_MEAS_SCRIPT_ENTER_CNF, payload: rfa_rf_meas_script_enter_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0803, "RFA_RF_MEAS_SCRIPT_ENTER_CNF", NULL ),

  /* RFA_RF_MEAS_SCRIPT_BUILD_SCRIPTS_CNF, payload: rfa_rf_meas_script_build_scripts_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0804, "RFA_RF_MEAS_SCRIPT_BUILD_SCRIPTS_CNF", NULL ),

  /* RFA_RF_MEAS_SCRIPT_EXIT_CNF, payload: rfa_rf_meas_script_exit_cnf_s */
  MSGR_UMID_TABLE_ENTRY ( 0x060c0805, "RFA_RF_MEAS_SCRIPT_EXIT_CNF", NULL ),

  /* RFA_FW_SET_FTM_MODE_CMD, payload: rfcmd_set_ftm_mode_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800100, "RFA_FW_SET_FTM_MODE_CMD", NULL ),

  /* RFA_FW_SEND_EVENT_CMD, payload: rfcmd_send_event_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800101, "RFA_FW_SEND_EVENT_CMD", NULL ),

  /* RFA_FW_REQ_TXC_BANKS_CMD, payload: rfcmd_req_txc_banks_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800102, "RFA_FW_REQ_TXC_BANKS_CMD", NULL ),

  /* RFA_FW_FREE_TXC_BANKS_CMD, payload: rfcmd_free_txc_banks_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800103, "RFA_FW_FREE_TXC_BANKS_CMD", NULL ),

  /* RFA_FW_DPD_TABLE_WRITE_CMD, payload: rfcmd_dpd_table_write_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800104, "RFA_FW_DPD_TABLE_WRITE_CMD", NULL ),

  /* RFA_FW_REQUEST_TQ_CMD, payload: rfcmd_request_tq_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800105, "RFA_FW_REQUEST_TQ_CMD", NULL ),

  /* RFA_FW_FREE_TQ_CMD, payload: rfcmd_free_tq_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800106, "RFA_FW_FREE_TQ_CMD", NULL ),

  /* RFA_FW_TXAGC_OVERRIDE_CMD, payload: rfcmd_txagc_override_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800107, "RFA_FW_TXAGC_OVERRIDE_CMD", NULL ),

  /* RFA_FW_ALLOC_TXC_BANKS_RSP, payload: rfcmd_alloc_txc_banks_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800300, "RFA_FW_ALLOC_TXC_BANKS_RSP", NULL ),

  /* RFA_FW_DPD_TABLE_WRITE_RSP, payload: rfcmd_dpd_table_write_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800301, "RFA_FW_DPD_TABLE_WRITE_RSP", NULL ),

  /* RFA_FW_REQUEST_TQ_RSP, payload: rfcmd_request_tq_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800302, "RFA_FW_REQUEST_TQ_RSP", NULL ),

  /* RFA_FW_FREE_TXC_BANKS_RSP, payload: rfcmd_free_txc_banks_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800303, "RFA_FW_FREE_TXC_BANKS_RSP", NULL ),

  /* RFA_FW_TXAGC_OVERRIDE_RSP, payload: rfcmd_txagc_override_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800304, "RFA_FW_TXAGC_OVERRIDE_RSP", NULL ),

  /* RFA_FW_SAMPLE_CAPT_DONE_RSP, payload: rfcmd_sample_capt_done_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800305, "RFA_FW_SAMPLE_CAPT_DONE_RSP", NULL ),

  /* RFA_FW_XPT_CAL_PROC_DONE_RSP, payload: rfcmd_xpt_cal_proc_done_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800306, "RFA_FW_XPT_CAL_PROC_DONE_RSP", NULL ),

  /* RFA_FW_FREE_TQ_RSP, payload: rfcmd_free_tq_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800307, "RFA_FW_FREE_TQ_RSP", NULL ),

  /* RFA_FW_QTF_TOKEN_RSP, payload: rfcmd_qtf_token_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800308, "RFA_FW_QTF_TOKEN_RSP", NULL ),

  /* RFA_FW_QTF_CL_MEAS_RSP, payload: rfcmd_qtf_cl_meas_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06800309, "RFA_FW_QTF_CL_MEAS_RSP", NULL ),

  /* RFA_FW_ASD_TOKEN_RSP, payload: rfcmd_asd_token_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0680030a, "RFA_FW_ASD_TOKEN_RSP", NULL ),

  /* RFA_FW_SHUTDOWN_CMDI, payload: rfcmd_shutdown_t */
  MSGR_UMID_TABLE_ENTRY ( 0x06801100, "RFA_FW_SHUTDOWN_CMDI", NULL ),

  /* CDMA_FW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x07000000, "CDMA_FW_LOOPBACK_SPR", NULL ),

  /* CDMA_FW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x07000001, "CDMA_FW_LOOPBACK_REPLY_SPR", NULL ),

  /* CDMA_FW_STATE_CFG_CMD, payload: cdmafw_state_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000100, "CDMA_FW_STATE_CFG_CMD", NULL ),

  /* CDMA_FW_FINGER_ASSIGN_CMD, payload: cdmafw_finger_assign_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000101, "CDMA_FW_FINGER_ASSIGN_CMD", NULL ),

  /* CDMA_FW_MULTIPLE_FINGERS_ASSIGN_CMD, payload: cdmafw_multiple_fingers_assign_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000102, "CDMA_FW_MULTIPLE_FINGERS_ASSIGN_CMD", NULL ),

  /* CDMA_FW_FINGER_CONFIG_CMD, payload: cdmafw_finger_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000103, "CDMA_FW_FINGER_CONFIG_CMD", NULL ),

  /* CDMA_FW_MULTIPLE_FINGERS_CONFIG_CMD, payload: cdmafw_multiple_fingers_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000104, "CDMA_FW_MULTIPLE_FINGERS_CONFIG_CMD", NULL ),

  /* CDMA_FW_FINGER_COMMON_CONFIG_CMD, payload: cdmafw_finger_common_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000105, "CDMA_FW_FINGER_COMMON_CONFIG_CMD", NULL ),

  /* CDMA_FW_AFC_MODE_CMD, payload: cdmafw_afc_mode_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000106, "CDMA_FW_AFC_MODE_CMD", NULL ),

  /* CDMA_FW_XO_AFC_STATIC_CONFIG_CMD, payload: cdmafw_xo_afc_static_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000107, "CDMA_FW_XO_AFC_STATIC_CONFIG_CMD", NULL ),

  /* CDMA_FW_XO_AFC_DYNAMIC_CONFIG_CMD, payload: cdmafw_xo_afc_dynamic_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000108, "CDMA_FW_XO_AFC_DYNAMIC_CONFIG_CMD", NULL ),

  /* CDMA_FW_FL_SIGNALING_CMD, payload: cdmafw_fl_signaling_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000109, "CDMA_FW_FL_SIGNALING_CMD", NULL ),

  /* CDMA_FW_APF_ENABLE_CMD, payload: cdmafw_apf_enable_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010a, "CDMA_FW_APF_ENABLE_CMD", NULL ),

  /* CDMA_FW_FPC_FREEZE_CONFIG_CMD, payload: cdmafw_fpc_freeze_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010b, "CDMA_FW_FPC_FREEZE_CONFIG_CMD", NULL ),

  /* CDMA_FW_FPC_OVERRIDE_CMD, payload: cdmafw_fpc_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010c, "CDMA_FW_FPC_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_SCH_ACC_SCALE_CMD, payload: cdmafw_sch_acc_scale_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010d, "CDMA_FW_SCH_ACC_SCALE_CMD", NULL ),

  /* CDMA_FW_FCH_TARGET_SETPOINT_CMD, payload: cdmafw_fch_target_setpoint_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010e, "CDMA_FW_FCH_TARGET_SETPOINT_CMD", NULL ),

  /* CDMA_FW_SCH_TARGET_SETPOINT_CMD, payload: cdmafw_sch_target_setpoint_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700010f, "CDMA_FW_SCH_TARGET_SETPOINT_CMD", NULL ),

  /* CDMA_FW_RDA_CONFIG_CMD, payload: cdmafw_rda_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000110, "CDMA_FW_RDA_CONFIG_CMD", NULL ),

  /* CDMA_FW_QOF_NOISE_EST_FPC_CMD, payload: cdmafw_qof_noise_est_fpc_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000111, "CDMA_FW_QOF_NOISE_EST_FPC_CMD", NULL ),

  /* CDMA_FW_DEMBACK_MODE_CONFIG_CMD, payload: cdmafw_demback_mode_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000112, "CDMA_FW_DEMBACK_MODE_CONFIG_CMD", NULL ),

  /* CDMA_FW_RL_SIGNALING_CMD, payload: cdmafw_rl_signaling_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000113, "CDMA_FW_RL_SIGNALING_CMD", NULL ),

  /* CDMA_FW_RL_LEGACY_RC_FRAME_CONFIG_CMD, payload: cdmafw_rl_legacy_rc_frame_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000114, "CDMA_FW_RL_LEGACY_RC_FRAME_CONFIG_CMD", NULL ),

  /* CDMA_FW_RL_RC8_FRAME_CONFIG_CMD, payload: cdmafw_rl_rc8_frame_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000115, "CDMA_FW_RL_RC8_FRAME_CONFIG_CMD", NULL ),

  /* CDMA_FW_RL_MPP_CONFIG_CMD, payload: cdmafw_rl_mpp_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000116, "CDMA_FW_RL_MPP_CONFIG_CMD", NULL ),

  /* CDMA_FW_RL_PWR_FILTER_CONFIG_CMD, payload: cfw_rl_pwr_est_filter_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000117, "CDMA_FW_RL_PWR_FILTER_CONFIG_CMD", NULL ),

  /* CDMA_FW_RL_ACK_TEST_MODE_CONFIG_CMD, payload: cdmafw_rl_ack_test_mode_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000118, "CDMA_FW_RL_ACK_TEST_MODE_CONFIG_CMD", NULL ),

  /* CDMA_FW_QLIC_WALSH_PWR_LOGGING_CMD, payload: cdmafw_qlic_walsh_pwr_logging_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000119, "CDMA_FW_QLIC_WALSH_PWR_LOGGING_CMD", NULL ),

  /* CDMA_FW_ADV1X_LOG_CONFIG_CMD, payload: cdmafw_adv1x_log_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011a, "CDMA_FW_ADV1X_LOG_CONFIG_CMD", NULL ),

  /* CDMA_FW_TX_AGC_CFG_CMD, payload: cfw_tx_agc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011b, "CDMA_FW_TX_AGC_CFG_CMD", NULL ),

  /* CDMA_FW_TX_PA_PARAMS_CONFIG_CMD, payload: cdmafw_tx_pa_params_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011c, "CDMA_FW_TX_PA_PARAMS_CONFIG_CMD", NULL ),

  /* CDMA_FW_TX_LIMIT_CONFIG_CMD, payload: cfw_tx_limit_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011d, "CDMA_FW_TX_LIMIT_CONFIG_CMD", NULL ),

  /* CDMA_FW_TX_HDET_CONFIG_CMD, payload: cfw_tx_hdet_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011e, "CDMA_FW_TX_HDET_CONFIG_CMD", NULL ),

  /* CDMA_FW_DAC_CAL_CONFIG_CMD, payload: cfw_tx_dac_cal_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700011f, "CDMA_FW_DAC_CAL_CONFIG_CMD", NULL ),

  /* CDMA_FW_TX_OVERRIDE_CMD, payload: cdmafw_tx_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000120, "CDMA_FW_TX_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_TX_CLOSED_LOOP_OVERRIDE_CMD, payload: cdmafw_tx_closed_loop_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000121, "CDMA_FW_TX_CLOSED_LOOP_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_TX_OPEN_LOOP_OVERRIDE_CMD, payload: cdmafw_tx_open_loop_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000122, "CDMA_FW_TX_OPEN_LOOP_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_TX_PA_STATE_OVERRIDE_CMD, payload: cdmafw_tx_pa_state_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000123, "CDMA_FW_TX_PA_STATE_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_TX_HDET_REQUEST_CMD, payload: cfw_tx_hdet_request_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000124, "CDMA_FW_TX_HDET_REQUEST_CMD", NULL ),

  /* CDMA_FW_RXLM_CONFIG_MASKED_CMD, payload: cdmafw_rxlm_config_masked_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000125, "CDMA_FW_RXLM_CONFIG_MASKED_CMD", NULL ),

  /* CDMA_FW_DYNAMIC_TXC_UPDATE_CMD, payload: cfw_dynamic_txc_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000126, "CDMA_FW_DYNAMIC_TXC_UPDATE_CMD", NULL ),

  /* CDMA_FW_TX_HDET_RESET_CMD, payload: cfw_tx_hdet_reset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000127, "CDMA_FW_TX_HDET_RESET_CMD", NULL ),

  /* CDMA_FW_RX_START_CFG_CMD, payload: cfw_rx_start_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000128, "CDMA_FW_RX_START_CFG_CMD", NULL ),

  /* CDMA_FW_RX_START_CMD, payload: cfw_rx_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000129, "CDMA_FW_RX_START_CMD", NULL ),

  /* CDMA_FW_RX_STOP_CMD, payload: cfw_rx_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012a, "CDMA_FW_RX_STOP_CMD", NULL ),

  /* CDMA_FW_TX_START_CMD, payload: cfw_tx_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012b, "CDMA_FW_TX_START_CMD", NULL ),

  /* CDMA_FW_TX_STOP_CMD, payload: cfw_tx_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012c, "CDMA_FW_TX_STOP_CMD", NULL ),

  /* CDMA_FW_TX_DAC_START_CMD, payload: cfw_tx_dac_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012d, "CDMA_FW_TX_DAC_START_CMD", NULL ),

  /* CDMA_FW_TX_DAC_STOP_CMD, payload: cfw_tx_dac_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012e, "CDMA_FW_TX_DAC_STOP_CMD", NULL ),

  /* CDMA_FW_INTELLICEIVER_UPDATE_CMD, payload: cfw_intelliceiver_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700012f, "CDMA_FW_INTELLICEIVER_UPDATE_CMD", NULL ),

  /* CDMA_FW_OQPCH_SEARCH_DONE_CMD, payload: cdmafw_oqpch_search_done_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000130, "CDMA_FW_OQPCH_SEARCH_DONE_CMD", NULL ),

  /* CDMA_FW_PILOT_MEAS_CFG_REQ_CMD, payload: cdmafw_pilot_meas_cfg_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000131, "CDMA_FW_PILOT_MEAS_CFG_REQ_CMD", NULL ),

  /* CDMA_FW_PILOT_MEAS_STOP_STREAM_CMD, payload: cdmafw_pilot_meas_stop_stream_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000132, "CDMA_FW_PILOT_MEAS_STOP_STREAM_CMD", NULL ),

  /* CDMA_FW_RX_AGC_CFG_CMD, payload: cfw_rx_agc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000133, "CDMA_FW_RX_AGC_CFG_CMD", NULL ),

  /* CDMA_FW_LNA_OVERRIDE_CMD, payload: cfw_lna_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000134, "CDMA_FW_LNA_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_LNA_GAIN_OFFSET_CMD, payload: cfw_lna_gain_offset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000135, "CDMA_FW_LNA_GAIN_OFFSET_CMD", NULL ),

  /* CDMA_FW_RX_AGC_STOP_CMD, payload: cfw_rx_agc_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000136, "CDMA_FW_RX_AGC_STOP_CMD", NULL ),

  /* CDMA_FW_RX_SET_FREQ_OFFSET_CMD, payload: cdmafw_rx_set_freq_offset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000137, "CDMA_FW_RX_SET_FREQ_OFFSET_CMD", NULL ),

  /* CDMA_FW_RXAGC_ACQ_MODE_CONFIG_CMD, payload: cdmafw_rxagc_acq_mode_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000138, "CDMA_FW_RXAGC_ACQ_MODE_CONFIG_CMD", NULL ),

  /* CDMA_FW_XPT_CONFIG_CMD, payload: cfw_xpt_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000139, "CDMA_FW_XPT_CONFIG_CMD", NULL ),

  /* CDMA_FW_EPT_CAPTURE_CMD, payload: cdmafw_ept_capture_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013a, "CDMA_FW_EPT_CAPTURE_CMD", NULL ),

  /* CDMA_FW_EPT_OVERRIDE_CMD, payload: cdmafw_ept_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013b, "CDMA_FW_EPT_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_EPT_CONFIG_CMD, payload: cfw_ept_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013c, "CDMA_FW_EPT_CONFIG_CMD", NULL ),

  /* CDMA_FW_EPT_SAMPLE_BUFFER_CFG_CMD, payload: cfw_ept_sample_buffer_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013d, "CDMA_FW_EPT_SAMPLE_BUFFER_CFG_CMD", NULL ),

  /* CDMA_FW_LOG_RX_IQ_SAMPLES_CMD, payload: cdmafw_rx_iq_samples_logging_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013e, "CDMA_FW_LOG_RX_IQ_SAMPLES_CMD", NULL ),

  /* CDMA_FW_DIAG_LOG_CONFIG_CMD, payload: cdmafw_diag_log_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700013f, "CDMA_FW_DIAG_LOG_CONFIG_CMD", NULL ),

  /* CDMA_FW_TDEC_BRDG_CONFIG_CMD, payload: cdmafw_tdec_brdg_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000140, "CDMA_FW_TDEC_BRDG_CONFIG_CMD", NULL ),

  /* CDMA_FW_XPT_CAPTURE_CMD, payload: cdmafw_xpt_capture_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000141, "CDMA_FW_XPT_CAPTURE_CMD", NULL ),

  /* CDMA_FW_STOP_TXAGC_UPDATE_CMD, payload: cdmafw_stop_txagc_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000142, "CDMA_FW_STOP_TXAGC_UPDATE_CMD", NULL ),

  /* CDMA_FW_TX_PA_RANGE_OVERRIDE_CMD, payload: cdmafw_tx_pa_range_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000143, "CDMA_FW_TX_PA_RANGE_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_RXAGC_LNA_CAL_OVERRIDE_CMD, payload: cdmafw_rxagc_lna_cal_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000144, "CDMA_FW_RXAGC_LNA_CAL_OVERRIDE_CMD", NULL ),

  /* CDMA_FW_JD_THRESHOLD_UPDATE_CMD, payload: cdmafw_jd_threshold_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000145, "CDMA_FW_JD_THRESHOLD_UPDATE_CMD", NULL ),

  /* CDMA_FW_JD_MODE_CMD, payload: cdmafw_jd_mode_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000146, "CDMA_FW_JD_MODE_CMD", NULL ),

  /* CDMA_FW_XPT_DELAY_UPDATE_CMD, payload: cfw_xpt_delay_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000147, "CDMA_FW_XPT_DELAY_UPDATE_CMD", NULL ),

  /* CDMA_FW_DEMBACK_SW_CONFIG_CMD, payload: cdmafw_demback_sw_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000148, "CDMA_FW_DEMBACK_SW_CONFIG_CMD", NULL ),

  /* CDMA_FW_REGISTER_RX_TX_ACTIVITY_CMD, payload: cdmafw_dsda_priority_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000149, "CDMA_FW_REGISTER_RX_TX_ACTIVITY_CMD", NULL ),

  /* CDMA_FW_ANT_SWITCH_ASDIV_CMD, payload: cfw_rf_execute_ant_switch_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700014a, "CDMA_FW_ANT_SWITCH_ASDIV_CMD", NULL ),

  /* CDMA_FW_QLIC_WALSH_PWR_LOGGING_READY_RSP, payload: cdmafw_qlic_walsh_pwr_logging_rdy_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000300, "CDMA_FW_QLIC_WALSH_PWR_LOGGING_READY_RSP", NULL ),

  /* CDMA_FW_STATE_CFG_RSP, payload: cdmafw_state_cfg_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000301, "CDMA_FW_STATE_CFG_RSP", NULL ),

  /* CDMA_FW_TX_HIGH_PWR_DET_RSP, payload: cdmafw_tx_high_pwr_det_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000302, "CDMA_FW_TX_HIGH_PWR_DET_RSP", NULL ),

  /* CDMA_FW_TX_START_RSP, payload: cfw_tx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000303, "CDMA_FW_TX_START_RSP", NULL ),

  /* CDMA_FW_TX_STOP_RSP, payload: cfw_tx_stop_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000304, "CDMA_FW_TX_STOP_RSP", NULL ),

  /* CDMA_FW_RX_START_RSP, payload: cfw_rx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000305, "CDMA_FW_RX_START_RSP", NULL ),

  /* CDMA_FW_RX_STOP_RSP, payload: cfw_rx_stop_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000306, "CDMA_FW_RX_STOP_RSP", NULL ),

  /* CDMA_FW_TX_HDET_RESPONSE_RSP, payload: cfw_tx_hdet_response_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000307, "CDMA_FW_TX_HDET_RESPONSE_RSP", NULL ),

  /* CDMA_FW_PILOT_MEAS_CFG_RSP, payload: cdmafw_pilot_meas_cfg_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000308, "CDMA_FW_PILOT_MEAS_CFG_RSP", NULL ),

  /* CDMA_FW_PILOT_MEAS_STOP_STREAM_RSP, payload: cdmafw_pilot_meas_stop_stream_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000309, "CDMA_FW_PILOT_MEAS_STOP_STREAM_RSP", NULL ),

  /* CDMA_FW_INTELLICEIVER_UPDATE_RSP, payload: cfw_intelliceiver_update_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030a, "CDMA_FW_INTELLICEIVER_UPDATE_RSP", NULL ),

  /* CDMA_FW_DAC_CAL_CONFIG_RSP, payload: cfw_dac_cal_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030b, "CDMA_FW_DAC_CAL_CONFIG_RSP", NULL ),

  /* CDMA_FW_IRAT_RX_START_RSP, payload: cfw_irat_rx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030c, "CDMA_FW_IRAT_RX_START_RSP", NULL ),

  /* CDMA_FW_EPT_CAPTURE_RSP, payload: cdmafw_ept_capture_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030d, "CDMA_FW_EPT_CAPTURE_RSP", NULL ),

  /* CDMA_FW_XPT_CAPTURE_RSP, payload: cdmafw_xpt_capture_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030e, "CDMA_FW_XPT_CAPTURE_RSP", NULL ),

  /* CDMA_FW_PA_PARAMS_CONFIG_RSP, payload: cfw_pa_params_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0700030f, "CDMA_FW_PA_PARAMS_CONFIG_RSP", NULL ),

  /* CDMA_FW_XPT_TRANS_COMPL_RSP, payload: cfw_xpt_trans_compl_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000310, "CDMA_FW_XPT_TRANS_COMPL_RSP", NULL ),

  /* CDMA_FW_DEMBACK_SW_CONFIG_RSP, payload: cdmafw_demback_sw_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000311, "CDMA_FW_DEMBACK_SW_CONFIG_RSP", NULL ),

  /* CDMA_FW_XPT_CONFIG_RSP, payload: cfw_xpt_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000312, "CDMA_FW_XPT_CONFIG_RSP", NULL ),

  /* CDMA_FW_ASDIV_PRI_CHANGE_RSP, payload: cdmafw_asdiv_pri_change_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000313, "CDMA_FW_ASDIV_PRI_CHANGE_RSP", NULL ),

  /* CDMA_FW_RXTX_FRAME_STATS_IND, payload: cdmafw_rxtx_frame_stats_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000400, "CDMA_FW_RXTX_FRAME_STATS_IND", NULL ),

  /* CDMA_FW_QLIC_FRAME_STATS_IND, payload: cdmafw_qlic_frame_stats_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000401, "CDMA_FW_QLIC_FRAME_STATS_IND", NULL ),

  /* CDMA_FW_RC11_FCH_DECODER_DONE_IND, payload: cdmafw_rc11_fch_decoder_done_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000402, "CDMA_FW_RC11_FCH_DECODER_DONE_IND", NULL ),

  /* CDMA_FW_ADV1X_LOG_BUFFER_RDY_IND, payload: cdmafw_adv1x_log_buffer_rdy_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000403, "CDMA_FW_ADV1X_LOG_BUFFER_RDY_IND", NULL ),

  /* CDMA_FW_VI_CTL_IND, payload: cdmafw_vi_ctl_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000404, "CDMA_FW_VI_CTL_IND", NULL ),

  /* CDMA_FW_RESET_REQ_IND, payload: cdmafw_reset_req_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000405, "CDMA_FW_RESET_REQ_IND", NULL ),

  /* CDMA_FW_EPT_PROC_DONE_IND, payload: cdmafw_ept_proc_done_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000406, "CDMA_FW_EPT_PROC_DONE_IND", NULL ),

  /* CDMA_FW_EPT_EXPANSION_DONE_IND, payload: cdmafw_ept_expansion_done_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07000407, "CDMA_FW_EPT_EXPANSION_DONE_IND", NULL ),

  /* CDMA_FW_MSGR_SHUT_DOWN_CMDI, payload: NULL */
  MSGR_UMID_TABLE_ENTRY ( 0x07001100, "CDMA_FW_MSGR_SHUT_DOWN_CMDI", NULL ),

  /* CDMA_FW_RX_START_CMDI, payload: NULL */
  MSGR_UMID_TABLE_ENTRY ( 0x07001101, "CDMA_FW_RX_START_CMDI", NULL ),

  /* CDMA_SRCH_FW_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x07010000, "CDMA_SRCH_FW_LOOPBACK_SPR", NULL ),

  /* CDMA_SRCH_FW_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x07010001, "CDMA_SRCH_FW_LOOPBACK_REPLY_SPR", NULL ),

  /* CDMA_SRCH_FW_SRCH_START_CMD, payload: srchfw_search_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010100, "CDMA_SRCH_FW_SRCH_START_CMD", NULL ),

  /* CDMA_SRCH_FW_SRCH_ABORT_CMD, payload: srchfw_abort_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010101, "CDMA_SRCH_FW_SRCH_ABORT_CMD", NULL ),

  /* CDMA_SRCH_FW_SRCH_OQPCH_CMD, payload: srchfw_oqpch_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010102, "CDMA_SRCH_FW_SRCH_OQPCH_CMD", NULL ),

  /* CDMA_SRCH_FW_SRCH_SHUTDOWN_CMD, payload: srchfw_shutdown_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010103, "CDMA_SRCH_FW_SRCH_SHUTDOWN_CMD", NULL ),

  /* CDMA_SRCH_FW_APP_CFG_CMD, payload: srchfw_app_cfg_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010104, "CDMA_SRCH_FW_APP_CFG_CMD", NULL ),

  /* CDMA_SRCH_FW_CDMA_SRCH_RESULTS_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010300, "CDMA_SRCH_FW_CDMA_SRCH_RESULTS_RSP", NULL ),

  /* CDMA_SRCH_FW_HDR_SRCH_RESULTS_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010301, "CDMA_SRCH_FW_HDR_SRCH_RESULTS_RSP", NULL ),

  /* CDMA_SRCH_FW_AFLT_SRCH_RESULTS_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010302, "CDMA_SRCH_FW_AFLT_SRCH_RESULTS_RSP", NULL ),

  /* CDMA_SRCH_FW_CDMA_SRCH_ABORT_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010303, "CDMA_SRCH_FW_CDMA_SRCH_ABORT_RSP", NULL ),

  /* CDMA_SRCH_FW_HDR_SRCH_ABORT_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010304, "CDMA_SRCH_FW_HDR_SRCH_ABORT_RSP", NULL ),

  /* CDMA_SRCH_FW_AFLT_SRCH_ABORT_RSP, payload: srchfw_search_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010305, "CDMA_SRCH_FW_AFLT_SRCH_ABORT_RSP", NULL ),

  /* CDMA_SRCH_FW_OQPCH_SRCH_RESULTS_RSP, payload: srchfw_oqpch_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010306, "CDMA_SRCH_FW_OQPCH_SRCH_RESULTS_RSP", NULL ),

  /* CDMA_SRCH_FW_APP_CFG_RSP, payload: srchfw_app_cfg_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x07010307, "CDMA_SRCH_FW_APP_CFG_RSP", NULL ),

  /* HDR_FW_STATE_CFG_CMD, payload: hdrfw_state_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000100, "HDR_FW_STATE_CFG_CMD", NULL ),

  /* HDR_FW_FING_ASSIGN_CMD, payload: hdrfw_fing_assign_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000101, "HDR_FW_FING_ASSIGN_CMD", NULL ),

  /* HDR_FW_RESET_SEQ_NUM_CMD, payload: hdrfw_reset_seq_num_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000102, "HDR_FW_RESET_SEQ_NUM_CMD", NULL ),

  /* HDR_FW_FTCMAC_CHANNEL_CFG_CMD, payload: hdrfw_ftcmac_channel_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000103, "HDR_FW_FTCMAC_CHANNEL_CFG_CMD", NULL ),

  /* HDR_FW_FMAC_ACTIVATE_CMD, payload: hdrfw_fmac_activate_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000104, "HDR_FW_FMAC_ACTIVATE_CMD", NULL ),

  /* HDR_FW_TUNEAWAY_START_CMD, payload: hdrfw_tuneaway_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000105, "HDR_FW_TUNEAWAY_START_CMD", NULL ),

  /* HDR_FW_HANDOFF_CFG_CMD, payload: hdrfw_handoff_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000106, "HDR_FW_HANDOFF_CFG_CMD", NULL ),

  /* HDR_FW_CC_DEMOD_CFG_CMD, payload: hdrfw_cc_demod_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000107, "HDR_FW_CC_DEMOD_CFG_CMD", NULL ),

  /* HDR_FW_REL0_RL_CFG_CMD, payload: hdrfw_rel0_rl_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000108, "HDR_FW_REL0_RL_CFG_CMD", NULL ),

  /* HDR_FW_EQ_CFG_CMD, payload: hdrfw_eq_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000109, "HDR_FW_EQ_CFG_CMD", NULL ),

  /* HDR_FW_EQ_OVERRIDE_CMD, payload: hdrfw_eq_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010a, "HDR_FW_EQ_OVERRIDE_CMD", NULL ),

  /* HDR_FW_FLL_START_ACQ_CMD, payload: hdrfw_fll_start_acq_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010b, "HDR_FW_FLL_START_ACQ_CMD", NULL ),

  /* HDR_FW_SLEEP_ADJ_CMD, payload: hdrfw_sleep_adj_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010c, "HDR_FW_SLEEP_ADJ_CMD", NULL ),

  /* HDR_FW_SCC_CMD, payload: hdrfw_scc_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010d, "HDR_FW_SCC_CMD", NULL ),

  /* HDR_FW_FPD_CMD, payload: hdrfw_fpd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010e, "HDR_FW_FPD_CMD", NULL ),

  /* HDR_FW_HYPERSPACE_CFG_CMD, payload: hdrfw_hyperspace_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800010f, "HDR_FW_HYPERSPACE_CFG_CMD", NULL ),

  /* HDR_FW_HSTR_OFFSET_CFG_CMD, payload: hdrfw_hstr_offset_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000110, "HDR_FW_HSTR_OFFSET_CFG_CMD", NULL ),

  /* HDR_FW_DECOB_UPDATE_CMD, payload: hdrfw_decob_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000111, "HDR_FW_DECOB_UPDATE_CMD", NULL ),

  /* HDR_FW_DRC_CFG_CMD, payload: hdrfw_drc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000112, "HDR_FW_DRC_CFG_CMD", NULL ),

  /* HDR_FW_INDICATION_CFG_CMD, payload: hdrfw_indication_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000113, "HDR_FW_INDICATION_CFG_CMD", NULL ),

  /* HDR_FW_DRC_TRANSLATION_CFG_CMD, payload: hdrfw_drc_translation_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000114, "HDR_FW_DRC_TRANSLATION_CFG_CMD", NULL ),

  /* HDR_FW_TIMERS_CFG_CMD, payload: hdrfw_timers_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000115, "HDR_FW_TIMERS_CFG_CMD", NULL ),

  /* HDR_FW_BCC_DETECT_CMD, payload: hdrfw_bcc_detect_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000116, "HDR_FW_BCC_DETECT_CMD", NULL ),

  /* HDR_FW_FLOW_CONTROL_CFG_CMD, payload: hdrfw_flow_control_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000117, "HDR_FW_FLOW_CONTROL_CFG_CMD", NULL ),

  /* HDR_FW_ASP_UPDATE_CMD, payload: hdrfw_asp_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000118, "HDR_FW_ASP_UPDATE_CMD", NULL ),

  /* HDR_FW_FLL_ACCUM_LOAD_CMD, payload: hdrfw_fll_accum_load_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000119, "HDR_FW_FLL_ACCUM_LOAD_CMD", NULL ),

  /* HDR_FW_FLL_GAIN_CFG_CMD, payload: hdrfw_fll_gain_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011a, "HDR_FW_FLL_GAIN_CFG_CMD", NULL ),

  /* HDR_FW_FLL_SLEW_CFG_CMD, payload: hdrfw_fll_slew_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011b, "HDR_FW_FLL_SLEW_CFG_CMD", NULL ),

  /* HDR_FW_DLL_CFG_CMD, payload: hdrfw_dll_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011c, "HDR_FW_DLL_CFG_CMD", NULL ),

  /* HDR_FW_FMAC_GAUP_CFG_CMD, payload: hdrfw_fmac_gaup_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011d, "HDR_FW_FMAC_GAUP_CFG_CMD", NULL ),

  /* HDR_FW_MSTR_SLAM_CMD, payload: hdrfw_mstr_slam_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011e, "HDR_FW_MSTR_SLAM_CMD", NULL ),

  /* HDR_FW_DECODE_CTL_CMD, payload: hdrfw_decode_ctl_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800011f, "HDR_FW_DECODE_CTL_CMD", NULL ),

  /* HDR_FW_PARTIAL_LD_RCL_CFG_CMD, payload: hdrfw_partial_ld_rcl_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000120, "HDR_FW_PARTIAL_LD_RCL_CFG_CMD", NULL ),

  /* HDR_FW_DECOB_IND_RATE_CMD, payload: hdrfw_decob_ind_rate_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000121, "HDR_FW_DECOB_IND_RATE_CMD", NULL ),

  /* HDR_FW_FING_LOCK_THRESH_CMD, payload: hdrfw_fing_lock_thresh_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000122, "HDR_FW_FING_LOCK_THRESH_CMD", NULL ),

  /* HDR_FW_LEGACY_FLOW_CONTROL_CMD, payload: hdrfw_legacy_flow_control_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000123, "HDR_FW_LEGACY_FLOW_CONTROL_CMD", NULL ),

  /* HDR_FW_FMAC_ACK_MODE_CFG_CMD, payload: hdrfw_fmac_ack_mode_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000124, "HDR_FW_FMAC_ACK_MODE_CFG_CMD", NULL ),

  /* HDR_FW_QPCH_CMD, payload: hdrfw_qpch_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000125, "HDR_FW_QPCH_CMD", NULL ),

  /* HDR_FW_TUNEAWAY_STOP_CMD, payload: hdrfw_tuneaway_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000126, "HDR_FW_TUNEAWAY_STOP_CMD", NULL ),

  /* HDR_FW_DRC_FILT_RESET_CMD, payload: hdrfw_drc_filt_reset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000127, "HDR_FW_DRC_FILT_RESET_CMD", NULL ),

  /* HDR_FW_FMAC_HANDOFF_CFG_CMD, payload: hdrfw_fmac_handoff_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000128, "HDR_FW_FMAC_HANDOFF_CFG_CMD", NULL ),

  /* HDR_FW_MUP_DECODE_FILT_CFG_CMD, payload: hdrfw_mup_decode_filt_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000129, "HDR_FW_MUP_DECODE_FILT_CFG_CMD", NULL ),

  /* HDR_FW_MIN_PREAM_THRESH_CFG_CMD, payload: hdrfw_min_pream_thresh_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012a, "HDR_FW_MIN_PREAM_THRESH_CFG_CMD", NULL ),

  /* HDR_FW_PLL_CFG_CMD, payload: hdrfw_pll_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012b, "HDR_FW_PLL_CFG_CMD", NULL ),

  /* HDR_FW_FLL_ROT_CFG_CMD, payload: hdrfw_fll_rot_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012c, "HDR_FW_FLL_ROT_CFG_CMD", NULL ),

  /* HDR_FW_FLL_ROT_OVERRIDE_CMD, payload: hdrfw_fll_rot_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012d, "HDR_FW_FLL_ROT_OVERRIDE_CMD", NULL ),

  /* HDR_FW_FLL_PDM_OVERRIDE_CMD, payload: hdrfw_fll_pdm_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012e, "HDR_FW_FLL_PDM_OVERRIDE_CMD", NULL ),

  /* HDR_FW_FLL_ENABLE_CMD, payload: hdrfw_fll_enable_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800012f, "HDR_FW_FLL_ENABLE_CMD", NULL ),

  /* HDR_FW_HANDOFF_OVERRIDE_CMD, payload: hdrfw_handoff_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000130, "HDR_FW_HANDOFF_OVERRIDE_CMD", NULL ),

  /* HDR_FW_DIVERSITY_CFG_CMD, payload: hdrfw_diversity_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000131, "HDR_FW_DIVERSITY_CFG_CMD", NULL ),

  /* HDR_FW_FORCE_COMB_MODE_CMD, payload: hdrfw_force_comb_mode_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000132, "HDR_FW_FORCE_COMB_MODE_CMD", NULL ),

  /* HDR_FW_MIN_BEST_ASP_SINR_CMD, payload: hdrfw_min_best_asp_sinr_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000133, "HDR_FW_MIN_BEST_ASP_SINR_CMD", NULL ),

  /* HDR_FW_MAX_DRC_CFG_CMD, payload: hdrfw_max_drc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000134, "HDR_FW_MAX_DRC_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_SUBFRAME_CFG_CMD, payload: hdrfw_rmac_subframe_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000135, "HDR_FW_RMAC_SUBFRAME_CFG_CMD", NULL ),

  /* HDR_FW_RMAC0_GAIN_OFFSET_CFG_CMD, payload: hdrfw_rmac0_gain_offset_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000136, "HDR_FW_RMAC0_GAIN_OFFSET_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_SEND_PACKET_CMD, payload: hdrfw_rmac_send_packet_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000137, "HDR_FW_RMAC_SEND_PACKET_CMD", NULL ),

  /* HDR_FW_RMAC_MISC_CFG_CMD, payload: hdrfw_rmac_misc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000138, "HDR_FW_RMAC_MISC_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_ATTRIB_CFG_CMD, payload: hdrfw_rmac_attrib_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000139, "HDR_FW_RMAC_ATTRIB_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_ENABLE_CARRIER_CMD, payload: hdrfw_rmac_enable_carrier_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013a, "HDR_FW_RMAC_ENABLE_CARRIER_CMD", NULL ),

  /* HDR_FW_RMAC_OPENLOOP_OVERRIDE_CMD, payload: hdrfw_rmac_openloop_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013b, "HDR_FW_RMAC_OPENLOOP_OVERRIDE_CMD", NULL ),

  /* HDR_FW_RMAC_HARQCOEFFC2I_CFG_CMD, payload: hdrfw_rmac_harqcoeffc2i_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013c, "HDR_FW_RMAC_HARQCOEFFC2I_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_RESET_PILOTPWR_FILT_CMD, payload: hdrfw_rmac_reset_pilotpwrfilt_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013d, "HDR_FW_RMAC_RESET_PILOTPWR_FILT_CMD", NULL ),

  /* HDR_FW_RMAC_RESET_RL_INTERLACE_CMD, payload: hdrfw_rmac_reset_rl_interlace_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013e, "HDR_FW_RMAC_RESET_RL_INTERLACE_CMD", NULL ),

  /* HDR_FW_RMAC_MODULATOR_CFG_CMD, payload: hdrfw_rmac_modulator_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800013f, "HDR_FW_RMAC_MODULATOR_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_PWRMARGIN_CFG_CMD, payload: hdrfw_rmac_pwrmargin_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000140, "HDR_FW_RMAC_PWRMARGIN_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_THROTTLE_CFG_CMD, payload: hdrfw_rmac_throttle_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000141, "HDR_FW_RMAC_THROTTLE_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_DTXMODE_CFG_CMD, payload: hdrfw_rmac_dtxmode_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000142, "HDR_FW_RMAC_DTXMODE_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_MAXBACKOFF_CFG_CMD, payload: hdrfw_rmac_maxbackoff_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000143, "HDR_FW_RMAC_MAXBACKOFF_CFG_CMD", NULL ),

  /* HDR_FW_RMAC_T2PBIAS_CFG_CMD, payload: hdrfw_rmac_t2pbias_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000144, "HDR_FW_RMAC_T2PBIAS_CFG_CMD", NULL ),

  /* HDR_FW_PILOT_MEAS_CFG_REQ_CMD, payload: hdrfw_pilot_meas_cfg_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000145, "HDR_FW_PILOT_MEAS_CFG_REQ_CMD", NULL ),

  /* HDR_FW_PILOT_MEAS_STOP_STREAM_CMD, payload: hdrfw_pilot_meas_stop_stream_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000146, "HDR_FW_PILOT_MEAS_STOP_STREAM_CMD", NULL ),

  /* HDR_FW_TX_PA_PARAMS_CONFIG_CMD, payload: hdrfw_tx_pa_params_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000147, "HDR_FW_TX_PA_PARAMS_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_LIMIT_CONFIG_CMD, payload: cfw_tx_limit_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000148, "HDR_FW_TX_LIMIT_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_LUT_PARAMS_CONFIG_CMD, payload: hdrfw_tx_lut_params_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000149, "HDR_FW_TX_LUT_PARAMS_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_FREQ_OFFSET_CONFIG_CMD, payload: hdrfw_tx_freq_offset_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014a, "HDR_FW_TX_FREQ_OFFSET_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_CLOSED_LOOP_CONFIG_CMD, payload: hdrfw_tx_closed_loop_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014b, "HDR_FW_TX_CLOSED_LOOP_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_OPEN_LOOP_CONFIG_CMD, payload: hdrfw_tx_open_loop_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014c, "HDR_FW_TX_OPEN_LOOP_CONFIG_CMD", NULL ),

  /* HDR_FW_TX_OPEN_LOOP_ANT_GAIN_DELTA_CMD, payload: hdrfw_tx_open_loop_ant_gain_delta_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014d, "HDR_FW_TX_OPEN_LOOP_ANT_GAIN_DELTA_CMD", NULL ),

  /* HDR_FW_RXLM_CONFIG_MASKED_CMD, payload: hdrfw_rxlm_config_masked_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014e, "HDR_FW_RXLM_CONFIG_MASKED_CMD", NULL ),

  /* HDR_FW_FLL_XO_CFG_CMD, payload: hdrfw_fll_xo_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800014f, "HDR_FW_FLL_XO_CFG_CMD", NULL ),

  /* HDR_FW_RX_START_CMD, payload: cfw_rx_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000150, "HDR_FW_RX_START_CMD", NULL ),

  /* HDR_FW_RX_START_CFG_CMD, payload: cfw_rx_start_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000151, "HDR_FW_RX_START_CFG_CMD", NULL ),

  /* HDR_FW_RX_AGC_CFG_CMD, payload: cfw_rx_agc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000152, "HDR_FW_RX_AGC_CFG_CMD", NULL ),

  /* HDR_FW_LNA_OVERRIDE_CMD, payload: cfw_lna_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000153, "HDR_FW_LNA_OVERRIDE_CMD", NULL ),

  /* HDR_FW_LNA_GAIN_OFFSET_CMD, payload: cfw_lna_gain_offset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000154, "HDR_FW_LNA_GAIN_OFFSET_CMD", NULL ),

  /* HDR_FW_RX_AGC_STOP_CMD, payload: cfw_rx_agc_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000155, "HDR_FW_RX_AGC_STOP_CMD", NULL ),

  /* HDR_FW_INTELLICEIVER_UPDATE_CMD, payload: cfw_intelliceiver_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000156, "HDR_FW_INTELLICEIVER_UPDATE_CMD", NULL ),

  /* HDR_FW_RXF_DYNAMIC_UPDATE_CMD, payload: cfw_rxf_dynamic_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000157, "HDR_FW_RXF_DYNAMIC_UPDATE_CMD", NULL ),

  /* HDR_FW_TX_PA_CTL_CMD, payload: hdrfw_tx_pa_ctl_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000158, "HDR_FW_TX_PA_CTL_CMD", NULL ),

  /* HDR_FW_RX_STOP_CMD, payload: cfw_rx_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000159, "HDR_FW_RX_STOP_CMD", NULL ),

  /* HDR_FW_HDET_REQUEST_CMD, payload: cfw_tx_hdet_request_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015a, "HDR_FW_HDET_REQUEST_CMD", NULL ),

  /* HDR_FW_TX_START_CMD, payload: cfw_tx_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015b, "HDR_FW_TX_START_CMD", NULL ),

  /* HDR_FW_TX_STOP_CMD, payload: cfw_tx_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015c, "HDR_FW_TX_STOP_CMD", NULL ),

  /* HDR_FW_TX_AGC_CFG_CMD, payload: cfw_tx_agc_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015d, "HDR_FW_TX_AGC_CFG_CMD", NULL ),

  /* HDR_FW_TX_DAC_START_CMD, payload: cfw_tx_dac_start_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015e, "HDR_FW_TX_DAC_START_CMD", NULL ),

  /* HDR_FW_TX_DAC_STOP_CMD, payload: cfw_tx_dac_stop_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800015f, "HDR_FW_TX_DAC_STOP_CMD", NULL ),

  /* HDR_FW_HDET_CONFIG_CMD, payload: cfw_tx_hdet_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000160, "HDR_FW_HDET_CONFIG_CMD", NULL ),

  /* HDR_FW_GRICE_SET_FAC_GAIN_CMD, payload: hdrfw_grice_set_fac_gain_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000161, "HDR_FW_GRICE_SET_FAC_GAIN_CMD", NULL ),

  /* HDR_FW_GRICE_OVERRIDE_CMD, payload: hdrfw_grice_override_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000162, "HDR_FW_GRICE_OVERRIDE_CMD", NULL ),

  /* HDR_FW_VI_CTL_CMD, payload: hdrfw_vi_ctl_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000163, "HDR_FW_VI_CTL_CMD", NULL ),

  /* HDR_FW_DYNAMIC_TXC_UPDATE_CMD, payload: cfw_dynamic_txc_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000164, "HDR_FW_DYNAMIC_TXC_UPDATE_CMD", NULL ),

  /* HDR_FW_TX_PWR_FILTER_CONFIG_CMD, payload: cfw_rl_pwr_est_filter_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000165, "HDR_FW_TX_PWR_FILTER_CONFIG_CMD", NULL ),

  /* HDR_FW_EPT_CONFIG_CMD, payload: cfw_ept_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000166, "HDR_FW_EPT_CONFIG_CMD", NULL ),

  /* HDR_FW_XPT_CONFIG_CMD, payload: cfw_xpt_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000167, "HDR_FW_XPT_CONFIG_CMD", NULL ),

  /* HDR_FW_DIAG_LOG_CONFIG_CMD, payload: hdrfw_diag_log_config_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000168, "HDR_FW_DIAG_LOG_CONFIG_CMD", NULL ),

  /* HDR_FW_JD_THRESHOLD_UPDATE_CMD, payload: hdrfw_jd_threshold_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000169, "HDR_FW_JD_THRESHOLD_UPDATE_CMD", NULL ),

  /* HDR_FW_JD_MODE_CMD, payload: hdrfw_jd_mode_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016a, "HDR_FW_JD_MODE_CMD", NULL ),

  /* HDR_FW_DLNA_CTL_CMD, payload: hdrfw_dlna_ctl_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016b, "HDR_FW_DLNA_CTL_CMD", NULL ),

  /* HDR_FW_EARLY_PREAM_CTL_CMD, payload: hdrfw_early_pream_ctl_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016c, "HDR_FW_EARLY_PREAM_CTL_CMD", NULL ),

  /* HDR_FW_FORCE_ODD_PREAM_CMD, payload: hdrfw_force_odd_pream_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016d, "HDR_FW_FORCE_ODD_PREAM_CMD", NULL ),

  /* HDR_FW_XPT_DELAY_UPDATE_CMD, payload: cfw_xpt_delay_update_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016e, "HDR_FW_XPT_DELAY_UPDATE_CMD", NULL ),

  /* HDR_FW_TX_HDET_RESET_CMD, payload: cfw_tx_hdet_reset_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800016f, "HDR_FW_TX_HDET_RESET_CMD", NULL ),

  /* HDR_FW_DSDX_ENABLE_CMD, payload: hdrfw_dsdx_enable_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000170, "HDR_FW_DSDX_ENABLE_CMD", NULL ),

  /* HDR_FW_REGISTER_RX_TX_ACTIVITY_CMD, payload: hdrfw_dsdx_priority_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000171, "HDR_FW_REGISTER_RX_TX_ACTIVITY_CMD", NULL ),

  /* HDR_FW_DSDX_FREQUENCY_UPDATE_CMD, payload: hdrfw_dsdx_frequency_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000172, "HDR_FW_DSDX_FREQUENCY_UPDATE_CMD", NULL ),

  /* HDR_FW_QTA_CFG_CMD, payload: hdrfw_qta_cfg_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000173, "HDR_FW_QTA_CFG_CMD", NULL ),

  /* HDR_FW_ANT_SWITCH_ASDIV_CMD, payload: cfw_rf_execute_ant_switch_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000174, "HDR_FW_ANT_SWITCH_ASDIV_CMD", NULL ),

  /* HDR_FW_STATE_CFG_RSP, payload: hdrfw_state_cfg_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000300, "HDR_FW_STATE_CFG_RSP", NULL ),

  /* HDR_FW_SRCH_FING_ASSIGN_RSP, payload: hdrfw_srch_fing_assign_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000301, "HDR_FW_SRCH_FING_ASSIGN_RSP", NULL ),

  /* HDR_FW_SRCH_FLL_ACQ_RSP, payload: hdrfw_srch_fll_acq_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000302, "HDR_FW_SRCH_FLL_ACQ_RSP", NULL ),

  /* HDR_FW_SRCH_SLAM_RSP, payload: hdrfw_srch_slam_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000303, "HDR_FW_SRCH_SLAM_RSP", NULL ),

  /* HDR_FW_SRCH_RESET_DRC_FILT_RSP, payload: hdrfw_srch_reset_drc_filt_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000304, "HDR_FW_SRCH_RESET_DRC_FILT_RSP", NULL ),

  /* HDR_FW_SRCH_FLL_ACCUM_LD_RSP, payload: hdrfw_srch_fll_accum_ld_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000305, "HDR_FW_SRCH_FLL_ACCUM_LD_RSP", NULL ),

  /* HDR_FW_SRCH_EQ_CONFIG_RSP, payload: hdrfw_srch_eq_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000306, "HDR_FW_SRCH_EQ_CONFIG_RSP", NULL ),

  /* HDR_FW_CANCEL_ACTIVE_PACKET_RSP, payload: hdrfw_cancel_active_packet_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000307, "HDR_FW_CANCEL_ACTIVE_PACKET_RSP", NULL ),

  /* HDR_FW_RX_START_RSP, payload: cfw_rx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000308, "HDR_FW_RX_START_RSP", NULL ),

  /* HDR_FW_RX_STOP_RSP, payload: cfw_rx_stop_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000309, "HDR_FW_RX_STOP_RSP", NULL ),

  /* HDR_FW_INTELLICEIVER_UPDATE_RSP, payload: cfw_intelliceiver_update_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030a, "HDR_FW_INTELLICEIVER_UPDATE_RSP", NULL ),

  /* HDR_FW_HDET_RESPONSE_RSP, payload: cfw_tx_hdet_response_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030b, "HDR_FW_HDET_RESPONSE_RSP", NULL ),

  /* HDR_FW_PILOT_MEAS_CFG_RSP, payload: hdrfw_pilot_meas_cfg_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030c, "HDR_FW_PILOT_MEAS_CFG_RSP", NULL ),

  /* HDR_FW_PILOT_MEAS_STOP_STREAM_RSP, payload: hdrfw_pilot_meas_stop_stream_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030d, "HDR_FW_PILOT_MEAS_STOP_STREAM_RSP", NULL ),

  /* HDR_FW_TX_START_RSP, payload: cfw_tx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030e, "HDR_FW_TX_START_RSP", NULL ),

  /* HDR_FW_TX_STOP_RSP, payload: cfw_tx_stop_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800030f, "HDR_FW_TX_STOP_RSP", NULL ),

  /* HDR_FW_ASP_UPDATE_RSP, payload: hdrfw_asp_update_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000310, "HDR_FW_ASP_UPDATE_RSP", NULL ),

  /* HDR_FW_TX_PA_CTL_RSP, payload: hdrfw_tx_pa_ctl_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000311, "HDR_FW_TX_PA_CTL_RSP", NULL ),

  /* HDR_FW_IRAT_RX_START_RSP, payload: cfw_irat_rx_start_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000312, "HDR_FW_IRAT_RX_START_RSP", NULL ),

  /* HDR_FW_PA_PARAMS_CONFIG_RSP, payload: cfw_pa_params_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000313, "HDR_FW_PA_PARAMS_CONFIG_RSP", NULL ),

  /* HDR_FW_XPT_TRANS_COMPL_RSP, payload: cfw_xpt_trans_compl_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000314, "HDR_FW_XPT_TRANS_COMPL_RSP", NULL ),

  /* HDR_FW_XPT_CONFIG_RSP, payload: cfw_xpt_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000315, "HDR_FW_XPT_CONFIG_RSP", NULL ),

  /* HDR_FW_SRCH_FING_ASSIGN_SLAM_RSP, payload: hdrfw_srch_fing_assign_slam_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000316, "HDR_FW_SRCH_FING_ASSIGN_SLAM_RSP", NULL ),

  /* HDR_FW_ASDIV_PRI_CHANGE_RSP, payload: hdrfw_asdiv_pri_change_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000317, "HDR_FW_ASDIV_PRI_CHANGE_RSP", NULL ),

  /* HDR_FW_DECOB_STATUS_IND, payload: hdrfw_decob_status_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000401, "HDR_FW_DECOB_STATUS_IND", NULL ),

  /* HDR_FW_BEST_ASP_CHANGE_IND, payload: hdrfw_best_asp_change_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000402, "HDR_FW_BEST_ASP_CHANGE_IND", NULL ),

  /* HDR_FW_RMAC0_FRAME_IND, payload: hdrfw_rmac0_frame_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000403, "HDR_FW_RMAC0_FRAME_IND", NULL ),

  /* HDR_FW_SCC_PREAMBLE_IND, payload: hdrfw_scc_preamble_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000404, "HDR_FW_SCC_PREAMBLE_IND", NULL ),

  /* HDR_FW_DRC_SUPERVISION_TIMEOUT_IND, payload: hdrfw_drc_supervision_timeout_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000405, "HDR_FW_DRC_SUPERVISION_TIMEOUT_IND", NULL ),

  /* HDR_FW_DRC_SUPERVISION_RESTART_IND, payload: hdrfw_drc_supervision_restart_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000406, "HDR_FW_DRC_SUPERVISION_RESTART_IND", NULL ),

  /* HDR_FW_LOG_BUFFER_IND, payload: hdrfw_log_buffer_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000407, "HDR_FW_LOG_BUFFER_IND", NULL ),

  /* HDR_FW_TIMERS_IND, payload: hdrfw_timers_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000408, "HDR_FW_TIMERS_IND", NULL ),

  /* HDR_FW_VI_CTL_IND, payload: hdrfw_vi_ctl_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08000409, "HDR_FW_VI_CTL_IND", NULL ),

  /* HDR_FW_RXAGC_TRACK_IND, payload: hdrfw_rxagc_track_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800040a, "HDR_FW_RXAGC_TRACK_IND", NULL ),

  /* HDR_FW_RESET_REQ_IND, payload: hdrfw_reset_req_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800040b, "HDR_FW_RESET_REQ_IND", NULL ),

  /* HDR_FW_SCC_DECODE_IND, payload: hdrfw_scc_decode_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800040c, "HDR_FW_SCC_DECODE_IND", NULL ),

  /* HDR_FW_FLEXCONN_REPOINT_IND, payload: hdrfw_tx_flexconn_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800040d, "HDR_FW_FLEXCONN_REPOINT_IND", NULL ),

  /* HDR_FW_RXAGC_TRACK_QTA_IND, payload: hdrfw_rxagc_track_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0800040e, "HDR_FW_RXAGC_TRACK_QTA_IND", NULL ),

  /* HDR_FW_RX_RTC_TMR, payload: NULL */
  MSGR_UMID_TABLE_ENTRY ( 0x08000900, "HDR_FW_RX_RTC_TMR", NULL ),

  /* HDR_SRCH_TRAN_SYS_TIME_REQ, payload: hdrsrch_tran_sys_time_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200200, "HDR_SRCH_TRAN_SYS_TIME_REQ", NULL ),

  /* HDR_SRCH_LTE_PILOT_MEAS_REQ, payload: hdrsrch_lte_pilot_meas_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200201, "HDR_SRCH_LTE_PILOT_MEAS_REQ", NULL ),

  /* HDR_SRCH_LTE_PILOT_MEAS_ABORT_REQ, payload: hdrsrch_lte_pilot_meas_abort_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200202, "HDR_SRCH_LTE_PILOT_MEAS_ABORT_REQ", NULL ),

  /* HDR_SRCH_LTE_PILOT_MEAS_RSP, payload: hdrsrch_lte_pilot_meas_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200300, "HDR_SRCH_LTE_PILOT_MEAS_RSP", NULL ),

  /* HDR_SRCH_LTE_PILOT_MEAS_ABORT_RSP, payload: hdrsrch_lte_pilot_meas_abort_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200301, "HDR_SRCH_LTE_PILOT_MEAS_ABORT_RSP", NULL ),

  /* HDR_SRCH_RF_STATUS_IND, payload: hdrsrch_rf_status_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08200400, "HDR_SRCH_RF_STATUS_IND", NULL ),

  /* HDR_HIT_PIL_ACQ_CMD, payload: hdrhit_pil_acq_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270100, "HDR_HIT_PIL_ACQ_CMD", NULL ),

  /* HDR_HIT_SYNC_CMD, payload: hdrhit_sync_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270101, "HDR_HIT_SYNC_CMD", NULL ),

  /* HDR_HIT_DEMOD_FTC_CMD, payload: hdrhit_demod_ftc_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270102, "HDR_HIT_DEMOD_FTC_CMD", NULL ),

  /* HDR_HIT_MOD_ACC_CMD, payload: hdrhit_mod_acc_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270103, "HDR_HIT_MOD_ACC_CMD", NULL ),

  /* HDR_HIT_MOD_RTC_CMD, payload: hdrhit_mod_rtc_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270104, "HDR_HIT_MOD_RTC_CMD", NULL ),

  /* HDR_HIT_DEASSIGN_CMD, payload: hdrhit_deassign_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270105, "HDR_HIT_DEASSIGN_CMD", NULL ),

  /* HDR_HIT_IDLE_STATE_CMD, payload: hdrhit_idle_state_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270106, "HDR_HIT_IDLE_STATE_CMD", NULL ),

  /* HDR_HIT_DEMOD_FW_LINK_REV0_CMD, payload: hdrhit_demod_fw_link_rev0_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270107, "HDR_HIT_DEMOD_FW_LINK_REV0_CMD", NULL ),

  /* HDR_HIT_RELA_MAC_CONFIG_CMD, payload: hdrhit_rela_mac_config_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270108, "HDR_HIT_RELA_MAC_CONFIG_CMD", NULL ),

  /* HDR_HIT_DEMOD_FW_LINK_REVA_CMD, payload: hdrhit_demod_fw_link_reva_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270109, "HDR_HIT_DEMOD_FW_LINK_REVA_CMD", NULL ),

  /* HDR_HIT_MOD_ACC_REVA_CMD, payload: hdrhit_mod_acc_reva_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010a, "HDR_HIT_MOD_ACC_REVA_CMD", NULL ),

  /* HDR_HIT_MOD_RTC_REVA_CMD, payload: hdrhit_mod_rtc_reva_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010b, "HDR_HIT_MOD_RTC_REVA_CMD", NULL ),

  /* HDR_HIT_MOD_RTC_REVB_CMD, payload: hdrhit_mod_rtc_revb_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010c, "HDR_HIT_MOD_RTC_REVB_CMD", NULL ),

  /* HDR_HIT_CONFIG_RTC_REVB_CMD, payload: hdrhit_config_rtc_revb_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010d, "HDR_HIT_CONFIG_RTC_REVB_CMD", NULL ),

  /* HDR_HIT_HARD_HANDOFF_CMD, payload: hdrhit_hard_handoff_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010e, "HDR_HIT_HARD_HANDOFF_CMD", NULL ),

  /* HDR_HIT_GET_FWD_LNK_STATS_CMD, payload: hdrhit_get_fwd_lnk_stats_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827010f, "HDR_HIT_GET_FWD_LNK_STATS_CMD", NULL ),

  /* HDR_HIT_RESET_FWD_LNK_STATS_CMD, payload: hdrhit_reset_fwd_lnk_stats_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270110, "HDR_HIT_RESET_FWD_LNK_STATS_CMD", NULL ),

  /* HDR_HIT_MODULATOR_CTL_CMD, payload: hdrhit_modulator_ctl_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270111, "HDR_HIT_MODULATOR_CTL_CMD", NULL ),

  /* HDR_HIT_CONFIG_FUNNEL_MODE_CMD, payload: hdrhit_config_funnel_mode_cmd_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270112, "HDR_HIT_CONFIG_FUNNEL_MODE_CMD", NULL ),

  /* HDR_HIT_PIL_ACQ_RSP, payload: hdrhit_pil_acq_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270300, "HDR_HIT_PIL_ACQ_RSP", NULL ),

  /* HDR_HIT_SYNC_RSP, payload: hdrhit_sync_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270301, "HDR_HIT_SYNC_RSP", NULL ),

  /* HDR_HIT_DEMOD_FTC_RSP, payload: hdrhit_demod_ftc_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270302, "HDR_HIT_DEMOD_FTC_RSP", NULL ),

  /* HDR_HIT_MOD_ACC_RSP, payload: hdrhit_mod_acc_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270303, "HDR_HIT_MOD_ACC_RSP", NULL ),

  /* HDR_HIT_MOD_RTC_RSP, payload: hdrhit_mod_rtc_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270304, "HDR_HIT_MOD_RTC_RSP", NULL ),

  /* HDR_HIT_DEASSIGN_RSP, payload: hdrhit_deassign_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270305, "HDR_HIT_DEASSIGN_RSP", NULL ),

  /* HDR_HIT_IDLE_STATE_RSP, payload: hdrhit_idle_state_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270306, "HDR_HIT_IDLE_STATE_RSP", NULL ),

  /* HDR_HIT_DEMOD_FW_LINK_REV0_RSP, payload: hdrhit_demod_fw_link_rev0_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270307, "HDR_HIT_DEMOD_FW_LINK_REV0_RSP", NULL ),

  /* HDR_HIT_RELA_MAC_CONFIG_RSP, payload: hdrhit_rela_mac_config_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270308, "HDR_HIT_RELA_MAC_CONFIG_RSP", NULL ),

  /* HDR_HIT_DEMOD_FW_LINK_REVA_RSP, payload: hdrhit_demod_fw_link_reva_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270309, "HDR_HIT_DEMOD_FW_LINK_REVA_RSP", NULL ),

  /* HDR_HIT_MOD_ACC_REVA_RSP, payload: hdrhit_mod_acc_reva_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030a, "HDR_HIT_MOD_ACC_REVA_RSP", NULL ),

  /* HDR_HIT_MOD_RTC_REVA_RSP, payload: hdrhit_mod_rtc_reva_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030b, "HDR_HIT_MOD_RTC_REVA_RSP", NULL ),

  /* HDR_HIT_MOD_RTC_REVB_RSP, payload: hdrhit_mod_rtc_revb_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030c, "HDR_HIT_MOD_RTC_REVB_RSP", NULL ),

  /* HDR_HIT_CONFIG_RTC_REVB_RSP, payload: hdrhit_config_rtc_revb_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030d, "HDR_HIT_CONFIG_RTC_REVB_RSP", NULL ),

  /* HDR_HIT_HARD_HANDOFF_RSP, payload: hdrhit_hard_handoff_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030e, "HDR_HIT_HARD_HANDOFF_RSP", NULL ),

  /* HDR_HIT_GET_FWD_LNK_STATS_RSP, payload: hdrhit_get_fwd_lnk_stats_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0827030f, "HDR_HIT_GET_FWD_LNK_STATS_RSP", NULL ),

  /* HDR_HIT_RESET_FWD_LNK_STATS_RSP, payload: hdrhit_reset_fwd_lnk_stats_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270310, "HDR_HIT_RESET_FWD_LNK_STATS_RSP", NULL ),

  /* HDR_HIT_MODULATOR_CTL_RSP, payload: hdrhit_modulator_ctl_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270311, "HDR_HIT_MODULATOR_CTL_RSP", NULL ),

  /* HDR_HIT_CONFIG_FUNNEL_MODE_RSP, payload: hdrhit_config_funnel_mode_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08270312, "HDR_HIT_CONFIG_FUNNEL_MODE_RSP", NULL ),

  /* HDR_CP_LOOPBACK_SPR, payload: msgr_spr_loopback_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x08280000, "HDR_CP_LOOPBACK_SPR", NULL ),

  /* HDR_CP_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_struct_type */
  MSGR_UMID_TABLE_ENTRY ( 0x08280001, "HDR_CP_LOOPBACK_REPLY_SPR", NULL ),

  /* HDR_CP_LTE_REDIR_REQ, payload: hdrcp_lte_redir_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280201, "HDR_CP_LTE_REDIR_REQ", NULL ),

  /* HDR_CP_LTE_ABORT_REDIR_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280202, "HDR_CP_LTE_ABORT_REDIR_REQ", NULL ),

  /* HDR_CP_LTE_1X_HRPD_CAPABILITIES_REQ, payload: hdrcp_lte_1x_hrpd_capabilities_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280203, "HDR_CP_LTE_1X_HRPD_CAPABILITIES_REQ", NULL ),

  /* HDR_CP_LTE_RESEL_REQ, payload: hdrcp_lte_resel_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280204, "HDR_CP_LTE_RESEL_REQ", NULL ),

  /* HDR_CP_LTE_ABORT_RESEL_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280205, "HDR_CP_LTE_ABORT_RESEL_REQ", NULL ),

  /* HDR_CP_LTE_REVERSE_TIMING_TRANSFER_REQ, payload: hdrcp_lte_rev_tt_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280206, "HDR_CP_LTE_REVERSE_TIMING_TRANSFER_REQ", NULL ),

  /* HDR_CP_LTE_ABORT_REVERSE_TIMING_TRANSFER_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280207, "HDR_CP_LTE_ABORT_REVERSE_TIMING_TRANSFER_REQ", NULL ),

  /* HDR_CP_LTE_ACTIVE_HO_REQ, payload: hdrcp_lte_active_ho_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280208, "HDR_CP_LTE_ACTIVE_HO_REQ", NULL ),

  /* HDR_CP_LTE_ABORT_ACTIVE_HO_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280209, "HDR_CP_LTE_ABORT_ACTIVE_HO_REQ", NULL ),

  /* HDR_CP_PROT_ACT_REQ, payload: hdrcp_activate_protocol_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020a, "HDR_CP_PROT_ACT_REQ", NULL ),

  /* HDR_CP_PROT_DEACT_REQ, payload: hdrcp_deactivate_protocol_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020b, "HDR_CP_PROT_DEACT_REQ", NULL ),

  /* HDR_CP_PROT_PH_STAT_CHGD_REQ, payload: hdrcp_ph_status_chgd_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020c, "HDR_CP_PROT_PH_STAT_CHGD_REQ", NULL ),

  /* HDR_CP_PROT_GEN_REQ, payload: hdrcp_generic_prot_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020d, "HDR_CP_PROT_GEN_REQ", NULL ),

  /* HDR_CP_CALL_ORIGINATION_REQ, payload: hdrcp_call_origination_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020e, "HDR_CP_CALL_ORIGINATION_REQ", NULL ),

  /* HDR_CP_CALL_END_REQ, payload: hdrcp_call_end_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828020f, "HDR_CP_CALL_END_REQ", NULL ),

  /* HDR_CP_IDLE_PGSLOT_CHANGED_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280210, "HDR_CP_IDLE_PGSLOT_CHANGED_REQ", NULL ),

  /* HDR_CP_UNLOCK_HDR_RF_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280211, "HDR_CP_UNLOCK_HDR_RF_REQ", NULL ),

  /* HDR_CP_MEAS_MODE_ON_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280212, "HDR_CP_MEAS_MODE_ON_REQ", NULL ),

  /* HDR_CP_MEAS_MODE_OFF_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280213, "HDR_CP_MEAS_MODE_OFF_REQ", NULL ),

  /* HDR_CP_MEAS_REQ_REQ, payload: hdrcp_irat_meas_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280214, "HDR_CP_MEAS_REQ_REQ", NULL ),

  /* HDR_CP_SEND_DATA_OVER_SIGNALING_REQ, payload: hdrcp_send_data_over_signaling_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280215, "HDR_CP_SEND_DATA_OVER_SIGNALING_REQ", NULL ),

  /* HDR_CP_TUNNEL_MSG_REQ, payload: hdrcp_tunnel_msg_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280216, "HDR_CP_TUNNEL_MSG_REQ", NULL ),

  /* HDR_CP_CSNA_MSG_CONFIG_REQ, payload: hdrcp_csna_msg_config_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280217, "HDR_CP_CSNA_MSG_CONFIG_REQ", NULL ),

  /* HDR_CP_SET_FTD_STATS_MASK_REQ, payload: hdrcp_set_ftd_stats_mask_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280218, "HDR_CP_SET_FTD_STATS_MASK_REQ", NULL ),

  /* HDR_CP_BCMCS_UPDATE_FLOW_REQ, payload: hdrcp_bcmcs_update_flow_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280219, "HDR_CP_BCMCS_UPDATE_FLOW_REQ", NULL ),

  /* HDR_CP_BCMCS_LEGACY_UPDATE_FLOW_REQ, payload: hdrcp_bcmcs_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021a, "HDR_CP_BCMCS_LEGACY_UPDATE_FLOW_REQ", NULL ),

  /* HDR_CP_BCMCS_DISCONTINUE_BROADCAST_REQ, payload: hdrcp_bcmcs_discontinue_broadcast_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021b, "HDR_CP_BCMCS_DISCONTINUE_BROADCAST_REQ", NULL ),

  /* HDR_CP_BCMCS_LEGACY_DISCONTINUE_BROADCAST_REQ, payload: hdrcp_bcmcs_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021c, "HDR_CP_BCMCS_LEGACY_DISCONTINUE_BROADCAST_REQ", NULL ),

  /* HDR_CP_BCMCS_SEND_REGISTRATION_REQ, payload: hdrcp_bcmcs_send_registration_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021d, "HDR_CP_BCMCS_SEND_REGISTRATION_REQ", NULL ),

  /* HDR_CP_BCMCS_BOM_CACHING_SETUP_REQ, payload: hdrcp_bcmcs_bom_caching_setup_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021e, "HDR_CP_BCMCS_BOM_CACHING_SETUP_REQ", NULL ),

  /* HDR_CP_BCMCS_ENABLE_REG_HANDOFF_REQ, payload: hdrcp_bcmcs_enable_reg_handoff_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828021f, "HDR_CP_BCMCS_ENABLE_REG_HANDOFF_REQ", NULL ),

  /* HDR_CP_QCHAT_OPT_REQ, payload: hdrcp_qchat_opt_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280220, "HDR_CP_QCHAT_OPT_REQ", NULL ),

  /* HDR_CP_REDIR_PROTOCOL_CNF_REQ, payload: hdrcp_redir_protocol_cnf_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280221, "HDR_CP_REDIR_PROTOCOL_CNF_REQ", NULL ),

  /* HDR_CP_ACTIVATE_PROTOCOL_CNF_REQ, payload: hdrcp_activate_protocol_cnf_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280222, "HDR_CP_ACTIVATE_PROTOCOL_CNF_REQ", NULL ),

  /* HDR_CP_STANDBY_PREF_CHGD_REQ, payload: hdrcp_standby_pref_chgd_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280223, "HDR_CP_STANDBY_PREF_CHGD_REQ", NULL ),

  /* HDR_CP_CM_ABORT_LTE_RESEL_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280224, "HDR_CP_CM_ABORT_LTE_RESEL_REQ", NULL ),

  /* HDR_CP_LTE_GET_CGI_REQ, payload: hdrcp_lte_get_cgi_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280225, "HDR_CP_LTE_GET_CGI_REQ", NULL ),

  /* HDR_CP_LTE_ABORT_CGI_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280226, "HDR_CP_LTE_ABORT_CGI_REQ", NULL ),

  /* HDR_CP_CM_LTE_FPLMN_UPDATE_REQ, payload: hdrcp_cm_lte_fplmn_update_req_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280227, "HDR_CP_CM_LTE_FPLMN_UPDATE_REQ", NULL ),

  /* HDR_CP_LTE_DEPRI_FREQ_REQ, payload: hdrcp_lte_depri_freq_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280228, "HDR_CP_LTE_DEPRI_FREQ_REQ", NULL ),

  /* HDR_CP_LTE_REDIR_FAILED_RSP, payload: hdrcp_lte_redir_failed_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280301, "HDR_CP_LTE_REDIR_FAILED_RSP", NULL ),

  /* HDR_CP_LTE_REDIR_ABORT_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280302, "HDR_CP_LTE_REDIR_ABORT_RSP", NULL ),

  /* HDR_CP_LTE_1X_HRPD_CAPABILITIES_RSP, payload: hdrcp_lte_1x_hrpd_capabilities_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280303, "HDR_CP_LTE_1X_HRPD_CAPABILITIES_RSP", NULL ),

  /* HDR_CP_LTE_RESEL_FAILED_RSP, payload: hdrcp_lte_resel_failed_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280304, "HDR_CP_LTE_RESEL_FAILED_RSP", NULL ),

  /* HDR_CP_LTE_RESEL_ABORT_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280305, "HDR_CP_LTE_RESEL_ABORT_RSP", NULL ),

  /* HDR_CP_LTE_REVERSE_TIMING_TRANSFER_RSP, payload: hdrcp_lte_rev_tt_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280306, "HDR_CP_LTE_REVERSE_TIMING_TRANSFER_RSP", NULL ),

  /* HDR_CP_LTE_REVERSE_TIMING_TRANSFER_ABORT_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280307, "HDR_CP_LTE_REVERSE_TIMING_TRANSFER_ABORT_RSP", NULL ),

  /* HDR_CP_LTE_ACTIVE_HO_FAILED_RSP, payload: hdrcp_lte_active_ho_fail_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280309, "HDR_CP_LTE_ACTIVE_HO_FAILED_RSP", NULL ),

  /* HDR_CP_LTE_ACTIVE_HO_ABORT_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0828030a, "HDR_CP_LTE_ACTIVE_HO_ABORT_RSP", NULL ),

  /* HDR_CP_LTE_GET_CGI_RSP, payload: hdrcp_lte_get_cgi_rsp_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0828030b, "HDR_CP_LTE_GET_CGI_RSP", NULL ),

  /* HDR_CP_HRPD_PREREG_STATUS_IND, payload: hdrcp_hrpd_prereg_status_ind_msg_t */
  MSGR_UMID_TABLE_ENTRY ( 0x08280401, "HDR_CP_HRPD_PREREG_STATUS_IND", NULL ),

  /* HDR_CP_RESELECT_EXIT_HDR_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x08280408, "HDR_CP_RESELECT_EXIT_HDR_IND", NULL ),

  /* GERAN_FW_ASYNC_CMD, payload: GfwHostAsyncCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000100, "GERAN_FW_ASYNC_CMD", NULL ),

  /* GERAN_FW_APP_MODE_CONFIG_CMD, payload: GfwHostAppModeConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000101, "GERAN_FW_APP_MODE_CONFIG_CMD", NULL ),

  /* GERAN_FW_RFM_ENTER_MODE_CMD, payload: GfwHostRfmEnterModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000103, "GERAN_FW_RFM_ENTER_MODE_CMD", NULL ),

  /* GERAN_FW_SLEEP_CMD, payload: GfwHostSleepCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000106, "GERAN_FW_SLEEP_CMD", NULL ),

  /* GERAN_FW_SLEEP_READY_CMD, payload: GfwHostAsyncSleepReadyCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000108, "GERAN_FW_SLEEP_READY_CMD", NULL ),

  /* GERAN_FW_ASYNC_IMMEDIATE_CMD, payload: GfwHostAsyncImmediateCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000109, "GERAN_FW_ASYNC_IMMEDIATE_CMD", NULL ),

  /* GERAN_FW_RFM_SET_TX_BAND_CMD, payload: GfwHostRfmSetTxBandCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0900010b, "GERAN_FW_RFM_SET_TX_BAND_CMD", NULL ),

  /* GERAN_FW_RFM_THERM_READ_CMD, payload: GfwHostRfmThermReadCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0900010d, "GERAN_FW_RFM_THERM_READ_CMD", NULL ),

  /* GERAN_FW_ASYNC_EXIT_CMD, payload: GfwHostAsyncExitCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000110, "GERAN_FW_ASYNC_EXIT_CMD", NULL ),

  /* GERAN_FW_RFM_EXIT_MODE_CMD, payload: GfwHostRfmExitModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000111, "GERAN_FW_RFM_EXIT_MODE_CMD", NULL ),

  /* GERAN_FW_DEVICE_MODE_CMD, payload: GfwHostAsyncDeviceModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000113, "GERAN_FW_DEVICE_MODE_CMD", NULL ),

  /* GERAN_FW_WTR_CFG_CMD, payload: GfwHostWtrConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000114, "GERAN_FW_WTR_CFG_CMD", NULL ),

  /* GERAN_FW_WLAN_CXM_POLICY_CMD, payload: GfwWlanCxmConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09000116, "GERAN_FW_WLAN_CXM_POLICY_CMD", NULL ),

  /* GERAN_FW_APP_MODE_CONFIG_RSP, payload: GfwHostAppModeConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000302, "GERAN_FW_APP_MODE_CONFIG_RSP", NULL ),

  /* GERAN_FW_RFM_ENTER_MODE_RSP, payload: GfwHostRfmEnterModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000304, "GERAN_FW_RFM_ENTER_MODE_RSP", NULL ),

  /* GERAN_FW_ASYNC_RSP, payload: GfwHostAsyncRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000305, "GERAN_FW_ASYNC_RSP", NULL ),

  /* GERAN_FW_SLEEP_RSP, payload: GfwHostSleepRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000307, "GERAN_FW_SLEEP_RSP", NULL ),

  /* GERAN_FW_ASYNC_IMMEDIATE_RSP, payload: GfwHostAsyncImmediateRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0900030a, "GERAN_FW_ASYNC_IMMEDIATE_RSP", NULL ),

  /* GERAN_FW_RFM_SET_TX_BAND_RSP, payload: GfwHostRfmSetTxBandRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0900030c, "GERAN_FW_RFM_SET_TX_BAND_RSP", NULL ),

  /* GERAN_FW_RFM_THERM_READ_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0900030e, "GERAN_FW_RFM_THERM_READ_RSP", NULL ),

  /* GERAN_FW_RFM_ASD_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0900030f, "GERAN_FW_RFM_ASD_RSP", NULL ),

  /* GERAN_FW_RFM_EXIT_MODE_RSP, payload: GfwHostRfmExitModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000312, "GERAN_FW_RFM_EXIT_MODE_RSP", NULL ),

  /* GERAN_FW_WTR_CFG_RSP, payload: GfwHostWtrConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000315, "GERAN_FW_WTR_CFG_RSP", NULL ),

  /* GERAN_FW_WLAN_CXM_POLICY_RSP, payload: GfwWlanCxmConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000317, "GERAN_FW_WLAN_CXM_POLICY_RSP", NULL ),

  /* GERAN_FW_INVALID_RFLM_TX_INDEX_RSP, payload: GfwHostInvalidTxRflmIndexRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09000318, "GERAN_FW_INVALID_RFLM_TX_INDEX_RSP", NULL ),

  /* GERAN_FW2_ASYNC_CMD, payload: GfwHostAsyncCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010100, "GERAN_FW2_ASYNC_CMD", NULL ),

  /* GERAN_FW2_APP_MODE_CONFIG_CMD, payload: GfwHostAppModeConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010101, "GERAN_FW2_APP_MODE_CONFIG_CMD", NULL ),

  /* GERAN_FW2_RFM_ENTER_MODE_CMD, payload: GfwHostRfmEnterModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010103, "GERAN_FW2_RFM_ENTER_MODE_CMD", NULL ),

  /* GERAN_FW2_SLEEP_CMD, payload: GfwHostSleepCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010106, "GERAN_FW2_SLEEP_CMD", NULL ),

  /* GERAN_FW2_SLEEP_READY_CMD, payload: GfwHostAsyncSleepReadyCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010108, "GERAN_FW2_SLEEP_READY_CMD", NULL ),

  /* GERAN_FW2_ASYNC_IMMEDIATE_CMD, payload: GfwHostAsyncImmediateCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010109, "GERAN_FW2_ASYNC_IMMEDIATE_CMD", NULL ),

  /* GERAN_FW2_RFM_SET_TX_BAND_CMD, payload: GfwHostRfmSetTxBandCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0901010b, "GERAN_FW2_RFM_SET_TX_BAND_CMD", NULL ),

  /* GERAN_FW2_RFM_THERM_READ_CMD, payload: GfwHostRfmThermReadCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0901010d, "GERAN_FW2_RFM_THERM_READ_CMD", NULL ),

  /* GERAN_FW2_ASYNC_EXIT_CMD, payload: GfwHostAsyncExitCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010110, "GERAN_FW2_ASYNC_EXIT_CMD", NULL ),

  /* GERAN_FW2_RFM_EXIT_MODE_CMD, payload: GfwHostRfmExitModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010111, "GERAN_FW2_RFM_EXIT_MODE_CMD", NULL ),

  /* GERAN_FW2_DEVICE_MODE_CMD, payload: GfwHostAsyncDeviceModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010113, "GERAN_FW2_DEVICE_MODE_CMD", NULL ),

  /* GERAN_FW2_WTR_CFG_CMD, payload: GfwHostWtrConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010114, "GERAN_FW2_WTR_CFG_CMD", NULL ),

  /* GERAN_FW2_WLAN_CXM_POLICY_CMD, payload: GfwWlanCxmConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09010116, "GERAN_FW2_WLAN_CXM_POLICY_CMD", NULL ),

  /* GERAN_FW2_APP_MODE_CONFIG_RSP, payload: GfwHostAppModeConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010302, "GERAN_FW2_APP_MODE_CONFIG_RSP", NULL ),

  /* GERAN_FW2_RFM_ENTER_MODE_RSP, payload: GfwHostRfmEnterModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010304, "GERAN_FW2_RFM_ENTER_MODE_RSP", NULL ),

  /* GERAN_FW2_ASYNC_RSP, payload: GfwHostAsyncRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010305, "GERAN_FW2_ASYNC_RSP", NULL ),

  /* GERAN_FW2_SLEEP_RSP, payload: GfwHostSleepRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010307, "GERAN_FW2_SLEEP_RSP", NULL ),

  /* GERAN_FW2_ASYNC_IMMEDIATE_RSP, payload: GfwHostAsyncImmediateRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0901030a, "GERAN_FW2_ASYNC_IMMEDIATE_RSP", NULL ),

  /* GERAN_FW2_RFM_SET_TX_BAND_RSP, payload: GfwHostRfmSetTxBandRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0901030c, "GERAN_FW2_RFM_SET_TX_BAND_RSP", NULL ),

  /* GERAN_FW2_RFM_THERM_READ_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0901030e, "GERAN_FW2_RFM_THERM_READ_RSP", NULL ),

  /* GERAN_FW2_RFM_ASD_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0901030f, "GERAN_FW2_RFM_ASD_RSP", NULL ),

  /* GERAN_FW2_RFM_EXIT_MODE_RSP, payload: GfwHostRfmExitModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010312, "GERAN_FW2_RFM_EXIT_MODE_RSP", NULL ),

  /* GERAN_FW2_WTR_CFG_RSP, payload: GfwHostWtrConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010315, "GERAN_FW2_WTR_CFG_RSP", NULL ),

  /* GERAN_FW2_WLAN_CXM_POLICY_RSP, payload: GfwWlanCxmConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010317, "GERAN_FW2_WLAN_CXM_POLICY_RSP", NULL ),

  /* GERAN_FW2_INVALID_RFLM_TX_INDEX_RSP, payload: GfwHostInvalidTxRflmIndexRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09010318, "GERAN_FW2_INVALID_RFLM_TX_INDEX_RSP", NULL ),

  /* GERAN_GRR_LTE_CS_CAPABILITIES_REQ, payload: geran_grr_lte_cs_capabilities_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020201, "GERAN_GRR_LTE_CS_CAPABILITIES_REQ", NULL ),

  /* GERAN_GRR_LTE_PS_CAPABILITIES_REQ, payload: geran_grr_lte_ps_capabilities_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020202, "GERAN_GRR_LTE_PS_CAPABILITIES_REQ", NULL ),

  /* GERAN_GRR_LTE_RESEL_REQ, payload: geran_grr_lte_resel_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020203, "GERAN_GRR_LTE_RESEL_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_RESEL_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020204, "GERAN_GRR_LTE_ABORT_RESEL_REQ", NULL ),

  /* GERAN_GRR_LTE_REDIR_REQ, payload: geran_grr_lte_redir_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020205, "GERAN_GRR_LTE_REDIR_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_REDIR_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020206, "GERAN_GRR_LTE_ABORT_REDIR_REQ", NULL ),

  /* GERAN_GRR_LTE_CCO_REQ, payload: geran_grr_lte_cco_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020207, "GERAN_GRR_LTE_CCO_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_CCO_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020208, "GERAN_GRR_LTE_ABORT_CCO_REQ", NULL ),

  /* GERAN_GRR_LTE_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x09020209, "GERAN_GRR_LTE_PLMN_SRCH_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020a, "GERAN_GRR_LTE_ABORT_PLMN_SRCH_REQ", NULL ),

  /* GERAN_GRR_LTE_DEDICATED_PRIORITIES_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020b, "GERAN_GRR_LTE_DEDICATED_PRIORITIES_REQ", NULL ),

  /* GERAN_GRR_LTE_GET_CGI_REQ, payload: geran_grr_lte_get_cgi_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020c, "GERAN_GRR_LTE_GET_CGI_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_CGI_REQ, payload: geran_grr_lte_abort_cgi_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020d, "GERAN_GRR_LTE_ABORT_CGI_REQ", NULL ),

  /* GERAN_GRR_LTE_HO_REQ, payload: geran_grr_lte_ho_req_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020e, "GERAN_GRR_LTE_HO_REQ", NULL ),

  /* GERAN_GRR_LTE_ABORT_HO_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0902020f, "GERAN_GRR_LTE_ABORT_HO_REQ", NULL ),

  /* GERAN_GRR_LTE_SGLTE_SERVING_MEAS_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020210, "GERAN_GRR_LTE_SGLTE_SERVING_MEAS_REQ", NULL ),

  /* GERAN_GRR_TDS_SGLTE_SERVING_MEAS_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020211, "GERAN_GRR_TDS_SGLTE_SERVING_MEAS_REQ", NULL ),

  /* GERAN_GRR_LTE_SGLTE_NEIGHBOR_MEAS_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020212, "GERAN_GRR_LTE_SGLTE_NEIGHBOR_MEAS_REQ", NULL ),

  /* GERAN_GRR_LTE_GET_PLMN_PRTL_RESULTS_REQ, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020213, "GERAN_GRR_LTE_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* GERAN_GRR_LTE_CS_CAPABILITIES_RSP, payload: geran_grr_lte_cs_capabilities_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020301, "GERAN_GRR_LTE_CS_CAPABILITIES_RSP", NULL ),

  /* GERAN_GRR_LTE_PS_CAPABILITIES_RSP, payload: geran_grr_lte_ps_capabilities_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020302, "GERAN_GRR_LTE_PS_CAPABILITIES_RSP", NULL ),

  /* GERAN_GRR_LTE_RESEL_FAILED_RSP, payload: geran_grr_lte_resel_failed_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020303, "GERAN_GRR_LTE_RESEL_FAILED_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_RESEL_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020304, "GERAN_GRR_LTE_ABORT_RESEL_RSP", NULL ),

  /* GERAN_GRR_LTE_REDIR_FAILED_RSP, payload: geran_grr_lte_redir_failed_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020305, "GERAN_GRR_LTE_REDIR_FAILED_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_REDIR_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020306, "GERAN_GRR_LTE_ABORT_REDIR_RSP", NULL ),

  /* GERAN_GRR_LTE_CCO_RSP, payload: geran_grr_lte_cco_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020307, "GERAN_GRR_LTE_CCO_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_CCO_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020308, "GERAN_GRR_LTE_ABORT_CCO_RSP", NULL ),

  /* GERAN_GRR_LTE_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x09020309, "GERAN_GRR_LTE_PLMN_SRCH_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030a, "GERAN_GRR_LTE_ABORT_PLMN_SRCH_RSP", NULL ),

  /* GERAN_GRR_LTE_DEDICATED_PRIORITIES_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030b, "GERAN_GRR_LTE_DEDICATED_PRIORITIES_RSP", NULL ),

  /* GERAN_GRR_LTE_GET_CGI_RSP, payload: geran_grr_lte_get_cgi_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030c, "GERAN_GRR_LTE_GET_CGI_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_CGI_RSP, payload: geran_grr_lte_abort_cgi_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030d, "GERAN_GRR_LTE_ABORT_CGI_RSP", NULL ),

  /* GERAN_GRR_LTE_HO_FAILED_RSP, payload: geran_grr_lte_ho_failed_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030e, "GERAN_GRR_LTE_HO_FAILED_RSP", NULL ),

  /* GERAN_GRR_LTE_ABORT_HO_RSP, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0902030f, "GERAN_GRR_LTE_ABORT_HO_RSP", NULL ),

  /* GERAN_GRR_LTE_SGLTE_SERVING_MEAS_RSP, payload: geran_grr_irat_sglte_serving_meas_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020310, "GERAN_GRR_LTE_SGLTE_SERVING_MEAS_RSP", NULL ),

  /* GERAN_GRR_TDS_SGLTE_SERVING_MEAS_RSP, payload: geran_grr_irat_sglte_serving_meas_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020311, "GERAN_GRR_TDS_SGLTE_SERVING_MEAS_RSP", NULL ),

  /* GERAN_GRR_LTE_SGLTE_NEIGHBOR_MEAS_RSP, payload: geran_grr_irat_sglte_neighbor_meas_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020312, "GERAN_GRR_LTE_SGLTE_NEIGHBOR_MEAS_RSP", NULL ),

  /* GERAN_GRR_CLEAR_DEDICATED_PRIORITIES_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020401, "GERAN_GRR_CLEAR_DEDICATED_PRIORITIES_IND", NULL ),

  /* GERAN_GRR_LTE_ABORT_CCO_FAILURE_PROCEDURE_IND, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x09020402, "GERAN_GRR_LTE_ABORT_CCO_FAILURE_PROCEDURE_IND", NULL ),

  /* GERAN_GRR_LTE_SUSPEND_PLMN_SRCH_IND, payload: lte_irat_suspend_plmn_srch_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x09020403, "GERAN_GRR_LTE_SUSPEND_PLMN_SRCH_IND", NULL ),

  /* GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND, payload: geran_grr_update_forbidden_bands_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09020404, "GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND", NULL ),

  /* GERAN_GL1_W2G_IRAT_CM_GSM_INIT_REQ, payload: x2g_irat_cm_gsm_init_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030203, "GERAN_GL1_W2G_IRAT_CM_GSM_INIT_REQ", NULL ),

  /* GERAN_GL1_L2G_IRAT_CM_GSM_INIT_REQ, payload: x2g_irat_cm_gsm_init_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030205, "GERAN_GL1_L2G_IRAT_CM_GSM_INIT_REQ", NULL ),

  /* GERAN_GL1_T2G_IRAT_CM_GSM_INIT_REQ, payload: x2g_irat_cm_gsm_init_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030207, "GERAN_GL1_T2G_IRAT_CM_GSM_INIT_REQ", NULL ),

  /* GERAN_GL1_ACQ_RSP, payload: gsm_irat_acquisition_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09030300, "GERAN_GL1_ACQ_RSP", NULL ),

  /* GERAN_GL1_SCH_RSP, payload: gsm_irat_sch_burst_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09030301, "GERAN_GL1_SCH_RSP", NULL ),

  /* GERAN_GL1_MEAS_RSP, payload: gsm_irat_meas_search_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x09030302, "GERAN_GL1_MEAS_RSP", NULL ),

  /* GERAN_GL1_W2G_IRAT_CM_GSM_INIT_RSP, payload: x2g_irat_cm_gsm_init_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030304, "GERAN_GL1_W2G_IRAT_CM_GSM_INIT_RSP", NULL ),

  /* GERAN_GL1_L2G_IRAT_CM_GSM_INIT_RSP, payload: x2g_irat_cm_gsm_init_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030306, "GERAN_GL1_L2G_IRAT_CM_GSM_INIT_RSP", NULL ),

  /* GERAN_GL1_T2G_IRAT_CM_GSM_INIT_RSP, payload: x2g_irat_cm_gsm_init_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x09030308, "GERAN_GL1_T2G_IRAT_CM_GSM_INIT_RSP", NULL ),

  /* GERAN_GL1_CFCM_CPU_MONITOR_IND, payload: cfcm_cmd_msg_type_s */
  MSGR_UMID_TABLE_ENTRY ( 0x09030409, "GERAN_GL1_CFCM_CPU_MONITOR_IND", NULL ),

  /* GERAN_FW3_ASYNC_CMD, payload: GfwHostAsyncCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040100, "GERAN_FW3_ASYNC_CMD", NULL ),

  /* GERAN_FW3_APP_MODE_CONFIG_CMD, payload: GfwHostAppModeConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040101, "GERAN_FW3_APP_MODE_CONFIG_CMD", NULL ),

  /* GERAN_FW3_RFM_ENTER_MODE_CMD, payload: GfwHostRfmEnterModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040103, "GERAN_FW3_RFM_ENTER_MODE_CMD", NULL ),

  /* GERAN_FW3_SLEEP_CMD, payload: GfwHostSleepCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040106, "GERAN_FW3_SLEEP_CMD", NULL ),

  /* GERAN_FW3_SLEEP_READY_CMD, payload: GfwHostAsyncSleepReadyCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040108, "GERAN_FW3_SLEEP_READY_CMD", NULL ),

  /* GERAN_FW3_ASYNC_IMMEDIATE_CMD, payload: GfwHostAsyncImmediateCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040109, "GERAN_FW3_ASYNC_IMMEDIATE_CMD", NULL ),

  /* GERAN_FW3_RFM_SET_TX_BAND_CMD, payload: GfwHostRfmSetTxBandCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0904010b, "GERAN_FW3_RFM_SET_TX_BAND_CMD", NULL ),

  /* GERAN_FW3_RFM_THERM_READ_CMD, payload: GfwHostRfmThermReadCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x0904010d, "GERAN_FW3_RFM_THERM_READ_CMD", NULL ),

  /* GERAN_FW3_ASYNC_EXIT_CMD, payload: GfwHostAsyncExitCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040110, "GERAN_FW3_ASYNC_EXIT_CMD", NULL ),

  /* GERAN_FW3_RFM_EXIT_MODE_CMD, payload: GfwHostRfmExitModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040111, "GERAN_FW3_RFM_EXIT_MODE_CMD", NULL ),

  /* GERAN_FW3_DEVICE_MODE_CMD, payload: GfwHostAsyncDeviceModeCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040113, "GERAN_FW3_DEVICE_MODE_CMD", NULL ),

  /* GERAN_FW3_WTR_CFG_CMD, payload: GfwHostWtrConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040114, "GERAN_FW3_WTR_CFG_CMD", NULL ),

  /* GERAN_FW3_WLAN_CXM_POLICY_CMD, payload: GfwWlanCxmConfigCmd */
  MSGR_UMID_TABLE_ENTRY ( 0x09040116, "GERAN_FW3_WLAN_CXM_POLICY_CMD", NULL ),

  /* GERAN_FW3_APP_MODE_CONFIG_RSP, payload: GfwHostAppModeConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040302, "GERAN_FW3_APP_MODE_CONFIG_RSP", NULL ),

  /* GERAN_FW3_RFM_ENTER_MODE_RSP, payload: GfwHostRfmEnterModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040304, "GERAN_FW3_RFM_ENTER_MODE_RSP", NULL ),

  /* GERAN_FW3_ASYNC_RSP, payload: GfwHostAsyncRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040305, "GERAN_FW3_ASYNC_RSP", NULL ),

  /* GERAN_FW3_SLEEP_RSP, payload: GfwHostSleepRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040307, "GERAN_FW3_SLEEP_RSP", NULL ),

  /* GERAN_FW3_ASYNC_IMMEDIATE_RSP, payload: GfwHostAsyncImmediateRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0904030a, "GERAN_FW3_ASYNC_IMMEDIATE_RSP", NULL ),

  /* GERAN_FW3_RFM_SET_TX_BAND_RSP, payload: GfwHostRfmSetTxBandRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0904030c, "GERAN_FW3_RFM_SET_TX_BAND_RSP", NULL ),

  /* GERAN_FW3_RFM_THERM_READ_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0904030e, "GERAN_FW3_RFM_THERM_READ_RSP", NULL ),

  /* GERAN_FW3_RFM_ASD_RSP, payload: GfwHostRfmRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x0904030f, "GERAN_FW3_RFM_ASD_RSP", NULL ),

  /* GERAN_FW3_RFM_EXIT_MODE_RSP, payload: GfwHostRfmExitModeRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040312, "GERAN_FW3_RFM_EXIT_MODE_RSP", NULL ),

  /* GERAN_FW3_WTR_CFG_RSP, payload: GfwHostWtrConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040315, "GERAN_FW3_WTR_CFG_RSP", NULL ),

  /* GERAN_FW3_WLAN_CXM_POLICY_RSP, payload: GfwWlanCxmConfigRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040317, "GERAN_FW3_WLAN_CXM_POLICY_RSP", NULL ),

  /* GERAN_FW3_INVALID_RFLM_TX_INDEX_RSP, payload: GfwHostInvalidTxRflmIndexRsp */
  MSGR_UMID_TABLE_ENTRY ( 0x09040318, "GERAN_FW3_INVALID_RFLM_TX_INDEX_RSP", NULL ),

  /* GPS_PGI_INIT_COMPLETE_CMD, payload: gnss_PgiInitComplete */
  MSGR_UMID_TABLE_ENTRY ( 0x0a010100, "GPS_PGI_INIT_COMPLETE_CMD", NULL ),

  /* GPS_PGI_INIT_COMPLETE_RSP, payload: gnss_PgiInitComplete */
  MSGR_UMID_TABLE_ENTRY ( 0x0a010301, "GPS_PGI_INIT_COMPLETE_RSP", NULL ),

  /* WCDMA_RRC_LTE_RESEL_REQ, payload: wcdma_rrc_lte_resel_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010201, "WCDMA_RRC_LTE_RESEL_REQ", NULL ),

  /* WCDMA_RRC_LTE_ABORT_RESEL_REQ, payload: wcdma_rrc_lte_abort_resel_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010202, "WCDMA_RRC_LTE_ABORT_RESEL_REQ", NULL ),

  /* WCDMA_RRC_LTE_REDIR_REQ, payload: wcdma_rrc_lte_redir_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010203, "WCDMA_RRC_LTE_REDIR_REQ", NULL ),

  /* WCDMA_RRC_LTE_ABORT_REDIR_REQ, payload: wcdma_rrc_lte_abort_redir_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010204, "WCDMA_RRC_LTE_ABORT_REDIR_REQ", NULL ),

  /* WCDMA_RRC_LTE_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010205, "WCDMA_RRC_LTE_PLMN_SRCH_REQ", NULL ),

  /* WCDMA_RRC_LTE_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010206, "WCDMA_RRC_LTE_ABORT_PLMN_SRCH_REQ", NULL ),

  /* WCDMA_RRC_LTE_UTRA_CAPABILITIES_REQ, payload: wcdma_rrc_lte_utra_capabilities_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010207, "WCDMA_RRC_LTE_UTRA_CAPABILITIES_REQ", NULL ),

  /* WCDMA_RRC_LTE_GET_DEDICATED_PRI_REQ, payload: wcdma_rrc_lte_get_dedicated_pri_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010208, "WCDMA_RRC_LTE_GET_DEDICATED_PRI_REQ", NULL ),

  /* WCDMA_RRC_LTE_PSHO_REQ, payload: wcdma_rrc_lte_psho_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010209, "WCDMA_RRC_LTE_PSHO_REQ", NULL ),

  /* WCDMA_RRC_LTE_ABORT_PSHO_REQ, payload: wcdma_rrc_lte_abort_psho_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01020a, "WCDMA_RRC_LTE_ABORT_PSHO_REQ", NULL ),

  /* WCDMA_RRC_LTE_GET_CGI_REQ, payload: wcdma_rrc_lte_get_cgi_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01020b, "WCDMA_RRC_LTE_GET_CGI_REQ", NULL ),

  /* WCDMA_RRC_LTE_ABORT_CGI_REQ, payload: wcdma_rrc_lte_abort_cgi_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01020c, "WCDMA_RRC_LTE_ABORT_CGI_REQ", NULL ),

  /* WCDMA_RRC_LTE_GET_PLMN_PRTL_RESULTS_REQ, payload: wcdma_rrc_lte_get_plmn_prtl_results_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01020d, "WCDMA_RRC_LTE_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* WCDMA_RRC_LTE_RESEL_FAILED_RSP, payload: wcdma_rrc_lte_resel_failed_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010301, "WCDMA_RRC_LTE_RESEL_FAILED_RSP", NULL ),

  /* WCDMA_RRC_LTE_ABORT_RESEL_RSP, payload: wcdma_rrc_lte_abort_resel_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010302, "WCDMA_RRC_LTE_ABORT_RESEL_RSP", NULL ),

  /* WCDMA_RRC_LTE_REDIR_FAILED_RSP, payload: wcdma_rrc_lte_redir_failed_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010303, "WCDMA_RRC_LTE_REDIR_FAILED_RSP", NULL ),

  /* WCDMA_RRC_LTE_ABORT_REDIR_RSP, payload: wcdma_rrc_lte_abort_redir_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010304, "WCDMA_RRC_LTE_ABORT_REDIR_RSP", NULL ),

  /* WCDMA_RRC_LTE_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010305, "WCDMA_RRC_LTE_PLMN_SRCH_RSP", NULL ),

  /* WCDMA_RRC_LTE_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010306, "WCDMA_RRC_LTE_ABORT_PLMN_SRCH_RSP", NULL ),

  /* WCDMA_RRC_LTE_UTRA_CAPABILITIES_RSP, payload: wcdma_rrc_lte_utra_capabilities_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010307, "WCDMA_RRC_LTE_UTRA_CAPABILITIES_RSP", NULL ),

  /* WCDMA_RRC_LTE_GET_DEDICATED_PRI_RSP, payload: wcdma_rrc_lte_get_dedicated_pri_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010308, "WCDMA_RRC_LTE_GET_DEDICATED_PRI_RSP", NULL ),

  /* WCDMA_RRC_LTE_PSHO_RSP, payload: wcdma_rrc_lte_psho_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010309, "WCDMA_RRC_LTE_PSHO_RSP", NULL ),

  /* WCDMA_RRC_LTE_ABORT_PSHO_RSP, payload: wcdma_rrc_lte_abort_psho_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01030a, "WCDMA_RRC_LTE_ABORT_PSHO_RSP", NULL ),

  /* WCDMA_RRC_LTE_GET_CGI_RSP, payload: wcdma_rrc_lte_get_cgi_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01030b, "WCDMA_RRC_LTE_GET_CGI_RSP", NULL ),

  /* WCDMA_RRC_LTE_ABORT_CGI_RSP, payload: wcdma_rrc_lte_abort_cgi_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01030c, "WCDMA_RRC_LTE_ABORT_CGI_RSP", NULL ),

  /* WCDMA_RRC_LTE_RESEL_RSP, payload: lte_rrc_wcdma_resel_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0b01030d, "WCDMA_RRC_LTE_RESEL_RSP", NULL ),

  /* WCDMA_RRC_LTE_CLEAR_DEDICATED_PRI_IND, payload: wcdma_rrc_lte_clear_dedicated_pri_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010401, "WCDMA_RRC_LTE_CLEAR_DEDICATED_PRI_IND", NULL ),

  /* WCDMA_RRC_LTE_PLMN_SRCH_SUSPEND_IND, payload: wcdma_rrc_lte_plmn_srch_suspend_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0b010402, "WCDMA_RRC_LTE_PLMN_SRCH_SUSPEND_IND", NULL ),

  /* DS_MSGRRECV_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c010000, "DS_MSGRRECV_LOOPBACK_SPR", NULL ),

  /* DS_MSGRRECV_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c010001, "DS_MSGRRECV_LOOPBACK_REPLY_SPR", NULL ),

  /* DS_MSGRRECV_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0c010002, "DS_MSGRRECV_THREAD_READY_SPR", NULL ),

  /* DS_MSGRRECV_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0c010003, "DS_MSGRRECV_THREAD_KILL_SPR", NULL ),

  /* DS_MSGRRECV_DESENSE_IND, payload: cxm_coex_desense_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c010401, "DS_MSGRRECV_DESENSE_IND", NULL ),

  /* DS_MSGRRECV_WDOG_RPT_TMR_EXPIRED_TMRI, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x0c011600, "DS_MSGRRECV_WDOG_RPT_TMR_EXPIRED_TMRI", NULL ),

  /* DS_LTE_PHYS_LINK_FLOW_DISABLE_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c030201, "DS_LTE_PHYS_LINK_FLOW_DISABLE_REQ", NULL ),

  /* DS_LTE_PHYS_LINK_FLOW_ENABLE_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c030202, "DS_LTE_PHYS_LINK_FLOW_ENABLE_REQ", NULL ),

  /* DS_3GPP_PHYS_LINK_FLOW_DISABLE_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c050201, "DS_3GPP_PHYS_LINK_FLOW_DISABLE_REQ", NULL ),

  /* DS_3GPP_PHYS_LINK_FLOW_ENABLE_REQ, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c050202, "DS_3GPP_PHYS_LINK_FLOW_ENABLE_REQ", NULL ),

  /* DS_3GPP_BEARER_QOS_INFO_IND, payload: ds_3gpp_bearer_qos_info_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x0c050421, "DS_3GPP_BEARER_QOS_INFO_IND", NULL ),

  /* DS_MGR_LOW_LATENCY_IND, payload: ds_mgr_latency_info_ext_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0c060424, "DS_MGR_LOW_LATENCY_IND", NULL ),

  /* NAS_EMM_DETACH_CMD, payload: emm_detach_cmd_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190103, "NAS_EMM_DETACH_CMD", NULL ),

  /* NAS_EMM_EPS_BEARER_STATUS_CMD, payload: emm_eps_bearer_status_cmd_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190104, "NAS_EMM_EPS_BEARER_STATUS_CMD", NULL ),

  /* NAS_EMM_EMC_SRV_STATUS_CMD, payload: emm_emc_srv_status_cmd_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190107, "NAS_EMM_EMC_SRV_STATUS_CMD", NULL ),

  /* NAS_EMM_SERVICE_REQ, payload: emm_service_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190201, "NAS_EMM_SERVICE_REQ", NULL ),

  /* NAS_EMM_DATA_REQ, payload: emm_data_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190202, "NAS_EMM_DATA_REQ", NULL ),

  /* NAS_EMM_IRAT_UL_MSG_REQ, payload: emm_irat_ul_msg_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190203, "NAS_EMM_IRAT_UL_MSG_REQ", NULL ),

  /* NAS_EMM_IRAT_HDR_UL_MSG_REQ, payload: emm_irat_3gpp2_ul_msg_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190204, "NAS_EMM_IRAT_HDR_UL_MSG_REQ", NULL ),

  /* NAS_EMM_1XCSFB_ESR_CALL_REQ, payload: emm_1xCSFB_esr_call_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190205, "NAS_EMM_1XCSFB_ESR_CALL_REQ", NULL ),

  /* NAS_EMM_1XCSFB_ESR_CALL_ABORT_REQ, payload: emm_1xCSFB_esr_call_abort_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190206, "NAS_EMM_1XCSFB_ESR_CALL_ABORT_REQ", NULL ),

  /* NAS_EMM_IRAT_DL_MSG_IND, payload: emm_irat_dl_msg_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190401, "NAS_EMM_IRAT_DL_MSG_IND", NULL ),

  /* NAS_EMM_IRAT_IMSI_ATTACH_IND, payload: emm_irat_imsi_attach_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190402, "NAS_EMM_IRAT_IMSI_ATTACH_IND", NULL ),

  /* NAS_EMM_IRAT_CTXT_LOST_IND, payload: emm_irat_ctxt_lost_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190403, "NAS_EMM_IRAT_CTXT_LOST_IND", NULL ),

  /* NAS_EMM_IRAT_FAILURE_IND, payload: emm_irat_failure_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190404, "NAS_EMM_IRAT_FAILURE_IND", NULL ),

  /* NAS_EMM_PLMN_CHANGE_IND, payload: emm_plmn_change_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190405, "NAS_EMM_PLMN_CHANGE_IND", NULL ),

  /* NAS_EMM_ATTACH_COMPLETE_IND, payload: emm_attach_complete_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190406, "NAS_EMM_ATTACH_COMPLETE_IND", NULL ),

  /* NAS_EMM_IRAT_HDR_FAILURE_IND, payload: emm_irat_failure_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190407, "NAS_EMM_IRAT_HDR_FAILURE_IND", NULL ),

  /* NAS_EMM_IRAT_HDR_DL_MSG_IND, payload: emm_irat_3gpp2_dl_msg_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190408, "NAS_EMM_IRAT_HDR_DL_MSG_IND", NULL ),

  /* NAS_EMM_TAU_COMPLETE_IND, payload: emm_tau_complete_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f19040b, "NAS_EMM_TAU_COMPLETE_IND", NULL ),

  /* NAS_EMM_T3402_CHANGED_IND, payload: emm_t3402_changed_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f19040c, "NAS_EMM_T3402_CHANGED_IND", NULL ),

  /* NAS_EMM_DEACT_NON_EMC_BEARER_IND, payload: emm_deact_non_emc_bearer_ind_s_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f19040d, "NAS_EMM_DEACT_NON_EMC_BEARER_IND", NULL ),

  /* NAS_EMM_RESET_APN_SWITCH_IND, payload: emm_reset_apn_switch_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f19040e, "NAS_EMM_RESET_APN_SWITCH_IND", NULL ),

  /* NAS_EMM_IRAT_UL_MSG_CNF, payload: emm_irat_ul_msg_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190801, "NAS_EMM_IRAT_UL_MSG_CNF", NULL ),

  /* NAS_EMM_IRAT_HDR_UL_MSG_CNF, payload: emm_irat_3gpp2_ul_msg_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x0f190802, "NAS_EMM_IRAT_HDR_UL_MSG_CNF", NULL ),

  /* NAS_ESM_BEARER_RESOURCE_ALLOC_REQ, payload: esm_bearer_resource_alloc_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0201, "NAS_ESM_BEARER_RESOURCE_ALLOC_REQ", NULL ),

  /* NAS_ESM_BEARER_RESOURCE_REL_REQ, payload: esm_bearer_resource_rel_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0202, "NAS_ESM_BEARER_RESOURCE_REL_REQ", NULL ),

  /* NAS_ESM_PDN_CONNECTIVTY_REQ, payload: esm_pdn_connectivity_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0203, "NAS_ESM_PDN_CONNECTIVTY_REQ", NULL ),

  /* NAS_ESM_PDN_DISCONNECT_REQ, payload: esm_pdn_disconnect_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0204, "NAS_ESM_PDN_DISCONNECT_REQ", NULL ),

  /* NAS_ESM_BEARER_RESOURCE_ALLOC_ABORT_REQ, payload: esm_bearer_resource_alloc_abort_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0205, "NAS_ESM_BEARER_RESOURCE_ALLOC_ABORT_REQ", NULL ),

  /* NAS_ESM_PDN_CONNECTIVITY_ABORT_REQ, payload: esm_pdn_connectivity_abort_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0206, "NAS_ESM_PDN_CONNECTIVITY_ABORT_REQ", NULL ),

  /* NAS_ESM_DRB_REESTABLISH_REQ, payload: esm_drb_reestablish_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0207, "NAS_ESM_DRB_REESTABLISH_REQ", NULL ),

  /* NAS_ESM_BEARER_RESOURCE_MODIFICATION_REQ, payload: esm_bearer_resource_modification_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c020e, "NAS_ESM_BEARER_RESOURCE_MODIFICATION_REQ", NULL ),

  /* NAS_ESM_1XCSFB_CALL_REQ, payload: esm_1xCSFB_call_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0210, "NAS_ESM_1XCSFB_CALL_REQ", NULL ),

  /* NAS_ESM_1XCSFB_ABORT_REQ, payload: esm_1xCSFB_abort_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0211, "NAS_ESM_1XCSFB_ABORT_REQ", NULL ),

  /* NAS_ESM_1XCSFB_ESR_CALL_RSP, payload: emm_1xCSFB_esr_call_rsp_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c030b, "NAS_ESM_1XCSFB_ESR_CALL_RSP", NULL ),

  /* NAS_ESM_1XCSFB_ESR_CALL_ABORT_RSP, payload: emm_1xCSFB_esr_call_abort_rsp_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c030c, "NAS_ESM_1XCSFB_ESR_CALL_ABORT_RSP", NULL ),

  /* NAS_ESM_DATA_IND, payload: esm_data_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0401, "NAS_ESM_DATA_IND", NULL ),

  /* NAS_ESM_FAILURE_IND, payload: esm_failure_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0402, "NAS_ESM_FAILURE_IND", NULL ),

  /* NAS_ESM_SIG_CONNECTION_RELEASED_IND, payload: esm_sig_connection_released_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0403, "NAS_ESM_SIG_CONNECTION_RELEASED_IND", NULL ),

  /* NAS_ESM_ACTIVE_EPS_IND, payload: esm_active_eps_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0404, "NAS_ESM_ACTIVE_EPS_IND", NULL ),

  /* NAS_ESM_DETACH_IND, payload: esm_detach_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0405, "NAS_ESM_DETACH_IND", NULL ),

  /* NAS_ESM_EPS_BEARER_STATUS_IND, payload: esm_eps_bearer_status_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0406, "NAS_ESM_EPS_BEARER_STATUS_IND", NULL ),

  /* NAS_ESM_GET_PDN_CONNECTIVITY_REQ_IND, payload: esm_get_pdn_connectivity_req_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0407, "NAS_ESM_GET_PDN_CONNECTIVITY_REQ_IND", NULL ),

  /* NAS_ESM_GET_ISR_STATUS_IND, payload: esm_get_isr_status_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0408, "NAS_ESM_GET_ISR_STATUS_IND", NULL ),

  /* NAS_ESM_ACT_DEFAULT_BEARER_CONTEXT_REJ_IND, payload: esm_act_default_bearer_context_rej_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0409, "NAS_ESM_ACT_DEFAULT_BEARER_CONTEXT_REJ_IND", NULL ),

  /* NAS_ESM_ISR_STATUS_CHANGE_IND, payload: esm_isr_status_change_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c040a, "NAS_ESM_ISR_STATUS_CHANGE_IND", NULL ),

  /* NAS_ESM_ACT_DEDICATED_BEARER_CONTEXT_REJ_IND, payload: esm_act_dedicated_bearer_context_rej_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c040b, "NAS_ESM_ACT_DEDICATED_BEARER_CONTEXT_REJ_IND", NULL ),

  /* NAS_ESM_MODIFY_BEARER_CONTEXT_REJ_IND, payload: esm_modify_bearer_context_rej_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c040c, "NAS_ESM_MODIFY_BEARER_CONTEXT_REJ_IND", NULL ),

  /* NAS_ESM_UNBLOCK_ALL_APNS_IND, payload: esm_unblock_all_apns_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c040d, "NAS_ESM_UNBLOCK_ALL_APNS_IND", NULL ),

  /* NAS_ESM_ACT_DEFAULT_BEARER_CONTEXT_ACCEPT_CNF, payload: esm_act_default_bearer_context_accept_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c0808, "NAS_ESM_ACT_DEFAULT_BEARER_CONTEXT_ACCEPT_CNF", NULL ),

  /* NAS_ESM_ACT_DEDICATED_BEARER_CONTEXT_ACCEPT_CNF, payload: esm_act_dedicated_bearer_context_accept_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c080a, "NAS_ESM_ACT_DEDICATED_BEARER_CONTEXT_ACCEPT_CNF", NULL ),

  /* NAS_ESM_MODIFY_BEARER_CONTEXT_ACCEPT_CNF, payload: esm_modify_bearer_context_accept_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c080d, "NAS_ESM_MODIFY_BEARER_CONTEXT_ACCEPT_CNF", NULL ),

  /* NAS_ESM_PDN_CONNECT_PACKED_CNF, payload: esm_pdn_connectivity_packed_cnf_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c080f, "NAS_ESM_PDN_CONNECT_PACKED_CNF", NULL ),

  /* NAS_ESM_TIMER_EXPIRED_TMRI, payload: esm_self_signal_timer_expired_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1c1601, "NAS_ESM_TIMER_EXPIRED_TMRI", NULL ),

  /* NAS_SM_PDP_CONTEXT_REQUEST_LOG_INFO_IND, payload: sm_pdp_context_request_log_info_ind_T */
  MSGR_UMID_TABLE_ENTRY ( 0x0f1d0401, "NAS_SM_PDP_CONTEXT_REQUEST_LOG_INFO_IND", NULL ),

  /* UTILS_CFM_LOOPBACK_SPR, payload: msgr_spr_loopback_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010000, "UTILS_CFM_LOOPBACK_SPR", NULL ),

  /* UTILS_CFM_LOOPBACK_REPLY_SPR, payload: msgr_spr_loopback_reply_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010001, "UTILS_CFM_LOOPBACK_REPLY_SPR", NULL ),

  /* UTILS_CFM_THREAD_READY_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x11010002, "UTILS_CFM_THREAD_READY_SPR", NULL ),

  /* UTILS_CFM_THREAD_KILL_SPR, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x11010003, "UTILS_CFM_THREAD_KILL_SPR", NULL ),

  /* UTILS_CFM_FC_CMD, payload: cfm_fc_cmd_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010100, "UTILS_CFM_FC_CMD", NULL ),

  /* UTILS_CFM_REG_IND, payload: cfm_reg_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010400, "UTILS_CFM_REG_IND", NULL ),

  /* UTILS_CFM_DEREG_IND, payload: cfm_dereg_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010401, "UTILS_CFM_DEREG_IND", NULL ),

  /* UTILS_CFM_MONITOR_IND, payload: cfm_monitor_ind_msg_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11010402, "UTILS_CFM_MONITOR_IND", NULL ),

  /* UTILS_CFM_EXPIRE_TMRI, payload: msgr_hdr_s */
  MSGR_UMID_TABLE_ENTRY ( 0x11011600, "UTILS_CFM_EXPIRE_TMRI", NULL ),

  /* TDSCDMA_FW_STATE_CFG_CMD, payload: tfw_state_cfg_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000100, "TDSCDMA_FW_STATE_CFG_CMD", NULL ),

  /* TDSCDMA_FW_PANIC_STOP_CMD, payload: tfw_panic_stop_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000101, "TDSCDMA_FW_PANIC_STOP_CMD", NULL ),

  /* TDSCDMA_FW_RX_CALIBRATION_CMD, payload: tfw_rx_calibration_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000102, "TDSCDMA_FW_RX_CALIBRATION_CMD", NULL ),

  /* TDSCDMA_FW_TX_CALIBRATION_CMD, payload: tfw_tx_calibration_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000103, "TDSCDMA_FW_TX_CALIBRATION_CMD", NULL ),

  /* TDSCDMA_FW_RX_CONFIG_CMD, payload: tfw_rx_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000104, "TDSCDMA_FW_RX_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_TX_CONFIG_CMD, payload: tfw_tx_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000105, "TDSCDMA_FW_TX_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_RX_TX_OVERRIDE_CONFIG_CMD, payload: tfw_rx_tx_override_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000106, "TDSCDMA_FW_RX_TX_OVERRIDE_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_IQ_CAPTURE_CMD, payload: tfw_iq_capture_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000107, "TDSCDMA_FW_IQ_CAPTURE_CMD", NULL ),

  /* TDSCDMA_FW_RX_CONFIG_UPDATE_CMD, payload: tfw_rx_config_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000108, "TDSCDMA_FW_RX_CONFIG_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_FTM_RX_CMD, payload: tfw_ftm_rx_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000109, "TDSCDMA_FW_FTM_RX_CMD", NULL ),

  /* TDSCDMA_FW_FTM_TX_CMD, payload: tfw_ftm_tx_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200010a, "TDSCDMA_FW_FTM_TX_CMD", NULL ),

  /* TDSCDMA_FW_IQMC_TEMP_COMPENSATION_CMD, payload: tfw_iqmc_temp_compensation_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200010b, "TDSCDMA_FW_IQMC_TEMP_COMPENSATION_CMD", NULL ),

  /* TDSCDMA_FW_ARD_CMD, payload: tfw_ard_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200010c, "TDSCDMA_FW_ARD_CMD", NULL ),

  /* TDSCDMA_FW_FTM_GET_MULTI_SYNTH_STATE_CMD, payload: tfw_ftm_get_multi_synth_state_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200010d, "TDSCDMA_FW_FTM_GET_MULTI_SYNTH_STATE_CMD", NULL ),

  /* TDSCDMA_FW_SRCH_POWER_SCAN_CMD, payload: tfw_srch_power_scan_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000110, "TDSCDMA_FW_SRCH_POWER_SCAN_CMD", NULL ),

  /* TDSCDMA_FW_SRCH_GAP_DETECT_CMD, payload: tfw_srch_gap_detect_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000111, "TDSCDMA_FW_SRCH_GAP_DETECT_CMD", NULL ),

  /* TDSCDMA_FW_SRCH_SYNCDL_MIDAMBLE_DETECT_CMD, payload: tfw_srch_syncdl_midamble_detect_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000112, "TDSCDMA_FW_SRCH_SYNCDL_MIDAMBLE_DETECT_CMD", NULL ),

  /* TDSCDMA_FW_SRCH_REACQ_CMD, payload: tfw_srch_reacq_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000113, "TDSCDMA_FW_SRCH_REACQ_CMD", NULL ),

  /* TDSCDMA_FW_MEASURE_RSCP_CMD, payload: tfw_measure_rscp_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000120, "TDSCDMA_FW_MEASURE_RSCP_CMD", NULL ),

  /* TDSCDMA_FW_MEASURE_ISCP_CMD, payload: tfw_measure_iscp_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000121, "TDSCDMA_FW_MEASURE_ISCP_CMD", NULL ),

  /* TDSCDMA_FW_MEASURE_ALL_RSCP_CMD, payload: tfw_measure_all_rscp_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000122, "TDSCDMA_FW_MEASURE_ALL_RSCP_CMD", NULL ),

  /* TDSCDMA_FW_CELL_SERVING_JDCS_CMD, payload: tfw_cell_serving_jdcs_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000130, "TDSCDMA_FW_CELL_SERVING_JDCS_CMD", NULL ),

  /* TDSCDMA_FW_BATON_HANDOVER_CMD, payload: tfw_baton_handover_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000131, "TDSCDMA_FW_BATON_HANDOVER_CMD", NULL ),

  /* TDSCDMA_FW_DL_MIDAMBLE_CONFIG_TABLE_CMD, payload: tfw_dl_midamble_config_table_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000140, "TDSCDMA_FW_DL_MIDAMBLE_CONFIG_TABLE_CMD", NULL ),

  /* TDSCDMA_FW_DL_PICH_CONFIG_CMD, payload: tfw_dl_pich_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000141, "TDSCDMA_FW_DL_PICH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DL_PCCPCH_CONFIG_CMD, payload: tfw_dl_pccpch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000142, "TDSCDMA_FW_DL_PCCPCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DL_SCCPCH_CONFIG_CMD, payload: tfw_dl_sccpch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000143, "TDSCDMA_FW_DL_SCCPCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DL_DPCH_CONFIG_CMD, payload: tfw_dl_dpch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000144, "TDSCDMA_FW_DL_DPCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DL_HS_CONFIG_CMD, payload: tfw_dl_hs_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000145, "TDSCDMA_FW_DL_HS_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_FPACH_CONFIG_CMD, payload: tfw_fpach_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000146, "TDSCDMA_FW_FPACH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_EAGCH_CONFIG_CMD, payload: tfw_eagch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000147, "TDSCDMA_FW_EAGCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_EHICH_CONFIG_CMD, payload: tfw_ehich_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000148, "TDSCDMA_FW_EHICH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_NON_SCHLD_GRANT_CONFIG_CMD, payload: tfw_non_schld_grant_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000149, "TDSCDMA_FW_NON_SCHLD_GRANT_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_RX_TIME_SYNC_CMD, payload: tfw_rx_time_sync_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200014a, "TDSCDMA_FW_RX_TIME_SYNC_CMD", NULL ),

  /* TDSCDMA_FW_SLOT_SUSPENSION_CONFIG_CMD, payload: tfw_slot_suspension_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200014b, "TDSCDMA_FW_SLOT_SUSPENSION_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_L1_INSYNC_CONFIG_CMD, payload: tfw_inSync_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200014c, "TDSCDMA_FW_L1_INSYNC_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_UPPCH_TX_PWR_TIMING_CONFIG_CMD, payload: tfw_uppch_tx_pwr_timing_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000150, "TDSCDMA_FW_UPPCH_TX_PWR_TIMING_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_PRACH_CONFIG_CMD, payload: tfw_prach_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000151, "TDSCDMA_FW_PRACH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DPCH_TX_PWR_TIMING_CONFIG_CMD, payload: tfw_dpch_tx_pwr_timing_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000152, "TDSCDMA_FW_DPCH_TX_PWR_TIMING_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_UL_DPCH_CONFIG_CMD, payload: tfw_ul_dpch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000153, "TDSCDMA_FW_UL_DPCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_ERUCCH_CONFIG_CMD, payload: tfw_erucch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000154, "TDSCDMA_FW_ERUCCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_EUL_CONFIG_CMD, payload: tfw_eul_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000155, "TDSCDMA_FW_EUL_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_EPUCH_CONFIG_CMD, payload: tfw_epuch_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000156, "TDSCDMA_FW_EPUCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_UL_DPCH_CANCEL_CMD, payload: tfw_ul_dpch_cancel_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000157, "TDSCDMA_FW_UL_DPCH_CANCEL_CMD", NULL ),

  /* TDSCDMA_FW_DPCH_TX_PWR_TIMING_UPDATE_CMD, payload: tfw_dpch_tx_pwr_timing_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000158, "TDSCDMA_FW_DPCH_TX_PWR_TIMING_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_HSSICH_OVERRIDE_CMD, payload: tfw_hssich_override_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000159, "TDSCDMA_FW_HSSICH_OVERRIDE_CMD", NULL ),

  /* TDSCDMA_FW_DL_DPCH_TARGETSIR_UPDATE_CMD, payload: tfw_dl_dpch_targetSIR_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000160, "TDSCDMA_FW_DL_DPCH_TARGETSIR_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_HSSCCH_TARGETSIR_UPDATE_CMD, payload: tfw_hsscch_targetSIR_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000161, "TDSCDMA_FW_HSSCCH_TARGETSIR_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_EAGCH_TARGETSIR_UPDATE_CMD, payload: tfw_eagch_targetSIR_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000162, "TDSCDMA_FW_EAGCH_TARGETSIR_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_PATHLOSS_UPDATE_CMD, payload: tfw_pathloss_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000163, "TDSCDMA_FW_PATHLOSS_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_HSPDSCH_SIR_UPDATE_CMD, payload: tfw_hspdsch_sir_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000166, "TDSCDMA_FW_HSPDSCH_SIR_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_ADC_CLOCK_GATING_CONFIG_CMD, payload: tfw_adc_clock_gating_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000167, "TDSCDMA_FW_ADC_CLOCK_GATING_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_HSDPA_FLOW_CONTORL_CMD, payload: tfw_hsdpa_flow_control_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000168, "TDSCDMA_FW_HSDPA_FLOW_CONTORL_CMD", NULL ),

  /* TDSCDMA_FW_ANTENNA_SWITCHING_CONFIG_CMD, payload: tfw_antenna_switching_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200016a, "TDSCDMA_FW_ANTENNA_SWITCHING_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_AOL_ABORT_CONFIG_CMD, payload: tfw_aol_abort_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200016b, "TDSCDMA_FW_AOL_ABORT_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_DL_TRCH_UPDATE_CMD, payload: tfw_dl_trCh_update_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000170, "TDSCDMA_FW_DL_TRCH_UPDATE_CMD", NULL ),

  /* TDSCDMA_FW_DL_CCTRCH_CONFIG_CMD, payload: tfw_dl_ccTrCh_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000171, "TDSCDMA_FW_DL_CCTRCH_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_XPT_CAPTURE_CMD, payload: tfw_xpt_capture_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200017a, "TDSCDMA_FW_XPT_CAPTURE_CMD", NULL ),

  /* TDSCDMA_FW_XPT_TX_CMD, payload: tfw_xpt_tx_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1200017b, "TDSCDMA_FW_XPT_TX_CMD", NULL ),

  /* TDSCDMA_FW_IRAT_GAP_DETECT_CMD, payload: tfw_irat_gap_detect_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000180, "TDSCDMA_FW_IRAT_GAP_DETECT_CMD", NULL ),

  /* TDSCDMA_FW_IRAT_SYNCDL_MIDAMBLE_DETECT_CMD, payload: tfw_irat_syncdl_midamble_detect_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000181, "TDSCDMA_FW_IRAT_SYNCDL_MIDAMBLE_DETECT_CMD", NULL ),

  /* TDSCDMA_FW_IRAT_MEASURE_RSCP_CMD, payload: tfw_irat_measure_rscp_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000182, "TDSCDMA_FW_IRAT_MEASURE_RSCP_CMD", NULL ),

  /* TDSCDMA_FW_IRAT_T2X_GAP_CFG_CMD, payload: tfw_irat_t2x_gap_cfg_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000183, "TDSCDMA_FW_IRAT_T2X_GAP_CFG_CMD", NULL ),

  /* TDSCDMA_FW_IRAT_T2X_CLEANUP_CMD, payload: tfw_irat_t2x_cleanup_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000184, "TDSCDMA_FW_IRAT_T2X_CLEANUP_CMD", NULL ),

  /* TDSCDMA_FW_MULTI_SIM_DSDS_QTA_GAP_CMD, payload: tfw_multi_sim_dsds_qta_gap_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000185, "TDSCDMA_FW_MULTI_SIM_DSDS_QTA_GAP_CMD", NULL ),

  /* TDSCDMA_FW_MULTI_SIM_CONFIG_CMD, payload: tfw_multi_sim_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000186, "TDSCDMA_FW_MULTI_SIM_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_WLAN_COEX_POLICY_CMD, payload: tfw_wlan_coex_policy_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000187, "TDSCDMA_FW_WLAN_COEX_POLICY_CMD", NULL ),

  /* TDSCDMA_FW_WLAN_COEX_METRIC_CTR_CMD, payload: tfw_wlan_coex_mertic_ctr_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000188, "TDSCDMA_FW_WLAN_COEX_METRIC_CTR_CMD", NULL ),

  /* TDSCDMA_FW_DIAG_LOG_MASK_CMD, payload: tfw_diag_log_mask_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000190, "TDSCDMA_FW_DIAG_LOG_MASK_CMD", NULL ),

  /* TDSCDMA_FW_TX_MP_LOG_CONFIG_CMD, payload: tfw_tx_mp_log_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000191, "TDSCDMA_FW_TX_MP_LOG_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_TS0_CELL_UPDT_CMD, payload: tfw_ts0_cell_updt_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000192, "TDSCDMA_FW_TS0_CELL_UPDT_CMD", NULL ),

  /* TDSCDMA_FW_NONTS0_CELL_UPDT_CMD, payload: tfw_nonts0_cell_updt_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000193, "TDSCDMA_FW_NONTS0_CELL_UPDT_CMD", NULL ),

  /* TDSCDMA_FW_SERVING_CELL_UPDT_CMD, payload: tfw_serving_cell_updt_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000194, "TDSCDMA_FW_SERVING_CELL_UPDT_CMD", NULL ),

  /* TDSCDMA_FW_TX_APT_CONFIG_CMD, payload: tfw_tx_apt_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000195, "TDSCDMA_FW_TX_APT_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_TX_HW_BLOCK_CONFIG_CMD, payload: tfw_tx_hw_block_config_cmd_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000196, "TDSCDMA_FW_TX_HW_BLOCK_CONFIG_CMD", NULL ),

  /* TDSCDMA_FW_STATE_CFG_RSP, payload: tfw_state_cfg_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000300, "TDSCDMA_FW_STATE_CFG_RSP", NULL ),

  /* TDSCDMA_FW_SRCH_POWER_SCAN_RSP, payload: tfw_srch_power_scan_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000310, "TDSCDMA_FW_SRCH_POWER_SCAN_RSP", NULL ),

  /* TDSCDMA_FW_SRCH_GAP_DETECT_RSP, payload: tfw_srch_gap_detect_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000311, "TDSCDMA_FW_SRCH_GAP_DETECT_RSP", NULL ),

  /* TDSCDMA_FW_SRCH_SYNCDL_MIDAMBLE_DETECT_RSP, payload: tfw_srch_syncdl_midamble_detect_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000312, "TDSCDMA_FW_SRCH_SYNCDL_MIDAMBLE_DETECT_RSP", NULL ),

  /* TDSCDMA_FW_SRCH_REACQ_RSP, payload: tfw_srch_reacq_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000313, "TDSCDMA_FW_SRCH_REACQ_RSP", NULL ),

  /* TDSCDMA_FW_MEASURE_RSCP_RSP, payload: tfw_measure_rscp_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000320, "TDSCDMA_FW_MEASURE_RSCP_RSP", NULL ),

  /* TDSCDMA_FW_MEASURE_ISCP_RSP, payload: tfw_measure_iscp_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000321, "TDSCDMA_FW_MEASURE_ISCP_RSP", NULL ),

  /* TDSCDMA_FW_MEASURE_ALL_RSCP_RSP, payload: tfw_measure_all_rscp_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000322, "TDSCDMA_FW_MEASURE_ALL_RSCP_RSP", NULL ),

  /* TDSCDMA_FW_ANTENNA_SWITCHING_CONFIG_RSP, payload: tfw_antenna_switching_config_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000327, "TDSCDMA_FW_ANTENNA_SWITCHING_CONFIG_RSP", NULL ),

  /* TDSCDMA_FW_IQ_CAPTURE_RSP, payload: tfw_iq_capture_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000330, "TDSCDMA_FW_IQ_CAPTURE_RSP", NULL ),

  /* TDSCDMA_FW_BATON_HANDOVER_RSP, payload: tfw_baton_handover_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000331, "TDSCDMA_FW_BATON_HANDOVER_RSP", NULL ),

  /* TDSCDMA_FW_TS0_CELL_RSP, payload: tfw_ts0_cell_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000332, "TDSCDMA_FW_TS0_CELL_RSP", NULL ),

  /* TDSCDMA_FW_NONTS0_CELL_RSP, payload: tfw_nonts0_cell_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000333, "TDSCDMA_FW_NONTS0_CELL_RSP", NULL ),

  /* TDSCDMA_FW_IRAT_T2X_CLEANUP_RSP, payload: tfw_irat_t2x_cleanup_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000340, "TDSCDMA_FW_IRAT_T2X_CLEANUP_RSP", NULL ),

  /* TDSCDMA_FW_FTM_RX_RSP_RSP, payload: tfw_ftm_rx_cmd_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000350, "TDSCDMA_FW_FTM_RX_RSP_RSP", NULL ),

  /* TDSCDMA_FW_FTM_TX_RSP_RSP, payload: tfw_ftm_tx_cmd_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000351, "TDSCDMA_FW_FTM_TX_RSP_RSP", NULL ),

  /* TDSCDMA_FW_FTM_GET_MULTI_SYNTH_STATE_RSP, payload: tfw_ftm_get_multi_synth_state_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000352, "TDSCDMA_FW_FTM_GET_MULTI_SYNTH_STATE_RSP", NULL ),

  /* TDSCDMA_FW_UL_TPC_UDT_IND, payload: tfw_ul_tpc_update_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000400, "TDSCDMA_FW_UL_TPC_UDT_IND", NULL ),

  /* TDSCDMA_FW_CELL_INFO_IND, payload: tfw_cell_info_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000401, "TDSCDMA_FW_CELL_INFO_IND", NULL ),

  /* TDSCDMA_FW_ERROR_DETECTED_IND, payload: tfw_error_detected_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000402, "TDSCDMA_FW_ERROR_DETECTED_IND", NULL ),

  /* TDSCDMA_FW_ARD_IND, payload: tfw_ard_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000403, "TDSCDMA_FW_ARD_IND", NULL ),

  /* TDSCDMA_FW_DSDS_CLEANUP_IND, payload: tfw_dsds_cleanup_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000404, "TDSCDMA_FW_DSDS_CLEANUP_IND", NULL ),

  /* TDSCDMA_FW_WLAN_TXFRMDNL_IND, payload: tfw_wlan_txfrmdnl_ind_t */
  MSGR_UMID_TABLE_ENTRY ( 0x12000405, "TDSCDMA_FW_WLAN_TXFRMDNL_IND", NULL ),

  /* TDSCDMA_FW_DUMMY_CMDI, payload: NULL */
  MSGR_UMID_TABLE_ENTRY ( 0x12001120, "TDSCDMA_FW_DUMMY_CMDI", NULL ),

  /* TDSCDMA_IRAT_INIT_CMD, payload: tdsirat_init_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090100, "TDSCDMA_IRAT_INIT_CMD", NULL ),

  /* TDSCDMA_IRAT_STOP_CMD, payload: tdsirat_stop_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090104, "TDSCDMA_IRAT_STOP_CMD", NULL ),

  /* TDSCDMA_IRAT_ABORT_CMD, payload: tdsirat_abort_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090105, "TDSCDMA_IRAT_ABORT_CMD", NULL ),

  /* TDSCDMA_IRAT_BUILD_RF_SCRIPTS_CMD, payload: tdsirat_build_rf_scripts_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1209010a, "TDSCDMA_IRAT_BUILD_RF_SCRIPTS_CMD", NULL ),

  /* TDSCDMA_IRAT_L2T_INIT_CMD, payload: tdsirat_init_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090110, "TDSCDMA_IRAT_L2T_INIT_CMD", NULL ),

  /* TDSCDMA_IRAT_L2T_STOP_CMD, payload: tdsirat_stop_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090114, "TDSCDMA_IRAT_L2T_STOP_CMD", NULL ),

  /* TDSCDMA_IRAT_L2T_ABORT_CMD, payload: tdsirat_abort_cmd_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090115, "TDSCDMA_IRAT_L2T_ABORT_CMD", NULL ),

  /* TDSCDMA_IRAT_STARTUP_REQ, payload: tdsirat_startup_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090201, "TDSCDMA_IRAT_STARTUP_REQ", NULL ),

  /* TDSCDMA_IRAT_ACQ_REQ, payload: tdsirat_acq_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090202, "TDSCDMA_IRAT_ACQ_REQ", NULL ),

  /* TDSCDMA_IRAT_MEAS_REQ, payload: tdsirat_meas_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090203, "TDSCDMA_IRAT_MEAS_REQ", NULL ),

  /* TDSCDMA_IRAT_L2T_STARTUP_REQ, payload: tdsirat_startup_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090211, "TDSCDMA_IRAT_L2T_STARTUP_REQ", NULL ),

  /* TDSCDMA_IRAT_L2T_ACQ_REQ, payload: tdsirat_acq_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090212, "TDSCDMA_IRAT_L2T_ACQ_REQ", NULL ),

  /* TDSCDMA_IRAT_L2T_MEAS_REQ, payload: tdsirat_meas_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090213, "TDSCDMA_IRAT_L2T_MEAS_REQ", NULL ),

  /* TDSCDMA_IRAT_ACQ_RSP, payload: tdsirat_acq_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090302, "TDSCDMA_IRAT_ACQ_RSP", NULL ),

  /* TDSCDMA_IRAT_MEAS_RSP, payload: tdsirat_meas_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090303, "TDSCDMA_IRAT_MEAS_RSP", NULL ),

  /* TDSCDMA_IRAT_L2T_ACQ_RSP, payload: tdsirat_acq_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090312, "TDSCDMA_IRAT_L2T_ACQ_RSP", NULL ),

  /* TDSCDMA_IRAT_L2T_MEAS_RSP, payload: tdsirat_meas_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090313, "TDSCDMA_IRAT_L2T_MEAS_RSP", NULL ),

  /* TDSCDMA_IRAT_INIT_CNF, payload: tdsirat_init_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090800, "TDSCDMA_IRAT_INIT_CNF", NULL ),

  /* TDSCDMA_IRAT_STOP_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x12090804, "TDSCDMA_IRAT_STOP_CNF", NULL ),

  /* TDSCDMA_IRAT_ABORT_CNF, payload: tdsirat_abort_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090805, "TDSCDMA_IRAT_ABORT_CNF", NULL ),

  /* TDSCDMA_IRAT_L2T_INIT_CNF, payload: tdsirat_init_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090810, "TDSCDMA_IRAT_L2T_INIT_CNF", NULL ),

  /* TDSCDMA_IRAT_L2T_STOP_CNF, payload: none */
  MSGR_UMID_TABLE_ENTRY ( 0x12090814, "TDSCDMA_IRAT_L2T_STOP_CNF", NULL ),

  /* TDSCDMA_IRAT_L2T_ABORT_CNF, payload: tdsirat_abort_cnf_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12090815, "TDSCDMA_IRAT_L2T_ABORT_CNF", NULL ),

  /* TDSCDMA_RRC_LTE_RESEL_REQ, payload: tds_rrc_lte_resel_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310201, "TDSCDMA_RRC_LTE_RESEL_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_RESEL_REQ, payload: tds_rrc_lte_abort_resel_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310202, "TDSCDMA_RRC_LTE_ABORT_RESEL_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_REDIR_REQ, payload: tds_rrc_lte_redir_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310203, "TDSCDMA_RRC_LTE_REDIR_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_REDIR_REQ, payload: tds_rrc_lte_abort_redir_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310204, "TDSCDMA_RRC_LTE_ABORT_REDIR_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_PLMN_SRCH_REQ, payload: lte_irat_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x12310205, "TDSCDMA_RRC_LTE_PLMN_SRCH_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_PLMN_SRCH_REQ, payload: lte_irat_abort_plmn_srch_req_s */
  MSGR_UMID_TABLE_ENTRY ( 0x12310206, "TDSCDMA_RRC_LTE_ABORT_PLMN_SRCH_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_UTRA_CAPABILITIES_REQ, payload: tds_rrc_lte_utra_capabilities_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310207, "TDSCDMA_RRC_LTE_UTRA_CAPABILITIES_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_GET_DEDICATED_PRI_REQ, payload: tds_rrc_lte_get_dedicated_pri_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310208, "TDSCDMA_RRC_LTE_GET_DEDICATED_PRI_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_PSHO_REQ, payload: tds_rrc_lte_psho_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310209, "TDSCDMA_RRC_LTE_PSHO_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_PSHO_REQ, payload: tds_rrc_lte_abort_psho_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020a, "TDSCDMA_RRC_LTE_ABORT_PSHO_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_GET_CGI_REQ, payload: tds_rrc_lte_get_cgi_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020b, "TDSCDMA_RRC_LTE_GET_CGI_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_CGI_REQ, payload: tds_rrc_lte_abort_cgi_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020c, "TDSCDMA_RRC_LTE_ABORT_CGI_REQ", NULL ),

  /* TDSCDMA_RRC_GPS_POS_CELL_INFO_REQ, payload: tds_rrc_gps_pos_cell_info_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020d, "TDSCDMA_RRC_GPS_POS_CELL_INFO_REQ", NULL ),

  /* TDSCDMA_RRC_GET_CONFIG_REQ, payload: tds_rrc_get_config_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020e, "TDSCDMA_RRC_GET_CONFIG_REQ", NULL ),

  /* TDSCDMA_RRC_SET_CONFIG_REQ, payload: tds_rrc_set_config_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231020f, "TDSCDMA_RRC_SET_CONFIG_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_GET_PLMN_PRTL_RESULTS_REQ, payload: tds_rrc_lte_get_plmn_prtl_results_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310210, "TDSCDMA_RRC_LTE_GET_PLMN_PRTL_RESULTS_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_DEPRI_FREQ_REQ, payload: tds_rrc_lte_depri_freq_req_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310211, "TDSCDMA_RRC_LTE_DEPRI_FREQ_REQ", NULL ),

  /* TDSCDMA_RRC_LTE_RESEL_FAILED_RSP, payload: tds_rrc_lte_resel_failed_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310301, "TDSCDMA_RRC_LTE_RESEL_FAILED_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_RESEL_RSP, payload: tds_rrc_lte_abort_resel_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310302, "TDSCDMA_RRC_LTE_ABORT_RESEL_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_REDIR_FAILED_RSP, payload: tds_rrc_lte_redir_failed_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310303, "TDSCDMA_RRC_LTE_REDIR_FAILED_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_REDIR_RSP, payload: tds_rrc_lte_abort_redir_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310304, "TDSCDMA_RRC_LTE_ABORT_REDIR_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_PLMN_SRCH_RSP, payload: lte_irat_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x12310305, "TDSCDMA_RRC_LTE_PLMN_SRCH_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_PLMN_SRCH_RSP, payload: lte_irat_abort_plmn_srch_rsp_s */
  MSGR_UMID_TABLE_ENTRY ( 0x12310306, "TDSCDMA_RRC_LTE_ABORT_PLMN_SRCH_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_UTRA_CAPABILITIES_RSP, payload: tds_rrc_lte_utra_capabilities_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310307, "TDSCDMA_RRC_LTE_UTRA_CAPABILITIES_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_GET_DEDICATED_PRI_RSP, payload: tds_rrc_lte_get_dedicated_pri_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310308, "TDSCDMA_RRC_LTE_GET_DEDICATED_PRI_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_PSHO_RSP, payload: tds_rrc_lte_psho_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310309, "TDSCDMA_RRC_LTE_PSHO_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_PSHO_RSP, payload: tds_rrc_lte_abort_psho_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030a, "TDSCDMA_RRC_LTE_ABORT_PSHO_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_GET_CGI_RSP, payload: tds_rrc_lte_get_cgi_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030b, "TDSCDMA_RRC_LTE_GET_CGI_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_ABORT_CGI_RSP, payload: tds_rrc_lte_abort_cgi_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030c, "TDSCDMA_RRC_LTE_ABORT_CGI_RSP", NULL ),

  /* TDSCDMA_RRC_GPS_POS_CELL_INFO_RSP, payload: tds_rrc_gps_pos_cell_info_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030d, "TDSCDMA_RRC_GPS_POS_CELL_INFO_RSP", NULL ),

  /* TDSCDMA_RRC_GET_CONFIG_RSP, payload: tds_rrc_get_config_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030e, "TDSCDMA_RRC_GET_CONFIG_RSP", NULL ),

  /* TDSCDMA_RRC_SET_CONFIG_RSP, payload: tds_rrc_set_config_rsp_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1231030f, "TDSCDMA_RRC_SET_CONFIG_RSP", NULL ),

  /* TDSCDMA_RRC_LTE_CLEAR_DEDICATED_PRI_IND, payload: tds_rrc_lte_clear_dedicated_pri_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310401, "TDSCDMA_RRC_LTE_CLEAR_DEDICATED_PRI_IND", NULL ),

  /* TDSCDMA_RRC_SCELL_SIGNAL_STATUS_IND, payload: tds_rrc_scell_signal_status_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310402, "TDSCDMA_RRC_SCELL_SIGNAL_STATUS_IND", NULL ),

  /* TDSCDMA_RRC_LTE_PLMN_SRCH_SUSPEND_IND, payload: tds_rrc_lte_plmn_srch_suspend_ind_type */
  MSGR_UMID_TABLE_ENTRY ( 0x12310403, "TDSCDMA_RRC_LTE_PLMN_SRCH_SUSPEND_IND", NULL ),

  /* QMI_NAS_CSG_SEARCH_SELECTION_CONFIG_CMD, payload: mmode_qmi_nas_csg_search_select_config_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16030104, "QMI_NAS_CSG_SEARCH_SELECTION_CONFIG_CMD", NULL ),

  /* QMI_NAS_CSG_IMMEDIATE_SEARCH_SELECTION_CMD, payload: mmode_qmi_nas_csg_immediate_search_select_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16030105, "QMI_NAS_CSG_IMMEDIATE_SEARCH_SELECTION_CMD", NULL ),

  /* QMI_VOICE_CONF_PARTICIPANTS_INFO_CMD, payload: mmode_qmi_voice_conf_participants_info_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16090102, "QMI_VOICE_CONF_PARTICIPANTS_INFO_CMD", NULL ),

  /* QMI_VOICE_TTY_MODE_INFO_CMD, payload: mmode_qmi_voice_tty_mode_info_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16090103, "QMI_VOICE_TTY_MODE_INFO_CMD", NULL ),

  /* QMI_VOICE_AUDIO_RAT_CHANGE_INFO_CMD, payload: mmode_qmi_voice_audio_session_rat_change_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16090109, "QMI_VOICE_AUDIO_RAT_CHANGE_INFO_CMD", NULL ),

  /* QMI_VOICE_UI_TTY_MODE_SETTING_CMD, payload: mmode_qmi_voice_tty_mode_info_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1609010a, "QMI_VOICE_UI_TTY_MODE_SETTING_CMD", NULL ),

  /* QMI_VOICE_CONF_PARTICIPANT_STATUS_INFO_CMD, payload: mmode_qmi_voice_participant_status_cmd_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x1609010b, "QMI_VOICE_CONF_PARTICIPANT_STATUS_INFO_CMD", NULL ),

  /* QMI_VOICE_VOICE_TTY_MODE_CHANGE_IND_IND, payload: mmode_qmi_voice_tty_mode_change_ind_msg_type */
  MSGR_UMID_TABLE_ENTRY ( 0x16090401, "QMI_VOICE_VOICE_TTY_MODE_CHANGE_IND_IND", NULL ),

  /* POLICYMAN_CFG_UPDATE_IND, payload: policyman_cfg_update_ind */
  MSGR_UMID_TABLE_ENTRY ( 0x18010401, "POLICYMAN_CFG_UPDATE_IND", NULL ),

  /* POLICYMAN_CFG_UPDATE_MSIM_IND, payload: policyman_cfg_update_msim_ind */
  MSGR_UMID_TABLE_ENTRY ( 0x18010402, "POLICYMAN_CFG_UPDATE_MSIM_IND", NULL ),

  /* RFLM_CMN_TUNER_AOL_RSP, payload: rflm_cmn_tuner_aol_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a000300, "RFLM_CMN_TUNER_AOL_RSP", NULL ),

  /* RFLM_CMN_TUNER_CL_RSP, payload: rflm_cmn_tuner_cl_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a000301, "RFLM_CMN_TUNER_CL_RSP", NULL ),

  /* RFLM_CMN_ASD_RSP, payload: rflm_cmn_asd_token_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a000302, "RFLM_CMN_ASD_RSP", NULL ),

  /* RFLM_GSM_THERM_READ_RSP, payload: rflm_gsm_therm_read_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010300, "RFLM_GSM_THERM_READ_RSP", NULL ),

  /* RFLM_GSM_ASD_RSP, payload: rflm_gsm_asd_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010301, "RFLM_GSM_ASD_RSP", NULL ),

  /* RFLM_GSM_DEVICE_STATUS_READ_RSP, payload: rflm_gsm_device_status_read_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010302, "RFLM_GSM_DEVICE_STATUS_READ_RSP", NULL ),

  /* RFLM_GSM_ASD_SUB2_RSP, payload: rflm_gsm_asd_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010310, "RFLM_GSM_ASD_SUB2_RSP", NULL ),

  /* RFLM_GSM_THERM_READ_SUB2_RSP, payload: rflm_gsm_therm_read_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010320, "RFLM_GSM_THERM_READ_SUB2_RSP", NULL ),

  /* RFLM_GSM_DEVICE_STATUS_READ_SUB2_RSP, payload: rflm_gsm_device_status_read_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010330, "RFLM_GSM_DEVICE_STATUS_READ_SUB2_RSP", NULL ),

  /* RFLM_GSM_ASD_SUB3_RSP, payload: rflm_gsm_asd_rsp_t */
  MSGR_UMID_TABLE_ENTRY ( 0x1a010340, "RFLM_GSM_ASD_SUB3_RSP", NULL ),

  /* RFLM_LTE_FBRX_GAIN_ERR_UPDATE_IND, payload: rflm_lte_fbrx_gain_err_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040401, "RFLM_LTE_FBRX_GAIN_ERR_UPDATE_IND", NULL ),

  /* RFLM_LTE_FED_SET_TIMER_IND, payload: rflm_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040402, "RFLM_LTE_FED_SET_TIMER_IND", NULL ),

  /* RFLM_LTE_FED_PRACH_WAKEUP_IND, payload: rflm_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040403, "RFLM_LTE_FED_PRACH_WAKEUP_IND", NULL ),

  /* RFLM_LTE_FED_SCHEDULER_WAKEUP_IND, payload: rflm_lte_fed_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040404, "RFLM_LTE_FED_SCHEDULER_WAKEUP_IND", NULL ),

  /* RFLM_LTE_HDET_READ_UPDATE_IND, payload: rflm_lte_hdet_read_update_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040405, "RFLM_LTE_HDET_READ_UPDATE_IND", NULL ),

  /* RFLM_LTE_FED_THERM_UPDATE_IND, payload: rflm_lte_fed_therm_ind_s */
  MSGR_UMID_TABLE_ENTRY ( 0x1a040406, "RFLM_LTE_FED_THERM_UPDATE_IND", NULL ),

#endif /* ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES)) */
};

/*! @brief Table mapping TECH/MODULE to strings */
static const msgr_module_name_entry_s  msgr_module_name_table[] = {
#if ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES))
  /* TF_MSCGEN & TF_UMID_NAMES are not defined, so no definitions */
  { 0x0000, "" },
#else
  /* MCS_MSGR */
  { 0x0001, "MSGR_MCS_MSGR" },

  /* MCS_APPMGR */
  { 0x0002, "MSGR_MCS_APPMGR" },

  /* MCS_FWS */
  { 0x0007, "MSGR_MCS_FWS" },

  /* MCS_CXM */
  { 0x000b, "MSGR_MCS_CXM" },

  /* MCS_CFCM */
  { 0x000c, "MSGR_MCS_CFCM" },

  /* MCS_APP_DSPSW */
  { 0x0010, "MSGR_MCS_APP_DSPSW" },

  /* MCS_APP_DSPFW */
  { 0x0011, "MSGR_MCS_APP_DSPFW" },

  /* MM_CM */
  { 0x0201, "MSGR_MM_CM" },

  /* MM_DOM_SEL */
  { 0x0202, "MSGR_MM_DOM_SEL" },

  /* MM_DOM_SEL_CONFIG */
  { 0x0203, "MSGR_MM_DOM_SEL_CONFIG" },

  /* MM_TUI */
  { 0x020a, "MSGR_MM_TUI" },

  /* MM_MMOC */
  { 0x0251, "MSGR_MM_MMOC" },

  /* LTE_ML1 */
  { 0x0401, "MSGR_LTE_ML1" },

  /* LTE_ML1_SM */
  { 0x0402, "MSGR_LTE_ML1_SM" },

  /* LTE_ML1_DLM */
  { 0x0403, "MSGR_LTE_ML1_DLM" },

  /* LTE_ML1_ULM */
  { 0x0404, "MSGR_LTE_ML1_ULM" },

  /* LTE_ML1_MGR */
  { 0x0405, "MSGR_LTE_ML1_MGR" },

  /* LTE_LL1_SRCH */
  { 0x0407, "MSGR_LTE_LL1_SRCH" },

  /* LTE_LL1_DL */
  { 0x0408, "MSGR_LTE_LL1_DL" },

  /* LTE_LL1_UL */
  { 0x0409, "MSGR_LTE_LL1_UL" },

  /* LTE_LL1_SYS */
  { 0x040a, "MSGR_LTE_LL1_SYS" },

  /* LTE_LL1_ASYNC */
  { 0x040b, "MSGR_LTE_LL1_ASYNC" },

  /* LTE_CPHY */
  { 0x040c, "MSGR_LTE_CPHY" },

  /* LTE_RRC */
  { 0x040d, "MSGR_LTE_RRC" },

  /* LTE_MAC */
  { 0x040f, "MSGR_LTE_MAC" },

  /* LTE_RLCDL */
  { 0x0411, "MSGR_LTE_RLCDL" },

  /* LTE_RLCUL */
  { 0x0412, "MSGR_LTE_RLCUL" },

  /* LTE_PDCPDL */
  { 0x0413, "MSGR_LTE_PDCPDL" },

  /* LTE_PDCPUL */
  { 0x0414, "MSGR_LTE_PDCPUL" },

  /* LTE_CPHY_TEST */
  { 0x041a, "MSGR_LTE_CPHY_TEST" },

  /* LTE_ML1_GM */
  { 0x041b, "MSGR_LTE_ML1_GM" },

  /* LTE_APP_DSPSW */
  { 0x041e, "MSGR_LTE_APP_DSPSW" },

  /* LTE_APP_DSPFW */
  { 0x041f, "MSGR_LTE_APP_DSPFW" },

  /* LTE_ML1_SCHDLR */
  { 0x0420, "MSGR_LTE_ML1_SCHDLR" },

  /* LTE_TLB */
  { 0x0427, "MSGR_LTE_TLB" },

  /* LTE_ML1_RFMGR */
  { 0x0429, "MSGR_LTE_ML1_RFMGR" },

  /* LTE_ML1_BPLMN */
  { 0x042a, "MSGR_LTE_ML1_BPLMN" },

  /* LTE_ML1_SLEEPMGR */
  { 0x042b, "MSGR_LTE_ML1_SLEEPMGR" },

  /* LTE_ML1_MD_GSM */
  { 0x042c, "MSGR_LTE_ML1_MD_GSM" },

  /* LTE_ML1_GAPMGR */
  { 0x042d, "MSGR_LTE_ML1_GAPMGR" },

  /* LTE_ML1_MD_WCDMA */
  { 0x042e, "MSGR_LTE_ML1_MD_WCDMA" },

  /* LTE_CPHY_IRAT_MEAS_W2L */
  { 0x042f, "MSGR_LTE_CPHY_IRAT_MEAS_W2L" },

  /* LTE_CPHY_IRAT_MEAS_G2L */
  { 0x0430, "MSGR_LTE_CPHY_IRAT_MEAS_G2L" },

  /* LTE_ML1_POS */
  { 0x0432, "MSGR_LTE_ML1_POS" },

  /* LTE_FC */
  { 0x0433, "MSGR_LTE_FC" },

  /* LTE_ML1_AFC */
  { 0x0434, "MSGR_LTE_ML1_AFC" },

  /* LTE_CPHY_IRAT_MEAS_D2L */
  { 0x0435, "MSGR_LTE_CPHY_IRAT_MEAS_D2L" },

  /* LTE_CPHY_IRAT_MEAS_C2L */
  { 0x0436, "MSGR_LTE_CPHY_IRAT_MEAS_C2L" },

  /* LTE_CPHY_IRAT_MEAS_F2L */
  { 0x0437, "MSGR_LTE_CPHY_IRAT_MEAS_F2L" },

  /* LTE_ML1_COEX */
  { 0x0438, "MSGR_LTE_ML1_COEX" },

  /* LTE_ML1_CXM */
  { 0x0439, "MSGR_LTE_ML1_CXM" },

  /* LTE_ML1_COMMON */
  { 0x043a, "MSGR_LTE_ML1_COMMON" },

  /* LTE_PDCPOFFLOAD */
  { 0x043b, "MSGR_LTE_PDCPOFFLOAD" },

  /* LTE_CPHY_IRAT_MEAS_T2L */
  { 0x043c, "MSGR_LTE_CPHY_IRAT_MEAS_T2L" },

  /* LTE_ML1_MCLK */
  { 0x043d, "MSGR_LTE_ML1_MCLK" },

  /* LTE_ML1_OFFLOAD */
  { 0x043e, "MSGR_LTE_ML1_OFFLOAD" },

  /* LTE_ML1_COEX_DSDA */
  { 0x043f, "MSGR_LTE_ML1_COEX_DSDA" },

  /* FTM_LTE_NS */
  { 0x0502, "MSGR_FTM_LTE_NS" },

  /* RFA_RF_CONTROL */
  { 0x0601, "MSGR_RFA_RF_CONTROL" },

  /* RFA_RF_GSM */
  { 0x0603, "MSGR_RFA_RF_GSM" },

  /* RFA_RF_1X */
  { 0x0604, "MSGR_RFA_RF_1X" },

  /* RFA_RF_HDR */
  { 0x0605, "MSGR_RFA_RF_HDR" },

  /* RFA_RF_GSM_FTM */
  { 0x0606, "MSGR_RFA_RF_GSM_FTM" },

  /* RFA_RF_LTE */
  { 0x0607, "MSGR_RFA_RF_LTE" },

  /* RFA_RF_LTE_FTM */
  { 0x0608, "MSGR_RFA_RF_LTE_FTM" },

  /* RFA_RF_TDSCDMA */
  { 0x060a, "MSGR_RFA_RF_TDSCDMA" },

  /* RFA_RF_TUNER */
  { 0x060b, "MSGR_RFA_RF_TUNER" },

  /* RFA_RF_MEAS */
  { 0x060c, "MSGR_RFA_RF_MEAS" },

  /* RFA_FW */
  { 0x0680, "MSGR_RFA_FW" },

  /* CDMA_FW */
  { 0x0700, "MSGR_CDMA_FW" },

  /* CDMA_SRCH_FW */
  { 0x0701, "MSGR_CDMA_SRCH_FW" },

  /* HDR_FW */
  { 0x0800, "MSGR_HDR_FW" },

  /* HDR_SRCH */
  { 0x0820, "MSGR_HDR_SRCH" },

  /* HDR_HIT */
  { 0x0827, "MSGR_HDR_HIT" },

  /* HDR_CP */
  { 0x0828, "MSGR_HDR_CP" },

  /* GERAN_FW */
  { 0x0900, "MSGR_GERAN_FW" },

  /* GERAN_FW2 */
  { 0x0901, "MSGR_GERAN_FW2" },

  /* GERAN_GRR */
  { 0x0902, "MSGR_GERAN_GRR" },

  /* GERAN_GL1 */
  { 0x0903, "MSGR_GERAN_GL1" },

  /* GERAN_FW3 */
  { 0x0904, "MSGR_GERAN_FW3" },

  /* GPS_PGI */
  { 0x0a01, "MSGR_GPS_PGI" },

  /* WCDMA_RRC */
  { 0x0b01, "MSGR_WCDMA_RRC" },

  /* DS_MSGRRECV */
  { 0x0c01, "MSGR_DS_MSGRRECV" },

  /* DS_LTE */
  { 0x0c03, "MSGR_DS_LTE" },

  /* DS_3GPP */
  { 0x0c05, "MSGR_DS_3GPP" },

  /* DS_MGR */
  { 0x0c06, "MSGR_DS_MGR" },

  /* NAS_EMM */
  { 0x0f19, "MSGR_NAS_EMM" },

  /* NAS_ESM */
  { 0x0f1c, "MSGR_NAS_ESM" },

  /* NAS_SM */
  { 0x0f1d, "MSGR_NAS_SM" },

  /* UTILS_CFM */
  { 0x1101, "MSGR_UTILS_CFM" },

  /* TDSCDMA_FW */
  { 0x1200, "MSGR_TDSCDMA_FW" },

  /* TDSCDMA_IRAT */
  { 0x1209, "MSGR_TDSCDMA_IRAT" },

  /* TDSCDMA_RRC */
  { 0x1231, "MSGR_TDSCDMA_RRC" },

  /* QMI_NAS */
  { 0x1603, "MSGR_QMI_NAS" },

  /* QMI_VOICE */
  { 0x1609, "MSGR_QMI_VOICE" },

  /* POLICYMAN_CFG */
  { 0x1801, "MSGR_POLICYMAN_CFG" },

  /* RFLM_CMN */
  { 0x1a00, "MSGR_RFLM_CMN" },

  /* RFLM_GSM */
  { 0x1a01, "MSGR_RFLM_GSM" },

  /* RFLM_LTE */
  { 0x1a04, "MSGR_RFLM_LTE" },

#endif /* ((!defined TF_MSCGEN) && (!defined TF_UMID_NAMES)) */
};


#define MSGR_MAX_MCS_MODULES 20
static uint16 msgr_mcs_jump_table[MSGR_MAX_MCS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MSGR (0x0001) */
  { 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0003, 0xffff, 0xffff, 0xffff, },
  /* APPMGR (0x0002) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FWS (0x0007) */
  { 0x0000, 0x0002, 0xffff, 0x0002, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CXM (0x000b) */
  { 0xffff, 0xffff, 0x0072, 0x0066, 0x0071, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CFCM (0x000c) */
  { 0x0000, 0xffff, 0x0001, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPSW (0x0010) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPFW (0x0011) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_MM_MODULES 84
static uint16 msgr_mm_jump_table[MSGR_MAX_MM_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CM (0x0001) */
  { 0xffff, 0xffff, 0x0020, 0x0025, 0x0037, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* DOM_SEL (0x0002) */
  { 0xffff, 0xffff, 0x0010, 0x0011, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* DOM_SEL_CONFIG (0x0003) */
  { 0xffff, 0xffff, 0x000d, 0x000b, 0x000f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* TUI (0x000a) */
  { 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0020)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0022)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0023)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0024)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0025)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0026)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0027)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0028)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0029)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0030)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0031)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0032)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0033)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0034)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0035)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0036)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0037)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0038)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0039)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0040)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0041)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0042)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0043)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0044)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0045)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0046)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0047)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0048)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0049)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0050)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MMOC (0x0051) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0052)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0053)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_LTE_MODULES 66
static uint16 msgr_lte_jump_table[MSGR_MAX_LTE_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1 (0x0001) */
  { 0x0003, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_SM (0x0002) */
  { 0xffff, 0xffff, 0x003f, 0xffff, 0x0045, 0xffff, 0xffff, 0xffff, 0x0039, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_DLM (0x0003) */
  { 0xffff, 0xffff, 0x0073, 0xffff, 0x0077, 0xffff, 0xffff, 0xffff, 0x0079, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_ULM (0x0004) */
  { 0xffff, 0xffff, 0x000d, 0xffff, 0x000c, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_MGR (0x0005) */
  { 0xffff, 0xffff, 0x0061, 0xffff, 0x0060, 0xffff, 0xffff, 0xffff, 0x0061, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LL1_SRCH (0x0007) */
  { 0xffff, 0xffff, 0x0013, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x000c, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LL1_DL (0x0008) */
  { 0xffff, 0xffff, 0x0030, 0xffff, 0x0006, 0xffff, 0xffff, 0xffff, 0x001a, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LL1_UL (0x0009) */
  { 0xffff, 0xffff, 0x000c, 0xffff, 0x0004, 0xffff, 0xffff, 0xffff, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LL1_SYS (0x000a) */
  { 0x0003, 0xffff, 0x001d, 0xffff, 0x0027, 0xffff, 0xffff, 0xffff, 0x0018, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LL1_ASYNC (0x000b) */
  { 0xffff, 0xffff, 0x000b, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY (0x000c) */
  { 0xffff, 0xffff, 0x0045, 0xffff, 0x0048, 0xffff, 0xffff, 0xffff, 0x0043, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RRC (0x000d) */
  { 0x0007, 0xffff, 0x009e, 0x009e, 0x0089, 0xffff, 0xffff, 0x0010, 0x0012, 0xffff, 0xffff, 0x0044, 0xffff, 0x0044, 0x003a, 0x003e, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MAC (0x000f) */
  { 0x0003, 0xffff, 0x0008, 0xffff, 0x000c, 0xffff, 0xffff, 0xffff, 0x0006, 0xffff, 0xffff, 0x0004, 0xffff, 0x0006, 0x0003, 0x0007, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RLCDL (0x0011) */
  { 0x0003, 0xffff, 0x0005, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0x0003, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, },
  /* RLCUL (0x0012) */
  { 0x0003, 0xffff, 0x0004, 0xffff, 0x0006, 0xffff, 0xffff, 0xffff, 0x0003, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, },
  /* PDCPDL (0x0013) */
  { 0x0003, 0xffff, 0x0010, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, },
  /* PDCPUL (0x0014) */
  { 0x0002, 0xffff, 0x000f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x000a, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0002, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_TEST (0x001a) */
  { 0xffff, 0xffff, 0x001f, 0xffff, 0x0024, 0xffff, 0xffff, 0xffff, 0x0018, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_GM (0x001b) */
  { 0x0003, 0xffff, 0x0035, 0xffff, 0x0037, 0xffff, 0xffff, 0xffff, 0x0035, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPSW (0x001e) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* APP_DSPFW (0x001f) */
  { 0x0003, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_SCHDLR (0x0020) */
  { 0xffff, 0xffff, 0x000d, 0xffff, 0x000c, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0022)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0023)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0024)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0025)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0026)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* TLB (0x0027) */
  { 0x0003, 0xffff, 0x0001, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0028)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_RFMGR (0x0029) */
  { 0xffff, 0xffff, 0x0017, 0xffff, 0x0039, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_BPLMN (0x002a) */
  { 0xffff, 0xffff, 0x000f, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_SLEEPMGR (0x002b) */
  { 0xffff, 0xffff, 0x000f, 0xffff, 0x000f, 0xffff, 0xffff, 0xffff, 0x0004, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_MD_GSM (0x002c) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0004, 0xffff, 0xffff, 0xffff, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_GAPMGR (0x002d) */
  { 0xffff, 0xffff, 0x0010, 0xffff, 0x0004, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_MD_WCDMA (0x002e) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_W2L (0x002f) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_G2L (0x0030) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0031)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_POS (0x0032) */
  { 0xffff, 0xffff, 0x0072, 0xffff, 0x0051, 0xffff, 0xffff, 0xffff, 0x000f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FC (0x0033) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_AFC (0x0034) */
  { 0xffff, 0xffff, 0x000a, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_D2L (0x0035) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_C2L (0x0036) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_F2L (0x0037) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_COEX (0x0038) */
  { 0xffff, 0xffff, 0x0002, 0xffff, 0x0006, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_CXM (0x0039) */
  { 0x0003, 0xffff, 0x000a, 0xffff, 0x000d, 0xffff, 0xffff, 0xffff, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_COMMON (0x003a) */
  { 0xffff, 0xffff, 0x0002, 0xffff, 0x0013, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* PDCPOFFLOAD (0x003b) */
  { 0x0003, 0xffff, 0x0002, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CPHY_IRAT_MEAS_T2L (0x003c) */
  { 0xffff, 0x0003, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_MCLK (0x003d) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0010, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_OFFLOAD (0x003e) */
  { 0x0003, 0xffff, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ML1_COEX_DSDA (0x003f) */
  { 0xffff, 0xffff, 0x0005, 0xffff, 0x000e, 0xffff, 0xffff, 0xffff, 0x0006, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0040)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0041)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_FTM_MODULES 5
static uint16 msgr_ftm_jump_table[MSGR_MAX_FTM_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LTE_NS (0x0002) */
  { 0xffff, 0xffff, 0x0010, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0x0010, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_RFA_MODULES 131
static uint16 msgr_rfa_jump_table[MSGR_MAX_RFA_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_CONTROL (0x0001) */
  { 0x0003, 0x0000, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_GSM (0x0003) */
  { 0xffff, 0xffff, 0x0058, 0xffff, 0x0051, 0xffff, 0xffff, 0xffff, 0x0058, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_1X (0x0004) */
  { 0xffff, 0x0001, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_HDR (0x0005) */
  { 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_GSM_FTM (0x0006) */
  { 0xffff, 0x0026, 0xffff, 0x0026, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_LTE (0x0007) */
  { 0xffff, 0xffff, 0x00bf, 0xffff, 0x00c0, 0xffff, 0xffff, 0xffff, 0x00bf, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_LTE_FTM (0x0008) */
  { 0xffff, 0x0023, 0xffff, 0x00ff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_TDSCDMA (0x000a) */
  { 0xffff, 0xffff, 0x0021, 0xffff, 0x001f, 0xffff, 0xffff, 0xffff, 0x0021, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_TUNER (0x000b) */
  { 0xffff, 0x0003, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RF_MEAS (0x000c) */
  { 0xffff, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0020)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0022)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0023)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0024)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0025)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0026)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0027)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0028)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0029)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0030)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0031)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0032)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0033)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0034)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0035)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0036)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0037)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0038)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0039)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x003f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0040)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0041)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0042)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0043)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0044)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0045)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0046)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0047)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0048)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0049)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x004f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0050)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0051)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0052)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0053)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0054)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0055)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0056)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0057)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0058)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0059)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x005f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0060)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0061)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0062)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0063)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0064)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0065)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0066)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0067)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0068)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0069)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x006f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0070)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0071)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0072)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0073)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0074)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0075)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0076)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0077)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0078)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0079)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x007f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FW (0x0080) */
  { 0xffff, 0x0007, 0xffff, 0x000a, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0081)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0082)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_CDMA_MODULES 4
static uint16 msgr_cdma_jump_table[MSGR_MAX_CDMA_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* FW (0x0000) */
  { 0x0001, 0x004a, 0xffff, 0x0013, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* SRCH_FW (0x0001) */
  { 0x0001, 0x0004, 0xffff, 0x0007, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_HDR_MODULES 43
static uint16 msgr_hdr_jump_table[MSGR_MAX_HDR_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* FW (0x0000) */
  { 0xffff, 0x0074, 0xffff, 0x0017, 0x000e, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* SRCH (0x0020) */
  { 0xffff, 0xffff, 0x0002, 0x0001, 0x0000, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0022)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0023)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0024)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0025)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0026)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* HIT (0x0027) */
  { 0xffff, 0x0012, 0xffff, 0x0012, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CP (0x0028) */
  { 0x0001, 0xffff, 0x0028, 0x000b, 0x0008, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0029)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_GERAN_MODULES 7
static uint16 msgr_geran_jump_table[MSGR_MAX_GERAN_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* FW (0x0000) */
  { 0xffff, 0x0016, 0xffff, 0x0018, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FW2 (0x0001) */
  { 0xffff, 0x0016, 0xffff, 0x0018, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* GRR (0x0002) */
  { 0xffff, 0xffff, 0x0013, 0x0012, 0x0004, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* GL1 (0x0003) */
  { 0xffff, 0xffff, 0x0007, 0x0008, 0x0009, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* FW3 (0x0004) */
  { 0xffff, 0x0016, 0xffff, 0x0018, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_GPS_MODULES 4
static uint16 msgr_gps_jump_table[MSGR_MAX_GPS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* PGI (0x0001) */
  { 0xffff, 0x0000, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_WCDMA_MODULES 4
static uint16 msgr_wcdma_jump_table[MSGR_MAX_WCDMA_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RRC (0x0001) */
  { 0xffff, 0xffff, 0x000d, 0x000d, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_DS_MODULES 9
static uint16 msgr_ds_jump_table[MSGR_MAX_DS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MSGRRECV (0x0001) */
  { 0x0003, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LTE (0x0003) */
  { 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* 3GPP (0x0005) */
  { 0xffff, 0xffff, 0x0002, 0xffff, 0x0021, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* MGR (0x0006) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0024, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_NAS_MODULES 32
static uint16 msgr_nas_jump_table[MSGR_MAX_NAS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0009)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* EMM (0x0019) */
  { 0xffff, 0x0007, 0x0006, 0xffff, 0x000e, 0xffff, 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* ESM (0x001c) */
  { 0xffff, 0xffff, 0x0011, 0x000c, 0x000d, 0xffff, 0xffff, 0xffff, 0x000f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, },
  /* SM (0x001d) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_UTILS_MODULES 4
static uint16 msgr_utils_jump_table[MSGR_MAX_UTILS_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CFM (0x0001) */
  { 0x0003, 0x0000, 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0000, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_TDSCDMA_MODULES 52
static uint16 msgr_tdscdma_jump_table[MSGR_MAX_TDSCDMA_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* FW (0x0000) */
  { 0xffff, 0x0096, 0xffff, 0x0052, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0020, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* IRAT (0x0009) */
  { 0xffff, 0x0015, 0x0013, 0x0013, 0xffff, 0xffff, 0xffff, 0xffff, 0x0015, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0010)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0011)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0012)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0013)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0014)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0015)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0016)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0017)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0018)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0019)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x001f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0020)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0021)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0022)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0023)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0024)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0025)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0026)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0027)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0028)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0029)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002c)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002d)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002e)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x002f)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0030)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* RRC (0x0031) */
  { 0xffff, 0xffff, 0x0011, 0x000f, 0x0003, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0032)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0033)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_QMI_MODULES 12
static uint16 msgr_qmi_jump_table[MSGR_MAX_QMI_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0001)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* NAS (0x0003) */
  { 0xffff, 0x0005, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0004)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0007)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0008)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* VOICE (0x0009) */
  { 0xffff, 0x000b, 0xffff, 0xffff, 0x0001, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000a)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x000b)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_POLICYMAN_MODULES 4
static uint16 msgr_policyman_jump_table[MSGR_MAX_POLICYMAN_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* Unused (0x0000)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* CFG (0x0001) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_MAX_RFLM_MODULES 7
static uint16 msgr_rflm_jump_table[MSGR_MAX_RFLM_MODULES][MSGR_NUM_TYPES] = 
{ /*   SPR,    CMD,    REQ,    RSP,    IND,    FLM,    OTA,    DLM,    CNF,    TMR,   CMDI,   REQI,   RSPI,   INDI,   CNFI,   TMRI,   INTI,  */
  /* CMN (0x0000) */
  { 0xffff, 0xffff, 0xffff, 0x0002, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* GSM (0x0001) */
  { 0xffff, 0xffff, 0xffff, 0x0040, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0002)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0003)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* LTE (0x0004) */
  { 0xffff, 0xffff, 0xffff, 0xffff, 0x0006, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0005)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
  /* Unused (0x0006)*/
  { 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, },
};

#define MSGR_NUM_TECHS 27
static msgr_tech_jump_entry_s msgr_gen_jump_table[MSGR_NUM_TECHS] = 
{
  { (uint16*)msgr_mcs_jump_table, 20 },
  { NULL, 0 },/* UMB (no messages) */
  { (uint16*)msgr_mm_jump_table, 84 },
  { NULL, 0 },/* WMX (no messages) */
  { (uint16*)msgr_lte_jump_table, 66 },
  { (uint16*)msgr_ftm_jump_table, 5 },
  { (uint16*)msgr_rfa_jump_table, 131 },
  { (uint16*)msgr_cdma_jump_table, 4 },
  { (uint16*)msgr_hdr_jump_table, 43 },
  { (uint16*)msgr_geran_jump_table, 7 },
  { (uint16*)msgr_gps_jump_table, 4 },
  { (uint16*)msgr_wcdma_jump_table, 4 },
  { (uint16*)msgr_ds_jump_table, 9 },
  { NULL, 0 },/* ONEX (no messages) */
  { NULL, 0 },/* C2K (no messages) */
  { (uint16*)msgr_nas_jump_table, 32 },
  { NULL, 0 },/* UIM (no messages) */
  { (uint16*)msgr_utils_jump_table, 4 },
  { (uint16*)msgr_tdscdma_jump_table, 52 },
  { NULL, 0 },/* WMS (no messages) */
  { NULL, 0 },/* CNE (no messages) */
  { NULL, 0 },/* IMS (no messages) */
  { (uint16*)msgr_qmi_jump_table, 12 },
  { NULL, 0 },/* ECALL (no messages) */
  { (uint16*)msgr_policyman_jump_table, 4 },
  { NULL, 0 },/* CORE (no messages) */
  { (uint16*)msgr_rflm_jump_table, 7 },
};

#define MSGR_NUM_TOTAL_MSGS 3102


/* Defines Number of Unique Combinations of Tech/Mod/Type in the system 
   when MSGR gets compiled */

#define MSGR_NUM_TECH_MOD_TYPE 275

/* Defines Number of Reg Nodes Slot Needed for Known UMIDS 
   when MSGR gets compiled */

#define MSGR_NUM_REGULAR_SLOTS 479

/* The size of the top-level jump table,
   which points to the correct tech-specific table.*/
#define MSGR_JUMP_TABLE_SIZE  (MSGR_NUM_TECHS * sizeof(msgr_tech_jump_entry_s))

/* The total size of all the tech-specific tables. For each TECH, one row for 
   each module. Each jump table is a 2D array of max module rows and num type 
   columns each of which is an uint16 */
#define MSGR_TECH_JUMP_TABLE_SIZE  (488 * MSGR_NUM_TYPES * sizeof(uint16))