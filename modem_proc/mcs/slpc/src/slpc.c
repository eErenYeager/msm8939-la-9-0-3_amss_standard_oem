/*=========================================================================
  FILE: slpc.c
 
  OVERVIEW:
 

  DEPENDENCIES:
 
 
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
=========================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE
 
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mcs/slpc/src/slpc.c#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who   what, where, why
----------   ---   ----------------------------------------------------------- 
05/31/2013   cab   Added support for TSTS
04/24/2013   cab   Added support for N-way technology slam 
04/04/2013   cab   Bumped up threshold for ustmr/qtimer sync due to SW exec 
03/29/2013   cab   Fix issue with ustmr rollover at start tick
03/21/2013   cab   Add case for second GSM client to tstmr write routine 
03/12/2013   cab   Revert start tick changes
03/12/2013   cab   Increased robustness when capturing start tick
01/30/2013   cab   Increased wakeup timer priority
12/11/2012   cab   Add notify callback deregister capability 
12/04/2012   cab   Add alternate tstmr to slpc_start for W/G operation
09/12/2012   cab   Fixed get_sysclk_count to correctly handle negative err 
08/29/2012   cab   Revert to timer_set_64 
08/17/2012   cab   Fix bug with dalsys sync object
08/10/2012   cab   Move to use absolute timer set
08/10/2012   cab   Update min warmup comparison to use signed ints
08/10/2012   cab   Update print statements to unsigned format where needed
07/16/2012   cab   Added alternate id for G/W simultaneous operation 
07/16/2012   cab   Added hardware macros, added slpc_sufficient_warmup_time
04/13/2012   cab   Removed USTMR init, moving to MCPM 
04/11/2012   cab   Update to new timer set interface
09/14/2011   cab   Initial version 

============================================================================*/



/*============================================================================

                           INCLUDE FILES

============================================================================*/

#include <mqueue.h>
#include <fcntl.h>
#include <qurt_timer.h>

#include "slpc_hwio.h"
#include <rcinit_rex.h>
#include "rcinit.h"
#include "qurt_mutex.h"
#include "qurt_anysignal.h"
#include "amssassert.h"
#include "DALSys.h"
#include "DALStdErr.h"
#include "DALDeviceId.h"
#include "DalDevice.h"
#include "DDIInterruptController.h"
#include "DDIChipInfo.h"
#include "err.h"
#include "msg.h"
#include "timer.h"
#include "tcxomgr.h"
#include "slpc.h"
#include "slpci.h"

#ifdef SLPC_VSTMR
#include "vstmr.h"
#include "vstmr_1x.h"
#include "vstmr_hdr.h"
#include "vstmr_geran.h"
#include "vstmr_wcdma.h"
#include "vstmr_lte.h"
#include "vstmr_tds.h"
#endif


/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typedefs,
and other items needed by this module.

============================================================================*/

/*----------------------------------------------------------------------------
  Global constants
----------------------------------------------------------------------------*/

#define ISR_LATENCY_THRESH          57600  /* 3ms */

#define SLPC_GSM_SYS_FREQ           ( 1625000uLL * 8uLL )
#define SLPC_1X_SYS_FREQ            ( 1228800uLL * 8uLL )
#define SLPC_HDR_SYS_FREQ           ( 1228800uLL * 8uLL )
#define SLPC_WCDMA_SYS_FREQ         ( 3840000uLL * 8uLL )
#define SLPC_LTE_SYS_FREQ           ( 3840000uLL * 8uLL )
#define SLPC_TDSCDMA_SYS_FREQ       ( 1280000uLL * 8uLL )
#define SLPC_GSM_ALT_SYS_FREQ       ( 1625000uLL * 8uLL )

#define SLPC_GSM_K                  ( 23 )
#define SLPC_1X_K                   ( 21 )
#define SLPC_HDR_K                  ( 21 )
#define SLPC_WCDMA_K                ( 25 )
#define SLPC_LTE_K                  ( 28 )
#define SLPC_TDSCDMA_K              ( 23 )
#define SLPC_GSM_ALT_K              ( 23 )

/* maximum allowed error to accumulate for tracking (3ppm)*/
#define SLPC_MAX_ACCUM_ERR          ( 3 * 1024 )

/* Number of ustmr timer hardware resources */
#define SLPC_MAX_RESOURCES                  3

/* Loop count timeout for read sync */
#define SLPC_READ_SYNC_LOOP_TIMEOUT         20

/* if qtimer and ustmr out of sync more than this many ticks, declare fatal */
#define SLPC_USTMR_SYNC_THRESH              5000

/* value to represent an invalid resource, or resource not held */
#define SLPC_NO_RESOURCE                   -1

/* signal that can be sent to slpc task */
#define SLPC_SIG_MASK                       0x2

/* size of messages to task */
#define SLPC_MSG_SIZE                       2

/* upper part of timetick not captured in ustmr count */
#define SLPC_TIMETICK_UPPER_MASK            0xFFFFFFFFF8000000uLL

#ifdef SLPC_MDM9x35
/* lower part of timetick captured in ustmr count */
#define SLPC_TIMETICK_LOWER_MASK            0x0000000000FFFFFFuLL
#else
/* lower part of timetick captured in ustmr count */
#define SLPC_TIMETICK_LOWER_MASK            0x0000000007FFFFFFuLL
#endif
/* max number of clients that can register for wakeup changes */
#define SLPC_MAX_WAKEUP_CALLBACKS  10

/* time required to slam vstmr in xo ticks */
#define SLPC_VSTMR_SLAM_EXEC_DUR  3840

/*
  Blast OS can return timer callbacks up to 100 usec early, so that error
  must be accounted for when sanity checking the wakeup timer.
  Double the buffer for safety.
*/
/* 200 usec = 3840 xo ticks */
#define SLPC_WAKEUP_TIMER_ERR      3840

DALSYSSyncObj slpc_sync_obj;
DALSYSSyncHandle slpc_sync_ptr;
#define INIT_ATOMIC_SECTION() \
  if (DAL_SUCCESS != DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, \
                     &slpc_sync_ptr, \
                     &slpc_sync_obj)) \
  { \
    ERR_FATAL("sleepctl_init: DALSYS_SyncCreate failed!", 0, 0, 0); \
  } 
#define ENTER_ATOMIC_SECTION() DALSYS_SyncEnter(slpc_sync_ptr)
#define LEAVE_ATOMIC_SECTION() DALSYS_SyncLeave(slpc_sync_ptr)

/* absolute value macro */
#define  ABS( x ) ( ((x) < 0) ? -(x) : (x) )

/*----------------------------------------------------------------------------
  Interrupt data
----------------------------------------------------------------------------*/

#define SLPC_RSRC0_INT_NUM                  96
#define SLPC_RSRC1_INT_NUM                  97
#define SLPC_RSRC2_INT_NUM                  223
#define SLPC_RSRC0_INT_TRIGGER DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER
#define SLPC_RSRC1_INT_TRIGGER DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER
#define SLPC_RSRC2_INT_TRIGGER DALINTRCTRL_ENABLE_RISING_EDGE_TRIGGER

enum
{
  SLPC_WAKEUP_EVENT,
  SLPC_OLS_EVENT
};

DalChipInfoFamilyType slpc_c_fam;
DalChipInfoVersionType slpc_c_ver;

typedef union 
{
  uint64 ud;
  uint32 uw[2];
}
slpc_dword_type;


/*----------------------------------------------------------------------------
  Sleep controller state
----------------------------------------------------------------------------*/

typedef enum
{
  /* Sleep controller in not active */
  SLPC_INACTIVE_STATE,

  /* Sleep controller has captured start point */
  SLPC_START_STATE,

  /* Sleep controller is counting slow clocks until the "wakeup" point */
  SLPC_SLEEP_STATE,

  /* Wakeup point has been reached.  Next: Warmup, or back to sleep? */
  SLPC_WAKEUP_STATE,

  /* Sleep controller is counting slow clocks until the end of sleep */
  SLPC_WARMUP_STATE
}
slpc_state_enum;


/*----------------------------------------------------------------------------
  Sleep start data
----------------------------------------------------------------------------*/

typedef struct
{
  /* USTMR value at read sync */
  ustmr_type                      ustmr_ts;

  /* TSTMR value at read sync */
  slpc_tstmr_type                 tstmr;

  /* timetick value at read sync */
  uint64                          tick;
}
slpc_start_type;


/*----------------------------------------------------------------------------
  Warmup data
----------------------------------------------------------------------------*/

typedef struct
{
  /* Warmup duration, in microseconds (Max = 65.535ms) */
  uint16                          dur_usec;

  /* Warmup duration, in timeticks */
  uint64                          dur_tick;

  /* Timestamp when we transition to warmup period */
  uint64                          tick;
}
slpc_warmup_type;


/*----------------------------------------------------------------------------
  Wakeup data
----------------------------------------------------------------------------*/

typedef struct
{
  /* Wakeup event notification callback */
  slpc_event_callback_type        cb;

  /* Wakeup event start timestamp, in XO units */
  uint64                          cb_start_tick;

  /* Wakeup event end timestamp, in XO units */
  uint64                          cb_end_tick;

  /* Timer group to be defined to non-deferrable */
  timer_group_type                slpc_timer_group;

  /* Timer used to generate wakeup event */
  timer_type                      timer;

  /* Number of times we recorded invalid wakeup event */
  uint16                          invalid_cnt;

  /* timetick value at wakeup */
  uint64                          tick;

  /* timetick value of last wakeup */
  uint64                          last;

  /* Indicates if wakeup point is an update of an earlier wakeup */
  boolean                         update;

  /* Indicates if wakeup point is an extension of an earlier wakeup */
  boolean                         extension;
}
slpc_wakeup_type;


/*----------------------------------------------------------------------------
  Sleep ending data
----------------------------------------------------------------------------*/

typedef struct
{
  /* Sleep ending event notification callback */
  slpc_event_callback_type        cb;

  /* ustmr value at ending point */
  ustmr_type                      ustmr_ts;

  /* OLS event start timestamp, in XO units */
  uint64                          isr_start_tick;

  /* OLS event end timestamp, in XO units */
  uint64                          isr_end_tick;

  /* Number of times we recorded invalid OLS interrupt */
  uint16                          invalid_cnt;

  /* timetick value at ending point */
  uint64                          tick;
}
slpc_ending_type;



/*----------------------------------------------------------------------------
  Last sleep data - used for slpc_error_feedback
----------------------------------------------------------------------------*/

typedef struct
{
  /* Last sleep duration, in universal STMR ticks */
  tstmr_type                      duration_tstmr;

  /* Last sleep duration, in technology STMR ticks */
  ustmr_type                      duration_ustmr;
}
slpc_last_sleep_type;

typedef struct
{
  /* flag to enable alternate stmr activation on online pulse */
  boolean                              valid;

  /* flag to enable alternate stmr activation on online pulse */
  boolean                              load_tstmr;

  /* the time value of this tstmr at start time transfer point */
  uint64                               start_time;

  /* the phase value of this tstmr at start time transfer point */
  uint64                               start_phase;

  /* value to load in alternate tech stmr at online pulse */
  uint64                               tstmr;
}
slpc_alt_id_type;

/*----------------------------------------------------------------------------
  Per technology data
----------------------------------------------------------------------------*/

typedef struct
{
  /* Sleep controller identifier */
  const char *                         id;

  /* Software state */
  slpc_state_enum                      state;

  /* Tech system frequency */
  uint64                               system_freq;

  /* Scaling needed in dyn gain err feedback */
  uint8                                k;

  /* TCXO Manager id's required for retrieving tech specific RGS data */
  tcxomgr_client_info_struct_type      tm_client;

  /* Bit value used to set sys online ctrl register to this tech */
  uint32                               route_mask;

  /* ustmr resource used by this client */
  int8                                 slpc_ustmr_rsrc;

  /* Accumulated slew error */
  int64                                slew_err_accum;

  /* Start of sleep data */
  slpc_start_type                      start;

  /* RF warmup time constants */
  slpc_warmup_type                     warmup;

  /* Wakeup event data */
  slpc_wakeup_type                     wakeup;

  /* End of sleep data */
  slpc_ending_type                     ending;

  /* Last sleep duration, for error_feedback */
  slpc_last_sleep_type                 last_sleep;

  /* Total sleep duration, in USTMR ticks */
  uint64                               ustmr_dur;

  /* Total sleep duration, in TSTMR ticks */
  uint64                               tstmr_dur;

  /* Total sleep duration, in TSTMR ticks, of last period */
  uint64                               last_tstmr_dur;

  /* value to load in tech stmr at online pulse */
  uint64                               tstmr_slam;

  /* List of other techs where timing must be maintained */
  slpc_alt_id_type                     alt_techs[SLPC_NUM_CLIENTS];
}
slpc_client_type;

static slpc_client_type slpc[SLPC_NUM_CLIENTS] =
{
  { "GSM", SLPC_INACTIVE_STATE, SLPC_GSM_SYS_FREQ, SLPC_GSM_K, 
    { TCXOMGR_CLIENT_GSM, TCXOMGR_AS_ID_1 } },

  { "1X", SLPC_INACTIVE_STATE, SLPC_1X_SYS_FREQ, SLPC_1X_K, 
    { TCXOMGR_CLIENT_CDMA_1X, TCXOMGR_AS_ID_1 } }, 

  { "HDR", SLPC_INACTIVE_STATE, SLPC_HDR_SYS_FREQ, SLPC_HDR_K, 
    { TCXOMGR_CLIENT_CDMA_HDR, TCXOMGR_AS_ID_1 } },

  { "WCDMA", SLPC_INACTIVE_STATE, SLPC_WCDMA_SYS_FREQ, SLPC_WCDMA_K, 
    { TCXOMGR_CLIENT_WCDMA, TCXOMGR_AS_ID_1 } },

  { "LTE", SLPC_INACTIVE_STATE, SLPC_LTE_SYS_FREQ, SLPC_LTE_K, 
    { TCXOMGR_CLIENT_LTE, TCXOMGR_AS_ID_1 } },

  { "TDSCDMA", SLPC_INACTIVE_STATE, SLPC_TDSCDMA_SYS_FREQ, SLPC_TDSCDMA_K, 
    { TCXOMGR_CLIENT_TDSCDMA, TCXOMGR_AS_ID_1 } },

  { "GSM2", SLPC_INACTIVE_STATE, SLPC_GSM_ALT_SYS_FREQ, SLPC_GSM_ALT_K, 
    { TCXOMGR_CLIENT_GSM, TCXOMGR_AS_ID_2 } },

  { "GSM3", SLPC_INACTIVE_STATE, SLPC_GSM_ALT_SYS_FREQ, SLPC_GSM_ALT_K, 
    { TCXOMGR_CLIENT_GSM, TCXOMGR_AS_ID_3 } }
};

/*----------------------------------------------------------------------------
  Forward declarations
----------------------------------------------------------------------------*/
static void slpc_ols_event( slpc_id_type id );
static void slpc_ustmr0_isr( void );
static void slpc_ustmr1_isr( void );
static void slpc_ustmr2_isr( void );

/*----------------------------------------------------------------------------
  USTMR resource data
----------------------------------------------------------------------------*/

typedef struct
{
  /* indicates this resource is in use by a client */
  boolean in_use;

  /* equivalent of "tramp irq"...the specific id of the interrupt */
  DALInterruptID int_number; 

  /* edge/level and high/low configuration information */
  uint32 int_trigger;

  /* pointer to the actual isr routine */
  DALISR isr_function;

  /* id of client using this resource */
  slpc_id_type id;
}
slpc_ustmr_rsrc_type;

#ifdef SLPC_VSTMR

typedef struct
{
  /* gsm1 technology view */
  vstmr_geran_view_s *gsm_view1;

  /* gsm2 technology view */
  vstmr_geran_view_s *gsm_view2;

  /* gsm3 technology view */
  vstmr_geran_view_s *gsm_view3;

  /* 1x technology view */
  vstmr_1x_view_s *onex_view;

  /* wcdma technology view */
  vstmr_wcdma_view_s *wcdma_view;

  /* hdr technology view */
  vstmr_hdr_view_s *hdr_view;

  /* lte technology view */
  vstmr_lte_view_s *lte_view;

  /* tdscdma technology view */
  vstmr_tds_view_s *tdscdma_view;
}
slpc_vstmr_data_type;

#endif

/*----------------------------------------------------------------------------
  Overall data
----------------------------------------------------------------------------*/

typedef struct
{
  /* flags for whether a resource is being used currently */
  slpc_ustmr_rsrc_type slpc_ustmr_rsrc[SLPC_MAX_RESOURCES];

  /* for atomic sections within slpc module */
  qurt_mutex_t slpc_lock;

  /* callbacks for anyone wanting to track wakeup point changes */
  slpc_notify_cb_type notify_cb[SLPC_MAX_WAKEUP_CALLBACKS];

  /* callbacks for anyone wanting wakeup event notifications */
  slpc_notify_wakeup_cb_type notify_wakeup_cb[SLPC_MAX_WAKEUP_CALLBACKS];

  /* handle used for all interrupt servicing */
  DalDeviceHandle *int_handle;

  /* handle used for timetick services */
  DalDeviceHandle *tt_handle;

  /* signal registered with OS */
  qurt_anysignal_t slpc_q6_signal;

  /* message queue for communicating with task */
  mqd_t msg_queue;

#ifdef SLPC_VSTMR
  /* handles and other data required in vstmr interface */
  slpc_vstmr_data_type vstmr_data;
#endif
}
slpc_type;

static slpc_type slpc_data =
{
  { 
    {FALSE, SLPC_RSRC0_INT_NUM, SLPC_RSRC0_INT_TRIGGER, (DALISR) slpc_ustmr0_isr},
    {FALSE, SLPC_RSRC1_INT_NUM, SLPC_RSRC1_INT_TRIGGER, (DALISR) slpc_ustmr1_isr},
    {FALSE, SLPC_RSRC2_INT_NUM, SLPC_RSRC2_INT_TRIGGER, (DALISR) slpc_ustmr2_isr}
  }
};

static uint8 slpc_num_rsrc;

/*----------------------------------------------------------------------------
  Function definitions
----------------------------------------------------------------------------*/

/******************************************************************************
  @brief SLPC_IS_ACTIVE
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
boolean slpc_is_active( slpc_id_type id )
{
  return ( slpc[id].state != SLPC_INACTIVE_STATE ? TRUE : FALSE );
}

/******************************************************************************
  @brief SLPC_WAKEUP_TIMER_CB
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_wakeup_timer_cb( slpc_id_type id )
{
  char slpc_msg[SLPC_MSG_SIZE] = { SLPC_WAKEUP_EVENT, id };
  TRACE(id, WAKEUP_TIMER_CB, 0, 0, 0); 
  mq_send( slpc_data.msg_queue, slpc_msg, SLPC_MSG_SIZE, MQ_PRIO_DEFAULT );
}

/******************************************************************************
  @brief SLPC_USTMR0_ISR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_ustmr0_isr( void )
{
  char slpc_msg[SLPC_MSG_SIZE] = 
       { SLPC_OLS_EVENT, slpc_data.slpc_ustmr_rsrc[0].id };
  mq_send( slpc_data.msg_queue, slpc_msg, SLPC_MSG_SIZE, MQ_PRIO_DEFAULT );
}

/******************************************************************************
  @brief SLPC_USTMR1_ISR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_ustmr1_isr( void )
{
  char slpc_msg[SLPC_MSG_SIZE] = 
       { SLPC_OLS_EVENT, slpc_data.slpc_ustmr_rsrc[1].id };
  mq_send( slpc_data.msg_queue, slpc_msg, SLPC_MSG_SIZE, MQ_PRIO_DEFAULT );
}

/******************************************************************************
  @brief SLPC_USTMR2_ISR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_ustmr2_isr( void )
{
  char slpc_msg[SLPC_MSG_SIZE] = 
       { SLPC_OLS_EVENT, slpc_data.slpc_ustmr_rsrc[2].id };
  mq_send( slpc_data.msg_queue, slpc_msg, SLPC_MSG_SIZE, MQ_PRIO_DEFAULT );
}

/******************************************************************************
  @brief SLPC_GET_TICK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
uint64 slpc_get_tick ( void )
{
  uint64 tick = 0;
  uint32 offset = 0;

  if (DAL_SUCCESS != DalTimetick_GetTimetick64( 
                     slpc_data.tt_handle, (DalTimetickTime64Type *) &tick ))
  {
    ERR_FATAL("timetick error!", 0, 0, 0); 
  }

  DalTimetick_GetOffset( slpc_data.tt_handle, &offset );

  return (tick - offset);
}

/******************************************************************************
  @brief SLPC_GET_START_TSTMR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
slpc_tstmr_type slpc_get_start_tstmr( slpc_id_type id, slpc_id_type alt_id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Storage for return of alt_id tech stmr value at time transfer */
  slpc_tstmr_type tstmr;

/*--------------------------------------------------------------------------*/

  ASSERT( id < SLPC_NUM_CLIENTS );
  ASSERT( alt_id < SLPC_NUM_CLIENTS );

  tstmr.time = sc->alt_techs[alt_id].start_time;
  tstmr.phase = sc->alt_techs[alt_id].start_phase;

  return tstmr;
}

#ifdef SLPC_VSTMR
/******************************************************************************
  @brief SLPC_VSTMR_TSTMR_READ_START
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static slpc_start_type slpc_vstmr_tstmr_read_start( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* return value */
  slpc_start_type start_ts = {0, {0, 0, 0, 0}, 0};

  /* local copy of tech dependent stmr formatted value */
  vstmr_gstmr_t vstmr_gstmr;
  vstmr_wstmr_t vstmr_wstmr;
  vstmr_1xframe_t vstmr_1xframe;
  vstmr_hstmr_t vstmr_hstmr;
  vstmr_ostmr_t vstmr_ostmr;
  vstmr_tdsstmr_t vstmr_tdsstmr;

/*--------------------------------------------------------------------------*/

  ASSERT( id < SLPC_NUM_CLIENTS );

  /* read the current XO value */
  start_ts.tick = VSTMR_XO_READ_FULL();
  start_ts.ustmr_ts = start_ts.tick & SLPC_TIMETICK_LOWER_MASK;

  /* find corresponding tstmr value for current xo, for all techs */
  vstmr_gstmr = vstmr_geran_xo_to_gstmr(slpc_data.vstmr_data.gsm_view1, 
                                        (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_GSM].start_time = vstmr_gstmr.all;
  vstmr_gstmr = vstmr_geran_xo_to_gstmr(slpc_data.vstmr_data.gsm_view2, 
                                        (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_GSM2].start_time = vstmr_gstmr.all;
  vstmr_gstmr = vstmr_geran_xo_to_gstmr(slpc_data.vstmr_data.gsm_view3, 
                                        (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_GSM3].start_time = vstmr_gstmr.all;
  vstmr_1xframe = vstmr_1x_xo_to_frame(slpc_data.vstmr_data.onex_view, 
                                       (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_1X].start_time = vstmr_1xframe.all;
  vstmr_hstmr = vstmr_hdr_xo_to_hstmr(slpc_data.vstmr_data.hdr_view, 
                                       (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_HDR].start_time = vstmr_hstmr.all;
  vstmr_wstmr = vstmr_wcdma_xo_to_wstmr(slpc_data.vstmr_data.wcdma_view, 
                                       (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_WCDMA].start_time = vstmr_wstmr.all;
  vstmr_ostmr = vstmr_lte_xo_to_ostmr(slpc_data.vstmr_data.lte_view, 
                                       (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_LTE].start_time = vstmr_ostmr.all;
  vstmr_tdsstmr = vstmr_tds_xo_to_stmr(slpc_data.vstmr_data.tdscdma_view, 
                                       (uint32) start_ts.ustmr_ts);
  sc->alt_techs[SLPC_TDSCDMA].start_time = vstmr_tdsstmr.all;

  /* to match legacy operation, return alt tech data to G and W */
  start_ts.tstmr.time = sc->alt_techs[id].start_time;
  if ( (id == SLPC_GSM) || (id == SLPC_GSM2) || (id == SLPC_GSM3) )
  {
    start_ts.tstmr.alt_time = sc->alt_techs[SLPC_WCDMA].start_time;
  }
  if ( id == SLPC_WCDMA )
  {
    start_ts.tstmr.alt_time = sc->alt_techs[SLPC_GSM].start_time;
  }

  return start_ts;
}

#else
/******************************************************************************
  @brief SLPC_HW_READ_TSTMR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static slpc_tstmr_type slpc_hw_read_tstmr( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* combined tstmr tick value */
  slpc_tstmr_type tstmr;

/*--------------------------------------------------------------------------*/

  ASSERT( id < SLPC_NUM_CLIENTS );

  sc->alt_techs[SLPC_GSM].start_time = SLPC_READ_TSTMR_TIME_GSM();
  sc->alt_techs[SLPC_GSM].start_phase = SLPC_READ_TSTMR_PHASE_GSM();
  sc->alt_techs[SLPC_GSM2].start_time = SLPC_READ_TSTMR_TIME_GSM2();
  sc->alt_techs[SLPC_GSM2].start_phase = SLPC_READ_TSTMR_PHASE_GSM2();
  sc->alt_techs[SLPC_GSM3].start_time = SLPC_READ_TSTMR_TIME_GSM3();
  sc->alt_techs[SLPC_GSM3].start_phase = SLPC_READ_TSTMR_PHASE_GSM3();
  sc->alt_techs[SLPC_1X].start_time = ( SLPC_READ_TSTMR_TIME_1X() >> 2 );
  sc->alt_techs[SLPC_1X].start_phase = SLPC_READ_TSTMR_PHASE_1X();
  sc->alt_techs[SLPC_HDR].start_time = SLPC_READ_TSTMR_TIME_HDR();
  sc->alt_techs[SLPC_HDR].start_phase = SLPC_READ_TSTMR_PHASE_HDR();
  sc->alt_techs[SLPC_WCDMA].start_time = SLPC_READ_TSTMR_TIME_WCDMA();
  sc->alt_techs[SLPC_WCDMA].start_phase = SLPC_READ_TSTMR_PHASE_WCDMA();
  sc->alt_techs[SLPC_LTE].start_time = SLPC_READ_TSTMR_TIME_LTE();
  sc->alt_techs[SLPC_LTE].start_phase = SLPC_READ_TSTMR_PHASE_LTE();
  sc->alt_techs[SLPC_TDSCDMA].start_time = SLPC_READ_TSTMR_TIME_TDSCDMA();
  sc->alt_techs[SLPC_TDSCDMA].start_phase = SLPC_READ_TSTMR_PHASE_TDSCDMA();

  tstmr.time = sc->alt_techs[id].start_time;
  tstmr.phase = sc->alt_techs[id].start_phase;
  if ( (id == SLPC_GSM) || (id == SLPC_GSM2) || (id == SLPC_GSM3))
  {
    tstmr.alt_time = sc->alt_techs[SLPC_WCDMA].start_time;
    tstmr.alt_phase = sc->alt_techs[SLPC_WCDMA].start_phase;
  }
  if ( id == SLPC_WCDMA )
  {
    tstmr.alt_time = sc->alt_techs[SLPC_GSM].start_time;
    tstmr.alt_phase = sc->alt_techs[SLPC_GSM].start_phase;
  }

  return tstmr;
}

/******************************************************************************
  @brief SLPC_HW_READ_STMR_SYNC
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static slpc_start_type slpc_hw_read_stmr_sync( slpc_id_type id )
{
  /* comparison variable to make sure read was latched */
  ustmr_type ustmr_comp_ts, ustmr_qtimer;

  /* comparison to make sure not on 23bit boundary */
  uint64 tick1, tick2;

  /* loop counter */
  uint8 count = 0;

  /* return value */
  slpc_start_type start_ts = {0, {0, 0, 0, 0}, 0};

/*--------------------------------------------------------------------------*/

  /* repeat the read until ustmr consistently reads the same */
  /* repeat the read until not on timetick upper bits boundary */
  do
  {
    ENTER_ATOMIC_SECTION();
    tick1 = slpc_get_tick();
    ustmr_qtimer = tick1 & SLPC_TIMETICK_LOWER_MASK;
    tick1 = tick1 & SLPC_TIMETICK_UPPER_MASK;

    /* trigger the synchronous read of ustmr and tstmr */
    SLPC_HW_READ_SYNC_CMD();

    start_ts.ustmr_ts = SLPC_READ_USTMR_TIME_SYNC();
    start_ts.tstmr = slpc_hw_read_tstmr( id );
    ustmr_comp_ts = SLPC_READ_USTMR_TIME_SYNC();
    LEAVE_ATOMIC_SECTION();
    tick2 = slpc_get_tick() & SLPC_TIMETICK_UPPER_MASK;
    if ( ++count >= (SLPC_READ_SYNC_LOOP_TIMEOUT - 2) )
    {
      /* something appears to be wrong, start logging the moving counters */
      TRACE(id, READ_SYNC_ERR, tick1, ustmr_qtimer, ustmr_comp_ts);
    }
    if ( count >= SLPC_READ_SYNC_LOOP_TIMEOUT )
    {
      ERR_FATAL("read sync failure, id: %d, ustmr1: %d, ustmr2: %d", 
                id, start_ts.ustmr_ts, ustmr_comp_ts);
    }
  }
  while ( (start_ts.ustmr_ts != ustmr_comp_ts) || (tick1 != tick2) || 
          (ABS(ustmr_qtimer - ustmr_comp_ts) > (int64) SLPC_USTMR_SYNC_THRESH) );

  /* upper bits from timetick coupled with lower bits from ustmr count */
  start_ts.tick = tick1 + start_ts.ustmr_ts;

  return( start_ts );
}

/******************************************************************************
  @brief SLPC_HW_WRITE_TSTMR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_hw_write_tstmr( slpc_id_type id, uint64 tstmr )
{
  switch( id )
  {
  case SLPC_GSM:
    SLPC_WRITE_TSTMR_TIME_GSM( tstmr );
    SLPC_WRITE_TSTMR_CMD_GSM();
    break;
  case SLPC_1X:
    SLPC_WRITE_TSTMR_TIME_1X( tstmr );
    break;
  case SLPC_HDR:
    SLPC_WRITE_TSTMR_TIME_HDR( tstmr );
    break;
  case SLPC_WCDMA:
    SLPC_WRITE_TSTMR_TIME_WCDMA( tstmr );
    SLPC_WRITE_TSTMR_CMD_WCDMA();
    break;
  case SLPC_LTE:
    SLPC_WRITE_TSTMR_TIME_LTE( tstmr );
    SLPC_WRITE_TSTMR_CMD_LTE();
    break;
  case SLPC_TDSCDMA:
    SLPC_WRITE_TSTMR_TIME_TDSCDMA( tstmr );
    SLPC_WRITE_TSTMR_CMD_TDSCDMA();
    break;
  case SLPC_GSM2:
    SLPC_WRITE_TSTMR_TIME_GSM2( tstmr );
    SLPC_WRITE_TSTMR_CMD_GSM2();
    break;
  case SLPC_GSM3:
    SLPC_WRITE_TSTMR_TIME_GSM3( tstmr );
    SLPC_WRITE_TSTMR_CMD_GSM3();
    break;
  default:
    ERR_FATAL("slpc_hw_write_tstmr, invalid id: %d", id, 0, 0); 
  };
}

/******************************************************************************
  @brief SLPC_HW_WRITE_STMR_SYNC
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_hw_write_stmr_sync( slpc_id_type id, ustmr_type ustmr_ts, 
                                     slpc_tstmr_type *tstmr )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Mask for which techs are to receive the online pulse event */
  uint32 routing = 0;

  /* loop counter */
  uint8 i;

/*--------------------------------------------------------------------------*/

  /* Set to route to primary, and any alternate techs if needed */
  routing = sc->route_mask;
  for ( i = 0; i < SLPC_NUM_CLIENTS; i++ )
  {
    if ( sc->alt_techs[i].valid )
    {
      TRACE(id, ALT_ID_SET, i, sc->alt_techs[i].valid, sc->alt_techs[i].tstmr);
      routing |= slpc[i].route_mask;
    }
  }

  /* route this ustmr hardware to both primary and alt technology */
  SLPC_USTMR_SET_ROUTING( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc, 
                          routing );

  /* program the timer value, the ustmr point to trigger the ols event */
  SLPC_USTMR_SET_TIME( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc, 
                       sc->ending.ustmr_ts );

  /* skip tstmr programming if client is doing it */
  if ( tstmr != NULL )
  {
    /* setup the primary tech stmr slam, arm it */
    slpc_hw_write_tstmr( id, tstmr->time );
  }

  /* setup any alternate tech stmr slam, arm it */
  for ( i = 0; i < SLPC_NUM_CLIENTS; i++ )
  {
    if ( sc->alt_techs[i].valid && sc->alt_techs[i].load_tstmr )
    {
      slpc_hw_write_tstmr( (slpc_id_type) i, sc->alt_techs[i].tstmr );
    }
  }

  /* enable the interrupt to the processor */
  SLPC_USTMR_EN_INT( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc );

  /* start the timer */
  SLPC_USTMR_START_TIMER( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc );
}
#endif

/******************************************************************************
  @brief slpc_enable_alt_id
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
void slpc_enable_alt_id( slpc_id_type id, slpc_id_type alt_id, 
                         slpc_tstmr_type *tstmr )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  ASSERT( id < SLPC_NUM_CLIENTS );
  ASSERT( alt_id < SLPC_NUM_CLIENTS );

  sc->alt_techs[alt_id].valid = TRUE;

  if ( tstmr == NULL )
  {
    sc->alt_techs[alt_id].load_tstmr = FALSE;
    sc->alt_techs[alt_id].tstmr = 0;
  }
  else
  {
    sc->alt_techs[alt_id].load_tstmr = TRUE;
    sc->alt_techs[alt_id].tstmr = tstmr->time;
  }
}

/******************************************************************************
  @brief slpc_disable_alt_id
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
void slpc_disable_alt_id( slpc_id_type id, slpc_id_type alt_id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  sc->alt_techs[alt_id].valid = FALSE;
  sc->alt_techs[alt_id].load_tstmr = FALSE;
  sc->alt_techs[alt_id].tstmr = 0;
}

/******************************************************************************
  @brief SLPC_ACQUIRE_RESOURCE
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return boolean for success/failure 
******************************************************************************/
boolean slpc_acquire_resource( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Loop counter */
  uint8 i;

  /* return value */
  boolean ret_val = TRUE;

/*--------------------------------------------------------------------------*/

  ENTER_ATOMIC_SECTION();

  /* find an available resource */
  for ( i = 0; i < slpc_num_rsrc; i++ )
  {
    if ( !slpc_data.slpc_ustmr_rsrc[i].in_use )
    {
      slpc_data.slpc_ustmr_rsrc[i].in_use = TRUE;
      slpc_data.slpc_ustmr_rsrc[i].id = id;
      sc->slpc_ustmr_rsrc = i;
      break;
    }
  }

  /* if no resources were available */
  if ( i == slpc_num_rsrc )
  {
    DBG_1(ERROR, "No resources left, id: %d", id);
    ret_val = FALSE;
  }

  LEAVE_ATOMIC_SECTION();
  return ret_val;
}

/******************************************************************************
  @brief SLPC_RELEASE_RESOURCE
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
void slpc_release_resource( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  ENTER_ATOMIC_SECTION();

  if ( sc->slpc_ustmr_rsrc != SLPC_NO_RESOURCE )
  {
    slpc_data.slpc_ustmr_rsrc[sc->slpc_ustmr_rsrc].in_use = FALSE;
    sc->slpc_ustmr_rsrc = SLPC_NO_RESOURCE;
  }

  LEAVE_ATOMIC_SECTION();
}

/******************************************************************************
  @brief SLPC_CALL_NOTIFY
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_call_notify( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* loop counter */
  uint8 i = 0;

/*--------------------------------------------------------------------------*/

  for ( i = 0; i < SLPC_MAX_WAKEUP_CALLBACKS; i++ )
  {
    if ( slpc_data.notify_cb[i] != NULL )
    {
      slpc_data.notify_cb[i]( id, sc->wakeup.tick, sc->wakeup.update, 
                              sc->wakeup.extension );
    }
  }
}

/******************************************************************************
  @brief SLPC_CALL_NOTIFY_WAKEUP
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static void slpc_call_notify_wakeup( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* loop counter */
  uint8 i = 0;

/*--------------------------------------------------------------------------*/

  for ( i = 0; i < SLPC_MAX_WAKEUP_CALLBACKS; i++ )
  {
    if ( slpc_data.notify_wakeup_cb[i] != NULL )
    {
      slpc_data.notify_wakeup_cb[i]( id, sc->wakeup.tick );
    }
  }
}

/******************************************************************************
  @brief SLPC_UPDATE_WAKEUP_POINT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
static slpc_err_type slpc_update_wakeup_point( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Local read of current timetick */
  uint64 tick;

  /* return value */
  slpc_err_type err_code = SLPC_ERR_NO_ERR;

/*--------------------------------------------------------------------------*/

  sc->wakeup.last = sc->wakeup.tick;
  sc->wakeup.tick = sc->ending.tick - sc->warmup.dur_tick;
#ifdef SLPC_VSTMR
  sc->wakeup.tick -= SLPC_VSTMR_SLAM_EXEC_DUR;
#endif

  ENTER_ATOMIC_SECTION();

  tick = slpc_get_tick();
  TRACE(id, UPDATE_WAKEUP, sc->state, tick, sc->wakeup.tick);

  if ( sc->wakeup.tick < tick )
  {
    DBG_3(ERROR, "Wakeup point is in the past! n=%lu w=%lu s=%lu",
          (uint32) tick, (uint32) sc->wakeup.tick, (uint32) sc->ending.tick);
    err_code = SLPC_ERR_WAKEUP_IN_PAST;
  }
  else
  {
    timer_set_64( &sc->wakeup.timer, (sc->wakeup.tick - tick), 0, T_TICK );
  }

  /* check if this is an update to a previously set wakeup point */
  if ( sc->state == SLPC_SLEEP_STATE ) 
  {
    sc->wakeup.update = TRUE;
  }
  else
  {
    sc->wakeup.update = FALSE;
  }

  /* check if new wakeup is later than old, so this is an extension */
  if ( sc->wakeup.tick > sc->wakeup.last ) 
  {
    sc->wakeup.extension = TRUE;
    }
  else
  {
    sc->wakeup.extension = FALSE;
  }

  LEAVE_ATOMIC_SECTION();

  return err_code;
}


/******************************************************************************
  @brief SLPC_TSTMR_TO_USTMR
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return ustmr_type - value of duration in USTMR units 
******************************************************************************/
static int64 slpc_tstmr_to_ustmr( slpc_id_type id, tstmr_type tstmr_dur )
{
  /* Duration in "uncorrected", or raw, XO's */
  int64 dur_xo = 0;

  /* Error storage, in ppm/1024 */
  int64 err = 0;

  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* XO manager xo error data */
  tcxomgr_vco_info_type vco = tcxomgr_get_client_rgs(sc->tm_client);

/*--------------------------------------------------------------------------*/

  /* collect all error in 1/1024 ppm, part from slew, part from RGS */
  err = sc->slew_err_accum + vco.rot_value;

  DBG_3(HIGH, "err: %ld, slew_err_accum: %ld, rot_err: %ld", 
        (int32) err, (int32) sc->slew_err_accum, (int32) vco.rot_value );

  /* convert to intermediate err, accounting for rounding */
  err = ((192 * err) + ((1024 * 10) >> 1)) / (1024 * 10);
  dur_xo = (int64) tstmr_dur * (19200000LL + err);
  dur_xo = (dur_xo + (sc->system_freq >> 1)) / sc->system_freq;

  TRACE(id, TSTMR_TO_USTMR, tstmr_dur, sc->slew_err_accum, dur_xo);

  return dur_xo;
}

/******************************************************************************
  @brief SLPC_GET_WAKEUP_TICK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
 
  @return absolute timetick of the wakeup point
******************************************************************************/
uint64 slpc_get_wakeup_tick( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  TRACE(id, GET_WAKEUP_TICK, sc->wakeup.tick, 0, 0);
  return sc->wakeup.tick;
}


/******************************************************************************
  @brief SLPC_GET_OLS_TICK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
uint64 slpc_get_ols_tick( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  TRACE(id, GET_OLS_TICK, sc->ending.tick, 0, 0);
  return sc->ending.tick;
}


/******************************************************************************
  @brief SLPC_GET_WAKEUP_AND_OLS_TICK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
 
  @return absolute timetick of the wakeup point
******************************************************************************/
void slpc_get_wakeup_and_ols_tick( slpc_id_type id, uint64 *wakeup_tick, 
                                   uint64 *ols_tick )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  ENTER_ATOMIC_SECTION();

  TRACE(id, GET_WAKEUP_TICK, sc->wakeup.tick, 0, 0);
  TRACE(id, GET_OLS_TICK, sc->ending.tick, 0, 0);

  if ( wakeup_tick != NULL )
  {
    *wakeup_tick = sc->wakeup.tick;
  }
  if ( ols_tick != NULL )
  {
    *ols_tick = sc->ending.tick;
  }

  LEAVE_ATOMIC_SECTION();
}

/******************************************************************************
  @brief SLPC_GET_SYSCLK_COUNT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
uint64 slpc_get_sysclk_count( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Error calculation in ppm */
  int64 err_ppm = 0;

  /* XO manager xo error data */
  tcxomgr_vco_info_type vco = tcxomgr_get_client_rgs(sc->tm_client);

  /* Local read of current timetick, duration in timeticks */
  uint64 tick, dur_xo, dur_tstmr;

/*--------------------------------------------------------------------------*/

  tick = slpc_get_tick();

  dur_xo = tick - sc->start.tick;

  /* collect all error in 1/1024 ppm, part from slew, part from RGS */
  err_ppm = sc->slew_err_accum + vco.rot_value;
  dur_xo = (int64) dur_xo - (int64) ((err_ppm * (int64) dur_xo) / 1024000000LL);
  dur_tstmr = (dur_xo * sc->system_freq) / 19200000LL;

  TRACE(id, GET_SYSCLK_COUNT, tick, err_ppm, dur_tstmr);
  return dur_tstmr;
}


/******************************************************************************
  @brief SLPC_ERROR_FEEDBACK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_error_feedback( slpc_id_type id, int64 slew_err )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Error calculation in ppm */
  int64 err_ppm = 0;

/*--------------------------------------------------------------------------*/

  DBG_2(HIGH, "dur: %lu sysclks, err: %ld sysclks", 
        (uint32) sc->last_tstmr_dur, (int32) slew_err);

  /* conv to 1/1024ppm err:  err = (slew_err / dur_sysclk) * 1024e6 */
  /* apply dyn gain:  err = err * (dur_syslk / 2^k)  */
  /* k is tech specific, (gain is 1 for ~1.25sec dur) */
  /* oombine:  err = (slew_err * 1e6) / (1 << (k-10)) */
  /* limit dyn gain to 4 when duration gets too large */
  if ( sc->last_tstmr_dur <= (1 << (sc->k + 2)))
  {
    err_ppm = (slew_err * 1000000LL) / (int64) (1 << (sc->k - 10));
  }
  else
  {
    err_ppm = (slew_err * 4096000000LL) / (int64) sc->last_tstmr_dur;
  }

  /* apply constant loop gain - 1/8 rounded correctly */
  err_ppm = err_ppm >> 2;
  err_ppm += 1;
  err_ppm = err_ppm >> 1;

  /* Add into accumulator to be used in future duration calcuations */
  sc->slew_err_accum += err_ppm;

  TRACE(id, ERR_FEEDBACK, slew_err, err_ppm, sc->slew_err_accum);

  /* Limit the accum slew error to avoid run away conditions */
  if ( sc->slew_err_accum > SLPC_MAX_ACCUM_ERR )
  {
    sc->slew_err_accum = SLPC_MAX_ACCUM_ERR;
    DBG_1(ERROR, "accum limit:  %ld/1024 ppm", (int32) sc->slew_err_accum);
  }
  else if ( sc->slew_err_accum < -SLPC_MAX_ACCUM_ERR )
  {
    sc->slew_err_accum = -SLPC_MAX_ACCUM_ERR;
    DBG_1(ERROR, "accum limit:  %ld/1024 ppm", (int32) sc->slew_err_accum);
  }

  DBG_2(HIGH, "err: %ld, accum: %ld", (int32) err_ppm, 
        (int32) sc->slew_err_accum);
}

/******************************************************************************
  @brief SLPC_START
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
slpc_tstmr_type slpc_start( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  if ( sc->state == SLPC_INACTIVE_STATE )
  {
    /* Get a synchronous snapshot of current timer values */
    #ifdef SLPC_VSTMR
    sc->start = slpc_vstmr_tstmr_read_start( id );
    #else
    sc->start = slpc_hw_read_stmr_sync( id );
    #endif

    sc->state = SLPC_START_STATE;

    TRACE(id, START, 
          sc->start.tstmr.time, sc->start.tick, sc->start.ustmr_ts);
  }
  else
  {
    DBG_2(ERROR, "slpc_start called by %s in state %d", sc->id, sc->state );
  }

  return ( sc->start.tstmr );
}

#ifdef SLPC_VSTMR

/******************************************************************************
  @brief SLPC_VSTMR_TSTMR_SLAM
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_vstmr_tstmr_slam( slpc_id_type pri_id, slpc_id_type alt_id, uint64 dur )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[pri_id];

  /* the corrected ustmr value, local container for slam calculations */
  uint64 ustmr_slam, slam;

  /* local container used in slam calculations */
  slpc_dword_type tstmr_slam;

  /* for storing the raw start time */
  uint32 start;

  /* local copy of tech dependent stmr formatted value */
  vstmr_gstmr_t vstmr_gstmr, vstmr_gstmr_res;
  vstmr_wstmr_t vstmr_wstmr, vstmr_wstmr_res;
  vstmr_1xframe_t vstmr_1xframe, vstmr_1xframe_res;
  vstmr_hstmr_t vstmr_hstmr, vstmr_hstmr_res;
  vstmr_ostmr_t vstmr_ostmr, vstmr_ostmr_res;
  vstmr_tdsstmr_t vstmr_tdsstmr, vstmr_tdsstmr_res;

/*--------------------------------------------------------------------------*/

  ustmr_slam = sc->ending.ustmr_ts;

  switch( alt_id )
  {
  case SLPC_GSM:
    vstmr_gstmr = (vstmr_gstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_geran_seg_to_rtc( vstmr_gstmr );
    dur %= VSTMR_GERAN_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_GERAN_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_gstmr.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_gstmr = vstmr_geran_segment_cnt( tstmr_slam.ud );
    vstmr_geran_gstmr_sync( slpc_data.vstmr_data.gsm_view1, vstmr_gstmr, 
                            ustmr_slam );
    vstmr_gstmr_res = vstmr_geran_get_current_gstmr( 
                      slpc_data.vstmr_data.gsm_view1 );
    TRACE(alt_id, VSMTR_READ, vstmr_gstmr.all, vstmr_gstmr_res.all, ustmr_slam); 
    break;
  case SLPC_GSM2:
    vstmr_gstmr = (vstmr_gstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_geran_seg_to_rtc( vstmr_gstmr );
    dur %= VSTMR_GERAN_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_GERAN_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_gstmr.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_gstmr = vstmr_geran_segment_cnt( tstmr_slam.ud );
    vstmr_geran_gstmr_sync( slpc_data.vstmr_data.gsm_view2, vstmr_gstmr, 
                            ustmr_slam );
    vstmr_gstmr_res = vstmr_geran_get_current_gstmr( 
                      slpc_data.vstmr_data.gsm_view2 );
    TRACE(alt_id, VSMTR_READ, vstmr_gstmr.all, vstmr_gstmr_res.all, ustmr_slam); 
    break;
  case SLPC_GSM3:
    vstmr_gstmr = (vstmr_gstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_geran_seg_to_rtc( vstmr_gstmr );
    dur %= VSTMR_GERAN_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_GERAN_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_gstmr.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_gstmr = vstmr_geran_segment_cnt( tstmr_slam.ud );
    vstmr_geran_gstmr_sync( slpc_data.vstmr_data.gsm_view3, vstmr_gstmr, 
                            ustmr_slam );
    vstmr_gstmr_res = vstmr_geran_get_current_gstmr( 
                      slpc_data.vstmr_data.gsm_view3 );
    TRACE(alt_id, VSMTR_READ, vstmr_gstmr.all, vstmr_gstmr_res.all, ustmr_slam); 
    break;
  case SLPC_1X:
    vstmr_1xframe = (vstmr_1xframe_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_1x_frame_to_cx8( vstmr_1xframe );
    dur %= VSTMR_1X_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_1X_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_1xframe.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_1xframe = vstmr_1x_frame_segment_cnt( tstmr_slam.ud );
    vstmr_1x_frame_sync( slpc_data.vstmr_data.onex_view, vstmr_1xframe, 
                         ustmr_slam );
    vstmr_1xframe_res = vstmr_1x_get_current_frame( 
                        slpc_data.vstmr_data.onex_view );
    TRACE(alt_id, VSMTR_READ, vstmr_1xframe.all, vstmr_1xframe_res.all, ustmr_slam); 
    break;
  case SLPC_HDR:
    vstmr_hstmr = (vstmr_hstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_hstmr.cx8;
    dur %= VSTMR_HDR_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_HDR_RTC_MODULO;
    vstmr_hstmr.cx8 = slam;
    vstmr_hdr_hstmr_sync( slpc_data.vstmr_data.hdr_view, vstmr_hstmr, 
                          ustmr_slam );
    vstmr_hstmr_res = vstmr_hdr_get_current_hstmr( 
                      slpc_data.vstmr_data.hdr_view );
    TRACE(alt_id, VSMTR_READ, vstmr_hstmr.all, vstmr_hstmr_res.all, ustmr_slam); 
    break;
  case SLPC_WCDMA:
    vstmr_wstmr = (vstmr_wstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_wcdma_seg_to_cx8( vstmr_wstmr );
    dur %= VSTMR_WCDMA_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_WCDMA_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_wstmr.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_wstmr = vstmr_wcdma_segment_cnt( tstmr_slam.ud );
    vstmr_wcdma_wstmr_sync( slpc_data.vstmr_data.wcdma_view, vstmr_wstmr, 
                            ustmr_slam );
    vstmr_wstmr_res = vstmr_wcdma_get_current_wstmr( 
                      slpc_data.vstmr_data.wcdma_view );
    TRACE(alt_id, VSMTR_READ, vstmr_wstmr.all, vstmr_wstmr_res.all, ustmr_slam); 
    break;
  case SLPC_LTE:
    vstmr_ostmr = (vstmr_ostmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_ostmr.ostmr_cnt;
    dur %= VSTMR_LTE_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_LTE_RTC_MODULO;
    vstmr_ostmr.ostmr_cnt = slam;
    vstmr_lte_ostmr_sync( slpc_data.vstmr_data.lte_view, vstmr_ostmr, 
                          ustmr_slam );
    vstmr_ostmr_res = vstmr_lte_get_current_ostmr( 
                      slpc_data.vstmr_data.lte_view );
    TRACE(alt_id, VSMTR_READ, vstmr_ostmr.all, vstmr_ostmr_res.all, ustmr_slam); 
    break;
  case SLPC_TDSCDMA:
    vstmr_tdsstmr = (vstmr_tdsstmr_t) sc->alt_techs[alt_id].start_time;
    start = vstmr_tds_seg_to_cx8( vstmr_tdsstmr );
    dur %= VSTMR_TDS_RTC_MODULO;
    slam = start + dur;
    slam %= VSTMR_TDS_RTC_MODULO;
    tstmr_slam.uw[0] = vstmr_tdsstmr.frac;
    tstmr_slam.uw[1] = slam;
    vstmr_tdsstmr = vstmr_tds_segment_cnt( tstmr_slam.ud );
    vstmr_tds_stmr_sync( slpc_data.vstmr_data.tdscdma_view, vstmr_tdsstmr, 
                         ustmr_slam );
    vstmr_tdsstmr_res = vstmr_tds_get_current_stmr( 
                        slpc_data.vstmr_data.tdscdma_view );
    TRACE(alt_id, VSMTR_READ, vstmr_tdsstmr.all, vstmr_tdsstmr_res.all, ustmr_slam); 
    break;
  default:
    ERR_FATAL("invalid id received for slam", 0, 0, 0);
  }

  DBG_4(HIGH, "vstmr slam %d, start=%d, dur=%d, slam=%d", alt_id, start, dur, slam);
  TRACE(alt_id, VSTMR_SLAM_CALC, start, dur, slam);
}

/******************************************************************************
  @brief SLPC_VSTMR_SLAM
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_vstmr_slam( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* For holding the converted duration for the alternate technologies */
  uint64 alt_dur;

  int i;

/*--------------------------------------------------------------------------*/

  /* slam the primary tech */
  slpc_vstmr_tstmr_slam( id, id, sc->tstmr_dur );

  TRACE(id, VSTMR_SLAM, sc->tstmr_dur, 0, 0);

  for ( i = 0; i < SLPC_NUM_CLIENTS; i++ )
  {
    if ( sc->alt_techs[i].valid )
    {
      /* calculate the alternate duration */
      alt_dur = (uint64) (sc->tstmr_dur * slpc[i].system_freq);
      alt_dur = (alt_dur + (sc->system_freq >> 1)) / sc->system_freq;

      TRACE(id, VSTMR_SLAM, i, alt_dur, 0);

      /* slam the alternate tech */
      slpc_vstmr_tstmr_slam( id, i, alt_dur );
    }
  }
}

#endif

/******************************************************************************
  @brief SLPC_SET_DURATION_AND_WARMUP
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
slpc_err_type slpc_set_duration_and_warmup( slpc_id_type id, uint64 tstmr_dur, 
                                            uint16 dur_usec )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* return value */
  slpc_err_type err_code = SLPC_ERR_NO_ERR;

/*--------------------------------------------------------------------------*/

  /* must first do slpc_start, cannot be past wakeup complete */
  if ( (sc->state == SLPC_INACTIVE_STATE) || (sc->state == SLPC_WARMUP_STATE) )
  {
    /* Get a synchronous snapshot of current timer values */
    DBG_2(ERROR, "slpc_set_duration_and_warmup called by %s in state %d", 
          sc->id, sc->state );
  }
  else
  {
    ENTER_ATOMIC_SECTION();

    /* Calculate ustmr duration */
    sc->tstmr_dur = tstmr_dur;
    sc->ustmr_dur = (ustmr_type) slpc_tstmr_to_ustmr( id, tstmr_dur );
    sc->ending.tick = sc->start.tick + sc->ustmr_dur;
    sc->ending.ustmr_ts = sc->ending.tick & SLPC_TIMETICK_LOWER_MASK;

    /* Update warmup and wakeup based on new duration */
    if ( sc->warmup.dur_usec != dur_usec )
    {
      sc->warmup.dur_usec = dur_usec;
      sc->warmup.dur_tick = (19200000ULL * sc->warmup.dur_usec) / 1000000ULL;
    }

    TRACE(id, SET_DURATION, sc->ending.tick, sc->tstmr_dur, sc->ustmr_dur);
    TRACE(id, SET_WARMUP, sc->warmup.dur_usec, sc->warmup.dur_tick, 0xFFFF);

    /* Update the wakeup point based on the new sleep duration/warmup */
    err_code = slpc_update_wakeup_point(id);

    LEAVE_ATOMIC_SECTION();

    /* notify anyone else about this technology dur/wakeup update */
    slpc_call_notify(id);

    DBG_3(MED, "Set duration and warmup: id=%s, wake=%lu, ols=%lu", 
          sc->id, (uint32) sc->wakeup.tick, (uint32) sc->ending.tick);

    /* Enter sleep state */
    sc->state = SLPC_SLEEP_STATE;
  }

  return err_code;
}

/******************************************************************************
  @brief SLPC_SET_DURATION
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
slpc_err_type slpc_set_duration( slpc_id_type id, uint64 tstmr_dur )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* return value */
  slpc_err_type err_code = SLPC_ERR_NO_ERR;

/*--------------------------------------------------------------------------*/

  /* must first do slpc_start, cannot be past wakeup complete */
  if ( (sc->state == SLPC_INACTIVE_STATE) || (sc->state == SLPC_WARMUP_STATE) )
  {
    /* Get a synchronous snapshot of current timer values */
    DBG_2(ERROR, "slpc_set_duration called by %s in state %d", 
          sc->id, sc->state );
  }
  else
  {
    ENTER_ATOMIC_SECTION();

    /* Calculate ustmr duration */
    sc->tstmr_dur = tstmr_dur;
    sc->ustmr_dur = (ustmr_type) slpc_tstmr_to_ustmr( id, tstmr_dur );
    sc->ending.tick = sc->start.tick + sc->ustmr_dur;
    sc->ending.ustmr_ts = sc->ending.tick & SLPC_TIMETICK_LOWER_MASK;

    TRACE(id, SET_DURATION, sc->ending.tick, sc->tstmr_dur, sc->ustmr_dur);

    /* Update the wakeup point based on the new sleep duration */
    err_code = slpc_update_wakeup_point(id);

    LEAVE_ATOMIC_SECTION();

    /* notify anyone else about this technology dur/wakeup update */
    slpc_call_notify(id);

    DBG_3(MED, "Set duration: id=%s, wake=%lu, ols=%lu", 
          sc->id, (uint32) sc->wakeup.tick, (uint32) sc->ending.tick);

    /* Enter sleep state */
    sc->state = SLPC_SLEEP_STATE;
  }

  return err_code;
}

/******************************************************************************
  @brief SLPC_SET_WARMUP
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
slpc_err_type slpc_set_warmup( slpc_id_type id, uint16 dur_usec )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* return value */
  slpc_err_type err_code = SLPC_ERR_NO_ERR;

/*--------------------------------------------------------------------------*/

  /* Convert microseconds to slow clocks only if warmup has changed */
  if ( sc->warmup.dur_usec != dur_usec )
  {
    ENTER_ATOMIC_SECTION();

    sc->warmup.dur_usec = dur_usec;
    sc->warmup.dur_tick = (19200000ULL * sc->warmup.dur_usec) / 1000000ULL;

    TRACE(id, SET_WARMUP, sc->warmup.dur_usec, sc->warmup.dur_tick, 0);

    /* Only update the wakeup point if we're SLEEP */
    if ( sc->state == SLPC_SLEEP_STATE )
    {
      /* Update the wakeup point based on the new warmup time */
      err_code = slpc_update_wakeup_point(id);

      LEAVE_ATOMIC_SECTION();

      /* notify anyone else about this technology dur/wakeup update */
      slpc_call_notify(id);
    }
    else
    {
      LEAVE_ATOMIC_SECTION();
    }
  }

  return err_code;
}

/******************************************************************************
  @brief SLPC_SET_NOTIFY_CALLBACK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_set_notify_callback( slpc_notify_cb_type n_cb )
{
  uint8 i = 0;

/*--------------------------------------------------------------------------*/

  while ( slpc_data.notify_cb[i++] != NULL );
  i--;

  if ( i < SLPC_MAX_WAKEUP_CALLBACKS )
  {
    /* Save the notify callback */
    slpc_data.notify_cb[i] = n_cb;
  }
  else
  {
    ERR_FATAL("no room for notify callback allocation", 0, 0, 0); 
}

}

/******************************************************************************
  @brief SLPC_SET_NOTIFY_WAKEUP_CALLBACK

  This callback will be called at any tech's wakeup event. For anyone
  wanting to track wakeups.

  @param id: n_cb: notify callback

  @return none. 
******************************************************************************/
void slpc_set_notify_wakeup_callback( slpc_notify_wakeup_cb_type n_cb )
{
  uint8 i = 0;

/*--------------------------------------------------------------------------*/

  while ( slpc_data.notify_wakeup_cb[i++] != NULL );
  i--;

  if ( i < SLPC_MAX_WAKEUP_CALLBACKS )
  {
    /* Save the notify callback */
    slpc_data.notify_wakeup_cb[i] = n_cb;
  }
  else
  {
    ERR_FATAL("no room for wakeup notify callback allocation", 0, 0, 0); 
  }
}

/******************************************************************************
  @brief SLPC_DEREGISTER_NOTIFY_CALLBACK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_deregister_notify_callback( slpc_notify_cb_type n_cb )
{
  uint8 i;

/*--------------------------------------------------------------------------*/

  for ( i = 0; i < SLPC_MAX_WAKEUP_CALLBACKS; i++ )
  {
    if ( slpc_data.notify_cb[i] == n_cb )
    {
      slpc_data.notify_cb[i] = NULL;
    }
  }
}

/******************************************************************************
  @brief SLPC_DEREGISTER_NOTIFY_WAKEUP_CALLBACK

  Deregister the wakeup callback, if previously set

  @param id: n_cb: notify callback

  @return none. 
******************************************************************************/
void slpc_deregister_notify_wakeup_callback( slpc_notify_wakeup_cb_type n_cb )
{
  uint8 i;

/*--------------------------------------------------------------------------*/

  for ( i = 0; i < SLPC_MAX_WAKEUP_CALLBACKS; i++ )
  {
    if ( slpc_data.notify_wakeup_cb[i] == n_cb )
    {
      slpc_data.notify_wakeup_cb[i] = NULL;
    }
  }
}

/******************************************************************************
  @brief SLPC_SET_WAKEUP_CALLBACK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_set_wakeup_callback( slpc_id_type id, slpc_event_callback_type w_cb )
{
  /* Save the wakeup event callback */
  slpc[id].wakeup.cb = w_cb;

}

/******************************************************************************
  @brief SLPC_SET_OLS_CALLBACK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_set_ols_callback( slpc_id_type id, slpc_event_callback_type e_cb )
{
  /* Save the ols event callback */
  slpc[id].ending.cb = e_cb;

}

/******************************************************************************
  @brief SLPC_WAKEUP_EVENT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_wakeup_event( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  ENTER_ATOMIC_SECTION();

  if ( sc->state != SLPC_SLEEP_STATE )
  {
    LEAVE_ATOMIC_SECTION();
    sc->wakeup.invalid_cnt++;
    DBG_2(ERROR, "Wakeup in wrong state %d, cnt=%d", sc->state, 
          sc->wakeup.invalid_cnt);
    return;
  }

  /* Capture the timestamp when the wakeup ISR starts */
  sc->wakeup.cb_start_tick = slpc_get_tick();

  TRACE(id, WAKEUP_START, sc->state, sc->wakeup.tick, sc->wakeup.cb_start_tick);

  /* Reasonable wakeup? */
  if ( (sc->wakeup.cb_start_tick + SLPC_WAKEUP_TIMER_ERR) >= sc->wakeup.tick )
  {
    /* Update the sleep controller's software state to "wakeup" state */
    sc->state = SLPC_WAKEUP_STATE;

    LEAVE_ATOMIC_SECTION();

    /* Invoke the wakeup callback */
    if (sc->wakeup.cb != NULL )
    {
      sc->wakeup.cb();
    }
  }
  else
  {
    LEAVE_ATOMIC_SECTION();
    DBG_3(ERROR, "%s Wakeup ignored!  now=%lu, wake=%lu", sc->id, 
          (uint32) sc->wakeup.cb_start_tick, (uint32) sc->wakeup.tick);
  }

  /* Capture the timestamp when the wakeup ISR ends */
  sc->wakeup.cb_end_tick = slpc_get_tick();

  /* notify anyone else about this technology wakeup event */
  slpc_call_notify_wakeup(id);

  DBG_3(MED, "Wakeup event: %s, start=%lu, end=%lu", sc->id, 
        (uint32) sc->wakeup.cb_start_tick, (uint32) sc->wakeup.cb_end_tick);

  TRACE(id, WAKEUP_END, sc->wakeup.tick,
          sc->wakeup.cb_start_tick, sc->wakeup.cb_end_tick); 
}

/******************************************************************************
  @brief SLPC_SUFFICIENT_WARMUP_TIME
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param 
  @see Func2
  @return none. 
******************************************************************************/
boolean slpc_sufficient_warmup_time ( slpc_id_type id, uint32 min_warmup )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

  /* Current timetick */
  uint64 tick;

  /* convert from usec to xo ticks */
  int64 min_ticks = (min_warmup * 19200000uLL) / 1000000uLL;

/*--------------------------------------------------------------------------*/

  tick = slpc_get_tick();
  if ( ( (int64) sc->ending.tick - (int64) tick) > min_ticks ) return TRUE;

  return FALSE;
}

/******************************************************************************
  @brief SLPC_COMPLETE_WAKEUP
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
boolean slpc_complete_wakeup ( slpc_id_type id, uint32 min_warmup, 
                               slpc_tstmr_type *tstmr )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

#ifndef SLPC_VSTMR
  /* Current timetick */
  uint64 tick;

  /* Current relative location to ending tick */
  int64 delta;

  /* convert from usec to xo ticks */
  int64 min_ticks = (min_warmup * 19200000uLL) / 1000000uLL;
#endif

/*--------------------------------------------------------------------------*/

#ifdef SLPC_VSTMR
  /* virtually slam the tech stmr */
  slpc_vstmr_slam( id );
  sc->state = SLPC_INACTIVE_STATE;
  TRACE(id, COMPLETE_WAKEUP, min_warmup, 0, 0);
  DBG_0(HIGH, "slpc_complete_wakeup called");
  /* Record the sleep duration, for error_feedback */
  sc->last_tstmr_dur = sc->tstmr_dur;
  return TRUE;
#else
  if (!slpc_acquire_resource( id ))
  {
    TRACE(id, COMPLETE_WAKEUP, 0xFFFFFFFF, sc->ending.tick, min_ticks);
    return FALSE;
  }

  ENTER_ATOMIC_SECTION();

  tick = slpc_get_tick();
  delta = (int64) sc->ending.tick - (int64) tick;

  if ( delta > min_ticks )
  {
    if ( sc->state != SLPC_WAKEUP_STATE )
    {
      ERR_FATAL("Complete wakeup, %d in wrong state=%d!", id, sc->state, 0);
    }

    /* Program new sleep ending point, taking into account h/w overhead */
    slpc_hw_write_stmr_sync( id, sc->ending.ustmr_ts, tstmr );

    /* Capture the timetick when the wakeup is committed to. */
    sc->warmup.tick = tick; 

    /* Update the sleep controller's software state to "warmup" */
    sc->state = SLPC_WARMUP_STATE;

    LEAVE_ATOMIC_SECTION();
    DBG_3(MED, "Complete wakeup: %s, warmup_ts=%lu, ols_ts=%lu", 
          sc->id, (uint32) sc->warmup.tick, (uint32) sc->ending.tick);
    TRACE(id, COMPLETE_WAKEUP, tick, sc->ending.tick, min_ticks);

    return TRUE;
  }
  else
  {
    LEAVE_ATOMIC_SECTION();
    slpc_release_resource( id );
    DBG_3(ERROR, "Complete wakeup: not enough time, %s, now=%lu, ols_ts=%lu", 
          sc->id, (uint32) tick, (uint32) sc->ending.tick);
    TRACE(id, COMPLETE_WAKEUP, tick, sc->ending.tick, min_ticks);

    return FALSE;
  }
#endif
}

/******************************************************************************
  @brief SLPC_ABORT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
void slpc_abort ( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  TRACE(id, ABORT, sc->state, sc->wakeup.tick, 0);

  ENTER_ATOMIC_SECTION();

  /* Record the sleep duration, for error_feedback */
  sc->last_tstmr_dur = sc->tstmr_dur;

  if ( (sc->state == SLPC_START_STATE) ||
       (sc->state == SLPC_SLEEP_STATE) )
  {
    timer_clr( &sc->wakeup.timer, T_NONE );
  }
  else if ( sc->state == SLPC_WARMUP_STATE )
  {
    /* Disable the ustmr interrupt */
    SLPC_USTMR_DIS_INT( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc );

    /* already setup OLS, need to tear it down */
    slpc_release_resource( id );
  }

  /* Update the sleep controller's software state to "inactive" state */
  sc->state = SLPC_INACTIVE_STATE;

  LEAVE_ATOMIC_SECTION();
}

/******************************************************************************
  @brief SLPC_OLS_EVENT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @param tstmr_duration : the duration of sleep requested, in
                        tstmr ticks
  @see Func2
  @return none. 
******************************************************************************/
static void slpc_ols_event( slpc_id_type id )
{
  /* Sleep controller structure */
  slpc_client_type *sc = &slpc[id];

/*--------------------------------------------------------------------------*/

  if ( sc->state != SLPC_WARMUP_STATE )
  {
    sc->ending.invalid_cnt++;
    DBG_2(ERROR, "OLS in wrong state %d, cnt=%d", sc->state, 
          sc->ending.invalid_cnt);
    return;
  }

  /* Capture the timestamp when the ols ISR starts */
  sc->ending.isr_start_tick = slpc_get_tick();

  /* If this ISR is excessively latent, print error */
  if ( sc->ending.isr_start_tick - sc->ending.tick >= ISR_LATENCY_THRESH ) 
  {
    DBG_2(ERROR, "%s ols event, latency: %ld ticks", sc->id,
          (int32) sc->ending.isr_start_tick - sc->ending.tick );
  }
  else
  {
    DBG_2(MED, "%s ols event, latency: %ld ticks", sc->id,
          (int32) sc->ending.isr_start_tick - sc->ending.tick );
  }

  /* Disable the ustmr interrupt */
  SLPC_USTMR_DIS_INT( slpc_c_fam, slpc_c_ver, sc->slpc_ustmr_rsrc );

  /* Update the sleep controller's software state to "inactive" state */
  sc->state = SLPC_INACTIVE_STATE;

  /* Record the sleep duration, for error_feedback */
  sc->last_tstmr_dur = sc->tstmr_dur;

  slpc_release_resource( id );

  TRACE(id, OLS_START, sc->state, sc->ending.tick, sc->ending.isr_start_tick);

  /* Invoke the ols callback */
  if (sc->ending.cb != NULL )
  {
    sc->ending.cb();
  }

  /* Record the timestamp of the end of this ISR */
  sc->ending.isr_end_tick = slpc_get_tick();

  TRACE(id, OLS_END, sc->ending.tick, sc->ending.isr_start_tick, 
        sc->ending.isr_end_tick);
}

/******************************************************************************
  @brief SLPC_TASK
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
void slpc_task( dword unused )
{
  /* incoming message buffer */
  char slpc_msg[SLPC_MSG_SIZE];

  /* holder for incoming message priority */
  unsigned int msg_prio;

/*--------------------------------------------------------------------------*/

  rcinit_handshake_startup();

  while ( mq_receive(slpc_data.msg_queue, slpc_msg, 
                     SLPC_MSG_SIZE, &msg_prio) != -1 )
  {
    switch ( slpc_msg[0] )
    {
    case SLPC_WAKEUP_EVENT:
      slpc_wakeup_event( slpc_msg[1] );
      break;
    case SLPC_OLS_EVENT:
      slpc_ols_event( slpc_msg[1] );
      break;
    }
  }
}


/******************************************************************************
  @brief SLPC_INIT
 
  Put a detailed description of the function here. This should
  function as a user's manual for your code, providing a
  developer everything that needs to be known in order to use
  your code. 
 
  @param id: identifies the calling client
  @see 
  @return none. 
******************************************************************************/
void slpc_init( void )
{
  /* loop variable */
  uint8 i, id;

  /* make sure initialization is done only once */
  static boolean slpc_initialized = FALSE;

/*--------------------------------------------------------------------------*/

  if ( slpc_initialized )
  {
    return;
  }

  slpc_initialized = TRUE;

  INIT_ATOMIC_SECTION();

  /* create message queue for communication with task */
  slpc_data.msg_queue = mq_open( "slpc", POSIX_O_RDWR | POSIX_O_CREAT, 0664, NULL );

  if( ( DAL_DeviceAttach(DALDEVICEID_INTERRUPTCONTROLLER, 
                        &slpc_data.int_handle) != DAL_SUCCESS ) || 
      ( slpc_data.int_handle == NULL ) )
  {
    ERR_FATAL("Cannot attach to int ctrl driver!", 0, 0, 0); 
  }

  if( ( DalTimetick_Attach("SystemTimer", &slpc_data.tt_handle) 
        != DAL_SUCCESS ) || ( slpc_data.tt_handle == NULL ) )
  {
    ERR_FATAL("Cannot attach to timetick driver!", 0, 0, 0); 
  }

  slpc_trace_init();

  slpc_c_fam = DalChipInfo_ChipFamily();
  slpc_c_ver = DalChipInfo_ChipVersion();

  if ( (slpc_c_fam == DALCHIPINFO_FAMILY_MSM8x26) &&
       (slpc_c_ver == DALCHIPINFO_VERSION(1,0)) )
  {
    slpc[SLPC_GSM].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_V1_BMSK;
    slpc[SLPC_1X].route_mask = HWIO_SYS_ONLINE_CTRLn_SYS_1X_EN_V1_BMSK;
    slpc[SLPC_HDR].route_mask = HWIO_SYS_ONLINE_CTRLn_DO_EN_V1_BMSK;
    slpc[SLPC_WCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_UMTS_EN_V1_BMSK;
    slpc[SLPC_LTE].route_mask = HWIO_SYS_ONLINE_CTRLn_LTE_EN_V1_BMSK;
    slpc[SLPC_TDSCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_TDS_EN_V1_BMSK;
    slpc[SLPC_GSM2].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G1_V1_BMSK;
    slpc[SLPC_GSM3].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G2_V1_BMSK;
    slpc_num_rsrc = HWIO_SYS_ONLINE_CTRLn_V1_MAXn + 1;
  }
  else if ( (slpc_c_fam == DALCHIPINFO_FAMILY_MSM8974) &&
            ( (slpc_c_ver == DALCHIPINFO_VERSION(2,0)) ||
              (slpc_c_ver == DALCHIPINFO_VERSION(2,1)) ||
              (slpc_c_ver == DALCHIPINFO_VERSION(2,2)) ) )
  {
    slpc[SLPC_GSM].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_8974V2_BMSK;
    slpc[SLPC_1X].route_mask = HWIO_SYS_ONLINE_CTRLn_SYS_1X_EN_8974V2_BMSK;
    slpc[SLPC_HDR].route_mask = HWIO_SYS_ONLINE_CTRLn_DO_EN_8974V2_BMSK;
    slpc[SLPC_WCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_UMTS_EN_8974V2_BMSK;
    slpc[SLPC_LTE].route_mask = HWIO_SYS_ONLINE_CTRLn_LTE_EN_8974V2_BMSK;
    slpc[SLPC_TDSCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_TDS_EN_8974V2_BMSK;
    slpc[SLPC_GSM2].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G1_8974V2_BMSK;
    slpc[SLPC_GSM3].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G2_8974V2_BMSK;
    slpc_num_rsrc = HWIO_SYS_ONLINE_CTRLn_8974V2_MAXn + 1;
  }
  else
  {
    slpc[SLPC_GSM].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_BMSK;
    slpc[SLPC_1X].route_mask = HWIO_SYS_ONLINE_CTRLn_SYS_1X_EN_BMSK;
    slpc[SLPC_HDR].route_mask = HWIO_SYS_ONLINE_CTRLn_DO_EN_BMSK;
    slpc[SLPC_WCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_UMTS_EN_BMSK;
    slpc[SLPC_LTE].route_mask = HWIO_SYS_ONLINE_CTRLn_LTE_EN_BMSK;
    slpc[SLPC_TDSCDMA].route_mask = HWIO_SYS_ONLINE_CTRLn_TDS_EN_BMSK;
    slpc[SLPC_GSM2].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G1_BMSK;
    slpc[SLPC_GSM3].route_mask = HWIO_SYS_ONLINE_CTRLn_GSM_EN_G2_BMSK;
    slpc_num_rsrc = HWIO_SYS_ONLINE_CTRLn_MAXn + 1;
  }

  ASSERT( slpc_num_rsrc <= SLPC_MAX_RESOURCES );

  for ( i = 0; i < slpc_num_rsrc; i++ )
  {
    if( DalInterruptController_RegisterISR( slpc_data.int_handle, 
            slpc_data.slpc_ustmr_rsrc[i].int_number, 
            slpc_data.slpc_ustmr_rsrc[i].isr_function,
            NULL, 
            slpc_data.slpc_ustmr_rsrc[i].int_trigger) != DAL_SUCCESS )
    {
      ERR_FATAL("Cannot register isr on rsrc %d", i, 0, 0); 
    }
  }

  for ( id = 0; id < SLPC_NUM_CLIENTS; id++ )
  {
    timer_group_set_deferrable(&slpc[id].wakeup.slpc_timer_group, FALSE);
    timer_def_priority( &slpc[id].wakeup.timer, &slpc[id].wakeup.slpc_timer_group, 
               NULL, 0, (timer_t1_cb_type) &slpc_wakeup_timer_cb, 
               (timer_cb_data_type) id, TIMER_PRIORITY_0);

    /* Initialize the sleep controller's state */
    slpc[id].state = SLPC_INACTIVE_STATE;
	slpc[id].slew_err_accum = 0;
    slpc[id].wakeup.invalid_cnt = 0;
    slpc[id].ending.invalid_cnt = 0;
    slpc[id].slpc_ustmr_rsrc = SLPC_NO_RESOURCE;
    slpc[id].warmup.dur_tick = 0;
    slpc[id].warmup.dur_usec = 0;

    for ( i = 0; i < SLPC_NUM_CLIENTS; i++ )
    {
      slpc[id].alt_techs[i].valid = FALSE;
      slpc[id].alt_techs[i].tstmr = 0;
    }
  }

  #ifdef SLPC_VSTMR
  slpc_data.vstmr_data.gsm_view1 = 
    vstmr_geran_get_view_handle(VSTMR_RTC_GERAN_SUB0, VSTMR_GERAN_GSTMR_VIEW);
  slpc_data.vstmr_data.gsm_view2 = 
    vstmr_geran_get_view_handle(VSTMR_RTC_GERAN_SUB1, VSTMR_GERAN_GSTMR_VIEW);
  slpc_data.vstmr_data.gsm_view3 = 
    vstmr_geran_get_view_handle(VSTMR_RTC_GERAN_SUB1, VSTMR_GERAN_GSTMR_VIEW);
  slpc_data.vstmr_data.wcdma_view = 
    vstmr_wcdma_get_view_handle(VSTMR_RTC_WCDMA_SUB0, VSTMR_WCDMA_REF_COUNT_VIEW);
  slpc_data.vstmr_data.onex_view = 
    vstmr_1x_get_view_handle(VSTMR_RTC_1X_SUB0, VSTMR_1X_RTC_VIEW);
  slpc_data.vstmr_data.hdr_view = 
    vstmr_hdr_get_view_handle(VSTMR_RTC_HDR_SUB0, VSTMR_HDR_RTC_VIEW);
  slpc_data.vstmr_data.lte_view = 
    vstmr_lte_get_view_handle(VSTMR_RTC_LTE_SUB0, VSTMR_LTE_OSTMR_VIEW);
  slpc_data.vstmr_data.tdscdma_view = 
    vstmr_tds_get_view_handle(VSTMR_RTC_TDS_SUB0, VSTMR_TDS_WALL_TIME_VIEW);
  #endif
}


