#ifndef RFM_MEAS_GSM_H
#define RFM_MEAS_GSM_H

/*
   @file
   rfm_meas_gsm.h

   @brief
   GSM specific interface file.

   @details
   
*/

/*===========================================================================
Copyright (c) 1999 - 2002, 2006, 2007 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
Copyright (c) 2008 by Qualcomm Technologies, Incorporated.  All Rights
                           Reserved. EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/api/meas/rfmeas_gsm.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/28/11   sar     Relocating file for rfa/api decoupling.
10/14/08   can     Initial version.

============================================================================*/

#include "rfcom.h"
#include "rfm_types.h"
#include "rfmeas_mc.h"
#include "rfmeas_types.h"

#endif /* RFM_MEAS_GSM_H */



