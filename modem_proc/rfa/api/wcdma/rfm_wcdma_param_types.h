#ifndef RFM_WCDMA_PARAM_TYPES_H
#define RFM_WCDMA_PARAM_TYPES_H
/*! @brief
  @file

  @brief
  This module contains the definitions of the WCDMA RFM interface parameters.
*/

/*===========================================================================


  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/api/wcdma/rfm_wcdma_param_types.h#1 $

when       who     what, where, why 
-------   ---     -------------------------------------------------------------
08/05/14  vbh     Updated the sleep params structure to take a flag to skip AGC disable
04/11/14  vbh     [FR17754] Added interface structure for tune 
3/05/14   vbh     Initial revision

==============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "rfcom.h"
#include "rfm_types.h"

/*----------------------------------------------------------------------------*/
/*! Macro defining maximum RFM devices supported for concurrent wakeup */
#define RFM_WCDMA_MAX_DEVICE RFM_DEVICE_4

/*----------------------------------------------------------------------------*/
/*! Macro defining maximum carriers supported on a device */
#define RFM_WCDMA_MAX_CARRIERS 3

/*----------------------------------------------------------------------------*/
/*! Structure to hold the parameters passed by L1 to wakeup WCDMA Rx  */
typedef struct
{
  struct
  {
     boolean is_valid;
     /*!< Flag to indicate if wakeup shall happen on this device */
 
     lm_handle_type rxlm_handle;
     /*!< RxLM buffer to be used to configure the modem hardware */
 
  } api_params[RFM_WCDMA_MAX_DEVICE];
  /*!< Structure to hold wakeup relevant parameters on all supported RFM devices */

}rfm_wcdma_rx_wakeup_params;

/*----------------------------------------------------------------------------*/
/*! Structure to hold the parameters passed by L1 to put WCDMA Rx to sleep  */
typedef struct
{
  struct
  {
     boolean is_valid;
     /*!< Flag to indicate if sleep shall happen on this device */
 
     lm_handle_type rxlm_handle;
     /*!< RxLM buffer to be used to configure the modem hardware */
 
     boolean skip_agc_disable; 
 
  } api_params[RFM_WCDMA_MAX_DEVICE];
  /*!< Structure to hold sleep relevant parameters on all supported RFM devices */

}rfm_wcdma_rx_sleep_params;

/*----------------------------------------------------------------------------*/
/*! Structure to hold the parameters passed by L1 to tune WCDMA Rx */
typedef struct
{
  struct
  {
     boolean is_valid;
     /*!< Flag to indicate if sleep shall happen on this device */
 
     lm_handle_type rxlm_handle;
     /*!< RxLM buffer to be used to configure the modem hardware */

     uint16 channel[RFM_WCDMA_MAX_CARRIERS];
     /*!< List of channels which need to be tuned on the particular device*/

     uint8 carrier_mask;
     /*!< Carrier mask to indicate the number of enabled carriers (0/1/2)*/
 
  } api_params[RFM_WCDMA_MAX_DEVICE];
  /*!< Structure to hold sleep relevant parameters on all supported RFM devices */

}rfm_wcdma_rx_tune_params;



#ifdef __cplusplus
}
#endif

#endif
