#ifndef RFLTE_MC_EXT_H
#define RFLTE_MC_EXT_H

/*
   @file
   rflte_ext_mc.h

   @brief
   RF LTE Driver's external interface file.

   @details

*/

/*===========================================================================
Copyright (c) 2002 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/api/lte/rflte_ext_mc.h#1 $
$DateTime: 2015/01/27 06:42:19 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
08/18/14   sk      MCL LTE support
07/31/14   ndb     Added support for extended EARFCN for LTE
07/18/14   jf      Return TX power in linear region to ML1
07/08/14   gvn     API for RRC to query split band channel raster
05/01/14   bsh     NLIC status to ML1
03/28/14   svi     Expose Intra Band CA capabilities to RRC
02/28/14   php     Add APIs for Light Sleep/Wakeup 
10/28/13   npi     Define LTE cell type enum
05/07/13   cri     Export temp comp scaling API
01/17/13   gvn     Expose some rflte_core_txpl APIs which are used by MCS team 
01/15/13   gvn     Fix compiler error 
01/04/13   gvn     Move interface to query RF LTE CA bc config from rfm to rflte_mc 
12/28/12   gvn     Initial Check-in

============================================================================*/

#include "rfcom.h"
#include "rfm_device_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*!
   @brief
   Enumeration for LTE Cells.

*/
typedef enum {
  RFLTE_MC_PCELL_IDX   = 0x00000000,
  RFLTE_MC_SCELL1_IDX  = 1,
  RFLTE_MC_SCELL2_IDX  = 2,
  RFLTE_MC_SCELL3_IDX  = 3,
  RFLTE_MC_SCELL4_IDX  = 4,
  RFLTE_MC_MAX_CELL_IDX_SUPPORTED
} rflte_mc_cell_idx_type;

typedef enum
{
  RFLTE_MC_EXT_CARR_TYPE_PRX = 0,
  RFLTE_MC_EXT_CARR_TYPE_DRX,
  RFLTE_MC_EXT_CARR_TYPE_INVALID = 0xFF
} rflte_mc_ext_carrier_type;

typedef struct 
{
    rfcom_lte_earfcn_type              pcc_tx_chan;
    rfcom_lte_earfcn_type              scc_rx_chan;
    rfcom_lte_bw_type   pcc_ul_bw;          /*PCELL UL BW*/ 
    rfcom_lte_bw_type   scc_dl_bw;          /*SCELL Rx BW*/ 
} nlic_status_input_params;

typedef struct 
{
    boolean                 nlic_possible;
    uint8                   victim_id;
} nlic_status_return_params;

typedef struct
{
   uint8 sf_num;
} rflte_mc_linear_tx_power_input_s;

typedef struct
{
   uint32 tx_power;
   uint32 tx_power_srs;
} rflte_mc_linear_tx_power_output_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

boolean rflte_mc_tx_coex_wlan_set_txplim(int16 coex_limit_db10) ;

uint64 rflte_mc_get_lte_ca_band_mask(rfcom_lte_band_type pcell_band);

uint32 rflte_core_get_tx_freq_from_uarfcn(rfcom_lte_earfcn_type tx_chan, rfcom_lte_band_type band);

uint32 rflte_core_get_rx_freq_from_uarfcn(rfcom_lte_earfcn_type rx_chan, rfcom_lte_band_type band);

uint16 rflte_core_get_bw_in_khz(rfcom_lte_bw_type bw);

uint16 rflte_core_temp_comp_raw_to_scaled(uint16 therm_raw_value);

void rflte_mc_light_sleep(void);

void rflte_mc_light_wakeup(void);

uint8 
rflte_mc_single_rx_ca_get_intra_band_ca_support
(
  rfcom_lte_band_type      band
);

void rflte_mc_get_nlic_status(nlic_status_input_params *nlic_input_params, nlic_status_return_params *nlic_return_params);

void rflte_mc_get_split_rx_band_channel_raster(rfcom_lte_band_type split_band, 
                                               rfcom_lte_earfcn_type *low_earfcn,
                                               rfcom_lte_earfcn_type *high_earfcn,
                                               uint8 *num_of_splits);

void rflte_mc_fed_get_linear_tx_power(rflte_mc_linear_tx_power_input_s* tx_power_input,
                                      rflte_mc_linear_tx_power_output_s* tx_power_output);

boolean rflte_mc_get_tuner_scenario_list(rfm_device_enum_type device,
                                         rflte_mc_ext_carrier_type carrier_type,
                                         uint32 channel,
                                         rfm_antenna_tuner_scenario_data *scenario_data,
                                         uint8 *free_space_index,
                                         rfm_tuner_handle_type *tuner_handle);

boolean rflte_mc_tuner_stop_mcl(rfm_device_enum_type device,
                                rflte_mc_ext_carrier_type carrier_type,
                                uint32 channel,
                                rfm_tuner_handle_type tuner_handle);



/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* RFLTE_MC_EXT_H */

