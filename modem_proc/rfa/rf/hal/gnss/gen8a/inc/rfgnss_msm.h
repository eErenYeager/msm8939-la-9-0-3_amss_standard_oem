#ifndef RFGNSS_MSM_H
#define RFGNSS_MSM_H

/*
   @file
   rfgnss_msm.h

   @brief
   This file contains RF GNSS msm definitions, enumerations and prototypes.

   @details
*/

/*===========================================================================
Copyright (c) 1999 - 2002, 2006, 2007 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
Copyright (c) 2008 - 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/rf/hal/gnss/gen8a/inc/rfgnss_msm.h#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/08/13   dbz     Add BP gain adjustment API for Biedou Triband mode
08/27/12   jr      Applied gain adjustment proportional to the PGA backoff 
04/26/12   vvr     Added get pre-mean interface, as part of FE CAL algorithm
02/24/12   dbz     Implemented new ADC power up/down sequence
12/06/11   dbz     Added support for IMD feature
09/14/11   dbz     Added LTE B13 gain adjustment algorithm
09/09/11   gy      Added RF Device revision for ADC setting
07/21/11   dbz     Added run time support for multi RF asic configs
07/06/11   dbz     Added API to trigger pre_mean/mon monitor in a single capture
05/18/11   gy      Added get GNSS ADC ref voltage and setpoint functions
04/27/11   gy      Decoupled gnss device from gnss api
04/23/11   gy      Ported for nikel
11/19/10   av      Fix compiler warning
11/09/10   av      Update the bb_gain_step when PGA is backed off
10/18/10   dbz     Added BP4 GLO notch filter algo
10/05/10   av      Merge changes from Genesis
06/02/10   dbz     Merged Mode 5 SRS support from Dev brach
05/27/10   dbz     Added support for FE cal for cucurrent ARS/SRS config
04/09/10   dbz     Added support for WB GNSS sync path support
                   for Callisto 1.x
04/05/10   dbz     Enable static notches for all modes (FTM, LPM, etc.)
01/06/10   dbz     Added rfm_gnss_debug support
12/17/09   dbz     Changed rfgnss_notch_cfg_type 
10/29/09   dbz     Fixed compiler warnings
10/21/09   dbz     Enabled RF status report 
10/16/09   dbz     Added support for GNSS spur mitigation algorithms
10/15/09   hp      Added support for ELNA API
10/13/09   dbz     Added GNSS ADC RC Tuning algorithms
09/30/09   dbz     Added RF status report support
12/20/08   gy      Initial version to port rf gnss msm.

============================================================================*/

#include "comdef.h"
#include "rfcom.h"
#include "rfgnss_mc.h"
#include "gnss_api.h"

/* ----------------------------------------------------------------------- */
/*!
   @brief
   This is a CGPS DAC code data structure.

   @details
*/
typedef struct
{
    int16    current_iDc;
    int16    current_qDc;
    uint16   current_iAmp;
    uint16   current_qAmp;
    uint32   current_GnssRtc;
    boolean  current_SampleCorrupted;
}rfgnss_meas_dc_gain;

typedef enum
{
  NAV_VER_1=1,
  NAV_VER_2,
  NAV_VER_3,
  NAV_VER_4,
  NAV_VER_MAX = NAV_VER_4
}rfgnss_nav_version_enum_type;

typedef struct
{
  rfgnss_nav_version_enum_type gnss_nav_version;
  gnss_BpMeanEstType rfgnss_msm_bp_mean_align;
  gnss_MeanMonConfigEnum rfgnss_msm_meas_period;
}rfgnss_nav_version_type;

typedef enum
{

  RFGNSS_MSM_BP_1_MODE2,
  RFGNSS_MSM_BP_1_3_MODE5,
  RFGNSS_MSM_BP_4_MODE5,

} rfgnss_msm_bp_mode_type;

typedef struct
{
  rfgnss_if_mode_enum_type pga_backoff_if_mode;
  rfgnss_mode_cfg_enum_type pga_backoff_rfgnss_mode;
}rfgnss_backoff_bb_update_type;

typedef struct
{
  uint8 i;
  uint8 q;
}rfgnss_adc_dcoffset_type;

typedef struct
{
  double i;
  double q;
}rfgnss_adc_mean_type;

typedef struct
{
  double i;
  double q;
}rfgnss_adc_ampl_type;

typedef struct 
{
  gnss_BpIfDownConvType gnss_bp1_if;
  gnss_BpIfDownConvType gnss_bp3_if;
  gnss_BpEqCoefType gnss_bp1_eq;
  gnss_BpEqCoefType gnss_bp3_eq;

} rfgnss_msm_rfasic_type; 

#define RFGNSS_DC_CANCELLATION_NFRAME      1  /* force 1 ms integration */
#define RFGNSS_STEADY_STATE_NFRAME         2  /* use Nframe method (typically 20 ms) */

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure sync ADC processing block.

   @details
     
   @param None

   @retval None

*/
void rfgnss_msm_sync_configure(void);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure BP1 for sync resampler input.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_sync_bp1_configure(rfgnss_if_mode_enum_type rfgnss_if_mode);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure BP3.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_bp3_configure(rfgnss_if_mode_enum_type rfgnss_id_mode);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Start/stop DC/amplitude estimation.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_meas_cmd(gnss_RfMeasCmdEnum e_MeasCmd);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Get DC mean value.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_get_dc_mean(gnss_AdcProcEnum e_AdcProc, gnss_MeanMonType * p_dc_mean);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Get BP preliminary amplitude.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_get_pre_gain(gnss_BpEnum e_Bp, gnss_AdcProcAmplMonType * p_pre_gain);


/*----------------------------------------------------------------------------*/
/*!
   @brief
   Get BP preliminary amplitude.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_get_pre_amp(gnss_BpEnum e_Bp, gnss_BpPreAmplMonType* p_pre_amp);


/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure GNSS I/Q input and ADC clock.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_input_configure( void );

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Do CGPS base band convertor initialization.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_bbc_init(rfgnss_mode_cfg_enum_type rfgnss_mode, rfgnss_if_mode_enum_type rfgnss_if_mode);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Enable/disable Digital Die GNSS ADC.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_adc_wakeup(rfgnss_adc_op_type wakeup);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure Digital Die GNSS ADC mode.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_adc_set_mode(rfgnss_adc_mode_enum_type rfgnss_adc_mode, uint8 rfdevice_encoded_rev);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure the sample clock M/N:D for digital die ADC and resampler.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_set_sample_clk( double d_SamplFreq );

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Do CGPS base band convertor FE cal setup.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_bootup_fe_cal_setup(void);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Do CGPS base band convertor FE cal cleanup.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_bootup_fe_cal_cleanup(void);

/*----------------------------------------------------------------------------*/
/*!
   @brief

   @details
   Sets up the environment for the baseband controller to configure 
   the gain depending on if external LNA is used or not. 
   
   @param device : RF Multimode device

   @param gnss_mode : GNSS Mode

*/
void rfgnss_msm_bbc_configure_ext_lna (boolean gnss_has_ext_lna);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function provides a status report on RF ASIC ID, ADC gain, DC I/Q dac 
   codes, RF VCO Tuning code, and RF OP mode.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_rf_status_report(gnss_RfStatusType *p_RfReport);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function provides the dynamic status report on DC I/Q dac, DC mean, Pre_amp

   @details

   @param None

   @retval None

*/
void rfgnss_msm_rf_dynamic_status_report
(
   gnss_RfDynamicStatusType *p_RfReport
);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Performs GNSS ADC DC Offset Tune.

   @details

   @param None

   @retval None

*/

void rfgnss_msm_adc_dc_offset_tune(rfgnss_adc_dcoffset_type *rfgnss_msm_adc_dcoffset);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function programs the notch filter settings

   @details

   @param None

   @retval None

*/
void rfgnss_msm_program_notch_filter(rfgnss_notch_cfg_type notch_cfg);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function programs the fixed notch filter settings

   @details

   @param None

   @retval None

*/
int rfgnss_msm_program_fixed_notch(void);

/*----------------------------------------------------------------------------*/
/*!
   @brief
  This function is used for debug purposes. This function queries the ADC to 
  ensure its powered up.

   @details

   @param None

   @retval   Boolean - TRUE if ADC is ON. FALSE otherwise.

*/
boolean rfgnss_msm_get_adc_state(void);

/*!
   @brief

   Query the nav version and determine the alignment mode and
   integration period.
 
   @details
 
   This function queries GPS SW to get nav version
   and is used to program in manual or automatic mode. The same modem
   can have different versions of the GPS engine.

   @param   

   @retval
*/
void rfgnss_msm_get_nav_version(void);

/*!
   @brief

   Query the nav version 

   @details
 
   This function returns the nav version.

   @param   

   @retval gnss_nav_version
*/
rfgnss_nav_version_enum_type rfgnss_msm_gps_get_version(void);

/*!
   @brief
   This function gets pre_amp

   @details

   @param None

   @retval None
*/
void rfgnss_msm_get_adc_amp(gnss_BpEnum e_Bp, rfgnss_adc_ampl_type * p_pre_gain);

/*!
   @brief
   This function gets pre_mean

   @details

   @param None

   @retval None
*/
void rfgnss_msm_get_adc_mean(gnss_AdcProcEnum e_AdcProc, rfgnss_adc_mean_type * p_dc_mean);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Do CGPS base band convertor FE cal setup.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_bootup_fe_cal_setup_mode(rfgnss_mode_cfg_enum_type gnss_mode,
                                                  rfgnss_lin_type_enum_type gnss_linearity);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Configure the sample clock rate for digital die ADC and resampler.

   @details

   @param None

   @retval None

*/
void rfgnss_msm_set_sample_clk_rate( gnss_AdcProcSampleRateEnum d_SamplRate );


/*!
   @brief
   This function updates the baseband gain step when the PGA gain is backed off

   @details

   @param None

   @retval None
*/

void rfgnss_msm_update_bb_gain_step( boolean pga_backoff, uint8 reduced_pga_gain );

/*!
   @brief
   This function updates the BP1 with the new bb_gain_step

   @details

   @param None

   @retval None
*/
void rfgnss_msm_bp1_update( boolean pga_backoff );

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function programs the fixed notch filter settings

   @details

   @param None

   @retval int

*/
int rfgnss_msm_program_fixed_notch_mode5_bp1(rfgnss_notch_cfg_type notch_cfg);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function programs the fixed notch filter settings

   @details

   @param None

   @retval int

*/
int rfgnss_msm_program_fixed_notch_mode5_bp3(rfgnss_notch_cfg_type notch_cfg);

/*----------------------------------------------------------------------------*/
/*!
   @brief
   This function programs the fixed notch filter settings

   @details

   @param None

   @retval int

*/
int rfgnss_msm_program_fixed_notch_mode5_bp4(rfgnss_notch_cfg_type notch_cfg);

/*!
   @brief
   This function provides GNSS ADC reference voltage

   @details

   @param None

   @retval rfgnss_adc_Vref: GNSS ADC ref voltage
*/
float rfgnss_msm_get_adc_vref( void );

/*!
   @brief
   This function provides GNSS ADC setpoint

   @details

   @param None

   @retval rfgnss_adc_Vsetpoint: GNSS ADC setpoint
*/
float rfgnss_msm_get_adc_vsetpoint( void );

/*!
   @brief
   This function gets pre_mean and adc_amp in a single capture

   @details

   @param None

   @retval None
*/
void rfgnss_msm_get_adc_mean_amp(gnss_AdcProcEnum e_AdcProc, gnss_BpEnum e_Bp, 
                             rfgnss_adc_mean_type * p_dc_mean, rfgnss_adc_ampl_type * p_pre_gain);


/*!
   @brief
   This function sets the RF asic specific MSM baseband settings

   @details

   @param None

   @retval None
*/
void rfgnss_msm_set_rfasic_specific(rfgnss_msm_rfasic_type* rf_asic);

void rfgnss_msm_rf_lte_b13_txstatus(boolean TxOn);

void rfgnss_msm_wwan_band_chan(  gnss_WwanStateEnum       e_CurWwan,
  gnss_RfBandType          e_Band,
  uint32                   q_Channel);

void rfgnss_msm_wwan_tx(boolean u_TxOn);

void rfgnss_msm_adc_clk_ctl(boolean enable);

/* -------------------------------------------------------------------------
 @brief
   This function adjust the BP gain step for BP2 for Beidou Triband mode
 
  Parameters:
   TxOn True - LTE B13 started transmitting
        False - LTE B13 has stopped transmitting
 
  Return value: 
   None
*/
void rfgnss_msm_bp2_gainstep_adjust( void );


#endif /* RFGNSS_MSM_H */


