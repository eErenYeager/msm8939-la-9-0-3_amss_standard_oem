/*!
   @file
   rfcommon_concurrency_manager.c

   @brief
 
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================
Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved. 

                       EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------------------------------------------------------------------------------
08/25/14   par     Optimize F3s impacting iRAT timelines
08/04/14   tks     Updated validate scenario api to return status per rfm dev
07/27/14   tks     Updated alternate rx path function to handle irat/standalone
07/23/14   tks     Included core utils header file to use helper function
07/22/14   tks     Update ccry mgr to support alt path for irat/ifreq 
07/07/14   tks     Modified return logic for validate scenario api
06/16/14   tks     Added support for trm concurrency functions
05/28/14   tks     Added interface api's for trm support
05/12/14   tks     Updated api names to indicate rx behavior
05/12/14   kab     Remove RFCOM_RECEIVER_DUAL Support
04/24/14   tks     Fix klocwork error
04/23/14   tks     Initial revision 
 
==============================================================================*/

#include "rfcommon_concurrency_manager.h"
#include "rfm.h"
#include "rfc_card.h"
#include "rfcommon_core_utils.h"

#ifdef TEST_FRAMEWORK
#error code not present
#endif 

/* One single copy of the concurrency manager */
static rfcmn_state_mgr_type rfcmn_concurrency_db;

/* Structure to hold Rx reconfig status for each logical device */
rfm_device_allocation_status_type rx_reconfig; 

/*----------------------------------------------------------------------------*/
/*          Function prototypes internal to concurrency manager               */
/*----------------------------------------------------------------------------*/
boolean rfcmn_reset_rx_concurrency_tbl_entry(rfm_device_enum_type rfm_dev); 

rfcmn_status_type rfcmn_concurrency_mgr_get_alt_rx_path
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band,
  uint8 *alt_path,
  boolean is_irat_mode
); 

/*----------------------------------------------------------------------------*/
/*                          Interface functions                               */
/*----------------------------------------------------------------------------*/
/*! 
  @brief
  API to initialize concurrency manager 
 
  @details
  Function is used to initialize the concurrency manager to default state. The
  current band and technologies per logical device are set to default state,
  while the alternate path and wide band indices are set to zero. This function
  should be called as part of rfm_init
 
  @return
  TRUE indicates initialization is successful 
*/
rfcmn_status_type rfcmn_concurrency_mgr_rx_init(void)
{
  uint8 loop_idx = 0;

  for(loop_idx = (uint8)RFM_DEVICE_0; loop_idx < RFM_MAX_WAN_DEVICES; loop_idx++)
  {
    rfcmn_reset_rx_concurrency_tbl_entry((rfm_device_enum_type)loop_idx); 
  }

  return RFCMN_PATH_SEL_SUCCESS; 

}/* end of rfcmn_concurrency_mgr_rx_init() */

/*----------------------------------------------------------------------------*/
/*! 
  @brief
  API to update concurrency manager at start of channel tune 
 
  @details
  Function is used to determine the alternate path to use for a particular
  scenario. The alternate path and wide band indices help decide which
  physical path needs to be configured in a particular scenario. The selection
  of a specific path depends on which tech is using a particular chain and how
  to perform configuration without breaking the existing link. 
 
  @param rfm_dev
  current logical device in use 
 
  @param curr_mode
  current technology in use 
 
  @param curr_band
  current band in use 

  @return
  TRUE is a successful match is found in path selection table, else return FALSE 

*/
rfcmn_status_type rfcmn_concurrency_mgr_update_rx_state
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band
)
{
  /* Update concurrency manager table */
  rfcmn_concurrency_db.curr_state[rfm_dev].logical_dev = rfm_dev;
  rfcmn_concurrency_db.curr_state[rfm_dev].curr_mode   = curr_mode;
  rfcmn_concurrency_db.curr_state[rfm_dev].curr_band   = curr_band;

  return RFCMN_CONCURRENCY_MGR_UPDATE_SUCCESS; 

} /* end of rfcmn_concurrency_mgr_update_rx_state() */

/*----------------------------------------------------------------------------*/
/*! 
  @brief
  API to update concurreny manager during exit mode 
 
  @details
  Function resets the all information related to particular logical device.
  Clean up here effectively means that a particular tech/band/chan is done using
  a specific logical device and is quitting that mode. 
 
  @param rfm_dev
  current logical device in use 
 
  @param curr_mode
  current technology in use 
 
  @param curr_band
  current band in use 

  @return
  TRUE if clean up of a specific logical device is successful, else return FALSE 

*/
rfcmn_status_type rfcmn_concurrency_mgr_rx_cleanup
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band
)
{
  boolean api_status = FALSE; 

  if( rfcmn_concurrency_db.curr_state[rfm_dev].logical_dev == rfm_dev &&
      rfcmn_concurrency_db.curr_state[rfm_dev].curr_mode == curr_mode &&
      rfcmn_concurrency_db.curr_state[rfm_dev].curr_band == curr_band )
  {
    api_status = rfcmn_reset_rx_concurrency_tbl_entry(rfm_dev);
  }

  if (api_status)
  {
    return RFCMN_PATH_SEL_SUCCESS; 
  }
  else
  {
    return RFCMN_PATH_SEL_FAILED; 
  }

} /* end of rfcmn_concurrency_mgr_rx_cleanup() */

/*----------------------------------------------------------------------------*/
/*! 
  @brief
  Helper function to detect alternate path information and update the devices
 
  @details
  Function is called when an alternate path selection has to be made based 
  on concurrency scenario. Based on the alternate path selected the api needs
  to figure out the path swapping requirement, cell reconfiguration and also
  indicate if a possible reconfig has succeeded or failed. 
 
  @param rfm_dev
  current logical device in use 
 
  @param curr_mode
  current technology in use 
 
  @param curr_band
  current band in use 
 
  @return alt_path
  return the alternate path to use for current logical device

*/
rfcmn_status_type rfcmn_concurrency_mgr_get_alt_rx_path
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band,
  uint8 *alt_path,
  boolean is_irat_mode
)
{
  uint8 idx                         = 0;
  uint8 tbl_index                   = 0; 
  uint8 tbl_size                    = 0;
  uint8 lst_size                    = 0; 
  boolean match_found        = FALSE;
  boolean entry_found        = FALSE; 
  rfm_device_enum_type temp_rfm_dev = RFM_INVALID_DEVICE; 
  rfcmn_status_type api_status      = RFCMN_PATH_SEL_INVALID; 
  boolean is_reconfig_needed[RFM_MAX_WAN_DEVICES] = {FALSE, FALSE, FALSE, FALSE, 
                                                     FALSE, FALSE}; 

  rfc_alt_path_sel_type *curr_tbl   = rfc_get_alt_path_tbl( &tbl_size, 
                                                            &lst_size );

  if (is_irat_mode)
  {
    curr_tbl = rfc_get_irat_alt_path_tbl( &tbl_size, &lst_size ); 
  }

  /* Check if this specific rf_card supports alternate path selection */
  if (curr_tbl == NULL)
  {
    /* Set default alternate path & return path selection is not supported */
    *alt_path = 0; 
    return RFCMN_PATH_SEL_NOT_SUPPORTED; 
  }

  /* Loop through the path swapping table looking for a match */
  for (idx = 0; idx < tbl_size; idx++)
  {
    match_found = TRUE;
    entry_found = FALSE; 

    /* Check if a match is found for current logical device */
    if ( ((curr_tbl + idx * lst_size + rfm_dev)->band == rfcmn_concurrency_db.curr_state[rfm_dev].curr_band) &&
         ((curr_tbl + idx * lst_size + rfm_dev)->tech == rfcmn_concurrency_db.curr_state[rfm_dev].curr_mode) )
    {
      /*** Loop through all logical devices ***/
      for ( temp_rfm_dev = RFM_DEVICE_0; temp_rfm_dev < RFM_DEVICE_4; temp_rfm_dev++ )
      {
        /* Check if logical device is of PRx path type */
        if( (curr_tbl + idx * lst_size + temp_rfm_dev)->rx_path == RFC_PRX_PATH )
        {
          /* Check if the band information matches for all PRx path */
          if( ((curr_tbl + idx * lst_size + temp_rfm_dev)->band == rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_band) &&
              ((curr_tbl + idx * lst_size + temp_rfm_dev)->tech == rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode) )
          {
            match_found &= TRUE; 
            entry_found  = TRUE; 
          }
          else
          {
            match_found &= FALSE; 
          }
        }
      }

      /*** If match found then store the table index ***/
      if (match_found == TRUE && entry_found == TRUE)
      {
        tbl_index = idx;
        break; 
      }
    }
  }

  /* Depending on the match found update the state machine for alt path */
  if (match_found == TRUE && entry_found == TRUE)
  {
    /* Store alternate path info for current logical device */
    for ( temp_rfm_dev = RFM_DEVICE_0; temp_rfm_dev < RFM_MAX_WAN_DEVICES; temp_rfm_dev++ )
    {
      /* Update the state machine with alt path info if the particular 
         logical device is in use */
      if ((rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode != RFM_PARKED_MODE) && 
          (rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode < RFM_NUM_MODES))
      {
        if ( (curr_tbl + tbl_index * lst_size + rfm_dev)->alt_path == 
             rfcmn_concurrency_db.curr_state[temp_rfm_dev].alt_path )
        {
          /* reconfig not-needed */
          is_reconfig_needed[temp_rfm_dev] = FALSE; 
        }
        else
        {
          /* reconfig needed */
          is_reconfig_needed[temp_rfm_dev] = TRUE; 

          /* Update concurrency manager with alternate path info */
          rfcmn_concurrency_db.curr_state[temp_rfm_dev].alt_path 
            = (curr_tbl + tbl_index * lst_size + temp_rfm_dev)->alt_path; 
        }
      }
      else
      {
        // Nothing to do 
      }
    }
    
    /* Set api status to success */
    api_status = RFCMN_PATH_SEL_SUCCESS; 
  }
  else
  {
    /* If not match is found set to default alternate path */
    rfcmn_concurrency_db.curr_state[rfm_dev].alt_path = 0; 
    api_status = RFCMN_PATH_SEL_SUCCESS; 
    RF_MSG(RF_HIGH, "No matching path selection entry found - "
                    "Setting ALT_PATH to DEFAULT");
  }

  /* Retrieve alt path to use for current rfm_device */
  *alt_path = rfcmn_concurrency_db.curr_state[rfm_dev].alt_path; 

  RF_MSG_5( RF_HIGH, 
            "Re-config requirements - RFM_DEVICE: "
            "DEV0 - %d DEV1 - %d DEV2 - %d DEV3 - %d, ALT PATH - %d", 
            is_reconfig_needed[RFM_DEVICE_0], is_reconfig_needed[RFM_DEVICE_1], 
            is_reconfig_needed[RFM_DEVICE_2], is_reconfig_needed[RFM_DEVICE_3], 
            *alt_path ); 

  return api_status; 

} /* end of rfcmn_concurrency_mgr_get_alt_rx_path() */

/*----------------------------------------------------------------------------*/
rfcmn_status_type rfcmn_concurrency_mgr_update_rx_path
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band,
  uint8 *alt_path
)
{
  /* Get alternate path for standalone tech operation */
  return 
    (rfcmn_concurrency_mgr_get_alt_rx_path(rfm_dev, curr_mode, 
                                           curr_band, alt_path, FALSE)); 

} /* end of rfcmn_concurrency_mgr_update_rx_path() */

/*----------------------------------------------------------------------------*/
rfcmn_status_type rfcmn_concurrency_mgr_update_irat_rx_path
(
  rfm_device_enum_type rfm_dev,
  rfcom_mode_enum_type curr_mode,
  int curr_band,
  uint8 *alt_path
)
{
  /* Get alternate path for inter-rat or inter-freq tech operation */
  return 
    (rfcmn_concurrency_mgr_get_alt_rx_path(rfm_dev, curr_mode, 
                                           curr_band, alt_path, TRUE)); 

} /* end of rfcmn_concurrency_mgr_update_irat_rx_path() */

/*----------------------------------------------------------------------------*/
/*                         TRM interface functions                            */
/*----------------------------------------------------------------------------*/
/*! 
  @brief
  Logical device allocation for a tech and band combo
 
  @details
  This function will update the rf concurrency manager with the current
  tech and band allocation details provided by TRM. 
 
  @param dev_allocation
  contains details about which band and tech is used on a specific logical
  device 
  
  @return 
  Flag to indicate if the device allocation has been successful or not 
*/
boolean 
rfcmn_concurrency_mgr_rx_update_device_alloc
(
  rfm_device_allocation_type *dev_list
)
{
  uint8 arr_idx = 0; 

  if (dev_list == NULL)
  {
    RF_MSG(RF_ERROR, "RF concurrency mgr device alloc list is empty"); 
    return FALSE; 
  }

  for ( arr_idx = (uint8)RFM_DEVICE_0; 
        arr_idx < (uint8)RFM_MAX_WAN_DEVICES; 
        arr_idx++ )
  {
    /* Update concurrency db with band info */
    rfcmn_concurrency_db.curr_state[arr_idx].curr_band 
      = rfcommon_core_convert_sys_band_to_rfcom_band(dev_list->device_allocation[arr_idx].band); 

    /* Update concurrency db with tech info */
    rfcmn_concurrency_db.curr_state[arr_idx].curr_mode
      = dev_list->device_allocation[arr_idx].tech; 

    /* Update concurrency db with logical dev info */
    rfcmn_concurrency_db.curr_state[arr_idx].logical_dev
      = (rfm_device_enum_type)arr_idx;

    /*! Should we reset alternate path every time state machine is updated ?? */
  }
  
  return TRUE; 

} /* end of rfcmn_concurrency_mgr_rx_update_device_alloc */

/*----------------------------------------------------------------------------*/
/*! 
  @brief
  Check concurrency possibilities for a particular tech and band combo
 
  @details
  The function which check with the rf concurrency manager if it is possible
  to allocate a particular logical device to a specific tech and band combo
 
  @param dev_client
  contains details about which band and tech is used on a specific logical
  device
 
  @return 
  Flag to indicate if the dev client info is valid or not
*/
rfm_device_allocation_status_type* 
rfcmn_concurrency_mgr_rx_validate_scenario
(
  rfm_device_client_info_type *dev_client
)
{
  uint8 idx           = 0;
  uint8 tbl_index     = 255; 
  uint8 tbl_size      = 0;
  uint8 lst_size      = 0; 
  boolean match_found        = FALSE;
  boolean entry_found        = FALSE; 
  boolean is_reconfig_needed = FALSE; 
  int test_rf_band    = -1; 
  rfcom_mode_enum_type test_rf_mode = RFCOM_NUM_MODES; 
  rfm_device_enum_type temp_rfm_dev = RFM_INVALID_DEVICE; 

  rfc_alt_path_sel_type *curr_tbl = rfc_get_alt_path_tbl( &tbl_size, 
                                                          &lst_size );

  memset(&rx_reconfig, RFM_DEVICE_STATUS_INVALID, sizeof(rfm_device_allocation_status_type)); 

  if (dev_client == NULL)
  {
    RF_MSG(RF_ERROR, "RF concurrency mgr device alloc client is empty"); 
    // return FALSE; 
    return &rx_reconfig;
  }

  if (curr_tbl == NULL)
  {
    RF_MSG(RF_HIGH, "RFC doesn't have path selection table - all ccry modes supproted"); 
    // return TRUE; 
    return &rx_reconfig; 
  }

  /* Check if the logical dev which TRM plans to add is already in use */
  if ( rfcmn_concurrency_db.curr_state[dev_client->rfm_dev].curr_mode < RFM_NUM_MODES && 
       rfcmn_concurrency_db.curr_state[dev_client->rfm_dev].curr_mode != RFM_PARKED_MODE )
  {
    /* Chain is already in use, hence deny the request */
    return &rx_reconfig; 
  }

  /* If the logial device is not used by anyone then lets compare the table 
     entries to see if we can get a match */
  for (idx = 0; idx < tbl_size; idx++)
  {
    match_found = TRUE;
    entry_found = FALSE; 

    /*** Loop through all logical devices ***/
    for ( temp_rfm_dev = RFM_DEVICE_0; 
          temp_rfm_dev < (rfm_device_enum_type)lst_size; temp_rfm_dev++ )
    {
      /* Determine the test params - rf mode, rf band and logical device */
      if(dev_client->rfm_dev == temp_rfm_dev)
      {
        // test_rfm_dev = dev_client->rfm_dev; 
        test_rf_mode = dev_client->tech;
        test_rf_band = rfcommon_core_convert_sys_band_to_rfcom_band(dev_client->band); 
      }
      else
      {
        // test_rfm_dev = rfcmn_concurrency_db.curr_state[temp_rfm_dev].logical_dev;
        test_rf_mode = rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode;
        test_rf_band = rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_band;
      }

      /* Check if a table match is found */
      if( ((curr_tbl + idx * lst_size + temp_rfm_dev)->rx_path == RFC_PRX_PATH)  && 
          ((curr_tbl + idx * lst_size + temp_rfm_dev)->tech    != RFM_ALL_MODES) && 
          ((curr_tbl + idx * lst_size + temp_rfm_dev)->band    != 0xFF) )
      {
        if( ((curr_tbl + idx * lst_size + temp_rfm_dev)->band == test_rf_band) && 
            ((curr_tbl + idx * lst_size + temp_rfm_dev)->tech == test_rf_mode ) )
        {
          match_found &= TRUE; 
          entry_found  = TRUE; 
        }
        else
        {
          match_found &= FALSE; 
        }
      }
    }

    /*** If match found then store the table index ***/
    if (match_found == TRUE && entry_found == TRUE)
    {
      tbl_index = idx;
      break; 
    }
  }

  RF_MSG_1(RF_HIGH,"RF concurrency mgr found a match - %d", tbl_index); 

  /*** Check if a match is found in the path selection table ***/
  if (match_found == TRUE && entry_found == TRUE)
  {
    /* Loop thro all logical devices and update status accordingly */
    for (temp_rfm_dev = RFM_DEVICE_0; 
          temp_rfm_dev < RFM_MAX_WAN_DEVICES; temp_rfm_dev++)
    {
      /* */
      if (temp_rfm_dev != dev_client->rfm_dev)
      {
        /* Current logical dev should be in a mode other num mode or parked mode */
        if (rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode != RFM_NUM_MODES && 
            rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode != RFM_PARKED_MODE)
        {
          /* If alt path is different then re-config is needed */
          if ( rfcmn_concurrency_db.curr_state[temp_rfm_dev].alt_path != 
               (curr_tbl + idx * lst_size + temp_rfm_dev)->alt_path )
          {
            rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_RECONFIG_REQUIRED; 
          }
          /* If alt path is not different then reconfig is not required */
          else
          {
            rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_RECONFIG_NOT_REQD; 
          }
        }
        /* Current logical dev is in parked or num mode which means its inactive */
        else
        {
          rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_CURRENTLY_INACTIVE; 
        }
      }
      /* Current device which is being checked for adding a new cell */
      else
      {
        rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_CURRENTLY_INACTIVE; 
      }
    }
  }
  /*** No match is found in path selection table but still need to return an
       accurate picture of what is recorded in rf state machine ***/
  else
  {
    for (temp_rfm_dev = RFM_DEVICE_0; 
          temp_rfm_dev < RFM_MAX_WAN_DEVICES; temp_rfm_dev++)
    {
      /* If current mode is not parked or num mode then mark it as no-reconfig */
      if (rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode != RFM_NUM_MODES && 
          rfcmn_concurrency_db.curr_state[temp_rfm_dev].curr_mode != RFM_PARKED_MODE)
      {
        rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_RECONFIG_NOT_REQD; 
      }
      /* If current mode is num or parked mode then mark it as inactive */
      else
      {
        rx_reconfig.curr_status[temp_rfm_dev] = RFM_DEVICE_CURRENTLY_INACTIVE; 
      }
    }
  }

  return &rx_reconfig; 

} /* end of rfcmn_concurrency_mgr_rx_validate_scenario */

/*----------------------------------------------------------------------------*/
/*                            Helper functions                                */
/*----------------------------------------------------------------------------*/
/*! 
  @brief
  Helper function to reset the a particular row in concurrency manager table 
 
  @param rfm_dev
  current logical device in use 

  @return
  TRUE if update is successful, else return FALSE 

*/
boolean rfcmn_reset_rx_concurrency_tbl_entry
(
  rfm_device_enum_type rfm_dev
)
{
  rfcmn_concurrency_db.curr_state[rfm_dev].logical_dev = rfm_dev; 
  rfcmn_concurrency_db.curr_state[rfm_dev].curr_mode   = RFCOM_NUM_MODES; 
  rfcmn_concurrency_db.curr_state[rfm_dev].curr_band   = 0xFF; 
  rfcmn_concurrency_db.curr_state[rfm_dev].alt_path    = 0;
  rfcmn_concurrency_db.curr_state[rfm_dev].wb_index    = 0;
  return TRUE; 

} /* end of rfcmn_reset_concurrency_tbl_entry() */

#ifdef TEST_FRAMEWORK
#error code not present
#endif

