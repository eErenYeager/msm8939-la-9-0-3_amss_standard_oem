/*!
   @file
   test_rfcommon_concurrency_manager.h

   @brief
 
*/

/*===========================================================================

Copyright (c) 2012 - 2014 by QUALCOMM Technologies Inc. All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
-------------------------------------------------------------------------------
04/23/14   tks     Initial version.

============================================================================*/

#include "rfcommon_concurrency_manager.h"

/* Interface function to retrieve the concurrency manager table */
rfcmn_state_mgr_type* rfcommon_get_concurrency_mgr_data(void);

