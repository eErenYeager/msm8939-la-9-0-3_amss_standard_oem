/* 
  File that stores all RF off target stubs. 
*/
/*===========================================================================
Copyright (c) 2010 by Qualcomm Technologies Incorporated.
All Rights Reserved.

                        EDIT HISTORY FOR MODULE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/rf/common/rf/mc/src/rf_offtarget_stubs.c#1 $
  $DateTime: 2015/01/27 06:42:19 $ 
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/11/13   sar     Updated file for APQ, GNSS only target.
06/20/12   sty     deleted rfm_init_bbrx_sleep 
06/14/12   zhw     Initial Revision
===========================================================================*/


#ifndef FEATURE_GNSS_ONLY_NO_WWAN
#ifdef TEST_FRAMEWORK
#error code not present
#endif /* TEST_FRAMEWORK */
#endif /* FEATURE_GNSS_ONLY_NO_WWAN */
