/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         F T M  C O M M O N

GENERAL DESCRIPTION
  This is the FTM common file that contains common FTM variables and functions

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

  Copyright (c) 2006 by Qualcomm Technologies INCORPORATED.
  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/rf/common/ftm/src/ftm_common.c#1 $
$DateTime: 2015/01/27 06:42:19 $
$Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/17/06   vm/jfc  Created for initial version of FTM for MSM7600

===========================================================================*/


/*===========================================================================

                         INCLUDE FILES FOR MODULE

===========================================================================*/

#include "rfa_variation.h"
#include "customer.h"
#include "comdef.h"

#ifdef FEATURE_FACTORY_TESTMODE

#include "ftm_common.h"

uint16 ftm_tx_rx_freq_cal_channels[FTM_TX_RX_FREQ_CAL_MAX_FREQS];

uint8  ftm_tx_rx_freq_cal_pa_ranges[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

uint16 ftm_tx_rx_freq_cal_pdm_list[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

uint8  ftm_tx_rx_freq_cal_hdet_read_list[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

uint8  ftm_tx_rx_freq_cal_rx_actions[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

int16  ftm_tx_rx_freq_cal_exp_agc[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

int16  ftm_tx_rx_freq_cal_rx_result[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ*FTM_TX_RX_FREQ_CAL_MAX_FREQS];

uint16 ftm_tx_rx_freq_cal_tx_result[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ*FTM_TX_RX_FREQ_CAL_MAX_FREQS];

uint8  ftm_tx_rx_freq_cal_rx_lpm_list[FTM_TX_RX_SWEEP_MAX_STEPS_PER_FREQ];

#endif /* FEATURE_FACTORY_TEST_MODE */
