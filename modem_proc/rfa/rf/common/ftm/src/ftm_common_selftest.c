/*!
  @file
  ftm_common_selftest.c

  @brief
  This module contains definitions and prototypes for common selftest functionality.
*/

/*==============================================================================

  Copyright (c) 2012 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/05/14   jmf     [SelfTest] Remove RFCMD FW message to set self-test flag
07/31/14   jmf     Add API to query Current Self Test Mode
                   Remove call to RFCMD MSG to set FW Self Test Mode
07/24/14   jmf     [SelfTest] Move self test mode global flag to .c
07/23/14   dbz     Added support for selftest VSWR split and EVM capture for 1x/LTE
07/17/14   jmf     Add api to set self test flag
06/30/14   daa     Created.

==============================================================================*/

#include "rfa_variation.h"
#include "target.h"    /* Target specific definitions            */
#include "comdef.h"    /* Common stuff                           */
#include "ftmicap.h"
#include "stringl.h" /* for memscpy */
#include "ftm_calibration_data_module.h"
#include "ftm_common_selftest.h"

#ifdef FEATURE_FACTORY_TESTMODE
#include "rex.h"
#include "task.h"
#include "ftm.h"
#include "ftmdiag.h"
#include "ftm_common_dispatch.h"
#include "ftm_common.h"
#include "rf_test_task_v.h"
#include "rfcommon_nv.h"
#include "rfm.h"
#include "rfm_internal.h"
#include "ftm_rf_cmd.h"
#ifndef FEATURE_GNSS_ONLY_NO_WWAN
#include "rxlm_intf.h"
#endif
#include "ftm_common_control.h"
#include "ftm_msg.h"
#include "rfc_card.h"
#ifndef FEATURE_GNSS_ONLY_NO_WWAN
#include "ftm_common_radio.h"
#include "ftm_cdma_cmd.h"
#include "ftm_rffe.h"
#endif
#include "ftm_gpio.h"
#include "modem_mem.h"
#include "rfcommon_mc.h"
#ifndef FEATURE_GNSS_ONLY_NO_WWAN
#include "ftm_common_xpt.h"
#include "rfcommon_core_xpt_buffer.h"
#include "rfcommon_core_xpt.h"
#endif
#include "rfcommon_mdsp_types.h"
#include "rfcommon_core_utils.h"

#ifdef FEATURE_WCDMA
#include "ftm_wcdma_ctl.h"
#include "rfdevice_wcdma_intf.h"
#include "rfnv_wcdma.h"
#include "rfwcdma_msm.h"
#include "rfdevice_wcdma_pa_intf.h"
#include "rfdevice_wcdma_asm_intf.h"
#include "ftm_wcdma_dispatch.h"
#include "ftm_wcdma_calv3.h"
#endif

#ifdef FTM_HAS_TDSCDMA
#include "ftm_tdscdma_ctl.h"
#include "rfdevice_tdscdma_pa_intf.h"
#include "rfdevice_tdscdma_asm_intf.h"
#include "rf_tdscdma_msm.h"
#include "rf_tdscdma_core_util.h"
#endif


#ifdef FEATURE_CDMA1X
#include "ftm_1x_control.h"
#include "ftm_1x_calibration.h"
#include "rfdevice_cdma_interface.h"
#include "ftm_cdma_dispatch.h"
#include "rfc_cdma.h"
#include "rfm_1x_ftm.h"
#include "rfcommon_core_device_manager.h"
#include "rfcommon_locks.h"
#include "rf_cdma_data.h"
#include "ftm_1x_xpt.h"
#include "ftm_cdma_data.h"
#include "rf_cdma_msm.h"
#include "rf_cdma_mc.h"
#include "rfm_cdma_ftm.h"
#endif

#ifdef FEATURE_GSM
#include "ftm_gsm_ctl.h"
#include "rfdevice_gsm_intf.h"
#include "ftm_gsm_rfprot.h"
#endif

#ifdef FEATURE_LTE
#include "rf_lte_ftm_cmd_proc.h"
#include "rflte_ftm_external_api.h"
#include "ftm_lte_xpt.h"
#include "rflte_ftm_mc.h"
#include "rflte_nv.h"
#include "rflte_msm.h"
#include "rflte_core_util.h"
#include "rfdevice_lte_pa_intf.h"
#include "rfdevice_lte_interface.h"
#include "rfdevice_lte_asm_intf.h"
#include "rflte_msm_iq_capture.h"
#include "ftm_lte_common_dispatch.h"
#endif

#ifdef FEATURE_TDSCDMA
#include "ftm_tdscdma_ctl.h"
#include "ftm_tdscdma_calv3.h"
#endif

#ifdef FEATURE_RF_HAS_QTUNER

#ifdef FEATURE_CDMA1X
#include "rf_cdma_utils_freq.h"
#include "rfdevice_hdet_cdma_intf.h"
#endif

#ifdef FEATURE_WCDMA
#include "rfwcdma_core_antenna_tuner.h"
#endif

#include "rfdevice_hdet_cmn_intf.h"
#include "rfdevice_hdet_wcdma_intf.h"
#endif

#include "ftm_common_calibration_v3.h"
#include "ftm_calibration_v3_data.h"
#include "rf_qmi_rfrpe_svc.h"

#if defined (FEATURE_XO_FACTORY_CAL)
#include "ftm_calibration_xo.h"
#endif

#include "rfdevice_logical_component_types.h"

// extern boolean ftm_selftest_pwr_only;
// coupler_direction_enum_type tdscdma_coupler_direction = COUPLER_DIRECTION_FORWARD;

uint8 ftm_common_selftest_modeset_flag = 0;

boolean
ftm_common_trigger_self_test_iq_cap
(
  ftm_rf_technology_type ftm_rf_tech,
  uint8  device,
  uint8  device_fb,
  uint16 num_samples,
  uint32 ftm_selftest_meas_type
)
{
  boolean do_processing = (ftm_selftest_meas_type > 0)? TRUE: FALSE;

  volatile boolean api_status = TRUE; /* shouldn't this be volatile? */
  rf_common_xpt_buffer_id_type buffer_ids; /* buffer ids for capture and dpd results */
  rfcommon_xpt_processing_type proc_flag = PROC_NONE;
  uint32 samp_rate;
  uint8 ftm_selftest_xpt_capture_type;


  /* Init Buffer ID values */
  buffer_ids.capt_buf_id = 0;
  buffer_ids.res_buf_id = 0;

  /* Ensure no previus capture/processing is still in progress */
  if( (api_status == TRUE) )
  {
    api_status &= rfcommon_xpt_is_capture_done();
    if( api_status == FALSE)
    {
      FTM_MSG_1( FTM_ERROR, "ftm_common_trigger_ept_iq_cap: IQ capture already "
                 "in progress for tech %d", ftm_rf_tech );
    }

    /* Monitor processing only if it was requested */
    if (do_processing == TRUE)
    {
      api_status &= rfcommon_xpt_is_processing_done();
      if( api_status == FALSE)
      {
        FTM_MSG_1( FTM_ERROR, "ftm_common_trigger_ept_iq_cap: IQ processing already "
                   "in progress for tech %d", ftm_rf_tech );
      }
    }
  }

  /* Set capture and processing flags */
  if( (api_status == TRUE) )
  {
    api_status &= rfcommon_xpt_set_capture_in_progress();
    if (do_processing == TRUE)
    {
      api_status &= rfcommon_xpt_set_processing_in_progress();
    }
    /* Initialize buffer pool for xpt captures */
    rfcommon_core_xpt_init_buffers();

    /* Get buffer ids, TODO: need to pass in do_processing flag to not get dpd result buffer */
    api_status &= rfcommon_core_xpt_get_buffers(&buffer_ids);
  }

  proc_flag = (do_processing == TRUE)?PROC_SELF_TEST:PROC_NONE;

  /* This needs to be changed
     I still don't know how these flags ultimately impact the capture type
     *See rfcommon_mdsp.c:750 or so
  */
  // if (ftm_selftest_pwr_only && ((ftm_selftest_meas_type & ENABLE_EVM_MEAS) == 0))
  if ((ftm_selftest_meas_type & ENABLE_VSWR_MEAS) != 0)
  {
    ftm_selftest_xpt_capture_type = XPT_CAPTURE_SELF_TEST_VSWR;
  }
  else if ((ftm_selftest_meas_type & ENABLE_EVM_MEAS) != 0)
  {
    ftm_selftest_xpt_capture_type = XPT_CAPTURE_SELF_TEST_EVM;
  }
  else // ((ftm_selftest_meas_type & ENABLE_TX_PWR_MEAS) != 0)
  {
    ftm_selftest_xpt_capture_type = XPT_CAPTURE_SELF_TEST_RX_RMS;
  }

  /* Trigger tech-specific capture */
  switch ( ftm_rf_tech )
  {
  case FTM_RF_TECH_CDMA:
    #ifdef FEATURE_CDMA1X
    {
      /* Perform capture */
      if(api_status == TRUE)
      {
        api_status &= ftm_1x_xpt_iq_capture(device,
                                            device_fb,
                                            ftm_selftest_xpt_capture_type,
                                            num_samples,
                                            TRUE,                  /* first_trig */
                                            TRUE,                   /* last_trig */
                                            proc_flag,
                                            buffer_ids,
                                            &(samp_rate));

      }
    }
    #endif
    break;

  case FTM_RF_TECH_WCDMA:
    #ifdef FEATURE_WCDMA
    {
      if(api_status == TRUE)
      {
        api_status &= ftm_wcdma_xpt_iq_capture(device,
                                               device_fb,
                                               ftm_selftest_xpt_capture_type,
                                               num_samples,
                                               TRUE, /* first_trig */
                                               TRUE, /* last_trig */
                                               proc_flag,
                                               buffer_ids,
                                               &samp_rate);
      }
    }
    #endif
    break;

  case FTM_RF_TECH_TDSCDMA:
    #ifdef FEATURE_TDSCDMA
    {
      if(api_status == TRUE)
      {
        api_status &= ftm_tdscdma_xpt_iq_capture(device,
                                                 device_fb,
                                                 ftm_selftest_xpt_capture_type,
                                                 num_samples,
                                                 TRUE, /* first_trig */
                                                 TRUE, /* last_trig */
                                                 proc_flag,
                                                 buffer_ids,
                                                 &samp_rate);
      }
    }
    #endif
    break;

  case FTM_RF_TECH_LTE:
    #ifdef FEATURE_LTE
    {
      if(api_status == TRUE)
      {
        api_status &= ftm_lte_xpt_iq_capture(device,
                                             device_fb,
                                             num_samples,
                                             TRUE,
                                             TRUE,
                                             proc_flag,
                                             ftm_selftest_xpt_capture_type,
                                             buffer_ids.capt_buf_id,
                                             buffer_ids.res_buf_id,
                                             &samp_rate);
      }
    }
    #endif
    break;

  default:
    FTM_MSG_1( FTM_ERROR, "ftm_common_trigger_self_test_iq_cap: Mode %d not supported",
               ftm_rf_tech );
    api_status = FALSE;
    break;
  } /* Trigger tech-specific capture */

  if ( api_status == FALSE)
  {
    FTM_MSG( FTM_ERROR, "ftm_common_trigger_self_test_iq_cap: api status = FAIL" );
  }
  return api_status;
}


// Commented out for 9x35 bringup
#if 0
/*===========================================================================

FUNCTION ftm_common_hdet_disable

DESCRIPTION
   This function will call the tech specific HDET disable or enable API.
   Different tech uses different mechanism to disable or enable HDET.

DEPENDENCIES

RETURN VALUE
   FALSE, fail to disable or enable HDET
   TRUE, successfully disable or enable HDET

SIDE EFFECTS
   None.

===========================================================================*/
boolean ftm_common_hdet_disable(ftm_rf_technology_type ftm_rf_tech, rfm_device_enum_type rfm_dev, boolean flag)
{
   boolean ret=FALSE;
   uint8 cnt=0;

   switch (ftm_rf_tech)
   {
   case FTM_RF_TECH_CDMA:
    #ifdef FEATURE_CDMA1X
    while (FALSE == ret)
    {
     ret = rfm_cdma_ftm_obtain_hdet_lock(rfm_dev,flag);

     /* Will try to obtain hdet lock maximum of 12 times before bailing out */
     if ((TRUE == ret) || (cnt > 12))
     {
      break;
     }

     /* Wait 200us before trying again */
     DALSYS_BusyWait(200);
     cnt++;
    }

    FTM_MSG_HIGH("FTM CDMA HDET: ena/dis=%d result=%d  cnt(200us/cnt)=%d", flag, ret, cnt);
    #endif
    break;
   case FTM_RF_TECH_WCDMA:
    break;
   case FTM_RF_TECH_GSM:
    break;
   case FTM_RF_TECH_LTE:
    #ifdef FEATURE_LTE
    ret = ftm_lte_disable_hdet(flag);
    #endif
    break;
   case FTM_RF_TECH_TDSCDMA:
    #ifdef FEATURE_TDSCDMA
    ret = ftm_tdscdma_disable_hdet(flag);

    if (TRUE == ret)
    {
       /* Wait for 5.1 ms */
     DALSYS_BusyWait(5100);
    }

      FTM_MSG_HIGH("FTM TDSCDMA HDET: ena/dis=%d result=%d  cnt=%d", flag, ret, cnt);
    #endif

    break;
   default:
    cnt=0;  /* Avoid compiler warning if cnt is not being used. */
    break;
   }

   return(ret);
}
#endif

boolean ftm_common_fbrx_iq_capture_processing
(
ftm_rf_technology_type ftm_rf_tech,
uint8 fbrx_chain,
uint32 ftm_selftest_capture_type,
uint16 num_samples,
int32 **iq_buf_rx,
int32 **iq_buf_tx,
uint16 *evm_value_ptr,
uint32 *rx_rms_mag_ptr
)
{
  uint8 iq_buffer_index;
  uint8 result_buffer_index      = 0;
  rfm_device_enum_type tx_device = RFM_DEVICE_0;
  boolean capture_trigerred      = FALSE;
  boolean capture_done           = FALSE;
  boolean processing_done        = FALSE;
  uint32 config_delay_ms         = 2;
  boolean api_status             = TRUE;

  boolean do_processing = (ftm_selftest_capture_type > 0)? TRUE: FALSE;

  // #if defined(FEATURE_CDMA) || defined(FEATURE_TDSCDMA)
  // const rfc_common_logical_device_params_type *device_params_ptr = NULL;
  // #endif

  /* HDET should have been already disabled in calibration state */
  // if (rfm_get_calibration_state() == FALSE)
  // {
  //   /* Disable HDET loop */
  //   ftm_common_hdet_disable(ftm_rf_tech, tx_device, TRUE);

  //   /* Wait for HDET to disable.  Temporary workaround for LTE HDET disable issue */
  //   ftm_clk_rex_wait(3);
  // }

  /* Allocate DPD sample capture buffer */
  rfcommon_mc_allocate_dpd_buffer();

  /* Trigger tech-specific capture */
  switch ( ftm_rf_tech )
  {
  case FTM_RF_TECH_CDMA:
  #ifdef FEATURE_CDMA1X
  {
    fbrx_chain = RFM_DEVICE_1;
    ftm_1x_xpt_cal_config((rfm_device_enum_type)tx_device,fbrx_chain);

    // if (ftm_common_get_selftest_flag()) {

    //   /* if in self-test mode, override the default WTR and coupler gain */

    //   device_params_ptr = rfc_common_get_logical_device_params(RFM_DEVICE_0);

    //   if(device_params_ptr != NULL){

    //     ftm_common_set_coupler_attn(ftm_common_get_selftest_coupler_fb_attn_state());

    //     rfdevice_cmn_set_et_path(device_params_ptr->cmn_device,RFM_1X_MODE,ftm_common_get_selftest_wtr_fb_attn_state(),TRUE);
    //   }
    //  }
    }
  #endif
    break;

  case FTM_RF_TECH_WCDMA:
    #ifdef FEATURE_WCDMA
    {
      fbrx_chain = RFM_DEVICE_1;
      ftm_wcdma_xpt_cal_config((rfm_device_enum_type)tx_device,fbrx_chain, (uint8)FTM_XPT_CONFIG_EPT, TRUE); /* True for config, False for deconfig */

      // if (ftm_common_get_selftest_flag()) {

      //     ftm_common_set_coupler_attn(ftm_common_get_selftest_coupler_fb_attn_state());

      //     ftm_wcdma_feedback_setup(RFM_DEVICE_0,
      //                             RFM_DEVICE_1,
      //                             FEEDBACK_SETUP_ACTION_MASK_CONFIGURE_WTR,
      //                             ftm_common_get_selftest_wtr_fb_attn_state(),
      //                             TRUE,
      //                             FALSE,
      //                             TRUE);
      // }
    }
    #endif
    break;

  case FTM_RF_TECH_TDSCDMA:
    #ifdef FEATURE_TDSCDMA
    {
      fbrx_chain = RFM_DEVICE_1;
      ftm_tdscdma_xpt_cal_config((rfm_device_enum_type)tx_device, fbrx_chain,(uint8)FTM_XPT_CONFIG_EPT, TRUE); /* True for config, False for deconfig */


      // if (ftm_common_get_selftest_flag()) {
      //   device_params_ptr = rfc_common_get_logical_device_params(RFM_DEVICE_0);

      //   if(device_params_ptr != NULL){


      //     ftm_common_set_coupler_direction(tdscdma_coupler_direction);


      //     ftm_common_set_coupler_attn(ftm_common_get_selftest_coupler_fb_attn_state());

      //     rfdevice_cmn_set_et_path(device_params_ptr->cmn_device,RFM_TDSCDMA_MODE,ftm_common_get_selftest_wtr_fb_attn_state(),TRUE);
      //   }
      // }
    }
    #endif
    break;

  case FTM_RF_TECH_LTE:
  #ifdef FEATURE_LTE
  {
    tx_device = rflte_ftm_mc_sm_get_tx_device();
    ftm_lte_xpt_cal_configure((rfm_device_enum_type)tx_device,fbrx_chain, (uint8)FTM_XPT_CONFIG_ET);

     //rflte_ftm_mc_sm_set_use_fb_path(TRUE);


    // if (ftm_common_get_selftest_flag()) {

    //   /* if in self-test mode, override the default WTR and coupler gain */

    //   ftm_common_set_coupler_attn(ftm_common_get_selftest_coupler_fb_attn_state());

    //   rflte_ftm_mc_set_et_path(ftm_common_get_selftest_wtr_fb_attn_state(), TRUE);

    // }

  }
  #endif
    break;

  default:
    FTM_MSG_1( FTM_ERROR, "ftm_common_fbrx_iq_capture_processing: Mode %d not supported",
               ftm_rf_tech );
    api_status = FALSE;
    break;
  }

  ftm_clk_rex_wait(config_delay_ms);

  FTM_MSG_1(FTM_HIGH, "xpt_cal_configure  done", 0);

  if (api_status == TRUE)
  {
    /* trigger capture*/
    capture_trigerred = ftm_common_trigger_self_test_iq_cap(ftm_rf_tech,
                                                          tx_device,
                                                          fbrx_chain,
                                                          num_samples,
                                                          ftm_selftest_capture_type);;
  }

  /* Wait for capture done */
  if((capture_trigerred == TRUE) )
  {
    capture_done = rfcommon_xpt_wait_for_capture_done( 500, 5000, &iq_buffer_index );
    if( capture_done == FALSE )
    {
      FTM_MSG_1( FTM_ERROR, "ftm_lte_fbrx_iq_capture_trigger_handler: Sample "
               "capture did not complete", 0);
      return FALSE;
    }
  }
  else
  {
    FTM_MSG_1( FTM_ERROR, "ftm_lte_fbrx_iq_capture_trigger_handler: Sample "
              "capture trigger failed", 0);
    return FALSE;
  }

  /* fetch the collected samples*/
  rfcommon_mc_fetch_selftest_iq_data(iq_buffer_index, num_samples, iq_buf_rx, iq_buf_tx);

  if(do_processing)
  {
    /* Wait for processing done */
    processing_done = rfcommon_xpt_wait_for_processing_done( 1000, 35000, &result_buffer_index );

    if ( processing_done == FALSE)
    {
      FTM_MSG_1( FTM_ERROR, "ftm_common_trigger_ept_iq_cap: "
                   "Processing did not complete for tech %d", ftm_rf_tech );
      return FALSE;
    }
    rfcommon_mc_fetch_selftest_meas(result_buffer_index, evm_value_ptr, rx_rms_mag_ptr);
  }

  /* release capture buffer, returns false now, TODO: implement deallocation of buffer_id and return true */
  rfcommon_core_xpt_release_capture_buffer(iq_buffer_index);

  /* release results buffer, returns false now, TODO: implement deallocation of buffer_id and return true */
  rfcommon_core_xpt_release_results_buffer(result_buffer_index);

  // if (ftm_common_get_selftest_flag()) {

  //    if in self-test mode, override the default WTR and coupler gain

  //   ftm_common_set_coupler_attn(COUPLER_ATTN_NORMAL);

  // }

  /* release all buffer ids */
  rfcommon_core_xpt_free_all_buffers();

  /* HDET should have been already disabled in calibration state */
  // if (rfm_get_calibration_state() == FALSE)
  // {
  //   /* Release HDET loop */
  //   ftm_common_hdet_disable(ftm_rf_tech, tx_device, FALSE);
  // }

  return TRUE;
}

void ftm_common_selftest_set_flag( uint8 * flag_value)
{
  if (*flag_value != ftm_common_selftest_modeset_flag)
  {
    FTM_MSG_2(FTM_HIGH, "ftm_common_selftest_set_flag: current value %d set to %d",ftm_common_selftest_modeset_flag,*flag_value);
    ftm_common_selftest_modeset_flag = *flag_value;
  }
  else
  {
    FTM_MSG_1(FTM_HIGH, "ftm_common_selftest_set_flag: No Change to current value %d",ftm_common_selftest_modeset_flag);
  }
}


uint8 ftm_common_get_selftest_capture_flag(void)
{
  return ftm_common_selftest_modeset_flag;
}
#endif /* FEATURE_FACTORY_TESTMODE */
