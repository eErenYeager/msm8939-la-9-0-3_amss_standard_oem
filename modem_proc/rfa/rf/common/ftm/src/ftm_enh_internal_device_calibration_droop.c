/*!
  @file
  ftm_enh_internal_device_calibration_droop.c

  @brief

*/
/*==============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfa/rf/common/ftm/src/ftm_enh_internal_device_calibration_droop.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/29/14   brath   Updates to provide STG frequency for device cal APIs
04/21/14   jmf     Updates to Enable All Tech-BW combinations to go through
04/14/14   jmf     Updates to Coeff Generating Matrix
04/08/14   jmf     Updates to accommodate error code returns / Droop Filter Processing
03/14/14   jmf     Updates to account for NV structure changes
03/07/14    aa     Latest updates for droop cal
02/06/14   jmf     STG-based Droop Cal capture sequence implementation, no processing yet
02/05/14    aa     Updated params for droop cal data
01/21/14   jmf     Add support to update global NV structure for Droop Filter
12/15/13   jmf     Initial Revision

==============================================================================*/
#ifndef FEATURE_GNSS_ONLY_NO_WWAN

#include "rfa_variation.h"
#include "comdef.h"
#include "ftm_common_timer.h"
#include "ftm_common_control.h"
#include "ftm_common_data.h"
#include "rfcommon_math.h"
#include "modem_mem.h"
#include "string.h"
#include "stringl.h" /* for memscpy */
#include "ftm_msg.h"
#include "ftm_common_enh_internal_device_cal.h"
//#include "rfm_internal.h"
//#include "DALSys.h" 
#include "rfcommon_time_profile.h" 
//#include "zlib.h"
#include "ftm_enh_internal_device_cal_droop.h"
#include "rfdevice_cmn_intf.h"
#include "rfcommon_core_xpt_buffer.h"
#include "rflm_api_fbrx.h"
#include "math.h"

#ifdef FEATURE_LTE
#include "rflte_util.h"
#include "rflte_mc.h"
#include "rflte_ftm_mc.h"
#endif

#ifdef FEATURE_WCDMA
#include "rfwcdma_mc.h"
#endif /* FEATURE_WCDMA */

#ifdef FEATURE_CDMA1X
#include "rf_cdma_data.h"
#include "rf_cdma_utils_freq.h"
#endif

#ifdef FEATURE_TDSCDMA
#include "rf_tdscdma_core_util.h"
#include "rf_tdscdma_mc.h"
#endif

#ifdef FEATURE_WCDMA
extern rfwcdma_mc_state_type rfwcdma_mc_state;
#endif

#ifdef FEATURE_TDSCDMA
extern rf_tdscdma_mc_state_type rf_tdscdma_mc_state;
#endif

extern uint8 rfcommon_fbrx_msm_convert_tx_cfg_to_mode( rfcommon_fbrx_tx_cfg_type cfg);

/*******************************************************/
/*       HARD-CODED PARAMS FOR PROCESSING ALG          */
/*    Ref:  FBRxDroopEqualization_V2_20140403.m        */
/*                                                     */
/*******************************************************/
#define KERNEL_FRAC_RES 14
#define PI 3.14159265358979323846
#define EQ_LEN 10

/*******************************************************/
/*       MAGIC  MATRICES FOR COEFF CALCULATION         */
/*    Ref:  FBRxDroopEqualization_V2_20140403.m        */
/*                                                     */
/*******************************************************/

// With assumption that captures are done at freq0 = 0.5M, freq1 = 2M, freq2 = 4.5M
// And WTR poles are located at [30,30] MHz
/* // First Version
static int16 M_LTE5[3][EQ_LEN] = { {1433,  3145,   -14077,  571,   27422,   28896,   1990,  -17330,  -3188,   3907 }, 
    {784,      -10904,       16821,       14378,      -11665,      -22762,       -4353,       15775,        7158,       -5233 },
    {-1002  ,      4101,       -2018  ,     -4389  ,      -367,        3971 ,       2926,       -1623,       -2967,        1366 }
};
uint8 fracBits_LTE5  = 15;*/

static int16 M_LTE5[3][EQ_LEN] = { {   179,   393, -1760,    71,  3428,  3612,   249, -2166,  -399,   488}, 
                                   {    98, -1363,  2103,  1798, -1458, -2845,  -544,  1972,   895,  -654},
                                   {  -125,   513,  -252,  -549,   -46,   496,   365,  -203,  -370,  171 }
                                 };
uint8 fracBits_LTE5  = 12;

// With assumption that captures are done at freq0 = 0.5M, freq1 = 2M, freq2 = 4.5M
// And WTR poles are located at [30,30] MHz
/* // First Version
static int16 M_LTE10[3][EQ_LEN] = {
    {87,         220,       -2270,        7463,      -12425,        6976,       14081,      -13572,        4283,        -748},
    {46,        -987,        4558,      -12022,       16923,       -2323,      -18836,       16897,       -5091,         837},
    {-101,         615,       -1911,        3834,       -3425,       -1601,        5377,       -3571,         898,        -117}
        };
uint8 fracBits_LTE10 = 12; */

static int16 M_LTE10[3][EQ_LEN] = { {    87,   220, -2270,  7463,-12425,  6976, 14081,-13572,  4283,  -748 },
                                    {    46,  -987,  4558,-12022, 16923, -2323,-18836, 16897, -5091,   837 },
                                    {  -101,   615, -1911,  3834, -3425, -1601,  5377, -3571,   898,  -117 }
                                  };
uint8 fracBits_LTE10 = 12;

// With assumption that captures are done at freq0 = 0.5M, freq1 = 4M, freq2 = 9M
// And WTR poles are located at [30,30] MHz
/* // First Version
static int16 M_LTE15[3][EQ_LEN] = {
    {2224,       -2330,      -10679,       19899,       22853,       -8558,      -14051,        8070,          20,       -1064},
    {-1505,       -2968,       20846,       -9214,      -22807,        6189,       16447,       -6711,       -2058,        1779},
    {-381 ,       3224   ,    -4989  ,     -1057  ,      4742    ,    1088  ,     -3321  ,      -126     ,   1420   ,     -597}
        };
uint8 fracBits_LTE15 = 14; */

static int16 M_LTE15[3][EQ_LEN] = { {   556,  -582, -2670,  4975,  5713, -2139, -3513,  2018,     5,  -266 },
                                    {  -376,  -742,  5212, -2303, -5702,  1547,  4112, -1678,  -514,   445 },
                                    {   -95,   806, -1247,  -265,  1186,   272,  -830,   -32,   355,  -149 }};
uint8 fracBits_LTE15  = 12;

// With assumption that captures are done at freq0 = 0.5M, freq1 = 4M, freq2 = 9M
// And WTR poles are located at [30,30] MHz
/* // First Version
static int16 M_LTE20[3][EQ_LEN] = {
    {451     ,  -1034    ,    -350  ,      7217   ,   -19182  ,     18915   ,    17137  ,    -20970  ,      7420   ,    -1411},
    {-276    ,     -21   ,    3600  ,    -13950   ,    26615  ,    -11381   ,   -23326  ,     25924  ,     -8764   ,     1579},
    {-116    ,     772   ,    -2545 ,       5344  ,     -5292 ,      -1517  ,      7529 ,      -5474 ,       1518  ,      -221}
        };
uint8 fracBits_LTE20 = 13; */

static int16 M_LTE20[3][EQ_LEN] = { {   225,  -517,  -175,  3608, -9591,  9457,  8569,-10485,  3710,  -706 },
                                    {  -138,   -11,  1800, -6975, 13307, -5691,-11663, 12962, -4382,   789 },
                                    {   -58,   386, -1272,  2672, -2646,  -758,  3764, -2737,   759,  -110 }
                                  };
uint8 fracBits_LTE20  = 12;

uint8 num_droop_filters = -1;


/*******************************************************/
/*       END MAGIC MATRICES FOR COEFF CALCULATION      */
/*******************************************************/

/*******************************************************/
/*       CONTAINERS FOR FILTER COEFFICIENTS            */
/*******************************************************/
static int32 droop_filter_coeffs[MAX_NUM_DROOP_FILTERS_GEN_PER_SWP][EQ_LEN] = {{0}, {0}, {0}, {0}};
static uint8 droop_filter_grp_delay[MAX_NUM_DROOP_FILTERS_GEN_PER_SWP] = {0};
static uint8 droop_filter_tech_bw[MAX_NUM_DROOP_FILTERS_GEN_PER_SWP] = {RFCOMMON_FBRX_NUM_TX_CONFIG_MAX};

static int16 sat_int32_to_int16(int32 in, uint8 * overflow_det);
static int32 sat_add_int32(int32 addend1, int32 addend2, uint8 * overflow_det);
static int16 sat_add_int16(int16 addend1, int16 addend2, uint8 * overflow_det);


#ifdef FEATURE_FACTORY_TESTMODE

boolean ftm_enh_internal_device_cal_fb_droop_config_fbrx(void)
/* put this in a common place */
{
  boolean ret_val = TRUE;
  // set up per tech spec
  return ret_val;
}

boolean ftm_enh_internal_device_cal_fb_droop_config_stg(void)
/* put this in a common place */
{
  boolean ret_val = TRUE;
  // set up per tech spec ? 
  return ret_val;
}


boolean ftm_enh_internal_device_cal_fb_droop_calc_coeffs(void)
{
  boolean ret_val = TRUE;
  // calc per tech spec ? 
  return ret_val;
}

boolean ftm_enh_internal_device_cal_fb_droop_update_global_nv_struct(void)
{
  boolean ret_val = TRUE;
  // update per tech/band spec
  return ret_val;
}

boolean ftm_enh_internal_device_cal_fb_droop_get_global_nv_struct(void)
{
  boolean ret_val = TRUE;
  // get per tech/band spec
  return ret_val;
}


static int16 sat_int32_to_int16(int32 in_val, uint8 * overflow_det)
{
  int16 out_val;
  int16 max_val = 0x7FFF;
  int16 min_val = 0x8000;
  *overflow_det = 0;
  if (in_val > (int32)max_val)
  {
    out_val = max_val;
    *overflow_det = 1;
    FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d > %d", in_val, max_val);
  }
  if (in_val < (int32)min_val)
  {
    out_val = min_val;
    *overflow_det = 1;
    FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d < %d", in_val, min_val);
  }

  if (*overflow_det == 0)
  {
    out_val = (int16)( in_val  & 0xFFFF );
  }
  return out_val;

}

static int32 sat_add_int32(int32 addend1, int32 addend2, uint8 * overflow_det)
{
  int32 out_val;
  int32 max_val = 0x7FFFFFFF;
  int32 min_val = 0x80000000;
  *overflow_det = 0;

  if (addend1 >=0)
  {
    if (max_val - addend1 < addend2)
    {
      out_val = max_val;
      *overflow_det = 1;
      FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d < %d", max_val - addend1, addend2);
    }
  }
  else
  {
    if ( addend2 < min_val - addend1 )
    {
      out_val = min_val;
      *overflow_det = 1;
      FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d < %d", addend2, min_val - addend1);
    }
  }
  if (*overflow_det == 0)
  {
    out_val = addend1 + addend2;
  }

  return out_val;
}

static int16 sat_add_int16(int16 addend1, int16 addend2, uint8 * overflow_det)
{
  int16 out_val;
  int16 max_val = 0x7FFF;
  int16 min_val = 0x8000;
  *overflow_det = 0;

  if (addend1 >=0)
  {
    if (max_val - addend1 < addend2)
    {
      out_val = max_val;
      *overflow_det = 1;
      FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d < %d", max_val - addend1, addend2);
    }
  }
  else
  {
    if ( addend2 < min_val - addend1 )
    {
      out_val = min_val;
      *overflow_det = 1;
      FTM_MSG_2(FTM_ERROR, "OVERFLOW DETECTED: %d < %d", addend2, min_val - addend1);
    }
  }
  if (*overflow_det == 0)
  {
    out_val = addend1 + addend2;
  }

  return out_val;
}



ftm_enh_internal_device_cal_error_code_type ftm_enh_internal_device_cal_fb_droop_stg_sequence(uint8 tx_device,
                                                          uint8 num_captures,
                                                          int32 *stg_offset_khz,
                                                          uint16 num_iq_samples,
                                                          uint8 debug_mode_en,
                                                          uint8 do_processing
                                                          )
{
  ftm_enh_internal_device_cal_error_code_type ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS;
  boolean api_status = TRUE;
  rfdevice_rxtx_common_class *device_ptr = NULL;
  uint64 stg_freq = 0;
  ftm_rf_technology_type ftm_rf_tech  = FTM_RF_TECH_UNKNOWN;
  int32 current_stg_freq_offset_khz = 0;
  uint32 tx_freq = 0;
  ftm_cal_xpt_instr_payload_iq_capture_type iq_cap_instr;
  char filename[20] = {'\0'};
  uint8 samp_buf_id[MAX_FREQ_OFFSETS_STG_IQ_CAP] = {-1};
  uint8 iter = 0;

  #ifdef FEATURE_CDMA
  const rf_cdma_data_status_type *dev_status_r;
  rfm_cdma_band_class_type band;
  #endif

  /* Get Current FTM RF Tech in order to get current Tx Freq 
     and also to dispatch tech-specific IQ capture command */
  ftm_rf_tech = ftm_get_rf_technology();

  if (num_captures > MAX_FREQ_OFFSETS_STG_IQ_CAP)
  {
    FTM_MSG_2(FTM_ERROR, "Number of Requested IQ Captures %d exceeds allowed %d", num_captures, MAX_FREQ_OFFSETS_STG_IQ_CAP);
    return FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_CAPTURE_NUM;
  }

  device_ptr = rfc_common_get_cmn_device_object(0);
  /* check for null pointer and throw error */
  if (device_ptr == NULL)
  {
    FTM_MSG(FTM_ERROR, "rfc_common_get_cmn_device_object(0) returns NULL");
    return FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
  }

  /****************************************************************/
  /* Get STG Frequency to tune to from Tx channel information     */
  /****************************************************************/

  switch ( ftm_rf_tech )
  {
  #if defined(FEATURE_CDMA1X) && defined(FEATURE_CDMA) 
  case FTM_RF_TECH_CDMA:
    dev_status_r = rf_cdma_get_device_status( RFM_DEVICE_0 ); // what is the current tx device?
    if( dev_status_r != NULL)
    {
      band = rf_cdma_sys_band_type_to_rf_band_type( dev_status_r->curr_band);
      tx_freq = rf_cdma_get_tx_carrier_freq( band, (int)
                                             dev_status_r->lo_tuned_to_chan );
    }
    else
    {
      FTM_MSG(FTM_ERROR, "rfc_common_get_cmn_device_object(0) returns NULL");
      return FTM_ENH_INTERNAL_DEVICE_CAL_GENERAL_FAILURE;
    }
    break;
  #endif

  #ifdef FEATURE_WCDMA
  case FTM_RF_TECH_WCDMA:
    tx_freq = rfwcdma_core_get_freq_from_ul_arfcn(rfwcdma_mc_state.car_path_state[RF_PATH_0][0].curr_chan_tx);
    FTM_MSG_1(FTM_HIGH, "ftm_enh_internal_device_cal_fb_droop_stg_sequence: curr_tx_chan: %d", tx_freq);
    break;
  #endif /* FEATURE_WCDMA */
  #ifdef FEATURE_LTE
  case FTM_RF_TECH_LTE:
    tx_freq = rflte_core_get_tx_freq_from_uarfcn(
                                   rflte_ftm_mc_sm_get_tx_chan(),
                                   rflte_ftm_mc_sm_get_band());
    break;
  #endif
  #ifdef FEATURE_TDSCDMA
  case FTM_RF_TECH_TDSCDMA:
    tx_freq = rf_tdscdma_core_util_get_freq_from_chan(rf_tdscdma_mc_state.curr_chan_tx);
    break;
  #endif
  default:
    FTM_MSG(FTM_ERROR, "Unsupported Tech Type: Return error handle");
    return FALSE;
    /* Flag Error as Unsupported Tech */
  }

  
  // check if there is going to be too many captures to save on the EFS
  iq_cap_instr.primary_device = RFM_DEVICE_0; /* What is the tx device, doesn't matter */
  iq_cap_instr.feedback_device = RFM_DEVICE_0; /* What is the fbrx device */
  iq_cap_instr.num_samples = num_iq_samples;
  iq_cap_instr.proc_flag = PROC_NONE;
  iq_cap_instr.capture_type = XPT_CAPTURE_FBRX;

  /* Reset Capture Buffers */
  rfcommon_core_xpt_init_buffers(); // must be done before IQ capture code is called - Mark in Documentation for IQ Capture

  for (iter = 0; iter<num_captures; iter++)
  {
    iq_cap_instr.first_trigger = 0;
    iq_cap_instr.last_trigger = 0;
    if (iter == 0)
    {
      iq_cap_instr.first_trigger = 1; 
    }
    if (iter == (num_captures-1))
    {
      iq_cap_instr.last_trigger = 1;
    }

    if (debug_mode_en!=3)
    {
      snprintf(&filename[0], 20,"iq_%05d_khz.bin", (int)stg_offset_khz[iter] );
    }
    else
    {
      snprintf(&filename[0], 20, "iq_mod.bin" );
    }

    stg_freq = (tx_freq + stg_offset_khz[iter])*1000;
    /* TODO: check to see if frequency to configure STG at is good
       And Return error as unsupported Freq */
    FTM_MSG_4(FTM_HIGH, "ftm_enh_internal_device_cal_fb_droop_stg_sequence: current tech %d, current tx freq: %d, current_stg_freq_offset_khz: %d, stg_freq: %d",ftm_rf_tech, tx_freq, current_stg_freq_offset_khz,stg_freq);
    /****************************************************************/
    /*                  Set STG Frequency and Enable                */
    /*                   Also connects STG to FBRx                  */
    /****************************************************************/
    if ( debug_mode_en != 3 )
    {
      api_status = rfdevice_cmn_enable_stg(
                                        device_ptr,
                                        stg_freq, 
                                        RFDEVICE_EXECUTE_IMMEDIATE,
                                        NULL
                                       );
      if (api_status == FALSE)
      {
        FTM_MSG(FTM_ERROR, "rfdevice_cmn_enable_stg returned FALSE ");
        rfcommon_core_xpt_free_all_buffers();  
        return FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
      }
    }
    /* Allow settling time for STG?? */

    /****************************************************************/
    /*              Setup and Trigger XPT IQ Capture                */
    /****************************************************************/

    api_status = ftm_enh_internal_device_cal_fbrx_iq_capture(ftm_rf_tech, &iq_cap_instr, &samp_buf_id[iter], debug_mode_en, &filename[0]);

    if (ret_val != FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS)
    {
      FTM_MSG_1(FTM_ERROR, "ftm_enh_internal_device_cal_fbrx_iq_capture failed for tech %d", (int)ftm_rf_tech);
      if (debug_mode_en != 3 )
      {
        api_status = rfdevice_cmn_disable_stg(
                                        device_ptr,
                                        RFDEVICE_EXECUTE_IMMEDIATE,
                                        NULL,
                                        stg_freq
                                       );
      }
      rfcommon_core_xpt_free_all_buffers();
      return ret_val;
    }

    FTM_MSG_1(FTM_HIGH, "Current Capture Buffer is %d", samp_buf_id[iter]);

    /****************************************************************/
    /*                     Disable FBRx STG                         */
    /****************************************************************/

    if (debug_mode_en !=3)
    {
      api_status = rfdevice_cmn_disable_stg(
                                      device_ptr,
                                      RFDEVICE_EXECUTE_IMMEDIATE,
                                      NULL,
                                      stg_freq
                                     );
      if (api_status == FALSE)
      {
        FTM_MSG_1(FTM_ERROR, "Something went wrong on loop #%d, breaking loop, check FTM Msgs", iter );
        rfcommon_core_xpt_free_all_buffers(); 
        return FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
      }
    }
  
  }

  // perform calculation of filter coeffs using all the capture buffers
  // set error flag whether calculation was successful

  if (do_processing == 1)
  {
    ret_val = ftm_enh_internal_device_cal_droop_stg_process(FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_STG_REAL_COEFFS, 
                                                            num_captures, &stg_offset_khz[0], &samp_buf_id[0],
                                                            num_iq_samples);
  }


  rfcommon_core_xpt_free_all_buffers();  // must be done after last IQ capture code is called - Mark in Documentation for IQ Capture

  return ret_val;
}


ftm_enh_internal_device_cal_error_code_type ftm_enh_internal_device_cal_fb_droop_mod_sequence(void)
{
  ftm_enh_internal_device_cal_error_code_type ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
//  rfdevice_rxtx_common_class *device_ptr = NULL;
//  device_ptr = rfc_common_get_cmn_device_object(0);

  // if first_trig
  //    perform first time configs

  // perform capture
  // if debug flag enabled
  //    save to EFS with certain name
  // perform calc
  // update global nv struct

  // if last_trig
  //    perform last time deconfigs
  FTM_MSG(FTM_ERROR, "ftm_enh_internal_device_cal_fb_droop_mod_sequence(): not supported yet!");

  return ret_val;

}

boolean ftm_enh_internal_device_cal_fb_droop_iq_capture(void)
{
  boolean ret_val = TRUE;


  return ret_val;
}


/*----------------------------------------------------------------------------*/
/*!
  @name: ftm_enh_internal_cal_fb_droop

  @brief: feedback droop calbration
  
 
  @details
  
  @param
  input: req_instr, payload instruction
  output: res_len:  cal result length
             fb_droop_result: output of feedback droop cal NV
  
  @return
     success or fail
*/

boolean ftm_enh_internal_device_cal_fb_droop
(
  void const *req_instr,
  uint16 * res_len,
  void *fb_droop_result 
)
{
   ftm_enh_internal_device_cal_error_code_type ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS;
   boolean api_status = TRUE;
   uint8 iter = 0;
   uint8 iter2 = 0;
   uint8 num_captures   = 0;   
   // number of IQ captures to perform; can be total STG freq offsets to capture or any number of captures to perform
   uint8 fbrx_gain_state = 0;
   uint8 tx_device = 0;
   uint8 fbrx_bw = RFCOMMON_FBRX_LTE_10;
   int32 stg_offset_khz[MAX_FREQ_OFFSETS_STG_IQ_CAP] = {0};
   uint16 num_iq_samples = 0;
   uint8 debug_mode_en = 0;
   rfdevice_rxtx_common_class* device_ptr = NULL;
   uint8 do_processing = 1;

   rfcommon_fbrx_nv_droop_type *droop_data_nv = NULL;
   rfcommon_fbrx_error_type nv_read_write_error;
   ftm_enh_internal_device_cal_data_type *internal_device_cal_data;

   ftm_rf_technology_type fbrx_tech = FTM_RF_TECH_UNKNOWN;
   ftm_rf_technology_type curr_tx_ftm_rf_tech = FTM_RF_TECH_UNKNOWN;

   ftm_enh_internal_device_cal_instr_payload_fb_droop_type * fb_droop_instr = (ftm_enh_internal_device_cal_instr_payload_fb_droop_type *)req_instr;
   ftm_enh_internal_device_cal_fb_droop_result_type * droop_result = (ftm_enh_internal_device_cal_fb_droop_result_type *) fb_droop_result;

   ftm_enh_internal_device_cal_fbrx_droop_cal_method_type cal_method = fb_droop_instr->cal_method;
   tx_device = fb_droop_instr->tx_device; // tx device set up independently, TODO: keep track of the valid tx device
   fbrx_gain_state = fb_droop_instr->fbrx_gain_state;
   num_captures = fb_droop_instr->num_captures;
   num_iq_samples = fb_droop_instr->num_iq_samples;

   memscpy(&stg_offset_khz[0], sizeof(int32)*MAX_FREQ_OFFSETS_STG_IQ_CAP, &(fb_droop_instr->stg_offset_khz[0]), sizeof(int32)*MAX_FREQ_OFFSETS_STG_IQ_CAP);

   internal_device_cal_data = ftm_common_data_get_enh_internal_device_cal_ptr();

   debug_mode_en = internal_device_cal_data->internal_device_cal_config.dbg_mode_en;
   fbrx_tech = (ftm_rf_technology_type)internal_device_cal_data->internal_device_cal_config.tech;

   fbrx_bw = internal_device_cal_data->internal_device_cal_config.bw;

   if (fbrx_bw >= RFCOMMON_FBRX_NUM_TX_CONFIG_MAX )
   {
     FTM_MSG_1(FTM_ERROR, "Unknown FBRx Tech/BW combination %d", fbrx_bw);
     internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_INVALID_TECHBW;
     return FALSE;
   }
   // For LTE5, need to perform LTE10 captures, processing will be different
   // For LTE15,need to perform LTE20 captures, processing will be different
   // For LTE1p4/3, need to perform LTE10 captures, use processing results from LTE5
   switch (fbrx_bw) 
   {
   case RFCOMMON_FBRX_LTE_15:
     fbrx_bw = RFCOMMON_FBRX_LTE_20;
     break;
   case RFCOMMON_FBRX_LTE_1P4:
   case RFCOMMON_FBRX_LTE_3:
   case RFCOMMON_FBRX_LTE_5:
     fbrx_bw = RFCOMMON_FBRX_LTE_10;
     break;
   default:
     break;
   }

   do_processing = fb_droop_instr->do_processing;
   *res_len = sizeof( ftm_enh_internal_device_cal_fb_droop_result_type);

   /*allocate memory*/
   if (droop_data_nv == NULL)
   {
     droop_data_nv = modem_mem_calloc(1, sizeof(rfcommon_fbrx_nv_droop_type), MODEM_MEM_CLIENT_RFA);
     if (droop_data_nv == NULL)
     {
       FTM_MSG(FTM_ERROR, " ftm_enh_internal_device_cal_fb_droop:  Failed to allocate memory for droop_data_nv !");
       internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_MEM_ALLOC_FAILURE;
       return FALSE;
     }
   }

   // make first filter tap 1 and the rest 0 by default in case rest of this flow falls apart
   memset((void*)&droop_filter_coeffs[0][0], 0, sizeof(int32)*EQ_LEN*MAX_NUM_DROOP_FILTERS_GEN_PER_SWP);
   for (iter = 0; iter < MAX_NUM_DROOP_FILTERS_GEN_PER_SWP; iter++)
   {
     droop_filter_tech_bw[iter] = RFCOMMON_FBRX_NUM_TX_CONFIG_MAX;
     droop_filter_grp_delay[iter] = 0;
     droop_filter_coeffs[iter][0] = 1 << 14; // Make default filter a unit impulse function with proper DC gain
   }

   curr_tx_ftm_rf_tech = ftm_get_rf_technology();
   if (fbrx_tech != curr_tx_ftm_rf_tech)
   {
     FTM_MSG_2(FTM_ERROR, "Current RF Tech %d does not match Tech for FBRx Cal %d", curr_tx_ftm_rf_tech, fbrx_tech );
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_TECH_MISMATCH;
     return FALSE;
   }

   FTM_MSG_1(FTM_HIGH, "Entering FB_DROOP_CAL, Cmd: %s", fb_droop_instr);
   if (debug_mode_en!=0)
   {
     FTM_MSG_5(FTM_MED, "Droop Cal Parameters -- calMethod: %d, txDevice: %d, fbrxGainState: %d, numCaptures: %d, numIQSamples: %d ", 
              cal_method, tx_device, fbrx_gain_state, num_captures, num_iq_samples ) ;

     FTM_MSG_4(FTM_MED, "Droop Cal Parameters -- Req FBRx Tech: %d, Current Tx Tech: %d, FBRx Tech/BW: %d, debugModeEn: %d ", 
              fbrx_tech, curr_tx_ftm_rf_tech, fbrx_bw, debug_mode_en ) ;

     for (iter = 0; iter<num_captures; iter++)
     {
       FTM_MSG_2(FTM_MED, "          stg_freq[%2d]  :   %5d kHz", iter, stg_offset_khz[iter] );
     }
   }

   device_ptr = rfc_common_get_cmn_device_object ( 0 );
   /* check for null pointer and throw error */
   if (device_ptr == NULL) 
   {
     FTM_MSG(FTM_ERROR, "rfc_common_get_cmn_device_object(0) returns NULL");
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
     return FALSE;
   }

   api_status = rfcommon_fbrx_mc_enable_rf_dev(device_ptr, fbrx_gain_state);
   if (api_status == FALSE)
   {
     FTM_MSG_1(FTM_ERROR, "Failed to Enable FBRx Device in Gain State %d " , fbrx_gain_state);
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
     return FALSE;
   }

   if (debug_mode_en != 0)
   {
     FTM_MSG(FTM_MED, "---------- M_LTE5 -----------" );
     for (iter = 0; iter < 3; iter++)
     {
       FTM_MSG_1(FTM_MED, "--------  Row     %d   ---------", iter);
       for (iter2 = 0; iter2 < EQ_LEN; iter2++)
       {
         FTM_MSG_1(FTM_MED, "    %10d ", M_LTE5[iter][iter2]);
       }
     }

     FTM_MSG(FTM_MED, "---------- M_LTE10 -----------" );
     for (iter = 0; iter < 3; iter++)
     {
       FTM_MSG_1(FTM_MED, "--------  Row     %d   ---------", iter);
       for (iter2 = 0; iter2 < EQ_LEN; iter2++)
       {
         FTM_MSG_1(FTM_MED, "    %10d ", M_LTE10[iter][iter2]);
       }
     }


     FTM_MSG(FTM_MED, "---------- M_LTE15 -----------" );
     for (iter = 0; iter < 3; iter++)
     {
       FTM_MSG_1(FTM_MED, "--------  Row     %d   ---------", iter);
       for (iter2 = 0; iter2 < EQ_LEN; iter2++)
       {
         FTM_MSG_1(FTM_MED, "    %10d ", M_LTE15[iter][iter2]);
       }
     }


     FTM_MSG(FTM_MED, "---------- M_LTE20 -----------" );
     for (iter = 0; iter < 3; iter++)
     {
       FTM_MSG_1(FTM_MED, "--------  Row     %d   ---------", iter);
       for (iter2 = 0; iter2 < EQ_LEN; iter2++)
       {
         FTM_MSG_1(FTM_MED, "    %10d ", M_LTE20[iter][iter2]);
       }
     }

   }

   ret_val = ftm_enh_internal_device_cal_fbrx_iq_cap_config(fbrx_tech, fbrx_bw);
   if (ret_val != FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS)
   {
     FTM_MSG_1(FTM_ERROR, "ftm_enh_internal_device_cal_fbrx_iq_cap_config failed with error code %d", ret_val );
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     rfcommon_fbrx_mc_disable_rf_dev(device_ptr);
     ftm_enh_internal_device_cal_fbrx_iq_cap_deconfig(fbrx_tech);
     internal_device_cal_data->internal_device_cal_status.error_log = ret_val;
     return FALSE;
   }

   if (cal_method == FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_STG_REAL_COEFFS)
   {
     // need to pass in gain state, num_caps, first_trig, last_trig, base_offset_khz, num_iq_samples
     ret_val = ftm_enh_internal_device_cal_fb_droop_stg_sequence( tx_device,
                                                                   num_captures,
                                                                   &stg_offset_khz[0],
                                                                   num_iq_samples,
                                                                   debug_mode_en,
                                                                  do_processing
                                                                  );

   }
   else if (cal_method == FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_STG_CPLX_COEFFS)
   {
     FTM_MSG(FTM_ERROR, "FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_STG_CPLX_COEFFS not supported yet!");
     ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
   }
   else if (cal_method == FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_MOD_REAL_COEFFS)
   {
     // need to pass in gain state, num_caps, first_trig, last_trig, num_iq_samples
     // ret_val = ftm_enh_internal_device_cal_fb_droop_mod_sequence(fbrx_gain_state,
     //                                                              num_captures,
     //                                                              first_trig,
     //                                                              last_trig,
     //                                                              num_iq_samples
     //                                                             );
     ret_val = ftm_enh_internal_device_cal_fb_droop_mod_sequence();
   }
   else if (cal_method == FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_MOD_CPLX_COEFFS)
   {
     FTM_MSG(FTM_ERROR, "FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_MOD_CPLX_COEFFS not supported yet!");
     ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
   }
   else
   {
     FTM_MSG(FTM_ERROR, "Undefined calibration method!");
     ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
   }

   if (ret_val != FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS)
   {
     FTM_MSG_1(FTM_ERROR, "ftm_enh_internal_device_cal_fb_dc_capture_sequence failed with error code %d", ret_val );
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     rfcommon_fbrx_mc_disable_rf_dev(device_ptr);
     ftm_enh_internal_device_cal_fbrx_iq_cap_deconfig(fbrx_tech);
     internal_device_cal_data->internal_device_cal_status.error_log = ret_val;
     return FALSE;
   }

   ret_val = ftm_enh_internal_device_cal_fbrx_iq_cap_deconfig(fbrx_tech);
   if (ret_val != FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS)
   {
     FTM_MSG_1(FTM_ERROR, "ftm_enh_internal_device_cal_fbrx_iq_cap_deconfig failed with error code %d", ret_val );
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     rfcommon_fbrx_mc_disable_rf_dev(device_ptr);
     internal_device_cal_data->internal_device_cal_status.error_log = ret_val;
     return FALSE;
   }

   api_status = rfcommon_fbrx_mc_disable_rf_dev(device_ptr);
   if (api_status == FALSE)
   {
     FTM_MSG(FTM_ERROR, "Failed to Disable FBRx Device ");
     modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
     internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DEV_ACTION_FAILED;
     return FALSE;
   }

   // init result struct
   droop_result->fbrx_gain_state = fbrx_gain_state;
   droop_result->num_filters = MAX_NUM_DROOP_FILTERS_GEN_PER_SWP;

   for (iter = 0; iter < MAX_NUM_DROOP_FILTERS_GEN_PER_SWP; iter++)
   {
     droop_result->fbrx_droop_res[iter].fbrx_tech_bw_type = -1;
     droop_result->fbrx_droop_res[iter].group_delay = 0;
     droop_result->fbrx_droop_res[iter].num_filter_taps = 1;
     memset((void*)&(droop_result->fbrx_droop_res[iter].fbrx_fir_tap_coeffs_nv[0]), 0, 
            FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_FILTER_LEN_MAX * sizeof(int32));
     droop_result->fbrx_droop_res[iter].fbrx_fir_tap_coeffs_nv[0] = 1;
   }

   if (debug_mode_en != 0)
   {
     FTM_MSG_2(FTM_MED, " Inited Result: FBRxGainState: %d, NumFilters: %d,  ", 
               droop_result->fbrx_gain_state, 
               droop_result->num_filters );
     for (iter = 0; iter<MAX_NUM_DROOP_FILTERS_GEN_PER_SWP; iter++)
     {
       FTM_MSG_4(FTM_MED,  " Filter #%d, FBRxTechBWType: %d, GroupDelay: %d, NumFilterTaps: %d ", 
                  iter, 
                  droop_result->fbrx_droop_res[iter].fbrx_tech_bw_type,
                  droop_result->fbrx_droop_res[iter].group_delay,
                  droop_result->fbrx_droop_res[iter].num_filter_taps);
       for (iter2 = 0; iter2<FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_FILTER_LEN_MAX; iter2++)
       {
         FTM_MSG_2(FTM_MED, "        Coeff[%2d]  :   %d     ", 
                   iter2,
                   droop_result->fbrx_droop_res[iter].fbrx_fir_tap_coeffs_nv[iter2]);
       }
     }   
   }

   if (do_processing == 1)
   {
     // fill up with processing results
     droop_result->fbrx_gain_state = fbrx_gain_state;
     droop_result->num_filters = num_droop_filters;
     for (iter = 0; iter<num_droop_filters; iter++)
     {
       droop_result->fbrx_droop_res[iter].fbrx_tech_bw_type = droop_filter_tech_bw[iter];
       droop_result->fbrx_droop_res[iter].group_delay = droop_filter_grp_delay[iter];
       droop_result->fbrx_droop_res[iter].num_filter_taps = EQ_LEN;
       for (iter2 = 0; iter2<EQ_LEN; iter2++)
       {
         droop_result->fbrx_droop_res[iter].fbrx_fir_tap_coeffs_nv[iter2] = droop_filter_coeffs[iter][iter2];
       }
     }
     if (debug_mode_en != 0)
     {
       FTM_MSG_2(FTM_MED, " Inited Result: FBRxGainState: %d, NumFilters: %d,  ", 
                 droop_result->fbrx_gain_state, 
                 droop_result->num_filters );
       for (iter = 0; iter<MAX_NUM_DROOP_FILTERS_GEN_PER_SWP; iter++)
       {
         FTM_MSG_4(FTM_MED,  " Filter #%d, FBRxTechBWType: %d, GroupDelay: %d, NumFilterTaps: %d ", 
                    iter, 
                    droop_result->fbrx_droop_res[iter].fbrx_tech_bw_type,
                    droop_result->fbrx_droop_res[iter].group_delay,
                    droop_result->fbrx_droop_res[iter].num_filter_taps);
         for (iter2 = 0; iter2<FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_FILTER_LEN_MAX; iter2++)
         {
           FTM_MSG_2(FTM_MED, "        Coeff[%2d]  :   %d     ", 
                     iter2,
                     droop_result->fbrx_droop_res[iter].fbrx_fir_tap_coeffs_nv[iter2]);
         }
       }   
     }


     for ( iter2 = 0; iter2 < num_droop_filters; iter2++ )
     {
       if (debug_mode_en != 0)
       {
         nv_read_write_error = rfcommon_fbrx_read_current_droop_globals((rfcommon_fbrx_tx_cfg_type)droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type , droop_data_nv);
         if (nv_read_write_error!= RFCOMMON_FBRX_SUCCESS)
         {
           FTM_MSG_2(FTM_ERROR, "rfcommon_fbrx_read_current_droop_globals() returned error %d for tx cfg %d", nv_read_write_error, droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type );
           modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
           internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_NV_ACTION_FAILED;
           return FALSE;
         }

         FTM_MSG_1(FTM_MED, "Current Droop NV data for Tx BW %d", droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type );
         FTM_MSG_3(FTM_MED, "Droop Filter: NV_ver: %d , Group Delay %d, Number of Taps: %d", droop_data_nv->nv_version, droop_data_nv->group_delay, droop_data_nv->num_filter_taps );
         for (iter = 0; iter < RFCOMMON_FBRX_NUM_FILTER_TAPS; iter++)
         {
           FTM_MSG_2(FTM_MED, "Filter Coeff %d : %d", iter, droop_data_nv->filter_coeff[iter]);
         }
       }

       droop_data_nv->nv_version = 0;
       droop_data_nv->group_delay = droop_result->fbrx_droop_res[iter2].group_delay;
       droop_data_nv->num_filter_taps = droop_result->fbrx_droop_res[iter2].num_filter_taps; // need to find this out
       memset((void*)&(droop_data_nv->filter_coeff), 0, sizeof(int32)*RFCOMMON_FBRX_NUM_FILTER_TAPS);
       memscpy((void*)&(droop_data_nv->filter_coeff), sizeof(int32)*droop_result->fbrx_droop_res[iter2].num_filter_taps,
               (void*)&(droop_result->fbrx_droop_res[iter2].fbrx_fir_tap_coeffs_nv[0]), sizeof(int32)*droop_result->fbrx_droop_res[iter2].num_filter_taps) ; 

       nv_read_write_error = rfcommon_fbrx_override_droop_globals((rfcommon_fbrx_tx_cfg_type)droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type , droop_data_nv);

       if (nv_read_write_error!= RFCOMMON_FBRX_SUCCESS)
       {
         FTM_MSG_2(FTM_ERROR, "rfcommon_fbrx_override_droop_globals() returned error %d for tx cfg %d", nv_read_write_error, droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type );
         modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
         internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_NV_ACTION_FAILED;
         return FALSE;
       }

       /* Should clear out droop_data_nv before reading again, otherwise previous value to update prints again */
       if (debug_mode_en != 0)
       {
         nv_read_write_error = rfcommon_fbrx_read_current_droop_globals((rfcommon_fbrx_tx_cfg_type)droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type, droop_data_nv);

         if (nv_read_write_error!= RFCOMMON_FBRX_SUCCESS)
         {
           FTM_MSG_2(FTM_ERROR, "rfcommon_fbrx_read_current_droop_globals() returned error %d for tx cfg %d", nv_read_write_error, droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type );
           modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);
           internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_NV_ACTION_FAILED;
           return FALSE;
         }

         FTM_MSG_1(FTM_MED, "Overridden Droop NV data for Tx BW %d", droop_result->fbrx_droop_res[iter2].fbrx_tech_bw_type);
         FTM_MSG_3(FTM_MED, "Droop Filter: NV_ver: %d , Group Delay %d, Number of Taps: %d", droop_data_nv->nv_version, droop_data_nv->group_delay, droop_data_nv->num_filter_taps );
         for (iter = 0; iter < RFCOMMON_FBRX_NUM_FILTER_TAPS; iter++)
         {
           FTM_MSG_2(FTM_MED, "Filter Coeff %d : %d", iter, droop_data_nv->filter_coeff[iter]);
         }

       }

     }
   }
   /*de-allocate memory*/
   modem_mem_free((void*)droop_data_nv, MODEM_MEM_CLIENT_RFA);

   return TRUE;

}


ftm_enh_internal_device_cal_error_code_type 
ftm_enh_internal_device_cal_droop_stg_process(
   ftm_enh_internal_device_cal_fbrx_droop_cal_method_type alg_type, 
   uint8 num_captures, 
   int32 * stg_offset_khz, 
   uint8 * samp_buffers,
   uint16 num_samps
   )
{
  ftm_enh_internal_device_cal_data_type *internal_device_cal_data;
  uint8 fbrx_bw = RFCOMMON_FBRX_LTE_10;
  uint8 iter = 0;
  uint16 iter2 = 0;
  uint8 iter3 = 0;
  int32 samp_freq_hz = 17200000;   /* TODO: MUST GET THIS FROM RXLM BUFFER */
  uint8 freq_ctr = 0;
  int32 stg_offsets[3] = {0};
  uint8 samp_bufs[3] = {-1};
  ftm_enh_internal_device_cal_error_code_type ret_val = FTM_ENH_INTERNAL_DEVICE_CAL_RESULTS_SUCCESS; 
  uint8 frac_bits_M[2] = {0};
  int16 magic_matrix[2][3][EQ_LEN] = { {{0}, {0}, {0}}, {{0}, {0}, {0}} };
  uint8 debug_mode_en;
  boolean api_status = TRUE;
  int32 * rx_samps;
  int32 * tx_samps;
  uint32 rx_samp_pcfl20;
  uint8 exponent;
  int32 rxI;
  int32 rxQ;
  int16 rxI_FP16;
  int16 rxQ_FP16;
  uint8 overflow_det;
  double cplxExpI_dbl;
  double cplxExpQ_dbl;
  int16 cplxExpI_FP16;
  int16 cplxExpQ_FP16;
  double freqOffset;
  int32 mult1;
  int32 mult2;
  int32 multSumI_FP32;
  int32 multSumQ_FP32;
  int16 multSumI_FP16;
  int16 multSumQ_FP16;
  int32 sumMultI_FP32;
  int32 sumMultQ_FP32;
  int16 sumMultScaledI_FP16;
  int16 sumMultScaledQ_FP16;
  int32 tonePower_FP32;
  int16 tonePowerScaled_FP16;
  int32 dArray[2] = {0};
  int16 dRef = 0;
  int8 dCtr = -1;
  int32 coeffsMult_FP32[EQ_LEN];
  int16 coeffsMult_FP16[EQ_LEN];
  int16 coeffsSum[2][EQ_LEN] = { {0}, {0} };
  int32 coeffsSum_for_Norm[2] = {0, 0};
  int32 coeffsFinalSum[2] = {0,0};
 
  uint8 peak_loc = -1;
  int16 peak_val = 0x8000;
  int32 * temprxsamp;

  internal_device_cal_data = ftm_common_data_get_enh_internal_device_cal_ptr();

  debug_mode_en = internal_device_cal_data->internal_device_cal_config.dbg_mode_en;
  fbrx_bw = internal_device_cal_data->internal_device_cal_config.bw;

  if (fbrx_bw >= RFCOMMON_FBRX_NUM_TX_CONFIG_MAX )
  {
    FTM_MSG_1(FTM_ERROR, "Unknown FBRx Tech/BW combination %d", fbrx_bw);
    internal_device_cal_data->internal_device_cal_status.error_log = FTM_ENH_INTERNAL_DEVICE_CAL_INVALID_TECHBW;
    return FALSE;
  }

  // For LTE15, need to perform LTE20 captures, different processing
  // For LTE5,  need to perform LTE10 captures, different processing
  // For LTE1p4/3, need to perform LTE10 captures, reuse processing results for LTE5
  switch (fbrx_bw)
  {
  case RFCOMMON_FBRX_LTE_15:
    fbrx_bw = RFCOMMON_FBRX_LTE_20;
    break;
  case RFCOMMON_FBRX_LTE_1P4:
  case RFCOMMON_FBRX_LTE_3:
  case RFCOMMON_FBRX_LTE_5:
    fbrx_bw = RFCOMMON_FBRX_LTE_10;
    break;
  default:
    break;
  }

  if (alg_type != FTM_ENH_INTERNAL_DEVICE_CAL_FBRX_DROOP_STG_REAL_COEFFS)
  {
    FTM_MSG(FTM_ERROR, "Droop Filter Calculation using STG currently supports only Real Coefficients!");
    return FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
  }

  if ( (fbrx_bw == RFCOMMON_FBRX_LTE_10) || (fbrx_bw == RFCOMMON_FBRX_LTE_20) )
  {
    num_droop_filters = 2;
    if (fbrx_bw == RFCOMMON_FBRX_LTE_10)
    {
      frac_bits_M[0] = fracBits_LTE10;
      frac_bits_M[1] = fracBits_LTE5;
      droop_filter_tech_bw[0] = RFCOMMON_FBRX_LTE_10;
      droop_filter_tech_bw[1] = RFCOMMON_FBRX_LTE_5;
      memscpy((void*)&magic_matrix[0][0][0], sizeof(M_LTE10), (void*)&M_LTE10, sizeof(M_LTE10));
      memscpy((void*)&magic_matrix[1][0][0], sizeof(M_LTE5), (void*)&M_LTE5, sizeof(M_LTE5));

      for (iter = 0; iter < num_captures; iter++ )
      {
        if (stg_offset_khz[iter] == 500)
        {
          stg_offsets[0] = 500000;
          samp_bufs[0] = samp_buffers[iter];
          freq_ctr++;
        }
        if (stg_offset_khz[iter] == 2000)
        {
          stg_offsets[1] = 2000000;
          samp_bufs[1] = samp_buffers[iter];
          freq_ctr++;
        }
        if (stg_offset_khz[iter] == 4500)
        {
          stg_offsets[2] = 4500000;
          samp_bufs[2] = samp_buffers[iter];
          freq_ctr++;
        }
      }

      if (freq_ctr < 3)
      {
        FTM_MSG_1(FTM_ERROR, "Insufficient set size of STG frequency offsets, %d, need 3 offsets at 0.5k, 2M, 4.5M required", freq_ctr);
        return FTM_ENH_INTERNAL_DEVICE_CAL_INSUFFICIENT_PARAMS;
      }

    }
    else if (fbrx_bw == RFCOMMON_FBRX_LTE_20)
    {
      samp_freq_hz = samp_freq_hz*2;   /* TODO: MUST GET THIS FROM RXLM BUFFER */
      frac_bits_M[0] = fracBits_LTE20;
      frac_bits_M[1] = fracBits_LTE15;
      droop_filter_tech_bw[0] = RFCOMMON_FBRX_LTE_20;
      droop_filter_tech_bw[1] = RFCOMMON_FBRX_LTE_15;
      memscpy((void*)&magic_matrix[0][0][0], sizeof(M_LTE20), (void*)&M_LTE20, sizeof(M_LTE20));
      memscpy((void*)&magic_matrix[1][0][0], sizeof(M_LTE15), (void*)&M_LTE15, sizeof(M_LTE15));

      for (iter = 0; iter < num_captures; iter++ )
      {
        if (stg_offset_khz[iter] == 500)
        {
          stg_offsets[0] = 500000;
          samp_bufs[0] = samp_buffers[iter];
          freq_ctr++;
        }
        if (stg_offset_khz[iter] == 4000)
        {
          stg_offsets[1] = 4000000;
          samp_bufs[1] = samp_buffers[iter];
          freq_ctr++;
        }
        if (stg_offset_khz[iter] == 9000)
        {
          stg_offsets[2] = 9000000;
          samp_bufs[2] = samp_buffers[iter];
          freq_ctr++;
        }
      }

      if (freq_ctr < 3)
      {
        FTM_MSG_1(FTM_ERROR, "Insufficient set size of STG frequency offsets, %d, 3 offsets at 0.5k, 4M, 9M required", freq_ctr);
        return FTM_ENH_INTERNAL_DEVICE_CAL_INSUFFICIENT_PARAMS;
      }

    }
  }
  else if ( (fbrx_bw == RFCOMMON_FBRX_UMTS_DC) || (fbrx_bw == RFCOMMON_FBRX_MCDO_7X) )
  {
    num_droop_filters = 1;
    droop_filter_tech_bw[0] = fbrx_bw;
    frac_bits_M[0] = fracBits_LTE10;    // UMTS_DC and MCDO_7X can reuse same results as LTE10
    memscpy((void*)&magic_matrix[0][0][0], sizeof(M_LTE10), (void*)&M_LTE10, sizeof(M_LTE10));

    for (iter = 0; iter < num_captures; iter++ )
    {
      if (stg_offset_khz[iter] == 500)
      {
        stg_offsets[0] = 500000;
        samp_bufs[0] = samp_buffers[iter];
        freq_ctr++;
      }
      if (stg_offset_khz[iter] == 2000)
      {
        stg_offsets[1] = 2000000;
        samp_bufs[1] = samp_buffers[iter];
        freq_ctr++;
      }
      if (stg_offset_khz[iter] == 4500)
      {
        stg_offsets[2] = 4500000;
        samp_bufs[2] = samp_buffers[iter];
        freq_ctr++;
      }
    }

    if (freq_ctr < 3)
    {
      FTM_MSG_1(FTM_ERROR, "Insufficient set size of STG frequency offsets, %d, need 3 offsets at 0.5k, 2M, 4.5M required", freq_ctr);
      return FTM_ENH_INTERNAL_DEVICE_CAL_INSUFFICIENT_PARAMS;
    }    
  }
  else if ( (fbrx_bw == RFCOMMON_FBRX_1X) || 
            (fbrx_bw == RFCOMMON_FBRX_MCDO_3X) || 
            (fbrx_bw == RFCOMMON_FBRX_UMTS_SC) || 
            (fbrx_bw == RFCOMMON_FBRX_TDS) ||
            (fbrx_bw == RFCOMMON_FBRX_SB_SVDO)
          )  // 1X, MCDO_3X, UMTS_SC, TDS, SB_SVDO can reuse LTE5 results
  {
    num_droop_filters = 1;
    droop_filter_tech_bw[0] = fbrx_bw;
    frac_bits_M[0] = fracBits_LTE5;
    memscpy((void*)&magic_matrix[0][0][0], sizeof(M_LTE5), (void*)&M_LTE5, sizeof(M_LTE5));

    for (iter = 0; iter < num_captures; iter++ )
    {
      if (stg_offset_khz[iter] == 500)
      {
        stg_offsets[0] = 500000;
        samp_bufs[0] = samp_buffers[iter];
        freq_ctr++;
      }
      if (stg_offset_khz[iter] == 2000)
      {
        stg_offsets[1] = 2000000;
        samp_bufs[1] = samp_buffers[iter];
        freq_ctr++;
      }
      if (stg_offset_khz[iter] == 4500)
      {
        stg_offsets[2] = 4500000;
        samp_bufs[2] = samp_buffers[iter];
        freq_ctr++;
      }
    }

    if (freq_ctr < 3)
    {
      FTM_MSG_1(FTM_ERROR, "Insufficient set size of STG frequency offsets, %d, need 3 offsets at 0.5k, 2M, 4.5M required", freq_ctr);
      return FTM_ENH_INTERNAL_DEVICE_CAL_INSUFFICIENT_PARAMS;
    }    
  }
  else
  {
    num_droop_filters = 1;
    droop_filter_tech_bw[0] = fbrx_bw;
    FTM_MSG_1(FTM_ERROR, "Unsupported Calibration for Tech/BW mode %d", fbrx_bw);
    return FTM_ENH_INTERNAL_DEVICE_CAL_UNSUPPORTED_ACTION;
  }

  if (debug_mode_en != 0)
  {
    for (iter = 0; iter < num_droop_filters; iter++)
    {
      for (iter2 = 0; iter2 < 3; iter2++)
      {
        FTM_MSG_1(FTM_MED, "--------  Row     %d   ---------", iter2);
        FTM_MSG_3(FTM_MED, "stg_offset %d samp buf id %d for freq index %d ", stg_offsets[iter2], samp_bufs[iter2], iter2);
        for (iter3 = 0; iter3 < EQ_LEN; iter3++)
        {
          FTM_MSG_1(FTM_MED, "    %10d ", magic_matrix[iter][iter2][iter3]);
        }
      }
    }
  }

  //rx_offset = 0;
  for (iter = 0; iter < freq_ctr; iter++)
  {
    api_status = rfcommon_mdsp_fetch_selftest_iq_data(samp_bufs[iter], num_samps, &rx_samps, &tx_samps);
    if (api_status == FALSE)
    {
      FTM_MSG_1(FTM_ERROR, "rfcommon_mdsp_fetch_selftest_iq_data failed with status %d", api_status);
      return FTM_ENH_INTERNAL_DEVICE_CAL_FETCH_SAMPLES_FAILED;
    }
    temprxsamp = rx_samps;
    freqOffset = ((double)stg_offsets[iter]*9216)/samp_freq_hz;
    if (debug_mode_en != 0)
    {
      FTM_MSG_4(FTM_MED, "---------- Rx Samples for Capture %d, Freq %d, num_samps %d, freqs %d -----------", iter, stg_offsets[iter], num_samps, freq_ctr );
    }
    sumMultI_FP32 = 0;
    sumMultQ_FP32 = 0;
    for (iter2 = 0; iter2 < num_samps; iter2++)
    {
      cplxExpI_dbl =  floor(cos((-2*PI*freqOffset*iter2)/num_samps)*pow(2,KERNEL_FRAC_RES));
      cplxExpQ_dbl =  floor(sin((-2*PI*freqOffset*iter2)/num_samps)*pow(2,KERNEL_FRAC_RES));
      cplxExpI_FP16 = (int16)cplxExpI_dbl;
      cplxExpQ_FP16 = (int16)cplxExpQ_dbl;

      if ( (iter2 <20) &  (debug_mode_en != 0))
      {
        FTM_MSG_3(FTM_MED, "~~~~~~~~~~~~ KERNEL %d : %16d   + j *   %16d ", iter2 , cplxExpI_FP16, cplxExpQ_FP16 );
      }
      if ( (iter2 > 9206) &  (debug_mode_en != 0))
      {
        FTM_MSG_3(FTM_MED, "~~~~~~~~~~~~ KERNEL %d : %16d   + j *   %16d ", iter2 , cplxExpI_FP16, cplxExpQ_FP16 );
      }
      if ( (iter2 <20) &  (debug_mode_en != 0))
      {
        FTM_MSG_1(FTM_MED, "Raw Samp 0x%8x", *(temprxsamp +iter2 ) );
      }
      if ( (iter2 > 9206) &  (debug_mode_en != 0))
      {
        FTM_MSG_1(FTM_MED, "Raw Samp 0x%8x", *(temprxsamp +iter2 ) );
      }

      if (iter2%8 == 0)
      {
        rx_samp_pcfl20 = (((uint32)*rx_samps) >> 0) & 0xFFFFF;
      }
      else if (iter2%8 == 1)
      {
        rx_samp_pcfl20 = ( ( ((uint32)*rx_samps) >> 20 )  +  ( ( (uint32)*(rx_samps + 1) ) << 12 )) & 0xFFFFF ;
      }
      else if (iter2%8 == 2)
      {
        rx_samp_pcfl20 = ( ((uint32)*(rx_samps+1) ) >> 8) & 0xFFFFF;
      }
      else if (iter2%8 == 3)
      {
        rx_samp_pcfl20 = ( ( ( (uint32)*( rx_samps + 1 ) ) >> 28 ) + ( ( (uint32) *( rx_samps + 2 ) ) << 4 ) ) & 0xFFFFF;
      }
      else if (iter2%8 == 4)
      {
        rx_samp_pcfl20 = ( ( ( (uint32) *( rx_samps + 2 ) ) >> 16 ) + (( (uint32) *( rx_samps + 3 ) ) << 16 ) ) & 0xFFFFF;
      }
      else if (iter2%8 == 5)
      {
        rx_samp_pcfl20 = (((uint32)*(rx_samps+3)) >> 4) & 0xFFFFF;
      }
      else if (iter2%8 == 6)
      {
        rx_samp_pcfl20 = ( ( ((uint32) *( rx_samps + 3 ) ) >> 24 ) + ( ( (uint32) *( rx_samps + 4 ) ) << 8 )) & 0xFFFFF;
      }
      else if (iter2%8 == 7)
      {
        rx_samp_pcfl20 = (((uint32)*(rx_samps+4)) >> 12) & 0xFFFFF;
        rx_samps = rx_samps + 5;
      }

      exponent = (uint8)((rx_samp_pcfl20>>16) & 0xFF);
      rxI = (int32)(((int8)((rx_samp_pcfl20>>8) & 0xFF)) << (15-exponent)); 
      rxQ = (int32)(((int8)((rx_samp_pcfl20>>0) & 0xFF)) << (15-exponent)); 

      rxI = rxI>>5;
      rxQ = rxQ>>5;

      rxI_FP16 = sat_int32_to_int16(rxI, &overflow_det);
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }
      rxQ_FP16 = sat_int32_to_int16(rxQ, &overflow_det);
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }

      if ( (iter2 <20) &  (debug_mode_en != 0))
      {
        FTM_MSG_6(FTM_MED, "samp ind %4d    =  %16d  +  j *  %16d   ,  %16d + j * %16d, overflow_det = %d", iter2, rxI, rxQ, rxI_FP16, rxQ_FP16, overflow_det);
      }
      if ( (iter2 > 9206) &  (debug_mode_en != 0))
      {
        FTM_MSG_6(FTM_MED, "samp ind %4d    =  %16d  +  j *  %16d   ,  %16d + j * %16d, overflow_det = %d", iter2, rxI, rxQ, rxI_FP16, rxQ_FP16, overflow_det);
      }

      mult1 = (rxI_FP16*cplxExpI_FP16);
      mult2 = (rxQ_FP16*cplxExpQ_FP16);
      multSumI_FP32 = (sat_add_int32( mult1, -mult2, &overflow_det )) >> 13;
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }
      multSumI_FP16 = sat_int32_to_int16(multSumI_FP32, &overflow_det);
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }

      mult1 = (rxQ_FP16*cplxExpI_FP16);
      mult2 = (rxI_FP16*cplxExpQ_FP16);
      multSumQ_FP32 = (sat_add_int32( mult1, mult2, &overflow_det )) >> 13;
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }
      multSumQ_FP16 = sat_int32_to_int16(multSumQ_FP32, &overflow_det);
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }

      if ( (iter2 <20) &  (debug_mode_en != 0))
      {
        FTM_MSG_6(FTM_MED, "MULT RES IND %4d   FP_32  =  %16d  +  j *  %16d,  FP_16   =  %16d  +  j *  %16d, overflow_det = %d", iter2, multSumI_FP32, multSumQ_FP32, multSumI_FP16, multSumQ_FP16, overflow_det );
      }
      if ( (iter2 > 9206) &  (debug_mode_en != 0))
      {
        FTM_MSG_6(FTM_MED, "MULT RES IND %4d   FP_32  =  %16d  +  j *  %16d,  FP_16   =  %16d  +  j *  %16d, overflow_det = %d", iter2, multSumI_FP32, multSumQ_FP32, multSumI_FP16, multSumQ_FP16, overflow_det );
      }

      sumMultI_FP32 = sat_add_int32( sumMultI_FP32, (int32)multSumI_FP16, &overflow_det );
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }

      sumMultQ_FP32 = sat_add_int32( sumMultQ_FP32, (int32)multSumQ_FP16, &overflow_det );
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }
    }

    sumMultScaledI_FP16 = sat_int32_to_int16( sumMultI_FP32 >> (10+4), &overflow_det);
    if (overflow_det == 1)
    {
      return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
    }
    sumMultScaledQ_FP16 = sat_int32_to_int16( sumMultQ_FP32 >> (10+4), &overflow_det);
    if (overflow_det == 1)
    {
      return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
    }

    if (debug_mode_en != 0)
    {
      FTM_MSG_5(FTM_MED, "Freq %d   Sum Mult FP32:  %16d  +   j  *  %16d , Sum Mult Scaled FP16:   %16d + j * %16d", iter, sumMultI_FP32, sumMultQ_FP32, sumMultScaledI_FP16, sumMultScaledQ_FP16 );
    }

    tonePower_FP32 = sat_add_int32( (sumMultScaledI_FP16*sumMultScaledI_FP16) , (sumMultScaledQ_FP16*sumMultScaledQ_FP16), &overflow_det );
    if (overflow_det == 1)
    {
      return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
    }

    tonePowerScaled_FP16 = (int16)(tonePower_FP32 >> 15);
    if (debug_mode_en != 0)
    {
      FTM_MSG_2(FTM_MED, "tonePower_FP32: %d, tonePowerScaled_FP16: %d", tonePower_FP32, tonePowerScaled_FP16);
    }
    if (dCtr == -1)
    {
      dRef = tonePowerScaled_FP16;
    }

    if (dCtr>=0)
    {
      tonePower_FP32 = (int32)((tonePowerScaled_FP16 << 15) & 0xFFFF8000);
      dArray[dCtr] = sat_int32_to_int16( (int32) ((int32)tonePower_FP32 /(int32)dRef ), &overflow_det); // how to detect over/under for division
      if (overflow_det == 1)
      {
        return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
      }
      if (debug_mode_en != 0)
      {
        FTM_MSG_4(FTM_MED, "relative calc - tonePower_FP32: %d, dRef: %d, D[%d]: %d", tonePower_FP32, dRef, dCtr, dArray[dCtr]);
      }
    }

    for (iter2 = 0; iter2 < num_droop_filters; iter2++)
    {
      if (debug_mode_en != 0)
      {
        FTM_MSG_1( FTM_MED, " << Coeff Calculation for Filter %d >> ", iter2  );
      }
      if ( dCtr == -1 )
      {
        for (iter3 = 0; iter3 < EQ_LEN; iter3++)
        {
          if ( (15 - frac_bits_M[iter2] - 3) >=0 )
          {
            coeffsMult_FP32[iter3] = (int32) ( magic_matrix[iter2][0][iter3] << (15 - frac_bits_M[iter2] - 3) ) ;
          }
          else
          {
            coeffsMult_FP32[iter3] = (int32) ( magic_matrix[iter2][0][iter3] >> (frac_bits_M[iter2] + 3 - 15 ) ) ;
          }
          coeffsMult_FP16[iter3] = sat_int32_to_int16(coeffsMult_FP32[iter3], &overflow_det);
          if (overflow_det == 1)
          {
            return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
          }
          coeffsSum[iter2][iter3] = sat_add_int16(coeffsSum[iter2][iter3], coeffsMult_FP16[iter3] , &overflow_det );
          if (overflow_det == 1)
          {
            return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
          }
          if (debug_mode_en != 0)
          {
            FTM_MSG_3( FTM_MED, " Coeff Sum idx %d - FP32: %d ,  FP16:  %d ", iter3, coeffsMult_FP32[iter3], coeffsMult_FP16[iter3] );
          }
        }
      }
      else
      {
        for (iter3 = 0; iter3 < EQ_LEN; iter3++)
        {
          coeffsMult_FP32[iter3] = ( magic_matrix[iter2][dCtr+1][iter3] * dArray[dCtr] ) >>  (frac_bits_M[iter2] + 3);
          coeffsMult_FP16[iter3] = sat_int32_to_int16(coeffsMult_FP32[iter3], &overflow_det);
          if (overflow_det == 1)
          {
            return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
          }
          coeffsSum[iter2][iter3] = sat_add_int16(coeffsSum[iter2][iter3], coeffsMult_FP16[iter3], &overflow_det );
          if (overflow_det == 1)
          {
            return FTM_ENH_INTERNAL_DEVICE_CAL_ARITHMETIC_OVERFLOW;
          }
          if (debug_mode_en != 0)
          {
            FTM_MSG_3( FTM_MED, " Coeff Sum idx %d - FP32: %d ,  FP16:  %d ", iter3, coeffsMult_FP32[iter3], coeffsMult_FP16[iter3] );
          }
        }
      }
    }

    dCtr += 1;
  }

  FTM_MSG(FTM_MED, "---------------- Unnormalized Coefficients ----------------");
  for (iter2 = 0; iter2 < num_droop_filters; iter2++)
  {
    FTM_MSG_1(FTM_MED, "  << Filter  %d  >>", iter2);
    peak_val = 0x8000;
    for (iter3 = 0; iter3 < EQ_LEN; iter3++)
    {
      droop_filter_coeffs[iter2][iter3] = coeffsSum[iter2][iter3] << 2;
      coeffsSum_for_Norm[iter2] += droop_filter_coeffs[iter2][iter3];
      if (droop_filter_coeffs[iter2][iter3] > peak_val)
      {
        peak_val = droop_filter_coeffs[iter2][iter3];
        peak_loc = iter3;
      }
      FTM_MSG_2(FTM_MED, "tap %d : %d", iter3, droop_filter_coeffs[iter2][iter3]);
    }
    droop_filter_grp_delay[iter2] = peak_loc;
    FTM_MSG_1(FTM_MED, "group delay : %d", droop_filter_grp_delay[iter2]);
  }

  FTM_MSG(FTM_MED, "---------------- Normalized Coefficients ----------------");
  for (iter2 = 0; iter2 < num_droop_filters; iter2++)
  {
    FTM_MSG_1(FTM_MED, "  << Filter  %d  >>", iter2);
    for (iter3 = 0; iter3 < EQ_LEN; iter3++)
    {
      droop_filter_coeffs[iter2][iter3] = (droop_filter_coeffs[iter2][iter3] << 14) / coeffsSum_for_Norm[iter2];
      coeffsFinalSum[iter2] += droop_filter_coeffs[iter2][iter3];
      FTM_MSG_2(FTM_MED, "tap %d : %d", iter3, droop_filter_coeffs[iter2][iter3]);
    }
  }

  FTM_MSG(FTM_MED, "---------------- Filter Norm ----------------");
  for (iter2 = 0; iter2 < num_droop_filters; iter2++)
  {
    FTM_MSG_1(FTM_MED, "  << Filter  %d  >>", iter2);
    FTM_MSG_1(FTM_MED, "Norm  %d", coeffsFinalSum[iter2]);
  }

  // reuse LTE5 filter data generated from LTE10 captures for LTE1p4/3
  if (fbrx_bw == RFCOMMON_FBRX_LTE_10)
  {
    num_droop_filters = 4;
    droop_filter_tech_bw[2] = RFCOMMON_FBRX_LTE_3;
    droop_filter_tech_bw[3] = RFCOMMON_FBRX_LTE_1P4;
    droop_filter_grp_delay[2] = droop_filter_grp_delay[1];
    droop_filter_grp_delay[3] = droop_filter_grp_delay[1];
    memscpy((void*)&droop_filter_coeffs[2][0], sizeof(int32)*EQ_LEN, (void*)&droop_filter_coeffs[1][0], sizeof(int32)*EQ_LEN); // copy over LTE5 filter for LTE3
    memscpy((void*)&droop_filter_coeffs[3][0], sizeof(int32)*EQ_LEN, (void*)&droop_filter_coeffs[1][0], sizeof(int32)*EQ_LEN); // copy over LTE5 filter for LTE3
  }

  return ret_val;


}


#endif /* FEATURE_FACTORY_TESTMODE */

#endif /* FEATURE_GNSS_ONLY_NO_WWAN */

/*! @} */


