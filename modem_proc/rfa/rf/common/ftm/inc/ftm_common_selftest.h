#ifndef FTM_COMMON_SELFTEST_H
#define FTM_COMMON_SELFTEST_H

/*!
  @file
  ftm_common_selftest.h

  @brief
  This module contains definitions and prototypes for common selftest functionality.
*/

/*==============================================================================

  Copyright (c) 2012 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     -------------------------------------------------------------
07/31/14   jmf     Correct Enum type for EVM capture
                   Add API to obtain current self test flag
07/24/14   jmf     [SelfTest] Move self test mode global flag to .c
07/24/14   dbz     Fix VSWR measurement mask
07/17/14   jmf     Add api to set self test flag / Define flag for self test mode
06/30/14   daa     Created.

==============================================================================*/

#include "ftm.h"
#include "DALStdDef.h"

#define ENABLE_TX_PWR_MEAS 0x01
#define ENABLE_ACLR_MEAS   0x04
#define ENABLE_EVM_MEAS    0x02
#define ENABLE_VSWR_MEAS   0x20
// #define ENABLE_RX_RMS_MEAS 0x01
// #define ENABLE_EVM_MEAS    0x02

boolean ftm_common_fbrx_iq_capture_processing
(
  ftm_rf_technology_type ftm_rf_tech,
  uint8 fbrx_chain,
  uint32 ftm_selftest_capture_type,
  uint16 num_samples,
  int32 **iq_buf_rx,
  int32 **iq_buf_tx,
  uint16 *evm_value_ptr,
  uint32 *rx_rms_mag_ptr
);

void ftm_common_selftest_set_flag( uint8 * flag_value);
uint8 ftm_common_get_selftest_capture_flag(void);
#endif