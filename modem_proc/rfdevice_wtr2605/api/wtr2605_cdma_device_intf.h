#ifndef WTR2605_CDMA_DEVICE_INTF_H
#define WTR2605_CDMA_DEVICE_INTF_H
/*! 
  @file
  wtr2605_cdma_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 CDMA driver

  @details
  Contains function prototypes to create WTR2605 CDMA devices and any other
  direct configuration from RFC like setting band/ports, selecting TX LUTs etc

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.

  This file will have to be included by RFC to communicate with WTR2605 and must 
  be shipped.

  @addtogroup WTR2605_CDMA
  @{
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_cdma_device_intf.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
12/10/13   cd      Added support for CDMA on second WTR device
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
05/25/12   bm      Initial Revision merged from 
                   rfa2/device/wtr1605/cdma/main/latest/inc/wtr1605_cdma_device_intf.h#2

==============================================================================*/

#include "rfdevice_cdma_interface.h"
#include "rfdevice_cmn_intf.h"

/*! Max number of devices supported by WTR2605 */
#define WTR2605_MAX_CDMA_DEVICES 2

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Structure contains pointers to the various CDMA devices in a WTR2605 device

  @details
  WTR2605 contains 1 CDMA Receive path: RX0 used as receiver 1 Transmitter. 
  This structure puts pointers to these CDMA devices together for a particular
  single WTR2605 device.

  A structure of this type is returned to RFC when it creates a WTR2605 CDMA 
  device thus giving RFC access to all CDMA devices in the new device that
  has been created.
*/
typedef struct
{
  /*! Primary CDMA receive device (on physical path 0) of WTR2605 */
  rfdevice_cdma_rx_type* rx0_device;

  /*! CDMA transmit device of WTR2605 */
  rfdevice_cdma_tx_type* tx_device;
} wtr2605_cdma_devices_type;

/*----------------------------------------------------------------------------*/
wtr2605_cdma_devices_type
wtr2605_cdma_create_device
(
  rfdevice_cmn_type* common_device
);

/*----------------------------------------------------------------------------*/
boolean
wtr2605_cdma_rx_set_band_port
(
  rfdevice_cdma_rx_type* rx_device,
  rfm_cdma_band_class_type rfm_band,
  uint8 rx_band_port
);

/*----------------------------------------------------------------------------*/
boolean
wtr2605_cdma_tx_set_band_port
(
  rfdevice_cdma_tx_type* tx_device,
  rfm_cdma_band_class_type rfm_band,
  uint8 tx_band_port
);

/*! @} */

#endif /* WTR2605_CDMA_DEVICE_INTF_H */
