#ifndef WTR2605_WCDMA_DEVICE_INTF_H
#define WTR2605_WCDMA_DEVICE_INTF_H
/*! 
  @file
  wtr2605_wcdma_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 WCDMA driver

  @details
  Contains function prototypes to create WTR2605 WCDMA devices and any other
  direct configuration from RFC like setting band/ports, selecting TX LUTs etc

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.  This file will have to be included by RFC to communicate with WTR2605
  and must be shipped.

  @addtogroup WTR2605_WCDMA
  @{
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: 

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
11/29/13   ak      Increasing the number of device instance for WCDMA to support OMRD.
11/28/12   ry      Updated rx and tx band port functions to accept
                   rfm_device as input parameter
11/16/12   av      Support for WTR2605 on Dime/Triton
09/28/12   swb     Add support for RFC virtual PA mapping
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
06/06/12   bm      Added interface for functions wtr2605_wcdma_rx_set_band_port()
                   and wtr2605_wcdma_tx_set_band_port()
05/25/12   bm      Initial Revision merged from 
                   rfa2/device/wtr1605/wcdma/main/latest/inc/wtr1605_wcdma_device_intf.h #1
==============================================================================*/

#include "rfdevice_wcdma_intf.h"
#include "rfdevice_cmn_intf.h"
#include "rfcom.h" 

/*! Max number of devices supported by WTR2605 */
#define WTR2605_MAX_WCDMA_DEVICES 2

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Structure contains pointers to the various WCDMA devices in a WTR2605 device

  @details
  WTR2605 contains 2 WCDMA Receive paths: RX0 used as a primary receiver and
  RX1 used for Diversity and SHDR and 1 Transmitter. This structure puts 
  pointers to these WCDMA devices together for a particular single WTR2605 
  device.

  A structure of this type is returned to RFC when it creates a WTR2605 WCDMA 
  device thus giving RFC access to all WCDMA devices in the new device that
  has been created.
*/
typedef struct
{
  /*! Primary WCDMA receive device (on physical path 0) of WTR2605 */
  rfwcdma_device_rx_type* rx0_device;

  /*! Secondary WCDMA receive device (on physical path 1) of WTR1605 */
  rfwcdma_device_rx_type* rx1_device;

  /*! WCDMA transmit device (on physical path 0) of WTR2605 */
  rfwcdma_device_tx_type* tx_device;

} wtr2605_wcdma_devices_type;

/*----------------------------------------------------------------------------*/
wtr2605_wcdma_devices_type wtr2605_wcdma_create_device
(
  rfdevice_cmn_type* common_device
);

/*----------------------------------------------------------------------------*/
extern boolean wtr2605_wcdma_rx_set_band_port
(
  rfm_device_enum_type rfm_device,
  rfwcdma_device_rx_type* rx_device,
  rfcom_wcdma_band_type rfcom_band,
  uint8 rx_band_port
);

/*----------------------------------------------------------------------------*/
extern boolean wtr2605_wcdma_tx_set_band_port
(
  rfm_device_enum_type rfm_device,
  rfwcdma_device_tx_type* tx_device,
  rfcom_wcdma_band_type rfcom_band,
  uint8 tx_band_port,
  uint32 tx_band_pa_lut_map
);


/*! @} */

#endif /* WTR2605_WCDMA_DEVICE_INTF_H */



