
#ifndef WTR2605_GNSS_DEVICE_INTF_H
#define WTR2605_GNSS_DEVICE_INTF_H
/*! 
  @file
  wtr2605_gnss_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 GNSS driver

  @details
  Contains function prototypes to create WTR2605 GNSS devices

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.

  This file will have to be included by RFC to communicate with WTR2605 and must 
  be shipped.

  @addtogroup WTR2605_GNSS
  @{
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_gnss_device_intf.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/25/12   bm     Initial Revision

==============================================================================*/

#include "rfdevice_cmn_intf.h"

/*----------------------------------------------------------------------------*/
/*!
   @brief
   Initialize GNSS device pointers.

   @details

   @param path: rf device path.

   @retval None.
*/
void rfdevice_wtr2605_gnss_config(rfgnss_path_enum_type rfgnss_path);

/*! @} */

#endif /* WTR2605_GNSS_DEVICE_INTF_H */
