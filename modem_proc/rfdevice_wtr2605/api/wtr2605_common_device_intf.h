#ifndef WTR2605_COMMON_DEVICE_INTF_H
#define WTR2605_COMMON_DEVICE_INTF_H
/*! 
  @file
  wtr2605_common_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 Common driver

  @details
  Contains function prototypes to create WTR2605 common devices and any other
  direct configuration from RFC.

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.

  This file will have to be included by RFC to communicate with WTR2605 and must 
  be shipped.

  @addtogroup WTR2605_COMMON
  @{
*/

/*==============================================================================

  Copyright (c) 2011-2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_common_device_intf.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
05/12/13   ry      Added "destructor"
01/04/13   av      DSDA changes
08/01/12   bm      Added interface for wtr2605_common_get_device()
05/25/12   bm      Initial Revision merged from
                   rfa2/device/wtr1605/common/main/latest/inc/wtr1605_common_device_intf.h#4

==============================================================================*/

#include "rfdevice_cmn_intf.h"

/*! Max number of Common devices supported by WTR2605 */
#define WTR2605_MAX_DEVICES 2

/*----------------------------------------------------------------------------*/
rfdevice_cmn_type*
wtr2605_common_create_device
(
  rfdevice_bus_info_type wtr2605_ssbi_info,
  rfdevice_cmn_int_dev_cal_data_type cal_data
);

/*----------------------------------------------------------------------------*/
boolean
wtr2605_common_do_internal_device_cal
(
  rfdevice_cmn_type* cmn_device,
  rfdevice_cmn_int_dev_cal_data_type* cal_data
);

/*----------------------------------------------------------------------------*/
void* 
wtr2605_common_get_device
(
  rfdevice_cmn_type* common_device,
  rfm_mode_enum_type rfm_mode,
  rfdevice_chain_enum_type chain 
);

/*----------------------------------------------------------------------------*/
void
wtr2605_common_destroy_device
(
  rfdevice_cmn_type* device
);
/*! @} */

#endif /* WTR2605_COMMON_DEVICE_INTF_H */
