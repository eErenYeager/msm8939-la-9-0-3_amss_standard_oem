#ifndef WTR2605_WCDMA_CLASS_H
#define WTR2605_WCDMA_CLASS_H
/*!
  @file wtr2605_wcdma_class.h 

  @brief
  This is the rf device factory which creates all devices.



*/

/*===========================================================================

  Copyright (c) 2009 - 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================


                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_wcdma_class.h#1 $

when       who     what, where, why
-------------------------------------------------------------------------------- 
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
05/24/12   vss     Initial revision.


===========================================================================*/

#include "rfdevice_intf_cmd.h"
#include "rfdevice_wcdma_class.h"

class wtr2605_wcdma_rx_class: public rfdevice_wcdma_rx_class
{  
  public: 
  wtr2605_wcdma_rx_class(rfwcdma_device_rx_type* instance);
  virtual ~wtr2605_wcdma_rx_class();
  virtual void init();
};


class wtr2605_wcdma_tx_class: public rfdevice_wcdma_tx_class
{
  
  public: 
  wtr2605_wcdma_tx_class(rfwcdma_device_tx_type* instance);
  virtual ~wtr2605_wcdma_tx_class();
  virtual void init();

};
#endif
