
#ifndef WTR2605_GSM_DEVICE_INTF_H
#define WTR2605_GSM_DEVICE_INTF_H
/*! 
  @file
  wtr2605_cdma_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 CDMA driver

  @details
  Contains function prototypes to create WTR2605 CDMA devices and any other
  direct configuration from RFC like setting band/ports, selecting TX LUTs etc

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.

  This file will have to be included by RFC to communicate with WTR2605 and must 
  be shipped.

  @addtogroup WTR2605_CDMA
  @{
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_gsm_device_intf.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
01/04/13   av      DSDA changes
11/29/12   av      Using proper featurization and removing featurization 
                    that is not needed
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
05/25/12   bm      Initial revision merged from
                   rfa2/device/wtr1605/gsm/main/latest/inc/wtr1605_gsm_device_intf.h#2

==============================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include "rfdevice_gsm_intf.h"
#include "rfdevice_cmn_intf.h"

/*! Max number of devices supported by WTR2605 */
#define WTR2605_MAX_GSM_DEVICES 2


/*----------------------------------------------------------------------------*/
rfdevice_gsm_type*
wtr2605_gsm_create_device
(
  rfdevice_cmn_type* common_device
);

/*----------------------------------------------------------------------------*/
void
wtr2605_gsm_set_band_port
(
  rfdevice_gsm_type *gsmdev,
  rfcom_gsm_band_type rfcom_band,
  uint8 rx_band_port,
  uint8 tx_band_port
);

/*----------------------------------------------------------------------------*/
void
wtr2605_gsm_set_rfc_gsm_core_ptr
(
  rfdevice_gsm_type *gsmdev,
  rfc_gsm_core_config_type *rfc_gsm_core_ptr
);


#ifdef __cplusplus
}
#endif

/*! @} */

#endif /* WTR2605_GSM_DEVICE_INTF_H */
