#ifndef WTR2605_TYPEDEF_AG_H
#define WTR2605_TYPEDEF_AG_H
/*
  WARNING: This file is auto-generated.

  Generated at:    0
  Generated using: RFDevice_AutoGenerate.pl
  Generated from:  V066 of the Nucleus SSBI spreadsheet
*/

/*!
  @file
  wtr2605_typedef_ag.h

  @brief
  WTR2605 Data Type Definition autogen header

  @details
  This file is auto-generated and it contains the WTR2605 Data Type Definition SSBI data type to support the 
  interaction with the QUALCOMM WTR2605 chip

  @addtogroup WTR2605 Data Type Definition
  @{
*/

/*=============================================================================

Copyright (c) 2011-2014 by QUALCOMM Technologies Incorporated.  All Rights Reserved.

Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. 
  Government. Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

$Header: //source/qcom/qct/modem/rfdevice/wtr2605/main/1.0.8/common/etc/RFDevice_LIBWTR.pm#1 : 2014/02/13 14:36:54 : coresvc 60============================================================================*/
  
/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_prx_port enum.
*/
typedef enum 
{
  WTR2605_CDMA_PRX_BAND0_PLB1, 
  WTR2605_CDMA_PRX_BAND0_PLB2, 
  WTR2605_CDMA_PRX_BAND10_PLB1, 
  WTR2605_CDMA_PRX_BAND10_PLB2, 
  WTR2605_CDMA_PRX_BAND1_PMB1, 
  WTR2605_CDMA_PRX_BAND1_PMB2, 
  WTR2605_CDMA_PRX_BAND14_PMB1, 
  WTR2605_CDMA_PRX_BAND14_PMB2, 
  WTR2605_CDMA_PRX_BAND4_PMB2, 
  WTR2605_CDMA_PRX_BAND6_PMB1, 
  WTR2605_CDMA_PRX_BAND6_PMB2, 
  WTR2605_CDMA_PRX_BAND15_PMB1, 
  WTR2605_CDMA_PRX_BAND15_PMB2, 
  WTR2605_CDMA_PRX_BAND_NUM, 
  WTR2605_CDMA_PRX_BAND_INVALID, 
} wtr2605_cdma_prx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_tx_port enum.
*/
typedef enum 
{
  WTR2605_CDMA_TX_BAND0_TDA1, 
  WTR2605_CDMA_TX_BAND0_TDA2, 
  WTR2605_CDMA_TX_BAND0_TDA3, 
  WTR2605_CDMA_TX_BAND10_TDA1, 
  WTR2605_CDMA_TX_BAND10_TDA2, 
  WTR2605_CDMA_TX_BAND10_TDA3, 
  WTR2605_CDMA_TX_BAND1_TDA2, 
  WTR2605_CDMA_TX_BAND1_TDA3, 
  WTR2605_CDMA_TX_BAND14_TDA2, 
  WTR2605_CDMA_TX_BAND14_TDA3, 
  WTR2605_CDMA_TX_BAND6_TDA2, 
  WTR2605_CDMA_TX_BAND6_TDA3, 
  WTR2605_CDMA_TX_BAND4_TDA2, 
  WTR2605_CDMA_TX_BAND4_TDA3, 
  WTR2605_CDMA_TX_BAND15_TDA2, 
  WTR2605_CDMA_TX_BAND15_TDA3, 
  WTR2605_CDMA_TX_BAND_NUM, 
  WTR2605_CDMA_TX_BAND_INVALID, 
} wtr2605_cdma_tx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 gnss enum.
*/
typedef enum 
{
  WTR2605_GNSS_M5_LL, 
  WTR2605_GNSS_M5_ELNA, 
  WTR2605_GNSS_M5_HL, 
  WTR2605_COMPASS_MC_HL, 
  WTR2605_COMPASS_MC_ELNA, 
  WTR2605_GNSS_M5ET, 
  WTR2605_GNSS_M5HT, 
  WTR2605_GNSS_MODE_NUM, 
  WTR2605_GNSS_MODE_INVALID, 
} wtr2605_gnss_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 gsm_prx_port enum.
*/
typedef enum 
{
  WTR2605_GSM_PRX_BAND850_PLB1, 
  WTR2605_GSM_PRX_BAND850_PLB2, 
  WTR2605_GSM_PRX_BAND900_PLB1, 
  WTR2605_GSM_PRX_BAND900_PLB2, 
  WTR2605_GSM_PRX_BAND1800_PMB1, 
  WTR2605_GSM_PRX_BAND1800_PMB2, 
  WTR2605_GSM_PRX_BAND1800_PMB3, 
  WTR2605_GSM_PRX_BAND1900_PMB1, 
  WTR2605_GSM_PRX_BAND1900_PMB2, 
  WTR2605_GSM_PRX_BAND1900_PMB3, 
  WTR2605_GSM_PRX_BAND_NUM, 
  WTR2605_GSM_PRX_BAND_INVALID, 
} wtr2605_gsm_prx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 gsm_tx_port enum.
*/
typedef enum 
{
  WTR2605_GSM_TX_BAND850_TDA1, 
  WTR2605_GSM_TX_BAND900_TDA1, 
  WTR2605_GSM_TX_BAND1800_TDA1, 
  WTR2605_GSM_TX_BAND1800_TDA2, 
  WTR2605_GSM_TX_BAND1800_TDA3, 
  WTR2605_GSM_TX_BAND1900_TDA1, 
  WTR2605_GSM_TX_BAND1900_TDA2, 
  WTR2605_GSM_TX_BAND1900_TDA3, 
  WTR2605_GSM_TX_BAND_NUM, 
  WTR2605_GSM_TX_BAND_INVALID, 
} wtr2605_gsm_tx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 tdscdma_prx_port enum.
*/
typedef enum 
{
  WTR2605_TDSCDMA_PRX_BAND34_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND34_PMB3, 
  WTR2605_TDSCDMA_PRX_BAND39_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND39_PMB3, 
  WTR2605_TDSCDMA_PRX_BAND40_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND34B_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND34B_PMB3, 
  WTR2605_TDSCDMA_PRX_BAND39B_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND39B_PMB3, 
  WTR2605_TDSCDMA_PRX_BAND40B_PMB2, 
  WTR2605_TDSCDMA_PRX_BAND_NUM, 
  WTR2605_TDSCDMA_PRX_BAND_INVALID, 
} wtr2605_tdscdma_prx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 tdscdma_tx_port enum.
*/
typedef enum 
{
  WTR2605_TDSCDMA_TX_BAND34_TDA1, 
  WTR2605_TDSCDMA_TX_BAND34_TDA3, 
  WTR2605_TDSCDMA_TX_BAND39_TDA1, 
  WTR2605_TDSCDMA_TX_BAND39_TDA3, 
  WTR2605_TDSCDMA_TX_BAND40_TDA1, 
  WTR2605_TDSCDMA_TX_BAND40_TDA3, 
  WTR2605_TDSCDMA_TX_BAND_NUM, 
  WTR2605_TDSCDMA_TX_BAND_INVALID, 
} wtr2605_tdscdma_tx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_prx_port enum.
*/
typedef enum 
{
  WTR2605_WCDMA_PRX_BAND5_PLB1, 
  WTR2605_WCDMA_PRX_BAND5_PLB2, 
  WTR2605_WCDMA_PRX_BAND6_PLB1, 
  WTR2605_WCDMA_PRX_BAND6_PLB2, 
  WTR2605_WCDMA_PRX_BAND19_PLB1, 
  WTR2605_WCDMA_PRX_BAND19_PLB2, 
  WTR2605_WCDMA_PRX_BAND8_PLB1, 
  WTR2605_WCDMA_PRX_BAND8_PLB2, 
  WTR2605_WCDMA_PRX_BAND1_PMB1, 
  WTR2605_WCDMA_PRX_BAND1_PMB2, 
  WTR2605_WCDMA_PRX_BAND4_PMB1, 
  WTR2605_WCDMA_PRX_BAND4_PMB2, 
  WTR2605_WCDMA_PRX_BAND10_PMB1, 
  WTR2605_WCDMA_PRX_BAND10_PMB2, 
  WTR2605_WCDMA_PRX_BAND2_PMB1, 
  WTR2605_WCDMA_PRX_BAND2_PMB2, 
  WTR2605_WCDMA_PRX_BAND3_PMB2, 
  WTR2605_WCDMA_PRX_BAND9_PMB2, 
  WTR2605_WCDMA_PRX_BAND_NUM, 
  WTR2605_WCDMA_PRX_BAND_INVALID, 
} wtr2605_wcdma_prx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_tx_port enum.
*/
typedef enum 
{
  WTR2605_WCDMA_TX_BAND5_TDA1, 
  WTR2605_WCDMA_TX_BAND5_TDA2, 
  WTR2605_WCDMA_TX_BAND5_TDA3, 
  WTR2605_WCDMA_TX_BAND6_TDA1, 
  WTR2605_WCDMA_TX_BAND6_TDA2, 
  WTR2605_WCDMA_TX_BAND6_TDA3, 
  WTR2605_WCDMA_TX_BAND19_TDA1, 
  WTR2605_WCDMA_TX_BAND19_TDA2, 
  WTR2605_WCDMA_TX_BAND19_TDA3, 
  WTR2605_WCDMA_TX_BAND8_TDA1, 
  WTR2605_WCDMA_TX_BAND8_TDA2, 
  WTR2605_WCDMA_TX_BAND8_TDA3, 
  WTR2605_WCDMA_TX_BAND1_TDA2, 
  WTR2605_WCDMA_TX_BAND1_TDA3, 
  WTR2605_WCDMA_TX_BAND2_TDA2, 
  WTR2605_WCDMA_TX_BAND2_TDA3, 
  WTR2605_WCDMA_TX_BAND3_TDA3, 
  WTR2605_WCDMA_TXWSAW_BAND3_TDA3, 
  WTR2605_WCDMA_TX_BAND9_TDA3, 
  WTR2605_WCDMA_TX_BAND4_TDA2, 
  WTR2605_WCDMA_TX_BAND4_TDA3, 
  WTR2605_WCDMA_TX_BAND10_TDA2, 
  WTR2605_WCDMA_TX_BAND10_TDA3, 
  WTR2605_WCDMA_TX_BAND_NUM, 
  WTR2605_WCDMA_TX_BAND_INVALID, 
} wtr2605_wcdma_tx_port_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 path_assn enum.
*/
typedef enum 
{
  WTR2605_ASSN_RX_PLB1 = 1, 
  WTR2605_ASSN_RX_PLB2, 
  WTR2605_ASSN_RX_PMB1, 
  WTR2605_ASSN_RX_PMB2, 
  WTR2605_ASSN_TX_DA1, 
  WTR2605_ASSN_TX_DA2, 
  WTR2605_ASSN_TX_DA3, 
  WTR2605_ASSN_ANA, 
  WTR2605_ASSN_RX_PMB3, 
} wtr2605_path_assn_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 reg struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 reg; 
  uint8 rev; 
  uint8 process; 
  uint16 path_assn; 
  uint8 instruction; 
} wtr2605_reg_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 ssbi struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 reg; 
} wtr2605_ssbi_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 ag_list struct.
*/
typedef struct 
{
  wtr2605_path_assn_data_type path_assn; 
  const uint16 autoa_start; 
  const wtr2605_reg_data_type *ag_reg_tbl; 
  const uint8 *ag_data_tbl; 
} wtr2605_ag_list_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 external_ref enum.
*/
typedef enum 
{
  WTR2605_NON_EXPLICIT_WRITE_AUTOB_REF = 0, 
  WTR2605_NON_EXPLICIT_WRITE_AUTOA_REF, 
  WTR2605_EXPLICIT_WRITE_REF, 
  WTR2605_FIRST_RC_CMN_REF, 
  WTR2605_AUTOB_RC_CMN_REF, 
  WTR2605_AUTOA_RC_CMN_REF, 
  WTR2605_FIRST_RC_BSP_REF, 
  WTR2605_AUTOB_RC_BSP_REF, 
  WTR2605_AUTOA_RC_BSP_REF, 
  WTR2605_PRX_GAIN_G0_RC_REF, 
  WTR2605_PRX_GAIN_G1_RC_REF, 
  WTR2605_PRX_GAIN_G2_RC_REF, 
  WTR2605_NPLER_REF, 
  WTR2605_PLL_REF, 
  WTR2605_LAST_REF, 
  WTR2605_AUTOAB_TRANSITION_REF, 
  WTR2605_DELAY_REF, 
  WTR2605_PDET_DCCAL_REF, 
} wtr2605_external_ref_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_bw enum.
*/
typedef enum 
{
  WTR2605_CDMA_BW_1X, 
  WTR2605_CDMA_BW_3X, 
  WTR2605_CDMA_BW_5X, 
  WTR2605_CDMA_BW_NUM, 
  WTR2605_CDMA_BW_INVALID, 
} wtr2605_cdma_bw_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_bw enum.
*/
typedef enum 
{
  WTR2605_WCDMA_BW_1X, 
  WTR2605_WCDMA_BW_2X, 
  WTR2605_WCDMA_BW_NUM, 
  WTR2605_WCDMA_BW_INVALID, 
} wtr2605_wcdma_bw_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 rsb_coefficient enum.
*/
typedef enum 
{
  WTR2605_RSB_COEFFICIENT_A, 
  WTR2605_RSB_COEFFICIENT_B, 
  WTR2605_RSB_COEFFICIENTS_NUM, 
} wtr2605_rsb_coefficient_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 lut_pwr enum.
*/
typedef enum 
{
  WTR2605_HP_LUT_TYPE, 
  WTR2605_MP_LUT_TYPE, 
  WTR2605_LP_LUT_TYPE, 
  WTR2605_LUT_PWR_NUM, 
  WTR2605_LUT_PWR_INVALID, 
} wtr2605_lut_pwr_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 lut_version enum.
*/
typedef enum 
{
  WTR2605_LUT_V1_TYPE, 
  WTR2605_LUT_V2_TYPE, 
  WTR2605_LUT_VER_NUM, 
  WTR2605_LUT_VER_INVALID, 
} wtr2605_lut_version_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 temperature enum.
*/
typedef enum 
{
  WTR2605_TEMPERATURE_LOW, 
  WTR2605_TEMPERATURE_ROOM, 
  WTR2605_TEMPERATURE_HIGH, 
  WTR2605_TEMPERATURE_NUM, 
  WTR2605_TEMPERATURE_INVALID, 
} wtr2605_temperature_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 dpd enum.
*/
typedef enum 
{
  WTR2605_DPD_DISABLE, 
  WTR2605_DPD_ENABLED, 
  WTR2605_DPD_NUM, 
  WTR2605_DPD_INVALID, 
} wtr2605_dpd_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_prx_mode_control struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 data[WTR2605_WCDMA_PRX_BAND_NUM]; 
} wtr2605_wcdma_prx_mode_control_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_tx_mode_control struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 data[WTR2605_WCDMA_TX_BAND_NUM]; 
} wtr2605_wcdma_tx_mode_control_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_rx_gain_list enum.
*/
typedef enum 
{
  WTR2605_CDMA_RX_G0, 
  WTR2605_CDMA_RX_G1, 
  WTR2605_CDMA_RX_G2, 
  WTR2605_CDMA_RX_G3, 
  WTR2605_CDMA_RX_G4, 
  WTR2605_CDMA_RX_GAIN_NUM, 
  WTR2605_CDMA_RX_GAIN_INVALID, 
} wtr2605_cdma_rx_gain_list_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_llin_rx_gain struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 gain_word[WTR2605_CDMA_BW_NUM][WTR2605_CDMA_RX_GAIN_NUM]; 
} wtr2605_cdma_llin_rx_gain_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 cdma_hlin_rx_gain struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 gain_word[WTR2605_CDMA_RX_GAIN_NUM]; 
} wtr2605_cdma_hlin_rx_gain_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_rx_gain_list enum.
*/
typedef enum 
{
  WTR2605_WCDMA_RX_G0, 
  WTR2605_WCDMA_RX_G1, 
  WTR2605_WCDMA_RX_G2, 
  WTR2605_WCDMA_RX_G3, 
  WTR2605_WCDMA_RX_G4, 
  WTR2605_WCDMA_RX_GAIN_NUM, 
  WTR2605_WCDMA_RX_GAIN_INVALID, 
} wtr2605_wcdma_rx_gain_list_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 wcdma_rx_gain struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 gain_word[WTR2605_WCDMA_BW_NUM][WTR2605_WCDMA_RX_GAIN_NUM]; 
} wtr2605_wcdma_rx_gain_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 gsm_rx_gain_list enum.
*/
typedef enum 
{
  WTR2605_GSM_RX_G0, 
  WTR2605_GSM_RX_G1, 
  WTR2605_GSM_RX_G2, 
  WTR2605_GSM_RX_GAIN_NUM, 
  WTR2605_GSM_RX_GAIN_INVALID, 
} wtr2605_gsm_rx_gain_list_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 gsm_rx_gain struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 gain_word[WTR2605_GSM_RX_GAIN_NUM]; 
} wtr2605_gsm_rx_gain_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 tdscdma_rx_gain_list enum.
*/
typedef enum 
{
  WTR2605_TDSCDMA_RX_G0, 
  WTR2605_TDSCDMA_RX_G1, 
  WTR2605_TDSCDMA_RX_G2, 
  WTR2605_TDSCDMA_RX_G3, 
  WTR2605_TDSCDMA_RX_GAIN_NUM, 
  WTR2605_TDSCDMA_RX_GAIN_INVALID, 
} wtr2605_tdscdma_rx_gain_list_data_type;

/*----------------------------------------------------------------------------*/
/*!
   It defines the WTR2605 tdscdma_rx_gain struct.
*/
typedef struct 
{
  uint8 bus; 
  uint8 page; 
  uint8 addr; 
  uint8 gain_word[WTR2605_TDSCDMA_RX_GAIN_NUM]; 
} wtr2605_tdscdma_rx_gain_data_type;


#endif /* WTR2605_TYPEDEF_AG_H */

/*! @} */ 
