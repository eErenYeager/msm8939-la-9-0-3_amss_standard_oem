#ifndef WTR2605_RXTX_COMMON_CLASS_H
#define WTR2605_RXTX_COMMON_CLASS_H
/*!
  @file wtr2605_rxtx_common_class.h 

  @brief
  This is the rf device factory which creates all devices.



*/

/*===========================================================================

  Copyright (c) 2009 - 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================


                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/rfdevice_wtr2605/api/wtr2605_rxtx_common_class.h#1 $

when       who     what, where, why
--------------------------------------------------------------------------------
08/15/14   sd      Added tech argument to FBRX Enable API
07/25/14   sd      Added frequency arg to FBRX Enable API to handle band
                   dependent sequences
04/20/14   sma     Added stg, rsb fbrx and dc cal apis
04/07/14   sma     Added get_tx_lut_scripts and dummy_trigger_script, fbrx 
05/12/13   ry      Added destructor 
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
05/24/12   vss     Initial revision.


===========================================================================*/

#include "rfdevice_intf_cmd.h"
#include "rfdevice_rxtx_common_class.h"

class wtr2605_rxtx_common_class: public rfdevice_rxtx_common_class
{
  
  public: 
  wtr2605_rxtx_common_class(rfdevice_bus_info_type wtr2605_ssbi_info,
                            rfdevice_cmn_int_dev_cal_data_type cal_data);

  boolean get_tx_lut_scripts( rfm_mode_enum_type tech,
                                rfcom_band_type_u band,
                                uint8 pa_state,
                                uint8 rgi,
                                rf_device_execution_type execution_type,
                                rfdevice_tx_lut_data_type* lut_data );


  boolean fbrx_setup(rf_device_execution_type execution_type,
                     rf_buffer_intf* fbrx_setup_script,
                     uint8* fbrx_setup_t_us);

  boolean get_tx_dummy_trigger_script(rf_buffer_intf* dummy_script);

  boolean fbrx_enable (uint8 fbrx_gain_state,
                       rf_device_execution_type execution_type,
                       rf_buffer_intf* fbrx_enable_script,
                       boolean add_delay_before_enable_writes,
                       uint32 tx_freq_khz,
                       rfm_mode_enum_type rfm_mode);

  boolean fbrx_disable (rf_device_execution_type execution_type,
                        rf_buffer_intf* fbrx_disable_script);

  boolean read_therm( uint16* therm_read );


  boolean stg_enable( uint64 stg_freq, 
                      rf_device_execution_type exec_type,
                      rf_buffer_intf* script_buffer );

  boolean stg_disable( rf_device_execution_type exec_type,
                       rf_buffer_intf* script_buffer );

  boolean fbrx_dc_cal_enable( rf_device_execution_type exec_type,
                              rf_buffer_intf* script_buffer );

  boolean fbrx_dc_cal_disable( rf_device_execution_type exec_type,
                               rf_buffer_intf* script_buffer );

  boolean fbrx_get_rsb_coeff( rfm_mode_enum_type tech,
                              rfcom_band_type_u band,
                              rfdevice_fbrx_rsb_data_type* rsb_coeff );
  virtual void init();
  ~wtr2605_rxtx_common_class();
};

#endif
