#ifndef WTR2605_TDSCDMA_DEVICE_INTF_H
#define WTR2605_TDSCDMA_DEVICE_INTF_H
/*! 
  @file
  wtr2605_tdscdma_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR2605 TDSCDMA driver

  @details
  Contains function prototypes to create WTR2605 TDSCDMA devices and any other
  direct configuration from RFC like setting band/ports, selecting TX LUTs etc

  This header is wtr2605 specific and hence must be included by WTR2605 RF cards 
  only.  This file will have to be included by RFC to communicate with WTR2605
  and must be shipped.

  @addtogroup WTR2605_TDSCDMA
  @{
*/

/*==============================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: 

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
08/22/12   cd      Merged with tips of WTR1605 to pick up device interface changes
05/05/12   bm      Initial Revision merged from
                   rfa2/device/wtr1605/tdscdma/main/latest/inc/wtr1605_tdscdma_device_intf.h #1

==============================================================================*/

#include "rfdevice_tdscdma_intf.h"
#include "rfdevice_cmn_intf.h"
#include "rfcom.h" 

/*! Max number of devices supported by WTR2605 */
#define WTR2605_MAX_TDSCDMA_DEVICES 1

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Structure contains pointers to the various TDSCDMA devices in a WTR2605 device

  @details
  WTR2605 contains 2 TDSCDMA Receive paths: RX0 used as a primary receiver and
  RX1 used for Diversity and SHDR and 1 Transmitter. This structure puts 
  pointers to these TDSCDMA devices together for a particular single WTR2605 
  device.

  A structure of this type is returned to RFC when it creates a WTR2605 TDSCDMA 
  device thus giving RFC access to all TDSCDMA devices in the new device that
  has been created.
*/
typedef struct
{
  /*! Primary TDSCDMA receive device (on physical path 0) of WTR2605 */
  rftdscdma_device_rx_type* rx0_device;

  /*! TDSCDMA transmit device (on physical path 0) of WTR2605 */
  rftdscdma_device_tx_type* tx_device;

} wtr2605_tdscdma_devices_type;

/*----------------------------------------------------------------------------*/
wtr2605_tdscdma_devices_type wtr2605_tdscdma_create_device
(
  rfdevice_cmn_type* common_device
);

/*! @} */

#endif /* WTR2605_TDSCDMA_DEVICE_INTF_H */



