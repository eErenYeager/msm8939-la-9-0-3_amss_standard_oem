/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

    C D M A    R E C E I V E   M E S S A G E   D E L I V E R Y   L A Y E R

GENERAL DESCRIPTION
  This module delivers L3 messages to the calling entity (MC).

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2001 through 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*===========================================================================

                           Edit History

$PVCSPath: O:/src/asw/COMMON/vcs/rxcmsg.h_v   1.0   06 Dec 2001 12:39:02   ldefauw  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/mux/src/rxcmsg.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/15/10   jtm     Initial SU API split for CP.
11/05/10   jtm     Added more _i.h includes for Modem 1x SU API support.
03/26/09   jtm     Removed cai.h include. It is not needed.
11/19/08   jtm     Split various header files into internal and external
                   declarations for Common Modem Interface.
08/11/05   bn      Added logic to relay the page match event to MC.
12/06/01   lad     Created file.

===========================================================================*/

//common
#include "comdef.h"
#include "customer.h"

#include "1x_variation.h"
#ifdef FEATURE_IS2000_REL_A

//cp
#include "cai_v.h"
#include "caix.h"
#include "caix_i.h"

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

FUNCTION RXCMSG_DELIVER

DESCRIPTION
  This function delivers a message to the mcc subtask from the rxtx task.

DEPENDENCIES
  None.

RETURN VALUE
  Boolean indicating success.

SIDE EFFECTS
  None.

===========================================================================*/
boolean rxcmsg_deliver (
  caix_chan_type chan,
  cai_rx_msg_type *ext_msg_ptr,
  unsigned int length,
  qword *frame_num,
  boolean page_match
);
#endif /* FEATURE_IS2000_REL_A */
