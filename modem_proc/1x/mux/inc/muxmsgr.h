#ifndef MUXMSGR_H
#define MUXMSGR_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

          M U X  M E S S A G E  R O U T E R  H E A D E R   F I L E


GENERAL DESCRIPTION
      This file contains external interfaces and declarations for the
      mux message router module.

EXPORTED FUNCTIONS:
      muxmsgr_init
      muxmsgr_register_msg
      muxmsgr_deregister_msg
      muxmsgr_register_1xdemod_msgs
      muxmsgr_deregister_1xdemod_msgs
      muxmsgr_process_rcv_msg
      muxmsgr_send_msg
      muxmsgr_stop

INITIALIZATION AND SEQUENCING REQUIREMENTS
      Must call muxmsgr_init() before any other functions in this module
      are invoked.

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    Copyright (c) 2009 - 2011
                  by Qualcomm Technologies Incorporated.  All Rights Reserved.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/mux/inc/muxmsgr.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/18/11   adw     Mainline 1x message router support.
08/05/10   vks     Move TX, RX MSGR module definitions from muxmsgr.h to
                   onex_msgr.h
05/17/10   vks     Replace MAX_NUM_TASKS with NUM_MUX_TASKS to fix lint error.
11/12/09   vks     Add MSGR/Q6 framework support.
09/09/09   vks     Initial implementation.

==========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Common */
#include "comdef.h"
#include "customer.h"
#include "modem_1x_defs.h"


/* FW */
#include "cdmafw_msg.h"

/* Message Router Includes*/
#include "msgr_umid.h"
#include "msgr.h"



/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/* Enum for MUX TASKS */
typedef enum
{
  TX_TASK        = 0,
  RX_TASK        = 1,
  NUM_MUX_TASKS  = 2,
} muxmsgr_tasks_enum_type;



/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

FUNCTION       MUXMSGR_INIT

DESCRIPTION    This function initializes the receive queue and registers the
               1x MUX Task client with the message router.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_init
(
  muxmsgr_tasks_enum_type    task                /* MUX Task               */
);

/*===========================================================================

FUNCTION       MUXMSGR_REGISTER_MSG

DESCRIPTION    This function registers a message that MUX is interested in
               collecting from the message router.

DEPENDENCIES   1X MUX client setup with the MSGR must have been completed.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_register_msg
(
  muxmsgr_tasks_enum_type    task,               /* MUX Task               */
  msgr_umid_type             umid                /* UMID of the message    */
);

/*===========================================================================

FUNCTION       MUXMSGR_DEREGISTER_MSG

DESCRIPTION    This function deregisters a message that MUX is no longer
               interested in receving from the message router.

DEPENDENCIES   1X MUX client setup with the MSGR must have been completed.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_deregister_msg
(
  muxmsgr_tasks_enum_type    task,               /* MUX Task               */
  msgr_umid_type             umid                /* UMID of the message    */
);

/*===========================================================================

FUNCTION       MUXMSGR_REGISTER_1XDEMOD_MSGS

DESCRIPTION    This function registers the demod messages that mux is
               interested in receiving from 1X DEMOD FW (cdmafw_msg.h).

DEPENDENCIES   Must be called after 1X DEMOD APP is successfully enabled.

RETURN VALUE   None.

SIDE EFFECTS   MUX starts receiving the registered messages from 1X DEMOD FW.

===========================================================================*/
extern void muxmsgr_register_1xdemod_msgs
(
  muxmsgr_tasks_enum_type    task                /* MUX Task               */
);

/*===========================================================================

FUNCTION       MUXMSGR_DEREGISTER_1XDEMOD_MSGS

DESCRIPTION    This function de-registers the demod messages that mux is no
               longer interested in receiving from 1X DEMOD FW.

DEPENDENCIES   Preferable to call after the 1X DEMOD APP has been disabled.

RETURN VALUE   None.

SIDE EFFECTS   MUX stops receiving the registered messages from 1X DEMOD FW.

===========================================================================*/
extern void muxmsgr_deregister_1xdemod_msgs
(
  muxmsgr_tasks_enum_type    task                /* MUX Task               */
);

/*===========================================================================

FUNCTION       MUXMSGR_PROCESS_RCV_MSG

DESCRIPTION    This function processes the received message.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_process_rcv_msg
(
  muxmsgr_tasks_enum_type    task                /* MUX Task               */
);

/*===========================================================================

FUNCTION       MUXMSGR_SEND_MSG

DESCRIPTION    This function initializes the header and sends the message to
               the message router.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_send_msg
(
  muxmsgr_tasks_enum_type    task,               /* MUX Task               */
  msgr_hdr_struct_type*      msg_hdr_ptr,        /* Pointer to MSG Header  */
  msgr_umid_type             umid,               /* UMID of the message    */
  int32                      size                /* Size of the message    */
);

/*===========================================================================

FUNCTION       MUXMSGR_STOP

DESCRIPTION    This function de-registers the MUX client from the message
               router. We would no longer be able to send/receive messages.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void muxmsgr_stop
(
  muxmsgr_tasks_enum_type    task              /* MUX Task                 */
);

#endif /* MUXMSGR_H */
