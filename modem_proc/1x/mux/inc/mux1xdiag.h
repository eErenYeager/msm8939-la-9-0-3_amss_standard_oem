#ifndef MUX1XDIAG_H
#define MUX1XDIAG_H
/*==========================================================================

                      1x MUX DIAGnostic services header

  Description:  Interfaces for external subsystems

Copyright (c) 2003 through 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/* <EJECT> */
/*===========================================================================

                            Edit History

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/mux/inc/mux1xdiag.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/03/10   trc     Rewrote this file to only export external interfaces
04/01/09   jtm     Added customer.h
10/21/03   bn      Added support for retrieving paging statistic counters via DIAG
05/22/03   vlc     Added extern prototype for mux1xdiag_init().
04/15/03   bn      Created new file for DIAG MUX1X sub-system access.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
//common
#include "comdef.h"
#include "customer.h"

// NOT WANTED HERE, but cdma2kdiag.c and mclog.c depend upon this #include
// being done for them indirectly via this file.
#include "diagcmd.h"
// Get rid of this at the earliest convenience!!!

/*===========================================================================

                EXPORTED FUNCTIONS

===========================================================================*/

/*--------------------------------------------------------------------------
  MUX1XDIAG_INIT() - Perform initial registration of DIAG packets
--------------------------------------------------------------------------*/
extern void mux1xdiag_init (void);


/*===========================================================================

              EXPORTED DEFINITIONS AND DECLARATIONS

This section contains definitions for constants, macros, types, variables
and other items exported by this module.

===========================================================================*/

/* Data structure for Frame Error Rate
*/
typedef struct
{

  uint16    meas_frames;
    /* Indicate # of measured frames depended on PWR_REP_FRAMES */

  uint16    err_frames;
    /* Indicate # of error frames during measured frames */

} mux1xdiag_fer_s_type;

#endif /* MUX1XDIAG_H */
