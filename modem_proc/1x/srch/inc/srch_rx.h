#ifndef SRCH_RX_H
#define SRCH_RX_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             S R C H  _ R X  ---  H E A D E R   F I L E

GENERAL DESCRIPTION
  This module contains header information pertaining to srch_rx.

EXTERNALIZED FUNCTIONS



INITIALIZATION AND SEQUENCING REQUIREMENTS

      Copyright (c) 2005 - 2014 by Qualcomm Technologies Incorporated.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/srch/inc/srch_rx.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/01/14   ab      Changes to update TRM with correct ASD sleep information 
07/01/14   ab      Add the PCH decode failure check for IDLE ASD
06/17/14   srk     Decouple Dynamic IDLE DIV from DIV NV.
06/02/14   srk     Fix IDLE DIV in QPCH OFFLINE timeline and SOOS cleanup.
05/30/14   srk     IDLE ASD fixes.
05/05/14   pk      Added API to check if primary rf tune is supported on a
                   particular band/chan.
03/25/14   bph     DSDS/DSDA Feature bring up. traffic+traffic
03/13/14   bph     Add feature gaurd for ASDiv DSDS/DSDA code
01/16/14   pk      Added API to check if rf tune is supported on a particular
                   band/chan.
01/15/14   bph     IDLE AS-DIV support
11/18/13   pap     To provide an API to provide the open-loop turnaround const.
10/21/13   srk     Remove unused APIs.
09/10/13   as      Reverting previous checkin to satisfy AsDIV feature
09/06/13   srk     Remove unused APIs.
07/23/13   srk     Replace RF API calls with mux API.
06/19/13   ab      New API added to return the RF device in use
04/16/13   bph     Avoid calling RF API w/ untuned RF device
02/19/13   cjb     Changes to support LTE->1x along with new LTE CA feature.
11/07/12   cjb     Updated RF device for SV-DIV operation.
10/31/12   bb      Changes to define srch_rx_get_owned_device()
10/19/12   srk     Remove unused function prototype.
10/15/12   dkb     Add mechanism to dynamically enable on the fly zz2_2 support
10/02/12   adw/dkb Add on the fly zz2_2 support.
09/19/12   vks     Div interlock support no longer required.
09/06/12   vks     Set wakeup chain to RX_ANY on chain denial for best possible
08/16/12   ab      Changes for Removing RX Manual Transition during Hard HO.
06/18/12   jj      Add wrapper function for rfm_cdma_get_sub_class,
                   rfm_cdma_is_band_chan_supported.
06/12/12   adw     Allow initial desired device to be specified in ZZ2_2 reset.
05/23/12   vks     Add wrapper function for rfm_get_band_mask.
05/15/12   srk     Removed redudant functions to return tx power.
04/13/12   mca     Added srch_rx_get_last_home_rx_agc()
03/01/12   sst     Added srch_rx_get_rin_enable
02/16/12   srk     Replaced FEATURE_LTE_TO_1X by FEATURE_MODEM_1X_IRAT_LTO1X.
02/10/12   adw     Add APIs to get RF prep and exec overheads separately.
02/01/12   sst     Correct srch_rx_rin_div_ctrl prototype
01/27/12   cjb/vks Use SRCH_RF_RECEIVER_4 for second chain in SV mode and
                   SRCH_RF_RECEIVER_SV_DIV for diversity in SV mode.
                   Remove usage of SRCH_RF_RECEIVER_DUAL (no longer valid).
01/24/12   sst     Implementation of RIN
01/23/12   srk     Feature Cleanup
01/11/12   srk     Feature Cleanup.
12/18/11   adw     Add function to set fing carrier and demod.
12/15/11   vks     Add support to recheck div interlock when HDR/1x retunes
                   to different band.
11/28/11   sst/jtm Updates for Diversity in SV mode
11/15/11   vks     Add api to set reserve chain.
11/10/11   jtm     Integrate ZZ2_2 support.
10/31/11   adw     Integrate TRAM support.
10/31/11   jtm     Feature clean up.
10/03/11   adw/cjb Fix dynamic switching for common chain override api.
09/02/11   sk      Rebased the code to the NikeL modem package by mainlining
                   FEATURE_MODEM_1X_NIKEL.
08/18/11   vks     Clean up references to sample server which does not exist
                   anymore (since genesis).
                   Clean up references to fast agc loop which does not exist
                   anymore (since poseidon).
08/17/11   vks     Remove references to sample server in SRCH RX module (
                   sample server is not present since 9k).
08/08/11   cjb     Added IRAT measurement support for NikeL.
                   Mainline FEATURE_ONEX_USES_RF_SCRIPTS_FOR_IRAT.
08/05/11   vks     Fix compiler issues.
07/12/11   vks/bb  Add support for SHDR functionality.
07/11/11   vks     Mainline FEATURE_SRCH_RX_STM and FEATURE_SRCH_RX_SVDO.
06/30/11   vks     Fix MOB compiler error.
06/28/11   jtm     Fixed SRCH_RF_MAX_TX_DEVICES definition.
06/22/11   vks     Add an api to check if srch rx is in sv mode.
06/22/11   vks     Add support for srch_rx_owns_other_chain().
06/20/11   vks     Initial set of changes to get RX DIV working with RX STM.
06/16/11   adw     Added execute wakeup interface.
06/03/11   adw     SV support.
05/05/11   adw     Fix compilation errors when lto1x is enabled on NikeL.
04/27/11   adw     Initial SV support.
04/25/11   cjb     Added function to return whether sleep should be disabled,
                   while we are in second_chain_test mode.
04/15/11   pk      Added support for SRCH_RX_STATUS
04/12/11   bb      Changes to define srch_rx_update_acq_band_info() function
04/12/11   vks     Add a notification callback for releasing owned chains.
03/16/11   mca     Support to store the wakeup information (agc, device, etc)
02/25/11   cjb     Fixed the O-freq scan limitation when DRx supports one band.
02/17/11   cjb     Changes needed for "DRx supports only one band".
02/25/11   vks     Fix compiler issue arising out of undefining 1x supports
                   rf feature.
02/11/11   sst     Add pass through functions for script based tuning
02/14/11   vks     First set of changes to get SRCH RX STM working.
01/27/11   vks     Add wrapper function for vbatt_read in srch_rx module.
01/26/11   vks     Featurize RF interface with FEATURE_MODEM_1X_SUPPORTS_RF.
                   Add wrapper functions to the rf functions that other modules
                   in SRCH are calling.
01/21/11   vks     Replace FEATURE_SRCH_RF_SVDO_API with FEATURE_1X_RF_SVDO_API
11/30/10   cjb     Second_Chain_Test [Option-2] implementation.
11/01/10   jtm     Modem 1x SU API changes.
07/20/10   bb      Code changes to support RF measurement scripts
                   for IRAT
06/24/10   bb      Code changes to fix the page fault issue
05/28/10   adw     Eliminated discrepancies between 1x and RF device type enums.
04/23/10   adw     Removed deprecated spectral inversion API.
04/12/10   sst     Added srch_rx_mdsp_pause() api
01/26/10   adw     Added srch_rx_set_sample_server_mode().
11/27/09   bb      Code changes to support "LTE to 1X IRAT" feature
                   implementation
10/28/09   sst     Update to RX_STM to work with legacy RF API
10/21/09   adw     Removed _chain_assn() as it should not be exported.
10/02/09   sst     TRMv3 & SVDO RF development (featurized)
06/10/09   sst     Initial changes to make trm v3 compile
04/24/09   adw     Changed srch_rx_enter_mode() paramater type.
04/06/09   mca     Changes for the RF SVDO API
04/01/09   adw     Categorized included header files and standardized format.
03/25/09   adw     Include modem_1x_defs.h for 1x specific modem defines.
03/03/09   adw     Lint fixes.
02/10/09   adw     Merge from 2h08 branch, based on VU_MODEM_1X_SRCH.13.00.01
01/30/09   sst     Added srch_rx_release_owned_chains()
12/01/08   adw     Commented out _v.h includes from subsystems outside 1x.
11/19/08   adw     Merge from main/latest, based on VU_MODEM_1X_SRCH.12.00.24
11/10/08   mca     Added new RF warmup API
10/27/08   aps     Removed featurization of srch_rx_mdsp_chain_assn()
09/08/08   aps     srch_rx_enter_mode() api change
08/18/08   adw     Added SINGLE_ANTENNA to featurize alt chain for ULC.
                   Added FEATURE_SRCH_HAS_NO_TRM to remove TRM for ULC.
06/23/08   aps     FTS - support for band configurability
06/20/08   mca     Merge from //depot
05/01/08   aps     Added parameter to srch_rx_exchange_devices() api
03/17/08   pa      Pass on origination pending to srch_rx_request_both_chains()
03/13/08   sst     Modify FTS version to be determined at runtime
02/22/08   aps     FTS3 - added srch_rx_request_and_notify_last_failed()
02/11/08   aps     Full Time SHDR 3/4 changes
04/27/07   aps     Wrapper function for SRCH_RX_MDSP_CHAIN_ASSN()
07/17/06   aps     Fix for rf sub packet being wrongly built in diversity
                   mode.
05/23/06   rkc     Add bypass_rtc_sync parameter to srch_rx_init_and_tune()
04/09/06   grl     Converted TRM_RX_BEST_POSSIBLE to TRM_RX_ANY if not enough
                   time from reservation to request.
04/04/06   awj     Added translation for TRM_RX_BEST and TRM_RX_BEST_POSSIBLE
02/07/06   bt      Added srch_rx_prepare_to_sleep.
02/02/06   awj     Added srch_rx_chain_is_ready
10/25/05   awj     Added default chain selection NV support
10/18/05   awj     Removed deprecated type
10/17/05   kwo     Lint Cleanup
09/20/05   bt      Use last valid AGC on home band/channel for SOOS and added
                   srch_rx_get_home_rx_agc.
08/29/05   kwo     Added srch_rx_complete_tune()
08/19/05   bt      Added srch_rx_bypass_rtc_sync.
08/19/05   ejv     Use srch_rx_chan_type for chan references.
08/09/05   grl     Added SOOS algorithm support.
07/19/05   grl     Simplified RF chain initialization API.
06/22/05   sfm     Added several new functions and updated function headers
06/09/05   sfm     Added srch_rx_change_priority()
06/07/05   sfm     removed srch_rx_tune_wait_cb()
06/03/05   sfm     srch_rx naming changes
06/01/05   grl     Added functions to get the current band and channel.
05/31/05   rng     Merged from SHDR Sandbox.
01/05/05   bt      Implementation, first cut.
===========================================================================*/

/*===========================================================================

                        INCLUDE FILES FOR MODULE

===========================================================================*/

/* Common */
#include "1x_variation.h"
#include "comdef.h"
#include "customer.h"
#include "modem_1x_defs.h"

/* Srch */
#include "srch_genlog.h"
#include "srch_genlog_i.h"
#include "srch_nv.h"
#include "srch_rx_t.h"
#include "srch_rx_t_i.h"

/* Other */
#include "lm_types.h"
#include "timetick.h"
#include "trm.h"

/* Feature Dependent */

#ifdef FEATURE_MODEM_1X_SUPPORTS_RF

#include "rfm_1x.h"

#ifdef FEATURE_MODEM_1X_IRAT_LTO1X
#include "rfm_meas.h"
#include "rfmeas_lte.h"
#include "rfmeas_types.h"
#endif /* FEATURE_MODEM_1X_IRAT_LTO1X */

#include "rfm_device_types.h"

#endif /* FEATURE_MODEM_1X_SUPPORTS_RF */


/*===========================================================================

                  DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

/*-------------------------------------------------------------------------
      Typedefs
-------------------------------------------------------------------------*/

/* Callback function definition for RF granted events. */
typedef void (*srch_rx_granted_cb_t)( void );

/* Callback function definition for tune completions. */
typedef void (*srch_rx_tune_cb_t)( void );

/* Carrier support */
typedef enum
{
   SRCH_CARRIER_0 = SRCH4_CARRIER_0,
   SRCH_CARRIER_1 = SRCH4_CARRIER_1,
   SRCH_CARRIER_2 = SRCH4_CARRIER_2,
   SRCH_CARRIER_3 = SRCH4_CARRIER_3
}
rx_sm_carrier_enum_t;

#ifdef FEATURE_MODEM_1X_SUPPORTS_RF

/* This is emulating the rf device type provided by rfcom_device_enum_type */
typedef enum
{
  SRCH_RF_TRANSCEIVER_0 = RFM_DEVICE_0,          /* Primary Radio transceiver */
  SRCH_RF_RECEIVER_1    = RFM_DEVICE_1,          /* Secondary Receiver        */
  SRCH_RF_TRANSCEIVER_1 = RFM_DEVICE_2,          /* 2nd-ary Radio transceiver */
  SRCH_RF_RECEIVER_4    = RFM_DEVICE_3,          /* Secondary Receiver Dev2   */
  SRCH_RF_RECEIVER_DIV  = RFM_MAX_DEVICES +1,    /* Recieve Diversity         */
  SRCH_RF_RECEIVER_SV_DIV,                       /* SV Recieve Diversity      */
  SRCH_RF_MAX_DEVICES                            /* Max devices               */
}
rx_sm_device_enum_t;

typedef enum
{
  SRCH_RF_TX_0 = RFM_DEVICE_0,                   /* Primary Radio transceiver */
  SRCH_RF_TX_1 = RFM_DEVICE_2,                   /* 2nd-ary Radio transceiver */
  SRCH_RF_MAX_TX_DEVICES = RFM_MAX_DEVICES
}
rx_sm_tx_device_enum_t;

#else /* FEATURE_MODEM_1X_SUPPORTS_RF */

/* This is emulating the rf device type provided by rfcom_device_enum_type */
typedef enum
{
  SRCH_RF_TRANSCEIVER_0,  /* Primary Radio transceiver */
  SRCH_RF_RECEIVER_1,     /* Secondary Receiver        */
  SRCH_RF_TRANSCEIVER_1,  /* 2nd-ary Radio transceiver */
  SRCH_RF_RECEIVER_4,     /* Secondary Receiver Dev2   */
  SRCH_RF_RECEIVER_DIV,   /* Recieve Diversity         */
  SRCH_RF_RECEIVER_SV_DIV,/* SV Recieve Diversity      */
  SRCH_RF_MAX_DEVICES     /* Max devices               */
}
rx_sm_device_enum_t;

typedef enum
{
  SRCH_RF_TX_0,                   /* Primary Radio transceiver */
  SRCH_RF_TX_1,                   /* 2nd-ary Radio transceiver */
  SRCH_RF_MAX_TX_DEVICES
}
rx_sm_tx_device_enum_t;

#endif /* FEATURE_MODEM_1X_SUPPORTS_RF */

/* Mapping to maintain backward compatibility with non RX_STM code */
#define srch_rx_device_enum_type     rx_sm_device_enum_t
#define srch_rx_tx_device_enum_type  rx_sm_tx_device_enum_t
#define srch_rx_carrier_enum_type    rx_sm_carrier_enum_t

typedef struct
{
  boolean wakeup_seen; /* marks that we've seen at least one wakeup */
  int8    rx0_agc;     /* receive power (chain 0) */
  int8    rx1_agc;     /* receive power (chain 1) */
  int16   tx_pwr;      /* transmit power */
  rx_sm_device_enum_t dev; /* which antenna collected the data */
} srch_rx_wakeup_info_type;

/*-------------------------------------------------------------------------
      Macros
-------------------------------------------------------------------------*/
#define SRCH_RX_SETUP_MSG_CLT( rx_client, command )                     \
  {                                                                     \
    rx_sm_req_t *rx_cmd_payload;                                        \
                                                                        \
    if ( rx_client == RX_CLIENT_1X_PRI)                                 \
    {                                                                   \
      /* grab a outgoing message buffer for srch rx */                  \
      rx_cmd_payload = (rx_sm_req_t *)                                  \
        STM_ALLOC_MSG_BUF( &SRCH_RX_SM_P,                               \
                           &SRCH_COMMON_GROUP,                          \
                           command,                                     \
                           sizeof(rx_sm_req_t));                        \
    }                                                                   \
    else                                                                \
    {                                                                   \
      /* grab a outgoing message buffer for srch rx */                  \
      rx_cmd_payload = (rx_sm_req_t *)                                  \
        STM_ALLOC_MSG_BUF( &SRCH_RX_SM_S,                               \
                           &SRCH_COMMON_GROUP,                          \
                           command,                                     \
                           sizeof(rx_sm_req_t));                        \
    }                                                                   \
                                                                        \
    srch_rx_init_stm_cmd( rx_cmd_payload );

#define SRCH_RX_SETUP_MSG( sm, command )                                \
  {                                                                     \
    rx_sm_req_t *rx_cmd_payload;                                        \
                                                                        \
    /* grab a outgoing message buffer for srch rx */                    \
    rx_cmd_payload = (rx_sm_req_t *)                                    \
      STM_ALLOC_MSG_BUF( sm,                                            \
                         &SRCH_COMMON_GROUP,                            \
                         command,                                       \
                         sizeof(rx_sm_req_t));                          \
                                                                        \
    srch_rx_init_stm_cmd( rx_cmd_payload );

#define SRCH_RX_SEND_MSG()                                              \
    stm_put_internal_cmd_payload( (void *) rx_cmd_payload );            \
  }


/*===========================================================================

                    FUNCTION DECLARATIONS FOR MODULE

===========================================================================*/

/*===========================================================================

FUNCTION       SRCH_RX_GET_DEVICE

DESCRIPTION    This function returns the RF device for the specified chain.
               RFCOM_RECEIVER_DIV will be returned if diversity is enabled.

DEPENDENCIES   None.

RETURN VALUE   The rf device.

SIDE EFFECTS   None.

===========================================================================*/
extern rx_sm_device_enum_t srch_rx_get_device
(
  rx_sm_client_enum_t chain_name    /* chain */
);

#ifdef FEATURE_MODEM_1X_SUPPORTS_RF
/*===========================================================================

FUNCTION       SRCH_RX_GET_DEVICE_IN_USE

DESCRIPTION    This function returns the RF device for the specified chain.

DEPENDENCIES   None.

RETURN VALUE   The rf device.

SIDE EFFECTS   None.

===========================================================================*/
extern rfm_device_enum_type srch_rx_get_device_in_use
(
  rx_sm_client_enum_t        rx_client      /* RX State Machine */
);

#endif /* FEATURE_MODEM_1X_SUPPORTS_RF */

/*===========================================================================

FUNCTION      SRCH_RX_GET_TUNED_DEVICE

DESCRIPTION   This function returns a tuned RF device.

               This function is not to be called if no chains are tuned.

DEPENDENCIES  None

RETURN VALUE  The tuned rf device, or if none tuned then SRCH_RF_MAX_DEVICES

SIDE EFFECTS  None

===========================================================================*/
extern rx_sm_device_enum_t srch_rx_get_tuned_device( void );

/*===========================================================================

FUNCTION      SRCH_RX_GET_TX_DEVICE

DESCRIPTION   This function returns the RF Tx device.

DEPENDENCIES  None

RETURN VALUE  The RF Tx device in use.

SIDE EFFECTS  None

===========================================================================*/
extern rx_sm_tx_device_enum_t srch_rx_get_tx_device( void );

/*===========================================================================

FUNCTION      SRCH_RX_GET_CARRIER

DESCRIPTION   This function returns which carrier is in use.

DEPENDENCIES  None

RETURN VALUE  The carrier in use.

SIDE EFFECTS  None

===========================================================================*/
extern rx_sm_carrier_enum_t srch_rx_get_carrier( void );

/*===========================================================================

FUNCTION      SRCH_RX_CHAIN_IS_IN_SV_MODE

DESCRIPTION   This function determines if the SRCH RX module is in SV mode

DEPENDENCIES  None

RETURN VALUE  True if the specified chain is RX1, FALSE otherwise

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_is_in_sv_mode( void );

/*===========================================================================

FUNCTION       SRCH_RX_CHAIN_IS_RXTX

DESCRIPTION    This function determines if the specified chain is RXTX

DEPENDENCIES   None.

RETURN VALUE   True if specified chain is RXTX, FALSE otherwise

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_chain_is_rxtx
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION       SRCH_RX_CHAIN_IS_RX_ONLY

DESCRIPTION    This function determines if the specified chain is RX

DEPENDENCIES   None.

RETURN VALUE   True if the specified chain is RX1, FALSE otherwise

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_chain_is_rx_only
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_STATUS

DESCRIPTION    This function returns the status of a chain

DEPENDENCIES   None.

RETURN VALUE   Chain status.

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_status_type srch_rx_get_status
(
  rx_sm_client_enum_t chain_name    /* chain */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_BAND

DESCRIPTION    Returns the current band for the specified chain

DEPENDENCIES   None.

RETURN VALUE   The current band.

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_band_type srch_rx_get_band
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_CHAN

DESCRIPTION    Returns the current channel for the specified chain

DEPENDENCIES   None.

RETURN VALUE   The current channel.

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_chan_type srch_rx_get_chan
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_HOME_BAND

DESCRIPTION    This function returns the home band

DEPENDENCIES   None.

RETURN VALUE   The home band.

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_band_type srch_rx_get_home_band( void );

/*===========================================================================

FUNCTION       SRCH_RX_GET_HOME_CHAN

DESCRIPTION    This function returns the home channel

DEPENDENCIES   None.

RETURN VALUE   The home channel.

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_chan_type srch_rx_get_home_chan( void );

/*===========================================================================

FUNCTION      SRCH_RX_IS_BAND_CHAN_SUPPORTED

DESCRIPTION   This function checks with RF is the band and channel passed is
              supported or not.

DEPENDENCIES  None.

RETURN VALUE  TRUE if band_chan supported, FALSE otherwise.

SIDE EFFECTS  None.

===========================================================================*/
extern boolean srch_rx_is_band_chan_supported
(
  srch_rx_band_type         band,           /* returned band class */
  srch_rx_chan_type         channel         /* returned channel number */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_CURR_BAND_CHAN

DESCRIPTION    This function returns the home band and channel

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   srch_rx is updated.

===========================================================================*/
extern void srch_rx_get_curr_band_chan
(
  rx_sm_client_enum_t      chain_name,    /* chain */
  srch_rx_band_type       *band,          /* returned band class */
  srch_rx_chan_type       *channel        /* returned channel number */
);

/*===========================================================================

FUNCTION      SRCH_RX_GET_BAND_MASK

DESCRIPTION   SRCH wrapper function for rfm_get_band_mask. The rf api is
              used to know the bands supported by a given device. The upper
              layers can use this srch wrapper to know the bands supported
              and SRCH uses the device in use to get the information. This
              way the upper layers need not worry about mapping the correct
              rf device.

DEPENDENCIES  None

RETURN VALUE  RF band mask - bands supported by RF

SIDE EFFECTS  None

===========================================================================*/
extern sys_band_mask_type srch_rx_get_band_mask( void );


/*===========================================================================

FUNCTION      SRCH_RX_GET_BAND_SUB_CLASS_MASK

DESCRIPTION   SRCH wrapper function for rfm_cdma_get_sub_class. The rf api is
              used to know the band subclass supported by a given device for a
              given band class.
              The upper layers can use this srch wrapper to know the bands
              subclass supported by a band class and SRCH uses the device in
              use to get the information. This way the upper layers need not
              worry about mapping the correct rf device.

DEPENDENCIES  None

RETURN VALUE  RF band subclass mask for a given band supported by RF

SIDE EFFECTS  None

===========================================================================*/
extern uint32 srch_rx_get_band_sub_class_mask( rf_card_band_type band_class );

/*===========================================================================

FUNCTION      SRCH_RX_IS_BAND_CHAN_SUPPORTED

DESCRIPTION   SRCH wrapper function for rfm_cdma_is_band_chan_supported.
              This function checks if the (band, chan) is valid by consulting
              this RF API.
              The upper layers can use this srch wrapper to know if a
              band/chan is supported in the device.This way the upper layers
              need not worry about mapping the correct rf device.

DEPENDENCIES  None

RETURN VALUE  RF band/chan which needs to be checked.

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_is_rf_band_chan_supported( sys_channel_type band_chan );

/*===========================================================================
FUNCTION      SRCH_RX_IS_RF_TUNE_SUPPORTED_ON_BAND_CHAN

DESCRIPTION   SRCH wrapper function for rfm_is_band_chan_supported_v2.
              This function consults RF API to determine if 1x can tune a
              channel, without requiring rfm_enter_mode() to be called first.

DEPENDENCIES  None

RETURN VALUE  is_tune_supported
                  TRUE if rf tune to band/channel is supported.
                  FALSE if rf tune to band/channel is not supported.

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_is_rf_tune_supported_on_band_chan
(
  srch_rx_band_type band,
  srch_rx_chan_type chan
);

/*===========================================================================
FUNCTION      SRCH_RX_IS_PRIMARY_RF_TUNE_SUPPORTED_ON_BAND_CHAN

DESCRIPTION   SRCH wrapper function for rfm_is_band_chan_supported.
              This function consults RF API to determine if 1x priamary RF
              client can tune to a channel.

DEPENDENCIES  None

RETURN VALUE  is_tune_supported
                TRUE if rf tune to band/channel is supported.
                FALSE if rf tune to band/channel is not supported.

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_is_primary_rf_tune_supported_on_band_chan
(
  srch_rx_band_type band,
  srch_rx_chan_type chan
);


/*===========================================================================

FUNCTION       SRCH_RX_GET_RX_CARRIER_FREQ

DESCRIPTION    This function returns the carrier frequency for a given band
               class and channel.

DEPENDENCIES   None.

RETURN VALUE   Frequency in KHz.

SIDE EFFECTS   None.

===========================================================================*/
extern uint32 srch_rx_get_rx_carrier_freq
(
  srch_rx_band_type         band,           /* returned band class */
  srch_rx_chan_type         channel         /* returned channel number */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_FREQ_RANGE

DESCRIPTION    This function returns the frequency range corresponding to
               a given band class.

DEPENDENCIES   None.

RETURN VALUE   Frequency range.

SIDE EFFECTS   none

===========================================================================*/
extern srch_rx_freq_range_type srch_rx_get_freq_range
(
  srch_rx_band_type band_class  /* Band class */
);

/*===========================================================================

FUNCTION       SRCH_RX_OWNS_CHAIN

DESCRIPTION    This function determines if 1X owns a chain associated with
               the specified chain_name.

DEPENDENCIES   None.

RETURN VALUE   True if we own an antenna, otherwise FALSE.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_owns_chain
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION      SRCH_RX_OWNS_OTHER_CHAIN

DESCRIPTION   This function determines if 1X owns a chain associated with
               the chain other than the specified chain_name.
               (TUNE, OWNED, OWNED_DISABLED, MODIFY_PENDING,
                MODIFY_TUNE_PENDING)

DEPENDENCIES  None

RETURN VALUE  True if we own an antenna, otherwise FALSE.

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_owns_other_chain
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION      SRCH_RX_OWNS_BOTH_CHAINS

DESCRIPTION   This function determines if 1X owns both the primary and
              secondary chain.
              (TUNE, OWNED, OWNED_DISABLED, MODIFY_PENDING,
               MODIFY_TUNE_PENDING)

DEPENDENCIES  None.

RETURN VALUE  True if we own both antennae, otherwise FALSE.

SIDE EFFECTS  None.

===========================================================================*/
extern boolean srch_rx_owns_both_chains( void );

/*===========================================================================

FUNCTION      SRCH_RX_OWNS_ANY_CHAIN

DESCRIPTION   This function determines if 1X owns any chain.
              (TUNE, OWNED, OWNED_DISABLED, MODIFY_PENDING,
               MODIFY_TUNE_PENDING)

DEPENDENCIES  None.

RETURN VALUE  True if we own an antenna, otherwise FALSE.

SIDE EFFECTS  None.

===========================================================================*/
extern boolean srch_rx_owns_any_chain( void );

/*===========================================================================

FUNCTION       SRCH_RX_TUNED

DESCRIPTION    This function determines if 1X has an active antenna that
               is tuned and ready to demodulate.

DEPENDENCIES   None.

RETURN VALUE   True if we are active on an antenna, otherwise FALSE.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_tuned
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION      SRCH_RX_USES_OTHER_CHAIN

DESCRIPTION   This function determines if 1X has an active antenna on the
              other chain that is tuned and ready to demodulate.
               (OWNED, MODIFY_PENDING, MODIFY_TUNE_PENDING)

DEPENDENCIES  None

RETURN VALUE  True if we are active on an antenna, otherwise FALSE.

SIDE EFFECTS  None

===========================================================================*/
extern boolean srch_rx_uses_other_chain
(
  rx_sm_client_enum_t  chain_name    /* chain */
);

/*===========================================================================

FUNCTION      SRCH_RX_USES_BOTH_CHAINS

DESCRIPTION   This function determines if 1X has an active antenna on both
              the chains that is tuned and ready to demodulate.
              (OWNED, MODIFY_PENDING, MODIFY_TUNE_PENDING)

DEPENDENCIES  None.

RETURN VALUE  True if we are active on both antennae, otherwise FALSE.

SIDE EFFECTS  None.

===========================================================================*/
extern boolean srch_rx_uses_both_chains( void );

/*===========================================================================

FUNCTION      SRCH_RX_USES_ANY_CHAIN

DESCRIPTION   This function determines if 1X has an active antenna on any of
              the chain that is tuned and ready to demodulate.
              (OWNED, MODIFY_PENDING, MODIFY_TUNE_PENDING)

DEPENDENCIES  None.

RETURN VALUE  True if we are active on any antenna, otherwise FALSE.

SIDE EFFECTS  None.

===========================================================================*/
extern boolean srch_rx_uses_any_chain( void );

/*===========================================================================

FUNCTION       SRCH_RX_SM_EXCHANGE_DEVICES

DESCRIPTION    This function exchanges the devices used by the
               DEMOD and ALT chains.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_sm_exchange_devices
(
 boolean fing_setup
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_RX_AGC

DESCRIPTION    This function gets the AGC for the specified receive chain

DEPENDENCIES   None.

RETURN VALUE   AGC for the specified receive chain.

SIDE EFFECTS   None.

===========================================================================*/
extern int8 srch_rx_get_rx_agc
(
  rx_sm_client_enum_t  chain_name   /* chain name */
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_HOME_RX_AGC

DESCRIPTION    This function gets the AGC for the specified receive chain
               when it is or was last tuned to the home band/channel

DEPENDENCIES   None.

RETURN VALUE   AGC for the demod chain when it is or was last tuned to the
               home band/channel.

SIDE EFFECTS   Stores the AGC for both the primary and secondary chains.

===========================================================================*/
extern int8 srch_rx_get_home_rx_agc(void);

/*===========================================================================

FUNCTION      SRCH_RX_GET_LAST_HOME_RX_AGC

DESCRIPTION   This function gets the last good AGC for the primary and
               secondary receive chains when they were last tuned to the
               home band/channel while in the online state.  If not active,
               -127 is returned instead.

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  Updates the parameters with the last RX AGC for the respective
               chains when they were last tuned to the home band/channel in
               0.334 dB units, centered at -63.248dBm on success, and -127
               on failure.

===========================================================================*/
extern void srch_rx_get_last_home_rx_agc( int8* primary, int8* secondary );

/*===========================================================================

FUNCTION       SRCH_RX_GET_PATH_RX_AGC_IN_DBM

DESCRIPTION    This function gets the path AGC for the specified chain in
               dBm

DEPENDENCIES   None.

RETURN VALUE   AGC for the specified receive chain.

SIDE EFFECTS   None.

===========================================================================*/
extern int16 srch_rx_get_path_agc_in_dBm
(
  rx_sm_client_enum_t chain_name    /* chain name */
);

/*===========================================================================

FUNCTION       SRCH_RX_IS_TUNED_HOME

DESCRIPTION    This function determines whether we are tuned to the home band
               and channel.

DEPENDENCIES   None.

RETURN VALUE   TRUE if the specified chain is already tuned to the given
               band class and channel.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_is_tuned_home
(
  rx_sm_client_enum_t chain_name    /* chain name */
);

/*===========================================================================

FUNCTION       SRCH_RX_IS_BAND_CHAN

DESCRIPTION    This function determines if the band class and channel for a
               specified chain match the passed in parameters.

DEPENDENCIES   None.

RETURN VALUE   TRUE if the band class and channel match the parameters.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_is_band_chan
(
  rx_sm_client_enum_t     chain_name,    /* chain name */
  srch_rx_band_type       band,          /* band_class */
  srch_rx_chan_type       channel        /* channel number */
);

/*===========================================================================

FUNCTION       SRCH_RX_CHAIN_OWNED

DESCRIPTION    Returns the current chain owned by 1x

DEPENDENCIES   None.

RETURN VALUE   Chain Name

SIDE EFFECTS   None.

===========================================================================*/
extern srch_rx_chain_owned_type srch_rx_chain_owned ( void );

/*===========================================================================

FUNCTION       SRCH_RX_GET_TX_PWR_LIMIT

DESCRIPTION    This function returns the CDMA Tx power limit setting in 10 bit
               binary format.  The returned value is interpreted as follows:

               Returned value               0........1023
               Tx power limit          -52.25........+31dBm
               Tx power limit register   +511........-512

DEPENDENCIES   None.

RETURN VALUE   10 bit Tx power limit value in dBm12

SIDE EFFECTS   None.

===========================================================================*/
extern int16 srch_rx_sm_get_tx_pwr_limit( void );

/*===========================================================================

FUNCTION      SRCH_RX_SM_GET_FILTERED_TX_PWR_DBM10

DESCRIPTION   This function returns Tx power reported by the FW Tx AGC filter.

DEPENDENCIES  None

RETURN VALUE  10 bit Filtered Tx power value in dBm10

SIDE EFFECTS  None

===========================================================================*/

extern int16 srch_rx_sm_get_filtered_tx_pwr_dbm10( void );

/*===========================================================================

FUNCTION       SRCH_RX_SM_GET_TX_AGC

DESCRIPTION    This function returns the CDMA Tx AGC setting in 10 bit
               binary format.

DEPENDENCIES   None.

RETURN VALUE   10 bit Tx AGC value in 1/10th dBm

SIDE EFFECTS   None.

===========================================================================*/
extern int16 srch_rx_sm_get_tx_agc( void );

/*========================================================================

FUNCTION       SRCH_RX_GET_WAKEUP_CHAIN

DESCRIPTION    This function returns the TRM chain resource that is required
               based on the last RX AGC reading.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

========================================================================*/
extern trm_resource_enum_t srch_rx_get_wakeup_chain( void );

/*========================================================================

FUNCTION       SRCH_RX_GET_RESERVED_CHAIN

DESCRIPTION    This function returns the TRM chain that was last reserved.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

========================================================================*/
extern trm_resource_enum_t srch_rx_get_reserved_chain( void );

/*========================================================================

FUNCTION SRCH_RX_SET_RESERVED_CHAIN

DESCRIPTION   This function sets the TRM chain for next request.

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None

========================================================================*/
extern void srch_rx_set_reserved_chain( trm_resource_enum_t resource );

/*========================================================================

FUNCTION       SRCH_RX_SET_RX_CHAIN_SEL_THRESH

DESCRIPTION    This function writes the RX chain selection thresholds that
               are read from NV.

DEPENDENCIES   Must call srch_rx_init prior to
               srch_rx_set_rx_chain_sel_thresh.

RETURN VALUE   None.

SIDE EFFECTS   None.

========================================================================*/
extern void srch_rx_set_rx_chain_sel_thresh
(
  int16                 upper_thresh, /* Above, transition to any RX chain */
  int16                 lower_thresh  /* Below, transition to best RX chain */
);

/*========================================================================

FUNCTION SRCH_RX_GET_WAKEUP_CHAIN

DESCRIPTION   This function returns the TRM chain resource that is required
               for acquisition based on chain sensitivity information.

DEPENDENCIES  None

RETURN VALUE  Wakeup chain name

SIDE EFFECTS  None

========================================================================*/
trm_resource_enum_t srch_rx_get_init_resource( void );

/*========================================================================

FUNCTION       SRCH_RX_DET_WAKEUP_CHAIN

DESCRIPTION    This function determines the next wakeup chain based on
               the current RX AGC and the current demod chain.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

========================================================================*/
extern void srch_rx_det_wakeup_chain
(
  boolean going_to_sleep 
);

/*========================================================================

FUNCTION      SRCH_RX_SET_WAKEUP_CHAIN

DESCRIPTION   This function sets the next wakeup chain with the value
              passed. Note that this overrides the srch_rx_det_wakeup_chain
              selection

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECTS  None

========================================================================*/
extern void srch_rx_set_wakeup_chain
(
  trm_resource_enum_t chain
);

/*========================================================================

FUNCTION       SRCH_RX_GET_PRIMARY_RESOURCE

DESCRIPTION    Return the RXTX capable resource from TRM list.

DEPENDENCIES   None

RETURN VALUE   RXTX capable resource

SIDE EFFECTS   None

========================================================================*/
extern trm_resource_enum_t srch_rx_get_primary_resource( void );

/*===========================================================================

FUNCTION       SRCH_RX_SET_RX_CHAIN_TRANS

DESCRIPTION    This function writes the RX chain translation as set in NV.

DEPENDENCIES   Must call srch_rx_init prior to
               srch_rx_set_rx_chain_trans.

RETURN VALUE   None.

SIDE EFFECTS   Updates static structure with trm_resource_enum_t translation
               of TRM_RX_ANY.

===========================================================================*/
extern void srch_rx_set_rx_chain_trans
(
  srch_nv_dbg_mask_type    rx_chain_trans,
  trm_resource_enum_t      request_type
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_RX_CHAIN_TRANS

DESCRIPTION    This function reads the RX chain translation as set in NV.

DEPENDENCIES   Must call srch_rx_init prior to
               srch_rx_get_rx_chain_trans.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern trm_resource_enum_t srch_rx_get_rx_chain_trans
(
  trm_resource_enum_t   request_type
);

/*========================================================================

FUNCTION       SRCH_RX_DRX_IS_BAND_TUNABLE

DESCRIPTION    Return if the current antenna-chain resources that
               1X has a lock on can be tuned to the given band.

DEPENDENCIES   None

RETURN VALUE   TRUE if current band can be tuned

SIDE EFFECTS   None

========================================================================*/
extern boolean srch_rx_drx_is_band_tunable
(
 srch_rx_band_type       band
);

/*========================================================================

FUNCTION       SRCH_RX_RESERVE_PRIMARY_FOR_NEXT_WAKEUP

DESCRIPTION    Sets a flag to indicate reserving of primary chain
               for next wakeup.

DEPENDENCIES   None

RETURN VALUE   None

SIDE EFFECTS   None

========================================================================*/
extern void srch_rx_reserve_primary_for_next_wakeup( void );

/*===========================================================================

FUNCTION       SRCH_RX_BUILD_RF_SUB

DESCRIPTION    This function builds the rf sub packet.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_build_rf_sub
(
  srch_genlog_packet_id_type   id,       /* id of packet to commit sub */
  sub_commit_func_type         commit_fp /* function to call to commit the
                                            subpacket */
);

/*===========================================================================

FUNCTION      SRCH_RX_GET_RF_PREP_WARMUP_TIME

DESCRIPTION   This function returns the RF prep warmup overhead in us.

DEPENDENCIES  None.

RETURN VALUE  The RF prep warmup overhead in us.

SIDE EFFECTS  None.

===========================================================================*/
extern uint16 srch_rx_get_rf_prep_warmup_time( void );

/*===========================================================================

FUNCTION      SRCH_RX_GET_RF_EXEC_WARMUP_TIME

DESCRIPTION   This function returns the RF exec warmup overhead in us.

DEPENDENCIES  None.

RETURN VALUE  The RF exec warmup overhead in us.

SIDE EFFECTS  None.

===========================================================================*/
extern uint16 srch_rx_get_rf_exec_warmup_time( void );

/*===========================================================================

FUNCTION       SRCH_RX_GET_CDMA_TX_AGC

DESCRIPTION    Wrapper function for rf function rfm_1x_get_cdma_tx_agc

DEPENDENCIES   None.

RETURN VALUE   The current CDMA Tx AGC setting.

SIDE EFFECTS   None.

===========================================================================*/
extern uint16 srch_rx_get_cdma_tx_agc( void );

/*===========================================================================

FUNCTION       SRCH_RX_FREEZE_CAGC

DESCRIPTION    Wrapper function for rf function rf_freeze_cagc

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_freeze_cagc( void );

/*===========================================================================

FUNCTION       SRCH_RX_UNFREEZE_CAGC

DESCRIPTION    Wrapper function for rf function rf_unfreeze_cagc

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_unfreeze_cagc( void );

/*===========================================================================

FUNCTION       SRCH_RX_SLEEP_TX

DESCRIPTION    Wrapper function for rf function rfm_tx_stop

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_sleep_tx( rx_sm_device_enum_t dev );

/*===========================================================================

FUNCTION       SRCH_RX_WAKEUP_TX

DESCRIPTION    Wrapper function for rf function rfm_tx_start

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_wakeup_tx( rx_sm_device_enum_t dev );

/*===========================================================================

FUNCTION SRCH_RX_GET_CDMA_TURNAROUND_CONST

DESCRIPTION
  This function returns the CDMA turnaround constant value for current RF band.

DEPENDENCIES
  None

RETURN VALUE
  The CDMA turn around constant in dBm for the current RF band.

SIDE EFFECTS
  None

===========================================================================*/
extern int32 srch_rx_get_cdma_turnaround_const(void);

#ifdef FEATURE_MODEM_1X_IRAT_LTO1X
#ifdef FEATURE_MODEM_1X_SUPPORTS_RF
/*===========================================================================

FUNCTION       SRCH_RX_MEAS_COMMON_SCRIPT_BUILD_SCRIPTS

DESCRIPTION    This function calls rf API to set up and build scripts for IRAT
               pilot measurements

DEPENDENCIES   LTE has to call rfm_meas_common_enter() first

RETURN VALUE   rfm_meas_result_type from RF pilot measurement script

SIDE EFFECTS   None.

===========================================================================*/
extern rfm_meas_result_type srch_rx_meas_common_script_build_scripts
(
  /* RF header (common info for L21x meas) */
  rfm_meas_header_type                              *p_rf_header,

  /* source tech parameters */
  rfm_meas_lte_setup_and_build_scripts_param_type   *p_source_param,

  /* target tech parameters */
  rfm_meas_1x_setup_and_build_scripts_param_type    *p_target_param
);
#endif /* FEATURE_MODEM_1X_SUPPORTS_RF */
#endif /* FEATURE_MODEM_1X_IRAT_LTO1X */

/*===========================================================================

FUNCTION       SRCH_RX_SM_TUNE_AND_WAIT

DESCRIPTION    This function tunes the specified chain to the band and channel
               requested. It also tunes successfully when diversity is turned
               on.  It will inform the user via a callback once the tune has
               completed.

               The currently owned receiver will be automatically enabled
               if necessary.  The sample server will be started in continuous
               mode.  The sample ram will be flushed if the demod chain is
               tuned.

               This is a synchronous call that will return after the RF is
               tuned and ready.

DEPENDENCIES   The chain_name must be associated with a RF chain owned
               by 1x.

RETURN VALUE   True if successfull.

SIDE EFFECTS   srch_rx is updated.

===========================================================================*/
extern boolean srch_rx_sm_tune_and_wait
(
  srch_rx_band_type                    band,              /* band class */
  srch_rx_chan_type                    chan               /* channel number */
);

/*===========================================================================

FUNCTION       SRCH_RX_INIT_STM_CMD

DESCRIPTION    This function initializes the command buffer that is to be
               passed to RX_SM

DEPENDENCIES   None

RETURN VALUE   None

SIDE EFFECTS   None

===========================================================================*/
extern void srch_rx_init_stm_cmd
(
  rx_sm_req_t                          *rx_cmd_payload
);

/*===========================================================================

FUNCTION       SRCH_RX_GET_WAKEUP_INFO

DESCRIPTION    Copies the last wakeup values into the parameter.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_get_wakeup_info( srch_rx_wakeup_info_type* dest );

/*===========================================================================

FUNCTION       SRCH_RX_UPDATE_WAKEUP_INFO

DESCRIPTION    Updates the wakeup struct with the current values

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   Wakeup struct is modified.

===========================================================================*/
extern void srch_rx_update_wakeup_info( void );

#ifdef FEATURE_SRCH_DRX_SUPPORTS_ONE_BAND
/*===========================================================================

FUNCTION       SRCH_RX_UPDATE_ACQ_BAND_INFO

DESCRIPTION    This function updates band information passed in the
               Acquisition command

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   srch_rx structure gets modified

===========================================================================*/
extern void srch_rx_update_acq_band_info
(
  srch_rx_band_type band
);
#endif /* FEATURE_SRCH_DRX_SUPPORTS_ONE_BAND */

/*===========================================================================

FUNCTION       SRCH_RX_SET_SECONDARY_CHAIN_TRANS

DESCRIPTION    This function updates the chain translation to return
               SECONDARY chain[DRx].

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_set_secondary_chain_trans( void );

/*===========================================================================

FUNCTION       SRCH_RX_BACKUP_CHAIN_TRANS

DESCRIPTION    This function backs-up the chain translation value into
               srch_rx.chain_trans_bkup.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_backup_chain_trans( void );

/*===========================================================================

FUNCTION       SRCH_RX_RESTORE_CHAIN_TRANS

DESCRIPTION    This function restores the chain translation value from
               srch_rx.chain_trans_bkup.

DEPENDENCIES   srch_rx_backup_chain_trans() must have been called earlier.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_restore_chain_trans( void );

/*===========================================================================

FUNCTION       SRCHRDIV_DISABLE_SLEEP_SECOND_CHAIN_TEST

DESCRIPTION    Returns whether SLEEP should be disabled during
               FTM second chain test mode.

DEPENDENCIES   None.

RETURN VALUE   TRUE if the sleep should be disabled in FTM second chain test.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srchrdiv_disable_sleep_second_chain_test( void );

/*===========================================================================

FUNCTION       SRCH_RX_CFS_TUNE_WAIT

DESCRIPTION    Wrapper function for rf macro RF_CFS_TUNE_WAIT

DEPENDENCIES   None.

RETURN VALUE   RF CFS tune time.

SIDE EFFECTS   None.

===========================================================================*/
extern uint32 srch_rx_cfs_tune_wait( void );

/*===========================================================================

FUNCTION       SRCH_RX_VBATT_READ

DESCRIPTION    Wrapper function for vbatt_read.

DEPENDENCIES   None.

RETURN VALUE   A scaled version of the batter level where empty is VBATT_SCALED_MIN,
               and full is VBATT_SCALE.

SIDE EFFECTS   None.

===========================================================================*/
extern byte srch_rx_vbatt_read( void );

/*===========================================================================

FUNCTION       SRCH_RX_GET_RXLM_BUFFER_HANDLE

DESCRIPTION    This function returns RXLM handle for the given chain.

DEPENDENCIES   None.

RETURN VALUE   RxLM handle for the input chain.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_get_rxlm_buffer_handle
(
  rx_sm_client_enum_t        rx_client,     /* RX State Machine */
  lm_handle_type             *rxlm_buf      /* RxLM handle */
);

/*===========================================================================

FUNCTION       SRCH_RX_SET_ANT_CARRIER_AND_DEMOD

DESCRIPTION    This function sets the fingers to the appropriate antenna,
               carrier, and demod path based on which RF device(s) are
               currently owned.

DEPENDENCIES   None

RETURN VALUE   None

SIDE EFFECTS   None

===========================================================================*/
void srch_rx_set_ant_carrier_and_demod( void );

#ifdef FEATURE_ZZ2_2

typedef enum
{
  SRCH_DYN_RID_QPCH_ERASURE,
  SRCH_DYN_RID_QPCH_PI_DETECTED,
  SRCH_DYN_RID_CHAN_EST_FAIL,
  SRCH_DYN_RID_REACQ_FAIL,
  SRCH_DYN_RID_PAGE_DECODE_FAIL,
  SRCH_DYN_RID_RX0_AGC_FAIL,
  SRCH_DYN_RID_RX1_AGC_FAIL,
  SRCH_DYN_RID_RX0_ECIO_FAIL,
  SRCH_DYN_RID_RX1_ECIO_FAIL,
  SRCH_DYN_RID_DESIRED_CHAIN_FAIL,

  SRCH_DYN_RID_QPCH_PI_NOT_DETECTED,
  SRCH_DYN_RID_CHAN_EST_PASS,
  SRCH_DYN_RID_REACQ_SUCCESS,
  SRCH_DYN_RID_PAGE_DECODE_SUCCESS,
  SRCH_DYN_RID_RX0_AGC_PASS,
  SRCH_DYN_RID_RX1_AGC_PASS,
  SRCH_DYN_RID_RX0_ECIO_PASS,
  SRCH_DYN_RID_RX1_ECIO_PASS,
  SRCH_DYN_RID_DESIRED_CHAIN_PASS,

  SRCH_DYN_RID_RUDE_WAKEUP,
  SRCH_DYN_RID_NON_QPCH_TL,
  SRCH_DYN_RID_SEL_DIV_EN,
  SRCH_DYN_RID_OTF_DIV_EN,
  SRCH_DYN_RID_ASDIV_EN
} srch_dyn_rid_bad_cond_enum_type;

/*===========================================================================

FUNCTION       SRCH_RX_INIT_ZZ2_2

DESCRIPTION    Initialize the ZZ2_2 mechanism.

               - resets the data structure
               - reloads the thresholds
               - reloads the selection params
               - resets the desired chain

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_init_zz2_2 (void);

/*===========================================================================

FUNCTION       SRCH_RX_IS_ZZ2_2_ENABLED

DESCRIPTION    Return whether or not 1x ZZ2_2 should be
               used at this time.

DEPENDENCIES   None.

RETURN VALUE   TRUE if we use it this time, FALSE otherwise.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_is_zz2_2_enabled (void);

/*===========================================================================

FUNCTION       SRCH_RX_DYN_ZZ2_2_ENABLED

DESCRIPTION    Return if dynamic ZZ2_2 is enabled based on the conditions
               mask.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern boolean srch_rx_dyn_zz2_2_enabled(void);

/*===========================================================================

FUNCTION       SRCH_RX_PRINT_ZZ2_2_PARMS

DESCRIPTION    Print the current thresholds and selection parameters used by
               ZZ2_2.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_print_zz2_2_parms (void);

/*===========================================================================

FUNCTION       SRCH_RX_SET_ZZ2_2_COND

DESCRIPTION    This allows the caller to specify a specific good/bad
               condition that has been met.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_set_zz2_2_cond
(
  srch_dyn_rid_bad_cond_enum_type condition
);

/*===========================================================================

FUNCTION       SRCH_RX_CHECK_ZZ2_2_CHAN_EST

DESCRIPTION    Check the channel estimate against the ZZ2_2 thresholds.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   The corresponding condition flag will be set if the thresholds
               are exceeded.

===========================================================================*/
extern void srch_rx_check_zz2_2_chan_est
(
  uint32 threshold,
  uint32 chan_est
);

/*===========================================================================

FUNCTION       SRCH_RX_CHECK_DYN_RID_ECIO

DESCRIPTION    Check the Ec/Io against the dynamic idle div thresholds.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   The corresponding condition flag will be set if the thresholds
               are exceeded.

===========================================================================*/
extern void srch_rx_check_dyn_rid_ecio (void);

/*===========================================================================

FUNCTION       SRCH_RX_CHECK_ZZ2_2_RXAGC

DESCRIPTION    Check the Rx AGC of a specified chain against the ZZ2_2 thresholds.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   The corresponding condition flag will be set if the thresholds
               are exceeded.

===========================================================================*/
extern void srch_rx_check_zz2_2_rxagc(void);

/*========================================================================

FUNCTION       SRCH_RX_ZZ2_2_RESET

DESCRIPTION    This function resets ZZ2_2 and initializes the desired
               device as specified by the caller.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

========================================================================*/
extern void srch_rx_zz2_2_reset
(
  rx_sm_device_enum_t desired_device  /* initial desired device */
);
#endif /* FEATURE_ZZ2_2 */

#ifdef FEATURE_INIT_2
/*===========================================================================

     D Y N A M I C    R E C E I V E R    I N I T    D I V E R S I T Y

===========================================================================*/
/*===========================================================================

FUNCTION       SRCH_RX_GET_RIN_ENABLE
DESCRIPTION    This function returns RIN enable mask
DEPENDENCIES   None

RETURN VALUE   uint8
SIDE EFFECTS   None

===========================================================================*/
extern uint8 srch_rx_get_rin_enable( void );

/*===========================================================================

FUNCTION       SRCH_RX_GET_RIN_PRIMARY/SEC_AGC_CK
DESCRIPTION    This function returns the threshold for the Primary/Secondar
               RxAGC check
DEPENDENCIES   None

RETURN VALUE   AGC value
SIDE EFFECTS   None

===========================================================================*/
extern int16 srch_rx_get_rin_primary_agc_ck( void );
extern int16 srch_rx_get_rin_sec_agc_ck( void );

/*===========================================================================

FUNCTION       SRCH_RX_SET/GET_RIN_AGC
DESCRIPTION    This function records the AGC values read during the last check
               A passed in value of "0" indicates "not reported"
DEPENDENCIES   None

RETURN VALUE   None
SIDE EFFECTS   None

===========================================================================*/
extern void srch_rx_set_rin_agc
(
  int16 pri_agc,
  int16 sec_agc
);
extern void srch_rx_get_rin_agc
(
  int16 *pri_agc,
  int16 *sec_agc
);

/*===========================================================================

FUNCTION       SRCH_RX_INIT_RIN
DESCRIPTION    This function initializes acquisition diversity
DEPENDENCIES   None

RETURN VALUE   None
SIDE EFFECTS   None

===========================================================================*/
extern void srch_rx_init_rin(void);

/*===========================================================================

FUNCTION       SRCH_RX_RIN_DIV_CTRL
DESCRIPTION    This function sets initial states diversity modes
               Thresholds may be passed as "0" to use default thresholds
DEPENDENCIES   None

RETURN VALUE   TRUE if successfully applied
SIDE EFFECTS   None

===========================================================================*/
extern boolean  srch_rx_rin_div_ctrl
(
  uint8                        en_mask,          /* enable mask */
  int16                        th0,              /* th0 threshold in dB */
  int16                        th1               /* th1 threshold in dB */
);
#endif /* FEATURE_INIT_2 */

#ifdef FEATURE_MODEM_1X_SRCH_ASD
/*===========================================================================

FUNCTION       SRCH_RX_GET_AGC_THRES_GOOD

DESCRIPTION    The function gets AGC Good threshold which is used to make switch
               decision in IDLE.

DEPENDENCIES   None.

RETURN VALUE   AGC Good threshold

SIDE EFFECTS   None.

===========================================================================*/
extern int32 srch_rx_get_agc_thresh_good( void );

#ifdef FEATURE_1X_TO_LTE
/*===========================================================================

FUNCTION       SRCH_RX_ASD_REQUEST_ANTENNA_SWITCH_LOCK
DESCRIPTION    This function is used to request a switch lock
DEPENDENCIES   None

RETURN VALUE   TRUE if successful, otherwise FALSE
SIDE EFFECTS   None

===========================================================================*/
extern boolean srch_rx_asd_request_antenna_switch_lock(void);

/*===========================================================================

FUNCTION       SRCH_RX_ASD_REQUEST_SWITCH_UNLOCK
DESCRIPTION    This function is used to request a switch unlock
DEPENDENCIES   None

RETURN VALUE   TRUE if successful, otherwise FALSE
SIDE EFFECTS   None

===========================================================================*/
extern boolean srch_rx_asd_request_antenna_switch_unlock(void);

/*===========================================================================

FUNCTION       SRCH_RX_ASD_SET_PCH_DECODE_COND

DESCRIPTION    The function sets/stores the pch decode result.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_asd_set_pch_decode_cond
( 
  boolean status
);

/*===========================================================================

FUNCTION       SRCH_RX_ASD_PCH_DECODE_COND_RESET

DESCRIPTION    The function resets the pch decode paramters.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
extern void srch_rx_asd_pch_decode_cond_reset(void);

#endif /* FEATURE_1X_TO_LTE */
#endif /* FEATURE_MODEM_1X_SRCH_ASD */

#endif /* SRCH_RX_H */

