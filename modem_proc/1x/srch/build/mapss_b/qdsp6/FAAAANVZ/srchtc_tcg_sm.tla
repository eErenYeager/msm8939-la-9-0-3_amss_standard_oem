###############################################################################
#
#    srchtc_tcg_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchtc_tcg_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: TCG_SM
# State machine:current state:input:next state                      fgcolor bgcolor

TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_INACTIVE              eea2 0 00 0    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_QUERY_MC              eea2 0 00 1    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_ACTIVE                eea2 0 00 2    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_WAIT_RETRY            eea2 0 00 3    @olive @white

TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_INACTIVE                eea2 0 01 0    @olive @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_QUERY_MC                eea2 0 01 1    @olive @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_ACTIVE                  eea2 0 01 2    @olive @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_WAIT_RETRY              eea2 0 01 3    @olive @white

TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE       eea2 0 02 0    @olive @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC       eea2 0 02 1    @olive @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE         eea2 0 02 2    @olive @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY     eea2 0 02 3    @olive @white

TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE        eea2 0 03 0    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC        eea2 0 03 1    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE          eea2 0 03 2    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY      eea2 0 03 3    @olive @white

TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE       eea2 0 04 0    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC       eea2 0 04 1    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE         eea2 0 04 2    @olive @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY     eea2 0 04 3    @olive @white

TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_INACTIVE                eea2 0 05 0    @olive @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_QUERY_MC                eea2 0 05 1    @olive @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_ACTIVE                  eea2 0 05 2    @olive @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_WAIT_RETRY              eea2 0 05 3    @olive @white

TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_INACTIVE              eea2 1 00 0    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_QUERY_MC              eea2 1 00 1    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_ACTIVE                eea2 1 00 2    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_WAIT_RETRY            eea2 1 00 3    @olive @white

TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_INACTIVE                eea2 1 01 0    @olive @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_QUERY_MC                eea2 1 01 1    @olive @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_ACTIVE                  eea2 1 01 2    @olive @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_WAIT_RETRY              eea2 1 01 3    @olive @white

TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE       eea2 1 02 0    @olive @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC       eea2 1 02 1    @olive @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE         eea2 1 02 2    @olive @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY     eea2 1 02 3    @olive @white

TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE        eea2 1 03 0    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC        eea2 1 03 1    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE          eea2 1 03 2    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY      eea2 1 03 3    @olive @white

TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE       eea2 1 04 0    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC       eea2 1 04 1    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE         eea2 1 04 2    @olive @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY     eea2 1 04 3    @olive @white

TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_INACTIVE                eea2 1 05 0    @olive @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_QUERY_MC                eea2 1 05 1    @olive @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_ACTIVE                  eea2 1 05 2    @olive @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_WAIT_RETRY              eea2 1 05 3    @olive @white

TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_INACTIVE                eea2 2 00 0    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_QUERY_MC                eea2 2 00 1    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_ACTIVE                  eea2 2 00 2    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_WAIT_RETRY              eea2 2 00 3    @olive @white

TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_INACTIVE                  eea2 2 01 0    @olive @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_QUERY_MC                  eea2 2 01 1    @olive @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_ACTIVE                    eea2 2 01 2    @olive @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_WAIT_RETRY                eea2 2 01 3    @olive @white

TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE         eea2 2 02 0    @olive @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC         eea2 2 02 1    @olive @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE           eea2 2 02 2    @olive @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY       eea2 2 02 3    @olive @white

TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE          eea2 2 03 0    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC          eea2 2 03 1    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE            eea2 2 03 2    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY        eea2 2 03 3    @olive @white

TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE         eea2 2 04 0    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC         eea2 2 04 1    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE           eea2 2 04 2    @olive @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY       eea2 2 04 3    @olive @white

TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_INACTIVE                  eea2 2 05 0    @olive @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_QUERY_MC                  eea2 2 05 1    @olive @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_ACTIVE                    eea2 2 05 2    @olive @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_WAIT_RETRY                eea2 2 05 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_INACTIVE            eea2 3 00 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_QUERY_MC            eea2 3 00 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_ACTIVE              eea2 3 00 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_WAIT_RETRY          eea2 3 00 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_INACTIVE              eea2 3 01 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_QUERY_MC              eea2 3 01 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_ACTIVE                eea2 3 01 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_WAIT_RETRY            eea2 3 01 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE     eea2 3 02 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC     eea2 3 02 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE       eea2 3 02 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY   eea2 3 02 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE      eea2 3 03 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC      eea2 3 03 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE        eea2 3 03 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY    eea2 3 03 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE     eea2 3 04 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC     eea2 3 04 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE       eea2 3 04 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY   eea2 3 04 3    @olive @white

TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_INACTIVE              eea2 3 05 0    @olive @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_QUERY_MC              eea2 3 05 1    @olive @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_ACTIVE                eea2 3 05 2    @olive @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_WAIT_RETRY            eea2 3 05 3    @olive @white



# End machine generated TLA code for state machine: TCG_SM

