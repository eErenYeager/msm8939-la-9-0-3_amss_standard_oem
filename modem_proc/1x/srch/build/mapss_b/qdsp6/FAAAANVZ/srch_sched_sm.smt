/*=============================================================================

  srch_sched_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_sched_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCH_SCHED_SM */

/* Transition function prototypes */
static stm_state_type sched_process_reacq_cmd(void *);
static stm_state_type sched_process_qpch_reacq_cmd(void *);
static stm_state_type sched_fake_reacquisition(void *);
static stm_state_type sched_fake_qpch_reacquisition(void *);
static stm_state_type sched_initiate_ofreq_srch(void *);
static stm_state_type sched_process_ofreq_handoff(void *);
static stm_state_type sched_ignore_prio(void *);
static stm_state_type sched_abort(void *);
static stm_state_type sched_abort_home(void *);
static stm_state_type sched_flush_energy_filters(void *);
static stm_state_type sched_process_dump_in_init(void *);
static stm_state_type sched_process_orig_pending(void *);
static stm_state_type sched_rebuild_reacq_lists(void *);
static stm_state_type sched_ignore_tune_done(void *);
static stm_state_type sched_process_init_timed_cmd(void *);
static stm_state_type sched_ignore_srch_tl_timer(void *);
static stm_state_type sched_process_ofreq_suspend(void *);
static stm_state_type sched_process_init_srch4_reg(void *);
static stm_state_type sched_process_timed_srch_tl_timer(void *);
static stm_state_type sched_process_ofreq_update_timer(void *);
static stm_state_type sched_process_timed_dump(void *);
static stm_state_type sched_process_scan_all_cmd(void *);
static stm_state_type sched_process_timed_cmd(void *);
static stm_state_type sched_process_timed_tune(void *);
static stm_state_type sched_process_timed_srch4_reg(void *);
static stm_state_type sched_process_scan_all_dump(void *);
static stm_state_type sched_process_scan_all_srch_tl_timer(void *);
static stm_state_type SRCH_SCHED_SM_ignore_input(void *);
static stm_state_type sched_reacq_abort(void *);
static stm_state_type sched_process_reacq(void *);
static stm_state_type sched_process_reacq_lost_dump(void *);
static stm_state_type sched_ignore_orig_pending(void *);
static stm_state_type sched_process_reacq_srch4_reg(void *);
static stm_state_type sched_qpch_reacq_abort(void *);
static stm_state_type sched_process_qpch_reacq(void *);
static stm_state_type sched_process_qpch_reacq_lost_dump(void *);
static stm_state_type sched_process_qpch_reacq_srch4_reg(void *);
static stm_state_type sched_process_prio(void *);
static stm_state_type sched_process_prio_lost_dump(void *);
static stm_state_type sched_setup_prio_srch(void *);
static stm_state_type sched_process_rescan(void *);
static stm_state_type sched_process_rescan_lost_dump(void *);
static stm_state_type sched_process_last_seen_neighbor(void *);
static stm_state_type sched_process_last_seen_neighbor_lost_dump(void *);
static stm_state_type sched_process_neighbor_scan(void *);
static stm_state_type sched_process_neighbor_scan_lost_dump(void *);
static stm_state_type sched_process_reacq_list_scan(void *);
static stm_state_type sched_process_reacq_list_scan_lost_dump(void *);
static stm_state_type sched_process_reacq_list_tune(void *);
static stm_state_type sched_process_ofreq_scan(void *);
static stm_state_type sched_process_ofreq_scan_lost_dump(void *);
static stm_state_type sched_process_ofreq_scan_tune(void *);
static stm_state_type sched_ofreq_scan_abort(void *);
static stm_state_type sched_process_zz_ofreq(void *);
static stm_state_type sched_process_ofreq_lost_dump(void *);
static stm_state_type sched_process_ofreq_tune(void *);
static stm_state_type sched_ofreq_zz_abort(void *);
static stm_state_type sched_process_ns_ofreq(void *);
static stm_state_type sched_ofreq_ns_abort(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SRCH_SCHED_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void sched_entry(stm_group_type *group);
static void sched_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void sched_enter_init(void *payload, stm_state_type previous_state);
static void sched_exit_init(void *payload, stm_state_type previous_state);
static void sched_enter_timed(void *payload, stm_state_type previous_state);
static void sched_exit_timed(void *payload, stm_state_type previous_state);
static void sched_enter_scan_all(void *payload, stm_state_type previous_state);
static void sched_exit_scan_all(void *payload, stm_state_type previous_state);
static void sched_enter_reacq(void *payload, stm_state_type previous_state);
static void sched_exit_reacq_states(void *payload, stm_state_type previous_state);
static void sched_enter_qpch_reacq(void *payload, stm_state_type previous_state);
static void sched_enter_prio(void *payload, stm_state_type previous_state);
static void sched_exit_prio(void *payload, stm_state_type previous_state);
static void sched_enter_rescan(void *payload, stm_state_type previous_state);
static void sched_enter_last_seen_neighbor(void *payload, stm_state_type previous_state);
static void sched_reset_lost_dump_cnt(void *payload, stm_state_type previous_state);
static void sched_enter_reacq_list_scan(void *payload, stm_state_type previous_state);
static void sched_enter_ofreq_scan(void *payload, stm_state_type previous_state);
static void sched_enter_ofreq(void *payload, stm_state_type previous_state);
static void sched_exit_ofreq(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCH_SCHED_SM_NUMBER_OF_STATES 13
#define SRCH_SCHED_SM_NUMBER_OF_INPUTS 22


/* State enumeration */
enum
{
  INIT_STATE,
  TIMED_STATE,
  SCAN_ALL_STATE,
  REACQ_STATE,
  QPCH_REACQ_STATE,
  PRIO_STATE,
  RESCAN_STATE,
  LAST_SEEN_NEIGHBOR_STATE,
  NEIGHBOR_SCAN_STATE,
  REACQ_LIST_SCAN_STATE,
  OFREQ_SCAN_STATE,
  OFREQ_ZZ_STATE,
  OFREQ_NS_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCH_SCHED_SM_states[ SRCH_SCHED_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", sched_enter_init, sched_exit_init},
  {"TIMED_STATE", sched_enter_timed, sched_exit_timed},
  {"SCAN_ALL_STATE", sched_enter_scan_all, sched_exit_scan_all},
  {"REACQ_STATE", sched_enter_reacq, sched_exit_reacq_states},
  {"QPCH_REACQ_STATE", sched_enter_qpch_reacq, sched_exit_reacq_states},
  {"PRIO_STATE", sched_enter_prio, sched_exit_prio},
  {"RESCAN_STATE", sched_enter_rescan, sched_exit_reacq_states},
  {"LAST_SEEN_NEIGHBOR_STATE", sched_enter_last_seen_neighbor, sched_exit_reacq_states},
  {"NEIGHBOR_SCAN_STATE", sched_reset_lost_dump_cnt, sched_exit_reacq_states},
  {"REACQ_LIST_SCAN_STATE", sched_enter_reacq_list_scan, sched_exit_reacq_states},
  {"OFREQ_SCAN_STATE", sched_enter_ofreq_scan, sched_exit_reacq_states},
  {"OFREQ_ZZ_STATE", sched_enter_ofreq, sched_exit_ofreq},
  {"OFREQ_NS_STATE", sched_enter_ofreq, sched_exit_ofreq},
};


/* Input value, name table */
static const stm_input_array_type
  SRCH_SCHED_SM_inputs[ SRCH_SCHED_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)REACQ_CMD, "REACQ_CMD"},
  {(stm_input_type)QPCH_REACQ_CMD, "QPCH_REACQ_CMD"},
  {(stm_input_type)FAKE_REACQ_CMD, "FAKE_REACQ_CMD"},
  {(stm_input_type)FAKE_QPCH_REACQ_CMD, "FAKE_QPCH_REACQ_CMD"},
  {(stm_input_type)OFREQ_CMD, "OFREQ_CMD"},
  {(stm_input_type)OFREQ_HANDOFF_CMD, "OFREQ_HANDOFF_CMD"},
  {(stm_input_type)PRIO_CMD, "PRIO_CMD"},
  {(stm_input_type)ABORT_CMD, "ABORT_CMD"},
  {(stm_input_type)ABORT_HOME_CMD, "ABORT_HOME_CMD"},
  {(stm_input_type)FLUSH_FILT_CMD, "FLUSH_FILT_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)ORIG_PENDING_CMD, "ORIG_PENDING_CMD"},
  {(stm_input_type)REBUILD_LISTS_CMD, "REBUILD_LISTS_CMD"},
  {(stm_input_type)RF_TUNE_TIMER_CMD, "RF_TUNE_TIMER_CMD"},
  {(stm_input_type)TIMED_CMD, "TIMED_CMD"},
  {(stm_input_type)SRCH_TL_TIMER_CMD, "SRCH_TL_TIMER_CMD"},
  {(stm_input_type)OFREQ_SUSPEND_CMD, "OFREQ_SUSPEND_CMD"},
  {(stm_input_type)SCHED_SRCH4_REG_CMD, "SCHED_SRCH4_REG_CMD"},
  {(stm_input_type)SRCH_OFREQ_UPDATE_TIMER_CMD, "SRCH_OFREQ_UPDATE_TIMER_CMD"},
  {(stm_input_type)SCAN_ALL_CMD, "SCAN_ALL_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)ABORT_OFREQ_CMD, "ABORT_OFREQ_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCH_SCHED_SM_transitions[ SRCH_SCHED_SM_NUMBER_OF_STATES *  SRCH_SCHED_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    sched_process_reacq_cmd,    /* REACQ_CMD */
    sched_process_qpch_reacq_cmd,    /* QPCH_REACQ_CMD */
    sched_fake_reacquisition,    /* FAKE_REACQ_CMD */
    sched_fake_qpch_reacquisition,    /* FAKE_QPCH_REACQ_CMD */
    sched_initiate_ofreq_srch,    /* OFREQ_CMD */
    sched_process_ofreq_handoff,    /* OFREQ_HANDOFF_CMD */
    sched_ignore_prio,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    sched_flush_energy_filters,    /* FLUSH_FILT_CMD */
    sched_process_dump_in_init,    /* SRCH_DUMP_CMD */
    sched_process_orig_pending,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    sched_ignore_tune_done,    /* RF_TUNE_TIMER_CMD */
    sched_process_init_timed_cmd,    /* TIMED_CMD */
    sched_ignore_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    sched_process_ofreq_suspend,    /* OFREQ_SUSPEND_CMD */
    sched_process_init_srch4_reg,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state TIMED_STATE */
    sched_process_reacq_cmd,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    sched_initiate_ofreq_srch,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    sched_ignore_prio,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_timed_dump,    /* SRCH_DUMP_CMD */
    NULL,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    sched_process_timed_tune,    /* RF_TUNE_TIMER_CMD */
    sched_process_timed_cmd,    /* TIMED_CMD */
    sched_process_timed_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    sched_process_ofreq_suspend,    /* OFREQ_SUSPEND_CMD */
    sched_process_timed_srch4_reg,    /* SCHED_SRCH4_REG_CMD */
    sched_process_ofreq_update_timer,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    sched_process_scan_all_cmd,    /* SCAN_ALL_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state SCAN_ALL_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    sched_ignore_prio,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_scan_all_dump,    /* SRCH_DUMP_CMD */
    NULL,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    SRCH_SCHED_SM_ignore_input,    /* TIMED_CMD */
    sched_process_scan_all_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    sched_process_ofreq_suspend,    /* OFREQ_SUSPEND_CMD */
    sched_process_timed_srch4_reg,    /* SCHED_SRCH4_REG_CMD */
    sched_process_ofreq_update_timer,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state REACQ_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_reacq,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    sched_process_reacq_srch4_reg,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_reacq_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state QPCH_REACQ_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_qpch_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_qpch_reacq,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    sched_process_qpch_reacq_srch4_reg,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_qpch_reacq_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state PRIO_STATE */
    sched_process_reacq_cmd,    /* REACQ_CMD */
    sched_process_qpch_reacq_cmd,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    sched_initiate_ofreq_srch,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    sched_setup_prio_srch,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    sched_flush_energy_filters,    /* FLUSH_FILT_CMD */
    sched_process_prio,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    sched_process_init_timed_cmd,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_prio_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state RESCAN_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_rescan,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_rescan_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state LAST_SEEN_NEIGHBOR_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_last_seen_neighbor,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_last_seen_neighbor_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state NEIGHBOR_SCAN_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_neighbor_scan,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    NULL,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_neighbor_scan_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state REACQ_LIST_SCAN_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_reacq_list_scan,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    sched_process_reacq_list_tune,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_reacq_list_scan_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state OFREQ_SCAN_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_reacq_abort,    /* ABORT_CMD */
    NULL,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_ofreq_scan,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    NULL,    /* REBUILD_LISTS_CMD */
    sched_process_ofreq_scan_tune,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_ofreq_scan_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    sched_ofreq_scan_abort,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state OFREQ_ZZ_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_zz_ofreq,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    sched_process_ofreq_tune,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    sched_process_ofreq_suspend,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_ofreq_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    sched_ofreq_zz_abort,    /* ABORT_OFREQ_CMD */

  /* Transition functions for state OFREQ_NS_STATE */
    NULL,    /* REACQ_CMD */
    NULL,    /* QPCH_REACQ_CMD */
    NULL,    /* FAKE_REACQ_CMD */
    NULL,    /* FAKE_QPCH_REACQ_CMD */
    NULL,    /* OFREQ_CMD */
    NULL,    /* OFREQ_HANDOFF_CMD */
    NULL,    /* PRIO_CMD */
    sched_abort,    /* ABORT_CMD */
    sched_abort_home,    /* ABORT_HOME_CMD */
    NULL,    /* FLUSH_FILT_CMD */
    sched_process_ns_ofreq,    /* SRCH_DUMP_CMD */
    sched_ignore_orig_pending,    /* ORIG_PENDING_CMD */
    sched_rebuild_reacq_lists,    /* REBUILD_LISTS_CMD */
    sched_process_ofreq_tune,    /* RF_TUNE_TIMER_CMD */
    NULL,    /* TIMED_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    sched_process_ofreq_suspend,    /* OFREQ_SUSPEND_CMD */
    NULL,    /* SCHED_SRCH4_REG_CMD */
    NULL,    /* SRCH_OFREQ_UPDATE_TIMER_CMD */
    NULL,    /* SCAN_ALL_CMD */
    sched_process_ofreq_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    sched_ofreq_ns_abort,    /* ABORT_OFREQ_CMD */

};


/* State machine definition */
stm_state_machine_type SRCH_SCHED_SM =
{
  "SRCH_SCHED_SM", /* state machine name */
  56524, /* unique SM id (hash of name) */
  sched_entry, /* state machine entry function */
  sched_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SRCH_SCHED_SM_NUMBER_OF_STATES,
  SRCH_SCHED_SM_NUMBER_OF_INPUTS,
  SRCH_SCHED_SM_states,
  SRCH_SCHED_SM_inputs,
  SRCH_SCHED_SM_transitions,
};

/* End machine generated code for state machine: SRCH_SCHED_SM */

