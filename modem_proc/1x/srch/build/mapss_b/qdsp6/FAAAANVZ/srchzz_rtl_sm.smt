/*=============================================================================

  srchzz_rtl_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_rtl_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_RTL_SM */

/* Transition function prototypes */
static stm_state_type rtl_setup_sleep(void *);
static stm_state_type rtl_remember_rude_wakeup(void *);
static stm_state_type rtl_adjust_combiner_time_after_sb(void *);
static stm_state_type rtl_process_freq_track_off_done(void *);
static stm_state_type rtl_process_rf_disabled_cmd(void *);
static stm_state_type rtl_process_go_to_sleep_cmd(void *);
static stm_state_type rtl_process_rf_disable_comp_cmd(void *);
static stm_state_type rtl_process_aflt_release_done(void *);
static stm_state_type rtl_process_rude_wakeup(void *);
static stm_state_type rtl_process_wakeup(void *);
static stm_state_type rtl_process_no_rf_lock(void *);
static stm_state_type rtl_process_sleep_timer_expired(void *);
static stm_state_type rtl_process_rf_chain_granted(void *);
static stm_state_type rtl_process_rf_chain_denied(void *);
static stm_state_type rtl_process_rf_granted(void *);
static stm_state_type rtl_process_wait_sb_roll(void *);
static stm_state_type rtl_process_dump_done(void *);
static stm_state_type rtl_process_lost_dump(void *);
static stm_state_type rtl_adjust_combiner_time(void *);
static stm_state_type rtl_process_till_reacq_timer_expired(void *);


/* State Machine entry/exit function prototypes */
static void rtl_entry(stm_group_type *group);
static void rtl_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void rtl_enter_init(void *payload, stm_state_type previous_state);
static void rtl_enter_sleep(void *payload, stm_state_type previous_state);
static void rtl_exit_sleep(void *payload, stm_state_type previous_state);
static void rtl_enter_wait_for_lock(void *payload, stm_state_type previous_state);
static void rtl_exit_wait_for_lock(void *payload, stm_state_type previous_state);
static void rtl_enter_wait_sb(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_RTL_SM_NUMBER_OF_STATES 5
#define SRCHZZ_RTL_SM_NUMBER_OF_INPUTS 17


/* State enumeration */
enum
{
  INIT_STATE,
  WAIT_FOR_AFLT_RELEASE_STATE,
  SLEEP_STATE,
  WAIT_FOR_LOCK_STATE,
  WAIT_SB_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_RTL_SM_states[ SRCHZZ_RTL_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", rtl_enter_init, NULL},
  {"WAIT_FOR_AFLT_RELEASE_STATE", NULL, NULL},
  {"SLEEP_STATE", rtl_enter_sleep, rtl_exit_sleep},
  {"WAIT_FOR_LOCK_STATE", rtl_enter_wait_for_lock, rtl_exit_wait_for_lock},
  {"WAIT_SB_STATE", rtl_enter_wait_sb, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_RTL_SM_inputs[ SRCHZZ_RTL_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)ADJUST_TIMING_CMD, "ADJUST_TIMING_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)RX_RF_DISABLED_CMD, "RX_RF_DISABLED_CMD"},
  {(stm_input_type)GO_TO_SLEEP_CMD, "GO_TO_SLEEP_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)AFLT_RELEASE_DONE_CMD, "AFLT_RELEASE_DONE_CMD"},
  {(stm_input_type)WAKEUP_CMD, "WAKEUP_CMD"},
  {(stm_input_type)NO_RF_LOCK_CMD, "NO_RF_LOCK_CMD"},
  {(stm_input_type)SLEEP_TIMER_EXPIRED_CMD, "SLEEP_TIMER_EXPIRED_CMD"},
  {(stm_input_type)RX_CH_GRANTED_CMD, "RX_CH_GRANTED_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)TILL_REACQ_TIMER_EXPIRED_CMD, "TILL_REACQ_TIMER_EXPIRED_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_RTL_SM_transitions[ SRCHZZ_RTL_SM_NUMBER_OF_STATES *  SRCHZZ_RTL_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    rtl_setup_sleep,    /* START_SLEEP_CMD */
    rtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    rtl_adjust_combiner_time_after_sb,    /* ADJUST_TIMING_CMD */
    rtl_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    rtl_process_rf_disabled_cmd,    /* RX_RF_DISABLED_CMD */
    rtl_process_go_to_sleep_cmd,    /* GO_TO_SLEEP_CMD */
    rtl_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* SLEEP_TIMER_EXPIRED_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* TILL_REACQ_TIMER_EXPIRED_CMD */

  /* Transition functions for state WAIT_FOR_AFLT_RELEASE_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* WAKEUP_NOW_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rtl_process_aflt_release_done,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* SLEEP_TIMER_EXPIRED_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* TILL_REACQ_TIMER_EXPIRED_CMD */

  /* Transition functions for state SLEEP_STATE */
    NULL,    /* START_SLEEP_CMD */
    rtl_process_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    rtl_process_wakeup,    /* WAKEUP_CMD */
    rtl_process_no_rf_lock,    /* NO_RF_LOCK_CMD */
    rtl_process_sleep_timer_expired,    /* SLEEP_TIMER_EXPIRED_CMD */
    rtl_process_rf_chain_granted,    /* RX_CH_GRANTED_CMD */
    rtl_process_rf_chain_denied,    /* RX_CH_DENIED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* TILL_REACQ_TIMER_EXPIRED_CMD */

  /* Transition functions for state WAIT_FOR_LOCK_STATE */
    NULL,    /* START_SLEEP_CMD */
    rtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* SLEEP_TIMER_EXPIRED_CMD */
    rtl_process_rf_granted,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* TILL_REACQ_TIMER_EXPIRED_CMD */

  /* Transition functions for state WAIT_SB_STATE */
    NULL,    /* START_SLEEP_CMD */
    rtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    rtl_adjust_combiner_time,    /* ADJUST_TIMING_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* SLEEP_TIMER_EXPIRED_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    rtl_process_wait_sb_roll,    /* ROLL_CMD */
    rtl_process_dump_done,    /* SRCH_DUMP_CMD */
    rtl_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    rtl_process_till_reacq_timer_expired,    /* TILL_REACQ_TIMER_EXPIRED_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_RTL_SM =
{
  "SRCHZZ_RTL_SM", /* state machine name */
  741, /* unique SM id (hash of name) */
  rtl_entry, /* state machine entry function */
  rtl_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_RTL_SM_NUMBER_OF_STATES,
  SRCHZZ_RTL_SM_NUMBER_OF_INPUTS,
  SRCHZZ_RTL_SM_states,
  SRCHZZ_RTL_SM_inputs,
  SRCHZZ_RTL_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_RTL_SM */

