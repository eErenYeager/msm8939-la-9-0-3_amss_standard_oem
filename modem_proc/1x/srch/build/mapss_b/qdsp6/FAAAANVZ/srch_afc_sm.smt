/*=============================================================================

  srch_afc_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_afc_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: AFC_SM */

/* Transition function prototypes */
static stm_state_type afc_go_idle(void *);
static stm_state_type afc_sleep_wakeup(void *);
static stm_state_type afc_remember_rot_push_flag(void *);
static stm_state_type afc_freq_track_off_reply(void *);
static stm_state_type afc_exit_access(void *);
static stm_state_type afc_idle_slow(void *);
static stm_state_type AFC_SM_ignore_input(void *);
static stm_state_type afc_start_acq(void *);
static stm_state_type afc_idle_to_inactive(void *);
static stm_state_type afc_enter_traffic(void *);
static stm_state_type afc_exit_traffic(void *);
static stm_state_type afc_idle_srl(void *);
static stm_state_type afc_enter_access(void *);
static stm_state_type afc_freq_track_off(void *);
static stm_state_type afc_freq_track_off_rtl(void *);
static stm_state_type afc_slow(void *);
static stm_state_type afc_srl(void *);
static stm_state_type afc_check_rotator_err_r_trk(void *);
static stm_state_type afc_log_timer(void *);
static stm_state_type afc_process_rot_push_flag(void *);
static stm_state_type afc_rot_push_sample_timer(void *);
static stm_state_type afc_rot_pullin_done(void *);
static stm_state_type afc_rot_pullin_fail(void *);
static stm_state_type afc_process_acq_done(void *);
static stm_state_type afc_ack_fast(void *);
static stm_state_type afc_ack_xo(void *);
static stm_state_type afc_pullin_done(void *);
static stm_state_type afc_pullin_fail(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type AFC_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void afc_entry(stm_group_type *group);
static void afc_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void afc_enter_inactive(void *payload, stm_state_type previous_state);
static void afc_enter_idle(void *payload, stm_state_type previous_state);
static void afc_enter_r_trk(void *payload, stm_state_type previous_state);
static void afc_enter_sanity_timer_off(void *payload, stm_state_type previous_state);
static void afc_enter_vco_pull_in(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define AFC_SM_NUMBER_OF_STATES 6
#define AFC_SM_NUMBER_OF_INPUTS 20


/* State enumeration */
enum
{
  AFC_INACTIVE,
  AFC_IDLE,
  AFC_R_TRK,
  AFC_R_ACQ,
  AFC_ACQ_DONE,
  AFC_VCO_PULL_IN,
};


/* State name, entry, exit table */
static const stm_state_array_type
  AFC_SM_states[ AFC_SM_NUMBER_OF_STATES ] =
{
  {"AFC_INACTIVE", afc_enter_inactive, NULL},
  {"AFC_IDLE", afc_enter_idle, NULL},
  {"AFC_R_TRK", afc_enter_r_trk, NULL},
  {"AFC_R_ACQ", afc_enter_sanity_timer_off, NULL},
  {"AFC_ACQ_DONE", NULL, NULL},
  {"AFC_VCO_PULL_IN", afc_enter_vco_pull_in, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  AFC_SM_inputs[ AFC_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)AFC_CDMA_CMD, "AFC_CDMA_CMD"},
  {(stm_input_type)AFC_FREQ_TRACK_ON_CMD, "AFC_FREQ_TRACK_ON_CMD"},
  {(stm_input_type)AFC_ROT_PUSH_FLAG_CMD, "AFC_ROT_PUSH_FLAG_CMD"},
  {(stm_input_type)AFC_FREQ_TRACK_OFF_CMD, "AFC_FREQ_TRACK_OFF_CMD"},
  {(stm_input_type)AFC_FREQ_TRACK_OFF_RTL_CMD, "AFC_FREQ_TRACK_OFF_RTL_CMD"},
  {(stm_input_type)AFC_ACCESS_EXIT_CMD, "AFC_ACCESS_EXIT_CMD"},
  {(stm_input_type)AFC_FAST_CMD, "AFC_FAST_CMD"},
  {(stm_input_type)AFC_SLOW_CMD, "AFC_SLOW_CMD"},
  {(stm_input_type)AFC_XO_CMD, "AFC_XO_CMD"},
  {(stm_input_type)AFC_SRL_CMD, "AFC_SRL_CMD"},
  {(stm_input_type)AFC_SANITY_TIMER_CMD, "AFC_SANITY_TIMER_CMD"},
  {(stm_input_type)AFC_OFF_CMD, "AFC_OFF_CMD"},
  {(stm_input_type)AFC_LOG_TIMER_CMD, "AFC_LOG_TIMER_CMD"},
  {(stm_input_type)AFC_START_ACQ_CMD, "AFC_START_ACQ_CMD"},
  {(stm_input_type)AFC_TRAFFIC_CMD, "AFC_TRAFFIC_CMD"},
  {(stm_input_type)AFC_TC_EXIT_CMD, "AFC_TC_EXIT_CMD"},
  {(stm_input_type)AFC_ACCESS_CMD, "AFC_ACCESS_CMD"},
  {(stm_input_type)AFC_ROT_PUSH_SAMPLE_TIMER_CMD, "AFC_ROT_PUSH_SAMPLE_TIMER_CMD"},
  {(stm_input_type)AFC_PULL_IN_DONE_CMD, "AFC_PULL_IN_DONE_CMD"},
  {(stm_input_type)AFC_PULL_IN_FAIL_CMD, "AFC_PULL_IN_FAIL_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  AFC_SM_transitions[ AFC_SM_NUMBER_OF_STATES *  AFC_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state AFC_INACTIVE */
    afc_go_idle,    /* AFC_CDMA_CMD */
    afc_sleep_wakeup,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_remember_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    afc_exit_access,    /* AFC_ACCESS_EXIT_CMD */
    afc_idle_slow,    /* AFC_FAST_CMD */
    afc_idle_slow,    /* AFC_SLOW_CMD */
    afc_idle_slow,    /* AFC_XO_CMD */
    AFC_SM_ignore_input,    /* AFC_SRL_CMD */
    AFC_SM_ignore_input,    /* AFC_SANITY_TIMER_CMD */
    AFC_SM_ignore_input,    /* AFC_OFF_CMD */
    AFC_SM_ignore_input,    /* AFC_LOG_TIMER_CMD */
    NULL,    /* AFC_START_ACQ_CMD */
    NULL,    /* AFC_TRAFFIC_CMD */
    NULL,    /* AFC_TC_EXIT_CMD */
    NULL,    /* AFC_ACCESS_CMD */
    NULL,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    NULL,    /* AFC_PULL_IN_DONE_CMD */
    NULL,    /* AFC_PULL_IN_FAIL_CMD */

  /* Transition functions for state AFC_IDLE */
    AFC_SM_ignore_input,    /* AFC_CDMA_CMD */
    NULL,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_remember_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_CMD */
    NULL,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    afc_exit_access,    /* AFC_ACCESS_EXIT_CMD */
    afc_idle_slow,    /* AFC_FAST_CMD */
    afc_idle_slow,    /* AFC_SLOW_CMD */
    afc_idle_slow,    /* AFC_XO_CMD */
    afc_idle_srl,    /* AFC_SRL_CMD */
    AFC_SM_ignore_input,    /* AFC_SANITY_TIMER_CMD */
    afc_idle_to_inactive,    /* AFC_OFF_CMD */
    AFC_SM_ignore_input,    /* AFC_LOG_TIMER_CMD */
    afc_start_acq,    /* AFC_START_ACQ_CMD */
    afc_enter_traffic,    /* AFC_TRAFFIC_CMD */
    afc_exit_traffic,    /* AFC_TC_EXIT_CMD */
    NULL,    /* AFC_ACCESS_CMD */
    NULL,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    NULL,    /* AFC_PULL_IN_DONE_CMD */
    NULL,    /* AFC_PULL_IN_FAIL_CMD */

  /* Transition functions for state AFC_R_TRK */
    afc_go_idle,    /* AFC_CDMA_CMD */
    AFC_SM_ignore_input,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_process_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off,    /* AFC_FREQ_TRACK_OFF_CMD */
    afc_freq_track_off_rtl,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    afc_exit_access,    /* AFC_ACCESS_EXIT_CMD */
    afc_slow,    /* AFC_FAST_CMD */
    afc_slow,    /* AFC_SLOW_CMD */
    afc_slow,    /* AFC_XO_CMD */
    afc_srl,    /* AFC_SRL_CMD */
    afc_check_rotator_err_r_trk,    /* AFC_SANITY_TIMER_CMD */
    NULL,    /* AFC_OFF_CMD */
    afc_log_timer,    /* AFC_LOG_TIMER_CMD */
    NULL,    /* AFC_START_ACQ_CMD */
    afc_enter_traffic,    /* AFC_TRAFFIC_CMD */
    afc_exit_traffic,    /* AFC_TC_EXIT_CMD */
    afc_enter_access,    /* AFC_ACCESS_CMD */
    afc_rot_push_sample_timer,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    NULL,    /* AFC_PULL_IN_DONE_CMD */
    NULL,    /* AFC_PULL_IN_FAIL_CMD */

  /* Transition functions for state AFC_R_ACQ */
    afc_go_idle,    /* AFC_CDMA_CMD */
    NULL,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_remember_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_CMD */
    NULL,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    NULL,    /* AFC_ACCESS_EXIT_CMD */
    NULL,    /* AFC_FAST_CMD */
    NULL,    /* AFC_SLOW_CMD */
    NULL,    /* AFC_XO_CMD */
    NULL,    /* AFC_SRL_CMD */
    AFC_SM_ignore_input,    /* AFC_SANITY_TIMER_CMD */
    NULL,    /* AFC_OFF_CMD */
    AFC_SM_ignore_input,    /* AFC_LOG_TIMER_CMD */
    AFC_SM_ignore_input,    /* AFC_START_ACQ_CMD */
    NULL,    /* AFC_TRAFFIC_CMD */
    NULL,    /* AFC_TC_EXIT_CMD */
    NULL,    /* AFC_ACCESS_CMD */
    NULL,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    afc_rot_pullin_done,    /* AFC_PULL_IN_DONE_CMD */
    afc_rot_pullin_fail,    /* AFC_PULL_IN_FAIL_CMD */

  /* Transition functions for state AFC_ACQ_DONE */
    afc_go_idle,    /* AFC_CDMA_CMD */
    NULL,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_remember_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_CMD */
    NULL,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    NULL,    /* AFC_ACCESS_EXIT_CMD */
    afc_process_acq_done,    /* AFC_FAST_CMD */
    afc_process_acq_done,    /* AFC_SLOW_CMD */
    afc_process_acq_done,    /* AFC_XO_CMD */
    afc_process_acq_done,    /* AFC_SRL_CMD */
    AFC_SM_ignore_input,    /* AFC_SANITY_TIMER_CMD */
    NULL,    /* AFC_OFF_CMD */
    AFC_SM_ignore_input,    /* AFC_LOG_TIMER_CMD */
    NULL,    /* AFC_START_ACQ_CMD */
    NULL,    /* AFC_TRAFFIC_CMD */
    NULL,    /* AFC_TC_EXIT_CMD */
    NULL,    /* AFC_ACCESS_CMD */
    NULL,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    NULL,    /* AFC_PULL_IN_DONE_CMD */
    NULL,    /* AFC_PULL_IN_FAIL_CMD */

  /* Transition functions for state AFC_VCO_PULL_IN */
    afc_go_idle,    /* AFC_CDMA_CMD */
    NULL,    /* AFC_FREQ_TRACK_ON_CMD */
    afc_remember_rot_push_flag,    /* AFC_ROT_PUSH_FLAG_CMD */
    afc_freq_track_off_reply,    /* AFC_FREQ_TRACK_OFF_CMD */
    NULL,    /* AFC_FREQ_TRACK_OFF_RTL_CMD */
    NULL,    /* AFC_ACCESS_EXIT_CMD */
    afc_ack_fast,    /* AFC_FAST_CMD */
    afc_ack_fast,    /* AFC_SLOW_CMD */
    afc_ack_xo,    /* AFC_XO_CMD */
    afc_ack_fast,    /* AFC_SRL_CMD */
    AFC_SM_ignore_input,    /* AFC_SANITY_TIMER_CMD */
    NULL,    /* AFC_OFF_CMD */
    AFC_SM_ignore_input,    /* AFC_LOG_TIMER_CMD */
    NULL,    /* AFC_START_ACQ_CMD */
    NULL,    /* AFC_TRAFFIC_CMD */
    NULL,    /* AFC_TC_EXIT_CMD */
    NULL,    /* AFC_ACCESS_CMD */
    NULL,    /* AFC_ROT_PUSH_SAMPLE_TIMER_CMD */
    afc_pullin_done,    /* AFC_PULL_IN_DONE_CMD */
    afc_pullin_fail,    /* AFC_PULL_IN_FAIL_CMD */

};


/* State machine definition */
stm_state_machine_type AFC_SM =
{
  "AFC_SM", /* state machine name */
  55205, /* unique SM id (hash of name) */
  afc_entry, /* state machine entry function */
  afc_exit, /* state machine exit function */
  AFC_INACTIVE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  AFC_SM_NUMBER_OF_STATES,
  AFC_SM_NUMBER_OF_INPUTS,
  AFC_SM_states,
  AFC_SM_inputs,
  AFC_SM_transitions,
};

/* End machine generated code for state machine: AFC_SM */

