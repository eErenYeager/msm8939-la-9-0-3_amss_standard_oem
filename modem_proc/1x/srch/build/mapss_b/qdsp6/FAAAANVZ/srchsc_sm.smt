/*=============================================================================

  srchsc_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchsc_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SYNC_SM */

/* Transition function prototypes */
static stm_state_type sc_go_to_cdma(void *);
static stm_state_type sc_go_to_slew(void *);
static stm_state_type sc_go_to_acq(void *);
static stm_state_type sc_go_to_sync(void *);
static stm_state_type sc_handle_rssi_timer(void *);
static stm_state_type sc_handle_search_dump(void *);
static stm_state_type sc_handle_lost_dump(void *);
static stm_state_type sc_handle_search_timer(void *);


/* State Machine entry/exit function prototypes */
static void sc_entry(stm_group_type *group);
static void sc_exit(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define SYNC_SM_NUMBER_OF_STATES 1
#define SYNC_SM_NUMBER_OF_INPUTS 8


/* State enumeration */
enum
{
  SYNC,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SYNC_SM_states[ SYNC_SM_NUMBER_OF_STATES ] =
{
  {"SYNC", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SYNC_SM_inputs[ SYNC_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_SLEW_F, "SRCH_SLEW_F"},
  {(stm_input_type)SRCH_ACQ_F, "SRCH_ACQ_F"},
  {(stm_input_type)SRCH_SC_F, "SRCH_SC_F"},
  {(stm_input_type)RSSI_TIMER_CMD, "RSSI_TIMER_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)SRCH_TL_TIMER_CMD, "SRCH_TL_TIMER_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SYNC_SM_transitions[ SYNC_SM_NUMBER_OF_STATES *  SYNC_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state SYNC */
    sc_go_to_cdma,    /* SRCH_CDMA_F */
    sc_go_to_slew,    /* SRCH_SLEW_F */
    sc_go_to_acq,    /* SRCH_ACQ_F */
    sc_go_to_sync,    /* SRCH_SC_F */
    sc_handle_rssi_timer,    /* RSSI_TIMER_CMD */
    sc_handle_search_dump,    /* SRCH_DUMP_CMD */
    sc_handle_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    sc_handle_search_timer,    /* SRCH_TL_TIMER_CMD */

};


/* State machine definition */
stm_state_machine_type SYNC_SM =
{
  "SYNC_SM", /* state machine name */
  239, /* unique SM id (hash of name) */
  sc_entry, /* state machine entry function */
  sc_exit, /* state machine exit function */
  SYNC, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SYNC_SM_NUMBER_OF_STATES,
  SYNC_SM_NUMBER_OF_INPUTS,
  SYNC_SM_states,
  SYNC_SM_inputs,
  SYNC_SM_transitions,
};

/* End machine generated code for state machine: SYNC_SM */
/* Begin machine generated code for state machine group: SRCH_SYNC_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean sync_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_SYNC_GROUP_sm_list[ 1 ] =
{
  &SYNC_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_SYNC_GROUP_sig_list[ 1 ] =
{
  SRCH_CMD_Q_SIG,
};


/* State machine group definition */
stm_group_type SRCH_SYNC_GROUP =
{
  "SRCH_SYNC_GROUP", /* state machine group name */
  SRCH_SYNC_GROUP_sig_list, /* signal mapping table */
  1, /* number of signals in mapping table */
  sync_sig_mapper, /* signal mapper function */
  SRCH_SYNC_GROUP_sm_list, /* state machine table */
  1, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_SYNC_GROUP */

