/*=============================================================================

  srch_rx_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_rx_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCH_RX_SM_P */

/* Transition function prototypes */
static stm_state_type rx_proc_request_chain(void *);
static stm_state_type rx_proc_exchange_devices(void *);
static stm_state_type rx_ignore_mod_reas_dur(void *);
static stm_state_type rx_ignore_tune(void *);
static stm_state_type rx_ignore_disable(void *);
static stm_state_type rx_ignore_release(void *);
static stm_state_type rx_proc_trm_enable(void *);
static stm_state_type rx_proc_trm_disable(void *);
static stm_state_type rx_proc_disable_comp(void *);
static stm_state_type rx_proc_reserve_at_in_inactive(void *);
static stm_state_type SRCH_RX_SM_P_ignore_input(void *);
static stm_state_type rx_proc_request_chain_wait_for_lock(void *);
static stm_state_type rx_proc_cancel_request(void *);
static stm_state_type rx_proc_trm_granted_lock(void *);
static stm_state_type rx_proc_trm_conflict_wfl(void *);
static stm_state_type rx_proc_request_on_owned(void *);
static stm_state_type rx_proc_release(void *);
static stm_state_type rx_proc_exec_wakeup(void *);
static stm_state_type rx_proc_disable_in_tune(void *);
static stm_state_type rx_proc_trm_conflict(void *);
static stm_state_type rx_proc_rf_prep_comp(void *);
static stm_state_type rx_proc_rf_tune_comp(void *);
static stm_state_type rx_proc_rf_tune_comp_delay(void *);
static stm_state_type rx_proc_reserve_at(void *);
static stm_state_type rx_proc_disable(void *);
static stm_state_type rx_proc_scheduled_disable(void *);
static stm_state_type rx_proc_mod_tune(void *);
static stm_state_type rx_proc_antenna_tuner(void *);
static stm_state_type rx_proc_mod_reason(void *);
static stm_state_type rx_proc_mod_criteria(void *);
static stm_state_type rx_proc_mod_duration(void *);
static stm_state_type rx_proc_mod_reas_dur(void *);
static stm_state_type rx_proc_irat_mod_reas_dur(void *);
static stm_state_type rx_proc_rf_rsp_slp_comp_on_owned(void *);
static stm_state_type rx_proc_rf_sched_slp_error(void *);
static stm_state_type rx_proc_request_on_owned_dis(void *);
static stm_state_type rx_proc_mod_dis_tune(void *);
static stm_state_type rx_proc_trm_mod_dis_granted(void *);
static stm_state_type rx_proc_trm_mod_tune_granted(void *);
static stm_state_type rx_proc_request_on_release_requested(void *);
static stm_state_type rx_proc_rf_rsp_slp_comp(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SRCH_RX_SM_P_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void rx_entry_p(stm_group_type *group);
static void rx_exit_p(stm_group_type *group);


/* State entry/exit function prototypes */
static void rx_sm_inactive_entry_p(void *payload, stm_state_type previous_state);
static void rx_sm_tune_entry_p(void *payload, stm_state_type previous_state);
static void rx_sm_owned_entry_p(void *payload, stm_state_type previous_state);
static void rx_sm_owned_exit_p(void *payload, stm_state_type previous_state);
static void rx_sm_owned_dis_entry_p(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCH_RX_SM_P_NUMBER_OF_STATES 8
#define SRCH_RX_SM_P_NUMBER_OF_INPUTS 26


/* State enumeration */
enum
{
  INACTIVE,
  WAIT_FOR_LOCK,
  TUNE,
  OWNED,
  OWNED_DISABLED,
  MODIFY_TUNE_PENDING,
  MODIFY_DISABLED_TUNE_PENDING,
  RELEASE_REQUESTED,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCH_RX_SM_P_states[ SRCH_RX_SM_P_NUMBER_OF_STATES ] =
{
  {"INACTIVE", rx_sm_inactive_entry_p, NULL},
  {"WAIT_FOR_LOCK", NULL, NULL},
  {"TUNE", rx_sm_tune_entry_p, NULL},
  {"OWNED", rx_sm_owned_entry_p, rx_sm_owned_exit_p},
  {"OWNED_DISABLED", rx_sm_owned_dis_entry_p, NULL},
  {"MODIFY_TUNE_PENDING", NULL, NULL},
  {"MODIFY_DISABLED_TUNE_PENDING", NULL, NULL},
  {"RELEASE_REQUESTED", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCH_RX_SM_P_inputs[ SRCH_RX_SM_P_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)RX_REQ_AND_NOTIFY_CMD, "RX_REQ_AND_NOTIFY_CMD"},
  {(stm_input_type)RX_EXCHANGE_DEVICES_CMD, "RX_EXCHANGE_DEVICES_CMD"},
  {(stm_input_type)RX_MOD_REAS_DUR_CMD, "RX_MOD_REAS_DUR_CMD"},
  {(stm_input_type)RX_MOD_IRAT_REAS_DUR_CMD, "RX_MOD_IRAT_REAS_DUR_CMD"},
  {(stm_input_type)RX_TUNE_CMD, "RX_TUNE_CMD"},
  {(stm_input_type)RX_DISABLE_CMD, "RX_DISABLE_CMD"},
  {(stm_input_type)RX_RELEASE_CMD, "RX_RELEASE_CMD"},
  {(stm_input_type)RX_TRM_CH_ENABLE_CMD, "RX_TRM_CH_ENABLE_CMD"},
  {(stm_input_type)RX_TRM_CH_DISABLE_CMD, "RX_TRM_CH_DISABLE_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)RX_RESERVE_AT_CMD, "RX_RESERVE_AT_CMD"},
  {(stm_input_type)RX_TRM_CH_GRANTED_CMD, "RX_TRM_CH_GRANTED_CMD"},
  {(stm_input_type)RX_TRM_CH_MODIFIED_CMD, "RX_TRM_CH_MODIFIED_CMD"},
  {(stm_input_type)RX_TRM_CH_CONFLICT_CMD, "RX_TRM_CH_CONFLICT_CMD"},
  {(stm_input_type)RX_TRM_CH_SHARING_CMD, "RX_TRM_CH_SHARING_CMD"},
  {(stm_input_type)RX_ANTENNA_TUNER_CMD, "RX_ANTENNA_TUNER_CMD"},
  {(stm_input_type)RX_RF_RSP_WUP_PREP_COMP_CMD, "RX_RF_RSP_WUP_PREP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_WUP_COMP_CMD, "RX_RF_RSP_WUP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)RX_TUNE_COMP_DELAY_CMD, "RX_TUNE_COMP_DELAY_CMD"},
  {(stm_input_type)RX_SCHEDULED_DISABLE_CMD, "RX_SCHEDULED_DISABLE_CMD"},
  {(stm_input_type)RX_MOD_REASON_CMD, "RX_MOD_REASON_CMD"},
  {(stm_input_type)RX_MOD_CRITERIA_CMD, "RX_MOD_CRITERIA_CMD"},
  {(stm_input_type)RX_MOD_DURATION_CMD, "RX_MOD_DURATION_CMD"},
  {(stm_input_type)RX_RF_RSP_SLP_COMP_CMD, "RX_RF_RSP_SLP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_SCHED_SLP_ERROR_CMD, "RX_RF_RSP_SCHED_SLP_ERROR_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCH_RX_SM_P_transitions[ SRCH_RX_SM_P_NUMBER_OF_STATES *  SRCH_RX_SM_P_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INACTIVE */
    rx_proc_request_chain,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_ignore_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    rx_proc_disable_comp,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at_in_inactive,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state WAIT_FOR_LOCK */
    rx_proc_request_chain_wait_for_lock,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_cancel_request,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at_in_inactive,    /* RX_RESERVE_AT_CMD */
    rx_proc_trm_granted_lock,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict_wfl,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state TUNE */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_exec_wakeup,    /* RX_TUNE_CMD */
    rx_proc_disable_in_tune,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    rx_proc_rf_prep_comp,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    rx_proc_rf_tune_comp,    /* RX_RF_RSP_WUP_COMP_CMD */
    rx_proc_rf_tune_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    rx_proc_rf_tune_comp_delay,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state OWNED */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_proc_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_proc_irat_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_mod_tune,    /* RX_TUNE_CMD */
    rx_proc_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    rx_proc_antenna_tuner,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    rx_proc_scheduled_disable,    /* RX_SCHEDULED_DISABLE_CMD */
    rx_proc_mod_reason,    /* RX_MOD_REASON_CMD */
    rx_proc_mod_criteria,    /* RX_MOD_CRITERIA_CMD */
    rx_proc_mod_duration,    /* RX_MOD_DURATION_CMD */
    rx_proc_rf_rsp_slp_comp_on_owned,    /* RX_RF_RSP_SLP_COMP_CMD */
    rx_proc_rf_sched_slp_error,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state OWNED_DISABLED */
    rx_proc_request_on_owned_dis,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_proc_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_proc_irat_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_mod_dis_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_dis_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    rx_proc_mod_reason,    /* RX_MOD_REASON_CMD */
    rx_proc_mod_criteria,    /* RX_MOD_CRITERIA_CMD */
    rx_proc_mod_duration,    /* RX_MOD_DURATION_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state MODIFY_TUNE_PENDING */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_proc_disable_in_tune,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state MODIFY_DISABLED_TUNE_PENDING */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

  /* Transition functions for state RELEASE_REQUESTED */
    rx_proc_request_on_release_requested,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    SRCH_RX_SM_P_ignore_input,    /* RX_ANTENNA_TUNER_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    rx_proc_rf_rsp_slp_comp,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */

};


/* State machine definition */
stm_state_machine_type SRCH_RX_SM_P =
{
  "SRCH_RX_SM_P", /* state machine name */
  65432, /* unique SM id (hash of name) */
  rx_entry_p, /* state machine entry function */
  rx_exit_p, /* state machine exit function */
  INACTIVE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SRCH_RX_SM_P_NUMBER_OF_STATES,
  SRCH_RX_SM_P_NUMBER_OF_INPUTS,
  SRCH_RX_SM_P_states,
  SRCH_RX_SM_P_inputs,
  SRCH_RX_SM_P_transitions,
};

/* End machine generated code for state machine: SRCH_RX_SM_P */
/* Begin machine generated code for state machine: SRCH_RX_SM_S */

/* Transition function prototypes */
static stm_state_type rx_proc_request_chain(void *);
static stm_state_type rx_proc_exchange_devices(void *);
static stm_state_type rx_ignore_mod_reas_dur(void *);
static stm_state_type rx_ignore_tune(void *);
static stm_state_type rx_ignore_disable(void *);
static stm_state_type rx_ignore_release(void *);
static stm_state_type rx_proc_trm_enable(void *);
static stm_state_type rx_proc_trm_disable(void *);
static stm_state_type rx_proc_disable_comp(void *);
static stm_state_type rx_proc_reserve_at_in_inactive(void *);
static stm_state_type SRCH_RX_SM_S_ignore_input(void *);
static stm_state_type rx_proc_request_chain_wait_for_lock(void *);
static stm_state_type rx_proc_cancel_request(void *);
static stm_state_type rx_proc_trm_granted_lock(void *);
static stm_state_type rx_proc_trm_conflict_wfl(void *);
static stm_state_type rx_remember_enable_div_cmd(void *);
static stm_state_type rx_proc_request_on_owned(void *);
static stm_state_type rx_proc_release(void *);
static stm_state_type rx_proc_exec_wakeup(void *);
static stm_state_type rx_proc_disable_in_tune(void *);
static stm_state_type rx_remember_request_disable_div(void *);
static stm_state_type rx_proc_trm_conflict(void *);
static stm_state_type rx_proc_rf_prep_comp(void *);
static stm_state_type rx_proc_rf_tune_comp(void *);
static stm_state_type rx_proc_rf_tune_comp_delay(void *);
static stm_state_type rx_proc_request_enable_div(void *);
static stm_state_type rx_proc_reserve_at(void *);
static stm_state_type rx_proc_disable(void *);
static stm_state_type rx_proc_scheduled_disable(void *);
static stm_state_type rx_proc_request_disable_div(void *);
static stm_state_type rx_proc_mod_tune(void *);
static stm_state_type rx_proc_mod_reason(void *);
static stm_state_type rx_proc_mod_criteria(void *);
static stm_state_type rx_proc_mod_duration(void *);
static stm_state_type rx_proc_mod_reas_dur(void *);
static stm_state_type rx_proc_irat_mod_reas_dur(void *);
static stm_state_type rx_proc_disable_div_comp(void *);
static stm_state_type rx_proc_rf_rsp_slp_comp_on_owned(void *);
static stm_state_type rx_proc_rf_sched_slp_error(void *);
static stm_state_type rx_proc_request_on_owned_dis(void *);
static stm_state_type rx_proc_mod_dis_tune(void *);
static stm_state_type rx_proc_trm_mod_tune_granted(void *);
static stm_state_type rx_proc_enable_div_comp(void *);
static stm_state_type rx_remember_disable_div_comp(void *);
static stm_state_type rx_proc_mod_ignore(void *);
static stm_state_type rx_proc_request_on_release_requested(void *);
static stm_state_type rx_proc_rf_rsp_slp_comp(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SRCH_RX_SM_S_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void rx_entry_s(stm_group_type *group);
static void rx_exit_s(stm_group_type *group);


/* State entry/exit function prototypes */
static void rx_sm_inactive_entry_s(void *payload, stm_state_type previous_state);
static void rx_sm_tune_entry_s(void *payload, stm_state_type previous_state);
static void rx_sm_owned_entry_s(void *payload, stm_state_type previous_state);
static void rx_sm_owned_exit_s(void *payload, stm_state_type previous_state);
static void rx_sm_owned_dis_entry_s(void *payload, stm_state_type previous_state);
static void rx_sm_owned_dis_exit_s(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCH_RX_SM_S_NUMBER_OF_STATES 8
#define SRCH_RX_SM_S_NUMBER_OF_INPUTS 29


/* State enumeration */
enum
{
  INACTIVE_S,
  WAIT_FOR_LOCK_S,
  TUNE_S,
  OWNED_S,
  OWNED_DISABLED_S,
  MODIFY_TUNE_PENDING_S,
  MODIFY_DISABLED_TUNE_PENDING_S,
  RELEASE_REQUESTED_S,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCH_RX_SM_S_states[ SRCH_RX_SM_S_NUMBER_OF_STATES ] =
{
  {"INACTIVE_S", rx_sm_inactive_entry_s, NULL},
  {"WAIT_FOR_LOCK_S", NULL, NULL},
  {"TUNE_S", rx_sm_tune_entry_s, NULL},
  {"OWNED_S", rx_sm_owned_entry_s, rx_sm_owned_exit_s},
  {"OWNED_DISABLED_S", rx_sm_owned_dis_entry_s, rx_sm_owned_dis_exit_s},
  {"MODIFY_TUNE_PENDING_S", NULL, NULL},
  {"MODIFY_DISABLED_TUNE_PENDING_S", NULL, NULL},
  {"RELEASE_REQUESTED_S", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCH_RX_SM_S_inputs[ SRCH_RX_SM_S_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)RX_REQ_AND_NOTIFY_CMD, "RX_REQ_AND_NOTIFY_CMD"},
  {(stm_input_type)RX_EXCHANGE_DEVICES_CMD, "RX_EXCHANGE_DEVICES_CMD"},
  {(stm_input_type)RX_MOD_REAS_DUR_CMD, "RX_MOD_REAS_DUR_CMD"},
  {(stm_input_type)RX_MOD_IRAT_REAS_DUR_CMD, "RX_MOD_IRAT_REAS_DUR_CMD"},
  {(stm_input_type)RX_TUNE_CMD, "RX_TUNE_CMD"},
  {(stm_input_type)RX_DISABLE_CMD, "RX_DISABLE_CMD"},
  {(stm_input_type)RX_RELEASE_CMD, "RX_RELEASE_CMD"},
  {(stm_input_type)RX_TRM_CH_ENABLE_CMD, "RX_TRM_CH_ENABLE_CMD"},
  {(stm_input_type)RX_TRM_CH_DISABLE_CMD, "RX_TRM_CH_DISABLE_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)RX_RESERVE_AT_CMD, "RX_RESERVE_AT_CMD"},
  {(stm_input_type)RX_TRM_CH_GRANTED_CMD, "RX_TRM_CH_GRANTED_CMD"},
  {(stm_input_type)RX_TRM_CH_MODIFIED_CMD, "RX_TRM_CH_MODIFIED_CMD"},
  {(stm_input_type)RX_TRM_CH_CONFLICT_CMD, "RX_TRM_CH_CONFLICT_CMD"},
  {(stm_input_type)RX_TRM_CH_SHARING_CMD, "RX_TRM_CH_SHARING_CMD"},
  {(stm_input_type)RX_REQ_ENABLE_DIV_CMD, "RX_REQ_ENABLE_DIV_CMD"},
  {(stm_input_type)RX_REQ_DISABLE_DIV_CMD, "RX_REQ_DISABLE_DIV_CMD"},
  {(stm_input_type)RX_RF_RSP_WUP_PREP_COMP_CMD, "RX_RF_RSP_WUP_PREP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_WUP_COMP_CMD, "RX_RF_RSP_WUP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)RX_TUNE_COMP_DELAY_CMD, "RX_TUNE_COMP_DELAY_CMD"},
  {(stm_input_type)RX_SCHEDULED_DISABLE_CMD, "RX_SCHEDULED_DISABLE_CMD"},
  {(stm_input_type)RX_MOD_REASON_CMD, "RX_MOD_REASON_CMD"},
  {(stm_input_type)RX_MOD_CRITERIA_CMD, "RX_MOD_CRITERIA_CMD"},
  {(stm_input_type)RX_MOD_DURATION_CMD, "RX_MOD_DURATION_CMD"},
  {(stm_input_type)RX_RF_DISABLE_DIV_COMP_CMD, "RX_RF_DISABLE_DIV_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_SLP_COMP_CMD, "RX_RF_RSP_SLP_COMP_CMD"},
  {(stm_input_type)RX_RF_RSP_SCHED_SLP_ERROR_CMD, "RX_RF_RSP_SCHED_SLP_ERROR_CMD"},
  {(stm_input_type)RX_RF_ENABLE_DIV_COMP_CMD, "RX_RF_ENABLE_DIV_COMP_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCH_RX_SM_S_transitions[ SRCH_RX_SM_S_NUMBER_OF_STATES *  SRCH_RX_SM_S_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INACTIVE_S */
    rx_proc_request_chain,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_ignore_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    rx_proc_disable_comp,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at_in_inactive,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    NULL,    /* RX_REQ_ENABLE_DIV_CMD */
    NULL,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state WAIT_FOR_LOCK_S */
    rx_proc_request_chain_wait_for_lock,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_cancel_request,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at_in_inactive,    /* RX_RESERVE_AT_CMD */
    rx_proc_trm_granted_lock,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict_wfl,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    rx_remember_enable_div_cmd,    /* RX_REQ_ENABLE_DIV_CMD */
    NULL,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state TUNE_S */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_exec_wakeup,    /* RX_TUNE_CMD */
    rx_proc_disable_in_tune,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    NULL,    /* RX_REQ_ENABLE_DIV_CMD */
    rx_remember_request_disable_div,    /* RX_REQ_DISABLE_DIV_CMD */
    rx_proc_rf_prep_comp,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    rx_proc_rf_tune_comp,    /* RX_RF_RSP_WUP_COMP_CMD */
    rx_proc_rf_tune_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    rx_proc_rf_tune_comp_delay,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state OWNED_S */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_proc_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_proc_irat_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_mod_tune,    /* RX_TUNE_CMD */
    rx_proc_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    rx_proc_request_enable_div,    /* RX_REQ_ENABLE_DIV_CMD */
    rx_proc_request_disable_div,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    rx_proc_scheduled_disable,    /* RX_SCHEDULED_DISABLE_CMD */
    rx_proc_mod_reason,    /* RX_MOD_REASON_CMD */
    rx_proc_mod_criteria,    /* RX_MOD_CRITERIA_CMD */
    rx_proc_mod_duration,    /* RX_MOD_DURATION_CMD */
    rx_proc_disable_div_comp,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    rx_proc_rf_rsp_slp_comp_on_owned,    /* RX_RF_RSP_SLP_COMP_CMD */
    rx_proc_rf_sched_slp_error,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state OWNED_DISABLED_S */
    rx_proc_request_on_owned_dis,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_proc_mod_ignore,    /* RX_MOD_REAS_DUR_CMD */
    rx_proc_mod_ignore,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_proc_mod_dis_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    rx_proc_request_enable_div,    /* RX_REQ_ENABLE_DIV_CMD */
    rx_remember_request_disable_div,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    rx_proc_mod_ignore,    /* RX_MOD_REASON_CMD */
    rx_proc_mod_ignore,    /* RX_MOD_CRITERIA_CMD */
    rx_proc_mod_ignore,    /* RX_MOD_DURATION_CMD */
    rx_remember_disable_div_comp,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    rx_proc_enable_div_comp,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state MODIFY_TUNE_PENDING_S */
    rx_proc_request_on_owned_dis,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_proc_disable_in_tune,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    NULL,    /* RX_REQ_ENABLE_DIV_CMD */
    NULL,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state MODIFY_DISABLED_TUNE_PENDING_S */
    rx_proc_request_on_owned,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_GRANTED_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    NULL,    /* RX_REQ_ENABLE_DIV_CMD */
    NULL,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    NULL,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

  /* Transition functions for state RELEASE_REQUESTED_S */
    rx_proc_request_on_release_requested,    /* RX_REQ_AND_NOTIFY_CMD */
    rx_proc_exchange_devices,    /* RX_EXCHANGE_DEVICES_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_REAS_DUR_CMD */
    rx_ignore_mod_reas_dur,    /* RX_MOD_IRAT_REAS_DUR_CMD */
    rx_ignore_tune,    /* RX_TUNE_CMD */
    rx_ignore_disable,    /* RX_DISABLE_CMD */
    rx_proc_release,    /* RX_RELEASE_CMD */
    rx_proc_trm_enable,    /* RX_TRM_CH_ENABLE_CMD */
    rx_proc_trm_disable,    /* RX_TRM_CH_DISABLE_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    rx_proc_reserve_at,    /* RX_RESERVE_AT_CMD */
    rx_proc_trm_mod_tune_granted,    /* RX_TRM_CH_GRANTED_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_MODIFIED_CMD */
    rx_proc_trm_conflict,    /* RX_TRM_CH_CONFLICT_CMD */
    SRCH_RX_SM_S_ignore_input,    /* RX_TRM_CH_SHARING_CMD */
    NULL,    /* RX_REQ_ENABLE_DIV_CMD */
    NULL,    /* RX_REQ_DISABLE_DIV_CMD */
    NULL,    /* RX_RF_RSP_WUP_PREP_COMP_CMD */
    NULL,    /* RX_RF_RSP_WUP_COMP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_DELAY_CMD */
    NULL,    /* RX_SCHEDULED_DISABLE_CMD */
    NULL,    /* RX_MOD_REASON_CMD */
    NULL,    /* RX_MOD_CRITERIA_CMD */
    NULL,    /* RX_MOD_DURATION_CMD */
    NULL,    /* RX_RF_DISABLE_DIV_COMP_CMD */
    rx_proc_rf_rsp_slp_comp,    /* RX_RF_RSP_SLP_COMP_CMD */
    NULL,    /* RX_RF_RSP_SCHED_SLP_ERROR_CMD */
    NULL,    /* RX_RF_ENABLE_DIV_COMP_CMD */

};


/* State machine definition */
stm_state_machine_type SRCH_RX_SM_S =
{
  "SRCH_RX_SM_S", /* state machine name */
  65432, /* unique SM id (hash of name) */
  rx_entry_s, /* state machine entry function */
  rx_exit_s, /* state machine exit function */
  INACTIVE_S, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SRCH_RX_SM_S_NUMBER_OF_STATES,
  SRCH_RX_SM_S_NUMBER_OF_INPUTS,
  SRCH_RX_SM_S_states,
  SRCH_RX_SM_S_inputs,
  SRCH_RX_SM_S_transitions,
};

/* End machine generated code for state machine: SRCH_RX_SM_S */

