###############################################################################
#
#    srch_tram_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srch_tram_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: TRAM_SM
# State machine:current state:input:next state                      fgcolor bgcolor

TRAM:READY:TRAM_REQUEST:READY                                f4f2 0 00 0    @purple @white
TRAM:READY:TRAM_REQUEST:TRANS                                f4f2 0 00 1    @purple @white

TRAM:READY:TRAM_TUNE:READY                                   f4f2 0 01 0    @purple @white
TRAM:READY:TRAM_TUNE:TRANS                                   f4f2 0 01 1    @purple @white

TRAM:READY:TRAM_MODIFY:READY                                 f4f2 0 02 0    @purple @white
TRAM:READY:TRAM_MODIFY:TRANS                                 f4f2 0 02 1    @purple @white

TRAM:READY:TRAM_DISABLE:READY                                f4f2 0 03 0    @purple @white
TRAM:READY:TRAM_DISABLE:TRANS                                f4f2 0 03 1    @purple @white

TRAM:READY:TRAM_RELEASE:READY                                f4f2 0 04 0    @purple @white
TRAM:READY:TRAM_RELEASE:TRANS                                f4f2 0 04 1    @purple @white

TRAM:READY:TRAM_PENDING:READY                                f4f2 0 05 0    @purple @white
TRAM:READY:TRAM_PENDING:TRANS                                f4f2 0 05 1    @purple @white

TRAM:READY:RX_CH_DENIED:READY                                f4f2 0 06 0    @purple @white
TRAM:READY:RX_CH_DENIED:TRANS                                f4f2 0 06 1    @purple @white

TRAM:READY:RX_MOD_DENIED:READY                               f4f2 0 07 0    @purple @white
TRAM:READY:RX_MOD_DENIED:TRANS                               f4f2 0 07 1    @purple @white

TRAM:READY:TRAM_DISPATCH:READY                               f4f2 0 08 0    @purple @white
TRAM:READY:TRAM_DISPATCH:TRANS                               f4f2 0 08 1    @purple @white

TRAM:TRANS:TRAM_REQUEST:READY                                f4f2 1 00 0    @purple @white
TRAM:TRANS:TRAM_REQUEST:TRANS                                f4f2 1 00 1    @purple @white

TRAM:TRANS:TRAM_TUNE:READY                                   f4f2 1 01 0    @purple @white
TRAM:TRANS:TRAM_TUNE:TRANS                                   f4f2 1 01 1    @purple @white

TRAM:TRANS:TRAM_MODIFY:READY                                 f4f2 1 02 0    @purple @white
TRAM:TRANS:TRAM_MODIFY:TRANS                                 f4f2 1 02 1    @purple @white

TRAM:TRANS:TRAM_DISABLE:READY                                f4f2 1 03 0    @purple @white
TRAM:TRANS:TRAM_DISABLE:TRANS                                f4f2 1 03 1    @purple @white

TRAM:TRANS:TRAM_RELEASE:READY                                f4f2 1 04 0    @purple @white
TRAM:TRANS:TRAM_RELEASE:TRANS                                f4f2 1 04 1    @purple @white

TRAM:TRANS:TRAM_PENDING:READY                                f4f2 1 05 0    @purple @white
TRAM:TRANS:TRAM_PENDING:TRANS                                f4f2 1 05 1    @purple @white

TRAM:TRANS:RX_CH_DENIED:READY                                f4f2 1 06 0    @purple @white
TRAM:TRANS:RX_CH_DENIED:TRANS                                f4f2 1 06 1    @purple @white

TRAM:TRANS:RX_MOD_DENIED:READY                               f4f2 1 07 0    @purple @white
TRAM:TRANS:RX_MOD_DENIED:TRANS                               f4f2 1 07 1    @purple @white

TRAM:TRANS:TRAM_DISPATCH:READY                               f4f2 1 08 0    @purple @white
TRAM:TRANS:TRAM_DISPATCH:TRANS                               f4f2 1 08 1    @purple @white



# End machine generated TLA code for state machine: TRAM_SM

