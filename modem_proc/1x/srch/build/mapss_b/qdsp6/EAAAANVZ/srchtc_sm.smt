/*=============================================================================

  srchtc_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchtc_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: TC_SM */

/* Transition function prototypes */
static stm_state_type tc_process_cdma(void *);
static stm_state_type tc_process_idle(void *);
static stm_state_type tc_process_idle_entry_div_disable(void *);
static stm_state_type tc_process_idle_entry_complete(void *);
static stm_state_type tc_process_parm(void *);
static stm_state_type tc_process_meas(void *);
static stm_state_type tc_process_scrm_meas(void *);
static stm_state_type tc_process_fpc_meas(void *);
static stm_state_type tc_process_tc_add_sch(void *);
static stm_state_type tc_process_tc_nset(void *);
static stm_state_type tc_process_ppsm(void *);
static stm_state_type tc_process_start_ppsm(void *);
static stm_state_type tc_process_stop_ppsm(void *);
static stm_state_type tc_process_start_cfs(void *);
static stm_state_type tc_process_stop_cfs(void *);
static stm_state_type tc_process_rif_hho_failure(void *);
static stm_state_type tc_process_rif_hho_success(void *);
static stm_state_type tc_process_tc_find_strong_pilots(void *);
static stm_state_type tc_process_mc_gps_visit_okay(void *);
static stm_state_type tc_process_mc_resume_cdma(void *);
static stm_state_type tc_process_rand_bits(void *);
static stm_state_type tc_process_dump(void *);
static stm_state_type tc_process_lost_dump(void *);
static stm_state_type tc_process_aset_update_pre_tune(void *);
static stm_state_type tc_process_aset_update_post_tune(void *);
static stm_state_type tc_process_cfs_action(void *);
static stm_state_type tc_process_entry_tune(void *);
static stm_state_type tc_process_action_time(void *);
static stm_state_type tc_process_do_srch(void *);
static stm_state_type tc_process_govern_timer(void *);
static stm_state_type tc_process_freq_timer(void *);
static stm_state_type tc_process_ref_timer(void *);
static stm_state_type tc_process_ctr_timer(void *);
static stm_state_type tc_process_frame_event(void *);
static stm_state_type tc_process_rssi_timer(void *);
static stm_state_type tc_process_ppssm_timer(void *);
static stm_state_type tc_process_rf_report_timer(void *);
static stm_state_type tc_process_ns_triage_timer(void *);
static stm_state_type tc_process_ns_ctr_timer(void *);
static stm_state_type tc_process_tune_done(void *);


/* State Machine entry/exit function prototypes */
static void tc_entry(stm_group_type *group);
static void tc_exit(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define TC_SM_NUMBER_OF_STATES 1
#define TC_SM_NUMBER_OF_INPUTS 40


/* State enumeration */
enum
{
  ONLINE_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  TC_SM_states[ TC_SM_NUMBER_OF_STATES ] =
{
  {"ONLINE_STATE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  TC_SM_inputs[ TC_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_IDLE_F, "SRCH_IDLE_F"},
  {(stm_input_type)IDLE_ENTRY_DIV_DISABLED_CMD, "IDLE_ENTRY_DIV_DISABLED_CMD"},
  {(stm_input_type)IDLE_ENTRY_COMPLETE_CMD, "IDLE_ENTRY_COMPLETE_CMD"},
  {(stm_input_type)SRCH_PARM_F, "SRCH_PARM_F"},
  {(stm_input_type)SRCH_MEAS_F, "SRCH_MEAS_F"},
  {(stm_input_type)SRCH_SCRM_MEAS_F, "SRCH_SCRM_MEAS_F"},
  {(stm_input_type)SRCH_FPC_MEAS_F, "SRCH_FPC_MEAS_F"},
  {(stm_input_type)SRCH_TC_ADD_SCH_F, "SRCH_TC_ADD_SCH_F"},
  {(stm_input_type)SRCH_TC_NSET_F, "SRCH_TC_NSET_F"},
  {(stm_input_type)SRCH_PPSM_F, "SRCH_PPSM_F"},
  {(stm_input_type)SRCH_START_PPSM_F, "SRCH_START_PPSM_F"},
  {(stm_input_type)SRCH_STOP_PPSM_F, "SRCH_STOP_PPSM_F"},
  {(stm_input_type)SRCH_START_CFS_F, "SRCH_START_CFS_F"},
  {(stm_input_type)SRCH_STOP_CFS_F, "SRCH_STOP_CFS_F"},
  {(stm_input_type)SRCH_RIF_HHO_FAILURE_F, "SRCH_RIF_HHO_FAILURE_F"},
  {(stm_input_type)SRCH_RIF_HHO_SUCCESS_F, "SRCH_RIF_HHO_SUCCESS_F"},
  {(stm_input_type)SRCH_TC_FIND_STRONG_PILOTS_F, "SRCH_TC_FIND_STRONG_PILOTS_F"},
  {(stm_input_type)SRCH_MC_GPS_VISIT_OKAY_F, "SRCH_MC_GPS_VISIT_OKAY_F"},
  {(stm_input_type)SRCH_MC_RESUME_CDMA_F, "SRCH_MC_RESUME_CDMA_F"},
  {(stm_input_type)SRCH_RAND_BITS_F, "SRCH_RAND_BITS_F"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)ASET_UPDATE_PRE_TUNE_CMD, "ASET_UPDATE_PRE_TUNE_CMD"},
  {(stm_input_type)ASET_UPDATE_POST_TUNE_CMD, "ASET_UPDATE_POST_TUNE_CMD"},
  {(stm_input_type)CFS_ACTION_CMD, "CFS_ACTION_CMD"},
  {(stm_input_type)ENTRY_TUNE_CMD, "ENTRY_TUNE_CMD"},
  {(stm_input_type)ACTION_TIME_CMD, "ACTION_TIME_CMD"},
  {(stm_input_type)DO_SRCH_CMD, "DO_SRCH_CMD"},
  {(stm_input_type)GOVERN_TIMER_CMD, "GOVERN_TIMER_CMD"},
  {(stm_input_type)AFC_RPT_TIMER_CMD, "AFC_RPT_TIMER_CMD"},
  {(stm_input_type)REF_TIMER_CMD, "REF_TIMER_CMD"},
  {(stm_input_type)CTR_TIMER_CMD, "CTR_TIMER_CMD"},
  {(stm_input_type)SYNTH_FRAME_STROBE_CMD, "SYNTH_FRAME_STROBE_CMD"},
  {(stm_input_type)RSSI_TIMER_CMD, "RSSI_TIMER_CMD"},
  {(stm_input_type)PPSMM_TIMER_CMD, "PPSMM_TIMER_CMD"},
  {(stm_input_type)RF_REPORT_TIMER_CMD, "RF_REPORT_TIMER_CMD"},
  {(stm_input_type)NS_TRIAGE_TIMER_CMD, "NS_TRIAGE_TIMER_CMD"},
  {(stm_input_type)NS_CTR_TIMER_CMD, "NS_CTR_TIMER_CMD"},
  {(stm_input_type)TUNE_DONE_CMD, "TUNE_DONE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  TC_SM_transitions[ TC_SM_NUMBER_OF_STATES *  TC_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state ONLINE_STATE */
    tc_process_cdma,    /* SRCH_CDMA_F */
    tc_process_idle,    /* SRCH_IDLE_F */
    tc_process_idle_entry_div_disable,    /* IDLE_ENTRY_DIV_DISABLED_CMD */
    tc_process_idle_entry_complete,    /* IDLE_ENTRY_COMPLETE_CMD */
    tc_process_parm,    /* SRCH_PARM_F */
    tc_process_meas,    /* SRCH_MEAS_F */
    tc_process_scrm_meas,    /* SRCH_SCRM_MEAS_F */
    tc_process_fpc_meas,    /* SRCH_FPC_MEAS_F */
    tc_process_tc_add_sch,    /* SRCH_TC_ADD_SCH_F */
    tc_process_tc_nset,    /* SRCH_TC_NSET_F */
    tc_process_ppsm,    /* SRCH_PPSM_F */
    tc_process_start_ppsm,    /* SRCH_START_PPSM_F */
    tc_process_stop_ppsm,    /* SRCH_STOP_PPSM_F */
    tc_process_start_cfs,    /* SRCH_START_CFS_F */
    tc_process_stop_cfs,    /* SRCH_STOP_CFS_F */
    tc_process_rif_hho_failure,    /* SRCH_RIF_HHO_FAILURE_F */
    tc_process_rif_hho_success,    /* SRCH_RIF_HHO_SUCCESS_F */
    tc_process_tc_find_strong_pilots,    /* SRCH_TC_FIND_STRONG_PILOTS_F */
    tc_process_mc_gps_visit_okay,    /* SRCH_MC_GPS_VISIT_OKAY_F */
    tc_process_mc_resume_cdma,    /* SRCH_MC_RESUME_CDMA_F */
    tc_process_rand_bits,    /* SRCH_RAND_BITS_F */
    tc_process_dump,    /* SRCH_DUMP_CMD */
    tc_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    tc_process_aset_update_pre_tune,    /* ASET_UPDATE_PRE_TUNE_CMD */
    tc_process_aset_update_post_tune,    /* ASET_UPDATE_POST_TUNE_CMD */
    tc_process_cfs_action,    /* CFS_ACTION_CMD */
    tc_process_entry_tune,    /* ENTRY_TUNE_CMD */
    tc_process_action_time,    /* ACTION_TIME_CMD */
    tc_process_do_srch,    /* DO_SRCH_CMD */
    tc_process_govern_timer,    /* GOVERN_TIMER_CMD */
    tc_process_freq_timer,    /* AFC_RPT_TIMER_CMD */
    tc_process_ref_timer,    /* REF_TIMER_CMD */
    tc_process_ctr_timer,    /* CTR_TIMER_CMD */
    tc_process_frame_event,    /* SYNTH_FRAME_STROBE_CMD */
    tc_process_rssi_timer,    /* RSSI_TIMER_CMD */
    tc_process_ppssm_timer,    /* PPSMM_TIMER_CMD */
    tc_process_rf_report_timer,    /* RF_REPORT_TIMER_CMD */
    tc_process_ns_triage_timer,    /* NS_TRIAGE_TIMER_CMD */
    tc_process_ns_ctr_timer,    /* NS_CTR_TIMER_CMD */
    tc_process_tune_done,    /* TUNE_DONE_CMD */

};


/* State machine definition */
stm_state_machine_type TC_SM =
{
  "TC_SM", /* state machine name */
  45974, /* unique SM id (hash of name) */
  tc_entry, /* state machine entry function */
  tc_exit, /* state machine exit function */
  ONLINE_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  TC_SM_NUMBER_OF_STATES,
  TC_SM_NUMBER_OF_INPUTS,
  TC_SM_states,
  TC_SM_inputs,
  TC_SM_transitions,
};

/* End machine generated code for state machine: TC_SM */
/* Begin machine generated code for state machine group: SRCH_TC_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean srchtc_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_TC_GROUP_sm_list[ 2 ] =
{
  &TC_SM,
  &TCG_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_TC_GROUP_sig_list[ 1 ] =
{
  SRCH_CMD_Q_SIG,
};


/* State machine group definition */
stm_group_type SRCH_TC_GROUP =
{
  "SRCH_TC_GROUP", /* state machine group name */
  SRCH_TC_GROUP_sig_list, /* signal mapping table */
  1, /* number of signals in mapping table */
  srchtc_sig_mapper, /* signal mapper function */
  SRCH_TC_GROUP_sm_list, /* state machine table */
  2, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_TC_GROUP */

