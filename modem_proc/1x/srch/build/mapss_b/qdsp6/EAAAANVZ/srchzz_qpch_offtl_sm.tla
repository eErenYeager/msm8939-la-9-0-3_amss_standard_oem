###############################################################################
#
#    srchzz_qpch_offtl_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_qpch_offtl_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_QPCH_OFFTL_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:DOZE                      1be0 0 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:SLEEP                     1be0 0 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:WAKEUP                    1be0 0 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:RECORD                    1be0 0 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:RECORD_FAILED             1be0 0 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:REACQ                     1be0 0 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:START_SLEEP:DEMOD                     1be0 0 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:ABORT:DOZE                            1be0 0 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:SLEEP                           1be0 0 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:WAKEUP                          1be0 0 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:RECORD                          1be0 0 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:RECORD_FAILED                   1be0 0 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:REACQ                           1be0 0 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ABORT:DEMOD                           1be0 0 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:DOZE                       1be0 0 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:SLEEP                      1be0 0 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:WAKEUP                     1be0 0 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:RECORD                     1be0 0 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:RECORD_FAILED              1be0 0 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:REACQ                      1be0 0 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP_NOW:DEMOD                      1be0 0 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:DOZE                    1be0 0 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:SLEEP                   1be0 0 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:WAKEUP                  1be0 0 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:RECORD                  1be0 0 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:RECORD_FAILED           1be0 0 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:REACQ                   1be0 0 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_GRANTED:DEMOD                   1be0 0 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:DOZE                     1be0 0 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:SLEEP                    1be0 0 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:WAKEUP                   1be0 0 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:RECORD                   1be0 0 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:RECORD_FAILED            1be0 0 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:REACQ                    1be0 0 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_CH_DENIED:DEMOD                    1be0 0 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:DOZE                       1be0 0 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:SLEEP                      1be0 0 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:WAKEUP                     1be0 0 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:RECORD                     1be0 0 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:RECORD_FAILED              1be0 0 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:REACQ                      1be0 0 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:NO_RF_LOCK:DEMOD                      1be0 0 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:DOZE                           1be0 0 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:SLEEP                          1be0 0 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:WAKEUP                         1be0 0 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:RECORD                         1be0 0 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:RECORD_FAILED                  1be0 0 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:REACQ                          1be0 0 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:WAKEUP:DEMOD                          1be0 0 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:DOZE              1be0 0 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:SLEEP             1be0 0 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:WAKEUP            1be0 0 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:RECORD            1be0 0 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:RECORD_FAILED     1be0 0 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:REACQ             1be0 0 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_RSP_TUNE_COMP:DEMOD             1be0 0 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:DOZE                           1be0 0 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:SLEEP                          1be0 0 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:WAKEUP                         1be0 0 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:RECORD                         1be0 0 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:RECORD_FAILED                  1be0 0 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:REACQ                          1be0 0 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:CX8_ON:DEMOD                          1be0 0 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:DOZE               1be0 0 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:SLEEP              1be0 0 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:WAKEUP             1be0 0 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:RECORD             1be0 0 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:RECORD_FAILED      1be0 0 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:REACQ              1be0 0 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLE_COMP:DEMOD              1be0 0 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:DOZE                     1be0 0 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:SLEEP                    1be0 0 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:WAKEUP                   1be0 0 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:RECORD                   1be0 0 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:RECORD_FAILED            1be0 0 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:REACQ                    1be0 0 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_TUNE_COMP:DEMOD                    1be0 0 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:DOZE                   1be0 0 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:SLEEP                  1be0 0 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:WAKEUP                 1be0 0 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:RECORD                 1be0 0 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:RECORD_FAILED          1be0 0 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:REACQ                  1be0 0 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED:DEMOD                  1be0 0 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:DOZE              1be0 0 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:SLEEP             1be0 0 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:WAKEUP            1be0 0 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:RECORD            1be0 0 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:RECORD_FAILED     1be0 0 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:REACQ             1be0 0 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:FREQ_TRACK_OFF_DONE:DEMOD             1be0 0 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:DOZE               1be0 0 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:SLEEP              1be0 0 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:WAKEUP             1be0 0 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:RECORD             1be0 0 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:RECORD_FAILED      1be0 0 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:REACQ              1be0 0 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:RX_RF_DISABLED_ERR:DEMOD              1be0 0 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:DOZE                    1be0 0 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:SLEEP                   1be0 0 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:WAKEUP                  1be0 0 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:RECORD                  1be0 0 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:RECORD_FAILED           1be0 0 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:REACQ                   1be0 0 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:ADJUST_TIMING:DEMOD                   1be0 0 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:DOZE                  1be0 0 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:SLEEP                 1be0 0 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:WAKEUP                1be0 0 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:RECORD                1be0 0 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:RECORD_FAILED         1be0 0 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:REACQ                 1be0 0 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:DOZE:QPCH_DEMOD_DONE:DEMOD                 1be0 0 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:DOZE                     1be0 1 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:SLEEP                    1be0 1 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:WAKEUP                   1be0 1 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:RECORD                   1be0 1 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:RECORD_FAILED            1be0 1 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:REACQ                    1be0 1 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:START_SLEEP:DEMOD                    1be0 1 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:DOZE                           1be0 1 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:SLEEP                          1be0 1 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:WAKEUP                         1be0 1 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:RECORD                         1be0 1 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:RECORD_FAILED                  1be0 1 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:REACQ                          1be0 1 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ABORT:DEMOD                          1be0 1 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:DOZE                      1be0 1 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:SLEEP                     1be0 1 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:WAKEUP                    1be0 1 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:RECORD                    1be0 1 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:RECORD_FAILED             1be0 1 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:REACQ                     1be0 1 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP_NOW:DEMOD                     1be0 1 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:DOZE                   1be0 1 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:SLEEP                  1be0 1 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:WAKEUP                 1be0 1 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:RECORD                 1be0 1 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:RECORD_FAILED          1be0 1 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:REACQ                  1be0 1 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_GRANTED:DEMOD                  1be0 1 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:DOZE                    1be0 1 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:SLEEP                   1be0 1 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:WAKEUP                  1be0 1 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:RECORD                  1be0 1 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:RECORD_FAILED           1be0 1 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:REACQ                   1be0 1 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_CH_DENIED:DEMOD                   1be0 1 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:DOZE                      1be0 1 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:SLEEP                     1be0 1 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:WAKEUP                    1be0 1 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:RECORD                    1be0 1 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:RECORD_FAILED             1be0 1 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:REACQ                     1be0 1 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:NO_RF_LOCK:DEMOD                     1be0 1 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:DOZE                          1be0 1 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:SLEEP                         1be0 1 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:WAKEUP                        1be0 1 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:RECORD                        1be0 1 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:RECORD_FAILED                 1be0 1 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:REACQ                         1be0 1 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:WAKEUP:DEMOD                         1be0 1 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:DOZE             1be0 1 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:SLEEP            1be0 1 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:WAKEUP           1be0 1 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:RECORD           1be0 1 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:RECORD_FAILED    1be0 1 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:REACQ            1be0 1 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_RSP_TUNE_COMP:DEMOD            1be0 1 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:DOZE                          1be0 1 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:SLEEP                         1be0 1 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:WAKEUP                        1be0 1 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:RECORD                        1be0 1 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:RECORD_FAILED                 1be0 1 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:REACQ                         1be0 1 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:CX8_ON:DEMOD                         1be0 1 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:DOZE              1be0 1 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:SLEEP             1be0 1 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:WAKEUP            1be0 1 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:RECORD            1be0 1 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:RECORD_FAILED     1be0 1 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:REACQ             1be0 1 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLE_COMP:DEMOD             1be0 1 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:DOZE                    1be0 1 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:SLEEP                   1be0 1 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:WAKEUP                  1be0 1 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:RECORD                  1be0 1 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:RECORD_FAILED           1be0 1 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:REACQ                   1be0 1 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_TUNE_COMP:DEMOD                   1be0 1 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:DOZE                  1be0 1 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:SLEEP                 1be0 1 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:WAKEUP                1be0 1 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:RECORD                1be0 1 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:RECORD_FAILED         1be0 1 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:REACQ                 1be0 1 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED:DEMOD                 1be0 1 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:DOZE             1be0 1 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:SLEEP            1be0 1 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:WAKEUP           1be0 1 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:RECORD           1be0 1 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:RECORD_FAILED    1be0 1 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:REACQ            1be0 1 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:FREQ_TRACK_OFF_DONE:DEMOD            1be0 1 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:DOZE              1be0 1 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:SLEEP             1be0 1 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:WAKEUP            1be0 1 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:RECORD            1be0 1 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:RECORD_FAILED     1be0 1 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:REACQ             1be0 1 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:RX_RF_DISABLED_ERR:DEMOD             1be0 1 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:DOZE                   1be0 1 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:SLEEP                  1be0 1 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:WAKEUP                 1be0 1 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:RECORD                 1be0 1 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:RECORD_FAILED          1be0 1 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:REACQ                  1be0 1 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:ADJUST_TIMING:DEMOD                  1be0 1 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:DOZE                 1be0 1 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:SLEEP                1be0 1 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:WAKEUP               1be0 1 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:RECORD               1be0 1 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:RECORD_FAILED        1be0 1 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:REACQ                1be0 1 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:SLEEP:QPCH_DEMOD_DONE:DEMOD                1be0 1 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:DOZE                    1be0 2 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:SLEEP                   1be0 2 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:WAKEUP                  1be0 2 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:RECORD                  1be0 2 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:RECORD_FAILED           1be0 2 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:REACQ                   1be0 2 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:START_SLEEP:DEMOD                   1be0 2 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:DOZE                          1be0 2 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:SLEEP                         1be0 2 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:WAKEUP                        1be0 2 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:RECORD                        1be0 2 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:RECORD_FAILED                 1be0 2 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:REACQ                         1be0 2 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ABORT:DEMOD                         1be0 2 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:DOZE                     1be0 2 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:SLEEP                    1be0 2 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:WAKEUP                   1be0 2 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:RECORD                   1be0 2 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:RECORD_FAILED            1be0 2 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:REACQ                    1be0 2 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP_NOW:DEMOD                    1be0 2 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:DOZE                  1be0 2 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:SLEEP                 1be0 2 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:WAKEUP                1be0 2 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:RECORD                1be0 2 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:RECORD_FAILED         1be0 2 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:REACQ                 1be0 2 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_GRANTED:DEMOD                 1be0 2 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:DOZE                   1be0 2 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:SLEEP                  1be0 2 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:WAKEUP                 1be0 2 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:RECORD                 1be0 2 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:RECORD_FAILED          1be0 2 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:REACQ                  1be0 2 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_CH_DENIED:DEMOD                  1be0 2 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:DOZE                     1be0 2 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:SLEEP                    1be0 2 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:WAKEUP                   1be0 2 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:RECORD                   1be0 2 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:RECORD_FAILED            1be0 2 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:REACQ                    1be0 2 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:NO_RF_LOCK:DEMOD                    1be0 2 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:DOZE                         1be0 2 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:SLEEP                        1be0 2 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:WAKEUP                       1be0 2 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:RECORD                       1be0 2 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:RECORD_FAILED                1be0 2 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:REACQ                        1be0 2 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:WAKEUP:DEMOD                        1be0 2 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:DOZE            1be0 2 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:SLEEP           1be0 2 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:WAKEUP          1be0 2 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:RECORD          1be0 2 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:RECORD_FAILED   1be0 2 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:REACQ           1be0 2 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_RSP_TUNE_COMP:DEMOD           1be0 2 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:DOZE                         1be0 2 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:SLEEP                        1be0 2 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:WAKEUP                       1be0 2 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:RECORD                       1be0 2 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:RECORD_FAILED                1be0 2 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:REACQ                        1be0 2 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:CX8_ON:DEMOD                        1be0 2 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:DOZE             1be0 2 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:SLEEP            1be0 2 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:WAKEUP           1be0 2 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:RECORD           1be0 2 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:RECORD_FAILED    1be0 2 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:REACQ            1be0 2 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLE_COMP:DEMOD            1be0 2 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:DOZE                   1be0 2 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:SLEEP                  1be0 2 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:WAKEUP                 1be0 2 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:RECORD                 1be0 2 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:RECORD_FAILED          1be0 2 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:REACQ                  1be0 2 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_TUNE_COMP:DEMOD                  1be0 2 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:DOZE                 1be0 2 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:SLEEP                1be0 2 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:WAKEUP               1be0 2 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:RECORD               1be0 2 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:RECORD_FAILED        1be0 2 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:REACQ                1be0 2 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED:DEMOD                1be0 2 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:DOZE            1be0 2 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:SLEEP           1be0 2 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:WAKEUP          1be0 2 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:RECORD          1be0 2 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:RECORD_FAILED   1be0 2 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:REACQ           1be0 2 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:FREQ_TRACK_OFF_DONE:DEMOD           1be0 2 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:DOZE             1be0 2 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:SLEEP            1be0 2 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:WAKEUP           1be0 2 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:RECORD           1be0 2 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:RECORD_FAILED    1be0 2 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:REACQ            1be0 2 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:RX_RF_DISABLED_ERR:DEMOD            1be0 2 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:DOZE                  1be0 2 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:SLEEP                 1be0 2 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:WAKEUP                1be0 2 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:RECORD                1be0 2 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:RECORD_FAILED         1be0 2 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:REACQ                 1be0 2 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:ADJUST_TIMING:DEMOD                 1be0 2 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:DOZE                1be0 2 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:SLEEP               1be0 2 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:WAKEUP              1be0 2 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:RECORD              1be0 2 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:RECORD_FAILED       1be0 2 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:REACQ               1be0 2 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:WAKEUP:QPCH_DEMOD_DONE:DEMOD               1be0 2 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:DOZE                    1be0 3 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:SLEEP                   1be0 3 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:WAKEUP                  1be0 3 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:RECORD                  1be0 3 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:RECORD_FAILED           1be0 3 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:REACQ                   1be0 3 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:START_SLEEP:DEMOD                   1be0 3 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:ABORT:DOZE                          1be0 3 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:SLEEP                         1be0 3 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:WAKEUP                        1be0 3 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:RECORD                        1be0 3 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:RECORD_FAILED                 1be0 3 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:REACQ                         1be0 3 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ABORT:DEMOD                         1be0 3 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:DOZE                     1be0 3 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:SLEEP                    1be0 3 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:WAKEUP                   1be0 3 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:RECORD                   1be0 3 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:RECORD_FAILED            1be0 3 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:REACQ                    1be0 3 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP_NOW:DEMOD                    1be0 3 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:DOZE                  1be0 3 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:SLEEP                 1be0 3 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:WAKEUP                1be0 3 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:RECORD                1be0 3 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:RECORD_FAILED         1be0 3 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:REACQ                 1be0 3 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_GRANTED:DEMOD                 1be0 3 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:DOZE                   1be0 3 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:SLEEP                  1be0 3 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:WAKEUP                 1be0 3 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:RECORD                 1be0 3 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:RECORD_FAILED          1be0 3 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:REACQ                  1be0 3 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_CH_DENIED:DEMOD                  1be0 3 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:DOZE                     1be0 3 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:SLEEP                    1be0 3 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:WAKEUP                   1be0 3 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:RECORD                   1be0 3 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:RECORD_FAILED            1be0 3 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:REACQ                    1be0 3 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:NO_RF_LOCK:DEMOD                    1be0 3 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:DOZE                         1be0 3 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:SLEEP                        1be0 3 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:WAKEUP                       1be0 3 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:RECORD                       1be0 3 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:RECORD_FAILED                1be0 3 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:REACQ                        1be0 3 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:WAKEUP:DEMOD                        1be0 3 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:DOZE            1be0 3 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:SLEEP           1be0 3 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:WAKEUP          1be0 3 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:RECORD          1be0 3 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:RECORD_FAILED   1be0 3 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:REACQ           1be0 3 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_RSP_TUNE_COMP:DEMOD           1be0 3 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:DOZE                         1be0 3 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:SLEEP                        1be0 3 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:WAKEUP                       1be0 3 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:RECORD                       1be0 3 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:RECORD_FAILED                1be0 3 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:REACQ                        1be0 3 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:CX8_ON:DEMOD                        1be0 3 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:DOZE             1be0 3 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:SLEEP            1be0 3 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:WAKEUP           1be0 3 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:RECORD           1be0 3 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:RECORD_FAILED    1be0 3 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:REACQ            1be0 3 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLE_COMP:DEMOD            1be0 3 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:DOZE                   1be0 3 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:SLEEP                  1be0 3 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:WAKEUP                 1be0 3 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:RECORD                 1be0 3 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:RECORD_FAILED          1be0 3 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:REACQ                  1be0 3 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_TUNE_COMP:DEMOD                  1be0 3 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:DOZE                 1be0 3 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:SLEEP                1be0 3 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:WAKEUP               1be0 3 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:RECORD               1be0 3 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:RECORD_FAILED        1be0 3 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:REACQ                1be0 3 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED:DEMOD                1be0 3 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:DOZE            1be0 3 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:SLEEP           1be0 3 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:WAKEUP          1be0 3 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:RECORD          1be0 3 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:RECORD_FAILED   1be0 3 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:REACQ           1be0 3 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:FREQ_TRACK_OFF_DONE:DEMOD           1be0 3 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:DOZE             1be0 3 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:SLEEP            1be0 3 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:WAKEUP           1be0 3 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:RECORD           1be0 3 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:RECORD_FAILED    1be0 3 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:REACQ            1be0 3 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:RX_RF_DISABLED_ERR:DEMOD            1be0 3 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:DOZE                  1be0 3 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:SLEEP                 1be0 3 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:WAKEUP                1be0 3 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:RECORD                1be0 3 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:RECORD_FAILED         1be0 3 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:REACQ                 1be0 3 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:ADJUST_TIMING:DEMOD                 1be0 3 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:DOZE                1be0 3 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:SLEEP               1be0 3 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:WAKEUP              1be0 3 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:RECORD              1be0 3 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:RECORD_FAILED       1be0 3 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:REACQ               1be0 3 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD:QPCH_DEMOD_DONE:DEMOD               1be0 3 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:DOZE             1be0 4 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:SLEEP            1be0 4 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:WAKEUP           1be0 4 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:RECORD           1be0 4 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:RECORD_FAILED    1be0 4 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:REACQ            1be0 4 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:START_SLEEP:DEMOD            1be0 4 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:DOZE                   1be0 4 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:SLEEP                  1be0 4 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:WAKEUP                 1be0 4 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:RECORD                 1be0 4 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:RECORD_FAILED          1be0 4 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:REACQ                  1be0 4 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ABORT:DEMOD                  1be0 4 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:DOZE              1be0 4 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:SLEEP             1be0 4 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:WAKEUP            1be0 4 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:RECORD            1be0 4 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:RECORD_FAILED     1be0 4 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:REACQ             1be0 4 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP_NOW:DEMOD             1be0 4 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:DOZE           1be0 4 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:SLEEP          1be0 4 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:WAKEUP         1be0 4 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:RECORD         1be0 4 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:RECORD_FAILED  1be0 4 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:REACQ          1be0 4 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_GRANTED:DEMOD          1be0 4 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:DOZE            1be0 4 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:SLEEP           1be0 4 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:WAKEUP          1be0 4 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:RECORD          1be0 4 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:RECORD_FAILED   1be0 4 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:REACQ           1be0 4 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_CH_DENIED:DEMOD           1be0 4 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:DOZE              1be0 4 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:SLEEP             1be0 4 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:WAKEUP            1be0 4 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:RECORD            1be0 4 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:RECORD_FAILED     1be0 4 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:REACQ             1be0 4 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:NO_RF_LOCK:DEMOD             1be0 4 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:DOZE                  1be0 4 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:SLEEP                 1be0 4 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:WAKEUP                1be0 4 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:RECORD                1be0 4 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:RECORD_FAILED         1be0 4 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:REACQ                 1be0 4 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:WAKEUP:DEMOD                 1be0 4 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:DOZE     1be0 4 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:SLEEP    1be0 4 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:WAKEUP   1be0 4 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:RECORD   1be0 4 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:RECORD_FAILED 1be0 4 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:REACQ    1be0 4 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_RSP_TUNE_COMP:DEMOD    1be0 4 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:DOZE                  1be0 4 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:SLEEP                 1be0 4 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:WAKEUP                1be0 4 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:RECORD                1be0 4 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:RECORD_FAILED         1be0 4 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:REACQ                 1be0 4 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:CX8_ON:DEMOD                 1be0 4 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:DOZE      1be0 4 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:SLEEP     1be0 4 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:WAKEUP    1be0 4 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:RECORD    1be0 4 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:RECORD_FAILED 1be0 4 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:REACQ     1be0 4 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLE_COMP:DEMOD     1be0 4 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:DOZE            1be0 4 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:SLEEP           1be0 4 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:WAKEUP          1be0 4 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:RECORD          1be0 4 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:RECORD_FAILED   1be0 4 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:REACQ           1be0 4 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_TUNE_COMP:DEMOD           1be0 4 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:DOZE          1be0 4 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:SLEEP         1be0 4 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:WAKEUP        1be0 4 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:RECORD        1be0 4 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:RECORD_FAILED 1be0 4 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:REACQ         1be0 4 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED:DEMOD         1be0 4 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:DOZE     1be0 4 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:SLEEP    1be0 4 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:WAKEUP   1be0 4 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:RECORD   1be0 4 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:RECORD_FAILED 1be0 4 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:REACQ    1be0 4 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:FREQ_TRACK_OFF_DONE:DEMOD    1be0 4 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:DOZE      1be0 4 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:SLEEP     1be0 4 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:WAKEUP    1be0 4 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:RECORD    1be0 4 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:RECORD_FAILED 1be0 4 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:REACQ     1be0 4 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:RX_RF_DISABLED_ERR:DEMOD     1be0 4 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:DOZE           1be0 4 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:SLEEP          1be0 4 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:WAKEUP         1be0 4 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:RECORD         1be0 4 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:RECORD_FAILED  1be0 4 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:REACQ          1be0 4 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:ADJUST_TIMING:DEMOD          1be0 4 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:DOZE         1be0 4 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:SLEEP        1be0 4 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:WAKEUP       1be0 4 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:RECORD       1be0 4 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:RECORD_FAILED 1be0 4 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:REACQ        1be0 4 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:RECORD_FAILED:QPCH_DEMOD_DONE:DEMOD        1be0 4 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:DOZE                     1be0 5 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:SLEEP                    1be0 5 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:WAKEUP                   1be0 5 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:RECORD                   1be0 5 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:RECORD_FAILED            1be0 5 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:REACQ                    1be0 5 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:START_SLEEP:DEMOD                    1be0 5 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:ABORT:DOZE                           1be0 5 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:SLEEP                          1be0 5 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:WAKEUP                         1be0 5 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:RECORD                         1be0 5 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:RECORD_FAILED                  1be0 5 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:REACQ                          1be0 5 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ABORT:DEMOD                          1be0 5 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:DOZE                      1be0 5 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:SLEEP                     1be0 5 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:WAKEUP                    1be0 5 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:RECORD                    1be0 5 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:RECORD_FAILED             1be0 5 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:REACQ                     1be0 5 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP_NOW:DEMOD                     1be0 5 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:DOZE                   1be0 5 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:SLEEP                  1be0 5 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:WAKEUP                 1be0 5 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:RECORD                 1be0 5 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:RECORD_FAILED          1be0 5 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:REACQ                  1be0 5 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_GRANTED:DEMOD                  1be0 5 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:DOZE                    1be0 5 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:SLEEP                   1be0 5 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:WAKEUP                  1be0 5 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:RECORD                  1be0 5 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:RECORD_FAILED           1be0 5 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:REACQ                   1be0 5 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_CH_DENIED:DEMOD                   1be0 5 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:DOZE                      1be0 5 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:SLEEP                     1be0 5 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:WAKEUP                    1be0 5 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:RECORD                    1be0 5 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:RECORD_FAILED             1be0 5 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:REACQ                     1be0 5 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:NO_RF_LOCK:DEMOD                     1be0 5 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:DOZE                          1be0 5 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:SLEEP                         1be0 5 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:WAKEUP                        1be0 5 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:RECORD                        1be0 5 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:RECORD_FAILED                 1be0 5 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:REACQ                         1be0 5 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:WAKEUP:DEMOD                         1be0 5 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:DOZE             1be0 5 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:SLEEP            1be0 5 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:WAKEUP           1be0 5 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:RECORD           1be0 5 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:RECORD_FAILED    1be0 5 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:REACQ            1be0 5 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_RSP_TUNE_COMP:DEMOD            1be0 5 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:DOZE                          1be0 5 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:SLEEP                         1be0 5 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:WAKEUP                        1be0 5 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:RECORD                        1be0 5 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:RECORD_FAILED                 1be0 5 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:REACQ                         1be0 5 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:CX8_ON:DEMOD                         1be0 5 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:DOZE              1be0 5 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:SLEEP             1be0 5 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:WAKEUP            1be0 5 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:RECORD            1be0 5 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:RECORD_FAILED     1be0 5 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:REACQ             1be0 5 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLE_COMP:DEMOD             1be0 5 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:DOZE                    1be0 5 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:SLEEP                   1be0 5 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:WAKEUP                  1be0 5 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:RECORD                  1be0 5 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:RECORD_FAILED           1be0 5 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:REACQ                   1be0 5 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_TUNE_COMP:DEMOD                   1be0 5 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:DOZE                  1be0 5 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:SLEEP                 1be0 5 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:WAKEUP                1be0 5 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:RECORD                1be0 5 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:RECORD_FAILED         1be0 5 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:REACQ                 1be0 5 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED:DEMOD                 1be0 5 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:DOZE             1be0 5 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:SLEEP            1be0 5 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:WAKEUP           1be0 5 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:RECORD           1be0 5 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:RECORD_FAILED    1be0 5 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:REACQ            1be0 5 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:FREQ_TRACK_OFF_DONE:DEMOD            1be0 5 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:DOZE              1be0 5 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:SLEEP             1be0 5 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:WAKEUP            1be0 5 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:RECORD            1be0 5 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:RECORD_FAILED     1be0 5 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:REACQ             1be0 5 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:RX_RF_DISABLED_ERR:DEMOD             1be0 5 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:DOZE                   1be0 5 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:SLEEP                  1be0 5 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:WAKEUP                 1be0 5 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:RECORD                 1be0 5 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:RECORD_FAILED          1be0 5 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:REACQ                  1be0 5 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:ADJUST_TIMING:DEMOD                  1be0 5 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:DOZE                 1be0 5 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:SLEEP                1be0 5 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:WAKEUP               1be0 5 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:RECORD               1be0 5 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:RECORD_FAILED        1be0 5 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:REACQ                1be0 5 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:REACQ:QPCH_DEMOD_DONE:DEMOD                1be0 5 0f 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:DOZE                     1be0 6 00 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:SLEEP                    1be0 6 00 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:WAKEUP                   1be0 6 00 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:RECORD                   1be0 6 00 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:RECORD_FAILED            1be0 6 00 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:REACQ                    1be0 6 00 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:START_SLEEP:DEMOD                    1be0 6 00 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:DOZE                           1be0 6 01 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:SLEEP                          1be0 6 01 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:WAKEUP                         1be0 6 01 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:RECORD                         1be0 6 01 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:RECORD_FAILED                  1be0 6 01 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:REACQ                          1be0 6 01 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ABORT:DEMOD                          1be0 6 01 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:DOZE                      1be0 6 02 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:SLEEP                     1be0 6 02 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:WAKEUP                    1be0 6 02 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:RECORD                    1be0 6 02 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:RECORD_FAILED             1be0 6 02 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:REACQ                     1be0 6 02 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP_NOW:DEMOD                     1be0 6 02 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:DOZE                   1be0 6 03 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:SLEEP                  1be0 6 03 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:WAKEUP                 1be0 6 03 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:RECORD                 1be0 6 03 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:RECORD_FAILED          1be0 6 03 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:REACQ                  1be0 6 03 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_GRANTED:DEMOD                  1be0 6 03 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:DOZE                    1be0 6 04 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:SLEEP                   1be0 6 04 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:WAKEUP                  1be0 6 04 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:RECORD                  1be0 6 04 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:RECORD_FAILED           1be0 6 04 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:REACQ                   1be0 6 04 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_CH_DENIED:DEMOD                   1be0 6 04 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:DOZE                      1be0 6 05 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:SLEEP                     1be0 6 05 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:WAKEUP                    1be0 6 05 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:RECORD                    1be0 6 05 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:RECORD_FAILED             1be0 6 05 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:REACQ                     1be0 6 05 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:NO_RF_LOCK:DEMOD                     1be0 6 05 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:DOZE                          1be0 6 06 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:SLEEP                         1be0 6 06 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:WAKEUP                        1be0 6 06 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:RECORD                        1be0 6 06 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:RECORD_FAILED                 1be0 6 06 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:REACQ                         1be0 6 06 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:WAKEUP:DEMOD                         1be0 6 06 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:DOZE             1be0 6 07 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:SLEEP            1be0 6 07 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:WAKEUP           1be0 6 07 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:RECORD           1be0 6 07 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:RECORD_FAILED    1be0 6 07 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:REACQ            1be0 6 07 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_RSP_TUNE_COMP:DEMOD            1be0 6 07 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:DOZE                          1be0 6 08 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:SLEEP                         1be0 6 08 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:WAKEUP                        1be0 6 08 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:RECORD                        1be0 6 08 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:RECORD_FAILED                 1be0 6 08 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:REACQ                         1be0 6 08 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:CX8_ON:DEMOD                         1be0 6 08 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:DOZE              1be0 6 09 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:SLEEP             1be0 6 09 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:WAKEUP            1be0 6 09 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:RECORD            1be0 6 09 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:RECORD_FAILED     1be0 6 09 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:REACQ             1be0 6 09 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLE_COMP:DEMOD             1be0 6 09 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:DOZE                    1be0 6 0a 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:SLEEP                   1be0 6 0a 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:WAKEUP                  1be0 6 0a 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:RECORD                  1be0 6 0a 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:RECORD_FAILED           1be0 6 0a 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:REACQ                   1be0 6 0a 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_TUNE_COMP:DEMOD                   1be0 6 0a 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:DOZE                  1be0 6 0b 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:SLEEP                 1be0 6 0b 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:WAKEUP                1be0 6 0b 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:RECORD                1be0 6 0b 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:RECORD_FAILED         1be0 6 0b 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:REACQ                 1be0 6 0b 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED:DEMOD                 1be0 6 0b 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:DOZE             1be0 6 0c 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:SLEEP            1be0 6 0c 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:WAKEUP           1be0 6 0c 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:RECORD           1be0 6 0c 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:RECORD_FAILED    1be0 6 0c 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:REACQ            1be0 6 0c 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:FREQ_TRACK_OFF_DONE:DEMOD            1be0 6 0c 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:DOZE              1be0 6 0d 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:SLEEP             1be0 6 0d 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:WAKEUP            1be0 6 0d 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:RECORD            1be0 6 0d 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:RECORD_FAILED     1be0 6 0d 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:REACQ             1be0 6 0d 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:RX_RF_DISABLED_ERR:DEMOD             1be0 6 0d 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:DOZE                   1be0 6 0e 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:SLEEP                  1be0 6 0e 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:WAKEUP                 1be0 6 0e 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:RECORD                 1be0 6 0e 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:RECORD_FAILED          1be0 6 0e 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:REACQ                  1be0 6 0e 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:ADJUST_TIMING:DEMOD                  1be0 6 0e 6    @navy @white

SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:DOZE                 1be0 6 0f 0    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:SLEEP                1be0 6 0f 1    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:WAKEUP               1be0 6 0f 2    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:RECORD               1be0 6 0f 3    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:RECORD_FAILED        1be0 6 0f 4    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:REACQ                1be0 6 0f 5    @navy @white
SRCHZZ_QPCH_OFFTL:DEMOD:QPCH_DEMOD_DONE:DEMOD                1be0 6 0f 6    @navy @white



# End machine generated TLA code for state machine: SRCHZZ_QPCH_OFFTL_SM

