/*=============================================================================

  srchcd_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchcd_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: CDMA_SM */

/* Transition function prototypes */
static stm_state_type cd_remember_ext_cmd_and_post_in_main_state(void *);
static stm_state_type cd_process_skip_freq_done(void *);
static stm_state_type cd_process_freq_track_off_done(void *);
static stm_state_type cd_process_unexpected_mdsp_reg(void *);
static stm_state_type cd_process_aflt_release_done(void *);
static stm_state_type cd_process_rf_release_done(void *);
static stm_state_type cd_go_to_inactive(void *);
static stm_state_type cd_go_to_acq(void *);
static stm_state_type cd_stay_in_cdma(void *);
static stm_state_type cd_lock_granted(void *);
static stm_state_type cd_process_srch_mdsp_reg(void *);
static stm_state_type cd_process_rf_req_fail(void *);
static stm_state_type cd_report_rf_req_fail(void *);
static stm_state_type cd_process_sys_meas_abort_complete(void *);
static stm_state_type cd_process_rx_mod_denied(void *);
static stm_state_type cd_process_lte_tt_cmd(void *);
static stm_state_type cd_process_srch4_mdsp_reg(void *);
static stm_state_type cd_govern_timer(void *);
static stm_state_type cd_process_acq_cmd_in_trans(void *);
static stm_state_type cd_tune_rf_in_trans(void *);


/* State Machine entry/exit function prototypes */
static void cd_entry(stm_group_type *group);
static void cd_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void cd_main_entry(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define CDMA_SM_NUMBER_OF_STATES 7
#define CDMA_SM_NUMBER_OF_INPUTS 17


/* State enumeration */
enum
{
  WAIT_FREQ_DONE,
  WAIT_FOR_AFLT_RELEASE,
  WAIT_FOR_RF_RELEASE,
  MAIN,
  MDSP_SRCH4_APP_REG,
  WAIT_GOVERN,
  TRANS,
};


/* State name, entry, exit table */
static const stm_state_array_type
  CDMA_SM_states[ CDMA_SM_NUMBER_OF_STATES ] =
{
  {"WAIT_FREQ_DONE", NULL, NULL},
  {"WAIT_FOR_AFLT_RELEASE", NULL, NULL},
  {"WAIT_FOR_RF_RELEASE", NULL, NULL},
  {"MAIN", cd_main_entry, NULL},
  {"MDSP_SRCH4_APP_REG", NULL, NULL},
  {"WAIT_GOVERN", NULL, NULL},
  {"TRANS", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  CDMA_SM_inputs[ CDMA_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SKIP_FREQ_DONE_CMD, "SKIP_FREQ_DONE_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)SRCH_START_CMD, "SRCH_START_CMD"},
  {(stm_input_type)AFLT_RELEASE_DONE_CMD, "AFLT_RELEASE_DONE_CMD"},
  {(stm_input_type)RF_RELEASE_DONE_CMD, "RF_RELEASE_DONE_CMD"},
  {(stm_input_type)SRCH_DEACTIVATE_F, "SRCH_DEACTIVATE_F"},
  {(stm_input_type)SRCH_ACQ_F, "SRCH_ACQ_F"},
  {(stm_input_type)CD_RF_LOCK_GRANTED_CMD, "CD_RF_LOCK_GRANTED_CMD"},
  {(stm_input_type)MDSP_START_CMD, "MDSP_START_CMD"},
  {(stm_input_type)RF_REQ_FAIL_TIMER_CMD, "RF_REQ_FAIL_TIMER_CMD"},
  {(stm_input_type)SRCH4_MDSP_REG_CMD, "SRCH4_MDSP_REG_CMD"},
  {(stm_input_type)SYS_MEAS_ABORT_COMPLETE_CMD, "SYS_MEAS_ABORT_COMPLETE_CMD"},
  {(stm_input_type)RX_MOD_DENIED_CMD, "RX_MOD_DENIED_CMD"},
  {(stm_input_type)SRCH_LTE_1X_TT_F, "SRCH_LTE_1X_TT_F"},
  {(stm_input_type)GOVERN_TIMER_CMD, "GOVERN_TIMER_CMD"},
  {(stm_input_type)CD_RF_DIV_DISABLED_CMD, "CD_RF_DIV_DISABLED_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  CDMA_SM_transitions[ CDMA_SM_NUMBER_OF_STATES *  CDMA_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state WAIT_FREQ_DONE */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_CDMA_F */
    cd_process_skip_freq_done,    /* SKIP_FREQ_DONE_CMD */
    cd_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    cd_process_unexpected_mdsp_reg,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state WAIT_FOR_AFLT_RELEASE */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH_START_CMD */
    cd_process_aflt_release_done,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state WAIT_FOR_RF_RELEASE */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    cd_process_rf_release_done,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state MAIN */
    cd_stay_in_cdma,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    cd_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    cd_process_unexpected_mdsp_reg,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    cd_report_rf_req_fail,    /* RF_RELEASE_DONE_CMD */
    cd_go_to_inactive,    /* SRCH_DEACTIVATE_F */
    cd_go_to_acq,    /* SRCH_ACQ_F */
    cd_lock_granted,    /* CD_RF_LOCK_GRANTED_CMD */
    cd_process_srch_mdsp_reg,    /* MDSP_START_CMD */
    cd_process_rf_req_fail,    /* RF_REQ_FAIL_TIMER_CMD */
    cd_process_unexpected_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    cd_process_sys_meas_abort_complete,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    cd_process_rx_mod_denied,    /* RX_MOD_DENIED_CMD */
    cd_process_lte_tt_cmd,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state MDSP_SRCH4_APP_REG */
    cd_stay_in_cdma,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    cd_go_to_inactive,    /* SRCH_DEACTIVATE_F */
    cd_go_to_acq,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    cd_process_srch4_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state WAIT_GOVERN */
    cd_stay_in_cdma,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    cd_go_to_inactive,    /* SRCH_DEACTIVATE_F */
    cd_go_to_acq,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* SRCH_LTE_1X_TT_F */
    cd_govern_timer,    /* GOVERN_TIMER_CMD */
    NULL,    /* CD_RF_DIV_DISABLED_CMD */

  /* Transition functions for state TRANS */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_CDMA_F */
    NULL,    /* SKIP_FREQ_DONE_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_DEACTIVATE_F */
    cd_process_acq_cmd_in_trans,    /* SRCH_ACQ_F */
    NULL,    /* CD_RF_LOCK_GRANTED_CMD */
    NULL,    /* MDSP_START_CMD */
    NULL,    /* RF_REQ_FAIL_TIMER_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* SYS_MEAS_ABORT_COMPLETE_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    cd_remember_ext_cmd_and_post_in_main_state,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* GOVERN_TIMER_CMD */
    cd_tune_rf_in_trans,    /* CD_RF_DIV_DISABLED_CMD */

};


/* State machine definition */
stm_state_machine_type CDMA_SM =
{
  "CDMA_SM", /* state machine name */
  61400, /* unique SM id (hash of name) */
  cd_entry, /* state machine entry function */
  cd_exit, /* state machine exit function */
  WAIT_FREQ_DONE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  CDMA_SM_NUMBER_OF_STATES,
  CDMA_SM_NUMBER_OF_INPUTS,
  CDMA_SM_states,
  CDMA_SM_inputs,
  CDMA_SM_transitions,
};

/* End machine generated code for state machine: CDMA_SM */
/* Begin machine generated code for state machine group: SRCH_CDMA_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean cdma_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_CDMA_GROUP_sm_list[ 1 ] =
{
  &CDMA_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_CDMA_GROUP_sig_list[ 1 ] =
{
  SRCH_CMD_Q_SIG,
};


/* State machine group definition */
stm_group_type SRCH_CDMA_GROUP =
{
  "SRCH_CDMA_GROUP", /* state machine group name */
  SRCH_CDMA_GROUP_sig_list, /* signal mapping table */
  1, /* number of signals in mapping table */
  cdma_sig_mapper, /* signal mapper function */
  SRCH_CDMA_GROUP_sm_list, /* state machine table */
  1, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_CDMA_GROUP */

