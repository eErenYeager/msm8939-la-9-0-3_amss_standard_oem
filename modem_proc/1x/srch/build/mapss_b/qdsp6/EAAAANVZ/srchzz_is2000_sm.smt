/*=============================================================================

  srchzz_is2000_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_is2000_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_IS2000_SM */

/* Transition function prototypes */
static stm_state_type is2000_setup_sleep(void *);
static stm_state_type is2000_xfer_from_qpch(void *);
static stm_state_type is2000_process_init_roll(void *);
static stm_state_type is2000_adjust_combiner_time_after_sb(void *);
static stm_state_type is2000_remember_rude_wakeup(void *);
static stm_state_type is2000_process_freq_track_off_done(void *);
static stm_state_type is2000_process_rf_disabled_cmd(void *);
static stm_state_type is2000_process_go_to_sleep_cmd(void *);
static stm_state_type is2000_process_rf_disable_comp_cmd(void *);
static stm_state_type is2000_process_rude_wakeup(void *);
static stm_state_type is2000_process_rf_chain_granted(void *);
static stm_state_type is2000_process_rf_chain_denied(void *);
static stm_state_type is2000_process_wakeup(void *);
static stm_state_type is2000_process_rf_prep_comp(void *);
static stm_state_type is2000_process_no_rf_lock(void *);
static stm_state_type is2000_remember_cx8_on(void *);
static stm_state_type is2000_process_wakeup_update(void *);
static stm_state_type is2000_process_disable_comp_in_sleep(void *);
static stm_state_type is2000_process_freq_track_off_done_in_sleep(void *);
static stm_state_type is2000_process_cx8_on(void *);
static stm_state_type is2000_process_tune_complete(void *);
static stm_state_type is2000_remember_wait_sb_roll(void *);
static stm_state_type is2000_adjust_combiner_time(void *);
static stm_state_type is2000_process_xfer_rf_granted(void *);
static stm_state_type is2000_process_xfer_rf_denied(void *);
static stm_state_type is2000_process_xfer_tune_complete(void *);
static stm_state_type is2000_process_wait_sb_roll(void *);


/* State Machine entry/exit function prototypes */
static void is2000_entry(stm_group_type *group);
static void is2000_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void is2000_enter_init(void *payload, stm_state_type previous_state);
static void is2000_enter_sleep(void *payload, stm_state_type previous_state);
static void is2000_exit_sleep(void *payload, stm_state_type previous_state);
static void is2000_enter_wakeup(void *payload, stm_state_type previous_state);
static void is2000_enter_wait_sb(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_IS2000_SM_NUMBER_OF_STATES 5
#define SRCHZZ_IS2000_SM_NUMBER_OF_INPUTS 17


/* State enumeration */
enum
{
  INIT_STATE,
  SLEEP_STATE,
  WAKEUP_STATE,
  PREP_SB_STATE,
  WAIT_SB_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_IS2000_SM_states[ SRCHZZ_IS2000_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", is2000_enter_init, NULL},
  {"SLEEP_STATE", is2000_enter_sleep, is2000_exit_sleep},
  {"WAKEUP_STATE", is2000_enter_wakeup, NULL},
  {"PREP_SB_STATE", NULL, NULL},
  {"WAIT_SB_STATE", is2000_enter_wait_sb, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_IS2000_SM_inputs[ SRCHZZ_IS2000_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)XFER_FROM_QPCH_CMD, "XFER_FROM_QPCH_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)ADJUST_TIMING_CMD, "ADJUST_TIMING_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)RX_RF_DISABLED_CMD, "RX_RF_DISABLED_CMD"},
  {(stm_input_type)GO_TO_SLEEP_CMD, "GO_TO_SLEEP_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)RX_CH_GRANTED_CMD, "RX_CH_GRANTED_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)WAKEUP_CMD, "WAKEUP_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)NO_RF_LOCK_CMD, "NO_RF_LOCK_CMD"},
  {(stm_input_type)CX8_ON_CMD, "CX8_ON_CMD"},
  {(stm_input_type)WAKEUP_UPDATE_CMD, "WAKEUP_UPDATE_CMD"},
  {(stm_input_type)RX_TUNE_COMP_CMD, "RX_TUNE_COMP_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_IS2000_SM_transitions[ SRCHZZ_IS2000_SM_NUMBER_OF_STATES *  SRCHZZ_IS2000_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    is2000_setup_sleep,    /* START_SLEEP_CMD */
    is2000_xfer_from_qpch,    /* XFER_FROM_QPCH_CMD */
    is2000_process_init_roll,    /* ROLL_CMD */
    is2000_adjust_combiner_time_after_sb,    /* ADJUST_TIMING_CMD */
    is2000_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    is2000_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    is2000_process_rf_disabled_cmd,    /* RX_RF_DISABLED_CMD */
    is2000_process_go_to_sleep_cmd,    /* GO_TO_SLEEP_CMD */
    is2000_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* WAKEUP_UPDATE_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state SLEEP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* XFER_FROM_QPCH_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    is2000_process_rude_wakeup,    /* WAKEUP_NOW_CMD */
    is2000_process_freq_track_off_done_in_sleep,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    is2000_process_disable_comp_in_sleep,    /* RX_RF_DISABLE_COMP_CMD */
    is2000_process_rf_chain_granted,    /* RX_CH_GRANTED_CMD */
    is2000_process_rf_chain_denied,    /* RX_CH_DENIED_CMD */
    is2000_process_wakeup,    /* WAKEUP_CMD */
    is2000_process_rf_prep_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    is2000_process_no_rf_lock,    /* NO_RF_LOCK_CMD */
    is2000_remember_cx8_on,    /* CX8_ON_CMD */
    is2000_process_wakeup_update,    /* WAKEUP_UPDATE_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state WAKEUP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* XFER_FROM_QPCH_CMD */
    is2000_remember_wait_sb_roll,    /* ROLL_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    is2000_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    is2000_process_cx8_on,    /* CX8_ON_CMD */
    NULL,    /* WAKEUP_UPDATE_CMD */
    is2000_process_tune_complete,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state PREP_SB_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* XFER_FROM_QPCH_CMD */
    is2000_remember_wait_sb_roll,    /* ROLL_CMD */
    is2000_adjust_combiner_time,    /* ADJUST_TIMING_CMD */
    is2000_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    is2000_process_xfer_rf_granted,    /* RX_CH_GRANTED_CMD */
    is2000_process_xfer_rf_denied,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* WAKEUP_UPDATE_CMD */
    is2000_process_xfer_tune_complete,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state WAIT_SB_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* XFER_FROM_QPCH_CMD */
    is2000_process_wait_sb_roll,    /* ROLL_CMD */
    is2000_adjust_combiner_time,    /* ADJUST_TIMING_CMD */
    is2000_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* WAKEUP_UPDATE_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_IS2000_SM =
{
  "SRCHZZ_IS2000_SM", /* state machine name */
  22014, /* unique SM id (hash of name) */
  is2000_entry, /* state machine entry function */
  is2000_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_IS2000_SM_NUMBER_OF_STATES,
  SRCHZZ_IS2000_SM_NUMBER_OF_INPUTS,
  SRCHZZ_IS2000_SM_states,
  SRCHZZ_IS2000_SM_inputs,
  SRCHZZ_IS2000_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_IS2000_SM */

