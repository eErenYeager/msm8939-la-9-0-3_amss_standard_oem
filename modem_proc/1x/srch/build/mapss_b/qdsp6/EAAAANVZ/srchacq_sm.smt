/*=============================================================================

  srchacq_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchacq_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: ACQ_SM */

/* Transition function prototypes */
static stm_state_type acq_go_to_cdma(void *);
static stm_state_type acq_stay_in_acq(void *);
static stm_state_type acq_go_to_sync(void *);
static stm_state_type acq_ready_process_ck_agc(void *);
static stm_state_type acq_ready_process_mode_det(void *);
static stm_state_type acq_process_srch4_mdsp_app_en(void *);
static stm_state_type acq_process_srch4_mdsp_app_dis(void *);
static stm_state_type acq_process_unexpected_timer_exp(void *);
static stm_state_type acq_abort_ongoing_acq(void *);
static stm_state_type acq_detect_process_srch(void *);
static stm_state_type acq_process_dump_lost(void *);
static stm_state_type acq_timeout_exp(void *);
static stm_state_type acq_dwell_process_srch(void *);
static stm_state_type acq_verify_process_rssi_timer(void *);
static stm_state_type acq_verify_process_verify_timer(void *);
static stm_state_type acq_process_pullin_timer(void *);
static stm_state_type acq_pullin_acq_mode_process_freqerr_timer(void *);
static stm_state_type acq_pullin_trk_mode_process_freqerr_timer(void *);
static stm_state_type acq_pullin_trk_mode_process_afc_start_vco_pullin(void *);
static stm_state_type acq_pullin_trk_mode_process_afc_acq_success(void *);
static stm_state_type ACQ_SM_ignore_input(void *);
static stm_state_type acq_wait_govern_timer(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type ACQ_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void acq_entry(stm_group_type *group);
static void acq_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void acq_ready_entry(void *payload, stm_state_type previous_state);
static void acq_detect_lgp_entry(void *payload, stm_state_type previous_state);
static void acq_detect_entry(void *payload, stm_state_type previous_state);
static void acq_dwell_entry(void *payload, stm_state_type previous_state);
static void acq_verify_entry(void *payload, stm_state_type previous_state);
static void acq_pullin_acq_mode_entry(void *payload, stm_state_type previous_state);
static void acq_pullin_trk_mode_entry(void *payload, stm_state_type previous_state);
static void acq_success_entry(void *payload, stm_state_type previous_state);
static void acq_failure_entry(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define ACQ_SM_NUMBER_OF_STATES 11
#define ACQ_SM_NUMBER_OF_INPUTS 18


/* State enumeration */
enum
{
  READY,
  DETECT_LGP,
  DETECT,
  DWELL,
  VERIFY,
  ACQ_PULLIN,
  TRK_PULLIN,
  SUCCESS,
  FAILURE,
  WAIT_GOVERN,
  DONE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  ACQ_SM_states[ ACQ_SM_NUMBER_OF_STATES ] =
{
  {"READY", acq_ready_entry, NULL},
  {"DETECT_LGP", acq_detect_lgp_entry, NULL},
  {"DETECT", acq_detect_entry, NULL},
  {"DWELL", acq_dwell_entry, NULL},
  {"VERIFY", acq_verify_entry, NULL},
  {"ACQ_PULLIN", acq_pullin_acq_mode_entry, NULL},
  {"TRK_PULLIN", acq_pullin_trk_mode_entry, NULL},
  {"SUCCESS", acq_success_entry, NULL},
  {"FAILURE", acq_failure_entry, NULL},
  {"WAIT_GOVERN", NULL, NULL},
  {"DONE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  ACQ_SM_inputs[ ACQ_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_ACQ_F, "SRCH_ACQ_F"},
  {(stm_input_type)SRCH_SC_F, "SRCH_SC_F"},
  {(stm_input_type)CK_AGC_CMD, "CK_AGC_CMD"},
  {(stm_input_type)MODE_DET_CMD, "MODE_DET_CMD"},
  {(stm_input_type)SRCH4_MDSP_APP_EN_CMD, "SRCH4_MDSP_APP_EN_CMD"},
  {(stm_input_type)SRCH4_MDSP_APP_DIS_CMD, "SRCH4_MDSP_APP_DIS_CMD"},
  {(stm_input_type)RSSI_TIMER_CMD, "RSSI_TIMER_CMD"},
  {(stm_input_type)FREQERR_CK_TIMER_CMD, "FREQERR_CK_TIMER_CMD"},
  {(stm_input_type)VERIFY_TO_TIMER_CMD, "VERIFY_TO_TIMER_CMD"},
  {(stm_input_type)PULLIN_TO_TIMER_CMD, "PULLIN_TO_TIMER_CMD"},
  {(stm_input_type)ACQ_TO_TIMER_CMD, "ACQ_TO_TIMER_CMD"},
  {(stm_input_type)ACQ_ABORT_CMD, "ACQ_ABORT_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)AFC_START_VCO_PULLIN_CMD, "AFC_START_VCO_PULLIN_CMD"},
  {(stm_input_type)AFC_ACQ_SUCCESS_CMD, "AFC_ACQ_SUCCESS_CMD"},
  {(stm_input_type)GOVERN_TIMER_CMD, "GOVERN_TIMER_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  ACQ_SM_transitions[ ACQ_SM_NUMBER_OF_STATES *  ACQ_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state READY */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    acq_ready_process_ck_agc,    /* CK_AGC_CMD */
    acq_ready_process_mode_det,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state DETECT_LGP */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    acq_detect_process_srch,    /* SRCH_DUMP_CMD */
    acq_process_dump_lost,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state DETECT */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    acq_detect_process_srch,    /* SRCH_DUMP_CMD */
    acq_process_dump_lost,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state DWELL */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    acq_dwell_process_srch,    /* SRCH_DUMP_CMD */
    acq_process_dump_lost,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state VERIFY */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_verify_process_rssi_timer,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_verify_process_verify_timer,    /* VERIFY_TO_TIMER_CMD */
    acq_process_pullin_timer,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state ACQ_PULLIN */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_pullin_acq_mode_process_freqerr_timer,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_pullin_timer,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state TRK_PULLIN */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_pullin_trk_mode_process_freqerr_timer,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_pullin_timer,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    acq_abort_ongoing_acq,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    acq_pullin_trk_mode_process_afc_start_vco_pullin,    /* AFC_START_VCO_PULLIN_CMD */
    acq_pullin_trk_mode_process_afc_acq_success,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state SUCCESS */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    ACQ_SM_ignore_input,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state FAILURE */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_timeout_exp,    /* ACQ_TO_TIMER_CMD */
    ACQ_SM_ignore_input,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state WAIT_GOVERN */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    NULL,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* ACQ_TO_TIMER_CMD */
    ACQ_SM_ignore_input,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    acq_wait_govern_timer,    /* GOVERN_TIMER_CMD */

  /* Transition functions for state DONE */
    acq_go_to_cdma,    /* SRCH_CDMA_F */
    acq_stay_in_acq,    /* SRCH_ACQ_F */
    acq_go_to_sync,    /* SRCH_SC_F */
    NULL,    /* CK_AGC_CMD */
    NULL,    /* MODE_DET_CMD */
    acq_process_srch4_mdsp_app_en,    /* SRCH4_MDSP_APP_EN_CMD */
    acq_process_srch4_mdsp_app_dis,    /* SRCH4_MDSP_APP_DIS_CMD */
    acq_process_unexpected_timer_exp,    /* RSSI_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* FREQERR_CK_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* VERIFY_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* PULLIN_TO_TIMER_CMD */
    acq_process_unexpected_timer_exp,    /* ACQ_TO_TIMER_CMD */
    ACQ_SM_ignore_input,    /* ACQ_ABORT_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* AFC_START_VCO_PULLIN_CMD */
    NULL,    /* AFC_ACQ_SUCCESS_CMD */
    NULL,    /* GOVERN_TIMER_CMD */

};


/* State machine definition */
stm_state_machine_type ACQ_SM =
{
  "ACQ_SM", /* state machine name */
  58786, /* unique SM id (hash of name) */
  acq_entry, /* state machine entry function */
  acq_exit, /* state machine exit function */
  READY, /* state machine initial state */
  TRUE, /* state machine starts active? */
  ACQ_SM_NUMBER_OF_STATES,
  ACQ_SM_NUMBER_OF_INPUTS,
  ACQ_SM_states,
  ACQ_SM_inputs,
  ACQ_SM_transitions,
};

/* End machine generated code for state machine: ACQ_SM */
/* Begin machine generated code for state machine group: SRCH_ACQ_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean acq_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_ACQ_GROUP_sm_list[ 1 ] =
{
  &ACQ_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_ACQ_GROUP_sig_list[ 1 ] =
{
  SRCH_CMD_Q_SIG,
};


/* State machine group definition */
stm_group_type SRCH_ACQ_GROUP =
{
  "SRCH_ACQ_GROUP", /* state machine group name */
  SRCH_ACQ_GROUP_sig_list, /* signal mapping table */
  1, /* number of signals in mapping table */
  acq_sig_mapper, /* signal mapper function */
  SRCH_ACQ_GROUP_sm_list, /* state machine table */
  1, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_ACQ_GROUP */

