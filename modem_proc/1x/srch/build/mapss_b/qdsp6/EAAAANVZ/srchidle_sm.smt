/*=============================================================================

  srchidle_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchidle_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHIDLE_SM */

/* Transition function prototypes */
static stm_state_type idle_process_cdma_cmd(void *);
static stm_state_type idle_process_sleep_cmd(void *);
static stm_state_type idle_process_tc_cmd(void *);
static stm_state_type idle_process_access_cmd(void *);
static stm_state_type idle_process_parm_cmd(void *);
static stm_state_type idle_process_idle_aset_cmd(void *);
static stm_state_type idle_process_idle_nset_cmd(void *);
static stm_state_type idle_process_reselect_meas_cmd(void *);
static stm_state_type idle_process_pc_assign_cmd(void *);
static stm_state_type idle_process_chan_config_cmd(void *);
static stm_state_type idle_process_bc_info_cmd(void *);
static stm_state_type idle_process_idle_cmd(void *);
static stm_state_type idle_process_roll(void *);
static stm_state_type idle_process_ho_request(void *);
static stm_state_type idle_process_frame_strobe(void *);
static stm_state_type idle_process_reselection_check(void *);
static stm_state_type idle_process_sleep_ok(void *);
static stm_state_type idle_process_rssi_timer(void *);
static stm_state_type idle_process_rf_report_timer(void *);
static stm_state_type idle_deactivate_zz(void *);
static stm_state_type idle_process_delayed_aset(void *);
static stm_state_type idle_process_online_rf_granted(void *);
static stm_state_type idle_process_online_rf_tuned(void *);
static stm_state_type idle_process_online_rx_mod_granted(void *);
static stm_state_type idle_process_online_rx_mod_denied(void *);
static stm_state_type idle_process_div_request_cmd(void *);
static stm_state_type SRCHIDLE_SM_ignore_input(void *);
static stm_state_type idle_process_zz_idle_nset_cmd(void *);
static stm_state_type idle_process_zz_idle_aset_cmd(void *);
static stm_state_type idle_process_zz_bc_info_cmd(void *);
static stm_state_type idle_process_zz_cdma_cmd(void *);
static stm_state_type idle_process_internal_cdma_cmd(void *);
static stm_state_type idle_process_zz_sleep_cmd(void *);
static stm_state_type idle_process_zz_idle_cmd(void *);
static stm_state_type idle_process_zz_idle_rsp_cmd(void *);
static stm_state_type idle_process_zz_roll(void *);
static stm_state_type idle_process_page_match(void *);
static stm_state_type idle_process_pilot_list_cmd(void *);
static stm_state_type idle_process_find_idle_strong_pilot_cmd(void *);
static stm_state_type idle_process_access_roll(void *);
static stm_state_type idle_process_scan_all_rpt(void *);
static stm_state_type idle_process_access_rf_granted(void *);
static stm_state_type idle_process_access_prep_tune(void *);
static stm_state_type idle_process_access_rf_tuned(void *);
static stm_state_type idle_process_access_rx_mod_granted(void *);
static stm_state_type idle_process_access_rx_mod_denied(void *);
static stm_state_type idle_process_assign_aset_cmd(void *);
static stm_state_type idle_process_assign_rpt(void *);
static stm_state_type idle_process_assign_after_abort(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SRCHIDLE_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void idle_entry(stm_group_type *group);
static void idle_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void idle_enter_online(void *payload, stm_state_type previous_state);
static void idle_exit_online(void *payload, stm_state_type previous_state);
static void idle_enter_sleep(void *payload, stm_state_type previous_state);
static void idle_exit_sleep(void *payload, stm_state_type previous_state);
static void idle_enter_access(void *payload, stm_state_type previous_state);
static void idle_exit_access(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHIDLE_SM_NUMBER_OF_STATES 4
#define SRCHIDLE_SM_NUMBER_OF_INPUTS 36


/* State enumeration */
enum
{
  ONLINE_STATE,
  SLEEP_STATE,
  ACCESS_STATE,
  ASSIGN_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHIDLE_SM_states[ SRCHIDLE_SM_NUMBER_OF_STATES ] =
{
  {"ONLINE_STATE", idle_enter_online, idle_exit_online},
  {"SLEEP_STATE", idle_enter_sleep, idle_exit_sleep},
  {"ACCESS_STATE", idle_enter_access, idle_exit_access},
  {"ASSIGN_STATE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHIDLE_SM_inputs[ SRCHIDLE_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_SLEEP_F, "SRCH_SLEEP_F"},
  {(stm_input_type)SRCH_TC_F, "SRCH_TC_F"},
  {(stm_input_type)SRCH_ACCESS_F, "SRCH_ACCESS_F"},
  {(stm_input_type)SRCH_PARM_F, "SRCH_PARM_F"},
  {(stm_input_type)SRCH_IDLE_ASET_F, "SRCH_IDLE_ASET_F"},
  {(stm_input_type)SRCH_IDLE_NSET_F, "SRCH_IDLE_NSET_F"},
  {(stm_input_type)SRCH_RESELECT_MEAS_F, "SRCH_RESELECT_MEAS_F"},
  {(stm_input_type)SRCH_PC_ASSIGN_F, "SRCH_PC_ASSIGN_F"},
  {(stm_input_type)SRCH_CHAN_CONFIG_F, "SRCH_CHAN_CONFIG_F"},
  {(stm_input_type)SRCH_BC_INFO_F, "SRCH_BC_INFO_F"},
  {(stm_input_type)SRCH_IDLE_F, "SRCH_IDLE_F"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)HO_REQUEST_CMD, "HO_REQUEST_CMD"},
  {(stm_input_type)FRAME_STROBE_CMD, "FRAME_STROBE_CMD"},
  {(stm_input_type)RESELECTION_CHECK_CMD, "RESELECTION_CHECK_CMD"},
  {(stm_input_type)SLEEP_OK_CMD, "SLEEP_OK_CMD"},
  {(stm_input_type)RSSI_TIMER_CMD, "RSSI_TIMER_CMD"},
  {(stm_input_type)RF_REPORT_TIMER_CMD, "RF_REPORT_TIMER_CMD"},
  {(stm_input_type)DEACTIVATE_ZZ_CMD, "DEACTIVATE_ZZ_CMD"},
  {(stm_input_type)DELAYED_ASET_CMD, "DELAYED_ASET_CMD"},
  {(stm_input_type)ONLINE_RF_GRANTED_CMD, "ONLINE_RF_GRANTED_CMD"},
  {(stm_input_type)ONLINE_TUNE_DONE_CMD, "ONLINE_TUNE_DONE_CMD"},
  {(stm_input_type)RX_MOD_GRANTED_CMD, "RX_MOD_GRANTED_CMD"},
  {(stm_input_type)RX_MOD_DENIED_CMD, "RX_MOD_DENIED_CMD"},
  {(stm_input_type)IDLE_DIV_REQUEST_CMD, "IDLE_DIV_REQUEST_CMD"},
  {(stm_input_type)PAGE_MATCH_CMD, "PAGE_MATCH_CMD"},
  {(stm_input_type)CDMA_RSP_CMD, "CDMA_RSP_CMD"},
  {(stm_input_type)IDLE_RSP_CMD, "IDLE_RSP_CMD"},
  {(stm_input_type)SRCH_PILOT_LIST_F, "SRCH_PILOT_LIST_F"},
  {(stm_input_type)SRCH_IDLE_FIND_STRONG_PILOT_F, "SRCH_IDLE_FIND_STRONG_PILOT_F"},
  {(stm_input_type)SCAN_ALL_RPT_CMD, "SCAN_ALL_RPT_CMD"},
  {(stm_input_type)RF_GRANTED_CMD, "RF_GRANTED_CMD"},
  {(stm_input_type)RF_PREP_CMD, "RF_PREP_CMD"},
  {(stm_input_type)TUNE_DONE_CMD, "TUNE_DONE_CMD"},
  {(stm_input_type)ABORT_COMPLETE_CMD, "ABORT_COMPLETE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHIDLE_SM_transitions[ SRCHIDLE_SM_NUMBER_OF_STATES *  SRCHIDLE_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state ONLINE_STATE */
    idle_process_cdma_cmd,    /* SRCH_CDMA_F */
    idle_process_sleep_cmd,    /* SRCH_SLEEP_F */
    idle_process_tc_cmd,    /* SRCH_TC_F */
    idle_process_access_cmd,    /* SRCH_ACCESS_F */
    idle_process_parm_cmd,    /* SRCH_PARM_F */
    idle_process_idle_aset_cmd,    /* SRCH_IDLE_ASET_F */
    idle_process_idle_nset_cmd,    /* SRCH_IDLE_NSET_F */
    idle_process_reselect_meas_cmd,    /* SRCH_RESELECT_MEAS_F */
    idle_process_pc_assign_cmd,    /* SRCH_PC_ASSIGN_F */
    idle_process_chan_config_cmd,    /* SRCH_CHAN_CONFIG_F */
    idle_process_bc_info_cmd,    /* SRCH_BC_INFO_F */
    idle_process_idle_cmd,    /* SRCH_IDLE_F */
    idle_process_roll,    /* ROLL_CMD */
    idle_process_ho_request,    /* HO_REQUEST_CMD */
    idle_process_frame_strobe,    /* FRAME_STROBE_CMD */
    idle_process_reselection_check,    /* RESELECTION_CHECK_CMD */
    idle_process_sleep_ok,    /* SLEEP_OK_CMD */
    idle_process_rssi_timer,    /* RSSI_TIMER_CMD */
    idle_process_rf_report_timer,    /* RF_REPORT_TIMER_CMD */
    idle_deactivate_zz,    /* DEACTIVATE_ZZ_CMD */
    idle_process_delayed_aset,    /* DELAYED_ASET_CMD */
    idle_process_online_rf_granted,    /* ONLINE_RF_GRANTED_CMD */
    idle_process_online_rf_tuned,    /* ONLINE_TUNE_DONE_CMD */
    idle_process_online_rx_mod_granted,    /* RX_MOD_GRANTED_CMD */
    idle_process_online_rx_mod_denied,    /* RX_MOD_DENIED_CMD */
    idle_process_div_request_cmd,    /* IDLE_DIV_REQUEST_CMD */
    SRCHIDLE_SM_ignore_input,    /* PAGE_MATCH_CMD */
    NULL,    /* CDMA_RSP_CMD */
    NULL,    /* IDLE_RSP_CMD */
    NULL,    /* SRCH_PILOT_LIST_F */
    NULL,    /* SRCH_IDLE_FIND_STRONG_PILOT_F */
    NULL,    /* SCAN_ALL_RPT_CMD */
    NULL,    /* RF_GRANTED_CMD */
    NULL,    /* RF_PREP_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state SLEEP_STATE */
    idle_process_zz_cdma_cmd,    /* SRCH_CDMA_F */
    idle_process_zz_sleep_cmd,    /* SRCH_SLEEP_F */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_ACCESS_F */
    idle_process_parm_cmd,    /* SRCH_PARM_F */
    idle_process_zz_idle_aset_cmd,    /* SRCH_IDLE_ASET_F */
    idle_process_zz_idle_nset_cmd,    /* SRCH_IDLE_NSET_F */
    NULL,    /* SRCH_RESELECT_MEAS_F */
    NULL,    /* SRCH_PC_ASSIGN_F */
    NULL,    /* SRCH_CHAN_CONFIG_F */
    idle_process_zz_bc_info_cmd,    /* SRCH_BC_INFO_F */
    idle_process_zz_idle_cmd,    /* SRCH_IDLE_F */
    idle_process_zz_roll,    /* ROLL_CMD */
    NULL,    /* HO_REQUEST_CMD */
    idle_process_frame_strobe,    /* FRAME_STROBE_CMD */
    idle_process_reselection_check,    /* RESELECTION_CHECK_CMD */
    NULL,    /* SLEEP_OK_CMD */
    NULL,    /* RSSI_TIMER_CMD */
    NULL,    /* RF_REPORT_TIMER_CMD */
    NULL,    /* DEACTIVATE_ZZ_CMD */
    NULL,    /* DELAYED_ASET_CMD */
    NULL,    /* ONLINE_RF_GRANTED_CMD */
    NULL,    /* ONLINE_TUNE_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    SRCHIDLE_SM_ignore_input,    /* IDLE_DIV_REQUEST_CMD */
    idle_process_page_match,    /* PAGE_MATCH_CMD */
    idle_process_internal_cdma_cmd,    /* CDMA_RSP_CMD */
    idle_process_zz_idle_rsp_cmd,    /* IDLE_RSP_CMD */
    NULL,    /* SRCH_PILOT_LIST_F */
    NULL,    /* SRCH_IDLE_FIND_STRONG_PILOT_F */
    NULL,    /* SCAN_ALL_RPT_CMD */
    NULL,    /* RF_GRANTED_CMD */
    NULL,    /* RF_PREP_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state ACCESS_STATE */
    idle_process_cdma_cmd,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_SLEEP_F */
    idle_process_tc_cmd,    /* SRCH_TC_F */
    idle_process_access_cmd,    /* SRCH_ACCESS_F */
    idle_process_parm_cmd,    /* SRCH_PARM_F */
    idle_process_idle_aset_cmd,    /* SRCH_IDLE_ASET_F */
    idle_process_idle_nset_cmd,    /* SRCH_IDLE_NSET_F */
    NULL,    /* SRCH_RESELECT_MEAS_F */
    idle_process_pc_assign_cmd,    /* SRCH_PC_ASSIGN_F */
    idle_process_chan_config_cmd,    /* SRCH_CHAN_CONFIG_F */
    NULL,    /* SRCH_BC_INFO_F */
    idle_process_idle_cmd,    /* SRCH_IDLE_F */
    idle_process_access_roll,    /* ROLL_CMD */
    idle_process_ho_request,    /* HO_REQUEST_CMD */
    idle_process_frame_strobe,    /* FRAME_STROBE_CMD */
    idle_process_reselection_check,    /* RESELECTION_CHECK_CMD */
    NULL,    /* SLEEP_OK_CMD */
    idle_process_rssi_timer,    /* RSSI_TIMER_CMD */
    idle_process_rf_report_timer,    /* RF_REPORT_TIMER_CMD */
    idle_deactivate_zz,    /* DEACTIVATE_ZZ_CMD */
    idle_process_delayed_aset,    /* DELAYED_ASET_CMD */
    SRCHIDLE_SM_ignore_input,    /* ONLINE_RF_GRANTED_CMD */
    SRCHIDLE_SM_ignore_input,    /* ONLINE_TUNE_DONE_CMD */
    idle_process_access_rx_mod_granted,    /* RX_MOD_GRANTED_CMD */
    idle_process_access_rx_mod_denied,    /* RX_MOD_DENIED_CMD */
    SRCHIDLE_SM_ignore_input,    /* IDLE_DIV_REQUEST_CMD */
    SRCHIDLE_SM_ignore_input,    /* PAGE_MATCH_CMD */
    NULL,    /* CDMA_RSP_CMD */
    NULL,    /* IDLE_RSP_CMD */
    idle_process_pilot_list_cmd,    /* SRCH_PILOT_LIST_F */
    idle_process_find_idle_strong_pilot_cmd,    /* SRCH_IDLE_FIND_STRONG_PILOT_F */
    idle_process_scan_all_rpt,    /* SCAN_ALL_RPT_CMD */
    idle_process_access_rf_granted,    /* RF_GRANTED_CMD */
    idle_process_access_prep_tune,    /* RF_PREP_CMD */
    idle_process_access_rf_tuned,    /* TUNE_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state ASSIGN_STATE */
    idle_process_cdma_cmd,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_SLEEP_F */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_ACCESS_F */
    NULL,    /* SRCH_PARM_F */
    idle_process_assign_aset_cmd,    /* SRCH_IDLE_ASET_F */
    NULL,    /* SRCH_IDLE_NSET_F */
    NULL,    /* SRCH_RESELECT_MEAS_F */
    NULL,    /* SRCH_PC_ASSIGN_F */
    NULL,    /* SRCH_CHAN_CONFIG_F */
    NULL,    /* SRCH_BC_INFO_F */
    NULL,    /* SRCH_IDLE_F */
    idle_process_roll,    /* ROLL_CMD */
    SRCHIDLE_SM_ignore_input,    /* HO_REQUEST_CMD */
    idle_process_frame_strobe,    /* FRAME_STROBE_CMD */
    idle_process_reselection_check,    /* RESELECTION_CHECK_CMD */
    NULL,    /* SLEEP_OK_CMD */
    idle_process_rssi_timer,    /* RSSI_TIMER_CMD */
    idle_process_rf_report_timer,    /* RF_REPORT_TIMER_CMD */
    NULL,    /* DEACTIVATE_ZZ_CMD */
    NULL,    /* DELAYED_ASET_CMD */
    NULL,    /* ONLINE_RF_GRANTED_CMD */
    NULL,    /* ONLINE_TUNE_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    SRCHIDLE_SM_ignore_input,    /* IDLE_DIV_REQUEST_CMD */
    SRCHIDLE_SM_ignore_input,    /* PAGE_MATCH_CMD */
    NULL,    /* CDMA_RSP_CMD */
    NULL,    /* IDLE_RSP_CMD */
    NULL,    /* SRCH_PILOT_LIST_F */
    NULL,    /* SRCH_IDLE_FIND_STRONG_PILOT_F */
    idle_process_assign_rpt,    /* SCAN_ALL_RPT_CMD */
    NULL,    /* RF_GRANTED_CMD */
    NULL,    /* RF_PREP_CMD */
    NULL,    /* TUNE_DONE_CMD */
    idle_process_assign_after_abort,    /* ABORT_COMPLETE_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHIDLE_SM =
{
  "SRCHIDLE_SM", /* state machine name */
  35446, /* unique SM id (hash of name) */
  idle_entry, /* state machine entry function */
  idle_exit, /* state machine exit function */
  ONLINE_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SRCHIDLE_SM_NUMBER_OF_STATES,
  SRCHIDLE_SM_NUMBER_OF_INPUTS,
  SRCHIDLE_SM_states,
  SRCHIDLE_SM_inputs,
  SRCHIDLE_SM_transitions,
};

/* End machine generated code for state machine: SRCHIDLE_SM */

