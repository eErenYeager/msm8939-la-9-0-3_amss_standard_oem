/*=============================================================================

  srchzz_qpch_ontl_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_qpch_ontl_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_QPCH_ONTL_SM */

/* Transition function prototypes */
static stm_state_type qpch_ontl_setup_sleep(void *);
static stm_state_type qpch_ontl_process_init_roll(void *);
static stm_state_type qpch_ontl_abort(void *);
static stm_state_type qpch_ontl_xfer_doze(void *);
static stm_state_type qpch_ontl_remember_rude_wakeup(void *);
static stm_state_type qpch_ontl_process_freq_track_off_done(void *);
static stm_state_type qpch_ontl_process_rf_disabled_cmd(void *);
static stm_state_type qpch_ontl_process_go_to_sleep_cmd(void *);
static stm_state_type qpch_ontl_process_rf_disable_comp_cmd(void *);
static stm_state_type SRCHZZ_QPCH_ONTL_SM_ignore_input(void *);
static stm_state_type qpch_ontl_change_sleep(void *);
static stm_state_type qpch_ontl_process_doze_rude_wakeup(void *);
static stm_state_type qpch_ontl_process_doze_freq_track_off_done(void *);
static stm_state_type qpch_ontl_process_doze_rf_disabled_cmd(void *);
static stm_state_type qpch_ontl_process_go_to_doze_cmd(void *);
static stm_state_type qpch_ontl_process_doze_rf_disable_comp_cmd(void *);
static stm_state_type qpch_ontl_process_wakeup(void *);
static stm_state_type qpch_ontl_process_chain_granted(void *);
static stm_state_type qpch_ontl_process_chain_denied(void *);
static stm_state_type qpch_ontl_process_rude_wakeup(void *);
static stm_state_type qpch_ontl_process_rf_prep_comp(void *);
static stm_state_type qpch_ontl_process_no_rf_lock(void *);
static stm_state_type qpch_ontl_remember_cx8_on(void *);
static stm_state_type qpch_ontl_process_cx8_on(void *);
static stm_state_type qpch_ontl_process_tune_complete(void *);
static stm_state_type qpch_ontl_adjust_combiner_time(void *);
static stm_state_type process_demod_roll(void *);
static stm_state_type qpch_ontl_process_demod_done(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SRCHZZ_QPCH_ONTL_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void qpch_ontl_entry(stm_group_type *group);
static void qpch_ontl_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void qpch_ontl_enter_init(void *payload, stm_state_type previous_state);
static void qpch_ontl_enter_doze(void *payload, stm_state_type previous_state);
static void qpch_ontl_exit_doze(void *payload, stm_state_type previous_state);
static void qpch_ontl_enter_sleep(void *payload, stm_state_type previous_state);
static void qpch_ontl_exit_sleep(void *payload, stm_state_type previous_state);
static void qpch_ontl_enter_wakeup(void *payload, stm_state_type previous_state);
static void qpch_ontl_enter_demod(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_STATES 5
#define SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_INPUTS 19


/* State enumeration */
enum
{
  INIT_STATE,
  DOZE_STATE,
  SLEEP_STATE,
  WAKEUP_STATE,
  DEMOD_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_QPCH_ONTL_SM_states[ SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", qpch_ontl_enter_init, NULL},
  {"DOZE_STATE", qpch_ontl_enter_doze, qpch_ontl_exit_doze},
  {"SLEEP_STATE", qpch_ontl_enter_sleep, qpch_ontl_exit_sleep},
  {"WAKEUP_STATE", qpch_ontl_enter_wakeup, NULL},
  {"DEMOD_STATE", qpch_ontl_enter_demod, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_QPCH_ONTL_SM_inputs[ SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)ABORT_CMD, "ABORT_CMD"},
  {(stm_input_type)DOZE_CMD, "DOZE_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)RX_RF_DISABLED_CMD, "RX_RF_DISABLED_CMD"},
  {(stm_input_type)GO_TO_SLEEP_CMD, "GO_TO_SLEEP_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)ADJUST_TIMING_CMD, "ADJUST_TIMING_CMD"},
  {(stm_input_type)GO_TO_DOZE_CMD, "GO_TO_DOZE_CMD"},
  {(stm_input_type)WAKEUP_CMD, "WAKEUP_CMD"},
  {(stm_input_type)RX_CH_GRANTED_CMD, "RX_CH_GRANTED_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)NO_RF_LOCK_CMD, "NO_RF_LOCK_CMD"},
  {(stm_input_type)CX8_ON_CMD, "CX8_ON_CMD"},
  {(stm_input_type)RX_TUNE_COMP_CMD, "RX_TUNE_COMP_CMD"},
  {(stm_input_type)QPCH_DEMOD_DONE_CMD, "QPCH_DEMOD_DONE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_QPCH_ONTL_SM_transitions[ SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_STATES *  SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    qpch_ontl_setup_sleep,    /* START_SLEEP_CMD */
    qpch_ontl_process_init_roll,    /* ROLL_CMD */
    qpch_ontl_abort,    /* ABORT_CMD */
    qpch_ontl_xfer_doze,    /* DOZE_CMD */
    qpch_ontl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    qpch_ontl_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    qpch_ontl_process_rf_disabled_cmd,    /* RX_RF_DISABLED_CMD */
    qpch_ontl_process_go_to_sleep_cmd,    /* GO_TO_SLEEP_CMD */
    qpch_ontl_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    SRCHZZ_QPCH_ONTL_SM_ignore_input,    /* ADJUST_TIMING_CMD */
    NULL,    /* GO_TO_DOZE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state DOZE_STATE */
    qpch_ontl_change_sleep,    /* START_SLEEP_CMD */
    NULL,    /* ROLL_CMD */
    qpch_ontl_abort,    /* ABORT_CMD */
    NULL,    /* DOZE_CMD */
    qpch_ontl_process_doze_rude_wakeup,    /* WAKEUP_NOW_CMD */
    qpch_ontl_process_doze_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    qpch_ontl_process_doze_rf_disabled_cmd,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    qpch_ontl_process_doze_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    qpch_ontl_process_go_to_doze_cmd,    /* GO_TO_DOZE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state SLEEP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ROLL_CMD */
    qpch_ontl_abort,    /* ABORT_CMD */
    NULL,    /* DOZE_CMD */
    qpch_ontl_process_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    qpch_ontl_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* GO_TO_DOZE_CMD */
    qpch_ontl_process_wakeup,    /* WAKEUP_CMD */
    qpch_ontl_process_chain_granted,    /* RX_CH_GRANTED_CMD */
    qpch_ontl_process_chain_denied,    /* RX_CH_DENIED_CMD */
    qpch_ontl_process_rf_prep_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    qpch_ontl_process_no_rf_lock,    /* NO_RF_LOCK_CMD */
    qpch_ontl_remember_cx8_on,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state WAKEUP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* ABORT_CMD */
    NULL,    /* DOZE_CMD */
    qpch_ontl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* GO_TO_DOZE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    qpch_ontl_process_cx8_on,    /* CX8_ON_CMD */
    qpch_ontl_process_tune_complete,    /* RX_TUNE_COMP_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state DEMOD_STATE */
    NULL,    /* START_SLEEP_CMD */
    process_demod_roll,    /* ROLL_CMD */
    NULL,    /* ABORT_CMD */
    NULL,    /* DOZE_CMD */
    qpch_ontl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    qpch_ontl_adjust_combiner_time,    /* ADJUST_TIMING_CMD */
    NULL,    /* GO_TO_DOZE_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    qpch_ontl_process_demod_done,    /* QPCH_DEMOD_DONE_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_QPCH_ONTL_SM =
{
  "SRCHZZ_QPCH_ONTL_SM", /* state machine name */
  59860, /* unique SM id (hash of name) */
  qpch_ontl_entry, /* state machine entry function */
  qpch_ontl_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_STATES,
  SRCHZZ_QPCH_ONTL_SM_NUMBER_OF_INPUTS,
  SRCHZZ_QPCH_ONTL_SM_states,
  SRCHZZ_QPCH_ONTL_SM_inputs,
  SRCHZZ_QPCH_ONTL_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_QPCH_ONTL_SM */

