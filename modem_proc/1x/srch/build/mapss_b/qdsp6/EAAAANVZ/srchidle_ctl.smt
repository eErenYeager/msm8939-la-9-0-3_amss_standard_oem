/*=============================================================================

  srchidle_ctl.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchidle_ctl.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine group: SRCH_IDLE_GROUP */

/* State machine group entry/exit function prototypes */
static void idlectl_group_entry(void);
static void idlectl_group_exit(void);



/* State machine group signal mapper function prototype */
static boolean idlectl_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_IDLE_GROUP_sm_list[ 10 ] =
{
  &SRCH_SCHED_SM,
  &SRCHIDLE_SM,
  &SRCHZZ_SM,
  &SRCHZZ_IS95A_SM,
  &SRCHZZ_IS2000_SM,
  &SRCHZZ_QPCH_SM,
  &SRCHZZ_QPCH_OFFTL_SM,
  &SRCHZZ_RTL_SM,
  &SRCHZZ_QPCH_ONTL_SM,
  &ONEXTOL_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_IDLE_GROUP_sig_list[ 1 ] =
{
  SRCH_CMD_Q_SIG,
};


/* State machine group definition */
stm_group_type SRCH_IDLE_GROUP =
{
  "SRCH_IDLE_GROUP", /* state machine group name */
  SRCH_IDLE_GROUP_sig_list, /* signal mapping table */
  1, /* number of signals in mapping table */
  idlectl_sig_mapper, /* signal mapper function */
  SRCH_IDLE_GROUP_sm_list, /* state machine table */
  10, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  idlectl_group_entry, /* group entry function */
  idlectl_group_exit, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_IDLE_GROUP */

