###############################################################################
#
#    srch_sys_meas_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srch_sys_meas_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SYS_MEAS_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SYS_MEAS:STANDBY:SYS_MEAS:STANDBY                            93a3 0 00 0    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:RF_WAIT                            93a3 0 00 1    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:MDSP_WAIT                          93a3 0 00 2    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:TUNE                               93a3 0 00 3    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:AGC_WAIT                           93a3 0 00 4    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:DETECT                             93a3 0 00 5    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:DWELL                              93a3 0 00 6    @red @white
SYS_MEAS:STANDBY:SYS_MEAS:DONE                               93a3 0 00 7    @red @white

SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:STANDBY                 93a3 0 01 0    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:RF_WAIT                 93a3 0 01 1    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:MDSP_WAIT               93a3 0 01 2    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:TUNE                    93a3 0 01 3    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:AGC_WAIT                93a3 0 01 4    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:DETECT                  93a3 0 01 5    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:DWELL                   93a3 0 01 6    @red @white
SYS_MEAS:STANDBY:FREQ_TRACK_OFF_DONE:DONE                    93a3 0 01 7    @red @white

SYS_MEAS:STANDBY:SRCH4_MDSP_REG:STANDBY                      93a3 0 02 0    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:RF_WAIT                      93a3 0 02 1    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:MDSP_WAIT                    93a3 0 02 2    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:TUNE                         93a3 0 02 3    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:AGC_WAIT                     93a3 0 02 4    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:DETECT                       93a3 0 02 5    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:DWELL                        93a3 0 02 6    @red @white
SYS_MEAS:STANDBY:SRCH4_MDSP_REG:DONE                         93a3 0 02 7    @red @white

SYS_MEAS:STANDBY:RF_RELEASE_DONE:STANDBY                     93a3 0 03 0    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:RF_WAIT                     93a3 0 03 1    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:MDSP_WAIT                   93a3 0 03 2    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:TUNE                        93a3 0 03 3    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:AGC_WAIT                    93a3 0 03 4    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:DETECT                      93a3 0 03 5    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:DWELL                       93a3 0 03 6    @red @white
SYS_MEAS:STANDBY:RF_RELEASE_DONE:DONE                        93a3 0 03 7    @red @white

SYS_MEAS:STANDBY:RF_LOCKED:STANDBY                           93a3 0 04 0    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:RF_WAIT                           93a3 0 04 1    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:MDSP_WAIT                         93a3 0 04 2    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:TUNE                              93a3 0 04 3    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:AGC_WAIT                          93a3 0 04 4    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:DETECT                            93a3 0 04 5    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:DWELL                             93a3 0 04 6    @red @white
SYS_MEAS:STANDBY:RF_LOCKED:DONE                              93a3 0 04 7    @red @white

SYS_MEAS:STANDBY:ABORT:STANDBY                               93a3 0 05 0    @red @white
SYS_MEAS:STANDBY:ABORT:RF_WAIT                               93a3 0 05 1    @red @white
SYS_MEAS:STANDBY:ABORT:MDSP_WAIT                             93a3 0 05 2    @red @white
SYS_MEAS:STANDBY:ABORT:TUNE                                  93a3 0 05 3    @red @white
SYS_MEAS:STANDBY:ABORT:AGC_WAIT                              93a3 0 05 4    @red @white
SYS_MEAS:STANDBY:ABORT:DETECT                                93a3 0 05 5    @red @white
SYS_MEAS:STANDBY:ABORT:DWELL                                 93a3 0 05 6    @red @white
SYS_MEAS:STANDBY:ABORT:DONE                                  93a3 0 05 7    @red @white

SYS_MEAS:STANDBY:TUNE_DONE:STANDBY                           93a3 0 06 0    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:RF_WAIT                           93a3 0 06 1    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:MDSP_WAIT                         93a3 0 06 2    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:TUNE                              93a3 0 06 3    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:AGC_WAIT                          93a3 0 06 4    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:DETECT                            93a3 0 06 5    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:DWELL                             93a3 0 06 6    @red @white
SYS_MEAS:STANDBY:TUNE_DONE:DONE                              93a3 0 06 7    @red @white

SYS_MEAS:STANDBY:AGC_TIMER:STANDBY                           93a3 0 07 0    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:RF_WAIT                           93a3 0 07 1    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:MDSP_WAIT                         93a3 0 07 2    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:TUNE                              93a3 0 07 3    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:AGC_WAIT                          93a3 0 07 4    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:DETECT                            93a3 0 07 5    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:DWELL                             93a3 0 07 6    @red @white
SYS_MEAS:STANDBY:AGC_TIMER:DONE                              93a3 0 07 7    @red @white

SYS_MEAS:STANDBY:SRCH_DUMP:STANDBY                           93a3 0 08 0    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:RF_WAIT                           93a3 0 08 1    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:MDSP_WAIT                         93a3 0 08 2    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:TUNE                              93a3 0 08 3    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:AGC_WAIT                          93a3 0 08 4    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:DETECT                            93a3 0 08 5    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:DWELL                             93a3 0 08 6    @red @white
SYS_MEAS:STANDBY:SRCH_DUMP:DONE                              93a3 0 08 7    @red @white

SYS_MEAS:STANDBY:SRCH_LOST_DUMP:STANDBY                      93a3 0 09 0    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:RF_WAIT                      93a3 0 09 1    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:MDSP_WAIT                    93a3 0 09 2    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:TUNE                         93a3 0 09 3    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:AGC_WAIT                     93a3 0 09 4    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:DETECT                       93a3 0 09 5    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:DWELL                        93a3 0 09 6    @red @white
SYS_MEAS:STANDBY:SRCH_LOST_DUMP:DONE                         93a3 0 09 7    @red @white

SYS_MEAS:STANDBY:MEAS_NEXT:STANDBY                           93a3 0 0a 0    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:RF_WAIT                           93a3 0 0a 1    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:MDSP_WAIT                         93a3 0 0a 2    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:TUNE                              93a3 0 0a 3    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:AGC_WAIT                          93a3 0 0a 4    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:DETECT                            93a3 0 0a 5    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:DWELL                             93a3 0 0a 6    @red @white
SYS_MEAS:STANDBY:MEAS_NEXT:DONE                              93a3 0 0a 7    @red @white

SYS_MEAS:STANDBY:MEAS_DONE:STANDBY                           93a3 0 0b 0    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:RF_WAIT                           93a3 0 0b 1    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:MDSP_WAIT                         93a3 0 0b 2    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:TUNE                              93a3 0 0b 3    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:AGC_WAIT                          93a3 0 0b 4    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:DETECT                            93a3 0 0b 5    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:DWELL                             93a3 0 0b 6    @red @white
SYS_MEAS:STANDBY:MEAS_DONE:DONE                              93a3 0 0b 7    @red @white

SYS_MEAS:RF_WAIT:SYS_MEAS:STANDBY                            93a3 1 00 0    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:RF_WAIT                            93a3 1 00 1    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:MDSP_WAIT                          93a3 1 00 2    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:TUNE                               93a3 1 00 3    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:AGC_WAIT                           93a3 1 00 4    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:DETECT                             93a3 1 00 5    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:DWELL                              93a3 1 00 6    @red @white
SYS_MEAS:RF_WAIT:SYS_MEAS:DONE                               93a3 1 00 7    @red @white

SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:STANDBY                 93a3 1 01 0    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:RF_WAIT                 93a3 1 01 1    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:MDSP_WAIT               93a3 1 01 2    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:TUNE                    93a3 1 01 3    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:AGC_WAIT                93a3 1 01 4    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:DETECT                  93a3 1 01 5    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:DWELL                   93a3 1 01 6    @red @white
SYS_MEAS:RF_WAIT:FREQ_TRACK_OFF_DONE:DONE                    93a3 1 01 7    @red @white

SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:STANDBY                      93a3 1 02 0    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:RF_WAIT                      93a3 1 02 1    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:MDSP_WAIT                    93a3 1 02 2    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:TUNE                         93a3 1 02 3    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:AGC_WAIT                     93a3 1 02 4    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:DETECT                       93a3 1 02 5    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:DWELL                        93a3 1 02 6    @red @white
SYS_MEAS:RF_WAIT:SRCH4_MDSP_REG:DONE                         93a3 1 02 7    @red @white

SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:STANDBY                     93a3 1 03 0    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:RF_WAIT                     93a3 1 03 1    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:MDSP_WAIT                   93a3 1 03 2    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:TUNE                        93a3 1 03 3    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:AGC_WAIT                    93a3 1 03 4    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:DETECT                      93a3 1 03 5    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:DWELL                       93a3 1 03 6    @red @white
SYS_MEAS:RF_WAIT:RF_RELEASE_DONE:DONE                        93a3 1 03 7    @red @white

SYS_MEAS:RF_WAIT:RF_LOCKED:STANDBY                           93a3 1 04 0    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:RF_WAIT                           93a3 1 04 1    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:MDSP_WAIT                         93a3 1 04 2    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:TUNE                              93a3 1 04 3    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:AGC_WAIT                          93a3 1 04 4    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:DETECT                            93a3 1 04 5    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:DWELL                             93a3 1 04 6    @red @white
SYS_MEAS:RF_WAIT:RF_LOCKED:DONE                              93a3 1 04 7    @red @white

SYS_MEAS:RF_WAIT:ABORT:STANDBY                               93a3 1 05 0    @red @white
SYS_MEAS:RF_WAIT:ABORT:RF_WAIT                               93a3 1 05 1    @red @white
SYS_MEAS:RF_WAIT:ABORT:MDSP_WAIT                             93a3 1 05 2    @red @white
SYS_MEAS:RF_WAIT:ABORT:TUNE                                  93a3 1 05 3    @red @white
SYS_MEAS:RF_WAIT:ABORT:AGC_WAIT                              93a3 1 05 4    @red @white
SYS_MEAS:RF_WAIT:ABORT:DETECT                                93a3 1 05 5    @red @white
SYS_MEAS:RF_WAIT:ABORT:DWELL                                 93a3 1 05 6    @red @white
SYS_MEAS:RF_WAIT:ABORT:DONE                                  93a3 1 05 7    @red @white

SYS_MEAS:RF_WAIT:TUNE_DONE:STANDBY                           93a3 1 06 0    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:RF_WAIT                           93a3 1 06 1    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:MDSP_WAIT                         93a3 1 06 2    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:TUNE                              93a3 1 06 3    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:AGC_WAIT                          93a3 1 06 4    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:DETECT                            93a3 1 06 5    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:DWELL                             93a3 1 06 6    @red @white
SYS_MEAS:RF_WAIT:TUNE_DONE:DONE                              93a3 1 06 7    @red @white

SYS_MEAS:RF_WAIT:AGC_TIMER:STANDBY                           93a3 1 07 0    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:RF_WAIT                           93a3 1 07 1    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:MDSP_WAIT                         93a3 1 07 2    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:TUNE                              93a3 1 07 3    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:AGC_WAIT                          93a3 1 07 4    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:DETECT                            93a3 1 07 5    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:DWELL                             93a3 1 07 6    @red @white
SYS_MEAS:RF_WAIT:AGC_TIMER:DONE                              93a3 1 07 7    @red @white

SYS_MEAS:RF_WAIT:SRCH_DUMP:STANDBY                           93a3 1 08 0    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:RF_WAIT                           93a3 1 08 1    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:MDSP_WAIT                         93a3 1 08 2    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:TUNE                              93a3 1 08 3    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:AGC_WAIT                          93a3 1 08 4    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:DETECT                            93a3 1 08 5    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:DWELL                             93a3 1 08 6    @red @white
SYS_MEAS:RF_WAIT:SRCH_DUMP:DONE                              93a3 1 08 7    @red @white

SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:STANDBY                      93a3 1 09 0    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:RF_WAIT                      93a3 1 09 1    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:MDSP_WAIT                    93a3 1 09 2    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:TUNE                         93a3 1 09 3    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:AGC_WAIT                     93a3 1 09 4    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:DETECT                       93a3 1 09 5    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:DWELL                        93a3 1 09 6    @red @white
SYS_MEAS:RF_WAIT:SRCH_LOST_DUMP:DONE                         93a3 1 09 7    @red @white

SYS_MEAS:RF_WAIT:MEAS_NEXT:STANDBY                           93a3 1 0a 0    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:RF_WAIT                           93a3 1 0a 1    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:MDSP_WAIT                         93a3 1 0a 2    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:TUNE                              93a3 1 0a 3    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:AGC_WAIT                          93a3 1 0a 4    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:DETECT                            93a3 1 0a 5    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:DWELL                             93a3 1 0a 6    @red @white
SYS_MEAS:RF_WAIT:MEAS_NEXT:DONE                              93a3 1 0a 7    @red @white

SYS_MEAS:RF_WAIT:MEAS_DONE:STANDBY                           93a3 1 0b 0    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:RF_WAIT                           93a3 1 0b 1    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:MDSP_WAIT                         93a3 1 0b 2    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:TUNE                              93a3 1 0b 3    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:AGC_WAIT                          93a3 1 0b 4    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:DETECT                            93a3 1 0b 5    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:DWELL                             93a3 1 0b 6    @red @white
SYS_MEAS:RF_WAIT:MEAS_DONE:DONE                              93a3 1 0b 7    @red @white

SYS_MEAS:MDSP_WAIT:SYS_MEAS:STANDBY                          93a3 2 00 0    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:RF_WAIT                          93a3 2 00 1    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:MDSP_WAIT                        93a3 2 00 2    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:TUNE                             93a3 2 00 3    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:AGC_WAIT                         93a3 2 00 4    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:DETECT                           93a3 2 00 5    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:DWELL                            93a3 2 00 6    @red @white
SYS_MEAS:MDSP_WAIT:SYS_MEAS:DONE                             93a3 2 00 7    @red @white

SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:STANDBY               93a3 2 01 0    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:RF_WAIT               93a3 2 01 1    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:MDSP_WAIT             93a3 2 01 2    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:TUNE                  93a3 2 01 3    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:AGC_WAIT              93a3 2 01 4    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:DETECT                93a3 2 01 5    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:DWELL                 93a3 2 01 6    @red @white
SYS_MEAS:MDSP_WAIT:FREQ_TRACK_OFF_DONE:DONE                  93a3 2 01 7    @red @white

SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:STANDBY                    93a3 2 02 0    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:RF_WAIT                    93a3 2 02 1    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:MDSP_WAIT                  93a3 2 02 2    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:TUNE                       93a3 2 02 3    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:AGC_WAIT                   93a3 2 02 4    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:DETECT                     93a3 2 02 5    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:DWELL                      93a3 2 02 6    @red @white
SYS_MEAS:MDSP_WAIT:SRCH4_MDSP_REG:DONE                       93a3 2 02 7    @red @white

SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:STANDBY                   93a3 2 03 0    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:RF_WAIT                   93a3 2 03 1    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:MDSP_WAIT                 93a3 2 03 2    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:TUNE                      93a3 2 03 3    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:AGC_WAIT                  93a3 2 03 4    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:DETECT                    93a3 2 03 5    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:DWELL                     93a3 2 03 6    @red @white
SYS_MEAS:MDSP_WAIT:RF_RELEASE_DONE:DONE                      93a3 2 03 7    @red @white

SYS_MEAS:MDSP_WAIT:RF_LOCKED:STANDBY                         93a3 2 04 0    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:RF_WAIT                         93a3 2 04 1    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:MDSP_WAIT                       93a3 2 04 2    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:TUNE                            93a3 2 04 3    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:AGC_WAIT                        93a3 2 04 4    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:DETECT                          93a3 2 04 5    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:DWELL                           93a3 2 04 6    @red @white
SYS_MEAS:MDSP_WAIT:RF_LOCKED:DONE                            93a3 2 04 7    @red @white

SYS_MEAS:MDSP_WAIT:ABORT:STANDBY                             93a3 2 05 0    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:RF_WAIT                             93a3 2 05 1    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:MDSP_WAIT                           93a3 2 05 2    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:TUNE                                93a3 2 05 3    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:AGC_WAIT                            93a3 2 05 4    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:DETECT                              93a3 2 05 5    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:DWELL                               93a3 2 05 6    @red @white
SYS_MEAS:MDSP_WAIT:ABORT:DONE                                93a3 2 05 7    @red @white

SYS_MEAS:MDSP_WAIT:TUNE_DONE:STANDBY                         93a3 2 06 0    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:RF_WAIT                         93a3 2 06 1    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:MDSP_WAIT                       93a3 2 06 2    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:TUNE                            93a3 2 06 3    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:AGC_WAIT                        93a3 2 06 4    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:DETECT                          93a3 2 06 5    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:DWELL                           93a3 2 06 6    @red @white
SYS_MEAS:MDSP_WAIT:TUNE_DONE:DONE                            93a3 2 06 7    @red @white

SYS_MEAS:MDSP_WAIT:AGC_TIMER:STANDBY                         93a3 2 07 0    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:RF_WAIT                         93a3 2 07 1    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:MDSP_WAIT                       93a3 2 07 2    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:TUNE                            93a3 2 07 3    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:AGC_WAIT                        93a3 2 07 4    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:DETECT                          93a3 2 07 5    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:DWELL                           93a3 2 07 6    @red @white
SYS_MEAS:MDSP_WAIT:AGC_TIMER:DONE                            93a3 2 07 7    @red @white

SYS_MEAS:MDSP_WAIT:SRCH_DUMP:STANDBY                         93a3 2 08 0    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:RF_WAIT                         93a3 2 08 1    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:MDSP_WAIT                       93a3 2 08 2    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:TUNE                            93a3 2 08 3    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:AGC_WAIT                        93a3 2 08 4    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:DETECT                          93a3 2 08 5    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:DWELL                           93a3 2 08 6    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_DUMP:DONE                            93a3 2 08 7    @red @white

SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:STANDBY                    93a3 2 09 0    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:RF_WAIT                    93a3 2 09 1    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:MDSP_WAIT                  93a3 2 09 2    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:TUNE                       93a3 2 09 3    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:AGC_WAIT                   93a3 2 09 4    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:DETECT                     93a3 2 09 5    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:DWELL                      93a3 2 09 6    @red @white
SYS_MEAS:MDSP_WAIT:SRCH_LOST_DUMP:DONE                       93a3 2 09 7    @red @white

SYS_MEAS:MDSP_WAIT:MEAS_NEXT:STANDBY                         93a3 2 0a 0    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:RF_WAIT                         93a3 2 0a 1    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:MDSP_WAIT                       93a3 2 0a 2    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:TUNE                            93a3 2 0a 3    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:AGC_WAIT                        93a3 2 0a 4    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:DETECT                          93a3 2 0a 5    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:DWELL                           93a3 2 0a 6    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_NEXT:DONE                            93a3 2 0a 7    @red @white

SYS_MEAS:MDSP_WAIT:MEAS_DONE:STANDBY                         93a3 2 0b 0    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:RF_WAIT                         93a3 2 0b 1    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:MDSP_WAIT                       93a3 2 0b 2    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:TUNE                            93a3 2 0b 3    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:AGC_WAIT                        93a3 2 0b 4    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:DETECT                          93a3 2 0b 5    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:DWELL                           93a3 2 0b 6    @red @white
SYS_MEAS:MDSP_WAIT:MEAS_DONE:DONE                            93a3 2 0b 7    @red @white

SYS_MEAS:TUNE:SYS_MEAS:STANDBY                               93a3 3 00 0    @red @white
SYS_MEAS:TUNE:SYS_MEAS:RF_WAIT                               93a3 3 00 1    @red @white
SYS_MEAS:TUNE:SYS_MEAS:MDSP_WAIT                             93a3 3 00 2    @red @white
SYS_MEAS:TUNE:SYS_MEAS:TUNE                                  93a3 3 00 3    @red @white
SYS_MEAS:TUNE:SYS_MEAS:AGC_WAIT                              93a3 3 00 4    @red @white
SYS_MEAS:TUNE:SYS_MEAS:DETECT                                93a3 3 00 5    @red @white
SYS_MEAS:TUNE:SYS_MEAS:DWELL                                 93a3 3 00 6    @red @white
SYS_MEAS:TUNE:SYS_MEAS:DONE                                  93a3 3 00 7    @red @white

SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:STANDBY                    93a3 3 01 0    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:RF_WAIT                    93a3 3 01 1    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:MDSP_WAIT                  93a3 3 01 2    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:TUNE                       93a3 3 01 3    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:AGC_WAIT                   93a3 3 01 4    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:DETECT                     93a3 3 01 5    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:DWELL                      93a3 3 01 6    @red @white
SYS_MEAS:TUNE:FREQ_TRACK_OFF_DONE:DONE                       93a3 3 01 7    @red @white

SYS_MEAS:TUNE:SRCH4_MDSP_REG:STANDBY                         93a3 3 02 0    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:RF_WAIT                         93a3 3 02 1    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:MDSP_WAIT                       93a3 3 02 2    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:TUNE                            93a3 3 02 3    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:AGC_WAIT                        93a3 3 02 4    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:DETECT                          93a3 3 02 5    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:DWELL                           93a3 3 02 6    @red @white
SYS_MEAS:TUNE:SRCH4_MDSP_REG:DONE                            93a3 3 02 7    @red @white

SYS_MEAS:TUNE:RF_RELEASE_DONE:STANDBY                        93a3 3 03 0    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:RF_WAIT                        93a3 3 03 1    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:MDSP_WAIT                      93a3 3 03 2    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:TUNE                           93a3 3 03 3    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:AGC_WAIT                       93a3 3 03 4    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:DETECT                         93a3 3 03 5    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:DWELL                          93a3 3 03 6    @red @white
SYS_MEAS:TUNE:RF_RELEASE_DONE:DONE                           93a3 3 03 7    @red @white

SYS_MEAS:TUNE:RF_LOCKED:STANDBY                              93a3 3 04 0    @red @white
SYS_MEAS:TUNE:RF_LOCKED:RF_WAIT                              93a3 3 04 1    @red @white
SYS_MEAS:TUNE:RF_LOCKED:MDSP_WAIT                            93a3 3 04 2    @red @white
SYS_MEAS:TUNE:RF_LOCKED:TUNE                                 93a3 3 04 3    @red @white
SYS_MEAS:TUNE:RF_LOCKED:AGC_WAIT                             93a3 3 04 4    @red @white
SYS_MEAS:TUNE:RF_LOCKED:DETECT                               93a3 3 04 5    @red @white
SYS_MEAS:TUNE:RF_LOCKED:DWELL                                93a3 3 04 6    @red @white
SYS_MEAS:TUNE:RF_LOCKED:DONE                                 93a3 3 04 7    @red @white

SYS_MEAS:TUNE:ABORT:STANDBY                                  93a3 3 05 0    @red @white
SYS_MEAS:TUNE:ABORT:RF_WAIT                                  93a3 3 05 1    @red @white
SYS_MEAS:TUNE:ABORT:MDSP_WAIT                                93a3 3 05 2    @red @white
SYS_MEAS:TUNE:ABORT:TUNE                                     93a3 3 05 3    @red @white
SYS_MEAS:TUNE:ABORT:AGC_WAIT                                 93a3 3 05 4    @red @white
SYS_MEAS:TUNE:ABORT:DETECT                                   93a3 3 05 5    @red @white
SYS_MEAS:TUNE:ABORT:DWELL                                    93a3 3 05 6    @red @white
SYS_MEAS:TUNE:ABORT:DONE                                     93a3 3 05 7    @red @white

SYS_MEAS:TUNE:TUNE_DONE:STANDBY                              93a3 3 06 0    @red @white
SYS_MEAS:TUNE:TUNE_DONE:RF_WAIT                              93a3 3 06 1    @red @white
SYS_MEAS:TUNE:TUNE_DONE:MDSP_WAIT                            93a3 3 06 2    @red @white
SYS_MEAS:TUNE:TUNE_DONE:TUNE                                 93a3 3 06 3    @red @white
SYS_MEAS:TUNE:TUNE_DONE:AGC_WAIT                             93a3 3 06 4    @red @white
SYS_MEAS:TUNE:TUNE_DONE:DETECT                               93a3 3 06 5    @red @white
SYS_MEAS:TUNE:TUNE_DONE:DWELL                                93a3 3 06 6    @red @white
SYS_MEAS:TUNE:TUNE_DONE:DONE                                 93a3 3 06 7    @red @white

SYS_MEAS:TUNE:AGC_TIMER:STANDBY                              93a3 3 07 0    @red @white
SYS_MEAS:TUNE:AGC_TIMER:RF_WAIT                              93a3 3 07 1    @red @white
SYS_MEAS:TUNE:AGC_TIMER:MDSP_WAIT                            93a3 3 07 2    @red @white
SYS_MEAS:TUNE:AGC_TIMER:TUNE                                 93a3 3 07 3    @red @white
SYS_MEAS:TUNE:AGC_TIMER:AGC_WAIT                             93a3 3 07 4    @red @white
SYS_MEAS:TUNE:AGC_TIMER:DETECT                               93a3 3 07 5    @red @white
SYS_MEAS:TUNE:AGC_TIMER:DWELL                                93a3 3 07 6    @red @white
SYS_MEAS:TUNE:AGC_TIMER:DONE                                 93a3 3 07 7    @red @white

SYS_MEAS:TUNE:SRCH_DUMP:STANDBY                              93a3 3 08 0    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:RF_WAIT                              93a3 3 08 1    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:MDSP_WAIT                            93a3 3 08 2    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:TUNE                                 93a3 3 08 3    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:AGC_WAIT                             93a3 3 08 4    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:DETECT                               93a3 3 08 5    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:DWELL                                93a3 3 08 6    @red @white
SYS_MEAS:TUNE:SRCH_DUMP:DONE                                 93a3 3 08 7    @red @white

SYS_MEAS:TUNE:SRCH_LOST_DUMP:STANDBY                         93a3 3 09 0    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:RF_WAIT                         93a3 3 09 1    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:MDSP_WAIT                       93a3 3 09 2    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:TUNE                            93a3 3 09 3    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:AGC_WAIT                        93a3 3 09 4    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:DETECT                          93a3 3 09 5    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:DWELL                           93a3 3 09 6    @red @white
SYS_MEAS:TUNE:SRCH_LOST_DUMP:DONE                            93a3 3 09 7    @red @white

SYS_MEAS:TUNE:MEAS_NEXT:STANDBY                              93a3 3 0a 0    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:RF_WAIT                              93a3 3 0a 1    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:MDSP_WAIT                            93a3 3 0a 2    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:TUNE                                 93a3 3 0a 3    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:AGC_WAIT                             93a3 3 0a 4    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:DETECT                               93a3 3 0a 5    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:DWELL                                93a3 3 0a 6    @red @white
SYS_MEAS:TUNE:MEAS_NEXT:DONE                                 93a3 3 0a 7    @red @white

SYS_MEAS:TUNE:MEAS_DONE:STANDBY                              93a3 3 0b 0    @red @white
SYS_MEAS:TUNE:MEAS_DONE:RF_WAIT                              93a3 3 0b 1    @red @white
SYS_MEAS:TUNE:MEAS_DONE:MDSP_WAIT                            93a3 3 0b 2    @red @white
SYS_MEAS:TUNE:MEAS_DONE:TUNE                                 93a3 3 0b 3    @red @white
SYS_MEAS:TUNE:MEAS_DONE:AGC_WAIT                             93a3 3 0b 4    @red @white
SYS_MEAS:TUNE:MEAS_DONE:DETECT                               93a3 3 0b 5    @red @white
SYS_MEAS:TUNE:MEAS_DONE:DWELL                                93a3 3 0b 6    @red @white
SYS_MEAS:TUNE:MEAS_DONE:DONE                                 93a3 3 0b 7    @red @white

SYS_MEAS:AGC_WAIT:SYS_MEAS:STANDBY                           93a3 4 00 0    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:RF_WAIT                           93a3 4 00 1    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:MDSP_WAIT                         93a3 4 00 2    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:TUNE                              93a3 4 00 3    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:AGC_WAIT                          93a3 4 00 4    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:DETECT                            93a3 4 00 5    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:DWELL                             93a3 4 00 6    @red @white
SYS_MEAS:AGC_WAIT:SYS_MEAS:DONE                              93a3 4 00 7    @red @white

SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:STANDBY                93a3 4 01 0    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:RF_WAIT                93a3 4 01 1    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:MDSP_WAIT              93a3 4 01 2    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:TUNE                   93a3 4 01 3    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:AGC_WAIT               93a3 4 01 4    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:DETECT                 93a3 4 01 5    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:DWELL                  93a3 4 01 6    @red @white
SYS_MEAS:AGC_WAIT:FREQ_TRACK_OFF_DONE:DONE                   93a3 4 01 7    @red @white

SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:STANDBY                     93a3 4 02 0    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:RF_WAIT                     93a3 4 02 1    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:MDSP_WAIT                   93a3 4 02 2    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:TUNE                        93a3 4 02 3    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:AGC_WAIT                    93a3 4 02 4    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:DETECT                      93a3 4 02 5    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:DWELL                       93a3 4 02 6    @red @white
SYS_MEAS:AGC_WAIT:SRCH4_MDSP_REG:DONE                        93a3 4 02 7    @red @white

SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:STANDBY                    93a3 4 03 0    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:RF_WAIT                    93a3 4 03 1    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:MDSP_WAIT                  93a3 4 03 2    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:TUNE                       93a3 4 03 3    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:AGC_WAIT                   93a3 4 03 4    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:DETECT                     93a3 4 03 5    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:DWELL                      93a3 4 03 6    @red @white
SYS_MEAS:AGC_WAIT:RF_RELEASE_DONE:DONE                       93a3 4 03 7    @red @white

SYS_MEAS:AGC_WAIT:RF_LOCKED:STANDBY                          93a3 4 04 0    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:RF_WAIT                          93a3 4 04 1    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:MDSP_WAIT                        93a3 4 04 2    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:TUNE                             93a3 4 04 3    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:AGC_WAIT                         93a3 4 04 4    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:DETECT                           93a3 4 04 5    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:DWELL                            93a3 4 04 6    @red @white
SYS_MEAS:AGC_WAIT:RF_LOCKED:DONE                             93a3 4 04 7    @red @white

SYS_MEAS:AGC_WAIT:ABORT:STANDBY                              93a3 4 05 0    @red @white
SYS_MEAS:AGC_WAIT:ABORT:RF_WAIT                              93a3 4 05 1    @red @white
SYS_MEAS:AGC_WAIT:ABORT:MDSP_WAIT                            93a3 4 05 2    @red @white
SYS_MEAS:AGC_WAIT:ABORT:TUNE                                 93a3 4 05 3    @red @white
SYS_MEAS:AGC_WAIT:ABORT:AGC_WAIT                             93a3 4 05 4    @red @white
SYS_MEAS:AGC_WAIT:ABORT:DETECT                               93a3 4 05 5    @red @white
SYS_MEAS:AGC_WAIT:ABORT:DWELL                                93a3 4 05 6    @red @white
SYS_MEAS:AGC_WAIT:ABORT:DONE                                 93a3 4 05 7    @red @white

SYS_MEAS:AGC_WAIT:TUNE_DONE:STANDBY                          93a3 4 06 0    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:RF_WAIT                          93a3 4 06 1    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:MDSP_WAIT                        93a3 4 06 2    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:TUNE                             93a3 4 06 3    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:AGC_WAIT                         93a3 4 06 4    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:DETECT                           93a3 4 06 5    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:DWELL                            93a3 4 06 6    @red @white
SYS_MEAS:AGC_WAIT:TUNE_DONE:DONE                             93a3 4 06 7    @red @white

SYS_MEAS:AGC_WAIT:AGC_TIMER:STANDBY                          93a3 4 07 0    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:RF_WAIT                          93a3 4 07 1    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:MDSP_WAIT                        93a3 4 07 2    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:TUNE                             93a3 4 07 3    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:AGC_WAIT                         93a3 4 07 4    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:DETECT                           93a3 4 07 5    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:DWELL                            93a3 4 07 6    @red @white
SYS_MEAS:AGC_WAIT:AGC_TIMER:DONE                             93a3 4 07 7    @red @white

SYS_MEAS:AGC_WAIT:SRCH_DUMP:STANDBY                          93a3 4 08 0    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:RF_WAIT                          93a3 4 08 1    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:MDSP_WAIT                        93a3 4 08 2    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:TUNE                             93a3 4 08 3    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:AGC_WAIT                         93a3 4 08 4    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:DETECT                           93a3 4 08 5    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:DWELL                            93a3 4 08 6    @red @white
SYS_MEAS:AGC_WAIT:SRCH_DUMP:DONE                             93a3 4 08 7    @red @white

SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:STANDBY                     93a3 4 09 0    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:RF_WAIT                     93a3 4 09 1    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:MDSP_WAIT                   93a3 4 09 2    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:TUNE                        93a3 4 09 3    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:AGC_WAIT                    93a3 4 09 4    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:DETECT                      93a3 4 09 5    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:DWELL                       93a3 4 09 6    @red @white
SYS_MEAS:AGC_WAIT:SRCH_LOST_DUMP:DONE                        93a3 4 09 7    @red @white

SYS_MEAS:AGC_WAIT:MEAS_NEXT:STANDBY                          93a3 4 0a 0    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:RF_WAIT                          93a3 4 0a 1    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:MDSP_WAIT                        93a3 4 0a 2    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:TUNE                             93a3 4 0a 3    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:AGC_WAIT                         93a3 4 0a 4    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:DETECT                           93a3 4 0a 5    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:DWELL                            93a3 4 0a 6    @red @white
SYS_MEAS:AGC_WAIT:MEAS_NEXT:DONE                             93a3 4 0a 7    @red @white

SYS_MEAS:AGC_WAIT:MEAS_DONE:STANDBY                          93a3 4 0b 0    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:RF_WAIT                          93a3 4 0b 1    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:MDSP_WAIT                        93a3 4 0b 2    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:TUNE                             93a3 4 0b 3    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:AGC_WAIT                         93a3 4 0b 4    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:DETECT                           93a3 4 0b 5    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:DWELL                            93a3 4 0b 6    @red @white
SYS_MEAS:AGC_WAIT:MEAS_DONE:DONE                             93a3 4 0b 7    @red @white

SYS_MEAS:DETECT:SYS_MEAS:STANDBY                             93a3 5 00 0    @red @white
SYS_MEAS:DETECT:SYS_MEAS:RF_WAIT                             93a3 5 00 1    @red @white
SYS_MEAS:DETECT:SYS_MEAS:MDSP_WAIT                           93a3 5 00 2    @red @white
SYS_MEAS:DETECT:SYS_MEAS:TUNE                                93a3 5 00 3    @red @white
SYS_MEAS:DETECT:SYS_MEAS:AGC_WAIT                            93a3 5 00 4    @red @white
SYS_MEAS:DETECT:SYS_MEAS:DETECT                              93a3 5 00 5    @red @white
SYS_MEAS:DETECT:SYS_MEAS:DWELL                               93a3 5 00 6    @red @white
SYS_MEAS:DETECT:SYS_MEAS:DONE                                93a3 5 00 7    @red @white

SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:STANDBY                  93a3 5 01 0    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:RF_WAIT                  93a3 5 01 1    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:MDSP_WAIT                93a3 5 01 2    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:TUNE                     93a3 5 01 3    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:AGC_WAIT                 93a3 5 01 4    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:DETECT                   93a3 5 01 5    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:DWELL                    93a3 5 01 6    @red @white
SYS_MEAS:DETECT:FREQ_TRACK_OFF_DONE:DONE                     93a3 5 01 7    @red @white

SYS_MEAS:DETECT:SRCH4_MDSP_REG:STANDBY                       93a3 5 02 0    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:RF_WAIT                       93a3 5 02 1    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:MDSP_WAIT                     93a3 5 02 2    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:TUNE                          93a3 5 02 3    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:AGC_WAIT                      93a3 5 02 4    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:DETECT                        93a3 5 02 5    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:DWELL                         93a3 5 02 6    @red @white
SYS_MEAS:DETECT:SRCH4_MDSP_REG:DONE                          93a3 5 02 7    @red @white

SYS_MEAS:DETECT:RF_RELEASE_DONE:STANDBY                      93a3 5 03 0    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:RF_WAIT                      93a3 5 03 1    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:MDSP_WAIT                    93a3 5 03 2    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:TUNE                         93a3 5 03 3    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:AGC_WAIT                     93a3 5 03 4    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:DETECT                       93a3 5 03 5    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:DWELL                        93a3 5 03 6    @red @white
SYS_MEAS:DETECT:RF_RELEASE_DONE:DONE                         93a3 5 03 7    @red @white

SYS_MEAS:DETECT:RF_LOCKED:STANDBY                            93a3 5 04 0    @red @white
SYS_MEAS:DETECT:RF_LOCKED:RF_WAIT                            93a3 5 04 1    @red @white
SYS_MEAS:DETECT:RF_LOCKED:MDSP_WAIT                          93a3 5 04 2    @red @white
SYS_MEAS:DETECT:RF_LOCKED:TUNE                               93a3 5 04 3    @red @white
SYS_MEAS:DETECT:RF_LOCKED:AGC_WAIT                           93a3 5 04 4    @red @white
SYS_MEAS:DETECT:RF_LOCKED:DETECT                             93a3 5 04 5    @red @white
SYS_MEAS:DETECT:RF_LOCKED:DWELL                              93a3 5 04 6    @red @white
SYS_MEAS:DETECT:RF_LOCKED:DONE                               93a3 5 04 7    @red @white

SYS_MEAS:DETECT:ABORT:STANDBY                                93a3 5 05 0    @red @white
SYS_MEAS:DETECT:ABORT:RF_WAIT                                93a3 5 05 1    @red @white
SYS_MEAS:DETECT:ABORT:MDSP_WAIT                              93a3 5 05 2    @red @white
SYS_MEAS:DETECT:ABORT:TUNE                                   93a3 5 05 3    @red @white
SYS_MEAS:DETECT:ABORT:AGC_WAIT                               93a3 5 05 4    @red @white
SYS_MEAS:DETECT:ABORT:DETECT                                 93a3 5 05 5    @red @white
SYS_MEAS:DETECT:ABORT:DWELL                                  93a3 5 05 6    @red @white
SYS_MEAS:DETECT:ABORT:DONE                                   93a3 5 05 7    @red @white

SYS_MEAS:DETECT:TUNE_DONE:STANDBY                            93a3 5 06 0    @red @white
SYS_MEAS:DETECT:TUNE_DONE:RF_WAIT                            93a3 5 06 1    @red @white
SYS_MEAS:DETECT:TUNE_DONE:MDSP_WAIT                          93a3 5 06 2    @red @white
SYS_MEAS:DETECT:TUNE_DONE:TUNE                               93a3 5 06 3    @red @white
SYS_MEAS:DETECT:TUNE_DONE:AGC_WAIT                           93a3 5 06 4    @red @white
SYS_MEAS:DETECT:TUNE_DONE:DETECT                             93a3 5 06 5    @red @white
SYS_MEAS:DETECT:TUNE_DONE:DWELL                              93a3 5 06 6    @red @white
SYS_MEAS:DETECT:TUNE_DONE:DONE                               93a3 5 06 7    @red @white

SYS_MEAS:DETECT:AGC_TIMER:STANDBY                            93a3 5 07 0    @red @white
SYS_MEAS:DETECT:AGC_TIMER:RF_WAIT                            93a3 5 07 1    @red @white
SYS_MEAS:DETECT:AGC_TIMER:MDSP_WAIT                          93a3 5 07 2    @red @white
SYS_MEAS:DETECT:AGC_TIMER:TUNE                               93a3 5 07 3    @red @white
SYS_MEAS:DETECT:AGC_TIMER:AGC_WAIT                           93a3 5 07 4    @red @white
SYS_MEAS:DETECT:AGC_TIMER:DETECT                             93a3 5 07 5    @red @white
SYS_MEAS:DETECT:AGC_TIMER:DWELL                              93a3 5 07 6    @red @white
SYS_MEAS:DETECT:AGC_TIMER:DONE                               93a3 5 07 7    @red @white

SYS_MEAS:DETECT:SRCH_DUMP:STANDBY                            93a3 5 08 0    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:RF_WAIT                            93a3 5 08 1    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:MDSP_WAIT                          93a3 5 08 2    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:TUNE                               93a3 5 08 3    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:AGC_WAIT                           93a3 5 08 4    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:DETECT                             93a3 5 08 5    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:DWELL                              93a3 5 08 6    @red @white
SYS_MEAS:DETECT:SRCH_DUMP:DONE                               93a3 5 08 7    @red @white

SYS_MEAS:DETECT:SRCH_LOST_DUMP:STANDBY                       93a3 5 09 0    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:RF_WAIT                       93a3 5 09 1    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:MDSP_WAIT                     93a3 5 09 2    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:TUNE                          93a3 5 09 3    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:AGC_WAIT                      93a3 5 09 4    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:DETECT                        93a3 5 09 5    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:DWELL                         93a3 5 09 6    @red @white
SYS_MEAS:DETECT:SRCH_LOST_DUMP:DONE                          93a3 5 09 7    @red @white

SYS_MEAS:DETECT:MEAS_NEXT:STANDBY                            93a3 5 0a 0    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:RF_WAIT                            93a3 5 0a 1    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:MDSP_WAIT                          93a3 5 0a 2    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:TUNE                               93a3 5 0a 3    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:AGC_WAIT                           93a3 5 0a 4    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:DETECT                             93a3 5 0a 5    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:DWELL                              93a3 5 0a 6    @red @white
SYS_MEAS:DETECT:MEAS_NEXT:DONE                               93a3 5 0a 7    @red @white

SYS_MEAS:DETECT:MEAS_DONE:STANDBY                            93a3 5 0b 0    @red @white
SYS_MEAS:DETECT:MEAS_DONE:RF_WAIT                            93a3 5 0b 1    @red @white
SYS_MEAS:DETECT:MEAS_DONE:MDSP_WAIT                          93a3 5 0b 2    @red @white
SYS_MEAS:DETECT:MEAS_DONE:TUNE                               93a3 5 0b 3    @red @white
SYS_MEAS:DETECT:MEAS_DONE:AGC_WAIT                           93a3 5 0b 4    @red @white
SYS_MEAS:DETECT:MEAS_DONE:DETECT                             93a3 5 0b 5    @red @white
SYS_MEAS:DETECT:MEAS_DONE:DWELL                              93a3 5 0b 6    @red @white
SYS_MEAS:DETECT:MEAS_DONE:DONE                               93a3 5 0b 7    @red @white

SYS_MEAS:DWELL:SYS_MEAS:STANDBY                              93a3 6 00 0    @red @white
SYS_MEAS:DWELL:SYS_MEAS:RF_WAIT                              93a3 6 00 1    @red @white
SYS_MEAS:DWELL:SYS_MEAS:MDSP_WAIT                            93a3 6 00 2    @red @white
SYS_MEAS:DWELL:SYS_MEAS:TUNE                                 93a3 6 00 3    @red @white
SYS_MEAS:DWELL:SYS_MEAS:AGC_WAIT                             93a3 6 00 4    @red @white
SYS_MEAS:DWELL:SYS_MEAS:DETECT                               93a3 6 00 5    @red @white
SYS_MEAS:DWELL:SYS_MEAS:DWELL                                93a3 6 00 6    @red @white
SYS_MEAS:DWELL:SYS_MEAS:DONE                                 93a3 6 00 7    @red @white

SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:STANDBY                   93a3 6 01 0    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:RF_WAIT                   93a3 6 01 1    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:MDSP_WAIT                 93a3 6 01 2    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:TUNE                      93a3 6 01 3    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:AGC_WAIT                  93a3 6 01 4    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:DETECT                    93a3 6 01 5    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:DWELL                     93a3 6 01 6    @red @white
SYS_MEAS:DWELL:FREQ_TRACK_OFF_DONE:DONE                      93a3 6 01 7    @red @white

SYS_MEAS:DWELL:SRCH4_MDSP_REG:STANDBY                        93a3 6 02 0    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:RF_WAIT                        93a3 6 02 1    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:MDSP_WAIT                      93a3 6 02 2    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:TUNE                           93a3 6 02 3    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:AGC_WAIT                       93a3 6 02 4    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:DETECT                         93a3 6 02 5    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:DWELL                          93a3 6 02 6    @red @white
SYS_MEAS:DWELL:SRCH4_MDSP_REG:DONE                           93a3 6 02 7    @red @white

SYS_MEAS:DWELL:RF_RELEASE_DONE:STANDBY                       93a3 6 03 0    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:RF_WAIT                       93a3 6 03 1    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:MDSP_WAIT                     93a3 6 03 2    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:TUNE                          93a3 6 03 3    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:AGC_WAIT                      93a3 6 03 4    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:DETECT                        93a3 6 03 5    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:DWELL                         93a3 6 03 6    @red @white
SYS_MEAS:DWELL:RF_RELEASE_DONE:DONE                          93a3 6 03 7    @red @white

SYS_MEAS:DWELL:RF_LOCKED:STANDBY                             93a3 6 04 0    @red @white
SYS_MEAS:DWELL:RF_LOCKED:RF_WAIT                             93a3 6 04 1    @red @white
SYS_MEAS:DWELL:RF_LOCKED:MDSP_WAIT                           93a3 6 04 2    @red @white
SYS_MEAS:DWELL:RF_LOCKED:TUNE                                93a3 6 04 3    @red @white
SYS_MEAS:DWELL:RF_LOCKED:AGC_WAIT                            93a3 6 04 4    @red @white
SYS_MEAS:DWELL:RF_LOCKED:DETECT                              93a3 6 04 5    @red @white
SYS_MEAS:DWELL:RF_LOCKED:DWELL                               93a3 6 04 6    @red @white
SYS_MEAS:DWELL:RF_LOCKED:DONE                                93a3 6 04 7    @red @white

SYS_MEAS:DWELL:ABORT:STANDBY                                 93a3 6 05 0    @red @white
SYS_MEAS:DWELL:ABORT:RF_WAIT                                 93a3 6 05 1    @red @white
SYS_MEAS:DWELL:ABORT:MDSP_WAIT                               93a3 6 05 2    @red @white
SYS_MEAS:DWELL:ABORT:TUNE                                    93a3 6 05 3    @red @white
SYS_MEAS:DWELL:ABORT:AGC_WAIT                                93a3 6 05 4    @red @white
SYS_MEAS:DWELL:ABORT:DETECT                                  93a3 6 05 5    @red @white
SYS_MEAS:DWELL:ABORT:DWELL                                   93a3 6 05 6    @red @white
SYS_MEAS:DWELL:ABORT:DONE                                    93a3 6 05 7    @red @white

SYS_MEAS:DWELL:TUNE_DONE:STANDBY                             93a3 6 06 0    @red @white
SYS_MEAS:DWELL:TUNE_DONE:RF_WAIT                             93a3 6 06 1    @red @white
SYS_MEAS:DWELL:TUNE_DONE:MDSP_WAIT                           93a3 6 06 2    @red @white
SYS_MEAS:DWELL:TUNE_DONE:TUNE                                93a3 6 06 3    @red @white
SYS_MEAS:DWELL:TUNE_DONE:AGC_WAIT                            93a3 6 06 4    @red @white
SYS_MEAS:DWELL:TUNE_DONE:DETECT                              93a3 6 06 5    @red @white
SYS_MEAS:DWELL:TUNE_DONE:DWELL                               93a3 6 06 6    @red @white
SYS_MEAS:DWELL:TUNE_DONE:DONE                                93a3 6 06 7    @red @white

SYS_MEAS:DWELL:AGC_TIMER:STANDBY                             93a3 6 07 0    @red @white
SYS_MEAS:DWELL:AGC_TIMER:RF_WAIT                             93a3 6 07 1    @red @white
SYS_MEAS:DWELL:AGC_TIMER:MDSP_WAIT                           93a3 6 07 2    @red @white
SYS_MEAS:DWELL:AGC_TIMER:TUNE                                93a3 6 07 3    @red @white
SYS_MEAS:DWELL:AGC_TIMER:AGC_WAIT                            93a3 6 07 4    @red @white
SYS_MEAS:DWELL:AGC_TIMER:DETECT                              93a3 6 07 5    @red @white
SYS_MEAS:DWELL:AGC_TIMER:DWELL                               93a3 6 07 6    @red @white
SYS_MEAS:DWELL:AGC_TIMER:DONE                                93a3 6 07 7    @red @white

SYS_MEAS:DWELL:SRCH_DUMP:STANDBY                             93a3 6 08 0    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:RF_WAIT                             93a3 6 08 1    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:MDSP_WAIT                           93a3 6 08 2    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:TUNE                                93a3 6 08 3    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:AGC_WAIT                            93a3 6 08 4    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:DETECT                              93a3 6 08 5    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:DWELL                               93a3 6 08 6    @red @white
SYS_MEAS:DWELL:SRCH_DUMP:DONE                                93a3 6 08 7    @red @white

SYS_MEAS:DWELL:SRCH_LOST_DUMP:STANDBY                        93a3 6 09 0    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:RF_WAIT                        93a3 6 09 1    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:MDSP_WAIT                      93a3 6 09 2    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:TUNE                           93a3 6 09 3    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:AGC_WAIT                       93a3 6 09 4    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:DETECT                         93a3 6 09 5    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:DWELL                          93a3 6 09 6    @red @white
SYS_MEAS:DWELL:SRCH_LOST_DUMP:DONE                           93a3 6 09 7    @red @white

SYS_MEAS:DWELL:MEAS_NEXT:STANDBY                             93a3 6 0a 0    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:RF_WAIT                             93a3 6 0a 1    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:MDSP_WAIT                           93a3 6 0a 2    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:TUNE                                93a3 6 0a 3    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:AGC_WAIT                            93a3 6 0a 4    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:DETECT                              93a3 6 0a 5    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:DWELL                               93a3 6 0a 6    @red @white
SYS_MEAS:DWELL:MEAS_NEXT:DONE                                93a3 6 0a 7    @red @white

SYS_MEAS:DWELL:MEAS_DONE:STANDBY                             93a3 6 0b 0    @red @white
SYS_MEAS:DWELL:MEAS_DONE:RF_WAIT                             93a3 6 0b 1    @red @white
SYS_MEAS:DWELL:MEAS_DONE:MDSP_WAIT                           93a3 6 0b 2    @red @white
SYS_MEAS:DWELL:MEAS_DONE:TUNE                                93a3 6 0b 3    @red @white
SYS_MEAS:DWELL:MEAS_DONE:AGC_WAIT                            93a3 6 0b 4    @red @white
SYS_MEAS:DWELL:MEAS_DONE:DETECT                              93a3 6 0b 5    @red @white
SYS_MEAS:DWELL:MEAS_DONE:DWELL                               93a3 6 0b 6    @red @white
SYS_MEAS:DWELL:MEAS_DONE:DONE                                93a3 6 0b 7    @red @white

SYS_MEAS:DONE:SYS_MEAS:STANDBY                               93a3 7 00 0    @red @white
SYS_MEAS:DONE:SYS_MEAS:RF_WAIT                               93a3 7 00 1    @red @white
SYS_MEAS:DONE:SYS_MEAS:MDSP_WAIT                             93a3 7 00 2    @red @white
SYS_MEAS:DONE:SYS_MEAS:TUNE                                  93a3 7 00 3    @red @white
SYS_MEAS:DONE:SYS_MEAS:AGC_WAIT                              93a3 7 00 4    @red @white
SYS_MEAS:DONE:SYS_MEAS:DETECT                                93a3 7 00 5    @red @white
SYS_MEAS:DONE:SYS_MEAS:DWELL                                 93a3 7 00 6    @red @white
SYS_MEAS:DONE:SYS_MEAS:DONE                                  93a3 7 00 7    @red @white

SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:STANDBY                    93a3 7 01 0    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:RF_WAIT                    93a3 7 01 1    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:MDSP_WAIT                  93a3 7 01 2    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:TUNE                       93a3 7 01 3    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:AGC_WAIT                   93a3 7 01 4    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:DETECT                     93a3 7 01 5    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:DWELL                      93a3 7 01 6    @red @white
SYS_MEAS:DONE:FREQ_TRACK_OFF_DONE:DONE                       93a3 7 01 7    @red @white

SYS_MEAS:DONE:SRCH4_MDSP_REG:STANDBY                         93a3 7 02 0    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:RF_WAIT                         93a3 7 02 1    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:MDSP_WAIT                       93a3 7 02 2    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:TUNE                            93a3 7 02 3    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:AGC_WAIT                        93a3 7 02 4    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:DETECT                          93a3 7 02 5    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:DWELL                           93a3 7 02 6    @red @white
SYS_MEAS:DONE:SRCH4_MDSP_REG:DONE                            93a3 7 02 7    @red @white

SYS_MEAS:DONE:RF_RELEASE_DONE:STANDBY                        93a3 7 03 0    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:RF_WAIT                        93a3 7 03 1    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:MDSP_WAIT                      93a3 7 03 2    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:TUNE                           93a3 7 03 3    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:AGC_WAIT                       93a3 7 03 4    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:DETECT                         93a3 7 03 5    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:DWELL                          93a3 7 03 6    @red @white
SYS_MEAS:DONE:RF_RELEASE_DONE:DONE                           93a3 7 03 7    @red @white

SYS_MEAS:DONE:RF_LOCKED:STANDBY                              93a3 7 04 0    @red @white
SYS_MEAS:DONE:RF_LOCKED:RF_WAIT                              93a3 7 04 1    @red @white
SYS_MEAS:DONE:RF_LOCKED:MDSP_WAIT                            93a3 7 04 2    @red @white
SYS_MEAS:DONE:RF_LOCKED:TUNE                                 93a3 7 04 3    @red @white
SYS_MEAS:DONE:RF_LOCKED:AGC_WAIT                             93a3 7 04 4    @red @white
SYS_MEAS:DONE:RF_LOCKED:DETECT                               93a3 7 04 5    @red @white
SYS_MEAS:DONE:RF_LOCKED:DWELL                                93a3 7 04 6    @red @white
SYS_MEAS:DONE:RF_LOCKED:DONE                                 93a3 7 04 7    @red @white

SYS_MEAS:DONE:ABORT:STANDBY                                  93a3 7 05 0    @red @white
SYS_MEAS:DONE:ABORT:RF_WAIT                                  93a3 7 05 1    @red @white
SYS_MEAS:DONE:ABORT:MDSP_WAIT                                93a3 7 05 2    @red @white
SYS_MEAS:DONE:ABORT:TUNE                                     93a3 7 05 3    @red @white
SYS_MEAS:DONE:ABORT:AGC_WAIT                                 93a3 7 05 4    @red @white
SYS_MEAS:DONE:ABORT:DETECT                                   93a3 7 05 5    @red @white
SYS_MEAS:DONE:ABORT:DWELL                                    93a3 7 05 6    @red @white
SYS_MEAS:DONE:ABORT:DONE                                     93a3 7 05 7    @red @white

SYS_MEAS:DONE:TUNE_DONE:STANDBY                              93a3 7 06 0    @red @white
SYS_MEAS:DONE:TUNE_DONE:RF_WAIT                              93a3 7 06 1    @red @white
SYS_MEAS:DONE:TUNE_DONE:MDSP_WAIT                            93a3 7 06 2    @red @white
SYS_MEAS:DONE:TUNE_DONE:TUNE                                 93a3 7 06 3    @red @white
SYS_MEAS:DONE:TUNE_DONE:AGC_WAIT                             93a3 7 06 4    @red @white
SYS_MEAS:DONE:TUNE_DONE:DETECT                               93a3 7 06 5    @red @white
SYS_MEAS:DONE:TUNE_DONE:DWELL                                93a3 7 06 6    @red @white
SYS_MEAS:DONE:TUNE_DONE:DONE                                 93a3 7 06 7    @red @white

SYS_MEAS:DONE:AGC_TIMER:STANDBY                              93a3 7 07 0    @red @white
SYS_MEAS:DONE:AGC_TIMER:RF_WAIT                              93a3 7 07 1    @red @white
SYS_MEAS:DONE:AGC_TIMER:MDSP_WAIT                            93a3 7 07 2    @red @white
SYS_MEAS:DONE:AGC_TIMER:TUNE                                 93a3 7 07 3    @red @white
SYS_MEAS:DONE:AGC_TIMER:AGC_WAIT                             93a3 7 07 4    @red @white
SYS_MEAS:DONE:AGC_TIMER:DETECT                               93a3 7 07 5    @red @white
SYS_MEAS:DONE:AGC_TIMER:DWELL                                93a3 7 07 6    @red @white
SYS_MEAS:DONE:AGC_TIMER:DONE                                 93a3 7 07 7    @red @white

SYS_MEAS:DONE:SRCH_DUMP:STANDBY                              93a3 7 08 0    @red @white
SYS_MEAS:DONE:SRCH_DUMP:RF_WAIT                              93a3 7 08 1    @red @white
SYS_MEAS:DONE:SRCH_DUMP:MDSP_WAIT                            93a3 7 08 2    @red @white
SYS_MEAS:DONE:SRCH_DUMP:TUNE                                 93a3 7 08 3    @red @white
SYS_MEAS:DONE:SRCH_DUMP:AGC_WAIT                             93a3 7 08 4    @red @white
SYS_MEAS:DONE:SRCH_DUMP:DETECT                               93a3 7 08 5    @red @white
SYS_MEAS:DONE:SRCH_DUMP:DWELL                                93a3 7 08 6    @red @white
SYS_MEAS:DONE:SRCH_DUMP:DONE                                 93a3 7 08 7    @red @white

SYS_MEAS:DONE:SRCH_LOST_DUMP:STANDBY                         93a3 7 09 0    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:RF_WAIT                         93a3 7 09 1    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:MDSP_WAIT                       93a3 7 09 2    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:TUNE                            93a3 7 09 3    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:AGC_WAIT                        93a3 7 09 4    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:DETECT                          93a3 7 09 5    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:DWELL                           93a3 7 09 6    @red @white
SYS_MEAS:DONE:SRCH_LOST_DUMP:DONE                            93a3 7 09 7    @red @white

SYS_MEAS:DONE:MEAS_NEXT:STANDBY                              93a3 7 0a 0    @red @white
SYS_MEAS:DONE:MEAS_NEXT:RF_WAIT                              93a3 7 0a 1    @red @white
SYS_MEAS:DONE:MEAS_NEXT:MDSP_WAIT                            93a3 7 0a 2    @red @white
SYS_MEAS:DONE:MEAS_NEXT:TUNE                                 93a3 7 0a 3    @red @white
SYS_MEAS:DONE:MEAS_NEXT:AGC_WAIT                             93a3 7 0a 4    @red @white
SYS_MEAS:DONE:MEAS_NEXT:DETECT                               93a3 7 0a 5    @red @white
SYS_MEAS:DONE:MEAS_NEXT:DWELL                                93a3 7 0a 6    @red @white
SYS_MEAS:DONE:MEAS_NEXT:DONE                                 93a3 7 0a 7    @red @white

SYS_MEAS:DONE:MEAS_DONE:STANDBY                              93a3 7 0b 0    @red @white
SYS_MEAS:DONE:MEAS_DONE:RF_WAIT                              93a3 7 0b 1    @red @white
SYS_MEAS:DONE:MEAS_DONE:MDSP_WAIT                            93a3 7 0b 2    @red @white
SYS_MEAS:DONE:MEAS_DONE:TUNE                                 93a3 7 0b 3    @red @white
SYS_MEAS:DONE:MEAS_DONE:AGC_WAIT                             93a3 7 0b 4    @red @white
SYS_MEAS:DONE:MEAS_DONE:DETECT                               93a3 7 0b 5    @red @white
SYS_MEAS:DONE:MEAS_DONE:DWELL                                93a3 7 0b 6    @red @white
SYS_MEAS:DONE:MEAS_DONE:DONE                                 93a3 7 0b 7    @red @white



# End machine generated TLA code for state machine: SYS_MEAS_SM

