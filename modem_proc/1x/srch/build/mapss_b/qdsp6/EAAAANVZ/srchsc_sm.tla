###############################################################################
#
#    srchsc_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchsc_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SYNC_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SYNC:SYNC:SRCH_CDMA_F:SYNC                                   00ef 0 00 0    @olive @white

SYNC:SYNC:SRCH_SLEW_F:SYNC                                   00ef 0 01 0    @olive @white

SYNC:SYNC:SRCH_ACQ_F:SYNC                                    00ef 0 02 0    @olive @white

SYNC:SYNC:SRCH_SC_F:SYNC                                     00ef 0 03 0    @olive @white

SYNC:SYNC:RSSI_TIMER:SYNC                                    00ef 0 04 0    @olive @white

SYNC:SYNC:SRCH_DUMP:SYNC                                     00ef 0 05 0    @olive @white

SYNC:SYNC:SRCH_LOST_DUMP:SYNC                                00ef 0 06 0    @olive @white

SYNC:SYNC:SRCH_TL_TIMER:SYNC                                 00ef 0 07 0    @olive @white



# End machine generated TLA code for state machine: SYNC_SM

