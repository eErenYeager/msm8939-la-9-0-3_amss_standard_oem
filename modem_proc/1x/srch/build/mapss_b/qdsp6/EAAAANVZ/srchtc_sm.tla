###############################################################################
#
#    srchtc_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchtc_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: TC_SM
# State machine:current state:input:next state                      fgcolor bgcolor

TC:ONLINE:SRCH_CDMA_F:ONLINE                                 b396 0 00 0    @purple @white

TC:ONLINE:SRCH_IDLE_F:ONLINE                                 b396 0 01 0    @purple @white

TC:ONLINE:IDLE_ENTRY_DIV_DISABLED:ONLINE                     b396 0 02 0    @purple @white

TC:ONLINE:IDLE_ENTRY_COMPLETE:ONLINE                         b396 0 03 0    @purple @white

TC:ONLINE:SRCH_PARM_F:ONLINE                                 b396 0 04 0    @purple @white

TC:ONLINE:SRCH_MEAS_F:ONLINE                                 b396 0 05 0    @purple @white

TC:ONLINE:SRCH_SCRM_MEAS_F:ONLINE                            b396 0 06 0    @purple @white

TC:ONLINE:SRCH_FPC_MEAS_F:ONLINE                             b396 0 07 0    @purple @white

TC:ONLINE:SRCH_TC_ADD_SCH_F:ONLINE                           b396 0 08 0    @purple @white

TC:ONLINE:SRCH_TC_NSET_F:ONLINE                              b396 0 09 0    @purple @white

TC:ONLINE:SRCH_PPSM_F:ONLINE                                 b396 0 0a 0    @purple @white

TC:ONLINE:SRCH_START_PPSM_F:ONLINE                           b396 0 0b 0    @purple @white

TC:ONLINE:SRCH_STOP_PPSM_F:ONLINE                            b396 0 0c 0    @purple @white

TC:ONLINE:SRCH_START_CFS_F:ONLINE                            b396 0 0d 0    @purple @white

TC:ONLINE:SRCH_STOP_CFS_F:ONLINE                             b396 0 0e 0    @purple @white

TC:ONLINE:SRCH_RIF_HHO_FAILURE_F:ONLINE                      b396 0 0f 0    @purple @white

TC:ONLINE:SRCH_RIF_HHO_SUCCESS_F:ONLINE                      b396 0 10 0    @purple @white

TC:ONLINE:SRCH_TC_FIND_STRONG_PILOTS_F:ONLINE                b396 0 11 0    @purple @white

TC:ONLINE:SRCH_MC_GPS_VISIT_OKAY_F:ONLINE                    b396 0 12 0    @purple @white

TC:ONLINE:SRCH_MC_RESUME_CDMA_F:ONLINE                       b396 0 13 0    @purple @white

TC:ONLINE:SRCH_RAND_BITS_F:ONLINE                            b396 0 14 0    @purple @white

TC:ONLINE:SRCH_DUMP:ONLINE                                   b396 0 15 0    @purple @white

TC:ONLINE:SRCH_LOST_DUMP:ONLINE                              b396 0 16 0    @purple @white

TC:ONLINE:ASET_UPDATE_PRE_TUNE:ONLINE                        b396 0 17 0    @purple @white

TC:ONLINE:ASET_UPDATE_POST_TUNE:ONLINE                       b396 0 18 0    @purple @white

TC:ONLINE:CFS_ACTION:ONLINE                                  b396 0 19 0    @purple @white

TC:ONLINE:ENTRY_TUNE:ONLINE                                  b396 0 1a 0    @purple @white

TC:ONLINE:ACTION_TIME:ONLINE                                 b396 0 1b 0    @purple @white

TC:ONLINE:DO_SRCH:ONLINE                                     b396 0 1c 0    @purple @white

TC:ONLINE:GOVERN_TIMER:ONLINE                                b396 0 1d 0    @purple @white

TC:ONLINE:AFC_RPT_TIMER:ONLINE                               b396 0 1e 0    @purple @white

TC:ONLINE:REF_TIMER:ONLINE                                   b396 0 1f 0    @purple @white

TC:ONLINE:CTR_TIMER:ONLINE                                   b396 0 20 0    @purple @white

TC:ONLINE:SYNTH_FRAME_STROBE:ONLINE                          b396 0 21 0    @purple @white

TC:ONLINE:RSSI_TIMER:ONLINE                                  b396 0 22 0    @purple @white

TC:ONLINE:PPSMM_TIMER:ONLINE                                 b396 0 23 0    @purple @white

TC:ONLINE:RF_REPORT_TIMER:ONLINE                             b396 0 24 0    @purple @white

TC:ONLINE:NS_TRIAGE_TIMER:ONLINE                             b396 0 25 0    @purple @white

TC:ONLINE:NS_CTR_TIMER:ONLINE                                b396 0 26 0    @purple @white

TC:ONLINE:TUNE_DONE:ONLINE                                   b396 0 27 0    @purple @white



# End machine generated TLA code for state machine: TC_SM
# Begin machine generated TLA code for state machine: TCG_SM
# State machine:current state:input:next state                      fgcolor bgcolor

TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_INACTIVE              eea2 0 00 0    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_QUERY_MC              eea2 0 00 1    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_ACTIVE                eea2 0 00 2    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_REQUEST:TCG_WAIT_RETRY            eea2 0 00 3    @purple @white

TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_INACTIVE                eea2 0 01 0    @purple @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_QUERY_MC                eea2 0 01 1    @purple @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_ACTIVE                  eea2 0 01 2    @purple @white
TCG:TCG_INACTIVE:TCG_MC_RESPONSE:TCG_WAIT_RETRY              eea2 0 01 3    @purple @white

TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE       eea2 0 02 0    @purple @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC       eea2 0 02 1    @purple @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE         eea2 0 02 2    @purple @white
TCG:TCG_INACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY     eea2 0 02 3    @purple @white

TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE        eea2 0 03 0    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC        eea2 0 03 1    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE          eea2 0 03 2    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY      eea2 0 03 3    @purple @white

TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE       eea2 0 04 0    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC       eea2 0 04 1    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE         eea2 0 04 2    @purple @white
TCG:TCG_INACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY     eea2 0 04 3    @purple @white

TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_INACTIVE                eea2 0 05 0    @purple @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_QUERY_MC                eea2 0 05 1    @purple @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_ACTIVE                  eea2 0 05 2    @purple @white
TCG:TCG_INACTIVE:TCG_RETRY_TIMER:TCG_WAIT_RETRY              eea2 0 05 3    @purple @white

TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_INACTIVE              eea2 1 00 0    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_QUERY_MC              eea2 1 00 1    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_ACTIVE                eea2 1 00 2    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_REQUEST:TCG_WAIT_RETRY            eea2 1 00 3    @purple @white

TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_INACTIVE                eea2 1 01 0    @purple @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_QUERY_MC                eea2 1 01 1    @purple @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_ACTIVE                  eea2 1 01 2    @purple @white
TCG:TCG_QUERY_MC:TCG_MC_RESPONSE:TCG_WAIT_RETRY              eea2 1 01 3    @purple @white

TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE       eea2 1 02 0    @purple @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC       eea2 1 02 1    @purple @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE         eea2 1 02 2    @purple @white
TCG:TCG_QUERY_MC:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY     eea2 1 02 3    @purple @white

TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE        eea2 1 03 0    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC        eea2 1 03 1    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE          eea2 1 03 2    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY      eea2 1 03 3    @purple @white

TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE       eea2 1 04 0    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC       eea2 1 04 1    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE         eea2 1 04 2    @purple @white
TCG:TCG_QUERY_MC:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY     eea2 1 04 3    @purple @white

TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_INACTIVE                eea2 1 05 0    @purple @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_QUERY_MC                eea2 1 05 1    @purple @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_ACTIVE                  eea2 1 05 2    @purple @white
TCG:TCG_QUERY_MC:TCG_RETRY_TIMER:TCG_WAIT_RETRY              eea2 1 05 3    @purple @white

TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_INACTIVE                eea2 2 00 0    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_QUERY_MC                eea2 2 00 1    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_ACTIVE                  eea2 2 00 2    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_REQUEST:TCG_WAIT_RETRY              eea2 2 00 3    @purple @white

TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_INACTIVE                  eea2 2 01 0    @purple @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_QUERY_MC                  eea2 2 01 1    @purple @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_ACTIVE                    eea2 2 01 2    @purple @white
TCG:TCG_ACTIVE:TCG_MC_RESPONSE:TCG_WAIT_RETRY                eea2 2 01 3    @purple @white

TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE         eea2 2 02 0    @purple @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC         eea2 2 02 1    @purple @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE           eea2 2 02 2    @purple @white
TCG:TCG_ACTIVE:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY       eea2 2 02 3    @purple @white

TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE          eea2 2 03 0    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC          eea2 2 03 1    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE            eea2 2 03 2    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY        eea2 2 03 3    @purple @white

TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE         eea2 2 04 0    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC         eea2 2 04 1    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE           eea2 2 04 2    @purple @white
TCG:TCG_ACTIVE:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY       eea2 2 04 3    @purple @white

TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_INACTIVE                  eea2 2 05 0    @purple @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_QUERY_MC                  eea2 2 05 1    @purple @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_ACTIVE                    eea2 2 05 2    @purple @white
TCG:TCG_ACTIVE:TCG_RETRY_TIMER:TCG_WAIT_RETRY                eea2 2 05 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_INACTIVE            eea2 3 00 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_QUERY_MC            eea2 3 00 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_ACTIVE              eea2 3 00 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_REQUEST:TCG_WAIT_RETRY          eea2 3 00 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_INACTIVE              eea2 3 01 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_QUERY_MC              eea2 3 01 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_ACTIVE                eea2 3 01 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_MC_RESPONSE:TCG_WAIT_RETRY            eea2 3 01 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_INACTIVE     eea2 3 02 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_QUERY_MC     eea2 3 02 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_ACTIVE       eea2 3 02 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_CANCEL_VISIT_REQUEST:TCG_WAIT_RETRY   eea2 3 02 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_INACTIVE      eea2 3 03 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_QUERY_MC      eea2 3 03 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_ACTIVE        eea2 3 03 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_PRE_TUNE:TCG_WAIT_RETRY    eea2 3 03 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_INACTIVE     eea2 3 04 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_QUERY_MC     eea2 3 04 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_ACTIVE       eea2 3 04 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_VISIT_DONE_POST_TUNE:TCG_WAIT_RETRY   eea2 3 04 3    @purple @white

TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_INACTIVE              eea2 3 05 0    @purple @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_QUERY_MC              eea2 3 05 1    @purple @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_ACTIVE                eea2 3 05 2    @purple @white
TCG:TCG_WAIT_RETRY:TCG_RETRY_TIMER:TCG_WAIT_RETRY            eea2 3 05 3    @purple @white



# End machine generated TLA code for state machine: TCG_SM

