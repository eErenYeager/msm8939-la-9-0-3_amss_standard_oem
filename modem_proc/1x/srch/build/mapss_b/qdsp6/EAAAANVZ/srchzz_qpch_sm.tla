###############################################################################
#
#    srchzz_qpch_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_qpch_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_QPCH_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_QPCH:INIT:START_SLEEP:INIT                            3ae7 0 00 0    @magenta @white
SRCHZZ_QPCH:INIT:START_SLEEP:PI1                             3ae7 0 00 1    @magenta @white
SRCHZZ_QPCH:INIT:START_SLEEP:PI2                             3ae7 0 00 2    @magenta @white
SRCHZZ_QPCH:INIT:START_SLEEP:CCI1                            3ae7 0 00 3    @magenta @white

SRCHZZ_QPCH:INIT:QPCH_CONTINUE:INIT                          3ae7 0 01 0    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_CONTINUE:PI1                           3ae7 0 01 1    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_CONTINUE:PI2                           3ae7 0 01 2    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_CONTINUE:CCI1                          3ae7 0 01 3    @magenta @white

SRCHZZ_QPCH:INIT:WAKEUP_NOW:INIT                             3ae7 0 02 0    @magenta @white
SRCHZZ_QPCH:INIT:WAKEUP_NOW:PI1                              3ae7 0 02 1    @magenta @white
SRCHZZ_QPCH:INIT:WAKEUP_NOW:PI2                              3ae7 0 02 2    @magenta @white
SRCHZZ_QPCH:INIT:WAKEUP_NOW:CCI1                             3ae7 0 02 3    @magenta @white

SRCHZZ_QPCH:INIT:QPCH_DEMOD_DONE:INIT                        3ae7 0 03 0    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_DEMOD_DONE:PI1                         3ae7 0 03 1    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_DEMOD_DONE:PI2                         3ae7 0 03 2    @magenta @white
SRCHZZ_QPCH:INIT:QPCH_DEMOD_DONE:CCI1                        3ae7 0 03 3    @magenta @white

SRCHZZ_QPCH:PI1:START_SLEEP:INIT                             3ae7 1 00 0    @magenta @white
SRCHZZ_QPCH:PI1:START_SLEEP:PI1                              3ae7 1 00 1    @magenta @white
SRCHZZ_QPCH:PI1:START_SLEEP:PI2                              3ae7 1 00 2    @magenta @white
SRCHZZ_QPCH:PI1:START_SLEEP:CCI1                             3ae7 1 00 3    @magenta @white

SRCHZZ_QPCH:PI1:QPCH_CONTINUE:INIT                           3ae7 1 01 0    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_CONTINUE:PI1                            3ae7 1 01 1    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_CONTINUE:PI2                            3ae7 1 01 2    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_CONTINUE:CCI1                           3ae7 1 01 3    @magenta @white

SRCHZZ_QPCH:PI1:WAKEUP_NOW:INIT                              3ae7 1 02 0    @magenta @white
SRCHZZ_QPCH:PI1:WAKEUP_NOW:PI1                               3ae7 1 02 1    @magenta @white
SRCHZZ_QPCH:PI1:WAKEUP_NOW:PI2                               3ae7 1 02 2    @magenta @white
SRCHZZ_QPCH:PI1:WAKEUP_NOW:CCI1                              3ae7 1 02 3    @magenta @white

SRCHZZ_QPCH:PI1:QPCH_DEMOD_DONE:INIT                         3ae7 1 03 0    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_DEMOD_DONE:PI1                          3ae7 1 03 1    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_DEMOD_DONE:PI2                          3ae7 1 03 2    @magenta @white
SRCHZZ_QPCH:PI1:QPCH_DEMOD_DONE:CCI1                         3ae7 1 03 3    @magenta @white

SRCHZZ_QPCH:PI2:START_SLEEP:INIT                             3ae7 2 00 0    @magenta @white
SRCHZZ_QPCH:PI2:START_SLEEP:PI1                              3ae7 2 00 1    @magenta @white
SRCHZZ_QPCH:PI2:START_SLEEP:PI2                              3ae7 2 00 2    @magenta @white
SRCHZZ_QPCH:PI2:START_SLEEP:CCI1                             3ae7 2 00 3    @magenta @white

SRCHZZ_QPCH:PI2:QPCH_CONTINUE:INIT                           3ae7 2 01 0    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_CONTINUE:PI1                            3ae7 2 01 1    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_CONTINUE:PI2                            3ae7 2 01 2    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_CONTINUE:CCI1                           3ae7 2 01 3    @magenta @white

SRCHZZ_QPCH:PI2:WAKEUP_NOW:INIT                              3ae7 2 02 0    @magenta @white
SRCHZZ_QPCH:PI2:WAKEUP_NOW:PI1                               3ae7 2 02 1    @magenta @white
SRCHZZ_QPCH:PI2:WAKEUP_NOW:PI2                               3ae7 2 02 2    @magenta @white
SRCHZZ_QPCH:PI2:WAKEUP_NOW:CCI1                              3ae7 2 02 3    @magenta @white

SRCHZZ_QPCH:PI2:QPCH_DEMOD_DONE:INIT                         3ae7 2 03 0    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_DEMOD_DONE:PI1                          3ae7 2 03 1    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_DEMOD_DONE:PI2                          3ae7 2 03 2    @magenta @white
SRCHZZ_QPCH:PI2:QPCH_DEMOD_DONE:CCI1                         3ae7 2 03 3    @magenta @white

SRCHZZ_QPCH:CCI1:START_SLEEP:INIT                            3ae7 3 00 0    @magenta @white
SRCHZZ_QPCH:CCI1:START_SLEEP:PI1                             3ae7 3 00 1    @magenta @white
SRCHZZ_QPCH:CCI1:START_SLEEP:PI2                             3ae7 3 00 2    @magenta @white
SRCHZZ_QPCH:CCI1:START_SLEEP:CCI1                            3ae7 3 00 3    @magenta @white

SRCHZZ_QPCH:CCI1:QPCH_CONTINUE:INIT                          3ae7 3 01 0    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_CONTINUE:PI1                           3ae7 3 01 1    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_CONTINUE:PI2                           3ae7 3 01 2    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_CONTINUE:CCI1                          3ae7 3 01 3    @magenta @white

SRCHZZ_QPCH:CCI1:WAKEUP_NOW:INIT                             3ae7 3 02 0    @magenta @white
SRCHZZ_QPCH:CCI1:WAKEUP_NOW:PI1                              3ae7 3 02 1    @magenta @white
SRCHZZ_QPCH:CCI1:WAKEUP_NOW:PI2                              3ae7 3 02 2    @magenta @white
SRCHZZ_QPCH:CCI1:WAKEUP_NOW:CCI1                             3ae7 3 02 3    @magenta @white

SRCHZZ_QPCH:CCI1:QPCH_DEMOD_DONE:INIT                        3ae7 3 03 0    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_DEMOD_DONE:PI1                         3ae7 3 03 1    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_DEMOD_DONE:PI2                         3ae7 3 03 2    @magenta @white
SRCHZZ_QPCH:CCI1:QPCH_DEMOD_DONE:CCI1                        3ae7 3 03 3    @magenta @white



# End machine generated TLA code for state machine: SRCHZZ_QPCH_SM

