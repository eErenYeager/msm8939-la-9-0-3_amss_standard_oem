###############################################################################
#
#    srchcd_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchcd_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: CDMA_SM
# State machine:current state:input:next state                      fgcolor bgcolor

CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:WAIT_FREQ_DONE               efd8 0 00 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE        efd8 0 00 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE          efd8 0 00 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:MAIN                         efd8 0 00 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:MDSP_SRCH4_APP_REG           efd8 0 00 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:WAIT_GOVERN                  efd8 0 00 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_CDMA_F:TRANS                        efd8 0 00 6    @purple @white

CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:WAIT_FREQ_DONE            efd8 0 01 0    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE     efd8 0 01 1    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE       efd8 0 01 2    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:MAIN                      efd8 0 01 3    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG        efd8 0 01 4    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:WAIT_GOVERN               efd8 0 01 5    @purple @white
CDMA:WAIT_FREQ_DONE:SKIP_FREQ_DONE:TRANS                     efd8 0 01 6    @purple @white

CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE       efd8 0 02 0    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE efd8 0 02 1    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE  efd8 0 02 2    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:MAIN                 efd8 0 02 3    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG   efd8 0 02 4    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:WAIT_GOVERN          efd8 0 02 5    @purple @white
CDMA:WAIT_FREQ_DONE:FREQ_TRACK_OFF_DONE:TRANS                efd8 0 02 6    @purple @white

CDMA:WAIT_FREQ_DONE:SRCH_START:WAIT_FREQ_DONE                efd8 0 03 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:WAIT_FOR_AFLT_RELEASE         efd8 0 03 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:WAIT_FOR_RF_RELEASE           efd8 0 03 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:MAIN                          efd8 0 03 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:MDSP_SRCH4_APP_REG            efd8 0 03 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:WAIT_GOVERN                   efd8 0 03 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_START:TRANS                         efd8 0 03 6    @purple @white

CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:WAIT_FREQ_DONE         efd8 0 04 0    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE  efd8 0 04 1    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE    efd8 0 04 2    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:MAIN                   efd8 0 04 3    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG     efd8 0 04 4    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:WAIT_GOVERN            efd8 0 04 5    @purple @white
CDMA:WAIT_FREQ_DONE:AFLT_RELEASE_DONE:TRANS                  efd8 0 04 6    @purple @white

CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:WAIT_FREQ_DONE           efd8 0 05 0    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE    efd8 0 05 1    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE      efd8 0 05 2    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:MAIN                     efd8 0 05 3    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG       efd8 0 05 4    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:WAIT_GOVERN              efd8 0 05 5    @purple @white
CDMA:WAIT_FREQ_DONE:RF_RELEASE_DONE:TRANS                    efd8 0 05 6    @purple @white

CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE         efd8 0 06 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE  efd8 0 06 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE    efd8 0 06 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:MAIN                   efd8 0 06 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG     efd8 0 06 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:WAIT_GOVERN            efd8 0 06 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_DEACTIVATE_F:TRANS                  efd8 0 06 6    @purple @white

CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:WAIT_FREQ_DONE                efd8 0 07 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE         efd8 0 07 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE           efd8 0 07 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:MAIN                          efd8 0 07 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:MDSP_SRCH4_APP_REG            efd8 0 07 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:WAIT_GOVERN                   efd8 0 07 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_ACQ_F:TRANS                         efd8 0 07 6    @purple @white

CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE        efd8 0 08 0    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE efd8 0 08 1    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE   efd8 0 08 2    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:MAIN                  efd8 0 08 3    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG    efd8 0 08 4    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:WAIT_GOVERN           efd8 0 08 5    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_LOCK_GRANTED:TRANS                 efd8 0 08 6    @purple @white

CDMA:WAIT_FREQ_DONE:MDSP_START:WAIT_FREQ_DONE                efd8 0 09 0    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:WAIT_FOR_AFLT_RELEASE         efd8 0 09 1    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:WAIT_FOR_RF_RELEASE           efd8 0 09 2    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:MAIN                          efd8 0 09 3    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:MDSP_SRCH4_APP_REG            efd8 0 09 4    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:WAIT_GOVERN                   efd8 0 09 5    @purple @white
CDMA:WAIT_FREQ_DONE:MDSP_START:TRANS                         efd8 0 09 6    @purple @white

CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE         efd8 0 0a 0    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE  efd8 0 0a 1    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE    efd8 0 0a 2    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:MAIN                   efd8 0 0a 3    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG     efd8 0 0a 4    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:WAIT_GOVERN            efd8 0 0a 5    @purple @white
CDMA:WAIT_FREQ_DONE:RF_REQ_FAIL_TIMER:TRANS                  efd8 0 0a 6    @purple @white

CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:WAIT_FREQ_DONE            efd8 0 0b 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE     efd8 0 0b 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE       efd8 0 0b 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:MAIN                      efd8 0 0b 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG        efd8 0 0b 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:WAIT_GOVERN               efd8 0 0b 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH4_MDSP_REG:TRANS                     efd8 0 0b 6    @purple @white

CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE   efd8 0 0c 0    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE efd8 0 0c 1    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE efd8 0 0c 2    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:MAIN             efd8 0 0c 3    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG efd8 0 0c 4    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN      efd8 0 0c 5    @purple @white
CDMA:WAIT_FREQ_DONE:SYS_MEAS_ABORT_COMPLETE:TRANS            efd8 0 0c 6    @purple @white

CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:WAIT_FREQ_DONE             efd8 0 0d 0    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE      efd8 0 0d 1    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE        efd8 0 0d 2    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:MAIN                       efd8 0 0d 3    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:MDSP_SRCH4_APP_REG         efd8 0 0d 4    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:WAIT_GOVERN                efd8 0 0d 5    @purple @white
CDMA:WAIT_FREQ_DONE:RX_MOD_DENIED:TRANS                      efd8 0 0d 6    @purple @white

CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE          efd8 0 0e 0    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE   efd8 0 0e 1    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE     efd8 0 0e 2    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:MAIN                    efd8 0 0e 3    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG      efd8 0 0e 4    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:WAIT_GOVERN             efd8 0 0e 5    @purple @white
CDMA:WAIT_FREQ_DONE:SRCH_LTE_1X_TT_F:TRANS                   efd8 0 0e 6    @purple @white

CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:WAIT_FREQ_DONE              efd8 0 0f 0    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE       efd8 0 0f 1    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:WAIT_FOR_RF_RELEASE         efd8 0 0f 2    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:MAIN                        efd8 0 0f 3    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:MDSP_SRCH4_APP_REG          efd8 0 0f 4    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:WAIT_GOVERN                 efd8 0 0f 5    @purple @white
CDMA:WAIT_FREQ_DONE:GOVERN_TIMER:TRANS                       efd8 0 0f 6    @purple @white

CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE        efd8 0 10 0    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE efd8 0 10 1    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE   efd8 0 10 2    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:MAIN                  efd8 0 10 3    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG    efd8 0 10 4    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:WAIT_GOVERN           efd8 0 10 5    @purple @white
CDMA:WAIT_FREQ_DONE:CD_RF_DIV_DISABLED:TRANS                 efd8 0 10 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:WAIT_FREQ_DONE        efd8 1 00 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE efd8 1 00 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE   efd8 1 00 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:MAIN                  efd8 1 00 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:MDSP_SRCH4_APP_REG    efd8 1 00 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:WAIT_GOVERN           efd8 1 00 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_CDMA_F:TRANS                 efd8 1 00 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:WAIT_FREQ_DONE     efd8 1 01 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE efd8 1 01 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE efd8 1 01 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:MAIN               efd8 1 01 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG efd8 1 01 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:WAIT_GOVERN        efd8 1 01 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SKIP_FREQ_DONE:TRANS              efd8 1 01 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE efd8 1 02 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE efd8 1 02 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE efd8 1 02 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:MAIN          efd8 1 02 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG efd8 1 02 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_GOVERN   efd8 1 02 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:TRANS         efd8 1 02 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:WAIT_FREQ_DONE         efd8 1 03 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:WAIT_FOR_AFLT_RELEASE  efd8 1 03 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:WAIT_FOR_RF_RELEASE    efd8 1 03 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:MAIN                   efd8 1 03 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:MDSP_SRCH4_APP_REG     efd8 1 03 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:WAIT_GOVERN            efd8 1 03 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_START:TRANS                  efd8 1 03 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_FREQ_DONE  efd8 1 04 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 1 04 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE efd8 1 04 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:MAIN            efd8 1 04 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG efd8 1 04 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_GOVERN     efd8 1 04 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:TRANS           efd8 1 04 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:WAIT_FREQ_DONE    efd8 1 05 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 1 05 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE efd8 1 05 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:MAIN              efd8 1 05 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG efd8 1 05 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:WAIT_GOVERN       efd8 1 05 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_RELEASE_DONE:TRANS             efd8 1 05 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE  efd8 1 06 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE efd8 1 06 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE efd8 1 06 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:MAIN            efd8 1 06 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG efd8 1 06 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:WAIT_GOVERN     efd8 1 06 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_DEACTIVATE_F:TRANS           efd8 1 06 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:WAIT_FREQ_DONE         efd8 1 07 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE  efd8 1 07 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE    efd8 1 07 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:MAIN                   efd8 1 07 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:MDSP_SRCH4_APP_REG     efd8 1 07 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:WAIT_GOVERN            efd8 1 07 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_ACQ_F:TRANS                  efd8 1 07 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE efd8 1 08 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE efd8 1 08 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE efd8 1 08 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:MAIN           efd8 1 08 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG efd8 1 08 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:WAIT_GOVERN    efd8 1 08 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_LOCK_GRANTED:TRANS          efd8 1 08 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:WAIT_FREQ_DONE         efd8 1 09 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:WAIT_FOR_AFLT_RELEASE  efd8 1 09 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:WAIT_FOR_RF_RELEASE    efd8 1 09 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:MAIN                   efd8 1 09 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:MDSP_SRCH4_APP_REG     efd8 1 09 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:WAIT_GOVERN            efd8 1 09 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:MDSP_START:TRANS                  efd8 1 09 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE  efd8 1 0a 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE efd8 1 0a 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE efd8 1 0a 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:MAIN            efd8 1 0a 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG efd8 1 0a 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:WAIT_GOVERN     efd8 1 0a 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RF_REQ_FAIL_TIMER:TRANS           efd8 1 0a 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:WAIT_FREQ_DONE     efd8 1 0b 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE efd8 1 0b 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE efd8 1 0b 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:MAIN               efd8 1 0b 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG efd8 1 0b 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:WAIT_GOVERN        efd8 1 0b 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH4_MDSP_REG:TRANS              efd8 1 0b 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE efd8 1 0c 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE efd8 1 0c 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE efd8 1 0c 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:MAIN      efd8 1 0c 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG efd8 1 0c 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN efd8 1 0c 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SYS_MEAS_ABORT_COMPLETE:TRANS     efd8 1 0c 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:WAIT_FREQ_DONE      efd8 1 0d 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE efd8 1 0d 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE efd8 1 0d 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:MAIN                efd8 1 0d 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:MDSP_SRCH4_APP_REG  efd8 1 0d 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:WAIT_GOVERN         efd8 1 0d 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:RX_MOD_DENIED:TRANS               efd8 1 0d 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE   efd8 1 0e 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE efd8 1 0e 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE efd8 1 0e 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:MAIN             efd8 1 0e 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG efd8 1 0e 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:WAIT_GOVERN      efd8 1 0e 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:SRCH_LTE_1X_TT_F:TRANS            efd8 1 0e 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:WAIT_FREQ_DONE       efd8 1 0f 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE efd8 1 0f 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:WAIT_FOR_RF_RELEASE  efd8 1 0f 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:MAIN                 efd8 1 0f 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:MDSP_SRCH4_APP_REG   efd8 1 0f 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:WAIT_GOVERN          efd8 1 0f 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:GOVERN_TIMER:TRANS                efd8 1 0f 6    @purple @white

CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE efd8 1 10 0    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE efd8 1 10 1    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE efd8 1 10 2    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:MAIN           efd8 1 10 3    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG efd8 1 10 4    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:WAIT_GOVERN    efd8 1 10 5    @purple @white
CDMA:WAIT_FOR_AFLT_RELEASE:CD_RF_DIV_DISABLED:TRANS          efd8 1 10 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:WAIT_FREQ_DONE          efd8 2 00 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE   efd8 2 00 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE     efd8 2 00 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:MAIN                    efd8 2 00 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:MDSP_SRCH4_APP_REG      efd8 2 00 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:WAIT_GOVERN             efd8 2 00 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_CDMA_F:TRANS                   efd8 2 00 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:WAIT_FREQ_DONE       efd8 2 01 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE efd8 2 01 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE  efd8 2 01 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:MAIN                 efd8 2 01 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG   efd8 2 01 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:WAIT_GOVERN          efd8 2 01 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SKIP_FREQ_DONE:TRANS                efd8 2 01 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE  efd8 2 02 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE efd8 2 02 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE efd8 2 02 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:MAIN            efd8 2 02 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG efd8 2 02 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_GOVERN     efd8 2 02 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:FREQ_TRACK_OFF_DONE:TRANS           efd8 2 02 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:WAIT_FREQ_DONE           efd8 2 03 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:WAIT_FOR_AFLT_RELEASE    efd8 2 03 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:WAIT_FOR_RF_RELEASE      efd8 2 03 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:MAIN                     efd8 2 03 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:MDSP_SRCH4_APP_REG       efd8 2 03 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:WAIT_GOVERN              efd8 2 03 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_START:TRANS                    efd8 2 03 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:WAIT_FREQ_DONE    efd8 2 04 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 2 04 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE efd8 2 04 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:MAIN              efd8 2 04 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG efd8 2 04 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:WAIT_GOVERN       efd8 2 04 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:AFLT_RELEASE_DONE:TRANS             efd8 2 04 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:WAIT_FREQ_DONE      efd8 2 05 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 2 05 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE efd8 2 05 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:MAIN                efd8 2 05 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG  efd8 2 05 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:WAIT_GOVERN         efd8 2 05 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_RELEASE_DONE:TRANS               efd8 2 05 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE    efd8 2 06 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE efd8 2 06 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE efd8 2 06 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:MAIN              efd8 2 06 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG efd8 2 06 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:WAIT_GOVERN       efd8 2 06 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_DEACTIVATE_F:TRANS             efd8 2 06 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:WAIT_FREQ_DONE           efd8 2 07 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE    efd8 2 07 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE      efd8 2 07 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:MAIN                     efd8 2 07 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:MDSP_SRCH4_APP_REG       efd8 2 07 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:WAIT_GOVERN              efd8 2 07 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_ACQ_F:TRANS                    efd8 2 07 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE   efd8 2 08 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE efd8 2 08 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE efd8 2 08 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:MAIN             efd8 2 08 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG efd8 2 08 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:WAIT_GOVERN      efd8 2 08 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_LOCK_GRANTED:TRANS            efd8 2 08 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:WAIT_FREQ_DONE           efd8 2 09 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:WAIT_FOR_AFLT_RELEASE    efd8 2 09 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:WAIT_FOR_RF_RELEASE      efd8 2 09 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:MAIN                     efd8 2 09 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:MDSP_SRCH4_APP_REG       efd8 2 09 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:WAIT_GOVERN              efd8 2 09 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:MDSP_START:TRANS                    efd8 2 09 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE    efd8 2 0a 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE efd8 2 0a 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE efd8 2 0a 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:MAIN              efd8 2 0a 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG efd8 2 0a 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:WAIT_GOVERN       efd8 2 0a 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RF_REQ_FAIL_TIMER:TRANS             efd8 2 0a 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:WAIT_FREQ_DONE       efd8 2 0b 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE efd8 2 0b 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE  efd8 2 0b 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:MAIN                 efd8 2 0b 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG   efd8 2 0b 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:WAIT_GOVERN          efd8 2 0b 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH4_MDSP_REG:TRANS                efd8 2 0b 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE efd8 2 0c 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE efd8 2 0c 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE efd8 2 0c 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:MAIN        efd8 2 0c 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG efd8 2 0c 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN efd8 2 0c 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SYS_MEAS_ABORT_COMPLETE:TRANS       efd8 2 0c 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:WAIT_FREQ_DONE        efd8 2 0d 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE efd8 2 0d 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE   efd8 2 0d 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:MAIN                  efd8 2 0d 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:MDSP_SRCH4_APP_REG    efd8 2 0d 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:WAIT_GOVERN           efd8 2 0d 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:RX_MOD_DENIED:TRANS                 efd8 2 0d 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE     efd8 2 0e 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE efd8 2 0e 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE efd8 2 0e 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:MAIN               efd8 2 0e 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG efd8 2 0e 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:WAIT_GOVERN        efd8 2 0e 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:SRCH_LTE_1X_TT_F:TRANS              efd8 2 0e 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:WAIT_FREQ_DONE         efd8 2 0f 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE  efd8 2 0f 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:WAIT_FOR_RF_RELEASE    efd8 2 0f 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:MAIN                   efd8 2 0f 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:MDSP_SRCH4_APP_REG     efd8 2 0f 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:WAIT_GOVERN            efd8 2 0f 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:GOVERN_TIMER:TRANS                  efd8 2 0f 6    @purple @white

CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE   efd8 2 10 0    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE efd8 2 10 1    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE efd8 2 10 2    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:MAIN             efd8 2 10 3    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG efd8 2 10 4    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:WAIT_GOVERN      efd8 2 10 5    @purple @white
CDMA:WAIT_FOR_RF_RELEASE:CD_RF_DIV_DISABLED:TRANS            efd8 2 10 6    @purple @white

CDMA:MAIN:SRCH_CDMA_F:WAIT_FREQ_DONE                         efd8 3 00 0    @purple @white
CDMA:MAIN:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE                  efd8 3 00 1    @purple @white
CDMA:MAIN:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE                    efd8 3 00 2    @purple @white
CDMA:MAIN:SRCH_CDMA_F:MAIN                                   efd8 3 00 3    @purple @white
CDMA:MAIN:SRCH_CDMA_F:MDSP_SRCH4_APP_REG                     efd8 3 00 4    @purple @white
CDMA:MAIN:SRCH_CDMA_F:WAIT_GOVERN                            efd8 3 00 5    @purple @white
CDMA:MAIN:SRCH_CDMA_F:TRANS                                  efd8 3 00 6    @purple @white

CDMA:MAIN:SKIP_FREQ_DONE:WAIT_FREQ_DONE                      efd8 3 01 0    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE               efd8 3 01 1    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE                 efd8 3 01 2    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:MAIN                                efd8 3 01 3    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG                  efd8 3 01 4    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:WAIT_GOVERN                         efd8 3 01 5    @purple @white
CDMA:MAIN:SKIP_FREQ_DONE:TRANS                               efd8 3 01 6    @purple @white

CDMA:MAIN:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE                 efd8 3 02 0    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE          efd8 3 02 1    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE            efd8 3 02 2    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:MAIN                           efd8 3 02 3    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG             efd8 3 02 4    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:WAIT_GOVERN                    efd8 3 02 5    @purple @white
CDMA:MAIN:FREQ_TRACK_OFF_DONE:TRANS                          efd8 3 02 6    @purple @white

CDMA:MAIN:SRCH_START:WAIT_FREQ_DONE                          efd8 3 03 0    @purple @white
CDMA:MAIN:SRCH_START:WAIT_FOR_AFLT_RELEASE                   efd8 3 03 1    @purple @white
CDMA:MAIN:SRCH_START:WAIT_FOR_RF_RELEASE                     efd8 3 03 2    @purple @white
CDMA:MAIN:SRCH_START:MAIN                                    efd8 3 03 3    @purple @white
CDMA:MAIN:SRCH_START:MDSP_SRCH4_APP_REG                      efd8 3 03 4    @purple @white
CDMA:MAIN:SRCH_START:WAIT_GOVERN                             efd8 3 03 5    @purple @white
CDMA:MAIN:SRCH_START:TRANS                                   efd8 3 03 6    @purple @white

CDMA:MAIN:AFLT_RELEASE_DONE:WAIT_FREQ_DONE                   efd8 3 04 0    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE            efd8 3 04 1    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE              efd8 3 04 2    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:MAIN                             efd8 3 04 3    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG               efd8 3 04 4    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:WAIT_GOVERN                      efd8 3 04 5    @purple @white
CDMA:MAIN:AFLT_RELEASE_DONE:TRANS                            efd8 3 04 6    @purple @white

CDMA:MAIN:RF_RELEASE_DONE:WAIT_FREQ_DONE                     efd8 3 05 0    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE              efd8 3 05 1    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE                efd8 3 05 2    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:MAIN                               efd8 3 05 3    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG                 efd8 3 05 4    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:WAIT_GOVERN                        efd8 3 05 5    @purple @white
CDMA:MAIN:RF_RELEASE_DONE:TRANS                              efd8 3 05 6    @purple @white

CDMA:MAIN:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE                   efd8 3 06 0    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE            efd8 3 06 1    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE              efd8 3 06 2    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:MAIN                             efd8 3 06 3    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG               efd8 3 06 4    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:WAIT_GOVERN                      efd8 3 06 5    @purple @white
CDMA:MAIN:SRCH_DEACTIVATE_F:TRANS                            efd8 3 06 6    @purple @white

CDMA:MAIN:SRCH_ACQ_F:WAIT_FREQ_DONE                          efd8 3 07 0    @purple @white
CDMA:MAIN:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE                   efd8 3 07 1    @purple @white
CDMA:MAIN:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE                     efd8 3 07 2    @purple @white
CDMA:MAIN:SRCH_ACQ_F:MAIN                                    efd8 3 07 3    @purple @white
CDMA:MAIN:SRCH_ACQ_F:MDSP_SRCH4_APP_REG                      efd8 3 07 4    @purple @white
CDMA:MAIN:SRCH_ACQ_F:WAIT_GOVERN                             efd8 3 07 5    @purple @white
CDMA:MAIN:SRCH_ACQ_F:TRANS                                   efd8 3 07 6    @purple @white

CDMA:MAIN:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE                  efd8 3 08 0    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE           efd8 3 08 1    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE             efd8 3 08 2    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:MAIN                            efd8 3 08 3    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG              efd8 3 08 4    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:WAIT_GOVERN                     efd8 3 08 5    @purple @white
CDMA:MAIN:CD_RF_LOCK_GRANTED:TRANS                           efd8 3 08 6    @purple @white

CDMA:MAIN:MDSP_START:WAIT_FREQ_DONE                          efd8 3 09 0    @purple @white
CDMA:MAIN:MDSP_START:WAIT_FOR_AFLT_RELEASE                   efd8 3 09 1    @purple @white
CDMA:MAIN:MDSP_START:WAIT_FOR_RF_RELEASE                     efd8 3 09 2    @purple @white
CDMA:MAIN:MDSP_START:MAIN                                    efd8 3 09 3    @purple @white
CDMA:MAIN:MDSP_START:MDSP_SRCH4_APP_REG                      efd8 3 09 4    @purple @white
CDMA:MAIN:MDSP_START:WAIT_GOVERN                             efd8 3 09 5    @purple @white
CDMA:MAIN:MDSP_START:TRANS                                   efd8 3 09 6    @purple @white

CDMA:MAIN:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE                   efd8 3 0a 0    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE            efd8 3 0a 1    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE              efd8 3 0a 2    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:MAIN                             efd8 3 0a 3    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG               efd8 3 0a 4    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:WAIT_GOVERN                      efd8 3 0a 5    @purple @white
CDMA:MAIN:RF_REQ_FAIL_TIMER:TRANS                            efd8 3 0a 6    @purple @white

CDMA:MAIN:SRCH4_MDSP_REG:WAIT_FREQ_DONE                      efd8 3 0b 0    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE               efd8 3 0b 1    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE                 efd8 3 0b 2    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:MAIN                                efd8 3 0b 3    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG                  efd8 3 0b 4    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:WAIT_GOVERN                         efd8 3 0b 5    @purple @white
CDMA:MAIN:SRCH4_MDSP_REG:TRANS                               efd8 3 0b 6    @purple @white

CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE             efd8 3 0c 0    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE      efd8 3 0c 1    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE        efd8 3 0c 2    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:MAIN                       efd8 3 0c 3    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG         efd8 3 0c 4    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN                efd8 3 0c 5    @purple @white
CDMA:MAIN:SYS_MEAS_ABORT_COMPLETE:TRANS                      efd8 3 0c 6    @purple @white

CDMA:MAIN:RX_MOD_DENIED:WAIT_FREQ_DONE                       efd8 3 0d 0    @purple @white
CDMA:MAIN:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE                efd8 3 0d 1    @purple @white
CDMA:MAIN:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE                  efd8 3 0d 2    @purple @white
CDMA:MAIN:RX_MOD_DENIED:MAIN                                 efd8 3 0d 3    @purple @white
CDMA:MAIN:RX_MOD_DENIED:MDSP_SRCH4_APP_REG                   efd8 3 0d 4    @purple @white
CDMA:MAIN:RX_MOD_DENIED:WAIT_GOVERN                          efd8 3 0d 5    @purple @white
CDMA:MAIN:RX_MOD_DENIED:TRANS                                efd8 3 0d 6    @purple @white

CDMA:MAIN:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE                    efd8 3 0e 0    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE             efd8 3 0e 1    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE               efd8 3 0e 2    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:MAIN                              efd8 3 0e 3    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG                efd8 3 0e 4    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:WAIT_GOVERN                       efd8 3 0e 5    @purple @white
CDMA:MAIN:SRCH_LTE_1X_TT_F:TRANS                             efd8 3 0e 6    @purple @white

CDMA:MAIN:GOVERN_TIMER:WAIT_FREQ_DONE                        efd8 3 0f 0    @purple @white
CDMA:MAIN:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE                 efd8 3 0f 1    @purple @white
CDMA:MAIN:GOVERN_TIMER:WAIT_FOR_RF_RELEASE                   efd8 3 0f 2    @purple @white
CDMA:MAIN:GOVERN_TIMER:MAIN                                  efd8 3 0f 3    @purple @white
CDMA:MAIN:GOVERN_TIMER:MDSP_SRCH4_APP_REG                    efd8 3 0f 4    @purple @white
CDMA:MAIN:GOVERN_TIMER:WAIT_GOVERN                           efd8 3 0f 5    @purple @white
CDMA:MAIN:GOVERN_TIMER:TRANS                                 efd8 3 0f 6    @purple @white

CDMA:MAIN:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE                  efd8 3 10 0    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE           efd8 3 10 1    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE             efd8 3 10 2    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:MAIN                            efd8 3 10 3    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG              efd8 3 10 4    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:WAIT_GOVERN                     efd8 3 10 5    @purple @white
CDMA:MAIN:CD_RF_DIV_DISABLED:TRANS                           efd8 3 10 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:WAIT_FREQ_DONE           efd8 4 00 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE    efd8 4 00 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE      efd8 4 00 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:MAIN                     efd8 4 00 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:MDSP_SRCH4_APP_REG       efd8 4 00 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:WAIT_GOVERN              efd8 4 00 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_CDMA_F:TRANS                    efd8 4 00 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:WAIT_FREQ_DONE        efd8 4 01 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE efd8 4 01 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE   efd8 4 01 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:MAIN                  efd8 4 01 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG    efd8 4 01 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:WAIT_GOVERN           efd8 4 01 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SKIP_FREQ_DONE:TRANS                 efd8 4 01 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE   efd8 4 02 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE efd8 4 02 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE efd8 4 02 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:MAIN             efd8 4 02 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG efd8 4 02 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:WAIT_GOVERN      efd8 4 02 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:FREQ_TRACK_OFF_DONE:TRANS            efd8 4 02 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH_START:WAIT_FREQ_DONE            efd8 4 03 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:WAIT_FOR_AFLT_RELEASE     efd8 4 03 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:WAIT_FOR_RF_RELEASE       efd8 4 03 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:MAIN                      efd8 4 03 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:MDSP_SRCH4_APP_REG        efd8 4 03 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:WAIT_GOVERN               efd8 4 03 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_START:TRANS                     efd8 4 03 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:WAIT_FREQ_DONE     efd8 4 04 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 4 04 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE efd8 4 04 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:MAIN               efd8 4 04 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG efd8 4 04 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:WAIT_GOVERN        efd8 4 04 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:AFLT_RELEASE_DONE:TRANS              efd8 4 04 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:WAIT_FREQ_DONE       efd8 4 05 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE efd8 4 05 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE  efd8 4 05 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:MAIN                 efd8 4 05 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG   efd8 4 05 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:WAIT_GOVERN          efd8 4 05 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_RELEASE_DONE:TRANS                efd8 4 05 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE     efd8 4 06 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE efd8 4 06 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE efd8 4 06 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:MAIN               efd8 4 06 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG efd8 4 06 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:WAIT_GOVERN        efd8 4 06 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_DEACTIVATE_F:TRANS              efd8 4 06 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:WAIT_FREQ_DONE            efd8 4 07 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE     efd8 4 07 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE       efd8 4 07 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:MAIN                      efd8 4 07 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:MDSP_SRCH4_APP_REG        efd8 4 07 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:WAIT_GOVERN               efd8 4 07 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_ACQ_F:TRANS                     efd8 4 07 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE    efd8 4 08 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE efd8 4 08 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE efd8 4 08 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:MAIN              efd8 4 08 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG efd8 4 08 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:WAIT_GOVERN       efd8 4 08 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_LOCK_GRANTED:TRANS             efd8 4 08 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:MDSP_START:WAIT_FREQ_DONE            efd8 4 09 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:WAIT_FOR_AFLT_RELEASE     efd8 4 09 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:WAIT_FOR_RF_RELEASE       efd8 4 09 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:MAIN                      efd8 4 09 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:MDSP_SRCH4_APP_REG        efd8 4 09 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:WAIT_GOVERN               efd8 4 09 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:MDSP_START:TRANS                     efd8 4 09 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE     efd8 4 0a 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE efd8 4 0a 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE efd8 4 0a 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:MAIN               efd8 4 0a 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG efd8 4 0a 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:WAIT_GOVERN        efd8 4 0a 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RF_REQ_FAIL_TIMER:TRANS              efd8 4 0a 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:WAIT_FREQ_DONE        efd8 4 0b 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE efd8 4 0b 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE   efd8 4 0b 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:MAIN                  efd8 4 0b 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG    efd8 4 0b 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:WAIT_GOVERN           efd8 4 0b 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH4_MDSP_REG:TRANS                 efd8 4 0b 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE efd8 4 0c 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE efd8 4 0c 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE efd8 4 0c 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:MAIN         efd8 4 0c 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG efd8 4 0c 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN  efd8 4 0c 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SYS_MEAS_ABORT_COMPLETE:TRANS        efd8 4 0c 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:WAIT_FREQ_DONE         efd8 4 0d 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE  efd8 4 0d 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE    efd8 4 0d 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:MAIN                   efd8 4 0d 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:MDSP_SRCH4_APP_REG     efd8 4 0d 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:WAIT_GOVERN            efd8 4 0d 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:RX_MOD_DENIED:TRANS                  efd8 4 0d 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE      efd8 4 0e 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE efd8 4 0e 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE efd8 4 0e 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:MAIN                efd8 4 0e 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG  efd8 4 0e 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:WAIT_GOVERN         efd8 4 0e 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:SRCH_LTE_1X_TT_F:TRANS               efd8 4 0e 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:WAIT_FREQ_DONE          efd8 4 0f 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE   efd8 4 0f 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:WAIT_FOR_RF_RELEASE     efd8 4 0f 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:MAIN                    efd8 4 0f 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:MDSP_SRCH4_APP_REG      efd8 4 0f 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:WAIT_GOVERN             efd8 4 0f 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:GOVERN_TIMER:TRANS                   efd8 4 0f 6    @purple @white

CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE    efd8 4 10 0    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE efd8 4 10 1    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE efd8 4 10 2    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:MAIN              efd8 4 10 3    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG efd8 4 10 4    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:WAIT_GOVERN       efd8 4 10 5    @purple @white
CDMA:MDSP_SRCH4_APP_REG:CD_RF_DIV_DISABLED:TRANS             efd8 4 10 6    @purple @white

CDMA:WAIT_GOVERN:SRCH_CDMA_F:WAIT_FREQ_DONE                  efd8 5 00 0    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE           efd8 5 00 1    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE             efd8 5 00 2    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:MAIN                            efd8 5 00 3    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:MDSP_SRCH4_APP_REG              efd8 5 00 4    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:WAIT_GOVERN                     efd8 5 00 5    @purple @white
CDMA:WAIT_GOVERN:SRCH_CDMA_F:TRANS                           efd8 5 00 6    @purple @white

CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:WAIT_FREQ_DONE               efd8 5 01 0    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE        efd8 5 01 1    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE          efd8 5 01 2    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:MAIN                         efd8 5 01 3    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG           efd8 5 01 4    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:WAIT_GOVERN                  efd8 5 01 5    @purple @white
CDMA:WAIT_GOVERN:SKIP_FREQ_DONE:TRANS                        efd8 5 01 6    @purple @white

CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE          efd8 5 02 0    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE   efd8 5 02 1    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE     efd8 5 02 2    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:MAIN                    efd8 5 02 3    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG      efd8 5 02 4    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:WAIT_GOVERN             efd8 5 02 5    @purple @white
CDMA:WAIT_GOVERN:FREQ_TRACK_OFF_DONE:TRANS                   efd8 5 02 6    @purple @white

CDMA:WAIT_GOVERN:SRCH_START:WAIT_FREQ_DONE                   efd8 5 03 0    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:WAIT_FOR_AFLT_RELEASE            efd8 5 03 1    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:WAIT_FOR_RF_RELEASE              efd8 5 03 2    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:MAIN                             efd8 5 03 3    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:MDSP_SRCH4_APP_REG               efd8 5 03 4    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:WAIT_GOVERN                      efd8 5 03 5    @purple @white
CDMA:WAIT_GOVERN:SRCH_START:TRANS                            efd8 5 03 6    @purple @white

CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:WAIT_FREQ_DONE            efd8 5 04 0    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE     efd8 5 04 1    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE       efd8 5 04 2    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:MAIN                      efd8 5 04 3    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG        efd8 5 04 4    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:WAIT_GOVERN               efd8 5 04 5    @purple @white
CDMA:WAIT_GOVERN:AFLT_RELEASE_DONE:TRANS                     efd8 5 04 6    @purple @white

CDMA:WAIT_GOVERN:RF_RELEASE_DONE:WAIT_FREQ_DONE              efd8 5 05 0    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE       efd8 5 05 1    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE         efd8 5 05 2    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:MAIN                        efd8 5 05 3    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG          efd8 5 05 4    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:WAIT_GOVERN                 efd8 5 05 5    @purple @white
CDMA:WAIT_GOVERN:RF_RELEASE_DONE:TRANS                       efd8 5 05 6    @purple @white

CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE            efd8 5 06 0    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE     efd8 5 06 1    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE       efd8 5 06 2    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:MAIN                      efd8 5 06 3    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG        efd8 5 06 4    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:WAIT_GOVERN               efd8 5 06 5    @purple @white
CDMA:WAIT_GOVERN:SRCH_DEACTIVATE_F:TRANS                     efd8 5 06 6    @purple @white

CDMA:WAIT_GOVERN:SRCH_ACQ_F:WAIT_FREQ_DONE                   efd8 5 07 0    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE            efd8 5 07 1    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE              efd8 5 07 2    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:MAIN                             efd8 5 07 3    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:MDSP_SRCH4_APP_REG               efd8 5 07 4    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:WAIT_GOVERN                      efd8 5 07 5    @purple @white
CDMA:WAIT_GOVERN:SRCH_ACQ_F:TRANS                            efd8 5 07 6    @purple @white

CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE           efd8 5 08 0    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE    efd8 5 08 1    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE      efd8 5 08 2    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:MAIN                     efd8 5 08 3    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG       efd8 5 08 4    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:WAIT_GOVERN              efd8 5 08 5    @purple @white
CDMA:WAIT_GOVERN:CD_RF_LOCK_GRANTED:TRANS                    efd8 5 08 6    @purple @white

CDMA:WAIT_GOVERN:MDSP_START:WAIT_FREQ_DONE                   efd8 5 09 0    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:WAIT_FOR_AFLT_RELEASE            efd8 5 09 1    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:WAIT_FOR_RF_RELEASE              efd8 5 09 2    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:MAIN                             efd8 5 09 3    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:MDSP_SRCH4_APP_REG               efd8 5 09 4    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:WAIT_GOVERN                      efd8 5 09 5    @purple @white
CDMA:WAIT_GOVERN:MDSP_START:TRANS                            efd8 5 09 6    @purple @white

CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE            efd8 5 0a 0    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE     efd8 5 0a 1    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE       efd8 5 0a 2    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:MAIN                      efd8 5 0a 3    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG        efd8 5 0a 4    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:WAIT_GOVERN               efd8 5 0a 5    @purple @white
CDMA:WAIT_GOVERN:RF_REQ_FAIL_TIMER:TRANS                     efd8 5 0a 6    @purple @white

CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:WAIT_FREQ_DONE               efd8 5 0b 0    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE        efd8 5 0b 1    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE          efd8 5 0b 2    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:MAIN                         efd8 5 0b 3    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG           efd8 5 0b 4    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:WAIT_GOVERN                  efd8 5 0b 5    @purple @white
CDMA:WAIT_GOVERN:SRCH4_MDSP_REG:TRANS                        efd8 5 0b 6    @purple @white

CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE      efd8 5 0c 0    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE efd8 5 0c 1    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE efd8 5 0c 2    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:MAIN                efd8 5 0c 3    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG  efd8 5 0c 4    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN         efd8 5 0c 5    @purple @white
CDMA:WAIT_GOVERN:SYS_MEAS_ABORT_COMPLETE:TRANS               efd8 5 0c 6    @purple @white

CDMA:WAIT_GOVERN:RX_MOD_DENIED:WAIT_FREQ_DONE                efd8 5 0d 0    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE         efd8 5 0d 1    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE           efd8 5 0d 2    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:MAIN                          efd8 5 0d 3    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:MDSP_SRCH4_APP_REG            efd8 5 0d 4    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:WAIT_GOVERN                   efd8 5 0d 5    @purple @white
CDMA:WAIT_GOVERN:RX_MOD_DENIED:TRANS                         efd8 5 0d 6    @purple @white

CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE             efd8 5 0e 0    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE      efd8 5 0e 1    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE        efd8 5 0e 2    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:MAIN                       efd8 5 0e 3    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG         efd8 5 0e 4    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:WAIT_GOVERN                efd8 5 0e 5    @purple @white
CDMA:WAIT_GOVERN:SRCH_LTE_1X_TT_F:TRANS                      efd8 5 0e 6    @purple @white

CDMA:WAIT_GOVERN:GOVERN_TIMER:WAIT_FREQ_DONE                 efd8 5 0f 0    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE          efd8 5 0f 1    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:WAIT_FOR_RF_RELEASE            efd8 5 0f 2    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:MAIN                           efd8 5 0f 3    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:MDSP_SRCH4_APP_REG             efd8 5 0f 4    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:WAIT_GOVERN                    efd8 5 0f 5    @purple @white
CDMA:WAIT_GOVERN:GOVERN_TIMER:TRANS                          efd8 5 0f 6    @purple @white

CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE           efd8 5 10 0    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE    efd8 5 10 1    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE      efd8 5 10 2    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:MAIN                     efd8 5 10 3    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG       efd8 5 10 4    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:WAIT_GOVERN              efd8 5 10 5    @purple @white
CDMA:WAIT_GOVERN:CD_RF_DIV_DISABLED:TRANS                    efd8 5 10 6    @purple @white

CDMA:TRANS:SRCH_CDMA_F:WAIT_FREQ_DONE                        efd8 6 00 0    @purple @white
CDMA:TRANS:SRCH_CDMA_F:WAIT_FOR_AFLT_RELEASE                 efd8 6 00 1    @purple @white
CDMA:TRANS:SRCH_CDMA_F:WAIT_FOR_RF_RELEASE                   efd8 6 00 2    @purple @white
CDMA:TRANS:SRCH_CDMA_F:MAIN                                  efd8 6 00 3    @purple @white
CDMA:TRANS:SRCH_CDMA_F:MDSP_SRCH4_APP_REG                    efd8 6 00 4    @purple @white
CDMA:TRANS:SRCH_CDMA_F:WAIT_GOVERN                           efd8 6 00 5    @purple @white
CDMA:TRANS:SRCH_CDMA_F:TRANS                                 efd8 6 00 6    @purple @white

CDMA:TRANS:SKIP_FREQ_DONE:WAIT_FREQ_DONE                     efd8 6 01 0    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:WAIT_FOR_AFLT_RELEASE              efd8 6 01 1    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:WAIT_FOR_RF_RELEASE                efd8 6 01 2    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:MAIN                               efd8 6 01 3    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:MDSP_SRCH4_APP_REG                 efd8 6 01 4    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:WAIT_GOVERN                        efd8 6 01 5    @purple @white
CDMA:TRANS:SKIP_FREQ_DONE:TRANS                              efd8 6 01 6    @purple @white

CDMA:TRANS:FREQ_TRACK_OFF_DONE:WAIT_FREQ_DONE                efd8 6 02 0    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE         efd8 6 02 1    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:WAIT_FOR_RF_RELEASE           efd8 6 02 2    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:MAIN                          efd8 6 02 3    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:MDSP_SRCH4_APP_REG            efd8 6 02 4    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:WAIT_GOVERN                   efd8 6 02 5    @purple @white
CDMA:TRANS:FREQ_TRACK_OFF_DONE:TRANS                         efd8 6 02 6    @purple @white

CDMA:TRANS:SRCH_START:WAIT_FREQ_DONE                         efd8 6 03 0    @purple @white
CDMA:TRANS:SRCH_START:WAIT_FOR_AFLT_RELEASE                  efd8 6 03 1    @purple @white
CDMA:TRANS:SRCH_START:WAIT_FOR_RF_RELEASE                    efd8 6 03 2    @purple @white
CDMA:TRANS:SRCH_START:MAIN                                   efd8 6 03 3    @purple @white
CDMA:TRANS:SRCH_START:MDSP_SRCH4_APP_REG                     efd8 6 03 4    @purple @white
CDMA:TRANS:SRCH_START:WAIT_GOVERN                            efd8 6 03 5    @purple @white
CDMA:TRANS:SRCH_START:TRANS                                  efd8 6 03 6    @purple @white

CDMA:TRANS:AFLT_RELEASE_DONE:WAIT_FREQ_DONE                  efd8 6 04 0    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE           efd8 6 04 1    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:WAIT_FOR_RF_RELEASE             efd8 6 04 2    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:MAIN                            efd8 6 04 3    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:MDSP_SRCH4_APP_REG              efd8 6 04 4    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:WAIT_GOVERN                     efd8 6 04 5    @purple @white
CDMA:TRANS:AFLT_RELEASE_DONE:TRANS                           efd8 6 04 6    @purple @white

CDMA:TRANS:RF_RELEASE_DONE:WAIT_FREQ_DONE                    efd8 6 05 0    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE             efd8 6 05 1    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:WAIT_FOR_RF_RELEASE               efd8 6 05 2    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:MAIN                              efd8 6 05 3    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:MDSP_SRCH4_APP_REG                efd8 6 05 4    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:WAIT_GOVERN                       efd8 6 05 5    @purple @white
CDMA:TRANS:RF_RELEASE_DONE:TRANS                             efd8 6 05 6    @purple @white

CDMA:TRANS:SRCH_DEACTIVATE_F:WAIT_FREQ_DONE                  efd8 6 06 0    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:WAIT_FOR_AFLT_RELEASE           efd8 6 06 1    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:WAIT_FOR_RF_RELEASE             efd8 6 06 2    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:MAIN                            efd8 6 06 3    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:MDSP_SRCH4_APP_REG              efd8 6 06 4    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:WAIT_GOVERN                     efd8 6 06 5    @purple @white
CDMA:TRANS:SRCH_DEACTIVATE_F:TRANS                           efd8 6 06 6    @purple @white

CDMA:TRANS:SRCH_ACQ_F:WAIT_FREQ_DONE                         efd8 6 07 0    @purple @white
CDMA:TRANS:SRCH_ACQ_F:WAIT_FOR_AFLT_RELEASE                  efd8 6 07 1    @purple @white
CDMA:TRANS:SRCH_ACQ_F:WAIT_FOR_RF_RELEASE                    efd8 6 07 2    @purple @white
CDMA:TRANS:SRCH_ACQ_F:MAIN                                   efd8 6 07 3    @purple @white
CDMA:TRANS:SRCH_ACQ_F:MDSP_SRCH4_APP_REG                     efd8 6 07 4    @purple @white
CDMA:TRANS:SRCH_ACQ_F:WAIT_GOVERN                            efd8 6 07 5    @purple @white
CDMA:TRANS:SRCH_ACQ_F:TRANS                                  efd8 6 07 6    @purple @white

CDMA:TRANS:CD_RF_LOCK_GRANTED:WAIT_FREQ_DONE                 efd8 6 08 0    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:WAIT_FOR_AFLT_RELEASE          efd8 6 08 1    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:WAIT_FOR_RF_RELEASE            efd8 6 08 2    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:MAIN                           efd8 6 08 3    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:MDSP_SRCH4_APP_REG             efd8 6 08 4    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:WAIT_GOVERN                    efd8 6 08 5    @purple @white
CDMA:TRANS:CD_RF_LOCK_GRANTED:TRANS                          efd8 6 08 6    @purple @white

CDMA:TRANS:MDSP_START:WAIT_FREQ_DONE                         efd8 6 09 0    @purple @white
CDMA:TRANS:MDSP_START:WAIT_FOR_AFLT_RELEASE                  efd8 6 09 1    @purple @white
CDMA:TRANS:MDSP_START:WAIT_FOR_RF_RELEASE                    efd8 6 09 2    @purple @white
CDMA:TRANS:MDSP_START:MAIN                                   efd8 6 09 3    @purple @white
CDMA:TRANS:MDSP_START:MDSP_SRCH4_APP_REG                     efd8 6 09 4    @purple @white
CDMA:TRANS:MDSP_START:WAIT_GOVERN                            efd8 6 09 5    @purple @white
CDMA:TRANS:MDSP_START:TRANS                                  efd8 6 09 6    @purple @white

CDMA:TRANS:RF_REQ_FAIL_TIMER:WAIT_FREQ_DONE                  efd8 6 0a 0    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:WAIT_FOR_AFLT_RELEASE           efd8 6 0a 1    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:WAIT_FOR_RF_RELEASE             efd8 6 0a 2    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:MAIN                            efd8 6 0a 3    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:MDSP_SRCH4_APP_REG              efd8 6 0a 4    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:WAIT_GOVERN                     efd8 6 0a 5    @purple @white
CDMA:TRANS:RF_REQ_FAIL_TIMER:TRANS                           efd8 6 0a 6    @purple @white

CDMA:TRANS:SRCH4_MDSP_REG:WAIT_FREQ_DONE                     efd8 6 0b 0    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:WAIT_FOR_AFLT_RELEASE              efd8 6 0b 1    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:WAIT_FOR_RF_RELEASE                efd8 6 0b 2    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:MAIN                               efd8 6 0b 3    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:MDSP_SRCH4_APP_REG                 efd8 6 0b 4    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:WAIT_GOVERN                        efd8 6 0b 5    @purple @white
CDMA:TRANS:SRCH4_MDSP_REG:TRANS                              efd8 6 0b 6    @purple @white

CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:WAIT_FREQ_DONE            efd8 6 0c 0    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_AFLT_RELEASE     efd8 6 0c 1    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:WAIT_FOR_RF_RELEASE       efd8 6 0c 2    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:MAIN                      efd8 6 0c 3    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:MDSP_SRCH4_APP_REG        efd8 6 0c 4    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:WAIT_GOVERN               efd8 6 0c 5    @purple @white
CDMA:TRANS:SYS_MEAS_ABORT_COMPLETE:TRANS                     efd8 6 0c 6    @purple @white

CDMA:TRANS:RX_MOD_DENIED:WAIT_FREQ_DONE                      efd8 6 0d 0    @purple @white
CDMA:TRANS:RX_MOD_DENIED:WAIT_FOR_AFLT_RELEASE               efd8 6 0d 1    @purple @white
CDMA:TRANS:RX_MOD_DENIED:WAIT_FOR_RF_RELEASE                 efd8 6 0d 2    @purple @white
CDMA:TRANS:RX_MOD_DENIED:MAIN                                efd8 6 0d 3    @purple @white
CDMA:TRANS:RX_MOD_DENIED:MDSP_SRCH4_APP_REG                  efd8 6 0d 4    @purple @white
CDMA:TRANS:RX_MOD_DENIED:WAIT_GOVERN                         efd8 6 0d 5    @purple @white
CDMA:TRANS:RX_MOD_DENIED:TRANS                               efd8 6 0d 6    @purple @white

CDMA:TRANS:SRCH_LTE_1X_TT_F:WAIT_FREQ_DONE                   efd8 6 0e 0    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:WAIT_FOR_AFLT_RELEASE            efd8 6 0e 1    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:WAIT_FOR_RF_RELEASE              efd8 6 0e 2    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:MAIN                             efd8 6 0e 3    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:MDSP_SRCH4_APP_REG               efd8 6 0e 4    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:WAIT_GOVERN                      efd8 6 0e 5    @purple @white
CDMA:TRANS:SRCH_LTE_1X_TT_F:TRANS                            efd8 6 0e 6    @purple @white

CDMA:TRANS:GOVERN_TIMER:WAIT_FREQ_DONE                       efd8 6 0f 0    @purple @white
CDMA:TRANS:GOVERN_TIMER:WAIT_FOR_AFLT_RELEASE                efd8 6 0f 1    @purple @white
CDMA:TRANS:GOVERN_TIMER:WAIT_FOR_RF_RELEASE                  efd8 6 0f 2    @purple @white
CDMA:TRANS:GOVERN_TIMER:MAIN                                 efd8 6 0f 3    @purple @white
CDMA:TRANS:GOVERN_TIMER:MDSP_SRCH4_APP_REG                   efd8 6 0f 4    @purple @white
CDMA:TRANS:GOVERN_TIMER:WAIT_GOVERN                          efd8 6 0f 5    @purple @white
CDMA:TRANS:GOVERN_TIMER:TRANS                                efd8 6 0f 6    @purple @white

CDMA:TRANS:CD_RF_DIV_DISABLED:WAIT_FREQ_DONE                 efd8 6 10 0    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:WAIT_FOR_AFLT_RELEASE          efd8 6 10 1    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:WAIT_FOR_RF_RELEASE            efd8 6 10 2    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:MAIN                           efd8 6 10 3    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:MDSP_SRCH4_APP_REG             efd8 6 10 4    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:WAIT_GOVERN                    efd8 6 10 5    @purple @white
CDMA:TRANS:CD_RF_DIV_DISABLED:TRANS                          efd8 6 10 6    @purple @white



# End machine generated TLA code for state machine: CDMA_SM

