/*=============================================================================

  srchna_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchna_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: INACTIVE_SM */

/* Transition function prototypes */
static stm_state_type na_stay_in_standby(void *);
static stm_state_type na_go_to_amps(void *);
static stm_state_type na_go_to_cdma(void *);
static stm_state_type na_process_enter_amps(void *);
static stm_state_type na_process_enter_cdma(void *);
static stm_state_type na_process_powerdown(void *);
static stm_state_type na_process_stop_done(void *);
static stm_state_type na_process_offline_done(void *);
static stm_state_type na_process_sclk_complete_cmd(void *);
static stm_state_type na_process_srchlte_tt_cmd(void *);
static stm_state_type na_process_irat_tt_cmd(void *);


/* State Machine entry/exit function prototypes */
static void na_enter_inactive(stm_group_type *group);
static void na_exit_inactive(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define INACTIVE_SM_NUMBER_OF_STATES 1
#define INACTIVE_SM_NUMBER_OF_INPUTS 12


/* State enumeration */
enum
{
  STANDBY_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  INACTIVE_SM_states[ INACTIVE_SM_NUMBER_OF_STATES ] =
{
  {"STANDBY_STATE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  INACTIVE_SM_inputs[ INACTIVE_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_DEACTIVATE_F, "SRCH_DEACTIVATE_F"},
  {(stm_input_type)SRCH_AMPS_F, "SRCH_AMPS_F"},
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)ENTER_AMPS_CMD, "ENTER_AMPS_CMD"},
  {(stm_input_type)ENTER_CDMA_CMD, "ENTER_CDMA_CMD"},
  {(stm_input_type)SRCH_POWERDOWN_F, "SRCH_POWERDOWN_F"},
  {(stm_input_type)SRCH_STOP_CMD, "SRCH_STOP_CMD"},
  {(stm_input_type)SRCH_OFFLINE_CMD, "SRCH_OFFLINE_CMD"},
  {(stm_input_type)SRCH_SCLK_COMPLETE_CMD, "SRCH_SCLK_COMPLETE_CMD"},
  {(stm_input_type)SRCHLTE_TT_F, "SRCHLTE_TT_F"},
  {(stm_input_type)IRAT_TT_F, "IRAT_TT_F"},
  {(stm_input_type)IRAT_PILOT_MEAS_CMD, "IRAT_PILOT_MEAS_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  INACTIVE_SM_transitions[ INACTIVE_SM_NUMBER_OF_STATES *  INACTIVE_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state STANDBY_STATE */
    na_stay_in_standby,    /* SRCH_DEACTIVATE_F */
    na_go_to_amps,    /* SRCH_AMPS_F */
    na_go_to_cdma,    /* SRCH_CDMA_F */
    na_process_enter_amps,    /* ENTER_AMPS_CMD */
    na_process_enter_cdma,    /* ENTER_CDMA_CMD */
    na_process_powerdown,    /* SRCH_POWERDOWN_F */
    na_process_stop_done,    /* SRCH_STOP_CMD */
    na_process_offline_done,    /* SRCH_OFFLINE_CMD */
    na_process_sclk_complete_cmd,    /* SRCH_SCLK_COMPLETE_CMD */
    na_process_srchlte_tt_cmd,    /* SRCHLTE_TT_F */
    na_process_irat_tt_cmd,    /* IRAT_TT_F */
    na_process_irat_tt_cmd,    /* IRAT_PILOT_MEAS_CMD */

};


/* State machine definition */
stm_state_machine_type INACTIVE_SM =
{
  "INACTIVE_SM", /* state machine name */
  37746, /* unique SM id (hash of name) */
  na_enter_inactive, /* state machine entry function */
  na_exit_inactive, /* state machine exit function */
  STANDBY_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  INACTIVE_SM_NUMBER_OF_STATES,
  INACTIVE_SM_NUMBER_OF_INPUTS,
  INACTIVE_SM_states,
  INACTIVE_SM_inputs,
  INACTIVE_SM_transitions,
};

/* End machine generated code for state machine: INACTIVE_SM */
/* Begin machine generated code for state machine group: SRCH_NA_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean na_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_NA_GROUP_sm_list[ 1 ] =
{
  &INACTIVE_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_NA_GROUP_sig_list[ 3 ] =
{
  SRCH_CMD_Q_SIG,
  SRCH_STOP_SIG,
  SRCH_OFFLINE_SIG,
};


/* State machine group definition */
stm_group_type SRCH_NA_GROUP =
{
  "SRCH_NA_GROUP", /* state machine group name */
  SRCH_NA_GROUP_sig_list, /* signal mapping table */
  3, /* number of signals in mapping table */
  na_sig_mapper, /* signal mapper function */
  SRCH_NA_GROUP_sm_list, /* state machine table */
  1, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_NA_GROUP */

