###############################################################################
#
#    srch_rx_div_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srch_rx_div_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: DIV_SM
# State machine:current state:input:next state                      fgcolor bgcolor

DIV:DIV_OFF:DIV_REQUEST:DIV_OFF                              eda8 0 00 0    @red @white
DIV:DIV_OFF:DIV_REQUEST:DIV_REQUESTED                        eda8 0 00 1    @red @white
DIV:DIV_OFF:DIV_REQUEST:DIV_INITING                          eda8 0 00 2    @red @white
DIV:DIV_OFF:DIV_REQUEST:DIV_ENABLED                          eda8 0 00 3    @red @white
DIV:DIV_OFF:DIV_REQUEST:DIV_ENABLED_PRX                      eda8 0 00 4    @red @white

DIV:DIV_OFF:DIV_STOP:DIV_OFF                                 eda8 0 01 0    @red @white
DIV:DIV_OFF:DIV_STOP:DIV_REQUESTED                           eda8 0 01 1    @red @white
DIV:DIV_OFF:DIV_STOP:DIV_INITING                             eda8 0 01 2    @red @white
DIV:DIV_OFF:DIV_STOP:DIV_ENABLED                             eda8 0 01 3    @red @white
DIV:DIV_OFF:DIV_STOP:DIV_ENABLED_PRX                         eda8 0 01 4    @red @white

DIV:DIV_OFF:DIV_AGC_CK:DIV_OFF                               eda8 0 02 0    @red @white
DIV:DIV_OFF:DIV_AGC_CK:DIV_REQUESTED                         eda8 0 02 1    @red @white
DIV:DIV_OFF:DIV_AGC_CK:DIV_INITING                           eda8 0 02 2    @red @white
DIV:DIV_OFF:DIV_AGC_CK:DIV_ENABLED                           eda8 0 02 3    @red @white
DIV:DIV_OFF:DIV_AGC_CK:DIV_ENABLED_PRX                       eda8 0 02 4    @red @white

DIV:DIV_OFF:RX_CH_DISABLE_DIV_COMP_DONE:DIV_OFF              eda8 0 03 0    @red @white
DIV:DIV_OFF:RX_CH_DISABLE_DIV_COMP_DONE:DIV_REQUESTED        eda8 0 03 1    @red @white
DIV:DIV_OFF:RX_CH_DISABLE_DIV_COMP_DONE:DIV_INITING          eda8 0 03 2    @red @white
DIV:DIV_OFF:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED          eda8 0 03 3    @red @white
DIV:DIV_OFF:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED_PRX      eda8 0 03 4    @red @white

DIV:DIV_OFF:DIV_UPDATE_PRIORITY:DIV_OFF                      eda8 0 04 0    @red @white
DIV:DIV_OFF:DIV_UPDATE_PRIORITY:DIV_REQUESTED                eda8 0 04 1    @red @white
DIV:DIV_OFF:DIV_UPDATE_PRIORITY:DIV_INITING                  eda8 0 04 2    @red @white
DIV:DIV_OFF:DIV_UPDATE_PRIORITY:DIV_ENABLED                  eda8 0 04 3    @red @white
DIV:DIV_OFF:DIV_UPDATE_PRIORITY:DIV_ENABLED_PRX              eda8 0 04 4    @red @white

DIV:DIV_OFF:DIV_INIT:DIV_OFF                                 eda8 0 05 0    @red @white
DIV:DIV_OFF:DIV_INIT:DIV_REQUESTED                           eda8 0 05 1    @red @white
DIV:DIV_OFF:DIV_INIT:DIV_INITING                             eda8 0 05 2    @red @white
DIV:DIV_OFF:DIV_INIT:DIV_ENABLED                             eda8 0 05 3    @red @white
DIV:DIV_OFF:DIV_INIT:DIV_ENABLED_PRX                         eda8 0 05 4    @red @white

DIV:DIV_OFF:DIV_READY:DIV_OFF                                eda8 0 06 0    @red @white
DIV:DIV_OFF:DIV_READY:DIV_REQUESTED                          eda8 0 06 1    @red @white
DIV:DIV_OFF:DIV_READY:DIV_INITING                            eda8 0 06 2    @red @white
DIV:DIV_OFF:DIV_READY:DIV_ENABLED                            eda8 0 06 3    @red @white
DIV:DIV_OFF:DIV_READY:DIV_ENABLED_PRX                        eda8 0 06 4    @red @white

DIV:DIV_OFF:DIV_PAUSE:DIV_OFF                                eda8 0 07 0    @red @white
DIV:DIV_OFF:DIV_PAUSE:DIV_REQUESTED                          eda8 0 07 1    @red @white
DIV:DIV_OFF:DIV_PAUSE:DIV_INITING                            eda8 0 07 2    @red @white
DIV:DIV_OFF:DIV_PAUSE:DIV_ENABLED                            eda8 0 07 3    @red @white
DIV:DIV_OFF:DIV_PAUSE:DIV_ENABLED_PRX                        eda8 0 07 4    @red @white

DIV:DIV_OFF:DIV_FING_START:DIV_OFF                           eda8 0 08 0    @red @white
DIV:DIV_OFF:DIV_FING_START:DIV_REQUESTED                     eda8 0 08 1    @red @white
DIV:DIV_OFF:DIV_FING_START:DIV_INITING                       eda8 0 08 2    @red @white
DIV:DIV_OFF:DIV_FING_START:DIV_ENABLED                       eda8 0 08 3    @red @white
DIV:DIV_OFF:DIV_FING_START:DIV_ENABLED_PRX                   eda8 0 08 4    @red @white

DIV:DIV_OFF:DIV_RELEASE:DIV_OFF                              eda8 0 09 0    @red @white
DIV:DIV_OFF:DIV_RELEASE:DIV_REQUESTED                        eda8 0 09 1    @red @white
DIV:DIV_OFF:DIV_RELEASE:DIV_INITING                          eda8 0 09 2    @red @white
DIV:DIV_OFF:DIV_RELEASE:DIV_ENABLED                          eda8 0 09 3    @red @white
DIV:DIV_OFF:DIV_RELEASE:DIV_ENABLED_PRX                      eda8 0 09 4    @red @white

DIV:DIV_REQUESTED:DIV_REQUEST:DIV_OFF                        eda8 1 00 0    @red @white
DIV:DIV_REQUESTED:DIV_REQUEST:DIV_REQUESTED                  eda8 1 00 1    @red @white
DIV:DIV_REQUESTED:DIV_REQUEST:DIV_INITING                    eda8 1 00 2    @red @white
DIV:DIV_REQUESTED:DIV_REQUEST:DIV_ENABLED                    eda8 1 00 3    @red @white
DIV:DIV_REQUESTED:DIV_REQUEST:DIV_ENABLED_PRX                eda8 1 00 4    @red @white

DIV:DIV_REQUESTED:DIV_STOP:DIV_OFF                           eda8 1 01 0    @red @white
DIV:DIV_REQUESTED:DIV_STOP:DIV_REQUESTED                     eda8 1 01 1    @red @white
DIV:DIV_REQUESTED:DIV_STOP:DIV_INITING                       eda8 1 01 2    @red @white
DIV:DIV_REQUESTED:DIV_STOP:DIV_ENABLED                       eda8 1 01 3    @red @white
DIV:DIV_REQUESTED:DIV_STOP:DIV_ENABLED_PRX                   eda8 1 01 4    @red @white

DIV:DIV_REQUESTED:DIV_AGC_CK:DIV_OFF                         eda8 1 02 0    @red @white
DIV:DIV_REQUESTED:DIV_AGC_CK:DIV_REQUESTED                   eda8 1 02 1    @red @white
DIV:DIV_REQUESTED:DIV_AGC_CK:DIV_INITING                     eda8 1 02 2    @red @white
DIV:DIV_REQUESTED:DIV_AGC_CK:DIV_ENABLED                     eda8 1 02 3    @red @white
DIV:DIV_REQUESTED:DIV_AGC_CK:DIV_ENABLED_PRX                 eda8 1 02 4    @red @white

DIV:DIV_REQUESTED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_OFF        eda8 1 03 0    @red @white
DIV:DIV_REQUESTED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_REQUESTED  eda8 1 03 1    @red @white
DIV:DIV_REQUESTED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_INITING    eda8 1 03 2    @red @white
DIV:DIV_REQUESTED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED    eda8 1 03 3    @red @white
DIV:DIV_REQUESTED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED_PRX eda8 1 03 4    @red @white

DIV:DIV_REQUESTED:DIV_UPDATE_PRIORITY:DIV_OFF                eda8 1 04 0    @red @white
DIV:DIV_REQUESTED:DIV_UPDATE_PRIORITY:DIV_REQUESTED          eda8 1 04 1    @red @white
DIV:DIV_REQUESTED:DIV_UPDATE_PRIORITY:DIV_INITING            eda8 1 04 2    @red @white
DIV:DIV_REQUESTED:DIV_UPDATE_PRIORITY:DIV_ENABLED            eda8 1 04 3    @red @white
DIV:DIV_REQUESTED:DIV_UPDATE_PRIORITY:DIV_ENABLED_PRX        eda8 1 04 4    @red @white

DIV:DIV_REQUESTED:DIV_INIT:DIV_OFF                           eda8 1 05 0    @red @white
DIV:DIV_REQUESTED:DIV_INIT:DIV_REQUESTED                     eda8 1 05 1    @red @white
DIV:DIV_REQUESTED:DIV_INIT:DIV_INITING                       eda8 1 05 2    @red @white
DIV:DIV_REQUESTED:DIV_INIT:DIV_ENABLED                       eda8 1 05 3    @red @white
DIV:DIV_REQUESTED:DIV_INIT:DIV_ENABLED_PRX                   eda8 1 05 4    @red @white

DIV:DIV_REQUESTED:DIV_READY:DIV_OFF                          eda8 1 06 0    @red @white
DIV:DIV_REQUESTED:DIV_READY:DIV_REQUESTED                    eda8 1 06 1    @red @white
DIV:DIV_REQUESTED:DIV_READY:DIV_INITING                      eda8 1 06 2    @red @white
DIV:DIV_REQUESTED:DIV_READY:DIV_ENABLED                      eda8 1 06 3    @red @white
DIV:DIV_REQUESTED:DIV_READY:DIV_ENABLED_PRX                  eda8 1 06 4    @red @white

DIV:DIV_REQUESTED:DIV_PAUSE:DIV_OFF                          eda8 1 07 0    @red @white
DIV:DIV_REQUESTED:DIV_PAUSE:DIV_REQUESTED                    eda8 1 07 1    @red @white
DIV:DIV_REQUESTED:DIV_PAUSE:DIV_INITING                      eda8 1 07 2    @red @white
DIV:DIV_REQUESTED:DIV_PAUSE:DIV_ENABLED                      eda8 1 07 3    @red @white
DIV:DIV_REQUESTED:DIV_PAUSE:DIV_ENABLED_PRX                  eda8 1 07 4    @red @white

DIV:DIV_REQUESTED:DIV_FING_START:DIV_OFF                     eda8 1 08 0    @red @white
DIV:DIV_REQUESTED:DIV_FING_START:DIV_REQUESTED               eda8 1 08 1    @red @white
DIV:DIV_REQUESTED:DIV_FING_START:DIV_INITING                 eda8 1 08 2    @red @white
DIV:DIV_REQUESTED:DIV_FING_START:DIV_ENABLED                 eda8 1 08 3    @red @white
DIV:DIV_REQUESTED:DIV_FING_START:DIV_ENABLED_PRX             eda8 1 08 4    @red @white

DIV:DIV_REQUESTED:DIV_RELEASE:DIV_OFF                        eda8 1 09 0    @red @white
DIV:DIV_REQUESTED:DIV_RELEASE:DIV_REQUESTED                  eda8 1 09 1    @red @white
DIV:DIV_REQUESTED:DIV_RELEASE:DIV_INITING                    eda8 1 09 2    @red @white
DIV:DIV_REQUESTED:DIV_RELEASE:DIV_ENABLED                    eda8 1 09 3    @red @white
DIV:DIV_REQUESTED:DIV_RELEASE:DIV_ENABLED_PRX                eda8 1 09 4    @red @white

DIV:DIV_INITING:DIV_REQUEST:DIV_OFF                          eda8 2 00 0    @red @white
DIV:DIV_INITING:DIV_REQUEST:DIV_REQUESTED                    eda8 2 00 1    @red @white
DIV:DIV_INITING:DIV_REQUEST:DIV_INITING                      eda8 2 00 2    @red @white
DIV:DIV_INITING:DIV_REQUEST:DIV_ENABLED                      eda8 2 00 3    @red @white
DIV:DIV_INITING:DIV_REQUEST:DIV_ENABLED_PRX                  eda8 2 00 4    @red @white

DIV:DIV_INITING:DIV_STOP:DIV_OFF                             eda8 2 01 0    @red @white
DIV:DIV_INITING:DIV_STOP:DIV_REQUESTED                       eda8 2 01 1    @red @white
DIV:DIV_INITING:DIV_STOP:DIV_INITING                         eda8 2 01 2    @red @white
DIV:DIV_INITING:DIV_STOP:DIV_ENABLED                         eda8 2 01 3    @red @white
DIV:DIV_INITING:DIV_STOP:DIV_ENABLED_PRX                     eda8 2 01 4    @red @white

DIV:DIV_INITING:DIV_AGC_CK:DIV_OFF                           eda8 2 02 0    @red @white
DIV:DIV_INITING:DIV_AGC_CK:DIV_REQUESTED                     eda8 2 02 1    @red @white
DIV:DIV_INITING:DIV_AGC_CK:DIV_INITING                       eda8 2 02 2    @red @white
DIV:DIV_INITING:DIV_AGC_CK:DIV_ENABLED                       eda8 2 02 3    @red @white
DIV:DIV_INITING:DIV_AGC_CK:DIV_ENABLED_PRX                   eda8 2 02 4    @red @white

DIV:DIV_INITING:RX_CH_DISABLE_DIV_COMP_DONE:DIV_OFF          eda8 2 03 0    @red @white
DIV:DIV_INITING:RX_CH_DISABLE_DIV_COMP_DONE:DIV_REQUESTED    eda8 2 03 1    @red @white
DIV:DIV_INITING:RX_CH_DISABLE_DIV_COMP_DONE:DIV_INITING      eda8 2 03 2    @red @white
DIV:DIV_INITING:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED      eda8 2 03 3    @red @white
DIV:DIV_INITING:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED_PRX  eda8 2 03 4    @red @white

DIV:DIV_INITING:DIV_UPDATE_PRIORITY:DIV_OFF                  eda8 2 04 0    @red @white
DIV:DIV_INITING:DIV_UPDATE_PRIORITY:DIV_REQUESTED            eda8 2 04 1    @red @white
DIV:DIV_INITING:DIV_UPDATE_PRIORITY:DIV_INITING              eda8 2 04 2    @red @white
DIV:DIV_INITING:DIV_UPDATE_PRIORITY:DIV_ENABLED              eda8 2 04 3    @red @white
DIV:DIV_INITING:DIV_UPDATE_PRIORITY:DIV_ENABLED_PRX          eda8 2 04 4    @red @white

DIV:DIV_INITING:DIV_INIT:DIV_OFF                             eda8 2 05 0    @red @white
DIV:DIV_INITING:DIV_INIT:DIV_REQUESTED                       eda8 2 05 1    @red @white
DIV:DIV_INITING:DIV_INIT:DIV_INITING                         eda8 2 05 2    @red @white
DIV:DIV_INITING:DIV_INIT:DIV_ENABLED                         eda8 2 05 3    @red @white
DIV:DIV_INITING:DIV_INIT:DIV_ENABLED_PRX                     eda8 2 05 4    @red @white

DIV:DIV_INITING:DIV_READY:DIV_OFF                            eda8 2 06 0    @red @white
DIV:DIV_INITING:DIV_READY:DIV_REQUESTED                      eda8 2 06 1    @red @white
DIV:DIV_INITING:DIV_READY:DIV_INITING                        eda8 2 06 2    @red @white
DIV:DIV_INITING:DIV_READY:DIV_ENABLED                        eda8 2 06 3    @red @white
DIV:DIV_INITING:DIV_READY:DIV_ENABLED_PRX                    eda8 2 06 4    @red @white

DIV:DIV_INITING:DIV_PAUSE:DIV_OFF                            eda8 2 07 0    @red @white
DIV:DIV_INITING:DIV_PAUSE:DIV_REQUESTED                      eda8 2 07 1    @red @white
DIV:DIV_INITING:DIV_PAUSE:DIV_INITING                        eda8 2 07 2    @red @white
DIV:DIV_INITING:DIV_PAUSE:DIV_ENABLED                        eda8 2 07 3    @red @white
DIV:DIV_INITING:DIV_PAUSE:DIV_ENABLED_PRX                    eda8 2 07 4    @red @white

DIV:DIV_INITING:DIV_FING_START:DIV_OFF                       eda8 2 08 0    @red @white
DIV:DIV_INITING:DIV_FING_START:DIV_REQUESTED                 eda8 2 08 1    @red @white
DIV:DIV_INITING:DIV_FING_START:DIV_INITING                   eda8 2 08 2    @red @white
DIV:DIV_INITING:DIV_FING_START:DIV_ENABLED                   eda8 2 08 3    @red @white
DIV:DIV_INITING:DIV_FING_START:DIV_ENABLED_PRX               eda8 2 08 4    @red @white

DIV:DIV_INITING:DIV_RELEASE:DIV_OFF                          eda8 2 09 0    @red @white
DIV:DIV_INITING:DIV_RELEASE:DIV_REQUESTED                    eda8 2 09 1    @red @white
DIV:DIV_INITING:DIV_RELEASE:DIV_INITING                      eda8 2 09 2    @red @white
DIV:DIV_INITING:DIV_RELEASE:DIV_ENABLED                      eda8 2 09 3    @red @white
DIV:DIV_INITING:DIV_RELEASE:DIV_ENABLED_PRX                  eda8 2 09 4    @red @white

DIV:DIV_ENABLED:DIV_REQUEST:DIV_OFF                          eda8 3 00 0    @red @white
DIV:DIV_ENABLED:DIV_REQUEST:DIV_REQUESTED                    eda8 3 00 1    @red @white
DIV:DIV_ENABLED:DIV_REQUEST:DIV_INITING                      eda8 3 00 2    @red @white
DIV:DIV_ENABLED:DIV_REQUEST:DIV_ENABLED                      eda8 3 00 3    @red @white
DIV:DIV_ENABLED:DIV_REQUEST:DIV_ENABLED_PRX                  eda8 3 00 4    @red @white

DIV:DIV_ENABLED:DIV_STOP:DIV_OFF                             eda8 3 01 0    @red @white
DIV:DIV_ENABLED:DIV_STOP:DIV_REQUESTED                       eda8 3 01 1    @red @white
DIV:DIV_ENABLED:DIV_STOP:DIV_INITING                         eda8 3 01 2    @red @white
DIV:DIV_ENABLED:DIV_STOP:DIV_ENABLED                         eda8 3 01 3    @red @white
DIV:DIV_ENABLED:DIV_STOP:DIV_ENABLED_PRX                     eda8 3 01 4    @red @white

DIV:DIV_ENABLED:DIV_AGC_CK:DIV_OFF                           eda8 3 02 0    @red @white
DIV:DIV_ENABLED:DIV_AGC_CK:DIV_REQUESTED                     eda8 3 02 1    @red @white
DIV:DIV_ENABLED:DIV_AGC_CK:DIV_INITING                       eda8 3 02 2    @red @white
DIV:DIV_ENABLED:DIV_AGC_CK:DIV_ENABLED                       eda8 3 02 3    @red @white
DIV:DIV_ENABLED:DIV_AGC_CK:DIV_ENABLED_PRX                   eda8 3 02 4    @red @white

DIV:DIV_ENABLED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_OFF          eda8 3 03 0    @red @white
DIV:DIV_ENABLED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_REQUESTED    eda8 3 03 1    @red @white
DIV:DIV_ENABLED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_INITING      eda8 3 03 2    @red @white
DIV:DIV_ENABLED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED      eda8 3 03 3    @red @white
DIV:DIV_ENABLED:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED_PRX  eda8 3 03 4    @red @white

DIV:DIV_ENABLED:DIV_UPDATE_PRIORITY:DIV_OFF                  eda8 3 04 0    @red @white
DIV:DIV_ENABLED:DIV_UPDATE_PRIORITY:DIV_REQUESTED            eda8 3 04 1    @red @white
DIV:DIV_ENABLED:DIV_UPDATE_PRIORITY:DIV_INITING              eda8 3 04 2    @red @white
DIV:DIV_ENABLED:DIV_UPDATE_PRIORITY:DIV_ENABLED              eda8 3 04 3    @red @white
DIV:DIV_ENABLED:DIV_UPDATE_PRIORITY:DIV_ENABLED_PRX          eda8 3 04 4    @red @white

DIV:DIV_ENABLED:DIV_INIT:DIV_OFF                             eda8 3 05 0    @red @white
DIV:DIV_ENABLED:DIV_INIT:DIV_REQUESTED                       eda8 3 05 1    @red @white
DIV:DIV_ENABLED:DIV_INIT:DIV_INITING                         eda8 3 05 2    @red @white
DIV:DIV_ENABLED:DIV_INIT:DIV_ENABLED                         eda8 3 05 3    @red @white
DIV:DIV_ENABLED:DIV_INIT:DIV_ENABLED_PRX                     eda8 3 05 4    @red @white

DIV:DIV_ENABLED:DIV_READY:DIV_OFF                            eda8 3 06 0    @red @white
DIV:DIV_ENABLED:DIV_READY:DIV_REQUESTED                      eda8 3 06 1    @red @white
DIV:DIV_ENABLED:DIV_READY:DIV_INITING                        eda8 3 06 2    @red @white
DIV:DIV_ENABLED:DIV_READY:DIV_ENABLED                        eda8 3 06 3    @red @white
DIV:DIV_ENABLED:DIV_READY:DIV_ENABLED_PRX                    eda8 3 06 4    @red @white

DIV:DIV_ENABLED:DIV_PAUSE:DIV_OFF                            eda8 3 07 0    @red @white
DIV:DIV_ENABLED:DIV_PAUSE:DIV_REQUESTED                      eda8 3 07 1    @red @white
DIV:DIV_ENABLED:DIV_PAUSE:DIV_INITING                        eda8 3 07 2    @red @white
DIV:DIV_ENABLED:DIV_PAUSE:DIV_ENABLED                        eda8 3 07 3    @red @white
DIV:DIV_ENABLED:DIV_PAUSE:DIV_ENABLED_PRX                    eda8 3 07 4    @red @white

DIV:DIV_ENABLED:DIV_FING_START:DIV_OFF                       eda8 3 08 0    @red @white
DIV:DIV_ENABLED:DIV_FING_START:DIV_REQUESTED                 eda8 3 08 1    @red @white
DIV:DIV_ENABLED:DIV_FING_START:DIV_INITING                   eda8 3 08 2    @red @white
DIV:DIV_ENABLED:DIV_FING_START:DIV_ENABLED                   eda8 3 08 3    @red @white
DIV:DIV_ENABLED:DIV_FING_START:DIV_ENABLED_PRX               eda8 3 08 4    @red @white

DIV:DIV_ENABLED:DIV_RELEASE:DIV_OFF                          eda8 3 09 0    @red @white
DIV:DIV_ENABLED:DIV_RELEASE:DIV_REQUESTED                    eda8 3 09 1    @red @white
DIV:DIV_ENABLED:DIV_RELEASE:DIV_INITING                      eda8 3 09 2    @red @white
DIV:DIV_ENABLED:DIV_RELEASE:DIV_ENABLED                      eda8 3 09 3    @red @white
DIV:DIV_ENABLED:DIV_RELEASE:DIV_ENABLED_PRX                  eda8 3 09 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_REQUEST:DIV_OFF                      eda8 4 00 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_REQUEST:DIV_REQUESTED                eda8 4 00 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_REQUEST:DIV_INITING                  eda8 4 00 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_REQUEST:DIV_ENABLED                  eda8 4 00 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_REQUEST:DIV_ENABLED_PRX              eda8 4 00 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_STOP:DIV_OFF                         eda8 4 01 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_STOP:DIV_REQUESTED                   eda8 4 01 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_STOP:DIV_INITING                     eda8 4 01 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_STOP:DIV_ENABLED                     eda8 4 01 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_STOP:DIV_ENABLED_PRX                 eda8 4 01 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_AGC_CK:DIV_OFF                       eda8 4 02 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_AGC_CK:DIV_REQUESTED                 eda8 4 02 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_AGC_CK:DIV_INITING                   eda8 4 02 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_AGC_CK:DIV_ENABLED                   eda8 4 02 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_AGC_CK:DIV_ENABLED_PRX               eda8 4 02 4    @red @white

DIV:DIV_ENABLED_PRX:RX_CH_DISABLE_DIV_COMP_DONE:DIV_OFF      eda8 4 03 0    @red @white
DIV:DIV_ENABLED_PRX:RX_CH_DISABLE_DIV_COMP_DONE:DIV_REQUESTED eda8 4 03 1    @red @white
DIV:DIV_ENABLED_PRX:RX_CH_DISABLE_DIV_COMP_DONE:DIV_INITING  eda8 4 03 2    @red @white
DIV:DIV_ENABLED_PRX:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED  eda8 4 03 3    @red @white
DIV:DIV_ENABLED_PRX:RX_CH_DISABLE_DIV_COMP_DONE:DIV_ENABLED_PRX eda8 4 03 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_UPDATE_PRIORITY:DIV_OFF              eda8 4 04 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_UPDATE_PRIORITY:DIV_REQUESTED        eda8 4 04 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_UPDATE_PRIORITY:DIV_INITING          eda8 4 04 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_UPDATE_PRIORITY:DIV_ENABLED          eda8 4 04 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_UPDATE_PRIORITY:DIV_ENABLED_PRX      eda8 4 04 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_INIT:DIV_OFF                         eda8 4 05 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_INIT:DIV_REQUESTED                   eda8 4 05 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_INIT:DIV_INITING                     eda8 4 05 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_INIT:DIV_ENABLED                     eda8 4 05 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_INIT:DIV_ENABLED_PRX                 eda8 4 05 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_READY:DIV_OFF                        eda8 4 06 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_READY:DIV_REQUESTED                  eda8 4 06 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_READY:DIV_INITING                    eda8 4 06 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_READY:DIV_ENABLED                    eda8 4 06 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_READY:DIV_ENABLED_PRX                eda8 4 06 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_PAUSE:DIV_OFF                        eda8 4 07 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_PAUSE:DIV_REQUESTED                  eda8 4 07 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_PAUSE:DIV_INITING                    eda8 4 07 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_PAUSE:DIV_ENABLED                    eda8 4 07 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_PAUSE:DIV_ENABLED_PRX                eda8 4 07 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_FING_START:DIV_OFF                   eda8 4 08 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_FING_START:DIV_REQUESTED             eda8 4 08 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_FING_START:DIV_INITING               eda8 4 08 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_FING_START:DIV_ENABLED               eda8 4 08 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_FING_START:DIV_ENABLED_PRX           eda8 4 08 4    @red @white

DIV:DIV_ENABLED_PRX:DIV_RELEASE:DIV_OFF                      eda8 4 09 0    @red @white
DIV:DIV_ENABLED_PRX:DIV_RELEASE:DIV_REQUESTED                eda8 4 09 1    @red @white
DIV:DIV_ENABLED_PRX:DIV_RELEASE:DIV_INITING                  eda8 4 09 2    @red @white
DIV:DIV_ENABLED_PRX:DIV_RELEASE:DIV_ENABLED                  eda8 4 09 3    @red @white
DIV:DIV_ENABLED_PRX:DIV_RELEASE:DIV_ENABLED_PRX              eda8 4 09 4    @red @white



# End machine generated TLA code for state machine: DIV_SM

