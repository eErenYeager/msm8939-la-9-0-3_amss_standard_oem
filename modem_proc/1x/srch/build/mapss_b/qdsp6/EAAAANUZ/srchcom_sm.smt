/*=============================================================================

  srchcom_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchcom_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHCOM_SM */

/* Transition function prototypes */
static stm_state_type com_process_ext_random_bits(void *);
static stm_state_type com_process_int_random_bits(void *);
static stm_state_type com_process_tune_done_cmd(void *);
static stm_state_type com_process_rx_tune_cmd(void *);
static stm_state_type com_process_peak_cmd(void *);
static stm_state_type com_process_request_system_restart_cmd(void *);
static stm_state_type com_process_lost_rf_resource_cmd(void *);
static stm_state_type com_process_sys_meas_cmd(void *);
static stm_state_type com_process_request_1x_ppm_op_cmd(void *);
static stm_state_type com_process_release_1x_ppm_op_cmd(void *);
static stm_state_type com_process_request_sync80_data_cmd(void *);
static stm_state_type com_process_get_srch_window_center_cmd(void *);
static stm_state_type com_process_get_ref_pn_cmd(void *);
static stm_state_type com_process_get_act_cand_sets_cmd(void *);
static stm_state_type com_process_enable_ttd_recording_cmd(void *);
static stm_state_type com_process_disable_ttd_recording_cmd(void *);
static stm_state_type com_process_request_systime_unc_cmd(void *);
static stm_state_type com_process_request_ttd_cmd(void *);
static stm_state_type com_process_cancel_ttd_request_cmd(void *);
static stm_state_type com_process_send_ppm_list_update_cmd(void *);
static stm_state_type com_process_request_rf_info_cmd(void *);
static stm_state_type com_process_msgr_srch4_msg_cmd(void *);
static stm_state_type com_process_msgr_1xdemod_msg_cmd(void *);
static stm_state_type com_process_ftm_init_cmd(void *);
static stm_state_type com_process_deactivate_cmd(void *);
static stm_state_type com_process_lte_nset_update_cmd(void *);
static stm_state_type com_process_asd_antenna_switch_cmd(void *);
static stm_state_type com_process_asd_rf_set_ant_cmd(void *);
static stm_state_type com_process_asd_clear_traffic_data_cmd(void *);


/* State Machine entry/exit function prototypes */


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define SRCHCOM_SM_NUMBER_OF_STATES 1
#define SRCHCOM_SM_NUMBER_OF_INPUTS 29


/* State enumeration */
enum
{
  MAIN,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHCOM_SM_states[ SRCHCOM_SM_NUMBER_OF_STATES ] =
{
  {"MAIN", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHCOM_SM_inputs[ SRCHCOM_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_RAND_BITS_F, "SRCH_RAND_BITS_F"},
  {(stm_input_type)RAND_BITS_CMD, "RAND_BITS_CMD"},
  {(stm_input_type)TUNE_DONE_CMD, "TUNE_DONE_CMD"},
  {(stm_input_type)RX_TUNE_CMD, "RX_TUNE_CMD"},
  {(stm_input_type)SRCH_PEAK_CMD, "SRCH_PEAK_CMD"},
  {(stm_input_type)REQUEST_SYSTEM_RESTART_CMD, "REQUEST_SYSTEM_RESTART_CMD"},
  {(stm_input_type)LOST_RF_RESOURCE_CMD, "LOST_RF_RESOURCE_CMD"},
  {(stm_input_type)SRCH_SYS_MEAS_F, "SRCH_SYS_MEAS_F"},
  {(stm_input_type)REQUEST_1X_PPM_OP_CMD, "REQUEST_1X_PPM_OP_CMD"},
  {(stm_input_type)RELEASE_1X_PPM_OP_CMD, "RELEASE_1X_PPM_OP_CMD"},
  {(stm_input_type)REQUEST_1X_SYNC80_DATA_CMD, "REQUEST_1X_SYNC80_DATA_CMD"},
  {(stm_input_type)GET_SRCH_WINDOW_CENTER_CMD, "GET_SRCH_WINDOW_CENTER_CMD"},
  {(stm_input_type)GET_REF_PN_CMD, "GET_REF_PN_CMD"},
  {(stm_input_type)GET_ACT_CAND_SETS_CMD, "GET_ACT_CAND_SETS_CMD"},
  {(stm_input_type)ENABLE_TTD_RECORDING_CMD, "ENABLE_TTD_RECORDING_CMD"},
  {(stm_input_type)DISABLE_TTD_RECORDING_CMD, "DISABLE_TTD_RECORDING_CMD"},
  {(stm_input_type)REQUEST_SYSTIME_UNC_CMD, "REQUEST_SYSTIME_UNC_CMD"},
  {(stm_input_type)REQUEST_TTD_CMD, "REQUEST_TTD_CMD"},
  {(stm_input_type)CANCEL_TTD_REQUEST_CMD, "CANCEL_TTD_REQUEST_CMD"},
  {(stm_input_type)SEND_PPM_LIST_UPDATE_CMD, "SEND_PPM_LIST_UPDATE_CMD"},
  {(stm_input_type)REQUEST_RF_INFO_CMD, "REQUEST_RF_INFO_CMD"},
  {(stm_input_type)MSGR_SRCH4_MSG_CMD, "MSGR_SRCH4_MSG_CMD"},
  {(stm_input_type)MSGR_1XDEMOD_MSG_CMD, "MSGR_1XDEMOD_MSG_CMD"},
  {(stm_input_type)IRAT_FTM_INIT_CMD, "IRAT_FTM_INIT_CMD"},
  {(stm_input_type)SRCH_DEACTIVATE_F, "SRCH_DEACTIVATE_F"},
  {(stm_input_type)SRCH_LTE_NSET_UPDATE_PARAMS_F, "SRCH_LTE_NSET_UPDATE_PARAMS_F"},
  {(stm_input_type)SRCH_ASD_ANTENNA_SWITCH_CMD, "SRCH_ASD_ANTENNA_SWITCH_CMD"},
  {(stm_input_type)SRCH_ASD_RF_SET_ANT_CMD, "SRCH_ASD_RF_SET_ANT_CMD"},
  {(stm_input_type)SRCH_ASD_CLEAR_TRAFFIC_DATA_CMD, "SRCH_ASD_CLEAR_TRAFFIC_DATA_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHCOM_SM_transitions[ SRCHCOM_SM_NUMBER_OF_STATES *  SRCHCOM_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state MAIN */
    com_process_ext_random_bits,    /* SRCH_RAND_BITS_F */
    com_process_int_random_bits,    /* RAND_BITS_CMD */
    com_process_tune_done_cmd,    /* TUNE_DONE_CMD */
    com_process_rx_tune_cmd,    /* RX_TUNE_CMD */
    com_process_peak_cmd,    /* SRCH_PEAK_CMD */
    com_process_request_system_restart_cmd,    /* REQUEST_SYSTEM_RESTART_CMD */
    com_process_lost_rf_resource_cmd,    /* LOST_RF_RESOURCE_CMD */
    com_process_sys_meas_cmd,    /* SRCH_SYS_MEAS_F */
    com_process_request_1x_ppm_op_cmd,    /* REQUEST_1X_PPM_OP_CMD */
    com_process_release_1x_ppm_op_cmd,    /* RELEASE_1X_PPM_OP_CMD */
    com_process_request_sync80_data_cmd,    /* REQUEST_1X_SYNC80_DATA_CMD */
    com_process_get_srch_window_center_cmd,    /* GET_SRCH_WINDOW_CENTER_CMD */
    com_process_get_ref_pn_cmd,    /* GET_REF_PN_CMD */
    com_process_get_act_cand_sets_cmd,    /* GET_ACT_CAND_SETS_CMD */
    com_process_enable_ttd_recording_cmd,    /* ENABLE_TTD_RECORDING_CMD */
    com_process_disable_ttd_recording_cmd,    /* DISABLE_TTD_RECORDING_CMD */
    com_process_request_systime_unc_cmd,    /* REQUEST_SYSTIME_UNC_CMD */
    com_process_request_ttd_cmd,    /* REQUEST_TTD_CMD */
    com_process_cancel_ttd_request_cmd,    /* CANCEL_TTD_REQUEST_CMD */
    com_process_send_ppm_list_update_cmd,    /* SEND_PPM_LIST_UPDATE_CMD */
    com_process_request_rf_info_cmd,    /* REQUEST_RF_INFO_CMD */
    com_process_msgr_srch4_msg_cmd,    /* MSGR_SRCH4_MSG_CMD */
    com_process_msgr_1xdemod_msg_cmd,    /* MSGR_1XDEMOD_MSG_CMD */
    com_process_ftm_init_cmd,    /* IRAT_FTM_INIT_CMD */
    com_process_deactivate_cmd,    /* SRCH_DEACTIVATE_F */
    com_process_lte_nset_update_cmd,    /* SRCH_LTE_NSET_UPDATE_PARAMS_F */
    com_process_asd_antenna_switch_cmd,    /* SRCH_ASD_ANTENNA_SWITCH_CMD */
    com_process_asd_rf_set_ant_cmd,    /* SRCH_ASD_RF_SET_ANT_CMD */
    com_process_asd_clear_traffic_data_cmd,    /* SRCH_ASD_CLEAR_TRAFFIC_DATA_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHCOM_SM =
{
  "SRCHCOM_SM", /* state machine name */
  31048, /* unique SM id (hash of name) */
  NULL, /* state machine entry function */
  NULL, /* state machine exit function */
  MAIN, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SRCHCOM_SM_NUMBER_OF_STATES,
  SRCHCOM_SM_NUMBER_OF_INPUTS,
  SRCHCOM_SM_states,
  SRCHCOM_SM_inputs,
  SRCHCOM_SM_transitions,
};

/* End machine generated code for state machine: SRCHCOM_SM */
/* Begin machine generated code for state machine group: SRCH_COMMON_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean com_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_COMMON_GROUP_sm_list[ 8 ] =
{
  &SRCHCOM_SM,
  &DIV_SM,
  &DIV_AUTO_SM,
  &AFC_SM,
  &SYS_MEAS_SM,
  &SRCH_RX_SM_P,
  &SRCH_RX_SM_S,
  &TRAM_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_COMMON_GROUP_sig_list[ 3 ] =
{
  SRCH_CMD_Q_SIG,
  SRCH_MSGR_Q_SIG,
  TSYNC_QMI_SIG,
};


/* State machine group definition */
stm_group_type SRCH_COMMON_GROUP =
{
  "SRCH_COMMON_GROUP", /* state machine group name */
  SRCH_COMMON_GROUP_sig_list, /* signal mapping table */
  3, /* number of signals in mapping table */
  com_sig_mapper, /* signal mapper function */
  SRCH_COMMON_GROUP_sm_list, /* state machine table */
  8, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  NULL, /* global group */
  srch_gps_sigs, /* user signal retrieval fn */
  srch_gps_handler /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_COMMON_GROUP */

