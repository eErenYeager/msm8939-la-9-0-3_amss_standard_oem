###############################################################################
#
#    srchzz_is2000_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_is2000_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_IS2000_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_IS2000:INIT:START_SLEEP:INIT                          55fe 0 00 0    @olive @white
SRCHZZ_IS2000:INIT:START_SLEEP:SLEEP                         55fe 0 00 1    @olive @white
SRCHZZ_IS2000:INIT:START_SLEEP:WAKEUP                        55fe 0 00 2    @olive @white
SRCHZZ_IS2000:INIT:START_SLEEP:PREP_SB                       55fe 0 00 3    @olive @white
SRCHZZ_IS2000:INIT:START_SLEEP:WAIT_SB                       55fe 0 00 4    @olive @white

SRCHZZ_IS2000:INIT:XFER_FROM_QPCH:INIT                       55fe 0 01 0    @olive @white
SRCHZZ_IS2000:INIT:XFER_FROM_QPCH:SLEEP                      55fe 0 01 1    @olive @white
SRCHZZ_IS2000:INIT:XFER_FROM_QPCH:WAKEUP                     55fe 0 01 2    @olive @white
SRCHZZ_IS2000:INIT:XFER_FROM_QPCH:PREP_SB                    55fe 0 01 3    @olive @white
SRCHZZ_IS2000:INIT:XFER_FROM_QPCH:WAIT_SB                    55fe 0 01 4    @olive @white

SRCHZZ_IS2000:INIT:ROLL:INIT                                 55fe 0 02 0    @olive @white
SRCHZZ_IS2000:INIT:ROLL:SLEEP                                55fe 0 02 1    @olive @white
SRCHZZ_IS2000:INIT:ROLL:WAKEUP                               55fe 0 02 2    @olive @white
SRCHZZ_IS2000:INIT:ROLL:PREP_SB                              55fe 0 02 3    @olive @white
SRCHZZ_IS2000:INIT:ROLL:WAIT_SB                              55fe 0 02 4    @olive @white

SRCHZZ_IS2000:INIT:ADJUST_TIMING:INIT                        55fe 0 03 0    @olive @white
SRCHZZ_IS2000:INIT:ADJUST_TIMING:SLEEP                       55fe 0 03 1    @olive @white
SRCHZZ_IS2000:INIT:ADJUST_TIMING:WAKEUP                      55fe 0 03 2    @olive @white
SRCHZZ_IS2000:INIT:ADJUST_TIMING:PREP_SB                     55fe 0 03 3    @olive @white
SRCHZZ_IS2000:INIT:ADJUST_TIMING:WAIT_SB                     55fe 0 03 4    @olive @white

SRCHZZ_IS2000:INIT:WAKEUP_NOW:INIT                           55fe 0 04 0    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_NOW:SLEEP                          55fe 0 04 1    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_NOW:WAKEUP                         55fe 0 04 2    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_NOW:PREP_SB                        55fe 0 04 3    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_NOW:WAIT_SB                        55fe 0 04 4    @olive @white

SRCHZZ_IS2000:INIT:FREQ_TRACK_OFF_DONE:INIT                  55fe 0 05 0    @olive @white
SRCHZZ_IS2000:INIT:FREQ_TRACK_OFF_DONE:SLEEP                 55fe 0 05 1    @olive @white
SRCHZZ_IS2000:INIT:FREQ_TRACK_OFF_DONE:WAKEUP                55fe 0 05 2    @olive @white
SRCHZZ_IS2000:INIT:FREQ_TRACK_OFF_DONE:PREP_SB               55fe 0 05 3    @olive @white
SRCHZZ_IS2000:INIT:FREQ_TRACK_OFF_DONE:WAIT_SB               55fe 0 05 4    @olive @white

SRCHZZ_IS2000:INIT:RX_RF_DISABLED:INIT                       55fe 0 06 0    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLED:SLEEP                      55fe 0 06 1    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLED:WAKEUP                     55fe 0 06 2    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLED:PREP_SB                    55fe 0 06 3    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLED:WAIT_SB                    55fe 0 06 4    @olive @white

SRCHZZ_IS2000:INIT:GO_TO_SLEEP:INIT                          55fe 0 07 0    @olive @white
SRCHZZ_IS2000:INIT:GO_TO_SLEEP:SLEEP                         55fe 0 07 1    @olive @white
SRCHZZ_IS2000:INIT:GO_TO_SLEEP:WAKEUP                        55fe 0 07 2    @olive @white
SRCHZZ_IS2000:INIT:GO_TO_SLEEP:PREP_SB                       55fe 0 07 3    @olive @white
SRCHZZ_IS2000:INIT:GO_TO_SLEEP:WAIT_SB                       55fe 0 07 4    @olive @white

SRCHZZ_IS2000:INIT:RX_RF_DISABLE_COMP:INIT                   55fe 0 08 0    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLE_COMP:SLEEP                  55fe 0 08 1    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLE_COMP:WAKEUP                 55fe 0 08 2    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLE_COMP:PREP_SB                55fe 0 08 3    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_DISABLE_COMP:WAIT_SB                55fe 0 08 4    @olive @white

SRCHZZ_IS2000:INIT:RX_CH_GRANTED:INIT                        55fe 0 09 0    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_GRANTED:SLEEP                       55fe 0 09 1    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_GRANTED:WAKEUP                      55fe 0 09 2    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_GRANTED:PREP_SB                     55fe 0 09 3    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_GRANTED:WAIT_SB                     55fe 0 09 4    @olive @white

SRCHZZ_IS2000:INIT:RX_CH_DENIED:INIT                         55fe 0 0a 0    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_DENIED:SLEEP                        55fe 0 0a 1    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_DENIED:WAKEUP                       55fe 0 0a 2    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_DENIED:PREP_SB                      55fe 0 0a 3    @olive @white
SRCHZZ_IS2000:INIT:RX_CH_DENIED:WAIT_SB                      55fe 0 0a 4    @olive @white

SRCHZZ_IS2000:INIT:WAKEUP:INIT                               55fe 0 0b 0    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP:SLEEP                              55fe 0 0b 1    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP:WAKEUP                             55fe 0 0b 2    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP:PREP_SB                            55fe 0 0b 3    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP:WAIT_SB                            55fe 0 0b 4    @olive @white

SRCHZZ_IS2000:INIT:RX_RF_RSP_TUNE_COMP:INIT                  55fe 0 0c 0    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_RSP_TUNE_COMP:SLEEP                 55fe 0 0c 1    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_RSP_TUNE_COMP:WAKEUP                55fe 0 0c 2    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_RSP_TUNE_COMP:PREP_SB               55fe 0 0c 3    @olive @white
SRCHZZ_IS2000:INIT:RX_RF_RSP_TUNE_COMP:WAIT_SB               55fe 0 0c 4    @olive @white

SRCHZZ_IS2000:INIT:NO_RF_LOCK:INIT                           55fe 0 0d 0    @olive @white
SRCHZZ_IS2000:INIT:NO_RF_LOCK:SLEEP                          55fe 0 0d 1    @olive @white
SRCHZZ_IS2000:INIT:NO_RF_LOCK:WAKEUP                         55fe 0 0d 2    @olive @white
SRCHZZ_IS2000:INIT:NO_RF_LOCK:PREP_SB                        55fe 0 0d 3    @olive @white
SRCHZZ_IS2000:INIT:NO_RF_LOCK:WAIT_SB                        55fe 0 0d 4    @olive @white

SRCHZZ_IS2000:INIT:CX8_ON:INIT                               55fe 0 0e 0    @olive @white
SRCHZZ_IS2000:INIT:CX8_ON:SLEEP                              55fe 0 0e 1    @olive @white
SRCHZZ_IS2000:INIT:CX8_ON:WAKEUP                             55fe 0 0e 2    @olive @white
SRCHZZ_IS2000:INIT:CX8_ON:PREP_SB                            55fe 0 0e 3    @olive @white
SRCHZZ_IS2000:INIT:CX8_ON:WAIT_SB                            55fe 0 0e 4    @olive @white

SRCHZZ_IS2000:INIT:WAKEUP_UPDATE:INIT                        55fe 0 0f 0    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_UPDATE:SLEEP                       55fe 0 0f 1    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_UPDATE:WAKEUP                      55fe 0 0f 2    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_UPDATE:PREP_SB                     55fe 0 0f 3    @olive @white
SRCHZZ_IS2000:INIT:WAKEUP_UPDATE:WAIT_SB                     55fe 0 0f 4    @olive @white

SRCHZZ_IS2000:INIT:RX_TUNE_COMP:INIT                         55fe 0 10 0    @olive @white
SRCHZZ_IS2000:INIT:RX_TUNE_COMP:SLEEP                        55fe 0 10 1    @olive @white
SRCHZZ_IS2000:INIT:RX_TUNE_COMP:WAKEUP                       55fe 0 10 2    @olive @white
SRCHZZ_IS2000:INIT:RX_TUNE_COMP:PREP_SB                      55fe 0 10 3    @olive @white
SRCHZZ_IS2000:INIT:RX_TUNE_COMP:WAIT_SB                      55fe 0 10 4    @olive @white

SRCHZZ_IS2000:SLEEP:START_SLEEP:INIT                         55fe 1 00 0    @olive @white
SRCHZZ_IS2000:SLEEP:START_SLEEP:SLEEP                        55fe 1 00 1    @olive @white
SRCHZZ_IS2000:SLEEP:START_SLEEP:WAKEUP                       55fe 1 00 2    @olive @white
SRCHZZ_IS2000:SLEEP:START_SLEEP:PREP_SB                      55fe 1 00 3    @olive @white
SRCHZZ_IS2000:SLEEP:START_SLEEP:WAIT_SB                      55fe 1 00 4    @olive @white

SRCHZZ_IS2000:SLEEP:XFER_FROM_QPCH:INIT                      55fe 1 01 0    @olive @white
SRCHZZ_IS2000:SLEEP:XFER_FROM_QPCH:SLEEP                     55fe 1 01 1    @olive @white
SRCHZZ_IS2000:SLEEP:XFER_FROM_QPCH:WAKEUP                    55fe 1 01 2    @olive @white
SRCHZZ_IS2000:SLEEP:XFER_FROM_QPCH:PREP_SB                   55fe 1 01 3    @olive @white
SRCHZZ_IS2000:SLEEP:XFER_FROM_QPCH:WAIT_SB                   55fe 1 01 4    @olive @white

SRCHZZ_IS2000:SLEEP:ROLL:INIT                                55fe 1 02 0    @olive @white
SRCHZZ_IS2000:SLEEP:ROLL:SLEEP                               55fe 1 02 1    @olive @white
SRCHZZ_IS2000:SLEEP:ROLL:WAKEUP                              55fe 1 02 2    @olive @white
SRCHZZ_IS2000:SLEEP:ROLL:PREP_SB                             55fe 1 02 3    @olive @white
SRCHZZ_IS2000:SLEEP:ROLL:WAIT_SB                             55fe 1 02 4    @olive @white

SRCHZZ_IS2000:SLEEP:ADJUST_TIMING:INIT                       55fe 1 03 0    @olive @white
SRCHZZ_IS2000:SLEEP:ADJUST_TIMING:SLEEP                      55fe 1 03 1    @olive @white
SRCHZZ_IS2000:SLEEP:ADJUST_TIMING:WAKEUP                     55fe 1 03 2    @olive @white
SRCHZZ_IS2000:SLEEP:ADJUST_TIMING:PREP_SB                    55fe 1 03 3    @olive @white
SRCHZZ_IS2000:SLEEP:ADJUST_TIMING:WAIT_SB                    55fe 1 03 4    @olive @white

SRCHZZ_IS2000:SLEEP:WAKEUP_NOW:INIT                          55fe 1 04 0    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_NOW:SLEEP                         55fe 1 04 1    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_NOW:WAKEUP                        55fe 1 04 2    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_NOW:PREP_SB                       55fe 1 04 3    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_NOW:WAIT_SB                       55fe 1 04 4    @olive @white

SRCHZZ_IS2000:SLEEP:FREQ_TRACK_OFF_DONE:INIT                 55fe 1 05 0    @olive @white
SRCHZZ_IS2000:SLEEP:FREQ_TRACK_OFF_DONE:SLEEP                55fe 1 05 1    @olive @white
SRCHZZ_IS2000:SLEEP:FREQ_TRACK_OFF_DONE:WAKEUP               55fe 1 05 2    @olive @white
SRCHZZ_IS2000:SLEEP:FREQ_TRACK_OFF_DONE:PREP_SB              55fe 1 05 3    @olive @white
SRCHZZ_IS2000:SLEEP:FREQ_TRACK_OFF_DONE:WAIT_SB              55fe 1 05 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_RF_DISABLED:INIT                      55fe 1 06 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLED:SLEEP                     55fe 1 06 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLED:WAKEUP                    55fe 1 06 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLED:PREP_SB                   55fe 1 06 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLED:WAIT_SB                   55fe 1 06 4    @olive @white

SRCHZZ_IS2000:SLEEP:GO_TO_SLEEP:INIT                         55fe 1 07 0    @olive @white
SRCHZZ_IS2000:SLEEP:GO_TO_SLEEP:SLEEP                        55fe 1 07 1    @olive @white
SRCHZZ_IS2000:SLEEP:GO_TO_SLEEP:WAKEUP                       55fe 1 07 2    @olive @white
SRCHZZ_IS2000:SLEEP:GO_TO_SLEEP:PREP_SB                      55fe 1 07 3    @olive @white
SRCHZZ_IS2000:SLEEP:GO_TO_SLEEP:WAIT_SB                      55fe 1 07 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_RF_DISABLE_COMP:INIT                  55fe 1 08 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLE_COMP:SLEEP                 55fe 1 08 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLE_COMP:WAKEUP                55fe 1 08 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLE_COMP:PREP_SB               55fe 1 08 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_DISABLE_COMP:WAIT_SB               55fe 1 08 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_CH_GRANTED:INIT                       55fe 1 09 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_GRANTED:SLEEP                      55fe 1 09 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_GRANTED:WAKEUP                     55fe 1 09 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_GRANTED:PREP_SB                    55fe 1 09 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_GRANTED:WAIT_SB                    55fe 1 09 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_CH_DENIED:INIT                        55fe 1 0a 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_DENIED:SLEEP                       55fe 1 0a 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_DENIED:WAKEUP                      55fe 1 0a 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_DENIED:PREP_SB                     55fe 1 0a 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_CH_DENIED:WAIT_SB                     55fe 1 0a 4    @olive @white

SRCHZZ_IS2000:SLEEP:WAKEUP:INIT                              55fe 1 0b 0    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP:SLEEP                             55fe 1 0b 1    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP:WAKEUP                            55fe 1 0b 2    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP:PREP_SB                           55fe 1 0b 3    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP:WAIT_SB                           55fe 1 0b 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_RF_RSP_TUNE_COMP:INIT                 55fe 1 0c 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_RSP_TUNE_COMP:SLEEP                55fe 1 0c 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_RSP_TUNE_COMP:WAKEUP               55fe 1 0c 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_RSP_TUNE_COMP:PREP_SB              55fe 1 0c 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_RF_RSP_TUNE_COMP:WAIT_SB              55fe 1 0c 4    @olive @white

SRCHZZ_IS2000:SLEEP:NO_RF_LOCK:INIT                          55fe 1 0d 0    @olive @white
SRCHZZ_IS2000:SLEEP:NO_RF_LOCK:SLEEP                         55fe 1 0d 1    @olive @white
SRCHZZ_IS2000:SLEEP:NO_RF_LOCK:WAKEUP                        55fe 1 0d 2    @olive @white
SRCHZZ_IS2000:SLEEP:NO_RF_LOCK:PREP_SB                       55fe 1 0d 3    @olive @white
SRCHZZ_IS2000:SLEEP:NO_RF_LOCK:WAIT_SB                       55fe 1 0d 4    @olive @white

SRCHZZ_IS2000:SLEEP:CX8_ON:INIT                              55fe 1 0e 0    @olive @white
SRCHZZ_IS2000:SLEEP:CX8_ON:SLEEP                             55fe 1 0e 1    @olive @white
SRCHZZ_IS2000:SLEEP:CX8_ON:WAKEUP                            55fe 1 0e 2    @olive @white
SRCHZZ_IS2000:SLEEP:CX8_ON:PREP_SB                           55fe 1 0e 3    @olive @white
SRCHZZ_IS2000:SLEEP:CX8_ON:WAIT_SB                           55fe 1 0e 4    @olive @white

SRCHZZ_IS2000:SLEEP:WAKEUP_UPDATE:INIT                       55fe 1 0f 0    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_UPDATE:SLEEP                      55fe 1 0f 1    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_UPDATE:WAKEUP                     55fe 1 0f 2    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_UPDATE:PREP_SB                    55fe 1 0f 3    @olive @white
SRCHZZ_IS2000:SLEEP:WAKEUP_UPDATE:WAIT_SB                    55fe 1 0f 4    @olive @white

SRCHZZ_IS2000:SLEEP:RX_TUNE_COMP:INIT                        55fe 1 10 0    @olive @white
SRCHZZ_IS2000:SLEEP:RX_TUNE_COMP:SLEEP                       55fe 1 10 1    @olive @white
SRCHZZ_IS2000:SLEEP:RX_TUNE_COMP:WAKEUP                      55fe 1 10 2    @olive @white
SRCHZZ_IS2000:SLEEP:RX_TUNE_COMP:PREP_SB                     55fe 1 10 3    @olive @white
SRCHZZ_IS2000:SLEEP:RX_TUNE_COMP:WAIT_SB                     55fe 1 10 4    @olive @white

SRCHZZ_IS2000:WAKEUP:START_SLEEP:INIT                        55fe 2 00 0    @olive @white
SRCHZZ_IS2000:WAKEUP:START_SLEEP:SLEEP                       55fe 2 00 1    @olive @white
SRCHZZ_IS2000:WAKEUP:START_SLEEP:WAKEUP                      55fe 2 00 2    @olive @white
SRCHZZ_IS2000:WAKEUP:START_SLEEP:PREP_SB                     55fe 2 00 3    @olive @white
SRCHZZ_IS2000:WAKEUP:START_SLEEP:WAIT_SB                     55fe 2 00 4    @olive @white

SRCHZZ_IS2000:WAKEUP:XFER_FROM_QPCH:INIT                     55fe 2 01 0    @olive @white
SRCHZZ_IS2000:WAKEUP:XFER_FROM_QPCH:SLEEP                    55fe 2 01 1    @olive @white
SRCHZZ_IS2000:WAKEUP:XFER_FROM_QPCH:WAKEUP                   55fe 2 01 2    @olive @white
SRCHZZ_IS2000:WAKEUP:XFER_FROM_QPCH:PREP_SB                  55fe 2 01 3    @olive @white
SRCHZZ_IS2000:WAKEUP:XFER_FROM_QPCH:WAIT_SB                  55fe 2 01 4    @olive @white

SRCHZZ_IS2000:WAKEUP:ROLL:INIT                               55fe 2 02 0    @olive @white
SRCHZZ_IS2000:WAKEUP:ROLL:SLEEP                              55fe 2 02 1    @olive @white
SRCHZZ_IS2000:WAKEUP:ROLL:WAKEUP                             55fe 2 02 2    @olive @white
SRCHZZ_IS2000:WAKEUP:ROLL:PREP_SB                            55fe 2 02 3    @olive @white
SRCHZZ_IS2000:WAKEUP:ROLL:WAIT_SB                            55fe 2 02 4    @olive @white

SRCHZZ_IS2000:WAKEUP:ADJUST_TIMING:INIT                      55fe 2 03 0    @olive @white
SRCHZZ_IS2000:WAKEUP:ADJUST_TIMING:SLEEP                     55fe 2 03 1    @olive @white
SRCHZZ_IS2000:WAKEUP:ADJUST_TIMING:WAKEUP                    55fe 2 03 2    @olive @white
SRCHZZ_IS2000:WAKEUP:ADJUST_TIMING:PREP_SB                   55fe 2 03 3    @olive @white
SRCHZZ_IS2000:WAKEUP:ADJUST_TIMING:WAIT_SB                   55fe 2 03 4    @olive @white

SRCHZZ_IS2000:WAKEUP:WAKEUP_NOW:INIT                         55fe 2 04 0    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_NOW:SLEEP                        55fe 2 04 1    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_NOW:WAKEUP                       55fe 2 04 2    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_NOW:PREP_SB                      55fe 2 04 3    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_NOW:WAIT_SB                      55fe 2 04 4    @olive @white

SRCHZZ_IS2000:WAKEUP:FREQ_TRACK_OFF_DONE:INIT                55fe 2 05 0    @olive @white
SRCHZZ_IS2000:WAKEUP:FREQ_TRACK_OFF_DONE:SLEEP               55fe 2 05 1    @olive @white
SRCHZZ_IS2000:WAKEUP:FREQ_TRACK_OFF_DONE:WAKEUP              55fe 2 05 2    @olive @white
SRCHZZ_IS2000:WAKEUP:FREQ_TRACK_OFF_DONE:PREP_SB             55fe 2 05 3    @olive @white
SRCHZZ_IS2000:WAKEUP:FREQ_TRACK_OFF_DONE:WAIT_SB             55fe 2 05 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLED:INIT                     55fe 2 06 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLED:SLEEP                    55fe 2 06 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLED:WAKEUP                   55fe 2 06 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLED:PREP_SB                  55fe 2 06 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLED:WAIT_SB                  55fe 2 06 4    @olive @white

SRCHZZ_IS2000:WAKEUP:GO_TO_SLEEP:INIT                        55fe 2 07 0    @olive @white
SRCHZZ_IS2000:WAKEUP:GO_TO_SLEEP:SLEEP                       55fe 2 07 1    @olive @white
SRCHZZ_IS2000:WAKEUP:GO_TO_SLEEP:WAKEUP                      55fe 2 07 2    @olive @white
SRCHZZ_IS2000:WAKEUP:GO_TO_SLEEP:PREP_SB                     55fe 2 07 3    @olive @white
SRCHZZ_IS2000:WAKEUP:GO_TO_SLEEP:WAIT_SB                     55fe 2 07 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLE_COMP:INIT                 55fe 2 08 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLE_COMP:SLEEP                55fe 2 08 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLE_COMP:WAKEUP               55fe 2 08 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLE_COMP:PREP_SB              55fe 2 08 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_DISABLE_COMP:WAIT_SB              55fe 2 08 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_CH_GRANTED:INIT                      55fe 2 09 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_GRANTED:SLEEP                     55fe 2 09 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_GRANTED:WAKEUP                    55fe 2 09 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_GRANTED:PREP_SB                   55fe 2 09 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_GRANTED:WAIT_SB                   55fe 2 09 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_CH_DENIED:INIT                       55fe 2 0a 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_DENIED:SLEEP                      55fe 2 0a 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_DENIED:WAKEUP                     55fe 2 0a 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_DENIED:PREP_SB                    55fe 2 0a 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_CH_DENIED:WAIT_SB                    55fe 2 0a 4    @olive @white

SRCHZZ_IS2000:WAKEUP:WAKEUP:INIT                             55fe 2 0b 0    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP:SLEEP                            55fe 2 0b 1    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP:WAKEUP                           55fe 2 0b 2    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP:PREP_SB                          55fe 2 0b 3    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP:WAIT_SB                          55fe 2 0b 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_RF_RSP_TUNE_COMP:INIT                55fe 2 0c 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_RSP_TUNE_COMP:SLEEP               55fe 2 0c 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_RSP_TUNE_COMP:WAKEUP              55fe 2 0c 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_RSP_TUNE_COMP:PREP_SB             55fe 2 0c 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_RF_RSP_TUNE_COMP:WAIT_SB             55fe 2 0c 4    @olive @white

SRCHZZ_IS2000:WAKEUP:NO_RF_LOCK:INIT                         55fe 2 0d 0    @olive @white
SRCHZZ_IS2000:WAKEUP:NO_RF_LOCK:SLEEP                        55fe 2 0d 1    @olive @white
SRCHZZ_IS2000:WAKEUP:NO_RF_LOCK:WAKEUP                       55fe 2 0d 2    @olive @white
SRCHZZ_IS2000:WAKEUP:NO_RF_LOCK:PREP_SB                      55fe 2 0d 3    @olive @white
SRCHZZ_IS2000:WAKEUP:NO_RF_LOCK:WAIT_SB                      55fe 2 0d 4    @olive @white

SRCHZZ_IS2000:WAKEUP:CX8_ON:INIT                             55fe 2 0e 0    @olive @white
SRCHZZ_IS2000:WAKEUP:CX8_ON:SLEEP                            55fe 2 0e 1    @olive @white
SRCHZZ_IS2000:WAKEUP:CX8_ON:WAKEUP                           55fe 2 0e 2    @olive @white
SRCHZZ_IS2000:WAKEUP:CX8_ON:PREP_SB                          55fe 2 0e 3    @olive @white
SRCHZZ_IS2000:WAKEUP:CX8_ON:WAIT_SB                          55fe 2 0e 4    @olive @white

SRCHZZ_IS2000:WAKEUP:WAKEUP_UPDATE:INIT                      55fe 2 0f 0    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_UPDATE:SLEEP                     55fe 2 0f 1    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_UPDATE:WAKEUP                    55fe 2 0f 2    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_UPDATE:PREP_SB                   55fe 2 0f 3    @olive @white
SRCHZZ_IS2000:WAKEUP:WAKEUP_UPDATE:WAIT_SB                   55fe 2 0f 4    @olive @white

SRCHZZ_IS2000:WAKEUP:RX_TUNE_COMP:INIT                       55fe 2 10 0    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_TUNE_COMP:SLEEP                      55fe 2 10 1    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_TUNE_COMP:WAKEUP                     55fe 2 10 2    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_TUNE_COMP:PREP_SB                    55fe 2 10 3    @olive @white
SRCHZZ_IS2000:WAKEUP:RX_TUNE_COMP:WAIT_SB                    55fe 2 10 4    @olive @white

SRCHZZ_IS2000:PREP_SB:START_SLEEP:INIT                       55fe 3 00 0    @olive @white
SRCHZZ_IS2000:PREP_SB:START_SLEEP:SLEEP                      55fe 3 00 1    @olive @white
SRCHZZ_IS2000:PREP_SB:START_SLEEP:WAKEUP                     55fe 3 00 2    @olive @white
SRCHZZ_IS2000:PREP_SB:START_SLEEP:PREP_SB                    55fe 3 00 3    @olive @white
SRCHZZ_IS2000:PREP_SB:START_SLEEP:WAIT_SB                    55fe 3 00 4    @olive @white

SRCHZZ_IS2000:PREP_SB:XFER_FROM_QPCH:INIT                    55fe 3 01 0    @olive @white
SRCHZZ_IS2000:PREP_SB:XFER_FROM_QPCH:SLEEP                   55fe 3 01 1    @olive @white
SRCHZZ_IS2000:PREP_SB:XFER_FROM_QPCH:WAKEUP                  55fe 3 01 2    @olive @white
SRCHZZ_IS2000:PREP_SB:XFER_FROM_QPCH:PREP_SB                 55fe 3 01 3    @olive @white
SRCHZZ_IS2000:PREP_SB:XFER_FROM_QPCH:WAIT_SB                 55fe 3 01 4    @olive @white

SRCHZZ_IS2000:PREP_SB:ROLL:INIT                              55fe 3 02 0    @olive @white
SRCHZZ_IS2000:PREP_SB:ROLL:SLEEP                             55fe 3 02 1    @olive @white
SRCHZZ_IS2000:PREP_SB:ROLL:WAKEUP                            55fe 3 02 2    @olive @white
SRCHZZ_IS2000:PREP_SB:ROLL:PREP_SB                           55fe 3 02 3    @olive @white
SRCHZZ_IS2000:PREP_SB:ROLL:WAIT_SB                           55fe 3 02 4    @olive @white

SRCHZZ_IS2000:PREP_SB:ADJUST_TIMING:INIT                     55fe 3 03 0    @olive @white
SRCHZZ_IS2000:PREP_SB:ADJUST_TIMING:SLEEP                    55fe 3 03 1    @olive @white
SRCHZZ_IS2000:PREP_SB:ADJUST_TIMING:WAKEUP                   55fe 3 03 2    @olive @white
SRCHZZ_IS2000:PREP_SB:ADJUST_TIMING:PREP_SB                  55fe 3 03 3    @olive @white
SRCHZZ_IS2000:PREP_SB:ADJUST_TIMING:WAIT_SB                  55fe 3 03 4    @olive @white

SRCHZZ_IS2000:PREP_SB:WAKEUP_NOW:INIT                        55fe 3 04 0    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_NOW:SLEEP                       55fe 3 04 1    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_NOW:WAKEUP                      55fe 3 04 2    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_NOW:PREP_SB                     55fe 3 04 3    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_NOW:WAIT_SB                     55fe 3 04 4    @olive @white

SRCHZZ_IS2000:PREP_SB:FREQ_TRACK_OFF_DONE:INIT               55fe 3 05 0    @olive @white
SRCHZZ_IS2000:PREP_SB:FREQ_TRACK_OFF_DONE:SLEEP              55fe 3 05 1    @olive @white
SRCHZZ_IS2000:PREP_SB:FREQ_TRACK_OFF_DONE:WAKEUP             55fe 3 05 2    @olive @white
SRCHZZ_IS2000:PREP_SB:FREQ_TRACK_OFF_DONE:PREP_SB            55fe 3 05 3    @olive @white
SRCHZZ_IS2000:PREP_SB:FREQ_TRACK_OFF_DONE:WAIT_SB            55fe 3 05 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLED:INIT                    55fe 3 06 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLED:SLEEP                   55fe 3 06 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLED:WAKEUP                  55fe 3 06 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLED:PREP_SB                 55fe 3 06 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLED:WAIT_SB                 55fe 3 06 4    @olive @white

SRCHZZ_IS2000:PREP_SB:GO_TO_SLEEP:INIT                       55fe 3 07 0    @olive @white
SRCHZZ_IS2000:PREP_SB:GO_TO_SLEEP:SLEEP                      55fe 3 07 1    @olive @white
SRCHZZ_IS2000:PREP_SB:GO_TO_SLEEP:WAKEUP                     55fe 3 07 2    @olive @white
SRCHZZ_IS2000:PREP_SB:GO_TO_SLEEP:PREP_SB                    55fe 3 07 3    @olive @white
SRCHZZ_IS2000:PREP_SB:GO_TO_SLEEP:WAIT_SB                    55fe 3 07 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLE_COMP:INIT                55fe 3 08 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLE_COMP:SLEEP               55fe 3 08 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLE_COMP:WAKEUP              55fe 3 08 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLE_COMP:PREP_SB             55fe 3 08 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_DISABLE_COMP:WAIT_SB             55fe 3 08 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_CH_GRANTED:INIT                     55fe 3 09 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_GRANTED:SLEEP                    55fe 3 09 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_GRANTED:WAKEUP                   55fe 3 09 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_GRANTED:PREP_SB                  55fe 3 09 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_GRANTED:WAIT_SB                  55fe 3 09 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_CH_DENIED:INIT                      55fe 3 0a 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_DENIED:SLEEP                     55fe 3 0a 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_DENIED:WAKEUP                    55fe 3 0a 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_DENIED:PREP_SB                   55fe 3 0a 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_CH_DENIED:WAIT_SB                   55fe 3 0a 4    @olive @white

SRCHZZ_IS2000:PREP_SB:WAKEUP:INIT                            55fe 3 0b 0    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP:SLEEP                           55fe 3 0b 1    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP:WAKEUP                          55fe 3 0b 2    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP:PREP_SB                         55fe 3 0b 3    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP:WAIT_SB                         55fe 3 0b 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_RF_RSP_TUNE_COMP:INIT               55fe 3 0c 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_RSP_TUNE_COMP:SLEEP              55fe 3 0c 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_RSP_TUNE_COMP:WAKEUP             55fe 3 0c 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_RSP_TUNE_COMP:PREP_SB            55fe 3 0c 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_RF_RSP_TUNE_COMP:WAIT_SB            55fe 3 0c 4    @olive @white

SRCHZZ_IS2000:PREP_SB:NO_RF_LOCK:INIT                        55fe 3 0d 0    @olive @white
SRCHZZ_IS2000:PREP_SB:NO_RF_LOCK:SLEEP                       55fe 3 0d 1    @olive @white
SRCHZZ_IS2000:PREP_SB:NO_RF_LOCK:WAKEUP                      55fe 3 0d 2    @olive @white
SRCHZZ_IS2000:PREP_SB:NO_RF_LOCK:PREP_SB                     55fe 3 0d 3    @olive @white
SRCHZZ_IS2000:PREP_SB:NO_RF_LOCK:WAIT_SB                     55fe 3 0d 4    @olive @white

SRCHZZ_IS2000:PREP_SB:CX8_ON:INIT                            55fe 3 0e 0    @olive @white
SRCHZZ_IS2000:PREP_SB:CX8_ON:SLEEP                           55fe 3 0e 1    @olive @white
SRCHZZ_IS2000:PREP_SB:CX8_ON:WAKEUP                          55fe 3 0e 2    @olive @white
SRCHZZ_IS2000:PREP_SB:CX8_ON:PREP_SB                         55fe 3 0e 3    @olive @white
SRCHZZ_IS2000:PREP_SB:CX8_ON:WAIT_SB                         55fe 3 0e 4    @olive @white

SRCHZZ_IS2000:PREP_SB:WAKEUP_UPDATE:INIT                     55fe 3 0f 0    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_UPDATE:SLEEP                    55fe 3 0f 1    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_UPDATE:WAKEUP                   55fe 3 0f 2    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_UPDATE:PREP_SB                  55fe 3 0f 3    @olive @white
SRCHZZ_IS2000:PREP_SB:WAKEUP_UPDATE:WAIT_SB                  55fe 3 0f 4    @olive @white

SRCHZZ_IS2000:PREP_SB:RX_TUNE_COMP:INIT                      55fe 3 10 0    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_TUNE_COMP:SLEEP                     55fe 3 10 1    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_TUNE_COMP:WAKEUP                    55fe 3 10 2    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_TUNE_COMP:PREP_SB                   55fe 3 10 3    @olive @white
SRCHZZ_IS2000:PREP_SB:RX_TUNE_COMP:WAIT_SB                   55fe 3 10 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:START_SLEEP:INIT                       55fe 4 00 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:START_SLEEP:SLEEP                      55fe 4 00 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:START_SLEEP:WAKEUP                     55fe 4 00 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:START_SLEEP:PREP_SB                    55fe 4 00 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:START_SLEEP:WAIT_SB                    55fe 4 00 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:XFER_FROM_QPCH:INIT                    55fe 4 01 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:XFER_FROM_QPCH:SLEEP                   55fe 4 01 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:XFER_FROM_QPCH:WAKEUP                  55fe 4 01 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:XFER_FROM_QPCH:PREP_SB                 55fe 4 01 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:XFER_FROM_QPCH:WAIT_SB                 55fe 4 01 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:ROLL:INIT                              55fe 4 02 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:ROLL:SLEEP                             55fe 4 02 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:ROLL:WAKEUP                            55fe 4 02 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:ROLL:PREP_SB                           55fe 4 02 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:ROLL:WAIT_SB                           55fe 4 02 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:ADJUST_TIMING:INIT                     55fe 4 03 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:ADJUST_TIMING:SLEEP                    55fe 4 03 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:ADJUST_TIMING:WAKEUP                   55fe 4 03 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:ADJUST_TIMING:PREP_SB                  55fe 4 03 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:ADJUST_TIMING:WAIT_SB                  55fe 4 03 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:WAKEUP_NOW:INIT                        55fe 4 04 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_NOW:SLEEP                       55fe 4 04 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_NOW:WAKEUP                      55fe 4 04 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_NOW:PREP_SB                     55fe 4 04 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_NOW:WAIT_SB                     55fe 4 04 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:FREQ_TRACK_OFF_DONE:INIT               55fe 4 05 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:FREQ_TRACK_OFF_DONE:SLEEP              55fe 4 05 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:FREQ_TRACK_OFF_DONE:WAKEUP             55fe 4 05 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:FREQ_TRACK_OFF_DONE:PREP_SB            55fe 4 05 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:FREQ_TRACK_OFF_DONE:WAIT_SB            55fe 4 05 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLED:INIT                    55fe 4 06 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLED:SLEEP                   55fe 4 06 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLED:WAKEUP                  55fe 4 06 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLED:PREP_SB                 55fe 4 06 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLED:WAIT_SB                 55fe 4 06 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:GO_TO_SLEEP:INIT                       55fe 4 07 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:GO_TO_SLEEP:SLEEP                      55fe 4 07 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:GO_TO_SLEEP:WAKEUP                     55fe 4 07 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:GO_TO_SLEEP:PREP_SB                    55fe 4 07 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:GO_TO_SLEEP:WAIT_SB                    55fe 4 07 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLE_COMP:INIT                55fe 4 08 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLE_COMP:SLEEP               55fe 4 08 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLE_COMP:WAKEUP              55fe 4 08 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLE_COMP:PREP_SB             55fe 4 08 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_DISABLE_COMP:WAIT_SB             55fe 4 08 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_CH_GRANTED:INIT                     55fe 4 09 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_GRANTED:SLEEP                    55fe 4 09 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_GRANTED:WAKEUP                   55fe 4 09 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_GRANTED:PREP_SB                  55fe 4 09 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_GRANTED:WAIT_SB                  55fe 4 09 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_CH_DENIED:INIT                      55fe 4 0a 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_DENIED:SLEEP                     55fe 4 0a 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_DENIED:WAKEUP                    55fe 4 0a 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_DENIED:PREP_SB                   55fe 4 0a 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_CH_DENIED:WAIT_SB                   55fe 4 0a 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:WAKEUP:INIT                            55fe 4 0b 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP:SLEEP                           55fe 4 0b 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP:WAKEUP                          55fe 4 0b 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP:PREP_SB                         55fe 4 0b 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP:WAIT_SB                         55fe 4 0b 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_RF_RSP_TUNE_COMP:INIT               55fe 4 0c 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_RSP_TUNE_COMP:SLEEP              55fe 4 0c 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_RSP_TUNE_COMP:WAKEUP             55fe 4 0c 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_RSP_TUNE_COMP:PREP_SB            55fe 4 0c 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_RF_RSP_TUNE_COMP:WAIT_SB            55fe 4 0c 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:NO_RF_LOCK:INIT                        55fe 4 0d 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:NO_RF_LOCK:SLEEP                       55fe 4 0d 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:NO_RF_LOCK:WAKEUP                      55fe 4 0d 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:NO_RF_LOCK:PREP_SB                     55fe 4 0d 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:NO_RF_LOCK:WAIT_SB                     55fe 4 0d 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:CX8_ON:INIT                            55fe 4 0e 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:CX8_ON:SLEEP                           55fe 4 0e 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:CX8_ON:WAKEUP                          55fe 4 0e 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:CX8_ON:PREP_SB                         55fe 4 0e 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:CX8_ON:WAIT_SB                         55fe 4 0e 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:WAKEUP_UPDATE:INIT                     55fe 4 0f 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_UPDATE:SLEEP                    55fe 4 0f 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_UPDATE:WAKEUP                   55fe 4 0f 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_UPDATE:PREP_SB                  55fe 4 0f 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:WAKEUP_UPDATE:WAIT_SB                  55fe 4 0f 4    @olive @white

SRCHZZ_IS2000:WAIT_SB:RX_TUNE_COMP:INIT                      55fe 4 10 0    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_TUNE_COMP:SLEEP                     55fe 4 10 1    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_TUNE_COMP:WAKEUP                    55fe 4 10 2    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_TUNE_COMP:PREP_SB                   55fe 4 10 3    @olive @white
SRCHZZ_IS2000:WAIT_SB:RX_TUNE_COMP:WAIT_SB                   55fe 4 10 4    @olive @white



# End machine generated TLA code for state machine: SRCHZZ_IS2000_SM

