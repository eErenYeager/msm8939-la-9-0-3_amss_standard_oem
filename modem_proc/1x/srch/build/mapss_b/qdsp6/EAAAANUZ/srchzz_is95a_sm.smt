/*=============================================================================

  srchzz_is95a_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_is95a_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_IS95A_SM */

/* Transition function prototypes */
static stm_state_type is95a_setup_sleep(void *);
static stm_state_type is95a_process_init_roll(void *);
static stm_state_type is95a_adjust_combiner_time_after_sb(void *);
static stm_state_type is95a_remember_rude_wakeup(void *);
static stm_state_type is95a_process_freq_track_off_done(void *);
static stm_state_type is95a_process_rf_disabled_cmd(void *);
static stm_state_type is95a_process_go_to_sleep_cmd(void *);
static stm_state_type is95a_process_rf_disable_comp_cmd(void *);
static stm_state_type is95a_process_rude_wakeup(void *);
static stm_state_type is95a_process_chain_granted(void *);
static stm_state_type is95a_process_chain_denied(void *);
static stm_state_type is95a_process_wakeup(void *);
static stm_state_type is95a_process_rf_prep_comp(void *);
static stm_state_type is95a_process_no_rf_lock(void *);
static stm_state_type is95a_remember_cx8_on(void *);
static stm_state_type is95a_process_cx8_on(void *);
static stm_state_type is95a_process_tune_complete(void *);
static stm_state_type is95a_adjust_combiner_time_fake_sync80(void *);
static stm_state_type is95a_process_fake_sync80_roll(void *);
static stm_state_type is95a_process_fake_sync80_acq_roll(void *);
static stm_state_type is95a_adjust_combiner_time_wait_sb(void *);
static stm_state_type is95a_process_wait_sb_roll(void *);


/* State Machine entry/exit function prototypes */
static void is95a_entry(stm_group_type *group);
static void is95a_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void is95a_enter_init(void *payload, stm_state_type previous_state);
static void is95a_enter_sleep(void *payload, stm_state_type previous_state);
static void is95a_exit_sleep(void *payload, stm_state_type previous_state);
static void is95a_enter_wakeup(void *payload, stm_state_type previous_state);
static void is95a_enter_fake_sync80(void *payload, stm_state_type previous_state);
static void is95a_enter_fake_sync80_acq(void *payload, stm_state_type previous_state);
static void is95a_enter_wait_sb(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_IS95A_SM_NUMBER_OF_STATES 6
#define SRCHZZ_IS95A_SM_NUMBER_OF_INPUTS 15


/* State enumeration */
enum
{
  INIT_STATE,
  SLEEP_STATE,
  WAKEUP_STATE,
  FAKE_SYNC80_STATE,
  FAKE_SYNC80_ACQ_STATE,
  WAIT_SB_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_IS95A_SM_states[ SRCHZZ_IS95A_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", is95a_enter_init, NULL},
  {"SLEEP_STATE", is95a_enter_sleep, is95a_exit_sleep},
  {"WAKEUP_STATE", is95a_enter_wakeup, NULL},
  {"FAKE_SYNC80_STATE", is95a_enter_fake_sync80, NULL},
  {"FAKE_SYNC80_ACQ_STATE", is95a_enter_fake_sync80_acq, NULL},
  {"WAIT_SB_STATE", is95a_enter_wait_sb, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_IS95A_SM_inputs[ SRCHZZ_IS95A_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)ADJUST_TIMING_CMD, "ADJUST_TIMING_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)RX_RF_DISABLED_CMD, "RX_RF_DISABLED_CMD"},
  {(stm_input_type)GO_TO_SLEEP_CMD, "GO_TO_SLEEP_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)RX_CH_GRANTED_CMD, "RX_CH_GRANTED_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)WAKEUP_CMD, "WAKEUP_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)NO_RF_LOCK_CMD, "NO_RF_LOCK_CMD"},
  {(stm_input_type)CX8_ON_CMD, "CX8_ON_CMD"},
  {(stm_input_type)RX_TUNE_COMP_CMD, "RX_TUNE_COMP_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_IS95A_SM_transitions[ SRCHZZ_IS95A_SM_NUMBER_OF_STATES *  SRCHZZ_IS95A_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    is95a_setup_sleep,    /* START_SLEEP_CMD */
    is95a_process_init_roll,    /* ROLL_CMD */
    is95a_adjust_combiner_time_after_sb,    /* ADJUST_TIMING_CMD */
    is95a_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    is95a_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    is95a_process_rf_disabled_cmd,    /* RX_RF_DISABLED_CMD */
    is95a_process_go_to_sleep_cmd,    /* GO_TO_SLEEP_CMD */
    is95a_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state SLEEP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    is95a_process_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    is95a_process_chain_granted,    /* RX_CH_GRANTED_CMD */
    is95a_process_chain_denied,    /* RX_CH_DENIED_CMD */
    is95a_process_wakeup,    /* WAKEUP_CMD */
    is95a_process_rf_prep_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    is95a_process_no_rf_lock,    /* NO_RF_LOCK_CMD */
    is95a_remember_cx8_on,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state WAKEUP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    is95a_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    is95a_process_cx8_on,    /* CX8_ON_CMD */
    is95a_process_tune_complete,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state FAKE_SYNC80_STATE */
    NULL,    /* START_SLEEP_CMD */
    is95a_process_fake_sync80_roll,    /* ROLL_CMD */
    is95a_adjust_combiner_time_fake_sync80,    /* ADJUST_TIMING_CMD */
    is95a_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state FAKE_SYNC80_ACQ_STATE */
    NULL,    /* START_SLEEP_CMD */
    is95a_process_fake_sync80_acq_roll,    /* ROLL_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    is95a_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

  /* Transition functions for state WAIT_SB_STATE */
    NULL,    /* START_SLEEP_CMD */
    is95a_process_wait_sb_roll,    /* ROLL_CMD */
    is95a_adjust_combiner_time_wait_sb,    /* ADJUST_TIMING_CMD */
    is95a_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* GO_TO_SLEEP_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_IS95A_SM =
{
  "SRCHZZ_IS95A_SM", /* state machine name */
  13834, /* unique SM id (hash of name) */
  is95a_entry, /* state machine entry function */
  is95a_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_IS95A_SM_NUMBER_OF_STATES,
  SRCHZZ_IS95A_SM_NUMBER_OF_INPUTS,
  SRCHZZ_IS95A_SM_states,
  SRCHZZ_IS95A_SM_inputs,
  SRCHZZ_IS95A_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_IS95A_SM */

