###############################################################################
#
#    srchidle_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchidle_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHIDLE_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHIDLE:ONLINE:SRCH_CDMA_F:ONLINE                           8a76 0 00 0    @red @white
SRCHIDLE:ONLINE:SRCH_CDMA_F:SLEEP                            8a76 0 00 1    @red @white
SRCHIDLE:ONLINE:SRCH_CDMA_F:ACCESS                           8a76 0 00 2    @red @white
SRCHIDLE:ONLINE:SRCH_CDMA_F:ASSIGN                           8a76 0 00 3    @red @white

SRCHIDLE:ONLINE:SRCH_SLEEP_F:ONLINE                          8a76 0 01 0    @red @white
SRCHIDLE:ONLINE:SRCH_SLEEP_F:SLEEP                           8a76 0 01 1    @red @white
SRCHIDLE:ONLINE:SRCH_SLEEP_F:ACCESS                          8a76 0 01 2    @red @white
SRCHIDLE:ONLINE:SRCH_SLEEP_F:ASSIGN                          8a76 0 01 3    @red @white

SRCHIDLE:ONLINE:SRCH_TC_F:ONLINE                             8a76 0 02 0    @red @white
SRCHIDLE:ONLINE:SRCH_TC_F:SLEEP                              8a76 0 02 1    @red @white
SRCHIDLE:ONLINE:SRCH_TC_F:ACCESS                             8a76 0 02 2    @red @white
SRCHIDLE:ONLINE:SRCH_TC_F:ASSIGN                             8a76 0 02 3    @red @white

SRCHIDLE:ONLINE:SRCH_ACCESS_F:ONLINE                         8a76 0 03 0    @red @white
SRCHIDLE:ONLINE:SRCH_ACCESS_F:SLEEP                          8a76 0 03 1    @red @white
SRCHIDLE:ONLINE:SRCH_ACCESS_F:ACCESS                         8a76 0 03 2    @red @white
SRCHIDLE:ONLINE:SRCH_ACCESS_F:ASSIGN                         8a76 0 03 3    @red @white

SRCHIDLE:ONLINE:SRCH_PARM_F:ONLINE                           8a76 0 04 0    @red @white
SRCHIDLE:ONLINE:SRCH_PARM_F:SLEEP                            8a76 0 04 1    @red @white
SRCHIDLE:ONLINE:SRCH_PARM_F:ACCESS                           8a76 0 04 2    @red @white
SRCHIDLE:ONLINE:SRCH_PARM_F:ASSIGN                           8a76 0 04 3    @red @white

SRCHIDLE:ONLINE:SRCH_IDLE_ASET_F:ONLINE                      8a76 0 05 0    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_ASET_F:SLEEP                       8a76 0 05 1    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_ASET_F:ACCESS                      8a76 0 05 2    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_ASET_F:ASSIGN                      8a76 0 05 3    @red @white

SRCHIDLE:ONLINE:SRCH_IDLE_NSET_F:ONLINE                      8a76 0 06 0    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_NSET_F:SLEEP                       8a76 0 06 1    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_NSET_F:ACCESS                      8a76 0 06 2    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_NSET_F:ASSIGN                      8a76 0 06 3    @red @white

SRCHIDLE:ONLINE:SRCH_RESELECT_MEAS_F:ONLINE                  8a76 0 07 0    @red @white
SRCHIDLE:ONLINE:SRCH_RESELECT_MEAS_F:SLEEP                   8a76 0 07 1    @red @white
SRCHIDLE:ONLINE:SRCH_RESELECT_MEAS_F:ACCESS                  8a76 0 07 2    @red @white
SRCHIDLE:ONLINE:SRCH_RESELECT_MEAS_F:ASSIGN                  8a76 0 07 3    @red @white

SRCHIDLE:ONLINE:SRCH_PC_ASSIGN_F:ONLINE                      8a76 0 08 0    @red @white
SRCHIDLE:ONLINE:SRCH_PC_ASSIGN_F:SLEEP                       8a76 0 08 1    @red @white
SRCHIDLE:ONLINE:SRCH_PC_ASSIGN_F:ACCESS                      8a76 0 08 2    @red @white
SRCHIDLE:ONLINE:SRCH_PC_ASSIGN_F:ASSIGN                      8a76 0 08 3    @red @white

SRCHIDLE:ONLINE:SRCH_CHAN_CONFIG_F:ONLINE                    8a76 0 09 0    @red @white
SRCHIDLE:ONLINE:SRCH_CHAN_CONFIG_F:SLEEP                     8a76 0 09 1    @red @white
SRCHIDLE:ONLINE:SRCH_CHAN_CONFIG_F:ACCESS                    8a76 0 09 2    @red @white
SRCHIDLE:ONLINE:SRCH_CHAN_CONFIG_F:ASSIGN                    8a76 0 09 3    @red @white

SRCHIDLE:ONLINE:SRCH_BC_INFO_F:ONLINE                        8a76 0 0a 0    @red @white
SRCHIDLE:ONLINE:SRCH_BC_INFO_F:SLEEP                         8a76 0 0a 1    @red @white
SRCHIDLE:ONLINE:SRCH_BC_INFO_F:ACCESS                        8a76 0 0a 2    @red @white
SRCHIDLE:ONLINE:SRCH_BC_INFO_F:ASSIGN                        8a76 0 0a 3    @red @white

SRCHIDLE:ONLINE:SRCH_IDLE_F:ONLINE                           8a76 0 0b 0    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_F:SLEEP                            8a76 0 0b 1    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_F:ACCESS                           8a76 0 0b 2    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_F:ASSIGN                           8a76 0 0b 3    @red @white

SRCHIDLE:ONLINE:ROLL:ONLINE                                  8a76 0 0c 0    @red @white
SRCHIDLE:ONLINE:ROLL:SLEEP                                   8a76 0 0c 1    @red @white
SRCHIDLE:ONLINE:ROLL:ACCESS                                  8a76 0 0c 2    @red @white
SRCHIDLE:ONLINE:ROLL:ASSIGN                                  8a76 0 0c 3    @red @white

SRCHIDLE:ONLINE:HO_REQUEST:ONLINE                            8a76 0 0d 0    @red @white
SRCHIDLE:ONLINE:HO_REQUEST:SLEEP                             8a76 0 0d 1    @red @white
SRCHIDLE:ONLINE:HO_REQUEST:ACCESS                            8a76 0 0d 2    @red @white
SRCHIDLE:ONLINE:HO_REQUEST:ASSIGN                            8a76 0 0d 3    @red @white

SRCHIDLE:ONLINE:FRAME_STROBE:ONLINE                          8a76 0 0e 0    @red @white
SRCHIDLE:ONLINE:FRAME_STROBE:SLEEP                           8a76 0 0e 1    @red @white
SRCHIDLE:ONLINE:FRAME_STROBE:ACCESS                          8a76 0 0e 2    @red @white
SRCHIDLE:ONLINE:FRAME_STROBE:ASSIGN                          8a76 0 0e 3    @red @white

SRCHIDLE:ONLINE:RESELECTION_CHECK:ONLINE                     8a76 0 0f 0    @red @white
SRCHIDLE:ONLINE:RESELECTION_CHECK:SLEEP                      8a76 0 0f 1    @red @white
SRCHIDLE:ONLINE:RESELECTION_CHECK:ACCESS                     8a76 0 0f 2    @red @white
SRCHIDLE:ONLINE:RESELECTION_CHECK:ASSIGN                     8a76 0 0f 3    @red @white

SRCHIDLE:ONLINE:SLEEP_OK:ONLINE                              8a76 0 10 0    @red @white
SRCHIDLE:ONLINE:SLEEP_OK:SLEEP                               8a76 0 10 1    @red @white
SRCHIDLE:ONLINE:SLEEP_OK:ACCESS                              8a76 0 10 2    @red @white
SRCHIDLE:ONLINE:SLEEP_OK:ASSIGN                              8a76 0 10 3    @red @white

SRCHIDLE:ONLINE:RSSI_TIMER:ONLINE                            8a76 0 11 0    @red @white
SRCHIDLE:ONLINE:RSSI_TIMER:SLEEP                             8a76 0 11 1    @red @white
SRCHIDLE:ONLINE:RSSI_TIMER:ACCESS                            8a76 0 11 2    @red @white
SRCHIDLE:ONLINE:RSSI_TIMER:ASSIGN                            8a76 0 11 3    @red @white

SRCHIDLE:ONLINE:RF_REPORT_TIMER:ONLINE                       8a76 0 12 0    @red @white
SRCHIDLE:ONLINE:RF_REPORT_TIMER:SLEEP                        8a76 0 12 1    @red @white
SRCHIDLE:ONLINE:RF_REPORT_TIMER:ACCESS                       8a76 0 12 2    @red @white
SRCHIDLE:ONLINE:RF_REPORT_TIMER:ASSIGN                       8a76 0 12 3    @red @white

SRCHIDLE:ONLINE:DEACTIVATE_ZZ:ONLINE                         8a76 0 13 0    @red @white
SRCHIDLE:ONLINE:DEACTIVATE_ZZ:SLEEP                          8a76 0 13 1    @red @white
SRCHIDLE:ONLINE:DEACTIVATE_ZZ:ACCESS                         8a76 0 13 2    @red @white
SRCHIDLE:ONLINE:DEACTIVATE_ZZ:ASSIGN                         8a76 0 13 3    @red @white

SRCHIDLE:ONLINE:DELAYED_ASET:ONLINE                          8a76 0 14 0    @red @white
SRCHIDLE:ONLINE:DELAYED_ASET:SLEEP                           8a76 0 14 1    @red @white
SRCHIDLE:ONLINE:DELAYED_ASET:ACCESS                          8a76 0 14 2    @red @white
SRCHIDLE:ONLINE:DELAYED_ASET:ASSIGN                          8a76 0 14 3    @red @white

SRCHIDLE:ONLINE:ONLINE_RF_GRANTED:ONLINE                     8a76 0 15 0    @red @white
SRCHIDLE:ONLINE:ONLINE_RF_GRANTED:SLEEP                      8a76 0 15 1    @red @white
SRCHIDLE:ONLINE:ONLINE_RF_GRANTED:ACCESS                     8a76 0 15 2    @red @white
SRCHIDLE:ONLINE:ONLINE_RF_GRANTED:ASSIGN                     8a76 0 15 3    @red @white

SRCHIDLE:ONLINE:ONLINE_TUNE_DONE:ONLINE                      8a76 0 16 0    @red @white
SRCHIDLE:ONLINE:ONLINE_TUNE_DONE:SLEEP                       8a76 0 16 1    @red @white
SRCHIDLE:ONLINE:ONLINE_TUNE_DONE:ACCESS                      8a76 0 16 2    @red @white
SRCHIDLE:ONLINE:ONLINE_TUNE_DONE:ASSIGN                      8a76 0 16 3    @red @white

SRCHIDLE:ONLINE:RX_MOD_GRANTED:ONLINE                        8a76 0 17 0    @red @white
SRCHIDLE:ONLINE:RX_MOD_GRANTED:SLEEP                         8a76 0 17 1    @red @white
SRCHIDLE:ONLINE:RX_MOD_GRANTED:ACCESS                        8a76 0 17 2    @red @white
SRCHIDLE:ONLINE:RX_MOD_GRANTED:ASSIGN                        8a76 0 17 3    @red @white

SRCHIDLE:ONLINE:RX_MOD_DENIED:ONLINE                         8a76 0 18 0    @red @white
SRCHIDLE:ONLINE:RX_MOD_DENIED:SLEEP                          8a76 0 18 1    @red @white
SRCHIDLE:ONLINE:RX_MOD_DENIED:ACCESS                         8a76 0 18 2    @red @white
SRCHIDLE:ONLINE:RX_MOD_DENIED:ASSIGN                         8a76 0 18 3    @red @white

SRCHIDLE:ONLINE:IDLE_DIV_REQUEST:ONLINE                      8a76 0 19 0    @red @white
SRCHIDLE:ONLINE:IDLE_DIV_REQUEST:SLEEP                       8a76 0 19 1    @red @white
SRCHIDLE:ONLINE:IDLE_DIV_REQUEST:ACCESS                      8a76 0 19 2    @red @white
SRCHIDLE:ONLINE:IDLE_DIV_REQUEST:ASSIGN                      8a76 0 19 3    @red @white

SRCHIDLE:ONLINE:PAGE_MATCH:ONLINE                            8a76 0 1a 0    @red @white
SRCHIDLE:ONLINE:PAGE_MATCH:SLEEP                             8a76 0 1a 1    @red @white
SRCHIDLE:ONLINE:PAGE_MATCH:ACCESS                            8a76 0 1a 2    @red @white
SRCHIDLE:ONLINE:PAGE_MATCH:ASSIGN                            8a76 0 1a 3    @red @white

SRCHIDLE:ONLINE:CDMA_RSP:ONLINE                              8a76 0 1b 0    @red @white
SRCHIDLE:ONLINE:CDMA_RSP:SLEEP                               8a76 0 1b 1    @red @white
SRCHIDLE:ONLINE:CDMA_RSP:ACCESS                              8a76 0 1b 2    @red @white
SRCHIDLE:ONLINE:CDMA_RSP:ASSIGN                              8a76 0 1b 3    @red @white

SRCHIDLE:ONLINE:IDLE_RSP:ONLINE                              8a76 0 1c 0    @red @white
SRCHIDLE:ONLINE:IDLE_RSP:SLEEP                               8a76 0 1c 1    @red @white
SRCHIDLE:ONLINE:IDLE_RSP:ACCESS                              8a76 0 1c 2    @red @white
SRCHIDLE:ONLINE:IDLE_RSP:ASSIGN                              8a76 0 1c 3    @red @white

SRCHIDLE:ONLINE:SRCH_PILOT_LIST_F:ONLINE                     8a76 0 1d 0    @red @white
SRCHIDLE:ONLINE:SRCH_PILOT_LIST_F:SLEEP                      8a76 0 1d 1    @red @white
SRCHIDLE:ONLINE:SRCH_PILOT_LIST_F:ACCESS                     8a76 0 1d 2    @red @white
SRCHIDLE:ONLINE:SRCH_PILOT_LIST_F:ASSIGN                     8a76 0 1d 3    @red @white

SRCHIDLE:ONLINE:SRCH_IDLE_FIND_STRONG_PILOT_F:ONLINE         8a76 0 1e 0    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_FIND_STRONG_PILOT_F:SLEEP          8a76 0 1e 1    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_FIND_STRONG_PILOT_F:ACCESS         8a76 0 1e 2    @red @white
SRCHIDLE:ONLINE:SRCH_IDLE_FIND_STRONG_PILOT_F:ASSIGN         8a76 0 1e 3    @red @white

SRCHIDLE:ONLINE:SCAN_ALL_RPT:ONLINE                          8a76 0 1f 0    @red @white
SRCHIDLE:ONLINE:SCAN_ALL_RPT:SLEEP                           8a76 0 1f 1    @red @white
SRCHIDLE:ONLINE:SCAN_ALL_RPT:ACCESS                          8a76 0 1f 2    @red @white
SRCHIDLE:ONLINE:SCAN_ALL_RPT:ASSIGN                          8a76 0 1f 3    @red @white

SRCHIDLE:ONLINE:RF_GRANTED:ONLINE                            8a76 0 20 0    @red @white
SRCHIDLE:ONLINE:RF_GRANTED:SLEEP                             8a76 0 20 1    @red @white
SRCHIDLE:ONLINE:RF_GRANTED:ACCESS                            8a76 0 20 2    @red @white
SRCHIDLE:ONLINE:RF_GRANTED:ASSIGN                            8a76 0 20 3    @red @white

SRCHIDLE:ONLINE:RF_PREP:ONLINE                               8a76 0 21 0    @red @white
SRCHIDLE:ONLINE:RF_PREP:SLEEP                                8a76 0 21 1    @red @white
SRCHIDLE:ONLINE:RF_PREP:ACCESS                               8a76 0 21 2    @red @white
SRCHIDLE:ONLINE:RF_PREP:ASSIGN                               8a76 0 21 3    @red @white

SRCHIDLE:ONLINE:TUNE_DONE:ONLINE                             8a76 0 22 0    @red @white
SRCHIDLE:ONLINE:TUNE_DONE:SLEEP                              8a76 0 22 1    @red @white
SRCHIDLE:ONLINE:TUNE_DONE:ACCESS                             8a76 0 22 2    @red @white
SRCHIDLE:ONLINE:TUNE_DONE:ASSIGN                             8a76 0 22 3    @red @white

SRCHIDLE:ONLINE:ABORT_COMPLETE:ONLINE                        8a76 0 23 0    @red @white
SRCHIDLE:ONLINE:ABORT_COMPLETE:SLEEP                         8a76 0 23 1    @red @white
SRCHIDLE:ONLINE:ABORT_COMPLETE:ACCESS                        8a76 0 23 2    @red @white
SRCHIDLE:ONLINE:ABORT_COMPLETE:ASSIGN                        8a76 0 23 3    @red @white

SRCHIDLE:SLEEP:SRCH_CDMA_F:ONLINE                            8a76 1 00 0    @red @white
SRCHIDLE:SLEEP:SRCH_CDMA_F:SLEEP                             8a76 1 00 1    @red @white
SRCHIDLE:SLEEP:SRCH_CDMA_F:ACCESS                            8a76 1 00 2    @red @white
SRCHIDLE:SLEEP:SRCH_CDMA_F:ASSIGN                            8a76 1 00 3    @red @white

SRCHIDLE:SLEEP:SRCH_SLEEP_F:ONLINE                           8a76 1 01 0    @red @white
SRCHIDLE:SLEEP:SRCH_SLEEP_F:SLEEP                            8a76 1 01 1    @red @white
SRCHIDLE:SLEEP:SRCH_SLEEP_F:ACCESS                           8a76 1 01 2    @red @white
SRCHIDLE:SLEEP:SRCH_SLEEP_F:ASSIGN                           8a76 1 01 3    @red @white

SRCHIDLE:SLEEP:SRCH_TC_F:ONLINE                              8a76 1 02 0    @red @white
SRCHIDLE:SLEEP:SRCH_TC_F:SLEEP                               8a76 1 02 1    @red @white
SRCHIDLE:SLEEP:SRCH_TC_F:ACCESS                              8a76 1 02 2    @red @white
SRCHIDLE:SLEEP:SRCH_TC_F:ASSIGN                              8a76 1 02 3    @red @white

SRCHIDLE:SLEEP:SRCH_ACCESS_F:ONLINE                          8a76 1 03 0    @red @white
SRCHIDLE:SLEEP:SRCH_ACCESS_F:SLEEP                           8a76 1 03 1    @red @white
SRCHIDLE:SLEEP:SRCH_ACCESS_F:ACCESS                          8a76 1 03 2    @red @white
SRCHIDLE:SLEEP:SRCH_ACCESS_F:ASSIGN                          8a76 1 03 3    @red @white

SRCHIDLE:SLEEP:SRCH_PARM_F:ONLINE                            8a76 1 04 0    @red @white
SRCHIDLE:SLEEP:SRCH_PARM_F:SLEEP                             8a76 1 04 1    @red @white
SRCHIDLE:SLEEP:SRCH_PARM_F:ACCESS                            8a76 1 04 2    @red @white
SRCHIDLE:SLEEP:SRCH_PARM_F:ASSIGN                            8a76 1 04 3    @red @white

SRCHIDLE:SLEEP:SRCH_IDLE_ASET_F:ONLINE                       8a76 1 05 0    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_ASET_F:SLEEP                        8a76 1 05 1    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_ASET_F:ACCESS                       8a76 1 05 2    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_ASET_F:ASSIGN                       8a76 1 05 3    @red @white

SRCHIDLE:SLEEP:SRCH_IDLE_NSET_F:ONLINE                       8a76 1 06 0    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_NSET_F:SLEEP                        8a76 1 06 1    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_NSET_F:ACCESS                       8a76 1 06 2    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_NSET_F:ASSIGN                       8a76 1 06 3    @red @white

SRCHIDLE:SLEEP:SRCH_RESELECT_MEAS_F:ONLINE                   8a76 1 07 0    @red @white
SRCHIDLE:SLEEP:SRCH_RESELECT_MEAS_F:SLEEP                    8a76 1 07 1    @red @white
SRCHIDLE:SLEEP:SRCH_RESELECT_MEAS_F:ACCESS                   8a76 1 07 2    @red @white
SRCHIDLE:SLEEP:SRCH_RESELECT_MEAS_F:ASSIGN                   8a76 1 07 3    @red @white

SRCHIDLE:SLEEP:SRCH_PC_ASSIGN_F:ONLINE                       8a76 1 08 0    @red @white
SRCHIDLE:SLEEP:SRCH_PC_ASSIGN_F:SLEEP                        8a76 1 08 1    @red @white
SRCHIDLE:SLEEP:SRCH_PC_ASSIGN_F:ACCESS                       8a76 1 08 2    @red @white
SRCHIDLE:SLEEP:SRCH_PC_ASSIGN_F:ASSIGN                       8a76 1 08 3    @red @white

SRCHIDLE:SLEEP:SRCH_CHAN_CONFIG_F:ONLINE                     8a76 1 09 0    @red @white
SRCHIDLE:SLEEP:SRCH_CHAN_CONFIG_F:SLEEP                      8a76 1 09 1    @red @white
SRCHIDLE:SLEEP:SRCH_CHAN_CONFIG_F:ACCESS                     8a76 1 09 2    @red @white
SRCHIDLE:SLEEP:SRCH_CHAN_CONFIG_F:ASSIGN                     8a76 1 09 3    @red @white

SRCHIDLE:SLEEP:SRCH_BC_INFO_F:ONLINE                         8a76 1 0a 0    @red @white
SRCHIDLE:SLEEP:SRCH_BC_INFO_F:SLEEP                          8a76 1 0a 1    @red @white
SRCHIDLE:SLEEP:SRCH_BC_INFO_F:ACCESS                         8a76 1 0a 2    @red @white
SRCHIDLE:SLEEP:SRCH_BC_INFO_F:ASSIGN                         8a76 1 0a 3    @red @white

SRCHIDLE:SLEEP:SRCH_IDLE_F:ONLINE                            8a76 1 0b 0    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_F:SLEEP                             8a76 1 0b 1    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_F:ACCESS                            8a76 1 0b 2    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_F:ASSIGN                            8a76 1 0b 3    @red @white

SRCHIDLE:SLEEP:ROLL:ONLINE                                   8a76 1 0c 0    @red @white
SRCHIDLE:SLEEP:ROLL:SLEEP                                    8a76 1 0c 1    @red @white
SRCHIDLE:SLEEP:ROLL:ACCESS                                   8a76 1 0c 2    @red @white
SRCHIDLE:SLEEP:ROLL:ASSIGN                                   8a76 1 0c 3    @red @white

SRCHIDLE:SLEEP:HO_REQUEST:ONLINE                             8a76 1 0d 0    @red @white
SRCHIDLE:SLEEP:HO_REQUEST:SLEEP                              8a76 1 0d 1    @red @white
SRCHIDLE:SLEEP:HO_REQUEST:ACCESS                             8a76 1 0d 2    @red @white
SRCHIDLE:SLEEP:HO_REQUEST:ASSIGN                             8a76 1 0d 3    @red @white

SRCHIDLE:SLEEP:FRAME_STROBE:ONLINE                           8a76 1 0e 0    @red @white
SRCHIDLE:SLEEP:FRAME_STROBE:SLEEP                            8a76 1 0e 1    @red @white
SRCHIDLE:SLEEP:FRAME_STROBE:ACCESS                           8a76 1 0e 2    @red @white
SRCHIDLE:SLEEP:FRAME_STROBE:ASSIGN                           8a76 1 0e 3    @red @white

SRCHIDLE:SLEEP:RESELECTION_CHECK:ONLINE                      8a76 1 0f 0    @red @white
SRCHIDLE:SLEEP:RESELECTION_CHECK:SLEEP                       8a76 1 0f 1    @red @white
SRCHIDLE:SLEEP:RESELECTION_CHECK:ACCESS                      8a76 1 0f 2    @red @white
SRCHIDLE:SLEEP:RESELECTION_CHECK:ASSIGN                      8a76 1 0f 3    @red @white

SRCHIDLE:SLEEP:SLEEP_OK:ONLINE                               8a76 1 10 0    @red @white
SRCHIDLE:SLEEP:SLEEP_OK:SLEEP                                8a76 1 10 1    @red @white
SRCHIDLE:SLEEP:SLEEP_OK:ACCESS                               8a76 1 10 2    @red @white
SRCHIDLE:SLEEP:SLEEP_OK:ASSIGN                               8a76 1 10 3    @red @white

SRCHIDLE:SLEEP:RSSI_TIMER:ONLINE                             8a76 1 11 0    @red @white
SRCHIDLE:SLEEP:RSSI_TIMER:SLEEP                              8a76 1 11 1    @red @white
SRCHIDLE:SLEEP:RSSI_TIMER:ACCESS                             8a76 1 11 2    @red @white
SRCHIDLE:SLEEP:RSSI_TIMER:ASSIGN                             8a76 1 11 3    @red @white

SRCHIDLE:SLEEP:RF_REPORT_TIMER:ONLINE                        8a76 1 12 0    @red @white
SRCHIDLE:SLEEP:RF_REPORT_TIMER:SLEEP                         8a76 1 12 1    @red @white
SRCHIDLE:SLEEP:RF_REPORT_TIMER:ACCESS                        8a76 1 12 2    @red @white
SRCHIDLE:SLEEP:RF_REPORT_TIMER:ASSIGN                        8a76 1 12 3    @red @white

SRCHIDLE:SLEEP:DEACTIVATE_ZZ:ONLINE                          8a76 1 13 0    @red @white
SRCHIDLE:SLEEP:DEACTIVATE_ZZ:SLEEP                           8a76 1 13 1    @red @white
SRCHIDLE:SLEEP:DEACTIVATE_ZZ:ACCESS                          8a76 1 13 2    @red @white
SRCHIDLE:SLEEP:DEACTIVATE_ZZ:ASSIGN                          8a76 1 13 3    @red @white

SRCHIDLE:SLEEP:DELAYED_ASET:ONLINE                           8a76 1 14 0    @red @white
SRCHIDLE:SLEEP:DELAYED_ASET:SLEEP                            8a76 1 14 1    @red @white
SRCHIDLE:SLEEP:DELAYED_ASET:ACCESS                           8a76 1 14 2    @red @white
SRCHIDLE:SLEEP:DELAYED_ASET:ASSIGN                           8a76 1 14 3    @red @white

SRCHIDLE:SLEEP:ONLINE_RF_GRANTED:ONLINE                      8a76 1 15 0    @red @white
SRCHIDLE:SLEEP:ONLINE_RF_GRANTED:SLEEP                       8a76 1 15 1    @red @white
SRCHIDLE:SLEEP:ONLINE_RF_GRANTED:ACCESS                      8a76 1 15 2    @red @white
SRCHIDLE:SLEEP:ONLINE_RF_GRANTED:ASSIGN                      8a76 1 15 3    @red @white

SRCHIDLE:SLEEP:ONLINE_TUNE_DONE:ONLINE                       8a76 1 16 0    @red @white
SRCHIDLE:SLEEP:ONLINE_TUNE_DONE:SLEEP                        8a76 1 16 1    @red @white
SRCHIDLE:SLEEP:ONLINE_TUNE_DONE:ACCESS                       8a76 1 16 2    @red @white
SRCHIDLE:SLEEP:ONLINE_TUNE_DONE:ASSIGN                       8a76 1 16 3    @red @white

SRCHIDLE:SLEEP:RX_MOD_GRANTED:ONLINE                         8a76 1 17 0    @red @white
SRCHIDLE:SLEEP:RX_MOD_GRANTED:SLEEP                          8a76 1 17 1    @red @white
SRCHIDLE:SLEEP:RX_MOD_GRANTED:ACCESS                         8a76 1 17 2    @red @white
SRCHIDLE:SLEEP:RX_MOD_GRANTED:ASSIGN                         8a76 1 17 3    @red @white

SRCHIDLE:SLEEP:RX_MOD_DENIED:ONLINE                          8a76 1 18 0    @red @white
SRCHIDLE:SLEEP:RX_MOD_DENIED:SLEEP                           8a76 1 18 1    @red @white
SRCHIDLE:SLEEP:RX_MOD_DENIED:ACCESS                          8a76 1 18 2    @red @white
SRCHIDLE:SLEEP:RX_MOD_DENIED:ASSIGN                          8a76 1 18 3    @red @white

SRCHIDLE:SLEEP:IDLE_DIV_REQUEST:ONLINE                       8a76 1 19 0    @red @white
SRCHIDLE:SLEEP:IDLE_DIV_REQUEST:SLEEP                        8a76 1 19 1    @red @white
SRCHIDLE:SLEEP:IDLE_DIV_REQUEST:ACCESS                       8a76 1 19 2    @red @white
SRCHIDLE:SLEEP:IDLE_DIV_REQUEST:ASSIGN                       8a76 1 19 3    @red @white

SRCHIDLE:SLEEP:PAGE_MATCH:ONLINE                             8a76 1 1a 0    @red @white
SRCHIDLE:SLEEP:PAGE_MATCH:SLEEP                              8a76 1 1a 1    @red @white
SRCHIDLE:SLEEP:PAGE_MATCH:ACCESS                             8a76 1 1a 2    @red @white
SRCHIDLE:SLEEP:PAGE_MATCH:ASSIGN                             8a76 1 1a 3    @red @white

SRCHIDLE:SLEEP:CDMA_RSP:ONLINE                               8a76 1 1b 0    @red @white
SRCHIDLE:SLEEP:CDMA_RSP:SLEEP                                8a76 1 1b 1    @red @white
SRCHIDLE:SLEEP:CDMA_RSP:ACCESS                               8a76 1 1b 2    @red @white
SRCHIDLE:SLEEP:CDMA_RSP:ASSIGN                               8a76 1 1b 3    @red @white

SRCHIDLE:SLEEP:IDLE_RSP:ONLINE                               8a76 1 1c 0    @red @white
SRCHIDLE:SLEEP:IDLE_RSP:SLEEP                                8a76 1 1c 1    @red @white
SRCHIDLE:SLEEP:IDLE_RSP:ACCESS                               8a76 1 1c 2    @red @white
SRCHIDLE:SLEEP:IDLE_RSP:ASSIGN                               8a76 1 1c 3    @red @white

SRCHIDLE:SLEEP:SRCH_PILOT_LIST_F:ONLINE                      8a76 1 1d 0    @red @white
SRCHIDLE:SLEEP:SRCH_PILOT_LIST_F:SLEEP                       8a76 1 1d 1    @red @white
SRCHIDLE:SLEEP:SRCH_PILOT_LIST_F:ACCESS                      8a76 1 1d 2    @red @white
SRCHIDLE:SLEEP:SRCH_PILOT_LIST_F:ASSIGN                      8a76 1 1d 3    @red @white

SRCHIDLE:SLEEP:SRCH_IDLE_FIND_STRONG_PILOT_F:ONLINE          8a76 1 1e 0    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_FIND_STRONG_PILOT_F:SLEEP           8a76 1 1e 1    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_FIND_STRONG_PILOT_F:ACCESS          8a76 1 1e 2    @red @white
SRCHIDLE:SLEEP:SRCH_IDLE_FIND_STRONG_PILOT_F:ASSIGN          8a76 1 1e 3    @red @white

SRCHIDLE:SLEEP:SCAN_ALL_RPT:ONLINE                           8a76 1 1f 0    @red @white
SRCHIDLE:SLEEP:SCAN_ALL_RPT:SLEEP                            8a76 1 1f 1    @red @white
SRCHIDLE:SLEEP:SCAN_ALL_RPT:ACCESS                           8a76 1 1f 2    @red @white
SRCHIDLE:SLEEP:SCAN_ALL_RPT:ASSIGN                           8a76 1 1f 3    @red @white

SRCHIDLE:SLEEP:RF_GRANTED:ONLINE                             8a76 1 20 0    @red @white
SRCHIDLE:SLEEP:RF_GRANTED:SLEEP                              8a76 1 20 1    @red @white
SRCHIDLE:SLEEP:RF_GRANTED:ACCESS                             8a76 1 20 2    @red @white
SRCHIDLE:SLEEP:RF_GRANTED:ASSIGN                             8a76 1 20 3    @red @white

SRCHIDLE:SLEEP:RF_PREP:ONLINE                                8a76 1 21 0    @red @white
SRCHIDLE:SLEEP:RF_PREP:SLEEP                                 8a76 1 21 1    @red @white
SRCHIDLE:SLEEP:RF_PREP:ACCESS                                8a76 1 21 2    @red @white
SRCHIDLE:SLEEP:RF_PREP:ASSIGN                                8a76 1 21 3    @red @white

SRCHIDLE:SLEEP:TUNE_DONE:ONLINE                              8a76 1 22 0    @red @white
SRCHIDLE:SLEEP:TUNE_DONE:SLEEP                               8a76 1 22 1    @red @white
SRCHIDLE:SLEEP:TUNE_DONE:ACCESS                              8a76 1 22 2    @red @white
SRCHIDLE:SLEEP:TUNE_DONE:ASSIGN                              8a76 1 22 3    @red @white

SRCHIDLE:SLEEP:ABORT_COMPLETE:ONLINE                         8a76 1 23 0    @red @white
SRCHIDLE:SLEEP:ABORT_COMPLETE:SLEEP                          8a76 1 23 1    @red @white
SRCHIDLE:SLEEP:ABORT_COMPLETE:ACCESS                         8a76 1 23 2    @red @white
SRCHIDLE:SLEEP:ABORT_COMPLETE:ASSIGN                         8a76 1 23 3    @red @white

SRCHIDLE:ACCESS:SRCH_CDMA_F:ONLINE                           8a76 2 00 0    @red @white
SRCHIDLE:ACCESS:SRCH_CDMA_F:SLEEP                            8a76 2 00 1    @red @white
SRCHIDLE:ACCESS:SRCH_CDMA_F:ACCESS                           8a76 2 00 2    @red @white
SRCHIDLE:ACCESS:SRCH_CDMA_F:ASSIGN                           8a76 2 00 3    @red @white

SRCHIDLE:ACCESS:SRCH_SLEEP_F:ONLINE                          8a76 2 01 0    @red @white
SRCHIDLE:ACCESS:SRCH_SLEEP_F:SLEEP                           8a76 2 01 1    @red @white
SRCHIDLE:ACCESS:SRCH_SLEEP_F:ACCESS                          8a76 2 01 2    @red @white
SRCHIDLE:ACCESS:SRCH_SLEEP_F:ASSIGN                          8a76 2 01 3    @red @white

SRCHIDLE:ACCESS:SRCH_TC_F:ONLINE                             8a76 2 02 0    @red @white
SRCHIDLE:ACCESS:SRCH_TC_F:SLEEP                              8a76 2 02 1    @red @white
SRCHIDLE:ACCESS:SRCH_TC_F:ACCESS                             8a76 2 02 2    @red @white
SRCHIDLE:ACCESS:SRCH_TC_F:ASSIGN                             8a76 2 02 3    @red @white

SRCHIDLE:ACCESS:SRCH_ACCESS_F:ONLINE                         8a76 2 03 0    @red @white
SRCHIDLE:ACCESS:SRCH_ACCESS_F:SLEEP                          8a76 2 03 1    @red @white
SRCHIDLE:ACCESS:SRCH_ACCESS_F:ACCESS                         8a76 2 03 2    @red @white
SRCHIDLE:ACCESS:SRCH_ACCESS_F:ASSIGN                         8a76 2 03 3    @red @white

SRCHIDLE:ACCESS:SRCH_PARM_F:ONLINE                           8a76 2 04 0    @red @white
SRCHIDLE:ACCESS:SRCH_PARM_F:SLEEP                            8a76 2 04 1    @red @white
SRCHIDLE:ACCESS:SRCH_PARM_F:ACCESS                           8a76 2 04 2    @red @white
SRCHIDLE:ACCESS:SRCH_PARM_F:ASSIGN                           8a76 2 04 3    @red @white

SRCHIDLE:ACCESS:SRCH_IDLE_ASET_F:ONLINE                      8a76 2 05 0    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_ASET_F:SLEEP                       8a76 2 05 1    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_ASET_F:ACCESS                      8a76 2 05 2    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_ASET_F:ASSIGN                      8a76 2 05 3    @red @white

SRCHIDLE:ACCESS:SRCH_IDLE_NSET_F:ONLINE                      8a76 2 06 0    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_NSET_F:SLEEP                       8a76 2 06 1    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_NSET_F:ACCESS                      8a76 2 06 2    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_NSET_F:ASSIGN                      8a76 2 06 3    @red @white

SRCHIDLE:ACCESS:SRCH_RESELECT_MEAS_F:ONLINE                  8a76 2 07 0    @red @white
SRCHIDLE:ACCESS:SRCH_RESELECT_MEAS_F:SLEEP                   8a76 2 07 1    @red @white
SRCHIDLE:ACCESS:SRCH_RESELECT_MEAS_F:ACCESS                  8a76 2 07 2    @red @white
SRCHIDLE:ACCESS:SRCH_RESELECT_MEAS_F:ASSIGN                  8a76 2 07 3    @red @white

SRCHIDLE:ACCESS:SRCH_PC_ASSIGN_F:ONLINE                      8a76 2 08 0    @red @white
SRCHIDLE:ACCESS:SRCH_PC_ASSIGN_F:SLEEP                       8a76 2 08 1    @red @white
SRCHIDLE:ACCESS:SRCH_PC_ASSIGN_F:ACCESS                      8a76 2 08 2    @red @white
SRCHIDLE:ACCESS:SRCH_PC_ASSIGN_F:ASSIGN                      8a76 2 08 3    @red @white

SRCHIDLE:ACCESS:SRCH_CHAN_CONFIG_F:ONLINE                    8a76 2 09 0    @red @white
SRCHIDLE:ACCESS:SRCH_CHAN_CONFIG_F:SLEEP                     8a76 2 09 1    @red @white
SRCHIDLE:ACCESS:SRCH_CHAN_CONFIG_F:ACCESS                    8a76 2 09 2    @red @white
SRCHIDLE:ACCESS:SRCH_CHAN_CONFIG_F:ASSIGN                    8a76 2 09 3    @red @white

SRCHIDLE:ACCESS:SRCH_BC_INFO_F:ONLINE                        8a76 2 0a 0    @red @white
SRCHIDLE:ACCESS:SRCH_BC_INFO_F:SLEEP                         8a76 2 0a 1    @red @white
SRCHIDLE:ACCESS:SRCH_BC_INFO_F:ACCESS                        8a76 2 0a 2    @red @white
SRCHIDLE:ACCESS:SRCH_BC_INFO_F:ASSIGN                        8a76 2 0a 3    @red @white

SRCHIDLE:ACCESS:SRCH_IDLE_F:ONLINE                           8a76 2 0b 0    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_F:SLEEP                            8a76 2 0b 1    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_F:ACCESS                           8a76 2 0b 2    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_F:ASSIGN                           8a76 2 0b 3    @red @white

SRCHIDLE:ACCESS:ROLL:ONLINE                                  8a76 2 0c 0    @red @white
SRCHIDLE:ACCESS:ROLL:SLEEP                                   8a76 2 0c 1    @red @white
SRCHIDLE:ACCESS:ROLL:ACCESS                                  8a76 2 0c 2    @red @white
SRCHIDLE:ACCESS:ROLL:ASSIGN                                  8a76 2 0c 3    @red @white

SRCHIDLE:ACCESS:HO_REQUEST:ONLINE                            8a76 2 0d 0    @red @white
SRCHIDLE:ACCESS:HO_REQUEST:SLEEP                             8a76 2 0d 1    @red @white
SRCHIDLE:ACCESS:HO_REQUEST:ACCESS                            8a76 2 0d 2    @red @white
SRCHIDLE:ACCESS:HO_REQUEST:ASSIGN                            8a76 2 0d 3    @red @white

SRCHIDLE:ACCESS:FRAME_STROBE:ONLINE                          8a76 2 0e 0    @red @white
SRCHIDLE:ACCESS:FRAME_STROBE:SLEEP                           8a76 2 0e 1    @red @white
SRCHIDLE:ACCESS:FRAME_STROBE:ACCESS                          8a76 2 0e 2    @red @white
SRCHIDLE:ACCESS:FRAME_STROBE:ASSIGN                          8a76 2 0e 3    @red @white

SRCHIDLE:ACCESS:RESELECTION_CHECK:ONLINE                     8a76 2 0f 0    @red @white
SRCHIDLE:ACCESS:RESELECTION_CHECK:SLEEP                      8a76 2 0f 1    @red @white
SRCHIDLE:ACCESS:RESELECTION_CHECK:ACCESS                     8a76 2 0f 2    @red @white
SRCHIDLE:ACCESS:RESELECTION_CHECK:ASSIGN                     8a76 2 0f 3    @red @white

SRCHIDLE:ACCESS:SLEEP_OK:ONLINE                              8a76 2 10 0    @red @white
SRCHIDLE:ACCESS:SLEEP_OK:SLEEP                               8a76 2 10 1    @red @white
SRCHIDLE:ACCESS:SLEEP_OK:ACCESS                              8a76 2 10 2    @red @white
SRCHIDLE:ACCESS:SLEEP_OK:ASSIGN                              8a76 2 10 3    @red @white

SRCHIDLE:ACCESS:RSSI_TIMER:ONLINE                            8a76 2 11 0    @red @white
SRCHIDLE:ACCESS:RSSI_TIMER:SLEEP                             8a76 2 11 1    @red @white
SRCHIDLE:ACCESS:RSSI_TIMER:ACCESS                            8a76 2 11 2    @red @white
SRCHIDLE:ACCESS:RSSI_TIMER:ASSIGN                            8a76 2 11 3    @red @white

SRCHIDLE:ACCESS:RF_REPORT_TIMER:ONLINE                       8a76 2 12 0    @red @white
SRCHIDLE:ACCESS:RF_REPORT_TIMER:SLEEP                        8a76 2 12 1    @red @white
SRCHIDLE:ACCESS:RF_REPORT_TIMER:ACCESS                       8a76 2 12 2    @red @white
SRCHIDLE:ACCESS:RF_REPORT_TIMER:ASSIGN                       8a76 2 12 3    @red @white

SRCHIDLE:ACCESS:DEACTIVATE_ZZ:ONLINE                         8a76 2 13 0    @red @white
SRCHIDLE:ACCESS:DEACTIVATE_ZZ:SLEEP                          8a76 2 13 1    @red @white
SRCHIDLE:ACCESS:DEACTIVATE_ZZ:ACCESS                         8a76 2 13 2    @red @white
SRCHIDLE:ACCESS:DEACTIVATE_ZZ:ASSIGN                         8a76 2 13 3    @red @white

SRCHIDLE:ACCESS:DELAYED_ASET:ONLINE                          8a76 2 14 0    @red @white
SRCHIDLE:ACCESS:DELAYED_ASET:SLEEP                           8a76 2 14 1    @red @white
SRCHIDLE:ACCESS:DELAYED_ASET:ACCESS                          8a76 2 14 2    @red @white
SRCHIDLE:ACCESS:DELAYED_ASET:ASSIGN                          8a76 2 14 3    @red @white

SRCHIDLE:ACCESS:ONLINE_RF_GRANTED:ONLINE                     8a76 2 15 0    @red @white
SRCHIDLE:ACCESS:ONLINE_RF_GRANTED:SLEEP                      8a76 2 15 1    @red @white
SRCHIDLE:ACCESS:ONLINE_RF_GRANTED:ACCESS                     8a76 2 15 2    @red @white
SRCHIDLE:ACCESS:ONLINE_RF_GRANTED:ASSIGN                     8a76 2 15 3    @red @white

SRCHIDLE:ACCESS:ONLINE_TUNE_DONE:ONLINE                      8a76 2 16 0    @red @white
SRCHIDLE:ACCESS:ONLINE_TUNE_DONE:SLEEP                       8a76 2 16 1    @red @white
SRCHIDLE:ACCESS:ONLINE_TUNE_DONE:ACCESS                      8a76 2 16 2    @red @white
SRCHIDLE:ACCESS:ONLINE_TUNE_DONE:ASSIGN                      8a76 2 16 3    @red @white

SRCHIDLE:ACCESS:RX_MOD_GRANTED:ONLINE                        8a76 2 17 0    @red @white
SRCHIDLE:ACCESS:RX_MOD_GRANTED:SLEEP                         8a76 2 17 1    @red @white
SRCHIDLE:ACCESS:RX_MOD_GRANTED:ACCESS                        8a76 2 17 2    @red @white
SRCHIDLE:ACCESS:RX_MOD_GRANTED:ASSIGN                        8a76 2 17 3    @red @white

SRCHIDLE:ACCESS:RX_MOD_DENIED:ONLINE                         8a76 2 18 0    @red @white
SRCHIDLE:ACCESS:RX_MOD_DENIED:SLEEP                          8a76 2 18 1    @red @white
SRCHIDLE:ACCESS:RX_MOD_DENIED:ACCESS                         8a76 2 18 2    @red @white
SRCHIDLE:ACCESS:RX_MOD_DENIED:ASSIGN                         8a76 2 18 3    @red @white

SRCHIDLE:ACCESS:IDLE_DIV_REQUEST:ONLINE                      8a76 2 19 0    @red @white
SRCHIDLE:ACCESS:IDLE_DIV_REQUEST:SLEEP                       8a76 2 19 1    @red @white
SRCHIDLE:ACCESS:IDLE_DIV_REQUEST:ACCESS                      8a76 2 19 2    @red @white
SRCHIDLE:ACCESS:IDLE_DIV_REQUEST:ASSIGN                      8a76 2 19 3    @red @white

SRCHIDLE:ACCESS:PAGE_MATCH:ONLINE                            8a76 2 1a 0    @red @white
SRCHIDLE:ACCESS:PAGE_MATCH:SLEEP                             8a76 2 1a 1    @red @white
SRCHIDLE:ACCESS:PAGE_MATCH:ACCESS                            8a76 2 1a 2    @red @white
SRCHIDLE:ACCESS:PAGE_MATCH:ASSIGN                            8a76 2 1a 3    @red @white

SRCHIDLE:ACCESS:CDMA_RSP:ONLINE                              8a76 2 1b 0    @red @white
SRCHIDLE:ACCESS:CDMA_RSP:SLEEP                               8a76 2 1b 1    @red @white
SRCHIDLE:ACCESS:CDMA_RSP:ACCESS                              8a76 2 1b 2    @red @white
SRCHIDLE:ACCESS:CDMA_RSP:ASSIGN                              8a76 2 1b 3    @red @white

SRCHIDLE:ACCESS:IDLE_RSP:ONLINE                              8a76 2 1c 0    @red @white
SRCHIDLE:ACCESS:IDLE_RSP:SLEEP                               8a76 2 1c 1    @red @white
SRCHIDLE:ACCESS:IDLE_RSP:ACCESS                              8a76 2 1c 2    @red @white
SRCHIDLE:ACCESS:IDLE_RSP:ASSIGN                              8a76 2 1c 3    @red @white

SRCHIDLE:ACCESS:SRCH_PILOT_LIST_F:ONLINE                     8a76 2 1d 0    @red @white
SRCHIDLE:ACCESS:SRCH_PILOT_LIST_F:SLEEP                      8a76 2 1d 1    @red @white
SRCHIDLE:ACCESS:SRCH_PILOT_LIST_F:ACCESS                     8a76 2 1d 2    @red @white
SRCHIDLE:ACCESS:SRCH_PILOT_LIST_F:ASSIGN                     8a76 2 1d 3    @red @white

SRCHIDLE:ACCESS:SRCH_IDLE_FIND_STRONG_PILOT_F:ONLINE         8a76 2 1e 0    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_FIND_STRONG_PILOT_F:SLEEP          8a76 2 1e 1    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_FIND_STRONG_PILOT_F:ACCESS         8a76 2 1e 2    @red @white
SRCHIDLE:ACCESS:SRCH_IDLE_FIND_STRONG_PILOT_F:ASSIGN         8a76 2 1e 3    @red @white

SRCHIDLE:ACCESS:SCAN_ALL_RPT:ONLINE                          8a76 2 1f 0    @red @white
SRCHIDLE:ACCESS:SCAN_ALL_RPT:SLEEP                           8a76 2 1f 1    @red @white
SRCHIDLE:ACCESS:SCAN_ALL_RPT:ACCESS                          8a76 2 1f 2    @red @white
SRCHIDLE:ACCESS:SCAN_ALL_RPT:ASSIGN                          8a76 2 1f 3    @red @white

SRCHIDLE:ACCESS:RF_GRANTED:ONLINE                            8a76 2 20 0    @red @white
SRCHIDLE:ACCESS:RF_GRANTED:SLEEP                             8a76 2 20 1    @red @white
SRCHIDLE:ACCESS:RF_GRANTED:ACCESS                            8a76 2 20 2    @red @white
SRCHIDLE:ACCESS:RF_GRANTED:ASSIGN                            8a76 2 20 3    @red @white

SRCHIDLE:ACCESS:RF_PREP:ONLINE                               8a76 2 21 0    @red @white
SRCHIDLE:ACCESS:RF_PREP:SLEEP                                8a76 2 21 1    @red @white
SRCHIDLE:ACCESS:RF_PREP:ACCESS                               8a76 2 21 2    @red @white
SRCHIDLE:ACCESS:RF_PREP:ASSIGN                               8a76 2 21 3    @red @white

SRCHIDLE:ACCESS:TUNE_DONE:ONLINE                             8a76 2 22 0    @red @white
SRCHIDLE:ACCESS:TUNE_DONE:SLEEP                              8a76 2 22 1    @red @white
SRCHIDLE:ACCESS:TUNE_DONE:ACCESS                             8a76 2 22 2    @red @white
SRCHIDLE:ACCESS:TUNE_DONE:ASSIGN                             8a76 2 22 3    @red @white

SRCHIDLE:ACCESS:ABORT_COMPLETE:ONLINE                        8a76 2 23 0    @red @white
SRCHIDLE:ACCESS:ABORT_COMPLETE:SLEEP                         8a76 2 23 1    @red @white
SRCHIDLE:ACCESS:ABORT_COMPLETE:ACCESS                        8a76 2 23 2    @red @white
SRCHIDLE:ACCESS:ABORT_COMPLETE:ASSIGN                        8a76 2 23 3    @red @white

SRCHIDLE:ASSIGN:SRCH_CDMA_F:ONLINE                           8a76 3 00 0    @red @white
SRCHIDLE:ASSIGN:SRCH_CDMA_F:SLEEP                            8a76 3 00 1    @red @white
SRCHIDLE:ASSIGN:SRCH_CDMA_F:ACCESS                           8a76 3 00 2    @red @white
SRCHIDLE:ASSIGN:SRCH_CDMA_F:ASSIGN                           8a76 3 00 3    @red @white

SRCHIDLE:ASSIGN:SRCH_SLEEP_F:ONLINE                          8a76 3 01 0    @red @white
SRCHIDLE:ASSIGN:SRCH_SLEEP_F:SLEEP                           8a76 3 01 1    @red @white
SRCHIDLE:ASSIGN:SRCH_SLEEP_F:ACCESS                          8a76 3 01 2    @red @white
SRCHIDLE:ASSIGN:SRCH_SLEEP_F:ASSIGN                          8a76 3 01 3    @red @white

SRCHIDLE:ASSIGN:SRCH_TC_F:ONLINE                             8a76 3 02 0    @red @white
SRCHIDLE:ASSIGN:SRCH_TC_F:SLEEP                              8a76 3 02 1    @red @white
SRCHIDLE:ASSIGN:SRCH_TC_F:ACCESS                             8a76 3 02 2    @red @white
SRCHIDLE:ASSIGN:SRCH_TC_F:ASSIGN                             8a76 3 02 3    @red @white

SRCHIDLE:ASSIGN:SRCH_ACCESS_F:ONLINE                         8a76 3 03 0    @red @white
SRCHIDLE:ASSIGN:SRCH_ACCESS_F:SLEEP                          8a76 3 03 1    @red @white
SRCHIDLE:ASSIGN:SRCH_ACCESS_F:ACCESS                         8a76 3 03 2    @red @white
SRCHIDLE:ASSIGN:SRCH_ACCESS_F:ASSIGN                         8a76 3 03 3    @red @white

SRCHIDLE:ASSIGN:SRCH_PARM_F:ONLINE                           8a76 3 04 0    @red @white
SRCHIDLE:ASSIGN:SRCH_PARM_F:SLEEP                            8a76 3 04 1    @red @white
SRCHIDLE:ASSIGN:SRCH_PARM_F:ACCESS                           8a76 3 04 2    @red @white
SRCHIDLE:ASSIGN:SRCH_PARM_F:ASSIGN                           8a76 3 04 3    @red @white

SRCHIDLE:ASSIGN:SRCH_IDLE_ASET_F:ONLINE                      8a76 3 05 0    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_ASET_F:SLEEP                       8a76 3 05 1    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_ASET_F:ACCESS                      8a76 3 05 2    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_ASET_F:ASSIGN                      8a76 3 05 3    @red @white

SRCHIDLE:ASSIGN:SRCH_IDLE_NSET_F:ONLINE                      8a76 3 06 0    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_NSET_F:SLEEP                       8a76 3 06 1    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_NSET_F:ACCESS                      8a76 3 06 2    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_NSET_F:ASSIGN                      8a76 3 06 3    @red @white

SRCHIDLE:ASSIGN:SRCH_RESELECT_MEAS_F:ONLINE                  8a76 3 07 0    @red @white
SRCHIDLE:ASSIGN:SRCH_RESELECT_MEAS_F:SLEEP                   8a76 3 07 1    @red @white
SRCHIDLE:ASSIGN:SRCH_RESELECT_MEAS_F:ACCESS                  8a76 3 07 2    @red @white
SRCHIDLE:ASSIGN:SRCH_RESELECT_MEAS_F:ASSIGN                  8a76 3 07 3    @red @white

SRCHIDLE:ASSIGN:SRCH_PC_ASSIGN_F:ONLINE                      8a76 3 08 0    @red @white
SRCHIDLE:ASSIGN:SRCH_PC_ASSIGN_F:SLEEP                       8a76 3 08 1    @red @white
SRCHIDLE:ASSIGN:SRCH_PC_ASSIGN_F:ACCESS                      8a76 3 08 2    @red @white
SRCHIDLE:ASSIGN:SRCH_PC_ASSIGN_F:ASSIGN                      8a76 3 08 3    @red @white

SRCHIDLE:ASSIGN:SRCH_CHAN_CONFIG_F:ONLINE                    8a76 3 09 0    @red @white
SRCHIDLE:ASSIGN:SRCH_CHAN_CONFIG_F:SLEEP                     8a76 3 09 1    @red @white
SRCHIDLE:ASSIGN:SRCH_CHAN_CONFIG_F:ACCESS                    8a76 3 09 2    @red @white
SRCHIDLE:ASSIGN:SRCH_CHAN_CONFIG_F:ASSIGN                    8a76 3 09 3    @red @white

SRCHIDLE:ASSIGN:SRCH_BC_INFO_F:ONLINE                        8a76 3 0a 0    @red @white
SRCHIDLE:ASSIGN:SRCH_BC_INFO_F:SLEEP                         8a76 3 0a 1    @red @white
SRCHIDLE:ASSIGN:SRCH_BC_INFO_F:ACCESS                        8a76 3 0a 2    @red @white
SRCHIDLE:ASSIGN:SRCH_BC_INFO_F:ASSIGN                        8a76 3 0a 3    @red @white

SRCHIDLE:ASSIGN:SRCH_IDLE_F:ONLINE                           8a76 3 0b 0    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_F:SLEEP                            8a76 3 0b 1    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_F:ACCESS                           8a76 3 0b 2    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_F:ASSIGN                           8a76 3 0b 3    @red @white

SRCHIDLE:ASSIGN:ROLL:ONLINE                                  8a76 3 0c 0    @red @white
SRCHIDLE:ASSIGN:ROLL:SLEEP                                   8a76 3 0c 1    @red @white
SRCHIDLE:ASSIGN:ROLL:ACCESS                                  8a76 3 0c 2    @red @white
SRCHIDLE:ASSIGN:ROLL:ASSIGN                                  8a76 3 0c 3    @red @white

SRCHIDLE:ASSIGN:HO_REQUEST:ONLINE                            8a76 3 0d 0    @red @white
SRCHIDLE:ASSIGN:HO_REQUEST:SLEEP                             8a76 3 0d 1    @red @white
SRCHIDLE:ASSIGN:HO_REQUEST:ACCESS                            8a76 3 0d 2    @red @white
SRCHIDLE:ASSIGN:HO_REQUEST:ASSIGN                            8a76 3 0d 3    @red @white

SRCHIDLE:ASSIGN:FRAME_STROBE:ONLINE                          8a76 3 0e 0    @red @white
SRCHIDLE:ASSIGN:FRAME_STROBE:SLEEP                           8a76 3 0e 1    @red @white
SRCHIDLE:ASSIGN:FRAME_STROBE:ACCESS                          8a76 3 0e 2    @red @white
SRCHIDLE:ASSIGN:FRAME_STROBE:ASSIGN                          8a76 3 0e 3    @red @white

SRCHIDLE:ASSIGN:RESELECTION_CHECK:ONLINE                     8a76 3 0f 0    @red @white
SRCHIDLE:ASSIGN:RESELECTION_CHECK:SLEEP                      8a76 3 0f 1    @red @white
SRCHIDLE:ASSIGN:RESELECTION_CHECK:ACCESS                     8a76 3 0f 2    @red @white
SRCHIDLE:ASSIGN:RESELECTION_CHECK:ASSIGN                     8a76 3 0f 3    @red @white

SRCHIDLE:ASSIGN:SLEEP_OK:ONLINE                              8a76 3 10 0    @red @white
SRCHIDLE:ASSIGN:SLEEP_OK:SLEEP                               8a76 3 10 1    @red @white
SRCHIDLE:ASSIGN:SLEEP_OK:ACCESS                              8a76 3 10 2    @red @white
SRCHIDLE:ASSIGN:SLEEP_OK:ASSIGN                              8a76 3 10 3    @red @white

SRCHIDLE:ASSIGN:RSSI_TIMER:ONLINE                            8a76 3 11 0    @red @white
SRCHIDLE:ASSIGN:RSSI_TIMER:SLEEP                             8a76 3 11 1    @red @white
SRCHIDLE:ASSIGN:RSSI_TIMER:ACCESS                            8a76 3 11 2    @red @white
SRCHIDLE:ASSIGN:RSSI_TIMER:ASSIGN                            8a76 3 11 3    @red @white

SRCHIDLE:ASSIGN:RF_REPORT_TIMER:ONLINE                       8a76 3 12 0    @red @white
SRCHIDLE:ASSIGN:RF_REPORT_TIMER:SLEEP                        8a76 3 12 1    @red @white
SRCHIDLE:ASSIGN:RF_REPORT_TIMER:ACCESS                       8a76 3 12 2    @red @white
SRCHIDLE:ASSIGN:RF_REPORT_TIMER:ASSIGN                       8a76 3 12 3    @red @white

SRCHIDLE:ASSIGN:DEACTIVATE_ZZ:ONLINE                         8a76 3 13 0    @red @white
SRCHIDLE:ASSIGN:DEACTIVATE_ZZ:SLEEP                          8a76 3 13 1    @red @white
SRCHIDLE:ASSIGN:DEACTIVATE_ZZ:ACCESS                         8a76 3 13 2    @red @white
SRCHIDLE:ASSIGN:DEACTIVATE_ZZ:ASSIGN                         8a76 3 13 3    @red @white

SRCHIDLE:ASSIGN:DELAYED_ASET:ONLINE                          8a76 3 14 0    @red @white
SRCHIDLE:ASSIGN:DELAYED_ASET:SLEEP                           8a76 3 14 1    @red @white
SRCHIDLE:ASSIGN:DELAYED_ASET:ACCESS                          8a76 3 14 2    @red @white
SRCHIDLE:ASSIGN:DELAYED_ASET:ASSIGN                          8a76 3 14 3    @red @white

SRCHIDLE:ASSIGN:ONLINE_RF_GRANTED:ONLINE                     8a76 3 15 0    @red @white
SRCHIDLE:ASSIGN:ONLINE_RF_GRANTED:SLEEP                      8a76 3 15 1    @red @white
SRCHIDLE:ASSIGN:ONLINE_RF_GRANTED:ACCESS                     8a76 3 15 2    @red @white
SRCHIDLE:ASSIGN:ONLINE_RF_GRANTED:ASSIGN                     8a76 3 15 3    @red @white

SRCHIDLE:ASSIGN:ONLINE_TUNE_DONE:ONLINE                      8a76 3 16 0    @red @white
SRCHIDLE:ASSIGN:ONLINE_TUNE_DONE:SLEEP                       8a76 3 16 1    @red @white
SRCHIDLE:ASSIGN:ONLINE_TUNE_DONE:ACCESS                      8a76 3 16 2    @red @white
SRCHIDLE:ASSIGN:ONLINE_TUNE_DONE:ASSIGN                      8a76 3 16 3    @red @white

SRCHIDLE:ASSIGN:RX_MOD_GRANTED:ONLINE                        8a76 3 17 0    @red @white
SRCHIDLE:ASSIGN:RX_MOD_GRANTED:SLEEP                         8a76 3 17 1    @red @white
SRCHIDLE:ASSIGN:RX_MOD_GRANTED:ACCESS                        8a76 3 17 2    @red @white
SRCHIDLE:ASSIGN:RX_MOD_GRANTED:ASSIGN                        8a76 3 17 3    @red @white

SRCHIDLE:ASSIGN:RX_MOD_DENIED:ONLINE                         8a76 3 18 0    @red @white
SRCHIDLE:ASSIGN:RX_MOD_DENIED:SLEEP                          8a76 3 18 1    @red @white
SRCHIDLE:ASSIGN:RX_MOD_DENIED:ACCESS                         8a76 3 18 2    @red @white
SRCHIDLE:ASSIGN:RX_MOD_DENIED:ASSIGN                         8a76 3 18 3    @red @white

SRCHIDLE:ASSIGN:IDLE_DIV_REQUEST:ONLINE                      8a76 3 19 0    @red @white
SRCHIDLE:ASSIGN:IDLE_DIV_REQUEST:SLEEP                       8a76 3 19 1    @red @white
SRCHIDLE:ASSIGN:IDLE_DIV_REQUEST:ACCESS                      8a76 3 19 2    @red @white
SRCHIDLE:ASSIGN:IDLE_DIV_REQUEST:ASSIGN                      8a76 3 19 3    @red @white

SRCHIDLE:ASSIGN:PAGE_MATCH:ONLINE                            8a76 3 1a 0    @red @white
SRCHIDLE:ASSIGN:PAGE_MATCH:SLEEP                             8a76 3 1a 1    @red @white
SRCHIDLE:ASSIGN:PAGE_MATCH:ACCESS                            8a76 3 1a 2    @red @white
SRCHIDLE:ASSIGN:PAGE_MATCH:ASSIGN                            8a76 3 1a 3    @red @white

SRCHIDLE:ASSIGN:CDMA_RSP:ONLINE                              8a76 3 1b 0    @red @white
SRCHIDLE:ASSIGN:CDMA_RSP:SLEEP                               8a76 3 1b 1    @red @white
SRCHIDLE:ASSIGN:CDMA_RSP:ACCESS                              8a76 3 1b 2    @red @white
SRCHIDLE:ASSIGN:CDMA_RSP:ASSIGN                              8a76 3 1b 3    @red @white

SRCHIDLE:ASSIGN:IDLE_RSP:ONLINE                              8a76 3 1c 0    @red @white
SRCHIDLE:ASSIGN:IDLE_RSP:SLEEP                               8a76 3 1c 1    @red @white
SRCHIDLE:ASSIGN:IDLE_RSP:ACCESS                              8a76 3 1c 2    @red @white
SRCHIDLE:ASSIGN:IDLE_RSP:ASSIGN                              8a76 3 1c 3    @red @white

SRCHIDLE:ASSIGN:SRCH_PILOT_LIST_F:ONLINE                     8a76 3 1d 0    @red @white
SRCHIDLE:ASSIGN:SRCH_PILOT_LIST_F:SLEEP                      8a76 3 1d 1    @red @white
SRCHIDLE:ASSIGN:SRCH_PILOT_LIST_F:ACCESS                     8a76 3 1d 2    @red @white
SRCHIDLE:ASSIGN:SRCH_PILOT_LIST_F:ASSIGN                     8a76 3 1d 3    @red @white

SRCHIDLE:ASSIGN:SRCH_IDLE_FIND_STRONG_PILOT_F:ONLINE         8a76 3 1e 0    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_FIND_STRONG_PILOT_F:SLEEP          8a76 3 1e 1    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_FIND_STRONG_PILOT_F:ACCESS         8a76 3 1e 2    @red @white
SRCHIDLE:ASSIGN:SRCH_IDLE_FIND_STRONG_PILOT_F:ASSIGN         8a76 3 1e 3    @red @white

SRCHIDLE:ASSIGN:SCAN_ALL_RPT:ONLINE                          8a76 3 1f 0    @red @white
SRCHIDLE:ASSIGN:SCAN_ALL_RPT:SLEEP                           8a76 3 1f 1    @red @white
SRCHIDLE:ASSIGN:SCAN_ALL_RPT:ACCESS                          8a76 3 1f 2    @red @white
SRCHIDLE:ASSIGN:SCAN_ALL_RPT:ASSIGN                          8a76 3 1f 3    @red @white

SRCHIDLE:ASSIGN:RF_GRANTED:ONLINE                            8a76 3 20 0    @red @white
SRCHIDLE:ASSIGN:RF_GRANTED:SLEEP                             8a76 3 20 1    @red @white
SRCHIDLE:ASSIGN:RF_GRANTED:ACCESS                            8a76 3 20 2    @red @white
SRCHIDLE:ASSIGN:RF_GRANTED:ASSIGN                            8a76 3 20 3    @red @white

SRCHIDLE:ASSIGN:RF_PREP:ONLINE                               8a76 3 21 0    @red @white
SRCHIDLE:ASSIGN:RF_PREP:SLEEP                                8a76 3 21 1    @red @white
SRCHIDLE:ASSIGN:RF_PREP:ACCESS                               8a76 3 21 2    @red @white
SRCHIDLE:ASSIGN:RF_PREP:ASSIGN                               8a76 3 21 3    @red @white

SRCHIDLE:ASSIGN:TUNE_DONE:ONLINE                             8a76 3 22 0    @red @white
SRCHIDLE:ASSIGN:TUNE_DONE:SLEEP                              8a76 3 22 1    @red @white
SRCHIDLE:ASSIGN:TUNE_DONE:ACCESS                             8a76 3 22 2    @red @white
SRCHIDLE:ASSIGN:TUNE_DONE:ASSIGN                             8a76 3 22 3    @red @white

SRCHIDLE:ASSIGN:ABORT_COMPLETE:ONLINE                        8a76 3 23 0    @red @white
SRCHIDLE:ASSIGN:ABORT_COMPLETE:SLEEP                         8a76 3 23 1    @red @white
SRCHIDLE:ASSIGN:ABORT_COMPLETE:ACCESS                        8a76 3 23 2    @red @white
SRCHIDLE:ASSIGN:ABORT_COMPLETE:ASSIGN                        8a76 3 23 3    @red @white



# End machine generated TLA code for state machine: SRCHIDLE_SM

