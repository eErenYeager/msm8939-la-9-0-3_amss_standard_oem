/*=============================================================================

  srchsl_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchsl_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SLEW_SM */

/* Transition function prototypes */
static stm_state_type sl_go_to_cdma(void *);
static stm_state_type sl_go_to_pc(void *);
static stm_state_type sl_handle_tune_comp(void *);
static stm_state_type SLEW_SM_ignore_input(void *);
static stm_state_type sl_handle_roll_in_tuned(void *);
static stm_state_type sl_handle_roll_in_phs_adj(void *);
static stm_state_type sl_handle_wait(void *);
static stm_state_type sl_handle_unlock(void *);
static stm_state_type sl_handle_report(void *);
static stm_state_type sl_handle_done(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type SLEW_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void sl_entry(stm_group_type *group);
static void sl_exit(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define SLEW_SM_NUMBER_OF_STATES 7
#define SLEW_SM_NUMBER_OF_INPUTS 4


/* State enumeration */
enum
{
  SLEW,
  TUNED,
  PHS_ADJ,
  WAIT,
  UNLOCK,
  REPORT,
  DONE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SLEW_SM_states[ SLEW_SM_NUMBER_OF_STATES ] =
{
  {"SLEW", NULL, NULL},
  {"TUNED", NULL, NULL},
  {"PHS_ADJ", NULL, NULL},
  {"WAIT", NULL, NULL},
  {"UNLOCK", NULL, NULL},
  {"REPORT", NULL, NULL},
  {"DONE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SLEW_SM_inputs[ SLEW_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_IDLE_F, "SRCH_IDLE_F"},
  {(stm_input_type)RX_TUNE_COMP_CMD, "RX_TUNE_COMP_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SLEW_SM_transitions[ SLEW_SM_NUMBER_OF_STATES *  SLEW_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state SLEW */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    sl_handle_tune_comp,    /* RX_TUNE_COMP_CMD */
    SLEW_SM_ignore_input,    /* ROLL_CMD */

  /* Transition functions for state TUNED */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_roll_in_tuned,    /* ROLL_CMD */

  /* Transition functions for state PHS_ADJ */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_roll_in_phs_adj,    /* ROLL_CMD */

  /* Transition functions for state WAIT */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_wait,    /* ROLL_CMD */

  /* Transition functions for state UNLOCK */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_unlock,    /* ROLL_CMD */

  /* Transition functions for state REPORT */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_report,    /* ROLL_CMD */

  /* Transition functions for state DONE */
    sl_go_to_cdma,    /* SRCH_CDMA_F */
    sl_go_to_pc,    /* SRCH_IDLE_F */
    NULL,    /* RX_TUNE_COMP_CMD */
    sl_handle_done,    /* ROLL_CMD */

};


/* State machine definition */
stm_state_machine_type SLEW_SM =
{
  "SLEW_SM", /* state machine name */
  63478, /* unique SM id (hash of name) */
  sl_entry, /* state machine entry function */
  sl_exit, /* state machine exit function */
  SLEW, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SLEW_SM_NUMBER_OF_STATES,
  SLEW_SM_NUMBER_OF_INPUTS,
  SLEW_SM_states,
  SLEW_SM_inputs,
  SLEW_SM_transitions,
};

/* End machine generated code for state machine: SLEW_SM */
/* Begin machine generated code for state machine group: SRCH_SLEW_GROUP */

/* State machine group entry/exit function prototypes */



/* State machine group signal mapper function prototype */
static boolean slew_sig_mapper(rex_tcb_type *,rex_sigs_type, stm_sig_cmd_type *);



/* Table of state machines in group */
static stm_state_machine_type *SRCH_SLEW_GROUP_sm_list[ 1 ] =
{
  &SLEW_SM,
};


/* Table of signals handled by the group's signal mapper */
static rex_sigs_type SRCH_SLEW_GROUP_sig_list[ 2 ] =
{
  SRCH_CMD_Q_SIG,
  SRCH_ROLL_ISR_SIG,
};


/* State machine group definition */
stm_group_type SRCH_SLEW_GROUP =
{
  "SRCH_SLEW_GROUP", /* state machine group name */
  SRCH_SLEW_GROUP_sig_list, /* signal mapping table */
  2, /* number of signals in mapping table */
  slew_sig_mapper, /* signal mapper function */
  SRCH_SLEW_GROUP_sm_list, /* state machine table */
  1, /* number of state machines in table */
  srch_wait, /* wait function for group's signals */
  NULL, /* TCB of task that processes group's signals */
  SRCH_INT_CMD_SIG, /* internal command queue signal */
  &srch_int_cmd_q, /* internal command queue */
  NULL, /* delayed internal command queue */
  NULL, /* group entry function */
  NULL, /* group exit function */
  srch_stm_debug_hook, /* debug hook function */
  NULL, /* group heap constructor fn */
  srch_bm_group_heapclean, /* group heap destructor fn */
  srch_bm_malloc, /* group malloc fn */
  srch_bm_free, /* group free fn */
  &SRCH_COMMON_GROUP, /* global group */
  NULL, /* user signal retrieval fn */
  NULL /* user signal handler fn */
};

/* End machine generated code for state machine group: SRCH_SLEW_GROUP */

