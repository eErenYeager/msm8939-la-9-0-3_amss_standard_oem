###############################################################################
#
#    srchzz_is95a_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_is95a_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_IS95A_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_IS95A:INIT:START_SLEEP:INIT                           360a 0 00 0    @red @white
SRCHZZ_IS95A:INIT:START_SLEEP:SLEEP                          360a 0 00 1    @red @white
SRCHZZ_IS95A:INIT:START_SLEEP:WAKEUP                         360a 0 00 2    @red @white
SRCHZZ_IS95A:INIT:START_SLEEP:FAKE_SYNC80                    360a 0 00 3    @red @white
SRCHZZ_IS95A:INIT:START_SLEEP:FAKE_SYNC80_ACQ                360a 0 00 4    @red @white
SRCHZZ_IS95A:INIT:START_SLEEP:WAIT_SB                        360a 0 00 5    @red @white

SRCHZZ_IS95A:INIT:ROLL:INIT                                  360a 0 01 0    @red @white
SRCHZZ_IS95A:INIT:ROLL:SLEEP                                 360a 0 01 1    @red @white
SRCHZZ_IS95A:INIT:ROLL:WAKEUP                                360a 0 01 2    @red @white
SRCHZZ_IS95A:INIT:ROLL:FAKE_SYNC80                           360a 0 01 3    @red @white
SRCHZZ_IS95A:INIT:ROLL:FAKE_SYNC80_ACQ                       360a 0 01 4    @red @white
SRCHZZ_IS95A:INIT:ROLL:WAIT_SB                               360a 0 01 5    @red @white

SRCHZZ_IS95A:INIT:ADJUST_TIMING:INIT                         360a 0 02 0    @red @white
SRCHZZ_IS95A:INIT:ADJUST_TIMING:SLEEP                        360a 0 02 1    @red @white
SRCHZZ_IS95A:INIT:ADJUST_TIMING:WAKEUP                       360a 0 02 2    @red @white
SRCHZZ_IS95A:INIT:ADJUST_TIMING:FAKE_SYNC80                  360a 0 02 3    @red @white
SRCHZZ_IS95A:INIT:ADJUST_TIMING:FAKE_SYNC80_ACQ              360a 0 02 4    @red @white
SRCHZZ_IS95A:INIT:ADJUST_TIMING:WAIT_SB                      360a 0 02 5    @red @white

SRCHZZ_IS95A:INIT:WAKEUP_NOW:INIT                            360a 0 03 0    @red @white
SRCHZZ_IS95A:INIT:WAKEUP_NOW:SLEEP                           360a 0 03 1    @red @white
SRCHZZ_IS95A:INIT:WAKEUP_NOW:WAKEUP                          360a 0 03 2    @red @white
SRCHZZ_IS95A:INIT:WAKEUP_NOW:FAKE_SYNC80                     360a 0 03 3    @red @white
SRCHZZ_IS95A:INIT:WAKEUP_NOW:FAKE_SYNC80_ACQ                 360a 0 03 4    @red @white
SRCHZZ_IS95A:INIT:WAKEUP_NOW:WAIT_SB                         360a 0 03 5    @red @white

SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:INIT                   360a 0 04 0    @red @white
SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:SLEEP                  360a 0 04 1    @red @white
SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:WAKEUP                 360a 0 04 2    @red @white
SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:FAKE_SYNC80            360a 0 04 3    @red @white
SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ        360a 0 04 4    @red @white
SRCHZZ_IS95A:INIT:FREQ_TRACK_OFF_DONE:WAIT_SB                360a 0 04 5    @red @white

SRCHZZ_IS95A:INIT:RX_RF_DISABLED:INIT                        360a 0 05 0    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLED:SLEEP                       360a 0 05 1    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLED:WAKEUP                      360a 0 05 2    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLED:FAKE_SYNC80                 360a 0 05 3    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLED:FAKE_SYNC80_ACQ             360a 0 05 4    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLED:WAIT_SB                     360a 0 05 5    @red @white

SRCHZZ_IS95A:INIT:GO_TO_SLEEP:INIT                           360a 0 06 0    @red @white
SRCHZZ_IS95A:INIT:GO_TO_SLEEP:SLEEP                          360a 0 06 1    @red @white
SRCHZZ_IS95A:INIT:GO_TO_SLEEP:WAKEUP                         360a 0 06 2    @red @white
SRCHZZ_IS95A:INIT:GO_TO_SLEEP:FAKE_SYNC80                    360a 0 06 3    @red @white
SRCHZZ_IS95A:INIT:GO_TO_SLEEP:FAKE_SYNC80_ACQ                360a 0 06 4    @red @white
SRCHZZ_IS95A:INIT:GO_TO_SLEEP:WAIT_SB                        360a 0 06 5    @red @white

SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:INIT                    360a 0 07 0    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:SLEEP                   360a 0 07 1    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:WAKEUP                  360a 0 07 2    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:FAKE_SYNC80             360a 0 07 3    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ         360a 0 07 4    @red @white
SRCHZZ_IS95A:INIT:RX_RF_DISABLE_COMP:WAIT_SB                 360a 0 07 5    @red @white

SRCHZZ_IS95A:INIT:RX_CH_GRANTED:INIT                         360a 0 08 0    @red @white
SRCHZZ_IS95A:INIT:RX_CH_GRANTED:SLEEP                        360a 0 08 1    @red @white
SRCHZZ_IS95A:INIT:RX_CH_GRANTED:WAKEUP                       360a 0 08 2    @red @white
SRCHZZ_IS95A:INIT:RX_CH_GRANTED:FAKE_SYNC80                  360a 0 08 3    @red @white
SRCHZZ_IS95A:INIT:RX_CH_GRANTED:FAKE_SYNC80_ACQ              360a 0 08 4    @red @white
SRCHZZ_IS95A:INIT:RX_CH_GRANTED:WAIT_SB                      360a 0 08 5    @red @white

SRCHZZ_IS95A:INIT:RX_CH_DENIED:INIT                          360a 0 09 0    @red @white
SRCHZZ_IS95A:INIT:RX_CH_DENIED:SLEEP                         360a 0 09 1    @red @white
SRCHZZ_IS95A:INIT:RX_CH_DENIED:WAKEUP                        360a 0 09 2    @red @white
SRCHZZ_IS95A:INIT:RX_CH_DENIED:FAKE_SYNC80                   360a 0 09 3    @red @white
SRCHZZ_IS95A:INIT:RX_CH_DENIED:FAKE_SYNC80_ACQ               360a 0 09 4    @red @white
SRCHZZ_IS95A:INIT:RX_CH_DENIED:WAIT_SB                       360a 0 09 5    @red @white

SRCHZZ_IS95A:INIT:WAKEUP:INIT                                360a 0 0a 0    @red @white
SRCHZZ_IS95A:INIT:WAKEUP:SLEEP                               360a 0 0a 1    @red @white
SRCHZZ_IS95A:INIT:WAKEUP:WAKEUP                              360a 0 0a 2    @red @white
SRCHZZ_IS95A:INIT:WAKEUP:FAKE_SYNC80                         360a 0 0a 3    @red @white
SRCHZZ_IS95A:INIT:WAKEUP:FAKE_SYNC80_ACQ                     360a 0 0a 4    @red @white
SRCHZZ_IS95A:INIT:WAKEUP:WAIT_SB                             360a 0 0a 5    @red @white

SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:INIT                   360a 0 0b 0    @red @white
SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:SLEEP                  360a 0 0b 1    @red @white
SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:WAKEUP                 360a 0 0b 2    @red @white
SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80            360a 0 0b 3    @red @white
SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ        360a 0 0b 4    @red @white
SRCHZZ_IS95A:INIT:RX_RF_RSP_TUNE_COMP:WAIT_SB                360a 0 0b 5    @red @white

SRCHZZ_IS95A:INIT:NO_RF_LOCK:INIT                            360a 0 0c 0    @red @white
SRCHZZ_IS95A:INIT:NO_RF_LOCK:SLEEP                           360a 0 0c 1    @red @white
SRCHZZ_IS95A:INIT:NO_RF_LOCK:WAKEUP                          360a 0 0c 2    @red @white
SRCHZZ_IS95A:INIT:NO_RF_LOCK:FAKE_SYNC80                     360a 0 0c 3    @red @white
SRCHZZ_IS95A:INIT:NO_RF_LOCK:FAKE_SYNC80_ACQ                 360a 0 0c 4    @red @white
SRCHZZ_IS95A:INIT:NO_RF_LOCK:WAIT_SB                         360a 0 0c 5    @red @white

SRCHZZ_IS95A:INIT:CX8_ON:INIT                                360a 0 0d 0    @red @white
SRCHZZ_IS95A:INIT:CX8_ON:SLEEP                               360a 0 0d 1    @red @white
SRCHZZ_IS95A:INIT:CX8_ON:WAKEUP                              360a 0 0d 2    @red @white
SRCHZZ_IS95A:INIT:CX8_ON:FAKE_SYNC80                         360a 0 0d 3    @red @white
SRCHZZ_IS95A:INIT:CX8_ON:FAKE_SYNC80_ACQ                     360a 0 0d 4    @red @white
SRCHZZ_IS95A:INIT:CX8_ON:WAIT_SB                             360a 0 0d 5    @red @white

SRCHZZ_IS95A:INIT:RX_TUNE_COMP:INIT                          360a 0 0e 0    @red @white
SRCHZZ_IS95A:INIT:RX_TUNE_COMP:SLEEP                         360a 0 0e 1    @red @white
SRCHZZ_IS95A:INIT:RX_TUNE_COMP:WAKEUP                        360a 0 0e 2    @red @white
SRCHZZ_IS95A:INIT:RX_TUNE_COMP:FAKE_SYNC80                   360a 0 0e 3    @red @white
SRCHZZ_IS95A:INIT:RX_TUNE_COMP:FAKE_SYNC80_ACQ               360a 0 0e 4    @red @white
SRCHZZ_IS95A:INIT:RX_TUNE_COMP:WAIT_SB                       360a 0 0e 5    @red @white

SRCHZZ_IS95A:SLEEP:START_SLEEP:INIT                          360a 1 00 0    @red @white
SRCHZZ_IS95A:SLEEP:START_SLEEP:SLEEP                         360a 1 00 1    @red @white
SRCHZZ_IS95A:SLEEP:START_SLEEP:WAKEUP                        360a 1 00 2    @red @white
SRCHZZ_IS95A:SLEEP:START_SLEEP:FAKE_SYNC80                   360a 1 00 3    @red @white
SRCHZZ_IS95A:SLEEP:START_SLEEP:FAKE_SYNC80_ACQ               360a 1 00 4    @red @white
SRCHZZ_IS95A:SLEEP:START_SLEEP:WAIT_SB                       360a 1 00 5    @red @white

SRCHZZ_IS95A:SLEEP:ROLL:INIT                                 360a 1 01 0    @red @white
SRCHZZ_IS95A:SLEEP:ROLL:SLEEP                                360a 1 01 1    @red @white
SRCHZZ_IS95A:SLEEP:ROLL:WAKEUP                               360a 1 01 2    @red @white
SRCHZZ_IS95A:SLEEP:ROLL:FAKE_SYNC80                          360a 1 01 3    @red @white
SRCHZZ_IS95A:SLEEP:ROLL:FAKE_SYNC80_ACQ                      360a 1 01 4    @red @white
SRCHZZ_IS95A:SLEEP:ROLL:WAIT_SB                              360a 1 01 5    @red @white

SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:INIT                        360a 1 02 0    @red @white
SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:SLEEP                       360a 1 02 1    @red @white
SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:WAKEUP                      360a 1 02 2    @red @white
SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:FAKE_SYNC80                 360a 1 02 3    @red @white
SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:FAKE_SYNC80_ACQ             360a 1 02 4    @red @white
SRCHZZ_IS95A:SLEEP:ADJUST_TIMING:WAIT_SB                     360a 1 02 5    @red @white

SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:INIT                           360a 1 03 0    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:SLEEP                          360a 1 03 1    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:WAKEUP                         360a 1 03 2    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:FAKE_SYNC80                    360a 1 03 3    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:FAKE_SYNC80_ACQ                360a 1 03 4    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP_NOW:WAIT_SB                        360a 1 03 5    @red @white

SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:INIT                  360a 1 04 0    @red @white
SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:SLEEP                 360a 1 04 1    @red @white
SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:WAKEUP                360a 1 04 2    @red @white
SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:FAKE_SYNC80           360a 1 04 3    @red @white
SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ       360a 1 04 4    @red @white
SRCHZZ_IS95A:SLEEP:FREQ_TRACK_OFF_DONE:WAIT_SB               360a 1 04 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:INIT                       360a 1 05 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:SLEEP                      360a 1 05 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:WAKEUP                     360a 1 05 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:FAKE_SYNC80                360a 1 05 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:FAKE_SYNC80_ACQ            360a 1 05 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLED:WAIT_SB                    360a 1 05 5    @red @white

SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:INIT                          360a 1 06 0    @red @white
SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:SLEEP                         360a 1 06 1    @red @white
SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:WAKEUP                        360a 1 06 2    @red @white
SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:FAKE_SYNC80                   360a 1 06 3    @red @white
SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:FAKE_SYNC80_ACQ               360a 1 06 4    @red @white
SRCHZZ_IS95A:SLEEP:GO_TO_SLEEP:WAIT_SB                       360a 1 06 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:INIT                   360a 1 07 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:SLEEP                  360a 1 07 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:WAKEUP                 360a 1 07 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:FAKE_SYNC80            360a 1 07 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ        360a 1 07 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_DISABLE_COMP:WAIT_SB                360a 1 07 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:INIT                        360a 1 08 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:SLEEP                       360a 1 08 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:WAKEUP                      360a 1 08 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:FAKE_SYNC80                 360a 1 08 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:FAKE_SYNC80_ACQ             360a 1 08 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_GRANTED:WAIT_SB                     360a 1 08 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:INIT                         360a 1 09 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:SLEEP                        360a 1 09 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:WAKEUP                       360a 1 09 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:FAKE_SYNC80                  360a 1 09 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:FAKE_SYNC80_ACQ              360a 1 09 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_CH_DENIED:WAIT_SB                      360a 1 09 5    @red @white

SRCHZZ_IS95A:SLEEP:WAKEUP:INIT                               360a 1 0a 0    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP:SLEEP                              360a 1 0a 1    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP:WAKEUP                             360a 1 0a 2    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP:FAKE_SYNC80                        360a 1 0a 3    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP:FAKE_SYNC80_ACQ                    360a 1 0a 4    @red @white
SRCHZZ_IS95A:SLEEP:WAKEUP:WAIT_SB                            360a 1 0a 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:INIT                  360a 1 0b 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:SLEEP                 360a 1 0b 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:WAKEUP                360a 1 0b 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80           360a 1 0b 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ       360a 1 0b 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_RF_RSP_TUNE_COMP:WAIT_SB               360a 1 0b 5    @red @white

SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:INIT                           360a 1 0c 0    @red @white
SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:SLEEP                          360a 1 0c 1    @red @white
SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:WAKEUP                         360a 1 0c 2    @red @white
SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:FAKE_SYNC80                    360a 1 0c 3    @red @white
SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:FAKE_SYNC80_ACQ                360a 1 0c 4    @red @white
SRCHZZ_IS95A:SLEEP:NO_RF_LOCK:WAIT_SB                        360a 1 0c 5    @red @white

SRCHZZ_IS95A:SLEEP:CX8_ON:INIT                               360a 1 0d 0    @red @white
SRCHZZ_IS95A:SLEEP:CX8_ON:SLEEP                              360a 1 0d 1    @red @white
SRCHZZ_IS95A:SLEEP:CX8_ON:WAKEUP                             360a 1 0d 2    @red @white
SRCHZZ_IS95A:SLEEP:CX8_ON:FAKE_SYNC80                        360a 1 0d 3    @red @white
SRCHZZ_IS95A:SLEEP:CX8_ON:FAKE_SYNC80_ACQ                    360a 1 0d 4    @red @white
SRCHZZ_IS95A:SLEEP:CX8_ON:WAIT_SB                            360a 1 0d 5    @red @white

SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:INIT                         360a 1 0e 0    @red @white
SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:SLEEP                        360a 1 0e 1    @red @white
SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:WAKEUP                       360a 1 0e 2    @red @white
SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:FAKE_SYNC80                  360a 1 0e 3    @red @white
SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:FAKE_SYNC80_ACQ              360a 1 0e 4    @red @white
SRCHZZ_IS95A:SLEEP:RX_TUNE_COMP:WAIT_SB                      360a 1 0e 5    @red @white

SRCHZZ_IS95A:WAKEUP:START_SLEEP:INIT                         360a 2 00 0    @red @white
SRCHZZ_IS95A:WAKEUP:START_SLEEP:SLEEP                        360a 2 00 1    @red @white
SRCHZZ_IS95A:WAKEUP:START_SLEEP:WAKEUP                       360a 2 00 2    @red @white
SRCHZZ_IS95A:WAKEUP:START_SLEEP:FAKE_SYNC80                  360a 2 00 3    @red @white
SRCHZZ_IS95A:WAKEUP:START_SLEEP:FAKE_SYNC80_ACQ              360a 2 00 4    @red @white
SRCHZZ_IS95A:WAKEUP:START_SLEEP:WAIT_SB                      360a 2 00 5    @red @white

SRCHZZ_IS95A:WAKEUP:ROLL:INIT                                360a 2 01 0    @red @white
SRCHZZ_IS95A:WAKEUP:ROLL:SLEEP                               360a 2 01 1    @red @white
SRCHZZ_IS95A:WAKEUP:ROLL:WAKEUP                              360a 2 01 2    @red @white
SRCHZZ_IS95A:WAKEUP:ROLL:FAKE_SYNC80                         360a 2 01 3    @red @white
SRCHZZ_IS95A:WAKEUP:ROLL:FAKE_SYNC80_ACQ                     360a 2 01 4    @red @white
SRCHZZ_IS95A:WAKEUP:ROLL:WAIT_SB                             360a 2 01 5    @red @white

SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:INIT                       360a 2 02 0    @red @white
SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:SLEEP                      360a 2 02 1    @red @white
SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:WAKEUP                     360a 2 02 2    @red @white
SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:FAKE_SYNC80                360a 2 02 3    @red @white
SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:FAKE_SYNC80_ACQ            360a 2 02 4    @red @white
SRCHZZ_IS95A:WAKEUP:ADJUST_TIMING:WAIT_SB                    360a 2 02 5    @red @white

SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:INIT                          360a 2 03 0    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:SLEEP                         360a 2 03 1    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:WAKEUP                        360a 2 03 2    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:FAKE_SYNC80                   360a 2 03 3    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:FAKE_SYNC80_ACQ               360a 2 03 4    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP_NOW:WAIT_SB                       360a 2 03 5    @red @white

SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:INIT                 360a 2 04 0    @red @white
SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:SLEEP                360a 2 04 1    @red @white
SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:WAKEUP               360a 2 04 2    @red @white
SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:FAKE_SYNC80          360a 2 04 3    @red @white
SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ      360a 2 04 4    @red @white
SRCHZZ_IS95A:WAKEUP:FREQ_TRACK_OFF_DONE:WAIT_SB              360a 2 04 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:INIT                      360a 2 05 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:SLEEP                     360a 2 05 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:WAKEUP                    360a 2 05 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:FAKE_SYNC80               360a 2 05 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:FAKE_SYNC80_ACQ           360a 2 05 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLED:WAIT_SB                   360a 2 05 5    @red @white

SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:INIT                         360a 2 06 0    @red @white
SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:SLEEP                        360a 2 06 1    @red @white
SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:WAKEUP                       360a 2 06 2    @red @white
SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:FAKE_SYNC80                  360a 2 06 3    @red @white
SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:FAKE_SYNC80_ACQ              360a 2 06 4    @red @white
SRCHZZ_IS95A:WAKEUP:GO_TO_SLEEP:WAIT_SB                      360a 2 06 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:INIT                  360a 2 07 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:SLEEP                 360a 2 07 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:WAKEUP                360a 2 07 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:FAKE_SYNC80           360a 2 07 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ       360a 2 07 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_DISABLE_COMP:WAIT_SB               360a 2 07 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:INIT                       360a 2 08 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:SLEEP                      360a 2 08 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:WAKEUP                     360a 2 08 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:FAKE_SYNC80                360a 2 08 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:FAKE_SYNC80_ACQ            360a 2 08 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_GRANTED:WAIT_SB                    360a 2 08 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:INIT                        360a 2 09 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:SLEEP                       360a 2 09 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:WAKEUP                      360a 2 09 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:FAKE_SYNC80                 360a 2 09 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:FAKE_SYNC80_ACQ             360a 2 09 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_CH_DENIED:WAIT_SB                     360a 2 09 5    @red @white

SRCHZZ_IS95A:WAKEUP:WAKEUP:INIT                              360a 2 0a 0    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP:SLEEP                             360a 2 0a 1    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP:WAKEUP                            360a 2 0a 2    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP:FAKE_SYNC80                       360a 2 0a 3    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP:FAKE_SYNC80_ACQ                   360a 2 0a 4    @red @white
SRCHZZ_IS95A:WAKEUP:WAKEUP:WAIT_SB                           360a 2 0a 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:INIT                 360a 2 0b 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:SLEEP                360a 2 0b 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:WAKEUP               360a 2 0b 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80          360a 2 0b 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ      360a 2 0b 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_RF_RSP_TUNE_COMP:WAIT_SB              360a 2 0b 5    @red @white

SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:INIT                          360a 2 0c 0    @red @white
SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:SLEEP                         360a 2 0c 1    @red @white
SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:WAKEUP                        360a 2 0c 2    @red @white
SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:FAKE_SYNC80                   360a 2 0c 3    @red @white
SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:FAKE_SYNC80_ACQ               360a 2 0c 4    @red @white
SRCHZZ_IS95A:WAKEUP:NO_RF_LOCK:WAIT_SB                       360a 2 0c 5    @red @white

SRCHZZ_IS95A:WAKEUP:CX8_ON:INIT                              360a 2 0d 0    @red @white
SRCHZZ_IS95A:WAKEUP:CX8_ON:SLEEP                             360a 2 0d 1    @red @white
SRCHZZ_IS95A:WAKEUP:CX8_ON:WAKEUP                            360a 2 0d 2    @red @white
SRCHZZ_IS95A:WAKEUP:CX8_ON:FAKE_SYNC80                       360a 2 0d 3    @red @white
SRCHZZ_IS95A:WAKEUP:CX8_ON:FAKE_SYNC80_ACQ                   360a 2 0d 4    @red @white
SRCHZZ_IS95A:WAKEUP:CX8_ON:WAIT_SB                           360a 2 0d 5    @red @white

SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:INIT                        360a 2 0e 0    @red @white
SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:SLEEP                       360a 2 0e 1    @red @white
SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:WAKEUP                      360a 2 0e 2    @red @white
SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:FAKE_SYNC80                 360a 2 0e 3    @red @white
SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:FAKE_SYNC80_ACQ             360a 2 0e 4    @red @white
SRCHZZ_IS95A:WAKEUP:RX_TUNE_COMP:WAIT_SB                     360a 2 0e 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:INIT                    360a 3 00 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:SLEEP                   360a 3 00 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:WAKEUP                  360a 3 00 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:FAKE_SYNC80             360a 3 00 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:FAKE_SYNC80_ACQ         360a 3 00 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:START_SLEEP:WAIT_SB                 360a 3 00 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:ROLL:INIT                           360a 3 01 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ROLL:SLEEP                          360a 3 01 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ROLL:WAKEUP                         360a 3 01 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ROLL:FAKE_SYNC80                    360a 3 01 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ROLL:FAKE_SYNC80_ACQ                360a 3 01 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ROLL:WAIT_SB                        360a 3 01 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:INIT                  360a 3 02 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:SLEEP                 360a 3 02 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:WAKEUP                360a 3 02 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:FAKE_SYNC80           360a 3 02 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:FAKE_SYNC80_ACQ       360a 3 02 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:ADJUST_TIMING:WAIT_SB               360a 3 02 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:INIT                     360a 3 03 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:SLEEP                    360a 3 03 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:WAKEUP                   360a 3 03 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:FAKE_SYNC80              360a 3 03 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:FAKE_SYNC80_ACQ          360a 3 03 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP_NOW:WAIT_SB                  360a 3 03 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:INIT            360a 3 04 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:SLEEP           360a 3 04 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:WAKEUP          360a 3 04 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:FAKE_SYNC80     360a 3 04 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ 360a 3 04 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:FREQ_TRACK_OFF_DONE:WAIT_SB         360a 3 04 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:INIT                 360a 3 05 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:SLEEP                360a 3 05 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:WAKEUP               360a 3 05 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:FAKE_SYNC80          360a 3 05 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:FAKE_SYNC80_ACQ      360a 3 05 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLED:WAIT_SB              360a 3 05 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:INIT                    360a 3 06 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:SLEEP                   360a 3 06 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:WAKEUP                  360a 3 06 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:FAKE_SYNC80             360a 3 06 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:FAKE_SYNC80_ACQ         360a 3 06 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:GO_TO_SLEEP:WAIT_SB                 360a 3 06 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:INIT             360a 3 07 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:SLEEP            360a 3 07 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:WAKEUP           360a 3 07 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:FAKE_SYNC80      360a 3 07 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ  360a 3 07 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_DISABLE_COMP:WAIT_SB          360a 3 07 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:INIT                  360a 3 08 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:SLEEP                 360a 3 08 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:WAKEUP                360a 3 08 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:FAKE_SYNC80           360a 3 08 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:FAKE_SYNC80_ACQ       360a 3 08 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_GRANTED:WAIT_SB               360a 3 08 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:INIT                   360a 3 09 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:SLEEP                  360a 3 09 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:WAKEUP                 360a 3 09 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:FAKE_SYNC80            360a 3 09 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:FAKE_SYNC80_ACQ        360a 3 09 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_CH_DENIED:WAIT_SB                360a 3 09 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:INIT                         360a 3 0a 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:SLEEP                        360a 3 0a 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:WAKEUP                       360a 3 0a 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:FAKE_SYNC80                  360a 3 0a 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:FAKE_SYNC80_ACQ              360a 3 0a 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:WAKEUP:WAIT_SB                      360a 3 0a 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:INIT            360a 3 0b 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:SLEEP           360a 3 0b 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:WAKEUP          360a 3 0b 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80     360a 3 0b 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ 360a 3 0b 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_RF_RSP_TUNE_COMP:WAIT_SB         360a 3 0b 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:INIT                     360a 3 0c 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:SLEEP                    360a 3 0c 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:WAKEUP                   360a 3 0c 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:FAKE_SYNC80              360a 3 0c 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:FAKE_SYNC80_ACQ          360a 3 0c 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:NO_RF_LOCK:WAIT_SB                  360a 3 0c 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:INIT                         360a 3 0d 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:SLEEP                        360a 3 0d 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:WAKEUP                       360a 3 0d 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:FAKE_SYNC80                  360a 3 0d 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:FAKE_SYNC80_ACQ              360a 3 0d 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:CX8_ON:WAIT_SB                      360a 3 0d 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:INIT                   360a 3 0e 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:SLEEP                  360a 3 0e 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:WAKEUP                 360a 3 0e 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:FAKE_SYNC80            360a 3 0e 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:FAKE_SYNC80_ACQ        360a 3 0e 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80:RX_TUNE_COMP:WAIT_SB                360a 3 0e 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:INIT                360a 4 00 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:SLEEP               360a 4 00 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:WAKEUP              360a 4 00 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:FAKE_SYNC80         360a 4 00 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:FAKE_SYNC80_ACQ     360a 4 00 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:START_SLEEP:WAIT_SB             360a 4 00 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:INIT                       360a 4 01 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:SLEEP                      360a 4 01 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:WAKEUP                     360a 4 01 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:FAKE_SYNC80                360a 4 01 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:FAKE_SYNC80_ACQ            360a 4 01 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ROLL:WAIT_SB                    360a 4 01 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:INIT              360a 4 02 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:SLEEP             360a 4 02 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:WAKEUP            360a 4 02 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:FAKE_SYNC80       360a 4 02 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:FAKE_SYNC80_ACQ   360a 4 02 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:ADJUST_TIMING:WAIT_SB           360a 4 02 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:INIT                 360a 4 03 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:SLEEP                360a 4 03 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:WAKEUP               360a 4 03 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:FAKE_SYNC80          360a 4 03 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:FAKE_SYNC80_ACQ      360a 4 03 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP_NOW:WAIT_SB              360a 4 03 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:INIT        360a 4 04 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:SLEEP       360a 4 04 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:WAKEUP      360a 4 04 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:FAKE_SYNC80 360a 4 04 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ 360a 4 04 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:FREQ_TRACK_OFF_DONE:WAIT_SB     360a 4 04 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:INIT             360a 4 05 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:SLEEP            360a 4 05 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:WAKEUP           360a 4 05 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:FAKE_SYNC80      360a 4 05 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:FAKE_SYNC80_ACQ  360a 4 05 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLED:WAIT_SB          360a 4 05 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:INIT                360a 4 06 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:SLEEP               360a 4 06 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:WAKEUP              360a 4 06 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:FAKE_SYNC80         360a 4 06 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:FAKE_SYNC80_ACQ     360a 4 06 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:GO_TO_SLEEP:WAIT_SB             360a 4 06 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:INIT         360a 4 07 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:SLEEP        360a 4 07 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:WAKEUP       360a 4 07 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:FAKE_SYNC80  360a 4 07 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ 360a 4 07 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_DISABLE_COMP:WAIT_SB      360a 4 07 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:INIT              360a 4 08 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:SLEEP             360a 4 08 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:WAKEUP            360a 4 08 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:FAKE_SYNC80       360a 4 08 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:FAKE_SYNC80_ACQ   360a 4 08 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_GRANTED:WAIT_SB           360a 4 08 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:INIT               360a 4 09 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:SLEEP              360a 4 09 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:WAKEUP             360a 4 09 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:FAKE_SYNC80        360a 4 09 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:FAKE_SYNC80_ACQ    360a 4 09 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_CH_DENIED:WAIT_SB            360a 4 09 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:INIT                     360a 4 0a 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:SLEEP                    360a 4 0a 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:WAKEUP                   360a 4 0a 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:FAKE_SYNC80              360a 4 0a 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:FAKE_SYNC80_ACQ          360a 4 0a 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:WAKEUP:WAIT_SB                  360a 4 0a 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:INIT        360a 4 0b 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:SLEEP       360a 4 0b 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:WAKEUP      360a 4 0b 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80 360a 4 0b 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ 360a 4 0b 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_RF_RSP_TUNE_COMP:WAIT_SB     360a 4 0b 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:INIT                 360a 4 0c 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:SLEEP                360a 4 0c 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:WAKEUP               360a 4 0c 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:FAKE_SYNC80          360a 4 0c 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:FAKE_SYNC80_ACQ      360a 4 0c 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:NO_RF_LOCK:WAIT_SB              360a 4 0c 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:INIT                     360a 4 0d 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:SLEEP                    360a 4 0d 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:WAKEUP                   360a 4 0d 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:FAKE_SYNC80              360a 4 0d 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:FAKE_SYNC80_ACQ          360a 4 0d 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:CX8_ON:WAIT_SB                  360a 4 0d 5    @red @white

SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:INIT               360a 4 0e 0    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:SLEEP              360a 4 0e 1    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:WAKEUP             360a 4 0e 2    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:FAKE_SYNC80        360a 4 0e 3    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:FAKE_SYNC80_ACQ    360a 4 0e 4    @red @white
SRCHZZ_IS95A:FAKE_SYNC80_ACQ:RX_TUNE_COMP:WAIT_SB            360a 4 0e 5    @red @white

SRCHZZ_IS95A:WAIT_SB:START_SLEEP:INIT                        360a 5 00 0    @red @white
SRCHZZ_IS95A:WAIT_SB:START_SLEEP:SLEEP                       360a 5 00 1    @red @white
SRCHZZ_IS95A:WAIT_SB:START_SLEEP:WAKEUP                      360a 5 00 2    @red @white
SRCHZZ_IS95A:WAIT_SB:START_SLEEP:FAKE_SYNC80                 360a 5 00 3    @red @white
SRCHZZ_IS95A:WAIT_SB:START_SLEEP:FAKE_SYNC80_ACQ             360a 5 00 4    @red @white
SRCHZZ_IS95A:WAIT_SB:START_SLEEP:WAIT_SB                     360a 5 00 5    @red @white

SRCHZZ_IS95A:WAIT_SB:ROLL:INIT                               360a 5 01 0    @red @white
SRCHZZ_IS95A:WAIT_SB:ROLL:SLEEP                              360a 5 01 1    @red @white
SRCHZZ_IS95A:WAIT_SB:ROLL:WAKEUP                             360a 5 01 2    @red @white
SRCHZZ_IS95A:WAIT_SB:ROLL:FAKE_SYNC80                        360a 5 01 3    @red @white
SRCHZZ_IS95A:WAIT_SB:ROLL:FAKE_SYNC80_ACQ                    360a 5 01 4    @red @white
SRCHZZ_IS95A:WAIT_SB:ROLL:WAIT_SB                            360a 5 01 5    @red @white

SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:INIT                      360a 5 02 0    @red @white
SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:SLEEP                     360a 5 02 1    @red @white
SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:WAKEUP                    360a 5 02 2    @red @white
SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:FAKE_SYNC80               360a 5 02 3    @red @white
SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:FAKE_SYNC80_ACQ           360a 5 02 4    @red @white
SRCHZZ_IS95A:WAIT_SB:ADJUST_TIMING:WAIT_SB                   360a 5 02 5    @red @white

SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:INIT                         360a 5 03 0    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:SLEEP                        360a 5 03 1    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:WAKEUP                       360a 5 03 2    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:FAKE_SYNC80                  360a 5 03 3    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:FAKE_SYNC80_ACQ              360a 5 03 4    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP_NOW:WAIT_SB                      360a 5 03 5    @red @white

SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:INIT                360a 5 04 0    @red @white
SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:SLEEP               360a 5 04 1    @red @white
SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:WAKEUP              360a 5 04 2    @red @white
SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:FAKE_SYNC80         360a 5 04 3    @red @white
SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:FAKE_SYNC80_ACQ     360a 5 04 4    @red @white
SRCHZZ_IS95A:WAIT_SB:FREQ_TRACK_OFF_DONE:WAIT_SB             360a 5 04 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:INIT                     360a 5 05 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:SLEEP                    360a 5 05 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:WAKEUP                   360a 5 05 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:FAKE_SYNC80              360a 5 05 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:FAKE_SYNC80_ACQ          360a 5 05 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLED:WAIT_SB                  360a 5 05 5    @red @white

SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:INIT                        360a 5 06 0    @red @white
SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:SLEEP                       360a 5 06 1    @red @white
SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:WAKEUP                      360a 5 06 2    @red @white
SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:FAKE_SYNC80                 360a 5 06 3    @red @white
SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:FAKE_SYNC80_ACQ             360a 5 06 4    @red @white
SRCHZZ_IS95A:WAIT_SB:GO_TO_SLEEP:WAIT_SB                     360a 5 06 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:INIT                 360a 5 07 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:SLEEP                360a 5 07 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:WAKEUP               360a 5 07 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:FAKE_SYNC80          360a 5 07 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:FAKE_SYNC80_ACQ      360a 5 07 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_DISABLE_COMP:WAIT_SB              360a 5 07 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:INIT                      360a 5 08 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:SLEEP                     360a 5 08 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:WAKEUP                    360a 5 08 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:FAKE_SYNC80               360a 5 08 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:FAKE_SYNC80_ACQ           360a 5 08 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_GRANTED:WAIT_SB                   360a 5 08 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:INIT                       360a 5 09 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:SLEEP                      360a 5 09 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:WAKEUP                     360a 5 09 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:FAKE_SYNC80                360a 5 09 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:FAKE_SYNC80_ACQ            360a 5 09 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_CH_DENIED:WAIT_SB                    360a 5 09 5    @red @white

SRCHZZ_IS95A:WAIT_SB:WAKEUP:INIT                             360a 5 0a 0    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP:SLEEP                            360a 5 0a 1    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP:WAKEUP                           360a 5 0a 2    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP:FAKE_SYNC80                      360a 5 0a 3    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP:FAKE_SYNC80_ACQ                  360a 5 0a 4    @red @white
SRCHZZ_IS95A:WAIT_SB:WAKEUP:WAIT_SB                          360a 5 0a 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:INIT                360a 5 0b 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:SLEEP               360a 5 0b 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:WAKEUP              360a 5 0b 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80         360a 5 0b 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:FAKE_SYNC80_ACQ     360a 5 0b 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_RF_RSP_TUNE_COMP:WAIT_SB             360a 5 0b 5    @red @white

SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:INIT                         360a 5 0c 0    @red @white
SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:SLEEP                        360a 5 0c 1    @red @white
SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:WAKEUP                       360a 5 0c 2    @red @white
SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:FAKE_SYNC80                  360a 5 0c 3    @red @white
SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:FAKE_SYNC80_ACQ              360a 5 0c 4    @red @white
SRCHZZ_IS95A:WAIT_SB:NO_RF_LOCK:WAIT_SB                      360a 5 0c 5    @red @white

SRCHZZ_IS95A:WAIT_SB:CX8_ON:INIT                             360a 5 0d 0    @red @white
SRCHZZ_IS95A:WAIT_SB:CX8_ON:SLEEP                            360a 5 0d 1    @red @white
SRCHZZ_IS95A:WAIT_SB:CX8_ON:WAKEUP                           360a 5 0d 2    @red @white
SRCHZZ_IS95A:WAIT_SB:CX8_ON:FAKE_SYNC80                      360a 5 0d 3    @red @white
SRCHZZ_IS95A:WAIT_SB:CX8_ON:FAKE_SYNC80_ACQ                  360a 5 0d 4    @red @white
SRCHZZ_IS95A:WAIT_SB:CX8_ON:WAIT_SB                          360a 5 0d 5    @red @white

SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:INIT                       360a 5 0e 0    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:SLEEP                      360a 5 0e 1    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:WAKEUP                     360a 5 0e 2    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:FAKE_SYNC80                360a 5 0e 3    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:FAKE_SYNC80_ACQ            360a 5 0e 4    @red @white
SRCHZZ_IS95A:WAIT_SB:RX_TUNE_COMP:WAIT_SB                    360a 5 0e 5    @red @white



# End machine generated TLA code for state machine: SRCHZZ_IS95A_SM

