/*=============================================================================

  srch_tram_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_tram_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: TRAM_SM */

/* Transition function prototypes */
static stm_state_type tram_queue_request(void *);
static stm_state_type tram_service_request(void *);
static stm_state_type tram_abort_request(void *);
static stm_state_type tram_denied(void *);
static stm_state_type tram_dispatch(void *);
static stm_state_type tram_ignore_pending(void *);


/* State Machine entry/exit function prototypes */
static void tram_entry(stm_group_type *group);
static void tram_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void tram_ready_entry(void *payload, stm_state_type previous_state);
static void tram_trans_entry(void *payload, stm_state_type previous_state);
static void tram_trans_exit(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define TRAM_SM_NUMBER_OF_STATES 2
#define TRAM_SM_NUMBER_OF_INPUTS 9


/* State enumeration */
enum
{
  READY,
  TRANS,
};


/* State name, entry, exit table */
static const stm_state_array_type
  TRAM_SM_states[ TRAM_SM_NUMBER_OF_STATES ] =
{
  {"READY", tram_ready_entry, NULL},
  {"TRANS", tram_trans_entry, tram_trans_exit},
};


/* Input value, name table */
static const stm_input_array_type
  TRAM_SM_inputs[ TRAM_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)TRAM_REQUEST_CMD, "TRAM_REQUEST_CMD"},
  {(stm_input_type)TRAM_TUNE_CMD, "TRAM_TUNE_CMD"},
  {(stm_input_type)TRAM_MODIFY_CMD, "TRAM_MODIFY_CMD"},
  {(stm_input_type)TRAM_DISABLE_CMD, "TRAM_DISABLE_CMD"},
  {(stm_input_type)TRAM_RELEASE_CMD, "TRAM_RELEASE_CMD"},
  {(stm_input_type)TRAM_PENDING_CMD, "TRAM_PENDING_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)RX_MOD_DENIED_CMD, "RX_MOD_DENIED_CMD"},
  {(stm_input_type)TRAM_DISPATCH_CMD, "TRAM_DISPATCH_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  TRAM_SM_transitions[ TRAM_SM_NUMBER_OF_STATES *  TRAM_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state READY */
    tram_queue_request,    /* TRAM_REQUEST_CMD */
    tram_queue_request,    /* TRAM_TUNE_CMD */
    tram_queue_request,    /* TRAM_MODIFY_CMD */
    tram_queue_request,    /* TRAM_DISABLE_CMD */
    tram_queue_request,    /* TRAM_RELEASE_CMD */
    tram_service_request,    /* TRAM_PENDING_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* TRAM_DISPATCH_CMD */

  /* Transition functions for state TRANS */
    tram_queue_request,    /* TRAM_REQUEST_CMD */
    tram_queue_request,    /* TRAM_TUNE_CMD */
    tram_queue_request,    /* TRAM_MODIFY_CMD */
    tram_queue_request,    /* TRAM_DISABLE_CMD */
    tram_abort_request,    /* TRAM_RELEASE_CMD */
    tram_ignore_pending,    /* TRAM_PENDING_CMD */
    tram_denied,    /* RX_CH_DENIED_CMD */
    tram_denied,    /* RX_MOD_DENIED_CMD */
    tram_dispatch,    /* TRAM_DISPATCH_CMD */

};


/* State machine definition */
stm_state_machine_type TRAM_SM =
{
  "TRAM_SM", /* state machine name */
  62706, /* unique SM id (hash of name) */
  tram_entry, /* state machine entry function */
  tram_exit, /* state machine exit function */
  READY, /* state machine initial state */
  TRUE, /* state machine starts active? */
  TRAM_SM_NUMBER_OF_STATES,
  TRAM_SM_NUMBER_OF_INPUTS,
  TRAM_SM_states,
  TRAM_SM_inputs,
  TRAM_SM_transitions,
};

/* End machine generated code for state machine: TRAM_SM */

