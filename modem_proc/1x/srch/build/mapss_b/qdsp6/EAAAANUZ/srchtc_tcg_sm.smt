/*=============================================================================

  srchtc_tcg_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchtc_tcg_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: TCG_SM */

/* Transition function prototypes */
static stm_state_type tcg_request_visit(void *);
static stm_state_type tcg_proc_visit_ok(void *);
static stm_state_type tcg_store_cancel(void *);
static stm_state_type tcg_finish_pre_tune_visit(void *);
static stm_state_type tcg_finish_post_tune_visit(void *);
static stm_state_type tcg_cancel_visit(void *);


/* State Machine entry/exit function prototypes */
static void tcg_entry(stm_group_type *group);
static void tcg_exit(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define TCG_SM_NUMBER_OF_STATES 4
#define TCG_SM_NUMBER_OF_INPUTS 6


/* State enumeration */
enum
{
  TCG_INACTIVE,
  TCG_QUERY_MC,
  TCG_ACTIVE,
  TCG_WAIT_RETRY,
};


/* State name, entry, exit table */
static const stm_state_array_type
  TCG_SM_states[ TCG_SM_NUMBER_OF_STATES ] =
{
  {"TCG_INACTIVE", NULL, NULL},
  {"TCG_QUERY_MC", NULL, NULL},
  {"TCG_ACTIVE", NULL, NULL},
  {"TCG_WAIT_RETRY", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  TCG_SM_inputs[ TCG_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)TCG_VISIT_REQUEST_CMD, "TCG_VISIT_REQUEST_CMD"},
  {(stm_input_type)TCG_MC_RESPONSE_CMD, "TCG_MC_RESPONSE_CMD"},
  {(stm_input_type)TCG_CANCEL_VISIT_REQUEST_CMD, "TCG_CANCEL_VISIT_REQUEST_CMD"},
  {(stm_input_type)TCG_VISIT_DONE_PRE_TUNE_CMD, "TCG_VISIT_DONE_PRE_TUNE_CMD"},
  {(stm_input_type)TCG_VISIT_DONE_POST_TUNE_CMD, "TCG_VISIT_DONE_POST_TUNE_CMD"},
  {(stm_input_type)TCG_RETRY_TIMER_CMD, "TCG_RETRY_TIMER_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  TCG_SM_transitions[ TCG_SM_NUMBER_OF_STATES *  TCG_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state TCG_INACTIVE */
    tcg_request_visit,    /* TCG_VISIT_REQUEST_CMD */
    NULL,    /* TCG_MC_RESPONSE_CMD */
    NULL,    /* TCG_CANCEL_VISIT_REQUEST_CMD */
    NULL,    /* TCG_VISIT_DONE_PRE_TUNE_CMD */
    NULL,    /* TCG_VISIT_DONE_POST_TUNE_CMD */
    NULL,    /* TCG_RETRY_TIMER_CMD */

  /* Transition functions for state TCG_QUERY_MC */
    NULL,    /* TCG_VISIT_REQUEST_CMD */
    tcg_proc_visit_ok,    /* TCG_MC_RESPONSE_CMD */
    tcg_store_cancel,    /* TCG_CANCEL_VISIT_REQUEST_CMD */
    NULL,    /* TCG_VISIT_DONE_PRE_TUNE_CMD */
    NULL,    /* TCG_VISIT_DONE_POST_TUNE_CMD */
    NULL,    /* TCG_RETRY_TIMER_CMD */

  /* Transition functions for state TCG_ACTIVE */
    NULL,    /* TCG_VISIT_REQUEST_CMD */
    NULL,    /* TCG_MC_RESPONSE_CMD */
    NULL,    /* TCG_CANCEL_VISIT_REQUEST_CMD */
    tcg_finish_pre_tune_visit,    /* TCG_VISIT_DONE_PRE_TUNE_CMD */
    tcg_finish_post_tune_visit,    /* TCG_VISIT_DONE_POST_TUNE_CMD */
    NULL,    /* TCG_RETRY_TIMER_CMD */

  /* Transition functions for state TCG_WAIT_RETRY */
    NULL,    /* TCG_VISIT_REQUEST_CMD */
    NULL,    /* TCG_MC_RESPONSE_CMD */
    tcg_cancel_visit,    /* TCG_CANCEL_VISIT_REQUEST_CMD */
    NULL,    /* TCG_VISIT_DONE_PRE_TUNE_CMD */
    NULL,    /* TCG_VISIT_DONE_POST_TUNE_CMD */
    tcg_request_visit,    /* TCG_RETRY_TIMER_CMD */

};


/* State machine definition */
stm_state_machine_type TCG_SM =
{
  "TCG_SM", /* state machine name */
  61090, /* unique SM id (hash of name) */
  tcg_entry, /* state machine entry function */
  tcg_exit, /* state machine exit function */
  TCG_INACTIVE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  TCG_SM_NUMBER_OF_STATES,
  TCG_SM_NUMBER_OF_INPUTS,
  TCG_SM_states,
  TCG_SM_inputs,
  TCG_SM_transitions,
};

/* End machine generated code for state machine: TCG_SM */

