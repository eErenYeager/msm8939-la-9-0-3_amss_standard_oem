###############################################################################
#
#    srchzz_rtl_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_rtl_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_RTL_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_RTL:INIT:START_SLEEP:INIT                             02e5 0 00 0    @blue @white
SRCHZZ_RTL:INIT:START_SLEEP:WAIT_FOR_AFLT_RELEASE            02e5 0 00 1    @blue @white
SRCHZZ_RTL:INIT:START_SLEEP:SLEEP                            02e5 0 00 2    @blue @white
SRCHZZ_RTL:INIT:START_SLEEP:WAIT_FOR_LOCK                    02e5 0 00 3    @blue @white
SRCHZZ_RTL:INIT:START_SLEEP:WAIT_SB                          02e5 0 00 4    @blue @white

SRCHZZ_RTL:INIT:WAKEUP_NOW:INIT                              02e5 0 01 0    @blue @white
SRCHZZ_RTL:INIT:WAKEUP_NOW:WAIT_FOR_AFLT_RELEASE             02e5 0 01 1    @blue @white
SRCHZZ_RTL:INIT:WAKEUP_NOW:SLEEP                             02e5 0 01 2    @blue @white
SRCHZZ_RTL:INIT:WAKEUP_NOW:WAIT_FOR_LOCK                     02e5 0 01 3    @blue @white
SRCHZZ_RTL:INIT:WAKEUP_NOW:WAIT_SB                           02e5 0 01 4    @blue @white

SRCHZZ_RTL:INIT:ADJUST_TIMING:INIT                           02e5 0 02 0    @blue @white
SRCHZZ_RTL:INIT:ADJUST_TIMING:WAIT_FOR_AFLT_RELEASE          02e5 0 02 1    @blue @white
SRCHZZ_RTL:INIT:ADJUST_TIMING:SLEEP                          02e5 0 02 2    @blue @white
SRCHZZ_RTL:INIT:ADJUST_TIMING:WAIT_FOR_LOCK                  02e5 0 02 3    @blue @white
SRCHZZ_RTL:INIT:ADJUST_TIMING:WAIT_SB                        02e5 0 02 4    @blue @white

SRCHZZ_RTL:INIT:FREQ_TRACK_OFF_DONE:INIT                     02e5 0 03 0    @blue @white
SRCHZZ_RTL:INIT:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE    02e5 0 03 1    @blue @white
SRCHZZ_RTL:INIT:FREQ_TRACK_OFF_DONE:SLEEP                    02e5 0 03 2    @blue @white
SRCHZZ_RTL:INIT:FREQ_TRACK_OFF_DONE:WAIT_FOR_LOCK            02e5 0 03 3    @blue @white
SRCHZZ_RTL:INIT:FREQ_TRACK_OFF_DONE:WAIT_SB                  02e5 0 03 4    @blue @white

SRCHZZ_RTL:INIT:RX_RF_DISABLED:INIT                          02e5 0 04 0    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLED:WAIT_FOR_AFLT_RELEASE         02e5 0 04 1    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLED:SLEEP                         02e5 0 04 2    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLED:WAIT_FOR_LOCK                 02e5 0 04 3    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLED:WAIT_SB                       02e5 0 04 4    @blue @white

SRCHZZ_RTL:INIT:GO_TO_SLEEP:INIT                             02e5 0 05 0    @blue @white
SRCHZZ_RTL:INIT:GO_TO_SLEEP:WAIT_FOR_AFLT_RELEASE            02e5 0 05 1    @blue @white
SRCHZZ_RTL:INIT:GO_TO_SLEEP:SLEEP                            02e5 0 05 2    @blue @white
SRCHZZ_RTL:INIT:GO_TO_SLEEP:WAIT_FOR_LOCK                    02e5 0 05 3    @blue @white
SRCHZZ_RTL:INIT:GO_TO_SLEEP:WAIT_SB                          02e5 0 05 4    @blue @white

SRCHZZ_RTL:INIT:RX_RF_DISABLE_COMP:INIT                      02e5 0 06 0    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLE_COMP:WAIT_FOR_AFLT_RELEASE     02e5 0 06 1    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLE_COMP:SLEEP                     02e5 0 06 2    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLE_COMP:WAIT_FOR_LOCK             02e5 0 06 3    @blue @white
SRCHZZ_RTL:INIT:RX_RF_DISABLE_COMP:WAIT_SB                   02e5 0 06 4    @blue @white

SRCHZZ_RTL:INIT:AFLT_RELEASE_DONE:INIT                       02e5 0 07 0    @blue @white
SRCHZZ_RTL:INIT:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE      02e5 0 07 1    @blue @white
SRCHZZ_RTL:INIT:AFLT_RELEASE_DONE:SLEEP                      02e5 0 07 2    @blue @white
SRCHZZ_RTL:INIT:AFLT_RELEASE_DONE:WAIT_FOR_LOCK              02e5 0 07 3    @blue @white
SRCHZZ_RTL:INIT:AFLT_RELEASE_DONE:WAIT_SB                    02e5 0 07 4    @blue @white

SRCHZZ_RTL:INIT:WAKEUP:INIT                                  02e5 0 08 0    @blue @white
SRCHZZ_RTL:INIT:WAKEUP:WAIT_FOR_AFLT_RELEASE                 02e5 0 08 1    @blue @white
SRCHZZ_RTL:INIT:WAKEUP:SLEEP                                 02e5 0 08 2    @blue @white
SRCHZZ_RTL:INIT:WAKEUP:WAIT_FOR_LOCK                         02e5 0 08 3    @blue @white
SRCHZZ_RTL:INIT:WAKEUP:WAIT_SB                               02e5 0 08 4    @blue @white

SRCHZZ_RTL:INIT:NO_RF_LOCK:INIT                              02e5 0 09 0    @blue @white
SRCHZZ_RTL:INIT:NO_RF_LOCK:WAIT_FOR_AFLT_RELEASE             02e5 0 09 1    @blue @white
SRCHZZ_RTL:INIT:NO_RF_LOCK:SLEEP                             02e5 0 09 2    @blue @white
SRCHZZ_RTL:INIT:NO_RF_LOCK:WAIT_FOR_LOCK                     02e5 0 09 3    @blue @white
SRCHZZ_RTL:INIT:NO_RF_LOCK:WAIT_SB                           02e5 0 09 4    @blue @white

SRCHZZ_RTL:INIT:SLEEP_TIMER_EXPIRED:INIT                     02e5 0 0a 0    @blue @white
SRCHZZ_RTL:INIT:SLEEP_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE    02e5 0 0a 1    @blue @white
SRCHZZ_RTL:INIT:SLEEP_TIMER_EXPIRED:SLEEP                    02e5 0 0a 2    @blue @white
SRCHZZ_RTL:INIT:SLEEP_TIMER_EXPIRED:WAIT_FOR_LOCK            02e5 0 0a 3    @blue @white
SRCHZZ_RTL:INIT:SLEEP_TIMER_EXPIRED:WAIT_SB                  02e5 0 0a 4    @blue @white

SRCHZZ_RTL:INIT:RX_CH_GRANTED:INIT                           02e5 0 0b 0    @blue @white
SRCHZZ_RTL:INIT:RX_CH_GRANTED:WAIT_FOR_AFLT_RELEASE          02e5 0 0b 1    @blue @white
SRCHZZ_RTL:INIT:RX_CH_GRANTED:SLEEP                          02e5 0 0b 2    @blue @white
SRCHZZ_RTL:INIT:RX_CH_GRANTED:WAIT_FOR_LOCK                  02e5 0 0b 3    @blue @white
SRCHZZ_RTL:INIT:RX_CH_GRANTED:WAIT_SB                        02e5 0 0b 4    @blue @white

SRCHZZ_RTL:INIT:RX_CH_DENIED:INIT                            02e5 0 0c 0    @blue @white
SRCHZZ_RTL:INIT:RX_CH_DENIED:WAIT_FOR_AFLT_RELEASE           02e5 0 0c 1    @blue @white
SRCHZZ_RTL:INIT:RX_CH_DENIED:SLEEP                           02e5 0 0c 2    @blue @white
SRCHZZ_RTL:INIT:RX_CH_DENIED:WAIT_FOR_LOCK                   02e5 0 0c 3    @blue @white
SRCHZZ_RTL:INIT:RX_CH_DENIED:WAIT_SB                         02e5 0 0c 4    @blue @white

SRCHZZ_RTL:INIT:ROLL:INIT                                    02e5 0 0d 0    @blue @white
SRCHZZ_RTL:INIT:ROLL:WAIT_FOR_AFLT_RELEASE                   02e5 0 0d 1    @blue @white
SRCHZZ_RTL:INIT:ROLL:SLEEP                                   02e5 0 0d 2    @blue @white
SRCHZZ_RTL:INIT:ROLL:WAIT_FOR_LOCK                           02e5 0 0d 3    @blue @white
SRCHZZ_RTL:INIT:ROLL:WAIT_SB                                 02e5 0 0d 4    @blue @white

SRCHZZ_RTL:INIT:SRCH_DUMP:INIT                               02e5 0 0e 0    @blue @white
SRCHZZ_RTL:INIT:SRCH_DUMP:WAIT_FOR_AFLT_RELEASE              02e5 0 0e 1    @blue @white
SRCHZZ_RTL:INIT:SRCH_DUMP:SLEEP                              02e5 0 0e 2    @blue @white
SRCHZZ_RTL:INIT:SRCH_DUMP:WAIT_FOR_LOCK                      02e5 0 0e 3    @blue @white
SRCHZZ_RTL:INIT:SRCH_DUMP:WAIT_SB                            02e5 0 0e 4    @blue @white

SRCHZZ_RTL:INIT:SRCH_LOST_DUMP:INIT                          02e5 0 0f 0    @blue @white
SRCHZZ_RTL:INIT:SRCH_LOST_DUMP:WAIT_FOR_AFLT_RELEASE         02e5 0 0f 1    @blue @white
SRCHZZ_RTL:INIT:SRCH_LOST_DUMP:SLEEP                         02e5 0 0f 2    @blue @white
SRCHZZ_RTL:INIT:SRCH_LOST_DUMP:WAIT_FOR_LOCK                 02e5 0 0f 3    @blue @white
SRCHZZ_RTL:INIT:SRCH_LOST_DUMP:WAIT_SB                       02e5 0 0f 4    @blue @white

SRCHZZ_RTL:INIT:TILL_REACQ_TIMER_EXPIRED:INIT                02e5 0 10 0    @blue @white
SRCHZZ_RTL:INIT:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 0 10 1    @blue @white
SRCHZZ_RTL:INIT:TILL_REACQ_TIMER_EXPIRED:SLEEP               02e5 0 10 2    @blue @white
SRCHZZ_RTL:INIT:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_LOCK       02e5 0 10 3    @blue @white
SRCHZZ_RTL:INIT:TILL_REACQ_TIMER_EXPIRED:WAIT_SB             02e5 0 10 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:START_SLEEP:INIT            02e5 1 00 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:START_SLEEP:WAIT_FOR_AFLT_RELEASE 02e5 1 00 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:START_SLEEP:SLEEP           02e5 1 00 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:START_SLEEP:WAIT_FOR_LOCK   02e5 1 00 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:START_SLEEP:WAIT_SB         02e5 1 00 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP_NOW:INIT             02e5 1 01 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP_NOW:WAIT_FOR_AFLT_RELEASE 02e5 1 01 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP_NOW:SLEEP            02e5 1 01 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP_NOW:WAIT_FOR_LOCK    02e5 1 01 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP_NOW:WAIT_SB          02e5 1 01 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ADJUST_TIMING:INIT          02e5 1 02 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ADJUST_TIMING:WAIT_FOR_AFLT_RELEASE 02e5 1 02 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ADJUST_TIMING:SLEEP         02e5 1 02 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ADJUST_TIMING:WAIT_FOR_LOCK 02e5 1 02 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ADJUST_TIMING:WAIT_SB       02e5 1 02 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:INIT    02e5 1 03 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE 02e5 1 03 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:SLEEP   02e5 1 03 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_FOR_LOCK 02e5 1 03 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:FREQ_TRACK_OFF_DONE:WAIT_SB 02e5 1 03 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLED:INIT         02e5 1 04 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLED:WAIT_FOR_AFLT_RELEASE 02e5 1 04 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLED:SLEEP        02e5 1 04 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLED:WAIT_FOR_LOCK 02e5 1 04 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLED:WAIT_SB      02e5 1 04 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:GO_TO_SLEEP:INIT            02e5 1 05 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:GO_TO_SLEEP:WAIT_FOR_AFLT_RELEASE 02e5 1 05 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:GO_TO_SLEEP:SLEEP           02e5 1 05 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:GO_TO_SLEEP:WAIT_FOR_LOCK   02e5 1 05 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:GO_TO_SLEEP:WAIT_SB         02e5 1 05 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLE_COMP:INIT     02e5 1 06 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLE_COMP:WAIT_FOR_AFLT_RELEASE 02e5 1 06 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLE_COMP:SLEEP    02e5 1 06 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLE_COMP:WAIT_FOR_LOCK 02e5 1 06 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_RF_DISABLE_COMP:WAIT_SB  02e5 1 06 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:INIT      02e5 1 07 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE 02e5 1 07 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:SLEEP     02e5 1 07 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_FOR_LOCK 02e5 1 07 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:AFLT_RELEASE_DONE:WAIT_SB   02e5 1 07 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP:INIT                 02e5 1 08 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP:WAIT_FOR_AFLT_RELEASE 02e5 1 08 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP:SLEEP                02e5 1 08 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP:WAIT_FOR_LOCK        02e5 1 08 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:WAKEUP:WAIT_SB              02e5 1 08 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:NO_RF_LOCK:INIT             02e5 1 09 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:NO_RF_LOCK:WAIT_FOR_AFLT_RELEASE 02e5 1 09 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:NO_RF_LOCK:SLEEP            02e5 1 09 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:NO_RF_LOCK:WAIT_FOR_LOCK    02e5 1 09 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:NO_RF_LOCK:WAIT_SB          02e5 1 09 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SLEEP_TIMER_EXPIRED:INIT    02e5 1 0a 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SLEEP_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 1 0a 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SLEEP_TIMER_EXPIRED:SLEEP   02e5 1 0a 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SLEEP_TIMER_EXPIRED:WAIT_FOR_LOCK 02e5 1 0a 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SLEEP_TIMER_EXPIRED:WAIT_SB 02e5 1 0a 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_GRANTED:INIT          02e5 1 0b 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_GRANTED:WAIT_FOR_AFLT_RELEASE 02e5 1 0b 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_GRANTED:SLEEP         02e5 1 0b 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_GRANTED:WAIT_FOR_LOCK 02e5 1 0b 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_GRANTED:WAIT_SB       02e5 1 0b 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_DENIED:INIT           02e5 1 0c 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_DENIED:WAIT_FOR_AFLT_RELEASE 02e5 1 0c 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_DENIED:SLEEP          02e5 1 0c 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_DENIED:WAIT_FOR_LOCK  02e5 1 0c 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:RX_CH_DENIED:WAIT_SB        02e5 1 0c 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ROLL:INIT                   02e5 1 0d 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ROLL:WAIT_FOR_AFLT_RELEASE  02e5 1 0d 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ROLL:SLEEP                  02e5 1 0d 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ROLL:WAIT_FOR_LOCK          02e5 1 0d 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:ROLL:WAIT_SB                02e5 1 0d 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_DUMP:INIT              02e5 1 0e 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_DUMP:WAIT_FOR_AFLT_RELEASE 02e5 1 0e 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_DUMP:SLEEP             02e5 1 0e 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_DUMP:WAIT_FOR_LOCK     02e5 1 0e 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_DUMP:WAIT_SB           02e5 1 0e 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_LOST_DUMP:INIT         02e5 1 0f 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_LOST_DUMP:WAIT_FOR_AFLT_RELEASE 02e5 1 0f 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_LOST_DUMP:SLEEP        02e5 1 0f 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_LOST_DUMP:WAIT_FOR_LOCK 02e5 1 0f 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:SRCH_LOST_DUMP:WAIT_SB      02e5 1 0f 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:TILL_REACQ_TIMER_EXPIRED:INIT 02e5 1 10 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 1 10 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:TILL_REACQ_TIMER_EXPIRED:SLEEP 02e5 1 10 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_LOCK 02e5 1 10 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_AFLT_RELEASE:TILL_REACQ_TIMER_EXPIRED:WAIT_SB 02e5 1 10 4    @blue @white

SRCHZZ_RTL:SLEEP:START_SLEEP:INIT                            02e5 2 00 0    @blue @white
SRCHZZ_RTL:SLEEP:START_SLEEP:WAIT_FOR_AFLT_RELEASE           02e5 2 00 1    @blue @white
SRCHZZ_RTL:SLEEP:START_SLEEP:SLEEP                           02e5 2 00 2    @blue @white
SRCHZZ_RTL:SLEEP:START_SLEEP:WAIT_FOR_LOCK                   02e5 2 00 3    @blue @white
SRCHZZ_RTL:SLEEP:START_SLEEP:WAIT_SB                         02e5 2 00 4    @blue @white

SRCHZZ_RTL:SLEEP:WAKEUP_NOW:INIT                             02e5 2 01 0    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP_NOW:WAIT_FOR_AFLT_RELEASE            02e5 2 01 1    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP_NOW:SLEEP                            02e5 2 01 2    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP_NOW:WAIT_FOR_LOCK                    02e5 2 01 3    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP_NOW:WAIT_SB                          02e5 2 01 4    @blue @white

SRCHZZ_RTL:SLEEP:ADJUST_TIMING:INIT                          02e5 2 02 0    @blue @white
SRCHZZ_RTL:SLEEP:ADJUST_TIMING:WAIT_FOR_AFLT_RELEASE         02e5 2 02 1    @blue @white
SRCHZZ_RTL:SLEEP:ADJUST_TIMING:SLEEP                         02e5 2 02 2    @blue @white
SRCHZZ_RTL:SLEEP:ADJUST_TIMING:WAIT_FOR_LOCK                 02e5 2 02 3    @blue @white
SRCHZZ_RTL:SLEEP:ADJUST_TIMING:WAIT_SB                       02e5 2 02 4    @blue @white

SRCHZZ_RTL:SLEEP:FREQ_TRACK_OFF_DONE:INIT                    02e5 2 03 0    @blue @white
SRCHZZ_RTL:SLEEP:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE   02e5 2 03 1    @blue @white
SRCHZZ_RTL:SLEEP:FREQ_TRACK_OFF_DONE:SLEEP                   02e5 2 03 2    @blue @white
SRCHZZ_RTL:SLEEP:FREQ_TRACK_OFF_DONE:WAIT_FOR_LOCK           02e5 2 03 3    @blue @white
SRCHZZ_RTL:SLEEP:FREQ_TRACK_OFF_DONE:WAIT_SB                 02e5 2 03 4    @blue @white

SRCHZZ_RTL:SLEEP:RX_RF_DISABLED:INIT                         02e5 2 04 0    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLED:WAIT_FOR_AFLT_RELEASE        02e5 2 04 1    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLED:SLEEP                        02e5 2 04 2    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLED:WAIT_FOR_LOCK                02e5 2 04 3    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLED:WAIT_SB                      02e5 2 04 4    @blue @white

SRCHZZ_RTL:SLEEP:GO_TO_SLEEP:INIT                            02e5 2 05 0    @blue @white
SRCHZZ_RTL:SLEEP:GO_TO_SLEEP:WAIT_FOR_AFLT_RELEASE           02e5 2 05 1    @blue @white
SRCHZZ_RTL:SLEEP:GO_TO_SLEEP:SLEEP                           02e5 2 05 2    @blue @white
SRCHZZ_RTL:SLEEP:GO_TO_SLEEP:WAIT_FOR_LOCK                   02e5 2 05 3    @blue @white
SRCHZZ_RTL:SLEEP:GO_TO_SLEEP:WAIT_SB                         02e5 2 05 4    @blue @white

SRCHZZ_RTL:SLEEP:RX_RF_DISABLE_COMP:INIT                     02e5 2 06 0    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLE_COMP:WAIT_FOR_AFLT_RELEASE    02e5 2 06 1    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLE_COMP:SLEEP                    02e5 2 06 2    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLE_COMP:WAIT_FOR_LOCK            02e5 2 06 3    @blue @white
SRCHZZ_RTL:SLEEP:RX_RF_DISABLE_COMP:WAIT_SB                  02e5 2 06 4    @blue @white

SRCHZZ_RTL:SLEEP:AFLT_RELEASE_DONE:INIT                      02e5 2 07 0    @blue @white
SRCHZZ_RTL:SLEEP:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE     02e5 2 07 1    @blue @white
SRCHZZ_RTL:SLEEP:AFLT_RELEASE_DONE:SLEEP                     02e5 2 07 2    @blue @white
SRCHZZ_RTL:SLEEP:AFLT_RELEASE_DONE:WAIT_FOR_LOCK             02e5 2 07 3    @blue @white
SRCHZZ_RTL:SLEEP:AFLT_RELEASE_DONE:WAIT_SB                   02e5 2 07 4    @blue @white

SRCHZZ_RTL:SLEEP:WAKEUP:INIT                                 02e5 2 08 0    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP:WAIT_FOR_AFLT_RELEASE                02e5 2 08 1    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP:SLEEP                                02e5 2 08 2    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP:WAIT_FOR_LOCK                        02e5 2 08 3    @blue @white
SRCHZZ_RTL:SLEEP:WAKEUP:WAIT_SB                              02e5 2 08 4    @blue @white

SRCHZZ_RTL:SLEEP:NO_RF_LOCK:INIT                             02e5 2 09 0    @blue @white
SRCHZZ_RTL:SLEEP:NO_RF_LOCK:WAIT_FOR_AFLT_RELEASE            02e5 2 09 1    @blue @white
SRCHZZ_RTL:SLEEP:NO_RF_LOCK:SLEEP                            02e5 2 09 2    @blue @white
SRCHZZ_RTL:SLEEP:NO_RF_LOCK:WAIT_FOR_LOCK                    02e5 2 09 3    @blue @white
SRCHZZ_RTL:SLEEP:NO_RF_LOCK:WAIT_SB                          02e5 2 09 4    @blue @white

SRCHZZ_RTL:SLEEP:SLEEP_TIMER_EXPIRED:INIT                    02e5 2 0a 0    @blue @white
SRCHZZ_RTL:SLEEP:SLEEP_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE   02e5 2 0a 1    @blue @white
SRCHZZ_RTL:SLEEP:SLEEP_TIMER_EXPIRED:SLEEP                   02e5 2 0a 2    @blue @white
SRCHZZ_RTL:SLEEP:SLEEP_TIMER_EXPIRED:WAIT_FOR_LOCK           02e5 2 0a 3    @blue @white
SRCHZZ_RTL:SLEEP:SLEEP_TIMER_EXPIRED:WAIT_SB                 02e5 2 0a 4    @blue @white

SRCHZZ_RTL:SLEEP:RX_CH_GRANTED:INIT                          02e5 2 0b 0    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_GRANTED:WAIT_FOR_AFLT_RELEASE         02e5 2 0b 1    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_GRANTED:SLEEP                         02e5 2 0b 2    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_GRANTED:WAIT_FOR_LOCK                 02e5 2 0b 3    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_GRANTED:WAIT_SB                       02e5 2 0b 4    @blue @white

SRCHZZ_RTL:SLEEP:RX_CH_DENIED:INIT                           02e5 2 0c 0    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_DENIED:WAIT_FOR_AFLT_RELEASE          02e5 2 0c 1    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_DENIED:SLEEP                          02e5 2 0c 2    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_DENIED:WAIT_FOR_LOCK                  02e5 2 0c 3    @blue @white
SRCHZZ_RTL:SLEEP:RX_CH_DENIED:WAIT_SB                        02e5 2 0c 4    @blue @white

SRCHZZ_RTL:SLEEP:ROLL:INIT                                   02e5 2 0d 0    @blue @white
SRCHZZ_RTL:SLEEP:ROLL:WAIT_FOR_AFLT_RELEASE                  02e5 2 0d 1    @blue @white
SRCHZZ_RTL:SLEEP:ROLL:SLEEP                                  02e5 2 0d 2    @blue @white
SRCHZZ_RTL:SLEEP:ROLL:WAIT_FOR_LOCK                          02e5 2 0d 3    @blue @white
SRCHZZ_RTL:SLEEP:ROLL:WAIT_SB                                02e5 2 0d 4    @blue @white

SRCHZZ_RTL:SLEEP:SRCH_DUMP:INIT                              02e5 2 0e 0    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_DUMP:WAIT_FOR_AFLT_RELEASE             02e5 2 0e 1    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_DUMP:SLEEP                             02e5 2 0e 2    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_DUMP:WAIT_FOR_LOCK                     02e5 2 0e 3    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_DUMP:WAIT_SB                           02e5 2 0e 4    @blue @white

SRCHZZ_RTL:SLEEP:SRCH_LOST_DUMP:INIT                         02e5 2 0f 0    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_LOST_DUMP:WAIT_FOR_AFLT_RELEASE        02e5 2 0f 1    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_LOST_DUMP:SLEEP                        02e5 2 0f 2    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_LOST_DUMP:WAIT_FOR_LOCK                02e5 2 0f 3    @blue @white
SRCHZZ_RTL:SLEEP:SRCH_LOST_DUMP:WAIT_SB                      02e5 2 0f 4    @blue @white

SRCHZZ_RTL:SLEEP:TILL_REACQ_TIMER_EXPIRED:INIT               02e5 2 10 0    @blue @white
SRCHZZ_RTL:SLEEP:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 2 10 1    @blue @white
SRCHZZ_RTL:SLEEP:TILL_REACQ_TIMER_EXPIRED:SLEEP              02e5 2 10 2    @blue @white
SRCHZZ_RTL:SLEEP:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_LOCK      02e5 2 10 3    @blue @white
SRCHZZ_RTL:SLEEP:TILL_REACQ_TIMER_EXPIRED:WAIT_SB            02e5 2 10 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:START_SLEEP:INIT                    02e5 3 00 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:START_SLEEP:WAIT_FOR_AFLT_RELEASE   02e5 3 00 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:START_SLEEP:SLEEP                   02e5 3 00 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:START_SLEEP:WAIT_FOR_LOCK           02e5 3 00 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:START_SLEEP:WAIT_SB                 02e5 3 00 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP_NOW:INIT                     02e5 3 01 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP_NOW:WAIT_FOR_AFLT_RELEASE    02e5 3 01 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP_NOW:SLEEP                    02e5 3 01 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP_NOW:WAIT_FOR_LOCK            02e5 3 01 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP_NOW:WAIT_SB                  02e5 3 01 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:ADJUST_TIMING:INIT                  02e5 3 02 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ADJUST_TIMING:WAIT_FOR_AFLT_RELEASE 02e5 3 02 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ADJUST_TIMING:SLEEP                 02e5 3 02 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ADJUST_TIMING:WAIT_FOR_LOCK         02e5 3 02 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ADJUST_TIMING:WAIT_SB               02e5 3 02 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:FREQ_TRACK_OFF_DONE:INIT            02e5 3 03 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE 02e5 3 03 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:FREQ_TRACK_OFF_DONE:SLEEP           02e5 3 03 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:FREQ_TRACK_OFF_DONE:WAIT_FOR_LOCK   02e5 3 03 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:FREQ_TRACK_OFF_DONE:WAIT_SB         02e5 3 03 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLED:INIT                 02e5 3 04 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLED:WAIT_FOR_AFLT_RELEASE 02e5 3 04 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLED:SLEEP                02e5 3 04 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLED:WAIT_FOR_LOCK        02e5 3 04 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLED:WAIT_SB              02e5 3 04 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:GO_TO_SLEEP:INIT                    02e5 3 05 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:GO_TO_SLEEP:WAIT_FOR_AFLT_RELEASE   02e5 3 05 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:GO_TO_SLEEP:SLEEP                   02e5 3 05 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:GO_TO_SLEEP:WAIT_FOR_LOCK           02e5 3 05 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:GO_TO_SLEEP:WAIT_SB                 02e5 3 05 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLE_COMP:INIT             02e5 3 06 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLE_COMP:WAIT_FOR_AFLT_RELEASE 02e5 3 06 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLE_COMP:SLEEP            02e5 3 06 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLE_COMP:WAIT_FOR_LOCK    02e5 3 06 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_RF_DISABLE_COMP:WAIT_SB          02e5 3 06 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:AFLT_RELEASE_DONE:INIT              02e5 3 07 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE 02e5 3 07 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:AFLT_RELEASE_DONE:SLEEP             02e5 3 07 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:AFLT_RELEASE_DONE:WAIT_FOR_LOCK     02e5 3 07 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:AFLT_RELEASE_DONE:WAIT_SB           02e5 3 07 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP:INIT                         02e5 3 08 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP:WAIT_FOR_AFLT_RELEASE        02e5 3 08 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP:SLEEP                        02e5 3 08 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP:WAIT_FOR_LOCK                02e5 3 08 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:WAKEUP:WAIT_SB                      02e5 3 08 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:NO_RF_LOCK:INIT                     02e5 3 09 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:NO_RF_LOCK:WAIT_FOR_AFLT_RELEASE    02e5 3 09 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:NO_RF_LOCK:SLEEP                    02e5 3 09 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:NO_RF_LOCK:WAIT_FOR_LOCK            02e5 3 09 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:NO_RF_LOCK:WAIT_SB                  02e5 3 09 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:SLEEP_TIMER_EXPIRED:INIT            02e5 3 0a 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SLEEP_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 3 0a 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SLEEP_TIMER_EXPIRED:SLEEP           02e5 3 0a 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SLEEP_TIMER_EXPIRED:WAIT_FOR_LOCK   02e5 3 0a 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SLEEP_TIMER_EXPIRED:WAIT_SB         02e5 3 0a 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_GRANTED:INIT                  02e5 3 0b 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_GRANTED:WAIT_FOR_AFLT_RELEASE 02e5 3 0b 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_GRANTED:SLEEP                 02e5 3 0b 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_GRANTED:WAIT_FOR_LOCK         02e5 3 0b 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_GRANTED:WAIT_SB               02e5 3 0b 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_DENIED:INIT                   02e5 3 0c 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_DENIED:WAIT_FOR_AFLT_RELEASE  02e5 3 0c 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_DENIED:SLEEP                  02e5 3 0c 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_DENIED:WAIT_FOR_LOCK          02e5 3 0c 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:RX_CH_DENIED:WAIT_SB                02e5 3 0c 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:ROLL:INIT                           02e5 3 0d 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ROLL:WAIT_FOR_AFLT_RELEASE          02e5 3 0d 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ROLL:SLEEP                          02e5 3 0d 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ROLL:WAIT_FOR_LOCK                  02e5 3 0d 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:ROLL:WAIT_SB                        02e5 3 0d 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_DUMP:INIT                      02e5 3 0e 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_DUMP:WAIT_FOR_AFLT_RELEASE     02e5 3 0e 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_DUMP:SLEEP                     02e5 3 0e 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_DUMP:WAIT_FOR_LOCK             02e5 3 0e 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_DUMP:WAIT_SB                   02e5 3 0e 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_LOST_DUMP:INIT                 02e5 3 0f 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_LOST_DUMP:WAIT_FOR_AFLT_RELEASE 02e5 3 0f 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_LOST_DUMP:SLEEP                02e5 3 0f 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_LOST_DUMP:WAIT_FOR_LOCK        02e5 3 0f 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:SRCH_LOST_DUMP:WAIT_SB              02e5 3 0f 4    @blue @white

SRCHZZ_RTL:WAIT_FOR_LOCK:TILL_REACQ_TIMER_EXPIRED:INIT       02e5 3 10 0    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 3 10 1    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:TILL_REACQ_TIMER_EXPIRED:SLEEP      02e5 3 10 2    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_LOCK 02e5 3 10 3    @blue @white
SRCHZZ_RTL:WAIT_FOR_LOCK:TILL_REACQ_TIMER_EXPIRED:WAIT_SB    02e5 3 10 4    @blue @white

SRCHZZ_RTL:WAIT_SB:START_SLEEP:INIT                          02e5 4 00 0    @blue @white
SRCHZZ_RTL:WAIT_SB:START_SLEEP:WAIT_FOR_AFLT_RELEASE         02e5 4 00 1    @blue @white
SRCHZZ_RTL:WAIT_SB:START_SLEEP:SLEEP                         02e5 4 00 2    @blue @white
SRCHZZ_RTL:WAIT_SB:START_SLEEP:WAIT_FOR_LOCK                 02e5 4 00 3    @blue @white
SRCHZZ_RTL:WAIT_SB:START_SLEEP:WAIT_SB                       02e5 4 00 4    @blue @white

SRCHZZ_RTL:WAIT_SB:WAKEUP_NOW:INIT                           02e5 4 01 0    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP_NOW:WAIT_FOR_AFLT_RELEASE          02e5 4 01 1    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP_NOW:SLEEP                          02e5 4 01 2    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP_NOW:WAIT_FOR_LOCK                  02e5 4 01 3    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP_NOW:WAIT_SB                        02e5 4 01 4    @blue @white

SRCHZZ_RTL:WAIT_SB:ADJUST_TIMING:INIT                        02e5 4 02 0    @blue @white
SRCHZZ_RTL:WAIT_SB:ADJUST_TIMING:WAIT_FOR_AFLT_RELEASE       02e5 4 02 1    @blue @white
SRCHZZ_RTL:WAIT_SB:ADJUST_TIMING:SLEEP                       02e5 4 02 2    @blue @white
SRCHZZ_RTL:WAIT_SB:ADJUST_TIMING:WAIT_FOR_LOCK               02e5 4 02 3    @blue @white
SRCHZZ_RTL:WAIT_SB:ADJUST_TIMING:WAIT_SB                     02e5 4 02 4    @blue @white

SRCHZZ_RTL:WAIT_SB:FREQ_TRACK_OFF_DONE:INIT                  02e5 4 03 0    @blue @white
SRCHZZ_RTL:WAIT_SB:FREQ_TRACK_OFF_DONE:WAIT_FOR_AFLT_RELEASE 02e5 4 03 1    @blue @white
SRCHZZ_RTL:WAIT_SB:FREQ_TRACK_OFF_DONE:SLEEP                 02e5 4 03 2    @blue @white
SRCHZZ_RTL:WAIT_SB:FREQ_TRACK_OFF_DONE:WAIT_FOR_LOCK         02e5 4 03 3    @blue @white
SRCHZZ_RTL:WAIT_SB:FREQ_TRACK_OFF_DONE:WAIT_SB               02e5 4 03 4    @blue @white

SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLED:INIT                       02e5 4 04 0    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLED:WAIT_FOR_AFLT_RELEASE      02e5 4 04 1    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLED:SLEEP                      02e5 4 04 2    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLED:WAIT_FOR_LOCK              02e5 4 04 3    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLED:WAIT_SB                    02e5 4 04 4    @blue @white

SRCHZZ_RTL:WAIT_SB:GO_TO_SLEEP:INIT                          02e5 4 05 0    @blue @white
SRCHZZ_RTL:WAIT_SB:GO_TO_SLEEP:WAIT_FOR_AFLT_RELEASE         02e5 4 05 1    @blue @white
SRCHZZ_RTL:WAIT_SB:GO_TO_SLEEP:SLEEP                         02e5 4 05 2    @blue @white
SRCHZZ_RTL:WAIT_SB:GO_TO_SLEEP:WAIT_FOR_LOCK                 02e5 4 05 3    @blue @white
SRCHZZ_RTL:WAIT_SB:GO_TO_SLEEP:WAIT_SB                       02e5 4 05 4    @blue @white

SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLE_COMP:INIT                   02e5 4 06 0    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLE_COMP:WAIT_FOR_AFLT_RELEASE  02e5 4 06 1    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLE_COMP:SLEEP                  02e5 4 06 2    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLE_COMP:WAIT_FOR_LOCK          02e5 4 06 3    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_RF_DISABLE_COMP:WAIT_SB                02e5 4 06 4    @blue @white

SRCHZZ_RTL:WAIT_SB:AFLT_RELEASE_DONE:INIT                    02e5 4 07 0    @blue @white
SRCHZZ_RTL:WAIT_SB:AFLT_RELEASE_DONE:WAIT_FOR_AFLT_RELEASE   02e5 4 07 1    @blue @white
SRCHZZ_RTL:WAIT_SB:AFLT_RELEASE_DONE:SLEEP                   02e5 4 07 2    @blue @white
SRCHZZ_RTL:WAIT_SB:AFLT_RELEASE_DONE:WAIT_FOR_LOCK           02e5 4 07 3    @blue @white
SRCHZZ_RTL:WAIT_SB:AFLT_RELEASE_DONE:WAIT_SB                 02e5 4 07 4    @blue @white

SRCHZZ_RTL:WAIT_SB:WAKEUP:INIT                               02e5 4 08 0    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP:WAIT_FOR_AFLT_RELEASE              02e5 4 08 1    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP:SLEEP                              02e5 4 08 2    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP:WAIT_FOR_LOCK                      02e5 4 08 3    @blue @white
SRCHZZ_RTL:WAIT_SB:WAKEUP:WAIT_SB                            02e5 4 08 4    @blue @white

SRCHZZ_RTL:WAIT_SB:NO_RF_LOCK:INIT                           02e5 4 09 0    @blue @white
SRCHZZ_RTL:WAIT_SB:NO_RF_LOCK:WAIT_FOR_AFLT_RELEASE          02e5 4 09 1    @blue @white
SRCHZZ_RTL:WAIT_SB:NO_RF_LOCK:SLEEP                          02e5 4 09 2    @blue @white
SRCHZZ_RTL:WAIT_SB:NO_RF_LOCK:WAIT_FOR_LOCK                  02e5 4 09 3    @blue @white
SRCHZZ_RTL:WAIT_SB:NO_RF_LOCK:WAIT_SB                        02e5 4 09 4    @blue @white

SRCHZZ_RTL:WAIT_SB:SLEEP_TIMER_EXPIRED:INIT                  02e5 4 0a 0    @blue @white
SRCHZZ_RTL:WAIT_SB:SLEEP_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 4 0a 1    @blue @white
SRCHZZ_RTL:WAIT_SB:SLEEP_TIMER_EXPIRED:SLEEP                 02e5 4 0a 2    @blue @white
SRCHZZ_RTL:WAIT_SB:SLEEP_TIMER_EXPIRED:WAIT_FOR_LOCK         02e5 4 0a 3    @blue @white
SRCHZZ_RTL:WAIT_SB:SLEEP_TIMER_EXPIRED:WAIT_SB               02e5 4 0a 4    @blue @white

SRCHZZ_RTL:WAIT_SB:RX_CH_GRANTED:INIT                        02e5 4 0b 0    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_GRANTED:WAIT_FOR_AFLT_RELEASE       02e5 4 0b 1    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_GRANTED:SLEEP                       02e5 4 0b 2    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_GRANTED:WAIT_FOR_LOCK               02e5 4 0b 3    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_GRANTED:WAIT_SB                     02e5 4 0b 4    @blue @white

SRCHZZ_RTL:WAIT_SB:RX_CH_DENIED:INIT                         02e5 4 0c 0    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_DENIED:WAIT_FOR_AFLT_RELEASE        02e5 4 0c 1    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_DENIED:SLEEP                        02e5 4 0c 2    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_DENIED:WAIT_FOR_LOCK                02e5 4 0c 3    @blue @white
SRCHZZ_RTL:WAIT_SB:RX_CH_DENIED:WAIT_SB                      02e5 4 0c 4    @blue @white

SRCHZZ_RTL:WAIT_SB:ROLL:INIT                                 02e5 4 0d 0    @blue @white
SRCHZZ_RTL:WAIT_SB:ROLL:WAIT_FOR_AFLT_RELEASE                02e5 4 0d 1    @blue @white
SRCHZZ_RTL:WAIT_SB:ROLL:SLEEP                                02e5 4 0d 2    @blue @white
SRCHZZ_RTL:WAIT_SB:ROLL:WAIT_FOR_LOCK                        02e5 4 0d 3    @blue @white
SRCHZZ_RTL:WAIT_SB:ROLL:WAIT_SB                              02e5 4 0d 4    @blue @white

SRCHZZ_RTL:WAIT_SB:SRCH_DUMP:INIT                            02e5 4 0e 0    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_DUMP:WAIT_FOR_AFLT_RELEASE           02e5 4 0e 1    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_DUMP:SLEEP                           02e5 4 0e 2    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_DUMP:WAIT_FOR_LOCK                   02e5 4 0e 3    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_DUMP:WAIT_SB                         02e5 4 0e 4    @blue @white

SRCHZZ_RTL:WAIT_SB:SRCH_LOST_DUMP:INIT                       02e5 4 0f 0    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_LOST_DUMP:WAIT_FOR_AFLT_RELEASE      02e5 4 0f 1    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_LOST_DUMP:SLEEP                      02e5 4 0f 2    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_LOST_DUMP:WAIT_FOR_LOCK              02e5 4 0f 3    @blue @white
SRCHZZ_RTL:WAIT_SB:SRCH_LOST_DUMP:WAIT_SB                    02e5 4 0f 4    @blue @white

SRCHZZ_RTL:WAIT_SB:TILL_REACQ_TIMER_EXPIRED:INIT             02e5 4 10 0    @blue @white
SRCHZZ_RTL:WAIT_SB:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_AFLT_RELEASE 02e5 4 10 1    @blue @white
SRCHZZ_RTL:WAIT_SB:TILL_REACQ_TIMER_EXPIRED:SLEEP            02e5 4 10 2    @blue @white
SRCHZZ_RTL:WAIT_SB:TILL_REACQ_TIMER_EXPIRED:WAIT_FOR_LOCK    02e5 4 10 3    @blue @white
SRCHZZ_RTL:WAIT_SB:TILL_REACQ_TIMER_EXPIRED:WAIT_SB          02e5 4 10 4    @blue @white



# End machine generated TLA code for state machine: SRCHZZ_RTL_SM

