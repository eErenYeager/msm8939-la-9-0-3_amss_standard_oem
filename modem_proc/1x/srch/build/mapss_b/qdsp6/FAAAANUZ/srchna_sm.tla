###############################################################################
#
#    srchna_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchna_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: INACTIVE_SM
# State machine:current state:input:next state                      fgcolor bgcolor

INACTIVE:STANDBY:SRCH_DEACTIVATE_F:STANDBY                   9372 0 00 0    @red @white

INACTIVE:STANDBY:SRCH_AMPS_F:STANDBY                         9372 0 01 0    @red @white

INACTIVE:STANDBY:SRCH_CDMA_F:STANDBY                         9372 0 02 0    @red @white

INACTIVE:STANDBY:ENTER_AMPS:STANDBY                          9372 0 03 0    @red @white

INACTIVE:STANDBY:ENTER_CDMA:STANDBY                          9372 0 04 0    @red @white

INACTIVE:STANDBY:SRCH_POWERDOWN_F:STANDBY                    9372 0 05 0    @red @white

INACTIVE:STANDBY:SRCH_STOP:STANDBY                           9372 0 06 0    @red @white

INACTIVE:STANDBY:SRCH_OFFLINE:STANDBY                        9372 0 07 0    @red @white

INACTIVE:STANDBY:SRCH_SCLK_COMPLETE:STANDBY                  9372 0 08 0    @red @white

INACTIVE:STANDBY:SRCHLTE_TT_F:STANDBY                        9372 0 09 0    @red @white

INACTIVE:STANDBY:IRAT_TT_F:STANDBY                           9372 0 0a 0    @red @white

INACTIVE:STANDBY:IRAT_PILOT_MEAS:STANDBY                     9372 0 0b 0    @red @white



# End machine generated TLA code for state machine: INACTIVE_SM

