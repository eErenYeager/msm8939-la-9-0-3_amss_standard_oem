/*=============================================================================

  srchzz_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_SM */

/* Transition function prototypes */
static stm_state_type zz_bail_to_idle(void *);
static stm_state_type zz_process_bc_info_cmd(void *);
static stm_state_type zz_trans_to_sleep(void *);
static stm_state_type zz_process_aflt_release_done(void *);
static stm_state_type zz_ofreq_trans_to_sleep(void *);
static stm_state_type zz_process_ofreq_handing_off(void *);
static stm_state_type zz_ignore_page_match(void *);
static stm_state_type zz_process_log_stats_cmd(void *);
static stm_state_type zz_process_wakeup_now(void *);
static stm_state_type zz_process_oconfig_nbr_found(void *);
static stm_state_type zz_process_ignore_abort(void *);
static stm_state_type zz_process_ofreq_handoff_done(void *);
static stm_state_type zz_process_idle_cmd_from_sleep(void *);
static stm_state_type zz_handle_qpch_reacq_done(void *);
static stm_state_type zz_trans_to_reacq(void *);
static stm_state_type zz_config_rxc(void *);
static stm_state_type zz_ignore_oconfig_nbr_found(void *);
static stm_state_type zz_process_idle_cmd(void *);
static stm_state_type zz_reacq_handle_reacq_done(void *);
static stm_state_type zz_reacq_handle_tl_done(void *);
static stm_state_type zz_process_reacq_done(void *);
static stm_state_type zz_remember_page_match(void *);
static stm_state_type zz_recall_reacq_info(void *);
static stm_state_type zz_process_page_match(void *);
static stm_state_type zz_meas_send_start_req(void *);
static stm_state_type zz_meas_trans_to_sleep(void *);
static stm_state_type zz_meas_process_abort_complete(void *);
static stm_state_type zz_meas_process_abort(void *);
static stm_state_type zz_meas_process_meas_abort(void *);
static stm_state_type zz_meas_to_idle_cmd(void *);


/* State Machine entry/exit function prototypes */
static void zz_entry(stm_group_type *group);
static void zz_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void zz_enter_wait_meas(void *payload, stm_state_type previous_state);
static void zz_exit_wait_meas(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_SM_NUMBER_OF_STATES 8
#define SRCHZZ_SM_NUMBER_OF_INPUTS 21


/* State enumeration */
enum
{
  INIT_STATE,
  OFREQ_HO_STATE,
  SLEEP_STATE,
  REACQ_STATE,
  WAIT_REACQ_DONE_STATE,
  WAIT_TL_DONE_STATE,
  WAIT_PAGE_MATCH_STATE,
  WAIT_MEAS_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_SM_states[ SRCHZZ_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", NULL, NULL},
  {"OFREQ_HO_STATE", NULL, NULL},
  {"SLEEP_STATE", NULL, NULL},
  {"REACQ_STATE", NULL, NULL},
  {"WAIT_REACQ_DONE_STATE", NULL, NULL},
  {"WAIT_TL_DONE_STATE", NULL, NULL},
  {"WAIT_PAGE_MATCH_STATE", NULL, NULL},
  {"WAIT_MEAS_STATE", zz_enter_wait_meas, zz_exit_wait_meas},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_SM_inputs[ SRCHZZ_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)IDLE_CMD, "IDLE_CMD"},
  {(stm_input_type)BC_INFO_CMD, "BC_INFO_CMD"},
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)AFLT_RELEASE_DONE_CMD, "AFLT_RELEASE_DONE_CMD"},
  {(stm_input_type)OFREQ_DONE_SLEEP_CMD, "OFREQ_DONE_SLEEP_CMD"},
  {(stm_input_type)OFREQ_HANDING_OFF_CMD, "OFREQ_HANDING_OFF_CMD"},
  {(stm_input_type)PAGE_MATCH_CMD, "PAGE_MATCH_CMD"},
  {(stm_input_type)LOG_STATS_CMD, "LOG_STATS_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)OCONFIG_NBR_FOUND_CMD, "OCONFIG_NBR_FOUND_CMD"},
  {(stm_input_type)ABORT_CMD, "ABORT_CMD"},
  {(stm_input_type)OFREQ_HANDOFF_DONE_CMD, "OFREQ_HANDOFF_DONE_CMD"},
  {(stm_input_type)QPCH_REACQ_DONE_CMD, "QPCH_REACQ_DONE_CMD"},
  {(stm_input_type)WAKEUP_DONE_CMD, "WAKEUP_DONE_CMD"},
  {(stm_input_type)CONFIG_RXC_CMD, "CONFIG_RXC_CMD"},
  {(stm_input_type)REACQ_DONE_CMD, "REACQ_DONE_CMD"},
  {(stm_input_type)TL_DONE_CMD, "TL_DONE_CMD"},
  {(stm_input_type)RX_MOD_GRANTED_CMD, "RX_MOD_GRANTED_CMD"},
  {(stm_input_type)RX_MOD_DENIED_CMD, "RX_MOD_DENIED_CMD"},
  {(stm_input_type)MEAS_DONE_CMD, "MEAS_DONE_CMD"},
  {(stm_input_type)ABORT_COMPLETE_CMD, "ABORT_COMPLETE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_SM_transitions[ SRCHZZ_SM_NUMBER_OF_STATES *  SRCHZZ_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    zz_bail_to_idle,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    zz_trans_to_sleep,    /* START_SLEEP_CMD */
    zz_process_aflt_release_done,    /* AFLT_RELEASE_DONE_CMD */
    zz_ofreq_trans_to_sleep,    /* OFREQ_DONE_SLEEP_CMD */
    zz_process_ofreq_handing_off,    /* OFREQ_HANDING_OFF_CMD */
    zz_ignore_page_match,    /* PAGE_MATCH_CMD */
    zz_process_log_stats_cmd,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    zz_process_oconfig_nbr_found,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    NULL,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state OFREQ_HO_STATE */
    zz_bail_to_idle,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    zz_trans_to_sleep,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    zz_ignore_page_match,    /* PAGE_MATCH_CMD */
    zz_process_log_stats_cmd,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    NULL,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    zz_process_ofreq_handoff_done,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    NULL,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state SLEEP_STATE */
    zz_process_idle_cmd_from_sleep,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    zz_trans_to_sleep,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    NULL,    /* PAGE_MATCH_CMD */
    zz_process_log_stats_cmd,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    zz_ignore_oconfig_nbr_found,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    zz_handle_qpch_reacq_done,    /* QPCH_REACQ_DONE_CMD */
    zz_trans_to_reacq,    /* WAKEUP_DONE_CMD */
    zz_config_rxc,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state REACQ_STATE */
    zz_process_idle_cmd,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    zz_ignore_page_match,    /* PAGE_MATCH_CMD */
    NULL,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    NULL,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    zz_config_rxc,    /* CONFIG_RXC_CMD */
    zz_reacq_handle_reacq_done,    /* REACQ_DONE_CMD */
    zz_reacq_handle_tl_done,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state WAIT_REACQ_DONE_STATE */
    zz_process_idle_cmd,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    zz_remember_page_match,    /* PAGE_MATCH_CMD */
    NULL,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    NULL,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    zz_config_rxc,    /* CONFIG_RXC_CMD */
    zz_process_reacq_done,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state WAIT_TL_DONE_STATE */
    zz_process_idle_cmd,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    zz_remember_page_match,    /* PAGE_MATCH_CMD */
    NULL,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    zz_process_oconfig_nbr_found,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    zz_config_rxc,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    zz_recall_reacq_info,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state WAIT_PAGE_MATCH_STATE */
    zz_bail_to_idle,    /* IDLE_CMD */
    zz_process_bc_info_cmd,    /* BC_INFO_CMD */
    zz_trans_to_sleep,    /* START_SLEEP_CMD */
    zz_process_aflt_release_done,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    zz_process_page_match,    /* PAGE_MATCH_CMD */
    NULL,    /* LOG_STATS_CMD */
    zz_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    zz_process_oconfig_nbr_found,    /* OCONFIG_NBR_FOUND_CMD */
    zz_process_ignore_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    NULL,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    NULL,    /* RX_MOD_GRANTED_CMD */
    NULL,    /* RX_MOD_DENIED_CMD */
    NULL,    /* MEAS_DONE_CMD */
    NULL,    /* ABORT_COMPLETE_CMD */

  /* Transition functions for state WAIT_MEAS_STATE */
    zz_meas_to_idle_cmd,    /* IDLE_CMD */
    NULL,    /* BC_INFO_CMD */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* OFREQ_DONE_SLEEP_CMD */
    NULL,    /* OFREQ_HANDING_OFF_CMD */
    NULL,    /* PAGE_MATCH_CMD */
    NULL,    /* LOG_STATS_CMD */
    zz_meas_process_abort,    /* WAKEUP_NOW_CMD */
    NULL,    /* OCONFIG_NBR_FOUND_CMD */
    zz_meas_process_meas_abort,    /* ABORT_CMD */
    NULL,    /* OFREQ_HANDOFF_DONE_CMD */
    NULL,    /* QPCH_REACQ_DONE_CMD */
    NULL,    /* WAKEUP_DONE_CMD */
    NULL,    /* CONFIG_RXC_CMD */
    NULL,    /* REACQ_DONE_CMD */
    NULL,    /* TL_DONE_CMD */
    zz_meas_send_start_req,    /* RX_MOD_GRANTED_CMD */
    zz_meas_trans_to_sleep,    /* RX_MOD_DENIED_CMD */
    zz_meas_trans_to_sleep,    /* MEAS_DONE_CMD */
    zz_meas_process_abort_complete,    /* ABORT_COMPLETE_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_SM =
{
  "SRCHZZ_SM", /* state machine name */
  20295, /* unique SM id (hash of name) */
  zz_entry, /* state machine entry function */
  zz_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_SM_NUMBER_OF_STATES,
  SRCHZZ_SM_NUMBER_OF_INPUTS,
  SRCHZZ_SM_states,
  SRCHZZ_SM_inputs,
  SRCHZZ_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_SM */

