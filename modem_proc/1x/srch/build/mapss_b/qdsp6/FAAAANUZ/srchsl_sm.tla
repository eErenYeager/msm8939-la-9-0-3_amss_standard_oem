###############################################################################
#
#    srchsl_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchsl_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SLEW_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SLEW:SLEW:SRCH_CDMA_F:SLEW                                   f7f6 0 00 0    @black @white
SLEW:SLEW:SRCH_CDMA_F:TUNED                                  f7f6 0 00 1    @black @white
SLEW:SLEW:SRCH_CDMA_F:PHS_ADJ                                f7f6 0 00 2    @black @white
SLEW:SLEW:SRCH_CDMA_F:WAIT                                   f7f6 0 00 3    @black @white
SLEW:SLEW:SRCH_CDMA_F:UNLOCK                                 f7f6 0 00 4    @black @white
SLEW:SLEW:SRCH_CDMA_F:REPORT                                 f7f6 0 00 5    @black @white
SLEW:SLEW:SRCH_CDMA_F:DONE                                   f7f6 0 00 6    @black @white

SLEW:SLEW:SRCH_IDLE_F:SLEW                                   f7f6 0 01 0    @black @white
SLEW:SLEW:SRCH_IDLE_F:TUNED                                  f7f6 0 01 1    @black @white
SLEW:SLEW:SRCH_IDLE_F:PHS_ADJ                                f7f6 0 01 2    @black @white
SLEW:SLEW:SRCH_IDLE_F:WAIT                                   f7f6 0 01 3    @black @white
SLEW:SLEW:SRCH_IDLE_F:UNLOCK                                 f7f6 0 01 4    @black @white
SLEW:SLEW:SRCH_IDLE_F:REPORT                                 f7f6 0 01 5    @black @white
SLEW:SLEW:SRCH_IDLE_F:DONE                                   f7f6 0 01 6    @black @white

SLEW:SLEW:RX_TUNE_COMP:SLEW                                  f7f6 0 02 0    @black @white
SLEW:SLEW:RX_TUNE_COMP:TUNED                                 f7f6 0 02 1    @black @white
SLEW:SLEW:RX_TUNE_COMP:PHS_ADJ                               f7f6 0 02 2    @black @white
SLEW:SLEW:RX_TUNE_COMP:WAIT                                  f7f6 0 02 3    @black @white
SLEW:SLEW:RX_TUNE_COMP:UNLOCK                                f7f6 0 02 4    @black @white
SLEW:SLEW:RX_TUNE_COMP:REPORT                                f7f6 0 02 5    @black @white
SLEW:SLEW:RX_TUNE_COMP:DONE                                  f7f6 0 02 6    @black @white

SLEW:SLEW:ROLL:SLEW                                          f7f6 0 03 0    @black @white
SLEW:SLEW:ROLL:TUNED                                         f7f6 0 03 1    @black @white
SLEW:SLEW:ROLL:PHS_ADJ                                       f7f6 0 03 2    @black @white
SLEW:SLEW:ROLL:WAIT                                          f7f6 0 03 3    @black @white
SLEW:SLEW:ROLL:UNLOCK                                        f7f6 0 03 4    @black @white
SLEW:SLEW:ROLL:REPORT                                        f7f6 0 03 5    @black @white
SLEW:SLEW:ROLL:DONE                                          f7f6 0 03 6    @black @white

SLEW:TUNED:SRCH_CDMA_F:SLEW                                  f7f6 1 00 0    @black @white
SLEW:TUNED:SRCH_CDMA_F:TUNED                                 f7f6 1 00 1    @black @white
SLEW:TUNED:SRCH_CDMA_F:PHS_ADJ                               f7f6 1 00 2    @black @white
SLEW:TUNED:SRCH_CDMA_F:WAIT                                  f7f6 1 00 3    @black @white
SLEW:TUNED:SRCH_CDMA_F:UNLOCK                                f7f6 1 00 4    @black @white
SLEW:TUNED:SRCH_CDMA_F:REPORT                                f7f6 1 00 5    @black @white
SLEW:TUNED:SRCH_CDMA_F:DONE                                  f7f6 1 00 6    @black @white

SLEW:TUNED:SRCH_IDLE_F:SLEW                                  f7f6 1 01 0    @black @white
SLEW:TUNED:SRCH_IDLE_F:TUNED                                 f7f6 1 01 1    @black @white
SLEW:TUNED:SRCH_IDLE_F:PHS_ADJ                               f7f6 1 01 2    @black @white
SLEW:TUNED:SRCH_IDLE_F:WAIT                                  f7f6 1 01 3    @black @white
SLEW:TUNED:SRCH_IDLE_F:UNLOCK                                f7f6 1 01 4    @black @white
SLEW:TUNED:SRCH_IDLE_F:REPORT                                f7f6 1 01 5    @black @white
SLEW:TUNED:SRCH_IDLE_F:DONE                                  f7f6 1 01 6    @black @white

SLEW:TUNED:RX_TUNE_COMP:SLEW                                 f7f6 1 02 0    @black @white
SLEW:TUNED:RX_TUNE_COMP:TUNED                                f7f6 1 02 1    @black @white
SLEW:TUNED:RX_TUNE_COMP:PHS_ADJ                              f7f6 1 02 2    @black @white
SLEW:TUNED:RX_TUNE_COMP:WAIT                                 f7f6 1 02 3    @black @white
SLEW:TUNED:RX_TUNE_COMP:UNLOCK                               f7f6 1 02 4    @black @white
SLEW:TUNED:RX_TUNE_COMP:REPORT                               f7f6 1 02 5    @black @white
SLEW:TUNED:RX_TUNE_COMP:DONE                                 f7f6 1 02 6    @black @white

SLEW:TUNED:ROLL:SLEW                                         f7f6 1 03 0    @black @white
SLEW:TUNED:ROLL:TUNED                                        f7f6 1 03 1    @black @white
SLEW:TUNED:ROLL:PHS_ADJ                                      f7f6 1 03 2    @black @white
SLEW:TUNED:ROLL:WAIT                                         f7f6 1 03 3    @black @white
SLEW:TUNED:ROLL:UNLOCK                                       f7f6 1 03 4    @black @white
SLEW:TUNED:ROLL:REPORT                                       f7f6 1 03 5    @black @white
SLEW:TUNED:ROLL:DONE                                         f7f6 1 03 6    @black @white

SLEW:PHS_ADJ:SRCH_CDMA_F:SLEW                                f7f6 2 00 0    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:TUNED                               f7f6 2 00 1    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:PHS_ADJ                             f7f6 2 00 2    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:WAIT                                f7f6 2 00 3    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:UNLOCK                              f7f6 2 00 4    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:REPORT                              f7f6 2 00 5    @black @white
SLEW:PHS_ADJ:SRCH_CDMA_F:DONE                                f7f6 2 00 6    @black @white

SLEW:PHS_ADJ:SRCH_IDLE_F:SLEW                                f7f6 2 01 0    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:TUNED                               f7f6 2 01 1    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:PHS_ADJ                             f7f6 2 01 2    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:WAIT                                f7f6 2 01 3    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:UNLOCK                              f7f6 2 01 4    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:REPORT                              f7f6 2 01 5    @black @white
SLEW:PHS_ADJ:SRCH_IDLE_F:DONE                                f7f6 2 01 6    @black @white

SLEW:PHS_ADJ:RX_TUNE_COMP:SLEW                               f7f6 2 02 0    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:TUNED                              f7f6 2 02 1    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:PHS_ADJ                            f7f6 2 02 2    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:WAIT                               f7f6 2 02 3    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:UNLOCK                             f7f6 2 02 4    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:REPORT                             f7f6 2 02 5    @black @white
SLEW:PHS_ADJ:RX_TUNE_COMP:DONE                               f7f6 2 02 6    @black @white

SLEW:PHS_ADJ:ROLL:SLEW                                       f7f6 2 03 0    @black @white
SLEW:PHS_ADJ:ROLL:TUNED                                      f7f6 2 03 1    @black @white
SLEW:PHS_ADJ:ROLL:PHS_ADJ                                    f7f6 2 03 2    @black @white
SLEW:PHS_ADJ:ROLL:WAIT                                       f7f6 2 03 3    @black @white
SLEW:PHS_ADJ:ROLL:UNLOCK                                     f7f6 2 03 4    @black @white
SLEW:PHS_ADJ:ROLL:REPORT                                     f7f6 2 03 5    @black @white
SLEW:PHS_ADJ:ROLL:DONE                                       f7f6 2 03 6    @black @white

SLEW:WAIT:SRCH_CDMA_F:SLEW                                   f7f6 3 00 0    @black @white
SLEW:WAIT:SRCH_CDMA_F:TUNED                                  f7f6 3 00 1    @black @white
SLEW:WAIT:SRCH_CDMA_F:PHS_ADJ                                f7f6 3 00 2    @black @white
SLEW:WAIT:SRCH_CDMA_F:WAIT                                   f7f6 3 00 3    @black @white
SLEW:WAIT:SRCH_CDMA_F:UNLOCK                                 f7f6 3 00 4    @black @white
SLEW:WAIT:SRCH_CDMA_F:REPORT                                 f7f6 3 00 5    @black @white
SLEW:WAIT:SRCH_CDMA_F:DONE                                   f7f6 3 00 6    @black @white

SLEW:WAIT:SRCH_IDLE_F:SLEW                                   f7f6 3 01 0    @black @white
SLEW:WAIT:SRCH_IDLE_F:TUNED                                  f7f6 3 01 1    @black @white
SLEW:WAIT:SRCH_IDLE_F:PHS_ADJ                                f7f6 3 01 2    @black @white
SLEW:WAIT:SRCH_IDLE_F:WAIT                                   f7f6 3 01 3    @black @white
SLEW:WAIT:SRCH_IDLE_F:UNLOCK                                 f7f6 3 01 4    @black @white
SLEW:WAIT:SRCH_IDLE_F:REPORT                                 f7f6 3 01 5    @black @white
SLEW:WAIT:SRCH_IDLE_F:DONE                                   f7f6 3 01 6    @black @white

SLEW:WAIT:RX_TUNE_COMP:SLEW                                  f7f6 3 02 0    @black @white
SLEW:WAIT:RX_TUNE_COMP:TUNED                                 f7f6 3 02 1    @black @white
SLEW:WAIT:RX_TUNE_COMP:PHS_ADJ                               f7f6 3 02 2    @black @white
SLEW:WAIT:RX_TUNE_COMP:WAIT                                  f7f6 3 02 3    @black @white
SLEW:WAIT:RX_TUNE_COMP:UNLOCK                                f7f6 3 02 4    @black @white
SLEW:WAIT:RX_TUNE_COMP:REPORT                                f7f6 3 02 5    @black @white
SLEW:WAIT:RX_TUNE_COMP:DONE                                  f7f6 3 02 6    @black @white

SLEW:WAIT:ROLL:SLEW                                          f7f6 3 03 0    @black @white
SLEW:WAIT:ROLL:TUNED                                         f7f6 3 03 1    @black @white
SLEW:WAIT:ROLL:PHS_ADJ                                       f7f6 3 03 2    @black @white
SLEW:WAIT:ROLL:WAIT                                          f7f6 3 03 3    @black @white
SLEW:WAIT:ROLL:UNLOCK                                        f7f6 3 03 4    @black @white
SLEW:WAIT:ROLL:REPORT                                        f7f6 3 03 5    @black @white
SLEW:WAIT:ROLL:DONE                                          f7f6 3 03 6    @black @white

SLEW:UNLOCK:SRCH_CDMA_F:SLEW                                 f7f6 4 00 0    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:TUNED                                f7f6 4 00 1    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:PHS_ADJ                              f7f6 4 00 2    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:WAIT                                 f7f6 4 00 3    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:UNLOCK                               f7f6 4 00 4    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:REPORT                               f7f6 4 00 5    @black @white
SLEW:UNLOCK:SRCH_CDMA_F:DONE                                 f7f6 4 00 6    @black @white

SLEW:UNLOCK:SRCH_IDLE_F:SLEW                                 f7f6 4 01 0    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:TUNED                                f7f6 4 01 1    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:PHS_ADJ                              f7f6 4 01 2    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:WAIT                                 f7f6 4 01 3    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:UNLOCK                               f7f6 4 01 4    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:REPORT                               f7f6 4 01 5    @black @white
SLEW:UNLOCK:SRCH_IDLE_F:DONE                                 f7f6 4 01 6    @black @white

SLEW:UNLOCK:RX_TUNE_COMP:SLEW                                f7f6 4 02 0    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:TUNED                               f7f6 4 02 1    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:PHS_ADJ                             f7f6 4 02 2    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:WAIT                                f7f6 4 02 3    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:UNLOCK                              f7f6 4 02 4    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:REPORT                              f7f6 4 02 5    @black @white
SLEW:UNLOCK:RX_TUNE_COMP:DONE                                f7f6 4 02 6    @black @white

SLEW:UNLOCK:ROLL:SLEW                                        f7f6 4 03 0    @black @white
SLEW:UNLOCK:ROLL:TUNED                                       f7f6 4 03 1    @black @white
SLEW:UNLOCK:ROLL:PHS_ADJ                                     f7f6 4 03 2    @black @white
SLEW:UNLOCK:ROLL:WAIT                                        f7f6 4 03 3    @black @white
SLEW:UNLOCK:ROLL:UNLOCK                                      f7f6 4 03 4    @black @white
SLEW:UNLOCK:ROLL:REPORT                                      f7f6 4 03 5    @black @white
SLEW:UNLOCK:ROLL:DONE                                        f7f6 4 03 6    @black @white

SLEW:REPORT:SRCH_CDMA_F:SLEW                                 f7f6 5 00 0    @black @white
SLEW:REPORT:SRCH_CDMA_F:TUNED                                f7f6 5 00 1    @black @white
SLEW:REPORT:SRCH_CDMA_F:PHS_ADJ                              f7f6 5 00 2    @black @white
SLEW:REPORT:SRCH_CDMA_F:WAIT                                 f7f6 5 00 3    @black @white
SLEW:REPORT:SRCH_CDMA_F:UNLOCK                               f7f6 5 00 4    @black @white
SLEW:REPORT:SRCH_CDMA_F:REPORT                               f7f6 5 00 5    @black @white
SLEW:REPORT:SRCH_CDMA_F:DONE                                 f7f6 5 00 6    @black @white

SLEW:REPORT:SRCH_IDLE_F:SLEW                                 f7f6 5 01 0    @black @white
SLEW:REPORT:SRCH_IDLE_F:TUNED                                f7f6 5 01 1    @black @white
SLEW:REPORT:SRCH_IDLE_F:PHS_ADJ                              f7f6 5 01 2    @black @white
SLEW:REPORT:SRCH_IDLE_F:WAIT                                 f7f6 5 01 3    @black @white
SLEW:REPORT:SRCH_IDLE_F:UNLOCK                               f7f6 5 01 4    @black @white
SLEW:REPORT:SRCH_IDLE_F:REPORT                               f7f6 5 01 5    @black @white
SLEW:REPORT:SRCH_IDLE_F:DONE                                 f7f6 5 01 6    @black @white

SLEW:REPORT:RX_TUNE_COMP:SLEW                                f7f6 5 02 0    @black @white
SLEW:REPORT:RX_TUNE_COMP:TUNED                               f7f6 5 02 1    @black @white
SLEW:REPORT:RX_TUNE_COMP:PHS_ADJ                             f7f6 5 02 2    @black @white
SLEW:REPORT:RX_TUNE_COMP:WAIT                                f7f6 5 02 3    @black @white
SLEW:REPORT:RX_TUNE_COMP:UNLOCK                              f7f6 5 02 4    @black @white
SLEW:REPORT:RX_TUNE_COMP:REPORT                              f7f6 5 02 5    @black @white
SLEW:REPORT:RX_TUNE_COMP:DONE                                f7f6 5 02 6    @black @white

SLEW:REPORT:ROLL:SLEW                                        f7f6 5 03 0    @black @white
SLEW:REPORT:ROLL:TUNED                                       f7f6 5 03 1    @black @white
SLEW:REPORT:ROLL:PHS_ADJ                                     f7f6 5 03 2    @black @white
SLEW:REPORT:ROLL:WAIT                                        f7f6 5 03 3    @black @white
SLEW:REPORT:ROLL:UNLOCK                                      f7f6 5 03 4    @black @white
SLEW:REPORT:ROLL:REPORT                                      f7f6 5 03 5    @black @white
SLEW:REPORT:ROLL:DONE                                        f7f6 5 03 6    @black @white

SLEW:DONE:SRCH_CDMA_F:SLEW                                   f7f6 6 00 0    @black @white
SLEW:DONE:SRCH_CDMA_F:TUNED                                  f7f6 6 00 1    @black @white
SLEW:DONE:SRCH_CDMA_F:PHS_ADJ                                f7f6 6 00 2    @black @white
SLEW:DONE:SRCH_CDMA_F:WAIT                                   f7f6 6 00 3    @black @white
SLEW:DONE:SRCH_CDMA_F:UNLOCK                                 f7f6 6 00 4    @black @white
SLEW:DONE:SRCH_CDMA_F:REPORT                                 f7f6 6 00 5    @black @white
SLEW:DONE:SRCH_CDMA_F:DONE                                   f7f6 6 00 6    @black @white

SLEW:DONE:SRCH_IDLE_F:SLEW                                   f7f6 6 01 0    @black @white
SLEW:DONE:SRCH_IDLE_F:TUNED                                  f7f6 6 01 1    @black @white
SLEW:DONE:SRCH_IDLE_F:PHS_ADJ                                f7f6 6 01 2    @black @white
SLEW:DONE:SRCH_IDLE_F:WAIT                                   f7f6 6 01 3    @black @white
SLEW:DONE:SRCH_IDLE_F:UNLOCK                                 f7f6 6 01 4    @black @white
SLEW:DONE:SRCH_IDLE_F:REPORT                                 f7f6 6 01 5    @black @white
SLEW:DONE:SRCH_IDLE_F:DONE                                   f7f6 6 01 6    @black @white

SLEW:DONE:RX_TUNE_COMP:SLEW                                  f7f6 6 02 0    @black @white
SLEW:DONE:RX_TUNE_COMP:TUNED                                 f7f6 6 02 1    @black @white
SLEW:DONE:RX_TUNE_COMP:PHS_ADJ                               f7f6 6 02 2    @black @white
SLEW:DONE:RX_TUNE_COMP:WAIT                                  f7f6 6 02 3    @black @white
SLEW:DONE:RX_TUNE_COMP:UNLOCK                                f7f6 6 02 4    @black @white
SLEW:DONE:RX_TUNE_COMP:REPORT                                f7f6 6 02 5    @black @white
SLEW:DONE:RX_TUNE_COMP:DONE                                  f7f6 6 02 6    @black @white

SLEW:DONE:ROLL:SLEW                                          f7f6 6 03 0    @black @white
SLEW:DONE:ROLL:TUNED                                         f7f6 6 03 1    @black @white
SLEW:DONE:ROLL:PHS_ADJ                                       f7f6 6 03 2    @black @white
SLEW:DONE:ROLL:WAIT                                          f7f6 6 03 3    @black @white
SLEW:DONE:ROLL:UNLOCK                                        f7f6 6 03 4    @black @white
SLEW:DONE:ROLL:REPORT                                        f7f6 6 03 5    @black @white
SLEW:DONE:ROLL:DONE                                          f7f6 6 03 6    @black @white



# End machine generated TLA code for state machine: SLEW_SM

