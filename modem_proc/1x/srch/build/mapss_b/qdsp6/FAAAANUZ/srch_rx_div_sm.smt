/*=============================================================================

  srch_rx_div_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_rx_div_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: DIV_SM */

/* Transition function prototypes */
static stm_state_type div_request(void *);
static stm_state_type div_ignore_stop(void *);
static stm_state_type DIV_SM_ignore_input(void *);
static stm_state_type div_ignore_request(void *);
static stm_state_type div_init(void *);
static stm_state_type div_cancel_request(void *);
static stm_state_type div_priority_update_rx(void *);
static stm_state_type div_ignore_init(void *);
static stm_state_type div_enabled(void *);
static stm_state_type div_cancel_init(void *);
static stm_state_type div_ignore_start(void *);
static stm_state_type div_pause(void *);
static stm_state_type div_fing_start(void *);
static stm_state_type div_release_rx_for_pause(void *);
static stm_state_type div_rerequest_rx_for_pause(void *);
static stm_state_type div_stop(void *);
static stm_state_type div_en_agc_ck(void *);
static stm_state_type div_prx_agc_ck(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type DIV_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void div_entry(stm_group_type *group);
static void div_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void div_off_entry(void *payload, stm_state_type previous_state);
static void div_enabled_entry(void *payload, stm_state_type previous_state);
static void div_enabled_exit(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define DIV_SM_NUMBER_OF_STATES 5
#define DIV_SM_NUMBER_OF_INPUTS 10


/* State enumeration */
enum
{
  DIV_OFF,
  DIV_REQUESTED,
  DIV_INITING,
  DIV_ENABLED,
  DIV_ENABLED_PRX,
};


/* State name, entry, exit table */
static const stm_state_array_type
  DIV_SM_states[ DIV_SM_NUMBER_OF_STATES ] =
{
  {"DIV_OFF", div_off_entry, NULL},
  {"DIV_REQUESTED", NULL, NULL},
  {"DIV_INITING", NULL, NULL},
  {"DIV_ENABLED", div_enabled_entry, div_enabled_exit},
  {"DIV_ENABLED_PRX", div_enabled_entry, div_enabled_exit},
};


/* Input value, name table */
static const stm_input_array_type
  DIV_SM_inputs[ DIV_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)DIV_REQUEST_CMD, "DIV_REQUEST_CMD"},
  {(stm_input_type)DIV_STOP_CMD, "DIV_STOP_CMD"},
  {(stm_input_type)DIV_AGC_CK_CMD, "DIV_AGC_CK_CMD"},
  {(stm_input_type)RX_CH_DISABLE_DIV_COMP_DONE_CMD, "RX_CH_DISABLE_DIV_COMP_DONE_CMD"},
  {(stm_input_type)DIV_UPDATE_PRIORITY_CMD, "DIV_UPDATE_PRIORITY_CMD"},
  {(stm_input_type)DIV_INIT_CMD, "DIV_INIT_CMD"},
  {(stm_input_type)DIV_READY_CMD, "DIV_READY_CMD"},
  {(stm_input_type)DIV_PAUSE_CMD, "DIV_PAUSE_CMD"},
  {(stm_input_type)DIV_FING_START_CMD, "DIV_FING_START_CMD"},
  {(stm_input_type)DIV_RELEASE_CMD, "DIV_RELEASE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  DIV_SM_transitions[ DIV_SM_NUMBER_OF_STATES *  DIV_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state DIV_OFF */
    div_request,    /* DIV_REQUEST_CMD */
    div_ignore_stop,    /* DIV_STOP_CMD */
    DIV_SM_ignore_input,    /* DIV_AGC_CK_CMD */
    DIV_SM_ignore_input,    /* RX_CH_DISABLE_DIV_COMP_DONE_CMD */
    DIV_SM_ignore_input,    /* DIV_UPDATE_PRIORITY_CMD */
    NULL,    /* DIV_INIT_CMD */
    NULL,    /* DIV_READY_CMD */
    NULL,    /* DIV_PAUSE_CMD */
    NULL,    /* DIV_FING_START_CMD */
    NULL,    /* DIV_RELEASE_CMD */

  /* Transition functions for state DIV_REQUESTED */
    div_ignore_request,    /* DIV_REQUEST_CMD */
    div_cancel_request,    /* DIV_STOP_CMD */
    DIV_SM_ignore_input,    /* DIV_AGC_CK_CMD */
    NULL,    /* RX_CH_DISABLE_DIV_COMP_DONE_CMD */
    div_priority_update_rx,    /* DIV_UPDATE_PRIORITY_CMD */
    div_init,    /* DIV_INIT_CMD */
    NULL,    /* DIV_READY_CMD */
    NULL,    /* DIV_PAUSE_CMD */
    NULL,    /* DIV_FING_START_CMD */
    NULL,    /* DIV_RELEASE_CMD */

  /* Transition functions for state DIV_INITING */
    div_ignore_request,    /* DIV_REQUEST_CMD */
    div_cancel_init,    /* DIV_STOP_CMD */
    DIV_SM_ignore_input,    /* DIV_AGC_CK_CMD */
    NULL,    /* RX_CH_DISABLE_DIV_COMP_DONE_CMD */
    DIV_SM_ignore_input,    /* DIV_UPDATE_PRIORITY_CMD */
    div_ignore_init,    /* DIV_INIT_CMD */
    div_enabled,    /* DIV_READY_CMD */
    NULL,    /* DIV_PAUSE_CMD */
    NULL,    /* DIV_FING_START_CMD */
    NULL,    /* DIV_RELEASE_CMD */

  /* Transition functions for state DIV_ENABLED */
    div_ignore_start,    /* DIV_REQUEST_CMD */
    div_stop,    /* DIV_STOP_CMD */
    div_en_agc_ck,    /* DIV_AGC_CK_CMD */
    div_rerequest_rx_for_pause,    /* RX_CH_DISABLE_DIV_COMP_DONE_CMD */
    div_priority_update_rx,    /* DIV_UPDATE_PRIORITY_CMD */
    NULL,    /* DIV_INIT_CMD */
    NULL,    /* DIV_READY_CMD */
    div_pause,    /* DIV_PAUSE_CMD */
    div_fing_start,    /* DIV_FING_START_CMD */
    div_release_rx_for_pause,    /* DIV_RELEASE_CMD */

  /* Transition functions for state DIV_ENABLED_PRX */
    div_ignore_start,    /* DIV_REQUEST_CMD */
    div_stop,    /* DIV_STOP_CMD */
    div_prx_agc_ck,    /* DIV_AGC_CK_CMD */
    div_rerequest_rx_for_pause,    /* RX_CH_DISABLE_DIV_COMP_DONE_CMD */
    div_priority_update_rx,    /* DIV_UPDATE_PRIORITY_CMD */
    NULL,    /* DIV_INIT_CMD */
    NULL,    /* DIV_READY_CMD */
    div_pause,    /* DIV_PAUSE_CMD */
    NULL,    /* DIV_FING_START_CMD */
    div_release_rx_for_pause,    /* DIV_RELEASE_CMD */

};


/* State machine definition */
stm_state_machine_type DIV_SM =
{
  "DIV_SM", /* state machine name */
  60840, /* unique SM id (hash of name) */
  div_entry, /* state machine entry function */
  div_exit, /* state machine exit function */
  DIV_OFF, /* state machine initial state */
  TRUE, /* state machine starts active? */
  DIV_SM_NUMBER_OF_STATES,
  DIV_SM_NUMBER_OF_INPUTS,
  DIV_SM_states,
  DIV_SM_inputs,
  DIV_SM_transitions,
};

/* End machine generated code for state machine: DIV_SM */

