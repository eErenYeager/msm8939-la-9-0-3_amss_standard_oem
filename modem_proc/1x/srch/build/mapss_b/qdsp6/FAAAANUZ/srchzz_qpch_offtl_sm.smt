/*=============================================================================

  srchzz_qpch_offtl_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_qpch_offtl_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_QPCH_OFFTL_SM */

/* Transition function prototypes */
static stm_state_type qpch_offtl_change_sleep(void *);
static stm_state_type qpch_offtl_abort(void *);
static stm_state_type qpch_offtl_fwd_wakeup_now(void *);
static stm_state_type qpch_offtl_process_rude_wakeup(void *);
static stm_state_type qpch_offtl_process_chain_granted(void *);
static stm_state_type qpch_offtl_process_chain_denied(void *);
static stm_state_type qpch_offtl_process_no_rf_lock(void *);
static stm_state_type qpch_offtl_process_wakeup(void *);
static stm_state_type qpch_offtl_process_rf_prep_comp(void *);
static stm_state_type qpch_offtl_remember_cx8_on(void *);
static stm_state_type qpch_offtl_process_rf_disable_comp_cmd(void *);
static stm_state_type qpch_offtl_process_cx8_on(void *);
static stm_state_type qpch_offtl_process_tune_complete(void *);
static stm_state_type qpch_offtl_remember_rude_wakeup(void *);
static stm_state_type qpch_offtl_process_rwkup_rf_disabled(void *);
static stm_state_type qpch_offtl_process_rwkup_freq_track_off_done(void *);
static stm_state_type qpch_offtl_process_record_done(void *);
static stm_state_type qpch_offtl_process_record_error(void *);
static stm_state_type qpch_offtl_process_freq_track_off_done(void *);
static stm_state_type qpch_offtl_process_rf_disable_done(void *);
static stm_state_type qpch_offtl_process_reacq_done(void *);
static stm_state_type qpch_offtl_process_demod_done(void *);


/* State Machine entry/exit function prototypes */
static void qpch_offtl_entry(stm_group_type *group);
static void qpch_offtl_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void qpch_offtl_enter_doze(void *payload, stm_state_type previous_state);
static void qpch_offtl_enter_sleep(void *payload, stm_state_type previous_state);
static void qpch_offtl_exit_sleep(void *payload, stm_state_type previous_state);
static void qpch_offtl_enter_wakeup(void *payload, stm_state_type previous_state);
static void qpch_offtl_enter_record(void *payload, stm_state_type previous_state);
static void qpch_offtl_enter_reacq(void *payload, stm_state_type previous_state);
static void qpch_offtl_enter_demod(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_STATES 7
#define SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_INPUTS 16


/* State enumeration */
enum
{
  DOZE_STATE,
  SLEEP_STATE,
  WAKEUP_STATE,
  RECORD_STATE,
  RECORD_FAILED_STATE,
  REACQ_STATE,
  DEMOD_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_QPCH_OFFTL_SM_states[ SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_STATES ] =
{
  {"DOZE_STATE", qpch_offtl_enter_doze, NULL},
  {"SLEEP_STATE", qpch_offtl_enter_sleep, qpch_offtl_exit_sleep},
  {"WAKEUP_STATE", qpch_offtl_enter_wakeup, NULL},
  {"RECORD_STATE", qpch_offtl_enter_record, NULL},
  {"RECORD_FAILED_STATE", NULL, NULL},
  {"REACQ_STATE", qpch_offtl_enter_reacq, NULL},
  {"DEMOD_STATE", qpch_offtl_enter_demod, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_QPCH_OFFTL_SM_inputs[ SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)ABORT_CMD, "ABORT_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)RX_CH_GRANTED_CMD, "RX_CH_GRANTED_CMD"},
  {(stm_input_type)RX_CH_DENIED_CMD, "RX_CH_DENIED_CMD"},
  {(stm_input_type)NO_RF_LOCK_CMD, "NO_RF_LOCK_CMD"},
  {(stm_input_type)WAKEUP_CMD, "WAKEUP_CMD"},
  {(stm_input_type)RX_RF_RSP_TUNE_COMP_CMD, "RX_RF_RSP_TUNE_COMP_CMD"},
  {(stm_input_type)CX8_ON_CMD, "CX8_ON_CMD"},
  {(stm_input_type)RX_RF_DISABLE_COMP_CMD, "RX_RF_DISABLE_COMP_CMD"},
  {(stm_input_type)RX_TUNE_COMP_CMD, "RX_TUNE_COMP_CMD"},
  {(stm_input_type)RX_RF_DISABLED_CMD, "RX_RF_DISABLED_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)RX_RF_DISABLED_ERR_CMD, "RX_RF_DISABLED_ERR_CMD"},
  {(stm_input_type)ADJUST_TIMING_CMD, "ADJUST_TIMING_CMD"},
  {(stm_input_type)QPCH_DEMOD_DONE_CMD, "QPCH_DEMOD_DONE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_QPCH_OFFTL_SM_transitions[ SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_STATES *  SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state DOZE_STATE */
    qpch_offtl_change_sleep,    /* START_SLEEP_CMD */
    qpch_offtl_abort,    /* ABORT_CMD */
    qpch_offtl_fwd_wakeup_now,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state SLEEP_STATE */
    NULL,    /* START_SLEEP_CMD */
    qpch_offtl_abort,    /* ABORT_CMD */
    qpch_offtl_process_rude_wakeup,    /* WAKEUP_NOW_CMD */
    qpch_offtl_process_chain_granted,    /* RX_CH_GRANTED_CMD */
    qpch_offtl_process_chain_denied,    /* RX_CH_DENIED_CMD */
    qpch_offtl_process_no_rf_lock,    /* NO_RF_LOCK_CMD */
    qpch_offtl_process_wakeup,    /* WAKEUP_CMD */
    qpch_offtl_process_rf_prep_comp,    /* RX_RF_RSP_TUNE_COMP_CMD */
    qpch_offtl_remember_cx8_on,    /* CX8_ON_CMD */
    qpch_offtl_process_rf_disable_comp_cmd,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state WAKEUP_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ABORT_CMD */
    qpch_offtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    qpch_offtl_process_cx8_on,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    qpch_offtl_process_tune_complete,    /* RX_TUNE_COMP_CMD */
    qpch_offtl_process_rwkup_rf_disabled,    /* RX_RF_DISABLED_CMD */
    qpch_offtl_process_rwkup_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state RECORD_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ABORT_CMD */
    qpch_offtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    qpch_offtl_process_record_done,    /* RX_RF_DISABLED_CMD */
    qpch_offtl_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    qpch_offtl_process_record_error,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state RECORD_FAILED_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ABORT_CMD */
    qpch_offtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    qpch_offtl_process_rf_disable_done,    /* RX_RF_DISABLED_CMD */
    qpch_offtl_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state REACQ_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ABORT_CMD */
    qpch_offtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    qpch_offtl_process_reacq_done,    /* ADJUST_TIMING_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state DEMOD_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* ABORT_CMD */
    qpch_offtl_remember_rude_wakeup,    /* WAKEUP_NOW_CMD */
    NULL,    /* RX_CH_GRANTED_CMD */
    NULL,    /* RX_CH_DENIED_CMD */
    NULL,    /* NO_RF_LOCK_CMD */
    NULL,    /* WAKEUP_CMD */
    NULL,    /* RX_RF_RSP_TUNE_COMP_CMD */
    NULL,    /* CX8_ON_CMD */
    NULL,    /* RX_RF_DISABLE_COMP_CMD */
    NULL,    /* RX_TUNE_COMP_CMD */
    NULL,    /* RX_RF_DISABLED_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* RX_RF_DISABLED_ERR_CMD */
    NULL,    /* ADJUST_TIMING_CMD */
    qpch_offtl_process_demod_done,    /* QPCH_DEMOD_DONE_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_QPCH_OFFTL_SM =
{
  "SRCHZZ_QPCH_OFFTL_SM", /* state machine name */
  7136, /* unique SM id (hash of name) */
  qpch_offtl_entry, /* state machine entry function */
  qpch_offtl_exit, /* state machine exit function */
  DOZE_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_STATES,
  SRCHZZ_QPCH_OFFTL_SM_NUMBER_OF_INPUTS,
  SRCHZZ_QPCH_OFFTL_SM_states,
  SRCHZZ_QPCH_OFFTL_SM_inputs,
  SRCHZZ_QPCH_OFFTL_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_QPCH_OFFTL_SM */

