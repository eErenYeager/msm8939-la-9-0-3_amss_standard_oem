/*=============================================================================

  srch_rx_div_auto_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_rx_div_auto_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: DIV_AUTO_SM */

/* Transition function prototypes */
static stm_state_type DIV_AUTO_SM_ignore_input(void *);
static stm_state_type div_auto_common_enable(void *);
static stm_state_type div_auto_disabled_fpc_enabled(void *);
static stm_state_type div_auto_common_disable(void *);
static stm_state_type div_auto_common_pause(void *);
static stm_state_type div_auto_common_force_off(void *);
static stm_state_type div_auto_common_is_data_call(void *);
static stm_state_type div_auto_common_fpc_enabled(void *);
static stm_state_type div_auto_on_off_dyn_autoswitch(void *);
static stm_state_type div_auto_off_tmr_autoswitch(void *);
static stm_state_type div_auto_off_force_resume(void *);
static stm_state_type div_auto_off_force_force_off(void *);
static stm_state_type div_auto_off_force_is_data_call(void *);
static stm_state_type div_auto_off_force_fpc_enabled(void *);
static stm_state_type div_auto_off_force_autoswitch(void *);
static stm_state_type div_auto_on_cap_tmr_autoswitch(void *);
static stm_state_type div_auto_on_qual_tmr_autoswitch(void *);
static stm_state_type div_auto_on_force_is_data_call(void *);
static stm_state_type div_auto_on_force_fpc_enabled(void *);
static stm_state_type div_auto_on_force_autoswitch(void *);
static stm_state_type div_auto_on_prep_off_autoswitch(void *);

/* Input ignoring transition function that returns STM_SAME_STATE */
static stm_state_type DIV_AUTO_SM_ignore_input(void *payload)
{
  /* Suppress lint/compiler warnings about unused payload variable */
  if(payload){}
  return(STM_SAME_STATE);
}


/* State Machine entry/exit function prototypes */
static void div_auto_entry(stm_group_type *group);
static void div_auto_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void div_auto_disabled_entry(void *payload, stm_state_type previous_state);
static void div_auto_common_exit(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define DIV_AUTO_SM_NUMBER_OF_STATES 9
#define DIV_AUTO_SM_NUMBER_OF_INPUTS 8


/* State enumeration */
enum
{
  DIV_AUTO_DISABLED,
  DIV_AUTO_OFF_DYN,
  DIV_AUTO_OFF_TMR,
  DIV_AUTO_OFF_FORCE,
  DIV_AUTO_ON_CAP_TMR,
  DIV_AUTO_ON_QUAL_TMR,
  DIV_AUTO_ON_DYN,
  DIV_AUTO_ON_FORCE,
  DIV_AUTO_ON_PREP_OFF,
};


/* State name, entry, exit table */
static const stm_state_array_type
  DIV_AUTO_SM_states[ DIV_AUTO_SM_NUMBER_OF_STATES ] =
{
  {"DIV_AUTO_DISABLED", div_auto_disabled_entry, NULL},
  {"DIV_AUTO_OFF_DYN", NULL, div_auto_common_exit},
  {"DIV_AUTO_OFF_TMR", NULL, div_auto_common_exit},
  {"DIV_AUTO_OFF_FORCE", NULL, div_auto_common_exit},
  {"DIV_AUTO_ON_CAP_TMR", NULL, div_auto_common_exit},
  {"DIV_AUTO_ON_QUAL_TMR", NULL, div_auto_common_exit},
  {"DIV_AUTO_ON_DYN", NULL, div_auto_common_exit},
  {"DIV_AUTO_ON_FORCE", NULL, div_auto_common_exit},
  {"DIV_AUTO_ON_PREP_OFF", NULL, div_auto_common_exit},
};


/* Input value, name table */
static const stm_input_array_type
  DIV_AUTO_SM_inputs[ DIV_AUTO_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)DIV_AUTO_DISABLE_CMD, "DIV_AUTO_DISABLE_CMD"},
  {(stm_input_type)DIV_AUTO_ENABLE_CMD, "DIV_AUTO_ENABLE_CMD"},
  {(stm_input_type)DIV_AUTO_PAUSE_CMD, "DIV_AUTO_PAUSE_CMD"},
  {(stm_input_type)DIV_AUTO_RESUME_CMD, "DIV_AUTO_RESUME_CMD"},
  {(stm_input_type)DIV_AUTO_FORCE_OFF_CMD, "DIV_AUTO_FORCE_OFF_CMD"},
  {(stm_input_type)DIV_AUTO_IS_DATA_CALL_CMD, "DIV_AUTO_IS_DATA_CALL_CMD"},
  {(stm_input_type)DIV_AUTO_FPC_ENABLED_CMD, "DIV_AUTO_FPC_ENABLED_CMD"},
  {(stm_input_type)DIV_AUTO_AUTOSWITCH_CMD, "DIV_AUTO_AUTOSWITCH_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  DIV_AUTO_SM_transitions[ DIV_AUTO_SM_NUMBER_OF_STATES *  DIV_AUTO_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state DIV_AUTO_DISABLED */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_FORCE_OFF_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_disabled_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_OFF_DYN */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_off_dyn_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_OFF_TMR */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_off_tmr_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_OFF_FORCE */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    div_auto_off_force_resume,    /* DIV_AUTO_RESUME_CMD */
    div_auto_off_force_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_off_force_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_off_force_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_off_force_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_ON_CAP_TMR */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_cap_tmr_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_ON_QUAL_TMR */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_qual_tmr_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_ON_DYN */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_off_dyn_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_ON_FORCE */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_on_force_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_on_force_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_force_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

  /* Transition functions for state DIV_AUTO_ON_PREP_OFF */
    div_auto_common_disable,    /* DIV_AUTO_DISABLE_CMD */
    div_auto_common_enable,    /* DIV_AUTO_ENABLE_CMD */
    div_auto_common_pause,    /* DIV_AUTO_PAUSE_CMD */
    DIV_AUTO_SM_ignore_input,    /* DIV_AUTO_RESUME_CMD */
    div_auto_common_force_off,    /* DIV_AUTO_FORCE_OFF_CMD */
    div_auto_common_is_data_call,    /* DIV_AUTO_IS_DATA_CALL_CMD */
    div_auto_common_fpc_enabled,    /* DIV_AUTO_FPC_ENABLED_CMD */
    div_auto_on_prep_off_autoswitch,    /* DIV_AUTO_AUTOSWITCH_CMD */

};


/* State machine definition */
stm_state_machine_type DIV_AUTO_SM =
{
  "DIV_AUTO_SM", /* state machine name */
  36511, /* unique SM id (hash of name) */
  div_auto_entry, /* state machine entry function */
  div_auto_exit, /* state machine exit function */
  DIV_AUTO_DISABLED, /* state machine initial state */
  TRUE, /* state machine starts active? */
  DIV_AUTO_SM_NUMBER_OF_STATES,
  DIV_AUTO_SM_NUMBER_OF_INPUTS,
  DIV_AUTO_SM_states,
  DIV_AUTO_SM_inputs,
  DIV_AUTO_SM_transitions,
};

/* End machine generated code for state machine: DIV_AUTO_SM */

