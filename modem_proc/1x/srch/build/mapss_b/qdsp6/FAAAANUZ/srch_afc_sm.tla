###############################################################################
#
#    srch_afc_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srch_afc_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: AFC_SM
# State machine:current state:input:next state                      fgcolor bgcolor

AFC:AFC_INACTIVE:AFC_CDMA:AFC_INACTIVE                       d7a5 0 00 0    @olive @white
AFC:AFC_INACTIVE:AFC_CDMA:AFC_IDLE                           d7a5 0 00 1    @olive @white
AFC:AFC_INACTIVE:AFC_CDMA:AFC_R_TRK                          d7a5 0 00 2    @olive @white
AFC:AFC_INACTIVE:AFC_CDMA:AFC_R_ACQ                          d7a5 0 00 3    @olive @white
AFC:AFC_INACTIVE:AFC_CDMA:AFC_ACQ_DONE                       d7a5 0 00 4    @olive @white
AFC:AFC_INACTIVE:AFC_CDMA:AFC_VCO_PULL_IN                    d7a5 0 00 5    @olive @white

AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_INACTIVE              d7a5 0 01 0    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_IDLE                  d7a5 0 01 1    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_R_TRK                 d7a5 0 01 2    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_R_ACQ                 d7a5 0 01 3    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE              d7a5 0 01 4    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN           d7a5 0 01 5    @olive @white

AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_INACTIVE              d7a5 0 02 0    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_IDLE                  d7a5 0 02 1    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_R_TRK                 d7a5 0 02 2    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_R_ACQ                 d7a5 0 02 3    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE              d7a5 0 02 4    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN           d7a5 0 02 5    @olive @white

AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_INACTIVE             d7a5 0 03 0    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_IDLE                 d7a5 0 03 1    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_R_TRK                d7a5 0 03 2    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_R_ACQ                d7a5 0 03 3    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE             d7a5 0 03 4    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN          d7a5 0 03 5    @olive @white

AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE         d7a5 0 04 0    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE             d7a5 0 04 1    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK            d7a5 0 04 2    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ            d7a5 0 04 3    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE         d7a5 0 04 4    @olive @white
AFC:AFC_INACTIVE:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN      d7a5 0 04 5    @olive @white

AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_INACTIVE                d7a5 0 05 0    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_IDLE                    d7a5 0 05 1    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_R_TRK                   d7a5 0 05 2    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_R_ACQ                   d7a5 0 05 3    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_ACQ_DONE                d7a5 0 05 4    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN             d7a5 0 05 5    @olive @white

AFC:AFC_INACTIVE:AFC_FAST:AFC_INACTIVE                       d7a5 0 06 0    @olive @white
AFC:AFC_INACTIVE:AFC_FAST:AFC_IDLE                           d7a5 0 06 1    @olive @white
AFC:AFC_INACTIVE:AFC_FAST:AFC_R_TRK                          d7a5 0 06 2    @olive @white
AFC:AFC_INACTIVE:AFC_FAST:AFC_R_ACQ                          d7a5 0 06 3    @olive @white
AFC:AFC_INACTIVE:AFC_FAST:AFC_ACQ_DONE                       d7a5 0 06 4    @olive @white
AFC:AFC_INACTIVE:AFC_FAST:AFC_VCO_PULL_IN                    d7a5 0 06 5    @olive @white

AFC:AFC_INACTIVE:AFC_SLOW:AFC_INACTIVE                       d7a5 0 07 0    @olive @white
AFC:AFC_INACTIVE:AFC_SLOW:AFC_IDLE                           d7a5 0 07 1    @olive @white
AFC:AFC_INACTIVE:AFC_SLOW:AFC_R_TRK                          d7a5 0 07 2    @olive @white
AFC:AFC_INACTIVE:AFC_SLOW:AFC_R_ACQ                          d7a5 0 07 3    @olive @white
AFC:AFC_INACTIVE:AFC_SLOW:AFC_ACQ_DONE                       d7a5 0 07 4    @olive @white
AFC:AFC_INACTIVE:AFC_SLOW:AFC_VCO_PULL_IN                    d7a5 0 07 5    @olive @white

AFC:AFC_INACTIVE:AFC_XO:AFC_INACTIVE                         d7a5 0 08 0    @olive @white
AFC:AFC_INACTIVE:AFC_XO:AFC_IDLE                             d7a5 0 08 1    @olive @white
AFC:AFC_INACTIVE:AFC_XO:AFC_R_TRK                            d7a5 0 08 2    @olive @white
AFC:AFC_INACTIVE:AFC_XO:AFC_R_ACQ                            d7a5 0 08 3    @olive @white
AFC:AFC_INACTIVE:AFC_XO:AFC_ACQ_DONE                         d7a5 0 08 4    @olive @white
AFC:AFC_INACTIVE:AFC_XO:AFC_VCO_PULL_IN                      d7a5 0 08 5    @olive @white

AFC:AFC_INACTIVE:AFC_SRL:AFC_INACTIVE                        d7a5 0 09 0    @olive @white
AFC:AFC_INACTIVE:AFC_SRL:AFC_IDLE                            d7a5 0 09 1    @olive @white
AFC:AFC_INACTIVE:AFC_SRL:AFC_R_TRK                           d7a5 0 09 2    @olive @white
AFC:AFC_INACTIVE:AFC_SRL:AFC_R_ACQ                           d7a5 0 09 3    @olive @white
AFC:AFC_INACTIVE:AFC_SRL:AFC_ACQ_DONE                        d7a5 0 09 4    @olive @white
AFC:AFC_INACTIVE:AFC_SRL:AFC_VCO_PULL_IN                     d7a5 0 09 5    @olive @white

AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_INACTIVE               d7a5 0 0a 0    @olive @white
AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_IDLE                   d7a5 0 0a 1    @olive @white
AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_R_TRK                  d7a5 0 0a 2    @olive @white
AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_R_ACQ                  d7a5 0 0a 3    @olive @white
AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_ACQ_DONE               d7a5 0 0a 4    @olive @white
AFC:AFC_INACTIVE:AFC_SANITY_TIMER:AFC_VCO_PULL_IN            d7a5 0 0a 5    @olive @white

AFC:AFC_INACTIVE:AFC_OFF:AFC_INACTIVE                        d7a5 0 0b 0    @olive @white
AFC:AFC_INACTIVE:AFC_OFF:AFC_IDLE                            d7a5 0 0b 1    @olive @white
AFC:AFC_INACTIVE:AFC_OFF:AFC_R_TRK                           d7a5 0 0b 2    @olive @white
AFC:AFC_INACTIVE:AFC_OFF:AFC_R_ACQ                           d7a5 0 0b 3    @olive @white
AFC:AFC_INACTIVE:AFC_OFF:AFC_ACQ_DONE                        d7a5 0 0b 4    @olive @white
AFC:AFC_INACTIVE:AFC_OFF:AFC_VCO_PULL_IN                     d7a5 0 0b 5    @olive @white

AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_INACTIVE                  d7a5 0 0c 0    @olive @white
AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_IDLE                      d7a5 0 0c 1    @olive @white
AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_R_TRK                     d7a5 0 0c 2    @olive @white
AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_R_ACQ                     d7a5 0 0c 3    @olive @white
AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_ACQ_DONE                  d7a5 0 0c 4    @olive @white
AFC:AFC_INACTIVE:AFC_LOG_TIMER:AFC_VCO_PULL_IN               d7a5 0 0c 5    @olive @white

AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_INACTIVE                  d7a5 0 0d 0    @olive @white
AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_IDLE                      d7a5 0 0d 1    @olive @white
AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_R_TRK                     d7a5 0 0d 2    @olive @white
AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_R_ACQ                     d7a5 0 0d 3    @olive @white
AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_ACQ_DONE                  d7a5 0 0d 4    @olive @white
AFC:AFC_INACTIVE:AFC_START_ACQ:AFC_VCO_PULL_IN               d7a5 0 0d 5    @olive @white

AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_INACTIVE                    d7a5 0 0e 0    @olive @white
AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_IDLE                        d7a5 0 0e 1    @olive @white
AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_R_TRK                       d7a5 0 0e 2    @olive @white
AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_R_ACQ                       d7a5 0 0e 3    @olive @white
AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_ACQ_DONE                    d7a5 0 0e 4    @olive @white
AFC:AFC_INACTIVE:AFC_TRAFFIC:AFC_VCO_PULL_IN                 d7a5 0 0e 5    @olive @white

AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_INACTIVE                    d7a5 0 0f 0    @olive @white
AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_IDLE                        d7a5 0 0f 1    @olive @white
AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_R_TRK                       d7a5 0 0f 2    @olive @white
AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_R_ACQ                       d7a5 0 0f 3    @olive @white
AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_ACQ_DONE                    d7a5 0 0f 4    @olive @white
AFC:AFC_INACTIVE:AFC_TC_EXIT:AFC_VCO_PULL_IN                 d7a5 0 0f 5    @olive @white

AFC:AFC_INACTIVE:AFC_ACCESS:AFC_INACTIVE                     d7a5 0 10 0    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS:AFC_IDLE                         d7a5 0 10 1    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS:AFC_R_TRK                        d7a5 0 10 2    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS:AFC_R_ACQ                        d7a5 0 10 3    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS:AFC_ACQ_DONE                     d7a5 0 10 4    @olive @white
AFC:AFC_INACTIVE:AFC_ACCESS:AFC_VCO_PULL_IN                  d7a5 0 10 5    @olive @white

AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE      d7a5 0 11 0    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE          d7a5 0 11 1    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK         d7a5 0 11 2    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ         d7a5 0 11 3    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE      d7a5 0 11 4    @olive @white
AFC:AFC_INACTIVE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN   d7a5 0 11 5    @olive @white

AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_INACTIVE               d7a5 0 12 0    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_IDLE                   d7a5 0 12 1    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_R_TRK                  d7a5 0 12 2    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_R_ACQ                  d7a5 0 12 3    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_ACQ_DONE               d7a5 0 12 4    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN            d7a5 0 12 5    @olive @white

AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_INACTIVE               d7a5 0 13 0    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_IDLE                   d7a5 0 13 1    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_R_TRK                  d7a5 0 13 2    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_R_ACQ                  d7a5 0 13 3    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_ACQ_DONE               d7a5 0 13 4    @olive @white
AFC:AFC_INACTIVE:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN            d7a5 0 13 5    @olive @white

AFC:AFC_IDLE:AFC_CDMA:AFC_INACTIVE                           d7a5 1 00 0    @olive @white
AFC:AFC_IDLE:AFC_CDMA:AFC_IDLE                               d7a5 1 00 1    @olive @white
AFC:AFC_IDLE:AFC_CDMA:AFC_R_TRK                              d7a5 1 00 2    @olive @white
AFC:AFC_IDLE:AFC_CDMA:AFC_R_ACQ                              d7a5 1 00 3    @olive @white
AFC:AFC_IDLE:AFC_CDMA:AFC_ACQ_DONE                           d7a5 1 00 4    @olive @white
AFC:AFC_IDLE:AFC_CDMA:AFC_VCO_PULL_IN                        d7a5 1 00 5    @olive @white

AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_INACTIVE                  d7a5 1 01 0    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_IDLE                      d7a5 1 01 1    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_R_TRK                     d7a5 1 01 2    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_R_ACQ                     d7a5 1 01 3    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE                  d7a5 1 01 4    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN               d7a5 1 01 5    @olive @white

AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_INACTIVE                  d7a5 1 02 0    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_IDLE                      d7a5 1 02 1    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_R_TRK                     d7a5 1 02 2    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_R_ACQ                     d7a5 1 02 3    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE                  d7a5 1 02 4    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN               d7a5 1 02 5    @olive @white

AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_INACTIVE                 d7a5 1 03 0    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_IDLE                     d7a5 1 03 1    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_R_TRK                    d7a5 1 03 2    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_R_ACQ                    d7a5 1 03 3    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE                 d7a5 1 03 4    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN              d7a5 1 03 5    @olive @white

AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE             d7a5 1 04 0    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE                 d7a5 1 04 1    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK                d7a5 1 04 2    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ                d7a5 1 04 3    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE             d7a5 1 04 4    @olive @white
AFC:AFC_IDLE:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN          d7a5 1 04 5    @olive @white

AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_INACTIVE                    d7a5 1 05 0    @olive @white
AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_IDLE                        d7a5 1 05 1    @olive @white
AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_R_TRK                       d7a5 1 05 2    @olive @white
AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_R_ACQ                       d7a5 1 05 3    @olive @white
AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_ACQ_DONE                    d7a5 1 05 4    @olive @white
AFC:AFC_IDLE:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN                 d7a5 1 05 5    @olive @white

AFC:AFC_IDLE:AFC_FAST:AFC_INACTIVE                           d7a5 1 06 0    @olive @white
AFC:AFC_IDLE:AFC_FAST:AFC_IDLE                               d7a5 1 06 1    @olive @white
AFC:AFC_IDLE:AFC_FAST:AFC_R_TRK                              d7a5 1 06 2    @olive @white
AFC:AFC_IDLE:AFC_FAST:AFC_R_ACQ                              d7a5 1 06 3    @olive @white
AFC:AFC_IDLE:AFC_FAST:AFC_ACQ_DONE                           d7a5 1 06 4    @olive @white
AFC:AFC_IDLE:AFC_FAST:AFC_VCO_PULL_IN                        d7a5 1 06 5    @olive @white

AFC:AFC_IDLE:AFC_SLOW:AFC_INACTIVE                           d7a5 1 07 0    @olive @white
AFC:AFC_IDLE:AFC_SLOW:AFC_IDLE                               d7a5 1 07 1    @olive @white
AFC:AFC_IDLE:AFC_SLOW:AFC_R_TRK                              d7a5 1 07 2    @olive @white
AFC:AFC_IDLE:AFC_SLOW:AFC_R_ACQ                              d7a5 1 07 3    @olive @white
AFC:AFC_IDLE:AFC_SLOW:AFC_ACQ_DONE                           d7a5 1 07 4    @olive @white
AFC:AFC_IDLE:AFC_SLOW:AFC_VCO_PULL_IN                        d7a5 1 07 5    @olive @white

AFC:AFC_IDLE:AFC_XO:AFC_INACTIVE                             d7a5 1 08 0    @olive @white
AFC:AFC_IDLE:AFC_XO:AFC_IDLE                                 d7a5 1 08 1    @olive @white
AFC:AFC_IDLE:AFC_XO:AFC_R_TRK                                d7a5 1 08 2    @olive @white
AFC:AFC_IDLE:AFC_XO:AFC_R_ACQ                                d7a5 1 08 3    @olive @white
AFC:AFC_IDLE:AFC_XO:AFC_ACQ_DONE                             d7a5 1 08 4    @olive @white
AFC:AFC_IDLE:AFC_XO:AFC_VCO_PULL_IN                          d7a5 1 08 5    @olive @white

AFC:AFC_IDLE:AFC_SRL:AFC_INACTIVE                            d7a5 1 09 0    @olive @white
AFC:AFC_IDLE:AFC_SRL:AFC_IDLE                                d7a5 1 09 1    @olive @white
AFC:AFC_IDLE:AFC_SRL:AFC_R_TRK                               d7a5 1 09 2    @olive @white
AFC:AFC_IDLE:AFC_SRL:AFC_R_ACQ                               d7a5 1 09 3    @olive @white
AFC:AFC_IDLE:AFC_SRL:AFC_ACQ_DONE                            d7a5 1 09 4    @olive @white
AFC:AFC_IDLE:AFC_SRL:AFC_VCO_PULL_IN                         d7a5 1 09 5    @olive @white

AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_INACTIVE                   d7a5 1 0a 0    @olive @white
AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_IDLE                       d7a5 1 0a 1    @olive @white
AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_R_TRK                      d7a5 1 0a 2    @olive @white
AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_R_ACQ                      d7a5 1 0a 3    @olive @white
AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_ACQ_DONE                   d7a5 1 0a 4    @olive @white
AFC:AFC_IDLE:AFC_SANITY_TIMER:AFC_VCO_PULL_IN                d7a5 1 0a 5    @olive @white

AFC:AFC_IDLE:AFC_OFF:AFC_INACTIVE                            d7a5 1 0b 0    @olive @white
AFC:AFC_IDLE:AFC_OFF:AFC_IDLE                                d7a5 1 0b 1    @olive @white
AFC:AFC_IDLE:AFC_OFF:AFC_R_TRK                               d7a5 1 0b 2    @olive @white
AFC:AFC_IDLE:AFC_OFF:AFC_R_ACQ                               d7a5 1 0b 3    @olive @white
AFC:AFC_IDLE:AFC_OFF:AFC_ACQ_DONE                            d7a5 1 0b 4    @olive @white
AFC:AFC_IDLE:AFC_OFF:AFC_VCO_PULL_IN                         d7a5 1 0b 5    @olive @white

AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_INACTIVE                      d7a5 1 0c 0    @olive @white
AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_IDLE                          d7a5 1 0c 1    @olive @white
AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_R_TRK                         d7a5 1 0c 2    @olive @white
AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_R_ACQ                         d7a5 1 0c 3    @olive @white
AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_ACQ_DONE                      d7a5 1 0c 4    @olive @white
AFC:AFC_IDLE:AFC_LOG_TIMER:AFC_VCO_PULL_IN                   d7a5 1 0c 5    @olive @white

AFC:AFC_IDLE:AFC_START_ACQ:AFC_INACTIVE                      d7a5 1 0d 0    @olive @white
AFC:AFC_IDLE:AFC_START_ACQ:AFC_IDLE                          d7a5 1 0d 1    @olive @white
AFC:AFC_IDLE:AFC_START_ACQ:AFC_R_TRK                         d7a5 1 0d 2    @olive @white
AFC:AFC_IDLE:AFC_START_ACQ:AFC_R_ACQ                         d7a5 1 0d 3    @olive @white
AFC:AFC_IDLE:AFC_START_ACQ:AFC_ACQ_DONE                      d7a5 1 0d 4    @olive @white
AFC:AFC_IDLE:AFC_START_ACQ:AFC_VCO_PULL_IN                   d7a5 1 0d 5    @olive @white

AFC:AFC_IDLE:AFC_TRAFFIC:AFC_INACTIVE                        d7a5 1 0e 0    @olive @white
AFC:AFC_IDLE:AFC_TRAFFIC:AFC_IDLE                            d7a5 1 0e 1    @olive @white
AFC:AFC_IDLE:AFC_TRAFFIC:AFC_R_TRK                           d7a5 1 0e 2    @olive @white
AFC:AFC_IDLE:AFC_TRAFFIC:AFC_R_ACQ                           d7a5 1 0e 3    @olive @white
AFC:AFC_IDLE:AFC_TRAFFIC:AFC_ACQ_DONE                        d7a5 1 0e 4    @olive @white
AFC:AFC_IDLE:AFC_TRAFFIC:AFC_VCO_PULL_IN                     d7a5 1 0e 5    @olive @white

AFC:AFC_IDLE:AFC_TC_EXIT:AFC_INACTIVE                        d7a5 1 0f 0    @olive @white
AFC:AFC_IDLE:AFC_TC_EXIT:AFC_IDLE                            d7a5 1 0f 1    @olive @white
AFC:AFC_IDLE:AFC_TC_EXIT:AFC_R_TRK                           d7a5 1 0f 2    @olive @white
AFC:AFC_IDLE:AFC_TC_EXIT:AFC_R_ACQ                           d7a5 1 0f 3    @olive @white
AFC:AFC_IDLE:AFC_TC_EXIT:AFC_ACQ_DONE                        d7a5 1 0f 4    @olive @white
AFC:AFC_IDLE:AFC_TC_EXIT:AFC_VCO_PULL_IN                     d7a5 1 0f 5    @olive @white

AFC:AFC_IDLE:AFC_ACCESS:AFC_INACTIVE                         d7a5 1 10 0    @olive @white
AFC:AFC_IDLE:AFC_ACCESS:AFC_IDLE                             d7a5 1 10 1    @olive @white
AFC:AFC_IDLE:AFC_ACCESS:AFC_R_TRK                            d7a5 1 10 2    @olive @white
AFC:AFC_IDLE:AFC_ACCESS:AFC_R_ACQ                            d7a5 1 10 3    @olive @white
AFC:AFC_IDLE:AFC_ACCESS:AFC_ACQ_DONE                         d7a5 1 10 4    @olive @white
AFC:AFC_IDLE:AFC_ACCESS:AFC_VCO_PULL_IN                      d7a5 1 10 5    @olive @white

AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE          d7a5 1 11 0    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE              d7a5 1 11 1    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK             d7a5 1 11 2    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ             d7a5 1 11 3    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE          d7a5 1 11 4    @olive @white
AFC:AFC_IDLE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN       d7a5 1 11 5    @olive @white

AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_INACTIVE                   d7a5 1 12 0    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_IDLE                       d7a5 1 12 1    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_R_TRK                      d7a5 1 12 2    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_R_ACQ                      d7a5 1 12 3    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_ACQ_DONE                   d7a5 1 12 4    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN                d7a5 1 12 5    @olive @white

AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_INACTIVE                   d7a5 1 13 0    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_IDLE                       d7a5 1 13 1    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_R_TRK                      d7a5 1 13 2    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_R_ACQ                      d7a5 1 13 3    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_ACQ_DONE                   d7a5 1 13 4    @olive @white
AFC:AFC_IDLE:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN                d7a5 1 13 5    @olive @white

AFC:AFC_R_TRK:AFC_CDMA:AFC_INACTIVE                          d7a5 2 00 0    @olive @white
AFC:AFC_R_TRK:AFC_CDMA:AFC_IDLE                              d7a5 2 00 1    @olive @white
AFC:AFC_R_TRK:AFC_CDMA:AFC_R_TRK                             d7a5 2 00 2    @olive @white
AFC:AFC_R_TRK:AFC_CDMA:AFC_R_ACQ                             d7a5 2 00 3    @olive @white
AFC:AFC_R_TRK:AFC_CDMA:AFC_ACQ_DONE                          d7a5 2 00 4    @olive @white
AFC:AFC_R_TRK:AFC_CDMA:AFC_VCO_PULL_IN                       d7a5 2 00 5    @olive @white

AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_INACTIVE                 d7a5 2 01 0    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_IDLE                     d7a5 2 01 1    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_R_TRK                    d7a5 2 01 2    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_R_ACQ                    d7a5 2 01 3    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE                 d7a5 2 01 4    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN              d7a5 2 01 5    @olive @white

AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_INACTIVE                 d7a5 2 02 0    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_IDLE                     d7a5 2 02 1    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_R_TRK                    d7a5 2 02 2    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_R_ACQ                    d7a5 2 02 3    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE                 d7a5 2 02 4    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN              d7a5 2 02 5    @olive @white

AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_INACTIVE                d7a5 2 03 0    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_IDLE                    d7a5 2 03 1    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_R_TRK                   d7a5 2 03 2    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_R_ACQ                   d7a5 2 03 3    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE                d7a5 2 03 4    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN             d7a5 2 03 5    @olive @white

AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE            d7a5 2 04 0    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE                d7a5 2 04 1    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK               d7a5 2 04 2    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ               d7a5 2 04 3    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE            d7a5 2 04 4    @olive @white
AFC:AFC_R_TRK:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN         d7a5 2 04 5    @olive @white

AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_INACTIVE                   d7a5 2 05 0    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_IDLE                       d7a5 2 05 1    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_R_TRK                      d7a5 2 05 2    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_R_ACQ                      d7a5 2 05 3    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_ACQ_DONE                   d7a5 2 05 4    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN                d7a5 2 05 5    @olive @white

AFC:AFC_R_TRK:AFC_FAST:AFC_INACTIVE                          d7a5 2 06 0    @olive @white
AFC:AFC_R_TRK:AFC_FAST:AFC_IDLE                              d7a5 2 06 1    @olive @white
AFC:AFC_R_TRK:AFC_FAST:AFC_R_TRK                             d7a5 2 06 2    @olive @white
AFC:AFC_R_TRK:AFC_FAST:AFC_R_ACQ                             d7a5 2 06 3    @olive @white
AFC:AFC_R_TRK:AFC_FAST:AFC_ACQ_DONE                          d7a5 2 06 4    @olive @white
AFC:AFC_R_TRK:AFC_FAST:AFC_VCO_PULL_IN                       d7a5 2 06 5    @olive @white

AFC:AFC_R_TRK:AFC_SLOW:AFC_INACTIVE                          d7a5 2 07 0    @olive @white
AFC:AFC_R_TRK:AFC_SLOW:AFC_IDLE                              d7a5 2 07 1    @olive @white
AFC:AFC_R_TRK:AFC_SLOW:AFC_R_TRK                             d7a5 2 07 2    @olive @white
AFC:AFC_R_TRK:AFC_SLOW:AFC_R_ACQ                             d7a5 2 07 3    @olive @white
AFC:AFC_R_TRK:AFC_SLOW:AFC_ACQ_DONE                          d7a5 2 07 4    @olive @white
AFC:AFC_R_TRK:AFC_SLOW:AFC_VCO_PULL_IN                       d7a5 2 07 5    @olive @white

AFC:AFC_R_TRK:AFC_XO:AFC_INACTIVE                            d7a5 2 08 0    @olive @white
AFC:AFC_R_TRK:AFC_XO:AFC_IDLE                                d7a5 2 08 1    @olive @white
AFC:AFC_R_TRK:AFC_XO:AFC_R_TRK                               d7a5 2 08 2    @olive @white
AFC:AFC_R_TRK:AFC_XO:AFC_R_ACQ                               d7a5 2 08 3    @olive @white
AFC:AFC_R_TRK:AFC_XO:AFC_ACQ_DONE                            d7a5 2 08 4    @olive @white
AFC:AFC_R_TRK:AFC_XO:AFC_VCO_PULL_IN                         d7a5 2 08 5    @olive @white

AFC:AFC_R_TRK:AFC_SRL:AFC_INACTIVE                           d7a5 2 09 0    @olive @white
AFC:AFC_R_TRK:AFC_SRL:AFC_IDLE                               d7a5 2 09 1    @olive @white
AFC:AFC_R_TRK:AFC_SRL:AFC_R_TRK                              d7a5 2 09 2    @olive @white
AFC:AFC_R_TRK:AFC_SRL:AFC_R_ACQ                              d7a5 2 09 3    @olive @white
AFC:AFC_R_TRK:AFC_SRL:AFC_ACQ_DONE                           d7a5 2 09 4    @olive @white
AFC:AFC_R_TRK:AFC_SRL:AFC_VCO_PULL_IN                        d7a5 2 09 5    @olive @white

AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_INACTIVE                  d7a5 2 0a 0    @olive @white
AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_IDLE                      d7a5 2 0a 1    @olive @white
AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_R_TRK                     d7a5 2 0a 2    @olive @white
AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_R_ACQ                     d7a5 2 0a 3    @olive @white
AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_ACQ_DONE                  d7a5 2 0a 4    @olive @white
AFC:AFC_R_TRK:AFC_SANITY_TIMER:AFC_VCO_PULL_IN               d7a5 2 0a 5    @olive @white

AFC:AFC_R_TRK:AFC_OFF:AFC_INACTIVE                           d7a5 2 0b 0    @olive @white
AFC:AFC_R_TRK:AFC_OFF:AFC_IDLE                               d7a5 2 0b 1    @olive @white
AFC:AFC_R_TRK:AFC_OFF:AFC_R_TRK                              d7a5 2 0b 2    @olive @white
AFC:AFC_R_TRK:AFC_OFF:AFC_R_ACQ                              d7a5 2 0b 3    @olive @white
AFC:AFC_R_TRK:AFC_OFF:AFC_ACQ_DONE                           d7a5 2 0b 4    @olive @white
AFC:AFC_R_TRK:AFC_OFF:AFC_VCO_PULL_IN                        d7a5 2 0b 5    @olive @white

AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_INACTIVE                     d7a5 2 0c 0    @olive @white
AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_IDLE                         d7a5 2 0c 1    @olive @white
AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_R_TRK                        d7a5 2 0c 2    @olive @white
AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_R_ACQ                        d7a5 2 0c 3    @olive @white
AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_ACQ_DONE                     d7a5 2 0c 4    @olive @white
AFC:AFC_R_TRK:AFC_LOG_TIMER:AFC_VCO_PULL_IN                  d7a5 2 0c 5    @olive @white

AFC:AFC_R_TRK:AFC_START_ACQ:AFC_INACTIVE                     d7a5 2 0d 0    @olive @white
AFC:AFC_R_TRK:AFC_START_ACQ:AFC_IDLE                         d7a5 2 0d 1    @olive @white
AFC:AFC_R_TRK:AFC_START_ACQ:AFC_R_TRK                        d7a5 2 0d 2    @olive @white
AFC:AFC_R_TRK:AFC_START_ACQ:AFC_R_ACQ                        d7a5 2 0d 3    @olive @white
AFC:AFC_R_TRK:AFC_START_ACQ:AFC_ACQ_DONE                     d7a5 2 0d 4    @olive @white
AFC:AFC_R_TRK:AFC_START_ACQ:AFC_VCO_PULL_IN                  d7a5 2 0d 5    @olive @white

AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_INACTIVE                       d7a5 2 0e 0    @olive @white
AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_IDLE                           d7a5 2 0e 1    @olive @white
AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_R_TRK                          d7a5 2 0e 2    @olive @white
AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_R_ACQ                          d7a5 2 0e 3    @olive @white
AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_ACQ_DONE                       d7a5 2 0e 4    @olive @white
AFC:AFC_R_TRK:AFC_TRAFFIC:AFC_VCO_PULL_IN                    d7a5 2 0e 5    @olive @white

AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_INACTIVE                       d7a5 2 0f 0    @olive @white
AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_IDLE                           d7a5 2 0f 1    @olive @white
AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_R_TRK                          d7a5 2 0f 2    @olive @white
AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_R_ACQ                          d7a5 2 0f 3    @olive @white
AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_ACQ_DONE                       d7a5 2 0f 4    @olive @white
AFC:AFC_R_TRK:AFC_TC_EXIT:AFC_VCO_PULL_IN                    d7a5 2 0f 5    @olive @white

AFC:AFC_R_TRK:AFC_ACCESS:AFC_INACTIVE                        d7a5 2 10 0    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS:AFC_IDLE                            d7a5 2 10 1    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS:AFC_R_TRK                           d7a5 2 10 2    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS:AFC_R_ACQ                           d7a5 2 10 3    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS:AFC_ACQ_DONE                        d7a5 2 10 4    @olive @white
AFC:AFC_R_TRK:AFC_ACCESS:AFC_VCO_PULL_IN                     d7a5 2 10 5    @olive @white

AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE         d7a5 2 11 0    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE             d7a5 2 11 1    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK            d7a5 2 11 2    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ            d7a5 2 11 3    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE         d7a5 2 11 4    @olive @white
AFC:AFC_R_TRK:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN      d7a5 2 11 5    @olive @white

AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_INACTIVE                  d7a5 2 12 0    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_IDLE                      d7a5 2 12 1    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_R_TRK                     d7a5 2 12 2    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_R_ACQ                     d7a5 2 12 3    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_ACQ_DONE                  d7a5 2 12 4    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN               d7a5 2 12 5    @olive @white

AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_INACTIVE                  d7a5 2 13 0    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_IDLE                      d7a5 2 13 1    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_R_TRK                     d7a5 2 13 2    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_R_ACQ                     d7a5 2 13 3    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_ACQ_DONE                  d7a5 2 13 4    @olive @white
AFC:AFC_R_TRK:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN               d7a5 2 13 5    @olive @white

AFC:AFC_R_ACQ:AFC_CDMA:AFC_INACTIVE                          d7a5 3 00 0    @olive @white
AFC:AFC_R_ACQ:AFC_CDMA:AFC_IDLE                              d7a5 3 00 1    @olive @white
AFC:AFC_R_ACQ:AFC_CDMA:AFC_R_TRK                             d7a5 3 00 2    @olive @white
AFC:AFC_R_ACQ:AFC_CDMA:AFC_R_ACQ                             d7a5 3 00 3    @olive @white
AFC:AFC_R_ACQ:AFC_CDMA:AFC_ACQ_DONE                          d7a5 3 00 4    @olive @white
AFC:AFC_R_ACQ:AFC_CDMA:AFC_VCO_PULL_IN                       d7a5 3 00 5    @olive @white

AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_INACTIVE                 d7a5 3 01 0    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_IDLE                     d7a5 3 01 1    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_R_TRK                    d7a5 3 01 2    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_R_ACQ                    d7a5 3 01 3    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE                 d7a5 3 01 4    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN              d7a5 3 01 5    @olive @white

AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_INACTIVE                 d7a5 3 02 0    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_IDLE                     d7a5 3 02 1    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_R_TRK                    d7a5 3 02 2    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_R_ACQ                    d7a5 3 02 3    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE                 d7a5 3 02 4    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN              d7a5 3 02 5    @olive @white

AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_INACTIVE                d7a5 3 03 0    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_IDLE                    d7a5 3 03 1    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_R_TRK                   d7a5 3 03 2    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_R_ACQ                   d7a5 3 03 3    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE                d7a5 3 03 4    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN             d7a5 3 03 5    @olive @white

AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE            d7a5 3 04 0    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE                d7a5 3 04 1    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK               d7a5 3 04 2    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ               d7a5 3 04 3    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE            d7a5 3 04 4    @olive @white
AFC:AFC_R_ACQ:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN         d7a5 3 04 5    @olive @white

AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_INACTIVE                   d7a5 3 05 0    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_IDLE                       d7a5 3 05 1    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_R_TRK                      d7a5 3 05 2    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_R_ACQ                      d7a5 3 05 3    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_ACQ_DONE                   d7a5 3 05 4    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN                d7a5 3 05 5    @olive @white

AFC:AFC_R_ACQ:AFC_FAST:AFC_INACTIVE                          d7a5 3 06 0    @olive @white
AFC:AFC_R_ACQ:AFC_FAST:AFC_IDLE                              d7a5 3 06 1    @olive @white
AFC:AFC_R_ACQ:AFC_FAST:AFC_R_TRK                             d7a5 3 06 2    @olive @white
AFC:AFC_R_ACQ:AFC_FAST:AFC_R_ACQ                             d7a5 3 06 3    @olive @white
AFC:AFC_R_ACQ:AFC_FAST:AFC_ACQ_DONE                          d7a5 3 06 4    @olive @white
AFC:AFC_R_ACQ:AFC_FAST:AFC_VCO_PULL_IN                       d7a5 3 06 5    @olive @white

AFC:AFC_R_ACQ:AFC_SLOW:AFC_INACTIVE                          d7a5 3 07 0    @olive @white
AFC:AFC_R_ACQ:AFC_SLOW:AFC_IDLE                              d7a5 3 07 1    @olive @white
AFC:AFC_R_ACQ:AFC_SLOW:AFC_R_TRK                             d7a5 3 07 2    @olive @white
AFC:AFC_R_ACQ:AFC_SLOW:AFC_R_ACQ                             d7a5 3 07 3    @olive @white
AFC:AFC_R_ACQ:AFC_SLOW:AFC_ACQ_DONE                          d7a5 3 07 4    @olive @white
AFC:AFC_R_ACQ:AFC_SLOW:AFC_VCO_PULL_IN                       d7a5 3 07 5    @olive @white

AFC:AFC_R_ACQ:AFC_XO:AFC_INACTIVE                            d7a5 3 08 0    @olive @white
AFC:AFC_R_ACQ:AFC_XO:AFC_IDLE                                d7a5 3 08 1    @olive @white
AFC:AFC_R_ACQ:AFC_XO:AFC_R_TRK                               d7a5 3 08 2    @olive @white
AFC:AFC_R_ACQ:AFC_XO:AFC_R_ACQ                               d7a5 3 08 3    @olive @white
AFC:AFC_R_ACQ:AFC_XO:AFC_ACQ_DONE                            d7a5 3 08 4    @olive @white
AFC:AFC_R_ACQ:AFC_XO:AFC_VCO_PULL_IN                         d7a5 3 08 5    @olive @white

AFC:AFC_R_ACQ:AFC_SRL:AFC_INACTIVE                           d7a5 3 09 0    @olive @white
AFC:AFC_R_ACQ:AFC_SRL:AFC_IDLE                               d7a5 3 09 1    @olive @white
AFC:AFC_R_ACQ:AFC_SRL:AFC_R_TRK                              d7a5 3 09 2    @olive @white
AFC:AFC_R_ACQ:AFC_SRL:AFC_R_ACQ                              d7a5 3 09 3    @olive @white
AFC:AFC_R_ACQ:AFC_SRL:AFC_ACQ_DONE                           d7a5 3 09 4    @olive @white
AFC:AFC_R_ACQ:AFC_SRL:AFC_VCO_PULL_IN                        d7a5 3 09 5    @olive @white

AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_INACTIVE                  d7a5 3 0a 0    @olive @white
AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_IDLE                      d7a5 3 0a 1    @olive @white
AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_R_TRK                     d7a5 3 0a 2    @olive @white
AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_R_ACQ                     d7a5 3 0a 3    @olive @white
AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_ACQ_DONE                  d7a5 3 0a 4    @olive @white
AFC:AFC_R_ACQ:AFC_SANITY_TIMER:AFC_VCO_PULL_IN               d7a5 3 0a 5    @olive @white

AFC:AFC_R_ACQ:AFC_OFF:AFC_INACTIVE                           d7a5 3 0b 0    @olive @white
AFC:AFC_R_ACQ:AFC_OFF:AFC_IDLE                               d7a5 3 0b 1    @olive @white
AFC:AFC_R_ACQ:AFC_OFF:AFC_R_TRK                              d7a5 3 0b 2    @olive @white
AFC:AFC_R_ACQ:AFC_OFF:AFC_R_ACQ                              d7a5 3 0b 3    @olive @white
AFC:AFC_R_ACQ:AFC_OFF:AFC_ACQ_DONE                           d7a5 3 0b 4    @olive @white
AFC:AFC_R_ACQ:AFC_OFF:AFC_VCO_PULL_IN                        d7a5 3 0b 5    @olive @white

AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_INACTIVE                     d7a5 3 0c 0    @olive @white
AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_IDLE                         d7a5 3 0c 1    @olive @white
AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_R_TRK                        d7a5 3 0c 2    @olive @white
AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_R_ACQ                        d7a5 3 0c 3    @olive @white
AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_ACQ_DONE                     d7a5 3 0c 4    @olive @white
AFC:AFC_R_ACQ:AFC_LOG_TIMER:AFC_VCO_PULL_IN                  d7a5 3 0c 5    @olive @white

AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_INACTIVE                     d7a5 3 0d 0    @olive @white
AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_IDLE                         d7a5 3 0d 1    @olive @white
AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_R_TRK                        d7a5 3 0d 2    @olive @white
AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_R_ACQ                        d7a5 3 0d 3    @olive @white
AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_ACQ_DONE                     d7a5 3 0d 4    @olive @white
AFC:AFC_R_ACQ:AFC_START_ACQ:AFC_VCO_PULL_IN                  d7a5 3 0d 5    @olive @white

AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_INACTIVE                       d7a5 3 0e 0    @olive @white
AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_IDLE                           d7a5 3 0e 1    @olive @white
AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_R_TRK                          d7a5 3 0e 2    @olive @white
AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_R_ACQ                          d7a5 3 0e 3    @olive @white
AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_ACQ_DONE                       d7a5 3 0e 4    @olive @white
AFC:AFC_R_ACQ:AFC_TRAFFIC:AFC_VCO_PULL_IN                    d7a5 3 0e 5    @olive @white

AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_INACTIVE                       d7a5 3 0f 0    @olive @white
AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_IDLE                           d7a5 3 0f 1    @olive @white
AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_R_TRK                          d7a5 3 0f 2    @olive @white
AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_R_ACQ                          d7a5 3 0f 3    @olive @white
AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_ACQ_DONE                       d7a5 3 0f 4    @olive @white
AFC:AFC_R_ACQ:AFC_TC_EXIT:AFC_VCO_PULL_IN                    d7a5 3 0f 5    @olive @white

AFC:AFC_R_ACQ:AFC_ACCESS:AFC_INACTIVE                        d7a5 3 10 0    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS:AFC_IDLE                            d7a5 3 10 1    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS:AFC_R_TRK                           d7a5 3 10 2    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS:AFC_R_ACQ                           d7a5 3 10 3    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS:AFC_ACQ_DONE                        d7a5 3 10 4    @olive @white
AFC:AFC_R_ACQ:AFC_ACCESS:AFC_VCO_PULL_IN                     d7a5 3 10 5    @olive @white

AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE         d7a5 3 11 0    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE             d7a5 3 11 1    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK            d7a5 3 11 2    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ            d7a5 3 11 3    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE         d7a5 3 11 4    @olive @white
AFC:AFC_R_ACQ:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN      d7a5 3 11 5    @olive @white

AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_INACTIVE                  d7a5 3 12 0    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_IDLE                      d7a5 3 12 1    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_R_TRK                     d7a5 3 12 2    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_R_ACQ                     d7a5 3 12 3    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_ACQ_DONE                  d7a5 3 12 4    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN               d7a5 3 12 5    @olive @white

AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_INACTIVE                  d7a5 3 13 0    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_IDLE                      d7a5 3 13 1    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_R_TRK                     d7a5 3 13 2    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_R_ACQ                     d7a5 3 13 3    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_ACQ_DONE                  d7a5 3 13 4    @olive @white
AFC:AFC_R_ACQ:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN               d7a5 3 13 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_INACTIVE                       d7a5 4 00 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_IDLE                           d7a5 4 00 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_R_TRK                          d7a5 4 00 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_R_ACQ                          d7a5 4 00 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_ACQ_DONE                       d7a5 4 00 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_CDMA:AFC_VCO_PULL_IN                    d7a5 4 00 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_INACTIVE              d7a5 4 01 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_IDLE                  d7a5 4 01 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_R_TRK                 d7a5 4 01 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_R_ACQ                 d7a5 4 01 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE              d7a5 4 01 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN           d7a5 4 01 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_INACTIVE              d7a5 4 02 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_IDLE                  d7a5 4 02 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_R_TRK                 d7a5 4 02 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_R_ACQ                 d7a5 4 02 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE              d7a5 4 02 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN           d7a5 4 02 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_INACTIVE             d7a5 4 03 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_IDLE                 d7a5 4 03 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_R_TRK                d7a5 4 03 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_R_ACQ                d7a5 4 03 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE             d7a5 4 03 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN          d7a5 4 03 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE         d7a5 4 04 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE             d7a5 4 04 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK            d7a5 4 04 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ            d7a5 4 04 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE         d7a5 4 04 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN      d7a5 4 04 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_INACTIVE                d7a5 4 05 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_IDLE                    d7a5 4 05 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_R_TRK                   d7a5 4 05 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_R_ACQ                   d7a5 4 05 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_ACQ_DONE                d7a5 4 05 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN             d7a5 4 05 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_FAST:AFC_INACTIVE                       d7a5 4 06 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_FAST:AFC_IDLE                           d7a5 4 06 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_FAST:AFC_R_TRK                          d7a5 4 06 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_FAST:AFC_R_ACQ                          d7a5 4 06 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_FAST:AFC_ACQ_DONE                       d7a5 4 06 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_FAST:AFC_VCO_PULL_IN                    d7a5 4 06 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_INACTIVE                       d7a5 4 07 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_IDLE                           d7a5 4 07 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_R_TRK                          d7a5 4 07 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_R_ACQ                          d7a5 4 07 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_ACQ_DONE                       d7a5 4 07 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_SLOW:AFC_VCO_PULL_IN                    d7a5 4 07 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_XO:AFC_INACTIVE                         d7a5 4 08 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_XO:AFC_IDLE                             d7a5 4 08 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_XO:AFC_R_TRK                            d7a5 4 08 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_XO:AFC_R_ACQ                            d7a5 4 08 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_XO:AFC_ACQ_DONE                         d7a5 4 08 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_XO:AFC_VCO_PULL_IN                      d7a5 4 08 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_SRL:AFC_INACTIVE                        d7a5 4 09 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_SRL:AFC_IDLE                            d7a5 4 09 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_SRL:AFC_R_TRK                           d7a5 4 09 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_SRL:AFC_R_ACQ                           d7a5 4 09 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_SRL:AFC_ACQ_DONE                        d7a5 4 09 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_SRL:AFC_VCO_PULL_IN                     d7a5 4 09 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_INACTIVE               d7a5 4 0a 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_IDLE                   d7a5 4 0a 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_R_TRK                  d7a5 4 0a 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_R_ACQ                  d7a5 4 0a 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_ACQ_DONE               d7a5 4 0a 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_SANITY_TIMER:AFC_VCO_PULL_IN            d7a5 4 0a 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_OFF:AFC_INACTIVE                        d7a5 4 0b 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_OFF:AFC_IDLE                            d7a5 4 0b 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_OFF:AFC_R_TRK                           d7a5 4 0b 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_OFF:AFC_R_ACQ                           d7a5 4 0b 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_OFF:AFC_ACQ_DONE                        d7a5 4 0b 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_OFF:AFC_VCO_PULL_IN                     d7a5 4 0b 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_INACTIVE                  d7a5 4 0c 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_IDLE                      d7a5 4 0c 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_R_TRK                     d7a5 4 0c 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_R_ACQ                     d7a5 4 0c 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_ACQ_DONE                  d7a5 4 0c 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_LOG_TIMER:AFC_VCO_PULL_IN               d7a5 4 0c 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_INACTIVE                  d7a5 4 0d 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_IDLE                      d7a5 4 0d 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_R_TRK                     d7a5 4 0d 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_R_ACQ                     d7a5 4 0d 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_ACQ_DONE                  d7a5 4 0d 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_START_ACQ:AFC_VCO_PULL_IN               d7a5 4 0d 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_INACTIVE                    d7a5 4 0e 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_IDLE                        d7a5 4 0e 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_R_TRK                       d7a5 4 0e 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_R_ACQ                       d7a5 4 0e 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_ACQ_DONE                    d7a5 4 0e 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_TRAFFIC:AFC_VCO_PULL_IN                 d7a5 4 0e 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_INACTIVE                    d7a5 4 0f 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_IDLE                        d7a5 4 0f 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_R_TRK                       d7a5 4 0f 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_R_ACQ                       d7a5 4 0f 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_ACQ_DONE                    d7a5 4 0f 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_TC_EXIT:AFC_VCO_PULL_IN                 d7a5 4 0f 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_INACTIVE                     d7a5 4 10 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_IDLE                         d7a5 4 10 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_R_TRK                        d7a5 4 10 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_R_ACQ                        d7a5 4 10 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_ACQ_DONE                     d7a5 4 10 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_ACCESS:AFC_VCO_PULL_IN                  d7a5 4 10 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE      d7a5 4 11 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE          d7a5 4 11 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK         d7a5 4 11 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ         d7a5 4 11 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE      d7a5 4 11 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN   d7a5 4 11 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_INACTIVE               d7a5 4 12 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_IDLE                   d7a5 4 12 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_R_TRK                  d7a5 4 12 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_R_ACQ                  d7a5 4 12 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_ACQ_DONE               d7a5 4 12 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN            d7a5 4 12 5    @olive @white

AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_INACTIVE               d7a5 4 13 0    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_IDLE                   d7a5 4 13 1    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_R_TRK                  d7a5 4 13 2    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_R_ACQ                  d7a5 4 13 3    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_ACQ_DONE               d7a5 4 13 4    @olive @white
AFC:AFC_ACQ_DONE:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN            d7a5 4 13 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_INACTIVE                    d7a5 5 00 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_IDLE                        d7a5 5 00 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_R_TRK                       d7a5 5 00 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_R_ACQ                       d7a5 5 00 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_ACQ_DONE                    d7a5 5 00 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_CDMA:AFC_VCO_PULL_IN                 d7a5 5 00 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_INACTIVE           d7a5 5 01 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_IDLE               d7a5 5 01 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_R_TRK              d7a5 5 01 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_R_ACQ              d7a5 5 01 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_ACQ_DONE           d7a5 5 01 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_ON:AFC_VCO_PULL_IN        d7a5 5 01 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_INACTIVE           d7a5 5 02 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_IDLE               d7a5 5 02 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_R_TRK              d7a5 5 02 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_R_ACQ              d7a5 5 02 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_ACQ_DONE           d7a5 5 02 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_FLAG:AFC_VCO_PULL_IN        d7a5 5 02 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_INACTIVE          d7a5 5 03 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_IDLE              d7a5 5 03 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_R_TRK             d7a5 5 03 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_R_ACQ             d7a5 5 03 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_ACQ_DONE          d7a5 5 03 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF:AFC_VCO_PULL_IN       d7a5 5 03 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_INACTIVE      d7a5 5 04 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_IDLE          d7a5 5 04 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_R_TRK         d7a5 5 04 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_R_ACQ         d7a5 5 04 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_ACQ_DONE      d7a5 5 04 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FREQ_TRACK_OFF_RTL:AFC_VCO_PULL_IN   d7a5 5 04 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_INACTIVE             d7a5 5 05 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_IDLE                 d7a5 5 05 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_R_TRK                d7a5 5 05 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_R_ACQ                d7a5 5 05 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_ACQ_DONE             d7a5 5 05 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS_EXIT:AFC_VCO_PULL_IN          d7a5 5 05 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_INACTIVE                    d7a5 5 06 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_IDLE                        d7a5 5 06 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_R_TRK                       d7a5 5 06 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_R_ACQ                       d7a5 5 06 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_ACQ_DONE                    d7a5 5 06 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_FAST:AFC_VCO_PULL_IN                 d7a5 5 06 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_INACTIVE                    d7a5 5 07 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_IDLE                        d7a5 5 07 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_R_TRK                       d7a5 5 07 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_R_ACQ                       d7a5 5 07 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_ACQ_DONE                    d7a5 5 07 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SLOW:AFC_VCO_PULL_IN                 d7a5 5 07 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_INACTIVE                      d7a5 5 08 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_IDLE                          d7a5 5 08 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_R_TRK                         d7a5 5 08 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_R_ACQ                         d7a5 5 08 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_ACQ_DONE                      d7a5 5 08 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_XO:AFC_VCO_PULL_IN                   d7a5 5 08 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_INACTIVE                     d7a5 5 09 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_IDLE                         d7a5 5 09 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_R_TRK                        d7a5 5 09 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_R_ACQ                        d7a5 5 09 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_ACQ_DONE                     d7a5 5 09 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SRL:AFC_VCO_PULL_IN                  d7a5 5 09 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_INACTIVE            d7a5 5 0a 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_IDLE                d7a5 5 0a 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_R_TRK               d7a5 5 0a 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_R_ACQ               d7a5 5 0a 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_ACQ_DONE            d7a5 5 0a 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_SANITY_TIMER:AFC_VCO_PULL_IN         d7a5 5 0a 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_INACTIVE                     d7a5 5 0b 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_IDLE                         d7a5 5 0b 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_R_TRK                        d7a5 5 0b 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_R_ACQ                        d7a5 5 0b 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_ACQ_DONE                     d7a5 5 0b 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_OFF:AFC_VCO_PULL_IN                  d7a5 5 0b 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_INACTIVE               d7a5 5 0c 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_IDLE                   d7a5 5 0c 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_R_TRK                  d7a5 5 0c 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_R_ACQ                  d7a5 5 0c 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_ACQ_DONE               d7a5 5 0c 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_LOG_TIMER:AFC_VCO_PULL_IN            d7a5 5 0c 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_INACTIVE               d7a5 5 0d 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_IDLE                   d7a5 5 0d 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_R_TRK                  d7a5 5 0d 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_R_ACQ                  d7a5 5 0d 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_ACQ_DONE               d7a5 5 0d 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_START_ACQ:AFC_VCO_PULL_IN            d7a5 5 0d 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_INACTIVE                 d7a5 5 0e 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_IDLE                     d7a5 5 0e 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_R_TRK                    d7a5 5 0e 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_R_ACQ                    d7a5 5 0e 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_ACQ_DONE                 d7a5 5 0e 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TRAFFIC:AFC_VCO_PULL_IN              d7a5 5 0e 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_INACTIVE                 d7a5 5 0f 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_IDLE                     d7a5 5 0f 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_R_TRK                    d7a5 5 0f 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_R_ACQ                    d7a5 5 0f 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_ACQ_DONE                 d7a5 5 0f 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_TC_EXIT:AFC_VCO_PULL_IN              d7a5 5 0f 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_INACTIVE                  d7a5 5 10 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_IDLE                      d7a5 5 10 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_R_TRK                     d7a5 5 10 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_R_ACQ                     d7a5 5 10 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_ACQ_DONE                  d7a5 5 10 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ACCESS:AFC_VCO_PULL_IN               d7a5 5 10 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_INACTIVE   d7a5 5 11 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_IDLE       d7a5 5 11 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_TRK      d7a5 5 11 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_R_ACQ      d7a5 5 11 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_ACQ_DONE   d7a5 5 11 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_ROT_PUSH_SAMPLE_TIMER:AFC_VCO_PULL_IN d7a5 5 11 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_INACTIVE            d7a5 5 12 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_IDLE                d7a5 5 12 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_R_TRK               d7a5 5 12 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_R_ACQ               d7a5 5 12 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_ACQ_DONE            d7a5 5 12 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_DONE:AFC_VCO_PULL_IN         d7a5 5 12 5    @olive @white

AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_INACTIVE            d7a5 5 13 0    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_IDLE                d7a5 5 13 1    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_R_TRK               d7a5 5 13 2    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_R_ACQ               d7a5 5 13 3    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_ACQ_DONE            d7a5 5 13 4    @olive @white
AFC:AFC_VCO_PULL_IN:AFC_PULL_IN_FAIL:AFC_VCO_PULL_IN         d7a5 5 13 5    @olive @white



# End machine generated TLA code for state machine: AFC_SM

