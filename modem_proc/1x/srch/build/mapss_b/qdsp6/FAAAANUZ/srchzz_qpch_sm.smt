/*=============================================================================

  srchzz_qpch_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srchzz_qpch_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SRCHZZ_QPCH_SM */

/* Transition function prototypes */
static stm_state_type qpch_process_qpch_start(void *);
static stm_state_type qpch_process_qpch_continue(void *);
static stm_state_type qpch_process_wakeup_now(void *);
static stm_state_type qpch_process_pi1_demod_done(void *);
static stm_state_type qpch_process_pi2_demod_done(void *);
static stm_state_type qpch_process_cci1_demod_done(void *);


/* State Machine entry/exit function prototypes */
static void qpch_entry(stm_group_type *group);
static void qpch_exit(stm_group_type *group);


/* State entry/exit function prototypes */


/* Total number of states and inputs */
#define SRCHZZ_QPCH_SM_NUMBER_OF_STATES 4
#define SRCHZZ_QPCH_SM_NUMBER_OF_INPUTS 4


/* State enumeration */
enum
{
  INIT_STATE,
  PI1_STATE,
  PI2_STATE,
  CCI1_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SRCHZZ_QPCH_SM_states[ SRCHZZ_QPCH_SM_NUMBER_OF_STATES ] =
{
  {"INIT_STATE", NULL, NULL},
  {"PI1_STATE", NULL, NULL},
  {"PI2_STATE", NULL, NULL},
  {"CCI1_STATE", NULL, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SRCHZZ_QPCH_SM_inputs[ SRCHZZ_QPCH_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)START_SLEEP_CMD, "START_SLEEP_CMD"},
  {(stm_input_type)QPCH_CONTINUE_CMD, "QPCH_CONTINUE_CMD"},
  {(stm_input_type)WAKEUP_NOW_CMD, "WAKEUP_NOW_CMD"},
  {(stm_input_type)QPCH_DEMOD_DONE_CMD, "QPCH_DEMOD_DONE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SRCHZZ_QPCH_SM_transitions[ SRCHZZ_QPCH_SM_NUMBER_OF_STATES *  SRCHZZ_QPCH_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state INIT_STATE */
    qpch_process_qpch_start,    /* START_SLEEP_CMD */
    qpch_process_qpch_continue,    /* QPCH_CONTINUE_CMD */
    qpch_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    NULL,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state PI1_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* QPCH_CONTINUE_CMD */
    qpch_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    qpch_process_pi1_demod_done,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state PI2_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* QPCH_CONTINUE_CMD */
    qpch_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    qpch_process_pi2_demod_done,    /* QPCH_DEMOD_DONE_CMD */

  /* Transition functions for state CCI1_STATE */
    NULL,    /* START_SLEEP_CMD */
    NULL,    /* QPCH_CONTINUE_CMD */
    qpch_process_wakeup_now,    /* WAKEUP_NOW_CMD */
    qpch_process_cci1_demod_done,    /* QPCH_DEMOD_DONE_CMD */

};


/* State machine definition */
stm_state_machine_type SRCHZZ_QPCH_SM =
{
  "SRCHZZ_QPCH_SM", /* state machine name */
  15079, /* unique SM id (hash of name) */
  qpch_entry, /* state machine entry function */
  qpch_exit, /* state machine exit function */
  INIT_STATE, /* state machine initial state */
  FALSE, /* state machine starts active? */
  SRCHZZ_QPCH_SM_NUMBER_OF_STATES,
  SRCHZZ_QPCH_SM_NUMBER_OF_INPUTS,
  SRCHZZ_QPCH_SM_states,
  SRCHZZ_QPCH_SM_inputs,
  SRCHZZ_QPCH_SM_transitions,
};

/* End machine generated code for state machine: SRCHZZ_QPCH_SM */

