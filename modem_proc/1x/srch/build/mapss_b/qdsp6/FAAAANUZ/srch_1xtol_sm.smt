/*=============================================================================

  srch_1xtol_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_1xtol_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: ONEXTOL_SM */

/* Transition function prototypes */
static stm_state_type onextol_process_start_req(void *);
static stm_state_type onextol_process_aflt_release_done(void *);
static stm_state_type onextol_process_init_cnf(void *);
static stm_state_type onextol_process_abort_req(void *);
static stm_state_type onextol_process_deinit_req(void *);
static stm_state_type onextol_process_srch_cnf(void *);
static stm_state_type onextol_process_meas_cnf(void *);
static stm_state_type onextol_process_cleanup_cnf(void *);
static stm_state_type onextol_process_abort_req_in_cleanup(void *);
static stm_state_type onextol_process_deinit_cnf(void *);
static stm_state_type onextol_process_reselection(void *);
static stm_state_type onextol_process_abort_cnf(void *);


/* State Machine entry/exit function prototypes */
static void onextol_entry(stm_group_type *group);
static void onextol_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void onextol_inactive_state_entry(void *payload, stm_state_type previous_state);
static void onextol_init_state_entry(void *payload, stm_state_type previous_state);
static void onextol_srch_state_entry(void *payload, stm_state_type previous_state);
static void onextol_meas_state_entry(void *payload, stm_state_type previous_state);
static void onextol_cleanup_state_entry(void *payload, stm_state_type previous_state);
static void onextol_deinit_state_entry(void *payload, stm_state_type previous_state);
static void onextol_abort_state_entry(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define ONEXTOL_SM_NUMBER_OF_STATES 7
#define ONEXTOL_SM_NUMBER_OF_INPUTS 11


/* State enumeration */
enum
{
  ONEXTOL_INACTIVE_STATE,
  ONEXTOL_INIT_STATE,
  ONEXTOL_SRCH_STATE,
  ONEXTOL_MEAS_STATE,
  ONEXTOL_CLEANUP_STATE,
  ONEXTOL_DEINIT_STATE,
  ONEXTOL_ABORT_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  ONEXTOL_SM_states[ ONEXTOL_SM_NUMBER_OF_STATES ] =
{
  {"ONEXTOL_INACTIVE_STATE", onextol_inactive_state_entry, NULL},
  {"ONEXTOL_INIT_STATE", onextol_init_state_entry, NULL},
  {"ONEXTOL_SRCH_STATE", onextol_srch_state_entry, NULL},
  {"ONEXTOL_MEAS_STATE", onextol_meas_state_entry, NULL},
  {"ONEXTOL_CLEANUP_STATE", onextol_cleanup_state_entry, NULL},
  {"ONEXTOL_DEINIT_STATE", onextol_deinit_state_entry, NULL},
  {"ONEXTOL_ABORT_STATE", onextol_abort_state_entry, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  ONEXTOL_SM_inputs[ ONEXTOL_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)IRAT_1XTOL_START_CMD, "IRAT_1XTOL_START_CMD"},
  {(stm_input_type)AFLT_RELEASE_DONE_CMD, "AFLT_RELEASE_DONE_CMD"},
  {(stm_input_type)IRAT_1XTOL_INIT_CNF_CMD, "IRAT_1XTOL_INIT_CNF_CMD"},
  {(stm_input_type)IRAT_1XTOL_ABORT_REQ_CMD, "IRAT_1XTOL_ABORT_REQ_CMD"},
  {(stm_input_type)IRAT_1XTOL_DEINIT_REQ_CMD, "IRAT_1XTOL_DEINIT_REQ_CMD"},
  {(stm_input_type)IRAT_1XTOL_SRCH_CNF_CMD, "IRAT_1XTOL_SRCH_CNF_CMD"},
  {(stm_input_type)IRAT_1XTOL_MEAS_CNF_CMD, "IRAT_1XTOL_MEAS_CNF_CMD"},
  {(stm_input_type)IRAT_1XTOL_CLEANUP_CNF_CMD, "IRAT_1XTOL_CLEANUP_CNF_CMD"},
  {(stm_input_type)IRAT_1XTOL_DEINIT_CNF_CMD, "IRAT_1XTOL_DEINIT_CNF_CMD"},
  {(stm_input_type)IRAT_1XTOL_RESEL_CHECK_CMD, "IRAT_1XTOL_RESEL_CHECK_CMD"},
  {(stm_input_type)IRAT_1XTOL_ABORT_CNF_CMD, "IRAT_1XTOL_ABORT_CNF_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  ONEXTOL_SM_transitions[ ONEXTOL_SM_NUMBER_OF_STATES *  ONEXTOL_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state ONEXTOL_INACTIVE_STATE */
    onextol_process_start_req,    /* IRAT_1XTOL_START_CMD */
    onextol_process_aflt_release_done,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_INIT_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    onextol_process_init_cnf,    /* IRAT_1XTOL_INIT_CNF_CMD */
    onextol_process_abort_req,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_SRCH_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    onextol_process_abort_req,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    onextol_process_deinit_req,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    onextol_process_srch_cnf,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_MEAS_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    onextol_process_abort_req,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    onextol_process_meas_cnf,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_CLEANUP_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    onextol_process_abort_req_in_cleanup,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    onextol_process_cleanup_cnf,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_DEINIT_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    onextol_process_deinit_cnf,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    onextol_process_reselection,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_CNF_CMD */

  /* Transition functions for state ONEXTOL_ABORT_STATE */
    NULL,    /* IRAT_1XTOL_START_CMD */
    NULL,    /* AFLT_RELEASE_DONE_CMD */
    NULL,    /* IRAT_1XTOL_INIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_ABORT_REQ_CMD */
    onextol_process_deinit_req,    /* IRAT_1XTOL_DEINIT_REQ_CMD */
    NULL,    /* IRAT_1XTOL_SRCH_CNF_CMD */
    NULL,    /* IRAT_1XTOL_MEAS_CNF_CMD */
    NULL,    /* IRAT_1XTOL_CLEANUP_CNF_CMD */
    NULL,    /* IRAT_1XTOL_DEINIT_CNF_CMD */
    NULL,    /* IRAT_1XTOL_RESEL_CHECK_CMD */
    onextol_process_abort_cnf,    /* IRAT_1XTOL_ABORT_CNF_CMD */

};


/* State machine definition */
stm_state_machine_type ONEXTOL_SM =
{
  "ONEXTOL_SM", /* state machine name */
  34644, /* unique SM id (hash of name) */
  onextol_entry, /* state machine entry function */
  onextol_exit, /* state machine exit function */
  ONEXTOL_INACTIVE_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  ONEXTOL_SM_NUMBER_OF_STATES,
  ONEXTOL_SM_NUMBER_OF_INPUTS,
  ONEXTOL_SM_states,
  ONEXTOL_SM_inputs,
  ONEXTOL_SM_transitions,
};

/* End machine generated code for state machine: ONEXTOL_SM */

