/*=============================================================================

  srch_sys_meas_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_sys_meas_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: SYS_MEAS_SM */

/* Transition function prototypes */
static stm_state_type sys_meas_process_meas_cmd(void *);
static stm_state_type sys_meas_process_freq_track_off_done(void *);
static stm_state_type sys_meas_process_unexpected_mdsp_reg(void *);
static stm_state_type sys_meas_process_rf_release_done(void *);
static stm_state_type sys_meas_process_rf_locked(void *);
static stm_state_type sys_meas_process_abort(void *);
static stm_state_type sys_meas_process_srch4_mdsp_reg(void *);
static stm_state_type sys_meas_process_tune_done(void *);
static stm_state_type sys_meas_process_agc_timer(void *);
static stm_state_type sys_meas_process_detect(void *);
static stm_state_type sys_meas_process_dwell(void *);
static stm_state_type sys_meas_process_meas_next(void *);
static stm_state_type sys_meas_process_meas_done(void *);


/* State Machine entry/exit function prototypes */
static void sys_meas_entry(stm_group_type *group);
static void sys_meas_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void sys_meas_enter_standby(void *payload, stm_state_type previous_state);
static void sys_meas_exit_standby(void *payload, stm_state_type previous_state);
static void sys_meas_enter_rf_wait(void *payload, stm_state_type previous_state);
static void sys_meas_enter_mdsp_wait(void *payload, stm_state_type previous_state);
static void sys_meas_enter_tune(void *payload, stm_state_type previous_state);
static void sys_meas_enter_agc_wait(void *payload, stm_state_type previous_state);
static void sys_meas_exit_agc_wait(void *payload, stm_state_type previous_state);
static void sys_meas_enter_detect(void *payload, stm_state_type previous_state);
static void sys_meas_enter_dwell(void *payload, stm_state_type previous_state);
static void sys_meas_enter_done(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define SYS_MEAS_SM_NUMBER_OF_STATES 8
#define SYS_MEAS_SM_NUMBER_OF_INPUTS 12


/* State enumeration */
enum
{
  STANDBY,
  RF_WAIT,
  MDSP_WAIT,
  TUNE_STATE,
  AGC_WAIT,
  DETECT,
  DWELL,
  DONE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  SYS_MEAS_SM_states[ SYS_MEAS_SM_NUMBER_OF_STATES ] =
{
  {"STANDBY", sys_meas_enter_standby, sys_meas_exit_standby},
  {"RF_WAIT", sys_meas_enter_rf_wait, NULL},
  {"MDSP_WAIT", sys_meas_enter_mdsp_wait, NULL},
  {"TUNE_STATE", sys_meas_enter_tune, NULL},
  {"AGC_WAIT", sys_meas_enter_agc_wait, sys_meas_exit_agc_wait},
  {"DETECT", sys_meas_enter_detect, NULL},
  {"DWELL", sys_meas_enter_dwell, NULL},
  {"DONE", sys_meas_enter_done, NULL},
};


/* Input value, name table */
static const stm_input_array_type
  SYS_MEAS_SM_inputs[ SYS_MEAS_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)SYS_MEAS_CMD, "SYS_MEAS_CMD"},
  {(stm_input_type)FREQ_TRACK_OFF_DONE_CMD, "FREQ_TRACK_OFF_DONE_CMD"},
  {(stm_input_type)SRCH4_MDSP_REG_CMD, "SRCH4_MDSP_REG_CMD"},
  {(stm_input_type)RF_RELEASE_DONE_CMD, "RF_RELEASE_DONE_CMD"},
  {(stm_input_type)RF_LOCKED_CMD, "RF_LOCKED_CMD"},
  {(stm_input_type)ABORT_CMD, "ABORT_CMD"},
  {(stm_input_type)TUNE_DONE_CMD, "TUNE_DONE_CMD"},
  {(stm_input_type)AGC_TIMER_CMD, "AGC_TIMER_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)MEAS_NEXT_CMD, "MEAS_NEXT_CMD"},
  {(stm_input_type)MEAS_DONE_CMD, "MEAS_DONE_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  SYS_MEAS_SM_transitions[ SYS_MEAS_SM_NUMBER_OF_STATES *  SYS_MEAS_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state STANDBY */
    sys_meas_process_meas_cmd,    /* SYS_MEAS_CMD */
    sys_meas_process_freq_track_off_done,    /* FREQ_TRACK_OFF_DONE_CMD */
    sys_meas_process_unexpected_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    sys_meas_process_rf_release_done,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    NULL,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state RF_WAIT */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    sys_meas_process_unexpected_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    sys_meas_process_rf_locked,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state MDSP_WAIT */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    sys_meas_process_srch4_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state TUNE_STATE */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    sys_meas_process_unexpected_mdsp_reg,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    sys_meas_process_tune_done,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state AGC_WAIT */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    sys_meas_process_agc_timer,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state DETECT */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    sys_meas_process_detect,    /* SRCH_DUMP_CMD */
    sys_meas_process_abort,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state DWELL */
    sys_meas_process_abort,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_abort,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    sys_meas_process_dwell,    /* SRCH_DUMP_CMD */
    sys_meas_process_abort,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* MEAS_NEXT_CMD */
    NULL,    /* MEAS_DONE_CMD */

  /* Transition functions for state DONE */
    sys_meas_process_meas_done,    /* SYS_MEAS_CMD */
    NULL,    /* FREQ_TRACK_OFF_DONE_CMD */
    NULL,    /* SRCH4_MDSP_REG_CMD */
    NULL,    /* RF_RELEASE_DONE_CMD */
    NULL,    /* RF_LOCKED_CMD */
    sys_meas_process_meas_done,    /* ABORT_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* AGC_TIMER_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    sys_meas_process_meas_next,    /* MEAS_NEXT_CMD */
    sys_meas_process_meas_done,    /* MEAS_DONE_CMD */

};


/* State machine definition */
stm_state_machine_type SYS_MEAS_SM =
{
  "SYS_MEAS_SM", /* state machine name */
  37795, /* unique SM id (hash of name) */
  sys_meas_entry, /* state machine entry function */
  sys_meas_exit, /* state machine exit function */
  STANDBY, /* state machine initial state */
  TRUE, /* state machine starts active? */
  SYS_MEAS_SM_NUMBER_OF_STATES,
  SYS_MEAS_SM_NUMBER_OF_INPUTS,
  SYS_MEAS_SM_states,
  SYS_MEAS_SM_inputs,
  SYS_MEAS_SM_transitions,
};

/* End machine generated code for state machine: SYS_MEAS_SM */

