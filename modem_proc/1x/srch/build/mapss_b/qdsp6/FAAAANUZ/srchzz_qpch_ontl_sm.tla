###############################################################################
#
#    srchzz_qpch_ontl_sm.tla
#
# Description:
#   This file contains the machine generated state machine TLA info from the
#   file:  srchzz_qpch_ontl_sm.smf
#
#
###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################


# Begin machine generated TLA code for state machine: SRCHZZ_QPCH_ONTL_SM
# State machine:current state:input:next state                      fgcolor bgcolor

SRCHZZ_QPCH_ONTL:INIT:START_SLEEP:INIT                       e9d4 0 00 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:START_SLEEP:DOZE                       e9d4 0 00 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:START_SLEEP:SLEEP                      e9d4 0 00 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:START_SLEEP:WAKEUP                     e9d4 0 00 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:START_SLEEP:DEMOD                      e9d4 0 00 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:ROLL:INIT                              e9d4 0 01 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ROLL:DOZE                              e9d4 0 01 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ROLL:SLEEP                             e9d4 0 01 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ROLL:WAKEUP                            e9d4 0 01 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ROLL:DEMOD                             e9d4 0 01 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:ABORT:INIT                             e9d4 0 02 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ABORT:DOZE                             e9d4 0 02 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ABORT:SLEEP                            e9d4 0 02 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ABORT:WAKEUP                           e9d4 0 02 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ABORT:DEMOD                            e9d4 0 02 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:DOZE:INIT                              e9d4 0 03 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:DOZE:DOZE                              e9d4 0 03 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:DOZE:SLEEP                             e9d4 0 03 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:DOZE:WAKEUP                            e9d4 0 03 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:DOZE:DEMOD                             e9d4 0 03 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:WAKEUP_NOW:INIT                        e9d4 0 04 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP_NOW:DOZE                        e9d4 0 04 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP_NOW:SLEEP                       e9d4 0 04 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP_NOW:WAKEUP                      e9d4 0 04 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP_NOW:DEMOD                       e9d4 0 04 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:FREQ_TRACK_OFF_DONE:INIT               e9d4 0 05 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:FREQ_TRACK_OFF_DONE:DOZE               e9d4 0 05 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:FREQ_TRACK_OFF_DONE:SLEEP              e9d4 0 05 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:FREQ_TRACK_OFF_DONE:WAKEUP             e9d4 0 05 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:FREQ_TRACK_OFF_DONE:DEMOD              e9d4 0 05 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLED:INIT                    e9d4 0 06 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLED:DOZE                    e9d4 0 06 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLED:SLEEP                   e9d4 0 06 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLED:WAKEUP                  e9d4 0 06 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLED:DEMOD                   e9d4 0 06 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:GO_TO_SLEEP:INIT                       e9d4 0 07 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_SLEEP:DOZE                       e9d4 0 07 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_SLEEP:SLEEP                      e9d4 0 07 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_SLEEP:WAKEUP                     e9d4 0 07 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_SLEEP:DEMOD                      e9d4 0 07 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLE_COMP:INIT                e9d4 0 08 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLE_COMP:DOZE                e9d4 0 08 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLE_COMP:SLEEP               e9d4 0 08 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLE_COMP:WAKEUP              e9d4 0 08 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_DISABLE_COMP:DEMOD               e9d4 0 08 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:ADJUST_TIMING:INIT                     e9d4 0 09 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ADJUST_TIMING:DOZE                     e9d4 0 09 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ADJUST_TIMING:SLEEP                    e9d4 0 09 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ADJUST_TIMING:WAKEUP                   e9d4 0 09 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:ADJUST_TIMING:DEMOD                    e9d4 0 09 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:GO_TO_DOZE:INIT                        e9d4 0 0a 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_DOZE:DOZE                        e9d4 0 0a 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_DOZE:SLEEP                       e9d4 0 0a 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_DOZE:WAKEUP                      e9d4 0 0a 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:GO_TO_DOZE:DEMOD                       e9d4 0 0a 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:WAKEUP:INIT                            e9d4 0 0b 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP:DOZE                            e9d4 0 0b 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP:SLEEP                           e9d4 0 0b 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP:WAKEUP                          e9d4 0 0b 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:WAKEUP:DEMOD                           e9d4 0 0b 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_CH_GRANTED:INIT                     e9d4 0 0c 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_GRANTED:DOZE                     e9d4 0 0c 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_GRANTED:SLEEP                    e9d4 0 0c 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_GRANTED:WAKEUP                   e9d4 0 0c 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_GRANTED:DEMOD                    e9d4 0 0c 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_CH_DENIED:INIT                      e9d4 0 0d 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_DENIED:DOZE                      e9d4 0 0d 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_DENIED:SLEEP                     e9d4 0 0d 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_DENIED:WAKEUP                    e9d4 0 0d 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_CH_DENIED:DEMOD                     e9d4 0 0d 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_RF_RSP_TUNE_COMP:INIT               e9d4 0 0e 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_RSP_TUNE_COMP:DOZE               e9d4 0 0e 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_RSP_TUNE_COMP:SLEEP              e9d4 0 0e 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_RSP_TUNE_COMP:WAKEUP             e9d4 0 0e 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_RF_RSP_TUNE_COMP:DEMOD              e9d4 0 0e 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:NO_RF_LOCK:INIT                        e9d4 0 0f 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:NO_RF_LOCK:DOZE                        e9d4 0 0f 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:NO_RF_LOCK:SLEEP                       e9d4 0 0f 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:NO_RF_LOCK:WAKEUP                      e9d4 0 0f 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:NO_RF_LOCK:DEMOD                       e9d4 0 0f 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:CX8_ON:INIT                            e9d4 0 10 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:CX8_ON:DOZE                            e9d4 0 10 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:CX8_ON:SLEEP                           e9d4 0 10 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:CX8_ON:WAKEUP                          e9d4 0 10 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:CX8_ON:DEMOD                           e9d4 0 10 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:RX_TUNE_COMP:INIT                      e9d4 0 11 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_TUNE_COMP:DOZE                      e9d4 0 11 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_TUNE_COMP:SLEEP                     e9d4 0 11 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_TUNE_COMP:WAKEUP                    e9d4 0 11 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:RX_TUNE_COMP:DEMOD                     e9d4 0 11 4    @olive @white

SRCHZZ_QPCH_ONTL:INIT:QPCH_DEMOD_DONE:INIT                   e9d4 0 12 0    @olive @white
SRCHZZ_QPCH_ONTL:INIT:QPCH_DEMOD_DONE:DOZE                   e9d4 0 12 1    @olive @white
SRCHZZ_QPCH_ONTL:INIT:QPCH_DEMOD_DONE:SLEEP                  e9d4 0 12 2    @olive @white
SRCHZZ_QPCH_ONTL:INIT:QPCH_DEMOD_DONE:WAKEUP                 e9d4 0 12 3    @olive @white
SRCHZZ_QPCH_ONTL:INIT:QPCH_DEMOD_DONE:DEMOD                  e9d4 0 12 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:START_SLEEP:INIT                       e9d4 1 00 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:START_SLEEP:DOZE                       e9d4 1 00 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:START_SLEEP:SLEEP                      e9d4 1 00 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:START_SLEEP:WAKEUP                     e9d4 1 00 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:START_SLEEP:DEMOD                      e9d4 1 00 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:ROLL:INIT                              e9d4 1 01 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ROLL:DOZE                              e9d4 1 01 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ROLL:SLEEP                             e9d4 1 01 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ROLL:WAKEUP                            e9d4 1 01 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ROLL:DEMOD                             e9d4 1 01 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:ABORT:INIT                             e9d4 1 02 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ABORT:DOZE                             e9d4 1 02 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ABORT:SLEEP                            e9d4 1 02 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ABORT:WAKEUP                           e9d4 1 02 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ABORT:DEMOD                            e9d4 1 02 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:DOZE:INIT                              e9d4 1 03 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:DOZE:DOZE                              e9d4 1 03 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:DOZE:SLEEP                             e9d4 1 03 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:DOZE:WAKEUP                            e9d4 1 03 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:DOZE:DEMOD                             e9d4 1 03 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:WAKEUP_NOW:INIT                        e9d4 1 04 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP_NOW:DOZE                        e9d4 1 04 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP_NOW:SLEEP                       e9d4 1 04 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP_NOW:WAKEUP                      e9d4 1 04 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP_NOW:DEMOD                       e9d4 1 04 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:FREQ_TRACK_OFF_DONE:INIT               e9d4 1 05 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:FREQ_TRACK_OFF_DONE:DOZE               e9d4 1 05 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:FREQ_TRACK_OFF_DONE:SLEEP              e9d4 1 05 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:FREQ_TRACK_OFF_DONE:WAKEUP             e9d4 1 05 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:FREQ_TRACK_OFF_DONE:DEMOD              e9d4 1 05 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLED:INIT                    e9d4 1 06 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLED:DOZE                    e9d4 1 06 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLED:SLEEP                   e9d4 1 06 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLED:WAKEUP                  e9d4 1 06 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLED:DEMOD                   e9d4 1 06 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:GO_TO_SLEEP:INIT                       e9d4 1 07 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_SLEEP:DOZE                       e9d4 1 07 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_SLEEP:SLEEP                      e9d4 1 07 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_SLEEP:WAKEUP                     e9d4 1 07 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_SLEEP:DEMOD                      e9d4 1 07 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLE_COMP:INIT                e9d4 1 08 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLE_COMP:DOZE                e9d4 1 08 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLE_COMP:SLEEP               e9d4 1 08 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLE_COMP:WAKEUP              e9d4 1 08 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_DISABLE_COMP:DEMOD               e9d4 1 08 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:ADJUST_TIMING:INIT                     e9d4 1 09 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ADJUST_TIMING:DOZE                     e9d4 1 09 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ADJUST_TIMING:SLEEP                    e9d4 1 09 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ADJUST_TIMING:WAKEUP                   e9d4 1 09 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:ADJUST_TIMING:DEMOD                    e9d4 1 09 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:GO_TO_DOZE:INIT                        e9d4 1 0a 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_DOZE:DOZE                        e9d4 1 0a 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_DOZE:SLEEP                       e9d4 1 0a 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_DOZE:WAKEUP                      e9d4 1 0a 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:GO_TO_DOZE:DEMOD                       e9d4 1 0a 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:WAKEUP:INIT                            e9d4 1 0b 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP:DOZE                            e9d4 1 0b 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP:SLEEP                           e9d4 1 0b 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP:WAKEUP                          e9d4 1 0b 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:WAKEUP:DEMOD                           e9d4 1 0b 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_CH_GRANTED:INIT                     e9d4 1 0c 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_GRANTED:DOZE                     e9d4 1 0c 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_GRANTED:SLEEP                    e9d4 1 0c 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_GRANTED:WAKEUP                   e9d4 1 0c 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_GRANTED:DEMOD                    e9d4 1 0c 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_CH_DENIED:INIT                      e9d4 1 0d 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_DENIED:DOZE                      e9d4 1 0d 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_DENIED:SLEEP                     e9d4 1 0d 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_DENIED:WAKEUP                    e9d4 1 0d 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_CH_DENIED:DEMOD                     e9d4 1 0d 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_RF_RSP_TUNE_COMP:INIT               e9d4 1 0e 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_RSP_TUNE_COMP:DOZE               e9d4 1 0e 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_RSP_TUNE_COMP:SLEEP              e9d4 1 0e 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_RSP_TUNE_COMP:WAKEUP             e9d4 1 0e 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_RF_RSP_TUNE_COMP:DEMOD              e9d4 1 0e 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:NO_RF_LOCK:INIT                        e9d4 1 0f 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:NO_RF_LOCK:DOZE                        e9d4 1 0f 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:NO_RF_LOCK:SLEEP                       e9d4 1 0f 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:NO_RF_LOCK:WAKEUP                      e9d4 1 0f 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:NO_RF_LOCK:DEMOD                       e9d4 1 0f 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:CX8_ON:INIT                            e9d4 1 10 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:CX8_ON:DOZE                            e9d4 1 10 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:CX8_ON:SLEEP                           e9d4 1 10 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:CX8_ON:WAKEUP                          e9d4 1 10 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:CX8_ON:DEMOD                           e9d4 1 10 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:RX_TUNE_COMP:INIT                      e9d4 1 11 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_TUNE_COMP:DOZE                      e9d4 1 11 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_TUNE_COMP:SLEEP                     e9d4 1 11 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_TUNE_COMP:WAKEUP                    e9d4 1 11 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:RX_TUNE_COMP:DEMOD                     e9d4 1 11 4    @olive @white

SRCHZZ_QPCH_ONTL:DOZE:QPCH_DEMOD_DONE:INIT                   e9d4 1 12 0    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:QPCH_DEMOD_DONE:DOZE                   e9d4 1 12 1    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:QPCH_DEMOD_DONE:SLEEP                  e9d4 1 12 2    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:QPCH_DEMOD_DONE:WAKEUP                 e9d4 1 12 3    @olive @white
SRCHZZ_QPCH_ONTL:DOZE:QPCH_DEMOD_DONE:DEMOD                  e9d4 1 12 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:START_SLEEP:INIT                      e9d4 2 00 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:START_SLEEP:DOZE                      e9d4 2 00 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:START_SLEEP:SLEEP                     e9d4 2 00 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:START_SLEEP:WAKEUP                    e9d4 2 00 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:START_SLEEP:DEMOD                     e9d4 2 00 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:ROLL:INIT                             e9d4 2 01 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ROLL:DOZE                             e9d4 2 01 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ROLL:SLEEP                            e9d4 2 01 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ROLL:WAKEUP                           e9d4 2 01 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ROLL:DEMOD                            e9d4 2 01 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:ABORT:INIT                            e9d4 2 02 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ABORT:DOZE                            e9d4 2 02 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ABORT:SLEEP                           e9d4 2 02 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ABORT:WAKEUP                          e9d4 2 02 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ABORT:DEMOD                           e9d4 2 02 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:DOZE:INIT                             e9d4 2 03 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:DOZE:DOZE                             e9d4 2 03 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:DOZE:SLEEP                            e9d4 2 03 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:DOZE:WAKEUP                           e9d4 2 03 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:DOZE:DEMOD                            e9d4 2 03 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP_NOW:INIT                       e9d4 2 04 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP_NOW:DOZE                       e9d4 2 04 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP_NOW:SLEEP                      e9d4 2 04 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP_NOW:WAKEUP                     e9d4 2 04 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP_NOW:DEMOD                      e9d4 2 04 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:FREQ_TRACK_OFF_DONE:INIT              e9d4 2 05 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:FREQ_TRACK_OFF_DONE:DOZE              e9d4 2 05 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:FREQ_TRACK_OFF_DONE:SLEEP             e9d4 2 05 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:FREQ_TRACK_OFF_DONE:WAKEUP            e9d4 2 05 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:FREQ_TRACK_OFF_DONE:DEMOD             e9d4 2 05 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLED:INIT                   e9d4 2 06 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLED:DOZE                   e9d4 2 06 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLED:SLEEP                  e9d4 2 06 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLED:WAKEUP                 e9d4 2 06 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLED:DEMOD                  e9d4 2 06 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_SLEEP:INIT                      e9d4 2 07 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_SLEEP:DOZE                      e9d4 2 07 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_SLEEP:SLEEP                     e9d4 2 07 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_SLEEP:WAKEUP                    e9d4 2 07 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_SLEEP:DEMOD                     e9d4 2 07 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLE_COMP:INIT               e9d4 2 08 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLE_COMP:DOZE               e9d4 2 08 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLE_COMP:SLEEP              e9d4 2 08 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLE_COMP:WAKEUP             e9d4 2 08 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_DISABLE_COMP:DEMOD              e9d4 2 08 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:ADJUST_TIMING:INIT                    e9d4 2 09 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ADJUST_TIMING:DOZE                    e9d4 2 09 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ADJUST_TIMING:SLEEP                   e9d4 2 09 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ADJUST_TIMING:WAKEUP                  e9d4 2 09 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:ADJUST_TIMING:DEMOD                   e9d4 2 09 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_DOZE:INIT                       e9d4 2 0a 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_DOZE:DOZE                       e9d4 2 0a 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_DOZE:SLEEP                      e9d4 2 0a 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_DOZE:WAKEUP                     e9d4 2 0a 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:GO_TO_DOZE:DEMOD                      e9d4 2 0a 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP:INIT                           e9d4 2 0b 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP:DOZE                           e9d4 2 0b 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP:SLEEP                          e9d4 2 0b 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP:WAKEUP                         e9d4 2 0b 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:WAKEUP:DEMOD                          e9d4 2 0b 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_GRANTED:INIT                    e9d4 2 0c 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_GRANTED:DOZE                    e9d4 2 0c 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_GRANTED:SLEEP                   e9d4 2 0c 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_GRANTED:WAKEUP                  e9d4 2 0c 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_GRANTED:DEMOD                   e9d4 2 0c 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_DENIED:INIT                     e9d4 2 0d 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_DENIED:DOZE                     e9d4 2 0d 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_DENIED:SLEEP                    e9d4 2 0d 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_DENIED:WAKEUP                   e9d4 2 0d 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_CH_DENIED:DEMOD                    e9d4 2 0d 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_RSP_TUNE_COMP:INIT              e9d4 2 0e 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_RSP_TUNE_COMP:DOZE              e9d4 2 0e 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_RSP_TUNE_COMP:SLEEP             e9d4 2 0e 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_RSP_TUNE_COMP:WAKEUP            e9d4 2 0e 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_RF_RSP_TUNE_COMP:DEMOD             e9d4 2 0e 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:NO_RF_LOCK:INIT                       e9d4 2 0f 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:NO_RF_LOCK:DOZE                       e9d4 2 0f 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:NO_RF_LOCK:SLEEP                      e9d4 2 0f 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:NO_RF_LOCK:WAKEUP                     e9d4 2 0f 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:NO_RF_LOCK:DEMOD                      e9d4 2 0f 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:CX8_ON:INIT                           e9d4 2 10 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:CX8_ON:DOZE                           e9d4 2 10 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:CX8_ON:SLEEP                          e9d4 2 10 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:CX8_ON:WAKEUP                         e9d4 2 10 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:CX8_ON:DEMOD                          e9d4 2 10 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:RX_TUNE_COMP:INIT                     e9d4 2 11 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_TUNE_COMP:DOZE                     e9d4 2 11 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_TUNE_COMP:SLEEP                    e9d4 2 11 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_TUNE_COMP:WAKEUP                   e9d4 2 11 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:RX_TUNE_COMP:DEMOD                    e9d4 2 11 4    @olive @white

SRCHZZ_QPCH_ONTL:SLEEP:QPCH_DEMOD_DONE:INIT                  e9d4 2 12 0    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:QPCH_DEMOD_DONE:DOZE                  e9d4 2 12 1    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:QPCH_DEMOD_DONE:SLEEP                 e9d4 2 12 2    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:QPCH_DEMOD_DONE:WAKEUP                e9d4 2 12 3    @olive @white
SRCHZZ_QPCH_ONTL:SLEEP:QPCH_DEMOD_DONE:DEMOD                 e9d4 2 12 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:START_SLEEP:INIT                     e9d4 3 00 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:START_SLEEP:DOZE                     e9d4 3 00 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:START_SLEEP:SLEEP                    e9d4 3 00 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:START_SLEEP:WAKEUP                   e9d4 3 00 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:START_SLEEP:DEMOD                    e9d4 3 00 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:ROLL:INIT                            e9d4 3 01 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ROLL:DOZE                            e9d4 3 01 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ROLL:SLEEP                           e9d4 3 01 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ROLL:WAKEUP                          e9d4 3 01 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ROLL:DEMOD                           e9d4 3 01 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:ABORT:INIT                           e9d4 3 02 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ABORT:DOZE                           e9d4 3 02 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ABORT:SLEEP                          e9d4 3 02 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ABORT:WAKEUP                         e9d4 3 02 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ABORT:DEMOD                          e9d4 3 02 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:DOZE:INIT                            e9d4 3 03 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:DOZE:DOZE                            e9d4 3 03 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:DOZE:SLEEP                           e9d4 3 03 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:DOZE:WAKEUP                          e9d4 3 03 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:DOZE:DEMOD                           e9d4 3 03 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP_NOW:INIT                      e9d4 3 04 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP_NOW:DOZE                      e9d4 3 04 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP_NOW:SLEEP                     e9d4 3 04 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP_NOW:WAKEUP                    e9d4 3 04 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP_NOW:DEMOD                     e9d4 3 04 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:FREQ_TRACK_OFF_DONE:INIT             e9d4 3 05 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:FREQ_TRACK_OFF_DONE:DOZE             e9d4 3 05 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:FREQ_TRACK_OFF_DONE:SLEEP            e9d4 3 05 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:FREQ_TRACK_OFF_DONE:WAKEUP           e9d4 3 05 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:FREQ_TRACK_OFF_DONE:DEMOD            e9d4 3 05 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLED:INIT                  e9d4 3 06 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLED:DOZE                  e9d4 3 06 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLED:SLEEP                 e9d4 3 06 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLED:WAKEUP                e9d4 3 06 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLED:DEMOD                 e9d4 3 06 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_SLEEP:INIT                     e9d4 3 07 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_SLEEP:DOZE                     e9d4 3 07 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_SLEEP:SLEEP                    e9d4 3 07 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_SLEEP:WAKEUP                   e9d4 3 07 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_SLEEP:DEMOD                    e9d4 3 07 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLE_COMP:INIT              e9d4 3 08 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLE_COMP:DOZE              e9d4 3 08 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLE_COMP:SLEEP             e9d4 3 08 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLE_COMP:WAKEUP            e9d4 3 08 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_DISABLE_COMP:DEMOD             e9d4 3 08 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:ADJUST_TIMING:INIT                   e9d4 3 09 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ADJUST_TIMING:DOZE                   e9d4 3 09 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ADJUST_TIMING:SLEEP                  e9d4 3 09 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ADJUST_TIMING:WAKEUP                 e9d4 3 09 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:ADJUST_TIMING:DEMOD                  e9d4 3 09 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_DOZE:INIT                      e9d4 3 0a 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_DOZE:DOZE                      e9d4 3 0a 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_DOZE:SLEEP                     e9d4 3 0a 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_DOZE:WAKEUP                    e9d4 3 0a 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:GO_TO_DOZE:DEMOD                     e9d4 3 0a 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP:INIT                          e9d4 3 0b 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP:DOZE                          e9d4 3 0b 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP:SLEEP                         e9d4 3 0b 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP:WAKEUP                        e9d4 3 0b 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:WAKEUP:DEMOD                         e9d4 3 0b 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_GRANTED:INIT                   e9d4 3 0c 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_GRANTED:DOZE                   e9d4 3 0c 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_GRANTED:SLEEP                  e9d4 3 0c 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_GRANTED:WAKEUP                 e9d4 3 0c 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_GRANTED:DEMOD                  e9d4 3 0c 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_DENIED:INIT                    e9d4 3 0d 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_DENIED:DOZE                    e9d4 3 0d 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_DENIED:SLEEP                   e9d4 3 0d 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_DENIED:WAKEUP                  e9d4 3 0d 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_CH_DENIED:DEMOD                   e9d4 3 0d 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_RSP_TUNE_COMP:INIT             e9d4 3 0e 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_RSP_TUNE_COMP:DOZE             e9d4 3 0e 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_RSP_TUNE_COMP:SLEEP            e9d4 3 0e 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_RSP_TUNE_COMP:WAKEUP           e9d4 3 0e 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_RF_RSP_TUNE_COMP:DEMOD            e9d4 3 0e 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:NO_RF_LOCK:INIT                      e9d4 3 0f 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:NO_RF_LOCK:DOZE                      e9d4 3 0f 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:NO_RF_LOCK:SLEEP                     e9d4 3 0f 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:NO_RF_LOCK:WAKEUP                    e9d4 3 0f 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:NO_RF_LOCK:DEMOD                     e9d4 3 0f 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:CX8_ON:INIT                          e9d4 3 10 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:CX8_ON:DOZE                          e9d4 3 10 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:CX8_ON:SLEEP                         e9d4 3 10 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:CX8_ON:WAKEUP                        e9d4 3 10 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:CX8_ON:DEMOD                         e9d4 3 10 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:RX_TUNE_COMP:INIT                    e9d4 3 11 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_TUNE_COMP:DOZE                    e9d4 3 11 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_TUNE_COMP:SLEEP                   e9d4 3 11 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_TUNE_COMP:WAKEUP                  e9d4 3 11 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:RX_TUNE_COMP:DEMOD                   e9d4 3 11 4    @olive @white

SRCHZZ_QPCH_ONTL:WAKEUP:QPCH_DEMOD_DONE:INIT                 e9d4 3 12 0    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:QPCH_DEMOD_DONE:DOZE                 e9d4 3 12 1    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:QPCH_DEMOD_DONE:SLEEP                e9d4 3 12 2    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:QPCH_DEMOD_DONE:WAKEUP               e9d4 3 12 3    @olive @white
SRCHZZ_QPCH_ONTL:WAKEUP:QPCH_DEMOD_DONE:DEMOD                e9d4 3 12 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:START_SLEEP:INIT                      e9d4 4 00 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:START_SLEEP:DOZE                      e9d4 4 00 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:START_SLEEP:SLEEP                     e9d4 4 00 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:START_SLEEP:WAKEUP                    e9d4 4 00 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:START_SLEEP:DEMOD                     e9d4 4 00 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:ROLL:INIT                             e9d4 4 01 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ROLL:DOZE                             e9d4 4 01 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ROLL:SLEEP                            e9d4 4 01 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ROLL:WAKEUP                           e9d4 4 01 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ROLL:DEMOD                            e9d4 4 01 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:ABORT:INIT                            e9d4 4 02 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ABORT:DOZE                            e9d4 4 02 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ABORT:SLEEP                           e9d4 4 02 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ABORT:WAKEUP                          e9d4 4 02 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ABORT:DEMOD                           e9d4 4 02 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:DOZE:INIT                             e9d4 4 03 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:DOZE:DOZE                             e9d4 4 03 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:DOZE:SLEEP                            e9d4 4 03 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:DOZE:WAKEUP                           e9d4 4 03 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:DOZE:DEMOD                            e9d4 4 03 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP_NOW:INIT                       e9d4 4 04 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP_NOW:DOZE                       e9d4 4 04 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP_NOW:SLEEP                      e9d4 4 04 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP_NOW:WAKEUP                     e9d4 4 04 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP_NOW:DEMOD                      e9d4 4 04 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:FREQ_TRACK_OFF_DONE:INIT              e9d4 4 05 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:FREQ_TRACK_OFF_DONE:DOZE              e9d4 4 05 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:FREQ_TRACK_OFF_DONE:SLEEP             e9d4 4 05 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:FREQ_TRACK_OFF_DONE:WAKEUP            e9d4 4 05 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:FREQ_TRACK_OFF_DONE:DEMOD             e9d4 4 05 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLED:INIT                   e9d4 4 06 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLED:DOZE                   e9d4 4 06 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLED:SLEEP                  e9d4 4 06 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLED:WAKEUP                 e9d4 4 06 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLED:DEMOD                  e9d4 4 06 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_SLEEP:INIT                      e9d4 4 07 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_SLEEP:DOZE                      e9d4 4 07 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_SLEEP:SLEEP                     e9d4 4 07 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_SLEEP:WAKEUP                    e9d4 4 07 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_SLEEP:DEMOD                     e9d4 4 07 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLE_COMP:INIT               e9d4 4 08 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLE_COMP:DOZE               e9d4 4 08 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLE_COMP:SLEEP              e9d4 4 08 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLE_COMP:WAKEUP             e9d4 4 08 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_DISABLE_COMP:DEMOD              e9d4 4 08 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:ADJUST_TIMING:INIT                    e9d4 4 09 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ADJUST_TIMING:DOZE                    e9d4 4 09 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ADJUST_TIMING:SLEEP                   e9d4 4 09 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ADJUST_TIMING:WAKEUP                  e9d4 4 09 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:ADJUST_TIMING:DEMOD                   e9d4 4 09 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_DOZE:INIT                       e9d4 4 0a 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_DOZE:DOZE                       e9d4 4 0a 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_DOZE:SLEEP                      e9d4 4 0a 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_DOZE:WAKEUP                     e9d4 4 0a 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:GO_TO_DOZE:DEMOD                      e9d4 4 0a 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP:INIT                           e9d4 4 0b 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP:DOZE                           e9d4 4 0b 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP:SLEEP                          e9d4 4 0b 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP:WAKEUP                         e9d4 4 0b 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:WAKEUP:DEMOD                          e9d4 4 0b 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_GRANTED:INIT                    e9d4 4 0c 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_GRANTED:DOZE                    e9d4 4 0c 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_GRANTED:SLEEP                   e9d4 4 0c 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_GRANTED:WAKEUP                  e9d4 4 0c 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_GRANTED:DEMOD                   e9d4 4 0c 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_DENIED:INIT                     e9d4 4 0d 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_DENIED:DOZE                     e9d4 4 0d 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_DENIED:SLEEP                    e9d4 4 0d 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_DENIED:WAKEUP                   e9d4 4 0d 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_CH_DENIED:DEMOD                    e9d4 4 0d 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_RSP_TUNE_COMP:INIT              e9d4 4 0e 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_RSP_TUNE_COMP:DOZE              e9d4 4 0e 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_RSP_TUNE_COMP:SLEEP             e9d4 4 0e 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_RSP_TUNE_COMP:WAKEUP            e9d4 4 0e 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_RF_RSP_TUNE_COMP:DEMOD             e9d4 4 0e 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:NO_RF_LOCK:INIT                       e9d4 4 0f 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:NO_RF_LOCK:DOZE                       e9d4 4 0f 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:NO_RF_LOCK:SLEEP                      e9d4 4 0f 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:NO_RF_LOCK:WAKEUP                     e9d4 4 0f 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:NO_RF_LOCK:DEMOD                      e9d4 4 0f 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:CX8_ON:INIT                           e9d4 4 10 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:CX8_ON:DOZE                           e9d4 4 10 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:CX8_ON:SLEEP                          e9d4 4 10 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:CX8_ON:WAKEUP                         e9d4 4 10 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:CX8_ON:DEMOD                          e9d4 4 10 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:RX_TUNE_COMP:INIT                     e9d4 4 11 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_TUNE_COMP:DOZE                     e9d4 4 11 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_TUNE_COMP:SLEEP                    e9d4 4 11 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_TUNE_COMP:WAKEUP                   e9d4 4 11 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:RX_TUNE_COMP:DEMOD                    e9d4 4 11 4    @olive @white

SRCHZZ_QPCH_ONTL:DEMOD:QPCH_DEMOD_DONE:INIT                  e9d4 4 12 0    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:QPCH_DEMOD_DONE:DOZE                  e9d4 4 12 1    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:QPCH_DEMOD_DONE:SLEEP                 e9d4 4 12 2    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:QPCH_DEMOD_DONE:WAKEUP                e9d4 4 12 3    @olive @white
SRCHZZ_QPCH_ONTL:DEMOD:QPCH_DEMOD_DONE:DEMOD                 e9d4 4 12 4    @olive @white



# End machine generated TLA code for state machine: SRCHZZ_QPCH_ONTL_SM

