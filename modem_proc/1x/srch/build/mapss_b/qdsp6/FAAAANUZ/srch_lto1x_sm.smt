/*=============================================================================

  srch_lto1x_sm.smt

Description:
  This file contains the machine generated source file for the state machine
  and/or state machine group specified in the file:
  srch_lto1x_sm.smf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

###############################################################################
# Copyright (c) 2018
# Qualcomm Technologies Incorporated.
# All Rights Reserved
# Qualcomm Confidential and Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law is prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and information contained therin are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
###############################################################################

=============================================================================*/

/* Include STM framework header */
#include "stm.h"

/* Begin machine generated code for state machine: LTO1X_SM */

/* Transition function prototypes */
static stm_state_type lto1x_process_tt_start(void *);
static stm_state_type lto1x_process_pilot_meas_cmd(void *);
static stm_state_type lto1x_resel_mdsp_ready_go_to_tune(void *);
static stm_state_type lto1x_resel_rf_locked_enable_mdsp(void *);
static stm_state_type lto1x_resel_process_tune_done(void *);
static stm_state_type lto1x_resel_srch_process_dump(void *);
static stm_state_type lto1x_process_lost_dump(void *);
static stm_state_type lto1x_process_srch_tl_timer(void *);
static stm_state_type lto1x_scan_all_process_dump(void *);
static stm_state_type lto1x_fing_lock_process_dump(void *);
static stm_state_type lto1x_process_fing_locked(void *);
static stm_state_type lto1x_fing_lock_failed(void *);
static stm_state_type lto1x_fing_locked_find_sync80(void *);
static stm_state_type lto1x_fing_locked_prep_slam(void *);
static stm_state_type lto1x_jump_failed(void *);
static stm_state_type lto1x_process_roll_cmd(void *);
static stm_state_type lto1x_trans_to_traffic(void *);
static stm_state_type lto1x_trans_to_idle(void *);
static stm_state_type lto1x_process_cdma_cmd(void *);
static stm_state_type lto1x_process_lte_1x_tt_cmd(void *);
static stm_state_type lto1x_process_deactivate_cmd(void *);
static stm_state_type lto1x_meas_setup_rf(void *);
static stm_state_type lto1x_meas_process_tune_done(void *);
static stm_state_type lto1x_meas_process_ftm_cmd(void *);
static stm_state_type lto1x_send_pilot_meas_cfg_req_msg_via_stm(void *);
static stm_state_type lto1x_meas_process_pilot_meas_cfg_rsp(void *);
static stm_state_type lto1x_meas_process_dump(void *);
static stm_state_type lto1x_meas_process_pilot_meas_stop(void *);
static stm_state_type lto1x_meas_process_lost_dump(void *);


/* State Machine entry/exit function prototypes */
static void lto1x_entry(stm_group_type *group);
static void lto1x_exit(stm_group_type *group);


/* State entry/exit function prototypes */
static void lto1x_capture_timing_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_wait_rf_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_tune_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_srch_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_scan_all_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_fing_verify_entry(void *payload, stm_state_type previous_state);
static void lto1x_resel_jump_entry(void *payload, stm_state_type previous_state);
static void lto1x_meas_prep_entry(void *payload, stm_state_type previous_state);
static void lto1x_meas_exit(void *payload, stm_state_type previous_state);
static void lto1x_meas_srch_entry(void *payload, stm_state_type previous_state);


/* Total number of states and inputs */
#define LTO1X_SM_NUMBER_OF_STATES 10
#define LTO1X_SM_NUMBER_OF_INPUTS 23


/* State enumeration */
enum
{
  LTO1X_CAPTURE_TIMING_STATE,
  LTO1X_RESEL_WAIT_RF_STATE,
  LTO1X_RESEL_TUNE_STATE,
  LTO1X_RESEL_SRCH_STATE,
  LTO1X_RESEL_SCAN_ALL_STATE,
  LTO1X_RESEL_FING_VERIFY_STATE,
  LTO1X_RESEL_JUMP_STATE,
  LTO1X_RESEL_TRANS_TO_NATIVE_1X_STATE,
  LTO1X_MEAS_PREP_STATE,
  LTO1X_MEAS_SRCH_STATE,
};


/* State name, entry, exit table */
static const stm_state_array_type
  LTO1X_SM_states[ LTO1X_SM_NUMBER_OF_STATES ] =
{
  {"LTO1X_CAPTURE_TIMING_STATE", lto1x_capture_timing_entry, NULL},
  {"LTO1X_RESEL_WAIT_RF_STATE", lto1x_resel_wait_rf_entry, NULL},
  {"LTO1X_RESEL_TUNE_STATE", lto1x_resel_tune_entry, NULL},
  {"LTO1X_RESEL_SRCH_STATE", lto1x_resel_srch_entry, NULL},
  {"LTO1X_RESEL_SCAN_ALL_STATE", lto1x_resel_scan_all_entry, NULL},
  {"LTO1X_RESEL_FING_VERIFY_STATE", lto1x_resel_fing_verify_entry, NULL},
  {"LTO1X_RESEL_JUMP_STATE", lto1x_resel_jump_entry, NULL},
  {"LTO1X_RESEL_TRANS_TO_NATIVE_1X_STATE", NULL, NULL},
  {"LTO1X_MEAS_PREP_STATE", lto1x_meas_prep_entry, lto1x_meas_exit},
  {"LTO1X_MEAS_SRCH_STATE", lto1x_meas_srch_entry, lto1x_meas_exit},
};


/* Input value, name table */
static const stm_input_array_type
  LTO1X_SM_inputs[ LTO1X_SM_NUMBER_OF_INPUTS ] =
{
  {(stm_input_type)IRAT_START_CDMA_TT_F, "IRAT_START_CDMA_TT_F"},
  {(stm_input_type)IRAT_PILOT_MEAS_CMD, "IRAT_PILOT_MEAS_CMD"},
  {(stm_input_type)MDSP_ENABLED_CMD, "MDSP_ENABLED_CMD"},
  {(stm_input_type)IRAT_RESEL_RF_LOCKED_CMD, "IRAT_RESEL_RF_LOCKED_CMD"},
  {(stm_input_type)TUNE_DONE_CMD, "TUNE_DONE_CMD"},
  {(stm_input_type)SRCH_DUMP_CMD, "SRCH_DUMP_CMD"},
  {(stm_input_type)SRCH_LOST_DUMP_CMD, "SRCH_LOST_DUMP_CMD"},
  {(stm_input_type)SRCH_TL_TIMER_CMD, "SRCH_TL_TIMER_CMD"},
  {(stm_input_type)IRAT_FING_LOCKED_CMD, "IRAT_FING_LOCKED_CMD"},
  {(stm_input_type)IRAT_FING_LOCK_FAILED_CMD, "IRAT_FING_LOCK_FAILED_CMD"},
  {(stm_input_type)IRAT_FIND_SYNC80_CMD, "IRAT_FIND_SYNC80_CMD"},
  {(stm_input_type)TT_SLAMCOMB_CMD, "TT_SLAMCOMB_CMD"},
  {(stm_input_type)IRAT_JUMP_FAILED_CMD, "IRAT_JUMP_FAILED_CMD"},
  {(stm_input_type)ROLL_CMD, "ROLL_CMD"},
  {(stm_input_type)SRCH_TC_F, "SRCH_TC_F"},
  {(stm_input_type)SRCH_IDLE_F, "SRCH_IDLE_F"},
  {(stm_input_type)SRCH_CDMA_F, "SRCH_CDMA_F"},
  {(stm_input_type)SRCH_LTE_1X_TT_F, "SRCH_LTE_1X_TT_F"},
  {(stm_input_type)SRCH_DEACTIVATE_F, "SRCH_DEACTIVATE_F"},
  {(stm_input_type)IRAT_FTM_MEAS_CMD, "IRAT_FTM_MEAS_CMD"},
  {(stm_input_type)IRAT_PILOT_MEAS_CFG_TIMER_CMD, "IRAT_PILOT_MEAS_CFG_TIMER_CMD"},
  {(stm_input_type)IRAT_PILOT_MEAS_CFG_RSP_CMD, "IRAT_PILOT_MEAS_CFG_RSP_CMD"},
  {(stm_input_type)IRAT_PILOT_MEAS_STOP_CMD, "IRAT_PILOT_MEAS_STOP_CMD"},
};


/* Transition table */
static const stm_transition_fn_type
  LTO1X_SM_transitions[ LTO1X_SM_NUMBER_OF_STATES *  LTO1X_SM_NUMBER_OF_INPUTS ] =
{
  /* Transition functions for state LTO1X_CAPTURE_TIMING_STATE */
    lto1x_process_tt_start,    /* IRAT_START_CDMA_TT_F */
    lto1x_process_pilot_meas_cmd,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_WAIT_RF_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    lto1x_resel_mdsp_ready_go_to_tune,    /* MDSP_ENABLED_CMD */
    lto1x_resel_rf_locked_enable_mdsp,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_TUNE_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    lto1x_resel_process_tune_done,    /* TUNE_DONE_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_SRCH_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_resel_srch_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    lto1x_process_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_SCAN_ALL_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_scan_all_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    lto1x_process_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_FING_VERIFY_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_fing_lock_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    lto1x_process_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    lto1x_process_fing_locked,    /* IRAT_FING_LOCKED_CMD */
    lto1x_fing_lock_failed,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_JUMP_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_fing_lock_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    lto1x_process_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    lto1x_fing_locked_find_sync80,    /* IRAT_FIND_SYNC80_CMD */
    lto1x_fing_locked_prep_slam,    /* TT_SLAMCOMB_CMD */
    lto1x_jump_failed,    /* IRAT_JUMP_FAILED_CMD */
    lto1x_process_roll_cmd,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    NULL,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_RESEL_TRANS_TO_NATIVE_1X_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_fing_lock_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    lto1x_process_srch_tl_timer,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    lto1x_trans_to_traffic,    /* SRCH_TC_F */
    lto1x_trans_to_idle,    /* SRCH_IDLE_F */
    lto1x_process_cdma_cmd,    /* SRCH_CDMA_F */
    lto1x_process_lte_1x_tt_cmd,    /* SRCH_LTE_1X_TT_F */
    lto1x_process_deactivate_cmd,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_MEAS_PREP_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    lto1x_meas_setup_rf,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    lto1x_meas_process_tune_done,    /* TUNE_DONE_CMD */
    NULL,    /* SRCH_DUMP_CMD */
    NULL,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    lto1x_process_deactivate_cmd,    /* SRCH_DEACTIVATE_F */
    lto1x_meas_process_ftm_cmd,    /* IRAT_FTM_MEAS_CMD */
    lto1x_send_pilot_meas_cfg_req_msg_via_stm,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    lto1x_meas_process_pilot_meas_cfg_rsp,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    NULL,    /* IRAT_PILOT_MEAS_STOP_CMD */

  /* Transition functions for state LTO1X_MEAS_SRCH_STATE */
    NULL,    /* IRAT_START_CDMA_TT_F */
    NULL,    /* IRAT_PILOT_MEAS_CMD */
    NULL,    /* MDSP_ENABLED_CMD */
    NULL,    /* IRAT_RESEL_RF_LOCKED_CMD */
    NULL,    /* TUNE_DONE_CMD */
    lto1x_meas_process_dump,    /* SRCH_DUMP_CMD */
    lto1x_meas_process_lost_dump,    /* SRCH_LOST_DUMP_CMD */
    NULL,    /* SRCH_TL_TIMER_CMD */
    NULL,    /* IRAT_FING_LOCKED_CMD */
    NULL,    /* IRAT_FING_LOCK_FAILED_CMD */
    NULL,    /* IRAT_FIND_SYNC80_CMD */
    NULL,    /* TT_SLAMCOMB_CMD */
    NULL,    /* IRAT_JUMP_FAILED_CMD */
    NULL,    /* ROLL_CMD */
    NULL,    /* SRCH_TC_F */
    NULL,    /* SRCH_IDLE_F */
    NULL,    /* SRCH_CDMA_F */
    NULL,    /* SRCH_LTE_1X_TT_F */
    lto1x_process_deactivate_cmd,    /* SRCH_DEACTIVATE_F */
    NULL,    /* IRAT_FTM_MEAS_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_TIMER_CMD */
    NULL,    /* IRAT_PILOT_MEAS_CFG_RSP_CMD */
    lto1x_meas_process_pilot_meas_stop,    /* IRAT_PILOT_MEAS_STOP_CMD */

};


/* State machine definition */
stm_state_machine_type LTO1X_SM =
{
  "LTO1X_SM", /* state machine name */
  18148, /* unique SM id (hash of name) */
  lto1x_entry, /* state machine entry function */
  lto1x_exit, /* state machine exit function */
  LTO1X_CAPTURE_TIMING_STATE, /* state machine initial state */
  TRUE, /* state machine starts active? */
  LTO1X_SM_NUMBER_OF_STATES,
  LTO1X_SM_NUMBER_OF_INPUTS,
  LTO1X_SM_states,
  LTO1X_SM_inputs,
  LTO1X_SM_transitions,
};

/* End machine generated code for state machine: LTO1X_SM */

