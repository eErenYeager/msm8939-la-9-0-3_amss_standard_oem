#ifndef TX_H
#define TX_H

/*===========================================================================

          M A I N   T R A N S M I T   T A S K   D E F I N I T I O N S

DESCRIPTION
  This file contains global declarations and external references
  for the Main Transmit task.

Copyright (c) 1992-2005,2009 by Qualcomm Technologies Incorporated.  All Rights Reserved.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MSM5100/CP_REL_A/vcs/tx.h_v   1.0.2.0   30 Nov 2001 17:46:32   fchan  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/api/public/tx.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/26/09   jj      Split this file as part of CMI Phase2.
04/06/09   mca     Added sigs for RF SVDO API
03/15/05   vlc     Mainlined TXC_TX_SHUTDOWN_SIG.
09/01/04   sr      Defined TXC_MDSP_DV_CHAN_TERM_SIG
08/17/04   sr      Defined TXC_MDSP_DV_APP_READY_SIG
05/28/04   bkm/ll  Define TXC_10MS_TICK_SIG.
02/10/04   az      Added TXC_ACTION_TIME_SIG for action time notification
09/15/03   jrp     Added signal TXC_TX_SHUTDOWN_SIG for feature
                   FEATURE_DELAYED_TX_CLOCK_SHUTDOWN.
01/12/02   fc      Fixed compiler warnings.
06/11/93   jca     Added TXC_RF_TIMER_SIG.
07/15/92   jai     Deleted TXC_STOP_SIG (not used).
07/08/92   jca     Deleted references to TXCDMA sigs (prefix is now TXC).
07/07/92   arh     Initial check-in for DMSS
04/16/92   jca     Creation

===========================================================================*/

/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

/* <EJECT> */
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

/* <EJECT> */
/*===========================================================================

                     PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================

FUNCTION TX_TASK

DESCRIPTION
  This procedure is the entrance procedure for the Main Transmit task.
  It contains the main processing loop for the Main Transmit task which
  controls the activation of the ACPTX and TXC subtasks.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void tx_task (
  dword dummy
    /* Required for REX, ignore */
);

#endif

