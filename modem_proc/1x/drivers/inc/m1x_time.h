#ifndef M1X_TIME_H
#define M1X_TIME_H

/*=============================================================================

              1 X  T I M E   S E R V I C E   S U B S Y S T E M

GENERAL DESCRIPTION
  Implements 1x modem timekeeping API


EXTERNALIZED FUNCTIONS
  m1x_time_get
    Return the system timestamp value.

Copyright(c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

=============================================================================*/


/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/drivers/inc/m1x_time.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/04/12   trc     Initial version, ported from time_cdma.h

=============================================================================*/


/*=============================================================================

                           INCLUDE FILES

=============================================================================*/

#include "time_types.h"

/** @addtogroup m1x_time
@{ */
/*=============================================================================

                           DATA DEFINITIONS

=============================================================================*/
/**
Time source status code returned by m1x_time_get()
*/
typedef enum
{
  M1X_TIME_INVALID = 0,                 /*!< 1x modem time is invalid/unset */
  M1X_TIME_CDMA_SOURCE,                 /*!< 1x modem time is network based */
  M1X_TIME_DEAD_RECKONING_SOURCE        /*!< 1x modem time is last-known good
                                             network time + timetick-based
                                             dead-reckoning */
} m1x_time_source_type;


/*=============================================================================

                           FUNCTION DEFINITIONS

=============================================================================*/

/**
Returns the 64-bit CDMA system timestamp.

@param[out] time_val Address of the qword in which to return the 64-bit system
                     timestamp.

@return
@see m1x_time_source_type
time_val is set to the current system time in timestamp format.

*/
m1x_time_source_type m1x_time_get
(
  time_type                       time_val
);

/** @} */ /* end_addtogroup m1x_time */

#endif /* M1X_TIME_H */
