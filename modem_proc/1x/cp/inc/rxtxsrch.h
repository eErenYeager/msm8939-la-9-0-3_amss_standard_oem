#ifndef RXTXSRCH_H
#define RXTXSRCH_H
/*===========================================================================

                   R X T X    S R C H    H E A D E R    F I L E

DESCRIPTION
  This file contains the interface between the rxtx (layer 2) task and the
  Search task.

Copyright (c) 2004-2005 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/inc/rxtxsrch.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/17/12   ppr     Feature Cleanup: Mainlined Always ON features
06/11/04   yll     Created file.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"

/* <EJECT> */
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/


/* <EJECT> */
/*===========================================================================

                      FUNCTION DECLARATIONS

===========================================================================*/



/*===========================================================================

FUNCTION RXTXSRCH_REG_RX_MSG_NOTIFIER

DESCRIPTION
  This registers a function to notify the received message.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
extern void rxtxsrch_reg_rx_msg_notifier
(
  void ( *rx_msg_notifier )( void )
);

/*===========================================================================

FUNCTION RXTXSRCH_REG_TX_MSG_NOTIFIER

DESCRIPTION
  This registers a function to notify the sent message.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
extern void rxtxsrch_reg_tx_msg_notifier
(
  void ( *tx_msg_notifier )( uint16 retry_count )
);



#endif /* RXTXSRCH_H */
