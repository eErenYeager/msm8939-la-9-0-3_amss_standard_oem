/*===========================================================================

         S E R V I C E   O P T I O N S   D E F I N I T I O N S

GENERAL DESCRIPTION
  This module contains definitions of service options other than data.

Copyright (c) 2000-2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath: L:/src/asw/MSM5100/CP_REL_A/vcs/mccso.h_v   1.0.2.0   30 Nov 2001 17:30:26   fchan  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/src/mccso.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/01/11   ag      Fix to release MVS before MUX is commanded to release SO.
01/04/11   ag      Mainlined FEATURE_MVS_MIGRATE.
09/14/10   ag      Featurized voice support under FEATURE_MODEM_1X_VOICE_SUPPORT.
05/15/08   bb      Code changes for "MVS API Update" feature
11/02/00   lcc     Initial release for FEATURE_COMPLETE_SNM.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "target.h"
#include "customer.h"
#include "modem_1x_defs.h"

#include "1x_variation.h"
#ifdef FEATURE_MODEM_1X_VOICE_SUPPORT
#include "mvs.h"
#endif /* FEATURE_MODEM_1X_VOICE_SUPPORT */

/*===========================================================================

                           FUNCTION PROTOTYPES

===========================================================================*/

#ifdef FEATURE_MODEM_1X_VOICE_SUPPORT
/*===========================================================================

FUNCTION mccso_cdmacodec_mvs_cb_func

DESCRIPTION
  This function initializes all the service options defined in mccso.c.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void mccso_cdmacodec_mvs_cb_func(mvs_event_type *event);
#endif /* FEATURE_MODEM_1X_VOICE_SUPPORT */

/*===========================================================================

FUNCTION MCCSO_RELEASE_MVS

DESCRIPTION
  This function releases MVS if it was acquired for CDMA voice services.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void mccso_release_mvs ( void );

/*===========================================================================

FUNCTION mccso_initialize

DESCRIPTION
  This function initializes all the service options defined in mccso.c.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void mccso_initialize( void );
