/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

       CDMA2000   P R O T O C O L   A C C E S S O R   F U N C T I O N S

DESCRIPTION

  This file contains the accessor functions for some fields' values which
  are dependent on certain features while the mobile station is operating in
  CDMA2000 mode.

Copyright (c) 1990-2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/src/cai.c#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14   agh     Fixed the accessibility issue of FEATURE_PASSPORT_MODE 
09/13/10   ag      Removed cai_get_xtra_last_parm_id() as last parm id is not
                   part of api/1x/cai.h file.
08/09/10   ag      Added file version information in the file header.
01/20/10   ag      Changed the value of cai_ac_alt_so_max to correspond with
                   CAI_AC_ALT_SO_MAX.
07/17/09   jj      Fixed Linker error
07/14/09   jj      Added function to initialtize cai_ac_alt_so_max.
05/20/09   ag      Fixed compiler warnings
05/13/09   ag      Changed the file header and replaced frwd by fwd.
04/09/09   jj      CMI phase2 changes.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "1x_variation.h"
#include "comdef.h"
#include "cai_v.h"
#include "modem_1x_defs.h"

#ifdef FEATURE_PASSPORT_MODE
const uint8 cai_ac_alt_so_max = 10;
#else
const uint8 cai_ac_alt_so_max = 8;
#endif /* FEATURE_PASSPORT_MODE */

uint16 fwd_tc_msg_len;
uint16 rev_tc_msg_len;

/*===========================================================================

FUNCTION CAI_GET_FWD_TC_EXTENDED_MSG_LEN

DESCRIPTION
  This function returns fwd tc msg len to caller based on whether
  FEATURE_TC_EXTENDED_MSG_LENGTH is enabled or not.

DEPENDENCIES
  None

RETURN VALUE
  Max Forward Traffic Channel msg length

SIDE EFFECTS
  None

===========================================================================*/
uint16 cai_get_fwd_tc_extended_msg_len(void)
{
#ifdef FEATURE_TC_EXTENDED_MSG_LENGTH
  #error code not present
#else
  fwd_tc_msg_len = 255;
  /* see IS-95A Section 7.7.3.2.1 */
#endif
  return fwd_tc_msg_len;
}

/*===========================================================================

FUNCTION CAI_GET_REV_TC_EXTENDED_MSG_LEN

DESCRIPTION
  This function returns rev tc msg len to caller based on whether
  FEATURE_TC_EXTENDED_MSG_LENGTH is enabled or not.

DEPENDENCIES
  None

RETURN VALUE
  Max Reverse Traffic Channel msg length

SIDE EFFECTS
  None

===========================================================================*/
uint16 cai_get_rev_tc_extended_msg_len(void)
{
#ifdef FEATURE_TC_EXTENDED_MSG_LENGTH
  #error code not present
#else
  rev_tc_msg_len = 255;
  /* see IS-95A Section 6.7.2.2 */
#endif
  return rev_tc_msg_len;
}

/*===========================================================================
FUNCTION CAI_GET_AC_ALT_SO_MAX

DESCRIPTION
  This function returns maximum value of ALT SO based on whether
  FEATURE_PASSPORT_MODE is enabled or not.

DEPENDENCIES
  None

RETURN VALUE
  maximum value of ALT SO

SIDE EFFECTS
  None

===========================================================================*/
uint8 cai_get_ac_alt_so_max(void)
{
  return cai_ac_alt_so_max;
}

