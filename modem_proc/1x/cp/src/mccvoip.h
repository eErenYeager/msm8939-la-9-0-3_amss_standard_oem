#ifndef MCCVOIP_H
  #define MCCVOIP_H

/*===========================================================================

            MAIN CONTROL VOIP-1X HANDOFF SUPPORT FUNCTIONS

GENERAL DESCRIPTION
  This module contains the functions that support VOIP-1X handoffs.

Copyright (c) 2001-2005 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/src/mccvoip.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/09/12   ppr     Feature Cleanup
02/14/07   sb      Moved logging structure to mccvoip.c.
01/15/07   sb      VOIP-1X handoff updates.
06/28/06   sb      Created module to support VOIP-1X handoffs.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "1x_variation.h"
#include "target.h"
#include "customer.h"
#include "log.h"

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed by this module.

===========================================================================*/

#endif /* MCCVOIP_H */
