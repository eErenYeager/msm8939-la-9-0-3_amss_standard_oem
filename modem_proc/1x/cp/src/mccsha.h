/* "mccsha.h" */
#ifndef MCCSHA_H
#define MCCSHA_H

/*===========================================================================

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/src/mccsha.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

===========================================================================*/

#include "1x_variation.h"
#include "comdef.h"

#if defined(FEATURE_IS2000_REL_A_AES)

/****************************************************************************
*
*  THIS SOFTWARE IS DELIVERED AND MADE PUBLICLY AVAILABLE "AS IS."  QUALCOMM
*  MAKES NO WARRANTY OF ANY NATURE, EITHER EXPRESS OR IMPLIED, WITH RESPECT TO
*  THE SOFTWARE, INCLUDING BUT NOT LIMITED TO ANY WARRANTY (i) OF ITS
*  ACCURACY, FUNCTIONALITY OR EFFECTIVENESS; (ii) FOR THE CONSEQUENCES OF ITS
*  USE; (iii) OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE; OR (iv)
*  THAT IT WILL BE FREE FROM INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
*  OR OTHER RIGHTS.
*
*  QUALCOMM SHALL NOT BE LIABLE TO ANY INDIVIDUAL,  COMPANY OR ENTITY FOR ANY
*  DETRIMENT, LOSS OR DAMAGE IN ANY WAY INCURRED, DIRECTLY OR INDIRECTLY, BY
*  REASON OF THE USE OF THE SOFTWARE, INCLUDING BUT NOT LIMITED TO DIRECT OR
*  INDIRECT DAMAGES, INCIDENTAL OR CONSEQUENTIAL DAMAGES, OR ANY OTHER LOSS OR
*  DAMAGE ARISING OUT OF THE USE OF THE SOFTWARE, WHETHER IN AN ACTION FOR OR
*  ARISING OUT OF BREACH OF CONTRACT, TORT OR ANY OTHER CAUSE OF ACTION.
*
*****************************************************************************
*
*   File:           mccsha.h
*
*   Contact:        John Noerenberg
*                   Qualcomm Technologies, Inc
*
*                   jwn2@qualcomm.com
*
*   Description:    Header for SHA-1 functions implemented for ESA
*
*****************************************************************************/

/* header for SHA and related procedures */
#define WORD unsigned long
#define DIGEST_LENGTH 20

typedef struct {
  unsigned char digest[DIGEST_LENGTH]; /* Message digest */
  WORD   count[2];    /* count of bits */
  WORD   data[16];    /* data buffer */
  } SHA_INFO;

void shaInitial(SHA_INFO *sha_info);
void shaUpdate(SHA_INFO *sha_info,
        unsigned char *buffer,
        unsigned long offset,
        unsigned long count);
void shaFinal(SHA_INFO *sha_info);

#endif /* FEATURE_IS2000_REL_A_AES */
#endif /* MCCSHA_H */
