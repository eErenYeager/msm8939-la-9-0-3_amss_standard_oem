#ifndef MCCUMTS_H
#define MCCUMTS_H

/*===========================================================================

                     1 X   U M T S   M A N A G E R
                   S U P P O R T   F O R   H A N D O V E R

GENERAL DESCRIPTION
  This module contains utilities to handle requests from Data Service Task.

Copyright (c) 2001-2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE
This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/1x/cp/src/mccumts.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/09/12   ppr     Feature Cleanup
07/19/10   ag      Replaced "PACKED <name>" with "PACK (<name>)".
10/26/06   fc      Changes on logging.
10/19/06   fc      Changes per code review.
09/30/06   fc      Changes per UT verification.
07/10/06   fc      Added definitions to support logging.
05/05/06   fc      Moved all UMTS related data definitions and function
                   prototypes from other modules to here.

===========================================================================*/
#include "1x_variation.h"

#endif /* MCCUMTS_H */
