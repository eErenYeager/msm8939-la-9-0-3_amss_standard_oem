#ifndef CM_EMGEXT_H
#define CM_EMGEXT_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

            C A L L   M A N A G E R   C A L L   M A N A G E R  LTE Emergency call 

GENERAL DESCRIPTION
  This module contains LTE emergency call access for remote IMS.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  cmdbm_init() must be called to initialize this module before any other
  function declared in this module is being called.

Copyright (c) 2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/cm/src/cmemgext.h#1 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
04/29/13    xs      Initial version

===========================================================================*/
#include "comdef.h"
#include "cmll_v.h"

#define EXT_E911_CALL_ID 0xFE

typedef struct
{
  boolean is_ready;
  cm_ac_reason_e_type reason;
}cmemg_s_type;


/*===========================================================================

FUNCTION cmemg_get_act_id

DESCRIPTION
  Return act id for access control  

DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
uint32 cmemg_get_act_id (void);

/*===========================================================================

FUNCTION cmemg_init

DESCRIPTION
  Access control initialize

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  none

===========================================================================*/
void cmemg_init( void );
/*===========================================================================

FUNCTION cmemg_msgr_cmd_proc

DESCRIPTION
  Process reports from the lower layers.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmemg_msgr_cmd_proc(

  const void   *cmd_ptr
    /* Pointer to the report sent by Message Router */
);

/*===========================================================================

FUNCTION cmemg_sd_rpt_proc

DESCRIPTION
  Process System Determination reports


DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmemg_sd_rpt_proc(

  const cm_hdr_type   *rpt_ptr
    /* Pointer to SD reports */
);
/*===========================================================================

FUNCTION cmemg_exit_emgergency_state

DESCRIPTION
  Request access control to exit emergency state.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmemg_exit_emgergency_state (boolean is_ecbm_required, void* user_data);

/*===========================================================================

FUNCTION cmemg_set_reason

DESCRIPTION
  Set the reason for access control

DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmemg_set_reason(cm_ac_reason_e_type res);
/*===========================================================================

FUNCTION cmemg_get_reason

DESCRIPTION
  Return the reason for access control

DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
cm_ac_reason_e_type cmemg_get_reason(void);
#endif
