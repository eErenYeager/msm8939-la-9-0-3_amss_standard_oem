/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

      C A L L   M A N A G E R   P O L I C Y   M A N A G E R   P R O X Y

GENERAL DESCRIPTION
  This file is the main implementation for the Policy Manager(PM) proxy. The
  purpose of PMProxy is to act as a proxy to interface with the Policy Manager.
  Interpreting the Policy Manager messages into Call Manager actions.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  cmpmprx_init() must be called to initialize this module before any other
  function declared in this module is being called.

Copyright (c) 2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/cm/src/cmpmprx.c#4 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
11/14/13   ss       Do not enforce policy when UE is OFFLINE
09/12/13    jvo     Fix 2 static analysis errors with one change relating to
                    policy comparison
04/29/13    ak      Initial design

===========================================================================*/
#include "mmcp_variation.h"
#include "customer.h"       /* Customer configuration file */
#include "comdef.h"         /* Definition for basic types and macros */
#include "cmph.h"
#include "cm_msgr_msg.h"
#include "cmtaski.h"
#include "cmidbg.h"
#include "cmpmprx.h"
#include "cmss.h"
#include "cmmsc.h"
#include "cmsimcoord.h"
#include "cmsoa.h"
#ifdef FEATURE_DOMAIN_SELECTION
#include "cmsds.h"
#endif

#ifdef CM_DEBUG
#error code not present
#endif


/*===========================================================================
=============================================================================
=============================================================================
============================  Internal Functions ==================================
=============================================================================
=============================================================================
===========================================================================*/

/* Forward declaration
*/
static policyman_status_t       cmpmprx_retrieve_policy_item_data
(
       cm_policy_config_s_type    *pm_ptr,

       policyman_item_collection_t const *policy_item,

       policy_change_mask         *change_mask,

       policy_change_mask         *device_change_mask

);
static void cmpmprx_init_pm_feature_mode( void );
static void cmpmprx_init_pm_ue_mode( void );
static void cmpmprx_set_policy_chg_flag( sys_modem_as_id_e_type asubs_id, policy_change_mask change_value );
static void cmpmprx_reset_policy_chg_flag( sys_modem_as_id_e_type asubs_id, policy_change_mask change_value );
static void cmpmprx_print_current_policy(sys_modem_as_id_e_type asubs_id);
static boolean cmpmprx_compare_policies(cm_policy_config_s_type    *policy_1,cm_policy_config_s_type    *policy_2);

static cm_policy_config_s_type cmpm_local;
  /* The one and only phone object */


/* Function Definations
*/
/*===========================================================================

FUNCTION cmpm_ptr

DESCRIPTION
  Return a pointer to the one and only phone policy object.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
cm_policy_config_s_type  *cmpm_ptr( void )
{

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return &cmpm_local;

} /* cmph_ptr() */

/*===========================================================================

FUNCTION cmpmprx_configure_num_policies

DESCRIPTION
  Based on the new number of policies add or delete an existing instance on
  policy in CMPMPRX.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void                      cmpmprx_configure_num_policies
(
       cm_policy_config_s_type   *pm_ptr,

       size_t                    new_nSims
)
{
  int                       min_sims = 0;
  uint8                     i=0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* If number of Sims has not changed, we still have to deal with same number
  ** of policies. */
  if (pm_ptr->device_mode.nSims == new_nSims)
  {
    return;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Compute the min value.
  */
  min_sims = MIN(pm_ptr->device_mode.nSims,new_nSims );


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* If new sims is greater create more policy structures.*/
  for (i=min_sims; i<new_nSims && i<POLICY_ITEM_ARRAY_SIZE;i++)
  {
    pm_ptr->current_policy[i].policy_mode_capability     = SD_SS_MODE_PREF_ANY;
    pm_ptr->current_policy[i].policy_band_capability     = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].policy_lte_band_capability = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].policy_tds_band_capability = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].service_scan_mode          = POLICYMAN_SVC_MODE_FULL;
    pm_ptr->current_policy[i].ue_operation_mode          = SYS_UE_MODE_NORMAL;

    pm_ptr->policy_changes[i] = PM_CHG_CHANGE_NONE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* If new sims is less than existing sims. reset the extra existing policies */
  for (i=min_sims;i<pm_ptr->device_mode.nSims && i<POLICY_ITEM_ARRAY_SIZE;i++)
  {
    pm_ptr->current_policy[i].policy_mode_capability     = SD_SS_MODE_PREF_ANY;
    pm_ptr->current_policy[i].policy_band_capability     = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].policy_lte_band_capability = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].policy_tds_band_capability = SYS_BAND_MASK_ANY;
    pm_ptr->current_policy[i].service_scan_mode          = POLICYMAN_SVC_MODE_FULL;
    pm_ptr->current_policy[i].ue_operation_mode          = SYS_UE_MODE_NORMAL;

    pm_ptr->policy_changes[i] = PM_CHG_CHANGE_NONE;
  }

}

static void cmpmprx_configure_policy_max_active( void )
{
  cm_policy_config_s_type   *pm_ptr = cmpm_ptr();
  cmph_s_type *ph_ptr = cmph_ptr();

    CM_MSG_HIGH_2("Inform device mode to MMOC nsim %d, nmaxactive %d", 
                pm_ptr->device_mode.nSims, pm_ptr->device_mode.nMaxActive); 
    /* Push standby pref change to MMOC */
    cmmsimc_proc_cmd_standby_pref_chgd(ph_ptr->internal_standby_pref, 
                                       ph_ptr->active_subs, 
                                       ph_ptr->is_cdma_subscription_available,
                                       ph_ptr->is_gwl_subscription_available,
                                       ph_ptr->is_hybr_gw_subscription_available,
                                       ph_ptr->is_hybr_gw3_subscription_available);
    
  /* Inform new Max active subs to MCS */
	cmph_sp_subs_info_update();
}

static void cmpmprx_configure_policy_max_active_data( void )
{

 #ifdef FEATURE_MMODE_DUAL_SIM

  sys_modem_as_id_e_type curr_subs;
  cm_ss_e_type ss;
  uint8 loop=0;
  cmph_s_type *ph_ptr = cmph_ptr();
  cm_policy_config_s_type *pm_ptr = cmpm_ptr();
  
  CM_MSG_HIGH_0("CM_PS_PS_DEBUG Inside cmpmprx_configure_policy_max_active_data Forcing CS Only"); 

 for(loop=0; loop<=pm_ptr->device_mode.nSims; loop++)  
  {

  curr_subs = loop;

  if( loop != ph_ptr->default_data_subs )
  {
   ss = cmph_map_subs_to_ss(curr_subs);

    
   if(cmcall_check_if_ps_call_active(ss))
   {
     CM_MSG_HIGH_1("Active PS calls on ss %d", ss);

	 if(cmcall_is_no_call_in_gw_ps(ss) == FALSE )
	 {
	   CM_MSG_HIGH_1("Ending active PS calls on ss %d", ss);
	   	
       cmcall_end_each_call_with_type(CM_CALL_TYPE_DATA,
				CM_CALL_DUAL_DATA_NOT_SUPPORTED);
	 }
    #if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
		cmcall_end_active_1x_datacalls();
    #endif

  } 
  if(ss == CM_SS_MAIN)
  {
   #if (defined(FEATURE_CDMA_800) || defined(FEATURE_CDMA_1900))
	cmcall_end_active_HDR_datacalls();
   #endif
  }

   /* Delete Data call pref from priority queue, which might be there
   ** for 4 sec after end of data call
   */
  if(cmtask_orig_para_search_act_type(ss,CM_ACT_TYPE_DATA_CALL) != NULL)
  {
    cmph_remove_orig_mode( ss, CM_ACT_TYPE_DATA_CALL, CM_ACT_PRIORITY_MAX );
   /* Force CS_ONLY on the non-DDS sub */
   /* Instead of forcing from persistent
   Force the preferences from top of the queue by calling cmph_force_orig_mode_on_ss
   */
   //cmph_force_orig_mode_on_ss(ss, ph_ptr);
  }

  if(cmtask_orig_para_search_act_type(CM_SS_HDR,CM_ACT_TYPE_DATA_CALL) != NULL)
  {
 	cmph_remove_orig_mode( CM_SS_HYBR_1, CM_ACT_TYPE_DATA_CALL, CM_ACT_PRIORITY_MAX );
  }

  } 

 /* Force CS Only mode to NON DDS Subs */

 if(cmph_map_subs_stack_to_ss(curr_subs,0) == CM_SS_MAIN)
 {
   CM_MSG_HIGH_0("CM_PS_PS_DEBUG Inside cmpmprx_configure_policy_max_active_data Forcing CS Only");  
   (void) cmph_force_pref_on_the_fly( CM_SS_MAX,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->main_stack_info.pref_info.pref_term,
                                         ph_ptr->main_stack_info.pref_info.mode_pref,
                                         ph_ptr->main_stack_info.pref_info.band_pref,
                                         ph_ptr->main_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->main_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->main_stack_info.pref_info.prl_pref,
                                         ph_ptr->main_stack_info.pref_info.roam_pref,
                                         ph_ptr->main_stack_info.pref_info.hybr_pref,
                                         ph_ptr->main_stack_info.pref_info.plmn,
                                         CM_SRV_DOMAIN_PREF_CS_ONLY,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->main_stack_info.pref_info.acq_order_pref,
                                         ph_ptr->main_stack_info.pref_info.network_sel_mode_pref,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->main_stack_info.pref_info.csg_id,
                                         ph_ptr->main_stack_info.pref_info.csg_rat);
   }

    #ifdef FEATURE_MMODE_DUAL_SIM
      if (!cmph_is_sxlte() && cmph_is_msim() && (cmph_map_subs_stack_to_ss(curr_subs,0) == CM_SS_HYBR_2))
      {
       CM_MSG_HIGH_0("CM_PS_PS_DEBUG Inside cmpmprx_configure_policy_max_active_data Forcing CS Only2");  
          (void) cmph_force_pref_on_the_fly( CM_SS_HYBR_2,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->hybr_2_stack_info.pref_info.pref_term,
                                         ph_ptr->hybr_2_stack_info.pref_info.mode_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.prl_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.roam_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.hybr_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.plmn,
                                         ph_ptr->hybr_2_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->hybr_2_stack_info.pref_info.acq_order_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.network_sel_mode_pref,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_rat);	

        CM_MSG_HIGH_0("DUAL_SIM, PREF forced on HYBR2 ");  
	  
	  }
 #endif	  
 
 #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
      if ((cmph_is_msim() || cmph_is_sxlte()) && (cmph_map_subs_stack_to_ss(curr_subs,0) == CM_SS_HYBR_3))
	  {
 
          (void) cmph_force_pref_on_the_fly( CM_SS_HYBR_3,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->hybr_3_stack_info.pref_info.pref_term,
                                         ph_ptr->hybr_3_stack_info.pref_info.mode_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.prl_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.roam_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.hybr_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.plmn,
                                         ph_ptr->hybr_3_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->hybr_3_stack_info.pref_info.acq_order_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.network_sel_mode_pref,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_rat);

        CM_MSG_HIGH_0("TRIPLE_SIM or SXLTE_G , PREF forced on HYBR3 ");
										 
      }
 #endif		

 }
 #endif /* FEATURE_MMODE_DUAL_SIM */ 
}

static void cmpmprx_enforce_device_policy( void )
{

  /* Enforce device mode policy only if any subscription is available */
  /* Otherwise device policy will be picked up during subscription available procedure */
  if(cmph_ptr()->oprt_mode == SYS_OPRT_MODE_ONLINE &&
     ( cmph_ptr()->is_cdma_subscription_available ||
       cmph_ptr()->is_gwl_subscription_available ||
       cmph_ptr()->is_hybr_gw_subscription_available ||
       cmph_ptr()->is_hybr_gw3_subscription_available))
  {
    
   if(cmpm_ptr()->device_policy_changes & PM_CHG_DEVICE_MODE) 
   {  
    cmpmprx_configure_policy_max_active();
   }
   if(cmpm_ptr()->device_policy_changes & PM_CHG_MAX_ACTIVE_DATA && 
	   (cmpmprx_get_num_of_active_data() == 1))
   {
	cmpmprx_configure_policy_max_active_data();
   }

  }
  else
  {
    CM_MSG_HIGH_0("Device policy discarded. UE NOT Online");
  }
  
  cmpm_ptr()->device_policy_changes = 0;

  return;
}

/*===========================================================================

FUNCTION cmpmprx_init_pm_feature_mode

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void cmpmprx_init_pm_feature_mode( void )
{

  cm_policy_config_s_type *pm_ptr           = cmpm_ptr();
  boolean                 is_policy_valid   = FALSE;
  policyman_status_t  status = POLICYMAN_STATUS_ERR;

  #ifndef CM_DEBUG
  policyman_item_collection_t const   *pCollection;

  policyman_item_id_t     id = POLICYMAN_ITEM_DEVICE_CONFIGURATION;

  if (POLICYMAN_SUCCEEDED(policyman_get_items_block_msim((policyman_item_id_t const *)&id,
                                                     1,
                                                     &pCollection)
                          )
     )
  {
    sys_overall_feature_t new_overall_feature_mode = SYS_OVERALL_FEATURE_MODE_NORMAL;
    size_t                new_nSims                = 0;
    size_t                new_nMaxActive           = 0;
	size_t                new_nMaxActiveData       = 1;  
    policyman_item_t const  *pItem = NULL;

    pItem = pCollection->pItems[0];         
    status = policyman_device_config_overall_feature( pItem,
                                                      &new_overall_feature_mode);
    if (POLICYMAN_SUCCEEDED(status))
    {
      if ( new_overall_feature_mode != SYS_OVERALL_FEATURE_MODE_MULTISIM )
      {
        new_nSims = new_nMaxActive = 1;
      }
      else
      {
        if(!POLICYMAN_SUCCEEDED(policyman_device_config_num_sims(pItem, &(new_nSims)))
            ||
		   !POLICYMAN_SUCCEEDED(policyman_device_config_max_active_data(pItem, &(new_nMaxActiveData)))
	        ||
           !POLICYMAN_SUCCEEDED(policyman_device_config_max_active(pItem, &(new_nMaxActive)))
          )
        {
          status = POLICYMAN_STATUS_ERR;
        }
      }
    }

    if (POLICYMAN_SUCCEEDED(status))
    {
      size_t i=0;
	  
      cmpmprx_configure_num_policies(pm_ptr,new_nSims);
      pm_ptr->feature_mode             = new_overall_feature_mode;
      pm_ptr->device_mode.nSims        = new_nSims;
      pm_ptr->device_mode.nMaxActive   = new_nMaxActive;
	  pm_ptr->device_mode.nMaxActiveData = new_nMaxActiveData;
      for (i=0;i<new_nSims && i<POLICY_ITEM_ARRAY_SIZE;i++)
      {
        sys_subs_feature_t          temp_sub_feature_mode;
        if(POLICYMAN_SUCCEEDED(policyman_device_config_get_subs_feature(pItem,0,i,&temp_sub_feature_mode)))
        {
          pm_ptr->current_policy[i].sub_feature_mode = temp_sub_feature_mode;
        }
        else
        {
          pm_ptr->current_policy[i].sub_feature_mode = SYS_SUBS_FEATURE_MODE_NORMAL;
        }
      }
    }
		 
    if(POLICYMAN_SUCCEEDED(status))
    {
      is_policy_valid = TRUE;
    }
  }
  
  policyman_item_collection_release(pCollection);
  #endif

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* If num of sims from PM exceeds the MAX_SIMS supported in CM,
  ** default the policy.
  */
  if (pm_ptr->device_mode.nSims > MAX_SIMS || pm_ptr->device_mode.nMaxActive > MAX_SIMS || pm_ptr->device_mode.nMaxActiveData > MAX_SIMS)
  {
    is_policy_valid = FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Error in reading the policy item(s). Discard everything and initialize to
  ** default values.
  */
  CM_MSG_HIGH_1("PMPRX: is feature_mode policy valid %d", is_policy_valid);
  if (!is_policy_valid)
  {
    pm_ptr->feature_mode = SYS_OVERALL_FEATURE_MODE_NORMAL;
    cmpmprx_configure_num_policies(pm_ptr,1);
    pm_ptr->device_mode.nSims = 1;
    pm_ptr->device_mode.nMaxActive = 1;
	pm_ptr->device_mode.nMaxActiveData = 1;
  }

  /* Print feature mode */
  CM_MSG_HIGH_1("Overall Feature_mode = %d", pm_ptr->feature_mode);

  return;
}

/*===========================================================================

FUNCTION cmpmprx_compare_policies

DESCRIPTION
  To compare policy for all subs.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: change in policy
  False: no change in policy.

SIDE EFFECTS
  none

===========================================================================*/
static boolean                    cmpmprx_compare_policies(

       cm_policy_config_s_type    *old_policy,

       cm_policy_config_s_type    *new_policy
)
{
  unsigned int i = 0;

  for(i=0; i<new_policy->device_mode.nSims; i++)
  {
    if(memcmp(&(old_policy->current_policy[i]),
              &(new_policy->current_policy[i]),
              sizeof(cm_policy_items_s_type)) != 0)
    {
      return TRUE;
    }
  }

  return FALSE;
  
}

/*===========================================================================

FUNCTION cmpmprx_read_pm_device_mode

DESCRIPTION
  Reads no. of SIM and max active SIMs.
===========================================================================*/

cm_policy_device_mode_s_type   cmpmprx_read_pm_device_mode(void)
{
  cm_policy_device_mode_s_type  device_mode;
  const policyman_item_t *pItem = NULL;
  const policyman_item_id_t id = POLICYMAN_ITEM_DEVICE_CONFIGURATION;
  cm_policy_config_s_type *pm_ptr = cmpm_ptr();

  /* If non multi-sim, set the default value */
  if(pm_ptr->feature_mode != SYS_OVERALL_FEATURE_MODE_MULTISIM)
  {
    device_mode.nSims = 1;
    device_mode.nMaxActive = 1;
  }
  else
  {
    if (policyman_get_items_block(&id, 1, &pItem)== POLICYMAN_STATUS_SUCCESS)
    {
      policyman_device_config_num_sims(pItem, &(device_mode.nSims));
      policyman_device_config_max_active(pItem, &(device_mode.nMaxActive));
    }
    else
    {
      device_mode.nSims = 1;
      device_mode.nMaxActive = 1;
    }
    policyman_item_release(pItem);
  }
  return device_mode;
}

/*===========================================================================

FUNCTION cmpmprx_init_pm_ue_mode

DESCRIPTION
  Read sglte related configuration from Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void cmpmprx_init_pm_ue_mode( void )
{

  cm_policy_config_s_type *pm_ptr           = cmpm_ptr();
  boolean                 is_policy_valid   = FALSE;
  uint8                   i                 = 0;

  #ifndef CM_DEBUG
  policy_change_mask      local_mask[3]     = {0,0,0};
  policyman_item_id_t     pIds[]            =
  {
    POLICYMAN_ITEM_RAT_CAPABILITY,
    POLICYMAN_ITEM_RF_BANDS,
    POLICYMAN_ITEM_SVC_MODE,
    POLICYMAN_ITEM_UE_MODE
  };

  size_t                  numIds            = ARR_SIZE(pIds);

  policyman_item_collection_t const  *pCollection;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT( pm_ptr != NULL );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Read the configuration from the policy manager. */
  if(POLICYMAN_SUCCEEDED(policyman_get_items_block_msim(pIds,numIds,&pCollection)))
  {
    is_policy_valid = TRUE;

    if(!POLICYMAN_SUCCEEDED(cmpmprx_retrieve_policy_item_data( pm_ptr,pCollection,local_mask,NULL)))
    {
      is_policy_valid = FALSE;
    }
    else
    {
      for (i=0;i<(int)pm_ptr->device_mode.nSims;i++)
      {
       pm_ptr->policy_changes[i] |= local_mask[i];
       CM_MSG_HIGH_2("Msgr policy[%d] mask = %x",i,pm_ptr->policy_changes[i]); 
      }
    }
  }

  policyman_item_collection_release(pCollection);

  #endif
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Error in reading the policy item(s). Discard everything and initialize to
  ** default values.
  */
  CM_MSG_HIGH_1("CMPRX: is_policy_valid %d", is_policy_valid);
  if (!is_policy_valid)
  {
    for(i = 0 ; i < pm_ptr->device_mode.nSims && i<POLICY_ITEM_ARRAY_SIZE; ++i)
    {
      pm_ptr->current_policy[i].policy_mode_capability     = SD_SS_MODE_PREF_ANY;
      pm_ptr->current_policy[i].policy_band_capability     = SYS_BAND_MASK_ANY;
      pm_ptr->current_policy[i].policy_lte_band_capability = SYS_BAND_MASK_ANY;
      pm_ptr->current_policy[i].policy_tds_band_capability = SYS_BAND_MASK_ANY;
      pm_ptr->current_policy[i].service_scan_mode          = POLICYMAN_SVC_MODE_FULL;
      pm_ptr->current_policy[i].ue_operation_mode          = SYS_UE_MODE_NORMAL;
      cmpmprx_reset_policy_chg_flag( i,PM_CHG_ALL );
    }
    /* No action need be taken to enforce this policy. So resetting all flags.
    */
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0 ; i < pm_ptr->device_mode.nSims ; ++i)
  {
    cmpmprx_update_cm_policy_consumption_eligibility_subsc( i, FALSE );
    cmpmprx_print_current_policy(i);
  }
  return;

}

/*===========================================================================

FUNCTION cmpmprx_init

DESCRIPTION
  Initialize the common functionality of Policy Manager Proxy object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmpmprx_init( void )
{

  /* Query feature_mode from policy mgr */
  cmpmprx_init_pm_feature_mode();

  #ifdef CM_DEBUG
  #error code not present
#endif

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Retrieve the initial policy from the Policy Manager. */
  cmpmprx_init_pm_ue_mode();

  /* Initialize is_pm_supsend */
  cmpm_ptr()->is_pm_suspend = FALSE;

  return;
}


/*===========================================================================

FUNCTION cmpmprx_set_policy_chg_flag

DESCRIPTION
  Maintain the bitmask for what all policy configuration items have changed
  since last policy was enforced.

  If PM_CHG_NO_CHANGE passed to the function, clear the bitmask.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void cmpmprx_set_policy_chg_flag(

       sys_modem_as_id_e_type asubs_id,

       policy_change_mask change_value
)
{
  if(asubs_id == SYS_MODEM_AS_ID_1 || asubs_id == SYS_MODEM_AS_ID_2 || asubs_id == SYS_MODEM_AS_ID_3 )
  {
  cmpm_ptr()->policy_changes[asubs_id] |= change_value;
    CM_MSG_HIGH_3("cmpmprx_set_policy_chg_flag() on subs_id = %d change_value = %d policy_changes = %d",asubs_id,change_value,cmpm_ptr()->policy_changes[asubs_id]);
  }
}

/*===========================================================================

FUNCTION cmpmprx_set_policy_chg_flag

DESCRIPTION
  Maintain the bitmask for what all policy configuration items have changed
  since last policy was enforced.

  If PM_CHG_NO_CHANGE passed to the function, clear the bitmask.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void                       cmpmprx_reset_policy_chg_flag(

       sys_modem_as_id_e_type     asubs_id,

       policy_change_mask         change_value
)
{
  if(asubs_id == SYS_MODEM_AS_ID_1 || asubs_id == SYS_MODEM_AS_ID_2 || asubs_id == SYS_MODEM_AS_ID_3 )
  {
  cmpm_ptr()->policy_changes[asubs_id] &= (~change_value);
  CM_MSG_HIGH_3("cmpmprx_reset_policy_chg_flag() on subs_id = %d change_value = %d policy_changes = %d",asubs_id,change_value,cmpm_ptr()->policy_changes[asubs_id]);
  }
}

/*===========================================================================

FUNCTION cmpmprx_is_mask_set

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_is_mask_set(

       sys_modem_as_id_e_type     asubs_id,

       policy_change_mask         check_mask,

       cm_policy_config_s_type   *pm_ptr
)
{
  if( pm_ptr == NULL )
  {
    pm_ptr = cmpm_ptr();
  }

  if (( asubs_id > (int)pm_ptr->device_mode.nSims &&
        asubs_id != SYS_MODEM_AS_ID_MAX )
        ||
        asubs_id == SYS_MODEM_AS_ID_NO_CHANGE
        ||
        asubs_id == SYS_MODEM_AS_ID_NONE
     )
  {
    CM_MSG_HIGH_3("PMPRX: Checking F policy mask 0x%x on subs_id %d nSim %d",check_mask,asubs_id,pm_ptr->device_mode.nSims);
    return FALSE;
  }

  if ( asubs_id == SYS_MODEM_AS_ID_MAX )
  {
    uint8 i=0;
    for (i=0;i<pm_ptr->device_mode.nSims;i++)
    {
      if (pm_ptr->policy_changes[i] & check_mask)
      {
        CM_MSG_HIGH_3("PMPRX: Checking T policy mask 0x%x on subs_id %d policy chg %x",check_mask,asubs_id,pm_ptr->policy_changes[i]);
        return TRUE;
      }
    }
  }
  else
  {
    CM_MSG_HIGH_3("PMPRX: Checking policy mask 0x%x on subs_id %d policy chg %x",check_mask,asubs_id,pm_ptr->policy_changes[asubs_id]);
    return (boolean)(pm_ptr->policy_changes[asubs_id] & check_mask);
  }

  CM_MSG_HIGH_2("PMPRX: Checking F policy mask 0x%x on subs_id %d",check_mask,asubs_id);
  return FALSE;
}

/*===========================================================================

FUNCTION cmpmprx_print_current_policy

DESCRIPTION
  Print the value of the configuration items of the current policy.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void cmpmprx_print_current_policy(sys_modem_as_id_e_type asubs_id)
{
  cm_policy_config_s_type *pm_ptr           = cmpm_ptr();
  if( (asubs_id > SYS_MODEM_AS_ID_NONE) && 
     (asubs_id < SYS_MODEM_AS_ID_NO_CHANGE) &&
     (asubs_id < POLICY_ITEM_ARRAY_SIZE)
  )
  {

  CM_MSG_HIGH_3("PMPRX: RATs supported = 0x%x, Service scan mode = %d, UE mode = %d",
                                  pm_ptr->current_policy[asubs_id].policy_mode_capability,
                                  pm_ptr->current_policy[asubs_id].service_scan_mode,
                                  pm_ptr->current_policy[asubs_id].ue_operation_mode);

  CM_MSG_HIGH_3("PMPRX: BANDs(MSB) GW = 0x%08x, LTE = 0x%08x, TDS = 0x%08x",
                          QWORD_HIGH(pm_ptr->current_policy[asubs_id].policy_band_capability),
                          QWORD_HIGH(pm_ptr->current_policy[asubs_id].policy_lte_band_capability),
                          QWORD_HIGH(pm_ptr->current_policy[asubs_id].policy_tds_band_capability) );

  CM_MSG_HIGH_3("PMPRX: BANDs(LSB) GW = 0x%08x, LTE = 0x%08x, TDS = 0x%08x",
                          QWORD_LOW(pm_ptr->current_policy[asubs_id].policy_band_capability),
                          QWORD_LOW(pm_ptr->current_policy[asubs_id].policy_lte_band_capability),
                          QWORD_LOW(pm_ptr->current_policy[asubs_id].policy_tds_band_capability) );

  CM_MSG_HIGH_3("PMPRX: Change flags = %d, CM Eligibility to consume subsc = %d, asubs_id = %d",
                                                       pm_ptr->policy_changes[asubs_id],
                                                       pm_ptr->is_cm_eligible[asubs_id].subsc,
                                                       asubs_id);
  }
}

/*===========================================================================

FUNCTION cmpmprx_retrieve_policy_item_data

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static policyman_status_t        cmpmprx_retrieve_policy_item_data
(
       cm_policy_config_s_type    *pm_ptr,

       policyman_item_collection_t const  *policy_item,

       policy_change_mask         *change_mask,

       policy_change_mask         *device_change_mask
)
{
  policyman_status_t  return_val = POLICYMAN_STATUS_ERR;
  sys_modem_as_id_e_type sub_id = SYS_MODEM_AS_ID_NONE;
  uint8 i=0, j=0;
  policyman_item_t const  *pItem;

  CM_ASSERT( pm_ptr != NULL );
  #ifndef CM_DEBUG
  CM_ASSERT( policy_item != NULL );
  #endif
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i=0;i<policy_item->numItems;i++)
  {
    pItem = policy_item->pItems[i];
    switch (policyman_item_get_id_msim(pItem,&sub_id))
    {
     case POLICYMAN_ITEM_RAT_CAPABILITY:
       if(sub_id > SYS_MODEM_AS_ID_NONE && 
         sub_id < SYS_MODEM_AS_ID_NO_CHANGE &&
         sub_id < POLICY_ITEM_ARRAY_SIZE
       )
       {
         return_val = policyman_get_rat_capability( pItem,
                        &(pm_ptr->current_policy[sub_id].policy_mode_capability));

         if (POLICYMAN_SUCCEEDED(return_val))
         {
           change_mask[sub_id] |= PM_CHG_MODE_CAP;
           CM_MSG_HIGH_2("PM -> PMPRX: mode_cap_chgd: asubs_id=%d, mode_cap=%d",
                            sub_id,
                            pm_ptr->current_policy[sub_id].policy_mode_capability);
         }
       }
       
       break;
     /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
     case POLICYMAN_ITEM_RF_BANDS:
       if(sub_id > SYS_MODEM_AS_ID_NONE && 
         sub_id < SYS_MODEM_AS_ID_NO_CHANGE &&
         sub_id < POLICY_ITEM_ARRAY_SIZE
       )
       {
         return_val = policyman_get_rf_bands(pItem,
                                           &(pm_ptr->current_policy[sub_id].policy_band_capability),
                                           &(pm_ptr->current_policy[sub_id].policy_lte_band_capability),
                                           &(pm_ptr->current_policy[sub_id].policy_tds_band_capability));

         if ( POLICYMAN_SUCCEEDED(return_val))
         {
           change_mask[sub_id] |= PM_CHG_BAND_CAP;
  
           if ( !POLICYMAN_SUCCEEDED(policyman_get_rf_bands_force( pItem,
                                                                 &(pm_ptr->is_force_registration)))
            )
           {
             pm_ptr->is_force_registration = FALSE;
           }
  
           CM_MSG_HIGH_2("PM -> PMPRX: band_cap_chgd: asubs_id=%d, is_force_registration=%d", 
                            sub_id, pm_ptr->is_force_registration);
         }
       }
       break;
     /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
     case POLICYMAN_ITEM_SVC_MODE:
       if(sub_id > SYS_MODEM_AS_ID_NONE &&
         sub_id < SYS_MODEM_AS_ID_NO_CHANGE &&
         sub_id < POLICY_ITEM_ARRAY_SIZE
       )
       {
         return_val = policyman_get_svc_mode( pItem,
                                              &(pm_ptr->current_policy[sub_id].service_scan_mode));

         if ( POLICYMAN_SUCCEEDED(return_val))
         {
           change_mask[sub_id] |= PM_CHG_SVC_SCAN;
           CM_MSG_HIGH_2("PM -> PMPRX: srv_mode_chgd: asubs_id=%d, srv_mode=%d",
                            sub_id,
                            pm_ptr->current_policy[sub_id].service_scan_mode);
         }
       }
       break;
     /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
     case POLICYMAN_ITEM_UE_MODE:
         if(sub_id > SYS_MODEM_AS_ID_NONE &&
           sub_id < SYS_MODEM_AS_ID_NO_CHANGE &&
           sub_id < POLICY_ITEM_ARRAY_SIZE
         )
         {
           return_val = policyman_get_ue_mode( pItem,
                                               &(pm_ptr->current_policy[sub_id].ue_operation_mode));

           if ( POLICYMAN_SUCCEEDED(return_val))
           {
             change_mask[sub_id] |= PM_CHG_UE_MODE;
             CM_MSG_HIGH_2("PM -> PMPRX: ue_mode_chgd: asubs_id=%d, ue_mode=%d",
                              sub_id,
                              pm_ptr->current_policy[sub_id].ue_operation_mode);
           }
         }

		 break;
     /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
     case POLICYMAN_ITEM_DEVICE_CONFIGURATION:
     {
       size_t                new_nSims                = 0;
       size_t                new_nMaxActive           = 0;
	   size_t                new_nMaxActiveData       = 0;

       if(!POLICYMAN_SUCCEEDED(policyman_device_config_num_sims(pItem, &(new_nSims)))
         ||
		 !POLICYMAN_SUCCEEDED(policyman_device_config_max_active_data(pItem, &(new_nMaxActiveData)))
		 ||
         !POLICYMAN_SUCCEEDED(policyman_device_config_max_active(pItem, &(new_nMaxActive)))
        )
       {
         return_val = POLICYMAN_STATUS_ERR;
         CM_MSG_HIGH_0("MaxSIM numSIM nMaxActiveData read error");
       }
       else
       {
         return_val = POLICYMAN_STATUS_SUCCESS;
       }

       if (POLICYMAN_SUCCEEDED(return_val))
       {
         CM_MSG_HIGH_3("PM -> PMPRX: device_mode_chgd: nSIM %d nMaxActive %d nMaxActiveData %d",
                          new_nSims, new_nMaxActive, new_nMaxActiveData);
  
         if ( device_change_mask != NULL )
         {
           if ( pm_ptr->device_mode.nSims != new_nSims ||
                pm_ptr->device_mode.nMaxActive != new_nMaxActive )
           {
             *device_change_mask = PM_CHG_DEVICE_MODE;
           }

		 if(pm_ptr->device_mode.nMaxActiveData != new_nMaxActiveData) 
		 {
		  *device_change_mask |= PM_CHG_MAX_ACTIVE_DATA;	 
		 }
         }

         pm_ptr->device_mode.nSims        = new_nSims;
         pm_ptr->device_mode.nMaxActive   = new_nMaxActive;
         pm_ptr->device_mode.nMaxActiveData   = new_nMaxActiveData;
		 CM_MSG_HIGH_1("retrieve policy item: nMaxActiveData value = %d",new_nMaxActive); 


         cmpmprx_configure_num_policies(pm_ptr,new_nSims);
       }
     }
     break;
     /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
     case POLICYMAN_ITEM_RAT_ACQ_ORDER:          
      if(sub_id > SYS_MODEM_AS_ID_NONE &&
         sub_id < SYS_MODEM_AS_ID_NO_CHANGE &&
         sub_id < POLICY_ITEM_ARRAY_SIZE
         )
       {
         CM_MSG_HIGH_1("PM -> PMPRX: policy changes for sub 0:%x", pm_ptr->policy_changes[0]);
         CM_MSG_HIGH_1("PM -> PMPRX: policy changes for sub 1:%x", pm_ptr->policy_changes[1]);

         return_val = policyman_get_rat_order(pItem,
                                           &(pm_ptr->current_policy[sub_id].acq_order_pref));
                                           
         if ( POLICYMAN_SUCCEEDED(return_val))
         {
           change_mask[sub_id] |= PM_CHG_RAT_ACQ_ORDER;

           for(j=0;j<pm_ptr->current_policy[sub_id].acq_order_pref.num_rat; j++)
           {
            CM_MSG_HIGH_2("PM -> PMPRX: rat_acq_chgd sub %d: list elements=%d",
                          sub_id, pm_ptr->current_policy[sub_id].acq_order_pref.acq_order[j]);
           }
         }
       }
     break;

     default:
       return_val = POLICYMAN_STATUS_SUCCESS;
    }
    CM_MSG_HIGH_3("Retrieve policy data pitem %d sub id %d change mask %x",pItem, sub_id, change_mask[sub_id]); 
  }

  CM_MSG_HIGH_1("PMPRX: retrieve policy item data returned %d",return_val);
  return return_val;
}

/*===========================================================================

FUNCTION cmpmprx_fetch_mode_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_fetch_mode_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_sys_mode_mask_e_type   *mode_capability_buffer
)
{
  if ( !cmpmprx_is_mask_set( asubs_id, PM_CHG_MODE_CAP, cmpm_ptr() )||
       mode_capability_buffer == NULL       ||
       !cmpmprx_read_cm_policy_consumption_eligibility(asubs_id)
     )
  {
    CM_MSG_MED_1("PMPRX -> CM: NO policyman mode capability update, asubs_id %d",asubs_id);
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_HIGH_2("PMPRX -> CM: Mode capability 0x%x, asubs_id %d",
                 cmpm_ptr()->current_policy[asubs_id].policy_mode_capability, asubs_id);

  *mode_capability_buffer = cmpm_ptr()->current_policy[asubs_id].policy_mode_capability;
  cmpmprx_reset_policy_chg_flag( asubs_id, PM_CHG_MODE_CAP );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return TRUE;
}

/*===========================================================================

FUNCTION cmpmprx_get_mode_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_get_mode_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_sys_mode_mask_e_type   *mode_capability_buffer
)
{

   if(asubs_id == SYS_MODEM_AS_ID_1 || asubs_id == SYS_MODEM_AS_ID_2 || asubs_id == SYS_MODEM_AS_ID_3 )
   {
     *mode_capability_buffer = cmpm_ptr()->current_policy[asubs_id].policy_mode_capability;
   }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return TRUE;
}


/*===========================================================================

FUNCTION cmpmprx_get_band_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_get_band_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_band_mask_e_type       *band_capability_buffer,

       sys_band_mask_e_type       *lte_band_capability_buffer,

       sys_band_mask_e_type       *tds_band_capability_buffer,

       boolean                    *force_registration
)
{
  if ( !cmpmprx_is_mask_set( asubs_id,PM_CHG_BAND_CAP, cmpm_ptr() )   ||
       band_capability_buffer     == NULL     ||
       lte_band_capability_buffer == NULL     ||
       tds_band_capability_buffer == NULL     ||
       force_registration         == NULL     ||
       !cmpmprx_read_cm_policy_consumption_eligibility(asubs_id)
     )
  {
    CM_MSG_MED_0("PMPRX -> CM: NO policyman band capability update");
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_HIGH_3("PMPRX -> CM(MSB): BANDs GW = 0x%08x, LTE = 0x%08x, TDS = 0x%08x",
                      QWORD_HIGH(cmpm_ptr()->current_policy[asubs_id].policy_band_capability),
                      QWORD_HIGH(cmpm_ptr()->current_policy[asubs_id].policy_lte_band_capability),
                      QWORD_HIGH(cmpm_ptr()->current_policy[asubs_id].policy_tds_band_capability) );
  CM_MSG_HIGH_3("PMPRX -> CM(LSB): BANDs GW = 0x%08x, LTE = 0x%08x, TDS = 0x%08x",
                      QWORD_LOW(cmpm_ptr()->current_policy[asubs_id].policy_band_capability),
                      QWORD_LOW(cmpm_ptr()->current_policy[asubs_id].policy_lte_band_capability),
                      QWORD_LOW(cmpm_ptr()->current_policy[asubs_id].policy_tds_band_capability) );

  CM_MSG_MED_1("PMPRX -> CM: force_registration = %d",
                      cmpm_ptr()->is_force_registration);

  *band_capability_buffer     = cmpm_ptr()->current_policy[asubs_id].policy_band_capability;
  *lte_band_capability_buffer = cmpm_ptr()->current_policy[asubs_id].policy_lte_band_capability;
  *tds_band_capability_buffer = cmpm_ptr()->current_policy[asubs_id].policy_tds_band_capability;
  *force_registration         = cmpm_ptr()->is_force_registration;

  cmpmprx_reset_policy_chg_flag( asubs_id, PM_CHG_BAND_CAP );
  cmpm_ptr()->is_force_registration = FALSE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return TRUE;
}

/*===========================================================================

FUNCTION cmpmprx_is_ok_to_enforce_policy

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmpmprx_is_ok_to_enforce_policy(

       cm_policy_config_s_type   *policy_ptr,

       sys_modem_as_id_e_type     asubs_id

)
{
  CM_MSG_HIGH_1("Entering into cmpmprx_is_ok_to_enforce_policy()  subs_id = %d ",asubs_id);
  /* If Subscription not available
  ** or operating mode not online
  ** or no policy change (not expected)
  */
  if ( !cmpmprx_read_cm_policy_consumption_eligibility(asubs_id)||
       !cmpmprx_is_mask_set( asubs_id, PM_CHG_ALL, policy_ptr ) )
  {
    CM_MSG_HIGH_0( "Policy Not forced, due to Subsc not avail or No policy changed");
    return FALSE;
  }

  /* Check the UE Operation Mode. If the UE is OFFLINE,
  ** return FALSE. Mode OFFLINE_CDMA and OFFLINE_AMPS need
  ** to be checked here for Legecy compatibility because
  ** QXDM sends down OFFLINE as OFFLINE_CDMA */
  if( cmph_ptr()->oprt_mode == SYS_OPRT_MODE_OFFLINE ||
      cmph_ptr()->oprt_mode == SYS_OPRT_MODE_OFFLINE_CDMA ||
      cmph_ptr()->oprt_mode == SYS_OPRT_MODE_OFFLINE_AMPS )
  {
    CM_MSG_HIGH_0( "UE is OFFLINE. Cannot Enforce Policy");
    return FALSE;
  }

  /* If ONLINE was deferred, policy update should also be deferred */
  if(cmph_ptr()->oprt_mode_send_time != CMPH_MAX_UPTIME)
  {
    CM_MSG_HIGH_2("ONLINE deferred till %d, so policy for as_id %d deferred",
                  cmph_ptr()->oprt_mode_send_time, asubs_id);
    return FALSE;
  }

  /* Evaluate individual policy items.*/
  /* UE mode first as it may cause change in number of active stacks. */
  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_UE_MODE, policy_ptr ) )
  {
    if ( cmtask_orig_para_is_obj_on_any_q(CM_ACT_TYPE_EMERG_CALL)||
         cmph_is_in_emergency_cb() )
    {
      CM_MSG_HIGH_0("Policy Not forced, due to Emerg call or UE in ECBM ");
      return FALSE;
    }

    /* If service scan mode is changed, active voice calls wont matter as they
    ** will be put below PM object.
    */
    if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_SVC_SCAN, policy_ptr ) )
    {
      CM_MSG_HIGH_0("Handling for PM_CHG_SVC_SCAN ");
      return TRUE;
    }

    /* Voice call in traffic. */
    if ( cmcall_find_active_voice_call_1(asubs_id) != CM_CALL_ID_INVALID )
    {
      CM_MSG_HIGH_0("Policy Not forced, due to active voice Call ");
      return FALSE;
    }

    /* No Voice calls in traffic or emergency calls. End all the others and
    ** proceed with the UE mode change as per policy.
    */
    cmcall_clean_voice_call_from_q();

	
    /* No SMS calls in traffic. End all the others and
    ** proceed with the UE mode change as per policy.
    */
    cmcall_clean_sms_call_from_q();

    /* No CS Data calls in traffic. End all the others and
    ** proceed with the UE mode change as per policy.
    */
    cmcall_clean_cs_data_call_from_q();

    return TRUE;
  }

  /* Service scan mode changed, UE mode not changed. */
  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_SVC_SCAN, policy_ptr ) )
  {
    return TRUE;
  }

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_MODE_CAP, policy_ptr ) )
  {
    return TRUE;
  }

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_BAND_CAP, policy_ptr ) )
  {
    return TRUE;
  }

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_RAT_ACQ_ORDER, policy_ptr) )
  {
    return TRUE;
  }

  CM_MSG_HIGH_0("Policy Not forced, No conditions meets..");
  return FALSE; /* Code execution should not come here. */
}

/*===========================================================================

FUNCTION cmpmprx_enforce_current_policy

DESCRIPTION
  Process the policy currently in cmpm_local.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void                       cmpmprx_enforce_current_policy(

       sys_modem_as_id_e_type     asubs_id

   )
{
  cm_policy_config_s_type *pm_ptr = cmpm_ptr();
  cmph_s_type *ph_ptr = cmph_ptr();
  boolean ue_mode_chgd = FALSE;
  

  CM_MSG_HIGH_2("cmpmprx_enforce_current_policy asubs_id = %d ss = %d ",asubs_id, cmph_map_subs_stack_to_ss(asubs_id,0));
	  
  cmpmprx_print_current_policy(asubs_id);

  if ( !cmpmprx_is_ok_to_enforce_policy( pm_ptr, asubs_id ) )
  {
    return;
  }

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_RAT_ACQ_ORDER, pm_ptr ) && 
       (asubs_id == cmph_map_cm_ss_to_as_id(cmph_map_sd_ss_type_to_cm_ss_type(cmmsimc_state_ptr()->ss_with_mm)))
	 )
  {
    CM_MSG_HIGH_1("PM->PMPRX :updating phone object:%d", asubs_id);
    ph_ptr->rat_acq_order_pref = pm_ptr->current_policy[asubs_id].acq_order_pref;
    cmpmprx_reset_policy_chg_flag(asubs_id,PM_CHG_RAT_ACQ_ORDER);
  }

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_MODE_CAP, pm_ptr ) )
  {
    CM_MSG_HIGH_1("PMPRX: mode_cap changed on sub:%d", asubs_id);
    cmph_sp_subs_info_update();
  }

  #if defined(FEATURE_LTE_TO_1X) && defined(FEATURE_MMODE_SC_SVLTE)
  if(cmph_is_subs_feature_mode_srlte(asubs_id) &&
     cmpmprx_is_mask_set(asubs_id, PM_CHG_UE_MODE, pm_ptr))
  {
    cmsoa_srlte_ue_mode_update(pm_ptr->current_policy[asubs_id].ue_operation_mode);
    ue_mode_chgd = TRUE;
  }
  #endif

  if ( cmpmprx_is_mask_set( asubs_id, PM_CHG_SVC_SCAN, pm_ptr ) )
  {
    cmpmprx_enforce_service_scan_mode(asubs_id);
	cmph_sp_subs_info_update();
  }
  else if(cmph_map_subs_stack_to_ss(asubs_id,0) == CM_SS_MAIN)
  {

    /* If forcing is to be done on MAIN/HYBR_1/HYBR_2 combined */

    cm_orig_q_s_type  *main_stack_top = cmtask_orig_para_get_top(CM_SS_MAIN);
    if((main_stack_top != NULL) && 
       (main_stack_top->act_type == CM_ACT_TYPE_PH_OBJ ||
        main_stack_top->act_type == CM_ACT_TYPE_POLICYMAN_RESTRICT) )
    {
      (void) cmph_force_pref_on_the_fly( CM_SS_MAX,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->main_stack_info.pref_info.pref_term,
                                         ph_ptr->main_stack_info.pref_info.mode_pref,
                                         ph_ptr->main_stack_info.pref_info.band_pref,
                                         ph_ptr->main_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->main_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->main_stack_info.pref_info.prl_pref,
                                         ph_ptr->main_stack_info.pref_info.roam_pref,
                                         ph_ptr->main_stack_info.pref_info.hybr_pref,
                                         ph_ptr->main_stack_info.pref_info.plmn,
                                         ph_ptr->main_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->main_stack_info.pref_info.acq_order_pref,
                                         ph_ptr->main_stack_info.pref_info.network_sel_mode_pref,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_ACT_START,
                                         TRUE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->main_stack_info.pref_info.csg_id,
                                         ph_ptr->main_stack_info.pref_info.csg_rat);
    }
    else
    {
      (void) cmph_force_pref_on_the_fly( CM_SS_MAX,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->main_stack_info.pref_info.pref_term,
                                         ph_ptr->main_stack_info.pref_info.mode_pref,
                                         ph_ptr->main_stack_info.pref_info.band_pref,
                                         ph_ptr->main_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->main_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->main_stack_info.pref_info.prl_pref,
                                         ph_ptr->main_stack_info.pref_info.roam_pref,
                                         ph_ptr->main_stack_info.pref_info.hybr_pref,
                                         ph_ptr->main_stack_info.pref_info.plmn,
                                         ph_ptr->main_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->main_stack_info.pref_info.acq_order_pref,
                                         ph_ptr->main_stack_info.pref_info.network_sel_mode_pref,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->main_stack_info.pref_info.csg_id,
                                         ph_ptr->main_stack_info.pref_info.csg_rat);
    }
  }
  
  #ifdef FEATURE_MMODE_DUAL_SIM
      if (!cmph_is_sxlte() && cmph_is_msim() && (cmph_map_subs_stack_to_ss(asubs_id,0) == CM_SS_HYBR_2))
      {
             CM_MSG_HIGH_0("DUAL_SIM, PREF forced on HYBR2 ");   
	  

               (void)cmph_force_pref_on_the_fly_ss(CM_SS_HYBR_2,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->hybr_2_stack_info.pref_info.network_sel_mode_pref,
                                         &(CMPH_HYBR_2_PREF(ph_ptr)),
                                         #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
                                         &(CMPH_HYBR_3_PREF(ph_ptr)),
                                         #endif
                                         CM_OTASP_ACT_CODE_NONE,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_rat);	
	  }
 #endif	  
 
 #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
      if ((cmph_is_msim() || cmph_is_sxlte()) && (cmph_map_subs_stack_to_ss(asubs_id,0) == CM_SS_HYBR_3))
	  {
 
                (void)cmph_force_pref_on_the_fly_ss(CM_SS_HYBR_3,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_PH_OBJ,
                                         ph_ptr->hybr_3_stack_info.pref_info.network_sel_mode_pref,
                                         &(CMPH_HYBR_2_PREF(ph_ptr)),
                                         #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
                                         &(CMPH_HYBR_3_PREF(ph_ptr)),
                                         #endif
                                         CM_OTASP_ACT_CODE_NONE,
                                         (cm_act_id_type)ph_ptr,
                                         CM_ACT_UPDATE_REAS_USER,
                                         FALSE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_rat);



        CM_MSG_HIGH_0("TRIPLE_SIM or SXLTE_G , PREF forced on HYBR3 ");
										 
      }
 #endif										 
#ifdef FEATURE_DOMAIN_SELECTION
 if(cmph_is_subs_feature_mode_srlte(asubs_id) &&
    ue_mode_chgd)
 {
   cmmsc_state_machine_s_type *state_machine = cmmsimc_state_machine_ptr(asubs_id);
   
   if(state_machine->stack_common_info.ue_mode
                 == SYS_UE_MODE_1XSRLTE_CSFB_PREFERRED)
   {
      /* For SRLTE, when we receive ue mode change indication from PM with new 
      ** ue mode=1xsrlte_csfb_pref(CSFB->SRLTE_CSFB_PREF transition) and we 
      ** are already in stable 1x service, we should perform c2k switch from single to
      ** dual stack after ue mode change has been processed.
      ** It can happen in below scenario:
      ** 1.ue_mode=CSFB, and acquired home 1x service
      ** 2.Switch to dual stack wasn't done here was ue_mode=CSFB.
      ** 3.Indicated 1x service to PM.
      ** 4.PM sends ue mode change from CSFB to CSFB_PREF.
      ** 5.we should perform c2k switch from sinlge->dual stack here.
      ** */
      #if (defined(FEATURE_LTE_TO_1X) && defined(FEATURE_MMODE_SC_SVLTE)) || defined(FEATURE_MMODE_DYNAMIC_SV_OPERATION_SWITCH)
        cmsoa_check_and_switch_fromc2k_to_srlte(CM_SS_MAIN);
      #endif
     
      if(cmsds_ptr()->pending_uemode_cmsds_skipped == TRUE)
      {
     sd_si_info_s_type     si_info;
     sd_si_info_ptr_get( SD_SS_MAIN, &si_info);
     CM_MSG_HIGH_2("resume dom sel srv %d mode %d",si_info.srv_status,si_info.mode);
     if(si_info.srv_status == SYS_SRV_STATUS_SRV && si_info.mode == SYS_SYS_MODE_LTE)
     {
       cmsds_process_srv_ind(CM_SS_MAIN, &si_info);
     }
   }    
 }
 }
#endif 
  
}

/*===========================================================================

FUNCTION cmpmprx_update_curr_policy

DESCRIPTION
  Update cmpm_local with new policy retrieved from Policy Manager..

DEPENDENCIES
  cmpm_init()

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void                              cmpmprx_update_curr_policy(

       cm_policy_config_s_type    *local_policy

)
{
  if ( local_policy != NULL )
  {
    cm_policy_config_s_type *curr_policy = cmpm_ptr();

    *curr_policy = *local_policy;
  }
  return;
}


/*===========================================================================

FUNCTION cmpmprx_process_retrieved_policy

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                       cmpmprx_process_retrieved_policy(

       sys_modem_as_id_e_type     asubs_id,

       cm_policy_config_s_type    *local_policy,

       policy_change_mask         local_mask
)

{
  if ( local_policy != NULL )
  {
    CM_MSG_HIGH_3("cmpmprx_process_retrieved_policy() on subs_id = %d local_mask = %d policy_changes = %d",asubs_id,local_mask,cmpm_ptr()->policy_changes[asubs_id]);
    local_policy->policy_changes[asubs_id] |= local_mask;
    cmpmprx_update_curr_policy(local_policy);
    cmpmprx_enforce_current_policy(asubs_id);
  }
}

/*===========================================================================

FUNCTION cmpmprx_policy_chg_ind_proc

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static boolean                    cmpmprx_process_msgr_msg(

       cm_policyman_cfg_s         *rpt_ptr,

       cm_policy_config_s_type    *local_policy
)
{
  int                     i                   = 0;
  msgr_attach_s           *pAttach            = NULL;
  boolean                 is_new_policy_valid = TRUE;
  policy_change_mask      local_mask[3]       = {0,0,0};
  policy_change_mask      device_local_mask   = 0;
  cm_policy_config_s_type *curr_policy        = cmpm_ptr();
  boolean                 is_any_policy_chg   = FALSE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for (i = 0 ; i < msgr_get_num_attach(&(rpt_ptr->msg_hdr)) ; ++i)
  {
    policyman_item_collection_t const *policy_item = NULL;

    pAttach = msgr_get_attach(&(rpt_ptr->msg_hdr), i);
    if (pAttach == NULL || pAttach->type != MSGR_ATTACH_REFCNT)
    {
      CM_MSG_HIGH_1("PMPRX: Invalid attachment %d.Abort new policy.",i);
      is_new_policy_valid = FALSE;
      break;
    }

    policy_item = pAttach->data.refcnt.refcnt_obj_ptr;
    if(!POLICYMAN_SUCCEEDED(cmpmprx_retrieve_policy_item_data( local_policy, policy_item, local_mask, &device_local_mask )))
    {
      CM_MSG_HIGH_1("PMPRX: Unable to retrieve data for attachment %d",i);
      is_new_policy_valid = FALSE;
      break;
    }
  } /* for() */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  is_any_policy_chg = (device_local_mask != 0)?TRUE:FALSE;
  
  for (i=0;i<(int)local_policy->device_mode.nSims;i++)
  {
    if(i<3)
    {
      is_any_policy_chg |= local_mask[i];
    }
	else
	{
	  break;
	}
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if ( !is_new_policy_valid                   ||
       !is_any_policy_chg                      ||
       (device_local_mask == 0 &&
        (cmpmprx_compare_policies(local_policy,curr_policy) == 0)))
  {
    CM_MSG_HIGH_3("PMPRX: Discard new policy valid = %d, is_any_policy_chg = %d device_local_mask %d",
                                            is_new_policy_valid,is_any_policy_chg,device_local_mask);
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /*Copy device level policy mask   */
  local_policy->device_policy_changes = device_local_mask;

  /* Copy change mask for each SIM */
  for (i=0;i<(int)local_policy->device_mode.nSims;i++)
  {
     local_policy->policy_changes[i] |= local_mask[i];
     CM_MSG_HIGH_2("Msgr policy[%d] mask = %x",i,local_policy->policy_changes[i]); 
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return TRUE;
}

/*===========================================================================

FUNCTION cmpmprx_policy_chg_ind_proc

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmpmprx_policy_chg_ind_proc(
    cm_policyman_cfg_s          *rpt_ptr
     /* Pointer to the report sent by Message Router */
   )
{
  cm_policy_config_s_type *local_policy = NULL;
  uint8 i=0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_HIGH_0("PM -> PMPRX: policy_chg_ind");

  if ( rpt_ptr == NULL )
  {
    CM_MSG_HIGH_0("PMPRX: Incoming policy message NULL");
    return;
  }

  /* Allocate local buffer for new memory and initialize it to current policy.
  */
  local_policy = (cm_policy_config_s_type *) cm_mem_malloc(sizeof(cm_policy_config_s_type));
  if ( local_policy == NULL )
  {
    CM_MSG_HIGH_0("PMPRX: unable to process policy as cannot allocate memory");
    return;
  }

  *local_policy = *(cmpm_ptr());
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Retrieve the policy data into a local buffer. If all the policy items in
  ** the new policy are successfully retrieved, only then update the current
  ** policy and attempt to enforce it.
  */
  if (cmpmprx_process_msgr_msg(rpt_ptr,local_policy))
  {
    cmpmprx_update_curr_policy(local_policy);
  }

  cm_mem_free(local_policy);

  policyman_msgr_attachments_release(&(rpt_ptr->msg_hdr));

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Enforce device level policy */
  if(cmpm_ptr()->device_policy_changes != 0)
  {
    cmpmprx_enforce_device_policy();
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Loop thru the subs_ids and see if any policy changed. If yes then enforce*/
  /* If any policy item has changed, attempt to enforce the new policy. */
  for (i=0;i<cmpm_ptr()->device_mode.nSims;i++)
  {
    if ( cmpmprx_is_mask_set( i, PM_CHG_ALL, cmpm_ptr() ) )
  {
      cmpmprx_enforce_current_policy(i);
    }
  }
  return;
}

/*===========================================================================

FUNCTION cmpmprx_update_cm_policy_consumption_eligibility

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void   cmpmprx_update_cm_policy_consumption_eligibility_subsc(

       sys_modem_as_id_e_type     asubs_id,

       boolean                    new_value
)
{
  boolean prev_eligibility = cmpmprx_read_cm_policy_consumption_eligibility(asubs_id);
  cm_policy_config_s_type *curr_policy = cmpm_ptr();
  cmph_s_type * ph_ptr = cmph_ptr();

  curr_policy->is_cm_eligible[asubs_id].subsc = new_value;

  CM_MSG_MED_1("PMPRX: Updating CM eligibility to consume policy: %d",
                                      curr_policy->is_cm_eligible[asubs_id].subsc);

  if ( cmpmprx_read_cm_policy_consumption_eligibility(asubs_id) &&
       cmpmprx_is_mask_set( asubs_id, PM_CHG_RAT_ACQ_ORDER, cmpm_ptr() ) &&
       (asubs_id == cmph_map_cm_ss_to_as_id(cmph_map_sd_ss_type_to_cm_ss_type(cmmsimc_state_ptr()->ss_with_mm)))
     )
  {
    CM_MSG_HIGH_1("CM_DEBUG PMPRX: rat acq changed on sub:%d", asubs_id);
    ph_ptr->rat_acq_order_pref = curr_policy->current_policy[asubs_id].acq_order_pref;
    cmpmprx_reset_policy_chg_flag(asubs_id,PM_CHG_RAT_ACQ_ORDER);
  }

  /* There are pending policy changes and CM just became eligible to consume
  ** Enforce service scan. All others will be read by subscription change.
  */
  if ( !prev_eligibility &&
       cmpmprx_read_cm_policy_consumption_eligibility(asubs_id) &&
       cmpmprx_is_mask_set( asubs_id, PM_CHG_SVC_SCAN, cmpm_ptr() ) )
  {
    cmpmprx_enforce_service_scan_mode(asubs_id);
  }

}

/*===========================================================================

FUNCTION cmpmprx_update_cm_policy_consumption_eligibility

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_enforce_policy_oprt_mode(

       sys_modem_as_id_e_type     asubs_id
)
{
  cm_policy_config_s_type *curr_policy = cmpm_ptr();
  uint8 i=0;
  boolean is_no_op = TRUE;
  /*
  **Loop thru all the subs ids and check for all.
  */
  if (asubs_id == SYS_MODEM_AS_ID_MAX)
  {
    for (i=0;i<curr_policy->device_mode.nSims;i++)
    {
      if ( curr_policy->current_policy[i].service_scan_mode != POLICYMAN_SVC_MODE_FULL )
      {
        cmpmprx_set_policy_chg_flag(i,PM_CHG_SVC_SCAN);
        is_no_op = FALSE;

        /* There are pending policy changes and CM just became eligible to consume*/
        if ( cmpmprx_read_cm_policy_consumption_eligibility(i) &&
             cmpmprx_is_mask_set( i, PM_CHG_ALL, cmpm_ptr() ) )
  {
          cmpmprx_enforce_current_policy(i);
        }
      }
    }
  }
  else
  {
    if ( curr_policy->current_policy[asubs_id].service_scan_mode != POLICYMAN_SVC_MODE_FULL )
    {
      cmpmprx_set_policy_chg_flag(asubs_id,PM_CHG_SVC_SCAN);
      is_no_op = FALSE;

  /* There are pending policy changes and CM just became eligible to consume*/
      if ( cmpmprx_read_cm_policy_consumption_eligibility(asubs_id) &&
           cmpmprx_is_mask_set( asubs_id, PM_CHG_ALL, cmpm_ptr() ) )
  {
        cmpmprx_enforce_current_policy(asubs_id);
      }
    }
  }

  return (!is_no_op);
}

/*===========================================================================

FUNCTION cmpmprx_notify_call_end - remove_feature

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                       cmpmprx_notify_call_end(

       sys_modem_as_id_e_type     asubs_id,

       boolean is_emerg
)
{
  if ( is_emerg == TRUE && cmpm_ptr()->is_pm_suspend)
  {
    CM_MSG_HIGH_0("PMPRX: resume PM");
    policyman_resume();
    cmpm_ptr()->is_pm_suspend = FALSE;
  }

  if ( cmpmprx_read_cm_policy_consumption_eligibility(asubs_id) &&
       cmpmprx_is_mask_set( asubs_id, PM_CHG_UE_MODE, cmpm_ptr() )
     )
  {
    cmpmprx_enforce_current_policy(asubs_id);
  }
}

/*===========================================================================

FUNCTION cmpmprx_notify_emg_call_start

DESCRIPTION
  The function is called to notify cmpmprx that emergency call has started.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void  cmpmprx_notify_emg_call_start()
{
  cm_policy_config_s_type *pm_ptr = cmpm_ptr();
  
  /* Suspend Policy Manager so it will not process any rules for the duration
  ** of the emergency call. 
  */
  if (cmph_is_msim()) 
  {
    if (!pm_ptr->is_pm_suspend)
    {
      CM_MSG_HIGH_0("PMPRX: suspend PM");
      policyman_suspend();
      pm_ptr->is_pm_suspend = TRUE;
    }
    else
    {
      CM_MSG_HIGH_0("PMPRX: PM already in suspend");
    }
  }
}

/*===========================================================================

FUNCTION cmpmprx_read_cm_policy_consumption_eligibility

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmpmprx_read_cm_policy_consumption_eligibility(

       sys_modem_as_id_e_type     asubs_id
)
{
  cm_policy_config_s_type *curr_policy = cmpm_ptr();

  //Size of array is 3 , For as_id 1, 2, 3
  if( (asubs_id > SYS_MODEM_AS_ID_NONE) && (asubs_id < SYS_MODEM_AS_ID_NO_CHANGE) )
  {
    return ( curr_policy->is_cm_eligible[asubs_id].subsc );
  }
  else
  {
    return FALSE ;
  }
}

/*===========================================================================

FUNCTION cmpmprx_compute_restricted_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static cm_network_sel_mode_pref_e_type cmpmprx_compute_restricted_mode(

       sys_modem_as_id_e_type       asubs_id,

   cm_network_sel_mode_pref_e_type  net_sel_mode_pref
)
{
  cm_policy_config_s_type *curr_policy = cmpm_ptr();

  CM_MSG_HIGH_2("asubs_id %d, net_sel_mode_pref %d", asubs_id, net_sel_mode_pref);
  
  switch (curr_policy->current_policy[asubs_id].service_scan_mode)
  {
     case POLICYMAN_SVC_MODE_LIMITED:
       return( CM_NETWORK_SEL_MODE_PREF_LIMITED_SRV );

     case POLICYMAN_SVC_MODE_CAMP_ONLY:
     case POLICYMAN_SVC_MODE_ALL:
       {
         cmph_s_type *ph_ptr = cmph_ptr();
         cmph_stack_info_s_type *stack_info;
    
         if(ph_ptr->hybr_3_stack_info.asubs_id == asubs_id)
         {
           stack_info = &(ph_ptr->hybr_3_stack_info);
         }
         else if(ph_ptr->hybr_2_stack_info.asubs_id == asubs_id && !cmph_is_sxlte())
         {
           stack_info = &(ph_ptr->hybr_2_stack_info);
         }
         else
         {
           stack_info = &(ph_ptr->main_stack_info);
         }

         if (stack_info->pref_info.network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL_CAMP_ONLY ||
           stack_info->pref_info.network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL_LIMITED_SRV ||
           stack_info->pref_info.network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL)
         {
           net_sel_mode_pref = CM_NETWORK_SEL_MODE_PREF_MANUAL_CAMP_ONLY;
         }
         else
         {
           net_sel_mode_pref = CM_NETWORK_SEL_MODE_PREF_AUTO_CAMP_ONLY;
         }

       }
       break;
     case POLICYMAN_SVC_MODE_FULL:
     default:
       CM_MSG_HIGH_0("Default case");
       break;
  } /* switch */

  CM_MSG_HIGH_1("cmpmprx_compute_restricted_mode: net_sel_mode_pref %d", net_sel_mode_pref);
  return net_sel_mode_pref;
}

/*===========================================================================

FUNCTION cmpmprx_enforce_service_scan_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                       cmpmprx_enforce_service_scan_mode(

       sys_modem_as_id_e_type     asubs_id
)
{
  cm_policy_config_s_type *curr_policy = cmpm_ptr();
  cmph_s_type             *ph_ptr = cmph_ptr();
  cm_network_sel_mode_pref_e_type rest_net_sel_mode_pref = ph_ptr->main_stack_info.pref_info.network_sel_mode_pref;

  if ( !cmpmprx_is_mask_set( asubs_id, PM_CHG_SVC_SCAN, cmpm_ptr() ) )
  {
    return;
  }

  cmpmprx_reset_policy_chg_flag(asubs_id,PM_CHG_SVC_SCAN);

  if ( curr_policy->current_policy[asubs_id].service_scan_mode == POLICYMAN_SVC_MODE_FULL)
  {
    int                   stack_id = 0;

    for ( stack_id = 0; stack_id < MAX_STACK_PER_SUB; stack_id++ )
    {
      cm_ss_e_type ss = CM_SS_NONE ;
      #if defined(FEATURE_MMODE_TRIPLE_SIM)
	  if( asubs_id == SYS_MODEM_AS_ID_3 )
	  {
        ss = ph_ptr->subs_stack_map[asubs_id][stack_id];
	  }
	  else
	  #endif
	  if( (asubs_id == SYS_MODEM_AS_ID_1) || (asubs_id == SYS_MODEM_AS_ID_2))
	  {
	    
        ss = ph_ptr->subs_stack_map[asubs_id][stack_id];
	  }
	  else
	  {
	    ss = CM_SS_NONE ;		
	  }
	  CM_MSG_HIGH_1("getting, ss=%d", ss);
      if(ss != CM_SS_NONE)
      {
        cm_orig_q_s_type *pm_rest_obj_ptr = cmtask_orig_para_search_act_type( ss,
                                                            CM_ACT_TYPE_POLICYMAN_RESTRICT);
        if( pm_rest_obj_ptr != NULL )
        {
           CM_MSG_HIGH_2("<- full_rat_search mode, asubs_id=%d, ss=%d", asubs_id, ss);
           cmph_delete_update_orig_mode( ss,
                                        (cm_act_id_type) curr_policy,
                                        TRUE );
        }
      }
    }
  }
  else
  {
    rest_net_sel_mode_pref = cmpmprx_compute_restricted_mode( asubs_id, ph_ptr->main_stack_info.pref_info.network_sel_mode_pref );

    if ( cmpmprx_read_cm_policy_consumption_eligibility(asubs_id) )
    {
      cm_ss_e_type ss = cmph_map_subs_stack_to_ss(asubs_id,0);

      if (cmph_is_subs_feature_mode_sglte(asubs_id) || 
          cmph_is_subs_feature_mode_1x_sxlte(asubs_id))
      {
        ss = CM_SS_MAX;
      }

      CM_MSG_HIGH_1("-> full_rat_search mode, asubs_id=%d", asubs_id);
      
	  if(ss == CM_SS_MAIN || ss == CM_SS_MAX)
      {
	    (void) cmph_force_pref_on_the_fly(ss,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_POLICYMAN_RESTRICT,
                                         ph_ptr->main_stack_info.pref_info.pref_term,
                                         ph_ptr->main_stack_info.pref_info.mode_pref,
                                         ph_ptr->main_stack_info.pref_info.band_pref,
                                         ph_ptr->main_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->main_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->main_stack_info.pref_info.prl_pref,
                                         ph_ptr->main_stack_info.pref_info.roam_pref,
                                         ph_ptr->main_stack_info.pref_info.hybr_pref,
                                         ph_ptr->main_stack_info.pref_info.plmn,
                                         ph_ptr->main_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->main_stack_info.pref_info.acq_order_pref,
                                         rest_net_sel_mode_pref,
                                         (cm_act_id_type) curr_policy,
                                         CM_ACT_UPDATE_REAS_ACT_START,
                                         TRUE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->main_stack_info.pref_info.csg_id,
                                         ph_ptr->main_stack_info.pref_info.csg_rat);
   	 }
	#ifdef FEATURE_MMODE_DUAL_SIM
      else if (!cmph_is_sxlte() && cmph_is_msim() && (ss == CM_SS_HYBR_2))
      { 
		 (void) cmph_force_pref_on_the_fly(CM_SS_HYBR_2,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_POLICYMAN_RESTRICT,
                                         ph_ptr->hybr_2_stack_info.pref_info.pref_term,
                                         ph_ptr->hybr_2_stack_info.pref_info.mode_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.prl_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.roam_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.hybr_pref,
                                         ph_ptr->hybr_2_stack_info.pref_info.plmn,
                                         ph_ptr->hybr_2_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->hybr_2_stack_info.pref_info.acq_order_pref,
                                         rest_net_sel_mode_pref,
                                         (cm_act_id_type) curr_policy,
                                         CM_ACT_UPDATE_REAS_ACT_START,
                                         TRUE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_2_stack_info.pref_info.csg_rat);
	}
  #endif

  #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
     else if ((cmph_is_msim()) && (ss == CM_SS_HYBR_3))
	 {
 
		(void) cmph_force_pref_on_the_fly(CM_SS_HYBR_3,
                                         ph_ptr,
                                         SD_SS_PREF_REAS_USER,
                                         CM_ACT_TYPE_POLICYMAN_RESTRICT,
                                         ph_ptr->hybr_3_stack_info.pref_info.pref_term,
                                         ph_ptr->hybr_3_stack_info.pref_info.mode_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.lte_band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.tds_band_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.prl_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.roam_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.hybr_pref,
                                         ph_ptr->hybr_3_stack_info.pref_info.plmn,
                                         ph_ptr->hybr_3_stack_info.pref_info.srv_domain_pref,
                                         CM_OTASP_ACT_CODE_NONE,
                                         ph_ptr->hybr_3_stack_info.pref_info.acq_order_pref,
                                         rest_net_sel_mode_pref,
                                         (cm_act_id_type) curr_policy,
                                         CM_ACT_UPDATE_REAS_ACT_START,
                                         TRUE,
                                         &ph_ptr->rat_acq_order_pref,
                                         CM_DEFAULT_VALUE,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_id,
                                         ph_ptr->hybr_3_stack_info.pref_info.csg_rat);										 
      }
  #endif		
    }
  }
}

/*===========================================================================

FUNCTION cmpmprx_unforce_policy

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                       cmpmprx_unforce_policy(

       sys_modem_as_id_e_type     asubs_id

)
{
  cm_orig_q_s_type        *pm_rest_obj_ptr  =
                          cmtask_orig_para_search_act_type( CM_SS_MAIN,
                                                            CM_ACT_TYPE_POLICYMAN_RESTRICT);

  cmpmprx_set_policy_chg_flag( asubs_id,PM_CHG_ALL );
  if (!cmph_is_subs_feature_mode_sglte(asubs_id))
  {
    cmpmprx_reset_policy_chg_flag(asubs_id,PM_CHG_UE_MODE);
  }

  if ( pm_rest_obj_ptr != NULL )
  {
    (void)cmtask_orig_para_delete(CM_SS_MAIN,pm_rest_obj_ptr,TRUE);
  }
}

/*===========================================================================

FUNCTION cmpmprx_sys_sel_pref_proc

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                         cmpmprx_sys_sel_pref_proc(

       cm_ph_cmd_info_s_type        *cmd_info_ptr,

       cm_mode_pref_e_type          prefered_mode,

       cm_pref_s_type               *hybr_2_pref_info_ptr,
           /* Preferences of the HYBR2 stack */
       
       #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
       cm_pref_s_type               *hybr_3_pref_info_ptr,
           /* Preferences of the HYBR3 stack */
       #endif

       cm_acq_pri_order_pref_s_type *rat_acq_pri_order
)
{
  cm_orig_q_s_type        *pm_rest_obj_ptr  =
                          cmtask_orig_para_search_act_type( cmph_map_as_id_to_cm_ss(cmd_info_ptr->cmd_subs),
                                                            CM_ACT_TYPE_POLICYMAN_RESTRICT);

  cm_network_sel_mode_pref_e_type net_sel_mode_pref = 
                cmpmprx_compute_restricted_mode( cmd_info_ptr->cmd_subs,cmd_info_ptr->network_sel_mode_pref);
  
  if ( pm_rest_obj_ptr == NULL )
  {
    return;
  }

  #if defined(FEATURE_MMODE_TRIPLE_SIM) || defined(FEATURE_MMODE_SXLTE_G)
  if(cmd_info_ptr->cmd_subs == SYS_MODEM_AS_ID_3 || 
      (cmd_info_ptr->cmd_subs == SYS_MODEM_AS_ID_2 && cmph_is_sxlte()))
  {
    hybr_3_pref_info_ptr->network_sel_mode_pref = net_sel_mode_pref;
  }
  else
  #endif
  if(cmd_info_ptr->cmd_subs == SYS_MODEM_AS_ID_2)
  {
    hybr_2_pref_info_ptr->network_sel_mode_pref = net_sel_mode_pref;
  }
  
  CM_MSG_HIGH_0("PMPRX: Force the user preferences on PolicyMan obj too");

  (void)cmph_force_pref_on_the_fly2(
                                 ((cmd_info_ptr->cmd_subs == SYS_MODEM_AS_ID_1)? CM_SS_MAX : (cmph_map_as_id_to_cm_ss(cmd_info_ptr->cmd_subs))),
                                     cmph_ptr(),
                                     SD_SS_PREF_REAS_USER,
                                     CM_ACT_TYPE_POLICYMAN_RESTRICT,
                                     cmph_ptr()->main_stack_info.pref_info.pref_term,
                                     prefered_mode,
                                     cmd_info_ptr->band_pref,
                                     cmd_info_ptr->lte_band_pref,
                                     cmd_info_ptr->tds_band_pref,
                                     cmd_info_ptr->prl_pref,
                                     cmd_info_ptr->roam_pref,
                                     cmd_info_ptr->hybr_pref,
                                     cmd_info_ptr->plmn,
                                     CM_SRV_DOMAIN_PREF_NO_CHANGE,
                                     cmd_info_ptr->acq_order_pref,
                                     net_sel_mode_pref,
                                 hybr_2_pref_info_ptr,
                                 #if defined(FEATURE_MMODE_TRIPLE_SIM) || defined(FEATURE_MMODE_SXLTE_G)
                                 hybr_3_pref_info_ptr,
                                 #endif
                                 CM_OTASP_ACT_CODE_NONE,
                                     (cm_act_id_type) cmpm_ptr(),
                                     CM_ACT_UPDATE_REAS_ACT_START,
                                     FALSE,
                                     rat_acq_pri_order,
                                     cmd_info_ptr->sys_sel_pref_req_id,
                                     cmd_info_ptr->csg_id,
                                     cmd_info_ptr->csg_rat);

}

/*===========================================================================

FUNCTION cmpmprx_get_ue_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_get_ue_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_ue_mode_e_type         *ue_mode_buffer
)
{
  if ( !cmpmprx_is_mask_set( asubs_id, PM_CHG_UE_MODE, cmpm_ptr() ) ||
       ue_mode_buffer == NULL       ||
       !cmpmprx_read_cm_policy_consumption_eligibility(asubs_id)
     )
  {
    CM_MSG_MED_0("PMPRX -> CM: NO policyman ue_mode update");
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_HIGH_1("PMPRX -> CM: Policyman ue_mode=%d",
               cmpm_ptr()->current_policy[asubs_id].ue_operation_mode);

  *ue_mode_buffer   = cmpm_ptr()->current_policy[asubs_id].ue_operation_mode;
  cmpmprx_reset_policy_chg_flag( asubs_id, PM_CHG_UE_MODE );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return TRUE;
}

/*===========================================================================

FUNCTION cmpmprx_read_ue_mode

DESCRIPTION
  This API should be used only when 
  cmmsc_init is not called yet .

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void                    cmpmprx_read_ue_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_ue_mode_e_type         *ue_mode_buffer
)
{
#ifdef CM_DEBUG
  #error code not present
#else
  CM_MSG_HIGH_1("PMPRX -> CM: Policyman ue_mode=%d",
               cmpm_ptr()->current_policy[asubs_id].ue_operation_mode);
  
  *ue_mode_buffer   = cmpm_ptr()->current_policy[asubs_id].ue_operation_mode;
#endif
}


/*===========================================================================

FUNCTION cmpmprx_get_device_mode

DESCRIPTION
  Gets device mode which is decided based on no. of SIM and max SIMs that are
  active at a given time.

===========================================================================*/
sys_modem_device_mode_e_type cmpmprx_get_device_mode( void )
{
  cm_policy_config_s_type           *pm_info = cmpm_ptr();
  cm_policy_device_mode_s_type    pm_device_mode = pm_info->device_mode;
  sys_modem_device_mode_e_type  mapped_device_mode = SYS_MODEM_DEVICE_MODE_SINGLE_SIM;

  if(pm_device_mode.nSims == 3)
  {
    mapped_device_mode = SYS_MODEM_DEVICE_MODE_TRIPLE_SIM_TRIPLE_STANDBY;
  }

  if(pm_device_mode.nSims == 2)
  {
    switch(pm_device_mode.nMaxActive)
    {
      case 2:
        mapped_device_mode = SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_ACTIVE;
        break;

      case 1:
      default:
        mapped_device_mode = SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_STANDBY;
    }
  }

  pm_info->device_policy_changes = 0;
  
  return mapped_device_mode;
}

/*===========================================================================

FUNCTION cmpmprx_get_num_of_sims

DESCRIPTION
  Get num of sims supported base on Policy manager configuration item.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
size_t  cmpmprx_get_num_of_sims( void )
{
  cm_policy_config_s_type           *pm_info = cmpm_ptr();
  return (pm_info->device_mode.nSims);
}

/*===========================================================================

FUNCTION cmpmprx_rread_device_mode

DESCRIPTION
  Added for the purpose of debugging in case we need to re-read device mode
===========================================================================*/
void cmpmprx_rread_device_mode( void )
{
  cm_policy_config_s_type *pm_ptr = cmpm_ptr();
  pm_ptr->device_mode = cmpmprx_read_pm_device_mode();
}

/*===========================================================================

FUNCTION cmpmprx_check_sys_sel_pref_policy_conflict

DESCRIPTION
  If the new system selection preferences cause a policy change, check if it 
  in conflict with current device state.
  If policy change results in UE mode change and voice call ongoing, then
  mark conflict as TRUE

DEPENDENCIES
  none

RETURN VALUE
  TRUE: New policy retrieved and updated.
  False: No change in policy.

SIDE EFFECTS
  none

===========================================================================*/
boolean                           cmpmprx_check_sys_sel_pref_policy_conflict(

  cm_ph_cmd_s_type           *ph_cmd_ptr
)
{
  cm_policyman_cfg_s      *pm_msgr_rsp  = NULL;
  boolean                 return_val    = FALSE;
  msgr_attach_s           *pAttach            = NULL;
  int                      i = 0;
  uint8                    j = 0;
  policy_change_mask       change_mask = 0;
  cm_sys_sel_pref_params_s_type pref_info;
  cm_ph_cmd_info_s_type*    cmd_info_ptr;
  sys_modem_as_id_e_type    asubs_id;
  cmph_s_type*              ph_ptr = cmph_ptr();
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmd_info_ptr = CMD_INFO_PTR( ph_cmd_ptr );
  asubs_id = cmd_info_ptr->cmd_subs;

  /* Call into Policy manager and retrieve new policy if needed.
  ** Use the same preferences as used in the force_pref_on_fly() below.
  */
  pref_info.mode_pref        = cmd_info_ptr->mode_pref;
  pref_info.term_pref        = ph_ptr->main_stack_info.pref_info.pref_term;
  pref_info.pref_duration    = cmd_info_ptr->pref_duration;
  pref_info.band_pref        = cmd_info_ptr->band_pref;
  pref_info.lte_band_pref    = cmd_info_ptr->lte_band_pref;
  pref_info.tds_band_pref    = cmd_info_ptr->tds_band_pref;
  pref_info.prl_pref         = cmd_info_ptr->prl_pref;
  pref_info.roam_pref        = cmd_info_ptr->roam_pref;
  pref_info.hybr_pref        = cmd_info_ptr->hybr_pref;
  pref_info.srv_domain_pref  = cmd_info_ptr->srv_domain_pref;
  pref_info.network_sel_mode_pref = cmd_info_ptr->network_sel_mode_pref;
   /* this is to avoid sending null PLMN values to PM when network sel mode is set to no_change from manual */
  if((cmd_info_ptr->network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL) && ((sys_plmn_id_is_null(cmd_info_ptr->plmn))|| (sys_plmn_id_is_undefined(cmd_info_ptr->plmn))))
  {
    pref_info.plmn_ptr	   = &(ph_ptr->main_stack_info.pref_info.plmn);
  }
  else
  {
    pref_info.plmn_ptr         = &(cmd_info_ptr->plmn);
  }

  //pref_ptr->rat_acq_order_pref_ptr = &rat_acq_pri_order;
  pref_info.csg_id           = cmd_info_ptr->csg_id;
  pref_info.csg_rat          = cmd_info_ptr->csg_rat;
  pref_info.ue_usage_setting = ph_ptr->main_stack_info.pref_info.ue_usage_setting;

  /* Ensure that the subscription id is valid.
  */
  if ( asubs_id > SYS_MODEM_AS_ID_3 )
  {
    asubs_id = SYS_MODEM_AS_ID_1;
  }
  
  if (cmd_info_ptr->srv_domain_pref == CM_SRV_DOMAIN_PREF_NO_CHANGE)
  {
    pref_info.srv_domain_pref = CMPH_SS_SRV_DOMAIN_PREF( asubs_id, ph_ptr );
  }

  if(pref_info.srv_domain_pref == CM_SRV_DOMAIN_PREF_PS_ATTACH)
  {
    if( ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_CS_ONLY ) ||
          ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_CS_PS ) )
    {
      pref_info.srv_domain_pref = CM_SRV_DOMAIN_PREF_CS_PS;
    }
    else if ( ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_NONE) ||
       ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_PS_ONLY) )
    {
      pref_info.srv_domain_pref = CM_SRV_DOMAIN_PREF_PS_ONLY;
    }
  }

  if(pref_info.srv_domain_pref == CM_SRV_DOMAIN_PREF_PS_DETACH)
  {
    if( ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_CS_ONLY ) ||
          ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_CS_PS ) )
    {
      pref_info.srv_domain_pref = CM_SRV_DOMAIN_PREF_CS_ONLY;
    }
    else if ( ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_NONE) ||
       ( CMPH_SS_SRV_DOMAIN_PREF(asubs_id,ph_ptr) == CM_SRV_DOMAIN_PREF_PS_ONLY) )
    {
      pref_info.srv_domain_pref = CM_SRV_DOMAIN_PREF_NONE;
    }
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Send the new preferences to Policy Manager and receive the new policy
  ** if it is changed.
  */
  pm_msgr_rsp = (cm_policyman_cfg_s*)
                      policyman_compute_policy_with_newpref( asubs_id, &pref_info);


  if ( pm_msgr_rsp == NULL )
  {
    CM_MSG_MED_0("PMPRX: New preferences have no effect on policy");

    /* For UT verification*/
    #ifdef CM_DEBUG
    #error code not present
#else
    return return_val;
    #endif
  }
  
  for (i = 0 ; i < msgr_get_num_attach(&(pm_msgr_rsp->msg_hdr)) ; ++i)
  {
    policyman_item_collection_t const *policy_item = NULL;
    pAttach = msgr_get_attach(&(pm_msgr_rsp->msg_hdr), i);
    if (pAttach == NULL || pAttach->type != MSGR_ATTACH_REFCNT)
    {
      CM_MSG_HIGH_1("PMPRX: Invalid attachment %d.Abort new policy.",i);
      change_mask = PM_CHG_CHANGE_NONE;
      break;
    }

    policy_item = pAttach->data.refcnt.refcnt_obj_ptr;
    
    for (j = 0 ;j < policy_item->numItems ; j++)
    {
      policyman_item_t const *pItem = policy_item->pItems[j];
      switch (policyman_item_get_id_msim(pItem,&asubs_id))
      {
       /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
       case POLICYMAN_ITEM_UE_MODE:
         change_mask |= PM_CHG_UE_MODE;
         break;
       /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
       case POLICYMAN_ITEM_DEVICE_CONFIGURATION:
         change_mask |= PM_CHG_DEVICE_MODE;
         break;
         
        /*- - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - -*/
       default:
         break;
      }
    }
  }

  #ifndef CM_DEBUG
  policyman_report_preference_msg_free( (msgr_hdr_s *)pm_msgr_rsp );
  #endif

  CM_MSG_HIGH_1("PMPRX: New Change mask:%d",change_mask);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(((change_mask & PM_CHG_UE_MODE)
      &&(( cmcall_find_active_cs_call_per_subs (asubs_id)
            != CM_CALL_ID_INVALID)
           || (cmcall_is_there_a_call_type_per_sub(asubs_id,CM_CALL_TYPE_SMS,NULL)
            != CM_CALL_ID_INVALID)
         ))
      ||
      ((change_mask & PM_CHG_DEVICE_MODE)
      &&( cmcall_find_active_voice_call()
      != CM_CALL_ID_INVALID)))
  {
    return TRUE;
  }

  return FALSE;
}
/*===========================================================================

FUNCTION cmpmprx_get_num_of_active_data

DESCRIPTION
  Get num of active data supported base on Policy manager configuration item.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
size_t  cmpmprx_get_num_of_active_data( void )
{
  cm_policy_config_s_type           *pm_info = cmpm_ptr();
  CM_MSG_HIGH_1("cmpmprx_get_num_of_active_data: Value of nMaxActiveData %d",pm_info->device_mode.nMaxActiveData);
  return (pm_info->device_mode.nMaxActiveData);
}

/*===========================================================================

FUNCTION cmpmprx_sys_sel_pref_policy_update

DESCRIPTION
  If the new system selection preferences cause a policy change, retrieve it
  from Policy Manager and update the cmpm_local.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: New policy retrieved and updated.
  False: No change in policy.

SIDE EFFECTS
  none

===========================================================================*/
boolean                           cmpmprx_sys_sel_pref_policy_update(

       sys_modem_as_id_e_type     asubs_id,

       cm_sys_sel_pref_params_s_type* pref_ptr
)
{
  cm_policyman_cfg_s      *pm_msgr_rsp  = NULL;
  cm_policy_config_s_type *local_policy = NULL;
  boolean                 return_val    = FALSE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if (pref_ptr == NULL)
  {
    CM_MSG_HIGH_0("PMPRX: Incoming pref_ptr NULL");
    return return_val;
  }

  /* Ensure that the subscription id is valid.
  */
  if ( asubs_id == SYS_MODEM_AS_ID_NO_CHANGE || asubs_id == SYS_MODEM_AS_ID_MAX 
      || asubs_id == SYS_MODEM_AS_ID_NONE)
  {
    CM_MSG_HIGH_1("Returning from cmpmprx_sys_sel_pref_policy_update() since as_id is %d",asubs_id);
    return FALSE;
  }

  CM_MSG_MED_1("ASUBS_ID(cmpmprx_sys_sel_pref_policy_update): %d",asubs_id);
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Send the new preferences to Policy Manager and receive the new policy
  ** if it is changed.
  */
  pm_msgr_rsp = (cm_policyman_cfg_s*)
                      policyman_report_preference_change( asubs_id, pref_ptr);


  if ( pm_msgr_rsp == NULL )
  {
    CM_MSG_MED_0("PMPRX: New preferences have no effect on policy");
    return return_val;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Policy changed so we need to retrieve the new policy data items.
  ** Allocate local buffer for new memory and initialize it to current policy.
  */
  local_policy = (cm_policy_config_s_type *) cm_mem_malloc(sizeof(cm_policy_config_s_type));
  if ( local_policy == NULL )
  {
    CM_MSG_HIGH_0("PMPRX: unable to process policy as cannot allocate memory");
    return return_val;
  }

  *local_policy = *(cmpm_ptr());
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Retrieve the policy data into a local buffer. If all the policy items in
  ** the new policy are successfully retrieved, only then update the current
  ** policy and attempt to enforce it.
  */
  return_val = cmpmprx_process_msgr_msg(pm_msgr_rsp,local_policy);

  /* If there is a pref change while ue is in voice all and the retrieved
  ** policy has ue_mode change, dont enforce the policy. Wait for it to arrive
  ** on message router message so it wont interfere with call.
  */
  if ( return_val == TRUE &&
       cmpmprx_is_ok_to_enforce_policy( local_policy, asubs_id ) )
  {
    cmpmprx_update_curr_policy(local_policy);
  }
 
   /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Enforce device level policy */
  if(cmpm_ptr()->device_policy_changes != 0)
  {
    cmpmprx_enforce_device_policy();
  }

  #ifndef CM_DEBUG
  policyman_report_preference_msg_free( (msgr_hdr_s *)pm_msgr_rsp );
  #endif

  cm_mem_free(local_policy);
  return return_val;
}

/*===========================================================================

FUNCTION cmpmprx_msim_subsc_changed

DESCRIPTION
  Called when susbcriptions in multi-mode phones change.
  This function calls into PM to update new susbcriptions.
  PM refreshes policies based on this update.
    - Currently PM shall decide enable/disable LTE in multimode sub
      when subscriptions change. Meaning MSIM standby pref change.
===========================================================================*/
boolean cmpmprx_msim_subsc_changed(

  uint8 active_subs
)
{
  cm_policyman_cfg_s      *pm_msgr_rsp  = NULL;
  cm_policy_config_s_type *local_policy = NULL;
  boolean                 return_val    = FALSE;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  pm_msgr_rsp = (cm_policyman_cfg_s*)
                  policyman_report_multimode_subs( active_subs);

  if ( pm_msgr_rsp == NULL )
  {
    CM_MSG_MED_0("PMPRX: New MSIM subs have no effect on policy");
    return TRUE;
  }

  local_policy = (cm_policy_config_s_type *)
                    cm_mem_malloc(sizeof(cm_policy_config_s_type));

  *local_policy = *(cmpm_ptr());

  return_val = cmpmprx_process_msgr_msg(pm_msgr_rsp,local_policy);

  if ( return_val == TRUE &&
       (((active_subs & BM(SYS_MODEM_AS_ID_1)) && cmpmprx_is_ok_to_enforce_policy(cmpm_ptr(), SYS_MODEM_AS_ID_1)) ||
        ((active_subs & BM(SYS_MODEM_AS_ID_2)) && cmpmprx_is_ok_to_enforce_policy(cmpm_ptr(), SYS_MODEM_AS_ID_2)) ||
       #ifdef FEATURE_MMODE_TRIPLE_SIM
        ((active_subs & BM(SYS_MODEM_AS_ID_3)) && cmpmprx_is_ok_to_enforce_policy(cmpm_ptr(), SYS_MODEM_AS_ID_3)) ||
       #endif
        local_policy->device_policy_changes != 0
      ))
  {
    cmpmprx_update_curr_policy(local_policy);
  }

  policyman_report_msg_free( (msgr_hdr_s *)pm_msgr_rsp );

  cm_mem_free(local_policy);

  /* this is called to update subs capability to TRM/MCS */
  cmph_sp_subs_info_update();

  return return_val;
}

/*===========================================================================

FUNCTION cmpmprx_emergency_call_policy_update

DESCRIPTION
  Enforce Full RAT mode on the UE.
  *** THIS IS TO BE USED ONLY FOR EMERGENCY CALL WHEN HYBRID2 NOT IN SRV ***

  Restore the original policy to cmpm_local so that when the call is ended, it
  will be enforced.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  none

===========================================================================*/
static void                       cmpmprx_emergency_call_policy_update(sys_modem_as_id_e_type  asubs_id)
{

  cm_policy_config_s_type *emerg_policy = NULL; // Store the full rat policy.
  cm_policy_config_s_type *curr_policy = NULL; // Store the full rat policy.

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Allocate memory for FULL RAT mode policy. */
  emerg_policy = (cm_policy_config_s_type *) cm_mem_malloc(sizeof(cm_policy_config_s_type));
  curr_policy = (cm_policy_config_s_type *) cm_mem_malloc(sizeof(cm_policy_config_s_type));

  if ( emerg_policy == NULL || curr_policy == NULL )
  {
    CM_MSG_HIGH_0("PMPRX: unable to enforce Full RAT as cannot allocate memory");
    return;
  }

  *emerg_policy = *(cmpm_ptr());
  *curr_policy = *(cmpm_ptr());

  /* Populate FULL RAT mode for the emergency call. */
  emerg_policy->current_policy[asubs_id].ue_operation_mode          = SYS_UE_MODE_SGLTE_ROAM;
  emerg_policy->current_policy[asubs_id].service_scan_mode          = POLICYMAN_SVC_MODE_LIMITED;
  emerg_policy->current_policy[asubs_id].policy_mode_capability     = SD_SS_MODE_PREF_ALL_MODES;
  emerg_policy->current_policy[asubs_id].policy_band_capability     = SYS_BAND_MASK_ANY;
  emerg_policy->current_policy[asubs_id].policy_lte_band_capability = SYS_BAND_MASK_ANY;
  emerg_policy->current_policy[asubs_id].policy_tds_band_capability = SYS_BAND_MASK_ANY;

  emerg_policy->policy_changes[asubs_id] = PM_CHG_ALL;

  cmpmprx_update_curr_policy(emerg_policy);

  cm_mem_free(emerg_policy);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Enforce FULL RAT mode for the emergency call. */
  cmpmprx_enforce_current_policy(asubs_id);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  curr_policy->policy_changes[0] = PM_CHG_ALL;

  cmpmprx_update_curr_policy(curr_policy);

  cm_mem_free(curr_policy);
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

}

/*===========================================================================

FUNCTION cmpmprx_is_call_to_reorig

DESCRIPTION
  If UE in SGLTE mode and Hybr GSM not in service, CM may need to modify
  policy and re-evaluate the call as per new policy.

  This is done to allow emergency call to be attempted on other RATs when GSM
  CS service not available in SGLTE.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: Call command requeued.
  False: Proceed with call origination.

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_is_call_to_reorig(

       cmcall_s_type              *call_ptr,

       void                       *curr_cmd,

       sys_ue_mode_e_type         curr_msc_ue_mode
)
{
  cm_cmd_type              *cmd_ptr = NULL;
  cm_service_status_s_type srv_info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* This processing applies only to emergency call orignation in SGLTE mode*/

  if (call_ptr == NULL ||
     (!cmph_is_subs_feature_mode_sglte(call_ptr->asubs_id))||      
      curr_cmd == NULL )
  {
    return FALSE;
  }

  if ( curr_msc_ue_mode    != SYS_UE_MODE_SGLTE_HOME ||
       call_ptr->call_type != CM_CALL_TYPE_EMERGENCY )
  {
    CM_MSG_MED_2("PMPRX: call_type %d ue_mode %d",call_ptr->call_type,
                                                curr_msc_ue_mode);
    return FALSE;
  }
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* If the stack being used for emergency call origination in */
  cmss_get_service_status_per_stack(call_ptr->ss,&srv_info);

  CM_MSG_HIGH_2("PMPRX: Service %d available on ss %d for emerg call",
                                                srv_info.srv_status,
                                                       call_ptr->ss);
  if (sys_srv_status_is_srv(srv_info.srv_status))
  {
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*
  **Change the policy locally to FULL RAT mode.
  */
  cmpmprx_emergency_call_policy_update(call_ptr->asubs_id);

  CM_MSG_HIGH_0("PMPRX: Requeing emerg call for orig on FULL RAT");
  /*
  **connection id will be reserved later.
  */
  cmcall_unreserve_connection_id (call_ptr);

  cmd_ptr = (cm_cmd_type *) curr_cmd;
  cmd_ptr->cmd_type = CM_CMD_TYPE_CALL;
  CALL_CMD_PTR(cmd_ptr)->cmd = CM_CALL_CMD_ORIG;
  cmd_ptr->is_reused = TRUE;
  cm_cmd_queue (cmd_ptr);

  return TRUE;
}

/*===========================================================================

FUNCTION cmpmprx_get_overall_featuremode

DESCRIPTION
  Called from other modules to read the current value of the overall
  featuremode in CMPM
===========================================================================*/
sys_overall_feature_t cmpmprx_get_overall_featuremode(void)
{
  return (cmpm_ptr()->feature_mode);
}

/*===========================================================================

FUNCTION cmpmprx_get_subs_feature_mode

DESCRIPTION
  API for CM to retrieve the new subs specific feature mode.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: New subs featuremode retrieved
  False: Operation failed.

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_get_subs_feature_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_subs_feature_t         *subs_featuremode_buffer
)
{
  if ( subs_featuremode_buffer == NULL
    /*    || !cmpmprx_read_cm_policy_consumption_eligibility(asubs_id)  */ /* Not required for subs feature mode */
     )
  {
    CM_MSG_MED_0("PMPRX -> CM: NO policyman ue_mode update");
    return FALSE;
  }



  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_HIGH_2("PMPRX -> CM: Policyman subs_id = %d, subs_feature =%d",
               asubs_id,
               cmpm_ptr()->current_policy[asubs_id].sub_feature_mode);

  *subs_featuremode_buffer   = cmpm_ptr()->current_policy[asubs_id].sub_feature_mode;

  #ifndef FEATURE_MMODE_SXLTE_G
  if (cmph_ptr()->feature_mode == SYS_OVERALL_FEATURE_MODE_MULTISIM)
  {
    CM_MSG_HIGH_2("fmode: changing subs_feature_mode to NORMAL for asubs_id %d, pm %d", asubs_id, *subs_featuremode_buffer);
    *subs_featuremode_buffer = SYS_SUBS_FEATURE_MODE_NORMAL;
  }
  #endif
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  return TRUE;
}
