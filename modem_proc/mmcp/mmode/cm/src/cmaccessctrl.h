#ifndef CM_ACCESSCTRL_H
#define CM_ACCESSCTRL_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

            C A L L   M A N A G E R   A C C E S S   C O N T R O L

GENERAL DESCRIPTION
  This module contains declarations used by the call manager in processing
  Access Control command. The module uses the callback function for the caller 
  and also MSGR is used for broadcasting the information between CM and all 
  other modules who are interested in the information of the access controller.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  cmdbm_init() must be called to initialize this module before any other
  function declared in this module is being called.

Copyright (c) 2013 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/cm/src/cmaccessctrl.h#1 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
08/08/13    xs      Remove emg handling from CMAC
02/15/13    th      Initial design
06/18/13    gm      FR 2617 - OMADM IMS and ADMIN PDN Disable Requirement

===========================================================================*/

        
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                                  GLOBALS                                */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

#define THIRD_PARTY_E911_CALL_ID 0xFE

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*                               FUNCTION DECLARATIONS                     */
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

/*===========================================================================

FUNCTION cmac_init

DESCRIPTION
  Access control initialize

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  none

===========================================================================*/
extern void cmac_init( void );

/*===========================================================================

FUNCTION cmac_is_in_emergency_only

DESCRIPTION
  Returns TRUE if CM AC is in emergency only state due to any of the reasons

===========================================================================*/
boolean cmac_is_in_emergency_only(void);

/*===========================================================================

FUNCTION cmac_get_act_id

DESCRIPTION
  Return act id for access control  

DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern cm_act_id_type cmac_get_act_id (void);


/*==========================================================================
FUNCTION cmac_is_1x_ppp_dereg_end

DESCRIPTION
  Return TRUE if 1X PPP clean up operation is completed and CM is restoring  
  phone preference and forcing to SD

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
==========================================================================*/

extern boolean cmac_is_1x_ppp_dereg_end(void);


/*===========================================================================

FUNCTION cmac_init

DESCRIPTION
  Access control initialize

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  none

===========================================================================*/
extern void cmac_init( void );

/*===========================================================================

FUNCTION cmac_is_in_thermal_emergency

DESCRIPTION
  Returns TRUE if CM AC is in emergency due to UNSAFE temperature

===========================================================================*/
boolean cmac_is_in_thermal_emergency(void);


/*===========================================================================

FUNCTION cmac_restrict_to_emergency

DESCRIPTION
  This function implements access control putting phone in emergency mode
  due to reasons in cm_ac_reason_e_type. This happens in two stages:
  1. If GWL protocols are active, then a deactivate is sent
  2. After recving conf for GWL activate, GWL protocols are put in limited
     service mode

  If at #1, GWL protocols are not active or in limited srv already, then #2
  is done without wait.
===========================================================================*/

void cmac_restrict_to_emergency(

  cm_ac_reason_e_type reason
    /* Reason for putting modem in emergency mode */
);

/*===========================================================================

FUNCTION cmac_enable_emergency_if_pending

DESCRIPTION
  This function enables emergency mode if it was deferred due to emergency
  call or callback mode or operation modes.
===========================================================================*/
void cmac_enable_emergency_if_pending(void);

/*===========================================================================

FUNCTION cmac_restore_to_normal

DESCRIPTION
  Restore access control normal state where normal calls are enabled.
  Before call of this API, phone may be in emergency only mode due to
  reasons in cm_ac_reason_e_type.
===========================================================================*/

void cmac_restore_to_normal(

  cm_ac_reason_e_type reason
    /* Reason due to which normal opr being restored. */
);

/*===========================================================================

FUNCTION cmac_timer_proc

DESCRIPTION
  Process timer events

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmac_timer_proc(

  cm_timer_event_type    timer_event
    /* Indicate specific timer event.
    **
    ** Note that currently timer event is not being used
    */
);

/*===========================================================================

FUNCTION cmbcmcs_client_cmd_proc

DESCRIPTION
  Process clients' ac data commands.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void cmac_client_cmd_proc(

  cm_ac_cmd_s_type  *ac_cmd_ptr
    /* pointer to a CM command
    */
);

/*===========================================================================

FUNCTION cmac_mc_rpt_proc

DESCRIPTION
  Process ac reports specific to DS

DEPENDENCIES
  None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void cmac_mc_rpt_proc(

  const cm_hdr_type   *rpt_ptr
    /* Pointer to MC report */
);

/*===========================================================================

FUNCTION cmac_nas_rpt_proc

DESCRIPTION
  Process reports from NAS. In CMAC, we are looking at STOP_MODE_CNF

===========================================================================*/
void cmac_nas_rpt_proc(  

  const cm_hdr_type   *rpt_ptr

);

/*===========================================================================

FUNCTION cmac_call_cmd_check

DESCRIPTION
  Process clients' call commands

DEPENDENCIES
  Call object must have already been initialized with
  cmcall_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern cm_call_cmd_err_e_type cmac_call_cmd_check(
  cm_call_cmd_s_type       *call_cmd_ptr
);

/*===========================================================================

FUNCTION cmac_send_notify_ind

DESCRIPTION
  Send event to client through message router with reason as payload

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

extern void cmac_send_notify_ind(
  msgr_umid_type ac_evt_umid,
  cm_ac_reason_e_type reason,
  void* user_data
);

/*===========================================================================
FUNCTION cmac_ph_cmd_proc

DESCRIPTION
  Process clients' ph commands

DEPENDENCIES
  Call object must have already been initialized with
  cmcall_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmac_ph_cmd_proc(

  const cm_ph_cmd_s_type     *ph_cmd_ptr

);

/*===========================================================================

FUNCTION cmac_enter_thermal_emergency_level_2

DESCRIPTION
  Updates AC state to thermal emergency level 2
  Ends locally held calls
===========================================================================*/

void cmac_enter_thermal_emergency_level_2(void);

/*===========================================================================

FUNCTION cmac_is_in_thermal_emergency_level2

DESCRIPTION
  Returns TRUE if CM AC is in level2 thermal emergency due to UNSAFE temperature

===========================================================================*/
boolean cmac_is_in_thermal_emergency_level2(void);

#endif //CM_ACCESSCTRL_H
