#ifndef CMPMPRX_H
#define CMPMPRX_H

/*===========================================================================

          P M P R O X Y    H E A D E R    F I L E

DESCRIPTION
  This file contains all the definitions necessary for the Call Manager ( CM )
  to interface with the Policy Manager module.

Copyright (c) 2002 - 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

   This section contains comments describing changes made to this file.
   Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/cm/src/cmpmprx.h#2 $

when       who     what, where, why
--------   ---    -------------------------------------------------------
04/29/13   ak     Initial release.

===========================================================================*/


#ifndef EXTERN
#define EXTERN
#endif

#include "policyman.h"
#include "policyman_msg.h"
#include "policyman_cm.h"
#include "sys.h"
#include "cmi.h"

#define PM_CHG_CHANGE_NONE  0
#define PM_CHG_SVC_SCAN   BM(0)
#define PM_CHG_MODE_CAP   BM(1)
#define PM_CHG_BAND_CAP   BM(2)
#define PM_CHG_UE_MODE    BM(3)
#define PM_CHG_DEVICE_MODE BM(4)
#define PM_CHG_MAX_ACTIVE_DATA BM(5)
#define PM_CHG_RAT_ACQ_ORDER BM(6)

#define PM_CHG_ALL        ( PM_CHG_SVC_SCAN|\
                            PM_CHG_MODE_CAP|\
                            PM_CHG_BAND_CAP|\
                            PM_CHG_UE_MODE |\
                            PM_CHG_DEVICE_MODE|\
                            PM_CHG_RAT_ACQ_ORDER) 

#define PM_SUB_FMODE_MASK_SVLTE
#define PM_SUB_FMODE_MASK_SGLTE
#define PM_SUB_FMODE_MASK_NORMAL

#define POLICY_ITEM_ARRAY_SIZE   3

typedef uint32  policy_change_mask;
typedef uint32  policy_sub_fmode_mask;

typedef struct
{
  boolean   subsc;
}cm_policy_elig_s_type;

typedef struct
{
  size_t                            nSims;
  size_t                            nMaxActive;
  size_t                            nMaxActiveData;
}cm_policy_device_mode_s_type;

typedef struct
{
  sys_subs_feature_t          sub_feature_mode;
  
  policyman_svc_mode_t        service_scan_mode;

  sys_ue_mode_e_type          ue_operation_mode;

  uint32                      policy_mode_capability;

  sys_band_mask_e_type        policy_band_capability;
  sys_band_mask_e_type        policy_lte_band_capability;
  sys_band_mask_e_type        policy_tds_band_capability;
  cm_acq_pri_order_pref_s_type  acq_order_pref;
}cm_policy_items_s_type;

typedef struct policy{

  cm_policy_elig_s_type       is_cm_eligible[3];
  policy_change_mask          policy_changes[3];
  policy_change_mask          device_policy_changes;
  boolean                     is_force_registration;
  boolean                     is_pm_suspend;
  sys_overall_feature_t       feature_mode;
  cm_policy_device_mode_s_type device_mode;

  cm_policy_items_s_type      current_policy[POLICY_ITEM_ARRAY_SIZE];
}cm_policy_config_s_type;


void cmpmprx_init( void );

/*===========================================================================

FUNCTION cmpmprx_update_cm_policy_consumption_eligibility

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmpmprx_update_cm_policy_consumption_eligibility_subsc(

       sys_modem_as_id_e_type     asubs_id,

       boolean                    new_value
);

/*===========================================================================

FUNCTION cmpmprx_enforce_policy_oprt_mode

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean                           cmpmprx_enforce_policy_oprt_mode(

       sys_modem_as_id_e_type     asubs_id

);

/*===========================================================================

FUNCTION cmpmprx_read_cm_policy_consumption_eligibility

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmpmprx_read_cm_policy_consumption_eligibility(

       sys_modem_as_id_e_type     asubs_id

);

/*===========================================================================

FUNCTION cmpmprx_is_mask_set

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_is_mask_set(

       sys_modem_as_id_e_type     asubs_id,

       policy_change_mask         check_mask,

       cm_policy_config_s_type   *pm_ptr
);
/*===========================================================================

FUNCTION cmpmprx_policy_chg_ind_proc

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void                       cmpmprx_policy_chg_ind_proc(

       cm_policyman_cfg_s         *rpt_ptr
        /* Pointer to the report sent by Message Router */
);

/*===========================================================================

FUNCTION cmpmprx_fetch_mode_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_fetch_mode_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_sys_mode_mask_e_type   *mode_capability_buffer
);

/*===========================================================================

FUNCTION cmpmprx_get_mode_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_get_mode_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_sys_mode_mask_e_type   *mode_capability_buffer
);


/*===========================================================================

FUNCTION cmpmprx_get_band_capability

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_get_band_capability(

       sys_modem_as_id_e_type     asubs_id,

       sys_band_mask_e_type       *band_capability_buffer,

       sys_band_mask_e_type       *lte_band_capability_buffer,

       sys_band_mask_e_type       *tds_band_capability_buffer,

       boolean                    *force_registration
);

/*===========================================================================

FUNCTION cmpmprx_get_ue_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_get_ue_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_ue_mode_e_type         *ue_mode_buffer
);

/*===========================================================================

FUNCTION cmpmprx_read_ue_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
EXTERN void cmpmprx_read_ue_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_ue_mode_e_type         *ue_mode_buffer
);



/*===========================================================================

FUNCTION cmpmprx_enforce_service_scan_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void                       cmpmprx_enforce_service_scan_mode(

       sys_modem_as_id_e_type     asubs_id
);

/*===========================================================================

FUNCTION cmpmprx_get_subs_feature_mode

DESCRIPTION
  API for CM to retrieve the new subs specific feature mode.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: New subs featuremode retrieved
  False: Operation failed.

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_get_subs_feature_mode(

       sys_modem_as_id_e_type     asubs_id,

       sys_subs_feature_t         *subs_featuremode_buffer
);
/*===========================================================================

FUNCTION cmpmprx_enforce_service_scan_mode

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void                       cmpmprx_unforce_policy(

       sys_modem_as_id_e_type     asubs_id

);

/*===========================================================================

FUNCTION cmpmprx_sys_sel_pref_proc

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void                         cmpmprx_sys_sel_pref_proc(

       cm_ph_cmd_info_s_type        *cmd_info_ptr,

       cm_mode_pref_e_type          prefered_mode,

       cm_pref_s_type              *hybr_2_pref_info_ptr,
           /* Preferences of the HYBR2 stack */
       
       #if defined(FEATURE_MMODE_TRIPLE_SIM)  || defined(FEATURE_MMODE_SXLTE_G)
       cm_pref_s_type              *hybr_3_pref_info_ptr,
           /* Preferences of the HYBR3 stack */
       #endif

       cm_acq_pri_order_pref_s_type *rat_acq_pri_order
);

/*===========================================================================

FUNCTION cmpmprx_notify_call_end

DESCRIPTION
  Process config commands.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern void                       cmpmprx_notify_call_end(

       sys_modem_as_id_e_type     asubs_id,
       boolean is_emerg     
);

/*===========================================================================

FUNCTION cmpmprx_notify_emg_call_start

DESCRIPTION
  The function is called to notify cmpmprx that emergency call has started.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void  cmpmprx_notify_emg_call_start(void);


/*===========================================================================

FUNCTION cmpmprx_get_device_mode

DESCRIPTION
  Gets device mode which is decided based on no. of SIM and max SIMs that are
  active at a given time.
  
===========================================================================*/
sys_modem_device_mode_e_type cmpmprx_get_device_mode( void );

/*===========================================================================

FUNCTION cmpmprx_get_num_of_sims

DESCRIPTION
  Get num of sims supported base on Policy manager configuration item.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
size_t  cmpmprx_get_num_of_sims( void );

/*===========================================================================

FUNCTION cmpmprx_get_num_of_active_data

DESCRIPTION
  Get num of active data supported base on Policy manager configuration item.

DEPENDENCIES
None

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
size_t  cmpmprx_get_num_of_active_data( void );


/*===========================================================================

FUNCTION cmpmprx_rread_device_mode

DESCRIPTION
  Added for the purpose of debugging in case we need to re-read device mode.
  
===========================================================================*/
void cmpmprx_rread_device_mode( void );

/*===========================================================================

FUNCTION cmpmprx_is_ok_to_enforce_policy

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_is_ok_to_enforce_policy(

       cm_policy_config_s_type *policy_ptr,

       sys_modem_as_id_e_type     asubs_id

);

/*===========================================================================

FUNCTION cmpmprx_sys_sel_pref_policy_update

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean                    cmpmprx_sys_sel_pref_policy_update(

       sys_modem_as_id_e_type     asubs_id,

       cm_sys_sel_pref_params_s_type* pref_ptr
);

/*===========================================================================

FUNCTION cmpmprx_sys_sel_pref_policy_update

DESCRIPTION
  Initialize the Policy Manager object

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
extern boolean         cmpmprx_check_sys_sel_pref_policy_conflict(
  cm_ph_cmd_s_type           *ph_cmd_ptr
);

/*===========================================================================

FUNCTION cmpmprx_is_call_to_reorig

DESCRIPTION
  If UE in SGLTE mode and Hybr GSM not in service, CM may need to modify
  policy and re-evaluate the call as per new policy.

  This is done to allow emergency call to be attempted on other RATs when GSM
  CS service not available in SGLTE.

DEPENDENCIES
  none

RETURN VALUE
  TRUE: Call command requeued.
  False: Proceed with call origination.

SIDE EFFECTS
  none

===========================================================================*/
EXTERN boolean                    cmpmprx_is_call_to_reorig(

       cmcall_s_type              *call_ptr,

       void                       *curr_cmd,

       sys_ue_mode_e_type         curr_msc_ue_mode
);

/*===========================================================================

FUNCTION cmpmprx_msim_subsc_changed

DESCRIPTION
  Called when susbcriptions in multi-mode phones change.
  This function calls into PM to update new susbcriptions.
  PM refreshes policies based on this update.
    - Currently PM shall decide enable/disable LTE in multimode sub
      when subscriptions change. Meaning MSIM standby pref change.
===========================================================================*/
boolean cmpmprx_msim_subsc_changed(

  uint8 active_subs
);

/*===========================================================================

FUNCTION cmpmprx_get_overall_featuremode

DESCRIPTION
  Called from other modules to read the current value of the overall
  featuremode in CMPM
===========================================================================*/
sys_overall_feature_t             cmpmprx_get_overall_featuremode(void);

#ifdef CM_DEBUG
#error code not present
#endif
extern cm_policy_config_s_type    *cmpm_ptr( void );


#endif  /* CMPMPRX_H */
