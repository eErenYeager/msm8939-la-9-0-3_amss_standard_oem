/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

C A L L   M A N A G E R


S U P P L E M E N T A R Y   S E R V I C E   M O D U L E

GENERAL DESCRIPTION
This module contains the Call Manager Sups Object functions for WCDMA/GSM and IP.


EXTERNALIZED FUNCTIONS

cmsups_init
Initializing the sups object.

cmsups_client_cmd_proc
Process call independent sups commands from the clients.

cmsups_rpt_proc
Process Lower Layer sups notifications/command replies.


INITIALIZATION AND SEQUENCING REQUIREMENTS
cmsups_init() must be called to initialize this module before any other
function declared in this module is being called.


Copyright (c) 2012 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/cm/src/cmsups.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/12/13   sm      Add error string for IP SUPS CONF 
11/22/12   xs      Added PS supplemetary service support
===========================================================================*/
#include "cm.h"
#include "cmsups.h"
#include "cmi.h"
#include "cmclient.h"
#include "cmtaski.h"
#include "cmidbg.h"
#include "cmph.h"
#include "cmwsups.h"
#if defined (CM_GW_SUPPORTED) || defined (FEATURE_IP_CALL)
/*===========================================================================

FUNCTION CMSUPS_PTR

DESCRIPTION
Return a pointer to the one and only sups object.

CM sups object.
The sups object is responsible for:
1. Processing clients' sups commands.
2. Processing MN replies for clients' sups commands.
3. Processing MN sups related notifications.
4. Notifying the client list of sups events.

DEPENDENCIES
none

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/
cmsups_s_type  *cmsups_ptr( void )
{
  static cmsups_s_type cmsups;
  /* The one and only sups object */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return &cmsups;
}

/*===========================================================================

FUNCTION CMSUPS_INIT_SUPS_INFO

DESCRIPTION
Initialize sups info buffer.

DEPENDENCIES
sups_info_ptr should be a valid non-NULL pointer

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/

void cmsups_init_sups_info(cm_sups_info_s_type * sups_info_ptr)
{

  /* Initialize sups parameters
  */
  memset (sups_info_ptr, CM_DEFAULT_VALUE, sizeof(cm_sups_info_s_type));

  /* client id */
  sups_info_ptr->sups_client_id                 = CM_CLIENT_ID_ANONYMOUS;

  /* Reset sups cmd reference  field */
  sups_info_ptr->ss_ref                         = CM_SS_REF_UNASSIGNED;

  /* Reset ss code  field */
  sups_info_ptr->ss_code                        = CM_SS_CODE_UNASSIGNED;

  /* Reset ss operation field */
  sups_info_ptr->ss_operation                   = CM_SS_OPERATION_UNASSIGNED;

  /* Reset ss invoke id field */
  sups_info_ptr->invoke_id                      = CM_SS_INVOKE_ID_UNASSIGNED;


  /* Reset guidance_info */
  sups_info_ptr->guidance_info                  = enterPW;

  /* Reset conf_type to no info */
  sups_info_ptr->conf_type                      = NO_INFO;

  /* Reset uss_data_type */
  sups_info_ptr->uss_data_type                  = CM_USS_DATA_NONE;

#if (defined(FEATURE_CM_MMGPS) || defined(FEATURE_CM_LCS))

  /* LCS MOLR Response does not carry any data */
  sups_info_ptr->lcs_molr_res.data_type              = NO_INFO;

#endif

  /* Reset client id */
  sups_info_ptr->requesting_client_id           = CM_CLIENT_ID_ANONYMOUS;

  /* Reset ccbs indx */
  sups_info_ptr->ccbs_indx                      = CM_CCBS_INDX_INVALID;
  
  #if defined (FEATURE_IP_CALL) && defined (FEATURE_CM_LTE)
  sups_info_ptr->sups_ss_error_text = NULL;
  sups_info_ptr->sups_time_info.time_present = FALSE;
  #endif

}


/*===========================================================================

FUNCTION CMSUPS_INFO_ALLOC

DESCRIPTION
Allocate a sups info buffer.

DEPENDENCIES
none

RETURN VALUE
Pointer to allocated sups info struct.

SIDE EFFECTS
none

===========================================================================*/
cm_sups_info_s_type  *cmsups_info_alloc( void )
{
  cm_sups_info_s_type  *sups_info_ptr;

  /*
  ** Dynamically allocate a sups info object
  */
  sups_info_ptr = (cm_sups_info_s_type *)cm_mem_malloc(
    sizeof(cm_sups_info_s_type));
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Initialize sups parameters
  */

  cmsups_init_sups_info(sups_info_ptr);

  /* Return a pointer to the dynamically allocated sups info buffer. */

  return sups_info_ptr;

}

/*===========================================================================

FUNCTION CMSUPS_GET_SUPS_INFO_PTR

DESCRIPTION
Return a pointer to Phase 2 ussd request message sent.

DEPENDENCIES
none

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/

cm_sups_info_s_type* cmsups_get_sups_info_ptr(

  cm_ss_e_type      ss
  /* stack whose sups info ptr to return */
  )
{
  if( ss == CM_SS_HYBR_2 )
  {
    return cmsups_ptr()->hybr_gw_sups_info;
  }
  else
    #if defined(FEATURE_MMODE_TRIPLE_SIM) || defined(FEATURE_MMODE_SXLTE_G)
  if( ss == CM_SS_HYBR_3 )
  {
    return cmsups_ptr()->hybr_gw3_sups_info;
  }
  else
 #endif
  {
    return cmsups_ptr()->sups_info;
  }
}

/*===========================================================================

FUNCTION: CMSUPS_INFO_GET

DESCRIPTION
Copy the current sups event information into a specified buffer.

DEPENDENCIES
SUPS object must have already been initialized with
cmsups_init().

RETURN VALUE
TRUE if information is valid, FALSE otherwise.

Note that the sups information only becomes valid after cmsups_init()
returns.

SIDE EFFECTS
none

===========================================================================*/
boolean cmsups_info_get(

                        cm_ss_e_type                 ss,
                        /* stack whose sups info needs to be retrieved */

                        cm_sups_info_s_type          *sups_info_ptr
                        /* Copy the event info into this buffer */

                        )
{

  cmsups_s_type       *sups_ptr  =  cmsups_ptr();
  /* get the SUPS object */

  cm_sups_info_s_type  *info_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT( sups_info_ptr != NULL );
  CM_ASSERT( sups_ptr != NULL );

  /* Verify that object was properly initialized.
  */
  CM_ASSERT( CM_INIT_CHECK(sups_ptr->init_mask) );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info_ptr = cmsups_get_sups_info_ptr(ss);


  /* If we have not allocated memory for info_ptr, that means
  ** no sups info need to be copied. Return from here.
  */
  if ( info_ptr == NULL )
  {
    /* Return boolean indicating whether the
    * information is valid or not
    */
    return (CM_BOOLEAN( CM_INIT_CHECK(sups_ptr->init_mask) ) );
  } 


  /* Copy sups parameters into sups_info struct from the sups object.
  */
  *sups_info_ptr = *info_ptr;

  sups_info_ptr->sups_client_id = cmclient_get_client_id( sups_ptr->sups_client_ptr );

  sups_info_ptr->asubs_id = cmsups_get_asubs_id(info_ptr);

  CM_MSG_MED_3 ("sups info get. mod_ss_params: cc result:%d call_id:%d length: %d",
    sups_info_ptr->mod_ss_params.call_control_result,
    sups_info_ptr->mod_ss_params.call_id, sups_info_ptr->cc_result_alpha.len);
  CM_MSG_MED_1 ("sups info get. cc_result_alpha : %d" , sups_info_ptr->cc_result_alpha.buf[0]);

  /* Return boolean indicating whether the
  * information is valid or not
  */
  return (CM_BOOLEAN( CM_INIT_CHECK(sups_ptr->init_mask) ) );
}

/*===========================================================================

FUNCTION cmsups_get_asubs_id

DESCRIPTION
  Get asubs_id for the call obj.

DEPENDENCIES
  None.

RETURN VALUE
  sys_modem_as_id_e_type.

SIDE EFFECTS
  None.
===========================================================================*/
sys_modem_as_id_e_type cmsups_get_asubs_id (
  const  cm_sups_info_s_type  *sups_info_ptr
)
{
  CM_ASSERT(sups_info_ptr    != NULL); 

  if (sups_info_ptr == NULL)
  {
    CM_ERR_FATAL_0("sups_info_ptr NULL");
    return SYS_MODEM_AS_ID_NONE;
  }

  if (cmph_is_msim())
  {
    return sups_info_ptr->asubs_id;
  }
  else
  {
    return SYS_MODEM_AS_ID_1;
  }
}


/*===========================================================================

FUNCTION CMSUPS_EVENT

DESCRIPTION
Notify the client list of a specified sups event.

DEPENDENCIES
SUPS object must have already been initialized with cmsups_init().

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/
void cmsups_event(

                  cm_ss_e_type            ss,
                  /* stack whose info is being sent */

                  cm_sups_event_e_type    sups_event
                  /* notify client list of this sups event */
                  )
{
  cm_sups_info_s_type *sups_info_ptr = cmsups_info_alloc();
  /* Allocate a sups info buffer to copy the sups event info*/

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT( BETWEEN(sups_event, CM_SUPS_EVENT_NONE, CM_SUPS_EVENT_MAX) );
  CM_ASSERT( sups_info_ptr != NULL );
  CM_MSG_MED_2( "SUPS event=%d on ss %d",sups_event, ss);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Copy all information into
  ** allocated sups info buffer.
  */
  (void) cmsups_info_get( ss, sups_info_ptr );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Notify clients list of sups event.
  */
  cmclient_list_sups_event_ntfy( sups_event, sups_info_ptr );

  /* Free a sups info buffer after sending sups event info */
  cm_mem_free(sups_info_ptr);

}



/*===========================================================================

FUNCTION cmsups_cmd_copy_fields_into_info

DESCRIPTION
Copy cmd fields into sups info.

DEPENDENCIES
SUPS object must have already been initialized with
cmsups_init().

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/
void cmsups_cmd_copy_fields_into_info(

                                      cm_sups_info_s_type         *sups_info_ptr,
                                      /* Pointer to info part of sups object */

                                      const cm_sups_cmd_info_s_type    *cmd_info_ptr
                                      /* Pointer to a sups command */

                                      )
{

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT( sups_info_ptr != NULL )

  if (sups_info_ptr == NULL || cmd_info_ptr == NULL)
  {
    CM_ERR_2("sups_info_ptr/cmd_info_ptr NULL ptr", sups_info_ptr, cmd_info_ptr);
    return;
  }
  
    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


    /* SS Code */
    if( cmd_info_ptr->ss_code != CM_SS_CODE_UNASSIGNED )
    {  
      sups_info_ptr->ss_code = cmd_info_ptr->ss_code;
    }

    /* SS operation */
    if( cmd_info_ptr->ss_operation != CM_SS_OPERATION_UNASSIGNED )
    {  
      sups_info_ptr->ss_operation = cmd_info_ptr->ss_operation;
    }

    /* SS reference */
    if( cmd_info_ptr->ss_ref != CM_SS_REF_UNASSIGNED )
    {  
      sups_info_ptr->ss_ref = cmd_info_ptr->ss_ref;
    }

    /* SS invoke ID */
    if( cmd_info_ptr->invoke_id != CM_SS_INVOKE_ID_UNASSIGNED )
    {  
      sups_info_ptr->invoke_id = cmd_info_ptr->invoke_id;
    }

    /* Basic service */
    if( cmd_info_ptr->basic_service.present )
    {  
      sups_info_ptr->basic_service =  cmd_info_ptr->basic_service;
    }

    /* Forwarded to Number */
    if( cmd_info_ptr->forwarded_to_number.present )
    {  
      sups_info_ptr->forwarded_to_number = cmd_info_ptr->forwarded_to_number;
    }

    /* Forwarded to subaddress */
    if( cmd_info_ptr->forwarded_to_subaddress.present )
    {  
      sups_info_ptr->forwarded_to_subaddress =  cmd_info_ptr->forwarded_to_subaddress;
    }

    /* No Reply Condition Timer */
    if( cmd_info_ptr->nr_timer.present)
    {  
      sups_info_ptr->nr_timer = cmd_info_ptr->nr_timer;
    }

    /* SS password */
    if( cmd_info_ptr->ss_password.present )
    {  
      sups_info_ptr->ss_password = cmd_info_ptr->ss_password;
    }

    /* SS new password */
    if( cmd_info_ptr->ss_new_password.present)
    {  
      sups_info_ptr->ss_new_password = cmd_info_ptr->ss_new_password;
    }

    /* SS new password confirmed */
    if( cmd_info_ptr->ss_new_password_again.present)
    {  
      sups_info_ptr->ss_new_password_again = cmd_info_ptr->ss_new_password_again;
    }

    if( cmd_info_ptr->code.present )
    {  
      sups_info_ptr->code = cmd_info_ptr->code;
    }

  if( cmd_info_ptr->call_barring_num_list)
  {
	sups_info_ptr->call_barring_num_list = cmd_info_ptr->call_barring_num_list;
        CM_MSG_HIGH_0("Copied call_barring_num_list to sups_info_ptr");
  }

    /* uss response type */
    if( cmd_info_ptr->uss_res_type == CM_USS_RES_SS_ERROR )
    {
      sups_info_ptr->ss_error = cmd_info_ptr->ss_error;
      sups_info_ptr->conf_type = SS_ERROR_INFO;
    }
    else if ( cmd_info_ptr->uss_res_type == CM_USS_RES_SS_DATA )
    {
      sups_info_ptr->uss_data = cmd_info_ptr->uss_data;
      sups_info_ptr->conf_type = USS_DATA_INFO;
    }

    CM_MSG_MED_3 ("Copied uss data res type: %d conf type: %d error: %d", 
      cmd_info_ptr->uss_res_type, 
      sups_info_ptr->conf_type,
      sups_info_ptr->ss_error.error_code);

    /* Copy ccbs index supplied by client
    */
    if (cmd_info_ptr->ccbs_indx != CM_CCBS_INDX_INVALID)
    {
      sups_info_ptr->ccbs_indx = cmd_info_ptr->ccbs_indx;
    }

    sups_info_ptr->asubs_id = cmd_info_ptr->cmd_subs_id;

} /* cmsups_cmd_copy_fields_into_info () */

/*===========================================================================

FUNCTION cmsups_cmd_copy_fields

DESCRIPTION
Copy the active fields of a sups command into the sups object.

DEPENDENCIES
SUPS object must have already been initialized with
cmsups_init().

RETURN VALUE
none

SIDE EFFECTS
none

===========================================================================*/
void cmsups_cmd_copy_fields(

                            cmsups_s_type               *sups_ptr,
                            /* Pointer to the sups object */

                            const cm_sups_cmd_s_type    *cmd_ptr
                            /* Pointer to a sups command */

                            )
{
  cm_sups_info_s_type               *sups_info_ptr;

  cm_ss_e_type                       sups_cmd_ss = CM_SS_NONE;


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT( sups_ptr != NULL )
  CM_ASSERT( cmd_ptr != NULL )

  if (sups_ptr == NULL || cmd_ptr == NULL)
  {
    CM_ERR_2("sups_ptr or cmd_ptr NULL ptr", sups_ptr, cmd_ptr);
    return;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  {
    sups_cmd_ss = cmph_map_subs_to_ss( cmd_ptr->cmd_info.cmd_subs_id );

    #ifdef CM_GW_SUPPORTED
    if ( cmph_is_subs_feature_mode_sglte( cmd_ptr->cmd_info.cmd_subs_id ) )
    {
      cmwsups_sglte_determine_ss_for_sups_cmd( cmd_ptr->cmd_info.cmd_subs_id,
                                               cmd_ptr->cmd, &sups_cmd_ss );
    }
    #endif

    sups_info_ptr = cmsups_get_sups_info_ptr( sups_cmd_ss );
  }

  if (sups_info_ptr == NULL)
  {
    CM_ERR_1("sups_info_ptr NULL ptr subs_id = %d", 
                  cmd_ptr->cmd_info.cmd_subs_id);
    return;
  }

  if( sups_cmd_ss == CM_SS_HYBR_3 )
  {
    sups_info_ptr = sups_ptr->hybr_gw3_sups_info;
    #ifdef FEATURE_SUPS_RETRY
    if(sups_ptr->hybr_gw3_sups_info_list != NULL)
    {
      sups_ptr->hybr_gw3_sups_info_list->cmd = cmd_ptr->cmd;
      *(&(sups_ptr->hybr_gw3_sups_info_list->sups_cmd_info)) = *(SUPS_CMD_INFO_PTR( cmd_ptr ));
      sups_ptr->hybr_gw3_sups_info_list->cc_result_alpha = cmd_ptr->cc_result_alpha;
      sups_ptr->hybr_gw3_sups_info_list->cc_result = cmd_ptr->cc_result;
      sups_ptr->hybr_gw3_sups_info_list->sups_client_ptr   = cmd_ptr->client_ptr;
    }
    #endif
  }
  else if( sups_cmd_ss == CM_SS_HYBR_2 )
  {
    sups_info_ptr = sups_ptr->hybr_gw_sups_info;
    #ifdef FEATURE_SUPS_RETRY
    if(sups_ptr->hybr_gw_sups_info_list != NULL)
    {
      sups_ptr->hybr_gw_sups_info_list->cmd = cmd_ptr->cmd;

      *(&(sups_ptr->hybr_gw_sups_info_list->sups_cmd_info)) = *(SUPS_CMD_INFO_PTR( cmd_ptr ));
      sups_ptr->hybr_gw_sups_info_list->cc_result_alpha = cmd_ptr->cc_result_alpha;
      sups_ptr->hybr_gw_sups_info_list->cc_result = cmd_ptr->cc_result;
      sups_ptr->hybr_gw_sups_info_list->sups_client_ptr   = cmd_ptr->client_ptr;
    }
    #endif
  }
  else
  {
    sups_info_ptr = sups_ptr->sups_info;
    #ifdef FEATURE_SUPS_RETRY
    if(sups_ptr->sups_info_list != NULL)
    {
      sups_ptr->sups_info_list->cmd = cmd_ptr->cmd;

      *(&(sups_ptr->sups_info_list->sups_cmd_info)) = *(SUPS_CMD_INFO_PTR( cmd_ptr ));
      sups_ptr->sups_info_list->cc_result_alpha = cmd_ptr->cc_result_alpha;
      sups_ptr->sups_info_list->cc_result = cmd_ptr->cc_result;
      sups_ptr->sups_info_list->sups_client_ptr = cmd_ptr->client_ptr;
    }
    #endif
  }

  /* client information */
  sups_ptr->sups_client_ptr = cmd_ptr->client_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  cmsups_cmd_copy_fields_into_info (sups_info_ptr, SUPS_CMD_INFO_PTR(cmd_ptr) );

  /* Copy Alpha from CC result */
  sups_info_ptr->cc_result_alpha = cmd_ptr->cc_result_alpha;

  /* Callc ontrol result */
  sups_info_ptr->mod_ss_params.call_control_result = cmd_ptr->cc_result;

}

cm_sups_event_e_type cmsups_cmd_to_event_map(cm_sups_cmd_e_type sups_cmd)
{
  switch(sups_cmd)
  {
  case CM_SUPS_CMD_ACTIVATE:
    return CM_SUPS_EVENT_ACTIVATE;
  case CM_SUPS_CMD_DEACTIVATE:
    return CM_SUPS_EVENT_DEACTIVATE;
  case CM_SUPS_CMD_ERASE:
    return CM_SUPS_EVENT_ERASE;
  case CM_SUPS_CMD_INTERROGATE:
    return CM_SUPS_EVENT_INTERROGATE;
  case CM_SUPS_CMD_REGISTER:
    return CM_SUPS_EVENT_REGISTER;
  case CM_SUPS_CMD_PROCESS_USS:
    return CM_SUPS_EVENT_PROCESS_USS;
  case CM_SUPS_CMD_REG_PASSWORD:
    return CM_SUPS_EVENT_REG_PASSWORD;
  case CM_SUPS_CMD_RELEASE:
    return CM_SUPS_EVENT_RELEASE;
  case CM_SUPS_CMD_GET_PASSWORD_RES:
    return CM_SUPS_EVENT_GET_PASSWORD_RES;
  case CM_SUPS_CMD_USS_NOTIFY_RES:
    return CM_SUPS_EVENT_USS_NOTIFY_RES;
  case CM_SUPS_CMD_USS_RES:
    return CM_SUPS_EVENT_USS_RES;
  case CM_SUPS_CMD_LCS_LOCATION_NOTIFICATION_RES:
    return CM_SUPS_EVENT_LCS_LOCATION_NOTIFICATION_RES;
  case CM_SUPS_CMD_LCS_MOLR:
    return CM_SUPS_EVENT_LCS_MOLR;
  case CM_SUPS_CMD_LCS_MOLR_COMPLETED:
    return CM_SUPS_EVENT_LCS_MOLR_COMPLETED;
  case CM_SUPS_CMD_ERASE_CC_ENTRY:
    return CM_SUPS_EVENT_DEACTIVATE;
  default:
    CM_ERR_1("cmsups_cmd_map: unsupport sups command %d",sups_cmd);
    return CM_SUPS_EVENT_NONE;
  }
}

cm_sups_event_e_type cmsups_conf_to_event_map(cm_name_type           sups_conf)
{
  switch(sups_conf)
  {
  case CM_IP_SUPS_ACTIVATE_SS_CONF:
    return CM_SUPS_EVENT_ACTIVATE_CONF;
  case CM_IP_SUPS_DEACTIVATE_SS_CONF:
    return CM_SUPS_EVENT_DEACTIVATE_CONF;
  case CM_IP_SUPS_ERASE_SS_CONF:
    return CM_SUPS_EVENT_ERASE_CONF;
  case CM_IP_SUPS_INTERROGATE_SS_CONF:
    return CM_SUPS_EVENT_INTERROGATE_CONF;
  case CM_IP_SUPS_REGISTER_SS_CONF:
    return CM_SUPS_EVENT_REGISTER_CONF;
  case CM_ACTIVATE_SS_CONF:
    return CM_SUPS_EVENT_ACTIVATE_CONF;
  case CM_DEACTIVATE_SS_CONF:
    return CM_SUPS_EVENT_DEACTIVATE_CONF;
  case CM_ERASE_SS_CONF:
    return CM_SUPS_EVENT_ERASE_CONF;
  case CM_INTERROGATE_SS_CONF:
    return CM_SUPS_EVENT_INTERROGATE_CONF;
  case CM_REGISTER_PASSWORD_CONF:
    return CM_SUPS_EVENT_REG_PASSWORD_CONF;
  case CM_REGISTER_SS_CONF:
    return CM_SUPS_EVENT_REGISTER_CONF;
  case CM_PROCESS_UNSTRUCTURED_SS_CONF:
  case CM_IP_SUPS_PROCESS_UNSTRUCTURED_SS_CONF:
    return CM_SUPS_EVENT_PROCESS_USS_CONF;
  case CM_PROCESS_UNSTRUCTURED_SS_DATA_CONF:
    return CM_SUPS_EVENT_PROCESS_USS_CONF;
  case CM_UNSTRUCTURED_SS_NOTIFY_IND:
    return CM_SUPS_EVENT_USS_NOTIFY_IND;
  case CM_GET_PASSWORD_IND:
    return CM_SUPS_EVENT_GET_PASSWORD_IND;
  case CM_RELEASE_UNSTRUCTURED_SS_IND:
  case CM_IP_SUPS_RELEASE_UNSTRUCTURED_SS_IND:
    return CM_SUPS_EVENT_RELEASE_USS_IND;
  case CM_LCS_LOCATION_NOTIFICATION_IND:
    return CM_SUPS_EVENT_LCS_LOCATION_NOTIFICATION_IND;
  case CM_LCS_MOLR_CONF:
    return CM_SUPS_EVENT_LCS_MOLR_CONF;
  case CM_ERASE_CC_ENTRY_CONF:
    return CM_SUPS_EVENT_DEACTIVATE_CONF;
  case CM_UNSTRUCTURED_SS_IND:
  case CM_IP_SUPS_UNSTRUCTURED_SS_IND:
    return CM_SUPS_EVENT_USS_IND;
  case CM_FORWARD_CHECK_SS_INDICATION_IND:
    return CM_SUPS_EVENT_FWD_CHECK_IND;
  default:
    CM_ERR_1("cmsups_conf_map: unsupport sups cnf %d",sups_conf);
    return CM_SUPS_EVENT_NONE;
  }
}

/*===========================================================================

FUNCTION cmsups_is_there_pending_sups_on_other_subs

DESCRIPTION
Check if there is a pending SUPS on other subs

RETURN VALUE
TRUE: There is pending SUPS on other subs
FALSE: There is no pending SUPS on other subs

SIDE EFFECTS
none

===========================================================================*/

boolean cmsups_is_there_pending_sups_on_other_subs(
  sys_modem_as_id_e_type        asubs_id
)
{
  cmsups_s_type          *sups_ptr         = cmsups_ptr();
  if ( sups_ptr->num_pending_sups_cmds == 0)
  {
    return FALSE;
  }

  if ( sups_ptr->sups_info != NULL &&
       sups_ptr->sups_info->asubs_id != asubs_id )
  {
    return TRUE;
  }

  if ( sups_ptr->hybr_gw_sups_info != NULL &&
       sups_ptr->hybr_gw_sups_info->asubs_id != asubs_id )
  {
    return TRUE;
  }

  #if defined(FEATURE_MMODE_TRIPLE_SIM) || defined(FEATURE_MMODE_SXLTE_G)
  if ( sups_ptr->hybr_gw3_sups_info != NULL &&
       sups_ptr->hybr_gw3_sups_info->asubs_id != asubs_id )
  {
    return TRUE;
  }
  #endif

  return FALSE;
}

#endif
