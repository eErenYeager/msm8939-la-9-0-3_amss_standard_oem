/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               S Y S T E M   D E T E R M I N A T I O N

                       SD EFS  I N T E R F A C E

GENERAL DESCRIPTION
  This file makes up the EFS-Interface component of the SD.

  The SD EFS-Interface component encapsulates all the functionality that is
  associated with reading and writing from/to EFS SD related items, such as
  MRU table.

EXTERNALIZED FUNCTIONS
  sdefs_write
  sdefs_read

REGIONAL FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  sdefs_init() must be call to initialize this component before any other
  function declared in this component is called.


Copyright (c) 2009 - 2012 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* <EJECT> */
/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/sd/src/sdefs.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/19/13   dj      New EFS Item added for 1XCSFB_ECBM feature.
10/09/12   sr      Support  to derive MCC of an EvDo system from eqprl.
08/17/12   gm      Mainline EFS feature - Remove FEATURE_EFS
07/17/12   jh      Added LTE Available File BSR support
05/11/12   jh      Fix SDEFS regular files/item files read handling
03/23/12   gm      DIME integration fix: TMC deprecation and others.
16/02/12   vk      Replace usage of AEE library function with corresponding
                   CoreBSP library functions
05/17/11   rn      WCDMA channel locking feature
04/22/11   cl      Fix how SD handles EFS_WRITE function
03/10/11   gm      Add QXDM NV Browser support for ECBM timer.
02/16/11   gm      Extend acquisition while in ECBM & sys loss
02/10/11   am      Adding QTF support.
02/10/11   xs      Fix JCDMA NV reading issue
02/07/11   xs      Dynamic FEATURE_JCDMA control in EFS
07/19/10   aj      Update sdefs_write() to truncate existing filesize
                   to 0 before writing new values.
07/14/10   mh      Fixed compile warnings
06/28/10   ay      Added EOOS
06/18/10   ay      Added EFS based MRU
06/18/10   aj      add validation for mapping function
05/18/10   rn      Added rat_pri_list_info in get_net_gw
05/12/10   aj      Add support to save equivalent PRL in EFS
03/22/10   aj      updated default TOT order
02/17/10   rn      Get UE_cap from EFS
02/10/09   rn      Initial revision
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "mmcp_variation.h"
#include "sd.h"       /* External interface to sd.c */
#include "sd_v.h"
#include "sddbg.h"    /* SD debug macros */
#include "msg.h"      /* Message output services */
#include "err.h"      /* Error and error fatal services */
#include "sdefs.h"    /* SD EFS interface */
#include "fs_public.h"  /* Interface to EFS services */
#include "fs_errno.h"   /* EFS error numbers */

#include <stringl/stringl.h>

#include "sdss.h"
#include "sdssscr.h"

#ifdef FEATURE_MMODE_QTF
#error code not present
#endif /* FEATURE_MMODE_QTF */


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

===========================================================================*/


/* Structure for EFS table
*/
typedef struct sdefs_tbl_s
{
   sdefs_id_e_type            efs_id;       /* EFS id */
   const char                *file_name;    /* EFS file name */
} sdefs_tbl_s_type;


/*
** EFS table that maps EFS Id to filename
*/
static const sdefs_tbl_s_type sdefs_tbl[] =
{
   {SDEFS_MRU_NAM_1,           SDEFS_MMODE_MRU_NAM_1_F_NAME} ,
   {SDEFS_MRU_NAM_2,           SDEFS_MMODE_MRU_NAM_2_F_NAME} ,
   {SDEFS_MMODE_BAND_PREF,     SDEFS_MMODE_BAND_PREF_F_NAME} ,
   {SDEFS_RAT_ACQ_ORDER,       SDEFS_RAT_ACQ_ORDER_F_NAME  } ,
   {SDEFS_MMSS_TIMER,          SDEFS_MMSS_TIMER_F_NAME     } ,
   {SDEFS_BST_TBL,             SDEFS_BST_TBL_F_NAME        } ,
   {SDEFS_MMSS_EQPRL,          SDEFS_MMSS_EQPRL_F_NAME     } ,
   {SDEFS_MMSS_IGNORE_UPLMN,   SDEFS_MMSS_IGNORE_UPLMN_F_NAME },
   {SDEFS_FEATURE_JCDMA,       SDEFS_FEATURE_FLAG_CFG_F_NAME },
   {SDEFS_SDSSSCR_TIMERS,      SDEFS_SDSSSCR_TIMERS_F_NAME } ,
   {SDEFS_LTE_AVAIL_FILE,      SDEFS_LTE_AVAIL_FILE_F_NAME } ,
   {SDEFS_LTE_NOT_AVAIL_FILE,  SDEFS_LTE_NOT_AVAIL_FILE_F_NAME } ,
   {SDEFS_WCDMA_FREQ_LOCK,     SDEFS_WCDMA_FREQ_LOCK_F_NAME} ,
   {SDEFS_LBSR_MCC_LIST,       SDEFS_LBSR_MCC_LIST_F_NAME  } ,
   {SDEFS_FULL_SRV_REQ_EMERG,  SDEFS_FULL_SRV_REQ_EMERG_F_NAME },
   {SDEFS_QMSS_ENABLED,        SDEFS_QMSS_ENABLED_F_NAME   },
   {SDEFS_OPERATOR_NAME,       SDEFS_OPERATOR_NAME_F_NAME  },
   {SDEFS_1XCSFB_ECBM,         SDEFS_1XCSFB_ECBM_F_NAME    },
   {SDEFS_CUSTOM_EMERG_INFO,   SDEFS_CUSTOM_EMERG_INFO_F_NAME},
   {SDEFS_LOCK_SID_NID,        SDEFS_LOCK_SID_NID_F_NAME   },
   {SDEFS_SCAN_SCOPE_RULE,     SDEFS_SCAN_SCOPE_RULE_F_NAME  },
   {SDEFS_MANUAL_SEARCH_IN_WRLF,  SDEFS_MANUAL_SEARCH_IN_WRLF_F_NAME},
   {SDEFS_CSG_AUTO_MODE,       SDEFS_CSG_AUTO_MODE_NAME },
   {SDEFS_EHRPD_OPT_REDIAL,    SDEFS_EHRPD_OPT_REDIAL_F_NAME },
   {SDEFS_C2K_RESEL_SPLMN,  SDEFS_C2K_RESEL_SPLMN_F_NAME},
   {SDEFS_BUFFER_INT_SRV_LOST,    SDEFS_BUFFER_INT_SRV_LOST_F_NAME},
   {SDEFS_MANUAL_SRCH_BST_TBL, SDEFS_MANUAL_SRCH_BST_TBL_F_NAME},
   {SDEFS_1XCSFB_CALL_END_OPT, SDEFS_1XCSFB_CALL_END_OPT_F_NAME },
   {SDEFS_RECENTLY_ACQUIRED_LIST, SDEFS_RECENTLY_ACQUIRED_LIST_F_NAME},
   {SDEFS_EMERGENCY_RAT_ACQ_ORDER, SDEFS_EMERGENCY_RAT_ACQ_ORDER_F_NAME},
   {SDEFS_INT_MAX,             SDEFS_NO_FILE               } ,
   {SDEFS_MAX,                 SDEFS_NO_FILE               }
};

#ifdef FEATURE_EOOS
    #error code not present
#endif
/* WCDMA channel locking status */
static boolean is_wcdma_freq_lock_efs_read_done = FALSE;

/*===========================================================================

FUNCTION sdefs_map_efs_id_to_filename

DESCRIPTION
  Map EFS id to EFS filename


DEPENDENCIES
  None

RETURN VALUE
  Return TRUE if mapping was successful, else FALSE

SIDE EFFECTS
  None.

===========================================================================*/

static boolean                  sdefs_map_efs_id_to_filename(

        sdefs_id_e_type          efs_id,
            /* passing the efs Id to map.
            */

        char                     *file_name
            /* Pointer to file name.
            */

)
{
  int i;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  SD_ASSERT(file_name != NULL);
  SD_ASSERT_ENUM_IS_INRANGE(efs_id,SDEFS_MAX);
  SD_ASSERT( efs_id != SDEFS_NONE &&
             efs_id != SDEFS_MAX  &&
             efs_id != SDEFS_INT_MAX );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  for(i = 0; i < ARR_SIZE(sdefs_tbl); i++)
  {
     if(sdefs_tbl[i].efs_id == efs_id)
     {
        (void)strlcpy(file_name,
                           sdefs_tbl[i].file_name,
                           SDEFS_FILENAME_MAX_LEN);
        return TRUE;

     }

     if(sdefs_tbl[i].efs_id == SDEFS_MAX)
     {
       break ;
     }
  }

  SD_MSG_HIGH_0("No matching EFS id found");

  /* efs id not found */
  return FALSE;

}

#ifndef FEATURE_DISABLE_SD_INIT_NV_WRITE
/*===========================================================================

FUNCTION sdefs_set_default

DESCRIPTION
  Write default value to EFS file denoted by EFS id

DEPENDENCIES
  None

RETURN VALUE
  Return TRUE if successful, else FALSE

SIDE EFFECTS
  None.

===========================================================================*/
static boolean                  sdefs_set_default(

        sdefs_id_e_type          efs_id
            /* efs Id
            */

)
{
  int i;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  switch(efs_id)
  {

    case SDEFS_MMODE_BAND_PREF:
      {
        sd_mmode_band_pref_s_type mmode_band_pref;
        mmode_band_pref.band_pref     = SD_SS_BAND_PREF_ANY;
        mmode_band_pref.lte_band_pref = SD_SS_BAND_PREF_LTE_ANY;

        return sdefs_write(efs_id,
                            (byte *)&mmode_band_pref,
                             sizeof(sd_mmode_band_pref_s_type));
      }


    case SDEFS_MRU_NAM_1:
      {
        sd_mmode_mru_table_s_type sd_mru_tbl;
        for(i = 0; i < SD_MRU_TABLE_SIZE; i++)
        {
          sd_mru_tbl.entry[i].mode = (byte)SD_MODE_INACT;
        }

        return sdefs_write(efs_id,
                            (byte *)&sd_mru_tbl,
                             sizeof(sd_mmode_mru_table_s_type)); 
      }
    case SDEFS_MRU_NAM_2:
      {
        sd_mmode_mru_table_s_type sd_mru_tbl;

        for(i = 0; i < SD_MRU_TABLE_SIZE; i++)
        {
          sd_mru_tbl.entry[i].mode = (byte)SD_MODE_INACT;
        }

        return sdefs_write(efs_id,
                            (byte *)&sd_mru_tbl,
                             sizeof(sd_mmode_mru_table_s_type));
      }

    case SDEFS_QMSS_ENABLED:
      {
        boolean def_qmss_enabled = FALSE;
        return sdefs_write(efs_id,
                           (byte *)&def_qmss_enabled,
                           sizeof(boolean) );
      }

    case SDEFS_OPERATOR_NAME:
      {
        uint8 def_operator_name[MAX_AS_IDS];
        for(i = 0; i < MAX_AS_IDS; i++)
        {
          def_operator_name[i] = (uint8) SD_OPERATOR_NULL;
        }
        return sdefs_write(efs_id,
                          (byte *)&def_operator_name,
                           sizeof(def_operator_name) );
      }  

    case SDEFS_LOCK_SID_NID:
      {
        sdprl_sid_nid_lock_type sd_lock_sid_nid;
        sd_lock_sid_nid.nam=0;
        for(i = 0; i < SD_MAX_SID_NID_LOCK; i++)
        {
          sd_lock_sid_nid.pair[i].sid = SD_WILDCARD_SID;
          sd_lock_sid_nid.pair[i].nid = SD_WILDCARD_NID;
          sd_lock_sid_nid.pair[i].mnc = SD_IMSI_MNC;
          sd_lock_sid_nid.pair[i].mcc = SD_IMSI_MCC;
        }
        return sdefs_write(efs_id, 
                            (byte *)&sd_lock_sid_nid, 
                             sizeof(sdprl_sid_nid_lock_type));
      }
    case SDEFS_MAX_ACQ_DB_SCAN_TIME:
      {
         unsigned short def_max_acq_db_scan_time = 5;
         return sdefs_write(efs_id,
                            (byte *)&def_max_acq_db_scan_time,
                             sizeof(unsigned short) );
      }

    case SDEFS_T_ACQ_SCAN:
      {
        unsigned short def_t_acq_scan = 10;
        return sdefs_write(efs_id,
                           (byte *)&def_t_acq_scan,
                           sizeof(unsigned short) );
      }

    case SDEFS_ACQ_SCAN_DURATION:
      {
        unsigned short def_acq_scan_duration = 2;
        return sdefs_write(efs_id,
                           (byte *)&def_acq_scan_duration,
                           sizeof(unsigned short) );
      }

    case SDEFS_SCAN_SCOPE_RULE:
      {
        sys_scan_scope_rule_e_type def_scan_scope_type = SYS_SCAN_SCOPE_RULE_FULL_BAND;
        return sdefs_write(efs_id,
                           (byte *)&def_scan_scope_type,
                           sizeof(def_scan_scope_type) );
      }

    case SDEFS_MANUAL_SEARCH_IN_WRLF:
      {
        boolean is_manual_search_in_wrlf = FALSE;
        return sdefs_write(efs_id, (byte *)&is_manual_search_in_wrlf, sizeof(boolean));
      }
      
    case SDEFS_BUFFER_INT_SRV_LOST:
      {
        boolean buffer_int_srv_lost = TRUE;
        return sdefs_write(efs_id,
                           (byte *)&buffer_int_srv_lost,
                           sizeof(boolean) );
      }
    case SDEFS_BST_TBL:
    case SDEFS_MANUAL_SRCH_BST_TBL:
    case SDEFS_MMSS_TIMER:
    case SDEFS_MMSS_EQPRL:
    case SDEFS_MMSS_IGNORE_UPLMN:
    case SDEFS_RAT_ACQ_ORDER:
    case SDEFS_SDSSSCR_TIMERS:
    case SDEFS_LTE_AVAIL_FILE:
    case SDEFS_LTE_NOT_AVAIL_FILE:
    case SDEFS_WCDMA_FREQ_LOCK:
    case SDEFS_CSG_AUTO_MODE:
    case SDEFS_LBSR_MCC_LIST:
    case SDEFS_EHRPD_OPT_REDIAL:
    case SDEFS_1XCSFB_CALL_END_OPT:
    case SDEFS_RECENTLY_ACQUIRED_LIST:
    case SDEFS_EMERGENCY_RAT_ACQ_ORDER:
      break;

    case SDEFS_EOOS_PARAM:
    {
      sdss_eoos_parameters eoos_parameters;
      #ifdef FEATURE_EOOS
      #error code not present
#else
      memset(&eoos_parameters,0,sizeof(eoos_parameters));
      #endif

      return sdefs_write(SDEFS_EOOS_PARAM, (byte *)&eoos_parameters,
                     sizeof(sdss_eoos_parameters) );
    }

    case SDEFS_FEATURE_JCDMA:
    case SDEFS_NONE:
    case SDEFS_INT_MAX:
    case SDEFS_MAX:
    default:
      SD_ERR_1("wrong efs_id %d", efs_id);
      break;
  }

  return FALSE;
}
#endif

#define EFS_DIRECTORY_SEPARATOR '/'

/*===========================================================================

FUNCTION sdefs_create_path

DESCRIPTION
  This function creates directory path in EFS.

DEPENDENCIES
  None

RETURN VALUE
  -1 if error, anything else is success.

SIDE EFFECTS
  None.

===========================================================================*/

static int sdefs_create_path(const char *dir_path)
{
  int result = -1;
  int path_i = 0;
  int dir_i = 0;
  char dir_name[SDEFS_FILENAME_MAX_LEN];

  memset(dir_name, 0, SDEFS_FILENAME_MAX_LEN);

  if(dir_path[path_i] != EFS_DIRECTORY_SEPARATOR)
  {
    SD_MSG_HIGH_0("Error, provide valid absolute path");
    return result;
  }

  dir_name[dir_i++] = dir_path[path_i++];

  while(dir_path[path_i] != '\0')
  {
    if(dir_path[path_i] == EFS_DIRECTORY_SEPARATOR)
    {
      result = efs_mkdir(dir_name,0777);
    }
    dir_name[dir_i++] = dir_path[path_i++];
  }
  result = efs_mkdir(dir_name,0777);

  return result;
}


/*===========================================================================

FUNCTION sdefs_get

DESCRIPTION
Get EFS item denoted by efs_id and copy to data_ptr

  MODE
DEPENDENCIES
  None

RETURN VALUE
  TRUE if read is successful,
  FALSE otherwise.

SIDE EFFECTS
  None.

==========================================================================*/
boolean                  sdefs_get(

        sdefs_id_e_type          efs_id,
            /* passing the efs Id to read.
            */
        byte                     *data_ptr,
            /* Pointer to a data buffer where to store the data that is
            ** associated with the EFS item.
            */
        int32                    size
            /* On input this parameter is the  maximum number
            ** of bytes to be copied into the buffer pointed by data_ptr.
            ** This parameter must not be nil.
            ** On output it is set *size_ptr to the actual number of bytes
            ** that were copied into the buffer pointed by data_ptr.
            */
)
{
  char          filename[SDEFS_FILENAME_MAX_LEN];
  int status;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  SD_ASSERT( data_ptr != NULL );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

   if(!sdefs_map_efs_id_to_filename(efs_id, filename))
   {
      SD_ERR_0("EFS file map failed");
      return FALSE;
   }


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Get data from file.
  */
  status = efs_get( filename, data_ptr, size );

  if( status < 0 )
  {
    SD_MSG_HIGH_1( "efs read error errno %d", efs_errno);
    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/


  return TRUE;
}

/*===========================================================================

FUNCTION sdefs_init

DESCRIPTION
  Initialize SD EFS items.

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void sdefs_init()
{
  int result = -1;
  int32 efs_size = sizeof(jcdma_me_capability);
  #ifndef FEATURE_DISABLE_SD_INIT_NV_WRITE
  int efs_id;
  char file_name[SDEFS_FILENAME_MAX_LEN];
  struct fs_stat file_stat;
  int fd;
  #endif

  if(!sdefs_read(SDEFS_FEATURE_JCDMA, (byte*)sd_jcdma_ptr(), &efs_size))
  {
    SD_MSG_HIGH_0("JCDMA NV not configured!");
    sd_jcdma_ptr()->service = JCDMA_SERVICE_DISABLED;
  }
  else if(JCDMA_SERVICE_ENABLED == sd_jcdma_ptr()->service)
  {
    SD_MSG_HIGH_0("NV JCDMA is enabled.");
  }

  /* Reset the flag with every Init */
  is_wcdma_freq_lock_efs_read_done = FALSE;

  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  result = sdefs_create_path("/sd");
  if(result == -1)
  {
      SD_MSG_HIGH_1("Error to create /sd %d",efs_errno);
      return;
  }
  result = sdefs_create_path("/nv/item_files/modem/mmode/sd");
  if(result == -1)
  {
      SD_MSG_HIGH_1("Error to create sdssscr_timers error %d",efs_errno);
      return;
  }

  #ifndef FEATURE_DISABLE_SD_INIT_NV_WRITE
  for(efs_id=0; efs_id < (int)SDEFS_INT_MAX; efs_id++)
  {
    if(!sdefs_map_efs_id_to_filename((sdefs_id_e_type)efs_id, file_name))
    {
      SD_ERR_0("EFS file map failed");
      return;
    }

    if(efs_stat (file_name, &file_stat) != 0 && efs_errno == ENOENT)
    {

      SD_MSG_HIGH_1("Efs %d not present yet", efs_id);

      if((fd = efs_creat( file_name, ALLPERMS )) != -1)
      {
        SD_MSG_HIGH_1("Efs %d created", efs_id);

        /* Close the file */
        (void) efs_close( fd );

        if(sdefs_set_default((sdefs_id_e_type)efs_id))
        {
          SD_MSG_HIGH_0("EFS written");
        }
      }
    }
  }
  #endif

  return;
}

/*===========================================================================

FUNCTION sdefs_read_item_files

DESCRIPTION
Read EFS item denoted by efs_id and copy to data_ptr
This is only used for SDEFS NV ITEMS files under /nv/item_files/

  MODE
DEPENDENCIES
  None

RETURN VALUE
  TRUE if read is successful,
  FALSE otherwise.

SIDE EFFECTS
  None.

==========================================================================*/
static boolean                  sdefs_read_item_files(

        sdefs_id_e_type          efs_id,
            /* passing the efs Id to read.
            */
        byte                     *data_ptr,
            /* Pointer to a data buffer where to store the data that is
            ** associated with the EFS item.
            */
        int32                    *size_ptr
            /* On input this parameter is the  maximum number
            ** of bytes to be copied into the buffer pointed by data_ptr.
            ** This parameter must not be nil.
            ** On output it is set *size_ptr to the actual number of bytes
            ** that were copied into the buffer pointed by data_ptr.
            */
)
{
  char          file[SDEFS_FILENAME_MAX_LEN];
  int           ret_val = 0;     /* return value for efs_put */

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  SD_ASSERT( data_ptr != NULL );
  SD_ASSERT( size_ptr != NULL );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(!sdefs_map_efs_id_to_filename(efs_id, file))
  {
     SD_ERR_0("EFS file map failed");
     return FALSE;
  }

  SD_MSG_HIGH_1( "reading file: %s using efs_get", file);

  ret_val = efs_get( file, data_ptr, *size_ptr );

  if( ret_val < 0 )
  {
    SD_MSG_HIGH_2( "efs read error %d errno %d", ret_val, efs_errno);
    *size_ptr = 0;
    return FALSE;
  }
  SD_MSG_LOW_1( "ret_val=%d", ret_val);

  return TRUE;
}


/*===========================================================================

FUNCTION sdefs_read

DESCRIPTION
Read EFS item denoted by efs_id and copy to data_ptr

  MODE
DEPENDENCIES
  None

RETURN VALUE
  TRUE if read is successful,
  FALSE otherwise.

SIDE EFFECTS
  None.

==========================================================================*/
boolean                  sdefs_read(

        sdefs_id_e_type          efs_id,
            /* passing the efs Id to read.
            */
        byte                     *data_ptr,
            /* Pointer to a data buffer where to store the data that is
            ** associated with the EFS item.
            */
        int32                    *size_ptr
            /* On input this parameter is the  maximum number
            ** of bytes to be copied into the buffer pointed by data_ptr.
            ** This parameter must not be nil.
            ** On output it is set *size_ptr to the actual number of bytes
            ** that were copied into the buffer pointed by data_ptr.
            */
)
{
  int                         fd;
  char          file[SDEFS_FILENAME_MAX_LEN];
  int32                       file_size = 0;
  int32                       size_requested = 0;
  int32                       byte_read = 0;
  struct fs_stat              file_stat;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  SD_ASSERT( data_ptr != NULL );

  /* cannot read this EFS due to null size_ptr */
  if(size_ptr == NULL)
  {
    return FALSE;
  }

  size_requested = *size_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

   if(!sdefs_map_efs_id_to_filename(efs_id, file))
   {
      SD_ERR_0("EFS file map failed");
      return FALSE;
   }

  /* If the file is NV ITEM Files, redirect to sdefs_read_item_files() */
  if(strncmp(SDEFS_ITEM_FILES_PREFIX,file,strlen(SDEFS_ITEM_FILES_PREFIX)) == FALSE)
  {
   return sdefs_read_item_files(efs_id, data_ptr,size_ptr);
  }

  /* get the efs actual file size */
  if((efs_stat(file, &file_stat) == 0))
  {
    file_size = (int32)file_stat.st_size;

    if (file_size <= 0)
    {
      SD_MSG_MED_1(" File size error: size %d", file_size);
      return FALSE;
    }

    SD_MSG_MED_2("file_size:%d, size_requested:%d", file_size, size_requested);
  }
  else
  {
    SD_ERR_2(" EFS_STAT ERROR: file-%d, errno:%d", efs_id, efs_errno);
    return FALSE;
  }

  /* Open the specified resource file.
  ** If open fails, return error code
  */
  SD_MSG_HIGH_2( "Open file=%s buffer size %d",
               file, *size_ptr);

  fd = efs_open( file, O_RDONLY );

  if( fd < 0 )
  {
    SD_MSG_HIGH_2( "open efs_err=%d errno %d", fd, efs_errno);

    return FALSE;
  }

  /* The max size for one efs_read() operation dependes on the file system.
  ** To support a big size file read operation, we need to run efs_read until
  ** we read the file upto the size requested or it reaches at the end of the file.
  */
  while((byte_read < size_requested) &&
        (file_size != byte_read))
  {
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Read data from file.
  */
    *size_ptr = efs_read( fd,
                          (byte*)data_ptr+byte_read,
                         (fs_size_t)(size_requested-byte_read) );

  if( *size_ptr < 0 )
  {
    SD_MSG_HIGH_2( "efs read error %d errno %d", *size_ptr, efs_errno);
    *size_ptr = 0;
    (void) efs_close( fd );
    return FALSE;
  }

    byte_read += *size_ptr;

  /* Return the number of bytes actually read.
  */
    SD_MSG_MED_2("size_req=%d, size_read=%d", size_requested, *size_ptr);
  }

  *size_ptr = byte_read;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Close the file */
  (void) efs_close( fd );


  return TRUE;
}



/*===========================================================================

FUNCTION sdefs_write

DESCRIPTION
Write contents of data_ptr in EFS item denoted by efs_id

  MODE
DEPENDENCIES
  None

RETURN VALUE
  TRUE if write is successful,
  FALSE otherwise.

SIDE EFFECTS
  None.

===========================================================================*/

boolean                  sdefs_write(

        sdefs_id_e_type          efs_id,
            /* passing the efs Id to write.
            */
        const byte               *data_ptr,
            /* Pointer to a data buffer to write to EFS.
            */
        int32                    size
            /* Size of the data_ptr buffer.
            */
)
{
  int                         fd;
  int32                       size_committed;
  char        file_name[SDEFS_FILENAME_MAX_LEN];
  int32                       byte_written = 0;
  byte*                       next_block = (byte*)data_ptr;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  SD_ASSERT( data_ptr != NULL );
  SD_ASSERT( size != 0 );

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

   if(!sdefs_map_efs_id_to_filename(efs_id, file_name))
   {
      SD_ERR_0("EFS file map failed");
      return FALSE;
   }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Open the specified resource file.
  ** If open fails, return error code
  */
  SD_MSG_HIGH_1( "Open file=%s",file_name);

  fd =  efs_open( file_name, O_WRONLY | O_CREAT | O_TRUNC);

  if ( fd < 0 )
  {
    SD_MSG_HIGH_2( "open efs_err=%d errno %d", fd, efs_errno);

    return FALSE;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Write data to file.
  */

  while(byte_written < size)
  {

    //commit next block
    size_committed = efs_write(fd, next_block, (fs_size_t)(size - byte_written));

    if ( size_committed <= 0 )
    {
      SD_MSG_HIGH_2( "write efs_err=%d errno %d", size_committed , efs_errno);

      /* Close the file */
      (void) efs_close( fd );

      return FALSE;
    }

    //Update number of byte written
    byte_written += size_committed;

    //update offset pointer
    next_block += size_committed;

  } //while loop

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Return the number of bytes actually written.
  */
  SD_MSG_HIGH_2( "size_req=%d, size_wrote=%d",
               size, byte_written);


  /* Close the file */
  (void) efs_close( fd );

  return TRUE;

}

#ifdef FEATURE_EOOS
#error code not present
#endif

/*===========================================================================

FUNCTION sd_jcdma_ptr

DESCRIPTION
  Return the pointer to feature


DEPENDENCIES
  None

RETURN VALUE
  pointer of the jcdma feature set

SIDE EFFECTS
  None.

===========================================================================*/
jcdma_me_capability* sd_jcdma_ptr(void )
{
  static jcdma_me_capability sd_feature_jcdma;

  return &sd_feature_jcdma;
}

/*===========================================================================

FUNCTION sd_is_jcdma_enable

DESCRIPTION
  If a specified feature is enabled return TRUE, otherwise return FALSE


DEPENDENCIES
  None

RETURN VALUE
  TRUE
  FALSE
SIDE EFFECTS
  None.

===========================================================================*/
boolean sd_is_jcdma_enable(void)
{
  jcdma_me_capability *feature_jcdma = sd_jcdma_ptr();

  switch(feature_jcdma->service)
  {
  case JCDMA_SERVICE_ENABLED:
    return TRUE;
  case JCDMA_SERVICE_DISABLED:
  default:
    return FALSE;
  }
}

/*===========================================================================

FUNCTION sdefs_wcdma_freq_lock_ptr

DESCRIPTION
  Return the pointer to WCDMA Freq Lock status


DEPENDENCIES
  None

RETURN VALUE
  pointer of the WCDMA Freq Lock status

SIDE EFFECTS
  None.

===========================================================================*/
uint16* sdefs_wcdma_freq_lock_ptr(void )
{
  static uint16 sd_wcdma_freq_lock = 0;

  if(!is_wcdma_freq_lock_efs_read_done)
  {
    if(!sdefs_get(SDEFS_WCDMA_FREQ_LOCK, (byte*)&sd_wcdma_freq_lock, sizeof(uint16)))
    {
      SD_MSG_HIGH_0("WCDMA Freq Lock EFS read failed!");
      sd_wcdma_freq_lock = 0;
    }
    else
    {
      is_wcdma_freq_lock_efs_read_done = TRUE;
      SD_MSG_HIGH_1("WCDMA Freq Lock status %d",sd_wcdma_freq_lock);
    }
  }

  return &sd_wcdma_freq_lock;
}
/*===========================================================================

FUNCTION sdefs_csg_auto_mode_bsr_disable

DESCRIPTION
  Returns true if bsr is disabled in NV


DEPENDENCIES
  None

RETURN VALUE
  boolean

SIDE EFFECTS
  None.

===========================================================================*/
boolean sdefs_csg_auto_mode_bsr_disable(void)
{
  sdss_efs_get_global_mode_csg csg_auto_mode;

  if(!sdefs_get(SDEFS_CSG_AUTO_MODE, (byte*)&csg_auto_mode, sizeof(sdss_efs_get_global_mode_csg)))
  {
    SD_MSG_HIGH_0("CSG Auto mode EFS read failed!");
    csg_auto_mode.disable_bsr_global_csg = 0;
    csg_auto_mode.csg_global_mode_support = 0;
    return FALSE;
  }
  else
  {
     SD_MSG_HIGH_2("CSG Auto mode csg support %d bsr %d",
     csg_auto_mode.csg_global_mode_support,
     csg_auto_mode.disable_bsr_global_csg);

     /*If disable bsr is set and csg global mode support is not none disable bsr */
     if((csg_auto_mode.disable_bsr_global_csg) &&
        (csg_auto_mode.csg_global_mode_support))
    {
      return TRUE;
    }
  }
  return FALSE;
}





