/*===========================================================================

                        QM_EFS.C

DESCRIPTION

  QMI_MMODE source file for efs module.

Copyright (c) 2013 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/qmi/src/qm_efs.c#2 $
  $Change: 8473567 $
  $DateTime: 2015/06/29 06:13:12 $
  $Author: c_smulag $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
02/27/13    ld     Initial version
===========================================================================*/

#include "qm_efs.h"
#include "fs_lib.h"
#include "sys_v.h"
#include "msg.h"
#include "qmi_voice_cm_if.h"
#include "qmi_voice_cm_util.h"
#include "qmi_mmode_msgr_msg.h"

#include "qmi_mmode_task_cmd.h"

//===========================================================================
// #defines, Typedefs and Data Structures
//===========================================================================

// filesize = 1 byte, valid values are 0 or 1 binary.
#define EFS_ALWAYS_RET_PLMN_FILE "/nv/item_files/modem/mmode/qmi/always_return_plmn"

// filesize = 1 byte, valid values are 0 or 1 binary.
#define EFS_AUTO_SETUP_RESP "/nv/item_files/modem/mmode/qmi/setup_response"

// filesize = 1 byte, valid values are 0 or 1 binary.
#define EFS_NO_WAIT_RESP "/nv/item_files/modem/mmode/qmi/no_wait_resp"

// filesize = 4 bytes, valid values are sys_csg_support_e_type
#define EFS_CSG_SUPPORTED_FILE "/nv/item_files/modem/nas/csg_support_configuration"


// filesize = 1 byte, valid values are 0 or 1 binary.
#define EFS_DISABLE_MODEM_CENTRIC_SOLUTION_FILE "/nv/item_files/modem/mmode/disable_global_mode"

struct qm_efs_cache_s
{
  struct
  {
    boolean inited;
    byte    ui_tty;
  } ui_tty;
};

struct qm_efs_global_s
{
  struct always_return_plmn_s
  {
    boolean is_file_read;
    boolean flag;
  } always_return_plmn;

  struct setup_autoanswer_s
  {
    boolean is_file_read;
    boolean is_auto_setup_resp_disabled; //If auto answer is disabled, wait for qmi-client to respond to setup
  } mt_setup_resp;

  struct no_wait_resp_s
  {
    boolean is_file_read;
    boolean is_enabled; //If no wait is enabled, send the resp immediately without waiting
  } no_wait_rsp;

  struct csg_supported_s
  {
    boolean                is_file_read;
    sys_csg_support_e_type is_supported;
  } csg_supported;

  struct qm_efs_cache_s cache;
};

//===========================================================================
// GLOBALS
//===========================================================================

static struct qm_efs_global_s efs_global;

//===========================================================================
// Function bodies
//===========================================================================

/*===========================================================================
  FUNCTION QM_EFS_INIT()

  DESCRIPTION
    This function initializes the efs global variables.

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    Initializes all three global variables in efs_global to zero.
===========================================================================*/
void qm_efs_init( void )
{
  memset( &efs_global, 0x00, sizeof(struct qm_efs_global_s) );

  efs_global.cache.ui_tty.inited = FALSE;
  efs_global.cache.ui_tty.ui_tty = QMI_VOICE_CM_IF_TTY_MODE_OFF;

  QM_MSG_MED("qm_efs_init() efs_global initialized.");
} // qm_efs_init()

/*===========================================================================
  FUNCTION QM_EFS_ALWAYS_RETURN_PLMN()

  DESCRIPTION
    Caches and verifies the boolean value contained in the EFS_ALWAYS_RET_PLMN_FILE.

  PARAMETERS
    NONE

  RETURN VALUE
    TRUE  - File was read successfully and contained a valid TRUE  value.
    FALSE - File was read successfully and contained a valid FALSE value, or
            an error encounterred, such as the file does not exist, or
            we do not have permissions to access it, or it contained an invalid
            value.

  DEPENDENCIES
    None

  SIDE EFFECTS
    If the file has not yet been cached, reads the  sets the global variables in efs_global.always_return_plmn
===========================================================================*/
boolean qm_efs_always_return_plmn( void )
{
  int status;

  if ( ! efs_global.always_return_plmn.is_file_read )
  {
    status = efs_get( EFS_ALWAYS_RET_PLMN_FILE, &efs_global.always_return_plmn.flag, (fs_size_t) sizeof(boolean) );

    QM_MSG_MED_3("%d = efs_get( %d (T/F), %d bytes )", status, efs_global.always_return_plmn.flag, sizeof(boolean) );

    efs_global.always_return_plmn.is_file_read = TRUE;

    if ( status == sizeof(boolean) )
    {
      if ( efs_global.always_return_plmn.flag != TRUE && efs_global.always_return_plmn.flag != FALSE )
      {
        QM_MSG_ERROR_1("qm_efs_always_return_plmn() file contains invalid value = %d, should be 0|1.", efs_global.always_return_plmn.flag);
        efs_global.always_return_plmn.flag = FALSE;   // reinitialize corrupted value
      }
    }
    else
    {
      QM_MSG_ERROR_2("qm_efs_always_return_plmn() failed: %d = efs_get( %d bytes )", status, sizeof(boolean));
      efs_global.always_return_plmn.flag = FALSE;     // Be paranoid, this is just in case get_efs() changes it.
    }
  }

  return efs_global.always_return_plmn.flag;
} //qm_efs_always_return_plmn()


/*===========================================================================
  FUNCTION QM_EFS_IS_AUTO_SETUP_RESPONSE_DISABLED()

  DESCRIPTION
    Qmi-voice default behaviour is to respond to setup_ind 
    for an MT call automatically(without taking respond from client).

    This function returns the status from EFS file if auto setup response is disabled.

  PARAMETERS
    None

  RETURN VALUE
    TRUE if auto setup response is disabled FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qm_efs_is_auto_setup_response_disabled( void )
{
  int status;

  if ( ! efs_global.mt_setup_resp.is_file_read )
  {
    status = efs_get( EFS_AUTO_SETUP_RESP, &efs_global.mt_setup_resp.is_auto_setup_resp_disabled, (fs_size_t) sizeof(boolean) );

    QM_MSG_MED_2("%d = efs_get(AUTO SETUP RESP DISABLED, %d (T/F) )", status, efs_global.mt_setup_resp.is_auto_setup_resp_disabled);

    efs_global.mt_setup_resp.is_file_read = TRUE;

    if ( status == sizeof(boolean) )
    {
      if ( efs_global.mt_setup_resp.is_auto_setup_resp_disabled != TRUE && efs_global.mt_setup_resp.is_auto_setup_resp_disabled != FALSE )
      {
        QM_MSG_ERROR_1("Auto setup resp file contains invalid value = %d, should be 0|1.", efs_global.mt_setup_resp.is_auto_setup_resp_disabled);
        efs_global.mt_setup_resp.is_auto_setup_resp_disabled = FALSE;   // reinitialize corrupted value
      }
    }
    else
    {
      QM_MSG_ERROR_2("qm_efs_is_auto_setup_response_disabled() failed: %d = efs_get( %d bytes )", status, sizeof(boolean));
      efs_global.mt_setup_resp.is_auto_setup_resp_disabled = FALSE;
    }
  }

  return efs_global.mt_setup_resp.is_auto_setup_resp_disabled;
} // qm_efs_is_auto_setup_response_disabled()

/*===========================================================================
  FUNCTION QM_EFS_IS_NO_WAIT_ENABLED()

  DESCRIPTION
    This function returns the status of no wait

  PARAMETERS
    None

  RETURN VALUE
    TRUE if no wait is enabled. FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qm_efs_is_no_wait_rsp_enabled( void )
{
  int status;

  if ( ! efs_global.no_wait_rsp.is_file_read )
  {
    status = efs_get( EFS_NO_WAIT_RESP, &efs_global.no_wait_rsp.is_enabled, (fs_size_t) sizeof(boolean) );

    QM_MSG_MED_2("%d = efs_get(NO WAIT RESP, %d (T/F) )", status, efs_global.no_wait_rsp.is_enabled);

    efs_global.no_wait_rsp.is_file_read = TRUE;

    if ( status == sizeof(boolean) )
    {
      if ( efs_global.no_wait_rsp.is_enabled != TRUE && efs_global.no_wait_rsp.is_enabled != FALSE )
      {
        QM_MSG_ERROR_1("No Wait response file contains invalid value = %d, should be 0|1.", efs_global.no_wait_rsp.is_enabled);
        efs_global.no_wait_rsp.is_enabled = FALSE;   // reinitialize corrupted value
      }
    }
    else
    {
      QM_MSG_ERROR_2("qm_efs_is_no_wait_rsp_enabled() failed: %d = efs_get( %d bytes )", status, sizeof(boolean));
      efs_global.no_wait_rsp.is_enabled = FALSE;
    }
  }

  return efs_global.no_wait_rsp.is_enabled;
} // qm_efs_is_no_wait_rsp_enabled()

/*=========================================================================== 
  FUNCTION CSG_SUPPORT_FROM_ENUM
 
  DESCRIPTION
    Static function to translate enum value to whether CSG is supported.
 
  PARAMETERS
    None
 
  RETURN VALUE
    boolean: whether CSG is supported
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
static boolean csg_support_from_enum( void )
{
  boolean ret_val = TRUE;

  switch ( efs_global.csg_supported.is_supported )
  {
    case SYS_CSG_SUPPORT_DISABLED:
      ret_val = FALSE;
      break;

    case SYS_CSG_SUPPORT_WCDMA:
    default:
      ret_val = TRUE;
      break;
  }

  QM_MSG_MED_2("csg support enum %d, ret %d", efs_global.csg_supported.is_supported, ret_val);

  return ret_val;
}

/*===========================================================================
  FUNCTION QM_EFS_CSG_SUPPORTED()

  DESCRIPTION
    This function determines if CSG is supported..

  PARAMETERS
    None

  RETURN VALUE
    boolean: whether CSG is supported

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qm_efs_csg_supported ( void )
{
  int status;

  if ( !efs_global.csg_supported.is_file_read )
  {
    status = efs_get( EFS_CSG_SUPPORTED_FILE, &efs_global.csg_supported.is_supported, (fs_size_t) sizeof(sys_csg_support_e_type) );

    QM_MSG_MED_3("%d efs get %d, %d bytes", status, efs_global.csg_supported.is_supported, sizeof(sys_csg_support_e_type));

    efs_global.csg_supported.is_file_read = TRUE;

    if ( status != sizeof(sys_csg_support_e_type) )
    {
      QM_MSG_HIGH("efs get failed");
      #ifdef FEATURE_DISABLE_CSG_BYDEFAULT
      QM_MSG_MED("FEATURE_DISABLE_CSG_BYDEFAULT enabled");
      efs_global.csg_supported.is_supported = SYS_CSG_SUPPORT_DISABLED;
      #else
      efs_global.csg_supported.is_supported = SYS_CSG_SUPPORT_WCDMA;
      #endif
    }
  }

  return csg_support_from_enum();
}
/*===========================================================================

FUNCTION qm_efs_get_ui_tty_setting()

DESCRIPTION
  Read ui_tty_setting from EFS item

DEPENDENCIES
  None

RETURN VALUE
  boolean : 1 = Get successful
            0 = Error in Get

SIDE EFFECTS
  None.

===========================================================================*/
boolean qm_efs_get_ui_tty_setting(
uint8            *ui_tty_setting_ptr,
                /* Pointer to a ui_tty_setting data.*/
int32           ui_tty_setting_size
                /* Size of the data_ptr buffer.*/
)
{
  boolean                  ret = FALSE; /* some error */
  qmi_voice_efs_id_e_type  efs_id = QMI_VOICE_UI_TTY_SETTING_PRI_F_NAME;

  if ( efs_global.cache.ui_tty.inited )
  {
    *ui_tty_setting_ptr = efs_global.cache.ui_tty.ui_tty;
    ret                 = TRUE;
  }
  else
  {
    memset( ui_tty_setting_ptr, 0x00, ui_tty_setting_size);
    ret = qmi_voice_cm_util_efs_read(efs_id, 
                       (uint8*)ui_tty_setting_ptr,
                       ui_tty_setting_size );

    if( ret == TRUE ) /* No error in EFS read */
    {
      if ( *ui_tty_setting_ptr <= QMI_VOICE_CM_IF_TTY_MODE_OFF )
      {
        efs_global.cache.ui_tty.ui_tty = *ui_tty_setting_ptr;
        efs_global.cache.ui_tty.inited = TRUE;
      }
      else
      {
        memset( ui_tty_setting_ptr, 0x00, ui_tty_setting_size);
        *ui_tty_setting_ptr = QMI_VOICE_CM_IF_TTY_MODE_OFF;

        ret = qmi_voice_cm_util_efs_write( efs_id, 
                           (uint8*)ui_tty_setting_ptr,
                           ui_tty_setting_size );
        if( ret == TRUE ) /* No error in EFS write */
        {
          efs_global.cache.ui_tty.ui_tty = *ui_tty_setting_ptr;
          efs_global.cache.ui_tty.inited = TRUE;
        }
        else
        {
          QM_MSG_ERROR_1("UI TTY EFS write failed %d", ret);
        }
      }
    }
    else
    {
      QM_MSG_ERROR_1("Failed to read EFS with efs_id %d", efs_id);
    }
  }

  QM_MSG_HIGH_2("UI TTY EFS get ret %d val %d subs %d", ret, efs_global.cache.ui_tty.ui_tty);

  return ret;
}


/*===========================================================================

FUNCTION qm_efs_put_ui_tty_setting()

DESCRIPTION
  Write ui_tty_setting to EFS item

DEPENDENCIES
  None

RETURN VALUE
  boolean : 1 = Put successful
            0 = Error in Put


SIDE EFFECTS
  None.

===========================================================================*/
boolean qm_efs_put_ui_tty_setting(
uint8           ui_tty_setting
                /* UI TTY Setting to write */
)
{
  boolean                                      ret = FALSE; /* some error */
  mmode_qmi_voice_tty_mode_info_cmd_msg_type   msg_ui_tty_pkt;
  errno_enum_type                              msgr_error;
  boolean                                      write_to_efs = TRUE;
  uint8                                        temp_ui_tty_buf;
  qmi_voice_efs_id_e_type                      efs_id = QMI_VOICE_UI_TTY_SETTING_PRI_F_NAME;

  if ( ui_tty_setting <= QMI_VOICE_CM_IF_TTY_MODE_OFF )
  {
    if(efs_global.cache.ui_tty.inited)
    {
      if(ui_tty_setting == efs_global.cache.ui_tty.ui_tty)
      {
        /* nothing to do as the values are identical */
        write_to_efs = FALSE;
        ret = TRUE;
      }
    }
    else
    {
      /* UI TTY not initialized yet. Read it and check if write is needed */
      temp_ui_tty_buf = 0;
      ret = qmi_voice_cm_util_efs_read(efs_id, 
                         (uint8*)&temp_ui_tty_buf,
                         sizeof(temp_ui_tty_buf) );

      if( ret == TRUE ) /* No error in EFS read */
      {
        efs_global.cache.ui_tty.inited = TRUE;
        efs_global.cache.ui_tty.ui_tty = temp_ui_tty_buf;
        if(temp_ui_tty_buf == ui_tty_setting)
        {
          /* nothing to do as the values are identical */
          write_to_efs = FALSE;
          ret = TRUE;
        }
      }
    }

    if(write_to_efs)
    {
      ret = qmi_voice_cm_util_efs_write( efs_id,
                                         (uint8*)&ui_tty_setting,
                                         sizeof(uint8) ); /* ui_tty_setting is uint8 per IDL */

      if ( ret == TRUE ) /* No error in efs_write() */
      {
        efs_global.cache.ui_tty.ui_tty    = ui_tty_setting;
        efs_global.cache.ui_tty.inited    = TRUE;

        /* send msgr after this EFS is updated */
        memset( &msg_ui_tty_pkt, 0x00, sizeof(msg_ui_tty_pkt) );

        msgr_init_hdr( (msgr_hdr_struct_type *)&msg_ui_tty_pkt, MSGR_QMI_VOICE, QMI_VOICE_UI_TTY_MODE_SETTING_CMD );

        msg_ui_tty_pkt.tty_mode = (mmode_qmi_voice_tty_mode_e_type)ui_tty_setting;

        msgr_error = msgr_send((msgr_hdr_struct_type*)&msg_ui_tty_pkt, sizeof(mmode_qmi_voice_tty_mode_info_cmd_msg_type));
      
        if (msgr_error != E_SUCCESS)
        {
          QM_MSG_ERROR_1("Error sending UI TTY Update to IMS/CM: %d\n", (uint16)msgr_error);
        }
        else
        {
          QM_MSG_MED_1("UI TTY Update sent to IMS/CM: %d\n", (uint16)msgr_error);
        }
      }
      else
      {
        QM_MSG_ERROR_1("UI TTY EFS write failed %d", ret);
      }
    }
    else
    {
      QM_MSG_MED_1("UI TTY EFS write not required for %d", ui_tty_setting);
    }
  }
  else
  {
    QM_MSG_MED_1("Invalid ui_tty_setting %d", ui_tty_setting);
  }

  QM_MSG_HIGH_2("UI TTY EFS put ret %d val %d", ret, efs_global.cache.ui_tty.ui_tty);

  return ret;
}

/*===========================================================================
  FUNCTION QM_EFS_MODEM_CENTRIC_SOLUTION_DISABLED()

  DESCRIPTION:
  This function checks the status of EFS item 
  /nv/item_files/modem/mmode/disable_global_mode

  RETURN VALUE:
  true if file is 1, false if not present or 0

  SIDE EFFECTS:
  None
===========================================================================*/
boolean qm_efs_modem_centric_solution_disabled ( void )
{
  int status = 0;
  byte enabled = 0;
  
  status = efs_get( EFS_DISABLE_MODEM_CENTRIC_SOLUTION_FILE, (void*)&enabled, (fs_size_t) sizeof(boolean) );
  if ( status != sizeof(boolean) )
  {
    QM_MSG_ERROR_1("qm_efs_modem_centric_solution_disabled() failed: %d, returning FALSE", status);
    return FALSE;
  }
  return enabled == 1;
}