#ifndef _QM_NV_H_
#define _QM_NV_H_

#include "nv.h"
#include "qm_comdef.h"

enum qm_nv_tty_mode_e
{
  QM_NV_TTY_MODE_MIN = -1,
  QM_NV_TTY_MODE_FULL, /* TTY Full */
  QM_NV_TTY_MODE_VCO,  /* TTY Voice Carryover */
  QM_NV_TTY_MODE_HCO,  /* TTY Hearing Carryover */
  QM_NV_TTY_MODE_OFF,  /* TTY Off */
  QM_NV_TTY_MODE_MAX
};

void qm_nv_init( void );

nv_stat_enum_type qm_nv_get( nv_items_enum_type item, nv_item_type *data, enum qm_subs_e subs );

nv_stat_enum_type qm_nv_put( nv_items_enum_type item, nv_item_type *data, enum qm_subs_e subs );

#endif // !_QM_NV_H_

