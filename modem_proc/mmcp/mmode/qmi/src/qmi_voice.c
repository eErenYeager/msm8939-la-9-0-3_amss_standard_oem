/*===========================================================================

                         D S _ Q M I _ VOICE . C

DESCRIPTION

 The Data Services Qualcomm MSM Interface Device Management Services source
 file.

EXTERNALIZED FUNCTIONS

  qmi_voice_init()
    Register the VOICE service with QMUX for all applicable QMI links
    

Copyright (c) 2009-2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/qmi/src/qmi_voice.c#4 $ $DateTime: 2010/04/22 22:49:27 $ $voiceor: nlocanin $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
08/25/09    vs     Initial Revision.
===========================================================================*/

/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "mmcp_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_QMI_SERVICE_SPLIT

#include "qmi_voice.h"

#include "dsm.h"
#include "nv.h"
#include "err.h"
#include "amssassert.h"
#include "qmi_voice_cm_if.h"
#include "qmi_voice_cmd_list.h"
#include "modem_mem.h"
#include "qmi_mmode_svc.h"
#include "qmi_mmode_task_cmd.h"

#include "qmi_si.h"
#include "qmi_idl_lib.h"
#include "voice_service_v02.h"
#include "voice_service_impl_v02.h"
#include "qm_timer.h"

/*===========================================================================

                            CONSTANT DEFINITIONS

===========================================================================*/

#ifndef FEATURE_NO_QMI_VOICE
/*---------------------------------------------------------------------------
  Service management
---------------------------------------------------------------------------*/
#define VOICEI_MAX_CLIDS    (10)

/*---------------------------------------------------------------------------
  Major and Minor Version Nos for VOICE
---------------------------------------------------------------------------*/
#define VOICEI_BASE_VER_MAJOR    (2)
#define VOICEI_BASE_VER_MINOR    (1)

#define VOICEI_ADDENDUM_VER_MAJOR  (0)
#define VOICEI_ADDENDUM_VER_MINOR  (0)

/*---------------------------------------------------------------------------
  Optional TLVs
---------------------------------------------------------------------------*/
#define VOICEI_IND_REG_DTMF             (0x10)
#define VOICEI_IND_REG_VOICE_PRIV       (0x11)
#define VOICEI_IND_REG_SUPS_NOTIFY      (0x12)
#define VOICEI_IND_REG_CALL_EVENTS_NOTIFY      (0x13)
#define VOICEI_IND_REG_HANDOVER_NOTIFY  (0x14)
#define VOICEI_IND_REG_SPEECH_NOTIFY        (0x15)
#define VOICEI_IND_REG_USSD_NOTIFY          (0x16)
#define VOICEI_IND_REG_SUPS_OTHER_NOTIFY    (0x17)
#define VOICEI_IND_REG_MODIFICATION_NOTIFY  (0x18)
#define VOICEI_IND_REG_UUS_NOTIFY           (0x19)
#define VOICEI_IND_REG_AOC_NOTIFY                         (0x1A)
#define VOICEI_IND_REG_CONF_NOTIFY           (0x1B)
#define VOICEI_IND_REG_EXT_BRST_INTL_NOTIFY     (0x1C)
#define VOICEI_IND_REG_MT_PAGE_MISS_NOTIFY  (0x1D)
#define VOICEI_IND_REG_CC_RESULT_NOTIFY     (0x1E)
#define VOICEI_IND_REG_CONF_PARTS_NOTIFY     (0x1F)
#define VOICEI_IND_REG_TTY_INFO_NOTIFY     (0x20)
#define VOICEI_IND_REG_E911_ORIG_FAIL_NOTIFY   (0x21)
#define VOICEI_IND_REG_AUDIO_RAT_CHANGE_NOTIFY (0x23)
#define VOICEI_IND_REG_ADD_CALL_INFO_NOTIFY     (0x24)

#define VOICEI_DIAL_CALL_TYPE   (0x10)
#define VOICEI_DIAL_CLIR_TYPE       (0x11)
#define VOICEI_DIAL_UUS_INFO        (0x12)
#define VOICEI_DIAL_CUG             (0x13)
#define VOICEI_DIAL_CALL_ID         (0x10)
#define VOICEI_DIAL_ALPHA_ID        (0x11)
#define VOICEI_DIAL_EMER_CAT        (0x14)
#define VOICEI_DIAL_CALLED_PARTY_SUB_ADDRRESS      (0x15)
#define VOICEI_DIAL_SRV_TYPE        (0x16)
#define VOICEI_DIAL_SIP_URI_OVERFLOW    (0x17)
#define VOICEI_DIAL_AUDIO_ATTRIB        (0x18)
#define VOICEI_DIAL_VIDEO_ATTRIB        (0x19)
#define VOICEI_DIAL_IP_CALL_PI          (0x1A)
#define VOICEI_DIAL_VS_CALL_ATTRIB      (0x1B)
#define VOICEI_DIAL_ECALL_VARIANT       (0x1C)
#define VOICEI_DIAL_CONFERENCE_INIT_URI      (0x1D)
#define VOICEI_DIAL_DISPLAY_TEXT        (0x1E)


#define VOICEI_DIAL_CC_RESULT_TYPE   (0x12)
#define VOICEI_DIAL_CC_RESULT_SUPS   (0x13)
#define VOICEI_DIAL_RESP_END_REASON  (0x14)
#define VOICEI_DIAL_RESP_MEDIA_ID    (0x15)

#define VOICEI_BURST_DTMF_LEN       (0x10)
#define VOICEI_BURST_DTMF_CALL_ID   (0x10)

#define VOICEI_END_CALL_ID          (0x10)

#define VOICEI_ANSWER_CALL_ID       (0x10)

#define VOICEI_ANSWER_CALL_TYPE        (0x10)
#define VOICEI_ANSWER_AUDIO_ATTRIB     (0x11)
#define VOICEI_ANSWER_VIDEO_ATTRIB     (0x12)
#define VOICEI_ANSWER_IP_CALL_PI       (0x13)
#define VOICEI_ANSWER_VS_FILE_ATTRIB   (0x14)
#define VOICEI_ANSWER_REJECT_CALL      (0x15)
#define VOICEI_ANSWER_REJECT_CAUSE     (0x16)
#define VOICEI_ANSWER_REJECT_SIP_CODE  (0x17)

#define VOICEI_SETUP_ANSWER_RESP_CALL_ID     (0x10)

#define VOICEI_SETUP_ANSWER_REJECT_CALL      (0x10)
#define VOICEI_SETUP_ANSWER_REJECT_CAUSE     (0x11)

#define VOICEI_GET_CALL_INFO        (0x10)
#define VOICEI_GET_CALL_RP_NUM      (0x11)
#define VOICEI_GET_CALL_SO          (0x12)
#define VOICEI_GET_CALL_VP          (0x13)
#define VOICEI_GET_CALL_OTSP        (0x14)
#define VOICEI_GET_CALL_RP_NAME     (0x15)
#define VOICEI_GET_CALL_UUS_INFO    (0x16)
#define VOICEI_GET_CALL_ALERTING    (0x17)
#define VOICEI_GET_CALL_ALPHA       (0x18)
#define VOICEI_GET_CALL_CONN_NUM_INFO       (0x19)
#define VOICEI_GET_CALL_DIAGNOSTICS       (0x1A)
#define VOICEI_GET_CALL_ALERTING_PATTERN  (0x1B)
#define VOICEI_GET_CALL_AUDIO_ATTRIB       (0x1C)
#define VOICEI_GET_CALL_VIDEO_ATTRIB       (0x1D)
#define VOICEI_GET_CALL_VS_VARIANT         (0x1E)
#define VOICEI_GET_CALL_SIP_URI            (0x1F)
#define VOICEI_GET_CALL_IS_SRVCC_CALL      (0x20)
#define VOICEI_GET_CALL_RP_NUM_EXT         (0x21)
#define VOICEI_GET_CALL_END_REASON_TEXT    (0x24)

#define VOICEI_SEND_FLASH_CALL_ID   (0x10)

#define VOICEI_START_CONT_DTMF_CALL_ID   (0x10)

#define VOICEI_STOP_CONT_DTMF_CALL_ID    (0x10)

#define VOICEI_DTMF_IND_PW              (0x10)
#define VOICEI_DTMF_IND_DIG_INT         (0x11)

#define VOICEI_ALL_CALL_IND_RP_NUM                          (0x10)
#define VOICEI_ALL_CALL_IND_RP_NAME                         (0x11)
#define VOICEI_ALL_CALL_IND_ALERTING                        (0x12)
#define VOICEI_ALL_CALL_IND_SO                              (0x13)
#define VOICEI_ALL_CALL_IND_END_REASON                      (0x14)
#define VOICEI_ALL_CALL_IND_ALPHA                           (0x15)
#define VOICEI_ALL_CALL_IND_CONNECTED_NUM                   (0x16)
#define VOICEI_ALL_CALL_IND_DIAGNOSTICS                     (0x17)
#define VOICEI_ALL_CALL_IND_CALLED_PARTY_NUM                (0x18)
#define VOICEI_ALL_CALL_IND_REDIRECTING_PARTY_NUM           (0x19)
#define VOICEI_ALL_CALL_IND_ALERTING_PATTERN                (0x1A)
#define VOICEI_ALL_CALL_IND_AUDIO_ATTRIB                    (0x1B)
#define VOICEI_ALL_CALL_IND_VIDEO_ATTRIB                    (0x1C)
#define VOICEI_ALL_CALL_IND_VS_VARIANT                      (0x1D)
#define VOICEI_ALL_CALL_IND_SIP_URI                         (0x1E)
#define VOICEI_ALL_CALL_IND_IS_SRVCC_CALL                   (0x1F)
#define VOICEI_ALL_CALL_IND_PARENT_CALL_INFO                (0x20)
#define VOICEI_ALL_CALL_IND_LOCAL_CAP                       (0x21)
#define VOICEI_ALL_CALL_IND_PEER_CAP                        (0x22)
#define VOICEI_ALL_CALL_IND_CHILD_NUM                       (0x23)
#define VOICEI_ALL_CALL_IND_DISPLAY_TEXT                    (0x24)
#define VOICEI_ALL_CALL_IND_RP_NUM_EXT                      (0x25)
#define VOICEI_ALL_CALL_IND_CONNECTED_NUM_EXT               (0x26)
#define VOICEI_ALL_CALL_IND_MEDIA_ID                        (0x27)
#define VOICEI_ALL_CALL_IND_IS_ADDITIONAL_CALL_INFO_PRESENT (0x28)
#define VOICEI_ALL_CALL_IND_CALL_ATTRIB_STATUS              (0x29)
#define VOICEI_ALL_CALL_IND_ORIG_FAIL_REASON                (0x2A)
#define VOICEI_ALL_CALL_IND_RP_NUM_EXT2                     (0x2B)
#define VOICEI_ALL_CALL_IND_IP_CALLER_NAME                  (0x2D)
#define VOICEI_ALL_CALL_IND_END_REASON_TEXT                 (0x2E)

#define VOICEI_GET_ALL_CALL_INFO                  (0x10)
#define VOICEI_GET_ALL_CALL_RP_NUM                (0x11)
#define VOICEI_GET_ALL_CALL_RP_NAME               (0x12)
#define VOICEI_GET_ALL_CALL_ALERTING              (0x13)
#define VOICEI_GET_ALL_CALL_UUS_INFO              (0x14)
#define VOICEI_GET_ALL_CALL_SO                    (0x15)
#define VOICEI_GET_ALL_CALL_OTASP                 (0x16)
#define VOICEI_GET_ALL_CALL_VP                    (0x17)
#define VOICEI_GET_ALL_CALL_END_REASON            (0x18)
#define VOICEI_GET_ALL_CALL_ALPHA                 (0x19)
#define VOICEI_GET_ALL_CALL_CONNECTED_NUM         (0x1A)
#define VOICEI_GET_ALL_CALL_DIAGNOSTICS           (0x1B)
#define VOICEI_GET_ALL_CALL_CALLED_PARTY_NUM      (0x1C)
#define VOICEI_GET_ALL_CALL_REDIRECTING_PARTY_NUM (0x1D)
#define VOICEI_GET_ALL_CALL_ALERTING_PATTERN      (0x1E)
#define VOICEI_GET_ALL_CALL_AUDIO_ATTRIBUTE       (0x1F)
#define VOICEI_GET_ALL_CALL_VIDEO_ATTRIBUTE       (0x20)
#define VOICEI_GET_ALL_CALL_VS_VARIANT            (0x21)
#define VOICEI_GET_ALL_CALL_SIP_URI               (0x22)
#define VOICEI_GET_ALL_CALL_IS_SRVCC_CALL         (0x23)
#define VOICEI_GET_ALL_CALL_ATTRIB_STATUS         (0x24)
#define VOICEI_GET_ALL_CALL_RP_NUM_EXT            (0x25)
#define VOICEI_GET_ALL_CALL_END_REASON_TEXT       (0x28)

#define VOICEI_MNG_CALLS_REQ_CALL_ID        (0x10)
#define VOICEI_MNG_CALLS_REQ_REJ_CAUSE        (0x11)

#define VOICEI_MNG_CALLS_FAILURE_CAUSE  (0x10)

#define VOICEI_SUPS_NOTIFY_IND_CUG                   (0x10)
#define VOICEI_SUPS_NOTIFY_IND_ECT                   (0x11)
#define VOICEI_SUPS_NOTIFY_IND_REASON                (0x12)
#define VOICEI_SUPS_NOTIFY_IND_IP_FORWARD_HIST_INFO  (0x13)

#define VOICEI_SET_SUPS_SC                   (0x10)
#define VOICEI_SET_SUPS_BARR_PWD             (0x11)
#define VOICEI_SET_SUPS_FWD_NUM              (0x12)
#define VOICEI_SET_SUPS_NR_TIMER             (0x13)
#define VOICEI_SET_SUPS_NUM_TYPE_PLAN        (0x14)
#define VOICEI_SET_SUPS_SC_EXT               (0x15)
#define VOICEI_SET_SUPS_BARR_NUM_LIST        (0x16)
#define VOICEI_SET_SUPS_COLR_PI              (0x17)
#define VOICEI_SET_SUPS_CALL_FWD_START_TIME  (0x18)
#define VOICEI_SET_SUPS_CALL_FWD_END_TIME    (0x19)

#define VOICEI_SET_SUPS_FAILURE_CAUSE     (0X10)
#define VOICEI_SET_SUPS_ALPHA_ID          (0X11)
#define VOICEI_SET_SUPS_CC_RESULT_TYPE    (0x12)
#define VOICEI_SET_SUPS_CC_RESULT_CALL_ID (0X13)
#define VOICEI_SET_SUPS_CC_RESULT_SUPS    (0X14)
#define VOICEI_SET_SUPS_SERVICE_STATUS    (0X15)
#define VOICEI_SET_SUPS_FAILURE_DESC      (0X16)
#define VOICEI_SET_SUPS_RETRY_DURATION    (0x17)

#define VOICEI_GET_CW_SC                (0x10)
#define VOICEI_GET_CW_REQ_SC_EXT        (0x11)

#define VOICEI_GET_CW_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_CW_ALPHA_ID          (0X12)
#define VOICEI_GET_CW_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_CW_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_CW_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_CW_RESP_SC_EXT       (0x16)
#define VOICEI_GET_CW_RETRY_DURATION    (0x17)

#define VOICEI_GET_CB_SC                (0x10)
#define VOICEI_GET_CB_REQ_SC_EXT             (0X11)

#define VOICEI_GET_CB_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_CB_ALPHA_ID          (0X12)
#define VOICEI_GET_CB_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_CB_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_CB_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_CB_RESP_SC_EXT       (0X16)
#define VOICEI_GET_CB_RESP_BARR_LIST    (0X17)
#define VOICEI_GET_CB_RETRY_DURATION    (0x18)

#define VOICEI_GET_CLIP_RESP              (0x10)
#define VOICEI_GET_CLIP_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_CLIP_ALPHA_ID          (0X12)
#define VOICEI_GET_CLIP_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_CLIP_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_CLIP_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_CLIP_RETRY_DURATION    (0x16)

#define VOICEI_GET_COLP_RESP              (0x10)
#define VOICEI_GET_COLP_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_COLP_ALPHA_ID          (0X12)
#define VOICEI_GET_COLP_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_COLP_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_COLP_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_COLP_RETRY_DURATION    (0X16)

#define VOICEI_GET_CLIR_RESP              (0x10)
#define VOICEI_GET_CLIR_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_CLIR_ALPHA_ID          (0X12)
#define VOICEI_GET_CLIR_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_CLIR_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_CLIR_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_CLIR_RETRY_DURATION    (0x16)

#define VOICEI_GET_CF_SC                   (0x10) //Optional Request TLV
#define VOICEI_GET_CF_REQ_SC_EXT               (0x11) 

#define VOICEI_GET_CF_INFO                (0x10)
#define VOICEI_GET_CF_FAILURE_CAUSE       (0X11)
#define VOICEI_GET_CF_ALPHA_ID            (0X12)
#define VOICEI_GET_CF_CC_RESULT_TYPE      (0x13)
#define VOICEI_GET_CF_CC_RESULT_CALL_ID   (0X14)
#define VOICEI_GET_CF_CC_RESULT_SUPS      (0X15)
#define VOICEI_GET_CF_EXTEN_INFO          (0X16)
#define VOICEI_GET_CF_EXTEN2_INFO         (0X17)
#define VOICEI_GET_CF_RETRY_DURATION      (0X18)
#define VOICEI_GET_CF_PROVISION_STATUS    (0X19)
#define VOICEI_GET_CF_CALL_FWD_START_TIME (0x1A)
#define VOICEI_GET_CF_CALL_FWD_END_TIME   (0x1B)

#define VOICEI_GET_ALS_LINE_SWITCH_STATUS   (0x10)

#define VOICEI_GET_ALS_SELECTED_LINE        (0x10)

#define VOICEI_SET_CB_PWD_FAILURE_CAUSE     (0X10)
#define VOICEI_SET_CB_PWD_ALPHA_ID          (0X11)
#define VOICEI_SET_CB_PWD_CC_RESULT_TYPE    (0x12)
#define VOICEI_SET_CB_PWD_CC_RESULT_CALL_ID (0X13)
#define VOICEI_SET_CB_PWD_CC_RESULT_SUPS    (0X14)
#define VOICEI_SET_CB_PWD_RETRY_DURATION    (0x15)

#define VOICEI_ORIG_USSD_FAILURE_CAUSE     (0X10)
#define VOICEI_ORIG_USSD_ALPHA_ID          (0X11)
#define VOICEI_ORIG_USSD_DATA              (0X12)
#define VOICEI_ORIG_USSD_CC_RESULT_TYPE    (0x13)
#define VOICEI_ORIG_USSD_CC_RESULT_CALL_ID  (0X14)
#define VOICEI_ORIG_USSD_CC_RESULT_SUPS     (0X15)
#define VOICEI_ORIG_USSD_DATA_UTF16         (0x16) 

#define VOICEI_USSD_IND_DATA               (0x10)
#define VOICEI_USSD_IND_DATA_UTF16         (0x11)

#define VOICEI_FLASH_PAYLOAD    (0x10)
#define VOICEI_FLASH_TYPE       (0x11)

#define VOICEI_ORIG_USSD_NO_WAIT_ERROR          (0x10)
#define VOICEI_ORIG_USSD_NO_WAIT_FAILURE_CAUSE  (0x11)
#define VOICEI_ORIG_USSD_NO_WAIT_DATA           (0x12)
#define VOICEI_ORIG_USSD_NO_WAIT_ALPHA_ID       (0x13)
#define VOICEI_ORIG_USSD_NO_WAIT_DATA_UTF16     (0x14)

#define VOICEI_MODEM_SET_CONFIG_AUTO_ANSWER          (0x10)
#define VOICEI_MODEM_SET_CONFIG_AIR_TIMER            (0x11)
#define VOICEI_MODEM_SET_CONFIG_ROAM_TIMER           (0x12)
#define VOICEI_MODEM_SET_CONFIG_TTY_MODE             (0x13)
#define VOICEI_MODEM_SET_CONFIG_PREF_VOICE_SO        (0x14)
#define VOICEI_MODEM_SET_CONFIG_PREF_VOICE_DOMAIN    (0x15)
#define VOICEI_MODEM_SET_CONFIG_UI_TTY_SETTING       (0x16)

#define VOICEI_MODEM_GET_CONFIG_AUTO_ANSWER          (0x10)
#define VOICEI_MODEM_GET_CONFIG_AIR_TIMER            (0x11)
#define VOICEI_MODEM_GET_CONFIG_ROAM_TIMER           (0x12)
#define VOICEI_MODEM_GET_CONFIG_TTY_MODE             (0x13)
#define VOICEI_MODEM_GET_CONFIG_PREF_VOICE_SO        (0x14)
#define VOICEI_MODEM_GET_CONFIG_AMR_STATUS           (0x15)
#define VOICEI_MODEM_GET_CONFIG_VOICE_PRIVACY        (0x16)
#define VOICEI_MODEM_GET_CONFIG_NAM_ID               (0x17)
#define VOICEI_MODEM_GET_CONFIG_PREF_VOICE_DOMAIN    (0x18)
#define VOICEI_MODEM_GET_CONFIG_UI_TTY_SETTING       (0x19)

#define VOICEI_MODEM_GET_CONFIG_RESP_PREF_VOICE_DOMAIN    (0x17)

#define VOICEI_BIND_SUBSCRIPTION             (0x10)

#define VOICEI_MNG_IP_CALLS_CALL_ID            (0x10)
#define VOICEI_MNG_IP_CALLS_CALL_TYPE          (0x11)
#define VOICEI_MNG_IP_CALLS_AUDIO_ATTRIB       (0x12)
#define VOICEI_MNG_IP_CALLS_VIDEO_ATTRIB       (0x13)
#define VOICEI_MNG_IP_CALLS_SIP_URI            (0x14)
#define VOICEI_MNG_IP_CALLS_SIP_REJECT_CAUSE   (0x15)
#define VOICEI_MNG_IP_CALLS_SIP_REJECT_CODE    (0x16)
#define VOICEI_MNG_IP_CALLS_SPEECH_CODEC      (0x17)

#define VOICEI_MNG_IP_CALLS_RESP_FAILURE_CAUSE        (0x11)


#define VOICEI_GET_COLR_RESP              (0x10)
#define VOICEI_GET_COLR_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_COLR_ALPHA_ID          (0X12)
#define VOICEI_GET_COLR_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_COLR_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_COLR_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_COLR_PI                (0x16)
#define VOICEI_GET_COLR_RETRY_DURATION    (0x17)

#define VOICEI_MODIFIED_CALL_ID            (0x01)
#define VOICEI_MODIFIED_CALL_TYPE          (0x10)
#define VOICEI_MODIFIED_CALL_AUDIO_ATTRIB  (0x11)
#define VOICEI_MODIFIED_CALL_VIDEO_ATTRIB  (0x12)
#define VOICEI_MODIFIED_CALL_FAILURE_CAUSE (0x13)
#define VOICEI_MODIFIED_CALL_MEDIA_ID      (0x14)
#define VOICEI_MODIFIED_CALL_ATTRIB_STATUS (0x15)

#define VOICEI_MODIFY_ACCEPT_CALL_ID   (0x01)
#define VOICEI_MODIFY_ACCEPT_CALL_TYPE   (0x10)
#define VOICEI_MODIFY_ACCEPT_AUDIO_ATTRIB   (0x11)
#define VOICEI_MODIFY_ACCEPT_VIDEO_ATTRIB   (0x12)


#define VOICEI_GET_CNAP_RESP              (0x10)
#define VOICEI_GET_CNAP_FAILURE_CAUSE     (0X11)
#define VOICEI_GET_CNAP_ALPHA_ID          (0X12)
#define VOICEI_GET_CNAP_CC_RESULT_TYPE    (0x13)
#define VOICEI_GET_CNAP_CC_RESULT_CALL_ID (0X14)
#define VOICEI_GET_CNAP_CC_RESULT_SUPS    (0X15)
#define VOICEI_GET_CNAP_RETRY_DURATION    (0x16)

#define VOICEI_AOC_GET_CALL_METER_INFO_ACM                (0x10)
#define VOICEI_AOC_GET_CALL_METER_INFO_ACMMAX             (0x11)
#define VOICEI_AOC_GET_CALL_METER_INFO_CCM                (0x12)

#define VOICEI_SRVCC_CALL_CONFIG_REQ_ALERTING_TYPE    (0x10)


/*---------------------------------------------------------------------------
  Response TLVs
---------------------------------------------------------------------------*/

#define VOICEI_INFO_REC_CALL_ID  (0x01)
#define VOICEI_CALL_STATUS       (0x01)
#define VOICEI_DTMF_INFORMATION  (0x01)
#define VOICEI_SIGNAL_INFO       (0x10)
#define VOICEI_CALLER_ID         (0x11)
#define VOICEI_DISPLAY_INFO      (0x12)
#define VOICEI_EXT_DISPLAY_INFO  (0x13)
#define VOICEI_CALLER_NAME_INFO  (0x14)
#define VOICEI_CALL_WAITING_IND  (0x15)
#define VOICEI_CONN_NUM_INFO     (0x16)
#define VOICEI_CALLING_PARTY_NUM_INFO (0x17)
#define VOICEI_CALLED_PARTY_NUM_INFO  (0x18)
#define VOICEI_REDIRECT_NUM_INFO  (0x19)
#define VOICEI_NSS_CLIR_INFO  (0x1A)
#define VOICEI_NSS_AUD_CTRL_INFO  (0x1B)
#define VOICEI_NSS_RELEASE_INFO  (0x1C)
#define VOICEI_LINE_CTRL_INFO  (0x1D)
#define VOICEI_EXT_DISPLAY_INFO_RECORD  (0x1E)


#define VOICEI_OTASP_STATUS      (0x01)

#define VOICEI_PRIVACY_INFO                 (0x01)
#define VOICEI_CALL_END_REASON              (0x10)
#define VOICEI_CALL_TYPE                    (0x11)
#define VOICEI_ALL_CALL_STATUS              (0x01)
#define VOICEI_SUPS_NOTIFY_INFO             (0x01)
#define VOICEI_USSD_NOTIFY_TYPE          (0x01)
#define VOICEI_UUS_IND_DATA               (0x01)

#define VOICEI_SUPS_IND_SERVICE_INFO                 (0x01)
#define VOICEI_SUPS_IND_SERVICE_CLASS               (0x10)
#define VOICEI_SUPS_IND_REASON                            (0x11)
#define VOICEI_SUPS_IND_CFW_NUM                        (0x12)
#define VOICEI_SUPS_IND_CFW_NR_TIMER               (0x13)
#define VOICEI_SUPS_IND_USS_INFO                        (0x14)
#define VOICEI_SUPS_IND_CALL_ID                           (0x15)
#define VOICEI_SUPS_IND_ALPHA                              (0x16)
#define VOICEI_SUPS_IND_PWD                                 (0x17)
#define VOICEI_SUPS_IND_NEW_PWD                        (0x18)
#define VOICEI_SUPS_IND_SUPS_DATA_SOURCE                (0x19)
#define VOICEI_SUPS_IND_FAILURE_CAUSE                   (0x1A)
#define VOICEI_SUPS_IND_CF_NTWK_DATA                    (0x1B)
#define VOICEI_SUPS_IND_CLIR_STATUS                     (0x1C)
#define VOICEI_SUPS_IND_CLIP_STATUS                     (0x1D)
#define VOICEI_SUPS_IND_COLP_STATUS                     (0x1E)
#define VOICEI_SUPS_IND_COLR_STATUS                     (0x1F)
#define VOICEI_SUPS_IND_CNAP_STATUS                     (0x20)
#define VOICEI_SUPS_IND_USS_INFO_UTF16                  (0x21)
#define VOICEI_SUPS_IND_SERVICE_CLASS_EXT               (0x22)
#define VOICEI_SUPS_IND_BARR_LIST                       (0x23)

#define VOICEI_SPEECH_CODEC_INFO_NW_MODE               (0x010)
#define VOICEI_SPEECH_CODEC_INFO_CODEC_TYPE            (0x011)
#define VOICEI_SPEECH_CODEC_INFO_SAMPLE_RATE           (0x012)
#define VOICEI_SPEECH_CODEC_INFO_CALL_ID               (0x013)

#define VOICEI_HANDOVER_STATE                         (0x01)
#define VOICEI_HANDOVER_TYPE                          (0x10)

#define VOICEI_EXT_BRST_INTL_DATA                         (0x01)

#define VOICEI_CONFERENCE_CONF_INFO_XML_DATA          (0x01)
#define VOICEI_CONFERENCE_CONF_INFO_SEQUENCE_NUM          (0x02)
#define VOICEI_CONFERENCE_CONF_INFO_TOTAL_XML_FILE_LENGTH          (0x10)
#define VOICEI_CONFERENCE_CONF_INFO_CALL_ID          (0x11)

#define VOICEI_CONFERENCE_JOIN_CALL_ID                    (0x01)
#define VOICEI_CONFERENCE_JOIN_PARTICIPANT_INFO           (0x02)

#define VOICEI_CONFERENCE_PARTICIPANT_UPDATE_INFO       (0x01)

#define VOICEI_CONFERENCE_PARTICIPANTS_INFO             (0x01)

#define VOICEI_PAGE_MISS_REASON                         (0x01)

#define VOICEI_CC_RESULT_INFO_RESULT                    (0x01)
#define VOICEI_CC_RESULT_INFO_ALPHA_PRESENCE            (0x02)
#define VOICEI_CC_RESULT_INFO_ALPHA_GSM8                (0x10)
#define VOICEI_CC_RESULT_INFO_ALPHA_UTF16               (0x11)

#define VOICEI_TTY_MODE             (0x01)

#define VOICEI_AUDIO_RAT_CHANGE_AUDIO_SESSION_INFO   (0x10)
#define VOICEI_AUDIO_RAT_CHANGE_RAT_INFO             (0x11)

#define VOICEI_CONF_PART_STATUS_INFO_CALL_ID             (0x01)
#define VOICEI_CONF_PART_STATUS_INFO_PART_ID             (0x02)
#define VOICEI_CONF_PART_STATUS_INFO_PART_OP_INFO             (0x10)


#define VOICEI_E911_ORIG_FAIL       (0x01)

#define VOICEI_ADDITIONAL_CALL_INFO_CALL_ID       (0x01)
#define VOICEI_ADDITIONAL_CALL_INFO_EXT_INFO_DATA          (0x10)


/*---------------------------------------------------------------------------
  Macro used in command handlers (common)
---------------------------------------------------------------------------*/
#define CHECK_RETVAL()  if (FALSE == retval) { dsm_free_packet(&response); \
                                               return NULL; }

#define VOICEI_TLV_TAG_SIZE     1  /* in bytes */
#define VOICEI_TLV_LENGTH_SIZE  2  /* in bytes */

#define QMI_VOICEI_PULL(sdu_in, value, siz) (siz == dsm_pullup( sdu_in, value, siz ))

/*===========================================================================

                                DATA TYPES

===========================================================================*/


/*---------------------------------------------------------------------------
  VOICE Command enum type - not equal to the actual command values!
  mapping is in qmi_voice_cmd_callbacks table

  DO NOT REORDER THIS ENUM!
---------------------------------------------------------------------------*/
typedef enum
{
  VOICEI_CMD_MIN                      = 0,
  VOICEI_CMD_INDICATION_REGISTER      = VOICEI_CMD_MIN,
  VOICEI_CMD_DIAL_CALL                ,
  VOICEI_CMD_END_CALL                 ,
  VOICEI_CMD_ANSWER_CALL              ,
  VOICEI_CMD_SETUP_ANSWER             ,
  VOICEI_CMD_GET_CALL_INFO            ,
  VOICEI_CMD_SEND_FLASH               ,
  VOICEI_CMD_BURST_DTMF               ,
  VOICEI_CMD_START_CONT_DTMF          ,
  VOICEI_CMD_STOP_CONT_DTMF           ,
  VOICEI_CMD_SET_PREFERRED_PRIVACY    ,
  VOICEI_CMD_SUPS_MNG_CALLS,
  VOICEI_CMD_SUPS_GET_CLIP,
  VOICEI_CMD_SUPS_GET_COLP,
  VOICEI_CMD_SUPS_GET_CALL_WAITING,
  VOICEI_CMD_SUPS_GET_CALL_BARRING,
  VOICEI_CMD_SUPS_SET_CALL_BARRING,
  VOICEI_CMD_GET_CLIR,
  VOICEI_CMD_GET_CALL_FORWARDING,
  VOICEI_CMD_GET_ALL_CALL_INFO ,
  VOICEI_CMD_SUPS_SET_SUPS_SERVICE,
  VOICEI_CMD_SUPS_ORIG_USSD,
  VOICEI_CMD_SUPS_ANSWER_USSD,
  VOICEI_CMD_SUPS_CANCEL_USSD,
  VOICEI_CMD_SET_CONFIG_ITEMS,
  VOICEI_CMD_GET_CONFIG_ITEMS,
  VOICEI_CMD_SUPS_GET_COLR,
  VOICEI_CMD_BIND_SUBSCRIPTION,
  VOICEI_CMD_SUPS_ORIG_USSD_NO_WAIT,
  VOICEI_CMD_ALS_SET_LINE_SWITCHING,
  VOICEI_CMD_ALS_SELECT_LINE,
  VOICEI_CMD_AOC_RESET_ACM                   ,
  VOICEI_CMD_AOC_SET_ACMMAX                  ,
  VOICEI_CMD_AOC_GET_CALL_METER_INFO         ,
  VOICEI_CMD_SUPS_MANAGE_IP_CALLS,
  VOICEI_CMD_ALS_GET_LINE_SWITCHING_STATUS,
  VOICEI_CMD_ALS_GET_SELECTED_LINE,
  VOICEI_CMD_SUPS_GET_CNAP,
  VOICEI_CMD_SRVCC_CALL_CONFIG_REQ,
  VOICEI_CMD_MAX, 
  VOICEI_CMD_WIDTH                    = 0xFFFF          
} qmi_voicei_cmd_e_type;

typedef enum
{
  VOICEI_CMD_VAL_INDICATION_REGISTER    = 0x0003,
  VOICEI_CMD_VAL_DIAL_CALL              = 0x0020,
  VOICEI_CMD_VAL_END_CALL               = 0x0021,
  VOICEI_CMD_VAL_ANSWER_CALL            = 0x0022,
  VOICEI_CMD_VAL_GET_CALL_INFO          = 0x0024,
  VOICEI_CMD_VAL_OTASP_STATUS           = 0x0025,
  VOICEI_CMD_VAL_INFO_REC_IND           = 0x0026,
  VOICEI_CMD_VAL_SEND_FLASH             = 0x0027,
  VOICEI_CMD_VAL_BURST_DTMF             = 0x0028,
  VOICEI_CMD_VAL_START_CONT_DTMF        = 0x0029,
  VOICEI_CMD_VAL_STOP_CONT_DTMF         = 0x002A,
  VOICEI_CMD_VAL_DTMF_IND               = 0x002B,
  VOICEI_CMD_VAL_SET_PREFERRED_PRIVACY  = 0x002C,
  VOICEI_CMD_VAL_PRIVACY_IND            = 0x002D,
  VOICEI_CMD_VAL_ALL_CALL_STATUS_IND    = 0x002E,
  VOICEI_CMD_VAL_GET_ALL_CALL_INFO      = 0x002F,
  /*Supplementary services API's*/
  VOICEI_CMD_VAL_SUPS_MANAGE_CALLS      = 0x0031,
  VOICEI_CMD_VAL_SUPS_NOTIFICATION_IND  = 0x0032,
  VOICEI_CMD_VAL_SUPS_SET_SUPS_SERVICE  = 0x0033,
  VOICEI_CMD_VAL_SUPS_GET_CALL_WAITING  = 0x0034,
  VOICEI_CMD_VAL_SUPS_GET_CALL_BARRING  = 0x0035,
  VOICEI_CMD_VAL_SUPS_GET_CLIP          = 0x0036,
  VOICEI_CMD_VAL_SUPS_GET_CLIR             = 0x0037,
  VOICEI_CMD_VAL_SUPS_GET_CALL_FORWARDING  = 0x0038,
  VOICEI_CMD_VAL_SUPS_SET_CALL_BARRING  = 0x0039,
  VOICEI_CMD_VAL_SUPS_ORIG_USSD            = 0x003A,
  VOICEI_CMD_VAL_SUPS_ANSWER_USSD          = 0x003B,
  VOICEI_CMD_VAL_SUPS_CANCEL_USSD          = 0x003C,
  VOICEI_CMD_VAL_SUPS_USSD_RELEASE_IND     = 0x003D,
  VOICEI_CMD_VAL_SUPS_USSD_IND             = 0x003E,
  VOICEI_CMD_VAL_SUPS_UUS_IND             = 0x003F,
  /*VOICE NV services API's*/
  VOICEI_CMD_VAL_SET_CONFIG_ITEMS             = 0x0040,
  VOICEI_CMD_VAL_GET_CONFIG_ITEMS             = 0x0041,
  VOICEI_CMD_VAL_SUPS_IND             = 0x0042,
  VOICEI_CMD_VAL_SUPS_ORIG_USSD_NO_WAIT       = 0x0043,
  VOICEI_CMD_VAL_SUPS_ORIG_USSD_NO_WAIT_IND   = 0x0043,
  /*Voice DSDS API's*/
  VOICEI_CMD_VAL_BIND_SUBSCRIPTION             = 0x0044,
  VOICEI_CMD_VAL_ALS_SET_LINE_SWITCHING    = 0x0045,
  VOICEI_CMD_VAL_ALS_SELECT_LINE           = 0x0046,
  VOICEI_CMD_VAL_AOC_RESET_ACM                        = 0x0047,
  VOICEI_CMD_VAL_AOC_SET_ACMMAX                       = 0x0048,
  VOICEI_CMD_VAL_AOC_GET_CALL_METER_INFO              = 0x0049,
  VOICEI_CMD_VAL_AOC_LOW_FUNDS_IND                    = 0x004A,
  VOICEI_CMD_VAL_SUPS_GET_COLP          = 0x004B,
  VOICEI_CMD_VAL_SUPS_GET_COLR             = 0x004C,
  VOICEI_CMD_VAL_SUPS_MANAGE_IP_CALLS      = 0x004E,
  VOICEI_CMD_VAL_ALS_GET_LINE_SWITCHING_STATUS    = 0x004F,
  VOICEI_CMD_VAL_ALS_GET_SELECTED_LINE           = 0x0050,
  VOICEI_CMD_VAL_CALL_MODIFIED_IND             = 0x0051,
  VOICEI_CMD_VAL_CALL_MODIFY_ACCEPT_IND             = 0x0052,
  VOICEI_CMD_VAL_SPEECH_CODEC_INFO_IND      = 0x0053,
  VOICEI_CMD_VAL_SUPS_GET_CNAP           = 0x004D,
  VOICEI_CMD_VAL_HANDOVER_IND           = 0x0054,
  VOICEI_CMD_VAL_CONF_INFO_IND          = 0x0055,
  VOICEI_CMD_VAL_CONF_JOIN_IND          = 0x0056,
  VOICEI_CMD_VAL_CONF_PARTICIPANT_UPDATE_IND          = 0x0057,
   VOICEI_CMD_VAL_EXT_BRST_INTL_IND           = 0x0058,
  VOICEI_CMD_VAL_MT_PAGE_MISS_IND       = 0x0059,
  VOICEI_CMD_VAL_CC_RESULT_INFO_IND      = 0x005A,
  VOICEI_CMD_VAL_CONFERENCE_PARTICIPANTS_INFO_IND  = 0x005B,
  VOICEI_CMD_VAL_SETUP_ANSWER           = 0x005C,
  VOICEI_CMD_VAL_TTY_IND  = 0x005D,
  VOICEI_CMD_VAL_ADDITIONAL_CALL_INFO_IND  = 0x0062,
    VOICEI_CMD_VAL_AUDIO_RAT_CHANGE_INFO_IND  = 0x0063,
  VOICEI_CMD_VAL_SET_SRVCC_CALL_CONFIG      = 0x0064,
  VOICEI_CMD_VAL_E911_ORIG_FAIL_IND  = 0x0065,
  VOICEI_CMD_VAL_CONF_PART_STATUS_IND  = 0x0066,  
/* QC EXTERNAL QMI COMMAND RANGE IS 0x0000 - 0x5555. 
   Add the next external QMI Command here */

/* VENDOR SPECIFIC QMI COMMAND RANGE IS 0x5556 - 0xAAAA.
   IMPORTANT!
   Add the vendor specific QMI Commands within this range only to avoid 
   conflicts with QC QMI commands that would get released in future */

/* RESERVED QC QMI COMMAND RANGE IS 0xAAAB - 0xFFFE 
   Internal QMI Commands must be added in DECREASING ORDER from Below */
  VOICEI_CMD_VAL_WIDTH                  = 0xFFFF
} qmi_voicei_cmd_val_e_type;


/*---------------------------------------------------------------------------
  QMI VOICE client state definition
---------------------------------------------------------------------------*/
typedef struct qmi_voicei_client_state_s
{
  qmi_common_client_state_type common; // must be first since we alias
  int16 instance;
  int16 service_id;
  struct
  {
    boolean                      reg_dtmf_events;
    boolean                      reg_voice_privacy_events;
    boolean                      reg_sups_events;
    boolean                      reg_call_events;
    boolean                      reg_handover_events;
    boolean                      reg_speech_events;
    boolean                      reg_ussd_notification_events;    
    boolean                      reg_modification_events;
    boolean                      reg_uus_events;   
    boolean                      reg_aoc_events;
    boolean                      reg_conference_events;
    boolean                      reg_brst_intl_events;
    boolean                      reg_mt_page_miss_events;
    boolean                      reg_cc_result_events;
    boolean                      reg_conf_participants_events;
    boolean                      reg_tty_info_events;
    boolean                      reg_audio_rat_change_events;
    boolean                      reg_e911_orig_fail_events;
    boolean                      reg_add_call_info_events;
  }voice_info;
  struct
  {
    boolean is_bind_set_by_client;
    uint8 as_id;
  }asubs_info;
} qmi_voicei_client_state_type;


/*---------------------------------------------------------------------------
  QMI VOICE State state definition
  Watermark queue to hold deferred responses
---------------------------------------------------------------------------*/
typedef struct
{
  uint16                        num_qmi_instances;
  qmi_voicei_client_state_type*  client[VOICEI_MAX_CLIDS];
} qmi_voicei_state_type;

static qmi_voicei_state_type  qmi_voice_state;

typedef struct
{
  qmi_mmode_svc_config_type         cmn_svc_cfg;
  /* Other svc config */
} qmi_voice_svc_config_type;

static qmi_voice_svc_config_type  qmi_voicei_cfg;

struct qmi_voice_e911_orig_fail_ind_s_type
{
  PACK(struct) qmi_voice_e911_orig_fail_ind_s 
  {
    uint8     call_id;
    uint16              end_reason;
  }e911_orig_fail_ind;
};

/*===========================================================================

                               INTERNAL DATA

===========================================================================*/

/*---------------------------------------------------------------------------
  QMI service command handlers
  forward declarations & cmd handler dispatch table definition
---------------------------------------------------------------------------*/

static dsm_item_type* qmi_voicei_dial_call(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_end_call(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_answer_call(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_setup_answer(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_send_flash(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_get_call_info(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_set_preferred_privacy(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_get_all_call_info(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_indication_register(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_burst_dtmf(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_start_cont_dtmf(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_stop_cont_dtmf(void*, void*, void*, dsm_item_type **);
/*Supplementary services*/
static dsm_item_type* qmi_voicei_sups_get_clir(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_call_forwarding(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_clip(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_colp(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_call_waiting(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_call_barring(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_set_call_barring(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_manage_calls(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_set_sups_service(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_orig_ussd(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_answer_ussd(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_cancel_ussd(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_colr(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_get_cnap(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_srvcc_call_config_req(void*, void*, void*, dsm_item_type **);


/*Voice NV services*/
static dsm_item_type* qmi_voicei_set_modem_config(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_get_modem_config(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_sups_orig_ussd_no_wait(void*, void*, void*, dsm_item_type **);
/*Voice DSDS**/
static dsm_item_type* qmi_voicei_bind_subscription(void*, void*, void*, dsm_item_type **);

/* ALS services*/
static dsm_item_type* qmi_voicei_als_set_line_switching(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_als_select_line(void*, void*, void*, dsm_item_type **);

/*AoC services*/
static dsm_item_type* qmi_voicei_aoc_reset_acm(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_aoc_set_acmmax(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_aoc_get_call_meter_info(void*, void*, void*, dsm_item_type **);

static dsm_item_type* qmi_voicei_als_get_line_switching_status(void*, void*, void*, dsm_item_type **);
static dsm_item_type* qmi_voicei_als_get_selected_line(void*, void*, void*, dsm_item_type **);

/* VoIP calls */
static dsm_item_type* qmi_voicei_sups_manage_ip_calls(void*, void*, void*, dsm_item_type **);


#define VOICE_HDLR(a,b)  QMI_SVC_HDLR( a, (qmi_svc_hdlr_ftype)b )

static qmi_svc_cmd_hdlr_type  qmi_voicei_cmd_callbacks[VOICEI_CMD_MAX] =
{   
  VOICE_HDLR( VOICEI_CMD_VAL_INDICATION_REGISTER,
                qmi_voicei_indication_register),
  VOICE_HDLR( VOICEI_CMD_VAL_DIAL_CALL,
                qmi_voicei_dial_call),
  VOICE_HDLR( VOICEI_CMD_VAL_END_CALL,
                qmi_voicei_end_call),
  VOICE_HDLR( VOICEI_CMD_VAL_ANSWER_CALL,
                qmi_voicei_answer_call),
  VOICE_HDLR( VOICEI_CMD_VAL_SETUP_ANSWER,
                qmi_voicei_setup_answer),
  VOICE_HDLR( VOICEI_CMD_VAL_GET_CALL_INFO,
                qmi_voicei_get_call_info),
  VOICE_HDLR( VOICEI_CMD_VAL_SEND_FLASH,
                qmi_voicei_send_flash),
  VOICE_HDLR( VOICEI_CMD_VAL_BURST_DTMF,
                qmi_voicei_burst_dtmf),
  VOICE_HDLR( VOICEI_CMD_VAL_START_CONT_DTMF,
                qmi_voicei_start_cont_dtmf),
  VOICE_HDLR( VOICEI_CMD_VAL_STOP_CONT_DTMF,
                qmi_voicei_stop_cont_dtmf),
  VOICE_HDLR( VOICEI_CMD_VAL_SET_PREFERRED_PRIVACY,
                qmi_voicei_set_preferred_privacy),
  VOICE_HDLR( VOICEI_CMD_VAL_GET_ALL_CALL_INFO,
                qmi_voicei_get_all_call_info),

  /* Sups command handlers */
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_MANAGE_CALLS,
                qmi_voicei_sups_manage_calls),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CLIP,
                qmi_voicei_sups_get_clip),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CALL_WAITING,
                qmi_voicei_sups_get_call_waiting),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CALL_BARRING,
                qmi_voicei_sups_get_call_barring),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_SET_CALL_BARRING,
                qmi_voicei_sups_set_call_barring),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_SET_SUPS_SERVICE,
                qmi_voicei_sups_set_sups_service),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CLIR,
                qmi_voicei_sups_get_clir),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CALL_FORWARDING,
                qmi_voicei_sups_get_call_forwarding),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_ORIG_USSD,
                qmi_voicei_sups_orig_ussd),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_ANSWER_USSD,
                qmi_voicei_sups_answer_ussd),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_CANCEL_USSD,
                qmi_voicei_sups_cancel_ussd),
  /* NV command handlers */
  VOICE_HDLR( VOICEI_CMD_VAL_SET_CONFIG_ITEMS,
                qmi_voicei_set_modem_config),
  VOICE_HDLR( VOICEI_CMD_VAL_GET_CONFIG_ITEMS,
                qmi_voicei_get_modem_config),
  VOICE_HDLR( VOICEI_CMD_VAL_BIND_SUBSCRIPTION,
                qmi_voicei_bind_subscription),
  VOICE_HDLR(VOICEI_CMD_VAL_SUPS_ORIG_USSD_NO_WAIT,
                qmi_voicei_sups_orig_ussd_no_wait),
  VOICE_HDLR( VOICEI_CMD_VAL_ALS_SET_LINE_SWITCHING,
                qmi_voicei_als_set_line_switching),
  VOICE_HDLR( VOICEI_CMD_VAL_ALS_SELECT_LINE,
                qmi_voicei_als_select_line),
  VOICE_HDLR( VOICEI_CMD_VAL_AOC_RESET_ACM,
                qmi_voicei_aoc_reset_acm),
  VOICE_HDLR( VOICEI_CMD_VAL_AOC_SET_ACMMAX,
                qmi_voicei_aoc_set_acmmax),
  VOICE_HDLR( VOICEI_CMD_VAL_AOC_GET_CALL_METER_INFO,
                qmi_voicei_aoc_get_call_meter_info),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_MANAGE_IP_CALLS,
                qmi_voicei_sups_manage_ip_calls),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_COLP,
                qmi_voicei_sups_get_colp),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_COLR,
                  qmi_voicei_sups_get_colr),
  VOICE_HDLR( VOICEI_CMD_VAL_ALS_GET_LINE_SWITCHING_STATUS,
                qmi_voicei_als_get_line_switching_status),
  VOICE_HDLR( VOICEI_CMD_VAL_ALS_GET_SELECTED_LINE,
                qmi_voicei_als_get_selected_line),
  VOICE_HDLR( VOICEI_CMD_VAL_SUPS_GET_CNAP,
                  qmi_voicei_sups_get_cnap),
  VOICE_HDLR( VOICEI_CMD_VAL_SET_SRVCC_CALL_CONFIG,
                  qmi_voicei_srvcc_call_config_req)
};



/*===========================================================================

                       FORWARD FUNCTION DECLARATIONS 

===========================================================================*/

static void qmi_voicei_dial_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_end_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_answer_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_setup_answer_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_send_flash_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_get_call_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_set_pref_privacy_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_burst_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_start_cont_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_stop_cont_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_get_all_call_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_dtmf_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_info_rec_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_otasp_status_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_privacy_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_notification_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_all_call_status_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_ussd_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_ussd_release_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_uus_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_orig_ussd_no_wait_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_speech_codec_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_conf_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_additional_call_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_conf_join_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_conf_participant_update_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_handover_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_aoc_low_funds_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_ext_brst_intl_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_conference_participants_info_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_mt_page_miss_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_call_control_result_info_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_tty_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_audio_rat_change_info_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_conference_participant_status_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);


static void qmi_voicei_e911_orig_fail_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

/* Sups command responses */
static void qmi_voicei_sups_get_clir_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);
/*Supplementary services resp*/

static void qmi_voicei_sups_get_call_forwarding_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);
static void qmi_voicei_sups_get_clip_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_get_colp_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);


static void qmi_voicei_sups_get_call_waiting_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_get_call_barring_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_set_call_barring_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_manage_calls_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_set_sups_service_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_orig_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_answer_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_cancel_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_set_modem_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_get_modem_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_als_set_line_switching_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_als_select_line_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_aoc_reset_acm_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_aoc_set_acmmax_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_aoc_get_call_meter_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_manage_ip_calls_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_get_colr_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_als_get_line_switching_status_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_als_get_selected_line_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_call_modified_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_call_modify_accept_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_sups_get_cnap_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);

static void qmi_voicei_srvcc_call_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
);


/* FW callbacks */
static void qmi_voice_fw_init_cback
(
  uint16 num_instances
);

static boolean qmi_voice_fw_alloc_clid_cback
( 
  qmi_framework_common_msg_hdr_type *msg_hdr
);

static void qmi_voice_fw_dealloc_clid_cback
(
  qmi_framework_common_msg_hdr_type *msg_hdr
);

static void qmi_voice_fw_req_cback 
( 
  qmi_framework_msg_hdr_type *msg_hdr,
  dsm_item_type ** sdu 
);

void qmi_voice_handle_fw_cmd
(
  qmi_mmode_cmd_enum_type cmd,
  void *            user_data_ptr
);

void qmi_voice_process_cmd
(
  qmi_mmode_cmd_enum_type        cmd,
  void                   * user_data_ptr
);

/*===========================================================================
  FUNCTION QMI_VOICE_INIT()

  DESCRIPTION
    Register the VOICE service with QMI FW

  PARAMETERS
    None

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voice_init
(
  void
)
{
  qmi_framework_err_e_type      errval;
  qmi_idl_service_object_type     svc_obj;
  uint32_t                          idl_minor_ver = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voice_init");

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /* Set the cmd handlers in QMI MMODE task */
  qmi_mmode_set_cmd_handler(QMI_MMODE_CMD_VOICE_FW_CB, qmi_voice_handle_fw_cmd);
    
  /* memset the service configuration */
    memset(&qmi_voicei_cfg.cmn_svc_cfg.fw_cfg, 0, sizeof(qmi_framework_svc_config_type) );
    
  /*-----------------------------------------------------------------------
  Step1: Fill configuration 
  -----------------------------------------------------------------------*/
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.base_version.major     = VOICEI_BASE_VER_MAJOR;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.base_version.minor     = VOICEI_BASE_VER_MINOR;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.addendum_version.major = VOICEI_ADDENDUM_VER_MAJOR;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.addendum_version.minor = VOICEI_ADDENDUM_VER_MINOR;
    
  /* mandatory callbacks should be filled */
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.cbs.alloc_clid         = qmi_voice_fw_alloc_clid_cback;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.cbs.dealloc_clid       = qmi_voice_fw_dealloc_clid_cback;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.cbs.init_cback         = qmi_voice_fw_init_cback;
  qmi_voicei_cfg.cmn_svc_cfg.fw_cfg.cbs.cmd_hdlr           = qmi_voice_fw_req_cback;
  
  qmi_voicei_cfg.cmn_svc_cfg.cmd_hdlr_array    = qmi_voicei_cmd_callbacks;
  qmi_voicei_cfg.cmn_svc_cfg.cmd_num_entries   = VOICEI_CMD_MAX;
  qmi_voicei_cfg.cmn_svc_cfg.service_id = QMUX_SERVICE_VOICE;
  
  /*-----------------------------------------------------------------------
  step 2:  calling QMI Framework API to register the service.
  ----------------------------------------------------------------------*/
  errval = qmi_framework_reg_service(QMUX_SERVICE_VOICE, &qmi_voicei_cfg.cmn_svc_cfg.fw_cfg);
  if (QMI_FRAMEWORK_ERR_NONE != errval)
  {
    QM_MSG_ERROR_2("Service registration failed Service:%d error:%d", QMUX_SERVICE_VOICE, errval);
    if (QMI_FRAMEWORK_ERR_FW_NOT_READY == errval)
    {
     /* Start timer and set signal to mmode task and try to do the registration again ?
       Have a max retry count and exit ? TBD */
    }
      /*-----------------------------------------------------------------------
    step 3:  Service registration fails. See error code. 
      -----------------------------------------------------------------------*/
    // Registration may have failied due to any one of the following reasons
    // 1. Service already registered  
    // 2. Mandatory callback is missing
    // 3. Framework is NOT ready
    // If Framework is not ready, then service needs to retry registration later.
    // Handling different failures is left to the discretion of the QMI service owner 
  }

  svc_obj =  voice_get_service_object_v02();
  (void) qmi_idl_get_idl_version( svc_obj, &idl_minor_ver );
  (void) qmi_si_register_id( QMUX_SERVICE_VOICE, 
                             0, /* service_instance */
                             2, /* IDL major version */
                             idl_minor_ver, 
                             voice_get_service_impl_v02() );

} /* qmi_voice_init() */


void qmi_voice_process_cmd
(
  qmi_mmode_cmd_enum_type        cmd,
  void                   * user_data_ptr
)
{
  #ifndef FEATURE_NO_QMI_VOICE
  qmi_mmode_cmd_data_buf_type *  data_ptr;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  QM_MSG_MED("QMI VOICE qmi_voice_process_cmd");

  ASSERT (user_data_ptr);

  data_ptr = (qmi_mmode_cmd_data_buf_type *) user_data_ptr;
  ASSERT (data_ptr->cmd.cm_if_data_ptr->header.cmd < QMI_VOICE_CM_IF_CMD_MAX);

  switch (data_ptr->cmd.cm_if_data_ptr->header.cmd)
  {
    case QMI_VOICE_CM_IF_CMD_DIAL_CALL_RESP:
      qmi_voicei_dial_call_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_END_CALL_RESP:
      qmi_voicei_end_call_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ANSWER_CALL_RESP:
      qmi_voicei_answer_call_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SETUP_ANSWER_RESP:
      qmi_voicei_setup_answer_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_GET_ALL_CALL_INFO_RESP:
      qmi_voicei_get_all_call_info_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SEND_FLASH_RESP:
      qmi_voicei_send_flash_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_INFO_REC_IND:
      qmi_voicei_info_rec_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_OTASP_STATUS_IND:
      qmi_voicei_otasp_status_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_GET_CALL_INFO_RESP:
      qmi_voicei_get_call_info_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SET_PREFERRED_PRIVACY_RESP:
      qmi_voicei_set_pref_privacy_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_BURST_DTMF_RESP:
      qmi_voicei_burst_dtmf_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_START_CONT_DTMF_RESP:
      qmi_voicei_start_cont_dtmf_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_STOP_CONT_DTMF_RESP:
      qmi_voicei_stop_cont_dtmf_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_PRIVACY_IND:
      qmi_voicei_privacy_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_DTMF_IND:
      qmi_voicei_dtmf_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_NOTIFICATION_IND:
      qmi_voicei_sups_notification_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ALL_CALL_STATUS_IND:
      qmi_voicei_all_call_status_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
/* Sups responses */
    case QMI_VOICE_CM_IF_CMD_SUPS_MANAGE_CALLS_RESP:
      qmi_voicei_sups_manage_calls_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CLIR_RESP:
      qmi_voicei_sups_get_clir_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_FORWARDING_RESP:
      qmi_voicei_sups_get_call_forwarding_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CLIP_RESP:
       qmi_voicei_sups_get_clip_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_WAITING_RESP:
       qmi_voicei_sups_get_call_waiting_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_BARRING_RESP:
       qmi_voicei_sups_get_call_barring_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
    case QMI_VOICE_CM_IF_CMD_SUPS_SET_CALL_BARRING_RESP:
       qmi_voicei_sups_set_call_barring_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
    case QMI_VOICE_CM_IF_CMD_SUPS_SET_SUPS_SERVICE_RESP:
       qmi_voicei_sups_set_sups_service_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
    case QMI_VOICE_CM_IF_CMD_SUPS_ORIG_USSD_RESP:
      qmi_voicei_sups_orig_ussd_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_ANSWER_USSD_RESP:
      qmi_voicei_sups_answer_ussd_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_CANCEL_USSD_RESP:
      qmi_voicei_sups_cancel_ussd_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_USSD_IND:
      qmi_voicei_sups_ussd_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_USSD_RELEASE_IND:
      qmi_voicei_sups_ussd_release_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_UUS_IND:
      qmi_voicei_uus_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_IND:
      qmi_voicei_sups_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_HANDOVER_IND:
      qmi_voicei_handover_ind( data_ptr->cmd.cm_if_data_ptr );
      break;	  
    case QMI_VOICE_CM_IF_CMD_AOC_LOW_FUNDS_IND:
      qmi_voicei_aoc_low_funds_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_EXT_BRST_INTL_IND:
      qmi_voicei_ext_brst_intl_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SPEECH_CODEC_INFO_IND:
      qmi_voicei_speech_codec_info_ind( data_ptr->cmd.cm_if_data_ptr );
      break;      
    /*Modem Configuration items response*/
    case QMI_VOICE_CM_IF_CMD_SET_MODEM_CONFIG_RESP:
      qmi_voicei_set_modem_config_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_GET_MODEM_CONFIG_RESP:
      qmi_voicei_get_modem_config_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_ORIG_USSD_NO_WAIT_IND:
      qmi_voicei_sups_orig_ussd_no_wait_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ALS_SET_LINE_SWITCHING_RESP:
      qmi_voicei_als_set_line_switching_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ALS_SELECT_LINE_RESP:
      qmi_voicei_als_select_line_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_AOC_RESET_ACM_RESP:
      qmi_voicei_aoc_reset_acm_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_AOC_SET_ACMMAX_RESP:
      qmi_voicei_aoc_set_acmmax_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_AOC_GET_CALL_METER_INFO_RESP:
      qmi_voicei_aoc_get_call_meter_info_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_MANAGE_IP_CALLS_RESP:
      qmi_voicei_sups_manage_ip_calls_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_COLP_RESP:
       qmi_voicei_sups_get_colp_resp( data_ptr->cmd.cm_if_data_ptr );
       break;
   case QMI_VOICE_CM_IF_CMD_SUPS_GET_COLR_RESP:
      qmi_voicei_sups_get_colr_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ALS_GET_LINE_SWITCHING_STATUS_RESP:
      qmi_voicei_als_get_line_switching_status_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ALS_GET_SELECTED_LINE_RESP:
      qmi_voicei_als_get_selected_line_resp( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_CALL_MODIFIED_IND:
      qmi_voicei_call_modified_ind( data_ptr->cmd.cm_if_data_ptr );
      break;      
    case QMI_VOICE_CM_IF_CMD_CALL_MODIFY_ACCEPT_IND:
      qmi_voicei_call_modify_accept_ind( data_ptr->cmd.cm_if_data_ptr );
      break;      
    case QMI_VOICE_CM_IF_CMD_SUPS_GET_CNAP_RESP:
      qmi_voicei_sups_get_cnap_resp( data_ptr->cmd.cm_if_data_ptr );
      break;      
    case QMI_VOICE_CM_IF_CMD_CONF_INFO_IND:
      qmi_voicei_conf_info_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_ADDITIONAL_CALL_INFO_IND:
      qmi_voicei_additional_call_info_ind( data_ptr->cmd.cm_if_data_ptr );
      break;      
    case QMI_VOICE_CM_IF_CMD_CONF_PARTICIPANT_STATUS_IND:
      qmi_voicei_conference_participant_status_ind( data_ptr->cmd.cm_if_data_ptr );
      break; 	  
    case QMI_VOICE_CM_IF_CMD_CONF_JOIN_IND:
      qmi_voicei_conf_join_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_CONF_PARTICIPANT_UPDATE_IND:
      qmi_voicei_conf_participant_update_ind( data_ptr->cmd.cm_if_data_ptr );
      break;
    case QMI_VOICE_CM_IF_CMD_CONF_PARTICIPANTS_INFO_IND:
      qmi_voicei_conference_participants_info_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_MT_PAGE_MISS_IND:
      qmi_voicei_mt_page_miss_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_CC_RESULT_INFO_IND:
      qmi_voicei_call_control_result_info_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_SRVCC_CALL_CONFIG_RESP:
      qmi_voicei_srvcc_call_config_resp(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_TTY_INFO_IND:
      qmi_voicei_tty_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_AUDIO_RAT_CHANGE_INFO_IND:
      qmi_voicei_audio_rat_change_info_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    case QMI_VOICE_CM_IF_CMD_E911_ORIG_FAIL_IND:
      qmi_voicei_e911_orig_fail_ind(data_ptr->cmd.cm_if_data_ptr);
      break;
    default:
      QM_MSG_MED_1("Ignoring cmd %d", data_ptr->cmd.cm_if_data_ptr->header.cmd);
      break;
  }
  QMI_VOICE_CM_IF_MEM_FREE(data_ptr->cmd.cm_if_data_ptr);
  #else
  (void) cmd;
  (void) user_data_ptr;
  #endif /* !FEATURE_NO_QMI_VOICE */
  return;
}

/*===========================================================================
  FUNCTION QMI_VOICE_HAS_VALID_CLIENT()

  DESCRIPTION
    Checks if there is a client using this service
    
  PARAMETERS
    None
    
  RETURN VALUE
    True if QMI VOICE has a valid client.
    False otherwise.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean qmi_voice_has_valid_client
( 
  void
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++)
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED))
    {
      return TRUE;
    }
  }

  return FALSE;
} /* qmi_voice_has_valid_client */


/*===========================================================================
  FUNCTION qmi_voicei_validate_cmd_buf_p()

  DESCRIPTION
    Function to validate returned cmd_buf_p is still allocated in client.

  PARAMETERS
    cmd_buf_p: pointer to queue element for comparison

RETURN VALUE
  Pointer to the client state if cmdbuf valid. otherwise NULL
===========================================================================*/
qmi_voicei_client_state_type* qmi_voicei_validate_cmd_buf_p( qmi_cmd_buf_type *cmd_buf_p )
{
  int i, j;
  qmi_voicei_state_type *svc_state_ptr = &qmi_voice_state;

  if(cmd_buf_p == NULL)
  {
    return NULL;
  }

  for( i = 0; i < VOICEI_MAX_CLIDS; i++ )
  {
    if( svc_state_ptr->client[i] != NULL )
    {
      for( j = 0; j < MAX_PENDING_COMMANDS; j++ )
      {
        if( cmd_buf_p == &(svc_state_ptr->client[i]->common.cmd_buf[j]) )
        {
          if( svc_state_ptr->client[i]->common.cmd_buf[j].x_p != NULL )
          {
            return svc_state_ptr->client[i];
          }
          else
          {
            QM_MSG_ERROR_2("Client %d cmd_buf %d has null transaction", i, j);
            return NULL;
          }
        }
      }
    }
  }

  QM_MSG_HIGH("Buffer does not match existing client transaction");
  return NULL;
}


/*===========================================================================

                             INTERNAL FUNCTIONS

===========================================================================*/

/*===========================HELPER FUNCTIONS==============================*/

/*===========================================================================
  FUNCTION QMI_VOICEI_GET_CLIENT_SP_BY_CLID()

  DESCRIPTION
    Retrieve client sp based on clid

  PARAMETERS
    clid  :  uint8

  RETURN VALUE
    cl_sp   :  client sp

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void * qmi_voicei_get_client_sp_by_clid
(
  uint8 clid
)
{
  qmi_voicei_state_type *voice_sp;
  int i=0;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if ((clid == QMI_SVC_CLID_UNUSED ) ||
      (clid > VOICEI_MAX_CLIDS ))
  {
    QM_MSG_MED_1("Invalid client ID clid :%d", clid);
    return NULL;
  }

  voice_sp = &qmi_voice_state;
  for(i=0; i < VOICEI_MAX_CLIDS; i++ ) 
  {
    if(voice_sp->client[i] != NULL)
    {
      if( voice_sp->client[i]->common.clid == clid)
      {
        return voice_sp->client[i];
      }
    }
  }
  return NULL;
} /* qmi_voicei_get_client_sp_by_clid() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SEND_RESPONSE()

  DESCRIPTION
    Send the response to framework

  PARAMETERS
    clid: uint8

  RETURN VALUE
    TRUE if sent successfully, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmi_voicei_send_response
(
  qmi_error_e_type        errval,
  qmi_cmd_buf_type *      cmd_buf_p,
  dsm_item_type *         msg_ptr
)
{
  qmi_framework_common_msg_hdr_type  common_hdr;
  qmi_voicei_client_state_type *    cl_sp;
  qmi_result_e_type       result;
  boolean status = TRUE;


  /* Check if the client is still present */
  if( (cl_sp = qmi_voicei_validate_cmd_buf_p(cmd_buf_p)) == NULL )
  {
    QM_MSG_ERROR("Client, transaction no longer valid. Cannot send response");
    dsm_free_packet(&msg_ptr);
    return FALSE;
  }

  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  if(!qmi_svc_put_result_tlv(&msg_ptr, result, errval))
  {
    QM_MSG_ERROR("qmi_svc_put_result_tlv failed. Cannot send response");
    qmi_mmode_svc_free_transaction_cmd_buf( &cmd_buf_p );
    dsm_free_packet(&msg_ptr);
    return FALSE;
  }

  /* Fill common header and send response */
  memset(&common_hdr, 0, sizeof(common_hdr));
  common_hdr.service = (qmux_service_e_type)cl_sp->service_id;
  common_hdr.client_id = cl_sp->common.clid;
  /* Transaction id will be filled by svc layer */
  common_hdr.qmi_instance = cl_sp->instance;

  /* Send the response */
  status = qmi_mmode_svc_send_response( &common_hdr,cmd_buf_p, msg_ptr);
  if(!status && ( msg_ptr != NULL ))
  {
    dsm_free_packet(&msg_ptr);
  }
  return status;
}/* qmi_voicei_send_response */


/*===========================================================================
  FUNCTION QMI_VOICEI_SEND_INDICATION()

  DESCRIPTION
    This function is calls the QMI Framework API to send out the VOICE service
    indication to client.

  PARAMETERS
    msg_hdr_p : QMI Framework message header
    cmd_type : type of indication
    ind : dsm item
 
  RETURN VALUE
    TRUE if ind sent to Framework sucessfully
    FALSE otherwise
 
  DEPENDENCIES
    QMI Voice service must be initialized and registered with Framework
 
  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmi_voicei_send_indication
(
  qmi_voicei_client_state_type * cl_sp,
  uint16                         cmd_type,
  dsm_item_type *                ind
)
{
  qmi_framework_common_msg_hdr_type  common_hdr;
  boolean status = TRUE;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  ASSERT(cl_sp);  

  /*-----------------------------------------------------------------------
    Fill the QMI Framework message common header 
  -----------------------------------------------------------------------*/
  common_hdr.client_id =  cl_sp->common.clid;
  QM_MSG_MED_2("QMI Voice indication(%d) being sent to client %d", cmd_type, common_hdr.client_id);
  common_hdr.qmi_instance = cl_sp->instance;
  common_hdr.service = (qmux_service_e_type)cl_sp->service_id;

  status = qmi_mmode_svc_send_indication(&common_hdr, cmd_type,ind );
  if(!status && (ind != NULL))
  {
    dsm_free_packet(&ind);
  }
  return status;
} /* qmi_voicei_send_indication */


/*===========================================================================
  FUNCTION QMI_VOICEI_ALLOC_CL_SP()

  DESCRIPTION
    Allocate the client data buffer

  PARAMETERS
    clid: uint8

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static qmi_voicei_client_state_type * qmi_voicei_alloc_cl_sp
(
  uint8 clid
)
{
  qmi_voicei_client_state_type *  cl_sp = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED_1("qmi_voicei_alloc_cl_sp clid :%d", clid);

  if ((clid == QMI_SVC_CLID_UNUSED ) ||
      (clid > VOICEI_MAX_CLIDS ))
  {
    QM_MSG_MED_1("Invalid client ID clid :%d", clid);
    return cl_sp;
  }
  cl_sp = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(qmi_voicei_client_state_type));
  if ( cl_sp == NULL ) 
  {
    QM_MSG_ERROR("Alloc msg_header:  No Can't get memory allocation!");
    ASSERT(cl_sp);
  }
  qmi_voice_state.client[clid - 1] = cl_sp;
  return cl_sp;
} /* qmi_voicei_alloc_cl_sp */


/*===========================================================================
  FUNCTION QMI_VOICE_FW_DEALLOC_CL_SP()

  DESCRIPTION
    Free the client data buffer for the given clid.

  PARAMETERS
    clid: uint8 type

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static boolean qmi_voicei_dealloc_cl_sp
(
  uint8 clid
)
{
  qmi_voicei_client_state_type *  cl_sp = NULL;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED_1("qmi_voicei_dealloc_cl_sp clid :%d", clid);

  if ((clid == QMI_SVC_CLID_UNUSED ) ||
      (clid > VOICEI_MAX_CLIDS ))
  {
    QM_MSG_MED_1("Invalid client ID clid :%d", clid);
    return FALSE;
  }

  cl_sp = (qmi_voicei_client_state_type *) qmi_voice_state.client[clid - 1];
  if(cl_sp)
  {
    q_destroy( &cl_sp->common.x_free_q );
    q_destroy( &cl_sp->common.cmd_free_q );

    QMI_VOICE_CM_IF_MEM_FREE(cl_sp);
    qmi_voice_state.client[clid - 1] = NULL;
  }
  else
  {
    QM_MSG_ERROR("Client state pointer is already NULL");
    return FALSE;
  }
  return TRUE;
} /* qmi_voicei_dealloc_cl_sp */


/*===========================================================================
  FUNCTION QMI_VOICEI_RESET_CLIENT()

  DESCRIPTION
    Reset the client data

  PARAMETERS
    cl_sp_ptr: qmi_voicei_client_state_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_reset_client
(
  qmi_voicei_client_state_type * cl_sp_ptr
)
{

  ASSERT(cl_sp_ptr);

  qmi_mmode_svc_reset_common_client(&cl_sp_ptr->common);
  
  cl_sp_ptr->voice_info.reg_dtmf_events = FALSE;
  cl_sp_ptr->voice_info.reg_voice_privacy_events = FALSE;
  cl_sp_ptr->voice_info.reg_sups_events = TRUE;
  cl_sp_ptr->voice_info.reg_call_events = TRUE;
  cl_sp_ptr->voice_info.reg_handover_events = FALSE;
  cl_sp_ptr->voice_info.reg_speech_events   = FALSE;
  cl_sp_ptr->voice_info.reg_ussd_notification_events = TRUE;
  cl_sp_ptr->voice_info.reg_modification_events = TRUE;
  cl_sp_ptr->voice_info.reg_uus_events = TRUE;
  cl_sp_ptr->voice_info.reg_aoc_events               = FALSE;
  cl_sp_ptr->voice_info.reg_conference_events        = FALSE;
  cl_sp_ptr->voice_info.reg_brst_intl_events = FALSE;
  cl_sp_ptr->voice_info.reg_conf_participants_events = FALSE;
  cl_sp_ptr->voice_info.reg_mt_page_miss_events = FALSE;
  cl_sp_ptr->voice_info.reg_cc_result_events = FALSE;
  cl_sp_ptr->voice_info.reg_tty_info_events = FALSE;
  cl_sp_ptr->voice_info.reg_audio_rat_change_events = FALSE;
  cl_sp_ptr->voice_info.reg_e911_orig_fail_events = FALSE;
  cl_sp_ptr->voice_info.reg_add_call_info_events = FALSE;

  QM_MSG_MED_1("qmi_voicei_reset_client clid =%d", cl_sp_ptr->common.clid);

} /* qmi_voicei_reset_client() */


/*===========================FW CALLBACK HANDLING==========================*/

/*===========================================================================
  FUNCTION QMI_VOICE_INIT_CBACK()

  DESCRIPTION
    Callback for Service initialization from Framework

  PARAMETERS
    num_instances: uint16

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voice_fw_init_cback
(
  uint16 num_instances
)
{
    qmi_mmode_cmd_data_buf_type *cmd_ptr;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    ;
    if( (cmd_ptr = qmi_mmode_get_cmd_data_buf()) == NULL)
    {
      QM_MSG_ERROR("Unable to allocate QMI VOICE cmd buf");
      return;
    }
    memset(cmd_ptr, 0, sizeof(qmi_mmode_cmd_data_buf_type));
    cmd_ptr->cmd.qmi_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(qmi_mmode_common_cmd_type));
    if( NULL == cmd_ptr->cmd.qmi_ptr )
    {
      QM_MSG_FATAL("Can't allocate memory");
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
      return;
    }

    
    cmd_ptr->cmd.qmi_ptr->id = QMI_MMODE_FW_INIT_CB;
    cmd_ptr->cmd.qmi_ptr->data.qmi_fw_info.init_cb.num_instances = num_instances;

    qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_FW_CB, cmd_ptr);
}


/*===========================================================================
  FUNCTION QMI_VOICEI_FW_INIT_CBACK_HDLR()

  DESCRIPTION
    Process the Service initialization request from Framework

  PARAMETERS
    num_instances: uint16

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voicei_fw_init_cback_hdlr
(
  uint16 num_instances
)
{
  qmi_voicei_state_type *voice_sp;

  voice_sp = &qmi_voice_state;
  memset(voice_sp, 0, sizeof(qmi_voicei_state_type));

  voice_sp->num_qmi_instances = num_instances;

  qmi_voicei_cfg.cmn_svc_cfg.svc_sp = voice_sp;

  qmi_mmode_set_cmd_handler(QMI_MMODE_CMD_VOICE_CB, qmi_voice_process_cmd);

  /* Initialize the Voice -> CM  interface */
  qmi_voice_cm_if_init();
  
}/* voicei_init_cback */


/*===========================================================================
  FUNCTION QMI_VOICE_ALLOC_CLID_CBACK()

  DESCRIPTION
    Callback to allocate the client from Framework

  PARAMETERS
    common_msg_hdr: qmi_framework_common_msg_hdr_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean  qmi_voice_fw_alloc_clid_cback
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_mmode_cmd_data_buf_type *cmd_ptr;

  ASSERT(common_msg_hdr);

  if( (cmd_ptr = qmi_mmode_get_cmd_data_buf()) == NULL)
  {
    QM_MSG_ERROR("Unable to allocate QMI VOICE cmd buf");
    return FALSE;
  }
  memset(cmd_ptr, 0, sizeof(qmi_mmode_cmd_data_buf_type));
  cmd_ptr->cmd.qmi_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(qmi_mmode_common_cmd_type));
  if( NULL == cmd_ptr->cmd.qmi_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    return FALSE;
  }

  cmd_ptr->cmd.qmi_ptr->id = QMI_MMODE_FW_ALLOC_CLID_CB;
  cmd_ptr->cmd.qmi_ptr->data.qmi_fw_info.alloc_clid.msg_hdr = *common_msg_hdr;

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_FW_CB, cmd_ptr);
  return TRUE;

}/* qmi_voice_fw_alloc_clid */


/*===========================================================================
  FUNCTION QMI_VOICE_ALLOC_CLID_HDLR()

  DESCRIPTION
    Process the client allocation request from Framework

  PARAMETERS
    common_msg_hdr: qmi_framework_common_msg_hdr_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voicei_fw_alloc_clid_hdlr
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_voicei_client_state_type   *  cl_sp;  
  qmi_result_e_type                result = QMI_RESULT_FAILURE;
  qmi_voicei_state_type *  voice_sp;
  qmi_error_e_type                errval = QMI_ERR_NONE;
/*-------------------------------------------------------------------------*/

  ASSERT(common_msg_hdr);
  cl_sp = NULL;

  QM_MSG_MED("qmi_voicei_fw_alloc_clid_hdlr");
  voice_sp = &qmi_voice_state;
  cl_sp = qmi_voicei_alloc_cl_sp(common_msg_hdr->client_id);

  if(cl_sp)
  {
    qmi_mmode_svc_initialize_common_client(&cl_sp->common, voice_sp);

    cl_sp->voice_info.reg_dtmf_events = FALSE;
    cl_sp->voice_info.reg_voice_privacy_events = FALSE;
    cl_sp->voice_info.reg_sups_events = TRUE;
    cl_sp->voice_info.reg_call_events = TRUE;
    cl_sp->voice_info.reg_handover_events = FALSE;
    cl_sp->voice_info.reg_speech_events = FALSE;
    cl_sp->voice_info.reg_ussd_notification_events = TRUE;
    cl_sp->voice_info.reg_modification_events = TRUE;
    cl_sp->voice_info.reg_uus_events = TRUE;
    cl_sp->voice_info.reg_aoc_events               = FALSE;
    cl_sp->voice_info.reg_conference_events = FALSE;
    cl_sp->voice_info.reg_brst_intl_events = FALSE;
    cl_sp->voice_info.reg_conf_participants_events = FALSE;
    cl_sp->voice_info.reg_mt_page_miss_events = FALSE;
    cl_sp->voice_info.reg_cc_result_events = FALSE;
    cl_sp->voice_info.reg_tty_info_events = FALSE;
    cl_sp->voice_info.reg_audio_rat_change_events = FALSE;
    cl_sp->voice_info.reg_e911_orig_fail_events = FALSE;
    cl_sp->voice_info.reg_add_call_info_events = FALSE;

    cl_sp->asubs_info.is_bind_set_by_client = FALSE;
    cl_sp->asubs_info.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

    cl_sp->common.clid = common_msg_hdr->client_id;
    cl_sp->instance    = (uint16)common_msg_hdr->qmi_instance;
    cl_sp->service_id  = common_msg_hdr->service ;

    QM_MSG_MED_1("qmi_voicei clid set to local client state ptr:  clid%d", cl_sp->common.clid);
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    QM_MSG_MED("No available service clids!");
    errval = QMI_ERR_CLIENT_IDS_EXHAUSTED;
  }

  /* Notifying the clid allocation back to client via QMI Framework*/
  qmi_framework_svc_send_alloc_clid_result_ex(result, common_msg_hdr,errval);

}/* qmi_voicei_fw_alloc_clid_hdlr */


/*===========================================================================
  FUNCTION QMI_VOICE_FW_DEALLOC_CLID_CBACK()

  DESCRIPTION
    Deallocate the client data.

  PARAMETERS
    common_msg_hdr: qmi_framework_common_msg_hdr_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void  qmi_voice_fw_dealloc_clid_cback
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_mmode_cmd_data_buf_type *cmd_ptr;

  ASSERT(common_msg_hdr);

  if( (cmd_ptr = qmi_mmode_get_cmd_data_buf()) == NULL)
  {
    QM_MSG_ERROR("Unable to allocate QMI VOICE cmd buf");
    return;
  }
  memset(cmd_ptr, 0, sizeof(qmi_mmode_cmd_data_buf_type));
  cmd_ptr->cmd.qmi_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(qmi_mmode_common_cmd_type));
  if( NULL == cmd_ptr->cmd.qmi_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    return;
  }

  cmd_ptr->cmd.qmi_ptr->id = QMI_MMODE_FW_DEALLOC_CLID_CB;
  cmd_ptr->cmd.qmi_ptr->data.qmi_fw_info.dealloc_clid.msg_hdr = *common_msg_hdr;

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_FW_CB, cmd_ptr);

}/* qmi_voice_fw_dealloc_clid_cback */


/*===========================================================================
  FUNCTION QMI_VOICE_FW_DEALLOC_CLID_HDLR()

  DESCRIPTION
    Process the dealloc clid request from framework.

  PARAMETERS
    common_msg_hdr: qmi_framework_common_msg_hdr_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voicei_fw_dealloc_clid_hdlr
(
  qmi_framework_common_msg_hdr_type * common_msg_hdr
)
{
  qmi_voicei_client_state_type   *  cl_sp;   
  qmi_result_e_type                result = QMI_RESULT_FAILURE; 
  qmi_error_e_type                errval = QMI_ERR_NONE;
 /*-------------------------------------------------------------------------*/
 
   ASSERT(common_msg_hdr);
 

  if ((common_msg_hdr->client_id == QMI_SVC_CLID_UNUSED ) ||
      (common_msg_hdr->client_id > VOICEI_MAX_CLIDS ))
  {
    QM_MSG_ERROR_1("Invalid client ID clid :%d", common_msg_hdr->client_id);
    return;
  }

   cl_sp = NULL;
   QM_MSG_MED("qmi_voicei_fw_dealloc_clid_hdlr");
 
   cl_sp = (qmi_voicei_client_state_type *) 
           qmi_voice_state.client[common_msg_hdr->client_id  - 1];
 
   if (cl_sp != NULL && cl_sp->common.clid == common_msg_hdr->client_id)
   {
     qmi_voicei_reset_client(cl_sp);
     qmi_voicei_dealloc_cl_sp(common_msg_hdr->client_id);
     QM_MSG_MED_1("qmi voice clid free to local client state ptr:  clid%d", common_msg_hdr->client_id);
     result = QMI_RESULT_SUCCESS;
   }
   else
   {
     QM_MSG_MED("No available service clids!");
     errval = QMI_ERR_INVALID_CLIENT_ID;
   }
 
   /* Notifying the clid deallocation back to client via QMI Framework*/
   qmi_framework_svc_send_dealloc_clid_result_ex(result, common_msg_hdr, errval);

}/* qmi_voicei_fw_dealloc_clid_hdlr */


/*===========================================================================
  FUNCTION QMI_VOICE_FW_REQ_CB()

  DESCRIPTION
    Callback for qmi request from Framework

  PARAMETERS
    msg_hdr: qmi_framework_msg_hdr_type ptr
    sdu: dsm_item_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voice_fw_req_cback
( 
  qmi_framework_msg_hdr_type *msg_hdr, 
  dsm_item_type **sdu
)
{
  qmi_mmode_cmd_data_buf_type *cmd_ptr;

  ASSERT(msg_hdr);

  if( (cmd_ptr = qmi_mmode_get_cmd_data_buf()) == NULL)
  {
    QM_MSG_ERROR("Unable to allocate QMI VOICE cmd buf");
    return;
  }
  memset(cmd_ptr, 0, sizeof(qmi_mmode_cmd_data_buf_type));
  cmd_ptr->cmd.qmi_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(
                                 sizeof(qmi_mmode_common_cmd_type));
  if( NULL == cmd_ptr->cmd.qmi_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    return;
  }

  cmd_ptr->cmd.qmi_ptr->id = QMI_MMODE_FW_REQ_CB;
  cmd_ptr->cmd.qmi_ptr->data.qmi_fw_info.req_cb.msg_hdr = *msg_hdr;
  cmd_ptr->cmd.qmi_ptr->data.qmi_fw_info.req_cb.sdu_in  = *sdu;

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_FW_CB, cmd_ptr);

}/* qmi_voice_fw_req_cb */


/*===========================================================================
  FUNCTION QMI_VOICE_FW_REQ_HDLR()

  DESCRIPTION
    Process qmi request from Framework

  PARAMETERS
    msg_hdr: qmi_framework_msg_hdr_type ptr
    sdu: dsm_item_type ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voicei_fw_req_hdlr
(
  qmi_framework_msg_hdr_type* msg_hdr,
  dsm_item_type * sdu_in 
)
{
  qmi_voicei_client_state_type * cl_sp;
  
/*-------------------------------------------------------------------------*/

  ASSERT(msg_hdr);
  ASSERT(sdu_in);

  QM_MSG_MED_3("Process QMI VOICE svc Request handlr clid %d tx_id %d ctl_flag %d", msg_hdr->common_hdr.client_id, msg_hdr->common_hdr.transaction_id, msg_hdr->msg_ctl_flag);

  if(msg_hdr->common_hdr.client_id > VOICEI_MAX_CLIDS)
  {
    QM_MSG_ERROR_1("Cannot process more than %d clients in qmi-Voice", VOICEI_MAX_CLIDS);
    return;
  }

  /*-------------------------------------------------------------------------
    Get a service state and client state pointer
  -------------------------------------------------------------------------*/
  if( msg_hdr->common_hdr.client_id > 0 )
  {
    cl_sp = (qmi_voicei_client_state_type *) 
               qmi_voice_state.client[msg_hdr->common_hdr.client_id - 1];
 
    ASSERT(cl_sp);

   /*-------------------------------------------------------------------------
     Invoke the common svc request handler
    -------------------------------------------------------------------------*/
   qmi_mmode_svc_req_hdlr(&qmi_voicei_cfg.cmn_svc_cfg, msg_hdr, &cl_sp->common, 
                          sdu_in);
  }
  else
  {
    QMI_MSG_ERROR("%s","Received Client id is non positive"); 
    return;
  }
}/* qmi_voicei_fw_req_hdlr */


/*===========================================================================
  FUNCTION QMI_VOICE_HANDLE_FW_CMD()

  DESCRIPTION
    Process QMI-Voice cmds posted from Framework callbacks

  PARAMETERS
    cmd: qmi_mmode_cmd_enum_type
    user_data_ptr: void ptr

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_voice_handle_fw_cmd
(
  qmi_mmode_cmd_enum_type cmd,
  void *            user_data_ptr
)
{
  qmi_mmode_cmd_data_buf_type* data_ptr;
  qmi_mmode_common_cmd_type *qmi_info;

  ASSERT (user_data_ptr);

  data_ptr = (qmi_mmode_cmd_data_buf_type *) user_data_ptr;
  ASSERT(data_ptr->cmd.qmi_ptr);
  ASSERT (data_ptr->cmd.qmi_ptr->id < QMI_VOICE_CM_IF_CMD_MAX);
  qmi_info = data_ptr->cmd.qmi_ptr;

  switch(qmi_info->id)
  {
    case QMI_MMODE_FW_INIT_CB:
      qmi_voicei_fw_init_cback_hdlr(qmi_info->data.qmi_fw_info.init_cb.num_instances);
      break;
    case QMI_MMODE_FW_ALLOC_CLID_CB:
      qmi_voicei_fw_alloc_clid_hdlr(&qmi_info->data.qmi_fw_info.alloc_clid.msg_hdr);
      break;
    case QMI_MMODE_FW_DEALLOC_CLID_CB:
      qmi_voicei_fw_dealloc_clid_hdlr(&qmi_info->data.qmi_fw_info.dealloc_clid.msg_hdr);
      break;
    case QMI_MMODE_FW_REQ_CB:
      qmi_voicei_fw_req_hdlr(&qmi_info->data.qmi_fw_info.req_cb.msg_hdr,
                               qmi_info->data.qmi_fw_info.req_cb.sdu_in);
      break;
    default:
      QM_MSG_ERROR("Unsupported qmi-voice fw cmd");
      break;
  }

  QMI_VOICE_CM_IF_MEM_FREE(qmi_info);

}/* qmi_voice_handle_fw_cmd */



/*=====================QMI-VOICE CMD DECODE & ENCODE APIS==================*/

/*===========================================================================
  FUNCTION QMI_VOICEI_DIAL_CALL()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_dial_call
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  uint16 vs_attrib_max_len=0;
  uint16 display_text_max_len=0;
  uint16 sip_overflow_uri_max_len = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_dial_call");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
 
  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  vs_attrib_max_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.vs_variant)+\
                      sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib_len)+\
                      sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib);

  sip_overflow_uri_max_len =  sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data_len)+\
                              sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data);

  display_text_max_len =  sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text)+\
                              sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text_len);

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        value = cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.calling_num;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.calling_num))
        {
          QM_MSG_MED("Invalid calling number length");
          errval = QMI_ERR_ARG_TOO_LONG;
          
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.calling_num_len =(uint8)len;
        got_v_in_required = TRUE;
        break;
      case VOICEI_DIAL_CALL_TYPE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.call_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.call_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.call_type_valid = TRUE;
        break;
      case VOICEI_DIAL_CLIR_TYPE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.clir_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.clir_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.clir_type_valid = TRUE;
        break;
      case VOICEI_DIAL_UUS_INFO:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.uus_info))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.uus_info;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.uus_info_valid= TRUE;
        break;
      case VOICEI_DIAL_CUG:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.cug_info))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.cug_info;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.cug_info_valid= TRUE;
        break;
      case VOICEI_DIAL_EMER_CAT:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.emer_cat);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.emer_cat;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.emer_cat_valid= TRUE;
        break;
      case VOICEI_DIAL_CALLED_PARTY_SUB_ADDRRESS:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.called_party_subaddress))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.called_party_subaddress;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.called_party_subaddress_valid= TRUE;
        break;
      case VOICEI_DIAL_SRV_TYPE:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.srv_type))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.srv_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.srv_type_valid= TRUE;
        break;        
      case VOICEI_DIAL_SIP_URI_OVERFLOW:
        if(len > sip_overflow_uri_max_len)
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        
        if ( !(QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data_len, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data_len)) &&
               QMI_VOICEI_PULL(sdu_in, cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data, cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_data_len)) )
        {
          QM_MSG_MED("Invalid length in SIP overflow TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }

        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.sip_uri_overflow_valid= TRUE;
        continue;
      case VOICEI_DIAL_AUDIO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.audio_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.audio_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.audio_attrib_valid= TRUE;
        break;
      case VOICEI_DIAL_VIDEO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.video_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.video_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.video_attrib_valid= TRUE;
        break;                  
      case VOICEI_DIAL_IP_CALL_PI:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ip_call_pi))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ip_call_pi;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ip_call_pi_valid= TRUE;
        break;
      case VOICEI_DIAL_ECALL_VARIANT:
        if(len != sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ecall_variant))
        {
          errval = QMI_ERR_INVALID_ARG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ecall_variant;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.ecall_variant_valid = TRUE;
        break;
      case VOICEI_DIAL_VS_CALL_ATTRIB:
        if(len > vs_attrib_max_len)
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        if ( !(QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.vs_variant, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.vs_variant)) &&
               QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib_len, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib_len)) &&
               QMI_VOICEI_PULL(sdu_in, cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib, cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes.file_attrib_len)) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }        
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.vs_call_attributes_valid = TRUE;
        continue;
      case VOICEI_DIAL_CONFERENCE_INIT_URI:

        value = cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.conf_init_uri;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.conf_init_uri))
        {
          QM_MSG_MED_1("Invalid conf creation URI length = %d", len);
          errval = QMI_ERR_ARG_TOO_LONG;
        
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.conf_init_uri_len = len;
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.conf_init_uri_valid= TRUE;
        break;
      case VOICEI_DIAL_DISPLAY_TEXT:
        if(len > display_text_max_len)
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        if ( !(QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text_len, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text_len)) &&
               QMI_VOICEI_PULL(sdu_in, cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text, 2*(cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.display_text.text_len))) )
        {
          QM_MSG_MED("Invalid display text data in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }        
        cmd_ptr->cmd.cm_if_data_ptr->data.dial_call.is_display_text_valid = TRUE;
        continue;

      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_DIAL_CALL;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }
  
  
  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_dial_call() */


/*===========================================================================
  FUNCTION QMI_VOICEI_DIAL_CALL_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_dial_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_dial_call_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.dial_call_resp.error;

  /* Fill Media ID if available */
  if(data_ptr->data.dial_call_resp.is_media_id_available)
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_RESP_MEDIA_ID,
                                      sizeof(data_ptr->data.dial_call_resp.media_id),
                                      (void *) &(data_ptr->data.dial_call_resp.media_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_CALL_ID,
                                      sizeof(data_ptr->data.dial_call_resp.call_id),
                                      (void *) &(data_ptr->data.dial_call_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( data_ptr->data.dial_call_resp.is_cc_modified )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.dial_call_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.dial_call_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.dial_call_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.dial_call_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.dial_call_resp.alpha_id.alpha_len) + \
                      data_ptr->data.dial_call_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.dial_call_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill End reason if available */
  if(data_ptr->data.dial_call_resp.is_end_reason_available )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_RESP_END_REASON,
                                      sizeof(data_ptr->data.dial_call_resp.end_reason),
                                      (void *) &(data_ptr->data.dial_call_resp.end_reason)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill sups data resulted from SIM call control */
  if(data_ptr->data.dial_call_resp.is_mod_to_sups)
  {
    sups_cc_data.srv_type = data_ptr->data.dial_call_resp.service_type;
    sups_cc_data.reason = data_ptr->data.dial_call_resp.reason;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_DIAL_CC_RESULT_SUPS,
                                      sizeof(sups_cc_data),
                                      (void *) &sups_cc_data))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response( errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE DIAL CALL operation");
  }
} /* qmi_voicei_dial_call_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_END_CALL()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_end_call
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_end_call");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  /* Deactivate the retransmission timer if started */
  qm_timer_stop(QM_TIMER_ID_INCOM_EVT_RESEND);

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.end_call.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.end_call.call_id;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_END_CALL;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}/* qmi_voicei_end_call() */


/*===========================================================================
  FUNCTION QMI_VOICEI_END_CALL_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_end_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_end_call_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.end_call_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_END_CALL_ID,
                                      sizeof(data_ptr->data.end_call_resp.call_id),
                                      (void *) &(data_ptr->data.end_call_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE END CALL operation");
  }

} /* qmi_voicei_end_call_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_ANSWER_CALL()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_answer_call
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_answer_call");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  /* Deactivate the retransmission timer if started */
  qm_timer_stop(QM_TIMER_ID_INCOM_EVT_RESEND);

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.call_id;
        got_v_in_required = TRUE;
        break;
      case VOICEI_ANSWER_CALL_TYPE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.call_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.call_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.call_type_valid = TRUE;
        break;
      case VOICEI_ANSWER_AUDIO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.audio_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.audio_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.audio_attrib_valid= TRUE;
        break;
      case VOICEI_ANSWER_VIDEO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.video_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.video_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.video_attrib_valid= TRUE;
        break;          
      case VOICEI_ANSWER_IP_CALL_PI:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.ip_call_pi))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.ip_call_pi;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.ip_call_pi_valid= TRUE;
        break;          
      case VOICEI_ANSWER_VS_FILE_ATTRIB:
        value = cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.vs_call_attrib.file_attrib;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.vs_call_attrib.file_attrib))
        {
          QM_MSG_MED_1("Invalid file attrib TLV length = %d", len);
          errval = QMI_ERR_ARG_TOO_LONG;
        
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.vs_call_attrib.file_attrib_len = len;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.vs_call_attrib_valid = TRUE;       
        break;
      case VOICEI_ANSWER_REJECT_CALL:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_call))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_call;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_call_valid= TRUE;
        break;
      case VOICEI_ANSWER_REJECT_CAUSE:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_cause))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_cause;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_cause_valid= TRUE;
        break;   
      case VOICEI_ANSWER_REJECT_SIP_CODE:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_sip_code))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_sip_code;
        cmd_ptr->cmd.cm_if_data_ptr->data.answer_call.reject_sip_code_valid= TRUE;
        break;   
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_ANSWER_CALL;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_SETUP_ANSWER()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_setup_answer
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_setup_answer");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.call_id;
        got_v_in_required = TRUE;
        break;

      case VOICEI_SETUP_ANSWER_REJECT_CALL:
        if(len != sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_call))
        {
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_call;
        cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_call_valid= TRUE;
        break;
      case VOICEI_SETUP_ANSWER_REJECT_CAUSE:
        if(len != sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_cause))
        {
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_cause;
        cmd_ptr->cmd.cm_if_data_ptr->data.setup_answer.reject_cause_valid= TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SETUP_ANSWER;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_ANSWER_CALL_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_answer_call_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_answer_call_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.answer_call_resp.error;
  response = NULL;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_ANSWER_CALL_ID,
                                      sizeof(data_ptr->data.answer_call_resp.call_id),
                                      (void *) &(data_ptr->data.answer_call_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE ANSWER CALL operation");
  }

}

/*===========================================================================
  FUNCTION QMI_VOICEI_SETUP_ANSWER_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_setup_answer_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_setup_answer_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.setup_answer_resp.error;
  response = NULL;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SETUP_ANSWER_RESP_CALL_ID,
                                      sizeof(data_ptr->data.setup_answer_resp.call_id),
                                      (void *) &(data_ptr->data.setup_answer_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SETUP ANSWER operation");
  }

}



/*===========================================================================
  FUNCTION QMI_VOICEI_SEND_FLASH()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_send_flash
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_send_flash");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
    case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.call_id;
        got_v_in_required = TRUE;
        break;
      case VOICEI_FLASH_PAYLOAD:
        value = cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_payload;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_payload))
        {
          QM_MSG_MED("Invalid calling number length");
          errval = QMI_ERR_ARG_TOO_LONG;
          
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_payload_len =(uint8)len;
        break;
      case VOICEI_FLASH_TYPE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.send_flash.flash_type_valid = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SEND_FLASH;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_send_flash() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SEND_FLASH_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_send_flash_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_send_flash_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.send_flash_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SEND_FLASH_CALL_ID,
                                      sizeof(data_ptr->data.send_flash_resp.call_id),
                                      (void *) &(data_ptr->data.send_flash_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SEND FLASH operation");
  }

} /* qmi_voicei_send_flash_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_CALL_INFO()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_get_call_info
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_get_call_info");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_info.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_info.call_id;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_GET_CALL_INFO;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_voicei_get_call_info() */

/*===========================================================================
  FUNCTION QMI_VOICEI_GET_CALL_INFO_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_get_call_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_voice_cm_if_get_call_info_resp_s *call_info_rsp;
  uint16 cnap_info_size = 0, uus_info_size = 0,alpha_info_size=0,conn_num_size=0,diags_size=0;
  uint8  end_reason_text_len = 0;
  uint16 num_len=0, sip_uri_len=0, rp_num_ext_len=0;
  int    tag=0;
  struct
  {
    uint8                   call_id;
    uint8                   call_state;
    uint8                   call_type;
    uint8                   direction;
    uint8                   mode;
  } call_info;

    struct
  {
    struct
    {
      uint8                 pi;
      uint8                 len;
    } fixed;
    uint8                   buf[QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN];
  } num_info;

  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_get_call_info_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_send_flash_resp");
    return;
  }

  call_info_rsp = (qmi_voice_cm_if_get_call_info_resp_s *)&data_ptr->data;
  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = call_info_rsp->error;
  response = NULL;

  if( QMI_ERR_NONE == errval )
  {
    if( (call_info_rsp->is_end_reason_text_available) && 
        (call_info_rsp->end_reason_text.end_reason_text_length < QMI_VOICE_CM_IF_MAX_END_REASON_TEXT_LEN) && 
        (call_info_rsp->end_reason_text.end_reason_text_length != 0) )
    {
      /* end_reason_text_length is the number of UTF16 characters */
      end_reason_text_len = sizeof(call_info_rsp->end_reason_text.end_reason_text_length) + 2*call_info_rsp->end_reason_text.end_reason_text_length;
      tag = VOICEI_GET_CALL_END_REASON_TEXT;
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)call_info_rsp->end_reason_text.end_reason_text_buf, 2*call_info_rsp->end_reason_text.end_reason_text_length) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->end_reason_text.end_reason_text_length, sizeof(call_info_rsp->end_reason_text.end_reason_text_length)) && 
          QMI_SVC_PKT_PUSH(&response, (void *)&end_reason_text_len, VOICEI_TLV_LENGTH_SIZE) && 
          QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)  ) )
      {          
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
      QMI_MSG_MED("end_reason_text = %s",call_info_rsp->end_reason_text.end_reason_text_buf);
    }
    if( call_info_rsp->is_num_available )
    {
      rp_num_ext_len = call_info_rsp->num.len + sizeof(call_info_rsp->num.len) + sizeof(call_info_rsp->num.num_plan) +\
                       sizeof(call_info_rsp->num.num_type) + sizeof(call_info_rsp->num.si) + sizeof(call_info_rsp->num.pi);
      tag = VOICEI_GET_CALL_RP_NUM_EXT;
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)call_info_rsp->num.buf, call_info_rsp->num.len) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.len, sizeof(call_info_rsp->num.len)) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.num_plan, sizeof(call_info_rsp->num.num_plan)) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.num_type, sizeof(call_info_rsp->num.num_type)) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.si, sizeof(call_info_rsp->num.si)) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.pi, sizeof(call_info_rsp->num.pi)) &&
          QMI_SVC_PKT_PUSH(&response, (void *)&rp_num_ext_len, VOICEI_TLV_LENGTH_SIZE) && 
          QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
      QMI_MSG_MED("Remote Pty Number Ext = %s for call id %d",(const char *)call_info_rsp->num.buf, call_info_rsp->call_id );
    }

    if(call_info_rsp->is_srvcc_call_available)
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_IS_SRVCC_CALL,
                                        sizeof(call_info_rsp->is_srvcc_call),
                                        (void *)&call_info_rsp->is_srvcc_call) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_num_available )
    {
      num_info.fixed.pi = call_info_rsp->num.pi;
      num_info.fixed.len = call_info_rsp->num.len;
      /* QMI_Security check*/  
      if(num_info.fixed.len > QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN)
      {
        num_info.fixed.len = QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN;
      }
      memscpy(num_info.buf, sizeof(num_info.buf), call_info_rsp->num.buf, num_info.fixed.len);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_RP_NUM,
                                        sizeof(num_info.fixed) + num_info.fixed.len,
                                        (void *) &num_info) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_num_available && (call_info_rsp->num.len>CM_MAX_CALLED_INFO_CHARS))
    {
      num_len = call_info_rsp->num.len;
      sip_uri_len = sizeof(num_len) + call_info_rsp->num.len;
      tag = VOICEI_GET_CALL_SIP_URI;
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)call_info_rsp->num.buf, call_info_rsp->num.len) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&call_info_rsp->num.len, sizeof(num_len)) && 
          QMI_SVC_PKT_PUSH(&response, (void *)&sip_uri_len, VOICEI_TLV_LENGTH_SIZE) && 
          QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)  ) )
      {          
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
      QMI_MSG_MED("SIP URI = %s",call_info_rsp->num.buf);
    }


    if( call_info_rsp->is_srv_opt_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_SO,
                                        sizeof(call_info_rsp->srv_opt),
                                        (void *)&call_info_rsp->srv_opt) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_voice_privacy_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_VP,
                                        sizeof(call_info_rsp->voice_privacy),
                                        (void *)&call_info_rsp->voice_privacy) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_otasp_status_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_OTSP,
                                        sizeof(call_info_rsp->otasp_status),
                                        (void *)&call_info_rsp->otasp_status) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_alerting_type_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_ALERTING,
                                        sizeof(call_info_rsp->alerting_type),
                                        (void *)&call_info_rsp->alerting_type) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_vs_call_variant_valid )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_VS_VARIANT,
                                        sizeof(call_info_rsp->vs_call_variant),
                                        (void *)&call_info_rsp->vs_call_variant) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_audio_attrib_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_AUDIO_ATTRIB,
                                        sizeof(call_info_rsp->audio_attrib),
                                        (void *)&call_info_rsp->audio_attrib) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_video_attrib_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_VIDEO_ATTRIB,
                                        sizeof(call_info_rsp->video_attrib),
                                        (void *)&call_info_rsp->video_attrib) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }    

    if( call_info_rsp->is_cnap_info_available )
    {
      cnap_info_size = sizeof(call_info_rsp->cnap_info.coding_scheme)+ \
                       sizeof(call_info_rsp->cnap_info.name_presentation) + \
                       sizeof(call_info_rsp->cnap_info.name_len) + \
                       call_info_rsp->cnap_info.name_len;
      
      /* Put cnap info */
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_RP_NAME,
                                        cnap_info_size,
                                        (void *)&call_info_rsp->cnap_info) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_uus_info_available)
    {
      uus_info_size = sizeof(call_info_rsp->uus_info.uus_type)+ \
                       sizeof(call_info_rsp->uus_info.uus_dcs) + \
                       sizeof(call_info_rsp->uus_info.uus_data_len) + \
                       call_info_rsp->uus_info.uus_data_len;
      
      /* Put uus info */
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_UUS_INFO,
                                        uus_info_size,
                                        (void *)&call_info_rsp->uus_info) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

      /* Put Alpha info */
    if( call_info_rsp->is_alpha_available )
    {
      alpha_info_size = sizeof(call_info_rsp->alpha_info.alpha_dcs)+ \
                                sizeof(call_info_rsp->alpha_info.alpha_len) + \
                                call_info_rsp->alpha_info.alpha_len;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_ALPHA,
                                        alpha_info_size,
                                        (void *)&call_info_rsp->alpha_info) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_conn_num_available )
    {
      conn_num_size = sizeof(call_info_rsp->conn_num_data.pi)+ \
                      sizeof(call_info_rsp->conn_num_data.si) + \
                      sizeof(call_info_rsp->conn_num_data.num_type) + \
                      sizeof(call_info_rsp->conn_num_data.num_plan) + \
                      sizeof(call_info_rsp->conn_num_data.len) + \
                      call_info_rsp->conn_num_data.len;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_CONN_NUM_INFO,
                                        conn_num_size,
                                        (void *)&call_info_rsp->conn_num_data) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_diagnostics_info_available )
    {
      diags_size = sizeof(call_info_rsp->diagnostic_data.diagnostic_length)+ \
                          call_info_rsp->diagnostic_data.diagnostic_length;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_DIAGNOSTICS,
                                        diags_size,
                                        (void *)&call_info_rsp->diagnostic_data) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    if( call_info_rsp->is_alerting_pattern_available )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CALL_ALERTING_PATTERN,
                                        sizeof(call_info_rsp->alerting_pattern),
                                        (void *)&call_info_rsp->alerting_pattern) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        goto send_result;
      }
    }

    /*Mandatory TLV*/
    call_info.call_id = call_info_rsp->call_id;
    call_info.call_state = call_info_rsp->call_state;
    call_info.call_type = call_info_rsp->call_type;
    call_info.direction = call_info_rsp->direction;
    call_info.mode = call_info_rsp->mode;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CALL_INFO,
                                      sizeof(call_info),
                                      (void *) &call_info))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
      goto send_result;
    }
  }

send_result:

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CALL INFO operation");
  }

} /* qmi_voicei_get_call_info_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SET_PREFERRED_PRIVACY()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_set_preferred_privacy
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_set_preferred_privacy");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_pref_privacy.privacy_pref);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_pref_privacy.privacy_pref;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SET_PREFERRED_PRIVACY;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_voicei_set_preferred_privacy() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SET_PREF_PRIVACY_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_set_pref_privacy_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_set_pref_privacy_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  response = NULL;
  errval = data_ptr->data.set_pref_privacy_resp.error;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SET PREFERRED PRIVACY operation");
  }

} /* qmi_voicei_set_pref_privacy_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_INDICATION_REGISTER()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_indication_register
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_priv_event=FALSE, got_dtmf_event=FALSE;
  boolean            got_sups_event=FALSE,got_call_event=FALSE, got_handover_event=FALSE;  
  boolean            got_speech_event=FALSE,got_ussd_notification_event=FALSE, got_modification_event=FALSE;  
  boolean            got_uus_event = FALSE, got_aoc_event=FALSE, got_conf_event=FALSE,got_ext_brst_intl_event = FALSE, got_page_miss_event_reg = FALSE;
  boolean            got_conf_parts_event_reg = FALSE;
  boolean            got_cc_result_event_reg = FALSE;
  boolean            got_tty_info_event = FALSE;
  boolean            got_audio_rat_change_event = FALSE;
  boolean            got_e911_orig_fail_event = FALSE;
  boolean            got_add_call_info_event = FALSE;
  uint8              priv_event, dtmf_event, sups_event, call_event, handover_event;
  uint8              speech_event, ussd_notification_event, modification_event, uus_event, aoc_event, conf_event,page_miss_event;
  uint8              ext_brst_intl_event,conf_parts_event,cc_result_event,tty_info_event, audio_rat_change_event, e911_orig_fail_event,add_call_info_event;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_indication_register");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_priv_event    = FALSE;
  got_dtmf_event    = FALSE;
  got_sups_event    = FALSE;
  got_call_event    = FALSE;
  got_conf_event    = FALSE;

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);

  if(client_sp != NULL)
  {
    priv_event = client_sp->voice_info.reg_voice_privacy_events;
    dtmf_event = client_sp->voice_info.reg_dtmf_events;
    sups_event = client_sp->voice_info.reg_sups_events;
    call_event = client_sp->voice_info.reg_call_events;
    handover_event = client_sp->voice_info.reg_handover_events;
    speech_event = client_sp->voice_info.reg_speech_events;
    ussd_notification_event = client_sp->voice_info.reg_ussd_notification_events;
    modification_event = client_sp->voice_info.reg_modification_events;
    uus_event = client_sp->voice_info.reg_uus_events;
    aoc_event = client_sp->voice_info.reg_aoc_events;
    conf_event = client_sp->voice_info.reg_conference_events;
    ext_brst_intl_event = client_sp->voice_info.reg_brst_intl_events;
    conf_parts_event = client_sp->voice_info.reg_conf_participants_events;
    page_miss_event = client_sp->voice_info.reg_mt_page_miss_events;
    cc_result_event = client_sp->voice_info.reg_cc_result_events;
    tty_info_event = client_sp->voice_info.reg_tty_info_events;
    audio_rat_change_event = client_sp->voice_info.reg_audio_rat_change_events;
    e911_orig_fail_event = client_sp->voice_info.reg_e911_orig_fail_events;
    add_call_info_event = client_sp->voice_info.reg_add_call_info_events;
  }
  else
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case VOICEI_IND_REG_VOICE_PRIV:
        expected_len = sizeof(priv_event);
        value = &priv_event;
        got_priv_event = TRUE;
        break;
      case VOICEI_IND_REG_DTMF:
        expected_len = sizeof(dtmf_event);
        value = &dtmf_event;
        got_dtmf_event = TRUE;
        break;
      case VOICEI_IND_REG_SUPS_NOTIFY:
        QM_MSG_MED("Sups notify type received");
        expected_len = sizeof(sups_event);
        value = &sups_event;
        got_sups_event = TRUE;
        break;
      case VOICEI_IND_REG_CALL_EVENTS_NOTIFY:
        QM_MSG_MED("Call events notify type received");
        expected_len = sizeof(call_event);
        value = &call_event;
        got_call_event = TRUE;
        break;

      case VOICEI_IND_REG_HANDOVER_NOTIFY:
        QM_MSG_MED("Handover notify type received");
        expected_len = sizeof(handover_event);
        value = &handover_event;
        got_handover_event = TRUE;
        break;

      case VOICEI_IND_REG_SPEECH_NOTIFY:
        QM_MSG_MED("Speech notify type received");
        expected_len = sizeof(speech_event);
        value = &speech_event;
        got_speech_event = TRUE;
        break;

      case VOICEI_IND_REG_USSD_NOTIFY:
        QM_MSG_MED("USSD notify type received");
        expected_len = sizeof(ussd_notification_event);
        value = &ussd_notification_event;
        got_ussd_notification_event = TRUE;
        break;

      case VOICEI_IND_REG_MODIFICATION_NOTIFY:
        QM_MSG_MED("Modification notify type received");
        expected_len = sizeof(modification_event);
        value = &modification_event;
        got_modification_event = TRUE;
        break;

      case VOICEI_IND_REG_UUS_NOTIFY:
        QM_MSG_MED("UUS notify type received");
        expected_len = sizeof(uus_event);
        value = &uus_event;
        got_uus_event = TRUE;
        break;

      case VOICEI_IND_REG_AOC_NOTIFY:
        QM_MSG_MED("AOC notify type received");
        expected_len = sizeof(aoc_event);
        value = &aoc_event;
        got_aoc_event = TRUE;
        break;

      case VOICEI_IND_REG_CONF_NOTIFY:
        QM_MSG_MED("Conf notify type received");
        expected_len = sizeof(conf_event);
        value = &conf_event;
        got_conf_event = TRUE;
        break;
      case VOICEI_IND_REG_EXT_BRST_INTL_NOTIFY:
        QM_MSG_MED("Ext burst intl type received");
        expected_len = sizeof(ext_brst_intl_event);
        value = &ext_brst_intl_event;
        got_ext_brst_intl_event = TRUE;
        break;
     case VOICEI_IND_REG_CONF_PARTS_NOTIFY:
       QM_MSG_MED("Conference participants type received");
       expected_len = sizeof(conf_parts_event);
       value = &conf_parts_event;
       got_conf_parts_event_reg = TRUE;
       break;
     case VOICEI_IND_REG_MT_PAGE_MISS_NOTIFY:
       QM_MSG_MED("MT Page miss type received");
       expected_len = sizeof(page_miss_event);
       value = &page_miss_event;
       got_page_miss_event_reg = TRUE;
       break;

     case VOICEI_IND_REG_CC_RESULT_NOTIFY:
       QM_MSG_MED("CC result type received");
       expected_len = sizeof(cc_result_event);
       value = &cc_result_event;
       got_cc_result_event_reg = TRUE;
       break;
     case VOICEI_IND_REG_TTY_INFO_NOTIFY:
       QM_MSG_MED("TTY Info type received");
       expected_len = sizeof(tty_info_event);
       value = &tty_info_event;
       got_tty_info_event = TRUE;
       break;
     case VOICEI_IND_REG_AUDIO_RAT_CHANGE_NOTIFY:
       QM_MSG_MED("Audio RAT Change type received");
       expected_len = sizeof(audio_rat_change_event);
       value = &audio_rat_change_event;
       got_audio_rat_change_event = TRUE;
       break;
     case VOICEI_IND_REG_E911_ORIG_FAIL_NOTIFY:
       QM_MSG_MED ("E911 Orig Fail type received");
       expected_len = sizeof(e911_orig_fail_event);
       value = &e911_orig_fail_event;
       got_e911_orig_fail_event = TRUE;
       break;
     case VOICEI_IND_REG_ADD_CALL_INFO_NOTIFY:
       QM_MSG_MED ("Additional call info type received");
       expected_len = sizeof(add_call_info_event);
       value = &add_call_info_event;
       got_add_call_info_event = TRUE;
       break;  
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if (got_priv_event||got_dtmf_event||got_sups_event ||got_call_event ||
      got_handover_event||got_speech_event||got_ussd_notification_event||
      got_modification_event||got_uus_event ||got_aoc_event||got_conf_event||got_ext_brst_intl_event || got_conf_parts_event_reg || 
      got_page_miss_event_reg || got_cc_result_event_reg || got_tty_info_event || got_audio_rat_change_event || got_e911_orig_fail_event|| got_add_call_info_event)
  {
    QM_MSG_MED("Received registration for some events");
  }
  else
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  client_sp->voice_info.reg_voice_privacy_events = priv_event;
  client_sp->voice_info.reg_dtmf_events          = dtmf_event;
  client_sp->voice_info.reg_sups_events          = sups_event;
  client_sp->voice_info.reg_call_events          = call_event;
  client_sp->voice_info.reg_handover_events      = handover_event;
  client_sp->voice_info.reg_speech_events      = speech_event;
  client_sp->voice_info.reg_ussd_notification_events      = ussd_notification_event;
  client_sp->voice_info.reg_modification_events      = modification_event;
  client_sp->voice_info.reg_uus_events      = uus_event;
  client_sp->voice_info.reg_aoc_events                    = aoc_event;
  client_sp->voice_info.reg_conference_events      = conf_event;
  client_sp->voice_info.reg_brst_intl_events   = ext_brst_intl_event;
  client_sp->voice_info.reg_mt_page_miss_events      = page_miss_event;
  client_sp->voice_info.reg_cc_result_events      = cc_result_event;
  client_sp->voice_info.reg_conf_participants_events      = conf_parts_event;
  client_sp->voice_info.reg_tty_info_events      = tty_info_event;
  client_sp->voice_info.reg_audio_rat_change_events      = audio_rat_change_event;
  client_sp->voice_info.reg_e911_orig_fail_events = e911_orig_fail_event;
  client_sp->voice_info.reg_add_call_info_events      = add_call_info_event;

  QM_MSG_MED_3("voice ind_reg with sups event = %d, call event = %d, handover event = %d", sups_event, call_event, handover_event);
  QM_MSG_MED_3("voice ind_reg with speech event = %d, ussd not event = %d, modifn event = %d", speech_event, ussd_notification_event, modification_event);
  QM_MSG_MED_3("voice ind_reg with uus event = %d, aoc event = %d, conf event = %d", uus_event, aoc_event, conf_event);
  QM_MSG_MED_3("voice ind_reg with ext brst intl event = %d, page miss event = %d, cc result event = %d", ext_brst_intl_event, page_miss_event, cc_result_event);
  QM_MSG_MED_3("voice ind_reg with conf parts = %d, tty info event = %d,e911_orig_fail=%d", conf_parts_event, tty_info_event,e911_orig_fail_event);
  QM_MSG_MED_2("voice ind_reg with add call info %d, audio rat change event = %d", add_call_info_event, audio_rat_change_event);

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_BURST_DTMF_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_burst_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_burst_dtmf_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_burst_dtmf_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.burst_dtmf_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_BURST_DTMF_CALL_ID,
                                      sizeof(data_ptr->data.burst_dtmf_resp.call_id),
                                      (void *) &(data_ptr->data.burst_dtmf_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE BURST DTMF operation");
  }

} /* qmi_voicei_burst_dtmf_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_START_CONT_DTMF_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_start_cont_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_start_cont_dtmf_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_start_cont_dtmf_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.start_cont_dtmf_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_START_CONT_DTMF_CALL_ID,
                                      sizeof(data_ptr->data.start_cont_dtmf_resp.call_id),
                                      (void *) &(data_ptr->data.start_cont_dtmf_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE START CONT DTMF operation");
  }

} /* qmi_voicei_start_cont_dtmf_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_STOP_CONT_DTMF_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_stop_cont_dtmf_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_stop_cont_dtmf_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_stop_cont_dtmf_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.stop_cont_dtmf_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_STOP_CONT_DTMF_CALL_ID,
                                      sizeof(data_ptr->data.stop_cont_dtmf_resp.call_id),
                                      (void *) &(data_ptr->data.stop_cont_dtmf_resp.call_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE STOP CONT DTMF operation");
  }

} /* qmi_voicei_stop_cont_dtmf_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_ALL_CALL_INFO()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_get_all_call_info
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_get_all_call_info");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  /* default the subs to primary */
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_GET_ALL_CALL_INFO;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  if( client_sp->asubs_info.is_bind_set_by_client )
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_get_all_call_info() */



/*===========================================================================
  FUNCTION QMI_VOICEI_GET_ALL_CALL_INFO_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_get_all_call_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_voice_cm_if_all_call_info_s       *all_call_info;
  uint8     i, num_of_instances=0,alpha_len=0;
  int       tot_len=0, uus_len, cnap_len, rp_num_len, tag, diags_len=0, con_len=0;
  int       redirect_len=0,called_len=0;
  uint16    num_len=0;
  struct
  {
    uint8      call_id;
    uint8      call_state;
    uint8      call_type;
    uint8      direction;
    uint8      mode;
    uint8      mpty;
    uint8      als;
  }basic_info;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_get_all_call_info_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_get_all_call_info_resp");
    return;
  }

  all_call_info = (qmi_voice_cm_if_all_call_info_s*)&data_ptr->data.all_call_info_resp.info;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.all_call_info_resp.error;


  if( QMI_ERR_NONE == errval )
  {
    num_of_instances = 0;
    tot_len = 0;
    
    /* Remote Party Number Ext Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
      
      if( info->is_num_available )
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)info->num.buf, info->num.len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.len, sizeof(info->num.len)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.num_plan, sizeof(info->num.num_plan)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.num_type, sizeof(info->num.num_type)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.si, sizeof(info->num.si)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.pi, sizeof(info->num.pi)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += info->num.len + sizeof(info->num.len) + sizeof(info->num.num_plan) +\
                   sizeof(info->num.num_type) + sizeof(info->num.si) + sizeof(info->num.pi) + sizeof(info->call_id);
        QMI_MSG_MED("RP Number Ext = %s for call id %d",(const char *)info->num.buf, info->call_id );
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_RP_NUM_EXT;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    num_of_instances = 0;
    tot_len = 0;

    /* End Reason Text Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];

      if( (info->is_end_reason_text_available) && 
          (info->end_reason_text.end_reason_text_length < QMI_VOICE_CM_IF_MAX_END_REASON_TEXT_LEN) && 
          (info->end_reason_text.end_reason_text_length != 0) )
      {
        /* end_reason_text_length is the number of UTF16 characters */
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->end_reason_text.end_reason_text_buf, 2*info->end_reason_text.end_reason_text_length) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&info->end_reason_text.end_reason_text_length, sizeof(info->end_reason_text.end_reason_text_length)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))))
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->end_reason_text.end_reason_text_length) + 2*info->end_reason_text.end_reason_text_length + sizeof(info->call_id);
        QM_MSG_MED_1("end_reason_text added for call id %d", info->call_id);
      }
    }

    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_END_REASON_TEXT;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    num_of_instances = 0;
    tot_len = 0;

    /* Call Attribute Status Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];

      if(info->is_call_attrib_status_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->call_attrib_status, sizeof(info->call_attrib_status)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
      
        num_of_instances++;
        tot_len += sizeof(info->call_attrib_status) + sizeof(info->call_id);
        QM_MSG_MED_2("call_attrib_status = %d for call id %d", info->call_attrib_status, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_ATTRIB_STATUS;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    num_of_instances = 0;
    tot_len = 0;

    /* Is SRVCC  Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];

      if(info->is_srvcc_call_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->is_srvcc_call, sizeof(info->is_srvcc_call)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->is_srvcc_call) + sizeof(info->call_id);
      }
      QM_MSG_MED_2("Is SRVCC CALL = %d for call id %d", info->is_srvcc_call, info->call_id);
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_IS_SRVCC_CALL;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    num_of_instances = 0;
    tot_len = 0;

    /* Alerting Pattern Optional TLV */
    for( i=0; i<all_call_info->num_of_calls; i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_alerting_pattern_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->alerting_pattern, sizeof(info->alerting_pattern)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->alerting_pattern) + sizeof(info->call_id);
        QM_MSG_MED_2("Alerting Pattern = %d for call id %d", info->alerting_pattern, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_ALERTING_PATTERN;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
      

      
    /* Connected number Info Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_conn_num_available)
      {
        con_len = sizeof(info->conn_num_data.pi) + \
                  sizeof(info->conn_num_data.si) + \
                  sizeof(info->conn_num_data.num_type) + \
                  sizeof(info->conn_num_data.num_plan) + \
                  sizeof(info->conn_num_data.len) + \
                  info->conn_num_data.len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->conn_num_data, con_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += con_len + sizeof(info->call_id);
        QMI_MSG_MED("Connected num info pi= %d,si=%d,num_len=%d for call id %d",info->conn_num_data.pi,info->conn_num_data.si,info->conn_num_data.len, info->call_id );
      }
    }
        
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_CONNECTED_NUM;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
        
    num_of_instances = 0;
    tot_len = 0;
      
    /* Called party number Info Optional TLV */
    for( i=0; i<all_call_info->num_of_calls; i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_called_party_num_available)
      {
        called_len = sizeof(info->called_party_num_data.pi) + \
                     sizeof(info->called_party_num_data.si) + \
                     sizeof(info->called_party_num_data.num_type) + \
                     sizeof(info->called_party_num_data.num_plan) + \
                     sizeof(info->called_party_num_data.len) + \
                     info->called_party_num_data.len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->called_party_num_data, called_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += called_len + sizeof(info->call_id);
        QMI_MSG_MED("Called party num info pi= %d,si=%d,num_len=%d for call id %d",info->called_party_num_data.pi,
                     info->called_party_num_data.si,info->called_party_num_data.len, info->call_id );
      }
    }
        
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_CALLED_PARTY_NUM;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    num_of_instances = 0;
    tot_len = 0;
      
    /* Redirecting party number Info Optional TLV */
    for( i=0; i<all_call_info->num_of_calls; i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_redirecting_party_num_available)
      {
        redirect_len = sizeof(info->redirecting_party_num_data.pi) + \
                       sizeof(info->redirecting_party_num_data.si) + \
                       sizeof(info->redirecting_party_num_data.num_type) + \
                       sizeof(info->redirecting_party_num_data.num_plan) + \
                       sizeof(info->redirecting_party_num_data.len) + \
                       info->redirecting_party_num_data.len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->redirecting_party_num_data, redirect_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += redirect_len + sizeof(info->call_id);
        QMI_MSG_MED("Redirect party num info pi= %d,si=%d,num_len=%d for call id %d",info->redirecting_party_num_data.pi,
                     info->redirecting_party_num_data.si,info->redirecting_party_num_data.len, info->call_id );
      }
    }
        
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_REDIRECTING_PARTY_NUM;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
        
    num_of_instances = 0;
    tot_len = 0;
      
    /* Call End Diagnostics Info Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
      
      if(info->is_diagnostics_info_available)
      {
        diags_len = sizeof(info->diagnostic_data.diagnostic_length) + \
                    info->diagnostic_data.diagnostic_length ;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->diagnostic_data, diags_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += diags_len + sizeof(info->call_id);
        QM_MSG_MED_2("Diagnostic info len=%d for call id %d", info->diagnostic_data.diagnostic_length, info->call_id);
      }
    }
        
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_DIAGNOSTICS;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    tot_len = 0;
    num_of_instances = 0;

    /* VS Variant Attributes Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_vs_call_variant_valid)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->vs_call_variant, sizeof(info->vs_call_variant)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->vs_call_variant) + sizeof(info->call_id);
        QM_MSG_MED_2("VS variant Attributes = %d for call id %d", info->vs_call_variant, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_VS_VARIANT;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    tot_len = 0;
    num_of_instances = 0;

    /* Audio Attributes Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_audio_attrib_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->audio_attrib, sizeof(info->audio_attrib)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->audio_attrib) + sizeof(info->call_id);
        QM_MSG_MED_2("Audio Attributes = %d for call id %d", info->audio_attrib, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_AUDIO_ATTRIBUTE;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    tot_len = 0;
    num_of_instances = 0;

    /* Video Attributes Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_video_attrib_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->video_attrib, sizeof(info->video_attrib)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->video_attrib) + sizeof(info->call_id);
        QM_MSG_MED_2("Video Attributes = %d for call id %d", info->video_attrib, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_VIDEO_ATTRIBUTE;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }    

    num_of_instances = 0;
    tot_len = 0;

    /* Alpha Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_alpha_available)
      {
        alpha_len = sizeof(info->alpha_info.alpha_dcs) + \
                          sizeof(info->alpha_info.alpha_len) + \
                          info->alpha_info.alpha_len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->alpha_info, alpha_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += alpha_len + sizeof(info->call_id);
        QM_MSG_MED_3("Alpha coding scheme = %d name length =%d for call id %d", info->alpha_info.alpha_dcs, info->alpha_info.alpha_len, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_ALPHA;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }

    tot_len = 0;
    num_of_instances = 0;

    /* Call End Reason Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_end_reason_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->end_reason, sizeof(info->end_reason)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->end_reason) + sizeof(info->call_id);
        QM_MSG_MED_2("End reason = %d for call id %d", info->end_reason, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_END_REASON;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    i = 0;
    
    /* Voice Privacy Optional TLV */
    /* For CDMA only one call at a time */
    if(all_call_info->num_of_calls == 1)
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_voice_privacy_available)
      {
        tag = VOICEI_GET_ALL_CALL_VP;
        tot_len = sizeof(info->voice_privacy);
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->voice_privacy, sizeof(info->voice_privacy))  &&
                QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ))
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
        }
        QM_MSG_MED_2("Voice Privacy = %d for call id %d", info->voice_privacy, info->call_id);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    i = 0;
    
    /* Otasp Status Optional TLV */
    /* For CDMA only one call at a time */
    if(all_call_info->num_of_calls == 1)
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
      if(info->is_otasp_status_available)
      {
        tag = VOICEI_GET_ALL_CALL_OTASP;
        tot_len = sizeof(info->otasp_status);
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->otasp_status, sizeof(info->otasp_status)) &&
                QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ))
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
        }
        QM_MSG_MED_2("Otasp Status = %d for call id %d", info->otasp_status, info->call_id);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* Service Opt Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS) ; i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_srv_opt_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->srv_opt, sizeof(info->srv_opt)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->srv_opt) + sizeof(info->call_id);
        QM_MSG_MED_2("Service option = %d for call id %d", info->srv_opt, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_SO;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* UUS Info Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_uus_info_available)
      {
        uus_len = sizeof(info->uus_info.uus_type) + \
                  sizeof(info->uus_info.uus_dcs) + \
                  sizeof(info->uus_info.uus_data_len) + \
                  info->uus_info.uus_data_len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->uus_info, uus_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += uus_len + sizeof(info->call_id);
        QM_MSG_MED_3("UUS Type = %d Dcs =%d for call id %d", info->uus_info.uus_type, info->uus_info.uus_dcs, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_UUS_INFO;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* Alerting Type Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_alerting_type_available)
      {
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->alerting_type, sizeof(info->alerting_type)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += sizeof(info->alerting_type) + sizeof(info->call_id);
        QM_MSG_MED_2("Alerting type = %d for call id %d", info->alerting_type, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_ALERTING;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* Cnap Info Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_cnap_info_available)
      {
        cnap_len = sizeof(info->cnap_info.name_presentation) + \
                   sizeof(info->cnap_info.coding_scheme) + \
                   sizeof(info->cnap_info.name_len) + \
                   info->cnap_info.name_len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->cnap_info, cnap_len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += cnap_len + sizeof(info->call_id);
        QM_MSG_MED_3("Cnap coding scheme = %d name length =%d for call id %d", info->cnap_info.coding_scheme, info->cnap_info.name_len, info->call_id);
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_RP_NAME;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* Remote Party Number Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
      if(info->is_num_available && (info->num.len<=CM_MAX_CALLED_INFO_CHARS))
      {
        rp_num_len = sizeof(info->num.pi)+ sizeof(info->num.len) + info->num.len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)&info->num.buf, info->num.len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.len, sizeof(info->num.len)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.pi, sizeof(info->num.pi)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += rp_num_len + sizeof(info->call_id);
        QMI_MSG_MED("Rp Number = %s for call id %d",(const char *)info->num.buf, info->call_id );
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_RP_NUM;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }
    
    num_of_instances = 0;
    tot_len = 0;
    
    /* SIP URI Optional TLV */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
  
      if(info->is_num_available && (info->num.len>CM_MAX_CALLED_INFO_CHARS))
      {
        rp_num_len = sizeof(info->num.len) + info->num.len;
        num_len = info->num.len;
        if( !(QMI_SVC_PKT_PUSH(&response, (void*)info->num.buf, info->num.len) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->num.len, sizeof(num_len)) &&
            QMI_SVC_PKT_PUSH(&response, (void*)&info->call_id, sizeof(info->call_id))) )
        {
          num_of_instances = 0;
          dsm_free_packet(&response);
          break;
        }
        num_of_instances++;
        tot_len += rp_num_len + sizeof(info->call_id);
        QMI_MSG_MED("SIP URI = %s for call id %d",(const char *)info->num.buf, info->call_id );
      }
    }
    
    if( num_of_instances > 0 )
    {
      tot_len += sizeof(num_of_instances);
      tag = VOICEI_GET_ALL_CALL_SIP_URI;
      /* Put the num_of_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, (void *)&num_of_instances, sizeof(num_of_instances)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        dsm_free_packet(&response);
      }
    }    

    tot_len = 0;
    
    /* Fill Mandatory basic call information */
    for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
    {
      const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
      /* Fill the basic call info */
      basic_info.call_id = info->call_id;
      basic_info.call_state = info->call_state;
      basic_info.call_type = info->call_type;
      basic_info.direction = info->direction;
      basic_info.mode = info->mode;
      basic_info.mpty = info->is_mpty;
      basic_info.als = info->als;
      if( !QMI_SVC_PKT_PUSH(&response, &basic_info, sizeof(basic_info)) )
      {
        dsm_free_packet(&response);
        errval = QMI_ERR_NO_MEMORY;
        goto send_result;
      }
      tot_len += sizeof(basic_info);
      QM_MSG_MED_1("Call information for call id %d is:", info->call_id);
      QM_MSG_MED_3("Call State =%d, call type =%d, direction %d", info->call_state, info->call_type, info->direction);
      QM_MSG_MED_3("Call Mode =%d, is mpty =%d, als =%d", info->mode, info->is_mpty, info->als);
    }
    
    tot_len += sizeof(all_call_info->num_of_calls);
    /* For get all call info response */
    tag = VOICEI_GET_ALL_CALL_INFO;

    /* Put the num_of_calls, total length, Tag id */
    if(!(QMI_SVC_PKT_PUSH(&response, (void *)&all_call_info->num_of_calls, 
                                                sizeof(all_call_info->num_of_calls)) &&
         QMI_SVC_PKT_PUSH(&response, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
         QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)
         ) )
    {
      dsm_free_packet(&response);
      errval = QMI_ERR_NO_MEMORY;
    }
  }

send_result:
  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET ALL CALL INFO operation");
  }
} /* qmi_voicei_get_all_call_info_resp() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_MANAGE_CALLS_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    None 
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_manage_calls_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  struct
  {
    uint16 failure_cause;
  }manage_calls_err_resp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_manage_calls_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_manage_calls_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.manage_calls_resp.error;

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    manage_calls_err_resp.failure_cause = data_ptr->data.manage_calls_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_MNG_CALLS_FAILURE_CAUSE,
                                      sizeof(manage_calls_err_resp),
                                      (void *) &manage_calls_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE MANAGE CALLS operation");
  }
}/* qmi_voicei_sups_manage_calls_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CLIP_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_clip_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  struct
  {
    uint8 active_status;
    uint8 provision_status;
  }clip_resp;

  struct
  {
    uint16 failure_cause;
  }clip_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_clip_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_clip_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_clip_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_clip_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIP_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_clip_resp.retry_duration),
                                      (void *) &data_ptr->data.get_clip_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    clip_resp.active_status = data_ptr->data.get_clip_resp.active_status;
    clip_resp.provision_status = data_ptr->data.get_clip_resp.provision_status;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIP_RESP,
                                      sizeof(clip_resp),
                                      (void *) &clip_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    clip_err_resp.failure_cause = data_ptr->data.get_clip_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIP_FAILURE_CAUSE,
                                      sizeof(clip_err_resp),
                                      (void *) &clip_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( data_ptr->data.get_clip_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_clip_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CLIP_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CLIP_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid data call control in response");
    }
    
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIP_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_clip_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_clip_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_clip_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_clip_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_clip_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_clip_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIP_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_clip_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CLIP operation");
  }

}/*qmi_voicei_sups_get_clip_resp*/


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_COLP_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_colp_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint8                   total_alpha_len = 0;
  struct
  {
    uint8 active_status;
    uint8 provision_status;
  }colp_resp;

  struct
  {
    uint16 failure_cause;
  }colp_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_colp_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_colp_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_colp_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_colp_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLP_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_colp_resp.retry_duration),
                                      (void *) &(data_ptr->data.get_colp_resp.retry_duration)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    colp_resp.active_status = data_ptr->data.get_colp_resp.active_status;
    colp_resp.provision_status = data_ptr->data.get_colp_resp.provision_status;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLP_RESP,
                                      sizeof(colp_resp),
                                      (void *) &colp_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    colp_err_resp.failure_cause = data_ptr->data.get_colp_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLP_FAILURE_CAUSE,
                                      sizeof(colp_err_resp),
                                      (void *) &colp_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.get_colp_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_colp_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_COLP_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_COLP_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid data call control in response");
    }
    
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLP_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_colp_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_colp_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_colp_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_colp_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_colp_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_colp_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLP_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_colp_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET COLP operation");
}
}/* qmi_voicei_sups_get_colp_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_WAITING_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_call_waiting_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  struct
  {
    uint8 service_class;
  }call_waiting_resp;

  struct
  {
    uint32 service_class_ext;
  }call_waiting_ext_resp;

  struct
  {
    uint16 failure_cause;
  }call_err_waiting_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_waiting_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_call_waiting_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_call_waiting_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_call_waiting_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_call_waiting_resp.retry_duration),
                                      (void *) &data_ptr->data.get_call_waiting_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    call_waiting_resp.service_class = data_ptr->data.get_call_waiting_resp.service_class;
    QM_MSG_MED_1("Sending QMI GET Call waiting with service_class = %d", call_waiting_resp.service_class);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_SC,
                                      sizeof(call_waiting_resp),
                                      (void *) &call_waiting_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    call_waiting_ext_resp.service_class_ext = data_ptr->data.get_call_waiting_resp.service_class_ext;
    QM_MSG_MED_1("Sending QMI GET Call waiting with service_class_ext = %d", call_waiting_ext_resp.service_class_ext);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_RESP_SC_EXT,
                                      sizeof(call_waiting_ext_resp),
                                      (void *) &call_waiting_ext_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    call_err_waiting_resp.failure_cause = data_ptr->data.get_call_waiting_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_FAILURE_CAUSE,
                                      sizeof(call_err_waiting_resp),
                                      (void *) &call_err_waiting_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.get_call_waiting_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_call_waiting_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CW_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CW_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid call control data in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_call_waiting_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_call_waiting_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_call_waiting_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_call_waiting_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_call_waiting_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_call_waiting_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CW_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_call_waiting_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CALL WAITING operation");
  }
}/* qmi_voicei_sups_get_call_waiting_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_BARRING_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    service class for the specified reason/facility for which call barring is active 
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_call_barring_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  uint16                  total_barr_list_tlv_size = 0;
  uint8                   barr_sc_count=0, barr_num_count=0;
  uint8 tag=0;
  uint8 i=0, j=0;
  struct
  {
    uint8 service_class;
  }call_barring_resp;

  struct
  {
    uint32 service_class_ext;
  }call_barring_ext_resp;

  struct
  {
    uint16 failure_cause;
  }get_call_barring_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_barring_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_call_barring_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_call_barring_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_call_barring_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_call_barring_resp.retry_duration),
                                      (void *) &data_ptr->data.get_call_barring_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    call_barring_resp.service_class = data_ptr->data.get_call_barring_resp.service_class;
    QM_MSG_MED_1("Sending QMI GET Call barring with service_class = %d", call_barring_resp.service_class);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_SC,
                                      sizeof(call_barring_resp),
                                      (void *) &call_barring_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    call_barring_ext_resp.service_class_ext = data_ptr->data.get_call_barring_resp.service_class_ext;
    QM_MSG_MED_1("Sending QMI GET Call barring with service_class_ext = %d", call_barring_ext_resp.service_class_ext);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_RESP_SC_EXT,
                                      sizeof(call_barring_ext_resp),
                                      (void *) &call_barring_ext_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    get_call_barring_err_resp.failure_cause = data_ptr->data.get_call_barring_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_FAILURE_CAUSE,
                                      sizeof(get_call_barring_err_resp),
                                      (void *) &get_call_barring_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.get_call_barring_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_call_barring_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CB_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CB_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid call control data in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_call_barring_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_call_barring_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }

  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_call_barring_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_call_barring_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_call_barring_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_call_barring_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CB_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_call_barring_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  
  //Fill TLV 0x17: Barred Number List
  if(data_ptr->data.get_call_barring_resp.sc_barr_num_list_valid)
  {
    tag = VOICEI_GET_CB_RESP_BARR_LIST;

    barr_sc_count = data_ptr->data.get_call_barring_resp.sc_barr_num_list_len;

    for(i=0;i<barr_sc_count;i++)
    {
      const qmi_voice_cm_if_icb_resp_s *barred_num = &data_ptr->data.get_call_barring_resp.sc_barr_num_list_info[i];
      barr_num_count = barred_num->barr_num_list_len;

      for(j=0;j<barr_num_count && j<QMI_VOICE_CM_IF_MAX_BARR_LIST_LEN;j++) 
      {
        const qmi_voice_cm_if_icb_sc_s *barred_num_sc = &barred_num->barr_num_list_info[j];
        if( !(QMI_SVC_PKT_PUSH(&response, (void *)barred_num_sc->barr_num, barred_num_sc->barr_num_len) &&
              QMI_SVC_PKT_PUSH(&response, (void *)&barred_num_sc->barr_num_len, sizeof(barred_num_sc->barr_num_len)) &&
              QMI_SVC_PKT_PUSH(&response, (void *)&barred_num_sc->barr_num_status, sizeof(barred_num_sc->barr_num_status))) )
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
        }  
        total_barr_list_tlv_size = total_barr_list_tlv_size+\
                                   sizeof(barred_num_sc->barr_num_status)+\
                                   sizeof(barred_num_sc->barr_num_len)+\
                                   barred_num_sc->barr_num_len;
      }
      if(barred_num->barr_num_list_info)
      {
        QMI_VOICE_CM_IF_MEM_FREE((void *)barred_num->barr_num_list_info);
      }
    
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)&barr_num_count, sizeof(barr_num_count)) &&
            QMI_SVC_PKT_PUSH(&response, (void *)&barred_num->barr_sc_ext, sizeof(barred_num->barr_sc_ext))) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
      total_barr_list_tlv_size = total_barr_list_tlv_size+\
                                 sizeof(barred_num->barr_sc_ext)+\
                                 sizeof(barr_num_count);
    }

    total_barr_list_tlv_size += sizeof(barr_sc_count);

    if( !(QMI_SVC_PKT_PUSH(&response, (void *)&barr_sc_count, sizeof(barr_sc_count)) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&total_barr_list_tlv_size, VOICEI_TLV_LENGTH_SIZE) &&
           QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }    
  }
  
  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CALL BARRING operation");
  }
}/* qmi_voicei_sups_get_call_barring_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_SET_CALL_BARRING_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    None 
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_set_call_barring_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  struct
  {
    uint16 failure_cause;
  }set_call_barring_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_set_call_barring_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_set_call_barring_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.set_call_barring_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.set_call_barring_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_CB_PWD_RETRY_DURATION,
                                      sizeof(data_ptr->data.set_call_barring_resp.retry_duration),
                                      (void *) &(data_ptr->data.set_call_barring_resp.retry_duration)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    set_call_barring_err_resp.failure_cause = data_ptr->data.set_call_barring_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_CB_PWD_FAILURE_CAUSE,
                                      sizeof(set_call_barring_err_resp),
                                      (void *) &set_call_barring_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.set_call_barring_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.set_call_barring_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_SET_CB_PWD_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_SET_CB_PWD_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid call control data in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_CB_PWD_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.set_call_barring_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.set_call_barring_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.set_call_barring_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.set_call_barring_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.set_call_barring_resp.alpha_id.alpha_len) + \
                      data_ptr->data.set_call_barring_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_CB_PWD_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.set_call_barring_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SET CALL BARRING operation");
  }
}/* qmi_voicei_sups_set_call_barring_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_SET_SUPS_SERVICE_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    None 
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_set_sups_service_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  struct
  {
    uint16 failure_cause;
  }set_sups_service_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;
  uint16                   tot_failure_desc_len;
  uint8                    tag; 
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_set_sups_service_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_set_sups_service_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.set_sups_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.set_sups_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_SUPS_RETRY_DURATION,
                                      sizeof(data_ptr->data.set_sups_resp.retry_duration),
                                      (void *) &data_ptr->data.set_sups_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Failure description optional TLV */
  if(data_ptr->data.set_sups_resp.is_failure_desc_available == TRUE)
  {
    QM_MSG_MED_1("Sending failure desc len = %d ", data_ptr->data.set_sups_resp.failure_desc_len);

    tot_failure_desc_len = sizeof(data_ptr->data.set_sups_resp.failure_desc_len) + \
                            (data_ptr->data.set_sups_resp.failure_desc_len)*2;
    tag = VOICEI_SET_SUPS_FAILURE_DESC;

    if( !(QMI_SVC_PKT_PUSH(&response, (void*)data_ptr->data.set_sups_resp.failure_desc, (data_ptr->data.set_sups_resp.failure_desc_len)*2) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&data_ptr->data.set_sups_resp.failure_desc_len, sizeof(data_ptr->data.set_sups_resp.failure_desc_len)) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&tot_failure_desc_len, VOICEI_TLV_LENGTH_SIZE) &&
          QMI_SVC_PKT_PUSH(&response, (void*)&tag, VOICEI_TLV_TAG_SIZE)) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }    
  }

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    set_sups_service_err_resp.failure_cause = data_ptr->data.set_sups_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_SUPS_FAILURE_CAUSE,
                                      sizeof(set_sups_service_err_resp),
                                      (void *) &set_sups_service_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.set_sups_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.set_sups_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_SET_SUPS_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_SET_SUPS_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid call control data in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_SUPS_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.set_sups_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.set_sups_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill Service Status info if it is present */
  if(data_ptr->data.set_sups_resp.error == QMI_ERR_NONE && 
     data_ptr->data.set_sups_resp.is_ss_status_valid == TRUE)
  {
     if(FALSE == qmi_svc_put_param_tlv(&response,
                                       VOICEI_SET_SUPS_SERVICE_STATUS,
                                        sizeof(data_ptr->data.set_sups_resp.service_status),
                                        (void *) &data_ptr->data.set_sups_resp.service_status))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
  }
  
  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.set_sups_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.set_sups_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.set_sups_resp.alpha_id.alpha_len) + \
                      data_ptr->data.set_sups_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_SET_SUPS_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.set_sups_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  
  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SET SUPS SVC operation");
  }
}/* qmi_voicei_sups_set_sups_service_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_DTMF_IND()

  DESCRIPTION
    
    
  PARAMETERS
    sp      : service provided state pointer (user data)
    clid    : client ID
    sdu_in  : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_dtmf_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;

  struct
  {
    struct
    {
      uint8    call_id;
      uint8    dtmf_event;
      uint8    digit_cnt;
    } fixed;
    uint8    digit_buffer[QMI_VOICE_CM_IF_MAX_INBAND_DIALED_DIGITS];
  } dtmf_ind;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  memset(&dtmf_ind,0,sizeof(dtmf_ind));
  dtmf_ind.fixed.call_id    = data_ptr->data.dtmf_ind.call_id;
  dtmf_ind.fixed.dtmf_event = (uint8)data_ptr->data.dtmf_ind.dtmf_event;
  dtmf_ind.fixed.digit_cnt  = data_ptr->data.dtmf_ind.digit_cnt;
  /* QMI_Security check*/  
  if(dtmf_ind.fixed.digit_cnt > QMI_VOICE_CM_IF_MAX_INBAND_DIALED_DIGITS)
  {
    dtmf_ind.fixed.digit_cnt = QMI_VOICE_CM_IF_MAX_INBAND_DIALED_DIGITS;
  }
  memscpy(dtmf_ind.digit_buffer, sizeof(dtmf_ind.digit_buffer), 
          data_ptr->data.dtmf_ind.digit_buffer, dtmf_ind.fixed.digit_cnt);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_dtmf_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if (data_ptr->data.dtmf_ind.on_length_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                             VOICEI_DTMF_IND_PW,
                                             sizeof(data_ptr->data.dtmf_ind.on_length),
                                             (void*)&data_ptr->data.dtmf_ind.on_length) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if (data_ptr->data.dtmf_ind.off_length_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                             VOICEI_DTMF_IND_DIG_INT,
                                             sizeof(data_ptr->data.dtmf_ind.off_length),
                                             (void*)&data_ptr->data.dtmf_ind.off_length) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        /* Mandatory TLV */    
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                           VOICEI_DTMF_INFORMATION,
                                           sizeof(dtmf_ind.fixed) + dtmf_ind.fixed.digit_cnt,
                                           &dtmf_ind) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_DTMF_IND,
                                 ind))
        {
        QM_MSG_MED("VOICE DTMF Indication could not be sent");
      }
    }
  }
} /* qmi_voicei_dtmf_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_HANDOVER_IND()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE

    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_handover_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_handover_ind_s *ho_ind;
  uint32   handover_state=0;
  uint32   handover_type=0;
  uint16   ho_tlv_len=0;
  uint8    ho_tlv_tag = 0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  ho_ind = (qmi_voice_cm_if_handover_ind_s*) &data_ptr->data.handover_ind;

  handover_state = ho_ind->handover_state;
  handover_type = ho_ind->handover_type;
  ho_tlv_len = sizeof(handover_state);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_handover_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
 
        /*HO Type optional TLV*/
        if(ho_ind->is_ho_type_available)
        {
          ho_tlv_tag = VOICEI_HANDOVER_TYPE;
          if(!((QMI_SVC_PKT_PUSH(&ind, (void*)&handover_type, sizeof(handover_type))) && 
                (QMI_SVC_PKT_PUSH(&ind, (void*)&ho_tlv_len, VOICEI_TLV_LENGTH_SIZE)) && 
                (QMI_SVC_PKT_PUSH(&ind, (void*)&ho_tlv_tag, VOICEI_TLV_TAG_SIZE))
                ))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
 
	    /* Mandatory TLV */    
        ho_tlv_tag = VOICEI_HANDOVER_STATE;
        if(!((QMI_SVC_PKT_PUSH(&ind, (void*)&handover_state, sizeof(handover_state))) && 
              (QMI_SVC_PKT_PUSH(&ind, (void*)&ho_tlv_len, VOICEI_TLV_LENGTH_SIZE)) && 
              (QMI_SVC_PKT_PUSH(&ind, (void*)&ho_tlv_tag, VOICEI_TLV_TAG_SIZE))
            ))
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_HANDOVER_IND,
                                 ind))
        {
        QM_MSG_MED("VOICE Handover Indication could not be sent");
      }
    }
  }
} /* qmi_voicei_handover_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_LOW_FUNDS_IND()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE

    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_aoc_low_funds_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_aoc_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
      ind = NULL;
      if( !qmi_voicei_send_indication(cl_sp,
                                      VOICEI_CMD_VAL_AOC_LOW_FUNDS_IND,
                                      ind))
      {
        QM_MSG_MED("VOICE Low Funds Indication could not be sent");
      }
    }
  }
} /* qmi_voicei_aoc_low_funds_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_EXT_BRST_INTL_IND()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE

    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_ext_brst_intl_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  uint16           tot_len=0;
  qmi_voice_cm_if_burst_international_ind_s   *info;
  uint8 ext_brt_data_tag = VOICEI_EXT_BRST_INTL_DATA;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  info = (qmi_voice_cm_if_burst_international_ind_s *)&data_ptr->data.ext_brst_intl_ind;

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_brst_intl_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
 
        /* Mandatory TLV */
        
        //Push each item as per the order in IDL
        if(!((QMI_SVC_PKT_PUSH(&ind, (void*)&info->unit, sizeof(info->unit)) )&&
            (QMI_SVC_PKT_PUSH(&ind, (void*)&info->sub_unit, sizeof(info->sub_unit))) &&
            (QMI_SVC_PKT_PUSH(&ind, (void*)&info->chg_ind, sizeof(info->chg_ind))) &&
            (QMI_SVC_PKT_PUSH(&ind, (void*)&info->db_subtype, sizeof(info->db_subtype))) &&
            (QMI_SVC_PKT_PUSH(&ind, (void*)&info->mcc, sizeof(info->mcc)))
            ))
        {
          dsm_free_packet(&ind);
          continue;
        }
 
        //Calcuate the total size  of the indication
        tot_len = sizeof(info->unit) +
                  sizeof(info->sub_unit) +
                  sizeof(info->chg_ind) +
                  sizeof(info->db_subtype) +
                  sizeof(info->mcc);

        //Push length and tag data
        if(!(QMI_SVC_PKT_PUSH(&ind, &tot_len, VOICEI_TLV_LENGTH_SIZE)&&
             QMI_SVC_PKT_PUSH(&ind, &ext_brt_data_tag, VOICEI_TLV_TAG_SIZE)
             ) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_EXT_BRST_INTL_IND,
                                      ind))
        {
          QM_MSG_MED("VOICE Extended Burst International could not be sent");
        }
    }
  }
} /* qmi_voicei_ext_brst_intl_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_CONFERENCE_PARTICIPANTS_INFO_IND()

  DESCRIPTION

  PARAMETERS

  RETURN VALUE

  DEPENDENCIES
None

  SIDE EFFECTS
None
===========================================================================*/
static void qmi_voicei_conference_participants_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            i=0,j=0;
  dsm_item_type *                ind;
  boolean                        is_client_registered = FALSE;
  qmi_voice_cm_if_parsed_conf_info_ind_s  *conf_info;
  qmi_voice_parsed_conf_info_participant  *conf_part;
  uint16                          total_len=0;

  uint8                          ind_tag = VOICEI_CONFERENCE_PARTICIPANTS_INFO;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  conf_info = (qmi_voice_cm_if_parsed_conf_info_ind_s*) &data_ptr->data.parsed_conf_info;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_conf_participants_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {

      ind = NULL;
      is_client_registered = TRUE;
      total_len=0;

      /* Mandatory CC result TLV */    

      QM_MSG_MED("sending conference participants data");

      for(i=0;i<conf_info->num_participants;i++)
      {
        conf_part = &conf_info->conf_part[i];        
         
        total_len =    total_len +\
                       conf_part->disc_info_len  +\
                       sizeof(conf_part->disc_info_len)  +\
                       sizeof(conf_part->disconnection_method)  +\
                       sizeof(conf_part->video_attributes)  +\
                       sizeof(conf_part->audio_attributes)  +\
                       sizeof(conf_part->status)  +\
                       2*conf_part->usr_uri_len  +\
                       sizeof(conf_part->usr_uri_len);

        if(!(QMI_SVC_PKT_PUSH(&ind, (void*)conf_part->disc_info,conf_part->disc_info_len) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->disc_info_len,sizeof(conf_part->disc_info_len)) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->disconnection_method,sizeof(conf_part->disconnection_method)) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->video_attributes,sizeof(conf_part->video_attributes)) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->audio_attributes,sizeof(conf_part->audio_attributes)) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->status,sizeof(conf_part->status)) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)conf_part->usr_uri,2*conf_part->usr_uri_len) && 
             QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part->usr_uri_len,sizeof(conf_part->usr_uri_len)))
           )
        {
          dsm_free_packet(&ind);
          continue;
        }
      }

      total_len = total_len+sizeof(conf_info->update_type)+sizeof(conf_info->num_participants);
      if(!(   QMI_SVC_PKT_PUSH(&ind, (void*)&conf_info->num_participants,sizeof(conf_info->num_participants)) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&conf_info->update_type,sizeof(conf_info->update_type)) && 
              QMI_SVC_PKT_PUSH(&ind, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&ind_tag, VOICEI_TLV_TAG_SIZE)
           )
        )
      {
        dsm_free_packet(&ind);
        continue;
      }
  
      if( !qmi_voicei_send_indication(cl_sp,
                                      VOICEI_CMD_VAL_CONFERENCE_PARTICIPANTS_INFO_IND,
                                      ind))
      {
        QM_MSG_MED("VOICE Conference participants info Indication could not be sent");
      }
    }
  }

  if(!is_client_registered)
  {
    QM_MSG_MED("No Client registered for VOICE conference participants info Indication");
  }
} /* qmi_voicei_conference_participants_info_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_MT_PAGE_MISS_IND()

  DESCRIPTION
	
	
  PARAMETERS

  RETURN VALUE

	
  DEPENDENCIES
	None

  SIDE EFFECTS
	None
===========================================================================*/
static void qmi_voicei_mt_page_miss_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *		 voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int							 j;
  dsm_item_type *				 ind;
  uint16              end_reason;
  boolean             is_client_registered = FALSE;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  end_reason = data_ptr->data.mt_page_miss_ind.end_reason;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
	cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
	if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
		 (cl_sp->voice_info.reg_mt_page_miss_events) &&
		 (!cl_sp->asubs_info.is_bind_set_by_client ||
		 (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
	{

	  ind = NULL;
          is_client_registered = TRUE;

          /* Mandatory TLV */    
	  if(FALSE == qmi_svc_put_param_tlv(&ind,
	                        	     VOICEI_PAGE_MISS_REASON,
					     sizeof(end_reason),
					     &end_reason) )
	  {
	    dsm_free_packet(&ind);
		continue;
	  }

	  if( !qmi_voicei_send_indication(cl_sp,
					  VOICEI_CMD_VAL_MT_PAGE_MISS_IND,
					  ind))
	  {
	    QM_MSG_MED("VOICE MT PAGE MISS Indication could not be sent");
	  }
	}
  }

  if(!is_client_registered)
  {
    QM_MSG_MED("No Client registered for VOICE MT PAGE MISS Indication");
  }
} /* qmi_voicei_mt_page_miss_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_CALL_CONTROL_RESULT_INFO_IND()

  DESCRIPTION

  PARAMETERS

  RETURN VALUE

  DEPENDENCIES
None

  SIDE EFFECTS
None
===========================================================================*/
static void qmi_voicei_call_control_result_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  uint32                         cc_result;
  boolean                        is_client_registered = FALSE;
  uint16                         total_alpha_len=0;
  uint8                          tag_gsm8 = VOICEI_CC_RESULT_INFO_ALPHA_GSM8;
  uint8                          tag_utf16 = VOICEI_CC_RESULT_INFO_ALPHA_UTF16;
  uint32                         alpha_presence=0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  cc_result = (uint32) data_ptr->data.cc_result_info_ind.cc_result;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_cc_result_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {

      ind = NULL;
      is_client_registered = TRUE;

      /* Mandatory CC result TLV */    
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                        VOICEI_CC_RESULT_INFO_RESULT,
                                        sizeof(cc_result),
                                        &cc_result) )
      {
        dsm_free_packet(&ind);
        continue;
      }

      /* Mandatory presence TLV */    
      alpha_presence = data_ptr->data.cc_result_info_ind.alpha_presence;
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                        VOICEI_CC_RESULT_INFO_ALPHA_PRESENCE,
                                        sizeof(alpha_presence),
                                        &alpha_presence) )
      {
        dsm_free_packet(&ind);
        continue;
      }

      /* Optional Alpha TLV */    
      QM_MSG_MED("sending alpha data");
    
      if(data_ptr->data.cc_result_info_ind.is_alpha_gsm8_available)
      {
        total_alpha_len = sizeof(data_ptr->data.cc_result_info_ind.alpha_gsm8_len) + \
                          data_ptr->data.cc_result_info_ind.alpha_gsm8_len;
        if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.cc_result_info_ind.alpha_gsm8,data_ptr->data.cc_result_info_ind.alpha_gsm8_len) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.cc_result_info_ind.alpha_gsm8_len, sizeof(data_ptr->data.cc_result_info_ind.alpha_gsm8_len)) &&
              QMI_SVC_PKT_PUSH(&ind, (void *)&total_alpha_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&ind, (void *)&tag_gsm8, VOICEI_TLV_TAG_SIZE)) )
        {
          dsm_free_packet(&ind);
          continue;
        }
      }

      if(data_ptr->data.cc_result_info_ind.is_alpha_utf16_available)
      {
        total_alpha_len = sizeof(data_ptr->data.cc_result_info_ind.alpha_utf16_len) + \
                          2*(data_ptr->data.cc_result_info_ind.alpha_utf16_len);
        if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.cc_result_info_ind.alpha_utf16,2*(data_ptr->data.cc_result_info_ind.alpha_utf16_len)) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.cc_result_info_ind.alpha_utf16_len, sizeof(data_ptr->data.cc_result_info_ind.alpha_utf16_len)) &&
              QMI_SVC_PKT_PUSH(&ind, (void *)&total_alpha_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&ind, (void *)&tag_utf16, VOICEI_TLV_TAG_SIZE)) )
        {
          dsm_free_packet(&ind);
          continue;
        }
      }

      if( !qmi_voicei_send_indication(cl_sp,
                                      VOICEI_CMD_VAL_CC_RESULT_INFO_IND,
                                      ind))
      {
        QM_MSG_MED("VOICE CC RESULT info Indication could not be sent");
      }
    }
  }

  if(!is_client_registered)
  {
    QM_MSG_MED("No Client registered for VOICE CC RESULT INFO Indication");
  }
} /* qmi_voicei_call_control_result_info_ind() */



/*===========================================================================
  FUNCTION QMI_VOICEI_ALL_CALL_STATUS_IND()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_all_call_status_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *               voice_sp;
  qmi_voicei_client_state_type          * cl_sp;
  int                                   j;
  dsm_item_type *                       ind;
  qmi_voice_cm_if_all_call_info_s       *all_call_info;
  uint8     i, num_of_instances=0;
  int       tot_len=0, cnap_len, alpha_len=0,rp_num_len, tag,con_len=0,diags_len=0;
  int       called_len=0,redirect_len=0;
  boolean   is_ind_sent = FALSE;
  enum qm_subs_e subs = QM_SUBS_PRI;
  uint8     is_incoming_call_present = FALSE;
  uint8     conn_num_pi=0;
  uint8     is_adnl_info_present= FALSE;
  struct
  {
    uint8      call_id;
    uint8      call_state;
    uint8      call_type;
    uint8      direction;
    uint8      mode;
    uint8      mpty;
    uint8      als;
  }basic_info;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  all_call_info = (qmi_voice_cm_if_all_call_info_s*)&data_ptr->data.all_call_status_ind.info;
  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_call_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;

        /* End Reason Text Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if( (info->is_end_reason_text_available) && 
              (info->end_reason_text.end_reason_text_length < QMI_VOICE_CM_IF_MAX_END_REASON_TEXT_LEN) && 
              (info->end_reason_text.end_reason_text_length != 0) )
          {
            /* end_reason_text_length is the number of UTF16 characters */
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->end_reason_text.end_reason_text_buf, 2*info->end_reason_text.end_reason_text_length) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&info->end_reason_text.end_reason_text_length, sizeof(info->end_reason_text.end_reason_text_length)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->end_reason_text.end_reason_text_length) + 2*info->end_reason_text.end_reason_text_length + sizeof(info->call_id);
            QM_MSG_MED_1("end_reason_text added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_END_REASON_TEXT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }

        num_of_instances = 0;
        tot_len = 0;
        
        /* Remote Party number Ext2 Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
          
          if(info->is_num_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->num.buf, info->num.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.len, sizeof(info->num.len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.num_plan, sizeof(info->num.num_plan)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.num_type, sizeof(info->num.num_type)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.si, sizeof(info->num.si)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.pi, sizeof(info->num.pi)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += info->num.len + sizeof(info->num.len) + sizeof(info->num.num_plan) +\
                       sizeof(info->num.num_type) + sizeof(info->num.si) + sizeof(info->num.pi) + sizeof(info->call_id);
            QMI_MSG_MED("Remote Pty Number Ext2 = %s for call id %d",(const char *)info->num.buf, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_RP_NUM_EXT2;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }


        /* Call attribute status info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_call_attrib_status_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_attrib_status, sizeof(info->call_attrib_status)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->call_attrib_status) + sizeof(info->call_id);
            QM_MSG_MED_1("Call attribute status added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_CALL_ATTRIB_STATUS;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  


        /* Is Additional Call info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_additional_call_info_available)
          {
            is_adnl_info_present = (uint8) info->is_additional_call_info_available;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void *)&info->num_indications, sizeof(info->num_indications)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&is_adnl_info_present, sizeof(is_adnl_info_present)) && 
                  QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(is_adnl_info_present) + sizeof(info->num_indications) + sizeof(info->call_id);
            QM_MSG_MED_1("Additional Info Flag added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_IS_ADDITIONAL_CALL_INFO_PRESENT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  


        /* Media ID Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];

          if(info->is_media_id_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->media_id, sizeof(info->media_id)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->media_id) + sizeof(info->call_id);
            QM_MSG_MED_2("Media ID = %d for call id %d", info->media_id, info->call_id);
          }
        }

        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_MEDIA_ID;
          /* Put the num_of_instances, total length, Tag id */
          if( !(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
          }
        }

                /* Orig fail reason Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_orig_fail_reason_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->orig_fail_reason, sizeof(info->orig_fail_reason)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id)) ))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->orig_fail_reason) + sizeof(info->call_id);
            QM_MSG_MED_1("Orig fail reason populated for call id %d", info->call_id);
          }
        }
        
        if(num_of_instances > 0)
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_ORIG_FAIL_REASON;
          /* Put the num_of_instances, total length, Tag id */
          if( !(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
              QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
              QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
          }
        }


        /* Local call capabilities info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_local_cap_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->local_cap.video_restrict_cause, sizeof(info->local_cap.video_restrict_cause)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->local_cap.video_attrib, sizeof(info->local_cap.video_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->local_cap.audio_restrict_cause, sizeof(info->local_cap.audio_restrict_cause)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->local_cap.audio_attrib, sizeof(info->local_cap.audio_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->local_cap.video_restrict_cause)  + sizeof(info->local_cap.video_attrib) +\
                       sizeof(info->local_cap.audio_restrict_cause)  + sizeof(info->local_cap.audio_attrib) + sizeof(info->call_id);
            QM_MSG_MED_1("Local call capabilities added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_LOCAL_CAP;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  


        /* Peer call capabilities info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_peer_cap_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->peer_cap.video_restrict_cause, sizeof(info->peer_cap.video_restrict_cause)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->peer_cap.video_attrib, sizeof(info->peer_cap.video_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->peer_cap.audio_restrict_cause, sizeof(info->peer_cap.audio_restrict_cause)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->peer_cap.audio_attrib, sizeof(info->peer_cap.audio_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->peer_cap.video_restrict_cause)  + sizeof(info->peer_cap.video_attrib) +\
                       sizeof(info->peer_cap.audio_restrict_cause)  + sizeof(info->peer_cap.audio_attrib) + sizeof(info->call_id);
            QM_MSG_MED_1("Peer call capabilities added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_PEER_CAP;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  


/* Child number info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_child_num_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->child_num.buf, info->child_num.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->child_num.len, sizeof(info->child_num.len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += info->child_num.len  + sizeof(info->child_num.len) + sizeof(info->call_id);
            QM_MSG_MED_1("Child number added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_CHILD_NUM;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  

        /* IP caller name Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_ip_caller_name_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->ip_caller_name.name, 2*(info->ip_caller_name.name_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->ip_caller_name.name_len, sizeof(info->ip_caller_name.name_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += 2*(info->ip_caller_name.name_len)  + sizeof(info->ip_caller_name.name_len) + sizeof(info->call_id);
            QM_MSG_MED_1("IP caller name added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_IP_CALLER_NAME;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  

        

        /* Display Text info Optional TLV */
        num_of_instances = 0;
        tot_len = 0;
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_display_text_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->display_text.text, 2*(info->display_text.text_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->display_text.text_len, sizeof(info->display_text.text_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))))
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += 2*(info->display_text.text_len)  + sizeof(info->display_text.text_len) + sizeof(info->call_id);
            QM_MSG_MED_1("Display text added for call id %d", info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_DISPLAY_TEXT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  


        num_of_instances = 0;
        tot_len = 0;

        /* Parent ID info  Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_parent_id_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->is_parent_call_id_cleared, sizeof(info->is_parent_call_id_cleared)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&info->parent_call_id, sizeof(info->parent_call_id)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->is_parent_call_id_cleared) + sizeof(info->parent_call_id) + sizeof(info->call_id);
            QM_MSG_MED_3("Parent_ID = %d, Cleared=%d for call id %d", info->parent_call_id, info->is_parent_call_id_cleared, info->call_id);
          }
        }
          
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_PARENT_CALL_INFO;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }

        num_of_instances = 0;
        tot_len = 0;

        /* Is SRVCC  Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_srvcc_call_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->is_srvcc_call, sizeof(info->is_srvcc_call)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->is_srvcc_call) + sizeof(info->call_id);
          }
          QM_MSG_MED_2("Is SRVCC Call = %d for call id %d", info->is_srvcc_call, info->call_id);
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_IS_SRVCC_CALL;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
    
        num_of_instances = 0;
        tot_len = 0;
/* Alerting Pattern Optional TLV */
        for( i=0; i<all_call_info->num_of_calls; i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
          if(info->is_alerting_pattern_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->alerting_pattern, sizeof(info->alerting_pattern)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->alerting_pattern) + sizeof(info->call_id);
            QM_MSG_MED_2("Alerting pattern = %d for call id %d", info->alerting_pattern, info->call_id);
          }
         }
    
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_ALERTING_PATTERN;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
    
        num_of_instances = 0;
        tot_len = 0;
          

          
        /* Connected number Info Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_conn_num_available)
          {
            con_len = sizeof(info->conn_num_data.pi) + \
                      sizeof(info->conn_num_data.si) + \
                      sizeof(info->conn_num_data.num_type) + \
                      sizeof(info->conn_num_data.num_plan) + \
                      sizeof(info->conn_num_data.len) + \
                      info->conn_num_data.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->conn_num_data, con_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += con_len + sizeof(info->call_id);
            QMI_MSG_MED("Connected num info pi= %d,si=%d,num_len=%d for call id %d",info->conn_num_data.pi,info->conn_num_data.si,info->conn_num_data.len, info->call_id );
          }
        }
            
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_CONNECTED_NUM;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
            
        num_of_instances = 0;
        tot_len = 0;
          
        /* Called party number Info Optional TLV */
        for( i=0; i<all_call_info->num_of_calls; i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_called_party_num_available)
          {
            called_len = sizeof(info->called_party_num_data.pi) + \
                         sizeof(info->called_party_num_data.si) + \
                         sizeof(info->called_party_num_data.num_type) + \
                         sizeof(info->called_party_num_data.num_plan) + \
                         sizeof(info->called_party_num_data.len) + \
                         info->called_party_num_data.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->called_party_num_data, called_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += called_len + sizeof(info->call_id);
            QMI_MSG_MED("Called party num info pi= %d,si=%d,num_len=%d for call id %d",info->called_party_num_data.pi,info->called_party_num_data.si,info->called_party_num_data.len, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_CALLED_PARTY_NUM;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
    
        num_of_instances = 0;
        tot_len = 0;
          
        /* Redirecting party number Info Optional TLV */
        for( i=0; i<all_call_info->num_of_calls; i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
    
          if(info->is_redirecting_party_num_available)
          {
            redirect_len = sizeof(info->redirecting_party_num_data.pi) + \
                           sizeof(info->redirecting_party_num_data.si) + \
                           sizeof(info->redirecting_party_num_data.num_type) + \
                           sizeof(info->redirecting_party_num_data.num_plan) + \
                           sizeof(info->redirecting_party_num_data.len) + \
                           info->redirecting_party_num_data.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->redirecting_party_num_data, redirect_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += redirect_len + sizeof(info->call_id);
            QMI_MSG_MED("Redirect party num info pi= %d,si=%d,num_len=%d for call id %d",info->redirecting_party_num_data.pi,
                         info->redirecting_party_num_data.si,info->redirecting_party_num_data.len, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_REDIRECTING_PARTY_NUM;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }    
    
        num_of_instances = 0;
        tot_len = 0;
      
        /* Call End Diagnostics Info Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
          
          if(info->is_diagnostics_info_available)
          {
            diags_len = sizeof(info->diagnostic_data.diagnostic_length) + \
                        info->diagnostic_data.diagnostic_length ;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->diagnostic_data, diags_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += diags_len + sizeof(info->call_id);
            QM_MSG_MED_2("Diagnostic info len=%d for call id %d", info->diagnostic_data.diagnostic_length, info->call_id);
          }
        }
            
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_DIAGNOSTICS;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
    
        num_of_instances = 0;
        tot_len = 0;
    
        /* VS Variant info Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_vs_call_variant_valid)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->vs_call_variant, sizeof(info->vs_call_variant)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->vs_call_variant) + sizeof(info->call_id);
            QM_MSG_MED_2("Vs variant = %d for call id %d", info->vs_call_variant, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_VS_VARIANT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }


        num_of_instances = 0;
        tot_len = 0;
    
        /* Video Call Attibutes Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_video_attrib_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->video_attrib, sizeof(info->video_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->video_attrib) + sizeof(info->call_id);
            QM_MSG_MED_2("Video Attrib = %d for call id %d", info->video_attrib, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_VIDEO_ATTRIB;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }         

        num_of_instances = 0;
        tot_len = 0;
    
        /* Audio Call Attibutes Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_audio_attrib_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->audio_attrib, sizeof(info->audio_attrib)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->audio_attrib) + sizeof(info->call_id);
            QM_MSG_MED_2("Audio Attrib = %d for call id %d", info->audio_attrib, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_AUDIO_ATTRIB;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }  

        num_of_instances = 0;
        tot_len = 0;
            /* Alpha Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_alpha_available)
          {
            alpha_len = sizeof(info->alpha_info.alpha_dcs) + \
                             sizeof(info->alpha_info.alpha_len) + \
                             info->alpha_info.alpha_len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->alpha_info, alpha_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += alpha_len + sizeof(info->call_id);
            QM_MSG_MED_3("Alpha coding scheme = %d name length =%d for call id %d", info->alpha_info.alpha_dcs, info->alpha_info.alpha_len, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_ALPHA;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        
        num_of_instances = 0;
        tot_len = 0;
    
        /* Call End Reason Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_end_reason_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->end_reason, sizeof(info->end_reason)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->end_reason) + sizeof(info->call_id);
            QM_MSG_MED_2("End reason = %d for call id %d", info->end_reason, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_END_REASON;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        
        num_of_instances = 0;
        tot_len = 0;
        
        /* Service Opt Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_srv_opt_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->srv_opt, sizeof(info->srv_opt)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->srv_opt) + sizeof(info->call_id);
            QM_MSG_MED_2("Service option = %d for call id %d", info->srv_opt, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_SO;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        num_of_instances = 0;
        tot_len = 0;
        
        /* Alerting Type Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_alerting_type_available)
          {
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->alerting_type, sizeof(info->alerting_type)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += sizeof(info->alerting_type) + sizeof(info->call_id);
            QM_MSG_MED_2("Alerting type = %d for call id %d", info->alerting_type, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_ALERTING;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        num_of_instances = 0;
        tot_len = 0;
        
        /* Cnap Info Optional TLV */
        for( i=0;(i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_cnap_info_available)
          {
            cnap_len = sizeof(info->cnap_info.name_presentation) + \
                       sizeof(info->cnap_info.coding_scheme) + \
                       sizeof(info->cnap_info.name_len) + \
                       info->cnap_info.name_len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->cnap_info, cnap_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += cnap_len + sizeof(info->call_id);
            QM_MSG_MED_3("Cnap coding scheme = %d name length =%d for call id %d", info->cnap_info.coding_scheme, info->cnap_info.name_len, info->call_id);
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_RP_NAME;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        num_of_instances = 0;
        tot_len = 0;
        
        /* Remote Party Number Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_num_available && (info->num.len<=CM_MAX_CALLED_INFO_CHARS))
          {
            rp_num_len = sizeof(info->num.pi)+ sizeof(info->num.len) + info->num.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.buf, info->num.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.len, sizeof(info->num.len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.pi, sizeof(info->num.pi)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += rp_num_len + sizeof(info->call_id);
            QMI_MSG_MED("Rp Number = %s for call id %d",(const char *)info->num.buf, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_RP_NUM;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        num_of_instances = 0;
        tot_len = 0;
        
        /* SIP URI Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_num_available && (info->num.len>CM_MAX_CALLED_INFO_CHARS))
          {
            rp_num_len = sizeof(info->num.len) + info->num.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->num.buf, info->num.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.len, sizeof(uint8)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += rp_num_len + sizeof(info->call_id);
            QMI_MSG_MED("SIP URI = %s for call id %d",(const char *)info->num.buf, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_SIP_URI;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }
        
        num_of_instances = 0;
        tot_len = 0;      
        
        /* Ext Remote Party number  info (IP) Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_num_available && (info->num.len>CM_MAX_CALLED_INFO_CHARS))
          {            
            rp_num_len = sizeof(info->num.len) + info->num.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->num.buf, info->num.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.len, sizeof(info->num.len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->num.pi, sizeof(info->num.pi)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += rp_num_len + sizeof(info->num.pi) + sizeof(info->call_id) ;
            QMI_MSG_MED("Ext Remote Pty number = %s for call id %d",(const char *)info->num.buf, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_RP_NUM_EXT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }


        num_of_instances = 0;
        tot_len = 0;
        conn_num_pi = 0;
        
        /* Ext Connected Party number  info (IP) Optional TLV */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
        
          if(info->is_conn_num_available && (info->conn_num_data.len>CM_MAX_CALLED_INFO_CHARS))
          {
            conn_num_pi = info->conn_num_data.pi;
            rp_num_len = sizeof(info->conn_num_data.len) + info->conn_num_data.len;
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)info->conn_num_data.num, info->conn_num_data.len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->conn_num_data.len, sizeof(info->conn_num_data.len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->conn_num_data.pi, sizeof(conn_num_pi)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&info->call_id, sizeof(info->call_id))) )
            {
              num_of_instances = 0;
              dsm_free_packet(&ind);
              break;
            }
            num_of_instances++;
            tot_len += rp_num_len + sizeof(conn_num_pi) + sizeof(info->call_id) ;
            QMI_MSG_MED("Ext connected number = %s for call id %d",(const char *)info->conn_num_data.num, info->call_id );
          }
        }
        
        if( num_of_instances > 0 )
        {
          tot_len += sizeof(num_of_instances);
          tag = VOICEI_ALL_CALL_IND_CONNECTED_NUM_EXT;
          /* Put the num_of_instances, total length, Tag id */
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&num_of_instances, sizeof(num_of_instances)) &&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
          }
        }

        num_of_instances = 0;
        tot_len = 0;
        
        /* Fill Mandatory basic call information */
        for( i=0; (i<all_call_info->num_of_calls) && (i<QMI_VOICE_CM_IF_CALL_ID_MAX_PER_SUBS); i++ )
        {
          const qmi_voice_cm_if_call_info_s *const info = &all_call_info->call_info[i];
          /* Fill the basic call info */
          basic_info.call_id = info->call_id;
          basic_info.call_state = info->call_state;
          basic_info.call_type = info->call_type;
          basic_info.direction = info->direction;
          basic_info.mode = info->mode;
          basic_info.mpty = info->is_mpty;
          basic_info.als = info->als;
          if( !QMI_SVC_PKT_PUSH(&ind, &basic_info, sizeof(basic_info)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          tot_len += sizeof(basic_info);
          QM_MSG_MED_1("Call information for call id %d is:", info->call_id);
          QM_MSG_MED_3("Call State =%d, call type =%d, direction %d", info->call_state, info->call_type, info->direction);
          QM_MSG_MED_3("Call Mode =%d, is mpty =%d, als =%d", info->mode, info->is_mpty, info->als);

          if(basic_info.call_state == QMI_VOICE_CM_IF_CALL_STATE_INCOMING)
          {
            is_incoming_call_present = TRUE;
          }
        }
        
        tot_len += sizeof(all_call_info->num_of_calls);
        tag = QMI_TYPE_REQUIRED_PARAMETERS;
    
        /* Put the num_of_calls, total length, Tag id */
        if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&all_call_info->num_of_calls, 
                                                    sizeof(all_call_info->num_of_calls)) &&
             QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
             QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
             ) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      /* This is a broadcast indication. Send this to all the clients one at
       a time just to avoid dealing with instances.*/
      if( !qmi_voicei_send_indication(cl_sp,
                            VOICEI_CMD_VAL_ALL_CALL_STATUS_IND,
                               ind))
      {
          QM_MSG_MED("VOICE ALL CALL STATUS Indication could not be sent");
      }
      else
      {
          is_ind_sent = TRUE;
          if(cl_sp->asubs_info.is_bind_set_by_client)
          {
            subs = (enum qm_subs_e)cl_sp->asubs_info.as_id;
          }
      }
    }    
  }

  if(is_ind_sent && is_incoming_call_present)
  {
    /* Check if indication retransmission is required and start the timer for retransmit */
    qm_timer_start(QM_TIMER_ID_INCOM_EVT_RESEND, subs);
  }

  return;
} /* qmi_voicei_all_call_status_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_USSD_IND()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_ussd_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  uint16 uss_data_len = 0;
  uint8 tag = VOICEI_USSD_IND_DATA_UTF16;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if( (cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
        (cl_sp->voice_info.reg_ussd_notification_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if( data_ptr->data.ussd_ind.ussd_info.uss_len > 0)
        {
          uss_data_len = sizeof(data_ptr->data.ussd_ind.ussd_info.uss_dcs) + \
                         sizeof(data_ptr->data.ussd_ind.ussd_info.uss_len) + \
                         data_ptr->data.ussd_ind.ussd_info.uss_len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_USSD_IND_DATA,
                                            uss_data_len,
                                            (void*)&data_ptr->data.ussd_ind.ussd_info) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if( data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len > 0)
        {
	  QM_MSG_MED("sending UCS2 data");
          uss_data_len = sizeof(data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len) + \
                        (data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len)*2;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16, (data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len)*2) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len, sizeof(data_ptr->data.ussd_ind.ussd_info_utf16.ussd_data_utf16_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&uss_data_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )          
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                          VOICEI_USSD_NOTIFY_TYPE,
                                          sizeof(data_ptr->data.ussd_ind.user_action),
                                          (void*)&data_ptr->data.ussd_ind.user_action) )
        {
          dsm_free_packet(&ind);
          continue;
        }
      
      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_SUPS_USSD_IND,
                                      ind))
        {
        QM_MSG_MED("VOICE SUPS USSD Indication could not be sent");
        }
      }
    }    
  return;
} /* qmi_voicei_sups_ussd_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_USSD_RELEASE_IND()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_ussd_release_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  voice_sp = &qmi_voice_state;
  
  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
      {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if( (cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
          (cl_sp->voice_info.reg_ussd_notification_events) &&     
           (!cl_sp->asubs_info.is_bind_set_by_client ||
            (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
        {
      if( !qmi_voicei_send_indication(cl_sp,
                            VOICEI_CMD_VAL_SUPS_USSD_RELEASE_IND,
                                   NULL))
          {
        QM_MSG_MED("VOICE SUPS USSD RELEASE Indication could not be sent");
        }
      }
    }
    return;
} /* qmi_voicei_sups_ussd_release_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_UUS_IND()
  
  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_uus_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  uint16 uus_data_len = 0,tot_len=0,tag=VOICEI_UUS_IND_DATA;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL)&& (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_uus_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if( data_ptr->data.uus_ind.uus_info.uus_data_len > 0)
        {
          uus_data_len = sizeof(data_ptr->data.uus_ind.uus_info.uus_type) + \
                                 sizeof(data_ptr->data.uus_ind.uus_info.uus_dcs) + \
                                 sizeof(data_ptr->data.uus_ind.uus_info.uus_data_len) + \
                                 data_ptr->data.uus_ind.uus_info.uus_data_len;
          tot_len = uus_data_len+sizeof(data_ptr->data.uus_ind.call_id);
    
          QM_MSG_MED_1("UUS IND :  call id is:%d", data_ptr->data.uus_ind.call_id);
          QM_MSG_MED_1("UUS IND :  uus_type is:  %d", data_ptr->data.uus_ind.uus_info.uus_type);
          QM_MSG_MED_1("UUS IND :  uus_dcs is :  %d", data_ptr->data.uus_ind.uus_info.uus_dcs);
          QM_MSG_MED_1("UUS IND :  uus_len is :  %d", data_ptr->data.uus_ind.uus_info.uus_data_len);
          QM_MSG_MED_1("UUS IND :  tot_len is :  %d", tot_len);
          
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.uus_ind.uus_info, uus_data_len) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.uus_ind.call_id, sizeof(data_ptr->data.uus_ind.call_id)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_SUPS_UUS_IND,
                                      ind))
        {
        QM_MSG_MED("VOICE UUS Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_uus_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_IND()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  uint16 tot_len=0;
  uint8 tag=VOICEI_UUS_IND_DATA;
  uint16 total_len = 0;
  uint16                  total_barr_list_tlv_size = 0;
  uint8                   barr_num_count = 0;
  uint8  i = 0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  QM_MSG_MED("qmi_voicei_sups_ind");
  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if( (cl_sp != NULL)&& (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_sups_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;

        if(data_ptr->data.sups_ind.is_ntwk_resp)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_SUPS_DATA_SOURCE,
                                            sizeof(data_ptr->data.sups_ind.is_ntwk_resp),
                                            (void *) &data_ptr->data.sups_ind.is_ntwk_resp))
          {
            dsm_free_packet(&ind);
            continue;
          }

          if(data_ptr->data.sups_ind.is_failure_cause_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_FAILURE_CAUSE,
                                            sizeof(data_ptr->data.sups_ind.sups_failure_cause),
                                            (void *) &data_ptr->data.sups_ind.sups_failure_cause)))
          {
            dsm_free_packet(&ind);
            continue;
          }

          if(data_ptr->data.sups_ind.is_clir_status_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_CLIR_STATUS,
                                            sizeof(data_ptr->data.sups_ind.cli_status),
                                            (void *) &data_ptr->data.sups_ind.cli_status)))
          {
            dsm_free_packet(&ind);
            continue;
          }

          if(data_ptr->data.sups_ind.is_clip_status_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_CLIP_STATUS,
                                            sizeof(data_ptr->data.sups_ind.cli_status),
                                            (void *) &data_ptr->data.sups_ind.cli_status)))
          {
            dsm_free_packet(&ind);
            continue;
          }

          if(data_ptr->data.sups_ind.is_colp_status_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_COLP_STATUS,
                                            sizeof(data_ptr->data.sups_ind.cli_status),
                                            (void *) &data_ptr->data.sups_ind.cli_status)))
          {
            dsm_free_packet(&ind);
            continue;
          }

	  if(data_ptr->data.sups_ind.is_colr_status_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_COLR_STATUS,
                                            sizeof(data_ptr->data.sups_ind.cli_status),
                                            (void *) &data_ptr->data.sups_ind.cli_status)))
          {
            dsm_free_packet(&ind);
            continue;
          }

	  if(data_ptr->data.sups_ind.is_cnap_status_valid && 
            (FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SUPS_IND_CNAP_STATUS,
                                            sizeof(data_ptr->data.sups_ind.cli_status),
                                            (void *) &data_ptr->data.sups_ind.cli_status)))
          {
            dsm_free_packet(&ind);
            continue;
          }

		  
          if(data_ptr->data.sups_ind.is_call_fwd_info_from_ntwk_valid)
          {
            const qmi_voice_cm_if_call_fwd_data_ntwk_s *fwd_rsp = &data_ptr->data.sups_ind.cf_data;
            int idx = 0;
            uint8 call_fwd_rsp_tag = VOICEI_SUPS_IND_CF_NTWK_DATA;
            boolean success = TRUE;

            /* Get the highest index */
            idx = fwd_rsp->num_cfw_instances - 1;
            total_len = 0;
            QMI_MSG_HIGH("CFW: Instances :%d,idx=%d  \n", fwd_rsp->num_cfw_instances,idx);
            /* Loop through the service class instances and fill the data */
            while( (idx >= 0) && (idx<QMI_VOICE_CM_IF_MAX_SERVICE_CLASS_INSTANCES))
            {
              /* Put No reply timer */
              if(!QMI_SVC_PKT_PUSH(&ind, (void*)&fwd_rsp->sc_instances[idx].no_reply_timer, 
                                                               sizeof(fwd_rsp->sc_instances[idx].no_reply_timer)))
              {
                dsm_free_packet(&ind);
                success = FALSE;
                break;
              }
              total_len += sizeof(uint8);
              /* Put fwd number if available */
              if(fwd_rsp->sc_instances[idx].num_len > 0)
              {
                if(!QMI_SVC_PKT_PUSH(&ind, (void*)fwd_rsp->sc_instances[idx].num_buf, fwd_rsp->sc_instances[idx].num_len))
                {
                  dsm_free_packet(&ind);\
                  success = FALSE;
                  break;
                }
                total_len += fwd_rsp->sc_instances[idx].num_len;
              }
              /* Put num_len, service_class, service_status */
              if(!(QMI_SVC_PKT_PUSH(&ind, (void*)&fwd_rsp->sc_instances[idx].num_len, 
                                                   sizeof(fwd_rsp->sc_instances[idx].num_len)) &&
                   QMI_SVC_PKT_PUSH(&ind, (void*)&fwd_rsp->sc_instances[idx].service_class, 
                                                   sizeof(fwd_rsp->sc_instances[idx].service_class))&&
                   QMI_SVC_PKT_PUSH(&ind, (void*)&fwd_rsp->sc_instances[idx].service_status,
                                                   sizeof(fwd_rsp->sc_instances[idx].service_status))
                   ) )
              {
                dsm_free_packet(&ind);
                success = FALSE;
                break;
              }
              total_len += 3 * sizeof(uint8);
              /* Decrement the index */
              idx--;
            } /* while */
            
            if( success )
            {
              total_len += sizeof(fwd_rsp->num_cfw_instances);
              QMI_MSG_HIGH("CFW: Instances :%d,total_len after=%d  \n", fwd_rsp->num_cfw_instances,total_len);
              /* Put the num_instances, total length, Tag id */
              if(!(QMI_SVC_PKT_PUSH(&ind, (void*)&fwd_rsp->num_cfw_instances, sizeof(fwd_rsp->num_cfw_instances)) &&
                   QMI_SVC_PKT_PUSH(&ind, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE)&&
                   QMI_SVC_PKT_PUSH(&ind, (void*)&call_fwd_rsp_tag, VOICEI_TLV_TAG_SIZE)
                   ) )
              {
                dsm_free_packet(&ind);
                continue;
              }
            }
            else
            {
              continue;
            }
          }/* Call fwd data filled */
        }

        if(data_ptr->data.sups_ind.is_new_pwd_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.new_pwd_again) + \
                        sizeof(data_ptr->data.sups_ind.new_pwd);
          tag = VOICEI_SUPS_IND_NEW_PWD;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.sups_ind.new_pwd_again, sizeof(data_ptr->data.sups_ind.new_pwd_again)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.sups_ind.new_pwd, sizeof(data_ptr->data.sups_ind.new_pwd)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_cb_pwd_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.pwd) ;
          tag = VOICEI_SUPS_IND_PWD;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.sups_ind.pwd, sizeof(data_ptr->data.sups_ind.pwd)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_alpha_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.alpha_info.alpha_dcs) + \
                        sizeof(data_ptr->data.sups_ind.alpha_info.alpha_len) + \
                        data_ptr->data.sups_ind.alpha_info.alpha_len;
          tag = VOICEI_SUPS_IND_ALPHA;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.alpha_info, tot_len) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_call_id_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.call_id) ;
          tag = VOICEI_SUPS_IND_CALL_ID;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.call_id, sizeof(data_ptr->data.sups_ind.call_id)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_uss_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.uss_info.uss_dcs) + \
                        sizeof(data_ptr->data.sups_ind.uss_info.uss_len) + \
                        data_ptr->data.sups_ind.uss_info.uss_len;
          tag = VOICEI_SUPS_IND_USS_INFO;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.uss_info, tot_len) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_ussd_info_utf16_available)
        {
          QM_MSG_MED("sending UCS2 data");
          tot_len = sizeof(data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16_len) + \
                        (data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16_len)*2;

          tag = VOICEI_SUPS_IND_USS_INFO_UTF16;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16, (data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16_len)*2) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16_len, sizeof(data_ptr->data.sups_ind.ussd_info_utf16.ussd_data_utf16_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        if(data_ptr->data.sups_ind.is_cfw_nrtimer_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.no_reply_timer) ;
          tag = VOICEI_SUPS_IND_CFW_NR_TIMER;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.no_reply_timer, sizeof(data_ptr->data.sups_ind.no_reply_timer)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_cfw_number_available)
        {
          tot_len = data_ptr->data.sups_ind.num_len;
          tag = VOICEI_SUPS_IND_CFW_NUM;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)data_ptr->data.sups_ind.num_buf, tot_len) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_reason_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.reason) ;
          tag = VOICEI_SUPS_IND_REASON;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.reason, sizeof(data_ptr->data.sups_ind.reason)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if(data_ptr->data.sups_ind.is_service_class_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.service_class) ;
          tag = VOICEI_SUPS_IND_SERVICE_CLASS;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.service_class, sizeof(data_ptr->data.sups_ind.service_class)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
	if(data_ptr->data.sups_ind.is_service_class_ext_available)
        {
          tot_len = sizeof(data_ptr->data.sups_ind.service_class_ext) ;
          tag = VOICEI_SUPS_IND_SERVICE_CLASS_EXT;
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.service_class_ext, sizeof(data_ptr->data.sups_ind.service_class_ext)) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        if(data_ptr->data.sups_ind.barr_num_list_valid)
        {
          tag = VOICEI_SUPS_IND_BARR_LIST;
          barr_num_count = data_ptr->data.sups_ind.barr_num_list_len;
          total_barr_list_tlv_size = 0;
          for(i=0;i<barr_num_count;i++) 
          {
            const qmi_voice_cm_if_icb_ind_s *barred_num = &data_ptr->data.sups_ind.barr_num_list[i];
            if( !(QMI_SVC_PKT_PUSH(&ind, (void*)barred_num->barr_num, barred_num->barr_num_len) &&
                  QMI_SVC_PKT_PUSH(&ind, (void *)&barred_num->barr_num_len, sizeof(barred_num->barr_num_len))) )
            {
              dsm_free_packet(&ind);
              continue;
            }  
            total_barr_list_tlv_size = total_barr_list_tlv_size+ \
                                       sizeof(barred_num->barr_num_len)+ \
                                       barred_num->barr_num_len;
          }
          
          total_barr_list_tlv_size += sizeof(barr_num_count);
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&barr_num_count, sizeof(barr_num_count)) &&
                 QMI_SVC_PKT_PUSH(&ind, (void *)&total_barr_list_tlv_size, VOICEI_TLV_LENGTH_SIZE) &&
                 QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }    
        }
    
    
    /*Mandatory TLV */
        tot_len = sizeof(data_ptr->data.sups_ind.service_type) + \
                      sizeof(data_ptr->data.sups_ind.is_modified_by_call_control);
        tag = VOICEI_SUPS_IND_SERVICE_INFO;
        if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.is_modified_by_call_control, sizeof(data_ptr->data.sups_ind.is_modified_by_call_control)) &&
                 QMI_SVC_PKT_PUSH(&ind, (void*)&data_ptr->data.sups_ind.service_type, sizeof(data_ptr->data.sups_ind.service_type)) &&
                 QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE) &&
                 QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_SUPS_IND,
                                      ind))
        {
        QM_MSG_MED("VOICE SUPS Indication could not be sent");
      }
    }
  }

  return;
} /* qmi_voicei_sups_ussd_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_speech_codec_info_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_speech_codec_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_speech_codec_info_ind_s *codec_info_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  codec_info_ptr = (qmi_voice_cm_if_speech_codec_info_ind_s*)&data_ptr->data;

  QMI_MSG_MED ("Codec info IND: NW mode=%ld",codec_info_ptr->network_mode);
  QMI_MSG_MED ("Codec info IND: codec=%ld,sample rate=%ld", codec_info_ptr->speech_codec,codec_info_ptr->speech_sample_rate);


  voice_sp = &qmi_voice_state;

    for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
      cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
      if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_speech_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if(codec_info_ptr->is_call_id_available)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SPEECH_CODEC_INFO_CALL_ID,
                                         sizeof(codec_info_ptr->call_id),
                                         (void*)&codec_info_ptr->call_id))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        if(codec_info_ptr->is_sample_rate_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SPEECH_CODEC_INFO_SAMPLE_RATE,
                                         sizeof(codec_info_ptr->speech_sample_rate),
                                         (void*)&codec_info_ptr->speech_sample_rate))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        if(codec_info_ptr->is_speech_codec_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SPEECH_CODEC_INFO_CODEC_TYPE,
                                         sizeof(codec_info_ptr->speech_codec),
                                         (void*)&codec_info_ptr->speech_codec))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        if(codec_info_ptr->is_nw_mode_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SPEECH_CODEC_INFO_NW_MODE,
                                         sizeof(codec_info_ptr->network_mode),
                                         (void*)&codec_info_ptr->network_mode))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_SPEECH_CODEC_INFO_IND, ind))
        {
          QM_MSG_MED("VOICE SPEECH CODEC INFO Indication could not be sent");
        }
      }
    }
  return;
} /* qmi_voicei_speech_codec_info_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_tty_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_tty_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_tty_info_ind_s *tty_info_ptr;
  uint8                          tty_mode;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  tty_info_ptr = (qmi_voice_cm_if_tty_info_ind_s*)&data_ptr->data;

  QM_MSG_MED_1("TTY info IND: mode=%d", tty_info_ptr->tty_mode);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_tty_info_events) &&     
       (!cl_sp->asubs_info.is_bind_set_by_client ||
        (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
  
      ind = NULL;
  
      tty_mode = tty_info_ptr->tty_mode;
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                     VOICEI_TTY_MODE,
                                     sizeof(tty_mode),
                                     (void*)&tty_mode))
      {
        dsm_free_packet(&ind);
        continue;
      }
  
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_TTY_IND, ind))
      {
        QM_MSG_MED("VOICE TTY Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_tty_ind() */


/*===========================================================================
  FUNCTION qmi_voicei_audio_rat_change_info_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_audio_rat_change_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_audio_rat_change_info_ind_s *audio_rat_change_info_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  audio_rat_change_info_ptr = (qmi_voice_cm_if_audio_rat_change_info_ind_s*)&data_ptr->data;

  QM_MSG_MED_2("Audio RAT Change Info IND: audio_session_info=%d, rat_info=%d", audio_rat_change_info_ptr->audio_session_info, audio_rat_change_info_ptr->rat_info);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_audio_rat_change_events) &&
       (!cl_sp->asubs_info.is_bind_set_by_client ||
        (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
  
      ind = NULL;
  
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                     VOICEI_AUDIO_RAT_CHANGE_RAT_INFO,
                                     sizeof(audio_rat_change_info_ptr->rat_info),
                                     (void*)&(audio_rat_change_info_ptr->rat_info)))
      {
        dsm_free_packet(&ind);
        continue;
      }
  
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                     VOICEI_AUDIO_RAT_CHANGE_AUDIO_SESSION_INFO,
                                     sizeof(audio_rat_change_info_ptr->audio_session_info),
                                     (void*)&(audio_rat_change_info_ptr->audio_session_info)))
      {
        dsm_free_packet(&ind);
        continue;
      }
  
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_AUDIO_RAT_CHANGE_INFO_IND, ind))
      {
        QM_MSG_MED("VOICE Audio RAT Change Info Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_audio_rat_change_info_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_conference_participant_status_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_conference_participant_status_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_conf_participant_status_ind_s *conf_part_status_info_ptr;

  uint16      op_status_len = 0;
  uint32     operation = 0;
  uint8      op_status_tag = VOICEI_CONF_PART_STATUS_INFO_PART_OP_INFO;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  conf_part_status_info_ptr = (qmi_voice_cm_if_conf_participant_status_ind_s*)&data_ptr->data;

  QM_MSG_MED_2("Participant status IND: call_id=%d, op=%d", conf_part_status_info_ptr->call_id, conf_part_status_info_ptr->operation);
  QM_MSG_MED_2("uri=%s, status_sip_code =%d", conf_part_status_info_ptr->sip_uri, conf_part_status_info_ptr->sip_status);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_conf_participants_events) &&
       (!cl_sp->asubs_info.is_bind_set_by_client ||
        (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
  
      ind = NULL;

      
      operation = conf_part_status_info_ptr->operation;

      //Operation STATUS TLV	  
      op_status_len = sizeof(conf_part_status_info_ptr->sip_status) + \
                   	  	sizeof(operation);
      if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&conf_part_status_info_ptr->sip_status, sizeof(conf_part_status_info_ptr->sip_status)) &&
          	QMI_SVC_PKT_PUSH(&ind, (void*)&operation, sizeof(operation)) &&
            QMI_SVC_PKT_PUSH(&ind, (void *)&op_status_len, VOICEI_TLV_LENGTH_SIZE) &&
            QMI_SVC_PKT_PUSH(&ind, (void *)&op_status_tag, VOICEI_TLV_TAG_SIZE)) )
      {
        dsm_free_packet(&ind);
        continue;
      }
      QM_MSG_MED("CONF PART STATUS: SIP STATUS TLV added");

      //Participant URI
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                        VOICEI_CONF_PART_STATUS_INFO_PART_ID ,
                                        conf_part_status_info_ptr->sip_uri_len,
                                        (void*)conf_part_status_info_ptr->sip_uri))
      {
        dsm_free_packet(&ind);
        continue;
      }
      QM_MSG_MED("CONF PART STATUS: URI added");

  	
      //CALL ID
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                        VOICEI_CONF_PART_STATUS_INFO_CALL_ID ,
                                        sizeof(conf_part_status_info_ptr->call_id),
                                        (void*)&conf_part_status_info_ptr->call_id))
      {
        dsm_free_packet(&ind);
        continue;
      }

      QM_MSG_MED("CONF PART STATUS: Call ID added ");

 
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CONF_PART_STATUS_IND, ind))
      {
        QM_MSG_MED("CONF PART STATUS Info Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_conference_participant_status_ind() */


/*===========================================================================
  FUNCTION qmi_voicei_conf_info_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_conf_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_conference_info_ind_s *conf_info_ptr;  
  uint8      xml_tag = VOICEI_CONFERENCE_CONF_INFO_XML_DATA;
  uint16     conf_xml_len = 0;
  uint16     xml_tlv_len = 0;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  conf_info_ptr = (qmi_voice_cm_if_conference_info_ind_s*)&data_ptr->data;


    voice_sp = &qmi_voice_state;

    for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
      cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
      if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_conference_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;

        //Conference_XML string
        if(conf_info_ptr->length > 0)
        {
          //Copying to conf_xml_len since conf_info_ptr->length is uint32 where as length field is only uint16
          conf_xml_len = (uint16) conf_info_ptr->length;
          xml_tlv_len = conf_xml_len + sizeof(conf_xml_len);
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)conf_info_ptr->conference_xml, conf_xml_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&conf_xml_len, sizeof(conf_xml_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&xml_tlv_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&xml_tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED("CONF INFO Indication XML data TLV added");
        }
    

        //sequence number
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_CONFERENCE_CONF_INFO_SEQUENCE_NUM ,
                                       sizeof(conf_info_ptr->sequence_num),
                                       (void*)&conf_info_ptr->sequence_num))
        {
          dsm_free_packet(&ind);
          continue;
        }
        QM_MSG_MED("CONF INFO Sequence number TLV added");

        //total xml file length
        if(conf_info_ptr->sequence_num  == 0)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_CONFERENCE_CONF_INFO_TOTAL_XML_FILE_LENGTH ,
                                         sizeof(conf_info_ptr->total_xml_file_length),
                                         (void*)&conf_info_ptr->total_xml_file_length))
          {
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED("CONF INFO Total file length TLV added");
        }

        if(conf_info_ptr->is_call_id_available)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_CONFERENCE_CONF_INFO_CALL_ID,
                                         sizeof(conf_info_ptr->call_id),
                                         (void*)&conf_info_ptr->call_id))
          {
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED_1("CONF INFO call id=%d", conf_info_ptr->call_id);
        }

        if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CONF_INFO_IND, ind))
        {          
          QM_MSG_MED("VOICE CONF INFO Indication could not be sent");
        }
      }
    }
    return;
} /* qmi_voicei_conf_info_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_additional_call_info_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_additional_call_info_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_additional_call_info_ind_s *ext_call_info_ptr;  
  uint8      call_info_tag = VOICEI_ADDITIONAL_CALL_INFO_EXT_INFO_DATA;
  uint16     call_info_len = 0;
  uint16     call_info_tlv_len = 0;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ext_call_info_ptr = (qmi_voice_cm_if_additional_call_info_ind_s*)&data_ptr->data;


    voice_sp = &qmi_voice_state;

    for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
      cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
      if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_add_call_info_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;

        //Additiona_call_info string
        if(ext_call_info_ptr->extension_info.call_info_len > 0)
        {
          //Copying to conf_xml_len since conf_info_ptr->length is uint32 where as length field is only uint16
          call_info_len = (uint16) ext_call_info_ptr->extension_info.call_info_len;
          call_info_tlv_len = call_info_len + sizeof(call_info_len) + sizeof(ext_call_info_ptr->extension_info.sequence_num) + sizeof(ext_call_info_ptr->extension_info.total_call_info_length);
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&ext_call_info_ptr->extension_info.total_call_info_length, sizeof(ext_call_info_ptr->extension_info.total_call_info_length)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)ext_call_info_ptr->extension_info.call_info, call_info_len) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&call_info_len, sizeof(call_info_len)) &&
                QMI_SVC_PKT_PUSH(&ind, (void*)&ext_call_info_ptr->extension_info.sequence_num, sizeof(ext_call_info_ptr->extension_info.sequence_num)) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&call_info_tlv_len, VOICEI_TLV_LENGTH_SIZE) &&
                QMI_SVC_PKT_PUSH(&ind, (void *)&call_info_tag, VOICEI_TLV_TAG_SIZE)) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED("Additonal call INFO Indication data TLV added");
        }

        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_ADDITIONAL_CALL_INFO_CALL_ID,
                                       sizeof(ext_call_info_ptr->call_id),
                                       (void*)&ext_call_info_ptr->call_id))
        {
          dsm_free_packet(&ind);
          continue;
        }
        QM_MSG_MED_1("Additional Call INFO call id=%d", ext_call_info_ptr->call_id);


        if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_ADDITIONAL_CALL_INFO_IND, ind))
        {          
          QM_MSG_MED("Additional Call INFO Indication could not be sent");
        }
      }
    }
    return;
} /* qmi_voicei_additional_call_info_ind() */


/*===========================================================================
  FUNCTION qmi_voicei_conf_join_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_conf_join_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j, tag, tot_len;
  dsm_item_type *                ind;
  qmi_voice_cm_if_conference_join_ind_s *join_info_ptr;

  struct {
 
    uint8 uri_name_len;  
    uint16 uri_name[QMI_VOICE_CONF_URI_MAX_LEN];

    uint8 uri_description_len;  
    uint16 uri_description[QMI_VOICE_CONF_DISPLAY_TEXT_MAX_LEN];
  }usr_uri_s;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  join_info_ptr = (qmi_voice_cm_if_conference_join_ind_s*)&data_ptr->data;


  voice_sp = &qmi_voice_state;

    for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
      cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
      if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_conference_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if(join_info_ptr != NULL)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_CONFERENCE_JOIN_CALL_ID,
                                         sizeof(join_info_ptr->call_id),
                                         (void*)&join_info_ptr->call_id))
          {
            dsm_free_packet(&ind);
            continue;
          }

          QM_MSG_MED("Conference JOIN IND Call ID TLV added");

          usr_uri_s.uri_name_len = join_info_ptr->participant_uri.uri_name_len;
          memscpy(usr_uri_s.uri_name,sizeof(usr_uri_s.uri_name),
                  join_info_ptr->participant_uri.uri_name, usr_uri_s.uri_name_len);


          usr_uri_s.uri_description_len = join_info_ptr->participant_uri.uri_description_len;
          memscpy(usr_uri_s.uri_description, sizeof(usr_uri_s.uri_description),
                  join_info_ptr->participant_uri.uri_description, usr_uri_s.uri_description_len);
          
          
          /* Participant Info Mandatory TLV */
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_description, usr_uri_s.uri_description_len) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_description_len, sizeof(usr_uri_s.uri_description_len)) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_name, usr_uri_s.uri_name_len) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_name_len, sizeof(usr_uri_s.uri_name_len))) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          tot_len = usr_uri_s.uri_description_len + sizeof(usr_uri_s.uri_description_len) + usr_uri_s.uri_name_len + sizeof(usr_uri_s.uri_name_len);
          /* Put the num_of_instances, total length, Tag id */
          tag = VOICEI_CONFERENCE_JOIN_PARTICIPANT_INFO;
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
            continue;
          }

          QM_MSG_MED("Conference JOIN IND Participant Info TLV added");
        }

        if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CONF_JOIN_IND, ind))
        {          
          QM_MSG_MED("VOICE CONF USER INFO Indication could not be sent");
        }
      }
    }
  return;
} /* qmi_voicei_conf_join_ind() */


/*===========================================================================
  FUNCTION qmi_voicei_conf_participant_update_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_conf_participant_update_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j, tag, tot_len;
  dsm_item_type *                ind;
  qmi_voice_cm_if_conference_participant_update_ind_s  *participant_info_ptr;

  //TLV
  struct {
 
    uint8 uri_name_len;  
    uint16 uri_name[QMI_VOICE_CONF_URI_MAX_LEN];

    uint8 uri_description_len;  
    uint16 uri_description[QMI_VOICE_CONF_DISPLAY_TEXT_MAX_LEN];
  }usr_uri_s;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  participant_info_ptr = (qmi_voice_cm_if_conference_participant_update_ind_s*)&data_ptr->data;

  memset(&usr_uri_s, 0, sizeof(usr_uri_s));

  voice_sp = &qmi_voice_state;

    for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
      cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
      if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_conference_events) &&     
         (!cl_sp->asubs_info.is_bind_set_by_client ||
          (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if(participant_info_ptr != NULL)
        {
          usr_uri_s.uri_name_len = participant_info_ptr->participant_uri.uri_name_len;
          memscpy(usr_uri_s.uri_name, sizeof(usr_uri_s.uri_name),
                  participant_info_ptr->participant_uri.uri_name, 
                  usr_uri_s.uri_name_len);


          usr_uri_s.uri_description_len = participant_info_ptr->participant_uri.uri_description_len;
          memscpy(usr_uri_s.uri_description, sizeof(usr_uri_s.uri_description),
                  participant_info_ptr->participant_uri.uri_description, 
                  usr_uri_s.uri_description_len);
          
          /* Participant Info Mandatory TLV */
          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_description, usr_uri_s.uri_description_len) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_description_len, sizeof(usr_uri_s.uri_description_len)) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_name, usr_uri_s.uri_name_len) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&usr_uri_s.uri_name_len, sizeof(usr_uri_s.uri_name_len))) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          tot_len = usr_uri_s.uri_description_len + sizeof(usr_uri_s.uri_description_len) + usr_uri_s.uri_name_len + sizeof(usr_uri_s.uri_name_len);
          /* Put the num_of_instances, total length, Tag id */
          tag = VOICEI_CONFERENCE_PARTICIPANT_UPDATE_INFO;
          if(!(QMI_SVC_PKT_PUSH(&ind, (void *)&tot_len, VOICEI_TLV_LENGTH_SIZE)&&
               QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)
               ) )
          {
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED("Participant Update IND Participant Info TLV added");
        }

        if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CONF_PARTICIPANT_UPDATE_IND, ind))
        {          
          QM_MSG_MED("VOICE CONF PARTICIPANT UPDATE INFO Indication could not be sent");
        }
      }
    }
  return;
}/* qmi_voicei_conf_participant_update_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_e911_orig_fail_ind()

  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_e911_orig_fail_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_e911_orig_fail_ind_s *e911_orig_info_ptr;
  struct qmi_voice_e911_orig_fail_ind_s_type orig_ind;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  e911_orig_info_ptr = (qmi_voice_cm_if_e911_orig_fail_ind_s*)&data_ptr->data;

  QM_MSG_MED_2 ("E911 orig info IND:call_id=%d,end_reason=%d",e911_orig_info_ptr->call_id,e911_orig_info_ptr->end_reason);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_e911_orig_fail_events) &&     
       (!cl_sp->asubs_info.is_bind_set_by_client ||
        (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
  
      ind = NULL;
  
      orig_ind.e911_orig_fail_ind.call_id = e911_orig_info_ptr->call_id;
      orig_ind.e911_orig_fail_ind.end_reason = e911_orig_info_ptr->end_reason;

      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                     VOICEI_E911_ORIG_FAIL,
                                     sizeof(orig_ind.e911_orig_fail_ind),
                                     (void*)&orig_ind.e911_orig_fail_ind))
      {
        dsm_free_packet(&ind);
        continue;
      }
  
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_E911_ORIG_FAIL_IND, ind))
      {
        QM_MSG_MED("VOICE TTY Indication could not be sent" );
      }
    }
  }
  return;
} /* qmi_voicei_e911_orig_fail_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_BURST_DTMF()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_burst_dtmf
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;
  struct 
  {
    uint8               call_id;
    uint8               digit_cnt;
    uint8               digit_buffer[QMI_VOICE_CM_IF_MAX_DTMF_BURST_LEN];
  }dtmf_req = {0,0,{0}};
  struct
  {
    uint8   on_len;
    uint8   off_len;
  }on_off_len = {0,0};

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_burst_dtmf");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  memset(&dtmf_req, 0, sizeof(dtmf_req));

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        value = &dtmf_req;
        if(len > (sizeof(dtmf_req.digit_buffer) + sizeof(dtmf_req.call_id) + sizeof(dtmf_req.digit_cnt)))
        {
          QM_MSG_MED_1("Invalid digit_cnt %d", len);
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        got_v_in_required = TRUE;
        break;
      case VOICEI_BURST_DTMF_LEN:
        expected_len = sizeof(on_off_len);
        value = &on_off_len;
        cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.on_off_length_valid = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QMI_MSG_MED("%s, len Recieved=%d","Invalid length in TLV",len);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.call_id = dtmf_req.call_id;
  cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.digit_cnt = dtmf_req.digit_cnt;
  /* QMI_Security check*/
  if(dtmf_req.digit_cnt > QMI_VOICE_CM_IF_MAX_DTMF_BURST_LEN)
  {
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }
  memscpy(cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.digit_buffer, 
          sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.digit_buffer),
          dtmf_req.digit_buffer, dtmf_req.digit_cnt );

  if(cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.on_off_length_valid)
  {
    cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.on_length = on_off_len.on_len;
    cmd_ptr->cmd.cm_if_data_ptr->data.burst_dtmf.off_length = on_off_len.off_len;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_BURST_DTMF;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_burst_dtmf() */

/*===========================================================================
  FUNCTION QMI_VOICEI_START_CONT_DTMF()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_start_cont_dtmf
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_start_cont_dtmf");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.start_cont_dtmf);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.start_cont_dtmf;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_START_CONT_DTMF;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_start_cont_dtmf() */

/*===========================================================================
  FUNCTION QMI_VOICEI_STOP_CONT_DTMF()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_stop_cont_dtmf
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_stop_cont_dtmf");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }
  
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.stop_cont_dtmf);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.stop_cont_dtmf;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_STOP_CONT_DTMF;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_stop_cont_dtmf() */


/*===========================================================================
  FUNCTION QMI_VOICEI_INFO_REC_IND()

  DESCRIPTION
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_info_rec_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *       voice_sp;
  dsm_item_type *               ind;
  uint16 total_size = 0;
  qmi_voicei_client_state_type * cl_sp;
  int j=0;
  struct
  {
    uint8       signal_type;
    uint8       signal_pitch;
    uint8       signal;
  } signal_info;

  struct
  {
    struct
    {
      uint8                 pi;
      uint8                 len;
    } fixed;
    uint8                   buf[QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN];
  } num_info;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  num_info.fixed.pi = data_ptr->data.info_rec_ind.caller_id.pi;
  num_info.fixed.len = data_ptr->data.info_rec_ind.caller_id.len;

  /* QMI_Security check*/  
  if(num_info.fixed.len > QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN)
  {
    num_info.fixed.len = QMI_VOICE_CM_IF_MAX_CALLING_NUM_LEN;
  }

  memscpy(num_info.buf, sizeof(num_info.buf),
          data_ptr->data.info_rec_ind.caller_id.buf, num_info.fixed.len);

  signal_info.signal_type = data_ptr->data.info_rec_ind.signal_info.signal_type;
  signal_info.signal_pitch = data_ptr->data.info_rec_ind.signal_info.alert_pitch;
  signal_info.signal = data_ptr->data.info_rec_ind.signal_info.signal;

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp!=NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_call_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;
    
        if( TRUE == data_ptr->data.info_rec_ind.signal_info.is_signal_info_avail )
        {  
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_SIGNAL_INFO,
                                            sizeof(signal_info),
                                            &signal_info) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if( data_ptr->data.info_rec_ind.caller_id.len > 0 || 
			data_ptr->data.info_rec_ind.is_calling_party_num_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CALLER_ID,
                                            sizeof(num_info.fixed) + num_info.fixed.len,
                                            (void *)&data_ptr->data.info_rec_ind.caller_id) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        if( data_ptr->data.info_rec_ind.display_info.len > 0 )
        {  
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_DISPLAY_INFO,
                                            data_ptr->data.info_rec_ind.display_info.len,
                                            (void *)data_ptr->data.info_rec_ind.display_info.buf) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        if( data_ptr->data.info_rec_ind.ext_display_info.len > 0 )
        {  
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_EXT_DISPLAY_INFO,
                                            data_ptr->data.info_rec_ind.ext_display_info.len,
                                            (void*)data_ptr->data.info_rec_ind.ext_display_info.buf) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        // Caller name information (VOICEI_CALLER_NAME__INFO) TLV
        if( data_ptr->data.info_rec_ind.caller_name.valid )
        {  
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CALLER_NAME_INFO,
                                            data_ptr->data.info_rec_ind.caller_name.len,
                                            (void*)data_ptr->data.info_rec_ind.caller_name.buf) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        // Call waiting indiactor (VOICEI_CALL_WAITING_IND) TLV
        if( data_ptr->data.info_rec_ind.is_callwaiting )
        {  
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CALL_WAITING_IND,
                                            sizeof(data_ptr->data.info_rec_ind.is_callwaiting),
                                            (void*)&data_ptr->data.info_rec_ind.is_callwaiting) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        if( data_ptr->data.info_rec_ind.is_conn_num_valid )
        {  
          total_size = sizeof(data_ptr->data.info_rec_ind.conn_num.pi) +
                       sizeof(data_ptr->data.info_rec_ind.conn_num.si) +
                       sizeof(data_ptr->data.info_rec_ind.conn_num.num_type) +
                       sizeof(data_ptr->data.info_rec_ind.conn_num.num_plan) +
                       sizeof(data_ptr->data.info_rec_ind.conn_num.len) +
                       data_ptr->data.info_rec_ind.conn_num.len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CONN_NUM_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.conn_num) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        
        total_size = 0 ;
        if( data_ptr->data.info_rec_ind.is_calling_party_num_valid )
        {  
          total_size = sizeof(data_ptr->data.info_rec_ind.calling_party_num.pi) +
                       sizeof(data_ptr->data.info_rec_ind.calling_party_num.si) +
                       sizeof(data_ptr->data.info_rec_ind.calling_party_num.num_type) +
                       sizeof(data_ptr->data.info_rec_ind.calling_party_num.num_plan) +
                       sizeof(data_ptr->data.info_rec_ind.calling_party_num.len) +
                       data_ptr->data.info_rec_ind.calling_party_num.len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CALLING_PARTY_NUM_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.calling_party_num) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
    
        total_size = 0 ;
        if( data_ptr->data.info_rec_ind.is_called_party_num_valid )
        {  
          total_size = sizeof(data_ptr->data.info_rec_ind.called_party_num.pi) +
                       sizeof(data_ptr->data.info_rec_ind.called_party_num.si) +
                       sizeof(data_ptr->data.info_rec_ind.called_party_num.num_type) +
                       sizeof(data_ptr->data.info_rec_ind.called_party_num.num_plan) +
                       sizeof(data_ptr->data.info_rec_ind.called_party_num.len) +
                       data_ptr->data.info_rec_ind.called_party_num.len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_CALLED_PARTY_NUM_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.called_party_num) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }    
    
        total_size = 0 ;
        if( data_ptr->data.info_rec_ind.is_redirect_num_valid )
        {  
          total_size = sizeof(data_ptr->data.info_rec_ind.redirect_num.pi) +
                       sizeof(data_ptr->data.info_rec_ind.redirect_num.si) +
                       sizeof(data_ptr->data.info_rec_ind.redirect_num.num_type) +
                       sizeof(data_ptr->data.info_rec_ind.redirect_num.num_plan) +
                       sizeof(data_ptr->data.info_rec_ind.redirect_num.reason) +
                       sizeof(data_ptr->data.info_rec_ind.redirect_num.len) +
                       data_ptr->data.info_rec_ind.redirect_num.len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_REDIRECT_NUM_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.redirect_num) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }   
    
        if( data_ptr->data.info_rec_ind.is_nss_clir_cause_valid)
        {     
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_NSS_CLIR_INFO,
                                            sizeof(data_ptr->data.info_rec_ind.nss_clir_cause),
                                            (void*)&data_ptr->data.info_rec_ind.nss_clir_cause) )
          {
            dsm_free_packet(&ind);
            continue;
          }    
        }
    
        total_size = 0 ;
        
        if( data_ptr->data.info_rec_ind.is_nss_aud_ctrl_valid)
        {     
          total_size = sizeof(data_ptr->data.info_rec_ind.nss_aud_ctrl.up_link) +
                       sizeof(data_ptr->data.info_rec_ind.nss_aud_ctrl.down_link );
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_NSS_AUD_CTRL_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.nss_aud_ctrl) )
          {
            dsm_free_packet(&ind);
            continue;
          }    
        }
    
        if( data_ptr->data.info_rec_ind.is_nss_release_valid)
        {     
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_NSS_RELEASE_INFO,
                                            sizeof(data_ptr->data.info_rec_ind.nss_release),
                                            (void*)&data_ptr->data.info_rec_ind.nss_release) )
          {
            dsm_free_packet(&ind);
            continue;
          }    
        }
    
        total_size = 0 ;   
        if( data_ptr->data.info_rec_ind.is_line_ctrl_valid)
        {     
          total_size = sizeof(data_ptr->data.info_rec_ind.line_ctrl.polarity_included) +
                       sizeof(data_ptr->data.info_rec_ind.line_ctrl.toogle_mode) +
                       sizeof(data_ptr->data.info_rec_ind.line_ctrl.reverse_polarity) +
                       sizeof(data_ptr->data.info_rec_ind.line_ctrl.power_denial_time);
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_LINE_CTRL_INFO,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.line_ctrl) )
          {
            dsm_free_packet(&ind);
            continue;
          }    
        }
    
        /*Extended Display info record TLV*/
        total_size = 0 ;
        if( data_ptr->data.info_rec_ind.ext_display_record_valid)
        {  
          total_size = sizeof(data_ptr->data.info_rec_ind.ext_display_record.display_type) +
                       sizeof(data_ptr->data.info_rec_ind.ext_display_record.ext_display_info_len) +
                       data_ptr->data.info_rec_ind.ext_display_record.ext_display_info_len;
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                            VOICEI_EXT_DISPLAY_INFO_RECORD,
                                            total_size,
                                            (void*)&data_ptr->data.info_rec_ind.ext_display_record) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }        
    
        /*Mandatory TLV*/
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                          VOICEI_INFO_REC_CALL_ID,
                                          sizeof(data_ptr->data.info_rec_ind.call_id),
                                          (void *)&data_ptr->data.info_rec_ind.call_id) )
        {
          dsm_free_packet(&ind);
          continue;
        }
    
      if( !qmi_voicei_send_indication(cl_sp,
                                VOICEI_CMD_VAL_INFO_REC_IND,
                                      ind))
        {
        QM_MSG_MED("VOICE INFO REC Indication could not be sent");
      }
    }
  }

} /* qmi_voicei_info_rec_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_OTASP_STATUS_IND()

  DESCRIPTION
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_otasp_status_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *       voice_sp;
  dsm_item_type *               ind;
  qmi_voicei_client_state_type * cl_sp;
  int j=0;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
    {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp!=NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_call_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {
        ind = NULL;

        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                          VOICEI_OTASP_STATUS,
                                          sizeof(data_ptr->data.otasp_status_ind),
                                          (void*)&data_ptr->data.otasp_status_ind) )
        {
          dsm_free_packet(&ind);
          continue;
        }
      /* This is a broadcast indication. Send this to all the clients one at
      a time just to avoid dealing with instances.*/
      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_OTASP_STATUS,
                                      ind))
        {
        QM_MSG_MED("VOICE OTASP STATUS Indication could not be sent");
      }
    }
  }

  return;
} /* qmi_voicei_otasp_status_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_PRIVACY_IND()

  DESCRIPTION
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_privacy_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_voice_privacy_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {

        ind = NULL;

        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                          VOICEI_PRIVACY_INFO,
                                          sizeof(data_ptr->data.privacy_ind),
                                          (void *)&data_ptr->data.privacy_ind) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_PRIVACY_IND,
                                      ind))
        {
        QM_MSG_MED("VOICE PRIVACY Indication could not be sent");
      }
    }
  }

  return;
} /* qmi_voicei_privacy_ind() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_NOTIFICATION_IND()

  DESCRIPTION
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_notification_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j, ect_info_len;
  dsm_item_type *                ind = NULL;
  qmi_voice_cm_if_sups_notification_ind_s *notify_ptr;
  struct
  {
    uint8 call_id;
    uint8 notify_type;
  }notify_info;
  uint16         ip_forward_hist_info_len = 0;
  int            tag = 0;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  notify_ptr = (qmi_voice_cm_if_sups_notification_ind_s*)&data_ptr->data;
  voice_sp = &qmi_voice_state;

  QM_MSG_MED("qmi_voicei_sups_notification_ind");

  for( j = 0; j < VOICEI_MAX_CLIDS; j++)
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if( (cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
         (cl_sp->voice_info.reg_sups_events) &&
         (!cl_sp->asubs_info.is_bind_set_by_client ||
         (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
      {
        ind = NULL;
        /* Fill the data from back to front */

        if( (notify_ptr->is_ip_forward_hist_info_available) &&
            (notify_ptr->ip_forward_hist_info.hist_info_length < QMI_VOICE_CM_IF_MAX_IP_FORWARD_HIST_INFO_LEN) &&
            (notify_ptr->ip_forward_hist_info.hist_info_length != 0) )
        {
          /* ip_forward_hist_info.hist_info_len is the number of UTF16 characters */
          ip_forward_hist_info_len = sizeof(notify_ptr->ip_forward_hist_info.hist_info_length) + (2*notify_ptr->ip_forward_hist_info.hist_info_length);
          tag = VOICEI_SUPS_NOTIFY_IND_IP_FORWARD_HIST_INFO;

          if( !(QMI_SVC_PKT_PUSH(&ind, (void*)notify_ptr->ip_forward_hist_info.hist_info_buf, 2*notify_ptr->ip_forward_hist_info.hist_info_length) &&
              QMI_SVC_PKT_PUSH(&ind, (void*)&notify_ptr->ip_forward_hist_info.hist_info_length, sizeof(notify_ptr->ip_forward_hist_info.hist_info_length)) && 
              QMI_SVC_PKT_PUSH(&ind, (void *)&ip_forward_hist_info_len, VOICEI_TLV_LENGTH_SIZE) && 
              QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)  ) )
          {          
            dsm_free_packet(&ind);
            continue;
          }
          QM_MSG_MED_1("ip_forward_hist_info: %d characters", notify_ptr->ip_forward_hist_info.hist_info_length);
        }

        if(notify_ptr->is_reason_available)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SUPS_NOTIFY_IND_REASON,
                                         sizeof(notify_ptr->reason),
                                         (void *)&notify_ptr->reason) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }

        if(notify_ptr->is_ect_info_valid)
        {
          ect_info_len = sizeof(notify_ptr->ect_info.ect_call_state) + \
                         sizeof(notify_ptr->ect_info.pi) + \
                         sizeof(notify_ptr->ect_info.ect_num_len) + \
                         notify_ptr->ect_info.ect_num_len;

          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SUPS_NOTIFY_IND_ECT,
                                         ect_info_len,
                                         (void*)&notify_ptr->ect_info))
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        if(notify_ptr->is_cug_idx_valid)
        {
          if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_SUPS_NOTIFY_IND_CUG,
                                         sizeof(notify_ptr->cug_idx),
                                         (void *)&notify_ptr->cug_idx) )
          {
            dsm_free_packet(&ind);
            continue;
          }
        }
        notify_info.call_id = notify_ptr->call_id;
        notify_info.notify_type = notify_ptr->notify_type;
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                          VOICEI_SUPS_NOTIFY_INFO,
                                          sizeof(notify_info),
                                          (void *)&notify_info) )
        {
          dsm_free_packet(&ind);
          continue;
        }

      if( !qmi_voicei_send_indication(cl_sp,
                          VOICEI_CMD_VAL_SUPS_NOTIFICATION_IND,
                                   ind))
        {
        QM_MSG_MED("VOICE SUPS NOTIFICATION Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_sups_notification_ind() */


/* Supplementary Services API's*/


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CLIR()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_clir
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;


/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_clir");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CLIR;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_clir() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CLIR_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_clir_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  uint16 clir_failure_cause=0;
  struct
  {
    uint8 active_status;
    uint8 provision_status;
  }clir_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_clir_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_get_clir_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_clir_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_clir_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIR_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_clir_resp.retry_duration),
                                      (void *) &data_ptr->data.get_clir_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    clir_resp.active_status = data_ptr->data.get_clir_resp.active_status;
    clir_resp.provision_status = data_ptr->data.get_clir_resp.provision_status;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIR_RESP,
                                      sizeof(clir_resp),
                                      (void *) &clir_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    clir_failure_cause = (uint16)data_ptr->data.get_clir_resp.sups_failure_cause;
    QMI_MSG_HIGH ("%s %d\n","QMI GET CLIR operation failure cause = ",clir_failure_cause);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    VOICEI_GET_CLIR_FAILURE_CAUSE,
                                    sizeof(clir_failure_cause),
                                    (void *) &clir_failure_cause))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( data_ptr->data.get_clir_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_clir_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CLIR_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CLIR_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid data call control in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIR_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_clir_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_clir_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_clir_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_clir_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_clir_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_clir_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CLIR_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_clir_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CLIR operation");
  }

} /* qmi_voicei_sups_get_clir_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_FORWARDING()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_call_forwarding
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_forwarding");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.reason);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.reason;
        got_v_in_required = TRUE;
        break;
      case VOICEI_GET_CF_SC:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_valid = TRUE;
        break;
      case VOICEI_GET_CF_REQ_SC_EXT:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_ext);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_ext;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_ext_valid = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if( (cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_valid &&
       cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_ext_valid)
  	   &&
      (cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class != 
       cmd_ptr->cmd.cm_if_data_ptr->data.get_call_fwd.service_class_ext)
    )
    {
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_FORWARDING;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_call_forwarding() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_FORWARDING_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_call_forwarding_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_voice_cm_if_sups_get_call_fwd_resp_s *fwd_rsp;
  int idx = 0 , idx_exten =0;
  uint16 total_len = 0;
  uint8 call_fwd_info_tag = VOICEI_GET_CF_INFO, call_fwd_exten_info_tag = VOICEI_GET_CF_EXTEN_INFO;
  uint8 call_fwd_exten2_info_tag = VOICEI_GET_CF_EXTEN2_INFO;
  uint16  total_alpha_len = 0;
  uint16 total_size=0;
  int    tag=0;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_forwarding_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_call_forwarding_resp");
    return;
  }

  fwd_rsp = (qmi_voice_cm_if_sups_get_call_fwd_resp_s*)&data_ptr->data;

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = fwd_rsp->error;

  /* Fill call fwd end time info if present */
  if( fwd_rsp->is_call_fwd_end_time_available == TRUE )
  {
    total_size = sizeof(fwd_rsp->call_fwd_end_time.year) +\
                 sizeof(fwd_rsp->call_fwd_end_time.month) +\
                 sizeof(fwd_rsp->call_fwd_end_time.day) +\
                 sizeof(fwd_rsp->call_fwd_end_time.hour) +\
                 sizeof(fwd_rsp->call_fwd_end_time.minute) +\
                 sizeof(fwd_rsp->call_fwd_end_time.second) +\
                 sizeof(uint8);
    tag = VOICEI_GET_CF_CALL_FWD_END_TIME;
    if( !(QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.time_zone, sizeof(uint8)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.second, sizeof(fwd_rsp->call_fwd_end_time.second)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.minute, sizeof(fwd_rsp->call_fwd_end_time.minute)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.hour, sizeof(fwd_rsp->call_fwd_end_time.hour)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.day, sizeof(fwd_rsp->call_fwd_end_time.day)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.month, sizeof(fwd_rsp->call_fwd_end_time.month)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_end_time.year, sizeof(fwd_rsp->call_fwd_end_time.year)) &&
        QMI_SVC_PKT_PUSH(&response, (void *)&total_size, VOICEI_TLV_LENGTH_SIZE) && 
        QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)  ) )
    {          
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill call fwd start time info if present */
  if( fwd_rsp->is_call_fwd_start_time_available == TRUE )
  {
    total_size = sizeof(fwd_rsp->call_fwd_start_time.year) +\
                 sizeof(fwd_rsp->call_fwd_start_time.month) +\
                 sizeof(fwd_rsp->call_fwd_start_time.day) +\
                 sizeof(fwd_rsp->call_fwd_start_time.hour) +\
                 sizeof(fwd_rsp->call_fwd_start_time.minute) +\
                 sizeof(fwd_rsp->call_fwd_start_time.second) +\
                 sizeof(uint8);
    tag = VOICEI_GET_CF_CALL_FWD_START_TIME;
    if( !(QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.time_zone, sizeof(uint8)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.second, sizeof(fwd_rsp->call_fwd_start_time.second)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.minute, sizeof(fwd_rsp->call_fwd_start_time.minute)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.hour, sizeof(fwd_rsp->call_fwd_start_time.hour)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.day, sizeof(fwd_rsp->call_fwd_start_time.day)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.month, sizeof(fwd_rsp->call_fwd_start_time.month)) &&
        QMI_SVC_PKT_PUSH(&response, (void*)&fwd_rsp->call_fwd_start_time.year, sizeof(fwd_rsp->call_fwd_start_time.year)) &&
        QMI_SVC_PKT_PUSH(&response, (void *)&total_size, VOICEI_TLV_LENGTH_SIZE) && 
        QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)  ) )
    {          
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Populate the Provision Status TLV if no error is present */
  if( errval == QMI_ERR_NONE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CF_PROVISION_STATUS,
                                      sizeof(fwd_rsp->provision_status),
                                      (void *) &(fwd_rsp->provision_status)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill Retry Duration info if no error is present */
  if( fwd_rsp->is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CF_RETRY_DURATION,
                                      sizeof(fwd_rsp->retry_duration),
                                      (void *) &(fwd_rsp->retry_duration)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    /* Get the highest index */
    idx = fwd_rsp->num_instances - 1;
    /* Loop through the service class instances and fill the data */
    while((idx >= 0) && (idx < QMI_VOICE_CM_IF_MAX_SERVICE_CLASS_INSTANCES))
    {
      /* Put No reply timer */
      if(!QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances[idx].no_reply_timer, 
                                                       sizeof(fwd_rsp->sc_instances[idx].no_reply_timer)))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        break;
      }
      total_len += sizeof(uint8);
      /* Put fwd number if available */
      if(fwd_rsp->sc_instances[idx].num_len > 0)
      {
        if(!QMI_SVC_PKT_PUSH(&response, fwd_rsp->sc_instances[idx].num_buf, fwd_rsp->sc_instances[idx].num_len))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
          break;
        }
        total_len += fwd_rsp->sc_instances[idx].num_len;
      }
      /* Put num_len, service_class, service_status */
      if(!(QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances[idx].num_len, 
                                           sizeof(fwd_rsp->sc_instances[idx].num_len)) &&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances[idx].service_class, 
                                           sizeof(fwd_rsp->sc_instances[idx].service_class))&&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances[idx].service_status,
                                           sizeof(fwd_rsp->sc_instances[idx].service_status))
           ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        break;
      }
      total_len += 3 * sizeof(uint8);
      /* Decrement the index */
      idx--;
    } /* while */

    if( QMI_ERR_NONE == errval )
    {
      total_len += sizeof(fwd_rsp->num_instances);
      /* Put the num_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, &fwd_rsp->num_instances, sizeof(fwd_rsp->num_instances)) &&
           QMI_SVC_PKT_PUSH(&response, &total_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, &call_fwd_info_tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }

    //Exten-info TLV starts here ..
    /* Loop through the service class instances and fill the data */
    total_len = 0;
    idx_exten = fwd_rsp->num_exten_instances - 1;
    while((idx_exten >= 0) && (idx_exten < QMI_VOICE_CM_IF_MAX_SERVICE_CLASS_INSTANCES))
    {
      /* Put fwd number if available */
      if(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.len > 0)
      {
        if(!QMI_SVC_PKT_PUSH(&response, fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.num, fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.len))
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
          break;
        }
        total_len += fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.len;
      }
      /* Put num_len, service_class, service_status */
      if(!(QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.len, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.len)) &&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.num_plan, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.num_plan)) &&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.num_type, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.num_type)) &&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.si, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.si))&&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.pi, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].cf_num_info_exten.pi))&&                                           
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].no_reply_timer, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].no_reply_timer)) &&
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].service_class, 
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].service_class))&&                                           
           QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten[idx_exten].service_status,
                                           sizeof(fwd_rsp->sc_instances_exten[idx_exten].service_status))
           ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
        break;
      }
      total_len += 8 * sizeof(uint8);
      /* Decrement the index */
      idx_exten--;
    } /* while */

    if( QMI_ERR_NONE == errval )
    {
      total_len += sizeof(fwd_rsp->num_exten_instances);
      /* Put the num_instances, total length, Tag id */
      if(!(QMI_SVC_PKT_PUSH(&response, &fwd_rsp->num_exten_instances, sizeof(fwd_rsp->num_exten_instances)) &&
           QMI_SVC_PKT_PUSH(&response, &total_len, VOICEI_TLV_LENGTH_SIZE)&&
           QMI_SVC_PKT_PUSH(&response, &call_fwd_exten_info_tag, VOICEI_TLV_TAG_SIZE)
           ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    

     //Exten-info 2 TLV starts here ..
     /* Loop through the service class instances and fill the data */
     total_len = 0;
     idx_exten = fwd_rsp->num_exten2_instances - 1;
     while((idx_exten >= 0) && (idx_exten < QMI_VOICE_CM_IF_MAX_SERVICE_CLASS_INSTANCES))
     {
       /* Put fwd number if available */
       if(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.len > 0)
       {
         if(!QMI_SVC_PKT_PUSH(&response, fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.num, fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.len))
         {
           errval = QMI_ERR_NO_MEMORY;
           dsm_free_packet(&response);
           break;
         }
         total_len += fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.len;
       }
       /* Put num_len, service_class, service_status */
       if(!(QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.len,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.len)) &&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.num_plan,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.num_plan)) &&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.num_type,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.num_type)) &&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.si,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.si))&&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.pi,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].cf_num_info_exten.pi))&&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].no_reply_timer, 
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].no_reply_timer)) &&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].service_class_ext,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].service_class_ext))&&
            QMI_SVC_PKT_PUSH(&response, &fwd_rsp->sc_instances_exten2[idx_exten].service_status,
                                             sizeof(fwd_rsp->sc_instances_exten2[idx_exten].service_status))
         ) )
         {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
          break;
         }
         total_len += (7 * sizeof(uint8) + sizeof(fwd_rsp->sc_instances_exten2[idx_exten].service_class_ext));
       /* Decrement the index */
       idx_exten--;
     } /* while */
     
     if( QMI_ERR_NONE == errval )
     {
       total_len += sizeof(fwd_rsp->num_exten2_instances);
       /* Put the num_instances, total length, Tag id */
       if(!( QMI_SVC_PKT_PUSH(&response, &fwd_rsp->num_exten2_instances, sizeof(fwd_rsp->num_exten2_instances)) &&
             QMI_SVC_PKT_PUSH(&response, &total_len, VOICEI_TLV_LENGTH_SIZE)&&
             QMI_SVC_PKT_PUSH(&response, &call_fwd_exten2_info_tag, VOICEI_TLV_TAG_SIZE)
         ) )
        {
          errval = QMI_ERR_NO_MEMORY;
          dsm_free_packet(&response);
        }
     }
    
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    uint16 fwd_failure_cause = (uint16)fwd_rsp->sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    VOICEI_GET_CF_FAILURE_CAUSE,
                                    sizeof(fwd_failure_cause),
                                    (void *) &fwd_failure_cause))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( fwd_rsp->is_cc_modified )
  {
    cc_res = fwd_rsp->cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CF_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CF_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid data call control in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CF_CC_RESULT_TYPE,
                                      sizeof(fwd_rsp->cc_mod_type),
                                      (void *) &(fwd_rsp->cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(fwd_rsp->is_alpha_available)
  {
    total_alpha_len = sizeof(fwd_rsp->alpha_id.alpha_dcs) + \
                      sizeof(fwd_rsp->alpha_id.alpha_len) + \
                      fwd_rsp->alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CF_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(fwd_rsp->alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET CALL FWD operation");
  }

} /* qmi_voicei_sups_get_call_forwarding_resp() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CLIP()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_clip
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_clip");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }


  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CLIP;;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }
  
  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_clip() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_COLP()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_colp
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_colp");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }


  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_COLP;;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_colp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_WAITING()

  DESCRIPTION
    Get the CallWaiting status of a service class if not specified 
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_get_call_waiting
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_waiting");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case VOICEI_GET_CW_SC:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_valid = TRUE;
        break;
      case VOICEI_GET_CW_REQ_SC_EXT:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_ext);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_ext;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_ext_valid = TRUE;
        break;		
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if((cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_valid && 
  	  cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_ext_valid
  	 )
  	 &&
  	 (cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class !=
  	  cmd_ptr->cmd.cm_if_data_ptr->data.get_call_waiting.service_class_ext
  	 )
    )
    {
      errval = QMI_ERR_INVALID_ARG;
      goto send_result; 	  
    }
  	  

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_WAITING;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }
  


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_call_waiting() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CALL_BARRING()

  DESCRIPTION
    Get the Callbarring status for a specified reason with service class.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_get_call_barring
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_call_barring");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.reason);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.reason;
        got_v_in_required = TRUE;
        break;
      case VOICEI_GET_CB_SC:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_valid = TRUE;
        break;
      case VOICEI_GET_CB_REQ_SC_EXT:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext_valid = TRUE;
        break;		
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if( (cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_valid &&
  	   cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext_valid)
  	   &&
  	  (cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class != 
  	   cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext)
    )
  {
     errval = QMI_ERR_INVALID_ARG;
     goto send_result;
  }

  QM_MSG_MED_2("Get qmi_voice_call_barring SC=%d, SC_EXT=%d", cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class, cmd_ptr->cmd.cm_if_data_ptr->data.get_call_barring.service_class_ext);

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CALL_BARRING;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_call_barring() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_SET_CALL_BARRING()

  DESCRIPTION
    Set the Callbarring Password for a specified reason with service class.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_set_call_barring
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_set_call_barring");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_call_barring);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_call_barring;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_SET_CALL_BARRING;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_call_barring() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_MANAGE_CALLS()

  DESCRIPTION
    Set the Callbarring Password for a specified reason with service class.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_manage_calls
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_manage_calls");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sups_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sups_type;
        got_v_in_required = TRUE;
        break;

      case VOICEI_MNG_CALLS_REQ_CALL_ID:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id_valid = TRUE;
        break;

      case VOICEI_MNG_CALLS_REQ_REJ_CAUSE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause_valid = TRUE;
        break;

      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_MANAGE_CALLS;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_get_call_barring() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_SET_SUPS_SERVICE()

  DESCRIPTION
    Set the Call Independant supplementary services with specified reason and service.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_set_sups_service
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

  uint8 i=0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_set_sups_service");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }
  
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.sups_service_info);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.sups_service_info;
        got_v_in_required = TRUE;
        break;
      case VOICEI_SET_SUPS_SC:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_SC_EXT:
	expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_ext);
	value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_ext;
	cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_ext_valid = TRUE;
	break;
      case VOICEI_SET_SUPS_BARR_PWD:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.password);
        value = cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.password;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.password_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_FWD_NUM:
        value = cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_number;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_number))
        {
          QM_MSG_MED("Invalid calling number length");
          errval = QMI_ERR_ARG_TOO_LONG;
          
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_number_len = (uint8)len;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_number_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_NR_TIMER:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.no_reply_timer);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.no_reply_timer;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.no_reply_timer_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_NUM_TYPE_PLAN:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.num_type_plan);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.num_type_plan;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.num_type_plan_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_BARR_NUM_LIST:
        if ( !(QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list_len, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list_len)) ) )
        {
          QM_MSG_MED("Invalid length in barr list TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        else
        {
          for(i=0;i<cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list_len;i++)
          {
            if(!(QMI_VOICEI_PULL(sdu_in, &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list[i].barr_num_len, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list[i].barr_num_len)) &&
                 QMI_VOICEI_PULL(sdu_in, cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list[i].barr_num, cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list[i].barr_num_len))
                 )
             {
               QM_MSG_MED("Invalid data in barr list TLV");
               errval = QMI_ERR_MALFORMED_MSG;
               goto send_result;
             }
          }
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.barr_num_list_valid = TRUE;
        continue; //By design; do not change.
      case VOICEI_SET_SUPS_COLR_PI:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.colr_pi);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.colr_pi;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.colr_pi_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_CALL_FWD_START_TIME:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_start_time))
        {
          QM_MSG_MED("Invalid Start time length");
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_start_time;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_start_time_valid = TRUE;
        break;
      case VOICEI_SET_SUPS_CALL_FWD_END_TIME:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_end_time))
        {
          QM_MSG_MED("Invalid End time length");
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_end_time;
        cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.fwd_end_time_valid = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if( (cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_valid &&
  	   cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_ext_valid)
  	   &&
  	  (cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class != 
  	   cmd_ptr->cmd.cm_if_data_ptr->data.set_sups_info.service_class_ext)
    )
    {
      errval = QMI_ERR_INVALID_ARG;
      goto send_result;
    }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_SET_SUPS_SERVICE;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_set_sups_service() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_ANSWER_USSD()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_answer_ussd
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_answer_ussd");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.answer_ussd.ussd_info))
        {
          QM_MSG_MED("Invalid USSD Info size");
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.answer_ussd.ussd_info;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_ANSWER_USSD;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_ANSWER_USSD_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_answer_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_answer_ussd_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_answer_ussd_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.answer_ussd_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE ANSWER USSD operation");
}
}/* qmi_voicei_sups_answer_ussd_resp */


/*===========================================================================
  FUNCTION QMI_VOICEI_ORIG_USSD()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_orig_ussd
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

  struct 
  {
    uint8               uss_dcs;
    uint8               uss_data_len;
    uint8               uss_data[QMI_VOICE_CM_IF_MAX_USS_DATA_LEN];
  }uusd_data;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_orig_ussd");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  memset(&uusd_data, 0, sizeof(uusd_data));

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        value = &uusd_data;
        if(len > (sizeof(uusd_data.uss_dcs) + sizeof(uusd_data.uss_data_len) + sizeof(uusd_data.uss_data)))
        {
          QM_MSG_MED_1("Invalid UUS DATA %d", len);
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_ORIG_USSD;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }
  

  
  cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_info.uss_dcs = uusd_data.uss_dcs;
  cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_info.uss_data_len = uusd_data.uss_data_len;
  /* QMI_Security check*/
  if(uusd_data.uss_data_len > QMI_VOICE_CM_IF_MAX_USS_DATA_LEN)
  {
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }
  memscpy(cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_info.uss_data, 
          sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_info.uss_data),
          uusd_data.uss_data, uusd_data.uss_data_len );

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_orig_ussd() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_ORIG_USSD_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_orig_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  uint16                   total_ussd_len = 0;
  uint8                   tag = VOICEI_ORIG_USSD_DATA_UTF16;
  struct
  {
    qmi_voice_cm_if_sups_orig_ussd_s ussd_data;
  }ussd_orig_resp;

  struct
  {
    uint16 failure_cause;
  }ussd_orig_err_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

  struct
  {
    qmi_voice_cm_if_ussd_info_utf16_s  ussd_info_utf16;    
  } ussd_orig_resp_utf16;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_orig_ussd_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_orig_ussd_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.ussd_orig_resp.error;

  if( (QMI_ERR_NONE == errval) && (data_ptr->data.ussd_orig_resp.is_ussd_data_available == TRUE))
  {
    memset(&ussd_orig_resp,0,sizeof(ussd_orig_resp));
    ussd_orig_resp.ussd_data.uss_data_len = data_ptr->data.ussd_orig_resp.ussd_data.uss_data_len;
    ussd_orig_resp.ussd_data.uss_dcs = data_ptr->data.ussd_orig_resp.ussd_data.uss_dcs;
    /* QMI_Security check*/
    if(ussd_orig_resp.ussd_data.uss_data_len > QMI_VOICE_CM_IF_MAX_USS_DATA_LEN)
    {
      ussd_orig_resp.ussd_data.uss_data_len = QMI_VOICE_CM_IF_MAX_USS_DATA_LEN;
    }
    memscpy(ussd_orig_resp.ussd_data.uss_data,
            sizeof(ussd_orig_resp.ussd_data.uss_data),
            data_ptr->data.ussd_orig_resp.ussd_data.uss_data,
            ussd_orig_resp.ussd_data.uss_data_len);
    total_ussd_len = sizeof(data_ptr->data.ussd_orig_resp.ussd_data.uss_dcs) + \
                             sizeof(data_ptr->data.ussd_orig_resp.ussd_data.uss_data_len) + \
                             data_ptr->data.ussd_orig_resp.ussd_data.uss_data_len;
    QM_MSG_MED_2("Sending QMI orig ussd with dcs = %d, len = %d ", ussd_orig_resp.ussd_data.uss_dcs, ussd_orig_resp.ussd_data.uss_data_len);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_ORIG_USSD_DATA,
                                      total_ussd_len,
                                      (void *) &ussd_orig_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if((QMI_ERR_NONE == errval) && (data_ptr->data.ussd_orig_resp.is_ussd_info_utf16_available == TRUE))
  {
    QM_MSG_MED_1("Sending QMI orig ussd with len = %d ", data_ptr->data.ussd_orig_resp.ussd_info_utf16.ussd_data_utf16_len);
    memset(&ussd_orig_resp_utf16,0,sizeof(ussd_orig_resp_utf16));
    ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len = data_ptr->data.ussd_orig_resp.ussd_info_utf16.ussd_data_utf16_len;    
    /* QMI_Security check*/
    if(ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len > QMI_VOICE_CM_IF_MAX_USS_DATA_LEN)
    {
      ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len = QMI_VOICE_CM_IF_MAX_USS_DATA_LEN;
    }
    memscpy(ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16,
            sizeof(ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16),
            data_ptr->data.ussd_orig_resp.ussd_info_utf16.ussd_data_utf16,
            (ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)*2);

    total_ussd_len = sizeof(data_ptr->data.ussd_orig_resp.ussd_info_utf16.ussd_data_utf16_len) + \
                            (data_ptr->data.ussd_orig_resp.ussd_info_utf16.ussd_data_utf16_len)*2;

    if( !(QMI_SVC_PKT_PUSH(&response, (void*)ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16, (ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)*2) &&
             QMI_SVC_PKT_PUSH(&response, (void*)&ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len, sizeof(ussd_orig_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)) &&
             QMI_SVC_PKT_PUSH(&response, (void *)&total_ussd_len, VOICEI_TLV_LENGTH_SIZE) &&
             QMI_SVC_PKT_PUSH(&response, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }    
  }


  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    ussd_orig_err_resp.failure_cause = data_ptr->data.ussd_orig_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_ORIG_USSD_FAILURE_CAUSE,
                                      sizeof(ussd_orig_err_resp),
                                      (void *) &ussd_orig_err_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.ussd_orig_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.ussd_orig_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_ORIG_USSD_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_ORIG_USSD_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid call control data in response");
    }

    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_ORIG_USSD_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.ussd_orig_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.ussd_orig_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.ussd_orig_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.ussd_orig_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.ussd_orig_resp.alpha_id.alpha_len) + \
                      data_ptr->data.ussd_orig_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_ORIG_USSD_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.ussd_orig_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE ORIG USSD operation");
  }
}/*qmi_voicei_sups_orig_ussd_resp*/


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_CANCEL_USSD()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_cancel_ussd
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_cancel_ussd");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_CANCEL_USSD;;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }
  
  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_cancel_ussd() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_ORIG_USSD_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_cancel_ussd_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_cancel_ussd_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_cancel_ussd_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.cancel_ussd_resp.error;
  response = NULL;
  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE CANCEL USSD operation");
  }
}/*qmi_voicei_sups_cancel_ussd_resp*/

/*===========================================================================
  FUNCTION QMI_VOICEI_SET_CONFIG_ITEMS()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_set_modem_config
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voice_cm_if_set_modem_config_s  *set_cfg_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_set_modem_config");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  set_cfg_ptr = &cmd_ptr->cmd.cm_if_data_ptr->data.set_config;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    switch (type)
    {
      case VOICEI_MODEM_SET_CONFIG_AUTO_ANSWER:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->auto_answer, sizeof(set_cfg_ptr->auto_answer)) ) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->auto_answer_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_AIR_TIMER:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->air_time.nam_id, sizeof(set_cfg_ptr->air_time.nam_id)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->air_time.air_timer, sizeof(set_cfg_ptr->air_time.air_timer))) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->air_timer_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_ROAM_TIMER:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->roam_time.nam_id, sizeof(set_cfg_ptr->roam_time.nam_id)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->roam_time.roam_timer, sizeof(set_cfg_ptr->roam_time.roam_timer))) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->roam_timer_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_TTY_MODE:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->tty_mode, sizeof(set_cfg_ptr->tty_mode)) ) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->tty_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_PREF_VOICE_SO:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->pref_voice_so.nam_id, sizeof(set_cfg_ptr->pref_voice_so.nam_id)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->pref_voice_so.evrc_capability, sizeof(set_cfg_ptr->pref_voice_so.evrc_capability)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->pref_voice_so.home_page_voice_so, sizeof(set_cfg_ptr->pref_voice_so.home_page_voice_so)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->pref_voice_so.home_orig_voice_so, sizeof(set_cfg_ptr->pref_voice_so.home_orig_voice_so)) &&
                 QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->pref_voice_so.roam_orig_voice_so, sizeof(set_cfg_ptr->pref_voice_so.roam_orig_voice_so))) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->pref_voice_so_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_PREF_VOICE_DOMAIN:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->voice_domain_pref, sizeof(set_cfg_ptr->voice_domain_pref)) ) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->voice_domain_pref_available = TRUE;
        break;
      case VOICEI_MODEM_SET_CONFIG_UI_TTY_SETTING:
        if ( !(QMI_VOICEI_PULL(sdu_in, &set_cfg_ptr->ui_tty_setting, sizeof(set_cfg_ptr->ui_tty_setting)) ) )
        {
          QM_MSG_MED("Invalid length in TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        set_cfg_ptr->ui_tty_setting_available = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SET_MODEM_CONFIG;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                              : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_set_config_items() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SET_MODEM_CONFIG_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_set_modem_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  boolean                 retval;
  qmi_voice_cm_if_set_modem_config_resp_s  set_config_resp;
  qmi_framework_common_msg_hdr_type  common_hdr;
  qmi_voicei_client_state_type *    cl_sp;
  qmi_result_e_type result;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_set_modem_config_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_set_modem_config_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.set_config_resp.error;
  memset(&set_config_resp,0,sizeof(set_config_resp));
  if( QMI_ERR_INTERNAL == errval )
  {
    if(data_ptr->data.set_config_resp.auto_answer_available)
    {
      set_config_resp.auto_answer_outcome = data_ptr->data.set_config_resp.auto_answer_outcome;
      QM_MSG_MED_1("Sending QMI SET Auto Answer status Response = %d", set_config_resp.auto_answer_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_AUTO_ANSWER,
                                        sizeof(set_config_resp.auto_answer_outcome),
                                        (void *) &set_config_resp.auto_answer_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.set_config_resp.air_timer_available)
    {
      set_config_resp.air_timer_outcome = data_ptr->data.set_config_resp.air_timer_outcome;
      QM_MSG_MED_1("Sending QMI SET Air Timer status Response = %d", set_config_resp.air_timer_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_AIR_TIMER,
                                        sizeof(set_config_resp.air_timer_outcome),
                                        (void *) &set_config_resp.air_timer_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.set_config_resp.roam_timer_available)
    {
      set_config_resp.roam_timer_outcome = data_ptr->data.set_config_resp.roam_timer_outcome;
      QM_MSG_MED_1("Sending QMI SET Roam Timer status Response = %d", set_config_resp.roam_timer_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_ROAM_TIMER,
                                        sizeof(set_config_resp.roam_timer_outcome),
                                        (void *) &set_config_resp.roam_timer_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.set_config_resp.tty_available)
    {
      set_config_resp.tty_mode_outcome = data_ptr->data.set_config_resp.tty_mode_outcome;
      QM_MSG_MED_1("Sending QMI SET TTY Mode status Response = %d", set_config_resp.tty_mode_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_TTY_MODE,
                                        sizeof(set_config_resp.tty_mode_outcome),
                                        (void *) &set_config_resp.tty_mode_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.set_config_resp.pref_voice_so_available)
    {
      set_config_resp.pref_voice_so_outcome = data_ptr->data.set_config_resp.pref_voice_so_outcome;
      QM_MSG_MED_1("Sending QMI SET Pref Voice SO status Response = %d", set_config_resp.pref_voice_so_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_PREF_VOICE_SO,
                                        sizeof(set_config_resp.pref_voice_so_outcome),
                                        (void *) &set_config_resp.pref_voice_so_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    } 
    if(data_ptr->data.set_config_resp.voice_domain_pref_available)
    {
      set_config_resp.voice_domain_pref_outcome = data_ptr->data.set_config_resp.voice_domain_pref_outcome;
      QM_MSG_MED_1("Sending QMI SET Pref Voice Domain Pref Outcome Response = %d", set_config_resp.voice_domain_pref_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_PREF_VOICE_DOMAIN,
                                        sizeof(set_config_resp.voice_domain_pref_outcome),
                                        (void *) &set_config_resp.voice_domain_pref_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    } 
    if(data_ptr->data.set_config_resp.ui_tty_setting_available)
    {
      set_config_resp.ui_tty_setting_outcome = data_ptr->data.set_config_resp.ui_tty_setting_outcome;
      QM_MSG_MED_1("Sending QMI SET UI TTY Setting status Response = %d", set_config_resp.ui_tty_setting_outcome);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_SET_CONFIG_UI_TTY_SETTING,
                                        sizeof(set_config_resp.ui_tty_setting_outcome),
                                        (void *) &set_config_resp.ui_tty_setting_outcome))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    } 
  }

  if (errval == QMI_ERR_NONE)
  {
    result = QMI_RESULT_SUCCESS;
  }
  else if ( (errval == QMI_ERR_INTERNAL) &&
             ((data_ptr->data.set_config_resp.auto_answer_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS) ||
              (data_ptr->data.set_config_resp.air_timer_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS) ||
              (data_ptr->data.set_config_resp.roam_timer_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS) ||
              (data_ptr->data.set_config_resp.tty_mode_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS) ||
              (data_ptr->data.set_config_resp.pref_voice_so_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS) || 
              (data_ptr->data.set_config_resp.voice_domain_pref_outcome == QMI_VOICE_CM_IF_SET_MODEM_SUCCESS)
           ) )
  {
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    result = QMI_RESULT_FAILURE;
  }

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    qmi_mmode_svc_free_transaction_cmd_buf( &cmd_buf_p );
    return;
  }

   /* Check if the client is still present */
   if( (cl_sp = qmi_voicei_validate_cmd_buf_p(cmd_buf_p)) == NULL )
   {
     QM_MSG_ERROR("Client, transaction no longer valid. Cannot send response");
     dsm_free_packet(&response);
     return;
   }

   /* Fill common header and send response */  
   memset(&common_hdr, 0, sizeof(common_hdr));
   common_hdr.service = (qmux_service_e_type)cl_sp->service_id;
   common_hdr.client_id = cl_sp->common.clid;
   /* Transaction id will be filled by svc layer */
   common_hdr.qmi_instance = cl_sp->instance;
  
   /* Send the response */
  if(!qmi_mmode_svc_send_response( &common_hdr,cmd_buf_p, response))
  {
    QM_MSG_HIGH("Unable to send response for QMI SET MODEM CONFIG operation");
    if(response != NULL)
    {
      dsm_free_packet(&response);
    }
  }

}/*qmi_voicei_set_modem_config_resp*/

/*===========================================================================
  FUNCTION QMI_VOICEI_SRVCC_CALL_CONFIG_REQ()

  DESCRIPTION
    This Command will fill the call information in CM for enabling SRVCC of calls
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_srvcc_call_config_req
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type,i=0,j=0;
  uint16             len;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voice_cm_if_set_srvcc_call_context_s  *srvcc_calls_info;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
  uint8              instance_id=0;
  boolean            instance_found = FALSE;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_srvcc_call_config_req");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  srvcc_calls_info = &cmd_ptr->cmd.cm_if_data_ptr->data.srvcc_calls_info;

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        if ( !(QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->num_calls, sizeof(srvcc_calls_info->num_calls)) ) )
        {
          QM_MSG_MED("Invalid length in call info TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        else
        {
          for(i=0;i<srvcc_calls_info->num_calls;i++)
          {
            if(!(QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].instance_id, sizeof(srvcc_calls_info->srvcc_calls[i].instance_id)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].call_type, sizeof(srvcc_calls_info->srvcc_calls[i].call_type)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].call_state, sizeof(srvcc_calls_info->srvcc_calls[i].call_state)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].call_substate, sizeof(srvcc_calls_info->srvcc_calls[i].call_substate)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].is_mpty_call, sizeof(srvcc_calls_info->srvcc_calls[i].is_mpty_call)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].direction, sizeof(srvcc_calls_info->srvcc_calls[i].direction)) &&
                 QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].num_len, sizeof(srvcc_calls_info->srvcc_calls[i].num_len)) &&
                 QMI_VOICEI_PULL(sdu_in, srvcc_calls_info->srvcc_calls[i].num, srvcc_calls_info->srvcc_calls[i].num_len))
                 )
             {
               QM_MSG_MED("Invalid data in call info TLV");
               errval = QMI_ERR_MALFORMED_MSG;
               goto send_result;
             }
          }
        }
        break;
      case VOICEI_SRVCC_CALL_CONFIG_REQ_ALERTING_TYPE:
        if ( !(QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->alerting_type_list_len, sizeof(srvcc_calls_info->alerting_type_list_len)) ) )
        {
          QM_MSG_MED("Invalid length in call info TLV");
          errval = QMI_ERR_MALFORMED_MSG;
          goto send_result;
        }
        else
        {
          for(i=0;i<srvcc_calls_info->alerting_type_list_len;i++)
          {
            if(QMI_VOICEI_PULL(sdu_in, &instance_id, sizeof(instance_id)))
            {
              instance_found = FALSE;
              for(j=0;j<srvcc_calls_info->num_calls;j++)
              {
                if(srvcc_calls_info->srvcc_calls[j].instance_id == instance_id)
                {
                  instance_found = TRUE;
                  srvcc_calls_info->srvcc_calls[j].is_alerting_type_valid = TRUE;
                  if(!QMI_VOICEI_PULL(sdu_in, &srvcc_calls_info->srvcc_calls[i].alerting_type, sizeof(srvcc_calls_info->srvcc_calls[i].alerting_type)))
                  {
                    QM_MSG_MED("Invalid data in alerting_type TLV");
                    errval = QMI_ERR_MALFORMED_MSG;
                    goto send_result;
                  }
                }
              }
              if(instance_found == FALSE)
              {
                QM_MSG_MED_1("instance ID %d given in the alerting_type not found in call list", instance_id);
                errval = QMI_ERR_MALFORMED_MSG;
                goto send_result;
              }
            }
            else
            {
               QM_MSG_MED("Invalid instance id in alerting_type TLV");
               errval = QMI_ERR_MALFORMED_MSG;
               goto send_result;
            }
          }
        }
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SRVCC_CALL_CONFIG;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                              : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_srvcc_call_config_req() */


/*===========================================================================
  FUNCTION qmi_voicei_srvcc_call_config_resp()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_srvcc_call_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_srvcc_call_config_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_srvcc_call_config_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.set_srvcc_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for qmi_voicei_srvcc_call_config operation");
  }
}

/*===========================================================================
  FUNCTION QMI_VOICEI_GET_CONFIG_ITEMS()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_get_modem_config
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_get_modem_config");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case VOICEI_MODEM_GET_CONFIG_AUTO_ANSWER:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.auto_answer);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.auto_answer;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.auto_answer_available = TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_AIR_TIMER:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.air_timer);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.air_timer;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.air_timer_available = TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_ROAM_TIMER:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.roam_timer);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.roam_timer;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.roam_timer_available = TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_TTY_MODE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.tty_mode);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.tty_mode;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.tty_mode_available= TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_PREF_VOICE_SO:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.pref_voice_so);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.pref_voice_so;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.pref_voice_so_available = TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_AMR_STATUS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.amr_status);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.amr_status;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.amr_status_available= TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_VOICE_PRIVACY:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_privacy);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_privacy;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_privacy_available= TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_NAM_ID:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.nam_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.nam_id;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.nam_id_available= TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_PREF_VOICE_DOMAIN:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_domain_pref);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_domain_pref;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.voice_domain_pref_available = TRUE;
        break;
      case VOICEI_MODEM_GET_CONFIG_UI_TTY_SETTING:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_config.ui_tty_setting);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_config.ui_tty_setting;
        cmd_ptr->cmd.cm_if_data_ptr->data.get_config.ui_tty_setting_available= TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_GET_MODEM_CONFIG;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }


  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_get_modem_config() */

/*===========================================================================
  FUNCTION QMI_VOICEI_GET_MODEM_CONFIG_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    None
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_get_modem_config_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  qmi_result_e_type       result;
  boolean                 retval;
  uint16                  total_len = 0;
  uint8 rec_tag=0;
  qmi_voice_cm_if_get_modem_config_resp_s  get_config_resp;
  qmi_framework_common_msg_hdr_type  common_hdr;
  qmi_voicei_client_state_type *    cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_get_modem_config_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_get_modem_config_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_config_resp.error;
  memset(&get_config_resp,0,sizeof(get_config_resp));
  if( (QMI_ERR_NONE == errval) || (QMI_ERR_INTERNAL== errval))
  {
    if(data_ptr->data.get_config_resp.auto_answer_available)
    {
      get_config_resp.auto_answer = data_ptr->data.get_config_resp.auto_answer;
      QM_MSG_MED_1("Sending QMI GET Auto Answer Response = %d", get_config_resp.auto_answer);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_GET_CONFIG_AUTO_ANSWER,
                                        sizeof(get_config_resp.auto_answer),
                                        (void *) &get_config_resp.auto_answer))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    
    if(data_ptr->data.get_config_resp.air_timer_available)
    {
      rec_tag = VOICEI_MODEM_GET_CONFIG_AIR_TIMER;
      get_config_resp.air_time.air_timer = data_ptr->data.get_config_resp.air_time.air_timer;
      get_config_resp.air_time.nam_id = data_ptr->data.get_config_resp.air_time.nam_id;
      QMI_MSG_MED ("Sending QMI GET Air Timer status Response namid = %d, time=%ld\n",get_config_resp.air_time.air_timer,get_config_resp.air_time.nam_id);
      total_len = sizeof(get_config_resp.air_time.air_timer) + sizeof(get_config_resp.air_time.nam_id);
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.air_time.air_timer, sizeof(get_config_resp.air_time.air_timer)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.air_time.nam_id, sizeof(get_config_resp.air_time.nam_id)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&rec_tag, VOICEI_TLV_TAG_SIZE) ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }

    if(data_ptr->data.get_config_resp.roam_timer_available)
    {
      rec_tag = VOICEI_MODEM_GET_CONFIG_ROAM_TIMER;
      get_config_resp.roam_time.roam_timer = data_ptr->data.get_config_resp.roam_time.roam_timer;
      get_config_resp.roam_time.nam_id = data_ptr->data.get_config_resp.roam_time.nam_id;
      QMI_MSG_MED ("Sending QMI GET Roam Timer status Response namid = %d, time=%ld\n",get_config_resp.roam_time.roam_timer,get_config_resp.roam_time.nam_id);
      total_len = sizeof(get_config_resp.roam_time.roam_timer) + sizeof(get_config_resp.roam_time.nam_id);
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.roam_time.roam_timer, sizeof(get_config_resp.roam_time.roam_timer)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.roam_time.nam_id, sizeof(get_config_resp.roam_time.nam_id)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&rec_tag, VOICEI_TLV_TAG_SIZE) ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.get_config_resp.tty_available)
    {
      get_config_resp.tty_mode = data_ptr->data.get_config_resp.tty_mode;
      QM_MSG_MED_1("Sending QMI GET TTY Mode Response = %d", get_config_resp.tty_mode);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_GET_CONFIG_TTY_MODE,
                                        sizeof(get_config_resp.tty_mode),
                                        (void *) &get_config_resp.tty_mode))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.get_config_resp.pref_voice_so_available)
    {
      rec_tag = VOICEI_MODEM_GET_CONFIG_PREF_VOICE_SO;
      get_config_resp.pref_voice_so.nam_id = data_ptr->data.get_config_resp.pref_voice_so.nam_id;
      get_config_resp.pref_voice_so.evrc_capability = data_ptr->data.get_config_resp.pref_voice_so.evrc_capability;
      get_config_resp.pref_voice_so.home_page_voice_so = data_ptr->data.get_config_resp.pref_voice_so.home_page_voice_so;
      get_config_resp.pref_voice_so.home_orig_voice_so = data_ptr->data.get_config_resp.pref_voice_so.home_orig_voice_so;
      get_config_resp.pref_voice_so.roam_orig_voice_so = data_ptr->data.get_config_resp.pref_voice_so.roam_orig_voice_so;

      QM_MSG_MED_2("Sending QMI GET PREF_VOICE Response namid = %d, evrc_capability=%d", get_config_resp.pref_voice_so.nam_id, get_config_resp.pref_voice_so.evrc_capability);
      QM_MSG_MED_2("Sending QMI GET PREF_VOICE Response home_page_voice_so = %d, home_orig_voice_so=%d", get_config_resp.pref_voice_so.home_page_voice_so, get_config_resp.pref_voice_so.home_orig_voice_so);
      QM_MSG_MED_1("Sending QMI GET PREF_VOICE Response roam_orig_voice_so = %d", get_config_resp.pref_voice_so.roam_orig_voice_so);
      
      total_len = sizeof(get_config_resp.pref_voice_so.roam_orig_voice_so) + \
                      sizeof(get_config_resp.pref_voice_so.home_orig_voice_so) + \
                      sizeof(get_config_resp.pref_voice_so.home_page_voice_so) + \
                      sizeof(get_config_resp.pref_voice_so.evrc_capability) + \
                      sizeof(get_config_resp.pref_voice_so.nam_id); 

      if( !(QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.pref_voice_so.roam_orig_voice_so, sizeof(get_config_resp.pref_voice_so.roam_orig_voice_so)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.pref_voice_so.home_orig_voice_so, sizeof(get_config_resp.pref_voice_so.home_orig_voice_so)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.pref_voice_so.home_page_voice_so, sizeof(get_config_resp.pref_voice_so.home_page_voice_so)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.pref_voice_so.evrc_capability, sizeof(get_config_resp.pref_voice_so.evrc_capability)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.pref_voice_so.nam_id, sizeof(get_config_resp.pref_voice_so.nam_id)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&rec_tag, VOICEI_TLV_TAG_SIZE) ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }

    if(data_ptr->data.get_config_resp.amr_status_available)
    {
      rec_tag = VOICEI_MODEM_GET_CONFIG_AMR_STATUS;
      get_config_resp.amr_status.gsm_amr_status = data_ptr->data.get_config_resp.amr_status.gsm_amr_status;
      get_config_resp.amr_status.umts_amr_status = data_ptr->data.get_config_resp.amr_status.umts_amr_status;
      QMI_MSG_MED ("Sending QMI GET AMR status Response gsm_amr_status = %d, umts_amr_status=%ld\n",get_config_resp.amr_status.gsm_amr_status,get_config_resp.amr_status.umts_amr_status);
      total_len = sizeof(get_config_resp.amr_status.gsm_amr_status) + sizeof(get_config_resp.amr_status.umts_amr_status);
      if( !(QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.amr_status.umts_amr_status, sizeof(get_config_resp.amr_status.umts_amr_status)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&get_config_resp.amr_status.gsm_amr_status, sizeof(get_config_resp.amr_status.gsm_amr_status)) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&total_len, VOICEI_TLV_LENGTH_SIZE) &&
              QMI_SVC_PKT_PUSH(&response, (void*)&rec_tag, VOICEI_TLV_TAG_SIZE) ) )
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }

    if(data_ptr->data.get_config_resp.voice_privacy_available)
    {
      get_config_resp.voice_privacy = data_ptr->data.get_config_resp.voice_privacy;
      QM_MSG_MED_1("Sending QMI GET Current Voice Privacy Pref Response = %d", get_config_resp.voice_privacy);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_GET_CONFIG_VOICE_PRIVACY,
                                        sizeof(get_config_resp.voice_privacy),
                                        (void *) &get_config_resp.voice_privacy))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
         
    if(data_ptr->data.get_config_resp.voice_domain_pref_available)
    {
      get_config_resp.voice_domain_pref = data_ptr->data.get_config_resp.voice_domain_pref;
      QM_MSG_MED_1("Sending QMI GET Current Voice Domain Preference Response = %d", get_config_resp.voice_domain_pref);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_GET_CONFIG_RESP_PREF_VOICE_DOMAIN,
                                        sizeof(get_config_resp.voice_domain_pref),
                                        (void *) &get_config_resp.voice_domain_pref))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.get_config_resp.ui_tty_setting_available)
    {
      get_config_resp.ui_tty_setting = data_ptr->data.get_config_resp.ui_tty_setting;
      QM_MSG_MED_1("Sending QMI GET UI TTY Setting Response = %d", get_config_resp.ui_tty_setting);
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MODEM_GET_CONFIG_UI_TTY_SETTING,
                                        sizeof(get_config_resp.ui_tty_setting),
                                        (void *) &get_config_resp.ui_tty_setting))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
         
  }

  if ( errval == QMI_ERR_NONE )
  {
    result = QMI_RESULT_SUCCESS;
  }
  else if ( (errval == QMI_ERR_INTERNAL) &&
             ((data_ptr->data.get_config_resp.auto_answer_available == TRUE) ||
              (data_ptr->data.get_config_resp.air_timer_available == TRUE) ||
              (data_ptr->data.get_config_resp.roam_timer_available == TRUE) ||
              (data_ptr->data.get_config_resp.tty_available == TRUE) ||
              (data_ptr->data.get_config_resp.pref_voice_so_available == TRUE) ||
              (data_ptr->data.get_config_resp.amr_status_available == TRUE) ||
              (data_ptr->data.get_config_resp.voice_privacy_available == TRUE) ||
              (data_ptr->data.get_config_resp.voice_domain_pref_available == TRUE)
             ) )
  {
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    result = QMI_RESULT_FAILURE;
  }

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  if(FALSE == retval)
  {
    dsm_free_packet(&response);
    qmi_mmode_svc_free_transaction_cmd_buf( &cmd_buf_p );
    return;
  }

   /* Check if the client is still present */
   if( (cl_sp = qmi_voicei_validate_cmd_buf_p(cmd_buf_p)) == NULL )
   {
     QM_MSG_ERROR("Client, transaction no longer valid. Cannot send response");
     dsm_free_packet(&response);
     return;
   }

   /* Fill common header and send response */  
   memset(&common_hdr, 0, sizeof(common_hdr));
   common_hdr.service = (qmux_service_e_type)cl_sp->service_id;
   common_hdr.client_id = cl_sp->common.clid;
   /* Transaction id will be filled by svc layer */
   common_hdr.qmi_instance = cl_sp->instance;
  
   /* Send the response */
  if(!qmi_mmode_svc_send_response( &common_hdr,cmd_buf_p, response))
  {
    QM_MSG_HIGH("Unable to send response for QMI GET MODEM CONFIG operation");
    if(response != NULL)
  {
    dsm_free_packet(&response);
  }
  }
}/*qmi_voicei_get_modem_config_resp*/


/*===========================================================================
  FUNCTION QMI_VOICEI_BIND_SUBSCRIPTION()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_bind_subscription
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_bind_req;
  uint8              bind_req_data = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_bind_subscription");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_bind_req    = FALSE;

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(bind_req_data);
        value = &bind_req_data;
        got_bind_req = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if ( !got_bind_req  )
  {
    QM_MSG_MED("Missing Mandatory TLV");
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }
  if(bind_req_data >= QMI_VOICE_CM_IF_AS_ID_MAX)
  {
    QM_MSG_MED_2("Unsupported TLV value (%d) for type (%d)", bind_req_data, type);
    errval = QMI_ERR_NO_SUBSCRIPTION;
    goto send_result;
  }
  
  if(client_sp != NULL)
  {
    client_sp->asubs_info.as_id = bind_req_data;
    client_sp->asubs_info.is_bind_set_by_client = TRUE;
    QMI_MSG_MED ("BIND REQUESTED BY: clid=%d Bounded to as_id=%d \n",client_sp->common.clid,client_sp->asubs_info.as_id);
  }
  else
  {
    return QMI_SVC_RESPONSE_NONE;
  }
  
send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}/*qmi_voicei_bind_subscription*/

/*===========================================================================
  FUNCTION QMI_VOICEI_ORIG_USSD_NO_WAIT()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_orig_ussd_no_wait
(
  void *                          sp,
  void *                          cmd_buf_p,
  void *                          cl_sp,
  dsm_item_type **                sdu_in
)
{
  dsm_item_type *                 response;
  qmi_error_e_type                errval;
  qmi_result_e_type               result;
  boolean                         retval;

  uint8                           type;
  uint16                          len;
  void *                          value;
  boolean                         got_v_in_required;
  qmi_voice_cmd_list_public_type  info_ptr;
  qmi_mmode_cmd_data_buf_type *         cmd_ptr = NULL;

  struct 
  {
    uint8                         uss_dcs;
    uint8                         uss_data_len;
    uint8                         uss_data[QMI_VOICE_CM_IF_MAX_USS_DATA_LEN];
  } uusd_data = {0, 0, {0}};
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_orig_ussd_no_wait");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response                        = NULL;
  errval                          = QMI_ERR_NONE;
  value                           = NULL;
  got_v_in_required               = FALSE;
  memset(&info_ptr, 0, sizeof(info_ptr));

  while (*sdu_in)
  {
    if (!qmi_svc_get_tl(sdu_in, &type, &len))
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        value = &uusd_data;
        if (len > (sizeof(uusd_data.uss_dcs) + 
                   sizeof(uusd_data.uss_data_len) + 
                   sizeof(uusd_data.uss_data)))
          {
          QM_MSG_MED_1("Invalid UUS DATA %d", len);
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if (len != dsm_pullup(sdu_in, value, len))
    {
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }                     

  if (!got_v_in_required)
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  /* Check the incoming UUSD parameters. */
  if ((uusd_data.uss_data_len == 0) ||
      (uusd_data.uss_data_len >= QMI_VOICE_CM_IF_MAX_USS_DATA_LEN) ||
      (uusd_data.uss_dcs < QMI_VOICE_CM_IF_USSD_DCS_ASCII) ||
      (uusd_data.uss_dcs > QMI_VOICE_CM_IF_USSD_DCS_UCS2))
  {
    QM_MSG_HIGH("Received ORIG_USSD_NO_WAIT with Invalid Data");
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }


  QM_MSG_MED_2("ORIG_USSD_NO_WAIT string format = %d, length = %d", uusd_data.uss_dcs, uusd_data.uss_data_len);

  if (NULL == (cmd_ptr = qmi_mmode_get_cmd_data_buf()))
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }
  
  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  /* Check to see if a USSD request is in progress. */
  if (qmi_voice_cmd_list_query_by_event(QMI_VOICE_CM_IF_EVT_SUPS_USS_IND,
                                        &info_ptr,cmd_ptr->cmd.cm_if_data_ptr->header.as_id) == E_SUCCESS)
  {
    errval = QMI_ERR_INCOMPATIBLE_STATE;
    goto send_result;
  }


  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_ORIG_USSD_NO_WAIT;
  /* The handle is the cmd_buf to complete for the blocking SUPS ORIG
     command. The nonblocking command will store the client state pointer
     to use to send the indication later.*/

  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)client_sp;
  cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_no_wait_info.uss_dcs = uusd_data.uss_dcs;
  cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_no_wait_info.uss_data_len = uusd_data.uss_data_len;
  /* QMI_Security check*/
  if(uusd_data.uss_data_len > QMI_VOICE_CM_IF_MAX_USS_DATA_LEN)
  {
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }
  memscpy(cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_no_wait_info.uss_data, sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.ussd_orig_no_wait_info.uss_data),
          uusd_data.uss_data,
          uusd_data.uss_data_len);

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

send_result:
  if (errval == QMI_ERR_NONE)
  {
    result = QMI_RESULT_SUCCESS;
  }
  else
  {
    result = QMI_RESULT_FAILURE;
    if(cmd_ptr != NULL)
    {
      if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
      {
        QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
      }
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
    }
  }

  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
} /* qmi_voicei_sups_orig_ussd_no_wait() */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_ORIG_USSD_NO_WAIT_IND()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_orig_ussd_no_wait_ind
(
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  qmi_voicei_client_state_type *  cl_sp;
  dsm_item_type *                 ind;
  qmi_error_e_type                errval;
  uint16                           total_alpha_len = 0;
  uint16                           total_ussd_len = 0;
  uint8 tag = VOICEI_ORIG_USSD_NO_WAIT_DATA_UTF16;

  struct
  {
    qmi_voice_cm_if_ussd_info_utf16_s  ussd_info_utf16;    
  } ussd_no_wait_resp_utf16;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_orig_ussd_no_wait_ind");

  /* Pull the client state pointer from the handle. For no_wait_ind, the
     handle is the client state and not the cmd_buf. */
  cl_sp = (qmi_voicei_client_state_type *)data_ptr->header.handle;

  /* Verify that the client state has not been freed. */
  if ((NULL == cl_sp) || (QMI_SVC_CLID_UNUSED == cl_sp->common.clid))
  {
    QM_MSG_MED("QMI VOICE invalid client state pointer");
    return;
  }
  else if(!cl_sp->voice_info.reg_ussd_notification_events)
  {
    QM_MSG_MED("QMI VOICE, client not registed for orig_ussd_no_wait_ind");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  ind = NULL;
  errval = data_ptr->data.ussd_orig_no_wait_ind.error;

  /* Fill alpha which resulted from SIM call control */
  if ((QMI_ERR_NONE == errval) && (data_ptr->data.ussd_orig_no_wait_ind.is_alpha_available))
  {
    total_alpha_len = sizeof(data_ptr->data.ussd_orig_no_wait_ind.alpha_id.alpha_dcs) + 
                      sizeof(data_ptr->data.ussd_orig_no_wait_ind.alpha_id.alpha_len) + 
                      data_ptr->data.ussd_orig_no_wait_ind.alpha_id.alpha_len;
    QM_MSG_MED_2("Sending QMI orig alpha id with dcs = %d, len = %d ", data_ptr->data.ussd_orig_no_wait_ind.alpha_id.alpha_dcs, data_ptr->data.ussd_orig_no_wait_ind.alpha_id.alpha_len);
    if(FALSE == qmi_svc_put_param_tlv(&ind,
                                      VOICEI_ORIG_USSD_NO_WAIT_ALPHA_ID,
                                      total_alpha_len,
                                      (void *)&(data_ptr->data.ussd_orig_no_wait_ind.alpha_id)))
    {
      QM_MSG_ERROR("QMI VOICE unable to add alpha id TLV");
      dsm_free_packet(&ind);
      return;
    }
  }

  /* Add the USS Data TLV to the indication. */
  if ((QMI_ERR_NONE == errval) && (data_ptr->data.ussd_orig_no_wait_ind.is_ussd_data_available))
  {
    total_ussd_len = sizeof(data_ptr->data.ussd_orig_no_wait_ind.ussd_data.uss_dcs) + 
                     sizeof(data_ptr->data.ussd_orig_no_wait_ind.ussd_data.uss_data_len) + 
                     data_ptr->data.ussd_orig_no_wait_ind.ussd_data.uss_data_len;
    QM_MSG_MED_2("Sending ussd data with dcs = %d, len = %d ", data_ptr->data.ussd_orig_no_wait_ind.ussd_data.uss_dcs, data_ptr->data.ussd_orig_no_wait_ind.ussd_data.uss_data_len);
    if(FALSE == qmi_svc_put_param_tlv(&ind,
                                      VOICEI_ORIG_USSD_NO_WAIT_DATA,
                                      total_ussd_len,
                                      (void *)&(data_ptr->data.ussd_orig_no_wait_ind.ussd_data)))
    {
      QM_MSG_ERROR("QMI VOICE unable to add uss data TLV");
      dsm_free_packet(&ind);
      return;
    }
  }

  total_ussd_len =0;
  if((QMI_ERR_NONE == errval) && (data_ptr->data.ussd_orig_no_wait_ind.is_ussd_info_utf16_available == TRUE))
  {
    QM_MSG_MED_1("No wait ussd ind with no of chars in UCS2 = %d ", data_ptr->data.ussd_orig_no_wait_ind.ussd_info_utf16.ussd_data_utf16_len);
    memset(&ussd_no_wait_resp_utf16,0,sizeof(ussd_no_wait_resp_utf16));
    ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len = data_ptr->data.ussd_orig_no_wait_ind.ussd_info_utf16.ussd_data_utf16_len;    
    /* QMI_Security check*/
    if(ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len > QMI_VOICE_CM_IF_MAX_USS_DATA_LEN)
    {
      ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len = QMI_VOICE_CM_IF_MAX_USS_DATA_LEN;
    }
    memscpy(ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16,
            sizeof(ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16),
            data_ptr->data.ussd_orig_no_wait_ind.ussd_info_utf16.ussd_data_utf16,
            (ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)*2);

    total_ussd_len = sizeof(data_ptr->data.ussd_orig_no_wait_ind.ussd_info_utf16.ussd_data_utf16_len) + \
                            (data_ptr->data.ussd_orig_no_wait_ind.ussd_info_utf16.ussd_data_utf16_len)*2;

    if( !(QMI_SVC_PKT_PUSH(&ind, (void*)ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16, (ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)*2) &&
             QMI_SVC_PKT_PUSH(&ind, (void*)&ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len, sizeof(ussd_no_wait_resp_utf16.ussd_info_utf16.ussd_data_utf16_len)) &&
             QMI_SVC_PKT_PUSH(&ind, (void *)&total_ussd_len, VOICEI_TLV_LENGTH_SIZE) &&
             QMI_SVC_PKT_PUSH(&ind, (void *)&tag, VOICEI_TLV_TAG_SIZE)) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&ind);
    }    
  }


  /* Add the failure cause TLV to the indication. */
  if (QMI_ERR_SUPS_FAILURE_CAUSE == errval)
  {
    if(FALSE == qmi_svc_put_param_tlv(&ind,
                                      VOICEI_ORIG_USSD_NO_WAIT_FAILURE_CAUSE,
                                      sizeof(data_ptr->data.ussd_orig_no_wait_ind.sups_failure_cause),
                                      (void *) &(data_ptr->data.ussd_orig_no_wait_ind.sups_failure_cause)))
    {
      QM_MSG_ERROR("QMI VOICE unable to add failure cause TLV");
      dsm_free_packet(&ind);
      return;
    }
  }

  /* Add the QMI error to the indication. */
  if (QMI_ERR_NONE != errval)
  {
    if (FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_ORIG_USSD_NO_WAIT_ERROR,
                                       sizeof(errval), 
                                       (void*)&errval))
    {
      QM_MSG_ERROR("QMI VOICE unable to add QMI error TLV");
      dsm_free_packet(&ind);
      return;
    }
  }

  if( !qmi_voicei_send_indication(cl_sp,
                         VOICEI_CMD_VAL_SUPS_ORIG_USSD_NO_WAIT_IND,
                               ind))
  {
    QM_MSG_MED("VOICE USSD NO WAIT Indication could not be sent");
  }
}/*qmi_voicei_sups_orig_ussd_no_wait_ind*/

/*===========================================================================
  FUNCTION qmi_voicei_call_modified_ind()
  
  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_call_modified_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_modified_ind_s *modify_ptr;
  uint16 failure_cause;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  modify_ptr = (qmi_voice_cm_if_modified_ind_s*)&data_ptr->data;

  QM_MSG_MED_2("Modified_IND:  call_id=%d,type=%d", modify_ptr->call_id, modify_ptr->call_type);
  QM_MSG_MED_2("Modified_IND:  audio_attrib=%d,Video_attrib=%d", modify_ptr->audio_attrib, modify_ptr->video_attrib);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_modification_events) &&     
       (!cl_sp->asubs_info.is_bind_set_by_client ||
       (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
      ind = NULL;
    
      if(modify_ptr->is_attrib_status_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_ATTRIB_STATUS,
                                       sizeof(modify_ptr->attrib_status),
                                       (void*)&modify_ptr->attrib_status))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->is_media_id_available)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_MEDIA_ID,
                                       sizeof(modify_ptr->media_id),
                                       (void*)&modify_ptr->media_id))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->is_sups_failure_cause_valid)
      {
        failure_cause = (uint16) modify_ptr->sups_failure_cause;
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_FAILURE_CAUSE,
                                       sizeof(failure_cause),
                                       (void*)&failure_cause))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }    

      if(modify_ptr->is_video_attrib_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_VIDEO_ATTRIB,
                                       sizeof(modify_ptr->video_attrib),
                                       (void*)&modify_ptr->video_attrib))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->is_audio_attrib_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_AUDIO_ATTRIB,
                                       sizeof(modify_ptr->audio_attrib),
                                       (void*)&modify_ptr->audio_attrib))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->call_type_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFIED_CALL_TYPE,
                                       sizeof(modify_ptr->call_type),
                                       (void*)&modify_ptr->call_type))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }

        /*Mandatory TLV*/
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_MODIFIED_CALL_ID,
                                         sizeof(modify_ptr->call_id),
                                         (void*)&modify_ptr->call_id))
      {
        dsm_free_packet(&ind);
        continue;
      }
              
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CALL_MODIFIED_IND, ind))
      {
        QM_MSG_MED("VOICE CALL modified Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_call_modified_ind() */

/*===========================================================================
  FUNCTION qmi_voicei_call_modify_accept_ind()
  
  DESCRIPTION 
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_call_modify_accept_ind
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{  
  qmi_voicei_state_type *        voice_sp;
  qmi_voicei_client_state_type * cl_sp;
  int                            j;
  dsm_item_type *                ind;
  qmi_voice_cm_if_modify_accept_ind_s *modify_ptr;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  modify_ptr = (qmi_voice_cm_if_modify_accept_ind_s*)&data_ptr->data;

  QM_MSG_MED_2("Modify Accept IND:  call_id=%d,type=%d", modify_ptr->call_id, modify_ptr->call_type);
  QM_MSG_MED_2("Modify Accept IND:  audio_attrib=%d,Video_attrib=%d", modify_ptr->audio_attrib, modify_ptr->video_attrib);

  voice_sp = &qmi_voice_state;

  for( j = 0; j < VOICEI_MAX_CLIDS; j++ )
  {
    cl_sp = (qmi_voicei_client_state_type *)voice_sp->client[j];
    if((cl_sp != NULL) && (cl_sp->common.clid != QMI_SVC_CLID_UNUSED) &&
       (cl_sp->voice_info.reg_modification_events) &&     
       (!cl_sp->asubs_info.is_bind_set_by_client ||
       (cl_sp->asubs_info.as_id == data_ptr->header.as_id)))
    {
      ind = NULL;
    
      if(modify_ptr->is_video_attrib_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFY_ACCEPT_VIDEO_ATTRIB,
                                       sizeof(modify_ptr->video_attrib),
                                       (void*)&modify_ptr->video_attrib))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->is_audio_attrib_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFY_ACCEPT_AUDIO_ATTRIB,
                                       sizeof(modify_ptr->audio_attrib),
                                       (void*)&modify_ptr->audio_attrib))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }
      if(modify_ptr->call_type_valid)
      {
        if(FALSE == qmi_svc_put_param_tlv(&ind,
                                       VOICEI_MODIFY_ACCEPT_CALL_TYPE,
                                       sizeof(modify_ptr->call_type),
                                       (void*)&modify_ptr->call_type))
        {
          dsm_free_packet(&ind);
          continue;
        }
      }

        /*Mandatory TLV*/
      if(FALSE == qmi_svc_put_param_tlv(&ind,
                                         VOICEI_MODIFY_ACCEPT_CALL_ID,
                                         sizeof(modify_ptr->call_id),
                                         (void*)&modify_ptr->call_id))
      {
        dsm_free_packet(&ind);
        continue;
      }
                
      if( !qmi_voicei_send_indication(cl_sp, VOICEI_CMD_VAL_CALL_MODIFY_ACCEPT_IND, ind))
      {
        QM_MSG_MED("VOICE CALL modified Indication could not be sent");
      }
    }
  }
  return;
} /* qmi_voicei_call_modify_accept_ind() */

/*===========================================================================
  FUNCTION QMI_VOICEI_ALS_SET_LINE_SWITCHING()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_als_set_line_switching
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  uint8              switch_option = 0;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_als_set_line_switching");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

   cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
   client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
   if(NULL == client_sp)
   {
     QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
     errval = QMI_ERR_INVALID_CLIENT_ID;
     goto send_result;
   }

   memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    expected_len =0;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
	expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_line_switch.is_line_switching_allowed);
        value = &switch_option;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if(switch_option > QMI_VOICE_CM_IF_ALS_LINE_SWITCH_ALLOWED)
  {
    QM_MSG_MED_1("QMI Voice:  Invalid Switching option %d", switch_option);
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->data.set_line_switch.is_line_switching_allowed = 
           (switch_option == QMI_VOICE_CM_IF_ALS_LINE_SWITCH_ALLOWED);


  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_ALS_SET_LINE_SWITCHING;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_ALS_SET_LINE_SWITCHING_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_als_set_line_switching_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_als_set_line_switching_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_als_set_line_switching_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.set_line_switch_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE ALS SET LINE SWITCH operation");
  }
}


/*===========================================================================
  FUNCTION QMI_VOICEI_ALS_SELECT_LINE()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_als_select_line
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  uint8              line = 0;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_als_select_line");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));


  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    value = NULL;
    expected_len =0;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.select_line.als_line);
        value = &line;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  if(line > QMI_VOICE_CM_IF_ALS_LINE_2)
  {
    QM_MSG_MED_1("QMI Voice:  Invalid selected line %d", line);
    errval = QMI_ERR_INVALID_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->data.select_line.als_line = line;

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_ALS_SELECT_LINE;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}

/*===========================================================================
  FUNCTION QMI_VOICEI_ALS_SELECT_LINE_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_als_select_line_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_als_select_line_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_als_select_line_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.select_line_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE ALS SELECT LINE operation");
  }

}/* qmi_voicei_als_select_line_resp */

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_COLR()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_colr
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
    dsm_item_type *    response;
    qmi_error_e_type   errval;
    qmi_result_e_type  result;
    boolean            retval;
  
    qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
    QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_colr");
  
    ASSERT(sp);
    ASSERT(cmd_buf_p);
    ASSERT(cl_sp);
    ASSERT(sdu_in);
  
    response          = NULL;
    errval            = QMI_ERR_NONE;
  
  
    cmd_ptr = qmi_mmode_get_cmd_data_buf();
  
    if(NULL == cmd_ptr)
    {
      QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
    if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
    {
      QM_MSG_FATAL("Can't allocate memory");
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
   
   cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
   client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
   if(NULL == client_sp)
   {
     QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
     errval = QMI_ERR_INVALID_CLIENT_ID;
     goto send_result;
   }


    if(*sdu_in != NULL)
    {
      QM_MSG_MED("No TLV data expected");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  
  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_COLR;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);
  
  return QMI_SVC_RESPONSE_PENDING;
  
  send_result:
    result = QMI_RESULT_FAILURE;
    if(cmd_ptr != NULL)
    {
      if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
      {
        QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
      }
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
    }
    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_voicei_sups_get_colr() */



/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_COLR_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_colr_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint8                   total_alpha_len = 0;
  uint16 colr_failure_cause=0;
  struct
  {
    uint8 active_status;
    uint8 provision_status;
  }colr_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_colr_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_colr_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_colr_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_colr_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLR_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_colr_resp.retry_duration),
                                      (void *) &(data_ptr->data.get_colr_resp.retry_duration)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    colr_resp.active_status = data_ptr->data.get_colr_resp.active_status;
    colr_resp.provision_status = data_ptr->data.get_colr_resp.provision_status;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLR_RESP,
                                      sizeof(colr_resp),
                                      (void *) &colr_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    colr_failure_cause = (uint16)data_ptr->data.get_colr_resp.sups_failure_cause;
    QMI_MSG_HIGH ("%s %d\n","QMI GET COLR operation failure cause = ",colr_failure_cause);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    VOICEI_GET_COLR_FAILURE_CAUSE,
                                    sizeof(colr_failure_cause),
                                    (void *) &colr_failure_cause))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.get_colr_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_colr_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_COLR_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_COLR_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("Invalid data call control in response");
    }
    
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLR_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_colr_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_colr_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_colr_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_colr_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_colr_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_colr_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLR_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_colr_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if(data_ptr->data.get_colr_resp.colr_pi_valid)
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_COLR_PI,
                                      sizeof(data_ptr->data.get_colr_resp.colr_pi),
                                      (void *) &(data_ptr->data.get_colr_resp.colr_pi) ) )
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else
  {
    QM_MSG_ERROR("Unable to send PI for QMI VOICE GET COLR operation");
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE GET COLR operation");
  }
}/*qmi_voicei_sups_get_colr_resp*/

/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CNAP()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_sups_get_cnap
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
    dsm_item_type *    response;
    qmi_error_e_type   errval;
    qmi_result_e_type  result;
    boolean            retval;
  
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  
    QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_cnap");
  
    ASSERT(sp);
    ASSERT(cmd_buf_p);
    ASSERT(cl_sp);
    ASSERT(sdu_in);
  
    response          = NULL;
    errval            = QMI_ERR_NONE;
  
  
    cmd_ptr = qmi_mmode_get_cmd_data_buf();
  
    if(NULL == cmd_ptr)
    {
      QM_MSG_ERROR("Out of DCC cmd buf");
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
    cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
    if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
    {
      QM_MSG_FATAL("Can't allocate memory");
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
      errval = QMI_ERR_INTERNAL;
      goto send_result;
    }
   
   cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
   client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
   if(NULL == client_sp)
   {
     QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
     errval = QMI_ERR_INVALID_CLIENT_ID;
     goto send_result;
   }

    if(*sdu_in != NULL)
    {
      QM_MSG_MED("No TLV data expected");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  
    cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_GET_CNAP;
    cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;
    
    if(client_sp->asubs_info.is_bind_set_by_client)
    {
      cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
    }


    qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);
  
    return QMI_SVC_RESPONSE_PENDING;
  
  send_result:
    result = QMI_RESULT_FAILURE;
    if(cmd_ptr != NULL)
    {
      if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
      {
        QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
      }
      qmi_mmode_free_cmd_data_buf(cmd_ptr);
    }
    retval = qmi_svc_put_result_tlv(&response, result, errval);
    CHECK_RETVAL();
    return response;
} /* qmi_voicei_sups_get_cnap() */



/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_GET_CNAP_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_get_cnap_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                   total_alpha_len = 0;
  uint16 				  cnap_failure_cause=0;
  struct
  {
    uint8 active_status;
    uint8 provision_status;
  }cnap_resp;
  struct
  {
    uint8 srv_type;
    uint8 reason;
  }sups_cc_data;
  qmi_voice_cm_if_sups_cc_result_s cc_res;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_cnap_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_get_cnap_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_cnap_resp.error;

  /* Fill Retry Duration info if no error is present */
  if( data_ptr->data.get_cnap_resp.is_retry_duration_available == TRUE )
  {
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CNAP_RETRY_DURATION,
                                      sizeof(data_ptr->data.get_cnap_resp.retry_duration),
                                      (void *) &data_ptr->data.get_cnap_resp.retry_duration))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if( QMI_ERR_NONE == errval )
  {
    cnap_resp.active_status = data_ptr->data.get_cnap_resp.active_status;
    cnap_resp.provision_status = data_ptr->data.get_cnap_resp.provision_status;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CNAP_RESP,
                                      sizeof(cnap_resp),
                                      (void *) &cnap_resp))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( QMI_ERR_SUPS_FAILURE_CAUSE == errval )
  {
    cnap_failure_cause = (uint16)data_ptr->data.get_cnap_resp.sups_failure_cause;
    QMI_MSG_HIGH ("%s %d\n","QMI GET CNAP operation failure cause = ",cnap_failure_cause);
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                    VOICEI_GET_CNAP_FAILURE_CAUSE,
                                    sizeof(cnap_failure_cause),
                                    (void *) &cnap_failure_cause))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  else if( data_ptr->data.get_cnap_resp.is_cc_modified )
  {
    cc_res = data_ptr->data.get_cnap_resp.cc_result;
    if( cc_res.is_mod_to_sups )
    {
      sups_cc_data.srv_type = cc_res.service;
      sups_cc_data.reason = cc_res.reason;
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CNAP_CC_RESULT_SUPS,
                                        sizeof(sups_cc_data),
                                        (void *) &sups_cc_data))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else if( cc_res.is_mod_to_voice )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_GET_CNAP_CC_RESULT_CALL_ID,
                                        sizeof(cc_res.call_id),
                                        (void *) &cc_res.call_id))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    else
    {
      QM_MSG_ERROR("GET_CNAP::  Invalid data call control in response");
    }
    
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CNAP_CC_RESULT_TYPE,
                                      sizeof(data_ptr->data.get_cnap_resp.cc_mod_type),
                                      (void *) &(data_ptr->data.get_cnap_resp.cc_mod_type)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }
  /* Fill alpha which resulted from SIM call control */
  if(data_ptr->data.get_cnap_resp.is_alpha_available)
  {
    total_alpha_len = sizeof(data_ptr->data.get_cnap_resp.alpha_id.alpha_dcs) + \
                      sizeof(data_ptr->data.get_cnap_resp.alpha_id.alpha_len) + \
                      data_ptr->data.get_cnap_resp.alpha_id.alpha_len;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_CNAP_ALPHA_ID,
                                      total_alpha_len,
                                      (void *) &(data_ptr->data.get_cnap_resp.alpha_id)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE SUPS GET CNAP operation");
  }
}/*qmi_voicei_sups_get_cnap_resp*/




/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_MANAGE_IP_CALLS()

  DESCRIPTION
    Manage the call dependant sups for VoIP Calls.
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_sups_manage_ip_calls
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  uint8              type;
  uint16             len;
  uint16             expected_len;
  void *             value;
  boolean            got_v_in_required;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_sups_manage_ip_calls");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  while (*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    // special value 0 = variable length or don't care (unrecognzied TLV)
    expected_len = 0;
    value = NULL;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sups_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sups_type;
        got_v_in_required = TRUE;
        break;

      case VOICEI_MNG_IP_CALLS_CALL_ID:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_id_valid = TRUE;
        break;

      case VOICEI_MNG_IP_CALLS_CALL_TYPE:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_type);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_type;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.call_type_valid = TRUE;
        break;

      case VOICEI_MNG_IP_CALLS_AUDIO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.audio_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.audio_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.is_audio_attrib_valid= TRUE;
        break;

      case VOICEI_MNG_IP_CALLS_VIDEO_ATTRIB:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.video_attrib))
        {
          errval = QMI_ERR_ARG_TOO_LONG;
          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.video_attrib;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.is_video_attrib_valid= TRUE;
        break;         

      case VOICEI_MNG_IP_CALLS_SIP_URI:        
        value = cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_uri;
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_uri))
        {
          QM_MSG_MED("Invalid SIP URI length");
          errval = QMI_ERR_ARG_TOO_LONG;
          
          goto send_result;
        }
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_uri_len =(uint8)len;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.is_sip_uri_valid = TRUE;
        break;
      case VOICEI_MNG_IP_CALLS_SIP_REJECT_CAUSE:                
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause))
        {
          QM_MSG_MED("Invalid Reject cause length");
          errval = QMI_ERR_ARG_TOO_LONG;

          goto send_result;
        }        
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.reject_cause_valid = TRUE;
        break;
      case VOICEI_MNG_IP_CALLS_SIP_REJECT_CODE:                
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_reject_code))
        {
          QM_MSG_MED("Invalid SIP Reject Code length");
          errval = QMI_ERR_ARG_TOO_LONG;
          
          goto send_result;
        }        
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_reject_code;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.sip_reject_code_valid = TRUE;
        break;
      case VOICEI_MNG_IP_CALLS_SPEECH_CODEC:
        if(len > sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.speech_codec))
        {
          QM_MSG_MED("Invalid Speech Codec length");
          errval = QMI_ERR_ARG_TOO_LONG;

          goto send_result;
        }
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.speech_codec;
        cmd_ptr->cmd.cm_if_data_ptr->data.manage_calls_info.is_speech_codec_valid = TRUE;
        break;

      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_SUPS_MANAGE_IP_CALLS;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;

  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_sups_manage_ip_calls() */


/*===========================================================================
  FUNCTION QMI_VOICEI_SUPS_MANAGE_IP_CALLS_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_sups_manage_ip_calls_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint16                  failure_cause;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_manage_ip_calls_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_sups_manage_ip_calls_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.mng_ip_calls_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    
    if( data_ptr->data.mng_ip_calls_resp.is_call_id_valid )
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_MNG_IP_CALLS_CALL_ID,
                                        sizeof(data_ptr->data.mng_ip_calls_resp.call_id),
                                        (void *) &(data_ptr->data.mng_ip_calls_resp.call_id)))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
  }

  if( QMI_ERR_SUPS_FAILURE_CAUSE == errval &&
      data_ptr->data.mng_ip_calls_resp.is_failure_cause_valid)
  {
    failure_cause = (uint16) data_ptr->data.mng_ip_calls_resp.sups_failure_cause;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_MNG_IP_CALLS_RESP_FAILURE_CAUSE,
                                      sizeof(failure_cause),
                                      (void *) &(failure_cause)))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE MNG IP CALLS operation");
  }
} /* qmi_voicei_sups_manage_ip_calls_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_LINE_SWITCHING_STATUS()

  DESCRIPTION
  
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_als_get_line_switching_status
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_get_line_switching_status");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;  


  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_ALS_GET_LINE_SWITCHING_STATUS;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_get_line_switching_status() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_LINE_SWITCHING_STATUS_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_als_get_line_switching_status_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  boolean                    switching_allowed=0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_sups_get_clir_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_get_line_switching_status_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_line_switch_status_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    switching_allowed = data_ptr->data.get_line_switch_status_resp.switch_allowed;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_ALS_LINE_SWITCH_STATUS,
                                      sizeof(switching_allowed),
                                      (void *) &switching_allowed))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI GET LINE SWITCHING STATUS operation");
  }
  
} /* qmi_voicei_get_line_switching_status_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_SELECTED_LINE()

  DESCRIPTION
  
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type* qmi_voicei_als_get_selected_line
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;

  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;
  
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_get_selected_line");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;  

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  // setting everything to 0 also handles the case when optional TLV is not
  // present because optional TLV call_type by default is 0 = VOICE
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  } 
  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_ALS_GET_SELECTED_LINE;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = QMI_RESULT_FAILURE;
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;

} /* qmi_voicei_get_line_switching_status() */


/*===========================================================================
  FUNCTION QMI_VOICEI_GET_SELECTED_LINE_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_als_get_selected_line_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;
  uint8                   line_value=0;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_als_get_selected_line_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_get_line_switching_status_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  response = NULL;
  errval = data_ptr->data.get_line_switch_status_resp.error;

  if( QMI_ERR_NONE == errval )
  {
    line_value = data_ptr->data.get_selected_line_resp.line_value;
    if(FALSE == qmi_svc_put_param_tlv(&response,
                                      VOICEI_GET_ALS_SELECTED_LINE,
                                      sizeof(line_value),
                                      (void *) &line_value))
    {
      errval = QMI_ERR_NO_MEMORY;
      dsm_free_packet(&response);
    }
  }

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI GET SELECTED LINE operation");
  }
  
} /* qmi_voicei_get_selected_line() */


/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_RESET_ACM()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_aoc_reset_acm
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type *cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_reset_acm");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();
  

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  if(*sdu_in != NULL)
  {
    QM_MSG_MED("No TLV data expected");
    errval = QMI_ERR_MALFORMED_MSG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_AOC_RESET_ACM;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}/* qmi_voicei_aoc_reset_acm() */

/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_RESET_ACM_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_aoc_reset_acm_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_reset_acm_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_aoc_reset_acm_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.reset_acm_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE AOC RESET ACM operation");
  }
}/* qmi_voicei_aoc_reset_acm_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_SET_ACMMAX()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_aoc_set_acmmax
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  uint8                          type;
  uint16                         len;
  uint16                         expected_len;
  void *                         value;
  boolean                        got_v_in_required;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type * cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_set_acmmax");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();

  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  while(*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    value = NULL;
    expected_len =0;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.set_acmmax.new_max);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.set_acmmax.new_max;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }

    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_AOC_SET_ACMMAX;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}/* qmi_voicei_aoc_set_acmmax() */

/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_SET_ACMMAX_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_aoc_set_acmmax_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_set_acmmax_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_aoc_set_acmmax_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.set_acmmax_resp.error;
  response = NULL;

  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE AOC SET ACMMAX operation");
  }
}/* qmi_voicei_aoc_set_acmmax_resp() */


/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_GET_CALL_MATER_INFO()

  DESCRIPTION
    
    
  PARAMETERS
    sp        : service provided state pointer
    cmd_buf_p : ptr to cmd buffer
    cl_sp     : client state pointer
    sdu_in    : incoming request

  RETURN VALUE
    ptr to response or NULL if none.
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static dsm_item_type * qmi_voicei_aoc_get_call_meter_info
(
  void *            sp,
  void *            cmd_buf_p,
  void *            cl_sp,
  dsm_item_type **  sdu_in
)
{
  dsm_item_type *    response;
  qmi_error_e_type   errval;
  qmi_result_e_type  result;
  boolean            retval;
  
  qmi_mmode_cmd_data_buf_type *  cmd_ptr;
  uint8                          type;
  uint16                         len;
  uint16                         expected_len;
  void *                         value;
  boolean                        got_v_in_required;
  qmi_voicei_client_state_type * client_sp;
  qmi_common_client_state_type * cmn_cl_sp;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_get_call_meter_info");

  ASSERT(sp);
  ASSERT(cmd_buf_p);
  ASSERT(cl_sp);
  ASSERT(sdu_in);

  response          = NULL;
  errval            = QMI_ERR_NONE;
  value             = NULL;
  got_v_in_required = FALSE;

  cmd_ptr = qmi_mmode_get_cmd_data_buf();
  
  if(NULL == cmd_ptr)
  {
    QM_MSG_ERROR("Out of QMI_MMODE cmd buf");
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }
  
  cmd_ptr->cmd.cm_if_data_ptr = QMI_VOICE_CM_IF_MEM_ALLOC(sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));
  if( NULL == cmd_ptr->cmd.cm_if_data_ptr )
  {
    QM_MSG_FATAL("Can't allocate memory");
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
    errval = QMI_ERR_INTERNAL;
    goto send_result;
  }

  cmn_cl_sp = (qmi_common_client_state_type *)cl_sp;
  client_sp = (qmi_voicei_client_state_type *) qmi_voicei_get_client_sp_by_clid(cmn_cl_sp->clid);
  if(NULL == client_sp)
  {
    QM_MSG_ERROR_1("Invalid client with id %d", cmn_cl_sp->clid);
    errval = QMI_ERR_INVALID_CLIENT_ID;
    goto send_result;
  }

  memset(cmd_ptr->cmd.cm_if_data_ptr, 0, sizeof(*(cmd_ptr->cmd.cm_if_data_ptr)));

  while(*sdu_in)
  {
    if( !qmi_svc_get_tl(sdu_in, &type, &len) )
    {
      continue;
    }

    value = NULL;
    expected_len =0;
    switch (type)
    {
      case QMI_TYPE_REQUIRED_PARAMETERS:
        expected_len = sizeof(cmd_ptr->cmd.cm_if_data_ptr->data.get_call_meter_info.info_mask);
        value = &cmd_ptr->cmd.cm_if_data_ptr->data.get_call_meter_info.info_mask;
        got_v_in_required = TRUE;
        break;
      default:
        QM_MSG_ERROR_1("Unrecognized TLV type (%d)", type);
        break;
    }
    
    if ( (expected_len != 0) && (expected_len != len) )
    {
      QM_MSG_MED_2("Invalid TLV len (%d) for type (%d)", len, type);
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }

    if ( len != dsm_pullup( sdu_in, value, len ) )
    {                                                                    
      QM_MSG_MED("Invalid length in TLV");
      errval = QMI_ERR_MALFORMED_MSG;
      goto send_result;
    }
  }

  if ( !got_v_in_required )
  {
    errval = QMI_ERR_MISSING_ARG;
    goto send_result;
  }

  cmd_ptr->cmd.cm_if_data_ptr->header.cmd = QMI_VOICE_CM_IF_CMD_AOC_GET_CALL_METER_INFO;
  cmd_ptr->cmd.cm_if_data_ptr->header.handle = (uint32)cmd_buf_p;
  cmd_ptr->cmd.cm_if_data_ptr->header.as_id = QMI_VOICE_CM_IF_AS_ID_PRIMARY;

  if(client_sp->asubs_info.is_bind_set_by_client)
  {
    cmd_ptr->cmd.cm_if_data_ptr->header.as_id = client_sp->asubs_info.as_id;
  }

  qmi_mmode_send_cmd(QMI_MMODE_CMD_VOICE_CM_IF_CB, cmd_ptr);

  return QMI_SVC_RESPONSE_PENDING;

send_result:
  result = (errval == QMI_ERR_NONE ? QMI_RESULT_SUCCESS 
                                   : QMI_RESULT_FAILURE);
  if(cmd_ptr != NULL)
  {
    if(cmd_ptr->cmd.cm_if_data_ptr != NULL)
    {
      QMI_VOICE_CM_IF_MEM_FREE(cmd_ptr->cmd.cm_if_data_ptr);
    }
    qmi_mmode_free_cmd_data_buf(cmd_ptr);
  }
  retval = qmi_svc_put_result_tlv(&response, result, errval);
  CHECK_RETVAL();
  return response;
}/* qmi_voicei_aoc_get_call_meter_info() */

/*===========================================================================
  FUNCTION QMI_VOICEI_AOC_GET_CALL_MATER_INFO_RESP()

  DESCRIPTION
    
    
  PARAMETERS

  RETURN VALUE
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
static void qmi_voicei_aoc_get_call_meter_info_resp
( 
  const qmi_voice_cm_if_info_item_s *const data_ptr
)
{
  qmi_cmd_buf_type *      cmd_buf_p;
  dsm_item_type *         response;
  qmi_error_e_type        errval;

  uint32                  acm;
  uint32                  acmmax;
  uint32                  ccm;

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
  QM_MSG_MED("QMI VOICE qmi_voicei_aoc_get_call_meter_info_resp");

  cmd_buf_p = (qmi_cmd_buf_type *) data_ptr->header.handle;

  if(cmd_buf_p == NULL)
  {
    QM_MSG_HIGH("Null cmd_buf ptr in qmi_voicei_aoc_get_call_meter_info_resp");
    return;
  }

  /*-------------------------------------------------------------------------
    Build the response from back to front
  -------------------------------------------------------------------------*/
  errval = data_ptr->data.set_acmmax_resp.error;
  response = NULL;

  if( QMI_ERR_NONE == errval )
  {
    acm = data_ptr->data.get_call_meter_info_resp.acm;
    acmmax = data_ptr->data.get_call_meter_info_resp.acmmax;
    ccm = data_ptr->data.get_call_meter_info_resp.ccm;
    if(data_ptr->data.get_call_meter_info_resp.is_acm_available)
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_AOC_GET_CALL_METER_INFO_ACM,
                                        sizeof(acm),
                                        (void *) &acm))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.get_call_meter_info_resp.is_acmmax_available)
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_AOC_GET_CALL_METER_INFO_ACMMAX,
                                        sizeof(acmmax),
                                        (void *) &acmmax))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
    if(data_ptr->data.get_call_meter_info_resp.is_ccm_available)
    {
      if(FALSE == qmi_svc_put_param_tlv(&response,
                                        VOICEI_AOC_GET_CALL_METER_INFO_CCM,
                                        sizeof(ccm),
                                        (void *) &ccm))
      {
        errval = QMI_ERR_NO_MEMORY;
        dsm_free_packet(&response);
      }
    }
  }
  
  if (!qmi_voicei_send_response(errval, (void*)cmd_buf_p, response))
  {
    QM_MSG_MED("Unable to send response for QMI VOICE AOC GET CALL METER INFO operation");
  }
}/* qmi_voicei_aoc_get_call_meter_info_resp() */



#endif/* !FEATURE_NO_QMI_VOICE*/

#endif // FEATURE_QMI_SERVICE_SPLIT



