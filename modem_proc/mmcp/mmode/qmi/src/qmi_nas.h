#ifndef _DS_QMI_NAS_H
#define _DS_QMI_NAS_H
/*===========================================================================

                         D S _ Q M I _ N A S . H

DESCRIPTION

 The Data Services Qualcomm Wireless Data Services MSM Interface header file.

EXTERNALIZED FUNCTIONS

  qmi_nas_init()
    Register the Wireless Data Service with QMUX for all applicable QMI links.
     
  qmi_nas_gen_serving_system_ind_cb()  
    Call back function called when the servign system changes.
    
  qmi_nas_rssi_change_cb()                             
    Callback to be called when signal strength changes.  

  
Copyright (c) 2004-2009 Qualcomm Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary.
===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/mmode/qmi/src/qmi_nas.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
08/10/11    sm     Moving qmi_nas.h to modem/api/mmode directory
===========================================================================*/
#include "modem_mem.h"
#include "mmgsdilib_common.h"
#include "sys.h"

#define QMI_NAS_MEM_ALLOC(siz)  modem_mem_alloc(siz, MODEM_MEM_CLIENT_QMI_MMODE)
#define QMI_NAS_MEM_FREE(ptr)  modem_mem_free(ptr, MODEM_MEM_CLIENT_QMI_MMODE)

/*===========================================================================
  FUNCTION QMI_NAS_PROCESS_MMGSDI_OPERATION_COMPLETE()

  DESCRIPTION
    The demultiplexing routine within NAS subsystem to send out pending 
    responses based on MMGSDI confirmations.
    
  PARAMETERS
    cnf_type : Type of MMGSDI confirmation
    cnf_data : Response data

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void qmi_nas_process_mmgsdi_operation_complete
(
  mmgsdi_cnf_enum_type    cnf_type,
  mmgsdi_cnf_type        *cnf_data
);

/*===========================================================================
  FUNCTION QMI_NAS_GENERATE_NET_SCAN_RESP()

  DESCRIPTION
    Used to send the network scan response to the control point when CM calls
    qmi_nas_network_scan_cb() with async response
    
  PARAMETERS
    available_networks : List of networks resulting from the network scan

  RETURN VALUE
    ptr to response
    
  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
extern void qmi_nas_generate_net_scan_resp
(
  sys_detailed_plmn_list_s_type  available_networks
);

/*=============================================================================
FUNCTION: QMI_NAS_GET_HOME_MCC_MNC

DESCRIPTION: Reads the MCC and MNC from SIM.

PARAMETERS:
  None

RETURN VALUE:
  None

DEPENDENCIES:
  None

SIDE EFFECTS:
  None
=============================================================================*/
void qmi_nas_get_home_mcc_mnc 
(
  void
);

#endif /* _DS_QMI_NAS_H */

