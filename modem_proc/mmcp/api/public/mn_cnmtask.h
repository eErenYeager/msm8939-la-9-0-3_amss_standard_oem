#ifndef mn_cnmtask_h
#define mn_cnmtask_h
/*===========================================================================
                       COPYRIGHT INFORMATION

Copyright (c) 2001 Qualcomm Technologies, Incorporated and its licensors.  All Rights 
Reserved.  QUALCOMM Proprietary.  Export of this technology or software 
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                       EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/cnm/vcs/mn_cnmtask.h_v   1.12   22 Apr 2002 09:48:00   cdealy  $   
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/api/public/mn_cnmtask.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/
extern void mn_cnm_main(dword argc);
#endif
