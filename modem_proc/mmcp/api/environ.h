#ifndef environ_h
#define environ_h

/*===========================================================================
                    COPYRIGHT INFORMATION

Copyright (c) 2001 Qualcomm Technologies, Incorporated and its licensors.  All Rights 
Reserved.  QUALCOMM Proprietary.  Export of this technology or software 
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                    EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/nas/vcs/environ.h_v   1.2   19 Mar 2001 17:43:02   tschwarz  $   
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/api/environ.h#1 $    $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------



===========================================================================*/

/*
 * general definitions
 */

/*
 * #pragma nocustack
 */


typedef void VOID_DATA;              /* Compiler.h */

typedef void VOID_FUNC;              /* Compiler.h */


typedef unsigned char  UBYTE;        /* Compiler.h */

typedef unsigned short UWORD;        /* Compiler.h */

typedef unsigned long  ULONG;        /* Compiler.h */

#endif

