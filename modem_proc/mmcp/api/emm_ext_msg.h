/*===========================================================================

  Copyright (c) 2010 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/api/emm_ext_msg.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/03/2011 sar     moved to API folder and removed the ifdef
===========================================================================*/
#ifndef _EMM_EXT_MSG_H_
#define _EMM_EXT_MSG_H_

#include <customer.h>

#include "comdef.h"
#include "msgr_umid.h"
#include "msgr_types.h"
#include "msgr_lte.h"
#include "msgr_nas.h"
#include "sys.h"

/*===========================================================================

                        MACROS DEFINITION

===========================================================================*/


/*===========================================================================

                        TYPES DEFINITION

===========================================================================*/
enum
{
  MSGR_DEFINE_UMID(NAS, EMM, IND, PLMN_CHANGE, 0x05, emm_plmn_change_ind_type),
  MSGR_DEFINE_UMID(NAS, EMM, IND,ATTACH_COMPLETE, 0x06, emm_attach_complete_ind_type),
  MSGR_DEFINE_UMID(NAS, EMM, IND, T3402_CHANGED, 0x0C, emm_t3402_changed_ind_s_type),
  MSGR_DEFINE_UMID(NAS, EMM, IND, DEACT_NON_EMC_BEARER, 0x0D, emm_deact_non_emc_bearer_ind_s_type),
  MSGR_DEFINE_UMID(NAS, EMM, IND, RESET_APN_SWITCH, 0x0E, emm_reset_apn_switch_ind_type),
  MSGR_DEFINE_UMID(NAS, EMM, IND, TAU_COMPLETE, 0x0B, emm_tau_complete_ind_type)

};

/* PLMN change ind */
typedef struct
{
  /* Message router header */
  msgr_hdr_s          msg_hdr;  

  /* New PLMN */
  sys_plmn_id_s_type  plmn;

}emm_plmn_change_ind_type;

/*APN reset ind*/
typedef struct
{
  /* Message router header */
  msgr_hdr_s          msg_hdr;  
}emm_reset_apn_switch_ind_type;



typedef struct
{
  /* Message router header */
  msgr_hdr_s          msg_hdr; 

}emm_attach_complete_ind_type;

typedef struct
{
  /* Message router header */
  msgr_hdr_s                    msg_hdr; 
  sys_plmn_id_s_type            plmn;
  sys_lac_type                  tac;
}emm_tau_complete_ind_type;


/* T3402 change ind */
typedef struct emm_t3402_changed_ind_s
{
  /* Message router header */
  msgr_hdr_s  msg_hdr;

  /* T3402 value */  
  dword  t3402_value;

} emm_t3402_changed_ind_s_type;

/* Deactivate non emergency bearer ind */
typedef struct emm_deact_non_emc_bearer_ind_s
{
  /* Message router header */
  msgr_hdr_s  msg_hdr;

} emm_deact_non_emc_bearer_ind_s_type;

#endif /* _EMM_EXT_MSG_H_ */



