/**
  @file policyman_cm.h

  @brief Interface to Policy Manager for Call Manager APIs
*/

/*
  Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/api/policyman_cm.h#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------

===========================================================================*/

#ifndef _POLICYMAN_CM_H_
#define _POLICYMAN_CM_H_

#include "policyman_msg.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "comdef.h"
#include <sys/types.h>
#include "sys.h"
#include "sd.h"
#include "cm.h"

#ifdef __cplusplus
}
#endif


/**  Database
*/

typedef struct
{
  sys_modem_stack_id_e_type stack;        /* Stack id in operation */
  sys_modem_as_id_e_type  asubs_id;       /* Subscription id*/
  sys_sys_id_type_e_type  id_type;        /* PLMN id type */
  sys_plmn_id_s_type      plmn;           /* PLMN of the serving system */
  sys_srv_domain_e_type   srv_domain;     /* Service Domain */
  sys_sys_mode_e_type     sys_mode;     /* System's mode */
  sys_srv_status_e_type   srv_status;   /* Service Status */  
} policyman_cm_serving_info_t;



/*-------- policyman_cm_serving_info_cb --------*/
/**
@brief Get the serving system information from Call Manager.

@param[in]  pCmServingInfo    Pointer to CM serving system information.

@return
  None

*/

void
policyman_cm_serving_info_cb(
  policyman_cm_serving_info_t *pCmServingInfo
  );


/*-------- policyman_report_msg_free --------*/
/**
@brief  Frees the MSGR message returned by policyman APIs.

@param[in]  pMsg  Pointer to the message returned by
            policyman APIs.

@return
  None.
*/
void
policyman_report_msg_free(
  msgr_hdr_s  *pMsg
  );


/*-------- policyman_report_preference_change --------*/
/**
@brief  CM calls this interface to report a preference change.

@param[in]  asubs_id  Subscription ID whose preference is changing.
@param[in]  pPref     Pointer to an allocated cm_sys_sel_pref_params_s_type
                      structure.  Policyman will take ownership of this memory
                      after this function is called - CM should not free it.

@return
  This function returns a pointer to a MSGR message identical to that received
  by a client of the POLICYMAN_CFG_UPDATE_IND message.  Handling of this is
  identical to that of the POLICYMAN_CFG_UPDATE_IND message, except that after
  releasing all attachments the caller of this API should free it with
  modem_mem_free(), rather than msgr_rex_free_msg_buf(), as this has not passed
  through MSGR.
*/
msgr_hdr_s *
policyman_report_preference_change(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  );

/*-------- policyman_compute_policy_with_newpref --------*/
/**
@brief  CM calls this interface to know if there would be a change in current policy 
           with the preference set passed in.

@param[in]  asubs_id  Subscription ID whose preference is changing.
@param[in]  pPref     Pointer to an allocated cm_sys_sel_pref_params_s_type
                      structure.

@return
  This function returns a pointer to a MSGR message identical to that received
  by a client of the POLICYMAN_CFG_UPDATE_IND message.  Handling of this is
  identical to that of the POLICYMAN_CFG_UPDATE_IND message, except that after
  releasing all attachments the caller of this API should free it with
  modem_mem_free(), rather than msgr_rex_free_msg_buf(), as this has not passed
  through MSGR.
*/
msgr_hdr_s *  
policyman_compute_policy_with_newpref(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  );


/*-------- policyman_report_preference_msg_free --------*/
/**
@brief  Frees the MSGR message returned by policyman_report_preference_change().

@param[in]  pMsg  Pointer to the message returned by
            policyman_report_preference_change().

@return
  None.
*/
void
policyman_report_preference_msg_free(
  msgr_hdr_s  *pMsg
  );

/*-------- policyman_report_multimode_subs --------*/
/**
@brief  CM calls this interface to report a change in number of subs.

@param[in]  active_subs_mask   Mask of active subscriptions

@return
  This function returns a pointer to a MSGR message identical to that received
  by a client of the POLICYMAN_CFG_UPDATE_IND message.  Handling of this is
  identical to that of the POLICYMAN_CFG_UPDATE_IND message, except that after
  releasing all attachments the caller of this API should free it with
  modem_mem_free(), rather than msgr_rex_free_msg_buf(), as this has not passed
  through MSGR.
*/
msgr_hdr_s *
policyman_report_multimode_subs(
  uint8 active_subs_mask
  );

#endif

