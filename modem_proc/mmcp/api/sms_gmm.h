#ifndef SMS_GMM_H
#define SMS_GMM_H

/*===========================================================================
                        S M S - G M M  Header File

DESCRIPTION

   Definition of primitive message types between the Short Message Entity
   and the GPRS Mobility Management Entity (SMS <-> GMM)

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2001, 2002, 2003 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.

===========================================================================*/
 
/*===========================================================================

            EDIT HISTORY FOR FILE

$PVCSPath:  L:/src/asw/msm5200/mm/vcs/sms_gmm.h_v   1.0   04 Apr 2001 16:57:34   jault  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/api/sms_gmm.h#1 $ $DateTime: 2015/01/27 06:42:19 $ $Author: mplp4svc $
            
when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/12/03   vdr     Added definition to support SMS Bearer Selection
02/06/02   vdr     Created file.
            
===========================================================================*/

/* <EJECT> */
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "customer.h"


#include "sys.h"



/* <EJECT> */
/*===========================================================================

                        DATA DECLARATIONS

===========================================================================*/

#define MM_SMS_BEARER_SELECTION_CMD 0x01

typedef enum
{
  CS_PREFERRED,
  PS_PREFERRED,
  CS_ONLY,
  PS_ONLY
} mm_sms_bearer_selection_enum_T;

typedef struct
{
  /* Message header needed by MM */
  IMH_T                            message_header;

  mm_sms_bearer_selection_enum_T   sms_bearer_selection;

  sys_modem_as_id_e_type           as_id;


  
} mm_sms_bearer_selection_req_T ;

#endif /* SMS_GMM_H */

