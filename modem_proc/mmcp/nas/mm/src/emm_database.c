

/*===========================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/nas/mm/src/emm_database.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
============================================================================
10/06/09   vdr     Compiler warnings fixed
04/14/09    RI      Added emm_db_get_ctrl_data() for MM Authentication Server.
============================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "mmcp_variation.h"
#include<customer.h>
#ifdef FEATURE_LTE

#include "comdef.h"
#include "emm.h"
#include "emm_database.h"

/*===========================================================================

                DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains definitions for constants, macros, types, variables
and other items needed by this module.

===========================================================================*/
#if defined(FEATURE_TRIPLE_SIM)
emm_ctrl_data_type *emm_ctrl_data_ptr_sim[MAX_AS_IDS] = {NULL, NULL,NULL};
#elif defined FEATURE_DUAL_SIM
emm_ctrl_data_type *emm_ctrl_data_ptr_sim[MAX_AS_IDS] = {NULL, NULL};
#endif
emm_ctrl_data_type *emm_ctrl_data_ptr = NULL;

boolean                    plmn_search_req_pending_ind = FALSE ;
mmr_plmn_search_req_s_type plmn_search_req_buffer ;

boolean                    reg_req_pending_ind = FALSE ;
mmr_plmn_search_req_s_type reg_req_buffer ;
                  
emm_reg_service_report_type reg_service_report ; 


/*===========================================================================
 
FUNCTION    EMM_DB_ALLOC_MEMORY

DESCRIPTION
  This function allocates the memory for emm_ctrl_data pointer for two stacks for a dual SIM device.
  For single SIM device only one heap block is allocated

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/

void emm_db_alloc_memory()
{
  size_t  size = sizeof(emm_ctrl_data_type);
#ifdef FEATURE_DUAL_SIM
  mm_as_id_e_type  as_id;
  for(as_id = MM_AS_ID_1; as_id < (mm_as_id_e_type)MAX_AS_IDS; as_id++)
  {
    emm_ctrl_data_ptr_sim[as_id] = modem_mem_alloc(size, MODEM_MEM_CLIENT_NAS);
    if(emm_ctrl_data_ptr_sim[as_id] != NULL)
    {
      /* Init the allocated memory */
      memset(emm_ctrl_data_ptr_sim[as_id], 0x0, size);
    }
    else
    {
      MSG_FATAL_DS(MM_SUB,"=EMM= Malloc failed - size: %d", size,0,0); 
    }
  }
#else
  emm_ctrl_data_ptr = modem_mem_alloc(size, MODEM_MEM_CLIENT_NAS);
  if(emm_ctrl_data_ptr != NULL)
  {
    /* Init the allocated memory */
    memset(emm_ctrl_data_ptr, 0x0, size);
  }
  else
  {
    MSG_FATAL_DS(MM_SUB,"=EMM= Malloc failed - size: %d", size,0,0); 
  }
#endif
}

 /*===========================================================================

FUNCTION    EMM_DB_GET_CTRL_DATA_PER_SUBS

DESCRIPTION
  This function returns a pointer to the heap memory block based on the as_id

DEPENDENCIES
  None

RETURN VALUE
  emm_ctrl_data_type*

SIDE EFFECTS
  None
===========================================================================*/

emm_ctrl_data_type *emm_db_get_ctrl_data_per_subs(mm_as_id_e_type as_id)
{
#ifdef FEATURE_DUAL_SIM
  return emm_ctrl_data_ptr_sim[as_id];
#else
  (void)as_id;
  return emm_ctrl_data_ptr;
#endif
}

/*===========================================================================

FUNCTION    EMM_DB_GET_CTRL_DATA

DESCRIPTION
  This function returns a pointer to the heap memory block based on the current subscription ID.

DEPENDENCIES
  None

RETURN VALUE
  emm_ctrl_data_type*

SIDE EFFECTS
  None
===========================================================================*/
emm_ctrl_data_type *emm_db_get_ctrl_data(void)
{
#ifdef FEATURE_DUAL_SIM
  return emm_ctrl_data_ptr_sim[mm_sub_id];
#else
  return emm_ctrl_data_ptr;
#endif
}
#endif /*FEATURE_LTE*/

