/*==============================================================================
                             COPYRIGHT INFORMATION

Copyright (c) 2002 Qualcomm Technologies, Incorporated and its licensors.  All Rights
Reserved.  QUALCOMM Proprietary.  Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                            EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/reg/vcs/reg_nv.c_v   1.1   06 Jun 2002 17:30:52   kabraham  $
$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/nas/reg/src/reg_nv.c#2 $ $DateTime: 2015/11/29 22:00:22 $ $Author: pstejg $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
11/28/12   am      Fixing compilation warning 
05/31/02   kwa     Initial version.
06/06/02   kwa     Eliminated use of memcpy.
12/30/02   kwa     Replaced ERR with MESSAGE_HIGH when attempt to read the
                   equivalent PLMN list from NVRAM fails in
                   reg_nv_read_equivalent_plmn_list.  This message is
                   consistently displayed if the equivalent PLMN list has never
                   been written to NVRAM.
12/31/02   kwa     Added F3 messages to print out the equivalent PLMN list
                   whenever it is saved to NVRAM.
05/22/03   kwa     Modified reg_nv_write_equivalent_plmn_list to pass the
                   eplmn_list_p parameter as a const pointer to eliminate
                   a lint warning.  Replaced reg_nv_equivalent_plmn_list
                   with reg_nv_item to eliminate lint warnings.
07/19/03   kwa     Added functionality to display the equivalent PLMN list
                   after reading it from NVRAM.
10/15/03   kwa     Added functions for reading and writing RPLMNACT.
12/03/03   kwa     Added functions for reading PS LOCI.
03/07/04   kwa     Added function reg_nv_write_cache.
03/17/04   kwa     Added function reg_nv_read_imsi.
04/28/04   kwa     Added call to reg_log_umts_nas_eplmn_list.
07/07/05   up      Added reg_nv_read_gprs_gcf_flag to support GCF 12.2.2.1d.
07/18/05   up      Added reg_nv_ens_enabled_flag to enable the ENS feature.
07/27/05   up      Added reg_nv_equivalent_plmn_list_get to current ePLMN list.
06/09/05   sn      Added flag reg_nv_data_valid flag.
08/09/11   abhi   Added reading of lpm_power_off EFS NV item
11/15/11  abhi   For EFRPLMNSI Selecting RLPMN if HPLMN is not found
09/12/11   HC     Updated the function definition with void argument 
                  for the deprecated function reg_nv_is_lpm_power_off_enabled()
==============================================================================*/

/*==============================================================================

                            INCLUDE FILES FOR MODULE

==============================================================================*/

#include "mmcp_variation.h"
#include "err.h"
#include "nv.h"
#include "reg_log.h"
#include "reg_nv.h"
#include "reg_task.h"
#include "reg_task_int.h"
#include "reg_timers.h"
#include "fs_lib.h"
#include <stringl/stringl.h>
#include "fs_public.h"
#include "reg_timers.h"
#include "reg_mode.h"
#ifdef FEATURE_MODEM_HEAP
#include "modem_mem.h"
#else
#include "gs_v.h"
#endif
#include "ghdi_exp.h"
#include "ghdi_exp_int.h"
#include "ghdi_nvmem.h"
#include "ghdi_nvmem_v.h"
#include "fs_errno.h"
/*==============================================================================

                                TYPE DEFINITIONS

==============================================================================*/

/*==============================================================================

                               DATA DECLARATIONS

==============================================================================*/
#define REG_NV_EFNAS_CONFIG_LENGTH 120
#define REG_NV_EFS_EHPLMN_LIST 6
#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_SGLTE)
static nv_equivalent_plmn_list_type reg_nv_equivalent_plmn_list_sim[MAX_NAS_STACKS];
#define reg_nv_equivalent_plmn_list reg_nv_equivalent_plmn_list_sim[reg_as_id]
static nv_rplmnact_type             reg_nv_rplmnact_sim[MAX_NAS_STACKS];
#define reg_nv_rplmnact          reg_nv_rplmnact_sim[reg_as_id]
static nv_item_type                 reg_nv_item_sim[MAX_NAS_STACKS];
#define reg_nv_item                reg_nv_item_sim[reg_as_id]

boolean                            reg_nv_gcf_flag_sim[MAX_NAS_STACKS] = {FALSE};                             
#define reg_nv_gcf_flag            reg_nv_gcf_flag_sim[reg_as_id]
static nv_cmd_type                  reg_nv_cmd;

static nv_cmd_ext_type              reg_nv_ext_cmd;
#define reg_nv_cmd_per_subs(a) reg_nv_ext_cmd.context = reg_nv_context_id_sim[reg_as_id]; \
                      reg_nv_ext_cmd.nvcmd = a; \
                      nv_cmd_ext(&reg_nv_ext_cmd)
static nv_extended_equivalent_plmn_list_type  reg_nv_extended_equivalent_plmn_list_sim[MAX_NAS_STACKS];
#define reg_nv_extended_equivalent_plmn_list  reg_nv_extended_equivalent_plmn_list_sim[reg_as_id] 

#define REG_EFS_CONTEXT_ID_SUB1  0
#define REG_EFS_CONTEXT_ID_SUB2  1
#define REG_EFS_CONTEXT_ID_SUB3  2

reg_nv_vplmn_list_s_type *reg_nv_vplmn_list_sim[MAX_NAS_STACKS]={NULL};
#define reg_nv_vplmn_list (reg_nv_vplmn_list_sim[reg_as_id])

#else
static nv_equivalent_plmn_list_type reg_nv_equivalent_plmn_list;

static nv_rplmnact_type             reg_nv_rplmnact;

static nv_item_type                 reg_nv_item;

static nv_cmd_type                  reg_nv_cmd;
boolean                                reg_nv_gcf_flag = FALSE;

#define reg_nv_cmd_per_subs(a)               nv_cmd(&reg_nv_cmd)

static nv_extended_equivalent_plmn_list_type  reg_nv_extended_equivalent_plmn_list;
reg_nv_vplmn_list_s_type *reg_nv_vplmn_list= NULL;                   
#endif /* FEATURE_DUAL_SIM*/

#ifdef FEATURE_DUAL_SIM
static rex_timer_cnt_type  reg_nv_t3245_timer_stored_value_sim[MAX_AS_IDS];
#define reg_nv_t3245_timer_stored_value  reg_nv_t3245_timer_stored_value_sim[reg_sub_id] 

static rex_timer_cnt_type  reg_nv_validate_sim_timer_value_sim[MAX_AS_IDS];
#define reg_nv_validate_sim_timer_value  reg_nv_validate_sim_timer_value_sim[reg_sub_id] 
static uint8  reg_nv_max_validate_sim_counter_sim[MAX_AS_IDS];
#define reg_nv_max_validate_sim_counter  reg_nv_max_validate_sim_counter_sim[reg_sub_id] 

static rex_timer_cnt_type  reg_nv_t3245_test_timer_value_sim[MAX_AS_IDS];
#define reg_nv_t3245_test_timer_value  reg_nv_t3245_test_timer_value_sim[reg_sub_id] 
#else
static rex_timer_cnt_type                      reg_nv_t3245_timer_stored_value ; /*Timer value is in milli seconds*/

static rex_timer_cnt_type                      reg_nv_validate_sim_timer_value;
static uint8                                   reg_nv_max_validate_sim_counter;

static rex_timer_cnt_type                      reg_nv_t3245_test_timer_value;
#endif

boolean reg_is_qrd_device = FALSE;                             


#ifdef FEATURE_DUAL_SIM
#ifdef FEATURE_ENHANCED_NW_SELECTION
#define reg_sim_read_ens_flag() reg_sim_per_nas_stacks_read_ens_flag(reg_sub_id)
#endif
#endif

#if defined(FEATURE_TRIPLE_SIM) || defined(FEATURE_SGLTE_DUAL_SIM)
static boolean                      reg_nv_data_valid_sim[MAX_NAS_STACKS] = { FALSE, FALSE,FALSE};
#define reg_nv_data_valid           reg_nv_data_valid_sim[reg_as_id] 

#elif defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE 
static boolean                      reg_nv_data_valid_sim[MAX_NAS_STACKS] = { FALSE, FALSE};
#define reg_nv_data_valid           reg_nv_data_valid_sim[reg_as_id] 
#else
static boolean                      reg_nv_data_valid = FALSE;
#endif

#ifdef FEATURE_ENHANCED_NW_SELECTION
/*
** NV Flag to enable the ENS Feature.
*/
static boolean                      reg_nv_ens_enabled_flag = FALSE;
#endif

#if defined(FEATURE_LTE) && defined(FEATURE_MMSS_3_1)
static boolean                      reg_nv_ignore_uplmn = FALSE;
#endif

static boolean                      reg_nv_imsi_switch      = FALSE;

static boolean                      reg_nv_wcdma_freq_lock_enabled = FALSE;
boolean                             reg_nv_lpm_power_off = TRUE;
static boolean                     reg_nv_efrplmnsi_selection = FALSE;
#if defined FEATURE_TDSCDMA || defined FEATURE_SGLTE
static boolean                      reg_nv_forced_irat_enabled = FALSE;
static reg_sim_plmn_list_s_type     reg_nv_tdscdma_op_plmn_efs_list; 
static uint32                       reg_nv_hplmn_irat_search_timer /*Timer value is in milli seconds*/;
#endif
#ifdef FEATURE_TDSCDMA 
static reg_sim_plmn_list_s_type    reg_nv_ehplmn_efs_list;
#define    REG_HPLMN_IRAT_SEARCH_TIMER     5*60*1000  /*default to 5 minutes*/
#endif

#ifdef FEATURE_FEMTO_CSG
#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_SGLTE)
static sys_plmn_id_s_type  reg_nv_rplmn_sim[MAX_NAS_STACKS];
#define  reg_nv_rplmn reg_nv_rplmn_sim[reg_as_id]

static boolean reg_nv_rplmn_dup_sim[MAX_NAS_STACKS];
#define reg_nv_rplmn_dup reg_nv_rplmn_dup_sim[reg_as_id]
#else
 /*
   RPLMN read from NV and also RPLMN used by REG. 
   This is updated with current RPLMN in all net sel modes except MANUAL CSG
*/
static sys_plmn_id_s_type  reg_nv_rplmn; 

/*
  This is set when reg_nv_rplmn is not updated in manaul csg mode.
  If device is powered off and this is set then only RPLMN is written to NV
*/
static boolean reg_nv_rplmn_dup;          
#endif /*FEATURE_DUAL_SIM*/
static sys_csg_support_e_type    reg_nv_csg_support = SYS_CSG_SUPPORT_WCDMA;
#endif 

boolean                                reg_nv_is_hplmn_to_be_selected = FALSE;
boolean                                reg_nv_is_roaming_on_in_eplmn = FALSE; 
boolean                                reg_nv_is_sim_invalid_recovery_enabled = FALSE;
#ifdef FEATURE_TDSCDMA
reg_sim_plmn_w_act_s_type  *reg_nv_preferred_plmn_list = NULL; 
int32 reg_nv_pref_plmn_list_length = 0;
#define REG_NV_MAX_PREF_PLMN_LIST 50
#endif 
/*==============================================================================

                          STATIC FUNCTION DEFINITIONS

==============================================================================*/


/*===========================================================================

FUNCTION reg_nv_populate_default_enhanced_hplmn_srch_tbl

DESCRIPTION
  Populate default tbl.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void reg_nv_populate_default_enhanced_hplmn_srch_tbl(byte *nv_config_tbl)
{
#if defined(FEATURE_DISABLE_MCC_FILTERING) && !defined(FEATURE_DISABLE_HPLMN_MCC_FILTERING)
#ifdef FEATURE_ENHANCED_NW_SELECTION
  byte default_tbl[REG_NV_CONFIG_TBL_MAX_SIZE] = {
                                            1, /* Config type is SET type */
                                            1, /* Only One Set present */
                                            10, /* 10 MCC present in the set */
                                                 /* 10 MCC entries */
                                            0x13, 0xF0,
                                            0x13, 0xF1,
                                            0x13, 0xF2,
                                            0x13, 0xF3,
                                            0x13, 0xF4,
                                            0x13, 0xF5,
                                            0x13, 0xF6,
                                            0x03, 0xF2,
                                            0x33, 0xF4,
                                            0x43, 0xF8 
                          };
  
  if(reg_sim_read_ens_flag())
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Populating default enhanced hplmn search tbl");
  
    /* Copy to tbl byte stream */
    memscpy((void*)nv_config_tbl,REG_NV_CONFIG_TBL_MAX_SIZE, (void*)default_tbl, sizeof(default_tbl));
  }
#endif
#endif

  return;

}
/*===========================================================================

FUNCTION reg_nv_output_vplmn_list

DESCRIPTION
  Prints the VPLMN

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void reg_nv_output_vplmn_list                
(
  void
)
{
  sys_plmn_id_s_type plmn;

  boolean plmn_id_is_undefined;
  boolean mnc_includes_pcs_digit;
  uint32  mcc;
  uint32  mnc;

  uint8 i;
  if(!reg_nv_vplmn_list)
  {
    MSG_FATAL_DS(REG_SUB, "=REG= reg_nv_vplmn_list is not initialized, and called for display", 0, 0, 0);
  }
  MSG_HIGH_DS_1(REG_SUB, "=REG= VPLMN PLMN list (length = %d)", reg_nv_vplmn_list->vplmn_length);

  if (reg_nv_vplmn_list->vplmn_length> 0)
  {
    for (i = 0; i < reg_nv_vplmn_list->vplmn_length; i++)
    {
        plmn.identity[0] = reg_nv_vplmn_list->vplmn_info[i].plmn.identity[0];
        plmn.identity[1] = reg_nv_vplmn_list->vplmn_info[i].plmn.identity[1];
        plmn.identity[2] = reg_nv_vplmn_list->vplmn_info[i].plmn.identity[2];
 
        sys_plmn_get_mcc_mnc
        (
         plmn,
         &plmn_id_is_undefined,
         &mnc_includes_pcs_digit,
         &mcc,
         &mnc
         );
        if(mnc_includes_pcs_digit)
        {
          MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu", i, mcc, mnc);
          MSG_HIGH_DS_1(REG_SUB, "=REG= RAT= %d", reg_nv_vplmn_list->vplmn_info[i].rat);
        }
        else
        {
          MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu", i, mcc, mnc);
          MSG_HIGH_DS_1(REG_SUB, "=REG= RAT= %d", reg_nv_vplmn_list->vplmn_info[i].rat);
        }
    }
  }

}
void reg_nv_output_eplmn_list
(
  void
)
{
  sys_plmn_id_s_type plmn;

  boolean plmn_id_is_undefined;
  boolean mnc_includes_pcs_digit;
  uint32  mcc;
  uint32  mnc;

  uint8 i;

  MSG_HIGH_DS_1(REG_SUB, "=REG= Equivalent PLMN list (length = %d)", reg_nv_equivalent_plmn_list.length);

  if (reg_nv_equivalent_plmn_list.length > 0)
  {
    plmn.identity[0] = reg_nv_equivalent_plmn_list.rplmn.identity[0];
    plmn.identity[1] = reg_nv_equivalent_plmn_list.rplmn.identity[1];
    plmn.identity[2] = reg_nv_equivalent_plmn_list.rplmn.identity[2];

    sys_plmn_get_mcc_mnc
    (
      plmn,
      &plmn_id_is_undefined,
      &mnc_includes_pcs_digit,
      &mcc,
      &mnc
    );

    if(mnc_includes_pcs_digit)
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG= Equivalent RPLMN(%03lu-%03lu)", mcc, mnc);
    }
    else
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG= Equivalent RPLMN(%03lu- %02lu)", mcc, mnc);
    }

    MSG_HIGH_DS_0(REG_SUB, "=REG=  # MCC-MNC");

    for (i = 0; i < reg_nv_equivalent_plmn_list.length; i++)
    {
      /* get the additional EPLMNs from extended nv list*/
      if(i >= NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)/*after this use new extended list*/
      {
        byte j = i- NV_EQUIVALENT_PLMN_LIST_MAX_SIZE;
        plmn.identity[0] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][0];
        plmn.identity[1] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][1];
        plmn.identity[2] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][2];
      }
      else
      {
        plmn.identity[0] = reg_nv_equivalent_plmn_list.plmn[i].identity[0];
        plmn.identity[1] = reg_nv_equivalent_plmn_list.plmn[i].identity[1];
        plmn.identity[2] = reg_nv_equivalent_plmn_list.plmn[i].identity[2];
      }
      sys_plmn_get_mcc_mnc
      (
        plmn,
        &plmn_id_is_undefined,
        &mnc_includes_pcs_digit,
        &mcc,
        &mnc
      );
      if(mnc_includes_pcs_digit)
      {
        MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu", i, mcc, mnc);
      }
      else
      {
        MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu", i, mcc, mnc);
      }
    }
  }
}
/*==============================================================================

                              FUNCTION DEFINITIONS

==============================================================================*/

#ifdef FEATURE_ENHANCED_NW_SELECTION
/*==============================================================================

FUNCTION NAME

  reg_nv_read_ens_enabled_flag

==============================================================================*/

boolean reg_nv_read_ens_enabled_flag
(
  void
)
{
  /*
  ** Fill NV command to read NV_ENS_ENABLED.
  */
  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_READ_F;
  reg_nv_cmd.item       = NV_ENS_ENABLED_I;
  reg_nv_cmd.data_ptr   = (nv_item_type*)(&reg_nv_ens_enabled_flag);

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  nv_cmd(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  /*
  ** NV_ENS_ENABLED could not be read from NVRAM.
  */
  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Reading NV_ENS_ENABLED FLAG failed");

    reg_nv_ens_enabled_flag = FALSE;
  }
  /*
  ** NV_ENS_ENABLED was successfully read from NVRAM.
  */
  else
  {
    MSG_HIGH_DS_1(REG_SUB, "=REG= Read NV_ENS_ENABLED FLAG  - %d", reg_nv_ens_enabled_flag);
  }
  
  return reg_nv_ens_enabled_flag;
}
#endif

/*==============================================================================

FUNCTION NAME

  reg_nv_equivalent_plmn_list_get

==============================================================================*/
nv_equivalent_plmn_list_type reg_nv_equivalent_plmn_list_get
(
  void
)
{
  return reg_nv_equivalent_plmn_list;
}
/*==============================================================================

FUNCTION NAME

  reg_nv_get_vplmn_list_length              

==============================================================================*/
int16 reg_nv_get_vplmn_list_length
(
  void
)
{
  if(reg_nv_vplmn_list)
  {
    return reg_nv_vplmn_list->vplmn_length;
  }
  else
  {
    return -1;
  }
}
/*============================================================================== 

FUNCTION NAME

  reg_nv_write_vplmn_list

==============================================================================*/
int16 reg_nv_write_vplmn_list
(
  void
)
{
  int16 status=0;
  uint32 data_size = (uint32)sizeof(reg_nv_vplmn_list_s_type);
  
  if(!reg_nv_vplmn_list)
  {
    MSG_FATAL_DS(REG_SUB, "=REG= Global Structure is not allocated memory during power up", 0, 0, 0);
  }

#ifdef FEATURE_DUAL_SIM
    if(reg_as_id == REG_AS_ID_2)
    {
      status = efs_put("/nv/item_files/modem/nas/vpmln_Subscription01",
                                            reg_nv_vplmn_list, 
                                            data_size,O_RDWR | O_CREAT | O_AUTODIR, 0777);
    }
    else
#endif
    {
      status = efs_put("/nv/item_files/modem/nas/vplmn", 
                                             reg_nv_vplmn_list, 
                                             data_size,O_RDWR | O_CREAT | O_AUTODIR, 0777);
    }

  
  MSG_HIGH_DS_1(REG_SUB, "=REG= VPLMN: Write to VPLMN list status = %d", status);

  return status;
}
/*============================================================================== 

FUNCTION NAME

  reg_nv_read_vplmn_list

==============================================================================*/
int16 reg_nv_read_vplmn_list
(
  void
)
{
  uint32 data_size;
  int16 status = 0;
  data_size = (uint32)sizeof(reg_nv_vplmn_list_s_type);
  if(!reg_nv_vplmn_list)
  {


    reg_nv_vplmn_list = (reg_nv_vplmn_list_s_type *) modem_mem_calloc(1,data_size, MODEM_MEM_CLIENT_NAS);
    if(!reg_nv_vplmn_list )
    {
       MSG_FATAL_DS(REG_SUB, "=REG= Could not allocate the memory", 0, 0, 0);
    }

    memset((void*)reg_nv_vplmn_list, 0, data_size);

#ifdef FEATURE_DUAL_SIM
    if(reg_as_id == REG_AS_ID_2)
    {
        status = efs_get("/nv/item_files/modem/nas/vpmln_Subscription01",
                                                     reg_nv_vplmn_list, 
                                                     data_size);
    }
    else
#endif
    {
         status = efs_get("/nv/item_files/modem/nas/vplmn", 
                                                      reg_nv_vplmn_list, 
                                                      data_size);
    }


    if(status != -1)
    {
       MSG_HIGH_DS_0(REG_SUB, "=REG= VPLMN: Reading VPLMN list from NV, updating the Global");
       reg_nv_output_vplmn_list();
    }
    else
    {
       MSG_HIGH_DS_1(REG_SUB, "=REG= VPLMN: Reading VPLMN list from NV failed status = %d", status);
       status = -1;
       reg_nv_vplmn_list->vplmn_length = 0;
    }

  }
  return status;
}
/*==============================================================================

FUNCTION NAME

  reg_nv_sys_equivalent_plmn_list_get

==============================================================================*/
void reg_nv_sys_equivalent_plmn_list_get
(
  sys_plmn_list_s_type* eplmn_list_p
)
{
  uint32 i;

  eplmn_list_p->length = MIN(reg_nv_equivalent_plmn_list.length,
                             NV_EQUIVALENT_PLMN_LIST_MAX_SIZE
                           + NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE);

  eplmn_list_p->length = MIN(eplmn_list_p->length,
                               SYS_PLMN_LIST_MAX_LENGTH);

  for (i = 0; i < eplmn_list_p->length; i++)
  {
    /*get additional EPLMNs from the extended list*/
    if(i >= NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)/*after this use new extended list*/
    {
      byte j = (byte)MIN(NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE-1,(i- NV_EQUIVALENT_PLMN_LIST_MAX_SIZE));
      eplmn_list_p->plmn[i].identity[0] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][0];
      eplmn_list_p->plmn[i].identity[1] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][1];
      eplmn_list_p->plmn[i].identity[2] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][2];
    }
    else
    {
      eplmn_list_p->plmn[i].identity[0] = reg_nv_equivalent_plmn_list.plmn[i].identity[0];
      eplmn_list_p->plmn[i].identity[1] = reg_nv_equivalent_plmn_list.plmn[i].identity[1];
      eplmn_list_p->plmn[i].identity[2] = reg_nv_equivalent_plmn_list.plmn[i].identity[2];
    }
  } 

  return;
}

/*==============================================================================

FUNCTION NAME

  reg_nv_read_equivalent_plmn_list

==============================================================================*/

void reg_nv_read_equivalent_plmn_list
(
  sys_plmn_id_s_type*   rplmn_p,
  sys_plmn_list_s_type* eplmn_list_p
)
{
  uint8 i;

  MSG_HIGH_DS_0(REG_SUB, "=REG= Read equivalent PLMN list from cache");
    
  reg_nv_output_eplmn_list();
  
  rplmn_p->identity[0] = reg_nv_equivalent_plmn_list.rplmn.identity[0];
  rplmn_p->identity[1] = reg_nv_equivalent_plmn_list.rplmn.identity[1];
  rplmn_p->identity[2] = reg_nv_equivalent_plmn_list.rplmn.identity[2];
  
    eplmn_list_p->length = MIN(reg_nv_equivalent_plmn_list.length,
                             NV_EQUIVALENT_PLMN_LIST_MAX_SIZE
                             + NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE);
  eplmn_list_p->length = MIN(eplmn_list_p->length,
                             SYS_PLMN_LIST_MAX_LENGTH);
  
  for (i = 0; i < eplmn_list_p->length; i++)
  {
     /*get additional EPLMNs from the extended list*/
      if(i >= NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)/*after this use new extended list*/
      {
        byte j = (byte)MIN(NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE-1, i- NV_EQUIVALENT_PLMN_LIST_MAX_SIZE);
        eplmn_list_p->plmn[i].identity[0] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][0];
        eplmn_list_p->plmn[i].identity[1] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][1];
        eplmn_list_p->plmn[i].identity[2] = reg_nv_extended_equivalent_plmn_list.plmn_identity[j][2];
      }
      else 
      {
        eplmn_list_p->plmn[i].identity[0] = reg_nv_equivalent_plmn_list.plmn[i].identity[0];
        eplmn_list_p->plmn[i].identity[1] = reg_nv_equivalent_plmn_list.plmn[i].identity[1];
        eplmn_list_p->plmn[i].identity[2] = reg_nv_equivalent_plmn_list.plmn[i].identity[2];
      }
  }

}


/*==============================================================================

FUNCTION NAME

  reg_nv_write_equivalent_plmn_list

==============================================================================*/

void reg_nv_write_equivalent_plmn_list
(
        sys_plmn_id_s_type    rplmn,
  const sys_plmn_list_s_type* eplmn_list_p
)
{
  uint8 i;

  /*
  ** Copy the RPLMN into the static variable to be written to NVRAM.
  */
  reg_nv_equivalent_plmn_list.rplmn.identity[0] = rplmn.identity[0];
  reg_nv_equivalent_plmn_list.rplmn.identity[1] = rplmn.identity[1];
  reg_nv_equivalent_plmn_list.rplmn.identity[2] = rplmn.identity[2];

  /*
  ** Copy the equivalent PLMN list into the static variable
  ** to be written to NVRAM.
  */
  reg_nv_equivalent_plmn_list.length = NV_EQUIVALENT_PLMN_LIST_MAX_SIZE
                                     + NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE;

  if ((uint8)SYS_PLMN_LIST_MAX_LENGTH < reg_nv_equivalent_plmn_list.length)
  {
    reg_nv_equivalent_plmn_list.length = (uint8)SYS_PLMN_LIST_MAX_LENGTH;
  }

  if (eplmn_list_p->length < reg_nv_equivalent_plmn_list.length)
  {
    reg_nv_equivalent_plmn_list.length = (uint8)eplmn_list_p->length;
  }

  for (i = 0; i < reg_nv_equivalent_plmn_list.length; i++)
  {
    /*get additional EPLMNs from the extended list*/
    if(i >= NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)/*after this use new extended list*/
    {
      uint8 j = i- NV_EQUIVALENT_PLMN_LIST_MAX_SIZE;
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][0] = eplmn_list_p->plmn[i].identity[0];
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][1] = eplmn_list_p->plmn[i].identity[1];
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][2] = eplmn_list_p->plmn[i].identity[2];
    }
    else
    {
      reg_nv_equivalent_plmn_list.plmn[i].identity[0] = eplmn_list_p->plmn[i].identity[0];
      reg_nv_equivalent_plmn_list.plmn[i].identity[1] = eplmn_list_p->plmn[i].identity[1];
      reg_nv_equivalent_plmn_list.plmn[i].identity[2] = eplmn_list_p->plmn[i].identity[2];
    }
  }

  /*
  ** Undefine the unused PLMN ids contained within the equivalent
  ** PLMN list to make it easier to verify that the NV item has
  ** been properly updated.
  */
  for (i = reg_nv_equivalent_plmn_list.length; i < NV_EQUIVALENT_PLMN_LIST_MAX_SIZE + NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE; i++)
  {
    /*get additional EPLMNs from the extended list*/
    if(i >= NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)/*after this use new extended list*/
    {
      uint8 j = i- NV_EQUIVALENT_PLMN_LIST_MAX_SIZE;
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][0] = 0xFF;
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][1] = 0xFF;
      reg_nv_extended_equivalent_plmn_list.plmn_identity[j][2] = 0xFF;
    }
    else
    {
      reg_nv_equivalent_plmn_list.plmn[i].identity[0] = 0xFF;
      reg_nv_equivalent_plmn_list.plmn[i].identity[1] = 0xFF;
      reg_nv_equivalent_plmn_list.plmn[i].identity[2] = 0xFF;
    }
  }

  reg_nv_data_valid = TRUE;

  reg_nv_output_eplmn_list();

  reg_log_umts_nas_eplmn_list
  (
    rplmn,
    eplmn_list_p
  );
}


/*==============================================================================

FUNCTION NAME

  reg_nv_read_rplmnact

==============================================================================*/

void reg_nv_read_rplmnact
(
  nv_rplmnact_type* rplmnact_p
)
{
  MSG_HIGH_DS_2(REG_SUB, "=REG= Read RPLMNACT %d %d from cache", reg_nv_rplmnact.act[0], reg_nv_rplmnact.act[1]);
  rplmnact_p->act[0] = reg_nv_rplmnact.act[0];
  rplmnact_p->act[1] = reg_nv_rplmnact.act[1];
}


/*==============================================================================

FUNCTION NAME

  reg_nv_write_rplmnact

==============================================================================*/

void reg_nv_write_rplmnact
(
  nv_rplmnact_type rplmnact
)
{
  reg_nv_data_valid = TRUE;
  reg_nv_rplmnact = rplmnact;
  
  MSG_HIGH_DS_2(REG_SUB, "=REG= Save RPLMNACT %d %d in cache", reg_nv_rplmnact.act[0], reg_nv_rplmnact.act[1]);
}

#ifdef FEATURE_FEMTO_CSG
/*==============================================================================

FUNCTION NAME

  reg_nv_read_rplmn

==============================================================================*/

sys_plmn_id_s_type reg_nv_read_rplmn
(
  void
)
{
  MSG_HIGH_DS_3(REG_SUB, "=REG= Read RPLMN  %x %x %x from cache",  
                reg_nv_rplmn.identity[0], 
                reg_nv_rplmn.identity[1], 
                reg_nv_rplmn.identity[2]);
  return reg_nv_rplmn;
}


/*==============================================================================

FUNCTION NAME

  reg_nv_write_rplmn

==============================================================================*/

void reg_nv_write_rplmn
(
  sys_plmn_id_s_type  rplmn
)
{
    reg_nv_rplmn = rplmn;
}

/*==============================================================================

FUNCTION NAME

  reg_nv_write_rplmn_dup

==============================================================================*/

void reg_nv_write_rplmn_dup
(
  boolean  rplmn_dup
)
{
  reg_nv_rplmn_dup = rplmn_dup;
}

/*==============================================================================

FUNCTION NAME

  reg_nv_read_rplmn_dup

==============================================================================*/

boolean reg_nv_read_rplmn_dup
(
  void
)
{
  return reg_nv_rplmn_dup;
}


/*==============================================================================

FUNCTION NAME

  reg_nv_read_csg_support

==============================================================================*/

sys_csg_support_e_type reg_nv_read_csg_support
(
  void
)
{
  return reg_nv_csg_support;
}


#endif 

/*==============================================================================

FUNCTION NAME

  reg_nv_read_psloci

==============================================================================*/

boolean reg_nv_read_psloci
(
  byte* psloci_p
)
{
  boolean status = FALSE;

  /*
  ** Fill NV command to read RPLMNACT.
  */
  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_READ_F;
  reg_nv_cmd.item       = NV_GSM_LOCIGPRS_I;
  reg_nv_cmd.data_ptr   = &reg_nv_item;

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  reg_nv_cmd_per_subs(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  /*
  ** PSLOCI could not be read from NVRAM.
  */
  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Reading PS LOCI failed");
  }
  /*
  ** PSLOCI was successfully read from NVRAM.
  */
  else
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Read PS LOCI");

    psloci_p[0]  = reg_nv_item.gsm_locigprs[0];
    psloci_p[1]  = reg_nv_item.gsm_locigprs[1];
    psloci_p[2]  = reg_nv_item.gsm_locigprs[2];
    psloci_p[3]  = reg_nv_item.gsm_locigprs[3];
    psloci_p[4]  = reg_nv_item.gsm_locigprs[4];
    psloci_p[5]  = reg_nv_item.gsm_locigprs[5];
    psloci_p[6]  = reg_nv_item.gsm_locigprs[6];
    psloci_p[7]  = reg_nv_item.gsm_locigprs[7];
    psloci_p[8]  = reg_nv_item.gsm_locigprs[8];
    psloci_p[9]  = reg_nv_item.gsm_locigprs[9];
    psloci_p[10] = reg_nv_item.gsm_locigprs[10];
    psloci_p[11] = reg_nv_item.gsm_locigprs[11];
    psloci_p[12] = reg_nv_item.gsm_locigprs[12];
    psloci_p[13] = reg_nv_item.gsm_locigprs[13];

    status = TRUE;
  }

  return status;
}


/*==============================================================================

FUNCTION NAME

  reg_nv_write_cache

==============================================================================*/

void reg_nv_write_cache
(
  void
)
{
  if(!reg_nv_data_valid)
  {
    MSG_MED_DS_0(REG_SUB, "=REG= Trying to write NV with uninitialized data - blocked");
    return;
  }
  /*
  ** Fill NV command to write RPLMNACT.
  */
  reg_nv_item.rplmnact = reg_nv_rplmnact;

  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_WRITE_F;
  reg_nv_cmd.item       = NV_RPLMNACT_I;
  reg_nv_cmd.data_ptr   = &reg_nv_item;

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  reg_nv_cmd_per_subs(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_ERROR_DS_0(REG_SUB, "=REG= Write RPLMNACT failed");
  }
  else
  {
    MSG_HIGH_DS_2(REG_SUB, "=REG= Wrote RPLMNACT %d %d", reg_nv_rplmnact.act[0], reg_nv_rplmnact.act[1]);
  }

  /*
  ** Fill NV command to write the equivalent PLMN list.
  */
  reg_nv_item.equivalent_plmn_list = reg_nv_equivalent_plmn_list;

  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_WRITE_F;
  reg_nv_cmd.item       = NV_EQUIVALENT_PLMN_LIST_I;
  reg_nv_cmd.data_ptr   = &reg_nv_item;

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  reg_nv_cmd_per_subs(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_ERROR_DS_0(REG_SUB, "=REG= Write equivalent PLMN list failed");
  }
  else
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Wrote equivalent PLMN list");
    reg_nv_data_valid = FALSE;
    /*
    ** Fill NV command to write the extended equivalent PLMN list.
    */
    reg_nv_item.extended_equivalent_plmn_list = reg_nv_extended_equivalent_plmn_list;

    reg_nv_cmd.tcb_ptr    = rex_self();
    reg_nv_cmd.sigs       = REG_NVIF_SIG;
    reg_nv_cmd.done_q_ptr = NULL;
    reg_nv_cmd.cmd        = NV_WRITE_F;
    reg_nv_cmd.item       = NV_EXTENDED_EQUIVALENT_PLMN_LIST_I;
    reg_nv_cmd.data_ptr   = &reg_nv_item;

    /*
    ** Clear the REG_NVIF_SIG signal.
    */
    (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

    /*
    ** Send the command to NV.
    */
    reg_nv_cmd_per_subs(&reg_nv_cmd);

    /*
    ** Wait for NV to set the REG_NVIF_SIG signal.
    */
    (void) reg_wait(REG_NVIF_SIG);

    if (reg_nv_cmd.status != NV_DONE_S)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote equivalent PLMN list and extended equivalent PLMN list failed");
    }
    else
    {
      MSG_HIGH_DS_0(REG_SUB, "=REG= Wrote equivalent PLMN list and Wrote extended equivalent PLMN list");
      reg_nv_data_valid = FALSE;
    }
  }
  
  #ifdef FEATURE_TDSCDMA
  /* Write HPLMN IRAT Search timer */
    if(efs_put("/nv/item_files/modem/nas/irat_search_timer" , 
                           &reg_nv_hplmn_irat_search_timer,
                           sizeof(uint32), O_CREAT | O_AUTODIR, 0777) == -1)
  {
    MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote HPLMN IRAT search timer to NV failed");
  }
  else
  {  
    MSG_HIGH_DS_0(REG_SUB, "=REG=  Wrote HPLMN IRAT search timer to NV");
  }
#endif 

#ifdef FEATURE_FEMTO_CSG
  if(reg_nv_rplmn_dup == FALSE)
  {
    sys_plmn_undefine_plmn_id(&reg_nv_rplmn);
  }
  /* Write RPLMN to NV */
#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_SGLTE)
  if(reg_as_id == REG_AS_ID_1)
  {
    if(efs_put("/nv/item_files/modem/nas/rplmn" , 
                         &reg_nv_rplmn,
                         sizeof(sys_plmn_id_s_type), O_CREAT | O_AUTODIR, 0777) == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote RPLMN to NV failed");
    }
    else
    {  
      MSG_HIGH_DS_3(REG_SUB, "=REG=  Wrote RPLMN to NV %d %d %d", 
                    reg_nv_rplmn.identity[0], 
                    reg_nv_rplmn.identity[1], 
                    reg_nv_rplmn.identity[2]);
    }
  }
  else if(reg_as_id == REG_AS_ID_2)
  {
    if(efs_put("/nv/item_files/modem/nas/rplmn1" , 
                             &reg_nv_rplmn,
                             sizeof(sys_plmn_id_s_type), O_CREAT | O_AUTODIR, 0777) == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote RPLMN to NV failed");
    }
    else
    {  
      MSG_HIGH_DS_3(REG_SUB, "=REG=  Wrote RPLMN to NV %d %d %d", 
                    reg_nv_rplmn.identity[0], 
                    reg_nv_rplmn.identity[1], 
                    reg_nv_rplmn.identity[2]);
    }
  }
#ifdef FEATURE_SGLTE_DUAL_SIM
  else if(reg_as_id == REG_AS_ID_3)
  {
    if(efs_put("/nv/item_files/modem/nas/rplmn2" , 
                             &reg_nv_rplmn,
                             sizeof(sys_plmn_id_s_type), O_CREAT | O_AUTODIR, 0777) == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote RPLMN to NV failed");
    }
    else
    {  
      MSG_HIGH_DS_3(REG_SUB, "=REG=  Wrote RPLMN to NV %d %d %d", 
                    reg_nv_rplmn.identity[0], 
                    reg_nv_rplmn.identity[1], 
                    reg_nv_rplmn.identity[2]);
    }
  }
#endif 
#else
  if(efs_put("/nv/item_files/modem/nas/rplmn" , 
                           &reg_nv_rplmn,
                           sizeof(sys_plmn_id_s_type), O_CREAT | O_AUTODIR, 0777) == -1)
  {
    MSG_ERROR_DS_0(REG_SUB, "=REG= Wrote RPLMN to NV failed");
  }
  else
  {  
    MSG_HIGH_DS_3(REG_SUB, "=REG=  Wrote RPLMN to NV %d %d %d", 
                  reg_nv_rplmn.identity[0], 
                  reg_nv_rplmn.identity[1], 
                  reg_nv_rplmn.identity[2]);
  }
#endif
#endif   /*FEATURE_FEMTO_CSG*/
  
}


/*==============================================================================

FUNCTION NAME

  reg_nv_read_imsi

==============================================================================*/

boolean reg_nv_read_imsi
(
  reg_nv_imsi_s_type* imsi_p
)
{
  boolean status = FALSE;

  int32 i;

  /*
  ** Fill NV command to read IMSI.
  */
  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_READ_F;
  reg_nv_cmd.item       = NV_GSMUMTS_IMSI_I;
  reg_nv_cmd.data_ptr   = &reg_nv_item;

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  reg_nv_cmd_per_subs(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  /*
  ** IMSI could not be read from NVRAM.
  */
  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Reading IMSI failed");
  }
  /*
  ** IMSI was successfully read from NVRAM.
  */
  else
  {
    for (i = 0; i < REG_NV_IMSI_MAX_LENGTH; i++)
    {
      imsi_p->digits[i] = reg_nv_item.gsmumts_imsi[i];

      MSG_HIGH_DS_2(REG_SUB, "=REG= IMSI[%u] = 0x%02x", i, imsi_p->digits[i]);
    }

    status = TRUE;
  }

  return status;
}

#ifdef FEATURE_TDSCDMA
/*==============================================================================

FUNCTION NAME

  reg_nv_read_preferred_plmn_list

==============================================================================*/

void reg_nv_read_preferred_plmn_list()
{
  byte *buffer;  
  uint8 length;
  int status;
  reg_nv_preferred_plmn_list = NULL; 
  reg_nv_pref_plmn_list_length = 0;

  buffer = (byte*) modem_mem_alloc(REG_NV_MAX_PREF_PLMN_LIST *5+1, MODEM_MEM_CLIENT_NAS);

  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/modem/nas/preferred_plmn_list",
                     &buffer[0],
                     (REG_NV_MAX_PREF_PLMN_LIST *5)+1);
  if(status > 0 )
  {  
    length = buffer[0];
  
    if(length > REG_NV_MAX_PREF_PLMN_LIST)
    {
      length = REG_NV_MAX_PREF_PLMN_LIST;
    }
  
    if(length > 0)
    {
      reg_nv_preferred_plmn_list = (reg_sim_plmn_w_act_s_type*)modem_mem_alloc(length*5, MODEM_MEM_CLIENT_NAS);
    
      memscpy( reg_nv_preferred_plmn_list, length*5, &buffer[1], length*5);
      reg_nv_pref_plmn_list_length = length;
    }
  }
  modem_mem_free(buffer, MODEM_MEM_CLIENT_NAS);
}

/*==============================================================================

FUNCTION NAME

  reg_nv_get_preferred_plmn_list

==============================================================================*/
reg_sim_plmn_w_act_s_type  * reg_nv_get_preferred_plmn_list(int32 *preferred_plmn_length)
{

  boolean plmn_id_is_undefined;
  boolean mnc_includes_pcs_digit;
  uint32  mcc;
  uint32  mnc;
  int i;
 
  if(reg_nv_preferred_plmn_list == NULL)
  {
    *preferred_plmn_length = 0;    
     return NULL;
  }
  if(reg_mode_is_usim_compliant())
  {
    *preferred_plmn_length = reg_nv_pref_plmn_list_length;
     MSG_HIGH_DS_1(REG_SUB, "=REG= NV Preferred PLMN list (length = %d)", reg_nv_pref_plmn_list_length);

     MSG_HIGH_DS_0(REG_SUB, "=REG=  # MCC-MNC  RAT   CAT");
     for (i = 0; i < reg_nv_pref_plmn_list_length; i++)
     {
       //SKIP the undefined plmn
       if(sys_plmn_id_is_undefined(reg_nv_preferred_plmn_list[i].plmn))
       {
         continue;
       }
       sys_plmn_get_mcc_mnc
       (
         reg_nv_preferred_plmn_list[i].plmn,
         &plmn_id_is_undefined,
         &mnc_includes_pcs_digit,
         &mcc,
         &mnc
       ); 

       if((reg_nv_preferred_plmn_list[i].act[0] & 0x80)  &&
           (reg_nv_preferred_plmn_list[i].act[1] == 0x00 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS PPLMN", i, mcc, mnc);
         }
       }
       else if((reg_nv_preferred_plmn_list[i].act[0] == 0x00) &&
           (reg_nv_preferred_plmn_list[i].act[1] & 0x80 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  GSM PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  GSM PPLMN", i, mcc, mnc);
         }
       }
       else if((reg_nv_preferred_plmn_list[i].act[0] & 0x80) &&
               (reg_nv_preferred_plmn_list[i].act[1] & 0x80 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS-GSM PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS-GSM PPLMN", i, mcc, mnc);
         }
       }
       else if((reg_nv_preferred_plmn_list[i].act[0] == 0x00) &&
               (reg_nv_preferred_plmn_list[i].act[1] == 0x00 ))
       {
         if(mnc_includes_pcs_digit)
         {
#ifdef FEATURE_LTE
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS-LTE-GSM PPLMN", i, mcc, mnc);
#else
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS-GSM PPLMN", i, mcc, mnc);
#endif
         }
         else
         {
#ifdef FEATURE_LTE
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS-LTE-GSM PPLMN", i, mcc, mnc);
#else
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS-GSM PPLMN", i, mcc, mnc);
#endif

         }
       }
#ifdef FEATURE_LTE
       else if((reg_nv_preferred_plmn_list[i].act[0] & 0x40) &&
           (reg_nv_preferred_plmn_list[i].act[1] == 0x00 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  LTE PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  LTE PPLMN", i, mcc, mnc);
         }
       }

       else if((reg_nv_preferred_plmn_list[i].act[0] & 0x40) &&
               (reg_nv_preferred_plmn_list[i].act[1] & 0x80 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  LTE-GSM PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  LTE-GSM PPLMN", i, mcc, mnc);
         }
       }
       else if((reg_nv_preferred_plmn_list[i].act[0] & 0xc0) &&
               (reg_nv_preferred_plmn_list[i].act[1] == 0x00 ))
       {
         if(mnc_includes_pcs_digit)
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS-LTE PPLMN", i, mcc, mnc);
         }
         else
         {
           MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS-LTE PPLMN", i, mcc, mnc);
         }
       }

       else if((reg_nv_preferred_plmn_list[i].act[0] & 0xc0) &&
               (reg_nv_preferred_plmn_list[i].act[1] & 0x80 ))
       {
         if(mnc_includes_pcs_digit)
         {
            MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu-%03lu  UMTS-LTE-GSM PPLMN", i, mcc, mnc);
         }
         else
         {
            MSG_HIGH_DS_3(REG_SUB, "=REG= %2lu %03lu- %02lu  UMTS-LTE-GSM PPLMN", i, mcc, mnc);
         }
       }
   #endif
     }
     return reg_nv_preferred_plmn_list;
  }
  else
  {
    *preferred_plmn_length = 0;    
     return NULL;
  }  
}
#endif 
/*==============================================================================

FUNCTION NAME

  reg_nv_initialize_cache

==============================================================================*/

void reg_nv_initialize_cache(void)
{
  int status;
#if defined(FEATURE_LTE) && defined(FEATURE_MMSS_3_1)
  byte ignore_uplmn;
#endif
  uint16 wcdma_freq_lock_item = 0;
/* NV item of REG*/
  reg_nv_items_T  reg_nv_items;                 
#if defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE 
  for ( reg_as_id=REG_AS_ID_1; reg_as_id < (reg_as_id_e_type)MAX_NAS_STACKS; reg_as_id++ )
#endif
  {
#if defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE 
    reg_sub_id = reg_sub_id_stack[reg_as_id];
#endif
    /*
    ** Fill NV command to read RPLMNACT.
    */
    reg_nv_cmd.tcb_ptr    = rex_self();
    reg_nv_cmd.sigs       = REG_NVIF_SIG;
    reg_nv_cmd.done_q_ptr = NULL;
    reg_nv_cmd.cmd        = NV_READ_F;
    reg_nv_cmd.item       = NV_RPLMNACT_I;
    reg_nv_cmd.data_ptr   = (nv_item_type*)(&reg_nv_rplmnact);
  
    /*
    ** Clear the REG_NVIF_SIG signal.
    */
    (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);
  
    /*
    ** Send the command to NV.
    */
    reg_nv_cmd_per_subs(&reg_nv_cmd);
  
    /*
    ** Wait for NV to set the REG_NVIF_SIG signal.
    */
    (void) reg_wait(REG_NVIF_SIG);
  
    /*
    ** RPLMNACT could not be read from NVRAM.
    */
    if (reg_nv_cmd.status != NV_DONE_S)
    {  
      MSG_HIGH_DS_0(REG_SUB, "=REG= Reading RPLMNACT failed");
  
      reg_nv_rplmnact.act[0] = 0;
      reg_nv_rplmnact.act[1] = 0;
    }
    /*
    ** RPLMNACT was successfully read from NVRAM.
    */
    else
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG= Read RPLMNACT %d %d", reg_nv_rplmnact.act[0], reg_nv_rplmnact.act[1]);
      reg_nv_data_valid = TRUE;
    }
  
    
    /*
    ** Fill NV command to read equivalent PLMN list.
    */
    reg_nv_cmd.tcb_ptr    = rex_self();
    reg_nv_cmd.sigs       = REG_NVIF_SIG;
    reg_nv_cmd.done_q_ptr = NULL;
    reg_nv_cmd.cmd        = NV_READ_F;
    reg_nv_cmd.item       = NV_EQUIVALENT_PLMN_LIST_I;
    reg_nv_cmd.data_ptr   = (nv_item_type*)(&reg_nv_equivalent_plmn_list);
  
    /*
    ** Clear the REG_NVIF_SIG signal.
    */
    (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);
  
    /*
    ** Send the command to NV.
    */
    reg_nv_cmd_per_subs(&reg_nv_cmd);
  
    /*
    ** Wait for NV to set the REG_NVIF_SIG signal.
    */
    (void) reg_wait(REG_NVIF_SIG);
  
    /*
    ** The equivalent PLMN list could not be read from NVRAM so
    ** undefine the output RPLMN and set the output list length to zero.
    */
    if (reg_nv_cmd.status != NV_DONE_S)
    {  
      reg_nv_equivalent_plmn_list.rplmn.identity[0] = 0xFF;
      reg_nv_equivalent_plmn_list.rplmn.identity[1] = 0xFF;
      reg_nv_equivalent_plmn_list.rplmn.identity[2] = 0xFF;
      
      reg_nv_equivalent_plmn_list.length = 0;
  
      MSG_HIGH_DS_0(REG_SUB, "=REG= Reading equivalent PLMN list failed");
    } 
    else
    {
      MSG_HIGH_DS_0(REG_SUB, "=REG= Read equivalent PLMN list");
  
      /*Read the EPLMN extended list if length of EPLMNs is greater than 5*/
      if(reg_nv_equivalent_plmn_list.length > NV_EQUIVALENT_PLMN_LIST_MAX_SIZE)
      {
         reg_nv_equivalent_plmn_list.length = MIN(reg_nv_equivalent_plmn_list.length, NV_EQUIVALENT_PLMN_LIST_MAX_SIZE + NV_EXTENDED_EQUIVALENT_PLMN_LIST_MAX_SIZE);
         /*
         ** Fill NV command to read extended equivalent PLMN list.
         */
         reg_nv_cmd.tcb_ptr    = rex_self();
         reg_nv_cmd.sigs   = REG_NVIF_SIG;
         reg_nv_cmd.done_q_ptr = NULL;
         reg_nv_cmd.cmd    = NV_READ_F;
         reg_nv_cmd.item   = NV_EXTENDED_EQUIVALENT_PLMN_LIST_I;
         reg_nv_cmd.data_ptr   = (nv_item_type*)(&reg_nv_extended_equivalent_plmn_list);
         /*
         ** Clear the REG_NVIF_SIG signal.
         */
         (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);
  
         /*
         ** Send the command to NV.
         */
         reg_nv_cmd_per_subs(&reg_nv_cmd);
    
         /*
         ** Wait for NV to set the REG_NVIF_SIG signal.
         */
         (void) reg_wait(REG_NVIF_SIG);
       
         /*
         ** The extended equivalent PLMN list could not be read from NVRAM so
         ** undefine the output RPLMN and set the output list length to zero.
         */
         if (reg_nv_cmd.status != NV_DONE_S)
         {
            MSG_HIGH_DS_0(REG_SUB, "=REG= Reading extended equivalent PLMN list failed");
         } 
         else
         {
           MSG_HIGH_DS_0(REG_SUB, "=REG= Read extended equivalent PLMN list");
         }
      }
      else
      {
         MSG_HIGH_DS_1(REG_SUB, "=REG= No need to read extended EPLMN list as length is %d", reg_nv_equivalent_plmn_list.length);
      }
  
      reg_nv_data_valid = TRUE;    
    }
  }

#if defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE 
  reg_as_id = REG_AS_ID_1;
#endif

#if defined (FEATURE_TDSCDMA) || defined (FEATURE_SGLTE)
  reg_nv_read_lte_cell_lists();
  reg_state_start_backoff_lte_search_timer_count(lte_non_available_list_ptr);
#endif

#if defined(FEATURE_LTE) && defined(FEATURE_MMSS_3_1)
  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/modem/nas/ignore_uplmn", 
                                &ignore_uplmn, 
                                sizeof(boolean));
  if (status > 0 && ignore_uplmn >= 1)              
  {
    reg_nv_ignore_uplmn = TRUE;
  }
  MSG_HIGH_DS_1(REG_SUB, "=REG= NV ignore_uplmn read from SIM Ignore UPLMN = %d", reg_nv_ignore_uplmn);
#endif

  memset( &reg_nv_items, 0x00, sizeof(reg_nv_items_T) );            

  status = efs_get("/nv/item_files/modem/nas/reg_nv_items",&reg_nv_items,sizeof(reg_nv_items_T));

  if(status != -1)
  {
    reg_nv_is_hplmn_to_be_selected = reg_nv_items.is_hplmn_has_to_be_selected;
    if(reg_nv_items.version >= 1)
    {
      reg_nv_is_roaming_on_in_eplmn = reg_nv_items.is_roaming_on_in_eplmn;
    }
    if(reg_nv_items.version >= 2)
    {
      reg_nv_is_sim_invalid_recovery_enabled = reg_nv_items.is_sim_invalid_recovery_enabled;
    }
  }

#ifdef FEATURE_DUAL_SIM
    if (cm_get_operator_info(reg_sub_id) == OPERATOR_CU)
#else
    if (cm_get_operator_info(SYS_MODEM_AS_ID_1) == OPERATOR_CU)
#endif
    {
      reg_nv_is_sim_invalid_recovery_enabled = TRUE;
    }

  MSG_HIGH_DS_3(REG_SUB, "=REG= NV reg_nv_is_hplmn_to_be_selected  = %d, reg_nv_is_roaming_on_in_eplmn = %d , reg_nv_is_sim_invalid_recovery_enabled = %d ",
                reg_nv_is_hplmn_to_be_selected,
                reg_nv_is_roaming_on_in_eplmn,
                reg_nv_is_sim_invalid_recovery_enabled);

  status = efs_get("/nv/item_files/modem/nas/imsi_switch", 
                                &reg_nv_imsi_switch, 
                                sizeof(boolean));

  MSG_HIGH_DS_2(REG_SUB, "=REG= NV imsi_switch read from EFS  with status %d and value is %d",
                status,
                reg_nv_imsi_switch);

  if (status < 0)
  {
    reg_nv_imsi_switch = FALSE;
  }
  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/wcdma/rrc/wcdma_rrc_freq_lock_item", 
                                &wcdma_freq_lock_item, 
                                sizeof(uint16));
  MSG_HIGH_DS_2(REG_SUB, "=REG= NV wcdma_freq_lock_item read from EFS  with status %d and value is %d",
                status,
                wcdma_freq_lock_item);

  if ((status >0 )&& (wcdma_freq_lock_item !=0))
  {
    reg_nv_wcdma_freq_lock_enabled = TRUE;
  }

  status = efs_get("/nv/item_files/modem/nas/lpm_power_off", 
                                &reg_nv_lpm_power_off, 
                                sizeof(boolean));
  if (status < 0)
  {
    reg_nv_lpm_power_off = TRUE;
  }
  MSG_HIGH_DS_1(REG_SUB, "=REG=  NV write EFS/NV at LPM read and value=%d",reg_nv_lpm_power_off);
  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/modem/nas/efrplmnsi_select_rplmn_after_hplmn", 
                                &reg_nv_efrplmnsi_selection, 
                                sizeof(boolean));
  if (status < 0)
  {
    reg_nv_efrplmnsi_selection = FALSE;
  }
  MSG_HIGH_DS_1 (REG_SUB, "=REG=  NV read EFRPLMNSI selection =%d",reg_nv_efrplmnsi_selection);

#ifdef FEATURE_TDSCDMA
   status = efs_get("/nv/item_files/modem/nas/forced_irat", 
                                &reg_nv_forced_irat_enabled, 
                                sizeof(boolean));

   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_forced_irat_enabled from EFS  with status %d and value is %d",
                 status,
                 reg_nv_forced_irat_enabled);    
   if (status < 0)
   { 
        reg_nv_forced_irat_enabled = FALSE;    
   }   
   reg_timers_pet_watchdog();
   status = efs_get("/nv/item_files/modem/nas/irat_search_timer", 
                                &reg_nv_hplmn_irat_search_timer, 
                                sizeof(uint32));

   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_hplmn_irat_search_timer from EFS  with status %d and value is %d",
                 status,
                 reg_nv_hplmn_irat_search_timer);    
   if (status < 0)
   { 
        reg_nv_hplmn_irat_search_timer = REG_HPLMN_IRAT_SEARCH_TIMER;   /*default valus is 15 minutes*/   
   }   
#endif

#ifdef FEATURE_FEMTO_CSG
#ifdef FEATURE_DUAL_SIM
  sys_plmn_undefine_plmn_id(&reg_nv_rplmn_sim[REG_AS_ID_1]);
  reg_nv_rplmn_dup_sim[REG_AS_ID_1] = FALSE;
  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/modem/nas/rplmn", 
                                &reg_nv_rplmn_sim[REG_AS_ID_1], 
                                sizeof(sys_plmn_id_s_type));
  if(status > 0)
  {
    MSG_HIGH_DS_3(REG_SUB, "=REG=  READ RPLMN from NV %d %d %d",
                  reg_nv_rplmn_sim[REG_AS_ID_1].identity[0],
                  reg_nv_rplmn_sim[REG_AS_ID_1].identity[1],
                  reg_nv_rplmn_sim[REG_AS_ID_1].identity[2]);
    if(!sys_plmn_id_is_undefined(reg_nv_rplmn_sim[REG_AS_ID_1]))
    {
      reg_nv_rplmn_dup_sim[REG_AS_ID_1] = TRUE;
    }
  }

  sys_plmn_undefine_plmn_id(&reg_nv_rplmn_sim[REG_AS_ID_2]);
  reg_nv_rplmn_dup_sim[REG_AS_ID_2] = FALSE;
  reg_timers_pet_watchdog(); 
  status = efs_get("/nv/item_files/modem/nas/rplmn1", 
                                &reg_nv_rplmn_sim[REG_AS_ID_2], 
                                sizeof(sys_plmn_id_s_type));
  if(status > 0)
  {
    MSG_HIGH_DS_3(REG_SUB, "=REG=  READ RPLMN from NV %d %d %d", 
                   reg_nv_rplmn_sim[REG_AS_ID_2].identity[0],
                   reg_nv_rplmn_sim[REG_AS_ID_2].identity[1],
                   reg_nv_rplmn_sim[REG_AS_ID_2].identity[2]);
    if(!sys_plmn_id_is_undefined(reg_nv_rplmn_sim[REG_AS_ID_2]))
    {
      reg_nv_rplmn_dup_sim[REG_AS_ID_2] = TRUE;
    }
  }
#ifdef FEATURE_SGLTE_DUAL_SIM
  sys_plmn_undefine_plmn_id(&reg_nv_rplmn_sim[REG_AS_ID_3]);
  reg_nv_rplmn_dup_sim[REG_AS_ID_3] = FALSE;
  reg_timers_pet_watchdog(); 
  status = efs_get("/nv/item_files/modem/nas/rplmn2", 
                                &reg_nv_rplmn_sim[REG_AS_ID_3], 
                                sizeof(sys_plmn_id_s_type));
  if(status > 0)
  {
    MSG_HIGH_DS_3(REG_SUB, "=REG=  READ RPLMN from NV %d %d %d", 
                   reg_nv_rplmn_sim[REG_AS_ID_3].identity[0],
                   reg_nv_rplmn_sim[REG_AS_ID_3].identity[1],
                   reg_nv_rplmn_sim[REG_AS_ID_3].identity[2]);
    if(!sys_plmn_id_is_undefined(reg_nv_rplmn_sim[REG_AS_ID_3]))
    {
      reg_nv_rplmn_dup_sim[REG_AS_ID_3] = TRUE;
    }
  }

#endif 
#else
  sys_plmn_undefine_plmn_id(&reg_nv_rplmn);
  reg_nv_rplmn_dup = FALSE;
  reg_timers_pet_watchdog();
  status = efs_get("/nv/item_files/modem/nas/rplmn", 
                                &reg_nv_rplmn, 
                                sizeof(sys_plmn_id_s_type));
  if(status > 0)
  {
    MSG_HIGH_DS_3(REG_SUB, "=REG=  READ RPLMN from NV %d %d %d", 
     reg_nv_rplmn.identity[0], reg_nv_rplmn.identity[1], reg_nv_rplmn.identity[2]);
    /* Value read from NV will be defined only if it is different from RPLMN
        So marking duplicate flag TRUE*/
    if(!sys_plmn_id_is_undefined(reg_nv_rplmn))
    {
      reg_nv_rplmn_dup = TRUE;
    }
  }
#endif /*FEATURE_DUAL_SIM*/

  status = efs_get("/nv/item_files/modem/nas/csg_support_configuration", 
                                &reg_nv_csg_support, 
                                sizeof(sys_csg_support_e_type));
  if(status < 0)
  {
    reg_nv_csg_support = SYS_CSG_SUPPORT_WCDMA;
  }

#endif /*FEATURE_FEMTO_CSG*/

                                                
    if(efs_get("/nv/item_files/modem/mmode/qmss_enabled",
                 &reg_is_qrd_device,
                 sizeof(boolean)) != -1)
    {
      MSG_HIGH_DS_1(REG_SUB,"=REG= setting QRD device setting to %d",reg_is_qrd_device);
    }
    else
    {
      MSG_HIGH_DS_0(REG_SUB,"=REG= setting QRD device to false as read failed");
      reg_is_qrd_device = FALSE;
    }


   if( reg_is_qrd_device == TRUE)
   {
#ifdef FEATURE_DUAL_SIM
     for ( reg_as_id=REG_AS_ID_1; reg_as_id < (reg_as_id_e_type)MAX_NAS_STACKS; reg_as_id++ )
#endif
     {
        status = reg_nv_read_vplmn_list();                      

        if( status != -1)
        {
          MSG_HIGH_DS_1(REG_SUB, "=REG= NV read VPLMN success Length of VPLMN = %d", reg_nv_get_vplmn_list_length());
        }
        else
        {
           MSG_HIGH_DS_1(REG_SUB, "=REG= NV read VPLMN Falied status = %d", status);
        }
      }
#ifdef FEATURE_DUAL_SIM
     reg_as_id=REG_AS_ID_1;
     reg_sub_id = reg_sub_id_stack[reg_as_id];
#endif

     reg_nv_is_sim_invalid_recovery_enabled = TRUE;
     MSG_HIGH_DS_1(REG_SUB, "=REG= Setting  reg_nv_is_sim_invalid_recovery_enabled for QRD device %d",
                   reg_nv_is_sim_invalid_recovery_enabled);
    }
#ifdef FEATURE_DUAL_SIM
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_stored",
                    &reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_1],
                    sizeof(rex_timer_cnt_type));
   if(status < 0)
   {
     reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_1] = REG_T3245_DEFUALT_VALUE;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_timer_stored_value from EFS  with status %d and value is %d for SIM1",
                 status,
                 reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_1]);
   
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_stored_Subscription01",
                    &reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_2],
                    sizeof(rex_timer_cnt_type));
   if(status < 0)
   {
     reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_2] = REG_T3245_DEFUALT_VALUE;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_timer_stored_value from EFS with status %d and value is %d for SIM2",
                 status,
                 reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_2]);

#ifdef FEATURE_TRIPLE_SIM
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_stored_Subscription02",
                    &reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_3],
                    sizeof(rex_timer_cnt_type));
   if(status < 0)
   {
     reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_3] = REG_T3245_DEFUALT_VALUE;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_timer_stored_value from EFS with status %d and value is %d for SIM3",
                 status,
                 reg_nv_t3245_timer_stored_value_sim[REG_AS_ID_3]);
#endif

   status = efs_get("/nv/item_files/modem/nas/t3245_timer",
                    &reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1] == 0)
   {
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1] = reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_validate_sim_timer_value from EFS  with status %d and value is %d(ms) for SIM1",
                 status,
                 reg_nv_validate_sim_timer_value_sim[REG_AS_ID_1]);
   
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_Subscription01",
                    &reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2] == 0)
   {
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2] = reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_validate_sim_timer_value from EFS with status %d and value is %d(ms) for SIM2",
                 status,
                 reg_nv_validate_sim_timer_value_sim[REG_AS_ID_2]);
   
#ifdef FEATURE_TRIPLE_SIM
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_Subscription02",
                    &reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3] == 0)
   {
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3] = reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_validate_sim_timer_value from EFS with status %d and value is %d(ms) for SIM3",
                 status, 
                 reg_nv_validate_sim_timer_value_sim[REG_AS_ID_3]);
#endif

   status = efs_get("/nv/item_files/modem/nas/max_validate_sim_counter",
                    &reg_nv_max_validate_sim_counter_sim[REG_AS_ID_1],
                    sizeof(uint8));
   if(status < 0 || reg_nv_max_validate_sim_counter_sim[REG_AS_ID_1] == 0)
   {
     reg_nv_max_validate_sim_counter_sim[REG_AS_ID_1] = REG_NV_DEFAULT_MAX_VALIDATE_SIM_COUNTER;
   }

   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_max_validate_sim_counter_sim from EFS  with status %d and value is %d for SIM1",
                 status,
                 reg_nv_max_validate_sim_counter_sim[REG_AS_ID_1]);
   
   status = efs_get("/nv/item_files/modem/nas/max_validate_sim_counter_Subscription01",
                    &reg_nv_max_validate_sim_counter_sim[REG_AS_ID_2],
                    sizeof(uint8));
   if(status < 0 || reg_nv_max_validate_sim_counter_sim[REG_AS_ID_2] == 0)
   {
     reg_nv_max_validate_sim_counter_sim[REG_AS_ID_2] = REG_NV_DEFAULT_MAX_VALIDATE_SIM_COUNTER;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_max_validate_sim_counter_sim from EFS with status %d and value is %d for SIM2",
                 status,
                 reg_nv_max_validate_sim_counter_sim[REG_AS_ID_2]);
   
#ifdef FEATURE_TRIPLE_SIM
   status = efs_get("/nv/item_files/modem/nas/max_validate_sim_counter_Subscription02",
                    &reg_nv_max_validate_sim_counter_sim[REG_AS_ID_3],
                    sizeof(uint8));
   if(status < 0 || reg_nv_max_validate_sim_counter_sim[REG_AS_ID_3] == 0)
   {
     reg_nv_max_validate_sim_counter_sim[REG_AS_ID_3] = REG_NV_DEFAULT_MAX_VALIDATE_SIM_COUNTER;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_max_validate_sim_counter_sim from EFS with status %d and value is %d for SIM3",
                 status,
                 reg_nv_max_validate_sim_counter_sim[REG_AS_ID_3]);
#endif

   status = efs_get("/nv/item_files/modem/nas/t3245_timer_test",
                    &reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1] == 0)
   {
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1] = reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_test_timer_value from EFS  with status %d and value is %d(ms) for SIM1",
                 status,
                 reg_nv_t3245_test_timer_value_sim[REG_AS_ID_1]);
   
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_test_Subscription01",
                    &reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2] == 0)
   {
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2] = reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_test_timer_value from EFS with status %d and value is %d(ms) for SIM2",
                 status,
                 reg_nv_t3245_test_timer_value_sim[REG_AS_ID_2]);

#ifdef FEATURE_TRIPLE_SIM
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_test_Subscription02",
                    &reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3],
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3] == 0)
   {
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3] = REG_TIMERS_MAX_TIMER_VALUE;
   }
   else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3] = reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3] * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_test_timer_value from EFS with status %d and value is %d(ms) for SIM3",
                 status,
                 reg_nv_t3245_test_timer_value_sim[REG_AS_ID_3]);
#endif

#else
   status = efs_get("/nv/item_files/modem/nas/t3245_timer_stored",
                    &reg_nv_t3245_timer_stored_value,
                    sizeof(rex_timer_cnt_type));
   if(status < 0)
   {
     reg_nv_t3245_timer_stored_value = REG_T3245_DEFUALT_VALUE;
   }
MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_timer_stored_value from EFS  with status %d and value is %d",
              status,
              reg_nv_t3245_timer_stored_value);


   status = efs_get("/nv/item_files/modem/nas/t3245_timer",
                    &reg_nv_validate_sim_timer_value,
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_validate_sim_timer_value == 0)
   {
     reg_nv_validate_sim_timer_value = REG_TIMERS_MAX_TIMER_VALUE;
   }
    else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_validate_sim_timer_value = reg_nv_validate_sim_timer_value * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_validate_sim_timer_value from EFS  with status %d and value is %d(ms)",
                 status,
                 reg_nv_validate_sim_timer_value);

   status = efs_get("/nv/item_files/modem/nas/max_validate_sim_counter",
                    &reg_nv_max_validate_sim_counter,
                    sizeof(uint8));
   if(status < 0)
   {
     reg_nv_max_validate_sim_counter = REG_NV_DEFAULT_MAX_VALIDATE_SIM_COUNTER;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_max_validate_sim_counter from EFS  with status %d and value is %d",
                 status,
                 reg_nv_max_validate_sim_counter);


   status = efs_get("/nv/item_files/modem/nas/t3245_timer_test",
                    &reg_nv_t3245_test_timer_value,
                    sizeof(rex_timer_cnt_type));
   if(status < 0 || reg_nv_t3245_test_timer_value == 0)
   {
     reg_nv_t3245_test_timer_value = REG_TIMERS_MAX_TIMER_VALUE;
   }
    else
   {
     //Timer value in EFS is provided in seconds convert to milliseconds.
     reg_nv_t3245_test_timer_value = reg_nv_t3245_test_timer_value * 1000;
   }
   MSG_HIGH_DS_2(REG_SUB, "=REG= NV reg_nv_t3245_test_timer_value from EFS  with status %d and value is %d",
                 status,
                 reg_nv_t3245_test_timer_value);
#endif
#if defined FEATURE_TDSCDMA || defined FEATURE_SGLTE
   status = efs_get("/nv/item_files/modem/nas/lte_available_timer_values", 
            &sglte_reg_lte_config, 
            sizeof(lte_search_timer_feature_config_T));

  if (status <0 )      
  {
    /* Failed to read, setting this value to zero so that default can be 40 sec in the overall*/
    sglte_reg_lte_config.short_lte_scan = 3;
    sglte_reg_lte_config.long_lte_scan  = 60;
    sglte_reg_lte_config.max_lte_failures = 3;
    sglte_reg_lte_config.max_lte_failures_on_neutral_cell = 2;
    MSG_HIGH_DS_3(MM_SUB ,"=REG= EFS read failed so appending default value short time %d long time %d max failures %d",
                  sglte_reg_lte_config.short_lte_scan,
                  sglte_reg_lte_config.long_lte_scan,
                  sglte_reg_lte_config.max_lte_failures);
    MSG_HIGH_DS_1(MM_SUB, "=REG= EFS read failed so appending default value max_lte_failures_on_neutral_cell %d ",
                  sglte_reg_lte_config.max_lte_failures_on_neutral_cell);
  }
  else
  {
    MSG_HIGH_DS_3(MM_SUB, "=REG= short time %d long time %d max failures %d",
                  sglte_reg_lte_config.short_lte_scan,
                  sglte_reg_lte_config.long_lte_scan,
                  sglte_reg_lte_config.max_lte_failures);
  }
#endif

#if defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE
  for ( reg_as_id=REG_AS_ID_1; reg_as_id < (reg_as_id_e_type)MAX_NAS_STACKS; reg_as_id++ )
#endif
  {
     /* --------------------------------------------------------------------                                
      ** Read the ANITE GCF FLAG value from the NV
      ** ------------------------------------------------------------------*/

    if ((status = ghdi_nvmem_read(NV_GPRS_ANITE_GCF_I,
          (ghdi_nvmem_data_T *)&reg_nv_gcf_flag)) != GHDI_SUCCESS)
    {
      reg_nv_gcf_flag = FALSE;  /* Set the default value */
      MSG_ERROR_DS_1(REG_SUB, "=REG= Unable to read NV for GCF Flag: %d", status);
    }
  }
  reg_nv_read_preferred_plmn_list();
#if defined FEATURE_DUAL_SIM || defined FEATURE_SGLTE
  reg_as_id = REG_AS_ID_1;
  reg_sub_id = reg_sub_id_stack[reg_as_id];
#endif
}

/*===========================================================================

FUNCTION reg_nv_t3245_timer_stored_value_get

DESCRIPTION
  Returns T3245 that is read from the NV.

DEPENDENCIES
  none

===========================================================================*/

rex_timer_cnt_type reg_nv_t3245_timer_stored_value_get
(
  void
)
{
    MSG_HIGH_DS_1(REG_SUB, "=REG= reg_nv_t3245_timer_stored_value is %d", reg_nv_t3245_timer_stored_value);
  return reg_nv_t3245_timer_stored_value;
}

/*===========================================================================

FUNCTION reg_nv_read_t3245_efnas_config

DESCRIPTION
  Returns T3245 that is read from the NV.

DEPENDENCIES
  none

===========================================================================*/

void reg_nv_read_t3245_efnas_config
(
  boolean *t3245_efnas_conf_enabled
)
{
    byte  *ef_nasconfig_p = NULL;
    int status = FALSE;
#ifdef FEATURE_MODEM_HEAP
  ef_nasconfig_p = (byte *)modem_mem_alloc((unsigned int)REG_NV_EFNAS_CONFIG_LENGTH, MODEM_MEM_CLIENT_NAS);
#else
  ef_nasconfig_p = (byte *)gs_alloc(REG_NV_EFNAS_CONFIG_LENGTH);
#endif
  if(ef_nasconfig_p == NULL)
  {
    MSG_ERROR_DS_0(REG_SUB, "=REG= Could not allocate the memory");    
    return;
  }
  memset((void*)ef_nasconfig_p, 0, REG_NV_EFNAS_CONFIG_LENGTH);

#ifdef FEATURE_DUAL_SIM
#ifdef FEATURE_TRIPLE_SIM
  if(reg_as_id == REG_AS_ID_3)
  {
    status = efs_get("/nv/item_files/modem/nas/efnas_config_Subscription02",
                    ef_nasconfig_p,
                    REG_NV_EFNAS_CONFIG_LENGTH);
  }
  else
#endif
  if(reg_as_id == REG_AS_ID_2)
  {
    status = efs_get("/nv/item_files/modem/nas/efnas_config_Subscription01",
                    ef_nasconfig_p,
                    REG_NV_EFNAS_CONFIG_LENGTH);
  }
  else if(reg_as_id == REG_AS_ID_1)
#endif
  {
    status = efs_get("/nv/item_files/modem/nas/efnas_config",
                    ef_nasconfig_p,
                    REG_NV_EFNAS_CONFIG_LENGTH);
  }

  if(status > 0)
  {
    MSG_HIGH_DS_1(REG_SUB, "=REG= NV reg_nv_efnas_config from EFS with status %d",status);
    reg_mode_parse_t3245_nas_config(ef_nasconfig_p,REG_NV_EFNAS_CONFIG_LENGTH,t3245_efnas_conf_enabled);
  }
#ifdef FEATURE_MODEM_HEAP
  modem_mem_free(ef_nasconfig_p, MODEM_MEM_CLIENT_NAS);
#else
  gs_free(ef_nasconfig_p);
#endif
  ef_nasconfig_p = NULL;
}



/*===========================================================================

FUNCTION reg_nv_validate_sim_timer_value_get

DESCRIPTION
  Returns T3245 that is read from the NV.

DEPENDENCIES
  none

===========================================================================*/

rex_timer_cnt_type reg_nv_validate_sim_timer_value_get
(
  void
)
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= reg_nv_validate_sim_timer_value value %d", reg_nv_validate_sim_timer_value);
  return reg_nv_validate_sim_timer_value;
}


/*===========================================================================

FUNCTION reg_nv_t3245_test_timer_value_get

DESCRIPTION
  Returns T3245 that is read from the NV.
  This value should only be used for test purposes.

DEPENDENCIES
  none

===========================================================================*/

rex_timer_cnt_type reg_nv_t3245_test_timer_value_get
(
  void
)
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= reg_nv_t3245_test_timer_value value %d", reg_nv_t3245_test_timer_value);
  return reg_nv_t3245_test_timer_value;
}


/*==============================================================================

FUNCTION NAME

  reg_nv_write_t3245_to_efs

==============================================================================*/

void reg_nv_write_t3245_to_efs(rex_timer_cnt_type t3245_value)
{
  int16 status=0;
  reg_nv_t3245_timer_stored_value = t3245_value;

#ifdef FEATURE_DUAL_SIM
#ifdef FEATURE_TRIPLE_SIM
  if(reg_as_id == REG_AS_ID_3)
  {
    status = efs_put("/nv/item_files/modem/nas/t3245_timer_stored_Subscription02" ,
                    &t3245_value,
                    sizeof(rex_timer_cnt_type), O_CREAT | O_AUTODIR, 0777);
    if(status == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Writing to NV for T3245 timer failed for SIM3");
    }
    else
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG= Wrote %d value for timer T3245 to NV for SIM3, status %d", t3245_value, status);
    }
  }
  else
#endif
  if(reg_as_id == REG_AS_ID_2)
  {
    status = efs_put("/nv/item_files/modem/nas/t3245_timer_stored_Subscription01" ,
                    &t3245_value,
                    sizeof(rex_timer_cnt_type), O_CREAT | O_AUTODIR, 0777);
    if( status == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Writing to NV for T3245 timer failed for SIM2");
    }
    else
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG= Wrote %d value for timer T3245 to NV for SIM2, status %d", t3245_value, status);
    }
  }
  else if(reg_as_id == REG_AS_ID_1)
#endif
  {
    status = efs_put("/nv/item_files/modem/nas/t3245_timer_stored" , 
               &t3245_value,
               sizeof(rex_timer_cnt_type), O_CREAT | O_AUTODIR, 0777);
    if(status == -1)
    {
      MSG_ERROR_DS_0(REG_SUB, "=REG= Writing to NV for T3245 timer failed");
    }
    else
    {
      MSG_HIGH_DS_2(REG_SUB, "=REG=  Wrote %d value for timer T3245 to NV, status %d", t3245_value, status);
    }
  }
}


/*===========================================================================

FUNCTION reg_nv_read_enhanced_hplmn_srch_tbl

DESCRIPTION
  Read ENHANCED_HPLMN_SRCH_TBL from NV.
  If read fails, populate default tbl.

DEPENDENCIES
  none

RETURN VALUE
  Whether NV read was successful or not.

SIDE EFFECTS
  none

===========================================================================*/
void reg_nv_read_enhanced_hplmn_srch_tbl
(
  byte* nv_config_tbl
)
{

  /*
  ** Fill NV command to read IMSI.
  */
  reg_nv_cmd.tcb_ptr    = rex_self();
  reg_nv_cmd.sigs       = REG_NVIF_SIG;
  reg_nv_cmd.done_q_ptr = NULL;
  reg_nv_cmd.cmd        = NV_READ_F;
  reg_nv_cmd.item       = NV_ENHANCED_HPLMN_SRCH_TBL_I;
  reg_nv_cmd.data_ptr   = &reg_nv_item;

  /*
  ** Clear the REG_NVIF_SIG signal.
  */
  (void) rex_clr_sigs(rex_self(), REG_NVIF_SIG);

  /*
  ** Send the command to NV.
  */
  reg_nv_cmd_per_subs(&reg_nv_cmd);

  /*
  ** Wait for NV to set the REG_NVIF_SIG signal.
  */
  (void) reg_wait(REG_NVIF_SIG);

  /*
  ** IMSI could not be read from NVRAM.
  */
  if (reg_nv_cmd.status != NV_DONE_S)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Reading ENHANCED_HPLMN_SRCH_TBL failed");
    /* Check ENS feature and populate default table if needed */
    reg_nv_populate_default_enhanced_hplmn_srch_tbl(nv_config_tbl);
  }
  /*
  ** TBL was successfully read from NVRAM.
  */
  else
  {
      memscpy((void*)nv_config_tbl, REG_NV_CONFIG_TBL_MAX_SIZE, (void*)(reg_nv_item.enhanced_hplmn_srch_tbl), REG_NV_CONFIG_TBL_MAX_SIZE);
      MSG_MED_DS_3(REG_SUB, "=REG= REG NV Bytes: %d, %d, %d", reg_nv_item.enhanced_hplmn_srch_tbl[0], reg_nv_item.enhanced_hplmn_srch_tbl[1], reg_nv_item.enhanced_hplmn_srch_tbl[2]);
  }
}

#ifdef FEATURE_LTE
/*===========================================================================

FUNCTION reg_nv_read_epsloci

DESCRIPTION
  Read EPSLOCI from EFS

DEPENDENCIES
  none

RETURN VALUE
  Whether NV read was successful or not.

SIDE EFFECTS
  none

===========================================================================*/

boolean reg_nv_read_epsloci(byte *epsloci)
{
  int status;
 
#ifdef FEATURE_DUAL_SIM
  if(reg_sub_id == REG_AS_ID_2)
  {
    status = efs_get("/nvm/alpha/modem/nas/lte_nas_eps_loci_Subscription01", 
                                epsloci, 
                                REG_EPSLOCI_LENGTH);
  }
  else
#endif
  {
    status = efs_get("/nvm/alpha/modem/nas/lte_nas_eps_loci", 
                                  epsloci, 
                                  REG_EPSLOCI_LENGTH);
  }
  return (status > 0);  
}

#ifdef FEATURE_MMSS_3_1
/*===========================================================================

FUNCTION reg_nv_read_ignore_uplmn

DESCRIPTION
 Whether to ignore uplmn or not

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_read_ignore_uplmn(void)
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= Ignore UPLMN = %d", reg_nv_ignore_uplmn);
  return reg_nv_ignore_uplmn;
}
#endif /*FEATURE_MMSS_3_1*/

#endif /*FEATURE_LTE*/

#ifdef FEATURE_EQUIVALENT_HPLMN
/*===========================================================================

FUNCTION reg_nv_read_efs_ehplmn_file

DESCRIPTION
  Read EHPLMN list from EFS

  Reading from EFS NV failed or NV is set with invalid EHPLMN list length
  (length greater than max supported length 20 or length equal to 0) this function will set ehplmn_list length as 0.
  WHEN NV read is successful and EHPLMN list length is valid this function will copy the EHPLMN list from NV
  into the ehplmn_list.


DEPENDENCIES
  none

RETURN VALUE
  TRUE/FALSE depending on whether valid EHPLMN list is successfully read from NV or not.

SIDE EFFECTS
  none

===========================================================================*/
boolean  reg_nv_read_efs_ehplmn_file
(
  reg_sim_plmn_list_s_type   *ehplmn_list
)
{
  int                 status;
  /*
  **EFS NV will contain length of the list in the first byte and length of the list *3 entries 
  **containing valid PLMN IDs. Total length of EFS NV item is 1+REG_EFS_EHPLMN_MAX_LENGTH*3
  */
  byte               plmn_list[REG_EFS_EHPLMN_MAX_LENGTH*3+1];
  sys_plmn_id_s_type       hplmn;
  uint32 i,j;
  boolean                  found_plmn = FALSE;

  hplmn = reg_sim_read_hplmn();
  for( j=0; j< REG_NV_EFS_EHPLMN_LIST; j++)
  {
    reg_timers_pet_watchdog();
    switch(j+1)
    {
      case 1:       
        status = efs_get("/nv/item_files/modem/nas/ehplmn", 
                              &plmn_list[0], 
                              (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);
        break;
      case 2:
        status = efs_get("/nv/item_files/modem/nas/ehplmn_Subscription01", 
                              &plmn_list[0], 
                              (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);
        break;
      case 3:
        status = efs_get("/nv/item_files/modem/nas/ehplmn_Subscription02", 
                               &plmn_list[0], 
                               (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);      
        break;
      case 4:
        status = efs_get("/nv/item_files/modem/nas/ehplmn_Subscription03", 
                               &plmn_list[0], 
                               (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);      
        break;
      case 5:
        status = efs_get("/nv/item_files/modem/nas/ehplmn_Subscription04", 
                               &plmn_list[0], 
                               (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);      
        break;
      case 6:
        status = efs_get("/nv/item_files/modem/nas/ehplmn_Subscription05", 
                               &plmn_list[0], 
                               (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);      
        break;
      default:
        break;
    }
    ehplmn_list->length=0;
    if (status >0)
    {
      MSG_MED_DS_1(REG_SUB,"=REG= Reading from EFS NV succeeded %d",status);
      /*Update EHPLMN list length as first byte read from the NV*/
      ehplmn_list->length = plmn_list[0];
      if(( ehplmn_list->length <= REG_EFS_EHPLMN_MAX_LENGTH) && (ehplmn_list->length != 0))
      {
        MSG_HIGH_DS_1(REG_SUB, "=REG= Received valid EHPLMN list length %d", ehplmn_list->length);
        memscpy( &ehplmn_list->plmn[0], sizeof(sys_plmn_id_s_type)*REG_SIM_PLMN_LIST_LENGTH, &plmn_list[1], ehplmn_list->length*3);
#ifdef FEATURE_TDSCDMA
      if (reg_mode_is_usim_compliant())
      {
        found_plmn = TRUE;
        MSG_HIGH_DS_0(REG_SUB, "=REG= SIM compliant use the EHPLMNs");
        break;
      }
#endif
        for (i = 0; i < ehplmn_list->length; ++i)
        {
          if ( sys_plmn_match( hplmn, ehplmn_list->plmn[i]))   
          {              
            found_plmn = TRUE;
            MSG_HIGH_DS_0(REG_SUB, "=REG= Match between HPLMN IMSI and EHPLMN");
            break;
          }
        }
      }
      else
      {
        MSG_HIGH_DS_1(REG_SUB, "=REG= Invalid EHPLMN list length %d is received ", ehplmn_list->length);
        /*EHPLMN list length is invalid. update length as 0*/
        ehplmn_list->length=0;
      }     
    }
    else
    {
      MSG_HIGH_DS_1(REG_SUB, "=REG= Reading from EFS EHPLMN NV failed %x", status);
      found_plmn = TRUE;
    }
    if(found_plmn)          
    {
      break;
    }
    else
    {
      continue;
    }
  }

  if(found_plmn == FALSE)
  {
    ehplmn_list->length=0;
  }
  return (ehplmn_list->length != 0);
}
#ifdef FEATURE_TDSCDMA
/*===========================================================================

FUNCTION reg_nv_read_tdscdma_plmn_file

DESCRIPTION
  Read PLMN list from EFS and populates a local database

  Used for TDSCDMA carrier specific requests


DEPENDENCIES
  none

RETURN VALUE
  TRUE/FALSE depending on whether valid EHPLMN list is successfully read from NV or not.

SIDE EFFECTS
  none

===========================================================================*/
boolean  reg_nv_read_tdscdma_plmn_file (void)
{
  int                 status;
 
  /*
  **EFS NV will contain length of the list in the first byte and length of the list *3 entries 
  **containing valid PLMN IDs. Total length of EFS NV item is 1+REG_EFS_EHPLMN_MAX_LENGTH*3
  */
  byte               plmn_list[REG_EFS_EHPLMN_MAX_LENGTH*3+1];
  
  
  status = efs_get("/nv/item_files/modem/nas/tdscdma_op_plmn_list", 
                      &plmn_list[0], 
                    (REG_EFS_EHPLMN_MAX_LENGTH*3)+1);
  
  reg_nv_tdscdma_op_plmn_efs_list.length = 0;
  if (status >0)
  {
    MSG_MED_DS_1(REG_SUB, "=REG= Reading from TDSCDMA OP PLMN LIST EFS NV succeeded %d", status);
    /*Update PLMN list length as first byte read from the NV*/
    reg_nv_tdscdma_op_plmn_efs_list.length = plmn_list[0];
    if(( reg_nv_tdscdma_op_plmn_efs_list.length <= REG_EFS_EHPLMN_MAX_LENGTH) && (reg_nv_tdscdma_op_plmn_efs_list.length != 0))
    {
      MSG_HIGH_DS_1(REG_SUB, "=REG= Read a valid TDSCDMA_PLMN list length %d", reg_nv_tdscdma_op_plmn_efs_list.length);
      memscpy( &reg_nv_tdscdma_op_plmn_efs_list.plmn[0],sizeof(sys_plmn_id_s_type)*REG_SIM_PLMN_LIST_LENGTH, &plmn_list[1], reg_nv_tdscdma_op_plmn_efs_list.length*3);      
      return TRUE;
    }
    else
    {
      MSG_HIGH_DS_1(REG_SUB, "=REG= Invalid TDSCDMA PLMN list length %d is received ", 
                    reg_nv_tdscdma_op_plmn_efs_list.length);
      /*PLMN list length is invalid. update length as 0*/
      reg_nv_tdscdma_op_plmn_efs_list.length=0;
      return FALSE;
    }
  }
  else
  {
    MSG_HIGH_DS_1(REG_SUB, "=REG= Reading from EFS PLMN NV failed %x", status);
    return FALSE;
  }
}

#endif
#endif

#if defined FEATURE_TDSCDMA || defined FEATURE_SGLTE
int reg_nv_read_lte_cell_lists(void)
{
  int16 status = 0;
  uint32 data_size = 0;
  uint32 total_size=0;
  byte *byte_array = NULL;

  
  struct fs_stat  nas_conf_stat;

//  memset((void *)nas_conf_stat,0,sizeof(nas_conf_stat));

  nas_conf_stat.st_size = 0;
  if (efs_stat("/nv/item_files/modem/nas/lte_avaialable_cell_list", &nas_conf_stat) == -1)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= efs stat failed");                             
  }
  else
  {
    MSG_HIGH_DS_1(REG_SUB, "=REG= lenght of file is %d",nas_conf_stat.st_size);           
  }

  total_size = nas_conf_stat.st_size;
  
  byte_array = (byte * )modem_mem_alloc(total_size,MODEM_MEM_CLIENT_NAS);
      status = efs_get("/nv/item_files/modem/nas/lte_avaialable_cell_list", 
                                                     byte_array, 
                                                      total_size);
  if (status!=-1)
  {
    memscpy((void *)&data_size,sizeof(data_size),(void*)&byte_array[0],sizeof(uint32));
    if (data_size == 0 )
    {
      MSG_HIGH_DS_0(REG_SUB, "=REG= LTE avaialble list total size is zero no reading further");             
    }
    else
    {
      lte_available_list_ptr = NULL;
      lte_available_list_ptr = reg_convert_byte_array_to_lte_cell_list(lte_available_list_ptr,byte_array);
    }
  } 
  else
  {
    MSG_HIGH_DS_1(MM_SUB, "=REG= lte avaialble list efs reading fail, error value %d ", efs_errno);         
  }

  if(byte_array != NULL)
  {
    modem_mem_free(byte_array,MODEM_MEM_CLIENT_NAS);
  }

  data_size = 0;
  total_size=0; 
  byte_array = NULL;
  status = 0;


 // memset((void *)nas_conf_stat,0,sizeof(nas_conf_stat));

  nas_conf_stat.st_size = 0;
  if (efs_stat("/nv/item_files/modem/nas/lte_non_avaialable_cell_list", &nas_conf_stat) == -1)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= efs stat failed");
  }
  else
  {
    MSG_HIGH_DS_1(REG_SUB, "=REG= lenght of file is %d", nas_conf_stat.st_size);
  }

  total_size = nas_conf_stat.st_size;

  byte_array = (byte * )modem_mem_alloc(total_size,MODEM_MEM_CLIENT_NAS);
      status = efs_get("/nv/item_files/modem/nas/lte_non_avaialable_cell_list", 
                                                           byte_array, 
                                                      total_size);

  if (status!=-1)
  {
    memscpy((void *)&data_size,sizeof(data_size),(void*)&byte_array[0],sizeof(uint32));
    if (total_size == 0 )
    {
      MSG_HIGH_DS_0(REG_SUB,"=REG= LTE not avaialble list total size is zero no reading further");            
    }
    else
    {
      lte_non_available_list_ptr = NULL;
      lte_non_available_list_ptr = reg_convert_byte_array_to_lte_cell_list(lte_non_available_list_ptr,byte_array);
    }
  } 
  else
  {
    MSG_HIGH_DS_0(MM_SUB, "=REG= lte not avaialble list list efs reading fail");                           
  }
  if(byte_array != NULL)
  {
    modem_mem_free(byte_array,MODEM_MEM_CLIENT_NAS);
  }
  return status;
}
int  reg_nv_write_lte_lists_to_efs(void)
{
  int16 efs_status = 0;
  uint32 data_size=0;
  byte *byte_array = NULL;
  int i;

  byte_array = reg_convert_lte_list_into_array(lte_available_list_ptr,&data_size);
  if (byte_array == NULL)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Array memory failed efs writing fail");                                     
  }
  else
  {
    for(i =0; i<byte_array[0]; i++)
    {
      MSG_HIGH_DS_1(REG_SUB, "=REG= byte_array[i] = %d", byte_array[i]);                                                          
    }

    efs_status = efs_put("/nv/item_files/modem/nas/lte_avaialable_cell_list" ,byte_array,
                             data_size, O_RDWR|O_CREAT| O_AUTODIR, 0777);  
    if(efs_status == -1)
    {
      MSG_HIGH_DS_0(REG_SUB, "=REG= lte avaialble list efs writing fail");
    }
  }

  if(byte_array != NULL)
  {
    modem_mem_free(byte_array,MODEM_MEM_CLIENT_NAS);
  }

  efs_status = 0;
  data_size=0;
  byte_array = NULL;
  byte_array = reg_convert_lte_list_into_array(lte_non_available_list_ptr,&data_size);
  if (byte_array == NULL)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= Array memmory failed efs writing fail");                                          
  }
  else
  {
    for(i =0; i<byte_array[0]; i++)
    {
       MSG_HIGH_DS_2(REG_SUB, "=REG= byte_array[%d] = %d", i, byte_array[i]);
    }
    efs_status = efs_put("/nv/item_files/modem/nas/lte_non_avaialable_cell_list" ,byte_array,
                             data_size, O_RDWR|O_CREAT| O_AUTODIR, 0777);  
    if(efs_status == -1)
    {
      MSG_HIGH_DS_0(REG_SUB, "=REG= lte non avaialble list efs writing fail");
    }
  }
  if(byte_array != NULL)
  {
    modem_mem_free(byte_array,MODEM_MEM_CLIENT_NAS);
  }
//  reg_state_clear_lte_lists();
//  reg_timers_stop_backoff_lte_search_period_timer();
  return efs_status;
}

int reg_nv_clear_non_available_efs_list(void)
{
  int16 efs_status = 0;
  uint32 length = 0;
  uint32 data_size = sizeof(uint32);
  efs_status = efs_put("/nv/item_files/modem/nas/lte_non_avaialable_cell_list" ,&length,
                             data_size, O_RDWR|O_CREAT| O_AUTODIR, 0777);
  if(efs_status == -1)
  {
     MSG_HIGH_DS_0(REG_SUB, "=REG= lte non available list efs writing fail");       
  }
  reg_timers_stop_backoff_lte_search_period_timer();
  return efs_status;
}

int reg_nv_clear_available_efs_list(void)
{
  int16 efs_status = 0;
  uint32 length = 0;
  uint32 data_size = sizeof(uint32);
  efs_status = efs_put("/nv/item_files/modem/nas/lte_avaialable_cell_list" ,&length,
                             data_size, O_RDWR|O_CREAT| O_AUTODIR, 0777);  
  if(efs_status == -1)
  {
    MSG_HIGH_DS_0(REG_SUB, "=REG= lte available list efs writing fail");                        
  }
  return efs_status;
}

#endif
/*===========================================================================

FUNCTION reg_nv_is_imsi_switch_enabled

DESCRIPTION
 Whether to Switch the IMSI or not after trying registration on all the available PLMNs

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_is_imsi_switch_enabled()
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= NV IMSI Switch = %d", reg_nv_imsi_switch);
  return reg_nv_imsi_switch;
}

/*===========================================================================

FUNCTION reg_nv_is_wcdma_freq_lock_enabled

DESCRIPTION
 Whether to Enable the UE to lock to ATT enaled WCDMA FREQ or NOT

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_is_wcdma_freq_lock_enabled()
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= NV WCDMA FREQ LOCK enabled flag = %d", reg_nv_wcdma_freq_lock_enabled);
  return reg_nv_wcdma_freq_lock_enabled;
}

/*===========================================================================

FUNCTION reg_nv_is_lpm_power_off_enabled

DESCRIPTION
 Whether to write NV/EFS item on LPM

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_is_lpm_power_off_enabled(void)
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= LPM power off behavior = %d", reg_nv_lpm_power_off);
  return reg_nv_lpm_power_off;  
}


/*===========================================================================

FUNCTION reg_nv_efrplmnsi_select_rplmn

DESCRIPTION
 This function return TRUE is UE should select RPLMN is HPMN is not found. 

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_efrplmnsi_select_rplmn(void)
{ 
  return reg_nv_efrplmnsi_selection;
}
#if defined FEATURE_TDSCDMA || defined FEATURE_SGLTE
/*===========================================================================

FUNCTION reg_nv_is_forced_irat_enabled

DESCRIPTION
 This function return TRUE is forced IRAT is enabled. 

DEPENDENCIES
  none

RETURN VALUE

SIDE EFFECTS
  none

===========================================================================*/
boolean reg_nv_is_forced_irat_enabled(void)
{     
    return reg_nv_forced_irat_enabled;
}
/*==============================================================================

FUNCTION NAME

  reg_nv_tdscdma_plmn_list_get

==============================================================================*/
reg_sim_plmn_list_s_type* reg_nv_tdscdma_plmn_list_get(void)
{
       return &reg_nv_tdscdma_op_plmn_efs_list; 
}
/*==============================================================================

FUNCTION NAME

  reg_nv_hplmn_irat_search_timer_set

==============================================================================*/
void reg_nv_hplmn_irat_search_timer_set(uint32 timer)
{
  reg_nv_hplmn_irat_search_timer = timer;
  MSG_HIGH_DS_1(REG_SUB, "=REG= Set HPLMN IRAT NV Search Timer to %d", timer);
}
/*==============================================================================

FUNCTION NAME

  reg_nv_hplmn_irat_search_timer_set

==============================================================================*/
uint32 reg_nv_hplmn_irat_search_timer_get()
{
  return reg_nv_hplmn_irat_search_timer;
}

#endif

/*===========================================================================

FUNCTION reg_nv_gcf_flag_get

===========================================================================*/

rex_timer_cnt_type reg_nv_gcf_flag_get
(
  void
)
{
  MSG_HIGH_DS_1(REG_SUB, "=REG= reg_nv_gcf_flag value set to %d", reg_nv_gcf_flag);
  return reg_nv_gcf_flag;
}

#ifdef FEATURE_TDSCDMA
/*==============================================================================

FUNCTION NAME

  reg_mode_ehplmn_list_set

==============================================================================*/
void reg_nv_ehplmn_list_set(reg_sim_plmn_list_s_type ehplmn_list_efs)
{
   uint32 i;   
   reg_nv_ehplmn_efs_list.length = ehplmn_list_efs.length;

   for(i = 0; i < ehplmn_list_efs.length; ++i)
   {
     reg_nv_ehplmn_efs_list.plmn[i] = ehplmn_list_efs.plmn[i];
   }
}


/*==============================================================================

FUNCTION NAME

  reg_nv_ehplmn_list_get

==============================================================================*/
void reg_nv_ehplmn_list_get(reg_sim_plmn_list_s_type* ehplmn_list_efs)
{
   uint32 i;
   
   ehplmn_list_efs->length = reg_nv_ehplmn_efs_list.length;

   for(i = 0; i < reg_nv_ehplmn_efs_list.length; ++i)
   {
     ehplmn_list_efs->plmn[i] = reg_nv_ehplmn_efs_list.plmn[i];
   }
}

#endif

/*==============================================================================

FUNCTION NAME

  reg_nv_max_validate_sim_counter_get
  DESCRIPTION
  This function returns the setting of max validate sim counter value read from NV.
==============================================================================*/
uint8 reg_nv_max_validate_sim_counter_get(void)
{
   MSG_HIGH_DS_1(REG_SUB, "=REG= reg_nv_max_validate_sim_counter value set to %d", reg_nv_max_validate_sim_counter);
   return reg_nv_max_validate_sim_counter;
}



