/**
  @file policy_rat_capability.c

  @brief  Policy to modify the RAT capability of the device based on the IIN
          of the current SIM.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_rat_capability.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_rat_capability.h"
#include "policyman_cfgitem.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_efs.h"
#include "policyman_rf.h"
#include "policyman_set.h"
#include "policyman_sglte_i.h"
#include "policyman_state.h"
#include "policyman_subs.h"
#include "policyman_svc_mode.h"
#include "policyman_task.h"
#include "policyman_ue_mode.h"
#include "policyman_util.h"
#include "policyman_xml.h"

#include "sys.h"
#include <stringl/stringl.h>


#define RAT_CAPABILITY_TAG         "rat_capability"
#define RAT_CAPABILITY_IF_TAG      "rat_capability_if"
#define POLICYMAN_RAT_MASK_NV_PATH "/policyman/rat_mask"



/*=============================================================================
  Rat capability methods for cfgitem
=============================================================================*/

/*-------- policyman_rat_capability_item_new --------*/
policyman_item_t *
policyman_rat_capability_item_new(
  policyman_state_t       *pState,
  uint32                  mask,
  sys_modem_as_id_e_type  subs
  )
{
  rat_capability_t            *prc;
  policyman_rf_device_info_t  deviceInfo;
  
  prc = (rat_capability_t *) policyman_cfgitem_new_item(
                                      POLICYMAN_ITEM_RAT_CAPABILITY,
                                      sizeof(rat_capability_t),
                                      policyman_simple_dtor
                                      );

  /*  Filter the RATs based on hardware capabilities.
   */
  policyman_state_get_rf_info(pState, 0, &deviceInfo);
  prc->ratMask = mask & deviceInfo.ratMask;
  if(mask != prc->ratMask)
  {
    POLICYMAN_MSG_HIGH_2("Filtered RAT mask %d based on HW capabilities %d", prc->ratMask, deviceInfo.ratMask); 
  }

  prc->asubs_id = subs;

  return (policyman_item_t *) prc;
}


/*-------- policyman_rat_capability_get_default_from_efs --------*/
policyman_item_t *
policyman_rat_capability_get_default_from_efs(
  sys_modem_as_id_e_type subs_id
)
{  
  policyman_item_t            *prc = NULL;

  uint32                      ratMask;
  sys_band_mask_type          gwBands, lteBands, tdsBands;

  /* read EFS for RAT/BAND capability
   */
  if(!policyman_retrieve_rats_bands(subs_id, &ratMask, &gwBands, &lteBands, &tdsBands))
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't populate RAT item from EFS", subs_id);
    goto Done;
  }

  prc = policyman_rat_capability_item_new(
            policyman_state_get_state(),
            ratMask,
            subs_id
            );

Done:

  return (policyman_item_t *)prc;
}


/*-------- policyman_rat_capability_get_default --------*/
policyman_item_t *
policyman_rat_capability_get_default(
  sys_modem_as_id_e_type subsId
  )
{
  policyman_rf_device_info_t  *pInfo  = policyman_rf_get_overall_info();
  uint32                      mask    = (SYS_SYS_MODE_MASK_GSM);

  /* The multimode subs gets hardware RAT capabilities, all other subs get GSM only.
   */
  if(    subsId == policyman_get_current_multimode_subs()
      && pInfo != NULL
    )
  {    
    mask = pInfo->ratMask;

    /* Don't allow CDMA and HDR for SUBS2, SUBS3...
      */
    if(subsId != SYS_MODEM_AS_ID_1)
    {
      mask &= ~(SYS_SYS_MODE_MASK_CDMA|SYS_SYS_MODE_MASK_HDR);
    }
  }

  POLICYMAN_MSG_HIGH_2("Default RAT capability for subs %d: mask = 0x%04x", subsId, mask);
  return policyman_rat_capability_item_new(
              policyman_state_get_state(),
              mask,
              subsId
              );
}


/*-------- policyman_rat_capability_compare --------*/
boolean
policyman_rat_capability_compare(
  policyman_item_t  *pData1,
  policyman_item_t  *pData2
  )
{
  rat_capability_t  *prc1 = (rat_capability_t *) pData1;
  rat_capability_t  *prc2 = (rat_capability_t *) pData2;

  return    prc1 != NULL
         && prc2 != NULL
         && prc1->ratMask == prc2->ratMask;
}


/*-------- policyman_rat_capability_display --------*/
void policyman_rat_capability_display
(
  policyman_item_t  *pItem
)
{
  rat_capability_t  *prc = (rat_capability_t *)pItem;

  POLICYMAN_MSG_HIGH_2("Rat capability for subs %d: mask = 0x%04x",
                      prc->asubs_id, prc->ratMask);
}

/*-----------------------------------------------------------------------------
  rat_capability action
-----------------------------------------------------------------------------*/


typedef struct
{
  POLICYMAN_ACTION_BASE;

  rat_capability_info_t rat_info;
  sys_modem_as_id_e_type asubs_id;  
} rat_capability_action_t;


#define INVALID_RAT_MASK  0xffffffff




/*-------- policyman_get_rat_capability --------*/
policyman_status_t
policyman_get_rat_capability(
  policyman_item_t const  *pItem,
  uint32                  *pRatCfg
  )
{
  rat_capability_t  *prc = (rat_capability_t *) pItem;

  if (prc == NULL || pRatCfg == NULL)
  { 
    POLICYMAN_MSG_ERROR_0("get_rat_capability returned err POLICYMAN_STATUS_ERR_INVALID_ARGS");
    return POLICYMAN_STATUS_ERR_INVALID_ARGS; 
  }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_RAT_CAPABILITY)
  { 
    POLICYMAN_MSG_ERROR_0("get_rat_capability returned err POLICYMAN_STATUS_ERR_INVALID_ITEM_ID");
    return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; 
  }
  
  *pRatCfg = prc->ratMask;
  POLICYMAN_MSG_HIGH_1("get_rat_capability returned 0x%04x", prc->ratMask);
  return POLICYMAN_STATUS_SUCCESS;
}





/*-------- policyman_rat_capability_str_to_rat --------*/
uint32
policyman_rat_capability_str_to_rat(
  const char  *pRatName
  )
{
  rat_map *rat_item;
  uint32  mask = INVALID_RAT_MASK;

  rat_item = policyman_util_str_to_rat(pRatName);

  if (NULL != rat_item)
    {
    mask = rat_item->mask;
  }

  return mask;
}



/*-------- policyman_rat_config_parse_rats --------*/
boolean
policyman_rat_config_parse_rats(
  char const  *pStr,
  uint32      *pMask
  )
{
  char    token[32];
  boolean succeeded = TRUE;
  uint32  mask;

  while (policyman_get_token(&pStr, token, sizeof(token)))
  {
    mask = policyman_rat_capability_str_to_rat(token);
    if (mask == INVALID_RAT_MASK)
    {
      POLICYMAN_UT_MSG("invalid RAT specified: %s", token, 0, 0);
      succeeded = FALSE;
      break;
    }
    *pMask |= mask;
  }

  return succeeded;
}


/*-------- policyman_rat_capability_read_info --------*/
policyman_status_t
policyman_rat_capability_read_info(
  policyman_xml_element_t const *pElem,
  rat_capability_info_t         *pInfo
  )
{
  policyman_status_t status = POLICYMAN_STATUS_ERR_INVALID_ACTION;
  size_t            nChildren;
  size_t            i;

  pInfo->base = POLICYMAN_BASE_NONE;
  pInfo->includeMask = 0;
  pInfo->excludeMask = 0;

  if (!policyman_util_get_base(pElem, &pInfo->base))        { goto Done; }

  nChildren = policyman_xml_num_children(pElem);
  if (nChildren == 0)
  {
    status = POLICYMAN_STATUS_SUCCESS;
    goto Done;
  }

  for (i = 0 ; i < nChildren ; ++i)
  {
    policyman_xml_element_t const *pChild;
    char const                  *pStr;

    pChild = policyman_xml_get_child(pElem, i);
    if (pChild == NULL)
    {
      POLICYMAN_MSG_ERROR_1("Internal error - no element #%d", i);
      goto Done;
    }
      
    pStr = policyman_xml_get_text(pChild);
    if      (policyman_xml_tag_is(pChild, "include"))
    {
      if (!policyman_rat_config_parse_rats(pStr, &pInfo->includeMask)) { goto Done; }
    }
    else if (policyman_xml_tag_is(pChild, "exclude"))
    {
      if (!policyman_rat_config_parse_rats(pStr, &pInfo->excludeMask)) { goto Done; }
    }
    else
    {
      goto Done;
    }
  }

  status = POLICYMAN_STATUS_SUCCESS;

Done:
  return status;
}


/*-------- policyman_get_configured_rats --------*/
static uint32
policyman_get_configured_rats(
  void
  )
{
  policyman_item_t const  *pItem;
  uint32                  mask;

  pItem = policyman_cfgitem_get_item(
              POLICYMAN_ITEM_DEVICE_CONFIGURATION,
              SYS_MODEM_AS_ID_1
              );

  if (pItem == NULL)
  {
    POLICYMAN_ERR_FATAL("Unable to get device configuration for <rat_capability> base=\"config\"", 0, 0, 0);
  }

  mask = device_config_get_config(pItem, 0, 0);
  policyman_item_release(pItem);

  return mask;
}


/*-------- policyman_rat_capability_evaluate --------*/
uint32
policyman_rat_capability_evaluate(
  rat_capability_info_t *pInfo,
  policyman_state_t     *pState,
  policyman_set_t       *pItemSet
  )
{
  uint32                      mask = 0;
  policyman_rf_device_info_t  deviceInfo;
  device_configuration_t      **ppItem = NULL;
    
  switch (pInfo->base)
  {
    case POLICYMAN_BASE_NONE:
      mask = 0;
      break;

    case POLICYMAN_BASE_CONFIG:
      if (NULL != pItemSet)
      {
        ppItem = policyman_cfgitem_find_item_in_itemset( pItemSet, POLICYMAN_ITEM_DEVICE_CONFIGURATION, SYS_MODEM_AS_ID_1 );
      }
      
      if (NULL != ppItem)
      {
        mask = device_config_get_config(*ppItem, 0, 0);
      }
      else
      {
        mask = policyman_get_configured_rats();
      }
      break;

    case POLICYMAN_BASE_HARDWARE:
      policyman_state_get_rf_info(pState, 0, &deviceInfo);
      mask = deviceInfo.ratMask;
      break;

    default:
      POLICYMAN_MSG_ERROR_1("invalid base type: %d", pInfo->base);
      break;
  }

  mask |= pInfo->includeMask;
  mask &= ~(pInfo->excludeMask);

  return mask;
}



/*-------- policyman_rat_capability_execute --------*/
static boolean
policyman_rat_capability_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  rat_capability_action_t *prcAction = (rat_capability_action_t *) pAction;
  policy_execute_ctx_t    *prcCtx = (policy_execute_ctx_t *) pCtx;
  uint32                  mask;
  policyman_item_t        *prc = NULL;
  policyman_state_t       *pState = prcCtx->pState;
  policyman_set_t         *pItemSet = prcCtx->pItemSet;

  mask = policyman_rat_capability_evaluate(&prcAction->rat_info, pState, pItemSet);
  prc = policyman_rat_capability_item_new(pState, mask, prcAction->asubs_id);
  policyman_set_replace(pItemSet, &prc);
  ref_cnt_obj_release(prc);

  POLICYMAN_MSG_HIGH_2("action <rat_capability> set mask to 0x%04x for subs %d", mask, prcAction->asubs_id);

  return TRUE;
}

/*-------- policyman_rat_capability_if_execute --------*/
static boolean
policyman_rat_capability_if_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean succeeded = FALSE;
  rat_capability_action_t *pratAction = (rat_capability_action_t *)pAction;

  succeeded = policyman_cfgitem_is_present(
                    POLICYMAN_ITEM_RAT_CAPABILITY,
                    pratAction->asubs_id
                    );

  if (succeeded)
  {
    POLICYMAN_UT_MSG_0("<rat_capability_if> skipping update - item present");
  }
  else
  {
    succeeded = policyman_rat_capability_execute(pAction, pCtx);
  }

  return succeeded;
}


/*-------- policyman_rat_capability_create_new_action --------*/
mre_status_t
policyman_rat_capability_create_new_action(
  policyman_xml_element_t const *pElem,
  policyman_policy_t            *pPolicy,
  policyman_action_t            **ppAction,
  pfn_execute_t                 executefn
  )
{
  mre_status_t      status = MRE_STATUS_ERR_INVALID_ACTION;
  rat_capability_action_t *pAction = NULL;
  sys_modem_as_id_e_type  subs;

  pAction = (rat_capability_action_t *)
                policyman_mem_alloc(sizeof(rat_capability_action_t));
  
  ref_cnt_obj_init(pAction, policyman_action_dtor);
  pAction->execute = executefn;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if(MRE_FAILED(status))
  {
    goto Done;
  }
  pAction->asubs_id = subs;

  status = policyman_rat_capability_read_info(pElem, &pAction->rat_info);

Done:
  if(MRE_SUCCEEDED(status))
  {
    *ppAction = (policyman_action_t *) pAction;
  }
  else
  {
    POLICYMAN_UT_MSG("<rat_capability>: invalid XML", 0, 0, 0);
    REF_CNT_OBJ_RELEASE_IF(pAction);
  }

  return status;
}


/*-------- policyman_rat_capability_action_new --------*/
mre_status_t
policyman_rat_capability_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_rat_capability_create_new_action(
                pElem, (policyman_policy_t *) pPolicy, ppAction,
                (pfn_execute_t) policyman_rat_capability_execute);
}

/*-------- policyman_rat_capability_if_action_new --------*/
mre_status_t
policyman_rat_capability_if_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
 return policyman_rat_capability_create_new_action(
          pElem, (policyman_policy_t *) pPolicy, ppAction,
          (pfn_execute_t) policyman_rat_capability_if_execute);
}

/*-------- policyman_rat_capability_full_rat_mode --------*/
void
policyman_rat_capability_full_rat_mode(
  policyman_set_t       *pItemSet,
  sys_ue_mode_e_type    ue_mode,
  policyman_svc_mode_t  svc_mode
  )
{
  policyman_svc_mode_item_t *pSvcItem;
  policyman_ue_mode_item_t  *pUeItem;
  policyman_item_t          *pRatItem;
  policyman_item_t          *pRfBandsItem;
  policyman_state_t         *pState = policyman_state_get_state();
  sys_modem_as_id_e_type    subs = policyman_get_current_multimode_subs();

  policyman_rf_device_info_t    rfInfo;

  POLICYMAN_MSG_HIGH_0("Moving to full RAT mode to find service");

  // Add UE mode to set
  pUeItem = policyman_ue_mode_item_new(ue_mode, subs);
  policyman_set_replace(pItemSet, &pUeItem);
  ref_cnt_obj_release(pUeItem);

  // Add SVC mode to set - FULL RAT mode
  pSvcItem = policyman_svc_mode_item_new(svc_mode, subs);
  policyman_set_replace(pItemSet, &pSvcItem);
  ref_cnt_obj_release(pSvcItem);
  
  // Get RF capabilities
  (void) policyman_state_get_rf_info(pState, 0, &rfInfo);

  // Add full rat capability to set
  pRatItem = policyman_rat_capability_item_new(pState, rfInfo.ratMask, subs);
  policyman_set_replace(pItemSet, &pRatItem);
  ref_cnt_obj_release(pRatItem);

  // Add all supported bands to set
  pRfBandsItem = policyman_rf_bands_item_new(rfInfo.gwBands, rfInfo.lteBands, rfInfo.tdsBands, FALSE);
  policyman_set_replace(pItemSet, &pRfBandsItem);
  ref_cnt_obj_release(pRfBandsItem);

  // Reset PLMN validity in CM state
  (void) policyman_ss_set_subs_srv_info_valid(pState->pSubsState[subs]->pSsInfo, FALSE);
}


typedef struct
{
  POLICYMAN_CMD_HDR;

  sys_ue_mode_e_type    ue_mode;
  policyman_svc_mode_t  svc_mode;
} policyman_full_rat_mode_cmd_t;



/*-------- policyman_rat_capability_full_rat_execute --------*/
/**
@brief  Called when the policyman_full_rat_mode_cmd_t is executed.  This
        updates the configuration database to move to full RAT mode
        in an attempt to find service.

@param

@return
  
*/
static void
policyman_rat_capability_full_rat_execute(
  policyman_cmd_t *pCmd
  )
{
  policyman_full_rat_mode_cmd_t *pFullRatCmd = (policyman_full_rat_mode_cmd_t *) pCmd;
  policyman_set_t               *pItemSet = policyman_itemset_new();
  policyman_item_t              *pro;

  policyman_rat_capability_full_rat_mode( pItemSet,
        pFullRatCmd->ue_mode,
        pFullRatCmd->svc_mode
        );

  /* Include rat_order information */
  pro = policy_sglte_get_rat_order_for_ueMode(pFullRatCmd->ue_mode, pFullRatCmd->pPolicy);
  if (pro != NULL)
  {
    policyman_set_add(pItemSet, &pro);
    ref_cnt_obj_release(pro);
  }

  /* Update the configuration database
  */
  policyman_cfgitem_update_items(pItemSet, policyman_get_current_multimode_subs());
  ref_cnt_obj_release(pItemSet);
}


/*-------- policyman_rat_capability_start_full_rat --------*/
void
policyman_rat_capability_start_full_rat(
  sys_ue_mode_e_type    ue_mode,
  policyman_svc_mode_t  svc_mode,
  policyman_policy_t    *pPolicy
  )
{
  policyman_full_rat_mode_cmd_t *pCmd;
  
  pCmd = (policyman_full_rat_mode_cmd_t *) policyman_cmd_new(
                                              sizeof(policyman_full_rat_mode_cmd_t),
                                              policyman_rat_capability_full_rat_execute,
                                              NULL,
                                              pPolicy
                                              );

  pCmd->ue_mode = ue_mode;
  pCmd->svc_mode = svc_mode;

  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
  ref_cnt_obj_release(pCmd);
}

/*-------- policyman_retrieve_rats_bands --------*/
boolean
policyman_retrieve_rats_bands(
  sys_modem_as_id_e_type  subsId,
  uint32                  *pRatMask,
  sys_band_mask_type      *pGWBand,
  sys_band_mask_type      *pLTEBand,
  sys_band_mask_type      *pTDSBand
  )
{
  int                       fd;
  struct fs_stat            st;

  policyman_efs_status_t    efsStatus = POLICYMAN_EFS_STATUS_ERROR;
  policyman_status_t        status    = POLICYMAN_STATUS_SUCCESS;
  char                      ratPath[128] = {0};

  memset(&st, 0x00, sizeof(struct fs_stat));

  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(ratPath, sizeof(ratPath), POLICYMAN_RAT_MASK_NV_PATH, subsId);

  /* Return if we detect error
  */
  if(status != POLICYMAN_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't retrieve from EFS", subsId);
    return FALSE;
  }

  /* Try to open the file, return error if we can't
  */
  fd = efs_open(ratPath, O_RDONLY);
  if (fd == -1)
  {
    efsStatus = POLICYMAN_EFS_STATUS_NOFILE;
    goto Done;
  }

  /* Get the size of the file.
  */
  if ( efs_fstat(fd, &st) == -1 || 
       st.st_size != sizeof(uint32)+3*sizeof(sys_band_mask_type)
     )
  {
    goto Done;
  }
  
  /*  If we were able to read the EFS, set the mask to that value.
   */
  if (
          efs_read(fd, pRatMask, sizeof(uint32)) == sizeof(uint32)
      &&  efs_read(fd, pGWBand, sizeof(sys_band_mask_type)) == sizeof(sys_band_mask_type)
      &&  efs_read(fd, pLTEBand, sizeof(sys_band_mask_type)) == sizeof(sys_band_mask_type)
      &&  efs_read(fd, pTDSBand, sizeof(sys_band_mask_type)) == sizeof(sys_band_mask_type)
     )
  {
    POLICYMAN_MSG_HIGH_0("---------- RATs and bands from EFS");
    POLICYMAN_MSG_HIGH_1("  RAT mask: 0x%04x", *pRatMask);
    policyman_rf_print_bands(*pGWBand, *pLTEBand, *pTDSBand);
    POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_HYPHEN);
    efsStatus = POLICYMAN_EFS_STATUS_SUCCESS;
  }
  
Done:

  if (fd != -1)
  {
    efs_close(fd);
  } 

  POLICYMAN_MSG_HIGH_3("subs %d: policyman_retrieve_rats_bands returned status %d, filesize %d", subsId, efsStatus, st.st_size);

  return efsStatus == POLICYMAN_EFS_STATUS_SUCCESS;
}

/*-------- policyman_rats_bands_persist --------*/
static policyman_efs_status_t
policyman_rats_bands_persist(
  sys_modem_as_id_e_type    subsId,
  uint32                    *pRatMask,
  sys_band_mask_type        *pGWBand,
  sys_band_mask_type        *pLTEBand,
  sys_band_mask_type        *pTDSBand  
)
{
  int                     fd;
  fs_size_t               bytesWrite = 0;
  policyman_efs_status_t  efsStatus     = POLICYMAN_EFS_STATUS_SUCCESS;
  char                    ratPath[128] = {0};

  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(ratPath, sizeof(ratPath), POLICYMAN_RAT_MASK_NV_PATH, subsId);

  /* Return if we detect error
  */
  if(efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't write into EFS", subsId);
    return efsStatus;
  }

  /* Try to open the file, return error if we can't
  */
  fd = efs_open(ratPath, O_WRONLY | O_CREAT | O_TRUNC);
  if (fd == -1)
  {
    efsStatus = POLICYMAN_EFS_STATUS_NOFILE;
    goto Done;
  }

  /* EFS file structure
      sizeof(uint32) = first 4 bytes = RAT MASK
      sizeof(gw_bands) = next 8 bytes = GW bands
      sizeof(lte_bands) = next 8 bytes = LTE bands       
      sizeof(tds_bands) = next 8 bytes = TDS bands
   */

  /* Write RAT mask to file
    */
   bytesWrite += efs_write(fd, pRatMask, sizeof(*pRatMask));

  /* Write GWBand to file
    */
   bytesWrite += efs_write(fd, pGWBand, sizeof(*pGWBand));

  /* Write LTEBand to file
    */
   bytesWrite += efs_write(fd, pLTEBand, sizeof(*pLTEBand));

  /* Write TDSBand to file
    */
   bytesWrite += efs_write(fd, pTDSBand, sizeof(*pTDSBand));
   POLICYMAN_ASSERT(bytesWrite == (sizeof(*pRatMask)+3*sizeof(*pGWBand)) );

Done:
  if (fd != -1)
  {
    efs_close(fd);
  }

  return efsStatus;
}

/*-------- policyman_retrieve_rats_bands_per_subs --------*/
policyman_status_t
policyman_retrieve_rats_bands_per_subs(
  size_t subs,
  uint32 *pRatMask,
  sys_band_mask_type *pGwBands,
  sys_band_mask_type *pLteBands,
  sys_band_mask_type *pTdsBands
)
{
  policyman_status_t  status;
  policyman_item_t    *pRatcfg = NULL;
  policyman_item_id_t ratId = POLICYMAN_ITEM_RAT_CAPABILITY;
  policyman_item_t    *pBandcfg = NULL;
  policyman_item_id_t rfId  = POLICYMAN_ITEM_RF_BANDS;

    /* get RAT capability first
   */
  status = policyman_cfgitem_get_items_per_subs( &ratId, 
                                                 1 , 
                                                 (policyman_item_t const **)&pRatcfg, 
                                                 subs
                                               );
  if(!POLICYMAN_SUCCEEDED(status))
  {
    POLICYMAN_MSG_ERROR_1("subs %d: policyman_get_items failed for RAT capability", subs);
    goto Done;
  }
  status = policyman_get_rat_capability((policyman_item_t const *)pRatcfg, pRatMask);

  /* get RF Band capability next
  */
  status = policyman_cfgitem_get_items_per_subs( &rfId, 
                                                 1 , 
                                                 (policyman_item_t const **)&pBandcfg,
                                                 subs
                                               );
  if(!POLICYMAN_SUCCEEDED(status))
  {
    POLICYMAN_MSG_ERROR_1("subs %d: policyman_get_items failed for Band capability", subs);
    goto Done;
  }
  status = policyman_get_rf_bands((policyman_item_t const *)pBandcfg, pGwBands, pLteBands, pTdsBands);

Done:

  REF_CNT_OBJ_RELEASE_IF(pRatcfg);
  REF_CNT_OBJ_RELEASE_IF(pBandcfg);
  return status;
}

/*-------- policyman_update_rats_bands_to_efs --------*/
void
policyman_update_rats_bands_to_efs(
  uint8 subsMask
)
{
  policyman_status_t     status;
  policyman_efs_status_t efsStatus;

  uint32              ratMask;
  sys_band_mask_type  gwBand;
  sys_band_mask_type  lteBand;
  sys_band_mask_type  tdsBand;

  size_t              activeSubs;
  size_t              subs;
  size_t              nSim = 0;

  /* Don't update UE mode into EFS for the SUBS which is not configured
   */
  (void)policyman_get_current_num_sim(&nSim);

  for(subs=0; subs < POLICYMAN_NUM_SUBS; subs++)
  {
    /* Get Active SUBS from bitmask. Don't update for SUBS 
           for which bit is not set or SUBS not configured
      */
    activeSubs = subsMask & 1<<subs;
    if(  activeSubs == 0
      || subs >= nSim
      )
    {
      continue;
    }

    status = policyman_retrieve_rats_bands_per_subs( subs,
                                                     &ratMask,
                                                     &gwBand,
                                                     &lteBand,
                                                     &tdsBand
                                                   );
    if(!POLICYMAN_SUCCEEDED(status))
    {
      POLICYMAN_MSG_ERROR_0("Failed to retrieve rat/band information, skipping EFS write");
      continue;
    }

    /* Write data to EFS file
    */
    efsStatus = policyman_rats_bands_persist( (sys_modem_as_id_e_type)subs,
                                               &ratMask, 
                                               &gwBand,
                                               &lteBand,
                                               &tdsBand
                                               );
    
    POLICYMAN_MSG_HIGH_2("subs %d: efs rat & band write status = %d", subs, efsStatus);

  }
}

/*-------- policyman_update_rats_bands_to_efs_per_tech --------*/
void
policyman_update_rats_bands_to_efs_per_tech(
  uint32  rat_include
)
{ 
  policyman_status_t     status;
  policyman_efs_status_t efsStatus;

  uint32              ratMask;
  sys_band_mask_type  gwBand;
  sys_band_mask_type  lteBand;
  sys_band_mask_type  tdsBand;

  sys_band_mask_type  hwGwBand;
  sys_band_mask_type  hwTdsBand;
  sys_modem_as_id_e_type subs = policyman_get_current_multimode_subs();

  policyman_get_hardware_bands_all_techs( &hwGwBand, NULL, &hwTdsBand );
  
    status = policyman_retrieve_rats_bands_per_subs( subs,
                                                     &ratMask,
                                                     &gwBand,
                                                     &lteBand,
                                                     &tdsBand
                                                   );
    if(!POLICYMAN_SUCCEEDED(status))
    {
      POLICYMAN_MSG_ERROR_0("Failed to retrieve rat/band information, skipping EFS write");
      return;
    }

    if( rat_include & SYS_SYS_MODE_MASK_WCDMA )
    {
      ratMask |= SYS_SYS_MODE_MASK_WCDMA;
      gwBand  |= hwGwBand & ALL_WCDMA_BANDS;
    }
    else
    {
      ratMask = ratMask & ~SYS_SYS_MODE_MASK_WCDMA;
      gwBand  = gwBand & ~ALL_WCDMA_BANDS;
    }

    if( rat_include & SYS_SYS_MODE_MASK_TDS )
    {
      ratMask |= SYS_SYS_MODE_MASK_TDS;
      tdsBand = hwTdsBand;
    }
    else
    {      
      ratMask = ratMask & ~SYS_SYS_MODE_MASK_TDS;
      tdsBand = 0;
    }
  
    /* Write data to EFS file
      */
      efsStatus = policyman_rats_bands_persist(  subs,
                                                 &ratMask, 
                                                 &gwBand,
                                                 &lteBand,
                                                 &tdsBand
                                                 );

      POLICYMAN_MSG_HIGH_2("subs %d: efs rat & band write status = %d", subs, efsStatus);
  
}

