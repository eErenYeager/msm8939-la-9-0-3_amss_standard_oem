/**
  @file policyman_sglte.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_sglte.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_cfgitem.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_efs.h"
#include "policyman_msgr_i.h"
#include "policyman_plmn.h"
#include "policyman_policy.h"
#include "policyman_rat_capability.h"
#include "policyman_rat_order.h"
#include "policyman_rf.h"
#include "policyman_serving_system.h"
#include "policyman_set.h"
#include "policyman_sglte_i.h"
#include "policyman_state.h"
#include "policyman_svc_mode.h"
#include "policyman_task.h"
#include "policyman_timer.h"
#include "policyman_ue_mode.h"
#include "policyman_util.h"

#include "geran_grr_msg.h"
#include <stringl/stringl.h>

typedef struct
{
  POLICYMAN_POLICY_BASE;

  /*  Timers used by SGLTE
   */
  policyman_timer_t   *pOosTimer;
  policyman_timer_t   *pB39ForbidTimer;

  /*  Bands in effect before the forbidden bands were removed
   */
  sys_band_mask_type  gwForRestore;
  sys_band_mask_type  lteForRestore;
  sys_band_mask_type  tdsForRestore;

  /*  Current set of band forbidden bands
   */
  sys_band_mask_type  gwForbidden;
  sys_band_mask_type  lteForbidden;
  sys_band_mask_type  tdsForbidden;
  boolean             haveForbiddenBands;

  rf_bands_t          *pSglteRfBands;

  /*  Current status if SGLTE is allowed
   */
  boolean             isSglteAllowed;
  policyman_set_t     *pSglteOperatorSet;

  /*  Include rat_acq_order item for SGLTE policy
   */
  boolean             rat_acq_order;
} policyman_policy_sglte_t;


#define SGLTE_POLICY_AS_BASE      ((policyman_policy_t *) &policyman_policy_sglte)
#define POLICY_TO_SGLTE_POLICY(p) ((policyman_policy_sglte_t *) (p))


#define SGLTE_OOS_TIMER       1
#define SGLTE_B39FORBID_TIMER 2


/*=============================================================================
  Function Protoypes
=============================================================================*/


/*-------- policy_sglte_update_forbidden_bands --------*/
/**
@brief  Update the forbidden bands in the policy.  Return whether anything changed.
*/
#if defined(FEATURE_SGLTE)
static boolean
policy_sglte_update_forbidden_bands(
  policyman_policy_sglte_t                *pPolicy,
  geran_grr_update_forbidden_bands_ind_t  *pBands
  )
{
  boolean bandsChanged  =   pPolicy->gwForbidden != pBands->geran_bands
                         || pPolicy->lteForbidden != pBands->lte_bands
                         || pPolicy->tdsForbidden != pBands->tds_bands
                         ;
  
  if (bandsChanged)
  {
    pPolicy->gwForbidden = pBands->geran_bands;
    pPolicy->lteForbidden = pBands->lte_bands;
    pPolicy->tdsForbidden = pBands->tds_bands;

    pPolicy->haveForbiddenBands =
        (pPolicy->gwForbidden | pPolicy->lteForbidden | pPolicy->tdsForbidden) != 0;

    POLICYMAN_MSG_HIGH_0("Forbidden bands:");
    policyman_rf_print_bands(pBands->geran_bands, pBands->lte_bands, pBands->tds_bands);
  }

  return bandsChanged;
}
#endif


/*-------- policy_sglte_restore_bands --------*/
/**
@brief  Restore the previously cached bands.
*/
static void
policy_sglte_restore_bands(
  policyman_policy_sglte_t  *pPolicy
  )
{
  policyman_set_t   *pSet = NULL;
  policyman_item_t  *prfb = NULL;

  /*  Update the SGLTE RF bands
   */
  policyman_rf_update_rf_bands(
        pPolicy->pSglteRfBands,
        pPolicy->gwForRestore,
        pPolicy->lteForRestore,
        pPolicy->tdsForRestore
        );

  /*  Send out an updated bands list.
   */
  pSet = policyman_itemset_new();
  prfb = policyman_rf_bands_item_new(
              pPolicy->gwForRestore,
              pPolicy->lteForRestore,
              pPolicy->tdsForRestore,
              TRUE
              );
  policyman_set_add(pSet, &prfb);
  policyman_cfgitem_update_items(pSet, policyman_get_current_multimode_subs());

  REF_CNT_OBJ_RELEASE_IF(prfb);
  REF_CNT_OBJ_RELEASE_IF(pSet);
}



/*-------- policy_sglte_handle_restore_bands_cmd --------*/
static void
policy_sglte_handle_restore_bands_cmd(
  policyman_cmd_t *pCmd
  )
{
  policy_sglte_restore_bands((policyman_policy_sglte_t *) pCmd->pPolicy);
}


/*-------- policy_sglte_get_restore_bands --------*/
/**
@brief  Get the base bands from which bands will be forbidden.  We cache
        these in the policy to use for later forbids or restores.
*/
#if defined(FEATURE_SGLTE)
static void
policy_sglte_get_restore_bands(
  policyman_policy_sglte_t  *pPolicy
  )
{
  policyman_status_t  status;
  policyman_item_t    *pCurRfBands = NULL;
  policyman_item_id_t id = POLICYMAN_ITEM_RF_BANDS;

  /*  Get the current bands, from which we're removing the forbidden bands.
   *  Cache these in the policy for later use.
   */
  status = policyman_get_items(&id, 1, (policyman_item_t const **) &pCurRfBands);
  if (POLICYMAN_SUCCEEDED(status))
  {
    policyman_get_rf_bands(
          pCurRfBands,
          &pPolicy->gwForRestore,
          &pPolicy->lteForRestore,
          &pPolicy->tdsForRestore
          );
  }

  REF_CNT_OBJ_RELEASE_IF(pCurRfBands);
}



/*-------- policy_sglte_forbid_bands --------*/
/**
@brief  Forbid the bands stored in the xxxForbidden members of the policy.
*/
static void
policy_sglte_forbid_bands(
  policyman_policy_sglte_t  *pPolicy
  )
{
  policyman_set_t           *pSet = NULL;
  policyman_item_t          *prfb = NULL;

  POLICYMAN_MSG_HIGH_0("SGLTE B39: forbidding bands");

  /*  Start the forbid timer.
   */
    policyman_timer_start(pPolicy->pB39ForbidTimer);

  /*  Update the SGLTE RF bands
   */
  policyman_rf_update_rf_bands(
        pPolicy->pSglteRfBands,
        pPolicy->gwForRestore & ~pPolicy->gwForbidden,
        pPolicy->lteForRestore & ~pPolicy->lteForbidden,
        pPolicy->tdsForRestore & ~pPolicy->tdsForbidden
        );

  /*  Send out an updated bands list.
   */
  pSet = policyman_itemset_new();
  prfb = policyman_rf_bands_item_new(
              pPolicy->gwForRestore & ~pPolicy->gwForbidden,
              pPolicy->lteForRestore & ~pPolicy->lteForbidden,
              pPolicy->tdsForRestore & ~pPolicy->tdsForbidden,
              TRUE
              );
  policyman_set_add(pSet, &prfb);
  policyman_cfgitem_update_items(pSet, policyman_get_current_multimode_subs());

  REF_CNT_OBJ_RELEASE_IF(prfb);
  REF_CNT_OBJ_RELEASE_IF(pSet);
}
#endif


/*-------- policy_sglte_b39_forbid_handle --------*/
/**
@brief  Handle a message from GERAN indicating that the forbidden bands have changed.
*/
void
policy_sglte_b39_forbid_handle(
  msgr_hdr_s  *pMsg,
  void        *pContext
  )
{
#if defined(FEATURE_SGLTE)
  geran_grr_update_forbidden_bands_ind_t  *pBands;
  policyman_policy_sglte_t                *pPolicy;
  boolean                                 currentlyForbidden;

  pBands = (geran_grr_update_forbidden_bands_ind_t *) pMsg;
  pPolicy = (policyman_policy_sglte_t *) pContext;

  POLICYMAN_MSG_HIGH_0("SGLTE B39: SGLTE policy received GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND");

  /*  Get the current state of "forbiddennes"
   */
  currentlyForbidden = pPolicy->haveForbiddenBands;

  /*  Update the band forbid state cached by the policy.  If the bands did not
   *  change, just return.
   */
  if (!policy_sglte_update_forbidden_bands(pPolicy, pBands))
  {
    POLICYMAN_MSG_HIGH_0("SGLTE B39: No change in bands to forbid");
    return;
  }

  POLICYMAN_MSG_HIGH_0("SGLTE B39: Forbidden bands have changed");

  /*  If the B39Forbid timer is running, just return.
   */
  if (policyman_timer_is_running(pPolicy->pB39ForbidTimer))
  {
    POLICYMAN_MSG_HIGH_0("SGLTE B39: Forbid timer running; no action taken");
    return;
  }

  /*  If we're moving from nothing forbidden to having forbidden bands,
   *  get the current set of bands for later restoration.
   */
  if (!currentlyForbidden && pPolicy->haveForbiddenBands)
  {
    POLICYMAN_MSG_HIGH_0("SGLTE B39: No forbidden bands -> forbidden bands");
    policy_sglte_get_restore_bands(pPolicy);
  }
      
  /*  At this point, we know that the band state has changed and the forbid
   *  timer is not running.  Do the appropriate forbid or restore, depending
   *  on the the request.
   */
  if (pPolicy->haveForbiddenBands)
  {
    policy_sglte_forbid_bands(pPolicy);
  }
  else
  {
    policy_sglte_restore_bands(pPolicy);
  }
#endif
}



/*-------- policy_sglte_b39forbid_time_expired --------*/
static void
policy_sglte_b39forbid_time_expired(
  policyman_timer_t *pTimer
  )
{
  policyman_policy_sglte_t  *pPolicy;
  policyman_cmd_t           *pCmd;
  
  pPolicy = (policyman_policy_sglte_t *) policyman_timer_get_policy(pTimer);

  POLICYMAN_MSG_HIGH_0("SGLTE B39: forbid timer expired");

  // TODO: This is technically not correct - we should really handle pending
  // TODO: changes to the forbidden bands as well.
  /*  If we no longer have forbidden bands when the timer expires, queue a
   *  command to restore the former bands.
   */
  if (pPolicy != NULL && !pPolicy->haveForbiddenBands)
  {
    POLICYMAN_MSG_HIGH_0("SGLTE B39: queueing restore bands command");

    pCmd = policyman_cmd_new(     sizeof(policyman_cmd_t),
                                  policy_sglte_handle_restore_bands_cmd,
                                  NULL,
                                  (policyman_policy_t *) pPolicy);

    policyman_queue_put_cmd(pCmd);
    ref_cnt_obj_release(pCmd);
  }  
}

/*-------- policy_sglte_get_rats_for_ueMode --------*/
policyman_item_t *
policy_sglte_get_rats_for_ueMode(
  sys_ue_mode_e_type current_ue_mode
  )
{
  sys_modem_as_id_e_type   subsId  = policyman_get_current_multimode_subs();
  policyman_state_t       *pState = policyman_state_get_state();
  uint32                  ratMask = SYS_SYS_MODE_MASK_GSM;
  
  if( current_ue_mode == SYS_UE_MODE_SGLTE_HOME ||
      current_ue_mode == SYS_UE_MODE_SGLTE_TEST_ONLY
    )
  {
    ratMask =   SYS_SYS_MODE_MASK_GSM
              | SYS_SYS_MODE_MASK_LTE
              | SYS_SYS_MODE_MASK_TDS;
  }
  else if (current_ue_mode == SYS_UE_MODE_SGLTE_ROAM)
  {
    /* Check if policy is still initializing or UE moved to 
           SGLTE_ROAM without any knowledge of serving MCC, 
           include GTWL in RAT mask for power up optimization.
      */
    boolean precondMet = policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subsId);

    if ( !precondMet 
       || policyman_is_initializing()
      )
    {
      ratMask =   SYS_SYS_MODE_MASK_GSM
                | SYS_SYS_MODE_MASK_LTE
                | SYS_SYS_MODE_MASK_WCDMA
                | SYS_SYS_MODE_MASK_TDS;
    }
    else
    {
      ratMask =   SYS_SYS_MODE_MASK_GSM
                | SYS_SYS_MODE_MASK_LTE
                | SYS_SYS_MODE_MASK_WCDMA;
    }
  }
  else if (current_ue_mode == SYS_UE_MODE_CSFB_ONLY)
  {
    ratMask =   SYS_SYS_MODE_MASK_GSM
              | SYS_SYS_MODE_MASK_LTE
              | SYS_SYS_MODE_MASK_TDS
              | SYS_SYS_MODE_MASK_WCDMA;
  }
  else if (current_ue_mode == SYS_UE_MODE_NORMAL)
  {
    ratMask = SYS_SYS_MODE_MASK_GSM;
  }    

  POLICYMAN_MSG_HIGH_2("default rat cap %u for ue_mode %d", ratMask, current_ue_mode);
  return  policyman_rat_capability_item_new(pState, ratMask, subsId);
              
}

/*-------- policy_sglte_get_bands_for_ueMode --------*/
static rf_bands_item_t * policy_sglte_get_bands_for_ueMode(
  sys_ue_mode_e_type  current_ue_mode,
  policyman_policy_t  *pPolicy
  )
{
  rf_bands_item_t *prfb=NULL;
  rf_bands_t *pBands=NULL;
  char *bands_id = NULL;
  sys_modem_as_id_e_type  subsId   = policyman_get_current_multimode_subs();

  if ( current_ue_mode == SYS_UE_MODE_SGLTE_HOME 
     || current_ue_mode == SYS_UE_MODE_SGLTE_TEST_ONLY
    )
  {
    bands_id = "sglte_rf_bands";
  }
  else if(current_ue_mode == SYS_UE_MODE_SGLTE_ROAM)
  {
    boolean precondMet = policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subsId);

    /* Check if policy is still initializing or UE moved to 
          SGLTE_ROAM without any knowledge of serving MCC, 
           make band id as 'rf_bands_gwlt' for power up optimization
      */
     if ( !precondMet 
        || policyman_is_initializing()
      )
    {
      bands_id = "rf_bands_gwlt";
    }
    else
    {
      bands_id = "rf_bands_gwl";
    }
  }
  else if(current_ue_mode == SYS_UE_MODE_CSFB_ONLY)
  {
    bands_id = "rf_bands_gwlt";
  }

  if (bands_id != NULL)
  {
    pBands = (rf_bands_t *) mre_named_object_find(
                              bands_id,
                              POLICYMAN_NAMED_RF_BANDS,
                              (mre_policy_t *) pPolicy
                              );        
  }
  
  PM_CHECK_PTR_GOTO(pBands,Done);
  
  prfb = policyman_get_bands_item_for_named_bands(pBands);

Done:
  return prfb;  
}

/*-------- policy_sglte_get_device_config_for_ueMode --------*/
static policyman_item_t *
policy_sglte_get_device_config_for_ueMode(
  sys_ue_mode_e_type ueMode
  )
{
  policyman_status_t         status     = POLICYMAN_STATUS_SUCCESS;
  device_configuration_t    *pCurrentdc = NULL; 
  device_configuration_t    *pdc        = NULL;
  size_t                    dcSize;
  size_t                    i;
  
  status = policyman_get_current_device_config((policyman_item_t **)&pCurrentdc);

  /* Create new device config from the current values
   */
  if(status == POLICYMAN_STATUS_SUCCESS)
  {
    /*  Get device configuration size from nSims
     */
    dcSize = sizeof(device_configuration_t) + sizeof(subs_info_t) * pCurrentdc->num_sims;
    pdc    = policyman_device_config_item_new(dcSize);

    /* Copy contents of current device configuration and modify parameters as per UE mode
      */
    pdc->specialization = pCurrentdc->specialization;
    pdc->num_cfgs       = pCurrentdc->num_cfgs;

    for (i = 0; i < pdc->num_cfgs; i++)
    {
      memscpy(&pdc->configs[i], sizeof(subs_info_t), &pCurrentdc->configs[i], sizeof(subs_info_t));
    }

    switch(ueMode)
    {
      /* DSDA
         */
      case SYS_UE_MODE_SGLTE_HOME:
        pdc->num_sims   = 2;
        pdc->max_active_voice = 2;
        pdc->max_active_data  = 2;
        break;       

      /* DSDS
         */
      case SYS_UE_MODE_SGLTE_ROAM:
        pdc->num_sims   = 2;
        pdc->max_active_voice = 1;
        pdc->max_active_data  = 1;
        break;

      default:
        /* Other parameters need to be reset based on use case
            */
        pdc->num_sims   = 1;
        pdc->max_active_voice = 1;
        pdc->max_active_data  = 1;
    }        
  }

  REF_CNT_OBJ_RELEASE_IF(pCurrentdc);
  return (policyman_item_t *)pdc;
}

/*-------- policy_sglte_get_rat_order_for_ueMode --------*/
policyman_item_t * policy_sglte_get_rat_order_for_ueMode(
  sys_ue_mode_e_type  ueMode,
  policyman_policy_t *pPolicy
  )
{
  policyman_rat_order_item_t *pRO = NULL;
  static sys_sys_mode_e_type  sglte_rat_acq_order[6]={SYS_SYS_MODE_GSM, SYS_SYS_MODE_WCDMA, 
                                                      SYS_SYS_MODE_LTE, SYS_SYS_MODE_TDS,
                                                      SYS_SYS_MODE_CDMA, SYS_SYS_MODE_HDR};

  if (((policyman_policy_sglte_t *)pPolicy)->rat_acq_order)
  {    
    pRO = policyman_rat_order_item_new();
    
    pRO->rat_order.num_rat = 6;
    memscpy(pRO->rat_order.acq_order, sizeof(pRO->rat_order.acq_order), sglte_rat_acq_order, sizeof(sglte_rat_acq_order));
  }
  else
  {
    POLICYMAN_MSG_HIGH_0("XML does not include rat_order, not sending item");
  }

  return (policyman_item_t *)pRO;  
}

/*-------- policyman_sglte_set_capability_for_ue_mode --------*/
static void policyman_sglte_set_capability_for_ue_mode(
  sys_ue_mode_e_type  ueMode,
  policyman_policy_t * pPolicy
  )
{
  policyman_set_t           *pItemSet = policyman_itemset_new();  
  sys_modem_as_id_e_type  subsId   = policyman_get_current_multimode_subs();

  /*  Get UE mode and add them to the itemset.
   */
  {
    policyman_ue_mode_item_t  *pUeItem;

    pUeItem = policyman_ue_mode_item_new(ueMode, subsId);
    if (pUeItem != NULL)
    {
      policyman_set_add(pItemSet, &pUeItem);
      ref_cnt_obj_release(pUeItem);
    }
  }

  /*  Get the bands for this UE mode and add them to the itemset.
   */
  {
    rf_bands_item_t  *prfb;

    prfb = policy_sglte_get_bands_for_ueMode(ueMode, pPolicy);
    if (prfb != NULL)
    {
      policyman_set_add(pItemSet, &prfb);
      ref_cnt_obj_release(prfb);
    }
  }
  
  /*  Get the RATs for this UE mode and add them to the itemset.
   */
  {
    policyman_item_t  *prc;

    prc = policy_sglte_get_rats_for_ueMode(ueMode);
    if (prc != NULL)
    {
      policyman_set_add(pItemSet, &prc);
      ref_cnt_obj_release(prc);
    }
  }

  /* Set the device configuration to DSDS/DSDA according to UE mode
       We are here it means we are operating in MSIM configuration
   */
  {
    policyman_item_t  *pdc;

    pdc = policy_sglte_get_device_config_for_ueMode(ueMode);
    if (pdc != NULL)
    {
      policyman_set_add(pItemSet, &pdc);
      ref_cnt_obj_release(pdc);
    }
  }

  /* Send the rat_order item 
   */
  {
    policyman_item_t  *pro;

    pro = policy_sglte_get_rat_order_for_ueMode(ueMode, pPolicy);
    if (pro != NULL)
    {
      policyman_set_add(pItemSet, &pro);
      ref_cnt_obj_release(pro);
    }
  }

  /* Update the items for multimode subs
    */
  policyman_cfgitem_update_items(pItemSet, subsId);
  REF_CNT_OBJ_RELEASE_IF(pItemSet);
}

/*-------- policyman_sglte_msim_oos_update_items --------*/
void policyman_sglte_msim_oos_update_items(
  policyman_policy_t * pPolicy
  )
{
  sys_ue_mode_e_type  ueMode = SYS_UE_MODE_SGLTE_ROAM;

  /* Check for SGLTE MCC on non-multimode SUBS and change UE mode
       to SGLTE_HOME
   */
  if (policyman_msim_non_multimode_subs_has_sglte_mcc())
  {
    ueMode = SYS_UE_MODE_SGLTE_HOME;
  }

  policyman_sglte_set_capability_for_ue_mode(ueMode, pPolicy);
}

/*-------- policy_sglte_determine_and_enforce_full_rat --------*/
static void policy_sglte_determine_and_enforce_full_rat(
  policyman_policy_t  *pPolicy
)
{
  policyman_policy_sglte_t  *pMe  = (policyman_policy_sglte_t *) pPolicy;
  boolean                   isSglteAllowed;
  size_t                    nSim = 1;

  if(policyman_policy_is_suspended())
  {
    POLICYMAN_MSG_ERROR_0("policy is suspended, not starting Full RAT mode");
    return;
  }

  isSglteAllowed = policyman_boolean_get_value("sglte_allowed");
  (void)policyman_get_current_num_sim(&nSim);

  /* Don't start Full RAT if device is single SIM and SGLTE is not allowed
        If SS precondition is not met then allow Full RAT
     */
  if(   nSim == 1
     && !isSglteAllowed     
     && policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS,policyman_get_current_multimode_subs())
    )
  {
    POLICYMAN_MSG_ERROR_0("not starting Full RAT - SGLTE is not allowed");
    return;
  }

  /* Check for MultiSim behavior and determine if other SUBS has service
   */
  if (   nSim > 1
      && policyman_msim_non_multimode_subs_in_service()
     )
  {
    POLICYMAN_MSG_HIGH_0("non multimode subs has service, not starting Full RAT mode");
    policyman_sglte_msim_oos_update_items((policyman_policy_t *) pMe);
  }
  else
  {
    /* For MultiSIM, don't check SGLTE allowed status and enter Full RAT
      */
    POLICYMAN_MSG_HIGH_0("entering Full RAT mode");
    policyman_rat_capability_start_full_rat( SYS_UE_MODE_SGLTE_ROAM,
                POLICYMAN_SVC_MODE_LIMITED,
                (policyman_policy_t *) pMe );
  }
}

/*-------- policyman_sglte_oos_timer_expired --------*/
static void policy_sglte_oos_timer_expired(
  policyman_timer_t *pTimer
)
{

  /* Determine and enter Full Rat mode 
        Restart OOS timer since SGLTE SUBS is still OOS.
   */
  policyman_policy_t *pPolicy;

  pPolicy = policyman_timer_get_policy(pTimer);
  if (pPolicy != NULL)
  {
    policy_sglte_determine_and_enforce_full_rat(pPolicy);
  }
  policyman_timer_start(pTimer);
}

/*-------- policy_sglte_ue_mode_from_efs --------*/
sys_ue_mode_e_type
policy_sglte_ue_mode_from_efs(
  void
  )
{
  policyman_efs_status_t    status;
  sys_ue_mode_e_type        *pMode;
  sys_ue_mode_e_type        ue_mode;
  size_t                    modeSize;

  /*  Try and read from the NV item for UE mode.
   */
  status = policyman_efs_get_file(POLICYMAN_UE_MODE_NV_PATH, (void **) &pMode, &modeSize);
  
  /*  If we were able to read the NV and if the value is in the proper range,
   *  set the mode to that value.  Otherwise just return NORMAL mode.
   */
  if (    status == POLICYMAN_EFS_STATUS_SUCCESS
      &&  modeSize == sizeof(sys_ue_mode_e_type)
      &&  pMode != NULL
      &&  *pMode > SYS_UE_MODE_NONE
      &&  *pMode <= SYS_UE_MODE_SGLTE_TEST_ONLY
     )
  {
    ue_mode = *pMode;
    policyman_mem_free(pMode);
    POLICYMAN_MSG_HIGH_1("SGLTE: read ue_mode %d from EFS", ue_mode);
  }
  else
  {
    ue_mode = SYS_UE_MODE_SGLTE_HOME;
    POLICYMAN_MSG_HIGH_1("SGLTE: unable to read ue_mode from EFS; returning %d", ue_mode);
  }

  return ue_mode;
}


sd_ss_mode_pref_e_type
cmph_map_cm_mode_pref_to_sd_mode_pref(
  cm_mode_pref_e_type mode_pref
);


/*-------- policy_sglte_get_default_ue_mode --------*/
policyman_item_t *
policy_sglte_get_default_ue_mode(
  void
  )
{
  policyman_ue_mode_item_t  *pItem;
  sys_ue_mode_e_type        ue_mode;

  ue_mode = policy_sglte_ue_mode_from_efs();

  /*  If the UE mode from EFS is one of the _ONLY modes, just use that.
   */
  if (ue_mode == SYS_UE_MODE_CSFB_ONLY || ue_mode == SYS_UE_MODE_SGLTE_TEST_ONLY)
  {
    POLICYMAN_MSG_HIGH_1("SGLTE: xxxx_ONLY ue_mode (%d) from EFS used", ue_mode);
    goto Done;
  }

  /*  If mode is SGLTE_HOME, check that we can support this based on user preferences.
   *  Fall back to SGLTE_ROAM if we can't - don't go to NORMAL as lower layers don't acknowledge in SGLTE operation
   */
  
  if (ue_mode == SYS_UE_MODE_SGLTE_HOME)
  {
    cm_mode_pref_e_type       cm_mode_pref  = policyman_ph_get_mode_pref(policyman_get_current_multimode_subs());
    cm_srv_domain_pref_e_type srv_domain    = policyman_ph_get_srv_domain(policyman_get_current_multimode_subs());
    sd_ss_mode_pref_e_type    rat_mask      = cmph_map_cm_mode_pref_to_sd_mode_pref(cm_mode_pref);

    if (
          srv_domain != CM_SRV_DOMAIN_PREF_CS_PS
       || (rat_mask & SD_SS_MODE_PREF_GSM_LTE) != SD_SS_MODE_PREF_GSM_LTE
       )
    {
      ue_mode = SYS_UE_MODE_SGLTE_ROAM;
      POLICYMAN_MSG_HIGH_3("SGLTE: mode %d not supported for domain pref %d, RAT pref 0x%x",
                          ue_mode, srv_domain, rat_mask);
      POLICYMAN_MSG_HIGH_1("SGLTE: Switching to SYS_UE_MODE_SGLTE_ROAM (mode %d)",
                          ue_mode);
    }
  }  

Done:
  pItem = policyman_ue_mode_item_new(
              ue_mode,
              policyman_get_current_multimode_subs()
              );

  return (policyman_item_t *) pItem;
}


/*=============================================================================
  SGLTE policy APIs
=============================================================================*/



/*-------- policy_sglte_init_status --------*/
static void
policy_sglte_init_status(
  policyman_policy_t  *pPolicy,
  policyman_status_t  status
  )
{
  switch (status)
  {
    case POLICYMAN_STATUS_SUCCESS:
      policyman_util_msg_separator(POLICYMAN_SEPARATOR_STAR);
      POLICYMAN_MSG_HIGH_0("SGLTE policy: successfully initialized");
      policyman_util_msg_separator(POLICYMAN_SEPARATOR_STAR);
      break;

    case POLICYMAN_STATUS_ERR_CONFIG_FAILURE:
      POLICYMAN_MSG_ERROR_0("SGLTE policy: configuration failed");
      break;

    case POLICYMAN_STATUS_ERR_POLICY_NOT_ENABLED:
      POLICYMAN_MSG_HIGH_0("SGLTE policy: policy disabled in XML");
      break;

    case POLICYMAN_STATUS_ERR_INVALID_VERSION:
      POLICYMAN_MSG_ERROR_0("SGLTE policy: version mismatch between XML and policy");
      break;

    case POLICYMAN_STATUS_ERR_MALFORMED_XML:
      POLICYMAN_MSG_ERROR_0("SGLTE policy: cannot parse XML file");
      break;

    default:
      POLICYMAN_MSG_ERROR_1("SGLTE policy: unknown status %d", status);
      break;
  }
}

#if 0
/*-------- policy_sglte_set_default_config --------*/
static sys_ue_mode_e_type
policy_sglte_set_default_config(
  policyman_policy_t  *pPolicy,
  policyman_set_t     *pItemSet
  )
{
  policyman_status_t  status;
  sys_ue_mode_e_type  ue_mode = SYS_UE_MODE_SGLTE_HOME;
  uint32              ratMask;
  policyman_item_t    *pue=NULL;
  rf_bands_item_t     *prfb=NULL;
  rat_capability_t    *prc=NULL;

  /*  Get the UE mode and add it to the itemset.
   */
  pue = policy_sglte_get_default_ue_mode();
  PM_CHECK_PTR_GOTO(pue,Done);
  policyman_set_add(pItemSet, &pue);

  status = policyman_get_ue_mode((policyman_item_t const *)pue, &ue_mode);
  PM_VALIDATE_GOTO(POLICYMAN_SUCCEEDED(status), Done);
  
  /*  Get the bands for this UE mode and add them to the itemset.
   */
  prfb = policy_sglte_get_bands_for_ueMode(ue_mode);
  PM_CHECK_PTR_GOTO(prfb,Done);      

  /*  Get the RATs for this UE mode and add them to the itemset.
   */
  prc = policy_sglte_get_rats_for_ueMode(ue_mode);
  PM_CHECK_PTR_GOTO(prc,Done);
  
  /* filter bands based on RAT capability and then add to set
   */  
  status = policyman_get_rat_capability((policyman_item_t const *)prc, &ratMask);
  if(POLICYMAN_SUCCEEDED(status))
  {
    policyman_rf_bands_filter_based_on_rat(prfb, ratMask);
  }
  policyman_set_add(pItemSet, &prfb);
  policyman_set_add(pItemSet, &prc);        
  
Done:
  REF_CNT_OBJ_RELEASE_IF(prfb);
  REF_CNT_OBJ_RELEASE_IF(prc);
  REF_CNT_OBJ_RELEASE_IF(pue);

  return ue_mode;
}
#endif

/*-------- policy_sglte_initialize_oos_timer --------*/
static void
policy_sglte_initialize_oos_timer(
  policyman_policy_sglte_t  *pMe,
  sys_ue_mode_e_type        ue_mode
  )
{
  policyman_timer_t         *pTimer;
  
  /*  The OOS timer is not used in CSFB_ONLY or SGLTE_TEST_ONLY modes.
   */
  if (  ue_mode == SYS_UE_MODE_CSFB_ONLY
     || ue_mode == SYS_UE_MODE_SGLTE_TEST_ONLY
     )
  {
    return;
  }

  pTimer = (policyman_timer_t *) mre_named_object_find(
                                    "sglte_oos",
                                    POLICYMAN_NAMED_TIMER,
                                    (mre_policy_t *) pMe
                                    );
  if (pTimer == NULL)
  {
    POLICYMAN_ERR_FATAL("SGLTE: OOS timer not defined", 0, 0, 0);
  }

  policyman_timer_set_expirefn(pTimer, policy_sglte_oos_timer_expired);
  policyman_timer_set_subs(pTimer, policyman_get_current_multimode_subs());
  ref_cnt_obj_add_ref(pTimer);
  pMe->pOosTimer = pTimer;
  POLICYMAN_MSG_HIGH_2("SGLTE: OOS timer (id %d) defined with interval %d seconds",
                      SGLTE_OOS_TIMER, 
                      policyman_timer_get_interval(pTimer));
}


/*-------- policy_sglte_initialize_b39forbid_timer --------*/
static void
policy_sglte_initialize_b39forbid_timer(
  policyman_policy_sglte_t  *pMe,
  sys_ue_mode_e_type        ue_mode
  )
{
  policyman_timer_t         *pTimer;

  /*  The B39Forbid timer is not used in CSFB_ONLY mode.
   */
  if (ue_mode == SYS_UE_MODE_CSFB_ONLY)
  {
    return;
  }

  pTimer = (policyman_timer_t *) mre_named_object_find(
                                    "b39forbid",
                                    POLICYMAN_NAMED_TIMER,
                                    (mre_policy_t *) pMe
                                    );
  if (pTimer == NULL)
  {
    POLICYMAN_ERR_FATAL("SGLTE: B39 forbid timer not defined", 0, 0, 0);
  }

  policyman_timer_set_expirefn(pTimer, policy_sglte_b39forbid_time_expired);
  ref_cnt_obj_add_ref(pTimer);
  pMe->pB39ForbidTimer = pTimer;
  policyman_timer_set_subs(pTimer, policyman_get_current_multimode_subs());
  POLICYMAN_MSG_HIGH_2("SGLTE: B39 forbid timer (id %d) defined with interval %d seconds",
                      SGLTE_B39FORBID_TIMER, 
                      policyman_timer_get_interval(pTimer));
}


/*=============================================================================
  SGLTE policy APIs
=============================================================================*/

/*-------- policyman_sglte_plmn_is_sglte --------*/
/**
@brief  Function to determine if PLMN is a sglte PLMN
*/
policyman_status_t
policyman_sglte_plmn_is_sglte(
  sys_plmn_id_s_type  *pPlmnId,
  boolean             *pIsSglte
  )
{
  policyman_set_t     *pSet;
  sys_mcc_type        mcc;
  sys_ue_mode_e_type  ueMode;
  policyman_status_t  status;
  
  if (pPlmnId == NULL || pIsSglte == NULL)
  {
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }

  /* Get UE mode Policy Item for SUBS1 as this is the current multimode SUBS
   */
  status = policyman_get_current_ue_mode(SYS_MODEM_AS_ID_1, &ueMode);

  /* MCC is allowed for SGLTE if UE mode is SGLTE_ONLY
   */
  if(  status == POLICYMAN_STATUS_SUCCESS
     && ueMode == SYS_UE_MODE_SGLTE_ONLY
    )
  {
    *pIsSglte = TRUE;
    goto Done;
  }

  /* get MCC out of PLMN
   */
  mcc = policyman_plmn_get_mcc(pPlmnId);

  /* find a set of SGLTE MCCs
   */
  pSet = (policyman_set_t*)policyman_policy_find_named_object(
                                "sglte_mccs",
                                POLICYMAN_NAMED_MCC_SET
                                );

  *pIsSglte = (pSet != NULL)?
                  policyman_set_contains(pSet, &mcc)
                : FALSE;

Done:
  return status;
}





/*-------- policy_sglte_dtor --------*/
static void
policy_sglte_dtor(
  void  *pPolicy
  )
{
  policyman_policy_sglte_t  *pMe = (policyman_policy_sglte_t *) pPolicy;
  
  REF_CNT_OBJ_RELEASE_IF(pMe->pOosTimer);
  REF_CNT_OBJ_RELEASE_IF(pMe->pB39ForbidTimer);
  REF_CNT_OBJ_RELEASE_IF(pMe->pSglteOperatorSet);
  REF_CNT_OBJ_RELEASE_IF(pMe->pSglteRfBands);
  policyman_policy_dtor(pPolicy);
}


/*-------- policy_sglte_new_instance --------*/
static policyman_policy_t  *
policy_sglte_new_instance(
  char const  *pName
  )
{
  policyman_policy_sglte_t  *pPolicy = NULL;
  
  if (strcmp(pName, "sglte") == 0)
  {
    pPolicy = policyman_mem_alloc(sizeof(policyman_policy_sglte_t));
    ref_cnt_obj_init(pPolicy, policy_sglte_dtor);

    pPolicy->pName = "sglte";
    pPolicy->schema_ver = 1;
    pPolicy->id = POLICYMAN_POLICY_ID_SGLTE;
  }

  return (policyman_policy_t *) pPolicy;
}


static void
policy_sglte_init_policy(
  policyman_policy_sglte_t  *pMe
  )
{
  pMe->pOosTimer = NULL;
  pMe->pB39ForbidTimer = NULL;

  pMe->gwForRestore = 0;
  pMe->lteForRestore = 0;
  pMe->tdsForRestore = 0;
  pMe->gwForbidden = 0;
  pMe->lteForbidden = 0;
  pMe->tdsForbidden = 0;
  pMe->haveForbiddenBands = FALSE;

  pMe->pSglteRfBands = NULL;

  pMe->isSglteAllowed = FALSE;
  pMe->pSglteOperatorSet = NULL;

  pMe->rat_acq_order = FALSE;
}


/*-------- policy_sglte_configure --------*/
static policyman_status_t policy_sglte_configure(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_item_t          *pItem;
  policyman_policy_sglte_t  *pMe                = POLICY_TO_SGLTE_POLICY(pPolicy);
  policyman_item_id_t       id = POLICYMAN_ITEM_UE_MODE;
  sys_ue_mode_e_type        ue_mode = SYS_UE_MODE_NONE;
  boolean                   immediate_fullrat;
  boolean                   currentMcc;

  POLICYMAN_MSG_HIGH_0("SGLTE: Configuring policy");

  /*  Initialize all variables in the policy
   */
  policy_sglte_init_policy(pMe);

  /*  Set the default configuration, and find out what UE mode we're in.
   */
  if( 1 == policyman_cfgitem_get_items_per_subs( (policyman_item_id_t const *)&id, 
                                                 1, 
                                                 (policyman_item_t const **)&pItem, 
                                                  pCtx->asubs_id
                                               )
    ) 
  {
    (void)policyman_get_ue_mode((policyman_item_t const *)pItem, &ue_mode);
    REF_CNT_OBJ_RELEASE_IF(pItem); 
  }
  else
  {
    /* UE mode not available in database means it is not created from EFS file
        * Default UE mode will be SGLTE_HOME from XML <initial> action
      */
    ue_mode = SYS_UE_MODE_SGLTE_HOME;
  }


  /*  Initialize the timers based on the UE mode.
   */
  policy_sglte_initialize_oos_timer(pMe, ue_mode);
  policy_sglte_initialize_b39forbid_timer(pMe, ue_mode);

  /*  Register for the GERAN band forbid message.
   */
#if defined(FEATURE_SGLTE)
  policyman_msgr_register(GERAN_GRR_UPDATE_FORBIDDEN_BANDS_IND,
      policy_sglte_b39_forbid_handle,
      pPolicy );
#endif

  /*  Get a pointer to the SGLTE RF bands list so that we can update it as
   *  necessary.
   */
  pMe->pSglteRfBands = (rf_bands_t *) mre_named_object_find( "sglte_rf_bands",
                                            POLICYMAN_NAMED_RF_BANDS,
                                            (mre_policy_t *) pPolicy );
  if (pMe->pSglteRfBands == NULL)
  {
    POLICYMAN_ERR_FATAL("SGLTE: policy requires an <rf_band_list> named 'sgtle_rf_bands'", 0, 0, 0);
  }

  ref_cnt_obj_add_ref(pMe->pSglteRfBands);

  /*  Get a pointer to the sglte_operators list so that we can use it as
   *  necessary.
   */
  pMe->pSglteOperatorSet = (policyman_set_t *) mre_named_object_find("sglte_operators",
                                                    POLICYMAN_NAMED_PLMN_SET,
                                                    (mre_policy_t *) pPolicy );
  if (pMe->pSglteOperatorSet == NULL)
  {
    POLICYMAN_ERR_FATAL("SGLTE: policy requires a PLMN list named 'sglte_operators'", 0, 0, 0);
  }

  ref_cnt_obj_add_ref(pMe->pSglteOperatorSet);

  /* Make boolean TRUE so OOS timer can be started before SS event is received
        this will be correctly set once rules execute
  */
  pMe->isSglteAllowed = TRUE;

  /* initialize the operation flags for the first time  */  
  policyman_ss_set_stack_opr(pCtx->asubs_id, SYS_MODEM_STACK_ID_1, TRUE);

  if (  ue_mode == SYS_UE_MODE_SGLTE_HOME
     || ue_mode == SYS_UE_MODE_SGLTE
     || ue_mode == SYS_UE_MODE_SGLTE_ONLY
     )
  {
    policyman_ss_set_stack_opr(pCtx->asubs_id, SYS_MODEM_STACK_ID_2, TRUE);
  }

  /* set current MCC valid if XML has boolean 'current_mcc' defined
   */
  currentMcc = policyman_boolean_get_value("include_hlos_mcc");
  if (currentMcc)
  {
    policyman_state_update_include_hlos_mcc(TRUE);
  }

  /* detect if we need optimization to enter Full RAT before OOS timer expiry
   */
  immediate_fullrat = policyman_boolean_get_value("pm:xml_full_rat");;
  policyman_state_update_full_rat(immediate_fullrat);
  pMe->rat_acq_order = policyman_boolean_is_defined("pm:xml_full_rat");

  POLICYMAN_MSG_HIGH_0("SGLTE: policy configured");
  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policy_sglte_notify_service --------*/
static void policy_sglte_notify_service(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_policy_sglte_t  *pMe = POLICY_TO_SGLTE_POLICY(pPolicy);
  sys_oprt_mode_e_type       oprt_mode = policyman_ph_get_oprt_mode();
  
  /* Optimization: Enter Full Rat mode first time after power up 
       after getting PWR_SAVE events on both SUBS or after LPM -> ONLINE.
   */
  if(   oprt_mode == SYS_OPRT_MODE_ONLINE
    && policyman_state_get_full_rat()
    )
  {        
    POLICYMAN_MSG_HIGH_0("SGLTE optimization: determining Full Rat");
    policyman_state_handle_full_rat_update(pPolicy, pCtx);
  }

  /*  Start or stop the OOS timer depending on whether we have service.
        start OOS timer only when operating mode is online
   */
  policyman_timer_set_subs(pMe->pOosTimer, pCtx->asubs_id);

  if ( !pCtx->haveService
     && oprt_mode == SYS_OPRT_MODE_ONLINE
     )
  {
    policyman_timer_start(pMe->pOosTimer);
  }
  else
  {
    policyman_timer_stop(pMe->pOosTimer);
  }
}


/*-------- policy_sglte_handle_user_pref_update --------*/
static void policy_sglte_handle_user_pref_update(
  policyman_policy_t       *pPolicy,
  policy_execute_ctx_t     *pCtx
)
{  
  policyman_policy_sglte_t  *pMe = POLICY_TO_SGLTE_POLICY(pPolicy);
  sys_oprt_mode_e_type       oprt_mode  = policyman_ph_get_oprt_mode();
  policyman_subs_state_t    *pSubsState = policyman_state_get_subs_state(pCtx->asubs_id);
  size_t i;

  if(oprt_mode == SYS_OPRT_MODE_LPM)
  {
    /* Stop any OOS timer if operating mode is LPM
     */
    policyman_timer_stop(pMe->pOosTimer);

    /* reset the SS state info and precondition if UE entered LPM mode
     */
    for(i=SYS_MODEM_STACK_ID_1; i<SYS_MODEM_STACK_ID_MAX; i++)
    {
      policyman_ss_set_default_value(pSubsState->pSsInfo, i);
    }
    policyman_state_reset_precondition_met(POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, pSubsState->asubs_id);
  }
}

/*-------- policy_sglte_handle_uim_update --------*/
static void 
policy_sglte_handle_uim_update(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
)
{  
  mmgsdi_app_enum_type      sim_app_type;
  policyman_ue_mode_item_t  *pUeItem =NULL;
  rf_bands_item_t           *prfb    =NULL;
  rf_bands_t                *pBands=NULL;
  boolean                   sglte_allowed_with_2g_sim;

  policyman_item_t          *pItem =NULL;
  sys_ue_mode_e_type        ue_mode=SYS_UE_MODE_NONE;
  policyman_status_t        status;
  size_t                    nSim;

  /* return if all precondiitons have already been met - nothing to do as rules will take care
  */
  if (policyman_policy_are_preconditions_met(pPolicy, pCtx->pState))
  {
    return;
  }

  /* return if this is MSIM - XML rules take care of changing ue_mode and device_config together in MSIM
   */
  status = policyman_get_current_num_sim(&nSim);
  if (POLICYMAN_FAILED(status))      { goto Done; }  

  /* Get UIM SIM type first
  */
  policyman_state_get_sim_type(pCtx->asubs_id, &sim_app_type);

  POLICYMAN_MSG_MED_1("SGLTE policy handle UIM update - sim app type %d", sim_app_type);

  sglte_allowed_with_2g_sim = policyman_boolean_get_value("allow_sgtds_with_2g_sim");

  /* If SIM is 2G, and 2G is not allowed for SGLTE mode, change UE mode
  */
  if(   sim_app_type == MMGSDI_APP_SIM 
     && !sglte_allowed_with_2g_sim
    )
  {
    /* return if this is MSIM - XML rules take care of changing ue_mode and device_config together
      */
    if(nSim > 1)
    {
      POLICYMAN_MSG_HIGH_0("waiting for IMSI PLMN to decide on new configuration");
      goto Done;
    }

    status = policyman_get_current_ue_mode(pCtx->asubs_id, &ue_mode);
    if (POLICYMAN_FAILED(status))     { goto Done; }

    /* Change UE mode only when current UE mode is SGLTE_HOME, retain *_ONLY modes
      */
    if( ue_mode == SYS_UE_MODE_SGLTE_HOME )
    {
      /* Add UE mode SGLTE_ROAM to set
       */
      pUeItem = policyman_ue_mode_item_new( SYS_UE_MODE_SGLTE_ROAM,
                                            pCtx->asubs_id );
      policyman_set_replace(pCtx->pItemSet, &pUeItem);

      /*  Since last ue_mode was SGLTE_HOME, Band mask will have TDS - add to the itemset.
       */
       pBands = (rf_bands_t *) mre_named_object_find( "csfb_rf_bands_with_tdscdma_no_fdd",
                                 POLICYMAN_NAMED_RF_BANDS,
                                 (mre_policy_t *) pPolicy );        
       
      PM_CHECK_PTR_GOTO(pBands,Done);
      prfb = policyman_get_bands_item_for_named_bands(pBands);
      PM_CHECK_PTR_GOTO(prfb,Done);
      policyman_set_replace(pCtx->pItemSet, &prfb); 

      /*  Since last ue_mode was SGLTE_HOME, RAT mask will have TDS - no need to add to the itemset.
       */
    }
    else
    {
      POLICYMAN_MSG_ERROR_1("not changing UE mode to SGLTE_ROAM, previous mode was %d", ue_mode);
    }
  }

Done:
  REF_CNT_OBJ_RELEASE_IF(prfb);
  REF_CNT_OBJ_RELEASE_IF(pUeItem);
  REF_CNT_OBJ_RELEASE_IF(pItem);
}

/*-------- policy_sglte_execute --------*/
static boolean
policy_sglte_execute(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_policy_sglte_t  *pMe = POLICY_TO_SGLTE_POLICY(pPolicy);

  pMe->isSglteAllowed = policyman_boolean_get_value("sglte_allowed");

  return TRUE;
}


/*-------- policyman_is_sglte_allowed --------*/
/**
@brief  Function to determine if SGLTE is allowed
*/
policyman_status_t
policyman_is_sglte_allowed(
  boolean             *pIsSglte
  )
{
  if(!pIsSglte)
  {
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }

  *pIsSglte = policyman_boolean_get_value("sglte_allowed");
  return POLICYMAN_STATUS_SUCCESS;
}

/*-----------------------------------------------------------------------------
  VTable for policy_sglte
-----------------------------------------------------------------------------*/
policyman_policy_vtbl_t  policyman_policy_sglte_vtbl =
{
  policy_sglte_new_instance,
  policy_sglte_init_status,
  policy_sglte_configure,
  policy_sglte_execute,
  policy_sglte_notify_service,
  policy_sglte_handle_user_pref_update,
  policy_sglte_handle_uim_update
};
