/**
  @file policyman_efs.c

  @brief PM abstraction layer above EFS
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_efs.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "policyman_dbg.h"
#include "policyman_efs.h"
#include "policyman_policy.h"
#include "policyman_task.h"
#include "policyman_util.h"

#if defined(TEST_FRAMEWORK)
#error code not present
#endif /* +TEST_FRAMEWORK. */

#include "stringl/stringl.h"
#include "stdio.h"
#include "fs_sys_types.h"
#include "fs_diag_access.h"
#include "fs_fcntl.h"


/*===========================================================================
FUNCTION policyman_diag_access_check_cb()

DESCRIPTION
  This function is called by the Diag module as a result of any Diag access
  of the policyman EFS directory. It is initially registered with Diag for
  knowing the access of policyman EFS directory.

PARAMETERS
  file_name_ptr: Name of the file that needs to be permitted
  op_type      : File operation type

RETURN VALUE
  TRUE:  If we want to allow access
  FALSE: If we want to reject access

DEPENDENCIES
  policyman service must be registered with Diag module via
  diag_fs_register_access_check API.

SIDE EFFECTS
  None

===========================================================================*/
static boolean policyman_diag_access_check_cb
(
  char      * file_name_ptr,
  uint32      op_type
)
{
  /* The conditions for rejecting are these:
     1. File name is of interest to us
     2. Operation is one of write, delete, rename, chmod */

  boolean ret =   (op_type == DIAG_FS_CHECK_OPEN)
               || (op_type == DIAG_FS_CHECK_DISP_DIRS)
               || (op_type == DIAG_FS_CHECK_DISP_FILES)
               || (op_type == DIAG_FS_CHECK_ITERATE)
               || (op_type == DIAG_FS_CHECK_READ)
               || (op_type == DIAG_FS_CHECK_GET_ATTRIB)
               || (op_type == DIAG_FS_CHECK_CLOSE)
               || (op_type == DIAG_FS_CHECK_OPENDIR)
               || (op_type == DIAG_FS_CHECK_READDIR)
               || (op_type == DIAG_FS_CHECK_CLOSEDIR)
               || (op_type == DIAG_FS_CHECK_STAT)
               || (op_type == DIAG_FS_CHECK_LSTAT)
               || (op_type == DIAG_FS_CHECK_FSTAT)
               || (op_type == DIAG_FS_CHECK_STATFS)
               || (op_type == DIAG_FS_CHECK_ACCESS)
               ;

  POLICYMAN_MSG_HIGH("policyman_diag_access_check_cb op_type %d ret %d",op_type,ret,0);


  return ret;
} /* policyman_diag_access_check_cb */



/*  Location to which to write the .conf file that specifies which files
 *  to back up to QCN.
 */
#define POLICYMAN_CONF_PATH "/nv/reg_files/conf/policyman.conf"

/*  String containing list of files to backed up to QCN.
 */
static char const policyman_conf_file_contents[] = "\
/policyman/device_config\n\
/policyman/rat_mask\n\
/policyman/ue_mode\n\
/policyman/locked\n\
";


static void
policyman_efs_write_conf_file(
  void
  )
{
  policyman_policy_enum_ctx fileCtx;
  char const                *pFileName;
  char                      buffer[2048];
  policyman_efs_status_t    efsStatus;

  char                      *pBuf = buffer;
  size_t                    bytesRemaining = sizeof(buffer);
  size_t                    bytesCopied;

  /*---------------------------------------------------------------------------
    Build a buffer to write to the file
  ---------------------------------------------------------------------------*/
  /*  Start with the persisted data and lock file
   */
  bytesCopied = strlcpy(buffer, policyman_conf_file_contents, bytesRemaining);
  pBuf += bytesCopied;
  bytesRemaining -= bytesCopied;
  
  /*  Enumerate the policy files and add them.
   */
  policyman_policy_file_enum_init(&fileCtx);
  while ((pFileName = policyman_policy_file_enum_next(&fileCtx)) != NULL)
  {
    bytesCopied = strlcpy(pBuf, pFileName, bytesRemaining);
    if (bytesCopied > bytesRemaining)
    {
      POLICYMAN_MSG_ERROR("policyman_efs_write_conf_file: buffer too small.", 0, 0, 0);
      return;
    }
    pBuf += bytesCopied;
    bytesRemaining -= bytesCopied;
    
    bytesCopied = strlcpy(pBuf, "\n", bytesRemaining);
    if (bytesCopied > bytesRemaining)
    {
      POLICYMAN_MSG_ERROR("policyman_efs_write_conf_file: buffer too small.", 0, 0, 0);
      return;
    }
    pBuf += bytesCopied;
    bytesRemaining -= bytesCopied;
  }

  /*---------------------------------------------------------------------------
    Write the data
  ---------------------------------------------------------------------------*/
  efsStatus = policyman_efs_put_file(POLICYMAN_CONF_PATH, (void *) buffer, pBuf - buffer, TRUE);

  if (efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR("Failed to create policyman.conf", 0, 0, 0 );
  }

}



/*-------- policyman_efs_initialize --------*/
void policyman_efs_initialize(
  void
  )
{
  static  const char  *efs_directory_list[] = {"/policyman"};
  struct  fs_stat     file_stat;
  
  /*  If a file named "locked" is in the policyman directory, restrict
   *  access to the directory via diag.
   */
  if (efs_stat("/policyman/locked",&file_stat) == 0)
  {
    diag_fs_register_access_check(efs_directory_list,
                                  ARR_SIZE(efs_directory_list),
                                  policyman_diag_access_check_cb);  
  }

  /*  Create the conf file that allows QPST to backup policyman files to the QCN.
   */
  if (efs_stat(POLICYMAN_CONF_PATH, &file_stat) != 0)
  {
    POLICYMAN_MSG_HIGH("policyman.conf not present; creating it now", 0, 0, 0);
    policyman_efs_write_conf_file();
  }

}


/*-------- policyman_efs_deinitialize --------*/
void
policyman_efs_deinitialize(
  void
  )
{
}

