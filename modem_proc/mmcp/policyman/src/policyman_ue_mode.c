/**
  @file policyman_ue_mode.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_ue_mode.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_ue_mode.h"
#include "policyman_util.h"
#include "policyman_efs.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_set.h"
#include "policyman_subs.h"
#include "policyman_xml.h"

#include <stringl/stringl.h>
#include "event.h"
#include "event_defs.h"

#define UE_MODE_TAG  "ue_mode"


/*=============================================================================
  UE mode APIs
=============================================================================*/


/*-------- policyman_ue_mode_item_new --------*/
policyman_ue_mode_item_t *
policyman_ue_mode_item_new(
  sys_ue_mode_e_type      ue_mode,
  sys_modem_as_id_e_type  subsId
  )
{
  policyman_ue_mode_item_t  *pItem;
  pItem = (policyman_ue_mode_item_t *) policyman_cfgitem_new_item(
                                          POLICYMAN_ITEM_UE_MODE,
                                          sizeof(policyman_ue_mode_item_t),
                                          policyman_simple_dtor
                                          );

  pItem->mode = ue_mode;
  pItem->asubs_id = subsId;

  return pItem;
}


/*-------- policyman_ue_mode_normalize_collection --------*/
void
policyman_ue_mode_normalize_collection(
  policyman_item_collection_t *pCollection
  )
{
  policyman_item_t          **ppItems = (policyman_item_t **) pCollection->pItems;
  policyman_ue_mode_item_t  *pOld;
  size_t                    i;
  sys_ue_mode_e_type        ue_mode;

  for (i = 0 ; i < pCollection->numItems ; ++i)
  {
    /*  If this isn't a UE_MODE item, continue.
     */
    if (policyman_item_get_id(ppItems[i]) != POLICYMAN_ITEM_UE_MODE)
    {
      continue;
    }
    
    /*  
     */
    pOld = (policyman_ue_mode_item_t *) ppItems[i];
    switch (pOld->mode)
    {
      case SYS_UE_MODE_SVLTE_ONLY:
        ue_mode = SYS_UE_MODE_SVLTE;
        break;

      default:
        ue_mode = SYS_UE_MODE_NONE;
        break;
    }
        
    if (ue_mode != SYS_UE_MODE_NONE)
    {
      policyman_ue_mode_item_t  *pNew;

      pNew = policyman_ue_mode_item_new(ue_mode, pOld->asubs_id);
      ppItems[i] = (policyman_item_t *) pNew;
      ref_cnt_obj_release(pOld);
    }
  }
}


/*-------- policyman_ue_mode_get_default_from_efs --------*/
policyman_item_t *
policyman_ue_mode_get_default_from_efs(
  sys_modem_as_id_e_type subs_id
)
{
  policyman_ue_mode_item_t  *pUeMode = NULL;
  sys_ue_mode_e_type         ueMode;

  if(!policyman_retrieve_ue_mode(subs_id, &ueMode))
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't populate UE mode Item from EFS", subs_id);
    goto Done;
  }

  pUeMode = policyman_ue_mode_item_new(ueMode, subs_id);

Done:

  return (policyman_item_t *)pUeMode;
}

/*-------- policyman_ue_mode_get_default --------*/
policyman_item_t *
policyman_ue_mode_get_default(
  sys_modem_as_id_e_type subsId
  )
{
  policyman_ue_mode_item_t  *pItem;
  sys_ue_mode_e_type         ueMode = SYS_UE_MODE_NORMAL;

  pItem = policyman_ue_mode_item_new(ueMode, subsId);

  POLICYMAN_MSG_HIGH_2("Default UE mode for subs %d: %d", subsId, pItem->mode);
  return (policyman_item_t *) pItem;
}

/*-------- policyman_ue_mode_compare --------*/
boolean
policyman_ue_mode_compare(
  policyman_item_t  *pData1,
  policyman_item_t  *pData2
  )
{
  policyman_ue_mode_item_t  *pMode1 = (policyman_ue_mode_item_t *) pData1;
  policyman_ue_mode_item_t  *pMode2 = (policyman_ue_mode_item_t *) pData2;

  return  pData1 != NULL
          && pData2 != NULL
          && pMode1->mode == pMode2->mode;
}

/*-------- policyman_ue_mode_display --------*/
void policyman_ue_mode_display
(
  policyman_item_t  *pItem
)
{
  policyman_ue_mode_item_t  *pMode = (policyman_ue_mode_item_t *) pItem;

  POLICYMAN_MSG_HIGH_2("UE mode for subs %d: %d", pMode->asubs_id, pMode->mode);
}

/*-------- policyman_ue_mode_on_update --------*/
void
policyman_ue_mode_on_update(
  policyman_item_t  *pItem
  )
{
  policyman_ue_mode_item_t  *pMode = (policyman_ue_mode_item_t *) pItem;
  uint8 event_ue_mode = pMode->mode;
  event_report_payload(EVENT_PM_UE_MODE, sizeof(event_ue_mode), &event_ue_mode);

  return;
}  

/*-------- policyman_ue_mode_update_to_efs --------*/
void
policyman_ue_mode_update_to_efs(
  policyman_item_t const *pItem
)
{
  policyman_ue_mode_item_t  *pMode = (policyman_ue_mode_item_t *) pItem;
  policyman_efs_status_t    efsStatus   = POLICYMAN_EFS_STATUS_SUCCESS;
  boolean                   writeStatus; 
  char                      uePath[128] = {0};
  size_t                    nSim = 0;

  /* Don't update UE mode into EFS for the SUBS which is not configured
   */
  (void)policyman_get_current_num_sim(&nSim);

  if((size_t)pItem->asubs_id >= nSim)
  {
    POLICYMAN_MSG_MED_1("subs %d not configured: EFS not updated for UE mode", pItem->asubs_id);
    return;
  }
 
  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(uePath, sizeof(uePath), POLICYMAN_UE_MODE_NV_PATH, pItem->asubs_id);

  /* Return if we detect error
  */
  if(efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't write into EFS", pItem->asubs_id);
    return;
  }

  writeStatus = policyman_cfgitem_persist_item(uePath, &pMode->mode, sizeof(pMode->mode));
  POLICYMAN_MSG_HIGH_3("subs %d: ue_mode %d write to efs status %d", pItem->asubs_id, pMode->mode, writeStatus);
}

/*-------- policyman_retrieve_ue_mode --------*/
boolean
policyman_retrieve_ue_mode(
  sys_modem_as_id_e_type subsId,
  sys_ue_mode_e_type *pUeMode
  )
{
  policyman_efs_status_t    status;
  sys_ue_mode_e_type        *pMode = NULL;
  size_t                    modeSize;
  char                      uePath[128] = {0};
  policyman_efs_status_t    efsStatus = POLICYMAN_EFS_STATUS_SUCCESS;

  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(uePath, sizeof(uePath), POLICYMAN_UE_MODE_NV_PATH, subsId);

  /* Return if we detect error
  */

  if(efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't retrieve from EFS", subsId);
    return FALSE;
  }

  /* Initialize UE mode to NONE
   */
  *pUeMode = SYS_UE_MODE_NONE;

  /*  Try and read from the NV item for UE mode.
   */
  status = policyman_efs_get_file(uePath, (void **) &pMode, &modeSize);
  
  /*  If we were able to read the NV and if the value is in the proper range,
   *  set the mode to that value.  Otherwise just return NORMAL mode.
   */
  if (    status == POLICYMAN_EFS_STATUS_SUCCESS
      &&  modeSize == sizeof(sys_ue_mode_e_type)
      &&  pMode != NULL
      &&  *pMode > SYS_UE_MODE_NONE
      &&  *pMode <= SYS_UE_MODE_1XSRLTE_ONLY
     )
  {
    *pUeMode = *pMode;
    POLICYMAN_MSG_HIGH_1("read ue_mode %d from EFS", *pUeMode);
  }

  POLICYMAN_MEM_FREE_IF(pMode);
  return *pUeMode != SYS_UE_MODE_NONE;
}

/*-------- policyman_get_current_ue_mode --------*/
policyman_status_t
policyman_get_current_ue_mode(
  sys_modem_as_id_e_type subsId,
  sys_ue_mode_e_type     *pMode
  )
{
  policyman_item_t    *pItem = NULL;
  policyman_item_id_t id = POLICYMAN_ITEM_UE_MODE;
  policyman_status_t  status = POLICYMAN_STATUS_SUCCESS;
  sys_ue_mode_e_type  ueMode;
  size_t nItem;

  if(pMode == NULL)
  {
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }
  
  /* Get UE mode Policy Item
   */
   nItem = policyman_cfgitem_get_items_per_subs((policyman_item_id_t const *)&id,
                                                 1,
                                                 (policyman_item_t const **)&pItem,
                                                 subsId
                                                );
  if(nItem == 1)
  {
    status = policyman_get_ue_mode(pItem, &ueMode);
    if(status == POLICYMAN_STATUS_SUCCESS)
    {
      *pMode = ueMode;
    }
  }
  /* Fetch current UE mode and return
   */

  REF_CNT_OBJ_RELEASE_IF(pItem);
  return status;
}


/*=============================================================================
  Accessor function for UE mode
=============================================================================*/


/*-------- policyman_get_ue_mode --------*/
policyman_status_t
policyman_get_ue_mode(
  policyman_item_t const  *pItem,
  sys_ue_mode_e_type      *pUeMode
  )
{
  policyman_ue_mode_item_t  *pMode = (policyman_ue_mode_item_t *) pItem;
  
  if (pMode == NULL || pUeMode == NULL)
  { 
    POLICYMAN_MSG_ERROR_0("get_ue_mode returned err POLICYMAN_STATUS_ERR_INVALID_ARGS");
    return POLICYMAN_STATUS_ERR_INVALID_ARGS; 
  }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_UE_MODE)
  { 
    POLICYMAN_MSG_ERROR_0("get_ue_mode returned err POLICYMAN_STATUS_ERR_INVALID_ARGS");
    return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; 
  }

  *pUeMode = pMode->mode;

  POLICYMAN_MSG_HIGH_1("get_ue_mode returned ue_mode %d", pMode->mode);

  return POLICYMAN_STATUS_SUCCESS;
}


typedef struct
{
  POLICYMAN_ACTION_BASE;

  sys_modem_as_id_e_type  subsId;
  sys_ue_mode_e_type ue_mode;
} ue_mode_action_t;

/*-------- policyman_rat_capability_str_to_rat --------*/
static sys_ue_mode_e_type
policyman_str_to_ue_mode(
  const char  *pUe_mode
  )
{
  typedef struct
  {
    const char  *pStr;
    sys_ue_mode_e_type      ue_mode;
  } mask_map;
  sys_ue_mode_e_type ue_mode = SYS_UE_MODE_NONE;

  static mask_map map[] =
  {
    {"NORMAL",          SYS_UE_MODE_NORMAL},
    {"CSFB",            SYS_UE_MODE_CSFB},
    {"SGLTE",           SYS_UE_MODE_SGLTE},
    {"SVLTE",           SYS_UE_MODE_SVLTE},
    {"CSFB_ONLY",       SYS_UE_MODE_CSFB_ONLY},
    {"SGLTE_ONLY",      SYS_UE_MODE_SGLTE_ONLY},
    {"SVLTE_ONLY",      SYS_UE_MODE_SVLTE_ONLY},
    {"1X_CSFB_PREF",    SYS_UE_MODE_1XSRLTE_CSFB_PREFERRED},
    {"GSM_CSFB_PREF",   SYS_UE_MODE_GSMSRLTE_CSFB_PREFERRED},
    {"GSMSRLTE_ONLY",   SYS_UE_MODE_GSMSRLTE_ONLY},
    {"1XSRLTE_ONLY",    SYS_UE_MODE_1XSRLTE_ONLY},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pUe_mode, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      ue_mode = map[i].ue_mode;
      break;
    }
  }

  return ue_mode;
}

/*-------- policyman_ue_mode_execute --------*/
static boolean
policyman_ue_mode_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  ue_mode_action_t          *pueAction = (ue_mode_action_t *) pAction;
  policyman_ue_mode_item_t  *prc = policyman_ue_mode_item_new(pueAction->ue_mode, pueAction->subsId);

  policyman_set_replace(((policy_execute_ctx_t *) pCtx)->pItemSet, &prc);
  ref_cnt_obj_release(prc);

  POLICYMAN_MSG_HIGH_2("action <ue_mode> set mode to %d for subs %d", prc->mode, pueAction->subsId);

  return TRUE;
}

/*-------- policyman_ue_mode_if_execute --------*/
static boolean
policyman_ue_mode_if_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean succeeded = FALSE;
  ue_mode_action_t    *pueAction = (ue_mode_action_t *) pAction;

  succeeded = policyman_cfgitem_is_present(
                    POLICYMAN_ITEM_UE_MODE,
                    pueAction->subsId
                    );

  if (succeeded)
  {
    POLICYMAN_UT_MSG_0("<ue_mode_if> skipping update - item present");
  }
  else
  {
    succeeded = policyman_ue_mode_execute(pAction, pCtx);
  }

  return succeeded;
}


/*-------- policyman_ue_mode_create_action --------*/
mre_status_t
policyman_ue_mode_create_new_action(
  policyman_xml_element_t const *pElem,
  policyman_policy_t            *pPolicy,
  policyman_action_t            **ppAction,
  pfn_execute_t                 actionfn
  )
{
  mre_status_t        status = MRE_STATUS_ERR_INVALID_ACTION;
  ue_mode_action_t    *pAction = NULL;
  sys_ue_mode_e_type  mode;
  char const          *pStr;
  sys_modem_as_id_e_type  subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<ue_mode>: requires a UE mode", 0, 0, 0);
    goto Done;
  }

  mode = policyman_str_to_ue_mode(pStr);
  if (mode == SYS_UE_MODE_NONE)
  {
    POLICYMAN_UT_MSG("<ue_mode>: invalid UE mode: %s", pStr, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pAction = (ue_mode_action_t *)
                policyman_mem_alloc(sizeof(ue_mode_action_t));
  
  ref_cnt_obj_init(pAction, policyman_action_dtor);
  pAction->execute = actionfn;
  pAction->ue_mode = mode;
  pAction->subsId = subs;

  *ppAction = (policyman_action_t *) pAction;

  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}


/*-------- policyman_ue_mode_action_new --------*/
mre_status_t
policyman_ue_mode_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_ue_mode_create_new_action(
                pElem, (policyman_policy_t *) pPolicy, ppAction,
                (pfn_execute_t) policyman_ue_mode_execute);
}

/*-------- policyman_ue_mode_if_action_new --------*/
mre_status_t
policyman_ue_mode_if_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
 return policyman_ue_mode_create_new_action(
                pElem, (policyman_policy_t *) pPolicy, ppAction,
                (pfn_execute_t) policyman_ue_mode_if_execute);

}


typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  sys_ue_mode_e_type  ue_mode;
} policyman_ue_mode_condition_t;



/*-------- policyman_condition_ue_mode_evaluate --------*/
static boolean
policyman_condition_ue_mode_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_status_t            status;
  policyman_item_id_t           id = POLICYMAN_ITEM_UE_MODE;
  policyman_item_t              *pItem = NULL;
  size_t                        numItems;
  sys_ue_mode_e_type            current_ue_mode = SYS_UE_MODE_NONE;
  boolean                       result = FALSE;
  policyman_ue_mode_condition_t *pCond = (policyman_ue_mode_condition_t *) pCondition;

  numItems = policyman_cfgitem_get_items_per_subs( &id, 
                                                   1, 
                                                   (policyman_item_t const **)&pItem,
                                                   pCond->subsId
                                                 );

  if (numItems != 1)                { goto Done; }

  status = policyman_get_ue_mode((policyman_item_t const *)pItem, &current_ue_mode);
  if (POLICYMAN_FAILED(status))     { goto Done; }

  result = (current_ue_mode == pCond->ue_mode);

Done:
  POLICYMAN_MSG_HIGH_1("condition <ue_mode_is> returns %d", result);
  REF_CNT_OBJ_RELEASE_IF(pItem);
  return result;
}


/*-------- policyman_condition_ue_mode_new --------*/
mre_status_t
policyman_condition_ue_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                  status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_ue_mode_condition_t *pCondition;
  char const                    *pStr;
  sys_ue_mode_e_type            ueMode;
  sys_modem_as_id_e_type        subs;


  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<ue_mode_is>: UE mode required", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  ueMode = policyman_str_to_ue_mode(pStr);
  if (ueMode == SYS_UE_MODE_NONE)
  {
    POLICYMAN_UT_MSG("<ue_mode_is>: invalid UE mode: %s", pStr, 0, 0);
    goto Done;
  }

  pCondition = (policyman_ue_mode_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_ue_mode_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_ue_mode_evaluate;
  pCondition->ue_mode = ueMode;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;
  status = MRE_STATUS_SUCCESS;
  
Done:  
  return status;
}

