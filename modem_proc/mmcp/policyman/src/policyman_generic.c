/**
  @file policy_generic.c

  @brief  Generic policy.  This policy is entirely rules-based, with the
          addition of OOS handling.
*/

/*
    Copyright (c) 2013-2016 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_generic.c#4 $
  $DateTime: 2016/04/17 23:50:31 $
  $Author: rkarth $
*/

#include "policyman_generic.h"
#include "policyman_device_config.h"
#include "policyman_dbg.h"
#include "policyman_plmn.h"
#include "policyman_rat_capability.h"
#include "policyman_rat_order.h"
#include "policyman_serving_system.h"
#include "policyman_set.h"
#include "policyman_state.h"
#include "policyman_timer.h"
#include "policyman_util.h"

#include "sys.h"
#include <stringl/stringl.h>



typedef struct
{
  POLICYMAN_POLICY_BASE;

  /*  Timers used by Generic policy
   */
  policyman_timer_t   *pOosTimer;

  /*  Include rat_acq_order item
   */
  boolean             xml_full_rat;
} policyman_policy_generic_t;


#define POLICY_TO_GENERIC_POLICY(p) ((policyman_policy_generic_t *) (p))

#define GENERIC_OOS_TIMER    1



/*=============================================================================
  Generic policy APIs
=============================================================================*/


/*-------- policy_generic_dtor --------*/
static void
policy_generic_dtor(
  void  *pPolicy
  )
{
  policyman_policy_generic_t *pMe = POLICY_TO_GENERIC_POLICY(pPolicy);

  REF_CNT_OBJ_RELEASE_IF(pMe->pOosTimer);
  policyman_policy_dtor(pPolicy);
}


/*-------- policy_generic_new_instance --------*/
policyman_policy_t  *
policy_generic_new_instance(
  char const  *pName
  )
{
  policyman_policy_generic_t  *pPolicy = NULL;
  
  if (
          strcmp(pName, "generic") == 0
      ||  strcmp(pName, "rat capability") == 0
      ||  strcmp(pName, "device configuration") == 0
     )
  {
    pPolicy = policyman_mem_alloc(sizeof(policyman_policy_generic_t));
    ref_cnt_obj_init(pPolicy, policy_generic_dtor);

    pPolicy->pName = "generic";
    pPolicy->schema_ver = 2;
    pPolicy->id = POLICYMAN_POLICY_ID_GENERIC;
  }

  return (policyman_policy_t *) pPolicy;
}


/*-------- policy_generic_init_status --------*/
static void
policy_generic_init_status(
  policyman_policy_t  *pPolicy,
  policyman_status_t  status
  )
{
  switch (status)
  {
    case POLICYMAN_STATUS_SUCCESS:
      POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_STAR);
      POLICYMAN_MSG_HIGH_0("Generic policy: successfully initialized");
      POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_STAR);
      break;

    case POLICYMAN_STATUS_ERR_CONFIG_FAILURE:
      POLICYMAN_MSG_HIGH_0("Generic policy: configuration failed");
      break;

    case POLICYMAN_STATUS_ERR_POLICY_NOT_ENABLED:
      POLICYMAN_MSG_HIGH_0("Generic policy: policy disabled in XML");
      break;

    case POLICYMAN_STATUS_ERR_INVALID_VERSION:
      POLICYMAN_MSG_HIGH_0("Generic policy: version mismatch between XML and policy");
      break;

    case POLICYMAN_STATUS_ERR_MALFORMED_XML:
      POLICYMAN_MSG_HIGH_0("Generic policy: cannot parse XML file");
      break;

    default:
      POLICYMAN_MSG_HIGH_1("Generic policy: unknown status %d", status);
      break;
  }
}


/*-------- policy_generic_init_policy --------*/
static void
policy_generic_init_policy(
  policyman_policy_generic_t  *pMe
  )
{
  pMe->pOosTimer      = NULL;
}


/*-------- policy_generic_get_rat_order_for_fullratmode --------*/
void policy_generic_get_rat_order_for_fullratmode(
  policyman_set_t     *pItemSet
  )
{
  policyman_rat_order_item_t                     *pRO  = NULL;
  static sys_sys_mode_e_type  default_rat_acq_order[6] = {SYS_SYS_MODE_GSM, SYS_SYS_MODE_WCDMA, 
                                                          SYS_SYS_MODE_LTE, SYS_SYS_MODE_TDS,
                                                          SYS_SYS_MODE_CDMA, SYS_SYS_MODE_HDR};

  pRO = policyman_rat_order_item_new();
  
  pRO->rat_order.num_rat = 6;
  memscpy(pRO->rat_order.acq_order,
          sizeof(pRO->rat_order.acq_order),
          default_rat_acq_order,
          sizeof(default_rat_acq_order));

  policyman_set_replace(pItemSet, &pRO);
  ref_cnt_obj_release(pRO);
}


/*-------- policy_generic_configure --------*/
static policyman_status_t
policy_generic_configure(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_policy_generic_t  *pMe = POLICY_TO_GENERIC_POLICY(pPolicy);
  policyman_timer_t           *pTimer;

  POLICYMAN_MSG_HIGH_0("Configuring Generic policy");

  policy_generic_init_policy(pMe);

  /*---------------------------------------------------------------------------
    Find and initialize the OOS timer.  If not present that's fine - we just
    won't do anything on OOS.
  ---------------------------------------------------------------------------*/
  pTimer = (policyman_timer_t *) mre_named_object_find( "oos",
                                    POLICYMAN_NAMED_TIMER,
                                    (mre_policy_t *) pPolicy );
  if (pTimer == NULL)
  {
    POLICYMAN_MSG_HIGH_0("OOS timer not defined in Generic policy");
  }
  else
  {
    pMe->pOosTimer = pTimer;
    policyman_timer_set_subs(pTimer, SYS_MODEM_AS_ID_1);
    ref_cnt_obj_add_ref(pTimer);
    POLICYMAN_MSG_HIGH_2("OOS timer (id %d) defined with interval %d seconds in Generic policy",
                          GENERIC_OOS_TIMER, 
                          policyman_timer_get_interval(pTimer) );
  }

  /* set include_hlos_mcc valid in state if XML has boolean 'include_hlos_mcc' defined
   */
  {
    boolean include_hlos_mcc;

    include_hlos_mcc = policyman_boolean_get_value("include_hlos_mcc");
    policyman_state_update_include_hlos_mcc(include_hlos_mcc);
  }

  /* detect if we need optimization to enter Full RAT before OOS timer expiry
   */
  {
    boolean immediate_fullrat;

    immediate_fullrat = policyman_boolean_get_value("pm:xml_full_rat");;
    policyman_state_update_full_rat(immediate_fullrat);
    pMe->xml_full_rat = policyman_boolean_is_defined("pm:xml_full_rat");
  }

  POLICYMAN_MSG_HIGH_0("GENERIC: policy configured");
  return POLICYMAN_STATUS_SUCCESS;
}


static boolean
policy_generic_execute(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_policy_generic_t *pMe = POLICY_TO_GENERIC_POLICY(pPolicy);
  policyman_timer_t          *pTimer = pMe->pOosTimer;
  sys_ue_mode_e_type          ueMode   = SYS_UE_MODE_NORMAL;
  policyman_svc_mode_t        svcMode  = POLICYMAN_SVC_MODE_LIMITED;

  if (policyman_policy_is_suspended())
  {
    POLICYMAN_MSG_ERROR_0("policy is suspended, not running execute method");
    return TRUE;
  }

  if (policyman_timer_is_expired(pTimer))
  {
    policyman_timer_set_handled(pTimer);
 
    /* If non-mmode SUBS has service then don't enforce Full RAT, 
         call for rule execution.
       */
    if (policyman_msim_non_multimode_subs_in_service())
    {
      POLICYMAN_MSG_HIGH_0("non multimode subs has service, executing policy instead of entering full RAT mode");
      policyman_policy_execute(pPolicy, pCtx);
      policyman_timer_start(pTimer);
      goto Done;
  }

    /* For single SIM case/complete OOS in MSIM enforce Full RAT rightaway. 
       For cases where RAT Acq Order policy item is to be updated include it and
       restart the timer.
       */
    if (pMe->xml_full_rat)
    {
      policy_generic_get_rat_order_for_fullratmode(pCtx->pItemSet);
    }
    policyman_rat_capability_full_rat_mode(pCtx->pItemSet, ueMode, svcMode);
    policyman_timer_start(pTimer);
  }

Done:
  return TRUE;
}


/*-------- policy_generic_notify_service --------*/
static void
policy_generic_notify_service(
  policyman_policy_t  *pPolicy,
  policy_execute_ctx_t *pCtx
  )
{
  policyman_policy_generic_t  *pMe = POLICY_TO_GENERIC_POLICY(pPolicy);
  sys_oprt_mode_e_type         oprt_mode = policyman_ph_get_oprt_mode();
  
    /*  Start or stop the OOS timer depending on whether we have service.
         optimization to enter Full RAT upon PWR_SAVE is handled in rule evaluation.
     */
  policyman_timer_set_subs(pMe->pOosTimer, pCtx->asubs_id);
	
  if(  !pCtx->haveService
    && oprt_mode == SYS_OPRT_MODE_ONLINE 
           )
    {
      policyman_timer_start(pMe->pOosTimer);
    POLICYMAN_MSG_HIGH_0("calling for rule execution from generic_notify_service");
    policyman_policy_execute(pPolicy, pCtx);
  }
  else
  {
    policyman_timer_stop(pMe->pOosTimer);
  }
}


/*-------- policy_generic_handle_user_pref_update --------*/
static void policy_generic_handle_user_pref_update(
  policyman_policy_t       *pPolicy,
  policy_execute_ctx_t     *pCtx
)
{  
  policyman_policy_generic_t  *pMe = POLICY_TO_GENERIC_POLICY(pPolicy);
  sys_oprt_mode_e_type         oprt_mode   = policyman_ph_get_oprt_mode();
  policyman_subs_state_t      *pSubsState  = policyman_state_get_subs_state(pCtx->asubs_id);
  size_t                       i;

  /* Stop any OOS timer, and reset the SS state info and preconditions if UE entered LPM mode
  */
  if( oprt_mode == SYS_OPRT_MODE_LPM)
  {
    policyman_timer_stop(pMe->pOosTimer);

    for(i=SYS_MODEM_STACK_ID_1; i< SYS_MODEM_STACK_ID_MAX; i++)
    {
      policyman_ss_set_default_value(pSubsState->pSsInfo, i);
    }
    policyman_state_reset_precondition_met(POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, pSubsState->asubs_id);
  }
}

/*-------- policyman_is_srlte_plmn --------*/
policyman_status_t policyman_is_srlte_plmn(
  sys_plmn_id_s_type  *pPlmnId,
  boolean              checkMccOnly,
  boolean             *pSvdPossible
  )
{
  policyman_set_t     *pMccSet;
  policyman_set_t     *pPlmnSet;
  sys_mcc_type        *listMcc;
  sys_mcc_type        mcc;
  policyman_status_t  retval = POLICYMAN_STATUS_SUCCESS;
  size_t              nElems;
  size_t              i;
  boolean             is_srlte_on_any_plmn;


  pMccSet  = (policyman_set_t *)policyman_policy_find_named_object( "sxlte_mccs",
                                                                    POLICYMAN_NAMED_MCC_SET );

  pPlmnSet = (policyman_set_t *)policyman_policy_find_named_object( "sxlte_plmns",
                                                                    POLICYMAN_NAMED_PLMN_SET );

  is_srlte_on_any_plmn = policyman_boolean_get_value("pm:srlte_on_any_plmn");

  if (!is_srlte_on_any_plmn)
  {
    // Same as pm:srlte_on_any_plmn set if have sxlte_mccs with no sxlte_plmns
    is_srlte_on_any_plmn = ( (NULL != pMccSet) && (NULL == pPlmnSet) );
  }

  /* If only MCC check is needed, compare against MCC of the PLMNs in list
   */
  if (checkMccOnly || is_srlte_on_any_plmn)
  {
    /* find a set of SVD MCCs, if not found then return error
      */
    if (pMccSet == NULL)
    {
      retval = POLICYMAN_STATUS_ERR_NOT_PRESENT;
      goto Done;
    }

    mcc    = policyman_plmn_get_mcc(pPlmnId);
    nElems = policyman_set_num_elems(pMccSet);
    
    for(i=0; i < nElems; i++)
    {
      listMcc = (sys_mcc_type *)policyman_set_elem_ptr(pMccSet, i);

      if (listMcc == NULL)
      {
        POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: listMcc is NULL");
        retval = POLICYMAN_STATUS_ERR_INVALID_ARGS;
        goto Done;
      }

      if(mcc == *listMcc)
      {
        *pSvdPossible = TRUE;
        break;
      }
    }
  }
  else
  {    
    // TODO: Alias 'sxlte_plmns' to 'svd_plmns' in XML and use 'svd_plmns' here
    /* find a set of SVD PLMNs, if not found then return error
      */
    if (pPlmnSet == NULL)
    {
      POLICYMAN_MSG_ERROR_0("policyman_svd_possible_on_plmn: set 'sxlte_plmns' is NULL");
      retval = POLICYMAN_STATUS_ERR_NOT_PRESENT;
      goto Done;
    }

    *pSvdPossible = policyman_set_contains(pPlmnSet, pPlmnId);
  }

Done:
  return retval;
}

/*-------- policyman_is_svlte_mcc --------*/
policyman_status_t policyman_is_svlte_mcc(
  sys_plmn_id_s_type  *pPlmnId,
  boolean             *pSvdPossible
  )
{
  policyman_set_t 	*pSet;
  sys_mcc_type		mcc;	
  policyman_status_t	retval = POLICYMAN_STATUS_SUCCESS;

  /* find a set of SVD MCCs, if not found then return error
   */
  pSet = (policyman_set_t*)policyman_policy_find_named_object( "svlte_mccs",
                                                                POLICYMAN_NAMED_MCC_SET );
  if(pSet == NULL)
  {
    retval = POLICYMAN_STATUS_ERR_NOT_PRESENT;
    goto Done;
  }

  mcc    = policyman_plmn_get_mcc(pPlmnId);
  *pSvdPossible = policyman_set_contains(pSet,&mcc);

Done:
  return retval;
}


/*-------- policyman_svd_possible_on_plmn --------*/
/**
@brief  Function to determine if SVD is possible on PLMN
*/
policyman_status_t policyman_svd_possible_on_plmn(
  sys_plmn_id_s_type  *pPlmnId,
  boolean             mccOnly,
  boolean             *pSvdPossible
  )
{
  sys_subs_feature_t     feature = SYS_SUBS_FEATURE_MODE_NORMAL;
  policyman_status_t     retval  = POLICYMAN_STATUS_SUCCESS;

  if (NULL == pPlmnId || NULL == pSvdPossible)
  {
    retval = POLICYMAN_STATUS_ERR_INVALID_ARGS;
    goto Done;
  }
  
  /* Get Device config Policy Item
   */
  {
    policyman_item_t    *pItem;
  retval = policyman_get_current_device_config((policyman_item_t **)&pItem); 

  if(retval != POLICYMAN_STATUS_SUCCESS)
  {
   goto Done;
  }

    (void)policyman_device_config_get_subs_feature(pItem, 0, SYS_MODEM_AS_ID_1, &feature);  
    ref_cnt_obj_release(pItem);
  }
  
  *pSvdPossible = FALSE;
  POLICYMAN_MSG_HIGH_0("queried PLMN is ");
  policyman_plmn_print(pPlmnId);

  if(feature == SYS_SUBS_FEATURE_MODE_SRLTE)
  {
    retval = policyman_is_srlte_plmn(pPlmnId,mccOnly,pSvdPossible);
  }
  else if(feature == SYS_SUBS_FEATURE_MODE_SVLTE)
  {
    retval = policyman_is_svlte_mcc(pPlmnId,pSvdPossible);
  }

Done:

  POLICYMAN_MSG_HIGH_3("policyman_svd_possible_on_plmn: feature=%d , retval=%d, svdpossible=%d",
                        feature,
                        retval,
                        *pSvdPossible );
  return retval;
}

/*-------- policyman_is_svd_allowed --------*/
policyman_status_t policyman_is_svd_allowed(
  boolean             *pIsSvd
  )
{
  if (!pIsSvd)
  {
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }

  *pIsSvd = policyman_boolean_get_value("sxlte_allowed");

  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_is_svd_operation_allowed --------*/
boolean policyman_is_svd_operation_allowed(
  sys_modem_as_id_e_type subsId
  )
{
  policyman_item_t      *pItem;
  policyman_status_t    status;
  sys_subs_feature_t    feature; 
  boolean               retval = FALSE;

  /* Check range for subsId
   */
  if(subsId < SYS_MODEM_AS_ID_1 || subsId > SYS_MODEM_AS_ID_3)
  {
    return retval;
  }

  /* Get Device config Policy Item
   */
  status = policyman_get_current_device_config((policyman_item_t **)&pItem); 

  /* UE is allowed for SVD if subs feature mode is not NORMAL and not SRLTE
   */
  if(status == POLICYMAN_STATUS_SUCCESS)
  {
    switch(subsId)
    {
      case SYS_MODEM_AS_ID_1:       

        /* get SUBS feature mode
             */
        status = policyman_device_config_get_subs_feature(pItem, 0, subsId, &feature);
        if(status == POLICYMAN_STATUS_SUCCESS)
        {
          retval = !(   feature == SYS_SUBS_FEATURE_MODE_NORMAL
                     || feature == SYS_SUBS_FEATURE_MODE_SRLTE
                    );
        }
        REF_CNT_OBJ_RELEASE_IF(pItem);
       break;

      /* Any other subscription is NORMAL
          */
      case SYS_MODEM_AS_ID_2:
      case SYS_MODEM_AS_ID_3:
      default:
        break;
    }
  }

  return retval;
}


/*-----------------------------------------------------------------------------
  VTable for policy_generic
-----------------------------------------------------------------------------*/
policyman_policy_vtbl_t  policyman_policy_generic_vtbl =
{
  policy_generic_new_instance,
  policy_generic_init_status,
  policy_generic_configure,
  policy_generic_execute,
  policy_generic_notify_service,
  policy_generic_handle_user_pref_update,
  NULL
};

