/**
  @file policy_device_config.c

  @brief  Policy which reads the device configuration and makes it available
          to the rest of modem.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_device_config.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre_regex.h"

#include "policyman_cfgitem.h"
#include "policyman_device_config.h"
#include "policyman_dbg.h"
#include "policyman_efs.h"
#include "policyman_policy.h"
#include "policyman_rat_capability.h"
#include "policyman_rules.h"
#include "policyman_subs.h"
#include "policyman_set.h"
#include "policyman_util.h"
#include "policyman_xml.h"

#include "trm.h"
#include <stringl/stringl.h>



static sys_subs_feature_t   g_mmodeFeature   = SYS_SUBS_FEATURE_MODE_NORMAL;
static sys_specialization_t g_specialization = SYS_SPECIAL_NONE;
static size_t               num_Sim          = 0;

#define DEVICE_MODE_NV_FILE "/nv/item_files/modem/mmode/device_mode"
#define POLICYMAN_DEVICE_CONFIG_NV_PATH "/policyman/device_config"
#define V1_SIZE (2*sizeof(size_t)+sizeof(sys_subs_feature_t)+sizeof(uint32)+sizeof(sys_specialization_t))
#define V2_SIZE (sizeof(pdc->num_sims)+sizeof(pdc->max_active_voice)+sizeof(pdc->configs[0].feature)+sizeof(pdc->configs[0].ratInfo)+sizeof(pdc->specialization)+sizeof(pdc->max_active_data))

/*-------- policyman_device_config_init --------*/
void policyman_device_config_init(
 void
)
{
  g_mmodeFeature   = SYS_SUBS_FEATURE_MODE_NORMAL;
  g_specialization = SYS_SPECIAL_NONE;
  num_Sim          = 0;
}

/*=============================================================================
  Device configuration item and APIs to access it.
=============================================================================*/

 /*-------- policyman_device_config_item_new --------*/
 device_configuration_t *
 policyman_device_config_item_new(
   size_t  dcSize
   )
 {
   return (device_configuration_t *) policyman_cfgitem_new_item(
                                      POLICYMAN_ITEM_DEVICE_CONFIGURATION,
                                      dcSize,
                                      policyman_simple_dtor
                                      );
 }


/*-------- policyman_check_trm_capability --------*/
boolean policyman_check_trm_capability(
  uint8 feature_mask
)
{
  uint8              capabilities;

  capabilities = policyman_state_get_trm_cap();
  return capabilities & feature_mask;
}


/*-------- policyman_device_config_get_default_feature --------*/
static sys_subs_feature_t policyman_device_config_get_default_feature(
  void
  )
{
  if (policyman_check_trm_capability(TRM_SVLTE_IS_ENABLED))
  {
    POLICYMAN_MSG_HIGH_0("returning Feature mode SYS_SUBS_FEATURE_MODE_SVLTE");
    return SYS_SUBS_FEATURE_MODE_SVLTE;
  }
  else
  {
    POLICYMAN_MSG_HIGH_0("returning Feature mode SYS_SUBS_FEATURE_MODE_NORMAL");
    return SYS_SUBS_FEATURE_MODE_NORMAL;
  }
}

/*-------- policyman_device_config_get_test_mode_feature --------*/
/*Make sure you call this after you check for test mode !*/
static sys_subs_feature_t policyman_device_config_get_test_mode_feature(
void
)
{
  sys_subs_feature_t feature = SYS_SUBS_FEATURE_MODE_NORMAL;

  if (policyman_check_trm_capability(TRM_SGLTE_SGTDS_IS_ENABLED))
  {
    feature = SYS_SUBS_FEATURE_MODE_SGLTE;
  }
  else if (policyman_check_trm_capability(TRM_SVLTE_IS_ENABLED))
  {
    feature = SYS_SUBS_FEATURE_MODE_SVLTE;
  }
  return feature;
   
}

/*-------- policyman_efs_put_file_device_config --------*/
static policyman_efs_status_t policyman_efs_put_file_device_config(
  policyman_item_t const    *pItem 
)
{
  int                     fd;
  fs_size_t               bytesWrite = 0;
  policyman_efs_status_t  efsStatus     = POLICYMAN_EFS_STATUS_SUCCESS;

  device_configuration_t *pdc = (device_configuration_t *)pItem;

  /* Try to open the file, return error if we can't
  */
  fd = efs_open(POLICYMAN_DEVICE_CONFIG_NV_PATH, O_WRONLY | O_CREAT | O_TRUNC);
  if (fd == -1)
  {
    efsStatus = POLICYMAN_EFS_STATUS_NOFILE;
    goto Done;
  }

  /* EFS file structure
      sizeof(nSIM)            = first 1 bytes = Number of supported SIMs
      sizeof(maxActive)     = next 1 bytes = Number of Max active SIMs
      sizeof(feature)         = next 1 bytes = Subsription 0 feature mode
      sizeof(ratMask)        = next 4 bytes = Rat mask       
      sizeof(specialization) = next 1 byte = Specialization
      sizeof(maxActiveData)  = next 1 bytes = Number of Max active Data
   */

  /* Write nSIM to file
    */
   bytesWrite += efs_write(fd, &pdc->num_sims, sizeof(pdc->num_sims));

  /* Write maxActive to file
    */
   bytesWrite += efs_write(fd, &pdc->max_active_voice, sizeof(pdc->max_active_voice));

  /* Write Subsription 0 feature mode to file
    */
   bytesWrite += efs_write(fd, &pdc->configs[0].feature, sizeof(pdc->configs[0].feature));

  /* Write ratMask to file
    */
   bytesWrite += efs_write(fd, &pdc->configs[0].ratInfo, sizeof(pdc->configs[0].ratInfo));

  /* Write specialization to file
    */
   bytesWrite += efs_write(fd, &pdc->specialization, sizeof(pdc->specialization));

  
  /* Write maxActiveData to file
    */
   bytesWrite += efs_write(fd, &pdc->max_active_data, sizeof(pdc->max_active_data));

   POLICYMAN_ASSERT( bytesWrite == V2_SIZE );

Done:
  if (fd != -1)
  {
    efs_close(fd);
  }

  return efsStatus;
}


/*-------- policyman_device_config_update_to_efs --------*/
void
policyman_device_config_update_to_efs(
  policyman_item_t const *pItem
)
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  policyman_efs_put_file_device_config(pItem);
  POLICYMAN_MSG_HIGH_4( "written device config into efs: numSims= %d, maxActiveVoice= %d, maxActiveData= %d, subs0: feature = %d", 
                        pdc->num_sims,
                        pdc->max_active_voice,
                        pdc->max_active_data,
                        pdc->configs[0].feature
                      );

  POLICYMAN_MSG_HIGH_2("                                ratMask= 0x%04x, specialization= %d",
                         pdc->configs[0].ratInfo,
                         pdc->specialization
                      );

  num_Sim = pdc->num_sims;
}


/*-------- policyman_rat_capability_get_default_from_efs --------*/
static boolean
policyman_retrieve_device_config(
 size_t               *pNSims, 
 size_t               *pMaxActiveVoice, 
 size_t               *pMaxActiveData, 
 sys_subs_feature_t   *pFeature,
 uint32               *pRatMask, 
 sys_specialization_t *pSpecialization
 )
{  
  int                       fd;
  struct fs_stat            st;
  
  boolean succeeded ;
  
  policyman_efs_status_t    status = POLICYMAN_EFS_STATUS_ERROR;

  memset(&st, 0x00, sizeof(struct fs_stat));
  
  *pMaxActiveData = 1 ;
  
  /* Try to open the file, return error if we can't
  */
  fd = efs_open(POLICYMAN_DEVICE_CONFIG_NV_PATH, O_RDONLY);
  if (fd == -1)
  {
    status = POLICYMAN_EFS_STATUS_NOFILE;
    goto Done;
  }
  
  /* Get the size of the file.
  */
  if ( efs_fstat(fd, &st) == -1 || 
       st.st_size < V1_SIZE
     )
  {
    goto Done;
  }
    
  /*  If we were able to read the EFS, set nSims, maxActive, ratMask and specialization value.
   */
   succeeded = (efs_read(fd, pNSims,     sizeof(size_t)) == sizeof(size_t)
      &&  efs_read(fd, pMaxActiveVoice, sizeof(size_t)) == sizeof(size_t)
      &&  efs_read(fd, pFeature, sizeof(sys_subs_feature_t)) == sizeof(sys_subs_feature_t)
      &&  efs_read(fd, pRatMask,   sizeof(uint32)) == sizeof(uint32)
      &&  efs_read(fd, pSpecialization, sizeof(sys_specialization_t)) == sizeof(sys_specialization_t)) ;
  
  if ( succeeded && st.st_size > V1_SIZE)
  {
    succeeded = efs_read(fd, pMaxActiveData, sizeof(size_t)) == sizeof(size_t) ;
  }

  if(succeeded)
  {
    POLICYMAN_MSG_HIGH_0("---------- Device Config from EFS");
    POLICYMAN_MSG_HIGH_6("  nSim= %d, maxActiveVoice= %d, maxActiveData= %d, subs[0] feature = %d, ratMask= 0x%04x, specialization= %d", 
                           *pNSims,
                           *pMaxActiveVoice,
                           *pMaxActiveData,
                           *pFeature,
                           *pRatMask,
                           *pSpecialization
                           );
    POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_HYPHEN);
    status = POLICYMAN_EFS_STATUS_SUCCESS;
  }
    
Done:
  
  if (fd != -1)
  {
    efs_close(fd);
  } 
  
  POLICYMAN_MSG_HIGH_2("policyman_retrieve_device_config returned status %d, filesize %d", status, st.st_size);
  
  return status == POLICYMAN_EFS_STATUS_SUCCESS;
}

 /*-------- policyman_device_config_get_default_from_efs --------*/
 policyman_item_t *
 policyman_device_config_get_default_from_efs(
   sys_modem_as_id_e_type subs_id
 )
{
  device_configuration_t  *pdc = NULL;
  size_t                  i;
  size_t                  dcSize;
  size_t                  nSims;
  size_t                  maxActiveVoice;
  size_t                  maxActiveData;
  uint32                  ratMask;
  sys_subs_feature_t      feature;
  sys_specialization_t    specialization;

  if(!policyman_retrieve_device_config(&nSims, &maxActiveVoice, &maxActiveData, &feature, &ratMask, &specialization))
  {
    POLICYMAN_MSG_ERROR_0("can't populate Device mode Item from EFS");
    goto Done;
  }

  /*  Get device configuration size from nSims
   */
  dcSize =  sizeof(device_configuration_t) + sizeof(subs_info_t) * nSims;

  pdc = policyman_device_config_item_new(dcSize);

  pdc->num_sims = nSims;
  pdc->max_active_voice = maxActiveVoice;
  pdc->max_active_data = maxActiveData;
  pdc->modified = FALSE;
  pdc->specialization = specialization;
  pdc->asubs_id = subs_id;

  /* Since we have got a valid config, update the static value
   */
  num_Sim = nSims;

  /*  We'll only deliver a single configuration when reading from EFS
        all other configurations are not supported.
   */
  pdc->num_cfgs = 1;

  /*  The first SIM gets hardware RAT capabilities. SUBS level feature 
        mode is always read from EFS since we definitely save it during EFS update.
   */
  pdc->configs[0].feature = feature;
  pdc->configs[0].ratInfo = ratMask;
  g_mmodeFeature = feature;
  
  /*  All other SIMs get GSM.
   */
  for (i = 1 ; i < nSims ; ++i)
  {
    pdc->configs[i].feature = SYS_SUBS_FEATURE_MODE_NORMAL;
    pdc->configs[i].ratInfo = SYS_SYS_MODE_MASK_GSM;
  }

Done:

  return (policyman_item_t *)pdc;
}



boolean
policyman_device_config_compare(
  policyman_item_t  *pData1,
  policyman_item_t  *pData2
  )
{
  device_configuration_t  *pdc1 = (device_configuration_t *) pData1;
  device_configuration_t  *pdc2 = (device_configuration_t *) pData2;
  size_t                  nCfgBytes;

  if (
        pdc1 == NULL
     || pdc2 == NULL
     || pdc1->num_sims != pdc2->num_sims
     || pdc1->num_cfgs != pdc2->num_cfgs
     || pdc1->max_active_voice != pdc2->max_active_voice
     || pdc1->max_active_data != pdc2->max_active_data
     )
  {
    return FALSE;
  }

  nCfgBytes = pdc1->num_sims * pdc1->num_cfgs * sizeof(uint32);

  return memcmp(pdc1->configs, pdc2->configs, nCfgBytes) == 0;
}



/*-------- policyman_device_config_display --------*/
void
policyman_device_config_display(
  policyman_item_t  *pItem
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;
  sys_overall_feature_t   feature;

  policyman_device_config_overall_feature(pItem, &feature);

  POLICYMAN_MSG_HIGH_0("Device configuration:");
  POLICYMAN_MSG_HIGH_4("  num_sims = %d, max_active_voice = %d, max_active_data = %d, subs[0] feature = %d", pdc->num_sims, pdc->max_active_voice, pdc->max_active_data, pdc->configs[0].feature);
  POLICYMAN_MSG_HIGH_3("  subs[0] mask = 0x%04x, overall feature = %d, specialization = %d", pdc->configs[0].ratInfo, feature, g_specialization);
}


/*-------- policyman_device_config_get_default_sims --------*/
void
policyman_device_config_get_default_sims(
  size_t  *pnSims,
  size_t  *pnActiveVoice,
  size_t  *pnActiveData
  )
{
  policyman_efs_status_t        status;
  void                          *pData = NULL;
  size_t                        size;
  sys_modem_device_mode_e_type  device_mode;
  policyman_rf_device_info_t    *pInfo = policyman_rf_get_overall_info();

  if (pInfo == NULL)
  {
    POLICYMAN_ERR_FATAL("No RF info when getting default device configuration", 0, 0, 0);
  }

  status = policyman_efs_get_file(DEVICE_MODE_NV_FILE, &pData, &size);
  
  if (
        status == POLICYMAN_EFS_STATUS_SUCCESS
     && size == sizeof(sys_modem_device_mode_e_type)
     )
  {
    device_mode = *((sys_modem_device_mode_e_type *) pData);
    POLICYMAN_MSG_HIGH_1("EFS read success device_mode %d", device_mode);
  }
  else
  {
    POLICYMAN_MSG_HIGH_0(DEVICE_MODE_NV_FILE " not present or invalid");
    device_mode = SYS_MODEM_DEVICE_MODE_SINGLE_SIM;
  }

  *pnActiveData = 1 ;

  switch (device_mode)
  {
    case SYS_MODEM_DEVICE_MODE_SINGLE_SIM:
      *pnSims = 1;
      *pnActiveVoice = 1;
      break;

    case SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_STANDBY:
      *pnSims = 2;
      *pnActiveVoice = 1;
      break;

    case SYS_MODEM_DEVICE_MODE_DUAL_SIM_DUAL_ACTIVE:
      *pnSims = 2;
      *pnActiveVoice = 2;
      break;

    case SYS_MODEM_DEVICE_MODE_TRIPLE_SIM_TRIPLE_STANDBY:
      *pnSims = 3;
      *pnActiveVoice = 1;
      break;

    default:
      POLICYMAN_MSG_ERROR_1("Unknown device mode from NV: %d, setting default device mode SINGLE_SIM", device_mode);
      *pnSims = 1;
      *pnActiveVoice = 1;
      break;
  }

  POLICYMAN_MEM_FREE_IF(pData);
}


/*-------- policyman_device_config_get_default --------*/
policyman_item_t *
policyman_device_config_get_default(
  sys_modem_as_id_e_type subsId
  )
{
  device_configuration_t   *pdc = NULL;
  size_t                   dcSize;
  size_t                   nSims = 1;
  size_t                   maxActiveVoice = 1;
  size_t                   maxActiveData = 1;
  sys_subs_feature_t       feature        = SYS_SUBS_FEATURE_MODE_NORMAL;
  sys_specialization_t     specialization = SYS_SPECIAL_NONE;
  uint32                   ratMask;

  size_t                      i;
  policyman_rf_device_info_t  *pInfo = policyman_rf_get_overall_info();

  /* Populate from EFS file device_mode
   */
  if (pInfo != NULL)
  {
    /*  In test mode we only work as single-SIM
     */
    if (!policyman_is_test_mode())
    {
      policyman_device_config_get_default_sims(&nSims, &maxActiveVoice, &maxActiveData);
    }
     
      /* For SINGLE sim config, keep the subs feature mode SVLTE if it is allowed so update fetaure with that
             Note: We are here because device mode item is not populated from XML so if NV 70266/EFS device mode
             has single SIM then keep SUBS feature mode as SVLTE. Anything else has to come from XML.
        */
      ratMask = pInfo->ratMask;
      if(nSims==1)
      {
        feature = policyman_is_test_mode()? 
                  policyman_device_config_get_test_mode_feature():
                  policyman_device_config_get_default_feature();
      }
    }
  /* Error case - can't populate device config
     */
    else
    {
      POLICYMAN_ERR_FATAL("No RF info when getting default device configuration", 0, 0, 0);
    }

  /*  We'll only deliver a single configuration when we're providing a default.
   */
  dcSize =  sizeof(device_configuration_t) + sizeof(subs_info_t) * nSims;

  pdc = policyman_device_config_item_new(dcSize);

  pdc->asubs_id = subsId;
  pdc->num_sims = nSims;
  pdc->max_active_voice = maxActiveVoice;
  pdc->max_active_data = maxActiveData;
  pdc->specialization = specialization;
  pdc->num_cfgs = 1;
  
  /* Since we have got a valid config, update the static value
   */
  num_Sim = nSims;

  /*  The first SIM gets hardware RAT capabilities.
        For overall feature mode MultiSIM, use subs feature mode as set by EFS read/deafult operation 
        to have legacy W/G+G DSDS/DSDA working
   */
  pdc->configs[0].feature = feature;
  pdc->configs[0].ratInfo = ratMask;
  g_mmodeFeature = feature;
  

  /*  All other SIMs get GSM.
   */
  for (i = 1 ; i < nSims ; ++i)
  {
    pdc->configs[i].feature = SYS_SUBS_FEATURE_MODE_NORMAL;
    pdc->configs[i].ratInfo = SYS_SYS_MODE_MASK_GSM;
  }

  POLICYMAN_MSG_HIGH_1("Default device configuration for subs %d created", subsId);

  return (policyman_item_t *) pdc;
}



/*-------- device_config_get_config --------*/
uint32
device_config_get_config(
  device_configuration_t const  *pdc,
  size_t                        nCfg,
  size_t                        nSim
  )
{
  size_t  index = nCfg * pdc->num_sims + nSim;

  return (nCfg >= pdc->num_cfgs || nSim > pdc->num_sims)?
              0
            : pdc->configs[index].ratInfo;
}


sys_subs_feature_t
device_config_get_subs_feature(
  device_configuration_t const  *pdc,
  size_t                        nCfg,
  size_t                        nSim
  )
{
  /*  SIM 0 of configuration 0 is the multimode subs configuration; everything
   *  else is just normal.
   */
  return (nCfg == 0 && nSim == 0)?
                g_mmodeFeature
              : SYS_SUBS_FEATURE_MODE_NORMAL;
}



/*-------- policyman_device_config_num_sims --------*/
policyman_status_t
policyman_device_config_num_sims(
  policyman_item_t const  *pItem,
  size_t                  *pnSims
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pnSims == NULL)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }
  
  *pnSims = pdc->num_sims;

  return (pdc->modified & DEV_CONFIG_NUM_SIMS_MODIFIED)?
          POLICYMAN_STATUS_WRN_MODIFIED : POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_config_max_active_voice --------*/
policyman_status_t
policyman_device_config_max_active_voice(
  policyman_item_t const  *pItem,
  size_t                  *pnMaxActive
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pnMaxActive == NULL)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  *pnMaxActive = pdc->max_active_voice ;

  return (pdc->modified & DEV_CONFIG_MAX_SUBS_MODIFIED)?
          POLICYMAN_STATUS_WRN_MODIFIED : POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_device_config_max_active_data --------*/
policyman_status_t
policyman_device_config_max_active_data(
  policyman_item_t const  *pItem,
  size_t                  *pnMaxActiveData
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pnMaxActiveData == NULL)
  { 
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
  { 
    return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID;
  }

  *pnMaxActiveData = pdc->max_active_data;

  return (pdc->modified & DEV_CONFIG_MAX_DATA_SUBS_MODIFIED)?
          POLICYMAN_STATUS_WRN_MODIFIED : POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_overall_config_feature --------*/
policyman_status_t
policyman_device_config_overall_feature(
  policyman_item_t const  *pItem,
  sys_overall_feature_t   *pFeature
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pFeature == NULL)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  if (pdc->num_sims > 1)
  {
    *pFeature = SYS_OVERALL_FEATURE_MODE_MULTISIM;
  }
  else if (g_mmodeFeature == SYS_SUBS_FEATURE_MODE_SGLTE)
  {
    *pFeature = SYS_OVERALL_FEATURE_MODE_SGLTE;
  }
  else if (g_mmodeFeature == SYS_SUBS_FEATURE_MODE_SVLTE)
  {
    *pFeature = SYS_OVERALL_FEATURE_MODE_SVLTE;
  }
  else if (g_mmodeFeature == SYS_SUBS_FEATURE_MODE_SRLTE)
  {
    *pFeature = SYS_OVERALL_FEATURE_MODE_SRLTE;
  }
  else
  {
    *pFeature = SYS_OVERALL_FEATURE_MODE_NORMAL;
  }

  return (pdc->modified & DEV_CONFIG_FEATURE_MODIFIED)?
          POLICYMAN_STATUS_WRN_MODIFIED : POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_config_num_cfgs --------*/
policyman_status_t
policyman_device_config_num_cfgs(
  policyman_item_t const  *pItem,
  size_t                  *pnCfgs
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pnCfgs == NULL)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  *pnCfgs = pdc->num_cfgs;
  return POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_config_get_config --------*/
policyman_status_t
policyman_device_config_get_config(
  policyman_item_t const  *pItem,
  size_t                  nCfg,
  size_t                  nSim,
  uint32                  *pRatCfg
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pRatCfg == NULL || nCfg >= pdc->num_cfgs || nSim >= pdc->num_sims)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  /*  For any configuration but the first, or a third SIM, return the
   *  old values.
   *  For the first configuration, return hardware capability for SIM 1,
   *  GSM-only for SIM 2.
   */
  if (0 != nCfg || nSim > 2)
  {
    *pRatCfg = device_config_get_config(pdc, nCfg, nSim);
  }
  else if (0 == nSim)
  {
    sys_band_mask_type  dontCare;
    policyman_get_hardware_rats_bands(pRatCfg, &dontCare, &dontCare, &dontCare);
  }
  else
  {
    *pRatCfg = SYS_SYS_MODE_MASK_GSM;
  }
  
  return POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_config_get_subs_feature --------*/
policyman_status_t
policyman_device_config_get_subs_feature(
  policyman_item_t const  *pItem,
  size_t                  nCfg,
  size_t                  nSim,
  sys_subs_feature_t      *pFeature
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pFeature == NULL || nCfg >= pdc->num_cfgs || nSim >= pdc->num_sims)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  *pFeature = device_config_get_subs_feature(pdc, nCfg, nSim);
  return POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_device_config_get_specialization --------*/
policyman_status_t
policyman_device_config_get_specialization(
  policyman_item_t const  *pItem,
  sys_specialization_t    *pSpecial
  )
{
  device_configuration_t  *pdc = (device_configuration_t *) pItem;

  if (pdc == NULL || pSpecial == NULL)
    { return POLICYMAN_STATUS_ERR_INVALID_ARGS; }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_DEVICE_CONFIGURATION)
    { return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; }

  *pSpecial = g_specialization;
  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_get_current_device_config --------*/
policyman_status_t
policyman_get_current_device_config(
  policyman_item_t ** pdc
  )
{  
  policyman_item_id_t  id     = POLICYMAN_ITEM_DEVICE_CONFIGURATION;
  policyman_status_t   status = POLICYMAN_STATUS_SUCCESS;

  status = policyman_get_items( (policyman_item_id_t const *)&id , 
                                1, 
                                (policyman_item_t const **)pdc
                              );

  return status;
}

/*-------- policyman_get_current_num_sim --------*/
policyman_status_t
policyman_get_current_num_sim(
  size_t *pnSim
  )
{
  device_configuration_t   *pdc = NULL;
  policyman_status_t   status = POLICYMAN_STATUS_SUCCESS;

  /* Return nSIM if it has been read once
       The value doesn't change in run time
   */
  if(num_Sim > 0)
  {
    *pnSim = num_Sim;
    return status;    
  }

  status = policyman_get_current_device_config((policyman_item_t **)&pdc);

  if(status == POLICYMAN_STATUS_SUCCESS)
  {
    *pnSim = pdc->num_sims;

    /* Update nSim as well if we have got info correctly
      */
      num_Sim = *pnSim;
    }

  REF_CNT_OBJ_RELEASE_IF(pdc);
  return status;
}

/*=============================================================================
  Device configuration action parsing and APIs
=============================================================================*/


typedef struct
{
  sys_subs_feature_t    feature;
  rat_capability_info_t ratInfo;
} subs_info_action_t;


typedef struct
{
  POLICYMAN_ACTION_BASE;

  size_t                num_sims;
  size_t                max_active_voice;
  size_t                max_active_data;
  size_t                num_cfgs;
  uint32                modified;
  sys_specialization_t  specialization;
  const char            *pConfigName;
  subs_info_action_t    subs_info[];
} device_config_action_t;


/*-------- device_config_update_possible --------*/
static boolean
device_config_update_possible(
  void
  )
{
  size_t activeSubs = policyman_get_num_subs();
  
  if (   !policyman_is_initializing()
      && activeSubs < 2
     )
  {
    POLICYMAN_MSG_HIGH_1("Unable to change device config with active subs = %d", activeSubs);
    return FALSE;
  }

  return TRUE;
}


/*-------- policyman_feature_is_supported_by_hardware --------*/
boolean policyman_feature_is_supported_by_hardware(
  policyman_set_t   *pItemSet
  )
{
  boolean                       is_supported = TRUE;
  device_configuration_t      **ppItem = NULL;

  ppItem = policyman_cfgitem_find_item_in_itemset( pItemSet, POLICYMAN_ITEM_DEVICE_CONFIGURATION, SYS_MODEM_AS_ID_1 );

  if (NULL != ppItem)
  {   
    sys_subs_feature_t  feature;
    feature = device_config_get_subs_feature(*ppItem, 0, 0);

    switch (feature)
    {
      case SYS_SUBS_FEATURE_MODE_NORMAL:
        break;

      case SYS_SUBS_FEATURE_MODE_SVLTE:
        if (!policyman_check_trm_capability(TRM_SVLTE_IS_ENABLED))
        {
          POLICYMAN_MSG_ERROR_0("SVLTE specified in configuration, but not supported");
          is_supported = FALSE;
        }
        break;

      case SYS_SUBS_FEATURE_MODE_SRLTE:
        /* Don't check TRM capabilities for SVLTE/SRLTE
         */
        break;

      case SYS_SUBS_FEATURE_MODE_SGLTE:
        if (!policyman_check_trm_capability(TRM_SGLTE_SGTDS_IS_ENABLED))
        {
          POLICYMAN_MSG_ERROR_0("SGLTE specified in configuration, but not supported");
          is_supported = FALSE;
        }
        break;

      default:
        POLICYMAN_MSG_ERROR_1("Unimplemented feature type: %d", feature);
        break;
    }
  }

  return is_supported;
}


/*-------- policyman_device_config_get_feature --------*/
static boolean policyman_device_config_get_feature(
  char const          *pStr,
  sys_subs_feature_t  *pFeature
  )
{
  boolean     succeeded = TRUE;
  char        token[32];
  size_t      i;


  /*  Element that maps a feature name to a feature.
   */
  typedef struct
  {
    char const          *pName;
    sys_subs_feature_t  feature;
  } feature_elem;

  /*  Array of feature elements.
   */
  static feature_elem features[] =
  {
    {"normal",  SYS_SUBS_FEATURE_MODE_NORMAL},
    {"svlte",   SYS_SUBS_FEATURE_MODE_SVLTE},
    {"sglte",   SYS_SUBS_FEATURE_MODE_SGLTE},
    {"srlte",   SYS_SUBS_FEATURE_MODE_SRLTE},
  };

  /*  If nothing is specified, use NORMAL.
   */
  if (!policyman_get_token(&pStr, token, sizeof(token)))
  {
    *pFeature = SYS_SUBS_FEATURE_MODE_NORMAL;
    goto Done;
  }

  /*  If the attribute is present, map the name to the feature.
   */
  for (i = 0 ; i < ARR_SIZE(features) ; ++i)
  {
    feature_elem  *pfe = &features[i];
    if (strcasecmp(token, pfe->pName) == 0)
    {
      *pFeature = pfe->feature;
      goto Done;
    }
  }

  succeeded = FALSE;
  POLICYMAN_MSG_ERROR_0("Invalid feature value in <device_configuration>");
  POLICYMAN_UT_MSG_1("feature = \"%s\" specified in <device_configuration>", pStr);
    
Done:
  return succeeded;
}


/*-------- policyman_device_config_get_subs_info --------*/
static mre_status_t policyman_device_config_get_subs_info(
  device_config_action_t        *pAction,
  policyman_xml_element_t const *pElem,
  size_t                        nCfg
  )
{
  mre_status_t        status = MRE_STATUS_ERR_INVALID_ACTION;
  size_t              subsId;
  size_t              subsIndex;
  subs_info_action_t  *pSubsInfo;
  const char          *pStr;

  pStr = policyman_xml_get_attribute(pElem, "id");
  if (pStr == NULL)
  {
    goto Done;
  }

  subsId = atoi(pStr);
  if (subsId > pAction->num_sims)
  {
    POLICYMAN_MSG_ERROR_2("device_configuration: config has a subs id of %d, but there are only %d SIMs",
                          subsId, pAction->num_sims);
    goto Done;
  }

  /* convert subsId to an index
  */
  subsIndex = pAction->num_sims * nCfg + (subsId - 1);
  pSubsInfo = &pAction->subs_info[subsIndex];

  /*  Get any feature mode associated with this subs - default is "normal".
   */
  pSubsInfo->feature = SYS_SUBS_FEATURE_MODE_NORMAL;
  pStr = policyman_xml_get_attribute(pElem, "feature");
  if (pStr != NULL)
  {
    POLICYMAN_MSG_ERROR_0("Setting features in <device_configuration> is deprecated");

    if (!policyman_device_config_get_feature(pStr, &pSubsInfo->feature))
    {
      goto Done;
    }

    pAction->modified |= 0;
  }

  pElem = policyman_xml_get_child(pElem, 0);
  if (pElem == NULL)
  {
    goto Done;
  }

  status = policyman_rat_capability_read_info(
                  pElem,
                  &pSubsInfo->ratInfo
                  );

Done:
  return status;
}


/*-------- policyman_device_config_read_cfg --------*/
static mre_status_t policyman_device_config_read_cfg(
  device_config_action_t        *pAction,
  const policyman_xml_element_t *pChild,
  size_t                        nCfg
  )
{
  mre_status_t  status = MRE_STATUS_ERR_INVALID_ACTION;
  size_t              i;
  size_t              nSIMs;

  nSIMs = policyman_xml_num_children(pChild);
  if (nSIMs != pAction->num_sims)
  {
    POLICYMAN_UT_MSG_3("device_configuration: number of SIMs is %d but configuration %d has %d elements",
                          pAction->num_sims, nCfg, nSIMs);
    goto Done;
  }

  status = MRE_STATUS_SUCCESS;
  for (i = 0 ; !MRE_FAILED(status) && i < nSIMs ; ++i)
  {
    const policyman_xml_element_t *pSubs = policyman_xml_get_child(pChild, i);
    if (pSubs == NULL)    { break; }
    if (!policyman_xml_tag_is(pSubs, "subs"))
    {
      POLICYMAN_UT_MSG_0("device_configuration: top level elements of 'config' must be 'subs'");
      goto Done;
    }

    status = policyman_device_config_get_subs_info(pAction, pSubs, nCfg);
  }

Done:
  return status;
}


/*-------- policyman_device_config_get_num_active_voice --------*/
static boolean policyman_device_config_get_num_active_voice(
  size_t                        *pnActive,
  policyman_xml_element_t const *pCfg
  )
{
  const char  *pStr;
  boolean     succeeded = FALSE;

  if (pnActive == NULL || pCfg == NULL)   { goto Done; }

  pStr = policyman_xml_get_attribute(pCfg, "max_active");

  if(pStr == NULL)
  {
    pStr = policyman_xml_get_attribute(pCfg, "max_active_voice");
  }
  
  succeeded = (pStr != NULL);

  if (succeeded)
  {
    *pnActive= atoi(pStr);
  }
  else
  {
    POLICYMAN_UT_MSG_0("<device_configuration> requires a max_active/max_active_voice attribute");
  }

Done:
  return succeeded;
}

/*-------- policyman_device_config_get_num_active_data --------*/
static boolean policyman_device_config_get_num_active_data(
  size_t                        *pnActiveData,
  policyman_xml_element_t const *pCfg
  )
{
  const char  *pStr;
  boolean     succeeded = FALSE;

  if (pnActiveData == NULL || pCfg == NULL)   { goto Done; }

  pStr = policyman_xml_get_attribute(pCfg, "max_active_data");
  succeeded = (pStr != NULL);

  if (succeeded)
  {
    *pnActiveData= atoi(pStr);
  }
  else
  {
    succeeded = TRUE ;
    *pnActiveData= 1;
    POLICYMAN_MSG_HIGH_0("<device_configuration> max_active_data is 1 as attribute is not present ");
  }

Done:
  return succeeded;
}


/*-------- policyman_device_config_get_num_sims --------*/
static boolean policyman_device_config_get_num_sims(
  size_t                        *pnSims,
  const policyman_xml_element_t *pElem
  )
{
  const char  *pStr;
  boolean     succeeded = FALSE;

  if (pnSims == NULL || pElem == NULL)   { goto Done; }

  pStr = policyman_xml_get_attribute(pElem, "num_sims");
  succeeded = (pStr != NULL);

  if (succeeded)
  {
    *pnSims = atoi(pStr);
  }
  else
  {
    POLICYMAN_UT_MSG_0("<device_configuration> requires a num_sims attribute");
  }

Done:
  return succeeded;
}


/*-------- policyman_device_config_get_special --------*/
static boolean
policyman_device_config_get_special(
  const char            *pStr,
  sys_specialization_t  *pSpecialization
  )
{
  boolean     succeeded = TRUE;
  size_t      i;

  typedef struct
  {
    char const            *pName;
    sys_specialization_t  specialization;
  } special_map_t;

  special_map_t special_map[] =
  {
    {"none",                  SYS_SPECIAL_NONE},
    {"sbm_rat_search_order",  SYS_SPECIAL_SBM_RAT_SEARCH_ORDER},
    {"slte",                  SYS_SPECIAL_SLTE},
    {"dsda_roam",             SYS_SPECIAL_DSDA_ROAM},
    {"dsds_roam",             SYS_SPECIAL_DSDS_ROAM},
  };

  if (pStr == NULL)
  {
    *pSpecialization = SYS_SPECIAL_NONE;
    goto Done;
  }

  for (i = 0 ; i < ARR_SIZE(special_map) ; ++i)
  {
    if (strcasecmp(pStr, special_map[i].pName) == 0)
    {
      *pSpecialization = special_map[i].specialization;
      goto Done;
    }
  }

  POLICYMAN_UT_MSG_1("specialization attribute of <device_configuration> invalid: %s", pStr);
  succeeded = FALSE;

Done:
  return succeeded;
}


/*-------- policyman_device_config_validate_sims --------*/
static uint32
policyman_device_config_validate_sims(
  size_t  *pnSims
  )
{
  // TODO: Implement check with UIM for this value.
  return 0;
}

  // TODO: Implement check with TRM for this value.
/*-------- policyman_device_config_validate_subs --------*/
#if 0
static uint32
policyman_device_config_validate_subs(
  size_t  *pnMaxActive
  )
{
  return 0;
}
#endif

/*-------- policyman_device_config_create --------*/
static device_configuration_t *
policyman_device_config_create(
  device_config_action_t  *pdcAction,
  policyman_state_t       *pState,
  policyman_set_t         *pItemSet
  )
{
  device_configuration_t  *pdc;
  size_t                  dcSize;
  size_t                  nSim;
  size_t                  nCfg;
  size_t                  index;

  POLICYMAN_MSG_HIGH_0("action <device_configuration> executing");

  dcSize =  sizeof(device_configuration_t) +
            sizeof(subs_info_t) * pdcAction->num_sims * pdcAction->num_cfgs;
  
  pdc = (device_configuration_t *) policyman_cfgitem_new_item(
                                      POLICYMAN_ITEM_DEVICE_CONFIGURATION,
                                      dcSize,
                                      policyman_simple_dtor
                                      );

  pdc->num_sims = pdcAction->num_sims;
  pdc->max_active_voice = pdcAction->max_active_voice;
  pdc->max_active_data = pdcAction->max_active_data;
  pdc->modified = pdcAction->modified;
  pdc->num_cfgs = pdcAction->num_cfgs;
  pdc->specialization = pdcAction->specialization;

  num_Sim = pdc->num_sims;

  for (nCfg = 0 ; nCfg < pdcAction->num_cfgs ; ++nCfg)
  {
    for (nSim = 0 ; nSim < pdcAction->num_sims ; ++nSim)
    {
      index = nCfg * pdcAction->num_sims + nSim;
      pdc->configs[index].feature = pdcAction->subs_info[index].feature;
      pdc->configs[index].ratInfo = policyman_rat_capability_evaluate(
                                      &pdcAction->subs_info[index].ratInfo,
                                      pState,
                                      pItemSet
                                      );
    }
  }

  /*  If the feature for the MMODE subs is not the same as what we have,
   *  warn that setting feature through <device_configuration> is deprecated,
   *  but set the MMODE feature to the new value.
   */
  if (g_mmodeFeature != pdc->configs[0].feature)
  {
    POLICYMAN_MSG_HIGH_2("Setting MMODE feature from %d to %d via <device_configuration>",
                          g_mmodeFeature, pdc->configs[0].feature);
    g_mmodeFeature = pdc->configs[0].feature;
  }

  /*  If the specialization is not the same as the global value, warn that this
   *  is deprecated, but set global anyway.
   */
  if (g_specialization != pdc->specialization)
  {
    POLICYMAN_MSG_HIGH_2("Setting specialization from %d to %d via <device_configuration>",
                          g_specialization, pdc->specialization);
    g_specialization = pdc->specialization;
  }

  return pdc;
}


/*-------- policyman_device_config_execute --------*/
static boolean
policyman_device_config_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  device_config_action_t  *pdcAction = (device_config_action_t *) pAction;
  policy_execute_ctx_t    *pdcCtx = (policy_execute_ctx_t *) pCtx;
  device_configuration_t  *pdc        = NULL;

  /* If we are not initializing then don't update 
        device config until first SUB is active
   */
  if (device_config_update_possible())
  {
    pdc = policyman_device_config_create(pdcAction, pdcCtx->pState, pdcCtx->pItemSet);
    policyman_set_replace(pdcCtx->pItemSet, &pdc);
    ref_cnt_obj_release(pdc);
  }

  return TRUE;
}


/*-------- policyman_device_config_present --------*/
static boolean
policyman_device_config_present(
  void
  )
{
  boolean                 isPresent;
  device_configuration_t  *pdc;

  pdc = (device_configuration_t *) policyman_cfgitem_get_item(
                                        POLICYMAN_ITEM_DEVICE_CONFIGURATION,
                                        SYS_MODEM_AS_ID_1
                                        );
  isPresent = (pdc != NULL && (pdc->modified & DEV_CONFIG_NUM_SIMS_MODIFIED) == 0);

  REF_CNT_OBJ_RELEASE_IF(pdc);

  return isPresent;
}


/*-------- policyman_device_config_if_execute --------*/
static boolean
policyman_device_config_if_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean succeeded;

  succeeded = policyman_device_config_present();
  if (succeeded)
  {
    POLICYMAN_UT_MSG_0("<device_config_if> skipping update - item present");
  }
  else
  {
    succeeded = policyman_device_config_execute(pAction, pCtx);
  }

  return succeeded;
}


/*-------- policyman_device_config_action_dtor --------*/
static void
policyman_device_config_action_dtor(
  void  *pObj
  )
{
  device_config_action_t  *pMe = (device_config_action_t *) pObj;

  POLICYMAN_MEM_FREE_IF(pMe->pConfigName);
  policyman_action_dtor(pObj);
}


/*-------- policyman_device_config_create_new_action --------*/
mre_status_t
policyman_device_config_create_new_action(
  policyman_xml_element_t const *pElem,
  policyman_policy_t            *pPolicy,
  policyman_action_t            **ppAction,
  pfn_execute_t                 executefn
  )
{
  mre_status_t            status = MRE_STATUS_ERR_INVALID_ACTION;
  device_config_action_t  *pAction = NULL;
  size_t                  nSims;
  size_t                  nMaxVoice;
  size_t                  nMaxData = 1;
  size_t                  nCfgs;
  const char              *pStr;
  sys_specialization_t    specialization;
  size_t                  i;
  uint32                  modified;


  /*  Get the number of SIM slots, simultaneously active subscriptions, and specialization.
   */
  if (!policyman_device_config_get_num_sims(&nSims, pElem))         { goto Done; }
  if (!policyman_device_config_get_num_active_voice(&nMaxVoice, pElem))        { goto Done; }
  if (!policyman_device_config_get_num_active_data(&nMaxData, pElem)) { goto Done; }
  nCfgs = policyman_xml_num_children(pElem);

  /*  Deal with the specialization, which should no longer be set here.
   */
  pStr = policyman_xml_get_attribute(pElem, "specialization");
  if (pStr != NULL)
  {
    POLICYMAN_MSG_ERROR_0("'specialization' attribute of <device_configuration> is deprecated");
  }
  if (!policyman_device_config_get_special(pStr, &specialization))  { goto Done; }

  /*  Validate that the number of SIMs is supported by the hardware.
   *  Modify the value to match hardware, if necessary.
   */
  modified = policyman_device_config_validate_sims(&nSims);

  /*  Allocate the action.
   */
  pAction = (device_config_action_t *)
                  policyman_mem_alloc(
                    sizeof(device_config_action_t) +
                    sizeof(subs_info_action_t) * nSims * nCfgs
                    );
  ref_cnt_obj_init(pAction, policyman_device_config_action_dtor);

  /*  Initialize the action.
   */
  pAction->execute = executefn;
  pAction->num_sims = nSims;
  pAction->max_active_voice = nMaxVoice;
  pAction->max_active_data = nMaxData;
  pAction->num_cfgs = nCfgs;
  pAction->modified |= modified;
  pAction->specialization = specialization;

  for (i = 0 ; i < nCfgs ; ++i)
  {
    const policyman_xml_element_t *pChild = policyman_xml_get_child(pElem, i);

    if (pChild == NULL || !policyman_xml_tag_is(pChild, "config"))
    {
      POLICYMAN_UT_MSG_0("Elements of device_configuration action must have tag 'config'");
      goto Done;
    }

    status = policyman_device_config_read_cfg(pAction, pChild, i);
    if (MRE_FAILED(status))
    {
      goto Done;
    }
  }

  status = MRE_STATUS_SUCCESS;

Done:
  if (MRE_FAILED(status))
  {
    REF_CNT_OBJ_RELEASE_IF(pAction);
  }

  *ppAction = (policyman_action_t *) pAction;

  return status;
}


/*-------- policyman_device_config_action_new --------*/
mre_status_t
policyman_device_config_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_device_config_create_new_action(
              pElem, (policyman_policy_t *) pPolicy, ppAction,
              (pfn_execute_t) policyman_device_config_execute);
}


/*-------- policyman_device_config_if_action_new --------*/
mre_status_t
policyman_device_config_if_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_device_config_create_new_action(
              pElem, (policyman_policy_t *) pPolicy, ppAction,
              (pfn_execute_t) policyman_device_config_if_execute);
}


/*=============================================================================
  <define_config>
=============================================================================*/


/*-------- named_device_configuration_t --------*/
typedef struct
{
  POLICYMAN_NAMED_OBJECT_BASE;

  device_configuration_t  *pdc;
} named_device_config_t;


/*-------- policyman_named_config_dtor --------*/
static void
policyman_named_config_dtor(
  void  *pObj
  )
{
  named_device_config_t *pMe = (named_device_config_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pMe->pdc);
  policyman_named_object_dtor(pObj);
}


/*-------- policyman_device_config_define_execute --------*/
static boolean
policyman_device_config_define_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean                 succeeded;
  device_config_action_t  *pdcAction = (device_config_action_t *) pAction;
  policy_execute_ctx_t    *pdcCtx = (policy_execute_ctx_t *) pCtx;
  device_configuration_t  *pdc = policyman_device_config_create(pdcAction, pdcCtx->pState, pdcCtx->pItemSet);
  named_device_config_t   *pNamedConfig;

  pNamedConfig = (named_device_config_t *)
                    policyman_mem_alloc(sizeof(named_device_config_t));
  ref_cnt_obj_init(pNamedConfig, policyman_named_config_dtor);

  policyman_named_object_init(
            (policyman_named_object_t *) pNamedConfig,
            pdcAction->pConfigName,
            POLICYMAN_NAMED_CONFIG
            );
  pNamedConfig->pdc = pdc;

  succeeded = policyman_policy_add_named_object(
                  pdcAction->pPolicy,
                  (policyman_named_object_t *) pNamedConfig
                  );
  ref_cnt_obj_release(pNamedConfig);

  if (succeeded)
  {
    POLICYMAN_MSG_HIGH_0("<define_config>: named configuration successfully created");
    POLICYMAN_UT_MSG_1("   name = %s", pNamedConfig->pName);
  }

  return succeeded;
}


/*-------- policyman_device_config_define_new --------*/
mre_status_t
policyman_device_config_define_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  mre_status_t            status;
  device_config_action_t  *pAction;
  char const              *pStr;
  
  status = policyman_device_config_create_new_action(
              pElem, (policyman_policy_t *) pPolicy, ppAction,
              (pfn_execute_t) policyman_device_config_define_execute);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pStr = policyman_xml_get_attribute(pElem, "name");
  if (pStr == NULL)
  {
    POLICYMAN_MSG_ERROR_0("<define_config> requires a \"name\" attribute");
    status = MRE_STATUS_ERR_INVALID_ACTION;
    goto Done;
  }

  pAction = (device_config_action_t *) *ppAction;
  pAction->pConfigName = policyman_str_dup(pStr);

Done:
  if (MRE_FAILED(status))
  {
    REF_CNT_OBJ_RELEASE_IF(*ppAction);
  }

  return status;
}


/*=============================================================================
  <use_config>
=============================================================================*/

typedef struct
{
  POLICYMAN_ACTION_BASE;

  char const              *pConfigName;
  device_configuration_t  *pConfig;
} use_config_action_t;


static void
policyman_use_config_action_dtor(
  void  *pObj
  )
{
  use_config_action_t *pMe = (use_config_action_t *) pObj;

  POLICYMAN_MEM_FREE_IF(pMe->pConfigName);
  REF_CNT_OBJ_RELEASE_IF(pMe->pConfig);
  policyman_action_dtor(pObj);
}


/*-------- policyman_device_config_use_execute --------*/
static boolean
policyman_device_config_use_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean                 succeeded = FALSE;
  use_config_action_t     *pCfgAct = (use_config_action_t *) pAction;
  named_device_config_t   *pNamedCfg;
  device_configuration_t  *pConfig;

  if (!device_config_update_possible())
  {
    succeeded = TRUE;
    goto Done;
  }
  
  /*  If there is a name in the configuration item, look up the 
   */
  if (pCfgAct->pConfigName != NULL)
  {
    pNamedCfg = (named_device_config_t *)
                      mre_named_object_find(
                          pCfgAct->pConfigName,
                          POLICYMAN_NAMED_CONFIG,
                          pAction->pPolicy
                          );
    if (pNamedCfg == NULL)
    {
      POLICYMAN_MSG_ERROR_0("<use_config>: unable to find named configuration");
      goto Done;
    }
    if (pNamedCfg->pdc == NULL)
    {
      POLICYMAN_MSG_ERROR_0("<use_config>: named configuration has no config item");
      goto Done;
    }

    pCfgAct->pConfig = pNamedCfg->pdc;
    ref_cnt_obj_add_ref(pCfgAct->pConfig);

    policyman_mem_free((void *) pCfgAct->pConfigName);
    pCfgAct->pConfigName = NULL;
  }

  pConfig = pCfgAct->pConfig;
  if (pConfig == NULL)
  {
    POLICYMAN_MSG_ERROR_0("<use_config>: no configuration");
    goto Done;
  }

  policyman_set_replace(((policy_execute_ctx_t *) pCtx)->pItemSet, &pConfig);
  POLICYMAN_MSG_HIGH_3("<use_config>: configuration with num_sims = %d, max_active_voice = %d, max_active_data = %d",
                        pConfig->num_sims, pConfig->max_active_voice , pConfig->max_active_data);
  succeeded = TRUE;

Done:
  return succeeded;
}


/*-------- policyman_device_config_use_if_execute --------*/
static boolean
policyman_device_config_use_if_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  boolean succeeded;

  succeeded = policyman_device_config_present();
  if (succeeded)
  {
    POLICYMAN_UT_MSG_0("<use_config_if> skipping update - item present");
  }
  else
  {
    succeeded = policyman_device_config_use_execute(pAction, pCtx);
  }

  return succeeded;
}


/*-------- policyman_device_config_use_create --------*/
mre_status_t
policyman_device_config_use_create(
  policyman_xml_element_t const *pElem,
  policyman_policy_t            *pPolicy,
  policyman_action_t            **ppAction,
  pfn_execute_t                 executefn
  )
{
  mre_status_t        status = MRE_STATUS_ERR_INVALID_ACTION;
  char const          *pStr;
  use_config_action_t *pAction;
  
  pStr = policyman_xml_get_attribute(pElem, "name");
  if (pStr == NULL)
  {
    POLICYMAN_MSG_ERROR_0("<use_config> requires a \"name\" attribute");
    goto Done;
  }

  pAction = (use_config_action_t *) policyman_mem_alloc(sizeof(use_config_action_t));
  ref_cnt_obj_init(pAction, policyman_use_config_action_dtor);

  pAction->execute = executefn;
  pAction->pConfigName = policyman_str_dup(pStr);

  *ppAction = (policyman_action_t *) pAction;
  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}


/*-------- policyman_device_config_use_new --------*/
mre_status_t
policyman_device_config_use_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_device_config_use_create(
              pElem, (policyman_policy_t *) pPolicy, ppAction,
              (pfn_execute_t) policyman_device_config_use_execute
              );
}


/*-------- policyman_device_config_use_if_new --------*/
mre_status_t
policyman_device_config_use_if_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  return policyman_device_config_use_create(
              pElem, (policyman_policy_t *) pPolicy, ppAction,
              (pfn_execute_t) policyman_device_config_use_if_execute
              );
}


/*=============================================================================
  <feature>

  NOTE:
  The <feature> action is only supported as part of the <initial> block, and
  sets the global feature and specialization states.  This doesn't result
  in a device_configuration item change, because that is not expected to change.
=============================================================================*/


typedef struct
{
  POLICYMAN_ACTION_BASE;

  sys_subs_feature_t    feature;
  sys_specialization_t  specialization;
} feature_action_t;


/*-------- policyman_device_config_feature_execute --------*/
static boolean
policyman_device_config_feature_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  feature_action_t  *pfAction = (feature_action_t *) pAction;

  g_mmodeFeature = pfAction->feature;
  g_specialization = pfAction->specialization;

  return TRUE;
}


/*-------- policyman_device_config_feature_new --------*/
mre_status_t
policyman_device_config_feature_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  )
{
  mre_status_t          status = MRE_STATUS_ERR_MALFORMED_XML;
  char const            *pStr;
  sys_subs_feature_t    feature;
  sys_specialization_t  specialization;
  feature_action_t      *pAction = NULL;

  
  pStr = policyman_xml_get_text(pElem);
  if (!policyman_device_config_get_feature(pStr, &feature))
  {
    goto Done;
  }

  pStr = policyman_xml_get_attribute(pElem, "specialization");
  if (!policyman_device_config_get_special(pStr, &specialization))
  {
    goto Done;
  }

  pAction = (feature_action_t *) policyman_mem_alloc(sizeof(feature_action_t));
  ref_cnt_obj_init(pAction, policyman_action_dtor);

  pAction->execute = (pfn_execute_t) policyman_device_config_feature_execute;
  pAction->feature = feature;
  pAction->specialization = specialization;

  status = MRE_STATUS_SUCCESS;

Done:
  *ppAction = (policyman_action_t *) pAction;
  return status;
}


/*-----------------------------------------------------------------------------
  <imei> condition
-----------------------------------------------------------------------------*/


/*-------- policyman_imei_condition_t --------*/
typedef struct
{
  POLICYMAN_CONDITION_BASE;

  mre_regex_t *pRegEx;
} policyman_imei_condition_t;


/*-------- policyman_condition_imei_dtor --------*/
static void
policyman_condition_imei_dtor(
  void  *pObj
  )
{
  policyman_imei_condition_t *pCond = (policyman_imei_condition_t *) pObj;

  if (pCond->pRegEx != NULL)
  {
    ref_cnt_obj_release(pCond->pRegEx);
  }

  policyman_condition_dtor(pObj);
}
  

/*-------- policyman_device_imei_print --------*/
void
policyman_device_imei_print(
  byte  *pImei
  )
{
  POLICYMAN_MSG_HIGH_0("Device IMEI:");
  POLICYMAN_MSG_HIGH_5("  %02x %02x %02x %02x %02x", pImei[0], pImei[1], pImei[2], pImei[3], pImei[4]);
  POLICYMAN_MSG_HIGH_4("  %02x %02x %02x %02x", pImei[5], pImei[6], pImei[7], pImei[8]);
}


/*-------- policyman_condition_imei_evaluate --------*/
static boolean
policyman_condition_imei_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_imei_condition_t  *pCond        = (policyman_imei_condition_t*)pCondition;
  policyman_device_info_t     *pDeviceInfo  = ((policy_execute_ctx_t *) pCtx)->pState->pDeviceInfo;
  char const                  *pIMEI        = pDeviceInfo->pIMEI;
  boolean                     result        = FALSE;
  
  policyman_device_imei_print(pDeviceInfo->rawIMEI);
  POLICYMAN_UT_MSG_1("IMEI: %s", pIMEI);
  
  result =      pIMEI != NULL
            &&  mre_regex_match(pCond->pRegEx, pIMEI);

  POLICYMAN_MSG_HIGH_1("condition <imei> with above device IMEI returns %d", result);

  return result;
}


/*-------- policyman_condition_imei_new --------*/
mre_status_t
policyman_condition_imei_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  policyman_status_t          status = POLICYMAN_STATUS_ERR_INVALID_CONDITION;
  policyman_imei_condition_t  *pCondition = NULL;
  char const                  *pStr;
  mre_regex_t                 *pRegEx;

  pStr = policyman_xml_get_attribute(pElem, "regex");
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG_0("<imei>: must specify regex attribute");
    goto Done;
  }

  pRegEx = mre_regex_create(pStr);
  if (pRegEx == NULL)
  {
    POLICYMAN_UT_MSG_1("<imei>: invalid regex: %s", pStr);
    goto Done;
  }

  /*  Allocate the condition
   */
  pCondition = (policyman_imei_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_imei_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_imei_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_imei_evaluate;
  pCondition->pRegEx = pRegEx;
  status = POLICYMAN_STATUS_SUCCESS;

Done:
  *ppCondition = (policyman_condition_t *) pCondition;

  return status;
}

