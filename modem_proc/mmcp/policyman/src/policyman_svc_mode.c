/**
  @file policyman_svc_mode.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_svc_mode.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_svc_mode.h"
#include "policyman_device_config.h"
#include "policyman_efs.h"
#include "policyman_set.h"
#include "policyman_subs.h"
#include "policyman_util.h"
#include "policyman_xml.h"

#include <stringl/stringl.h>

#define SVC_MODE_TAG  "svc_mode"
#define POLICYMAN_SVC_MODE_NV_PATH "/policyman/svc_mode"


/*=============================================================================
  SVC mode APIs
=============================================================================*/

 typedef struct
 {
   POLICYMAN_ACTION_BASE;
 
   sys_modem_as_id_e_type subsId;
   policyman_svc_mode_t svc_mode;
 } svc_mode_action_t;


/*-------- policyman_svc_mode_item_new --------*/
policyman_svc_mode_item_t *
policyman_svc_mode_item_new(
  policyman_svc_mode_t    svcMode,
  sys_modem_as_id_e_type  subsId
  )
{
  policyman_svc_mode_item_t *pItem;
  
  pItem = (policyman_svc_mode_item_t *) policyman_cfgitem_new_item(
                                            POLICYMAN_ITEM_SVC_MODE,
                                            sizeof(policyman_svc_mode_item_t),
                                            policyman_simple_dtor
                                            );
  pItem->mode = svcMode;
  pItem->asubs_id = subsId;

  return pItem;
}

/*-------- policyman_retrieve_svc_mode --------*/
static boolean policyman_retrieve_svc_mode(
  sys_modem_as_id_e_type subsId,
  policyman_svc_mode_t  *pSvcMode
)
{
  policyman_efs_status_t    status;
  policyman_svc_mode_t     *pMode        = NULL;
  size_t                    modeSize;
  char                      svcPath[128] = {0};
  policyman_efs_status_t    efsStatus    = POLICYMAN_EFS_STATUS_SUCCESS;
  boolean                   retval       = FALSE;

  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(svcPath, sizeof(svcPath), POLICYMAN_SVC_MODE_NV_PATH, subsId);

  /* Return if we detect error
  */

  if (efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_0("Internal error: EFS path buffer too small");
    return FALSE;
  }

  /*  Try and read from the NV item for UE mode.
   */
  status = policyman_efs_get_file(svcPath, (void **) &pMode, &modeSize);
  
  /*  If we were able to read the NV and if the value is in the proper range,
   *  set the mode to that value.  Otherwise just return NORMAL mode.
   */
  if (    status == POLICYMAN_EFS_STATUS_SUCCESS
      &&  modeSize == sizeof(policyman_svc_mode_t)
      &&  pMode != NULL
      &&  *pMode >= POLICYMAN_SVC_MODE_LIMITED
      &&  *pMode <= POLICYMAN_SVC_MODE_ALL
     )
  {
    *pSvcMode = *pMode;
    retval = TRUE;
    POLICYMAN_MSG_HIGH_1("read svc_mode %d from EFS", *pSvcMode);
  }

  POLICYMAN_MEM_FREE_IF(pMode);
  return retval;
}

/*-------- policyman_svc_mode_get_default_from_efs --------*/
policyman_item_t *policyman_svc_mode_get_default_from_efs(
  sys_modem_as_id_e_type subs_id
)
{
  policyman_svc_mode_item_t *pSvcMode = NULL;
  policyman_svc_mode_t       svcMode;

  if (!policyman_retrieve_svc_mode(subs_id, &svcMode))
  {
    POLICYMAN_MSG_ERROR_1("subs %d: can't populate SVC mode Item from EFS", subs_id);
    goto Done;
  }

  pSvcMode = policyman_svc_mode_item_new(svcMode, subs_id);

Done:

  return (policyman_item_t *)pSvcMode;
}

/*-------- policyman_svc_mode_get_default --------*/
policyman_item_t *
policyman_svc_mode_get_default(
  sys_modem_as_id_e_type subsId
  )
{
  policyman_svc_mode_item_t  *pItem;

  pItem = policyman_svc_mode_item_new(POLICYMAN_SVC_MODE_FULL, subsId);

  POLICYMAN_MSG_HIGH_2("Default SVC mode for subs %d: %d", subsId, pItem->mode);

  return (policyman_item_t *) pItem;
}

/*-------- policyman_svc_mode_compare --------*/
boolean
policyman_svc_mode_compare(
  policyman_item_t  *pItem1,
  policyman_item_t  *pItem2
  )
{
  policyman_svc_mode_item_t  *pMode1 = (policyman_svc_mode_item_t *) pItem1;
  policyman_svc_mode_item_t  *pMode2 = (policyman_svc_mode_item_t *) pItem2;

  return pMode1 != NULL && pMode2 != NULL && pMode1->mode == pMode2->mode;
}


/*-------- policyman_get_svc_mode --------*/
policyman_status_t
policyman_get_svc_mode(
  policyman_item_t const  *pItem,
  policyman_svc_mode_t    *pSvcMode
  )
{
  policyman_svc_mode_item_t  *pMode = (policyman_svc_mode_item_t *) pItem;
  
  if (pMode == NULL || pSvcMode == NULL)
  { 
    POLICYMAN_MSG_ERROR_0("get_svc_mode returned err POLICYMAN_STATUS_ERR_INVALID_ARGS");
    return POLICYMAN_STATUS_ERR_INVALID_ARGS; 
  }
  if (policyman_cfgitem_id(pItem) != POLICYMAN_ITEM_SVC_MODE)
  { 
    POLICYMAN_MSG_ERROR_0("get_svc_mode returned err POLICYMAN_STATUS_ERR_INVALID_ITEM_ID");
    return POLICYMAN_STATUS_ERR_INVALID_ITEM_ID; 
  }

  *pSvcMode = pMode->mode;

  POLICYMAN_MSG_HIGH_1("get_svc_mode returned svc_mode %d", pMode->mode);

  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_ue_mode_display --------*/
void policyman_svc_mode_display
(
  policyman_item_t  *pItem
)
{
  policyman_svc_mode_item_t  *pMode = (policyman_svc_mode_item_t *) pItem;

  POLICYMAN_MSG_HIGH_2("SVC mode for subs %d: %d", pMode->asubs_id, pMode->mode);
}

/*-------- policyman_svc_mode_update_to_efs --------*/
void policyman_svc_mode_update_to_efs(
  policyman_item_t const *pItem
)
{
  policyman_svc_mode_item_t *pSvcMode = (policyman_svc_mode_item_t *) pItem;
  policyman_efs_status_t     efsStatus   = POLICYMAN_EFS_STATUS_SUCCESS;
  boolean                    writeStatus; 
  char                       svcPath[128] = {0};
  size_t                     nSim = 0;

  /* Don't update UE mode into EFS for the SUBS which is not configured
   */
  (void)policyman_get_current_num_sim(&nSim);

  if ((size_t)pItem->asubs_id >= nSim)
  {
    POLICYMAN_MSG_MED_1("subs %d not configured: EFS not updated for SVC mode", pItem->asubs_id);
    return;
  }
 
  /* Get Full Path 
   */
  efsStatus = policyman_efs_get_subs_path(svcPath, sizeof(svcPath), POLICYMAN_SVC_MODE_NV_PATH, pItem->asubs_id);

  /* Return if we detect error
  */
  if (efsStatus != POLICYMAN_EFS_STATUS_SUCCESS)
  {
    POLICYMAN_MSG_ERROR_0("Internal error: EFS path buffer too small");
    return;
  }

  writeStatus = policyman_cfgitem_persist_item(svcPath, &pSvcMode->mode, sizeof(pSvcMode->mode));
  POLICYMAN_MSG_HIGH_3("subs %d: svc_mode %d write to efs status %d", pItem->asubs_id, pSvcMode->mode, writeStatus);
}

/*-------- policyman_str_to_svc_mode --------*/
static boolean
policyman_str_to_svc_mode(
  const char            *pStr,
  policyman_svc_mode_t  *pSvcMode
  )
{
  typedef struct
  {
    const char            *pStr;
    policyman_svc_mode_t  svcMode;
  } mask_map;

  static mask_map map[] =
  {
    {"FULL",      POLICYMAN_SVC_MODE_FULL},
    {"CAMP_ONLY", POLICYMAN_SVC_MODE_CAMP_ONLY},
    {"LIMITED",   POLICYMAN_SVC_MODE_LIMITED},
    {"ALL",       POLICYMAN_SVC_MODE_ALL},
  };

  size_t  i;
  char    token[32];
  boolean succeeded = FALSE;


  *pSvcMode = POLICYMAN_SVC_MODE_FULL;

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      *pSvcMode = map[i].svcMode;
      succeeded = TRUE;
      break;
    }
  }

  return succeeded;
}


/*-------- policyman_svc_mode_read_info --------*/
policyman_status_t policyman_svc_mode_read_info(
  policyman_xml_element_t const *pElem,
  policyman_svc_mode_t         *pSvcmode
  )
{
  policyman_status_t status = POLICYMAN_STATUS_ERR_INVALID_ACTION;
  char const         *pStr;
  
  *pSvcmode = POLICYMAN_SVC_MODE_FULL;
     
  if (!policyman_xml_tag_is(pElem, SVC_MODE_TAG))     { goto Done; }
  pStr = policyman_xml_get_text(pElem);
  policyman_str_to_svc_mode(pStr, pSvcmode);
  status = POLICYMAN_STATUS_SUCCESS;
  
  Done:
    return status;  
}

/*-------- policyman_svc_mode_execute --------*/
static boolean
policyman_svc_mode_execute(
  policyman_action_t const  *pAction,
  void                      *pCtx
  )
{
  svc_mode_action_t         *psvcAction = (svc_mode_action_t *) pAction;
  policyman_svc_mode_item_t *pSm        = policyman_svc_mode_item_new(psvcAction->svc_mode, psvcAction->subsId);

  policyman_set_replace(((policy_execute_ctx_t *) pCtx)->pItemSet, &pSm);
  ref_cnt_obj_release(pSm);
  
  POLICYMAN_MSG_HIGH_2("action <svc_mode> set mode to %d for subs %d", pSm->mode, psvcAction->subsId);

  return TRUE;
}

/*-------- policyman_svc_mode_action_new --------*/
mre_status_t
policyman_svc_mode_action_new
(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
)
{
  mre_status_t            status = MRE_STATUS_ERR_INVALID_ACTION;
  svc_mode_action_t       *pAction = NULL;
  policyman_svc_mode_t    mode;
  sys_modem_as_id_e_type  subs;

  status = policyman_svc_mode_read_info(pElem, &mode);
  if (MRE_FAILED(status))
  {
    POLICYMAN_UT_MSG("<svc_mode>: invalid service mode", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pAction = (svc_mode_action_t *)policyman_mem_alloc(sizeof(svc_mode_action_t));
  ref_cnt_obj_init(pAction, policyman_action_dtor);
  pAction->execute = (pfn_execute_t) policyman_svc_mode_execute;
  pAction->svc_mode = mode;
  pAction->subsId = subs;

  *ppAction = (policyman_action_t *) pAction;

Done:
  return status;
}


typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  policyman_svc_mode_t  svc_mode;
} policyman_svc_mode_condition_t;


/*-------- policyman_condition_svc_mode_evaluate --------*/
static boolean
policyman_condition_svc_mode_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_status_t            status;
  policyman_item_id_t           id = POLICYMAN_ITEM_SVC_MODE;
  policyman_item_t              *pItem;
  size_t                        numItems;
  policyman_svc_mode_t          current_svc_mode;
  boolean                       result = FALSE;
  policyman_svc_mode_condition_t *pCond = (policyman_svc_mode_condition_t *) pCondition;

  numItems = policyman_cfgitem_get_items_per_subs( &id, 
                                                   1, 
                                                   (policyman_item_t const **)&pItem,
                                                   pCond->subsId
                                                 );

  if (numItems != 1)                { goto Done; }

  status = policyman_get_svc_mode((policyman_item_t const *)pItem, &current_svc_mode);
  if (POLICYMAN_FAILED(status))     { goto Done; }

  result = (current_svc_mode == pCond->svc_mode);

Done:
  POLICYMAN_MSG_HIGH_1("condition <svc_mode_is> returns %d", result);
  REF_CNT_OBJ_RELEASE_IF(pItem);
  return result;
}


/*-------- policyman_condition_svc_mode_new --------*/
mre_status_t
policyman_condition_svc_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                    status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_svc_mode_condition_t  *pCondition;
  char const                      *pStr;
  policyman_svc_mode_t            svcMode;
  sys_modem_as_id_e_type          subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<svc_mode_is>: service mode required", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if(MRE_FAILED(status))
  {
    goto Done;
  }

  if (!policyman_str_to_svc_mode(pStr, &svcMode))
  {
    POLICYMAN_UT_MSG("<svc_mode_is>: invalid service mode: %s", 0, 0, 0);
    goto Done;
  }

  pCondition = (policyman_svc_mode_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_svc_mode_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_svc_mode_evaluate;
  pCondition->svc_mode = svcMode;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;

  status = MRE_STATUS_SUCCESS;
  
Done:
  return status;
}

