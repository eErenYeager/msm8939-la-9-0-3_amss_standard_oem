/**
  @file mre_rules.c

  @brief  Utility APIs to help in interpreting components of rules.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/mre_rules.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre.h"
#include "mre_dbg.h"
#include "mre_engine.h"
#include "mre_lang.h"
#include "mre_rules.h"
#include "mre_set.h"
#include "mre_util.h"
#include "mre_xml.h"

#include <stringl/stringl.h>



/*-----------------------------------------------------------------------------
  Definition of a rule
-----------------------------------------------------------------------------*/

/*  A rule is just a set of conditions and a set of actions.
 */
typedef struct
{
  _REF_CNT_OBJ;

  mre_policy_t  *pPolicy;
  mre_set_t     *pCondSet;
  mre_set_t     *pActionSet;
  uint32        preconds;
  uint32        id;
} mre_rule_t;




/*=============================================================================
  Conditions
=============================================================================*/


/*  Mappings between condition tags and the functions to create a new condition
 *  from the XML document.
 */
static mre_condition_map_element_t const  condition_map[] =
{
  /*---------------------------------------------------------------------------
    Language primitives
  ---------------------------------------------------------------------------*/
  {"true",                mre_condition_true_new,           MRE_PRECOND_NONE},
  {"not",                 mre_condition_not_new,            MRE_PRECOND_NONE},
  {"any_of",              mre_condition_or_new,             MRE_PRECOND_NONE},
  {"all_of",              mre_condition_and_new,            MRE_PRECOND_NONE},
  {"boolean_test",        mre_boolean_test_new,             MRE_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Terminator
  ---------------------------------------------------------------------------*/
  {NULL,                  NULL,                             MRE_PRECOND_NONE}
};


/*-------- mre_condition_dtor --------*/
void
mre_condition_dtor(
  void  *pObj
  )
{
  mre_condition_t *pCond = (mre_condition_t *) pObj;

  mre_mem_free(pCond);
}


/*-------- mre_find_condition --------*/
static mre_condition_map_element_t const *
mre_find_condition(
  char                        const *pCondName,
  mre_condition_map_element_t const *pMap
  )
{
  while (NULL != pMap->pTag)
  {
    if (strcmp(pCondName, pMap->pTag) == 0)
    {
      return pMap;
    }

    ++pMap;
  }

  return NULL;
}


/*-------- mre_find_condition_entry --------*/
static mre_condition_map_element_t const *
mre_find_condition_entry(
  char const    *pCondName,
  mre_policy_t  *pPolicy
  )
{
  mre_condition_map_element_t const *pEntry;

  /*  Search the MRE condition map first
   */
  pEntry = mre_find_condition(pCondName, condition_map);

  /*  If not found in the MRE condition map, search the client's map.
   */
  if (NULL == pEntry && NULL != MRE_POLICY_CONDTBL(pPolicy))
  {
    pEntry = mre_find_condition(pCondName, MRE_POLICY_CONDTBL(pPolicy));
  }

  if (pEntry == NULL)
  {
    MRE_UT_MSG_1("Unknown condition tag: %s", pCondName);
  }

  return pEntry;
}


/*-------- mre_rules_add_condition --------*/
boolean
mre_rule_add_condition(
  mre_policy_t            *pPolicy,
  mre_set_t               *pCondSet,
  mre_xml_element_t const *pElem
  )
{
  char const                        *pCondName;
  mre_condition_map_element_t const *pEntry;
  mre_condition_t                   *pCondition = NULL;

  pCondName = mre_xml_get_tag(pElem);
  pEntry = mre_find_condition_entry(pCondName, pPolicy);

  /*  If we found an entry, call its create function to create a condition.
   */
  if (NULL != pEntry)
  {
    mre_status_t      status;
    
    status = pEntry->pfnNew(pElem, pPolicy, &pCondition);
    if (MRE_FAILED(status))
    {
      MRE_UT_MSG_2("Error %08x while creating condition %s", status, pCondName);
    }
  }

  /*  If condition creation was successful, fill out the condition.
   */
  if (NULL != pCondition)
  {
    pCondition->pPolicy = pPolicy;
    mre_set_add(pCondSet, &pCondition);
    ref_cnt_obj_release(pCondition);

    MRE_POLICY_PRECONDITIONS(pPolicy) |= pEntry->precondition;
  }

  return pCondition != NULL;
}



/*-------- mre_rule_get_rule_conditions --------*/
static boolean
mre_rule_get_rule_conditions(
  mre_policy_t                      *pPolicy,
  mre_rule_t                        *pRule,
  mre_xml_element_t           const *pElem
  )
{
  boolean succeeded = TRUE;
  size_t  nConds;
  size_t  i;

  nConds = mre_xml_num_children(pElem);

  pRule->pCondSet = mre_set_refcnt_new(nConds, nConds, NULL);

  for (i = 0 ; succeeded && i < nConds ; ++i)
  {
    mre_xml_element_t const *pChild = mre_xml_get_child(pElem, i);

    if (pChild == NULL)
    {
      MRE_MSG_ERROR_2(
          "Internal error: unable to get child %d of node when number of nodes is %d",
          i, nConds);
      succeeded = FALSE;
      break;
    }

    MRE_XML_MSG("processing %s", mre_xml_get_tag(pChild), 0, 0);
    
    succeeded = mre_rule_add_condition(pPolicy, pRule->pCondSet, pChild);
  }

  return succeeded;
}


/*-------- condition_is_true --------*/
static boolean
condition_is_true(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  mre_condition_t const *pCond = *((mre_condition_t const **) pElem);
  void                  *pCtx = pData1;

  /*  If the condition is true, we continue execution; otherwise we stop
   *  evaluating conditions in the set.
   */
  return pCond->isTrue(pCond, pCtx);
}


/*-------- mre_condset_evaluate --------*/
boolean
mre_condset_evaluate(
  mre_set_t const   *pCondSet,
  void              *pCtx
  )
{
  return mre_set_iterate((mre_set_t *) pCondSet, condition_is_true, pCtx, NULL);
}



/*=============================================================================
  Actions
=============================================================================*/

/*  Mappings between action tags and the functions to create a new action
 *  from the XML document.
 */
static mre_action_map_element_t const action_map[] =
{
  /*---------------------------------------------------------------------------
    Language actions
  ---------------------------------------------------------------------------*/
  {"boolean_define",          mre_boolean_define_new},
  {"boolean_set",             mre_boolean_set_new},
  {"continue",                mre_continue_new},

  /*---------------------------------------------------------------------------
    Terminator
  ---------------------------------------------------------------------------*/
  {NULL,                      NULL}
};


/*=============================================================================
  General internal functions to handle rules
=============================================================================*/


/*-------- mre_action_dtor --------*/
void
mre_action_dtor(
  void  *pObj
  )
{
  mre_action_t  *pAction = (mre_action_t *) pObj;

  mre_mem_free(pAction);
}


/*-------- mre_find_action --------*/
static mre_action_map_element_t const *
mre_find_action(
  char const                      *pActName,
  mre_action_map_element_t const  *pMap
  )
{
  while (NULL != pMap->pTag)
  {
    if (strcmp(pActName, pMap->pTag) == 0)
    {
      return pMap;
    }

    ++pMap;
  }

  return NULL;
}


/*-------- mre_find_action_entry --------*/
static mre_action_map_element_t const *
mre_find_action_entry(
  char const    *pActName,
  mre_policy_t  *pPolicy
  )
{
  mre_action_map_element_t const *pEntry;

  /*  Search the MRE action map first
   */
  pEntry = mre_find_action(pActName, action_map);

  /*  If not found in the MRE action map, search the client's map.
   */
  if (NULL == pEntry && NULL != MRE_POLICY_ACTTBL(pPolicy))
  {
    pEntry = mre_find_action(pActName, MRE_POLICY_ACTTBL(pPolicy));
  }

  if (NULL == pEntry)
  {
    MRE_UT_MSG_1("Unknown action tag: %s", pActName);
  }

  return pEntry;
}


/*-------- mre_rule_add_action --------*/
static boolean
mre_rule_add_action(
  mre_policy_t            *pPolicy,
  mre_set_t               *pActionSet,
  mre_xml_element_t const *pElem
  )
{
  char const                      *pActName;
  mre_action_map_element_t const  *pEntry;
  mre_action_t                    *pAction = NULL;

  pActName = mre_xml_get_tag(pElem);
  pEntry = mre_find_action_entry(pActName, pPolicy);

  /*  If we found an entry, call its create function to create an action.
   */
  if (NULL != pEntry)
  {
    mre_status_t      status;
    
    status = pEntry->pfnNew(pElem, pPolicy, &pAction);
    if (MRE_FAILED(status))
    {
      MRE_UT_MSG_2("Error %08x while creating condition %s", status, pActName);
    }
  }

  /*  If condition creation was successful, fill out the action.
   */
  if (NULL != pAction)
  {
    pAction->pPolicy = pPolicy;
    mre_set_add(pActionSet, &pAction);
    ref_cnt_obj_release(pAction);
  }

  return pAction != NULL;
}


/*-------- mre_rules_get_rule_actions --------*/
static boolean
mre_rule_get_rule_actions(
  mre_policy_t                    *pPolicy,
  mre_rule_t                      *pRule,
  mre_xml_element_t         const *pElem
  )
{
  boolean succeeded = TRUE;
  size_t  i;
  size_t  nActions;

  nActions = mre_xml_num_children(pElem);
  pRule->pActionSet = mre_set_refcnt_new(
                        (nActions == 0)? 1 : nActions,  /* zero actions is OK, but we need a set */
                        MRE_SET_NO_MAX_SIZE, NULL);

  for (i = 0 ; succeeded && i < nActions ; ++i)
  {
    mre_xml_element_t const *pChild = mre_xml_get_child(pElem, i);

    if (pChild == NULL)
    {
      MRE_MSG_ERROR_2(
          "Internal error: unable to get child %d of node when number of nodes is %d",
          i, nActions);
      succeeded = FALSE;
      break;
    }

    MRE_XML_MSG("processing %s", mre_xml_get_tag(pChild), 0, 0);

    succeeded = mre_rule_add_action(pPolicy, pRule->pActionSet, pChild);
  }

  return succeeded;
}


/*-------- execute_action --------*/
static boolean
execute_action(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  mre_action_t const  *pAction = *((mre_action_t const **) pElem);
  void                *pCtx = pData1;

  pAction->execute(pAction, pCtx);

  /*  We always continue executing actions...
   */
  return TRUE;
}


/*-------- mre_actionset_execute --------*/
void
mre_actionset_execute(
  mre_set_t const *pActionSet,
  void            *pCtx
  )
{
  mre_set_iterate((mre_set_t *) pActionSet, execute_action, pCtx, NULL);
}


/*-------- mre_rules_continue_evaluation --------*/
void
mre_rules_continue_evaluation(
  mre_policy_t  *pPolicy
  )
{
  MRE_POLICY_INFO(pPolicy)->contEval = TRUE;
}


/*-------- mre_rule_preconditions_met --------*/
static boolean
mre_rule_preconditions_met(
  mre_rule_t  const *pRule,
  uint32            precondsMet
  )
{
  boolean       met;
  mre_policy_t  *pPolicy = pRule->pPolicy;
  uint32        rulePreconds = pRule->preconds;

  switch (rulePreconds)
  {
    case MRE_PRECOND_NONE:
      met = TRUE;
      MRE_MSG_HIGH_1("Rule #%d: evaluated; no preconditions",
                        MRE_POLICY_RULENUM(pPolicy)
                    );
      break;

    case MRE_PRECOND_POLICY:
      met = MRE_POLICY_PRECONDSAREMET(pPolicy);
      if (!met)
      {
        MRE_MSG_HIGH_1("Rule #%d: skipped; policy preconditions not met",
                        MRE_POLICY_RULENUM(pPolicy)
                      );
      }
      break;

    default:
      met = ((rulePreconds & precondsMet) == rulePreconds);
      if (!met)
      {
        MRE_MSG_HIGH_3("Rule #%d: skipped due to preconditions; rule %d, met %d",
                        MRE_POLICY_RULENUM(pPolicy), rulePreconds, precondsMet
                      );
      }
      break;
  }      

  return met;
}


/*-------- process_rule --------*/
static boolean
process_rule(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  mre_rule_t const  *pRule      = *((mre_rule_t const **) pElem);
  void              *pCtx       = pData1;
  uint32            precondsMet = (uint32) pData2;
  mre_policy_t      *pPolicy    = pRule->pPolicy;
  boolean           ruleFired;

  ++MRE_POLICY_RULENUM(pPolicy);

  /*  If preconditions for this rule aren't met, keep processing rules.
   */
  if (!mre_rule_preconditions_met(pRule, precondsMet))
  {
    return TRUE;
  }

  MRE_MSG_HIGH_1("Rule #%d: evaluating conditions", MRE_POLICY_RULENUM(pPolicy));

  ruleFired = mre_condset_evaluate(pRule->pCondSet, pCtx);
  if (ruleFired)
  {
    MRE_MSG_HIGH_1("Rule #%d: executing actions", MRE_POLICY_RULENUM(pPolicy));
    MRE_POLICY_CONTEVAL(pPolicy) = FALSE;
    mre_actionset_execute(pRule->pActionSet, pCtx);
    ruleFired = !MRE_POLICY_CONTEVAL(pPolicy);
  }

  /*  We stop processing rules on the first match, so if this rule matched
   *  we want to return FALSE.
   */
  return !ruleFired;
}  


/*=============================================================================
  APIs exposed to mre_policy
=============================================================================*/


/*-------- mre_actionset_add --------*/
boolean
mre_actionset_add(
  mre_policy_t                    *pPolicy,
  mre_set_t                       **ppActionSet,
  mre_xml_element_t         const *pElem
  )
{
  boolean   succeeded = FALSE;
  mre_set_t *pActionSet = *ppActionSet;
  size_t    nActions;
  size_t    i;

  /*  Move to the actions node.
   */
  pElem = mre_xml_get_child(pElem, 0);
  if (pElem == NULL || strcmp("actions", mre_xml_get_tag(pElem)) != 0)
  {
    MRE_MSG_ERROR_0("Initial node must have an actions subnode");
    MRE_UT_MSG_1("  node tag was %s", mre_xml_get_tag(pElem));
    goto Done;
  }

  /*  Get the number of actions in this initset
   */
  nActions = mre_xml_num_children(pElem);

  /*  If there's no actionset, create it based on the number of actions.
   */
  if (*ppActionSet == NULL)
  {
    pActionSet = mre_set_refcnt_new(nActions, MRE_SET_NO_MAX_SIZE, NULL);
    *ppActionSet = pActionSet;
  }

  for (i = 0, succeeded = TRUE ; succeeded && i < nActions ; ++i)
  {
    mre_xml_element_t const *pChild = mre_xml_get_child(pElem, i);

    if (pChild == NULL)
    {
      MRE_MSG_ERROR_2(
          "Internal error: unable to get child %d of node when number of nodes is %d",
          i, nActions);
      succeeded = FALSE;
      break;
    }

    MRE_XML_MSG("processing %s", mre_xml_get_tag(pChild), 0, 0);

    succeeded = mre_rule_add_action(pPolicy, pActionSet, pChild);
  }
  
Done:
  return succeeded;
}



/*-------- mre_ruleset_dtor --------*/
void
mre_rule_dtor(
  void  *pObj
  )
{
  mre_rule_t  *pRule = (mre_rule_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pRule->pCondSet);
  REF_CNT_OBJ_RELEASE_IF(pRule->pActionSet);
  mre_mem_free(pRule);
}


/*-------- mre_lookup_precond --------*/
static boolean
mre_lookup_precond(
  mre_precond_map_element_t const *pPrecondTbl,
  char                      const *pStr,
  uint32                          *pMask
  )
{
  while (NULL != pPrecondTbl->pTag)
  {
    if (strcasecmp(pStr, pPrecondTbl->pTag) == 0)
    {
      *pMask |= pPrecondTbl->mask;
      return TRUE;
    }
    ++pPrecondTbl;
  }

  return FALSE;
}


/*-------- mre_rule_parse_rule_preconds --------*/
static boolean
mre_rule_parse_rule_preconds(
  mre_precond_map_element_t const *pPrecondTbl,
  mre_rule_t                      *pRule,
  char                      const *pStr
  )
{
  char    token[100];
  uint32  mask = MRE_PRECOND_NONE;
  boolean noneSpecified = FALSE;

  while (mre_get_token(&pStr, token, sizeof(token)))
  {
    if (strcasecmp(token, "none") == 0)
    {
      noneSpecified = TRUE;
      if (MRE_PRECOND_NONE != mask)
      {
        MRE_MSG_ERROR_0("'none' precondition combined with other preconditions");
        return FALSE;
      }
    }
    else if (noneSpecified)
    {
      MRE_MSG_ERROR_0("'none' precondition combined with other preconditions");
      return FALSE;
    }
    else if (strcasecmp(token, "policy") == 0)
    {
      mask = MRE_PRECOND_POLICY;
    }
    else if (NULL == pPrecondTbl)
    {
      MRE_MSG_ERROR_0("'precond' attribute specified but no precondition table passed");
      return FALSE;
    }
    else if (!mre_lookup_precond(pPrecondTbl, token, &mask))
    {
      MRE_MSG_ERROR_0("Unsupported precondition in rule");
      return FALSE;
    }
  }

  pRule->preconds = mask;
  return TRUE;
}


/*-------- mre_rule_get_rule_preconditions --------*/
static boolean
mre_rule_get_rule_preconditions(
  mre_policy_t            *pPolicy,
  mre_xml_element_t const *pElem,
  mre_rule_t              *pRule
  )
{
  boolean     succeeded = TRUE;
  boolean     precondFound = FALSE;
  char const  *pStr;

  /*  Default preconditions (in absence of override) is to use overall
   *  policy preconditions.
   */
  pRule->preconds = MRE_PRECOND_POLICY;

  /*  If there us a "precond" attribute, we'll use that.
   */
  pStr = mre_xml_get_attribute(pElem, "precond");
  if (NULL != pStr)
  {
    precondFound = TRUE;
    succeeded = mre_rule_parse_rule_preconds(MRE_POLICY_PRECONDTBL(pPolicy), pRule, pStr);
    if (!succeeded)
    {
      goto Done;
    }
  }

  /*  If the deprecated "always_run" attribute is present *and* if there
   *  are no preconditions set, set preconditions to none.
   */
  pStr = mre_xml_get_attribute(pElem, "always_run");
  if (NULL != pStr)
  {
    if (precondFound)
    {
      MRE_MSG_ERROR_0("Rule has both 'always_run' and 'precond' attributes");
      succeeded = FALSE;
    }
    else if (strcasecmp(pStr, "true") == 0)
    {
      pRule->preconds = MRE_PRECOND_NONE;
    }
  }

Done:
  return succeeded;
}


/*-------- mre_ruleset_add --------*/
boolean
mre_ruleset_add_rule(
  mre_policy_t            *pPolicy,
  mre_xml_element_t const *pElem
  )
{
  boolean                 succeeded = FALSE;
  mre_set_t               *pRuleSet = MRE_POLICY_RULESET(pPolicy);
  mre_rule_t              *pRule = NULL;
  mre_xml_element_t const *pChild;
  char const              *pStr;
  size_t                  i;

  /*  Check that we have at two elements (conditions and actions).
   */
  if (mre_xml_num_children(pElem) != 2)
  {
    MRE_MSG_ERROR_0("A rule node must have <conditions> and <actions> tags");
    goto Done;
  }

  /*  Create a ruleset for the policy if there is not already one in place.
   */
  if (pRuleSet == NULL)
  {
    pRuleSet = mre_set_refcnt_new(5, MRE_SET_NO_MAX_SIZE, NULL);
    MRE_POLICY_RULESET(pPolicy) = pRuleSet;
  }

  /*  Create the rule object for this node.
   */
  pRule = (mre_rule_t *) mre_mem_alloc(sizeof(mre_rule_t));
  ref_cnt_obj_init(pRule, mre_rule_dtor);
  pRule->pPolicy = pPolicy;

  succeeded = mre_rule_get_rule_preconditions(pPolicy, pElem, pRule);
  if (!succeeded)
  {
    goto Done;
  }

  succeeded = TRUE;
  for (i = 0 ; succeeded && i < 2 ; ++i)
  {
    pChild = mre_xml_get_child(pElem, i);
    if (pChild == NULL)
    {
      MRE_MSG_ERROR_1("Internal error - no element #%d", i);
      goto Done;
    }
    
    pStr = mre_xml_get_tag(pChild);
    MRE_XML_MSG("processing %s", pStr, 0, 0);
    if      (strcasecmp(pStr, "conditions") == 0)
    {
      succeeded = mre_rule_get_rule_conditions(pPolicy, pRule, pChild);
    }
    else if (strcasecmp(pStr, "actions") == 0)
    {
      succeeded = mre_rule_get_rule_actions(pPolicy, pRule, pChild);
    }
    else
    {
      MRE_MSG_ERROR_0("Illegal tag in rule - only conditions and actions are allowed");
      MRE_UT_MSG_1("Illegal tag: %s", pStr);
      succeeded = FALSE;
      goto Done;
    }
  }

  mre_set_add(pRuleSet, &pRule);
  ref_cnt_obj_release(pRule);
  
Done:
  if (!succeeded)
  {
    REF_CNT_OBJ_RELEASE_IF(pRule);
  }
  
  return succeeded;
}


/*-------- mre_ruleset_execute --------*/
void
mre_ruleset_execute(
  mre_set_t *pRuleset,
  void      *pCtx,
  uint32    precondsMet
  )
{
  (void) mre_set_iterate(pRuleset, process_rule, pCtx, (void *) precondsMet);
}

