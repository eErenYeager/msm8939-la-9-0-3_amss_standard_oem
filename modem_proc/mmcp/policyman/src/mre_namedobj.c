/**
  @file mre_namedobj.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/mre_namedobj.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre_engine.h"
#include "mre_namedobj.h"
#include "mre_set.h"
#include "mre_util.h"



/*=============================================================================
  Named object APIs
=============================================================================*/


/*-------- mre_named_object_dtor --------*/
void
mre_named_object_dtor(
  void  *pObj
  )
{
  mre_named_object_t  *pNamedObj = (mre_named_object_t *) pObj;

  MRE_MEM_FREE_IF(pNamedObj->pName);
  mre_mem_free(pNamedObj);
}



/*-------- mre_named_object_init --------*/
void
mre_named_object_init(
  mre_named_object_t      *pObj,
  char const              *pName,
  mre_named_object_type_t objType
  )
{
  if (pObj != NULL && pName != NULL)
  {
    pObj->pName = mre_str_dup(pName);
    pObj->objType = objType;
  }
}



/*-------- mre_ensure_obj_set --------*/
static void
mre_ensure_obj_set(
  mre_set_t **ppSet
  )
{
  if (NULL == *ppSet)
  {
    *ppSet = mre_set_refcnt_new(5, MRE_SET_NO_MAX_SIZE, NULL);
  }
}


/*-------- mre_named_object_add --------*/
boolean
mre_named_object_add(
  mre_policy_t        *pPolicy,
  mre_named_object_t  *pObj
  )
{
  boolean succeeded;

  pObj->pPolicy = pPolicy;
  mre_ensure_obj_set(MRE_POLICY_NAMEDOBJS(pPolicy));
  succeeded = mre_set_add(*MRE_POLICY_NAMEDOBJS(pPolicy), &pObj);

  return succeeded;
}


/*-------- mre_named_object_find_in_set --------*/
mre_named_object_t *
mre_named_object_find_in_set(
  char const              *pName,
  mre_named_object_type_t objType,
  mre_set_t               *pSet
  )
{
  mre_named_object_t  *pObj = NULL;

  if (NULL != pSet && NULL != pName)
  {
    pObj = mre_set_find_named_object(pSet, pName, objType);  
  }

  return  pObj;
}


/*-------- mre_named_object_find --------*/
mre_named_object_t *
mre_named_object_find(
  char const              *pName,
  mre_named_object_type_t objType,
  mre_policy_t            *pPolicy
  )
{
  mre_named_object_t  *pObj = NULL;
  
  if (  NULL != pName
     && NULL != MRE_POLICY_NAMEDOBJS(pPolicy)
     )
  {
    pObj = mre_named_object_find_in_set(pName, objType, *MRE_POLICY_NAMEDOBJS(pPolicy));
  }

  return pObj;
}

