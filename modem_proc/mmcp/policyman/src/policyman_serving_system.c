/**
  @file policyman_serving_system.c

  @brief  
*/

/*
    Copyright (c) 2013-2016 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_serving_system.c#11 $
  $DateTime: 2016/04/17 23:47:04 $
  $Author: rkarth $
*/

#include "policyman_cm.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_nas.h"
#include "policyman_plmn.h"
#include "policyman_policy.h"
#include "policyman_rat_capability.h"
#include "policyman_rules.h"
#include "policyman_serving_system.h"
#include "policyman_set.h"
#include "policyman_state.h"
#include "policyman_task.h"
#include "policyman_timer.h"
#include "policyman_util.h"
#include "policyman_xml.h"

#include <stringl/stringl.h>

#define INVALID_RAT_MASK  0xffffffff


/*=============================================================================
  Data Structure definitions.
=============================================================================*/
typedef struct 
{  
  /*  stack variables
   */
  boolean                 is_operational;
  byte                    reject_cause;        /* registration reject cause*/
  sys_sys_id_type_e_type  id_type;             /* PLMN id type */
  boolean                 serving_info_valid;  /* Whether PLMN should be used */
  sys_plmn_id_s_type      plmn;                /* PLMN of the serving system */
  sys_plmn_mcc_list       mcc_list_3gpp2;      /* List of 3GPP2 MCCs  */
  sys_plmn_mcc_list       mcc_list_hdr;       /* List of HDR MCCs  */
  sys_mnc_type            mnc_3gpp2;           /* 3GPP2 MNC */
  sys_mnc_type            mnc_hdr;            /* HDR MNC */
  sys_sys_mode_e_type     sys_mode;            /* System's mode */
  sys_srv_status_e_type   srv_status;          /* Service Status */
  sys_band_class_e_type   active_band;         /* serving RF band */
  sys_srv_domain_e_type   srv_domain ;         /* Serving domain */
} policyman_stack_info_t;


struct policyman_ss_info_t
{
  sys_modem_as_id_e_type  asubs_id;     /* Subscription for this info */
  policyman_stack_info_t  stack[SYS_MODEM_STACK_ID_MAX];
};

/*=============================================================================
  Serving system Event callback definition.
=============================================================================*/
struct policyman_cmss_evt_cb_s
{
  POLICYMAN_CMD_HDR;
  cm_ss_event_e_type   evt;
  cm_mm_ss_info_s_type info;
};

struct policyman_cm_serving_info_cb_s
{
  POLICYMAN_CMD_HDR;
  policyman_cm_serving_info_t info;
};

struct policyman_hlos_mcc_cmd_s
{
  POLICYMAN_CMD_HDR;

  sys_mcc_type  mcc;
  size_t        confidenceLevel;
  size_t        status;
};

/*=============================================================================
  Serving system Event callback definition for Multisim
=============================================================================*/
struct policyman_msim_cmss_evt_cb_s
{
  POLICYMAN_CMD_HDR;
  cm_ss_event_e_type        evt;
  cm_mm_msim_ss_info_s_type info;
};



/*=============================================================================
  APIs for serving system information
=============================================================================*/
void
policyman_ss_set_default_value(
   policyman_ss_info_t         *pSsInfo,
   sys_modem_stack_id_e_type   num_stack
   )
{
  policyman_stack_info_t *pStack;

  if (pSsInfo != NULL)
  {
    pStack = &pSsInfo->stack[num_stack];

    pStack->sys_mode = SYS_SYS_MODE_NONE;
    pStack->srv_status = SYS_SRV_STATUS_NO_SRV;     
    pStack->active_band = SYS_BAND_CLASS_NONE;

    // make PLMN digits undefined upon power up
    pStack->id_type = SYS_SYS_ID_TYPE_UNDEFINED;

    // PLMN is 3 bytes, set to undefined
    pStack->serving_info_valid = FALSE;
     policyman_plmn_clear(&pStack->plmn);
     POLICYMAN_MSG_MED_2("Subs %d stack %d does not have valid data",
                          pSsInfo->asubs_id, num_stack);
  }
}

/*-------- policyman_ss_init --------*/
boolean
policyman_ss_init(
  sys_modem_as_id_e_type  subs,
  policyman_ss_info_t     **ppInfo
  )
{
  size_t              i;
  policyman_ss_info_t *pInfo;
  
  pInfo = (policyman_ss_info_t *) policyman_mem_alloc(sizeof(policyman_ss_info_t));

  for (i = SYS_MODEM_STACK_ID_1 ; i < SYS_MODEM_STACK_ID_MAX ; i++)
  {
    pInfo->asubs_id = subs;
    policyman_ss_set_default_value(pInfo, i);
  }

  *ppInfo = pInfo;
  return TRUE;
}


/*-------- policyman_ss_deinit --------*/
void
policyman_ss_deinit(
  policyman_ss_info_t *pInfo
  )
{
  POLICYMAN_MEM_FREE_IF(pInfo);
}

/*-------- policyman_ss_get_ss_info --------*/
policyman_ss_info_t*
policyman_ss_get_ss_info(
  sys_modem_as_id_e_type subsId
)
{
  if(subsId < 0 || subsId >= POLICYMAN_NUM_SUBS)
  {
    POLICYMAN_ERR_FATAL("subsId out of range", 0, 0, 0);
  }

  return policyman_state_get_state()->pSubsState[subsId]->pSsInfo;
}

/*-------- policyman_ss_get_stack_info --------*/
static policyman_stack_info_t  *
policyman_ss_get_stack_info(
  sys_modem_as_id_e_type subsId,
  sys_modem_stack_id_e_type stackId
)
{
  if(stackId < SYS_MODEM_STACK_ID_1 || stackId > SYS_MODEM_STACK_ID_2)
  {
    POLICYMAN_ERR_FATAL("stack id out of range", 0, 0, 0);
  }

  return  &policyman_ss_get_ss_info(subsId)->stack[stackId];
}

/*-------- policyman_ss_set_stack_opr --------*/
void 
policyman_ss_set_stack_opr(
  sys_modem_as_id_e_type subsId,
  sys_modem_stack_id_e_type stackId,
  boolean isOperational
  )
{
    policyman_stack_info_t* pStackInfo = policyman_ss_get_stack_info(subsId,stackId);
    pStackInfo->is_operational = isOperational;
  }

/*-------- policyman_ss_is_stack_plmn_valid --------*/
boolean
policyman_ss_is_stack_plmn_valid(
  sys_modem_stack_id_e_type stackId,
  policyman_ss_info_t       *pInfo
)
{
  policyman_stack_info_t* pStackInfo = &pInfo->stack[stackId]; 

  return     pStackInfo->is_operational == TRUE
          && pStackInfo->id_type != SYS_SYS_ID_TYPE_UNDEFINED
          && pStackInfo->serving_info_valid == TRUE;
}

/*-------- policyman_ss_status_is_service --------*/
static boolean
policyman_ss_status_is_service(
  sys_srv_status_e_type srv_status
  )
{
  boolean retval = FALSE;

  switch(srv_status)
  {
    case SYS_SRV_STATUS_SRV:
    case SYS_SRV_STATUS_LIMITED:
    case SYS_SRV_STATUS_LIMITED_REGIONAL:
      retval = TRUE;
      break;

    case SYS_SRV_STATUS_NONE:
    case SYS_SRV_STATUS_NO_SRV:
    case SYS_SRV_STATUS_PWR_SAVE:
    default:
      break;
  }

  return retval;
}

/*-------- policyman_ss_find_timer --------*/
static policyman_timer_t *policyman_ss_find_timer(
  char const                  *pName,
  policyman_timer_expirefn_t   pfnExpire,
  sys_modem_as_id_e_type       subsId
  )
{
  policyman_timer_t *pTimer = NULL;

  pTimer = policyman_timer_get_timer(pName);
  policyman_timer_set_subs(pTimer, subsId);
  policyman_timer_set_expirefn(pTimer, pfnExpire);

  return pTimer;
}

/*-------- policyman_update_ss_precond_for_subs --------*/
void policyman_update_ss_precond_for_subs(
  sys_modem_as_id_e_type    subsId,
  policyman_ss_info_t      *pSsinfo
  )
{
  boolean                    plmnValid = FALSE;
  sys_modem_stack_id_e_type  Stack = SYS_MODEM_STACK_ID_1;

  for (Stack = SYS_MODEM_STACK_ID_1; !plmnValid && (Stack < SYS_MODEM_STACK_ID_MAX); Stack++)
  {
    plmnValid = policyman_ss_is_stack_plmn_valid(Stack, pSsinfo);
  }

  if (plmnValid)
  {
    policyman_state_set_precondition_met(POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, subsId);
  }
  else
  {
    POLICYMAN_MSG_ERROR_1("subs %d: PLMN not valid on any stack, reset preconditions", subsId);
    policyman_state_reset_precondition_met(POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, subsId);
  }
}


/*-------- policyman_ss_handle_timer_expired --------*/
static void policyman_ss_handle_timer_expired(
  policyman_timer_t        *pTimer,
  sys_modem_stack_id_e_type stackId
  )
{
  sys_modem_as_id_e_type    subsId  = policyman_timer_get_subs(pTimer);
  policyman_stack_info_t   *pStack = policyman_ss_get_stack_info(subsId, stackId);


  /* Timer has expired so mark stack as invalid for decision making
   */
  if (pStack != NULL)
  {
    policyman_ss_info_t  *pSsinfo;
    
    pStack->serving_info_valid = FALSE;
    
    pSsinfo = policyman_ss_get_ss_info(subsId);

    policyman_update_ss_precond_for_subs(subsId, pSsinfo);
    policyman_state_handle_update(SYS_MODEM_AS_ID_1);
  }
}

/*-------- policyman_ss_handle_1x_oos_expired --------*/
static void policyman_ss_handle_1x_oos_expired(
  policyman_timer_t *pTimer
  )
{
  policyman_ss_handle_timer_expired(pTimer, SYS_MODEM_STACK_ID_1);
}

/*-------- policyman_ss_handle_hybrid_oos_expired --------*/
static void policyman_ss_handle_hybrid_oos_expired(
  policyman_timer_t *pTimer
  )
{
  policyman_ss_handle_timer_expired(pTimer, SYS_MODEM_STACK_ID_2);
}

/*-------- policyman_ss_get_timer_for_stack --------*/
static policyman_timer_t *policyman_ss_get_timer_for_stack(
  policyman_stack_info_t    *pStack,
  sys_modem_stack_id_e_type stackId,
  sys_modem_as_id_e_type    subsId
  )
{  
  policyman_timer_t       *pTimer = NULL;
	
    if (stackId == SYS_MODEM_STACK_ID_1
     && subsId == SYS_MODEM_AS_ID_1
     )
    {
    pTimer = policyman_ss_find_timer("1x_oos", policyman_ss_handle_1x_oos_expired, subsId);
    }
  /* For secondary stack look up for hybr_oos timer
     */
  else if (stackId == SYS_MODEM_STACK_ID_2
          && subsId == SYS_MODEM_AS_ID_1
          )
  {
    pTimer = policyman_ss_find_timer("hybr_oos", policyman_ss_handle_hybrid_oos_expired, subsId);
  }

  return pTimer;  
}

/*-------- policyman_ss_check_stack_oos --------*/
static void policyman_ss_check_stack_oos(
  policyman_stack_info_t    *pStack,
  sys_modem_stack_id_e_type stackId,
  sys_modem_as_id_e_type    subsId
  )
{  
  policyman_timer_t *pTimer = NULL;
  sys_ue_mode_e_type       ueMode;

   ueMode = SYS_UE_MODE_CSFB;
  (void)policyman_get_current_ue_mode(SYS_MODEM_AS_ID_1, &ueMode);

  pTimer = policyman_ss_get_timer_for_stack(pStack, stackId, subsId);

  if (NULL != pTimer)
  {
    if (policyman_ss_status_is_service(pStack->srv_status))
    {
      policyman_timer_stop(pTimer);
    }
    /* start timer only if no service and 
     * not in CSFB(single stack) mode
     */
    else if(ueMode != SYS_UE_MODE_CSFB)
    {
      policyman_timer_start(pTimer);
    }
  }
}


/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_MCC()

  DESCRIPTION
    GET MCC for stack from PM STATE
 ===========================================================================*/
uint32 policyman_ss_get_mcc
(
  policyman_ss_info_t         *pInfo,
  sys_modem_stack_id_e_type   num_stack
  )
{
  if(pInfo) {return policyman_plmn_get_mcc(&pInfo->stack[num_stack].plmn);}
  else      {return 0;}
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_PLMN()

  DESCRIPTION
    GET PLMN for stack from PM STATE
 ===========================================================================*/
void policyman_ss_get_plmn
(
  policyman_ss_info_t         *pInfo,
  sys_modem_stack_id_e_type   num_stack,
  sys_plmn_id_s_type          *pPlmn
  )
{
  policyman_stack_info_t *pStack;
  
  if (pInfo != NULL && pPlmn != NULL)
  {
    pStack = &pInfo->stack[num_stack];
    
    if(pStack->is_operational && pStack->serving_info_valid)
    {
      *pPlmn = pStack->plmn;
    }
    else
  {
      POLICYMAN_MSG_ERROR_3("Subs %d stack %d not operational or PLMN invalid %d, returning PLMN 0",
                             pInfo->asubs_id, 
                             num_stack, 
                             pStack->serving_info_valid );

      sys_plmn_set_mcc_mnc(FALSE, 0, 0, pPlmn);
    }
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_PLMN_ID_TYPE()

  DESCRIPTION
    GET PLMN ID type for MAIN stack
 ===========================================================================*/
sys_sys_id_type_e_type 
policyman_ss_get_plmn_id_type(
  policyman_ss_info_t         *pSsInfo,
  sys_modem_stack_id_e_type   num_stack
)
{
  if(pSsInfo){ return pSsInfo->stack[num_stack].id_type; }
  else return SYS_SYS_ID_TYPE_UNDEFINED;
}

/*-------- policyman_ss_some_stack_has_service --------*/
static boolean
policyman_ss_some_stack_has_service(
  policyman_ss_info_t *pCurrentInfo
  )
{
  size_t  i;

  for (i = SYS_MODEM_STACK_ID_1 ; i < SYS_MODEM_STACK_ID_MAX ; i++)
  {
    if (  pCurrentInfo->stack[i].is_operational
       && policyman_ss_status_is_service(pCurrentInfo->stack[i].srv_status)
       )
    {
      return TRUE;
    }
  }

  return FALSE;
}
  
/*-------- policyman_is_new_srv_status_changed --------*/
static boolean 
policyman_is_new_srv_status_changed(
  sys_srv_status_e_type current_srv_status, 
  sys_srv_status_e_type new_srv_status
)
{  
  /* The valid changes are:
    * In Service -> OOS, PWR_SAVE, LIMITED REGIONAL
    * OOS -> PWR-SAVE, SRV, LIMITED SRV, LIMITED REGIONAL
    * LIMITED <-> FULL SERVICE
    */

  return new_srv_status != current_srv_status;

}

/*-------- policyman_is_reg_reject_info_changed --------*/
static boolean policyman_is_reg_reject_info_changed(
   byte current_rej_cause, 
   byte new_rej_cause
)
{
  return !(current_rej_cause == new_rej_cause);   
}

/*-------- policyman_3gpp2_mccs_are_equal --------*/
static boolean policyman_3gpp2_mccs_are_equal(
  sys_plmn_mcc_list *pCurrentList,
  sys_plmn_mcc_list *pNewList
)
{
  return memcmp(pCurrentList, pNewList, sizeof(sys_plmn_mcc_list)) == 0;
}

/*-------- policyman_ss_get_mcc_from_subs --------*/
STATIC boolean policyman_ss_get_mcc_from_subs(
  sys_modem_as_id_e_type   subsId,
  sys_mcc_type            *pCurentMcc
)
{
  size_t                   numStack;
  policyman_stack_info_t  *pStack;
  boolean                  haveService = FALSE; 

  /* loop through all the stacks */
  for (numStack = SYS_MODEM_STACK_ID_1; !haveService && numStack <= SYS_MODEM_STACK_ID_2; numStack++)
  {
    pStack      = policyman_ss_get_stack_info(subsId, numStack);
    haveService = policyman_ss_status_is_service(pStack->srv_status);

    if (haveService)
    {
      /* For 3GPP2 system use MCC from MCC list, otherwise use 3GPP PLMN */
      if (pStack->sys_mode == SYS_SYS_MODE_CDMA)
      {
        if (pStack->mcc_list_3gpp2.count == 0)
        {
          POLICYMAN_MSG_ERROR_0("not updating current MCC as MCC list count is 0");
          haveService = FALSE;
        }
        else
        {
          *pCurentMcc = pStack->mcc_list_3gpp2.mcc_list[0];
        }
      }
      else if (pStack->sys_mode == SYS_SYS_MODE_HDR)
      {
        if (pStack->mcc_list_hdr.count == 0)
        {
          POLICYMAN_MSG_ERROR_0("not updating current MCC as MCC list count is 0");
          haveService = FALSE;
        }
        else
        {
          *pCurentMcc = pStack->mcc_list_hdr.mcc_list[0];
        }
      }
      else
      {
        *pCurentMcc = policyman_plmn_get_mcc(&pStack->plmn);
      }      
    }
  }

  return haveService;  
}

/*-------- policyman_ss_update_current_mcc --------*/
STATIC void policyman_ss_update_current_mcc(
  sys_modem_as_id_e_type              subsId
)
{
  size_t            subs;
  sys_mcc_type            current_mcc;
  boolean           haveMcc     = FALSE;
  
  if (subsId != policyman_get_current_multimode_subs())
  {
    POLICYMAN_MSG_ERROR_1("not updating current MCC in state: subs %d is not multimode SUBS", subsId);
    return;
  }

  /* Fetch MCC from stacks if PLMN is available on SUBS
       Otherwise fetch it from HLOS MCC if available
   */
  if (policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subsId))
  {
    for (subs = 0; !haveMcc && subs < POLICYMAN_NUM_SUBS; subs++)
    {
      haveMcc = policyman_ss_get_mcc_from_subs(subs, &current_mcc);
    }
  }
  else if (policyman_state_check_precondition_met(POLICYMAN_PRECOND_LOCATION, subsId))
  {
    current_mcc = policyman_state_get_hlos_mcc();
    haveMcc     = TRUE;
  }

  if (haveMcc)
  {
    policyman_state_update_current_mcc(current_mcc);
  }
}

/*-------- policyman_ss_update_state --------*/
void
policyman_ss_update_state(
    policyman_ss_info_t     *pCurrentInfo,
    policyman_ss_info_t     *pNewInfo,
    sys_modem_as_id_e_type  asubs_id
    )
{
  size_t  i;
  boolean stateChanged[SYS_MODEM_STACK_ID_MAX];
  boolean haveService;

  sys_modem_as_id_e_type  currentSubs = policyman_get_current_multimode_subs();
  cm_mode_pref_e_type     mode_pref = policyman_ph_get_mode_pref(currentSubs);

  if (pCurrentInfo == NULL || pNewInfo == NULL)   { return; }

  POLICYMAN_MSG_HIGH_3("Updating SS state for subs %d; multimode subs is %d with mode pref: %d", asubs_id, currentSubs, mode_pref);

  for (i = SYS_MODEM_STACK_ID_1 ; i < SYS_MODEM_STACK_ID_MAX ; i++)
  {

    stateChanged[i] =
          pCurrentInfo->stack[i].is_operational != pNewInfo->stack[i].is_operational
       || !policyman_plmns_are_equal(&pCurrentInfo->stack[i].plmn, &pNewInfo->stack[i].plmn)
       || !policyman_3gpp2_mccs_are_equal(&pCurrentInfo->stack[i].mcc_list_3gpp2, &pNewInfo->stack[i].mcc_list_3gpp2)
       || policyman_is_new_srv_status_changed(pCurrentInfo->stack[i].srv_status, pNewInfo->stack[i].srv_status)
       || policyman_is_reg_reject_info_changed(pCurrentInfo->stack[i].reject_cause, pNewInfo->stack[i].reject_cause);
  }

  /*  Update the state
   */
  *pCurrentInfo = *pNewInfo;
  haveService = policyman_ss_some_stack_has_service(pCurrentInfo);

  /*  If there was no state change, just return.
   */
  if (  !stateChanged[SYS_MODEM_STACK_ID_1]
     && !stateChanged[SYS_MODEM_STACK_ID_2]
     )
  {
    POLICYMAN_MSG_HIGH_1("Subs %d: information not changed on any stack", asubs_id);
    goto Done;
  }

  /* Update current_mcc as we have got our state updated now with the latest info 
     */
  policyman_ss_update_current_mcc(asubs_id);

  /*  If the service status is valid for any stack, handle updates
   */
  if (haveService)
  {
    POLICYMAN_MSG_HIGH_3("Information changed on subs %d stacks: main %d, gw_hybr %d, handle update",
                        asubs_id,
                        stateChanged[SYS_MODEM_STACK_ID_1], 
                        stateChanged[SYS_MODEM_STACK_ID_2]
                        );

    policyman_state_handle_update(asubs_id);
  }

Done: 
  /*  Pass overall service status to policies for multimode subs.
        Other subs service status should not be considered.
   */
  if (asubs_id == currentSubs)
  {
    policyman_policy_notify_service(haveService);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_SET_PLMN()

  DESCRIPTION
    SET PLMN for stack into PM STATE
 ===========================================================================*/
boolean 
policyman_ss_set_plmn(
  policyman_ss_info_t          *pInfo,
  sys_modem_stack_id_e_type    num_stack,
  sys_plmn_id_s_type           *pPlmn
)
{
  boolean changed = FALSE;
  policyman_stack_info_t *pStack;
  
  if (
        pInfo != NULL
     && pPlmn != NULL
     )
  {
    pStack = &pInfo->stack[num_stack];

    if( !policyman_plmns_are_equal(&pStack->plmn, pPlmn) )
    {
      pStack->plmn = *pPlmn;
      pStack->serving_info_valid = TRUE;
      pStack->is_operational = TRUE;
    changed = TRUE;
  }
  }

  return changed;
}


/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_SRV_STATUS()
 
  DESCRIPTION
    GET srv_status for stack
 ===========================================================================*/
sys_srv_status_e_type
policyman_ss_get_srv_status(  
  policyman_ss_info_t        *pInfo, 
  sys_modem_stack_id_e_type  num_stack
)
{
  return (pInfo != NULL)? pInfo->stack[num_stack].srv_status : SYS_SRV_STATUS_NO_SRV;
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_SET_SRV_STATUS()
 
  DESCRIPTION
    SET srv_status for stack into PolicyMan state

 RETURN VALUE
   TRUE is policyman state is updated correctly, FALSE otherwise
 ===========================================================================*/
boolean 
policyman_ss_set_srv_status(  
  policyman_ss_info_t        *pInfo, 
  sys_modem_stack_id_e_type  num_stack,
  sys_srv_status_e_type      srv_status
)
{
  boolean changed = FALSE;

  if ( pInfo != NULL && 
       (pInfo->stack[num_stack].srv_status != srv_status)
     )
  {
    pInfo->stack[num_stack].srv_status = srv_status;
    changed =  pInfo->stack[num_stack].is_operational = TRUE;
  }

  return changed;
}

/*-------- policyman_ss_check_subs_in_service --------*/
STATIC boolean policyman_ss_check_subs_in_service(
  sys_modem_as_id_e_type subsId
)
{
  policyman_stack_info_t  *pStack1 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_1);
  policyman_stack_info_t  *pStack2 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_2);

  /* don't check stack operational as it won't have service if inactive */
  return    policyman_ss_status_is_service(pStack1->srv_status)
         || policyman_ss_status_is_service(pStack2->srv_status);
}


/*===========================================================================
  FUNCTION POLICYMAN_SS_SET_SUBS_SRV_INFO_VALID()
 
  DESCRIPTION
    SET service_info_valid for each stack in subs into PolicyMan state

 RETURN VALUE
   TRUE is policyman state is updated correctly, FALSE otherwise
 ===========================================================================*/
boolean 
policyman_ss_set_subs_srv_info_valid(  
  policyman_ss_info_t        *pInfo, 
  boolean                    srv_info_valid
)
{
  boolean changed = FALSE;
  policyman_stack_info_t *pStack;
  size_t              i;

  if ( pInfo != NULL )
  {
    for( i = SYS_MODEM_STACK_ID_1; i < SYS_MODEM_STACK_ID_MAX; i++ )
    {
      pStack = &pInfo->stack[i];

      if( pStack->serving_info_valid != srv_info_valid )
      {
        pStack->serving_info_valid = srv_info_valid;
        changed = TRUE;
      }
    }
  }

  return changed;
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_ACTIVE_BAND()
 
  DESCRIPTION
    GET active band for stack
 ===========================================================================*/
void 
policyman_ss_get_active_band(  
  policyman_ss_info_t        *pInfo, 
  sys_modem_stack_id_e_type  num_stack,
  sys_band_class_e_type     *active_band
)
{
  if (pInfo != NULL && active_band != NULL && pInfo->stack[num_stack].is_operational)
  {
    *active_band = pInfo->stack[num_stack].active_band;
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_SS_GET_REJECT_CAUSE()
 
  DESCRIPTION
    GET registration reject cause for MAIN stack
 ===========================================================================*/
void 
policyman_ss_get_reject_cause(  
  policyman_ss_info_t        *pInfo, 
  sys_modem_stack_id_e_type  num_stack,
  byte                      *reject_cause
)
{
  if (pInfo != NULL && reject_cause != NULL && pInfo->stack[num_stack].is_operational)
  {
    *reject_cause = pInfo->stack[num_stack].reject_cause;
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_SERVING_DOMAIN_CAMPED()
 
  DESCRIPTION
    Check CM serving domain is CAMPED ONLY 
===========================================================================*/
boolean policyman_serving_domain_camped(sys_srv_domain_e_type srv_domain)
{
  return srv_domain == SYS_SRV_DOMAIN_CAMPED;
}

/*===========================================================================
  FUNCTION POLICYMAN_SRV_STATUS_CHECK_NO_SRV()
 
  DESCRIPTION
    Check if Service Status is No service
===========================================================================*/
boolean policyman_srv_status_check_no_srv
(
  sys_srv_status_e_type srv_status
)
{
  return srv_status == SYS_SRV_STATUS_NO_SRV
         || srv_status == SYS_SRV_STATUS_NONE;
}

/*===========================================================================
  FUNCTION policyman_copy_mcc_list()

  DESCRIPTION
    Copy MCCs to a list
  ===========================================================================*/
STATIC void policyman_copy_mcc_list(
  uint32 *pDst,
  uint32 *pSrc,
  uint8   count
 )
{
  while (count-- > 0)
  {
    POLICYMAN_MSG_HIGH_1("  %d", *pSrc);
    *pDst++ = *pSrc++;
  }
}

/*===========================================================================
  FUNCTION policyman_read_hdr_mcc_list()

  DESCRIPTION
    Get the list of MCCs for HDR
===========================================================================*/
STATIC void policyman_read_hdr_mcc_list(
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt,
  policyman_stack_info_t              *pStack,
  cm_mm_msim_ss_stack_info_s_type     *pcmStack,
  size_t                               stackNum
)
{
  uint8  cmHdrCount = cm_ss_evt->info.prl_hdr_mcc.count;
  
  if (cmHdrCount < 1)
  {
    POLICYMAN_MSG_ERROR_0("CM sent empty HDR MCC list, don't wipe the previous info");
    return;
  }
  else if (cmHdrCount > SD_PLMN_MCC_LIST_MAX)
  {
    POLICYMAN_ERR_FATAL("CM sent incorrect HDR MCC list with count %d", cmHdrCount, 0, 0);
  }

  policyman_plmn_clear(&pStack->plmn);
	
  pStack->id_type  = pcmStack->sys_id.id_type;
  pStack->mcc_list_hdr.count = cmHdrCount;
  pStack->mnc_hdr            = (sys_mnc_type)pcmStack->sys_id.id.is95.imsi_11_12;

  POLICYMAN_MSG_HIGH_2("HDR MCC list for subs %d stack %d", 
                         cm_ss_evt->info.asubs_id , 
                        stackNum);
              
  policyman_copy_mcc_list(pStack->mcc_list_hdr.mcc_list, cm_ss_evt->info.prl_hdr_mcc.mcc_list, pStack->mcc_list_hdr.count);
}

/*===========================================================================
  FUNCTION policyman_read_1x_mcc_list()

  DESCRIPTION
    Get list of MCCs for 1x
===========================================================================*/
STATIC void policyman_read_1x_mcc_list(
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt,
  policyman_stack_info_t              *pStack,
  cm_mm_msim_ss_stack_info_s_type     *pcmStack,
  size_t                               stackNum
)
{
  uint8 cm3GPP2Count = cm_ss_evt->info.prl_3gpp2_mcc.count;

  if (cm3GPP2Count < 1)
  {
    POLICYMAN_MSG_ERROR_0("CM sent empty 3GPP2 MCC list, don't wipe the previous info");
    return;
  }
  else if (cm3GPP2Count > SD_PLMN_MCC_LIST_MAX)
  {
    POLICYMAN_ERR_FATAL("CM sent incorrect 3GPP2 MCC list with count %d", cm3GPP2Count, 0, 0);
  }

  policyman_plmn_clear(&pStack->plmn);

  pStack->id_type              = pcmStack->sys_id.id_type;
  pStack->mcc_list_3gpp2.count = cm3GPP2Count;
  pStack->mnc_3gpp2 = (sys_mnc_type)pcmStack->sys_id.id.is95.imsi_11_12;

  POLICYMAN_MSG_HIGH_2("3GPP2 MCC list for subs %d stack %d", 
                          cm_ss_evt->info.asubs_id,
                        stackNum);
  
  policyman_copy_mcc_list(pStack->mcc_list_3gpp2.mcc_list, 
                          cm_ss_evt->info.prl_3gpp2_mcc.mcc_list, 
                          pStack->mcc_list_3gpp2.count);
}

/*===========================================================================
  FUNCTION POLICYMAN_GET_3GPP2_PLMN_FROM_LIST()

  DESCRIPTION
    Get list of MCCs for 3GPP2
===========================================================================*/
STATIC void policyman_get_3gpp2_plmn_from_list(
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt,
  policyman_stack_info_t              *pStack,
  cm_mm_msim_ss_stack_info_s_type     *pcmStack,
  size_t                               stackNum
)
{
  if (pcmStack->sys_mode == SYS_SYS_MODE_HDR)
  {
    policyman_read_hdr_mcc_list(cm_ss_evt, pStack, pcmStack, stackNum); 
  }
  else 
  {
    policyman_read_1x_mcc_list(cm_ss_evt, pStack, pcmStack, stackNum); 
  }
}

/*===========================================================================
FUNCTION POLICYMAN_EXTRACT_SYS_ID()

DESCRIPTION
  Get PLMN for 3gpp and 3gpp2
===========================================================================*/
void policyman_extract_sys_id 
(
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt,
  policyman_stack_info_t              *pStack,
  cm_mm_msim_ss_stack_info_s_type     *pcmStack,
  size_t                               stackNum
)
{
  POLICYMAN_MSG_HIGH_2("Received system id type %d and service domain %d", pcmStack->sys_id.id_type, pcmStack->srv_domain);
  
  switch (pcmStack->sys_id.id_type)
  {
    case SYS_SYS_ID_TYPE_UMTS:
      
      /* Ignore Main stack 3GPP PLMN if srv_domain is CAMPED_ONLY
               */
      if(!policyman_serving_domain_camped(pcmStack->srv_domain))
      {
        pStack->id_type  = pcmStack->sys_id.id_type;
        pStack->plmn = pcmStack->sys_id.id.plmn;
        POLICYMAN_MSG_HIGH_2("subs %d stack %d PLMN:", cm_ss_evt->info.asubs_id, stackNum);
        policyman_plmn_print(&pStack->plmn);
      }
      break;
  
    case SYS_SYS_ID_TYPE_IS856:
    case SYS_SYS_ID_TYPE_IS95:
      policyman_get_3gpp2_plmn_from_list(cm_ss_evt, pStack, pcmStack, stackNum);
      break;
     
    default:
      POLICYMAN_MSG_ERROR_2("subs %d stack %d PLMN ignored", cm_ss_evt->info.asubs_id, stackNum);
      break;
  }
}

/*-------- policyman_ss_stack_has_service --------*/
STATIC boolean policyman_ss_stack_has_service(
  policyman_ss_info_t       *pSsInfo,
  sys_modem_stack_id_e_type  stackId
)
{
  sys_srv_status_e_type srvStatus;
 
  srvStatus = policyman_ss_get_srv_status(pSsInfo, stackId);
  return policyman_ss_status_is_service(srvStatus);
}

/*-------- policyman_ss_check_subs_pwr_save --------*/
boolean policyman_ss_check_subs_pwr_save(
  sys_modem_as_id_e_type subsId
)
{
  boolean                  retval   = FALSE;
  policyman_stack_info_t  *pStack1 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_1);
  policyman_stack_info_t  *pStack2 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_2);

  if (  pStack1->is_operational 
     && pStack2->is_operational
     )
  {
    retval =   pStack1->srv_status == SYS_SRV_STATUS_PWR_SAVE 
            && pStack2->srv_status == SYS_SRV_STATUS_PWR_SAVE;
  }
  else if (pStack1->is_operational)
  {
    retval =   pStack1->srv_status == SYS_SRV_STATUS_PWR_SAVE;
  }
  else
  {
    retval =   pStack2->srv_status == SYS_SRV_STATUS_PWR_SAVE;
  }

  return retval;
}



/*-------- policyman_subs_has_location --------*/
STATIC boolean policyman_subs_has_location(
  sys_modem_as_id_e_type  subsId
  )
{
  return    policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subsId)
        &&  !policyman_ss_check_subs_pwr_save(subsId) ;
}


/*-------- policyman_determine_subs_for_location_info --------*/
STATIC boolean policyman_determine_subs_for_location_info(
  sys_modem_as_id_e_type  *pLocationSubs
)
{
  size_t  index;
  size_t  nSim;
  boolean result = FALSE;

  (void)policyman_get_current_num_sim(&nSim);

  /* loop through all SUBS and determine which one has location info */
  for (index = SYS_MODEM_AS_ID_1; index < nSim; index++)
  {
    result = policyman_subs_has_location(index);
    if (result)
    {
      *pLocationSubs = (sys_modem_as_id_e_type)index;
      break;
    }
  }

  return result;
}

/*-------- policyman_determine_location_info --------*/
STATIC boolean policyman_determine_location_info(
  policyman_state_t const *pState
)
{
  policyman_ss_info_t    *pSsInfo;
  boolean                 haveService       = FALSE;
  boolean                 haveLocation  = FALSE;
  sys_modem_as_id_e_type  locationSubs;
  sys_plmn_id_s_type      devicePlmn;
  size_t                  stack;

  haveLocation = policyman_determine_subs_for_location_info(&locationSubs);
  memset(&devicePlmn, 0xFF, sizeof(devicePlmn));

  if (haveLocation)
  {
    pSsInfo = policyman_ss_get_ss_info(locationSubs);

   /* if modem location is known then loop through all stacks and update the PLMN */
    for (stack = SYS_MODEM_STACK_ID_1; !haveService && stack <= SYS_MODEM_STACK_ID_2; stack++)
    {
      haveService = policyman_ss_stack_has_service(pSsInfo, stack);
      if (haveService)
      {
        policyman_state_get_serving_plmn(pState, stack, locationSubs, &devicePlmn);
      }
    }
  }

  POLICYMAN_MSG_HIGH_2("determine location info: found new location %d, location subs %d, returned PLMN:", haveLocation, locationSubs);
  policyman_plmn_print(&devicePlmn);
  if (haveService)
  {
    policyman_state_update_device_plmn(&devicePlmn);
  }

  return haveService;
}

/*===========================================================================
  FUNCTION POLICYMAN_MSIM_GET_STACK_INFO()
 
  DESCRIPTION
    Get Main stack info
  ===========================================================================*/
void policyman_msim_get_stack_info(
  policyman_ss_info_t                 *cm_ss_info, 
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt
)
{
  size_t                       stackId;
  sys_modem_as_id_e_type       subsId;
  cm_mm_msim_ss_info_s_type   *pcmSSInfo; 

  subsId = cm_ss_info->asubs_id;
  pcmSSInfo = &cm_ss_evt->info;  

  for(stackId=0; stackId < pcmSSInfo->number_of_stacks; stackId++)
  {
    policyman_stack_info_t          *pStack;
    cm_mm_msim_ss_stack_info_s_type *pcmStack;

    /* get current state info locally
      */
    pStack = &cm_ss_info->stack[stackId];
    pcmStack = &pcmSSInfo->stack_info[stackId];

    pStack->is_operational = pcmStack->is_operational;

    /* mark stack as valid
      */
    if(!pcmStack->is_operational)
    {
      POLICYMAN_MSG_MED_2("CM sent subs %d stack %d is not operational",
                           subsId,
                           stackId );

      policyman_ss_set_default_value(cm_ss_info, stackId);
      continue ;
    }

    /* get stack info based on corresponding mask
        */
    if (   (pcmStack->changed_fields & CM_SS_EVT_SYS_ID_MASK)
         ||(pcmSSInfo->changed_fields_subs & CM_SS_EVT_PRL_3GPP2_MCC_MASK)
         ||(pcmSSInfo->changed_fields_subs & CM_SS_EVT_PRL_HDR_MCC_MASK)         
       )
    {
      policyman_extract_sys_id(cm_ss_evt, pStack, pcmStack, stackId);
    }

    if(pcmStack->changed_fields & CM_SS_EVT_SRV_STATUS_MASK)
    {
      pStack->srv_status = pcmStack->srv_status;
      if( policyman_ss_status_is_service( pcmStack->srv_status ) )
      {
        pStack->serving_info_valid = TRUE;
      }
      POLICYMAN_MSG_HIGH_3("subs %d stack %d - srv_status %d",
                            subsId,
                            stackId,
                            pcmStack->srv_status );
    }    
    
    /* Ignore sys mode change if associated service status is NO_SRV/PWR_SAVE
       */
    if( (pcmStack->changed_fields & CM_SS_EVT_SYS_MODE_MASK)
      && policyman_ss_status_is_service(pcmStack->srv_status)
      )
    {
      pStack->sys_mode = pcmStack->sys_mode;
      POLICYMAN_MSG_HIGH_3("subs %d stack %d - sys_mode %d",
                            subsId,
                            stackId,
                            pStack->sys_mode );
    }
      
    if(pcmStack->changed_fields & CM_SS_EVT_SRV_DOMAIN_MASK)
    {
      pStack->srv_domain = pcmStack->srv_domain;
      POLICYMAN_MSG_HIGH_3("subs %d stack %d - srv_domain %d",
                            subsId,
                            stackId,
                            pcmStack->srv_domain );
    }

    if(pcmStack->changed_fields & CM_SS_EVT_ACTIVE_BAND_MASK)
    {
      pStack->active_band = pcmStack->active_band;
      POLICYMAN_MSG_HIGH_3("subs %d stack %d - active_band %d",
                            subsId,
                            stackId,
                            pcmStack->active_band );
    }    

    /* Decide if 1x_oos/hybr_oos timer needs to be started or stopped
      */
    policyman_ss_check_stack_oos(pStack, stackId, subsId);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_MSIM_GET_SS_INFO()
 
  DESCRIPTION
    GET changed CM SS Info from event into local policyman_ss_info_t structure
 
  PARAMETERS
    ss_info : Policyman local structure to retain changed SS info
    ss_evt : Incoming CM SS EVENT for Multisim
  ===========================================================================*/
void policyman_msim_get_ss_info
(
  policyman_ss_info_t                 *cm_ss_info, 
  struct policyman_msim_cmss_evt_cb_s *cm_ss_evt
)
{
  if(cm_ss_info != NULL && cm_ss_evt != NULL)
  {
    sys_modem_as_id_e_type subsId = cm_ss_evt->info.asubs_id;

    /* restore the old PM state info
      */
    *cm_ss_info = *policyman_ss_get_ss_info(subsId);    

    /* get stack info
      */
    policyman_msim_get_stack_info(cm_ss_info, cm_ss_evt);

    /* set preconditions if either of stacks is operational and camped and serving_info is valid
      */
    policyman_update_ss_precond_for_subs(subsId, cm_ss_info);

  }
  else
  {
    POLICYMAN_MSG_ERROR_2("msim_get_ss_info NULL pointer, data not copied - cm ss info %x, cm ss evt %x", cm_ss_info, cm_ss_evt);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_MSIM_SS_UPDATE_REJECT_INFO()
 
  DESCRIPTION
    Update for reject info into PM serving system state for Multisim
===========================================================================*/
void policyman_msim_ss_update_reject_info(struct policyman_msim_cmss_evt_cb_s *ss_evt)
{
  size_t i;
  policyman_ss_info_t *cm_ss_info = policyman_ss_get_ss_info(ss_evt->info.asubs_id);

  if(cm_ss_info)
  {    
    // update reject cause in state
    for(i =SYS_MODEM_STACK_ID_1; i<SYS_MODEM_STACK_ID_MAX; i++)
    {
      if( ss_evt->info.stack_info[i].is_operational)
      {
        cm_ss_info->stack[i].reject_cause = ss_evt->info.stack_info[i].reg_reject_info.reject_cause;
        POLICYMAN_MSG_HIGH_4("subs %d stack %d - reject_cause %d, reject_domain %d", 
                            cm_ss_info->asubs_id, i,
                            ss_evt->info.stack_info[i].reg_reject_info.reject_cause, 
                            ss_evt->info.stack_info[i].reg_reject_info.reject_srv_domain
                          );
      }
    }
  }
}


/*===========================================================================
  FUNCTION POLICYMAN_EXECUTE_MSIM_PROCESS_CM_REJECT_EVT()
 
  DESCRIPTION
    Execute CM SS Set update for reject info for Multisim
 ===========================================================================*/
void policyman_execute_msim_process_cm_reject_evt
(
  policyman_cmd_t *pCmd
)
  {
  struct policyman_msim_cmss_evt_cb_s *ss_evt = (struct policyman_msim_cmss_evt_cb_s*) pCmd;

  if(ss_evt)
  {
    /* only update the state, don't run the rules now
       next SS event will be LIMITED service so run rules there
    */
    policyman_msim_ss_update_reject_info(ss_evt);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_EXECUTE_MSIM_PROCESS_CMSS_EVT()
 
  DESCRIPTION
    Execute CM SS Set update and policy check for Multisim
 ===========================================================================*/
void policyman_execute_msim_process_cmss_evt
(
  policyman_cmd_t *pCmd
)
{
  struct policyman_msim_cmss_evt_cb_s *ss_evt = (struct policyman_msim_cmss_evt_cb_s*) pCmd;
  struct policyman_ss_info_t ss_info;

  if(ss_evt)
  {
    policyman_msim_get_ss_info(&ss_info, ss_evt);
    policyman_state_msim_update_ss_info(&ss_info, ss_evt->info.asubs_id);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_MSIM_CMSS_EVENT_CB()

  DESCRIPTION
    Handle CM SS event callback function for Multisim

  PARAMETERS
    evt     : CM SS EVENT type
    p_info : actual payload of CM SS EVENT
===========================================================================*/
void policyman_msim_cmss_event_cb
( 
  cm_ss_event_e_type               evt, 
  const cm_mm_msim_ss_info_s_type *p_info
)
{
  struct policyman_msim_cmss_evt_cb_s *pCmd    = NULL;
  execute_fn_t policyman_execute_msim_cm_event = NULL;

  switch(evt)
  {
    case CM_SS_EVENT_REG_REJECT:
      POLICYMAN_MSG_HIGH_1("subs %d received msim CM_SS_EVENT_REG_REJECT", p_info->asubs_id);
      policyman_execute_msim_cm_event = policyman_execute_msim_process_cm_reject_evt;
      break;

    case CM_SS_EVENT_SRV_CHANGED:
      POLICYMAN_MSG_HIGH_1("subs %d received msim CM_SS_EVENT_SRV_CHANGED", p_info->asubs_id);
      policyman_execute_msim_cm_event = policyman_execute_msim_process_cmss_evt;
      break;

    default:
      break;     
  }

  if(policyman_execute_msim_cm_event)
  {
    pCmd = (struct policyman_msim_cmss_evt_cb_s *) policyman_cmd_new( sizeof(struct policyman_msim_cmss_evt_cb_s), 
                                                                  policyman_execute_msim_cm_event, 
                                                                  NULL,
                                                                  NULL                               // No policy associated with CM SS Event
                                                               );

    pCmd->evt    = evt;
    pCmd->info = *p_info;

    policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
    ref_cnt_obj_release(pCmd);
  }
}

/*=============================================================================
  Conditions based on serving system.
=============================================================================*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  size_t          subsId;
  char const      *pSetName;
  policyman_set_t *pPlmnSet;
} policyman_serving_plmn_condition_t;

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  sys_sys_mode_mask_e_type rat_mask;
} policyman_serving_rat_condition_t;

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  policyman_set_t *pRfBandSet;   
} policyman_serving_rf_band_condition_t;

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  policyman_set_t *pRejectCauseSet;   
} policyman_reg_reject_condition_t;


/*-------- sglte_special_stack_test --------*/
static boolean
sglte_special_stack_test(
  policyman_stack_info_t const  *pMain,
  policyman_stack_info_t const  *pHybrid
  )
{
  boolean result = FALSE;

  // check if one stack is in service, other is OOS - check for MAIN operational
  if (
        (
            policyman_ss_status_is_service(pMain->srv_status)
         && policyman_srv_status_check_no_srv(pHybrid->srv_status)
        )
      ||
(
            policyman_ss_status_is_service(pHybrid->srv_status)
         && policyman_srv_status_check_no_srv(pMain->srv_status)
        )
)
{
    result = TRUE;
    POLICYMAN_MSG_HIGH_0("sglte_special_stack_test: special case - one stack is OOS, return TRUE");
    goto Done;
}

  POLICYMAN_MSG_HIGH_0("sglte_special_stack_test: default return of FALSE");

Done:
  return result;
}

/*-------- policyman_ss_3gpp2_plmn_print --------*/
STATIC void policyman_ss_3gpp2_plmn_print(
  policyman_stack_info_t const *pStack,
  sys_plmn_mcc_list      const *pMccList,
  sys_mnc_type                  mnc
)
{
  size_t                 index;
  sys_plmn_id_s_type     plmn;
  
  for (index = 0; index < pMccList->count; index++)
  {
    policyman_plmn_from_mcc_mnc(&plmn, pMccList->mcc_list[index], mnc);
    policyman_plmn_print(&plmn);
  }
}

/*-------- policyman_ss_print_stack_info --------*/
void
policyman_ss_print_stack_info(
  policyman_state_t const     *pState,
  sys_modem_as_id_e_type       asubs_id
  )
{
  policyman_stack_info_t const  *pStacks;
  policyman_stack_info_t const  *pStack;
  size_t                        nStacks;
  size_t                        stack;
  size_t                        index;
  sys_plmn_id_s_type            plmn;

  pStacks = pState->pSubsState[asubs_id]->pSsInfo->stack;
  nStacks = ARR_SIZE(pState->pSubsState[asubs_id]->pSsInfo->stack);

  /*  Print out stack information
   */
  for (stack = 0 ; stack < nStacks ; ++stack)
  {
    pStack = pStacks + stack;
    POLICYMAN_MSG_HIGH_2("Subs %d stack %d:", asubs_id, stack);
    POLICYMAN_MSG_HIGH_3("   srv_status = %d sys_mode = %d band = %d",
                            pStack->srv_status,
                            pStack->sys_mode,
                            pStack->active_band
                         );

    /* For 3GPP, PLMN print is fine
           For 3GPP2, it should come from MCC list
      */

    if (pStack->is_operational && pStack->sys_mode == SYS_SYS_MODE_CDMA)
    {
      policyman_ss_3gpp2_plmn_print(pStack, &pStack->mcc_list_3gpp2, pStack->mnc_3gpp2);
    }
    else if (pStack->sys_mode == SYS_SYS_MODE_HDR)
      {
      policyman_ss_3gpp2_plmn_print(pStack, &pStack->mcc_list_hdr, pStack->mnc_hdr);
      }
    else if (pStack->is_operational)
    {
      policyman_plmn_print((sys_plmn_id_s_type *)&pStack->plmn);
    }
  }
}

/*-------- policyman_check_serving_plmn_in_stack --------*/
static boolean policyman_check_serving_plmn_in_stack
(
  policyman_stack_info_t const *pStack,
  policyman_set_t              *pPlmnSet
)
{
  boolean                 retval = FALSE;
  size_t                  index;
  sys_plmn_id_s_type      plmn;
  sys_plmn_mcc_list const *pPlmnList;

  if (   pStack->is_operational
      && pStack->srv_status != SYS_SRV_STATUS_PWR_SAVE
      && pStack->serving_info_valid
     )
  {
    switch (pStack->sys_mode)
    {
      case SYS_SYS_MODE_CDMA:
        /* get PLMN for each MCC in list and check in set
            */
        pPlmnList = &pStack->mcc_list_3gpp2;
        POLICYMAN_MSG_HIGH_1("checking %d CDMA PLMNs in set", pPlmnList->count);

        for (index = 0; index < pPlmnList->count; index++)
        {
          policyman_plmn_from_mcc_mnc(&plmn, pPlmnList->mcc_list[index] , pStack->mnc_3gpp2);
          policyman_plmn_print(&plmn);
          retval = policyman_set_contains(pPlmnSet, (void *)&plmn);
          if (retval)
          {
            break;
          }
        }
        break;

      default:
        retval = policyman_set_contains(pPlmnSet, (void *)&pStack->plmn);
        break;
    }
  }

  return retval;
}

/*-------- policyman_condition_serving_plmn_evaluate --------*/
static boolean
policyman_condition_serving_plmn_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
)
{
  policyman_serving_plmn_condition_t  *pCond = NULL;
  policyman_state_t                   *pState = ((policy_execute_ctx_t *) pCtx)->pState;
  policyman_stack_info_t const        *pStacks;
  policyman_stack_info_t const        *pStack;
  size_t                              nStacks;
  size_t                              stack;
  boolean                             result = FALSE;

  pCond = (policyman_serving_plmn_condition_t *) pCondition;

  POLICYMAN_MSG_HIGH_0("evaluating <serving_plmn_in>");

  /*  Return FALSE if the SS precondition is not met.
   */
  if (!policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, pCond->subsId))
  {
    POLICYMAN_MSG_ERROR_0("SS Precondition is not met, returning FALSE");
    goto Done;
  }

  /*  Find the PLMN set to use; if we don't have one we'll return FALSE.
   */
  if (pCond->pPlmnSet == NULL && pCond->pSetName != NULL)
  {    
    pCond->pPlmnSet = (policyman_set_t *) mre_named_object_find( pCond->pSetName,
                                                                 POLICYMAN_NAMED_PLMN_SET,
                                                                 pCondition->pPolicy );

    POLICYMAN_MEM_FREE_IF(pCond->pSetName);

    if (NULL != pCond->pPlmnSet)
    {
      ref_cnt_obj_add_ref(pCond->pPlmnSet);
    }

  }
  if (pCond->pPlmnSet == NULL)             
  {
    POLICYMAN_MSG_ERROR_0("PLMN set not found");
    goto Done;
  }

  /*  Print stack information.  This call probably wants to go into
   *  policyman_state_handle_update(), so we print it at the beginning
   *  of each evaluation of the rules.  Perhaps include more state information
   *  in this as well - call it policyman_display_state() or something.
   */
  policyman_ss_print_stack_info(pState, pCond->subsId);

  /*  Get stack information in easy-to-use variables
   */
  pStacks = pState->pSubsState[pCond->subsId]->pSsInfo->stack;
  nStacks = ARR_SIZE(pState->pSubsState[pCond->subsId]->pSsInfo->stack);

  /*  Check to see if some stack's PLMN is in the list.
   */
  for (stack = 0 ; stack < nStacks ; ++stack)
  {
    pStack = pStacks + stack;

    if (policyman_check_serving_plmn_in_stack(pStack,  pCond->pPlmnSet))
    {
      POLICYMAN_MSG_HIGH_1("PLMN of stack %d is in the PLMN set", stack);
      result = TRUE;
         goto Done;
    }
    else
    {
      POLICYMAN_MSG_HIGH_4("PLMN not found in stack %d, is_operational %d, srv_status %d, plmn valid %d", 
                            stack, 
                            pStack->is_operational, 
                            pStack->srv_status,
                            pStack->serving_info_valid );
    }
  }

Done:
  POLICYMAN_MSG_HIGH_1("condition <serving_plmn_in> returns %d", result);
  return result;
}

/*-------- policyman_condition_serving_plmn_dtor --------*/
static void
policyman_condition_serving_plmn_dtor(
  void  *pObj
  )
{
  policyman_serving_plmn_condition_t *pCondition = 
                                    (policyman_serving_plmn_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pPlmnSet);
  POLICYMAN_MEM_FREE_IF(pCondition->pSetName);
  policyman_condition_dtor(pCondition);
}


/*-------- policyman_condition_serving_plmn_new --------*/
mre_status_t
policyman_condition_serving_plmn_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                        status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_serving_plmn_condition_t  *pCondition = NULL;
  const char                          *pStr;
  policyman_set_t                     *pSet = NULL;
  char                                *pSetName = NULL;
  sys_modem_as_id_e_type               subs;

  /*  See if there is a named PLMN list to use for this condition.
   */
  pStr = policyman_xml_get_attribute(pElem, "list");
  if (pStr != NULL)
  {
    pSetName = policyman_str_dup(pStr);
  }
  else
  {
    /*  No name - read an inline list.
     */
    pStr = policyman_xml_get_text(pElem);
    if (pStr != NULL)
    {
      pSet = policyman_plmn_list_read(pStr);
    }  
  }
  
  /*  If there is neither an inline or named list, bail.
   */
  if (pSet == NULL && pSetName == NULL)
  {
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  /*  Create the condition
   */
  pCondition = (policyman_serving_plmn_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_serving_plmn_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_serving_plmn_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_serving_plmn_evaluate;
  pCondition->pPlmnSet = pSet;
  pCondition->pSetName = pSetName;
  pCondition->subsId = subs;

  status = MRE_STATUS_SUCCESS;

Done:
  *ppCondition = (policyman_condition_t *) pCondition;

  return status;
}

/*-------- policyman_check_3gpp_mcc_in_stack --------*/
static boolean policyman_check_3gpp_mcc_in_stack
(
  policyman_stack_info_t const    *pStack,
  policyman_set_t                 *pMccSet
)
{
  uint32                            servingMcc;
  boolean                           result = FALSE;
  
  servingMcc = policyman_plmn_get_mcc((sys_plmn_id_s_type *)&pStack->plmn);
  
  if (    pStack->is_operational
      &&  pStack->srv_status != SYS_SRV_STATUS_PWR_SAVE
      &&  pStack->serving_info_valid
      &&  policyman_set_contains(pMccSet, &servingMcc)
     )
  {
    result = TRUE;
  }

  return result;
}

/*-------- policyman_check_mcc_in_list --------*/
STATIC boolean policyman_does_mcc_list_intersect_mcc_set(
  policyman_stack_info_t const   *pStack,
  policyman_set_t               *pMccSet,
  sys_plmn_mcc_list      const  *pMccList
)
{  
  size_t   index;
  boolean  result = FALSE;
  
  if (    pStack->is_operational
      &&  pStack->srv_status != SYS_SRV_STATUS_PWR_SAVE
      &&  pStack->serving_info_valid
     )
  {
    for (index = 0; !result && index < pMccList->count; index++)
      {
      result = policyman_set_contains(pMccSet, (void *)&pMccList->mcc_list[index]);
    }
  }

  return result;
}


/*-------- policyman_serving_mcc_in_subs --------*/
static boolean
policyman_serving_mcc_in_subs(
  policyman_state_t const *pState,
  policyman_set_t         *pMccSet,     /* list of MCCs to check for */
  sys_modem_as_id_e_type  subs,
  policyman_policy_id_t   id
  )
{
  policyman_stack_info_t const      *pStacks;
  policyman_stack_info_t const      *pStack;
  size_t                            nStacks;
  size_t                            stack;
  boolean                           result = FALSE;

  /*  Return FALSE if the Location precondition is not met.
   */
  if (!policyman_state_check_precondition_met(POLICYMAN_PRECOND_LOCATION, subs))
  {
    POLICYMAN_MSG_ERROR_1("Location Precondition is not met on subs %d, skipping evaluation", subs);
    goto Done;
  }

  /* If Modem MCC is not available then HLOS MCC is available because Location precondition is set
       Try evaluation by looking into HLOS MCC
   */
  if (!policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subs))
  {
    sys_mcc_type hlos_mcc = policyman_state_get_hlos_mcc();

    result = policyman_set_contains(pMccSet, &hlos_mcc);
    POLICYMAN_MSG_HIGH_2("looking into HLOS MCC %d, match found = %d", hlos_mcc, result);
    goto Done;
  }

  /*  Print stack information.  This call probably wants to go into
   *  policyman_state_handle_update(), so we print it at the beginning
   *  of each evaluation of the rules.  Perhaps include more state information
   *  in this as well - call it policyman_display_state() or something.
   */
  policyman_ss_print_stack_info(pState, subs);

  /*  Get stack information in easy-to-use variables
   */
  pStacks = policyman_ss_get_ss_info(subs)->stack;
  nStacks = ARR_SIZE(policyman_ss_get_ss_info(subs)->stack);

  /*  Check to see if some stack's MCC is in the list.
   */
  for (stack = 0 ; stack < nStacks ; ++stack)
  {
    pStack = pStacks + stack;
  
    if (pStack->sys_mode == SYS_SYS_MODE_CDMA)
    {
      result = policyman_does_mcc_list_intersect_mcc_set(pStack, pMccSet, &pStack->mcc_list_3gpp2);
    }
    else if (pStack->sys_mode == SYS_SYS_MODE_HDR)
    {
      result = policyman_does_mcc_list_intersect_mcc_set(pStack, pMccSet, &pStack->mcc_list_hdr);
    }
    else
    {
      result = policyman_check_3gpp_mcc_in_stack(pStack, pMccSet);
    } 

    if (result)
    {
      goto Done;
    }
  }
  
  /*  Special test: If one stack is in service and one is not, we're going to
   *  pretend that the other stack might be on the PLMN if it had service.
   *  This is a hack for SGLTE that needs to be moved into the policy itself
   *  or cleaned up in some other way.
   */
  if (    subs == policyman_get_current_multimode_subs()
      &&  id == POLICYMAN_POLICY_ID_SGLTE
     )
  {
    if ( pStacks[SYS_MODEM_STACK_ID_1].is_operational &&
         pStacks[SYS_MODEM_STACK_ID_2].is_operational
       )
    {
      result = sglte_special_stack_test( pStacks + SYS_MODEM_STACK_ID_1,
                                         pStacks + SYS_MODEM_STACK_ID_2
                                       );
    }
    else
    {
      POLICYMAN_MSG_ERROR_0("one of the stacks is not operational - sglte special test not triggered");
    }
  }

Done:
  return result;
}


/*-------- policyman_serving_mcc_in_nonmmode_subs --------*/
static boolean
policyman_serving_mcc_in_nonmmode_subs(
  policyman_state_t const *pState,
  policyman_set_t         *pMccSet,     /* list of MCCs to check for */
  policyman_policy_id_t   id
  )
{
  sys_modem_as_id_e_type  subs;
  size_t                  nSim = 0;
  sys_modem_as_id_e_type  mmodeSubs = policyman_get_current_multimode_subs();

  
  (void)policyman_get_current_num_sim(&nSim);

  for (subs = SYS_MODEM_AS_ID_1 ; subs < nSim ; ++subs)
  {
    if (
          subs != mmodeSubs
       && policyman_serving_mcc_in_subs(pState, pMccSet, subs, id)
       )
    {
      return TRUE;
    }
  }

  return FALSE;
}

/*-------- policyman_ss_ensure_mcc_set --------*/
STATIC mre_set_t * policyman_ss_ensure_mcc_set(
  policyman_mcc_condition_t *pCond
  )
{
  // If we don't have a set but *do* have a set name, try to find the named set.
  if (pCond->pMccSet == NULL && pCond->pSetName != NULL)
  {    
    pCond->pMccSet = (policyman_set_t *) mre_named_object_find( pCond->pSetName,
                                                                POLICYMAN_NAMED_MCC_SET,
                                                                pCond->pPolicy );

    POLICYMAN_MEM_FREE_IF(pCond->pSetName);

    if (NULL != pCond->pMccSet)
    {
      ref_cnt_obj_add_ref(pCond->pMccSet);
    }
  }

  return pCond->pMccSet;
}

/*-------- policyman_condition_serving_mcc_evaluate --------*/
static boolean policyman_condition_serving_mcc_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_mcc_condition_t *pCond     = (policyman_mcc_condition_t *) pCondition;
  policyman_state_t         *pState    = ((policy_execute_ctx_t *)pCtx)->pState;
  policyman_set_t           *pMccSet;
  sys_modem_as_id_e_type     mmodeSubs = policyman_get_current_multimode_subs();
  policyman_policy_id_t      id        = policyman_policy_get_id(pCond->pPolicy);
  boolean                    result    = FALSE;

  pMccSet = policyman_ss_ensure_mcc_set(pCond);

  /*  Without a set, we can't work.
   */
  if (pMccSet == NULL)             
  { 
    POLICYMAN_MSG_ERROR_0("named PLMN set not found");
    goto Done;
  }

  switch (pCond->subs)
  {
    case SUBS_MMODE:
      result = policyman_serving_mcc_in_subs(pState, pMccSet, mmodeSubs, id);
      break;
  
    case SUBS_NON_MMODE:
      result = policyman_serving_mcc_in_nonmmode_subs(pState, pMccSet, id);
      break;
  
    case SUBS_ALL:
      result =      policyman_serving_mcc_in_subs(pState, pMccSet, mmodeSubs, id)
                ||  policyman_serving_mcc_in_nonmmode_subs(pState, pMccSet, id)
                ;
      break;

    default:
      POLICYMAN_MSG_ERROR_1("Unsupported sub type in policyman_condition_serving_mcc_evaluate: %d",
                            pCond->subs);
      break;
  }

Done:
  POLICYMAN_MSG_HIGH_1("condition <serving_mcc_in> returns %d", result);
  return result;
}


/*-------- policyman_condition_mcc_dtor --------*/
void policyman_condition_mcc_dtor(
  void  *pObj
)
{
  policyman_mcc_condition_t *pCondition;
  pCondition = (policyman_mcc_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pMccSet);
  POLICYMAN_MEM_FREE_IF(pCondition->pSetName);
  policyman_condition_dtor(pCondition);
}



/*-------- policyman_condition_serving_mcc_new --------*/
mre_status_t
policyman_condition_serving_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  return  policyman_plmn_mcc_new( pElem, 
                                  pPolicy, 
                                  ppCondition, 
                                  (pfn_evaluate_t) policyman_condition_serving_mcc_evaluate
                                );
}

/*-------- policyman_condition_location_mcc_evaluate --------*/
static boolean policyman_condition_location_mcc_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
)
{
  policyman_mcc_condition_t *pCond      = (policyman_mcc_condition_t *) pCondition;
  policyman_state_t         *pState     = ((policy_execute_ctx_t *)pCtx)->pState;
  policyman_set_t           *pMccSet;
  boolean                    haveLocation = FALSE;
  boolean                    result     = FALSE;
  sys_modem_as_id_e_type     subsId     = SYS_MODEM_AS_ID_1;
  sys_modem_as_id_e_type     mmodeSubs  = policyman_get_current_multimode_subs();

  switch (pCond->subs)
  {
    case SUBS_MMODE:
      subsId = mmodeSubs;
      break;

    default:
      if (mmodeSubs == SYS_MODEM_AS_ID_1)
      {
        subsId = SYS_MODEM_AS_ID_2;
      }
      break;
  }

  /*  Use serving MCC condition to look into the correct MCC when PLMN info is available. */  
  if (policyman_subs_has_location(subsId))
  {
    result = policyman_condition_serving_mcc_evaluate(pCondition, pCtx);
    goto Done;
  }

  pMccSet = policyman_ss_ensure_mcc_set(pCond);
    
  /*  Without a set, we can't work.
   */
  if (pMccSet == NULL)             
  { 
    POLICYMAN_MSG_ERROR_0("named MCC set not found");
    goto Done;
  }
    
  /* determine UE location from elsewhere and use it in MCC check 
       Try evaluation by looking into HLOS MCC or other SUBS info
  */
  haveLocation = policyman_determine_location_info(pState);

  if (haveLocation)
  {
    sys_mcc_type mcc;
   
    mcc    = policyman_plmn_get_mcc(policyman_state_get_device_plmn(pState));
    result = policyman_set_contains(pMccSet, &mcc);
    POLICYMAN_MSG_HIGH_2("looking into Device MCC %d, match found = %d", mcc, result);
  }
  else
  {
    POLICYMAN_MSG_ERROR_1("Device location is not known, skipping evaluation on subs %d", subsId);
  }

Done:

 return result;
}

/*-------- policyman_condition_location_mcc_new --------*/
mre_status_t policyman_condition_location_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
)
{
  return  policyman_plmn_mcc_new( pElem, 
                                  pPolicy, 
                                  ppCondition, 
                                  policyman_condition_location_mcc_evaluate
                                );
}

/*-----------------------------------------------------------------------------
  <service_status_in>
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  policyman_set_t   *pSrvStatusSet;
  policyman_subs_t  subs;
} policyman_service_status_condition_t;

/*-------- policyman_str_to_service_status --------*/
sys_srv_status_e_type
policyman_str_to_service_status(
  char const  *pStr
  )
{
  typedef struct
  {
    const char                *pStr;
    sys_srv_status_e_type     srv_status;
  } service_status_map;

  sys_srv_status_e_type     srv_status = SYS_SRV_STATUS_NONE;

  static service_status_map map[] =
  {
    {"FULL",              SYS_SRV_STATUS_SRV},
    {"LIMITED",           SYS_SRV_STATUS_LIMITED},
    {"LIMITED-REGIONAL",  SYS_SRV_STATUS_LIMITED_REGIONAL},
    {"OOS",               SYS_SRV_STATUS_NO_SRV},
    {"POWER_SAVE",        SYS_SRV_STATUS_PWR_SAVE},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      srv_status = map[i].srv_status;
      break;
    }
  }

#if defined(TEST_FRAMEWORK)
  #error code not present
#endif /* +TEST_FRAMEWORK. */

  return srv_status;
}


/*-------- policyman_service_status_in_subs --------*/
boolean
policyman_service_status_in_subs(
  sys_modem_as_id_e_type  subsId,
  policyman_set_t         *pSrvStatusSet,
  policyman_state_t const *pState
  )
{
  boolean retval = FALSE;
  policyman_stack_info_t  *pStack0 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_1);
  policyman_stack_info_t  *pStack1 = policyman_ss_get_stack_info(subsId, SYS_MODEM_STACK_ID_2);  

  /* If both stacks are operational then check for both
   */
  if(   pStack0->is_operational 
     && pStack1->is_operational
    )
  {
    retval =    policyman_set_contains(pSrvStatusSet, &pStack0->srv_status)
             || policyman_set_contains(pSrvStatusSet, &pStack1->srv_status);
  }

  /* Check stack 0 has srv in set
   */
  else if(pStack0->is_operational)
  {
    retval = policyman_set_contains(pSrvStatusSet, &pStack0->srv_status);
  }

  /* Check stack 1 has srv in set
   */
  else if(pStack1->is_operational)
  {
    retval = policyman_set_contains(pSrvStatusSet, &pStack1->srv_status);
  }

  return retval;
}


/*-------- policyman_condition_service_status_evaluate --------*/
static boolean
policyman_condition_service_status_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_service_status_condition_t const  *pCond;
  sys_modem_as_id_e_type                      mmodeSubs;
  sys_srv_status_e_type                       srvMmodeStack0;
  sys_srv_status_e_type                       srvMmodeStack1;
  sys_srv_status_e_type                       srvNonMmode;
  boolean                                     result;
  policyman_state_t const                     *pState = ((policy_execute_ctx_t *) pCtx)->pState;

  pCond = (policyman_service_status_condition_t *) pCondition;
  mmodeSubs = policyman_get_current_multimode_subs();

  /* MMODE subs can have 2 stacks active so we should check the
       service status on both the stacks if they are active.
   */

  srvMmodeStack0 = policyman_state_get_srv_status(
                        pState,
                        SYS_MODEM_STACK_ID_1,
                        mmodeSubs
                        );

  srvMmodeStack1 = policyman_state_get_srv_status(
                        pState,
                        SYS_MODEM_STACK_ID_2,
                        mmodeSubs
                        );

  srvNonMmode = policyman_state_get_srv_status(
                        pState,
                        SYS_MODEM_STACK_ID_1,
                        (mmodeSubs == SYS_MODEM_AS_ID_1)? SYS_MODEM_AS_ID_2 : SYS_MODEM_AS_ID_1
                        );

  
  switch (pCond->subs)
  {
    case SUBS_MMODE:
      POLICYMAN_MSG_HIGH_2("<service_status_in> for subs=\"mmode\": service is stack[0] %d stack[1] %d", 
                            srvMmodeStack0, srvMmodeStack1);

      result =  policyman_service_status_in_subs(mmodeSubs, pCond->pSrvStatusSet, pState);  
      break;
      
    case SUBS_NON_MMODE:
      POLICYMAN_MSG_HIGH_1("<service_status_in> for subs=\"non-mmode\": service is %d", srvNonMmode);
      result = policyman_set_contains(pCond->pSrvStatusSet, &srvNonMmode);
      break;

    case SUBS_ALL:
      POLICYMAN_MSG_HIGH_3("<service_status_in> for subs=\"all\": service is mmode: stack[0] %d, stack[1] %d and non-mmode: stack[0] %d", 
                            srvMmodeStack0, srvMmodeStack1, srvNonMmode);

      result =    policyman_service_status_in_subs(mmodeSubs, pCond->pSrvStatusSet, pState)
               && policyman_set_contains(pCond->pSrvStatusSet, &srvNonMmode);
      break;

    default:
      result = FALSE;
      break;
  }

  POLICYMAN_MSG_HIGH_1("condition <service_status_in> returns %d", result);

  return result;
}

/*-------- policyman_condition_service_status_dtor --------*/
static void
policyman_condition_service_status_dtor(
  void  *pObj
  )
{
  policyman_service_status_condition_t *pCondition;
  pCondition = (policyman_service_status_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pSrvStatusSet);
  policyman_condition_dtor(pCondition);
}


/*-------- policyman_get_srv_status_set --------*/
static policyman_set_t *
policyman_get_srv_status_set(
  policyman_xml_element_t const *pElem
  )
{
  policyman_set_t       *pSet = NULL;
  char const            *pStr;
  char                  token[32];
  sys_srv_status_e_type srv_status;
  
  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    goto Done;
  }

  pSet = policyman_set_new(sizeof(sys_srv_status_e_type),
                           5, POLICYMAN_SET_NO_MAX_SIZE,
                           NULL, NULL
                           );
  
  while (policyman_get_token(&pStr, token, sizeof(token)))
  {
    srv_status = policyman_str_to_service_status(token);
    if (srv_status == SYS_SRV_STATUS_NONE)
    { 
      REF_CNT_OBJ_RELEASE_IF(pSet);
      goto Done;
    }
    policyman_set_add(pSet, &srv_status);
  }

Done:
  return pSet;
}


/*-------- policyman_condition_service_status_new --------*/
mre_status_t
policyman_condition_service_status_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                          status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_subs_t                      subs = SUBS_MMODE;
  policyman_set_t                       *pSrvStatusSet = NULL;
  policyman_service_status_condition_t  *pCondition = NULL;
  char const                            *pStr;

  /*---------------------------------------------------------------------------
    Get which stack(s) to check
  ---------------------------------------------------------------------------*/
  pStr = policyman_xml_get_attribute(pElem, "subs");
  if (pStr != NULL)
  {
    subs = policyman_str_to_subs(pStr);
    if (subs == SUBS_NONE)
    {
      goto Done;
    }
  }

  /*---------------------------------------------------------------------------
    Get the list of statuses.
  ---------------------------------------------------------------------------*/
  pSrvStatusSet = policyman_get_srv_status_set(pElem);
  if (pSrvStatusSet == NULL)
  {
    POLICYMAN_MSG_ERROR_0("<service_status_in>: list of service status required");
    goto Done;
  }
  
  /*---------------------------------------------------------------------------
    Create the condition object.
  ---------------------------------------------------------------------------*/
  pCondition = (policyman_service_status_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_service_status_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_service_status_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_service_status_evaluate;
  pCondition->pSrvStatusSet = pSrvStatusSet;
  ref_cnt_obj_add_ref(pSrvStatusSet);
  pCondition->subs = subs;
  
  status = MRE_STATUS_SUCCESS;

Done:
  REF_CNT_OBJ_RELEASE_IF(pSrvStatusSet);
  if (MRE_FAILED(status))
  {
    REF_CNT_OBJ_RELEASE_IF(pCondition);
  }

  *ppCondition = (policyman_condition_t *) pCondition;
  POLICYMAN_MSG_HIGH_1("policyman_condition_service_status_new status %d", status);
  
  return status;
}


/*-------- policyman_condition_serving_rat_evaluate --------*/
static boolean
policyman_condition_serving_rat_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_serving_rat_condition_t const *pCond = (policyman_serving_rat_condition_t *) pCondition;
  policyman_ss_info_t const               *pSSInfo = policyman_ss_get_ss_info(pCond->subsId);
  boolean                                 result;

  /* return result for MAIN stack only
    */
  result = (pCond->rat_mask & BM(pSSInfo->stack[SYS_MODEM_STACK_ID_1].sys_mode)) != 0;

  POLICYMAN_MSG_HIGH_1("condition <serving_rat_in> returns %d", result);
  return result;
}


/*-------- policyman_condition_serving_rat_new --------*/
mre_status_t
policyman_condition_serving_rat_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                      status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_serving_rat_condition_t *pCondition;
  char const                        *pStr;
  char                              token[16];
  uint32                            mask;
  sys_modem_as_id_e_type            subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<serving_rat_in> no list of RATs specified", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_serving_rat_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_serving_rat_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_serving_rat_evaluate;
  pCondition->rat_mask = 0;

  while (policyman_get_token(&pStr, token, sizeof(token)))
  {
    mask = policyman_rat_capability_str_to_rat(token);
    if(mask == INVALID_RAT_MASK)
    {
      POLICYMAN_UT_MSG("<serving_rat_in>: invalid RAT specified: %s", token, 0, 0);
      break;
    }
    pCondition->rat_mask |= mask;
  }

  status = (pCondition->rat_mask != SYS_SYS_MODE_MASK_NONE)?
                MRE_STATUS_SUCCESS
              : MRE_STATUS_ERR_MALFORMED_XML;


  if (MRE_SUCCEEDED(status))
  {
    pCondition->subsId = subs;
    *ppCondition = (policyman_condition_t *) pCondition;
  }
  else
  {
    REF_CNT_OBJ_RELEASE_IF(pCondition);
  }

  Done:
    return status;
}


/*-------- policyman_condition_serving_rf_band_evaluate --------*/
static boolean
policyman_condition_serving_rf_band_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_serving_rf_band_condition_t *pCond = (policyman_serving_rf_band_condition_t *) pCondition;
  sys_band_class_e_type                 active_band;
  boolean                               ret;

  /* Check only for stack 1
   */
  active_band = policyman_state_get_active_band(((policy_execute_ctx_t *) pCtx)->pState, SYS_MODEM_STACK_ID_1, pCond->subsId);
  
  ret = policyman_set_contains(pCond->pRfBandSet, &active_band);
  POLICYMAN_MSG_HIGH_1("condition <serving_band_in> returns %d", ret);
  return ret;
}

/*-------- policyman_condition_serving_rf_band_dtor --------*/
static void
policyman_condition_serving_rf_band_dtor(
  void  *pObj
  )
{
  policyman_serving_rf_band_condition_t *pCondition;
  pCondition = (policyman_serving_rf_band_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pRfBandSet);
  policyman_condition_dtor(pCondition);
}



/*-------- policyman_condition_serving_rf_band_new --------*/
mre_status_t
policyman_condition_serving_rf_band_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                          status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_serving_rf_band_condition_t *pCondition = NULL;
  char const                            *pStr;
  char                                  activeBandStr[4];
  sys_band_class_e_type                 active_band;
  sys_modem_as_id_e_type                subs;
  policyman_set_t                       *pRfBandSet = NULL;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<serving_band_in>: no band list specified", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_serving_rf_band_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_serving_rf_band_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_serving_rf_band_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_serving_rf_band_evaluate;
  pRfBandSet = policyman_set_new(
                               sizeof(sys_band_class_e_type),
                               5, POLICYMAN_SET_NO_MAX_SIZE,
                               NULL, NULL
                             );
  pCondition->pRfBandSet = pRfBandSet;

  while (policyman_get_token(&pStr, activeBandStr, sizeof(activeBandStr)))
  {
    active_band = atoi(activeBandStr);
    if (active_band < 0)
    { 
      POLICYMAN_UT_MSG("<serving_band_in>: invalid band specified: %s", activeBandStr, 0, 0);
      goto Done;
    }

    policyman_set_add(pRfBandSet, &active_band);
  }
  
  pCondition->subsId = subs;
  status = MRE_STATUS_SUCCESS;

Done:
  if (MRE_FAILED(status))
  {
    REF_CNT_OBJ_RELEASE_IF(pCondition);
  }
  
  *ppCondition = (policyman_condition_t *) pCondition;

  return status;
}

/*-------- policyman_condition_reg_reject_evaluate --------*/
static boolean
policyman_condition_reg_reject_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_reg_reject_condition_t  *pCond;
  byte                              reject_cause;
  boolean                           ret;

  pCond = (policyman_reg_reject_condition_t *) pCondition;
  reject_cause = policyman_state_get_reject_cause(((policy_execute_ctx_t *) pCtx)->pState, SYS_MODEM_STACK_ID_1, pCond->subsId);
  
  ret = policyman_set_contains(pCond->pRejectCauseSet, &reject_cause);
  POLICYMAN_MSG_HIGH_1("condition <reg_reject_cause_in> returns %d", ret);
  return ret;
}

/*-------- policyman_condition_reg_reject_dtor --------*/
static void
policyman_condition_reg_reject_dtor(
  void  *pObj
  )
{
  policyman_reg_reject_condition_t *pCondition;
  pCondition = (policyman_reg_reject_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pRejectCauseSet);
  policyman_condition_dtor(pCondition);
}

/*-------- policyman_condition_reg_reject_new --------*/
mre_status_t
policyman_condition_reg_reject_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                      status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_reg_reject_condition_t *pCondition = NULL;
  char const                        *pStr;
  char                              RejCauseStr[4];
  byte                              reject_cause;
  sys_modem_as_id_e_type            subs;
  policyman_set_t                   *pRejectCauseSet = NULL;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<reg_reject_cause_in>: list of rejection codes required", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_reg_reject_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_reg_reject_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_reg_reject_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_reg_reject_evaluate;
  pRejectCauseSet = policyman_set_new(
                               sizeof(byte),
                               5, POLICYMAN_SET_NO_MAX_SIZE,
                               NULL, NULL
                             );
  pCondition->pRejectCauseSet = pRejectCauseSet;

  {
    int temp_rej;
  while (policyman_get_token(&pStr, RejCauseStr, sizeof(RejCauseStr)))
  {
      temp_rej = atoi(RejCauseStr);
      if (temp_rej < 0)
    { 
      POLICYMAN_UT_MSG("<reg_reject_cause_in>: invalid rejection cause: %s", RejCauseStr, 0, 0);
      goto Done;
    }

      reject_cause = (byte) temp_rej;
    policyman_set_add(pRejectCauseSet, &reject_cause);
  }
  }
  
  pCondition->subsId = subs;
  status = MRE_STATUS_SUCCESS;

Done:
  if (MRE_FAILED(status))
  {
    REF_CNT_OBJ_RELEASE_IF(pCondition);
  }
  
  *ppCondition = (policyman_condition_t *) pCondition;

  return status;
}

/*-----------------------------------------------------------------------------
  Serving domain pref
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  sys_srv_domain_e_type srv_domain_pref;
} policyman_serving_domain_pref_condition_t;

/*-------- policyman_str_to_serving_domain --------*/
static sys_srv_domain_e_type
policyman_str_to_serving_domain(
  char const  *pStr
  )
{
  typedef struct
  {
    const char                *pStr;
    sys_srv_domain_e_type     domain;
  } domain_map;

  sys_srv_domain_e_type domain = SYS_SRV_DOMAIN_NONE;

  static domain_map map[] =
  {
    {"CS",    SYS_SRV_DOMAIN_CS_ONLY},
    {"PS",    SYS_SRV_DOMAIN_PS_ONLY},
    {"CSPS",  SYS_SRV_DOMAIN_CS_PS},
    {"CAMP",  SYS_SRV_DOMAIN_CAMPED},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      domain = map[i].domain;
      break;
    }
  }

  if (domain == SYS_SRV_DOMAIN_NONE)
  {
    POLICYMAN_UT_MSG("Invalid value for <sys_domain_pref>.  Valid values are:", 0, 0, 0);
    for (i = 0 ; i < ARR_SIZE(map) ; ++i)
    {
      POLICYMAN_UT_MSG("    %s", map[i].pStr, 0, 0);
    }
  }

  return domain;
}


/*-------- policyman_condition_ph_domain_pref_evaluate --------*/
static boolean
policyman_condition_serving_domain_has_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_serving_domain_pref_condition_t const *pCond = (policyman_serving_domain_pref_condition_t *) pCondition;
  policyman_ss_info_t                       const *pSsInfo = policyman_ss_get_ss_info(pCond->subsId);
  policyman_stack_info_t                    const *pStacks;
  policyman_stack_info_t                    const *pStack;
  size_t                                          nStacks;
  size_t                                          stack;
  boolean                                         result = FALSE;

  /*  Get stack information in easy-to-use variables
   */
  pStacks = pSsInfo->stack;
  nStacks = ARR_SIZE(pSsInfo->stack);

  /*  Check to see if some stack's MCC is in the list.
   */
  for (stack = 0 ; stack < nStacks ; ++stack)
  {
    pStack = pStacks + stack;
  
    if (    pStack->is_operational
        &&  pCond->srv_domain_pref == pStack->srv_domain
       )
    {
      result = TRUE;
      goto Done;
    }
    else
    {
      POLICYMAN_MSG_HIGH_3("srv_domain not matches in stack %d, is_operational %d, srv_domain %d", 
                          stack, 
                          pStack->is_operational, 
                          pStack->srv_domain
                        );
    }
  }

Done:
  POLICYMAN_MSG_HIGH_1("condition <svc_domain_has> returns %d", result);
  return result;
}

/*-------- policyman_condition_serving_domain_has_new --------*/
mre_status_t
policyman_condition_serving_domain_has_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                              status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_serving_domain_pref_condition_t *pCondition;
  char const                                *pStr;
  sys_srv_domain_e_type                     domainPref;
  sys_modem_as_id_e_type                    subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<svc_domain_has>: must specify a domain preference", 0, 0, 0);
    goto Done;
  }
  
  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  domainPref= policyman_str_to_serving_domain(pStr);
  if (domainPref == SYS_SRV_DOMAIN_NONE)
  {
    POLICYMAN_UT_MSG("<svc_domain_has>: invalid domain preferences: %s", pStr, 0, 0);
    goto Done;
  }

  pCondition = (policyman_serving_domain_pref_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_serving_domain_pref_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_serving_domain_has_evaluate;
  pCondition->srv_domain_pref = domainPref;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;
  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}

/*-----------------------------------------------------------------------------
  Enforce Full RAT mode
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;
} policyman_enforce_fullrat_condition_t;

/*-------- policyman_condition_enforce_full_rat_evaluate --------*/
static boolean policyman_condition_enforce_full_rat_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  boolean                                      result;
  policy_execute_ctx_t  *pContext = (policy_execute_ctx_t *)pCtx;

  /* Full RAT is enforced only when XML defines the boolean.
        Check for precondition is kept to detect this is "first" PWR_SAVE event after coming 
        ONLINE, and not a regular PWR_SAVE event sent after service loss.
   */
  result =    policyman_state_get_full_rat()
           && policyman_ss_check_subs_pwr_save(pContext->asubs_id)
           && !policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, pContext->asubs_id);

  POLICYMAN_MSG_HIGH_2("condition <enforce_full_rat> on subs %d returns %d", pContext->asubs_id, result);
  return result;
}

/*-------- policyman_condition_enforce_full_rat_new --------*/
mre_status_t policyman_condition_enforce_full_rat_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                           status     = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_enforce_fullrat_condition_t *pCondition = NULL;

  pCondition = (policyman_enforce_fullrat_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_enforce_fullrat_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  
  pCondition->isTrue = policyman_condition_enforce_full_rat_evaluate;

  *ppCondition = (policyman_condition_t *) pCondition;
  status = MRE_STATUS_SUCCESS;

  POLICYMAN_MSG_HIGH_1("policyman_condition_enforce_full_rat_new status %d", status);
  return status;
}

/*-----------------------------------------------------------------------------
  Have Service
-----------------------------------------------------------------------------*/

/*-------- policyman_condition_have_service_evaluate --------*/
STATIC boolean policyman_condition_have_service_evaluate(
  mre_condition_t const *pCondition,
  void                  *pCtx
)
{
  boolean   haveService = FALSE;
  size_t    subs;

  for (subs = SYS_MODEM_AS_ID_1; !haveService && subs < POLICYMAN_NUM_SUBS; subs++)
  {
    haveService = policyman_ss_check_subs_in_service(subs);
  }

  POLICYMAN_MSG_HIGH_1("condition <have_service> returns %d", haveService);
  return haveService;
}

/*-------- policyman_condition_have_service_new --------*/
mre_status_t policyman_condition_have_service_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  mre_condition_t              **ppCondition
)
{
  mre_status_t     status;
  mre_condition_t *pCondition = NULL;

  pCondition = (mre_condition_t *)policyman_mem_alloc(sizeof(mre_condition_t));
  ref_cnt_obj_init(pCondition, mre_condition_dtor);
  
  pCondition->isTrue = policyman_condition_have_service_evaluate;

  *ppCondition = pCondition;
  status = MRE_STATUS_SUCCESS;

  POLICYMAN_MSG_HIGH_1("policyman_condition_have_service_new returned status %d", status);
  return status;
}

/*===========================================================================
  FUNCTION POLICYMAN_GET_CM_SERVING_INFO()
 
  DESCRIPTION
    GET changed CM SS Info from event into local policyman_ss_info_t structure
 
  PARAMETERS
    ss_info : Policyman local structure to retain changed SS info
    ss_evt : Incoming CM Serving Info
  ===========================================================================*/
void policyman_get_cm_serving_info
(
  policyman_ss_info_t *pm_ss_info, 
  policyman_cm_serving_info_t *cm_serving_info
)
{
  sys_modem_stack_id_e_type pm_stack;
  policyman_stack_info_t *pStack;

  if(pm_ss_info != NULL && cm_serving_info != NULL)
  {    
    /* get stack info from serving system info
      */
    if(policyman_check_stack_is_valid(cm_serving_info->stack))
    {
      /* Mark stack operational since CM is sending data for that stack
              get srv_Status 
        */
      pm_stack = cm_serving_info->stack;
      pStack = &pm_ss_info->stack[pm_stack];
      
      pStack->is_operational = TRUE;

      pStack->srv_status = cm_serving_info->srv_status;

      /* get sys_mode 
         */
      pStack->sys_mode = cm_serving_info->sys_mode;

      POLICYMAN_MSG_HIGH_4("cm_serving_info: subs=%d, stack %d, received sys_mode %d, srv_status %d",
                          cm_serving_info->asubs_id,
                          pm_stack, 
                          cm_serving_info->sys_mode, 
                          cm_serving_info->srv_status
                        );

      /* update the PLMN id - CM guarantees to give only 3GPP PLMN id
        */
      pStack->id_type = cm_serving_info->id_type;
      if(cm_serving_info->id_type == SYS_SYS_ID_TYPE_UMTS)
      {
        pStack->plmn = cm_serving_info->plmn;
        POLICYMAN_MSG_HIGH_1("stack %d PLMN:", pm_stack);
        policyman_plmn_print(&pStack->plmn);
        if( policyman_ss_status_is_service( cm_serving_info->srv_status ) )
        {
          pStack->serving_info_valid = TRUE;
        }
      }
      else
      {
        POLICYMAN_MSG_ERROR_0("PLMN id is not 3GPP, PLMN not updated");
      }
    }
    else
    {
      POLICYMAN_MSG_ERROR_1("cm_serving_info ignored, cm stack %d not handled", cm_serving_info->stack);
    }
  }
  else
  {
    POLICYMAN_MSG_ERROR_2("get_cm_serving_info NULL pointer, data not copied - pm_ss_info %x, cm_serving_info %x", pm_ss_info, cm_serving_info);
  }
}


/*===========================================================================
  FUNCTION POLICYMAN_EXECUTE_PROCESS_CM_SERVING_INFO()
 
  DESCRIPTION
    Process serving system information received from Call Manager 
 ===========================================================================*/
void policyman_execute_process_cm_serving_info
(
  policyman_cmd_t *pCmd
)
{
  struct policyman_cm_serving_info_cb_s *serving_info = (struct policyman_cm_serving_info_cb_s*) pCmd;
  struct policyman_ss_info_t            ss_info;
  policyman_cm_serving_info_t           *pInfo = NULL;

  if(serving_info != NULL)
  {
    pInfo = &serving_info->info;

    /* Allow API to update for all service domains but for subs 0, subs 1 & subs2
      */
    if(  pInfo
      && pInfo->asubs_id > SYS_MODEM_AS_ID_NONE 
      && pInfo->asubs_id < SYS_MODEM_AS_ID_NO_CHANGE
      )
    {
      /* restore the old state first
          */
      ss_info = *policyman_ss_get_ss_info(pInfo->asubs_id);
      policyman_get_cm_serving_info(&ss_info, pInfo);
      policyman_state_set_precondition_met(POLICYMAN_PRECOND_SS | POLICYMAN_PRECOND_LOCATION, pInfo->asubs_id);

      policyman_state_update_ss_info(&ss_info, pInfo->asubs_id);
    }
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_CM_SERVING_INFO_CB()

  DESCRIPTION
    Get Serving system information from CM

  PARAMETERS
    pCmServingInfo    Pointer to CM serving system information.
===========================================================================*/
void policyman_cm_serving_info_cb(policyman_cm_serving_info_t *pCmServingInfo)
{
  struct policyman_cm_serving_info_cb_s *pCmd;

  if (pCmServingInfo != NULL)
  {
    POLICYMAN_MSG_HIGH_1("received cm serving info cb for subs %d", pCmServingInfo->asubs_id);

    pCmd = (struct policyman_cm_serving_info_cb_s *) policyman_cmd_new( sizeof(struct policyman_cm_serving_info_cb_s), 
                                                                 policyman_execute_process_cm_serving_info, 
                                                                 NULL,
                                                                 NULL                               // No policy associated with CM SS Event
                                                               );

    pCmd->info = *pCmServingInfo;

    policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
    ref_cnt_obj_release(pCmd);
  }
}

/*-------- policyman_msim_non_multimode_subs_in_service --------*/
boolean policyman_msim_non_multimode_subs_in_service(
  void
)
{
  sys_modem_as_id_e_type nonMmodeSubs = SYS_MODEM_AS_ID_1;

  /* Determine non-multimode subs id
   */
  if(policyman_get_current_multimode_subs() == SYS_MODEM_AS_ID_1)
  {
    nonMmodeSubs = SYS_MODEM_AS_ID_2;
  }
  
  return policyman_ss_some_stack_has_service(policyman_ss_get_ss_info(nonMmodeSubs));
}

/*-------- policyman_msim_non_multimode_subs_has_sglte_mcc --------*/
boolean
policyman_msim_non_multimode_subs_has_sglte_mcc(
  void
  )
{  
  sys_modem_as_id_e_type nonMmodeSubs = SYS_MODEM_AS_ID_1;
  policyman_ss_info_t    *pSsInfo;
  boolean                retval       = FALSE;
  policyman_stack_info_t *pStack;

  size_t                 i;
  policyman_set_t        *pSet;
  sys_mcc_type           mcc;

  /* Determine non-multimode subs id
   */
  if(policyman_get_current_multimode_subs() == SYS_MODEM_AS_ID_1)
  {
    nonMmodeSubs = SYS_MODEM_AS_ID_2;
  }

  /* get sglte_mccs set
    */
   pSet = (policyman_set_t*) policyman_policy_find_named_object(
                                 "sglte_mccs",
                                 POLICYMAN_NAMED_MCC_SET
                                 );
  /*  Without a set, we can't work.
   */
  if (pSet == NULL)             
  { 
    POLICYMAN_MSG_ERROR_0("named PLMN set not found");
    goto Done;
  }   

  pSsInfo = policyman_ss_get_ss_info(nonMmodeSubs);

  /* Print stack info
   */
  policyman_ss_print_stack_info(policyman_state_get_state(), nonMmodeSubs);

  /* check if MCC is in set
   */
   for(i=SYS_MODEM_STACK_ID_1; i<SYS_MODEM_STACK_ID_MAX; i++)
   {
    pStack = &pSsInfo->stack[i];
     mcc = policyman_ss_get_mcc(pSsInfo, i);     

     if(  pStack->is_operational
       && pStack->serving_info_valid
       && policyman_set_contains(pSet, &mcc)
       )
     {
       POLICYMAN_MSG_HIGH_1("MCC of stack %d is in the MCC set", i);
       retval = TRUE;
       goto Done;
     }
     else
     {
       POLICYMAN_MSG_HIGH_3("MCC not found in stack %d, is_operational %d, serving_info_valid %d", i, pStack->is_operational, pStack->serving_info_valid);
     }
   }

Done:
  return retval;
}

/*-------- policyman_execute_process_hlos_mcc --------*/
void policyman_execute_process_hlos_mcc(
  policyman_cmd_t *pCmd
)
{  
  struct policyman_hlos_mcc_cmd_s *pHlosInfo = (struct policyman_hlos_mcc_cmd_s *) pCmd;
  sys_modem_as_id_e_type           subsId    = policyman_get_current_multimode_subs();

  /* No handling should be done if XML doesn't define boolean 'include_hlos_mcc'
  */
  if (!policyman_state_get_include_hlos_mcc())
  {
    POLICYMAN_MSG_ERROR_0("Boolean 'include_hlos_mcc' not defined in XML, hlos_mcc not used");
    return;
  }

  /*  Ignore HLOS MCC if Modem MCC is available.
   */
  if (policyman_state_check_precondition_met(POLICYMAN_PRECOND_SS, subsId))
  {
    POLICYMAN_MSG_HIGH_0("ignoring HLOS MCC, MCC from Modem is available");
    return;
  }

  /*  Sanitize the input parameter. MCC is 3 digits
   */
  if (  pHlosInfo->status != 0
     || pHlosInfo->mcc < 100
     || pHlosInfo->mcc > 999
     )
  {
    POLICYMAN_MSG_ERROR_0("ignoring HLOS MCC, status is not SUCCESS or MCC out of range !!");
    return;
  }
 
  policyman_state_update_hlos_mcc( pHlosInfo->mcc, 
                                   pHlosInfo->confidenceLevel, 
                                   pHlosInfo->status );

  policyman_state_set_precondition_met(POLICYMAN_PRECOND_LOCATION, subsId);

  /* Update current_mcc for 3GPP system only and call for rule execution
    */
  policyman_ss_update_current_mcc(subsId);
  policyman_state_handle_update(subsId);
}

/*-------- policyman_set_hlos_mcc --------*/
policyman_status_t policyman_set_hlos_mcc(
  sys_mcc_type  mcc,
  size_t        confidenceLevel,
  size_t        status
)
{
  struct policyman_hlos_mcc_cmd_s *pCmd;
 
  POLICYMAN_MSG_HIGH_3( "received HLOS MCC %d, confidence level %d, HLOS status %d", 
                        mcc, 
                        confidenceLevel, 
                        status );
 
  pCmd = (struct policyman_hlos_mcc_cmd_s *) policyman_cmd_new( sizeof(struct policyman_hlos_mcc_cmd_s), 
                                                                policyman_execute_process_hlos_mcc, 
                                                                NULL,
                                                                NULL );
 
  pCmd->mcc             = mcc;
  pCmd->confidenceLevel = confidenceLevel;
  pCmd->status          = status;

  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
  ref_cnt_obj_release(pCmd);

  return POLICYMAN_STATUS_SUCCESS;
}

/*-------- policyman_get_current_mcc --------*/
policyman_status_t policyman_get_current_mcc(
  sys_modem_as_id_e_type subsId,
  sys_mcc_type           *pMcc
)
{
  boolean provide_current_mcc;
  boolean include_hlos_mcc;

  if ( subsId < SYS_MODEM_AS_ID_1
     || subsId > SYS_MODEM_AS_ID_2
     || pMcc == NULL
     )
  {
    POLICYMAN_MSG_ERROR_1("Invalid argument: subsId %d", subsId);
    return POLICYMAN_STATUS_ERR_INVALID_ARGS;
  }

  /* return error if XML doesn't allow to return current MCC
   */
  include_hlos_mcc = policyman_state_get_include_hlos_mcc();
  provide_current_mcc = policyman_boolean_get_value("pm:provide_current_mcc");

  if (  subsId != policyman_get_current_multimode_subs()
     || !(include_hlos_mcc || provide_current_mcc)
     )
  {
    POLICYMAN_MSG_ERROR_3( "requested current_mcc for subsId %d, include_hlos_mcc = %d, provide_mcc_to_nas = %d: error returned", 
                           subsId, 
                           include_hlos_mcc,
                           provide_current_mcc);

    return POLICYMAN_STATUS_ERR_NOT_PRESENT;
  }
  
  /* fetch MCC and return
   */
  *pMcc = policyman_state_get_current_mcc();
  return POLICYMAN_STATUS_SUCCESS;
}
