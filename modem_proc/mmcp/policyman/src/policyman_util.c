/**
  @file policyman_util.c

  @brief Policy Manager utility functions.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_util.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_util.h"
#include "policyman_dbg.h"
#include "policyman_rf.h"

#include "modem_mem.h"
#include <stringl/stringl.h>



/*=============================================================================
  XML parsing helpers
=============================================================================*/


/*-------- policyman_util_get_base --------*/
boolean
policyman_util_get_base(
  policyman_xml_element_t const *pElem,
  policyman_base_t              *pBase
  )
{
  boolean     succeeded = TRUE;
  char const  *pStr;  

  pStr = mre_xml_get_attribute(pElem, "base");

  if      (pStr == NULL)
  {
    succeeded = FALSE;
  }
  else if (strcasecmp(pStr, "none") == 0)
  {
    *pBase = POLICYMAN_BASE_NONE;
  }
  else if (strcasecmp(pStr, "config") == 0)
  {
    *pBase = POLICYMAN_BASE_CONFIG;
  }    
  else if (strcasecmp(pStr, "hardware") == 0)
  {
    *pBase = POLICYMAN_BASE_HARDWARE;
  }
  else
  {
    POLICYMAN_UT_MSG("invalid 'base' specified: %s", pStr, 0, 0);
    succeeded = FALSE;
  }

  return succeeded;
}

/*-------- policyman_util_get_subs --------*/
mre_status_t
policyman_util_get_subs(
  policyman_xml_element_t const *pElem,
  sys_modem_as_id_e_type        *pSubs
  )
{
  mre_status_t  status = MRE_STATUS_SUCCESS;
  char const    *pStr;
  size_t        subsId;

  pStr = mre_xml_get_attribute(pElem, "subs");

  if      (pStr == NULL)
  {
    *pSubs = SYS_MODEM_AS_ID_1;
  }
  else 
  {
    subsId = atoi(pStr);
    if( subsId <= POLICYMAN_MAX_SUB )
    {
      *pSubs = subsId - 1;
    }
    else
    {
      POLICYMAN_MSG_ERROR_2("subs: element has a subs id of %d, but there are only %d Subs Allowed",
                            subsId, POLICYMAN_MAX_SUB);
      status = MRE_STATUS_ERR_MALFORMED_XML;
    }
  }

  return status;
}

/*-------- policyman_check_stack_is_valid --------*/
boolean 
policyman_check_stack_is_valid(
  sys_modem_stack_id_e_type stack
  )
{

  return    stack == SYS_MODEM_STACK_ID_1
         || stack == SYS_MODEM_STACK_ID_2;
}

/*-------- policyman_util_get_rats_from_bands --------*/
void
policyman_util_get_rats_from_bands(
  cm_band_pref_e_type  gwBands,
  cm_band_pref_e_type  lteBands,
  cm_band_pref_e_type  tdsBands,
  uint32               *pratMask
  )
{
  if(pratMask)
  {
    *pratMask = 0;

    if (gwBands & ALL_GSM_BANDS)
    {
      *pratMask |= SYS_SYS_MODE_MASK_GSM;
    }

    if (gwBands & ALL_WCDMA_BANDS)
    {
      *pratMask |= SYS_SYS_MODE_MASK_WCDMA;
    }

    if (lteBands & ALL_LTE_BANDS)
    {
      *pratMask |= SYS_SYS_MODE_MASK_LTE;
    }
  
    if (tdsBands & ALL_TDS_BANDS)
    {
      *pratMask |= SYS_SYS_MODE_MASK_TDS;
    }
  }
}


/*-------- policyman_util_check_valid_sys_mode --------*/
boolean policyman_util_check_valid_sys_mode(
  sys_sys_mode_e_type sys_mode
  )
{
  if (   sys_mode != SYS_SYS_MODE_CDMA
      && sys_mode != SYS_SYS_MODE_HDR
      && sys_mode != SYS_SYS_MODE_GSM
      && sys_mode != SYS_SYS_MODE_WCDMA
      && sys_mode != SYS_SYS_MODE_LTE
      && sys_mode != SYS_SYS_MODE_TDS
     )
  {
    return FALSE;
  }

  return TRUE;
}

/*-------- policyman_util_str_to_rat --------*/
rat_map * policyman_util_str_to_rat(
  const char              *pStr
  )
{
  static rat_map map[] =
  {
    {"CDMA",    SYS_SYS_MODE_MASK_CDMA,  SYS_SYS_MODE_CDMA},
    {"HDR",     SYS_SYS_MODE_MASK_HDR,   SYS_SYS_MODE_HDR},
    {"GSM",     SYS_SYS_MODE_MASK_GSM,   SYS_SYS_MODE_GSM},
    {"WCDMA",   SYS_SYS_MODE_MASK_WCDMA, SYS_SYS_MODE_WCDMA},
    {"LTE",     SYS_SYS_MODE_MASK_LTE,   SYS_SYS_MODE_LTE},
    {"TDSCDMA", SYS_SYS_MODE_MASK_TDS,   SYS_SYS_MODE_TDS},
    {"C",       SYS_SYS_MODE_MASK_CDMA,  SYS_SYS_MODE_CDMA},
    {"H",       SYS_SYS_MODE_MASK_HDR,   SYS_SYS_MODE_HDR},
    {"G",       SYS_SYS_MODE_MASK_GSM,   SYS_SYS_MODE_GSM},
    {"W",       SYS_SYS_MODE_MASK_WCDMA, SYS_SYS_MODE_WCDMA},
    {"L",       SYS_SYS_MODE_MASK_LTE,   SYS_SYS_MODE_LTE},
    {"T",       SYS_SYS_MODE_MASK_TDS,   SYS_SYS_MODE_TDS}
  };
  
  size_t  i;
  rat_map * element = NULL;

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(pStr, map[i].pStr) == 0)
    {
      element = &map[i];
      break;
    }
  }
  
  return element;
}


