/**
  @file policyman_cfgitem.c

  @brief Functions to manage PM configuration items.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_cfgitem.c#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_cfgitem.h"
#include "policyman_cm.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_efs.h"
#include "policyman_msg.h"
#include "policyman_rat_capability.h"
#include "policyman_rat_order.h"
#include "policyman_set.h"
#include "policyman_svc_mode.h"
#include "policyman_task.h"
#include "policyman_ue_mode.h"
#include "policyman_uim.h"
#include "policyman_util.h"

#include "stringl/stringl.h"



typedef policyman_item_t *  (*get_default_fn_t)(sys_modem_as_id_e_type subsId);
typedef boolean             (*items_equal_fn_t)(policyman_item_t *i1, policyman_item_t *i2);
typedef void                (*display_fn_t)(policyman_item_t *item);
typedef void                (*on_update_fn_t)(policyman_item_t *item);
typedef policyman_item_t *  (*get_efs_fn_t)(sys_modem_as_id_e_type subs_id);

// TODO: look into making this a cp_mutex
extern policyman_crit_sect_t   policyman_item_crit_sect;

struct policyman_config_info_t
{
  policyman_item_id_t id;
  const char          *pName;
  get_default_fn_t     pfnGetDefault;
  items_equal_fn_t     pfnItemsEqual;
  display_fn_t         pfnDisplay;
  on_update_fn_t       pfnOnUpdate;
  get_efs_fn_t         pfnGetEFS;

};

typedef struct 
{
  policyman_item_id_t    id;
  sys_modem_as_id_e_type asubs_id;
} policyman_item_cmp_t;

/* Database of policyman config info
*/
STATIC policyman_config_info_t  policyman_cfg_info[] =
{
  {
    POLICYMAN_ITEM_DEVICE_CONFIGURATION,
    "Modem configuration",
    policyman_device_config_get_default,
    policyman_device_config_compare,
    policyman_device_config_display,
    NULL,
    policyman_device_config_get_default_from_efs
  },
  {
    POLICYMAN_ITEM_RAT_CAPABILITY,
    "RAT capability",
    policyman_rat_capability_get_default,
    policyman_rat_capability_compare,
    policyman_rat_capability_display,
    NULL,
    policyman_rat_capability_get_default_from_efs
  },
  {
    POLICYMAN_ITEM_RF_BANDS,
    "RF bands",
    policyman_rf_bands_get_default,
    policyman_rf_bands_compare,
    policyman_rf_bands_display,
    NULL,
    policyman_rf_bands_get_default_from_efs
  },
  {
    POLICYMAN_ITEM_SVC_MODE,
    "SVC mode",
    policyman_svc_mode_get_default,
    policyman_svc_mode_compare,
    policyman_svc_mode_display,
    NULL,
    policyman_svc_mode_get_default_from_efs
  },
  {
    POLICYMAN_ITEM_UE_MODE,
    "UE mode",
    policyman_ue_mode_get_default,
    policyman_ue_mode_compare,
    policyman_ue_mode_display,
    policyman_ue_mode_on_update,
    policyman_ue_mode_get_default_from_efs
  },
  {
    POLICYMAN_ITEM_RAT_ACQ_ORDER,
    "Rat acquisition order",
    policyman_rat_order_get_default,
    policyman_rat_order_compare,
    policyman_rat_order_display,
    NULL,
    NULL
  },
};

typedef struct
{
  policyman_config_info_t *pCfgInfo;
  sys_modem_as_id_e_type  subsId;
  
  policyman_item_t        *pItem;  
} policyman_config_item_t;

/* Database of actual policyman items
    Array size is subtracted by 1 as we wan't only one device config item
*/
static policyman_config_item_t policyman_cfgitems[(ARR_SIZE(policyman_cfg_info) - 1)*POLICYMAN_NUM_SUBS + 1];

static void
policyman_cfgitem_notify_clients(
  policyman_set_t *pItemSet
  );



/*-------- policyman_cfgitem_init --------*/
void
policyman_cfgitem_init(
  void
  )
{
  size_t                   numSubs; 
  size_t                   cfgIndex; 
  size_t                   itemIndex;
  policyman_config_item_t *pCfgItem;

  itemIndex = 0;
  for(numSubs = 0; numSubs < POLICYMAN_NUM_SUBS; ++numSubs)
  {
    for(cfgIndex = 0; cfgIndex < ARR_SIZE(policyman_cfg_info); ++cfgIndex)
    {
      /* Skip updating device configuration item for all subsequent subs
         */
      if(  numSubs > 0
        && (numSubs*ARR_SIZE(policyman_cfg_info)+ cfgIndex)% ARR_SIZE(policyman_cfg_info) == 0
        )
      {
        continue;
      }

      pCfgItem           = &policyman_cfgitems[itemIndex];
      pCfgItem->pCfgInfo = &policyman_cfg_info[cfgIndex];
      pCfgItem->subsId   = numSubs;
      pCfgItem->pItem    = NULL;

      if(
            !policyman_is_test_mode()
         && pCfgItem->pCfgInfo->pfnGetEFS != NULL
        )
      {
        pCfgItem->pItem = pCfgItem->pCfgInfo->pfnGetEFS(numSubs);
      }
      ++itemIndex;
    }
  }
}


/*-------- policyman_persist_cfgitems --------*/
void
policyman_persist_cfgitems(
  policyman_set_t *pNotifySet
)
{
  size_t i;
  size_t nElems;

  policyman_item_id_t     id;
  policyman_item_t const  *pItem;
  policyman_item_t const  **ppItem;
  boolean                 doUpdate = FALSE;
  uint8                   subsMask = 0;


  /*  Do not persist if we're in test mode, or if device has not been calibrated yet.
   */
  if (policyman_is_test_mode() || !policyman_state_device_calibrated() )
  {
    return;
  }

  nElems = policyman_set_num_elems(pNotifySet);
  for (i = 0; i < nElems ; ++i)
  {
    ppItem = (policyman_item_t const **) policyman_set_elem_ptr(pNotifySet, i);
    if (ppItem == NULL)
    {
      break;
    }

    pItem = *ppItem;   
    id = policyman_item_get_id(pItem);

    switch(id)
    {
      case POLICYMAN_ITEM_UE_MODE:
        policyman_ue_mode_update_to_efs(pItem);
        break;

      case POLICYMAN_ITEM_RAT_ACQ_ORDER:
        policyman_rat_order_update_to_efs(pItem);
        break;

      case POLICYMAN_ITEM_SVC_MODE:
        policyman_svc_mode_update_to_efs(pItem);
        break;

      case POLICYMAN_ITEM_RAT_CAPABILITY:
      case POLICYMAN_ITEM_RF_BANDS:
        doUpdate = TRUE;
        subsMask |= 1<<(uint8)pItem->asubs_id;
        break;

      case POLICYMAN_ITEM_DEVICE_CONFIGURATION:
        policyman_device_config_update_to_efs(pItem);
        break;

      default:
        POLICYMAN_MSG_MED_2("subs %d: items id %d doesn't need update", pItem->asubs_id, id);
    }
  }

  if(doUpdate)
  {
    policyman_update_rats_bands_to_efs(subsMask);
  }
}

/*-------- policyman_subs_id_is_configured --------*/
boolean 
policyman_subs_id_is_configured(
  sys_modem_as_id_e_type subs_id 
  )
{
  
  size_t  nSim = 0;

  (void)policyman_get_current_num_sim(&nSim) ;

  return (size_t)subs_id < nSim;
}


/*-------- policyman_send_all_config_items --------*/
void
policyman_send_all_config_items(
  void
  )
{
  policyman_set_t   *pItemSet = policyman_itemset_new();
  policyman_item_t  *pItem;
  size_t            i;

  policyman_enter_crit_sect(&policyman_item_crit_sect);

  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    if(policyman_subs_id_is_configured(policyman_cfgitems[i].subsId))
    {
      pItem = policyman_cfgitems[i].pItem;
      policyman_set_add(pItemSet, &pItem);
    }
  }

  policyman_leave_crit_sect(&policyman_item_crit_sect);

  policyman_cfgitem_notify_clients(pItemSet);
  policyman_persist_cfgitems(pItemSet);

  ref_cnt_obj_release(pItemSet);
}


/*-------- policyman_cfgitem_ensure_item --------*/
static void policyman_cfgitem_ensure_item(
  policyman_config_item_t *pInfo
)
{
  /*  See if the default value is needed for this item
   */
  if (    (pInfo->pItem == NULL)                      // Don't have the item yet
       && (pInfo->pCfgInfo->pfnGetDefault != NULL) )  // ... and the default function is assigned
  {
    policyman_item_t        *pItem;

    /*  Get the default value outside the critical section.
     */

    /* subsId is already created in policyman_cfgitem_init()
     * so use it and pass to default function
     */
    pItem = pInfo->pCfgInfo->pfnGetDefault(pInfo->subsId);

    if (pItem != NULL)
    {
      /*  Enter the critical section and check that the default value is still
       *  needed.  If so, increment the reference count for the item and add it.
       */
      policyman_enter_crit_sect(&policyman_item_crit_sect);

      if (pInfo->pItem == NULL)      
      {
        ref_cnt_obj_add_ref(pItem);
        pInfo->pItem = pItem;
      }

      policyman_leave_crit_sect(&policyman_item_crit_sect);

      /*  Release the reference on the object.  If it has been added to the item
       *  entry, the entry will have its own reference.  Otherwise, the item was
       *  added while we were retrieving it and this will release the item.
       */
      ref_cnt_obj_release(pItem);
    }
  }
}

/*-------- policyman_cfgitem_ensure_items --------*/
void policyman_cfgitem_ensure_items(
  void
  )
{
  size_t                  i;
  policyman_config_item_t *pInfo;

  POLICYMAN_MSG_HIGH_0("========== Ensure all database items");

  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    pInfo = &policyman_cfgitems[i];    
    policyman_cfgitem_ensure_item(pInfo);
  }

  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_EQUAL);
}


/*-------- policyman_cfgitem_ensure_device_config --------*/
void policyman_cfgitem_ensure_device_config(
  void
  )
{
  policyman_config_item_t *pInfo;

  POLICYMAN_MSG_HIGH_0("========== Ensure device config");

  pInfo = &policyman_cfgitems[0];
  policyman_cfgitem_ensure_item(pInfo);

  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_EQUAL);
}

/*-------- policyman_cfgitem_deinit --------*/
void
policyman_cfgitem_deinit(
  void
  )
{
  size_t            i;
  policyman_item_t  *pItem;

  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    /*  If the item is NULL move to the next.
     */
    if (policyman_cfgitems[i].pItem == NULL)
    {
      continue;
    }

    /*  Enter the critical section and remove the item from its entry.
     */
    policyman_enter_crit_sect(&policyman_item_crit_sect);

    pItem = policyman_cfgitems[i].pItem;
    policyman_cfgitems[i].pItem = NULL;

    policyman_leave_crit_sect(&policyman_item_crit_sect);

    /*  Release the entry's reference on the item.
     */
    REF_CNT_OBJ_RELEASE_IF(pItem);
  }
}


/*-------- policyman_cfgitem_new_item --------*/
policyman_item_t*
policyman_cfgitem_new_item(
  policyman_item_id_t    id,
  size_t                 itemSize,
  destructor_ptr         dtor
  )
{
  policyman_item_t  *pItem;

  if (id == POLICYMAN_ITEM_INVALID)
  {
    POLICYMAN_ERR_FATAL("Attempting to create policyman item with invalid ID", 0, 0, 0);
  }

  pItem = policyman_mem_alloc(itemSize);
  ref_cnt_obj_init(pItem, dtor);
  
  pItem->pCfgInfo = &policyman_cfg_info[id - 1];

  return pItem;
}



/*-------- policyman_cfgitem_find_item --------*/
/**
@brief  Find an item in the configuration item database.  Private function.

@param[in]  id               ID of the configuration item.

@return
  - Pointer to the configuration item if it is in the database.
  - NULL otherwise.
*/
static policyman_config_item_t*
policyman_cfgitem_find_item(
  policyman_item_id_t    id,
  sys_modem_as_id_e_type asubs_id
  )
{
  size_t  i;

  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    if (policyman_cfgitems[i].pCfgInfo->id == id)
    {
       /* Don't check asubs_id for DEVICE_CONFIGURATION
         */
      if(id == POLICYMAN_ITEM_DEVICE_CONFIGURATION)
      {        
        return &policyman_cfgitems[i];
      }

      /* Return the first empty item for matching id
         */
      if(policyman_cfgitems[i].pItem == NULL)
      {
        return &policyman_cfgitems[i];
      }

      /* Return the matching id and asubs_id
         */
      if(policyman_cfgitems[i].pItem->asubs_id == asubs_id)
      {
        return &policyman_cfgitems[i];
      }
       
    }
  }

  return NULL;
}

/*-------- policyman_items_equal --------*/
static boolean 
policyman_items_equal(
  policyman_item_t		*pItem1,
  policyman_item_t      *pItem2
)
{
  sys_modem_as_id_e_type asubs_id;
  policyman_item_id_t 	    id    = policyman_cfgitem_id_msim(pItem1,&asubs_id);
  policyman_config_item_t  *pInfo = policyman_cfgitem_find_item(id, asubs_id);

  if (NULL == pInfo)
  {
    POLICYMAN_MSG_ERROR_1("Unable to retrieve config item id %d for compare", id);
    return FALSE;
  }
  
  return pInfo->pCfgInfo->pfnItemsEqual(pItem1,pItem2);
}


/*-------- policyman_cfgitem_update_item --------*/
/**
@brief  Update (or add) an item in the configuration database.  Private function.

@param[in]  pNewItem    Pointer to the item with which to replace the old
                        configuration item.
@param[in]  asubs_id   Subscription Id for which old configuration item to be replaced.

@return
  TRUE if item was updated (i.e. the new item differs from the cached item).
*/
static boolean
policyman_cfgitem_update_item(
  policyman_item_t        *pNewItem
  )
{
  policyman_item_id_t       id = policyman_cfgitem_id(pNewItem);

  policyman_config_item_t  *pInfo = policyman_cfgitem_find_item(id, pNewItem->asubs_id);
  policyman_item_t         *pOldItem = NULL;
  boolean                  itemUpdated = FALSE;

  if (pInfo != NULL)
  {
    /* Add a reference on the new item for the array.
    */
    ref_cnt_obj_add_ref(pNewItem);

    policyman_enter_crit_sect(&policyman_item_crit_sect);

    pOldItem = pInfo->pItem;
    pInfo->pItem = pNewItem;

    policyman_leave_crit_sect(&policyman_item_crit_sect);

    itemUpdated = !policyman_items_equal(pNewItem, pOldItem);

    if (   itemUpdated
        && !policyman_is_initializing()
        && pInfo->pCfgInfo->pfnOnUpdate != NULL
       )
    {
      pInfo->pCfgInfo->pfnOnUpdate(pNewItem);
    }

    if (itemUpdated && pInfo->pCfgInfo->pfnDisplay != NULL)
    {
      pInfo->pCfgInfo->pfnDisplay(pNewItem);
    }

    REF_CNT_OBJ_RELEASE_IF(pOldItem);
  }

  return itemUpdated;
}


/*-------- policyman_cfgitem_release_msg_attach --------*/
void
policyman_cfgitem_release_msg_attach(
  msgr_hdr_s  *pMsg
  )
{
  msgr_attach_s *pAttach;
  size_t        i;

  /*  Release all reference-counted attachments
  */
  for (i = 0 ; i < msgr_get_num_attach(pMsg) ; ++i)
  {
    pAttach = msgr_get_attach(pMsg, i);
    if (pAttach->type == MSGR_ATTACH_REFCNT)
    {
      REF_CNT_OBJ_RELEASE_IF(pAttach->data.refcnt.refcnt_obj_ptr);
    }
  }
}


/*-------- policyman_item_collection_dtor --------*/
static void
policyman_item_collection_dtor(
  void  *pObj
  )
{
  policyman_item_collection_t *pCollection;
  size_t                      i;

  pCollection = (policyman_item_collection_t *) pObj;
  for (i = 0 ; i < pCollection->numItems ; ++i)
  {
    ref_cnt_obj_release((void *) pCollection->pItems[i]);
  }

  policyman_mem_free(pObj);
}


/*-------- policyman_cfgitem_notify_msg_dtor --------*/
void
policyman_cfgitem_notify_msg_dtor(
  void  *pObject
  )
{
  msgr_hdr_s    *pMsg = (msgr_hdr_s *) pObject;

  policyman_cfgitem_release_msg_attach(pMsg);

  /*  Free the message memory.
   */
  policyman_mem_free(pMsg);
}


/*-------- policyman_item_collection_new --------*/
static policyman_item_collection_t *
policyman_item_collection_new(
  size_t            nItems,
  policyman_item_t  **ppItems,
  boolean           addRef
  )
{
  policyman_item_collection_t *pCollection;
  size_t                      bufSize = nItems * sizeof(policyman_item_t *);
  size_t                      i;

  /*  Allocate a collection of the proper size and initialize it.
   */
  pCollection = (policyman_item_collection_t *)
                    policyman_mem_alloc(sizeof(policyman_item_collection_t) + bufSize);
  ref_cnt_obj_init(pCollection, policyman_item_collection_dtor);

  /*  Add a reference to all items going into the collection.
   */
  if (addRef)
  {
    for (i = 0 ; i < nItems ; ++i)
    {
      ref_cnt_obj_add_ref(ppItems[i]);
    }
  }

  /*  Add the items to the collection.
   */
  pCollection->numItems = nItems;
  memscpy((policyman_item_t *) pCollection->pItems, bufSize, ppItems, bufSize);

  policyman_ue_mode_normalize_collection(pCollection);

  return pCollection;
}


/*-------- policyman_cfgitem_notify_msg_msim_new --------*/
msgr_hdr_s *
policyman_cfgitem_notify_msg_msim_new(
  policyman_item_t  **ppItems,
  size_t            numItems,
  size_t            *pMsgSize
  )
{
  size_t                      msgSize = sizeof(msgr_hdr_s) + sizeof(msgr_attach_s);
  msgr_hdr_s                  *pMsg;
  msgr_attach_s               *pAttach;
  policyman_item_collection_t *pCollection;

  /*  Allocate a message with a single attachment (the collection).
   */
  pMsg = policyman_mem_alloc(msgSize);

  /*  Set up the message.
   */
  msgr_init_hdr_attach(
        pMsg,
        MSGR_POLICYMAN_CFG,
        POLICYMAN_CFG_UPDATE_MSIM_IND,
        MSGR_PRIORITY_HIGH,
        1
        );

  /*  Get the items into a collection and attach the collection to the message.
   *  Have the collection add references for each item.
   */
  pCollection = policyman_item_collection_new(numItems, ppItems, TRUE);
  pAttach = msgr_get_attach(pMsg, 0);
  if (pAttach != NULL)
  {
    pAttach->type = MSGR_ATTACH_REFCNT;
    pAttach->data.refcnt.refcnt_obj_ptr = pCollection;
  }

  if (pMsgSize != NULL)
  {
    *pMsgSize = msgSize;
  }

  return pMsg;
}



static  notification_hook   g_notificationHook;
static  void                *g_pNotificationContext;


/*-------- policyman_cfgitem_hook_notification --------*/
void
policyman_cfgitem_hook_notification(
  notification_hook hook,
  void              *pContext
  )
{
  g_notificationHook = hook;
  g_pNotificationContext = pContext;
}


/*-------- policyman_cfg_item_display --------*/
boolean
policyman_cfg_item_display(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  policyman_item_t  *pItem = *((policyman_item_t **) pElem);

  if (pItem == NULL || pItem->pCfgInfo == NULL)
  {
    POLICYMAN_MSG_ERROR_0("NULL pointer in policyman_cfg_item_display()");
    return TRUE;
  }

  pItem->pCfgInfo->pfnDisplay(pItem);
  return TRUE;
}


/*-------- policyman_cfgitem_display_all --------*/
void
policyman_cfgitem_display_all(
  void
  )
{
  size_t                  i;
  policyman_config_item_t *pCfgItem;
    
  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_HYPHEN);

  policyman_enter_crit_sect(&policyman_item_crit_sect);

  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    if(policyman_subs_id_is_configured(policyman_cfgitems[i].subsId))
    {
      pCfgItem = &policyman_cfgitems[i];
      if (pCfgItem->pItem != NULL)
      {
        policyman_cfg_item_display(&pCfgItem->pItem, NULL, NULL);
      }
    }
  }

  policyman_leave_crit_sect(&policyman_item_crit_sect);
}


/*-------- policyman_cfgitem_notify_clients --------*/
static void
policyman_cfgitem_notify_clients(
  policyman_set_t *pItemSet
  )
{
  size_t            nItems;
  policyman_item_t  **ppItems;
  size_t            msgSize;
  msgr_hdr_s        *pMsg;

  /*  Get the number of items to be udpated and a pointer to the buffer
   *  in the itemSet that holds those pointers.
   */
  nItems = policyman_set_num_elems(pItemSet);
  ppItems = (policyman_item_t **) policyman_set_get_buf_ptr(pItemSet);

  POLICYMAN_MSG_HIGH_1("********** Notifying clients of changes to %d items:", nItems);

  policyman_set_iterate(pItemSet, policyman_cfg_item_display, NULL, NULL);

  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_STAR);

  if (g_notificationHook != NULL)
  {
    g_notificationHook(g_pNotificationContext, nItems, ppItems);
  }

  /*  Send the MSIM message
   */
  pMsg = policyman_cfgitem_notify_msg_msim_new(ppItems, nItems, &msgSize);
  msgr_send(pMsg, msgSize);
  policyman_cfgitem_notify_msg_dtor(pMsg);
}



/*-------- update_item --------*/
static boolean
update_item(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  policyman_item_t        *pItem = *((policyman_item_t **) pElem);
  policyman_set_t         *pNotifySet = (policyman_set_t *) pData1;
  boolean                 itemUpdated;

  /* Don't update items for SUBS which is not active, but return TRUE
     so that we continue to update for other SUBS in set iterator
   */
  if(   !policyman_is_initializing()
     && !policyman_is_subs_active(pItem->asubs_id)
    )
  {
    POLICYMAN_MSG_HIGH_1("subs %d not active: not updating cfgitems", pItem->asubs_id);
    return TRUE;
  }

  itemUpdated = policyman_cfgitem_update_item(pItem);
  if (itemUpdated && pNotifySet != NULL)
  {
    policyman_set_add(pNotifySet, &pItem);
  }

  return TRUE;
}

/*-------- compare_item --------*/
static boolean
find_and_collect_changed_items(
  void  *pElem,
  void  *pData1,
  void  *pData2
  )
{
  policyman_item_t        *pNewItem    = *((policyman_item_t **) pElem);
  sys_modem_as_id_e_type  asubs_id;
  policyman_item_id_t     item_id      = policyman_cfgitem_id_msim(pNewItem,&asubs_id);
  policyman_config_item_t *pCfgItem    = policyman_cfgitem_find_item(item_id,asubs_id);
  policyman_item_t        *pActualItem = NULL;
  policyman_set_t         *pNotifySet  = (policyman_set_t *) pData1;
  boolean                 itemSame;

  if (NULL != pCfgItem)
  {
    pActualItem = pCfgItem->pItem;
  }
  else
  {
    POLICYMAN_MSG_ERROR_1("Unable to retrieve changed config item id %d", item_id);
    return FALSE;
  }

  /* Don't update items for SUBS which is not active
   */
  if(   !policyman_is_initializing()
     && !policyman_is_subs_active(asubs_id)
    )
  {
	  POLICYMAN_MSG_HIGH_1("compare_item: subs %d not active", pNewItem->asubs_id);
    return FALSE;
  }

  itemSame = policyman_items_equal(pNewItem, pActualItem);
  if (!itemSame )
  {
    policyman_set_add(pNotifySet, &pNewItem);
  }

  return TRUE;
}



/*-------- policyman_cfgitem_update_items --------*/
/*
This queues a command to update the items to the Policy Manager task thread for
later processing in order to decouple updating items and client notification
from the event that caused the policy change and update.
*/
boolean
policyman_cfgitem_update_items(
  policyman_set_t        *pItemSet,
  sys_modem_as_id_e_type asubs_id
  )
{
  policyman_set_t *pNotifySet = NULL;


  if (policyman_set_is_empty(pItemSet))
  {
    POLICYMAN_MSG_MED_0("policyman_cfgitem_update_items pItemSet is empty");
    return FALSE;
  }

  if (asubs_id == SYS_MODEM_AS_ID_NONE)
  {
    POLICYMAN_MSG_MED_0("policyman_cfgitem_update_items subs_id is invalid");
    return FALSE;
  }

  if (!policyman_is_initializing())
  {
    pNotifySet = policyman_itemset_new();
  }

  policyman_set_iterate(pItemSet, update_item, pNotifySet, NULL);  

  /* Notify all clients of the changes. Don't bother during initialization. */
  if (pNotifySet != NULL && !policyman_set_is_empty(pNotifySet))
  {
    /* Update EFS writing of updating policy items    
      */
    policyman_persist_cfgitems(pNotifySet);
    policyman_cfgitem_notify_clients(pNotifySet);
  }
  else
  {
    POLICYMAN_MSG_HIGH_0("No configuration items need updating");
  }

  REF_CNT_OBJ_RELEASE_IF(pNotifySet);

  return TRUE;
}


/*-------- policyman_cfgitem_id --------*/
policyman_item_id_t
policyman_cfgitem_id(
  policyman_item_t const  *pItem
  )
{
  return pItem->pCfgInfo->id;
}


/*-------- policyman_cfgitem_id_msim --------*/
policyman_item_id_t
policyman_cfgitem_id_msim(
  policyman_item_t const  *pItem,
  sys_modem_as_id_e_type  *pSubsId
  )
{
  *pSubsId = pItem->asubs_id;
  return policyman_cfgitem_id(pItem);
}


/*-------- policyman_cfgitem_get_items --------*/
size_t
policyman_cfgitem_get_items(
  policyman_item_id_t const *pItemIds,
  size_t                  numIds,
  policyman_item_t const  **ppItems
  )
{
  size_t                    i;
  size_t                    numItems = 0;
  policyman_config_item_t  *pItemInfo;
  policyman_item_t          *pItem;

  policyman_enter_crit_sect(&policyman_item_crit_sect);

  for (i = 0 ; i < numIds ; ++i)
  {
    POLICYMAN_MSG_HIGH_1("policyman_cfgitem_get_items requested for item id %d", pItemIds[i]);

    /* Default search for subscription 1
       Use MSIM API if items needed for other subscriptions
      */    
    pItemInfo = policyman_cfgitem_find_item(pItemIds[i], SYS_MODEM_AS_ID_1);
    if (pItemInfo != NULL)
    {
      pItem = pItemInfo->pItem;
      if (pItem != NULL)
      {
        ref_cnt_obj_add_ref(pItem);
        ppItems[numItems++] = pItem;
      }
    }
  }

  policyman_leave_crit_sect(&policyman_item_crit_sect);

  return numItems;
}


/*-------- policyman_cfgitem_get_item --------*/
policyman_item_t  const *
policyman_cfgitem_get_item(
  policyman_item_id_t     id,
  sys_modem_as_id_e_type  subs
  )
{
  size_t                  i;
  policyman_config_item_t *pCfg;
  policyman_item_t        *pItem = NULL;

  policyman_enter_crit_sect(&policyman_item_crit_sect);

  for (
        i = 0, pCfg = policyman_cfgitems ;
        i < ARR_SIZE(policyman_cfgitems) ;
        ++i, ++pCfg
      )
  {
    pItem = pCfg->pItem;
    if (
          pItem != NULL
       && pCfg->subsId == subs
       && pCfg->pCfgInfo != NULL
       && pCfg->pCfgInfo->id == id
       )
    {
      ref_cnt_obj_add_ref(pItem);
      break;
    }
  }

  policyman_leave_crit_sect(&policyman_item_crit_sect);

  return pItem;
}


/*-------- policyman_cfgitem_is_present --------*/
boolean
policyman_cfgitem_is_present(
  policyman_item_id_t    id,
  sys_modem_as_id_e_type subsId
  )
{
  boolean                 isPresent = FALSE;
  policyman_item_t const  *pItem;
 
  pItem = policyman_cfgitem_get_item(id, subsId);
  if (pItem != NULL)
  {
    ref_cnt_obj_release((void *) pItem);
    isPresent = TRUE;
  }

  return isPresent;
}


/*-------- policyman_cfgitem_id_in_request --------*/
/**
@brief  Return TRUE if the given ID was requested in a call to
        policyman_cfgitem_get_items_msim.
*/
static boolean
policyman_cfgitem_id_in_request(
  policyman_item_id_t const id,
  policyman_item_id_t const *pIds,
  size_t                    nIds
  )
{
  while (nIds-- != 0)
  {
    if (*pIds++ == id)
    {
      return TRUE;
    }
  }

  return FALSE;
}


/*-------- policyman_cfgitem_get_items_msim --------*/
policyman_status_t
policyman_cfgitem_get_items_msim(
  policyman_item_id_t const   *pIds,
  size_t                      nIds,
  policyman_item_collection_t **ppItems
  )
{
  size_t                    i;
  size_t                    numItems = 0;
  policyman_item_t          *items[ARR_SIZE(policyman_cfg_info) * POLICYMAN_NUM_SUBS];

  /*  Enter the critical section and collect ref-counted copies of any items
   *  requested into our local buffer.
   */
  policyman_enter_crit_sect(&policyman_item_crit_sect);
  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; ++i)
  {
    if ((policyman_cfgitem_id_in_request(policyman_cfgitems[i].pCfgInfo->id, pIds, nIds))
        && (policyman_subs_id_is_configured(policyman_cfgitems[i].subsId)))
    {
      items[numItems] = policyman_cfgitems[i].pItem;

      if (items[numItems] == NULL) { continue; }

      ref_cnt_obj_add_ref(items[numItems]);
      ++numItems;
    }
  }
  policyman_leave_crit_sect(&policyman_item_crit_sect);

  /*  Now, outside the critical section, allocate memory for the item collection
   *  and copy the items to it.  We've already added a reference to the items
   *  while in the critical section, so no need for the collection to do so.
   */
  *ppItems = policyman_item_collection_new(numItems, items, FALSE);

  return POLICYMAN_STATUS_SUCCESS;
}


/*-------- policyman_cfgitem_get_items_per_subs --------*/
size_t
policyman_cfgitem_get_items_per_subs(
  policyman_item_id_t const *pItemIds,
  size_t                  numIds,
  policyman_item_t const  **ppItems,
  sys_modem_as_id_e_type   subs_id
  )
{
  size_t                  i;
  size_t                  numItems = 0;
  policyman_config_item_t  *pItemInfo;
  policyman_item_t        *pItem;

  policyman_enter_crit_sect(&policyman_item_crit_sect);

  for (i = 0 ; i < numIds ; ++i)
  {
    POLICYMAN_MSG_HIGH_1("policyman_cfgitem_get_items_per_subs requested for item id %d", pItemIds[i]);

    /* Default search for subscription 1
       Use MSIM API if items needed for other subscriptions
      */    
    pItemInfo = policyman_cfgitem_find_item(pItemIds[i], subs_id);
    if (pItemInfo != NULL)
    {
      pItem = pItemInfo->pItem;
      if (pItem != NULL)
      {
        ref_cnt_obj_add_ref(pItem);
        ppItems[numItems++] = pItem;
      }
    }
  }

  policyman_leave_crit_sect(&policyman_item_crit_sect);

  return numItems;
}


/*-------- ids_are_equal --------*/
static boolean
ids_are_equal(
  void  *pElem1,
  void  *pElem2
  )
{
  policyman_item_t  *pItem1 = *((policyman_item_t **) pElem1);
  policyman_item_t  *pItem2 = *((policyman_item_t **) pElem2);
  
  return    policyman_cfgitem_id(pItem1) == policyman_cfgitem_id(pItem2)
         && pItem1->asubs_id == pItem2->asubs_id
         ;
}


/*-------- policyman_itemset_new --------*/
policyman_set_t *
policyman_itemset_new(
  void
  )
{
  return  policyman_set_refcnt_new(
              ARR_SIZE(policyman_cfg_info),
              ARR_SIZE(policyman_cfgitems),
              ids_are_equal
            );
}


/*-------- policyman_report_msg_free --------*/
void
policyman_report_msg_free(
  msgr_hdr_s  *pMsg
  )
{
  if (pMsg != NULL)
  {
    policyman_cfgitem_notify_msg_dtor(pMsg);
  }
}

/*-------- policyman_ensure_default_items --------*/
void policyman_ensure_default_items(
  sys_modem_as_id_e_type  asubs_id,
  policyman_set_t         *pItemSet
  )
{
  size_t i;
  policyman_config_item_t *pCfgItem;
  policyman_item_t        *pDefaultItem;

  POLICYMAN_MSG_HIGH_1("++++++++++ Getting default items for subs %d", asubs_id);

  /* Update items for this subs
    */
  for (i = 0 ; i < ARR_SIZE(policyman_cfgitems) ; i++)
  {
    pCfgItem = &policyman_cfgitems[i];
    if(   pCfgItem->pCfgInfo->id != POLICYMAN_ITEM_DEVICE_CONFIGURATION
       && pCfgItem->subsId == asubs_id
      )
    {
      pDefaultItem = pCfgItem->pCfgInfo->pfnGetDefault(asubs_id);
      if (pDefaultItem != NULL)
      {
        policyman_set_add(pItemSet, &pDefaultItem);
        REF_CNT_OBJ_RELEASE_IF(pDefaultItem);
      }
    }
  }

  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_PLUS);
}

/*-------- policyman_cfigitem_cmp_ids --------*/
static boolean
policyman_cfigitem_cmp_ids(
  void  *pData1,
  void  *pData2
  )
{
  policyman_item_t     *pItem1 = *((policyman_item_t **) pData1);
  policyman_item_cmp_t *pItem2 = (policyman_item_cmp_t *) pData2;

  return    policyman_cfgitem_id(pItem1) == pItem2->id
         && pItem1->asubs_id == pItem2->asubs_id
         ;
}

/*-------- policyman_cfgitem_find_item_in_itemset --------*/
void *
policyman_cfgitem_find_item_in_itemset(
  policyman_set_t       *pItemSet,
  policyman_item_id_t    id,
  sys_modem_as_id_e_type asubs_id
  )
{
  policyman_item_cmp_t item;
  item.id = id;
  item.asubs_id = asubs_id;
    
  return policyman_set_find( pItemSet, policyman_cfigitem_cmp_ids, (void *)&item );
}

static void
policyman_compute_changed_set(
  policyman_set_t       *pItemSet,
  policyman_set_t       *pChangeSet
)
{
  policyman_set_iterate(pItemSet, find_and_collect_changed_items, pChangeSet, NULL);
}

/*-------- policyman_cfgitem_get_change_msg_hdr --------
   checks if pItemSet has config items that differ from config items
   if found any such items, generates a ref cnted attach object
   and returns hdr to the same
*/

msgr_hdr_s *
policyman_cfgitem_get_change_msg_hdr(
  policyman_set_t  *pItemSet
)
{
  
  policyman_set_t    *pChangeSet = policyman_itemset_new();
  msgr_hdr_s          *pMsg = NULL;
  policyman_item_t   **ppItems;

  
  /* If itemset is not empty, generate the changed set */
  if(!policyman_set_is_empty(pItemSet))
  {
    policyman_compute_changed_set(pItemSet,pChangeSet);
  }

  /* If changeset is not empty, generate the changed set */
  if(!policyman_set_is_empty(pChangeSet))
  {
     ppItems = (policyman_item_t **) policyman_set_get_buf_ptr(pChangeSet);
     pMsg = policyman_cfgitem_notify_msg_msim_new(ppItems, policyman_set_num_elems(pChangeSet), NULL);
  }
  
  REF_CNT_OBJ_RELEASE_IF(pChangeSet);

  return pMsg;
}

/*-------- policyman_cfgitem_persist_item --------*/
boolean policyman_cfgitem_persist_item(
  char const    *pPath,
  void          *pData,
  size_t         dataSize
)
{
  policyman_efs_status_t status;
  /* the last argument is false as Policyman doesn't
   * want to persist the item if /policyman folder
   * is not already present 
   */
  status =  policyman_efs_put_file( pPath,
                                    pData,
                                    dataSize,
                      /*O_AUTODIR*/ FALSE );

  return (POLICYMAN_EFS_STATUS_SUCCESS == status);
  
}



