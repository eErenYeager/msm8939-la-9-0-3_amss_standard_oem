/**
  @file policyman_phone_events.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_phone_events.c#2 $
  $DateTime: 2015/06/26 02:43:52 $
  $Author: arkhanna $
*/

#include "policyman_dbg.h"
#include "policyman_call_events.h"
#include "policyman_phone_events.h"
#include "policyman_set.h"
#include "policyman_util.h"
#include "policyman_task.h"
#include "policyman_state.h"
#include "policyman_xml.h"
#include "policyman_rat_capability.h"
#include "policyman_efs.h"
#include "policyman_timer.h"
#include "policyman_cm.h"
#include "policyman_cfgitem.h"
#include "policyman_plmn.h"
#include "policyman_device_config.h"

#include "cm.h"
#include "cm_v.h"
#include "sys.h"
#include "cm_dualsim.h"
#include "modem_mem.h"
#include <stringl/stringl.h>

#define PMNV_EFS_ID_TDS_BANDPREF "/nv/item_files/modem/mmode/tds_bandpref"

/*=============================================================================
 Data Structure for NV item read
==============================================================================*/
nv_item_type   nv_item;


/*=============================================================================
 Data Structure for CM PH Info
==============================================================================*/
struct policyman_cmph_info_s
{
  sys_modem_as_id_e_type    asubs_id;         // Subscription ID to which these preferences should apply.
  cm_mode_pref_e_type       mode_pref;        // Indicates the current mode preference.
  cm_srv_domain_pref_e_type srv_domain_pref;  // The service domain preferred by the client. (GSM/WCDMA/TDS only).
  sys_oprt_mode_e_type      oprt_mode;        // current operating mode
  cm_network_sel_mode_pref_e_type   network_sel_mode_pref;  // current network selection mode preference (AUTOMATIC/MANUAL)
  sys_plmn_id_s_type        user_plmn;       // user selected PLMN if network_sel_mode_pref = MANUAL
  cm_band_pref_e_type       gw_bands;         // user preference for GW bands
  cm_band_pref_e_type       lte_bands;        // user preference for LTE bands
  cm_band_pref_e_type       tds_bands;        // user preference for TDS bands
};

/*=============================================================================
 Data Structure for event registration
==============================================================================*/
 struct policyman_event_s
 {
   cm_client_id_type  cm_clnt_id;
 };
 
 struct policyman_cmph_event_s
 {
   cm_ph_event_e_type beg;
   cm_ph_event_e_type end;
 };
 
 struct policyman_cmss_event_s
 {
   cm_ss_event_e_type beg;
   cm_ss_event_e_type end;
 };
 
 struct policyman_cmcall_event_s
 {
   cm_call_event_e_type beg;
   cm_call_event_e_type end;
 };

 struct policyman_event_s policyman_event;

/*=============================================================================
  Phone Event callback definition.
=============================================================================*/
struct policyman_cmph_evt_cb_s
{
  POLICYMAN_CMD_HDR;
  cm_ph_event_e_type evt;
  cm_ph_info_s_type info;
};

/*=============================================================================
  Function Protoypes
=============================================================================*/
static cm_mode_pref_e_type policyman_map_mode_pref_nv2cm( nv_mode_enum_type  nv_mode );
static cm_srv_domain_pref_e_type  policyman_map_srv_domain_pref_nv2cm(nv_srv_domain_pref_enum_type   nv_mode);

/*------------------------------------------------------------------------
  FUNCTION POLICYMAN_GET_CM_EVENT_CLIENT_ID()

  DESCRIPTION
    Function to get CM client id that registered for events 
--------------------------------------------------------------------------*/
cm_client_id_type policyman_get_cm_event_client_id(void);


/*-------- policyman_ph_get_cm_ph_state --------*/
policyman_cmph_info_t * policyman_ph_get_cm_ph_state(
  sys_modem_as_id_e_type asubs_id
  )
{
  return policyman_state_get_subs_state(asubs_id)->pCmPhInfo;
}

/*=============================================================================
  APIs for CM Phone Event information
=============================================================================*/
boolean policyman_cmph_init
(
  policyman_cmph_info_t **ppInfo,
  sys_modem_as_id_e_type asubs_id
)
{
  nv_stat_enum_type         nv_status;
  policyman_efs_status_t    status;
  cm_band_pref_e_type       *pBand = NULL;
  size_t                    bandSize;


  *ppInfo = (policyman_cmph_info_t *) policyman_mem_alloc(sizeof(policyman_cmph_info_t));

  /* initialize PM client id to 0 for CM Events
   */
  memset(&policyman_event, 0x00, sizeof(struct policyman_event_s));

  memset(&nv_item, 0x00, sizeof(nv_item_type));

  /* read NV item for NV #10 to get mode_pref
   */
  nv_status = policyman_efs_get_nv_item(NV_PREF_MODE_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->mode_pref = policyman_map_mode_pref_nv2cm(nv_item.pref_mode.mode); 
  }
  else 
  { 
    (*ppInfo)->mode_pref = CM_MODE_PREF_GSM_ONLY; 
  }
  POLICYMAN_MSG_HIGH_1("Got CM mode_pref %d", (*ppInfo)->mode_pref);
  
  /* read NV item for NV #850 to get srv_domain_pref
   */
  nv_status = policyman_efs_get_nv_item(NV_SERVICE_DOMAIN_PREF_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->srv_domain_pref = policyman_map_srv_domain_pref_nv2cm(nv_item.service_domain_pref.srv_domain);
  }
  else
  {
    (*ppInfo)->srv_domain_pref = CM_SRV_DOMAIN_PREF_CS_ONLY; 
  }
  POLICYMAN_MSG_HIGH_1("Got CM service_domain_pref %d", (*ppInfo)->srv_domain_pref);

  /* read NV item 441 to get first 16 bits of GW Band pref
   */
  nv_status = policyman_efs_get_nv_item(NV_BAND_PREF_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->gw_bands |= (cm_band_pref_e_type)(nv_item.band_pref.band);
  }
  else
  {
    (*ppInfo)->gw_bands = CM_BAND_PREF_ANY; 
  }

  /* read NV item 946 to get 16-31 bits of GW Band pref
   */
  nv_status = policyman_efs_get_nv_item(NV_BAND_PREF_16_31_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->gw_bands |= ((cm_band_pref_e_type)(nv_item.band_pref.band)<<16);
  }
  else
  {
    (*ppInfo)->gw_bands = CM_BAND_PREF_ANY; 
  }

  /* read NV item 962 to get 32-63 bits of GW Band pref
   */
  nv_status = policyman_efs_get_nv_item(NV_BAND_PREF_32_63_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->gw_bands |= ((cm_band_pref_e_type)(nv_item.band_pref_32_63.band)<<32);
  }
  else
  {
    (*ppInfo)->gw_bands = CM_BAND_PREF_ANY; 
  }

  /* read NV item 6828 to get LTE Band pref
   */
  nv_status = policyman_efs_get_nv_item(NV_LTE_BC_CONFIG_I, &nv_item, asubs_id);

  if(nv_status != NV_BUSY_S)
  {
    (*ppInfo)->lte_bands |= (cm_band_pref_e_type)(nv_item.lte_bc_config.lte_bc_config);
  }
  else
  {
    (*ppInfo)->lte_bands = CM_BAND_PREF_LTE_ANY; 
  }

  /* read TDSCDMA RF Band preference
    */
  status = policyman_efs_get_file(PMNV_EFS_ID_TDS_BANDPREF , (void **) &pBand, &bandSize);
  
  /*  If we were able to read the NV and if the value is in the proper range,
   *  set the mode to that value.  Otherwise just return NORMAL mode.
   */
  if (    status == POLICYMAN_EFS_STATUS_SUCCESS
      &&  bandSize == sizeof(cm_band_pref_e_type)
      &&  pBand != NULL
     )
  {
    (*ppInfo)->tds_bands = *pBand;
    POLICYMAN_MSG_HIGH_2("subs %d: read TDS band %lld from EFS success", asubs_id, (*ppInfo)->tds_bands);
  }
  else
  {
    (*ppInfo)->tds_bands = CM_BAND_PREF_TDS_ANY;
    POLICYMAN_MSG_HIGH_1("subs %d: unable to read TDS band from EFS; setting to default CM_BAND_PREF_TDS_ANY", asubs_id);
  }    

  /* update Ph state with asubs id and set the precondition
   */
 (*ppInfo)->asubs_id = asubs_id;
 (*ppInfo)->oprt_mode = SYS_OPRT_MODE_PWROFF;

  POLICYMAN_MEM_FREE_IF(pBand);
  return TRUE;
}


void
policyman_cmph_deinit(
  policyman_cmph_info_t *pInfo
  )
{
  POLICYMAN_MEM_FREE_IF(pInfo);
}


/*===========================================================================
  FUNCTION POLICYMAN_GET_PH_INFO()
 
  DESCRIPTION
    GET changed CM Phone Info from event into local policyman_ph_info_t structure
 
  PARAMETERS
    ph_info : Policyman local structure to retain changed Phone info
    ph_evt : Incoming CM Phone EVENT (~ 5k in size)
  
  RETURN VALUE
    None
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
void policyman_get_ph_info
(
  policyman_cmph_info_t *cm_ph_info, 
  struct policyman_cmph_evt_cb_s *cm_ph_evt
)
{
  if(cm_ph_info == NULL || cm_ph_evt == NULL)
  {
    POLICYMAN_MSG_ERROR_2("get_ph_info NULL pointer, data not copied - cm ph info %x, cm ph evt %x", cm_ph_info, cm_ph_evt);
    return;
  }

  if (cm_ph_info->mode_pref != cm_ph_evt->info.mode_pref)
  {
    POLICYMAN_MSG_ERROR_2("mode_pref updated from %d to %d in PH event rather than through policyman_report_preference_change()",
                         cm_ph_info->mode_pref, cm_ph_evt->info.mode_pref);
  }

  if (cm_ph_info->srv_domain_pref != cm_ph_evt->info.srv_domain_pref)
  {
    POLICYMAN_MSG_ERROR_2("srv_domain updated from %d to %d in PH event rather than through policyman_report_preference_change()",
                         cm_ph_info->srv_domain_pref, cm_ph_evt->info.srv_domain_pref);
  }

  cm_ph_info->asubs_id = cm_ph_evt->info.asubs_id;
    cm_ph_info->oprt_mode = cm_ph_evt->info.oprt_mode;

  POLICYMAN_MSG_HIGH_3("mode_pref %d, srv_domain %d, oprt_mode %d",
                     cm_ph_info->mode_pref,
                     cm_ph_info->srv_domain_pref,
                     cm_ph_info->oprt_mode
                     );
}


/*===========================================================================
  FUNCTION POLICYMAN_EXECUTE_PROCESS_CM_PHONE_EVT()
 
  DESCRIPTION
    Execute CM SS Set update update and policy check 
 
  RETURN VALUE
    None
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
void policyman_execute_process_cmph_evt
(
  policyman_cmd_t *pCmd
)
{
  struct policyman_cmph_evt_cb_s *ph_evt = (struct policyman_cmph_evt_cb_s*) pCmd;
  policyman_cmph_info_t           ph_info;

  if(ph_evt)
  {
    POLICYMAN_MSG_HIGH_1("process cm ph event %d", ph_evt->evt);

    switch(ph_evt->evt)
    {
      case CM_PH_EVENT_SYS_SEL_PREF:
      case CM_PH_EVENT_INFO:
        policyman_get_ph_info(&ph_info, ph_evt);
        policyman_state_update_ph_info(&ph_info, ph_evt->info.asubs_id);
        break;

      case CM_PH_EVENT_OPRT_MODE:
        policyman_state_update_ph_oprt_mode(ph_evt->info.oprt_mode);
        break;

      default:
        break;
    }
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_CMPH_EVENT_CB()

  DESCRIPTION
    Handle CM Phone event callback

  PARAMETERS
    evt     : CM Phone EVENT type
    p_info : actual payload of CM Phone EVENT

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void policyman_cmph_event_cb
( 
  cm_ph_event_e_type evt, 
  const cm_ph_info_s_type *p_info
)
{
  struct policyman_cmph_evt_cb_s *pCmd = NULL;

  POLICYMAN_MSG_HIGH_1("received cm ph evt %d", evt);

  pCmd = (struct policyman_cmph_evt_cb_s *) policyman_cmd_new( sizeof(struct policyman_cmph_evt_cb_s), 
                                                               policyman_execute_process_cmph_evt, 
                                                               NULL,
                                                               NULL                               // No policy associated with CM Phone Event
                                                             );

  pCmd->evt    = evt;
  pCmd->info = *p_info;

  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
  ref_cnt_obj_release(pCmd);
}


void
policyman_cmph_update_state(
  policyman_cmph_info_t *pCurInfo,
  policyman_cmph_info_t *pNewInfo
  )
{
  /*  If anything has changed, save the new state and handle a state update.
   */
  if (
        pNewInfo->mode_pref       != pCurInfo->mode_pref
     || pNewInfo->srv_domain_pref != pCurInfo->srv_domain_pref
     )
  {
    *pCurInfo = *pNewInfo;
    policyman_state_handle_update(pCurInfo->asubs_id);
  }
}



/*-------- policyman_ph_get_mode_pref --------*/
cm_mode_pref_e_type
policyman_ph_get_mode_pref(
  sys_modem_as_id_e_type  asubs_id
  )
{
  policyman_cmph_info_t *pPhInfo = policyman_ph_get_cm_ph_state(asubs_id);

  return (pPhInfo != NULL)? pPhInfo->mode_pref : CM_MODE_PREF_NONE;
}


/*-------- policyman_ph_get_srv_domain --------*/
cm_srv_domain_pref_e_type
policyman_ph_get_srv_domain(
  sys_modem_as_id_e_type  asubs_id
  )
{
  policyman_cmph_info_t *pPhInfo = policyman_ph_get_cm_ph_state(asubs_id);

  return (pPhInfo != NULL)? pPhInfo->srv_domain_pref: CM_SRV_DOMAIN_PREF_NONE;
}


/*-------- policyman_ph_get_band_pref --------*/
void policyman_ph_get_band_pref(
  sys_modem_as_id_e_type  asubs_id,
  cm_band_pref_e_type     *gw_bands,
  cm_band_pref_e_type     *lte_bands,
  cm_band_pref_e_type     *tds_bands
)
{
  policyman_cmph_info_t *pPhInfo = policyman_ph_get_cm_ph_state(asubs_id);
 
  if(pPhInfo)
  {
    *gw_bands = pPhInfo->gw_bands;
    *lte_bands = pPhInfo->lte_bands;
    *tds_bands = pPhInfo->tds_bands;
  }
}

sd_ss_mode_pref_e_type
cmph_map_cm_mode_pref_to_sd_mode_pref(
  cm_mode_pref_e_type mode_pref
);


/*-------- policyman_is_subs_multimode --------*/
static boolean 
policyman_is_subs_multimode(
  sys_modem_as_id_e_type  asubs_id
)
{
  cm_mode_pref_e_type         mode_pref = policyman_ph_get_mode_pref(asubs_id);
  sd_ss_mode_pref_e_type      rat_mask = cmph_map_cm_mode_pref_to_sd_mode_pref(mode_pref);

  /* A subs is multimode if it has non GSM mode_pref
   */
  rat_mask &= ~SD_SS_MODE_PREF_GSM;

  return rat_mask != SD_SS_MODE_PREF_NONE; 
}

/*-------- policyman_determine_multimode_asubs_id --------*/
boolean 
policyman_determine_multimode_asubs_id(
 void
 )
{
  size_t i;
  size_t                      nSim = 0;
  sys_modem_as_id_e_type      mmodSubs = policyman_get_current_multimode_subs();

  /* For multiple SUBSCRIPTIONS, evaluate multimode subs after reading the phone state
       if all SUBS evaluate to FALSE, retain the old subs as Multimode SUBS
   */

  (void)policyman_get_current_num_sim(&nSim);

  /* Based on modepref we should calculate multimode sub in policyman init phase*/ 
  for (i = SYS_MODEM_AS_ID_1 ; i < nSim ; i++)
  {
    /* Check if subscription has multimode settings
      */
    if ((policyman_is_subs_active(i) || policyman_is_initializing())
      && policyman_is_subs_multimode(i)
      )
    {
      policyman_subs_set_multimode_subs(i);
      break;
    }
  } 

  return  mmodSubs != policyman_get_current_multimode_subs();
}

/*-------- policyman_init_multimode_subs_id --------*/
void 
policyman_init_multimode_subs_id(
  void
)
{
  size_t                  i;
  size_t                  nSim = 0;
  boolean                 subsChanged;

  (void)policyman_get_current_num_sim(&nSim);

  /*
   In case of single sim, no need to check multimode on the basis of mode pref
  */	
  
  if (nSim < 2)
  {  
    POLICYMAN_MSG_HIGH_1("nSim = %d, not determining multimode SUBS", nSim);
  }
  else
  {
    subsChanged = policyman_determine_multimode_asubs_id();  
    POLICYMAN_MSG_HIGH_2("Init mmode Subs is %d and mmode subs changed %d ", policyman_get_current_multimode_subs(), subsChanged);
  }
}


/*------------------------------------------------------------------------
  FUNCTION POLICYMAN_REGISTER_FOR_CM_EVENTS()

  DESCRIPTION
    With this function, Policy Manager registers with CM events as a client 
--------------------------------------------------------------------------*/
void policyman_register_for_cm_events(void)
{
  cm_client_status_e_type ret_val = CM_CLIENT_ERR_CLIENT_ID;
  int                     i = 0;
  static boolean registered_for_events;

  const struct policyman_cmph_event_s ph_event_list[] = {
                                                          { CM_PH_EVENT_INFO, CM_PH_EVENT_INFO },
                                                          { CM_PH_EVENT_OPRT_MODE, CM_PH_EVENT_OPRT_MODE }
                                                      };

  const struct policyman_cmss_event_s ss_event_list[] = { 
                                                          { CM_SS_EVENT_SRV_CHANGED, CM_SS_EVENT_INFO },
                                                          { CM_SS_EVENT_REG_REJECT, CM_SS_EVENT_REG_REJECT }

                                                        };

  const struct policyman_cmcall_event_s call_event_list[] = {
                                                              { CM_CALL_EVENT_ORIG, CM_CALL_EVENT_ORIG },
                                                              { CM_CALL_EVENT_INCOM, CM_CALL_EVENT_INCOM },
                                                              { CM_CALL_EVENT_CONNECT, CM_CALL_EVENT_CONNECT },
                                                              { CM_CALL_EVENT_END, CM_CALL_EVENT_END }
                                                            };


  if(!registered_for_events)
  {

    ret_val = cm_client_init( CM_CLIENT_TYPE_POLICY_MANAGER, &policyman_event.cm_clnt_id);
    if ( ret_val != CM_CLIENT_OK ) 
    {
      goto Done;
    }

    /* register for CM Phone Events
      */
    for ( i=0; i<ARR_SIZE(ph_event_list); i++ )
    {
      ret_val = cm_client_ph_reg( policyman_event.cm_clnt_id,
                                  policyman_cmph_event_cb,
                                  CM_CLIENT_EVENT_REG,
                                  ph_event_list[i].beg,
                                  ph_event_list[i].end,
                                  NULL 
                                );

      if ( ret_val != CM_CLIENT_OK )
      {
        break;
      }
   }

    /* register for CM Serving System Events
      */
    for ( i=0; i<ARR_SIZE(ss_event_list); i++ )
    {
      ret_val = cm_mm_client_ss_reg_msim( policyman_event.cm_clnt_id,
                                          policyman_msim_cmss_event_cb,
                                          CM_CLIENT_EVENT_REG,
                                          ss_event_list[i].beg,
                                          ss_event_list[i].end,
                                          NULL,
                                          SYS_MODEM_AS_ID_1_MASK|SYS_MODEM_AS_ID_2_MASK
                                        );

      if ( ret_val != CM_CLIENT_OK )
      {
        break;
      }
    }

    /* register for CM Call Events
      */
    for ( i=0; i<ARR_SIZE(call_event_list); i++ )
    {
      ret_val = cm_mm_client_call_reg( policyman_event.cm_clnt_id,
                                       policyman_cmcall_event_cb,
                                       CM_CLIENT_EVENT_REG,
                                       call_event_list[i].beg,
                                       call_event_list[i].end,
                                       NULL
                                     );

    if ( ret_val != CM_CLIENT_OK ) 
    {
         break;
    }
  }

  registered_for_events = TRUE;
  POLICYMAN_MSG_HIGH_0("PM registered for CM Events");
  return;

Done:
    /* release clients in case of an error
      */
    (void)cm_client_release( policyman_event.cm_clnt_id );
    POLICYMAN_MSG_ERROR_1("PM failed to register to CM Events with error cause %d", ret_val);
  }
}

/*=============================================================================
  Phone-related conditions
=============================================================================*/


/*-----------------------------------------------------------------------------
  User domain pref
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type    subsId;
  cm_srv_domain_pref_e_type domain_pref;
} policyman_domain_pref_condition_t;



/*-------- policyman_str_to_domain --------*/
cm_srv_domain_pref_e_type
policyman_str_to_domain(
  char const  *pStr
  )
{
  typedef struct
  {
    const char                *pStr;
    cm_srv_domain_pref_e_type domain;
  } domain_map;

  cm_srv_domain_pref_e_type domain = CM_SRV_DOMAIN_PREF_NONE;

  static domain_map map[] =
  {
    {"CS",    CM_SRV_DOMAIN_PREF_CS_ONLY},
    {"PS",    CM_SRV_DOMAIN_PREF_PS_ONLY},
    {"CSPS",  CM_SRV_DOMAIN_PREF_CS_PS},
    {"ANY",   CM_SRV_DOMAIN_PREF_ANY},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      domain = map[i].domain;
      break;
    }
  }

  if (domain == CM_SRV_DOMAIN_PREF_NONE)
  {
    POLICYMAN_UT_MSG("Invalid value for <user_domain_pref>.  Valid values are:", 0, 0, 0);
    for (i = 0 ; i < ARR_SIZE(map) ; ++i)
    {
      POLICYMAN_UT_MSG("    %s", map[i].pStr, 0, 0);
    }
  }

  return domain;
}

/*-------- policyman_condition_ph_domain_pref_evaluate --------*/
static boolean
policyman_condition_ph_domain_pref_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_domain_pref_condition_t const *pCond = (policyman_domain_pref_condition_t *) pCondition;
  policyman_cmph_info_t const             *pPhInfo = policyman_ph_get_cm_ph_state(pCond->subsId);

  boolean                                 result;

  result =   pCond->domain_pref == CM_SRV_DOMAIN_PREF_ANY
          || pCond->domain_pref == pPhInfo->srv_domain_pref
          ;

  POLICYMAN_MSG_HIGH_2("condition <user_domain_pref> with srv_domain %d returns %d", pPhInfo->srv_domain_pref, result);
  return result;
}


/*-------- policyman_condition_ph_domain_pref_new --------*/
mre_status_t
policyman_condition_ph_domain_pref_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                      status = MRE_STATUS_ERR_MALFORMED_XML;
  cm_srv_domain_pref_e_type         domainPref;
  policyman_domain_pref_condition_t *pCondition;
  char const                        *pStr;
  sys_modem_as_id_e_type            subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<user_domain_pref>: must specify a domain preference", 0, 0, 0);
    goto Done;
  }

  domainPref= policyman_str_to_domain(pStr);
  if (domainPref == CM_SRV_DOMAIN_PREF_NONE)
  {
    POLICYMAN_UT_MSG("<user_domain_pref>: invalid domain preferences: %s", pStr, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_domain_pref_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_domain_pref_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_ph_domain_pref_evaluate;
  pCondition->domain_pref = domainPref;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;
  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}


/*-----------------------------------------------------------------------------
  User mode pref
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  uint32  mask;
  sys_modem_as_id_e_type asubs_id;  
} policyman_mode_pref_condition_t;


sd_ss_mode_pref_e_type
cmph_map_cm_mode_pref_to_sd_mode_pref(
  cm_mode_pref_e_type mode_pref
);


static boolean
policyman_condition_ph_mode_pref_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_mode_pref_condition_t const *pCond = (policyman_mode_pref_condition_t *) pCondition;
  policyman_cmph_info_t const           *pPhInfo = policyman_ph_get_cm_ph_state(pCond->asubs_id);
  uint32                                mask;
  boolean                               result;

  mask = (uint32) cmph_map_cm_mode_pref_to_sd_mode_pref(pPhInfo->mode_pref);

  result = ((mask & pCond->mask) == pCond->mask);

  POLICYMAN_MSG_HIGH_2("condition <user_mode_pref_contains> with mask %d returns %d", mask, result);
  return result;
}


mre_status_t
policyman_condition_ph_rat_pref_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                    status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_mode_pref_condition_t *pCondition;
  char const                      *pStr;
  uint32                          mask = 0;
  sys_modem_as_id_e_type          subs;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<user_mode_pref_contains>: must specify RATs", 0, 0, 0);
    goto Done;
  }

  if (!policyman_rat_config_parse_rats(pStr, &mask))
  {
    POLICYMAN_UT_MSG("<user_mode_pref_contains>: invalid RAT", 0, 0, 0);
    goto Done;
  }

  pCondition = (policyman_mode_pref_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_mode_pref_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_ph_mode_pref_evaluate;
  pCondition->mask = mask;
  pCondition->asubs_id = subs;

  *ppCondition = (policyman_condition_t *) pCondition;
  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}

/*-----------------------------------------------------------------------------
  Operating mode pref
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type subsId;
  sys_oprt_mode_e_type oprt_mode;
} policyman_operating_mode_condition_t;


/*-------- policyman_str_to_operating_mode --------*/
sys_oprt_mode_e_type
policyman_str_to_operating_mode(
  char const  *pStr
  )
{
  typedef struct
  {
    const char                *pStr;
    sys_oprt_mode_e_type      oprt_mode;
  } operating_mode_map;

  sys_oprt_mode_e_type oprt_mode = SYS_OPRT_MODE_NONE;

  static operating_mode_map map[] =
  {
    {"SLEEP",   SYS_OPRT_MODE_LPM},
    {"ONLINE",  SYS_OPRT_MODE_ONLINE},
    {"OFFLINE", SYS_OPRT_MODE_OFFLINE},
    {"RESET",   SYS_OPRT_MODE_RESET},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      oprt_mode = map[i].oprt_mode;
      break;
    }
  }

  if (oprt_mode == SYS_OPRT_MODE_NONE)
  {
    POLICYMAN_UT_MSG("Invalid value for <phone_operating_mode>: %s.  Valid values are:", token, 0, 0);
    for (i = 0 ; i < ARR_SIZE(map) ; ++i)
    {
      POLICYMAN_UT_MSG("    %s", map[i].pStr, 0, 0);
    }
  }

  return oprt_mode;
}



/*-------- policyman_condition_ph_operating_mode_evaluate --------*/
static boolean
policyman_condition_ph_operating_mode_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_operating_mode_condition_t const *pCond = (policyman_operating_mode_condition_t *) pCondition;
  policyman_cmph_info_t const                *pPhInfo = policyman_ph_get_cm_ph_state(pCond->subsId);
  boolean                                     result;

  result = pCond->oprt_mode == pPhInfo->oprt_mode;

  POLICYMAN_MSG_HIGH_1("condition <phone_operating_mode> returns %d", result);
  return result;
}


/*-------- policyman_condition_ph_operating_mode_new --------*/
mre_status_t
policyman_condition_ph_operating_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                          status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_operating_mode_condition_t *pCondition;
  char const                            *pStr;
  sys_oprt_mode_e_type                  mode;
  sys_modem_as_id_e_type                subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<phone_operating_mode>: must specify an operating mode", 0, 0, 0);
    goto Done;
  }

  mode= policyman_str_to_operating_mode(pStr);
  if (mode == SYS_OPRT_MODE_NONE)
  {
    POLICYMAN_UT_MSG("<phone_operating_mode>: invalid operating mode", 0, 0, 0);
    goto Done;
  }

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_operating_mode_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_operating_mode_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_ph_operating_mode_evaluate;
  pCondition->oprt_mode = mode;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;

  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}

/*-----------------------------------------------------------------------------
  User MCC preference
-----------------------------------------------------------------------------*/

/*-------- policyman_condition_user_mcc_evaluate --------*/
static boolean
policyman_condition_user_mcc_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  sys_modem_as_id_e_type          asubs_id = policyman_get_current_multimode_subs();
  policyman_cmph_info_t const     *pPhInfo = policyman_ph_get_cm_ph_state(asubs_id);

  policyman_mcc_condition_t       *pCond = (policyman_mcc_condition_t *) pCondition;
  uint32                          userMcc = 0;
  boolean                         result = FALSE;

  /*  If we don't have a set but *do* have a set name, try to find the named set.
   */
  if (pCond->pMccSet == NULL && pCond->pSetName != NULL)
  {
    pCond->pMccSet = (policyman_set_t *) mre_named_object_find(
                            pCond->pSetName,
                            POLICYMAN_NAMED_MCC_SET,
                            pCondition->pPolicy
                          );
    POLICYMAN_MEM_FREE_IF(pCond->pSetName);

    if (NULL != pCond->pMccSet)
    {
      ref_cnt_obj_add_ref(pCond->pMccSet);
    }
  }

  /*  Without a set, we can't work.
   */
  if (pCond->pMccSet == NULL)             
  {
    POLICYMAN_MSG_ERROR_0("named PLMN set not found");
    goto Done;
  }

  /*  Check to see if some stack's MCC is in the list.
   */
  userMcc = policyman_plmn_get_mcc((sys_plmn_id_s_type *)&pPhInfo->user_plmn);
  
  if (policyman_set_contains(pCond->pMccSet, &userMcc))
  {
    POLICYMAN_MSG_HIGH_1("User selected MCC %d is in the MCC set", userMcc);
    result = TRUE;
    goto Done;
  }
  else
  {
    POLICYMAN_MSG_HIGH_1("User selected MCC %d not found in set", userMcc);
  }
  
Done:
  POLICYMAN_MSG_HIGH_2("condition <serving_mcc_in> with user MCC %d returns %d", userMcc, result);  
  return result;
}

/*-------- policyman_condition_ph_user_mcc_new --------*/
mre_status_t
policyman_condition_ph_user_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  return  policyman_plmn_mcc_new( pElem, 
                                  pPolicy, 
                                  ppCondition, 
                                  (pfn_evaluate_t) policyman_condition_user_mcc_evaluate
                                );

}

/*-----------------------------------------------------------------------------
  Network Selection mode pref
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type            subsId;
  cm_network_sel_mode_pref_e_type user_selection_mode;
} policyman_user_selection_mode_condition_t;


/*-------- policyman_str_to_user_selection_mode --------*/
cm_network_sel_mode_pref_e_type
policyman_str_to_user_selection_mode(
  char const  *pStr
  )
{
  typedef struct
  {
    const char                           *pStr;
    cm_network_sel_mode_pref_e_type      user_selection_mode;
  } operating_mode_map;

  cm_network_sel_mode_pref_e_type user_selection_mode = CM_NETWORK_SEL_MODE_PREF_NONE;

  static operating_mode_map map[] =
  {
    {"MANUAL",     CM_NETWORK_SEL_MODE_PREF_MANUAL},
    {"AUTOMATIC",  CM_NETWORK_SEL_MODE_PREF_AUTOMATIC},
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pStr, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      user_selection_mode = map[i].user_selection_mode;
      break;
    }
  }

  if (user_selection_mode == CM_NETWORK_SEL_MODE_PREF_NONE)
  {
    POLICYMAN_UT_MSG("Invalid value for <phone_user_selection_mode>: %s.  Valid values are:", token, 0, 0);
    for (i = 0 ; i < ARR_SIZE(map) ; ++i)
    {
      POLICYMAN_UT_MSG("    %s", map[i].pStr, 0, 0);
    }
  }

  return user_selection_mode;
}



/*-------- policyman_condition_ph_network_selection_mode_evaluate --------*/
static boolean
policyman_condition_ph_network_selection_mode_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_user_selection_mode_condition_t const *pCond = (policyman_user_selection_mode_condition_t *) pCondition;
  policyman_cmph_info_t const                     *pPhInfo = policyman_ph_get_cm_ph_state(pCond->subsId);
  boolean                                         result;

  result = pCond->user_selection_mode == pPhInfo->network_sel_mode_pref;

  POLICYMAN_MSG_HIGH_2("condition <phone_user_selection_mode> with user_sel %d returns %d", 
                      pPhInfo->network_sel_mode_pref, result);
  return result;
  }

/*-------- policyman_condition_ph_network_selection_mode_new --------*/
mre_status_t
policyman_condition_ph_network_selection_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                              status = MRE_STATUS_ERR_MALFORMED_XML;
  cm_network_sel_mode_pref_e_type           user_selection_mode;
  policyman_user_selection_mode_condition_t *pCondition;
  char const                                *pStr;
  sys_modem_as_id_e_type                    subs;

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    goto Done;
  }

  /* Get a valid value
    */
  user_selection_mode = policyman_str_to_user_selection_mode(pStr);
  if(user_selection_mode == CM_NETWORK_SEL_MODE_PREF_NONE)
  {
    goto Done;
  }

  /* Does this action indicate the subscription
    *  Default is AS_ID_1
    */
   status = policyman_util_get_subs( pElem, &subs);
   if (MRE_FAILED(status))
   {
     goto Done;
   }

  /* Create the condition
    */
   pCondition = (policyman_user_selection_mode_condition_t *)
                   policyman_mem_alloc(sizeof(policyman_user_selection_mode_condition_t));
   
   ref_cnt_obj_init(pCondition, policyman_condition_dtor);
   pCondition->isTrue = (pfn_evaluate_t) policyman_condition_ph_network_selection_mode_evaluate;
   
   /* Copy value to condition
     */
   pCondition->user_selection_mode = user_selection_mode;
   pCondition->subsId = subs;
   status = MRE_STATUS_SUCCESS;

  *ppCondition = (policyman_condition_t *) pCondition;

Done:
  return status;
}


/*===========================================================================

FUNCTION policyman_map_mode_pref_nv2cm

DESCRIPTION
  Translates NV enum to CM enum for mode preference

DEPENDENCIES
  none

RETURN VALUE
  CM enum

SIDE EFFECTS
  none

===========================================================================*/
static cm_mode_pref_e_type 
policyman_map_mode_pref_nv2cm( 
  nv_mode_enum_type  nv_mode
  )
{

  switch (nv_mode)
  {
    case NV_MODE_DIGITAL_ONLY:
      return CM_MODE_PREF_DIGITAL_ONLY;

    case NV_MODE_ANALOG_ONLY:
      return CM_MODE_PREF_AMPS_ONLY;

    case NV_MODE_AUTOMATIC:
      return CM_MODE_PREF_AUTOMATIC;

    case NV_MODE_E911:
      return CM_MODE_PREF_EMERGENCY;

    case NV_MODE_CDMA_ONLY:
      return CM_MODE_PREF_CDMA_ONLY;

    case NV_MODE_HDR_ONLY:
      return CM_MODE_PREF_HDR_ONLY;

    case NV_MODE_CDMA_AMPS_ONLY:
      return CM_MODE_PREF_CDMA_AMPS_ONLY;

    case NV_MODE_GPS_ONLY:
      return CM_MODE_PREF_GPS_ONLY;

    case NV_MODE_GSM_ONLY:
      return CM_MODE_PREF_GSM_ONLY;

    case NV_MODE_WCDMA_ONLY:
      return CM_MODE_PREF_WCDMA_ONLY;

    case NV_MODE_ANY_BUT_HDR:
      return CM_MODE_PREF_ANY_BUT_HDR;

    case NV_MODE_GSM_WCDMA_ONLY:
      return CM_MODE_PREF_GSM_WCDMA_ONLY;

    case NV_MODE_DIGITAL_LESS_HDR_ONLY:
      return CM_MODE_PREF_DIGITAL_LESS_HDR_ONLY;

    case NV_MODE_CDMA_HDR_ONLY:
      return CM_MODE_PREF_CDMA_HDR_ONLY;

    case NV_MODE_CDMA_AMPS_HDR_ONLY:
      return CM_MODE_PREF_CDMA_AMPS_HDR_ONLY;

    case NV_MODE_LTE_ONLY:
      return CM_MODE_PREF_LTE_ONLY;

    case NV_MODE_GWL:
      return CM_MODE_PREF_GWL;

    case NV_MODE_HDR_LTE_ONLY:
      return CM_MODE_PREF_HDR_LTE_ONLY;

    case NV_MODE_CDMA_HDR_LTE_ONLY:
      return CM_MODE_PREF_CDMA_HDR_LTE_ONLY;

    case NV_MODE_CDMA_HDR_GSM_WCDMA:
      return CM_MODE_PREF_CDMA_HDR_GW;

    case NV_MODE_CDMA_GSM_WCDMA:
      return CM_MODE_PREF_CDMA_GW;

    case NV_MODE_CDMA_LTE_ONLY:
      return CM_MODE_PREF_CDMA_LTE_ONLY;

    case NV_MODE_CDMA_HDR_GSM:
      return CM_MODE_PREF_CDMA_HDR_GSM;

    case NV_MODE_CDMA_GSM:
      return CM_MODE_PREF_CDMA_GSM;

    case NV_MODE_GSM_LTE_ONLY:
      return CM_MODE_PREF_GSM_LTE;

    case NV_MODE_CDMA_GSM_LTE_ONLY:
      return CM_MODE_PREF_CDMA_GSM_LTE;

    case NV_MODE_HDR_GSM_LTE_ONLY:
      return CM_MODE_PREF_HDR_GSM_LTE;

    case NV_MODE_WCDMA_LTE_ONLY:
      return CM_MODE_PREF_WCDMA_LTE;

    case NV_MODE_CDMA_WCDMA_LTE_ONLY:
      return CM_MODE_PREF_CDMA_WCDMA_LTE;

    case NV_MODE_HDR_WCDMA_LTE_ONLY:
      return CM_MODE_PREF_HDR_WCDMA_LTE;

    case NV_MODE_TDS_ONLY:
      return CM_MODE_PREF_TDS_ONLY;

    case NV_MODE_TDS_GSM:
      return CM_MODE_PREF_TDS_GSM;

    case NV_MODE_TDS_GSM_LTE:
      return CM_MODE_PREF_TDS_GSM_LTE;

    case NV_MODE_TDS_LTE:
      return CM_MODE_PREF_TDS_LTE;

    case NV_MODE_TDS_GSM_WCDMA_LTE:
      return CM_MODE_PREF_TDS_GSM_WCDMA_LTE;

    case NV_MODE_TDS_GSM_WCDMA:
      return CM_MODE_PREF_TDS_GSM_WCDMA;

    case NV_MODE_CDMA_HDR_GSM_WCDMA_LTE:
       return CM_MODE_PREF_CDMA_HDR_GSM_WCDMA_LTE;

    case NV_MODE_CDMA_GSM_WCDMA_LTE:
       return CM_MODE_PREF_CDMA_GSM_WCDMA_LTE;

    case NV_MODE_TDS_WCDMA:
       return CM_MODE_PREF_TDS_WCDMA;

    case NV_MODE_TDS_WCDMA_LTE:
        return CM_MODE_PREF_TDS_WCDMA_LTE;

    case NV_MODE_CDMA_HDR_GSM_TDS_LTE:
       return CM_MODE_PREF_CDMA_HDR_GSM_TDS_LTE;

    default:
      POLICYMAN_MSG_ERROR_1("Invalid NV MODE %d, return AUTO",nv_mode);
      return CM_MODE_PREF_AUTOMATIC;
  }
} /* policyman_map_mode_pref_nv2cm() */

/*===========================================================================

FUNCTION policyman_map_srv_domain_pref_nv2cm

DESCRIPTION
  Translates NV enum to CM enum for service domain preference

DEPENDENCIES
  none

RETURN VALUE
  CM enum

SIDE EFFECTS
  none

===========================================================================*/
static cm_srv_domain_pref_e_type  
policyman_map_srv_domain_pref_nv2cm(
  nv_srv_domain_pref_enum_type   nv_mode
)
{
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  switch (nv_mode)
  {
    case NV_SRV_DOMAIN_PREF_CS_ONLY: return CM_SRV_DOMAIN_PREF_CS_ONLY;

    case NV_SRV_DOMAIN_PREF_PS_ONLY: return CM_SRV_DOMAIN_PREF_PS_ONLY;

    case NV_SRV_DOMAIN_PREF_CS_PS:   return CM_SRV_DOMAIN_PREF_CS_PS;

    case NV_SRV_DOMAIN_PREF_ANY:     return CM_SRV_DOMAIN_PREF_ANY;

    default:                         return ( cm_srv_domain_pref_e_type ) nv_mode;
  }

} /* policyman_map_srv_domain_pref_nv2cm */

/*===========================================================================
  FUNCTION POLICYMAN_PH_SET_OPRT_MODE()

  DESCRIPTION
    Set operating mode into PM Phone state

  PARAMETERS
    pPhInfo     : Pointer to PM Phone State info
    oprt_mode : operating mode to be set

  RETURN VALUE
    TRUE if mode_pref changed, FALSE otherwise
===========================================================================*/
boolean 
policyman_ph_set_oprt_mode(
  policyman_cmph_info_t *pPhInfo, 
  sys_oprt_mode_e_type oprt_mode
  )
{
  if(   pPhInfo != NULL 
     && pPhInfo->oprt_mode != oprt_mode
    )
  {
    pPhInfo->oprt_mode = oprt_mode;
    POLICYMAN_MSG_HIGH_1("oprt_mode %d updated in PM state", oprt_mode);

    /* we don't want to run rules in FTM mode
      */
    return oprt_mode != SYS_OPRT_MODE_FTM;
  }

  return FALSE;
}

/*===========================================================================
  FUNCTION POLICYMAN_PH_GET_OPRT_MODE()

  DESCRIPTION
    Get operating mode from PM Phone state

  PARAMETERS
    None

  RETURN VALUE
    TRUE if mode_pref changed, FALSE otherwise
===========================================================================*/
sys_oprt_mode_e_type 
policyman_ph_get_oprt_mode(
  void
  )
{
  sys_modem_as_id_e_type  asubs_id = policyman_get_current_multimode_subs();

  return policyman_state_get_state()->pSubsState[asubs_id]->pCmPhInfo->oprt_mode;
}




/*=============================================================================
===============================================================================

  Preference handling

===============================================================================
=============================================================================*/

typedef struct
{
  POLICYMAN_CMD_HDR;

  sys_modem_as_id_e_type              asubs_id;
  cm_sys_sel_pref_params_s_type       *pPref;
} policyman_cmd_report_pref_chg_t;


 static boolean
 policyman_pref_update_needs_policy_run(
   policyman_cmph_info_t               *pPhState,
   cm_sys_sel_pref_params_s_type const *pPref
  )
{
  boolean                             doUpdate = FALSE;
  sys_mcc_type                         userMcc;

  if (pPhState->mode_pref != pPref->mode_pref)
  {
    POLICYMAN_MSG_HIGH_2("mode_pref updated from %d to %d",
                        pPhState->mode_pref, pPref->mode_pref);
    pPhState->mode_pref = pPref->mode_pref;
    doUpdate = TRUE;
  }

  if (pPhState->srv_domain_pref != pPref->srv_domain_pref)
  {
    POLICYMAN_MSG_HIGH_2("srv_domain_pref updated from %d to %d",
                        pPhState->srv_domain_pref, pPref->srv_domain_pref);
    pPhState->srv_domain_pref = pPref->srv_domain_pref;
    doUpdate = TRUE;
  }

  /* Check for MANUAL mode if PLMN is valid and then only run rules
       otherwise reset to AUATOMATIC
    */
  if(pPref->network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL)
  {
    userMcc = policyman_plmn_get_mcc((sys_plmn_id_s_type  *)pPref->plmn_ptr);
    if( userMcc > 0 && userMcc < 1000)
    {
      pPhState->network_sel_mode_pref = CM_NETWORK_SEL_MODE_PREF_MANUAL;
      pPhState->user_plmn = *pPref->plmn_ptr;
      POLICYMAN_MSG_HIGH_0("Network selection = MANUAL with new PLMN:");
      doUpdate = TRUE;
    }
    else
    {
      pPhState->network_sel_mode_pref = CM_NETWORK_SEL_MODE_PREF_AUTOMATIC;
      policyman_plmn_clear(&pPhState->user_plmn);
      POLICYMAN_MSG_ERROR_0("Network selection = MANUAL with invalid PLMN");
    }
    policyman_plmn_print((sys_plmn_id_s_type *)pPref->plmn_ptr);
  }
  else
  {
    pPhState->network_sel_mode_pref = CM_NETWORK_SEL_MODE_PREF_AUTOMATIC;
    policyman_plmn_clear(&pPhState->user_plmn);
  }

  return doUpdate;
  
  }

 
/*-------- policyman_report_preference_execute --------*/
static void
policyman_report_preference_execute(
  policyman_cmd_t *pCmd
  )
{
  cm_sys_sel_pref_params_s_type const *pPref;
  policyman_cmd_report_pref_chg_t     *pPrefCmd;
  policyman_cmph_info_t               *pState;

  pPrefCmd = (policyman_cmd_report_pref_chg_t *) pCmd;
  pPref = pPrefCmd->pPref;
  
  pState = policyman_ph_get_cm_ph_state(pPrefCmd->asubs_id);

  POLICYMAN_MSG_HIGH_3("executing pref change command with subs = %d, mode_pref = %d, srv_domain_pref = %d",
                      pPrefCmd->asubs_id,  
                      pPref->mode_pref, 
                      pPref->srv_domain_pref
                    );

  if (policyman_pref_update_needs_policy_run(pState,pPref))
  {
    policyman_subs_update_multimode_subs(pCmd);
  }

  policyman_mem_free(pPrefCmd->pPref) ;
}


/*-------- policyman_report_preference_execute --------*/
static void
policyman_compute_policy_execute(
  policyman_cmd_t *pCmd
  )
{
  cm_sys_sel_pref_params_s_type const *pPref;
  policyman_cmd_report_pref_chg_t     *pPrefCmd;
  policyman_cmph_info_t               *pPhState,*pPhStateOriginal;
  policyman_set_t                     *pItemSet = policyman_itemset_new();
  policyman_state_t                   *pState = policyman_state_get_state();
  

  pPrefCmd = (policyman_cmd_report_pref_chg_t *) pCmd;
  pPref = pPrefCmd->pPref;

  pPhState = (policyman_cmph_info_t *) policyman_mem_alloc(sizeof(policyman_cmph_info_t));
  
  *pPhState = *policyman_ph_get_cm_ph_state(pPrefCmd->asubs_id);
  pPhStateOriginal = policyman_ph_get_cm_ph_state(pPrefCmd->asubs_id);
  pState->pSubsState[pPrefCmd->asubs_id]->pCmPhInfo = pPhState;

  POLICYMAN_MSG_HIGH_3("executing compute policy command with subs = %d, mode_pref = %d, srv_domain_pref = %d",
                      pPrefCmd->asubs_id,  
                      pPref->mode_pref, 
                      pPref->srv_domain_pref
                    );

  /* Run a policy check if needed.
   */
  if (policyman_pref_update_needs_policy_run(pPhState,pPref))
  {
    policyman_policy_run_policy_check(pState, pPrefCmd->asubs_id, pItemSet);
  }

  pCmd->pMsg = policyman_cfgitem_get_change_msg_hdr(pItemSet);
  	
  pState->pSubsState[pPrefCmd->asubs_id]->pCmPhInfo = pPhStateOriginal;
  
  policyman_mem_free(pPrefCmd->pPref) ;
  policyman_mem_free(pPhState);
  REF_CNT_OBJ_RELEASE_IF(pItemSet);
  
  /*  Unblock the thread that issued the command.
   */
  policyman_client_block_signal_set(pCmd->signal);

}


/*-------- policyman_report_prefs_have_changed --------*/
static boolean
policyman_report_prefs_have_changed(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  )
{
  policyman_cmph_info_t *pInfo = policyman_ph_get_cm_ph_state(asubs_id);

  return    pInfo->mode_pref != pPref->mode_pref
         || pInfo->srv_domain_pref != pPref->srv_domain_pref
         || pInfo->network_sel_mode_pref != pPref->network_sel_mode_pref
         || ( pPref->network_sel_mode_pref == CM_NETWORK_SEL_MODE_PREF_MANUAL
              && !policyman_plmns_are_equal( (sys_plmn_id_s_type  *)pPref->plmn_ptr, 
                                            &pInfo->user_plmn
                                          ) 
            );
}  


/*-------- policyman_block_for_pref_change --------*/
static boolean
policyman_block_for_pref_change(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  )
{

  /*  Block the caller of policyman_report_preference_change() if:
   *    - We're not initializing
   *    - and there are changes in preferences that we care about
   */
  return    !policyman_is_initializing()
         && policyman_report_prefs_have_changed(asubs_id, pPref);
}


/*-------- policyman_report_preference_change --------*/
msgr_hdr_s *
policyman_report_preference_change(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  )
{
  policyman_cmd_report_pref_chg_t *pCmd;
  msgr_hdr_s                      *pMsg = NULL;

  /*  If we aren't going to block to process this request, just return.
   */
  if (!policyman_block_for_pref_change(asubs_id, pPref))
  {
    goto Done;
  }

  /*  If we should block the calling thread to do this update, get a signal on
   *  which to block for command completion, and put it in the command.
   *  Queue the command and wait for completion.
   */
  pCmd = (policyman_cmd_report_pref_chg_t *) policyman_cmd_new(
                                                sizeof(policyman_cmd_report_pref_chg_t),
                                                policyman_report_preference_execute,
                                                NULL,
                                                NULL
                                                );
  pCmd->asubs_id = asubs_id;
  
  pCmd->pPref = (cm_sys_sel_pref_params_s_type *)
                policyman_mem_alloc(sizeof(cm_sys_sel_pref_params_s_type));
  *pCmd->pPref = *pPref;

  pCmd->pMsg = NULL;

  POLICYMAN_MSG_HIGH_0("blocking caller of policyman_report_preference_change()");
  pCmd->signal = policyman_client_block_sig_get();
  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);

  /* Block the caller only when policy is not suspended
  */
  if(!policyman_policy_is_suspended())
  {
    policyman_client_block_signal_wait(pCmd->signal);
  }

  /*  Get any message to be returned and then release the command.
   */
  pMsg = pCmd->pMsg;
  ref_cnt_obj_release(pCmd);

Done:
  POLICYMAN_MSG_HIGH_0("returning to caller of policyman_report_preference_change()");
  return pMsg;
}

/*-------- policyman_report_preference_change --------*/
msgr_hdr_s *
policyman_compute_policy_with_newpref(
  sys_modem_as_id_e_type              asubs_id,
  cm_sys_sel_pref_params_s_type const *pPref
  )
{
  policyman_cmd_report_pref_chg_t *pCmd;
  msgr_hdr_s                      *pMsg = NULL;

  /*  If we aren't going to block to process this request, just return.
   */
  if (!policyman_block_for_pref_change(asubs_id, pPref))
  {
    POLICYMAN_MSG_HIGH_0("Either PM initializing or there is pref change PM interested in");
    goto Done;
  }

  /*  If we should block the calling thread to do this update, get a signal on
   *  which to block for command completion, and put it in the command.
   *  Queue the command and wait for completion.
   */
  pCmd = (policyman_cmd_report_pref_chg_t *) policyman_cmd_new(
                                                sizeof(policyman_cmd_report_pref_chg_t),
                                                policyman_compute_policy_execute,
                                                NULL,
                                                NULL
                                                );
  pCmd->asubs_id = asubs_id;
  
  pCmd->pPref = (cm_sys_sel_pref_params_s_type *)
                policyman_mem_alloc(sizeof(cm_sys_sel_pref_params_s_type));
  *pCmd->pPref = *pPref;

  pCmd->pMsg = NULL;

  POLICYMAN_MSG_HIGH_0("blocking caller of policyman_compute_policy_with_newpref");
  pCmd->signal = policyman_client_block_sig_get();
  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);

  /* Block the caller only when policy is not suspended
  */
  if(!policyman_policy_is_suspended())
  {
    policyman_client_block_signal_wait(pCmd->signal);
  }

  /*  Get any message to be returned and then release the command.
   */
  pMsg = pCmd->pMsg;
  ref_cnt_obj_release(pCmd);

Done:
  POLICYMAN_MSG_HIGH_0("returning to caller of policyman_report_preference_change()");
  return pMsg;
}


/*-------- policyman_report_preference_msg_free --------*/
void
policyman_report_preference_msg_free(
  msgr_hdr_s  *pMsg
  )
{
  policyman_report_msg_free(pMsg);
}

