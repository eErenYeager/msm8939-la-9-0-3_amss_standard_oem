/**
  @file policyman_uim.c

  @brief
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_uim.c#2 $
  $DateTime: 2015/06/26 02:43:52 $
  $Author: arkhanna $
*/

#include "policyman_cfgitem.h"
#include "policyman_task.h"
#include "policyman_util.h"
#include "policyman_uim.h"
#include "policyman_rules.h"
#include "policyman_set.h"
#include "policyman_xml.h"
#include "policyman_plmn.h"
#include "policyman_dbg.h"
#include "policyman_task.h"
#include "mmgsdilib.h"
#include "mmgsdisessionlib.h"
#include <stringl/stringl.h>

#if defined(VOID) && defined(XX_WIN32)
#undef VOID
#endif
#define VOID

#define SIM_TYPE_TAG  "sim_type"


typedef struct
{
  mmgsdi_session_type_enum_type session_type;  /* the key */ 
  sys_modem_as_id_e_type        asubs_id;      /* we assign this */
  mmgsdi_session_id_type        session_id;    /* comes from mmgsdi */
  mmgsdi_app_enum_type          app_type;      /* comes from mmgsdi */
  boolean                       in_use;        /* we manage. true if session open */ 
  boolean                       active;        /* we manage. true if session active */ 
} pm_mmgsdi_session_id_table_entry_s_type;

static boolean is_mmgsdi_subscription_ready_received = FALSE;

static pm_mmgsdi_session_id_table_entry_s_type pm_mmgsdi_session_id_table[] = { {MMGSDI_GW_PROV_PRI_SESSION, SYS_MODEM_AS_ID_1}, /*lint !e785 */
                                                                                {MMGSDI_1X_PROV_PRI_SESSION, SYS_MODEM_AS_ID_1}, /*lint !e785 */
                                                                                {MMGSDI_GW_PROV_SEC_SESSION, SYS_MODEM_AS_ID_2}, /*lint !e785 */
                                                                                {MMGSDI_1X_PROV_SEC_SESSION, SYS_MODEM_AS_ID_2}, /*lint !e785 */
                                                                              };


/*=============================================================================
  Function Prototype
=============================================================================*/
static void pm_mmgsdi_card_status_cb(const mmgsdi_event_data_type   *mmgsdi_event);

static void pm_mmgsdi_reg_status_cb(mmgsdi_return_enum_type status,
                                  mmgsdi_cnf_enum_type    data_type,
                                  const mmgsdi_cnf_type  *data_ptr);

static void pm_open_sessions (void);

static policyman_status_t pm_mmgsdi_read
(
    mmgsdi_session_type_enum_type     session_type,
      /* session whose files to read */

    mmgsdi_file_enum_type             file_name,
        /* Name of the file to read */

    mmgsdi_rec_num_type               record_number,
        /* Record number to begin with, in the file */

    mmgsdi_len_type                   request_length,
        /* Total bytes to be read */

    mmgsdi_client_data_type client_ref

);

boolean sys_get_num_of_mnc_digits(
  mmgsdi_data_type * imsi_ptr,
  mmgsdi_data_type * ad_ptr,
  uint8            * num_mnc_digits_ptr
);


/*=============================================================================
  Data structures
=============================================================================*/


/*-------- policyman_uim_subs_info_t --------*/
/**
@brief  Per-subscription information
*/


/*-------- policyman_uim_info_t --------*/
/**
@brief  Information from all SIMs
*/


/*=============================================================================
  APIs for UIM information
=============================================================================*/


/*-------- policyman_uim_init --------*/
boolean
policyman_uim_init(policyman_uim_info_t  **pUimInfo )
{

   //mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;

   *pUimInfo = (policyman_uim_info_t *) policyman_mem_alloc(sizeof(policyman_uim_info_t));

   return (*pUimInfo != NULL);

}

/*-------- policyman_uim_deinit --------*/
void
policyman_uim_deinit(
  policyman_uim_info_t  *pUimInfo
  )
{
  POLICYMAN_MEM_FREE_IF(pUimInfo);
}

/*-------- policyman_register_for_uim_events --------*/
void policyman_register_for_uim_events(
  void
)
{

  mmgsdi_return_enum_type mmgsdi_status = MMGSDI_SUCCESS;

  is_mmgsdi_subscription_ready_received = FALSE;

  mmgsdi_status = mmgsdi_client_id_and_evt_reg( pm_mmgsdi_card_status_cb,
                                                pm_mmgsdi_reg_status_cb,
                                                0 );
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    POLICYMAN_MSG_ERROR("mmgsdi_client_id_and_evt_reg failed %d", mmgsdi_status, 0, 0);
  }
}


/*-------- policyman_uim_get_uim_info --------*/
static policyman_uim_info_t* policyman_uim_get_uim_info(
  sys_modem_as_id_e_type subsId
  )
{
  if(subsId < 0 || subsId >= POLICYMAN_NUM_SUBS)
  {
    POLICYMAN_ERR_FATAL("subsId out of range", 0, 0, 0);
  }
 
  return policyman_state_get_state()->pSubsState[subsId]->pUimInfo;
}

typedef struct
{
  POLICYMAN_CMD_HDR;
  mmgsdi_event_data_type   mmgsdi_event;
} policyman_uim_evt_cb_s;

typedef struct
{
   POLICYMAN_CMD_HDR;
   mmgsdi_return_enum_type status;
   mmgsdi_cnf_enum_type    data_type;
   mmgsdi_cnf_type         data;
} pm_mmgsdi_resp_s_type;

static mmgsdi_client_id_type pm_mmgsdi_client_id = 0;

/*===========================================================================
  FUNCTION PM_PROCESS_MMGSDI_REG_STATUS()

  DESCRIPTION
    MMGSDI process function
 ===========================================================================*/
void pm_process_mmgsdi_reg_status(policyman_cmd_t *pCmd)
{
  pm_mmgsdi_resp_s_type *pMmgsdi_resp = (pm_mmgsdi_resp_s_type*)pCmd;

  if (pMmgsdi_resp->status == MMGSDI_SUCCESS)
  {
    if (pMmgsdi_resp->data_type== MMGSDI_CLIENT_ID_AND_EVT_REG_CNF)
    {
      pm_mmgsdi_client_id = pMmgsdi_resp->data.client_id_and_evt_reg_cnf.response_header.client_id;

      //open sessions irrespective of knowing the SIM inserted or not
      pm_open_sessions();
    }
  }
  else
  {
    switch (pMmgsdi_resp->data_type)
    {
      case MMGSDI_CLIENT_ID_AND_EVT_REG_CNF:
        POLICYMAN_MSG_ERROR("MMGSDI failed registering PM client ID & EVT registration.  Status %d, Confirmation %d",
        pMmgsdi_resp->status, pMmgsdi_resp->data_type, 0);
        break;

      default:
        POLICYMAN_MSG_ERROR("MMGSDI returned Failure async. Status %d, Confirmation %d",
        pMmgsdi_resp->status, pMmgsdi_resp->data_type, 0);
        break;
    }
  }
}

/*===========================================================================
  FUNCTION pm_mmgsdi_ssn_tbl_get_entry_by_type()

  DESCRIPTION
 ===========================================================================*/
static pm_mmgsdi_session_id_table_entry_s_type*
                                       pm_mmgsdi_ssn_tbl_get_entry_by_type
(
  mmgsdi_session_type_enum_type session_type
)
{
  int index;
  pm_mmgsdi_session_id_table_entry_s_type* ssn_entry = NULL;


  for(index = 0; index < ARR_SIZE(pm_mmgsdi_session_id_table); index++)
  {
    if( pm_mmgsdi_session_id_table[index].session_type == session_type &&
        pm_mmgsdi_session_id_table[index].in_use == TRUE
      )
    {
      ssn_entry = &pm_mmgsdi_session_id_table[index];
      break;
    }
  }
  return ssn_entry;
}

/*===========================================================================
  FUNCTION pm_mmgsdi_ssn_tbl_get_entry_by_id()

  DESCRIPTION
 ===========================================================================*/
static pm_mmgsdi_session_id_table_entry_s_type*
                                       pm_mmgsdi_ssn_tbl_get_entry_by_id
(
  mmgsdi_session_id_type        session_id
)
{
  int index;
  pm_mmgsdi_session_id_table_entry_s_type* ssn_entry = NULL;


  for(index = 0; index < ARR_SIZE(pm_mmgsdi_session_id_table); index++)
  {
    if( (pm_mmgsdi_session_id_table[index].session_id == session_id) &&
        (pm_mmgsdi_session_id_table[index].in_use == TRUE)
      )
    {
      ssn_entry = &pm_mmgsdi_session_id_table[index];
      break;
    }
  }
  if(ssn_entry == NULL)
  {
    POLICYMAN_MSG_ERROR("invalid session-id 0x%x", session_id, 0, 0);
  }
  return ssn_entry;
}

/*===========================================================================
  FUNCTION print_session_info()
 ===========================================================================*/
static void print_session_info(
  pm_mmgsdi_session_id_table_entry_s_type *ssn_entry
  )
{
  POLICYMAN_MSG_MED_4("session identified Type:%d App: %d active:%d inuse: %d",
                       ssn_entry->session_type,
                       ssn_entry->app_type,
                       ssn_entry->active,
                       ssn_entry->in_use);
}

/*===========================================================================
  Data Structure for READ CNF from MMGSDI
 ===========================================================================*/

#define PM_MMGSDI_RPM_AD_SIZE           4 /* EF-AD */

typedef struct
{
   POLICYMAN_CMD_HDR;
   mmgsdi_cnf_type  pm_mmgsdi_cnf;
} pm_mmgsdi_read_cnf_s_type;

/*===========================================================================
  FUNCTION pm_dtor_mmgsdi_read_cnf()

  DESCRIPTION
 ===========================================================================*/
void pm_dtor_mmgsdi_read_cnf
(
  policyman_cmd_t *pCmd
)
{
   /* Free the memory for the array of items. */
   POLICYMAN_MSG_MED("pm_dtor_mmgsdi_read_cnf called for mmgsdi cmd", 0, 0, 0);
   POLICYMAN_MEM_FREE_IF(((pm_mmgsdi_read_cnf_s_type*)pCmd)->pm_mmgsdi_cnf.read_cnf.read_data.data_ptr);
}

/*===========================================================================
  FUNCTION pm_process_mmgsdi_record()

  DESCRIPTION
 ===========================================================================*/
void
pm_process_mmgsdi_record(
  policyman_cmd_t *pCmd
  )
{
  mmgsdi_cnf_type           *pMmgsdi_cnf = &((pm_mmgsdi_read_cnf_s_type*)pCmd)->pm_mmgsdi_cnf;
  boolean                   status    = FALSE;
  mmgsdi_data_type          *admin_data;

  mmgsdi_data_type          *imsi_data_ptr = NULL;
  uint8                      num_mnc_digits;

  policyman_uim_info_t subs_info;
  pm_mmgsdi_session_id_table_entry_s_type *sess_info=NULL;

  sess_info = pm_mmgsdi_ssn_tbl_get_entry_by_id(pMmgsdi_cnf->read_cnf.response_header.session_id);
  PM_CHECK_PTR_RET(sess_info,VOID);


  POLICYMAN_MSG_HIGH("PM read data len %d file %d", pMmgsdi_cnf->read_cnf.read_data.data_len, pMmgsdi_cnf->read_cnf.access.file.file_enum, 0);

  if (  pMmgsdi_cnf->read_cnf.access.file.file_enum ==
          (sess_info->app_type == MMGSDI_APP_SIM ? MMGSDI_GSM_IMSI : MMGSDI_USIM_IMSI)
     )
  {
    //completed reading IMSI
    //remember IMSI and continue reading EF-AD
    imsi_data_ptr = (mmgsdi_data_type *)policyman_mem_alloc(sizeof(mmgsdi_data_type));
    imsi_data_ptr->data_ptr = (uint8*)policyman_mem_alloc(MMGSDI_IMSI_LEN);

    PM_CHECK_PTR_RET(imsi_data_ptr,VOID);
    PM_CHECK_PTR_RET(imsi_data_ptr->data_ptr, VOID);

    imsi_data_ptr->data_len = pMmgsdi_cnf->read_cnf.read_data.data_len;
    memscpy(imsi_data_ptr->data_ptr, MMGSDI_IMSI_LEN, pMmgsdi_cnf->read_cnf.read_data.data_ptr, MMGSDI_IMSI_LEN);

    status = pm_mmgsdi_read( sess_info->session_type,
                             (sess_info->app_type == MMGSDI_APP_SIM ? MMGSDI_GSM_AD : MMGSDI_USIM_AD),
                             0,
                             PM_MMGSDI_RPM_AD_SIZE,
                             (mmgsdi_client_data_type)imsi_data_ptr
                           );
    POLICYMAN_MSG_MED("pm_mmgsdi_read returned status %d", status, 0, 0);
  }
  else if ( pMmgsdi_cnf->read_cnf.access.file.file_enum ==
              (sess_info->app_type == MMGSDI_APP_SIM ? MMGSDI_GSM_AD : MMGSDI_USIM_AD)
          )
  {
    imsi_data_ptr = (mmgsdi_data_type*)pMmgsdi_cnf->response_header.client_data;
    admin_data = &(pMmgsdi_cnf->read_cnf.read_data);
    subs_info.plmn.identity[0] = (byte)(( ( imsi_data_ptr->data_ptr[1] & 0xF0 ) / 0x10 ) +
                                          ( ( imsi_data_ptr->data_ptr[2] & 0x0F ) * 0x10 ));

    /*
    ** Fill MCC Digit 3 and MNC Digit 3
    */
    subs_info.plmn.identity[1] = (byte)(( ( imsi_data_ptr->data_ptr[2] & 0xF0 ) / 0x10 ) +
                                        ( ( imsi_data_ptr->data_ptr[4] & 0x0F ) * 0x10 ));

    /*
    ** If 2 digits MNC is indicated in EF-AD then set MNC digit 3 to 'F'.
    */
    if(sys_get_num_of_mnc_digits(imsi_data_ptr, admin_data, &num_mnc_digits))
    {
      if(num_mnc_digits == 2)
      {
        POLICYMAN_MSG_HIGH("2 digit MNC",0,0,0);
  
        subs_info.plmn.identity[1] |= 0xF0;
      }
    }

    /*
    ** Fill MNC Digit 1 and MNC Digit 2
    */
    subs_info.plmn.identity[2] = imsi_data_ptr->data_ptr[3];
    POLICYMAN_MSG_HIGH("IMSI PLMN:", 0, 0, 0);
    policyman_plmn_print(&subs_info.plmn);

    policyman_state_set_precondition_met(POLICYMAN_PRECOND_IMSI, sess_info->asubs_id);
    policyman_state_set_imsi_plmn(sess_info->asubs_id, &subs_info.plmn);
    policyman_mem_free(imsi_data_ptr->data_ptr);
    policyman_mem_free(imsi_data_ptr);
  }
}

/*===========================================================================
  FUNCTION PM_MMGSDI_READ_CB()

  DESCRIPTION
    MMGSDI callback function
 ===========================================================================*/
static void pm_mmgsdi_read_cb(
    mmgsdi_return_enum_type    status,
     /* Status of 'read' sent by MMGSDI */
    mmgsdi_cnf_enum_type       cnf,
     /* command conf from MMGSDI */
    const mmgsdi_cnf_type      *cnf_ptr
     /* conf pointer returned by MMGSDI */
)
{
  pm_mmgsdi_read_cnf_s_type *pCmd = NULL;

  if(cnf_ptr == NULL || status != MMGSDI_SUCCESS || cnf != MMGSDI_READ_CNF)
  {
    POLICYMAN_MSG_MED ("cnf_ptr - %d status - %d cnf - %d ",cnf_ptr,status,cnf);
    return;
  }

  POLICYMAN_MSG_LOW("Values read from MMGSDI",0,0,0);
  POLICYMAN_MSG_LOW("data_len: %x, data_ptr: %x,file_enum: %x",
                 cnf_ptr->read_cnf.read_data.data_len,
                 cnf_ptr->read_cnf.read_data.data_ptr,
                 cnf_ptr->read_cnf.access.file.file_enum);

  POLICYMAN_MSG_MED("cmmmgsdi_wait_for_read_complete_cb: cnf- %d,data_len - %d",
                  cnf, cnf_ptr->read_cnf.read_data.data_len, 0);

  pCmd = (pm_mmgsdi_read_cnf_s_type *) policyman_cmd_new( sizeof(pm_mmgsdi_read_cnf_s_type),
                                            pm_process_mmgsdi_record,
                                            pm_dtor_mmgsdi_read_cnf,
                                            NULL
                                            );
  pCmd->pm_mmgsdi_cnf = *cnf_ptr;

  if( MMGSDI_READ_CNF == cnf &&  
      cnf_ptr->read_cnf.read_data.data_len != 0
    )
  {
    /* malloc the pointer for mmgsdi cnf type and copy the info over. */
    pCmd->pm_mmgsdi_cnf.read_cnf.read_data.data_len = cnf_ptr->read_cnf.read_data.data_len;
    pCmd->pm_mmgsdi_cnf.read_cnf.read_data.data_ptr = (uint8*)policyman_mem_alloc( sizeof(uint8) * (cnf_ptr->read_cnf.read_data.data_len));

    memscpy( pCmd->pm_mmgsdi_cnf.read_cnf.read_data.data_ptr, sizeof(uint8) * (cnf_ptr->read_cnf.read_data.data_len),
            cnf_ptr->read_cnf.read_data.data_ptr, sizeof(uint8) * cnf_ptr->read_cnf.read_data.data_len);
  }  /* if */

  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
  ref_cnt_obj_release(pCmd);
}  /* cmmmgsdi_wait_for_read_complete_cb() */

/*===========================================================================
  FUNCTION pm_mmgsdi_read()

  DESCRIPTION
 ===========================================================================*/
static policyman_status_t pm_mmgsdi_read
(
    mmgsdi_session_type_enum_type     session_type,
      /* session whose files to read */

    mmgsdi_file_enum_type             file_name,
        /* Name of the file to read */

    mmgsdi_rec_num_type               record_number,
        /* Record number to begin with, in the file */

    mmgsdi_len_type                   request_length,
        /* Total bytes to be read */

    mmgsdi_client_data_type client_ref

)
{
  mmgsdi_return_enum_type	      mmgsdi_status = MMGSDI_ERROR;
  mmgsdi_session_id_type		  pm_mmgsdi_session_id;
  mmgsdi_access_type			  file_access;
  pm_mmgsdi_session_id_table_entry_s_type *ssn_entry;

  file_access.access_method = MMGSDI_EF_ENUM_ACCESS;
  file_access.file.file_enum = file_name;

  if (NULL == (ssn_entry = pm_mmgsdi_ssn_tbl_get_entry_by_type(session_type)))
  {
    POLICYMAN_MSG_ERROR("Unable to get session id; session type: %d", session_type, 0, 0);
    return POLICYMAN_STATUS_ERR;
  }
  else
  {
    pm_mmgsdi_session_id = ssn_entry->session_id;
  }

  mmgsdi_status = mmgsdi_session_read_transparent( pm_mmgsdi_session_id,
                                                   file_access,
                                                   0,
                                                   request_length,
                                                   pm_mmgsdi_read_cb,
                                                   client_ref
                                                 );
  if (mmgsdi_status != MMGSDI_SUCCESS)
  {
    POLICYMAN_MSG_ERROR("mmgsdi_read_transparent failed.", 0, 0, 0);
    return POLICYMAN_STATUS_ERR;
  }

  return POLICYMAN_STATUS_SUCCESS;
}

/*===========================================================================
  FUNCTION pm_read_imsi_hplmn()

  DESCRIPTION
 ===========================================================================*/
static policyman_status_t pm_read_imsi_hplmn(mmgsdi_session_type_enum_type session_type)
{
  policyman_status_t            status    = POLICYMAN_STATUS_ERR;
  pm_mmgsdi_session_id_table_entry_s_type* ssn_entry = NULL;
  ssn_entry = pm_mmgsdi_ssn_tbl_get_entry_by_type(session_type);
  PM_CHECK_PTR_RET(ssn_entry,POLICYMAN_STATUS_ERR);

  /* Read EF-IMSI */
  status = pm_mmgsdi_read( session_type,
                          (ssn_entry->app_type == MMGSDI_APP_SIM ? MMGSDI_GSM_IMSI : MMGSDI_USIM_IMSI),
                          0,
                          MMGSDI_IMSI_LEN,
                          0
                         );

  /* Read EF-AD to get MNC length */
  return status;
} /* cmrpm_read_imsi_hplmn() */

#define PM_MMGSDI_NUM_REGISTRED_FILES 2

/*-------- policyman_uim_set_subs_active --------*/
static void policyman_uim_set_subs_active(
  sys_modem_as_id_e_type subsId,
  boolean                state
)
{  
  /* Update in state that SUBS is ready*/
  policyman_uim_info_t *pUimInfo = policyman_uim_get_uim_info(subsId);

  if (pUimInfo->subs_active != state)
  {
    pUimInfo->subs_active = state;
    POLICYMAN_MSG_HIGH_3("subs %d: active state updated from %d to %d", subsId, pUimInfo->subs_active, state);
  }  
}

/*-------- policyman_uim_get_subs_active --------*/
boolean policyman_uim_get_subs_active(
  sys_modem_as_id_e_type subsId
)
{  
  policyman_uim_info_t *pUimInfo = policyman_uim_get_uim_info(subsId);

  return pUimInfo->subs_active;
}


/*===========================================================================
  FUNCTION pm_mmgsdi_refresh_cb()

  DESCRIPTION
 ===========================================================================*/
static void pm_mmgsdi_refresh_cb(mmgsdi_return_enum_type status,
                                  mmgsdi_cnf_enum_type    cnf,
                                  const mmgsdi_cnf_type  *cnf_ptr)
{
   /* DO Nothing */
   if ((cnf != MMGSDI_REFRESH_CNF) || (status != MMGSDI_SUCCESS))
   {
      POLICYMAN_MSG_ERROR("Refresh Complete Ack failed %d %d", cnf, status, 0);
   }
} /* pm_mmgsdi_refresh_cb */




/*===========================================================================
 FUNCTION policyman_uim_set_sim_type()

 DESCRIPTION
   Set SIM app type into UIM state
===========================================================================*/
STATIC boolean policyman_uim_set_sim_type(
  policyman_state_t    *pInfo,
  size_t                subsId,
  mmgsdi_app_enum_type sim_app_type
)
{
  policyman_uim_info_t *pUimInfo = policyman_uim_get_uim_info(subsId);
  boolean               changed = FALSE;

  if (sim_app_type != pUimInfo->sim_app_type)
  {
    pUimInfo->sim_app_type = sim_app_type;
    POLICYMAN_MSG_HIGH("updated sim_app_type %d for subs %d", sim_app_type, subsId, 0);
    changed = TRUE;
  }

  return changed;
}


/*===========================================================================
 FUNCTION policyman_uim_set_cdma_sim_type()

 DESCRIPTION
   Set CDMA app type into UIM state
===========================================================================*/
STATIC boolean policyman_uim_set_cdma_sim_type(
  policyman_state_t    *pInfo,
  size_t                subsId,
  mmgsdi_app_enum_type sim_app_type
)
{
  policyman_uim_info_t *pUimInfo = policyman_uim_get_uim_info(subsId);
  boolean               changed = FALSE;

  if (sim_app_type != pUimInfo->cdma_app_type)
  {
    pUimInfo->cdma_app_type = sim_app_type;
    POLICYMAN_MSG_HIGH("updated cdma_sim_app type %d for subs %d", sim_app_type, subsId, 0);
    changed = TRUE;
  }

  return changed;
}


/*===========================================================================
  FUNCTION pm_process_uim_evt()

  DESCRIPTION
 ===========================================================================*/
void pm_process_uim_evt(policyman_cmd_t *pCmd)
{
  mmgsdi_event_data_type   *pMmgsdi_event = &((policyman_uim_evt_cb_s*)pCmd)->mmgsdi_event;
  pm_mmgsdi_session_id_table_entry_s_type *ssn_entry = NULL;
  policyman_status_t pm_status;
  policyman_state_t                       *pState = policyman_state_get_state();

  POLICYMAN_MSG_HIGH("PM got MMGSDI evt %d", pMmgsdi_event->evt, 0, 0);
  ssn_entry = pm_mmgsdi_ssn_tbl_get_entry_by_id(pMmgsdi_event->session_id);
  PM_CHECK_PTR_RET(ssn_entry,VOID);

  print_session_info(ssn_entry);

  switch (pMmgsdi_event->evt)
  {
    case MMGSDI_PIN1_EVT:
      break;

    case MMGSDI_SUBSCRIPTION_READY_EVT:
    {
      mmgsdi_refresh_file_list_type  file_info;
      mmgsdi_file_enum_type files[PM_MMGSDI_NUM_REGISTRED_FILES]={MMGSDI_GSM_IMSI,MMGSDI_GSM_AD};
      mmgsdi_return_enum_type mmgsdi_status;

      policyman_state_set_uim_num_subs(policyman_state_get_uim_num_subs() + 1);

      if (is_mmgsdi_subscription_ready_received == FALSE)
      {
        is_mmgsdi_subscription_ready_received = TRUE;
        /* Print out all configuration database when receiving the first MMGSDI_SUBSCRIPTION_READY_EVT
         */
        POLICYMAN_MSG_MED_0("Received the first MMGSDI_SUB_READY_EVT, print all configuration database");
        policyman_cfgitem_display_all();
      }

      ssn_entry->app_type=  pMmgsdi_event->data.subscription_ready.app_info.app_data.app_type;

      ssn_entry->active = TRUE;

      if (ssn_entry->session_type == MMGSDI_GW_PROV_PRI_SESSION || ssn_entry->session_type == MMGSDI_GW_PROV_SEC_SESSION)
      {
        pm_status = pm_read_imsi_hplmn(ssn_entry->session_type);

        if (pm_status != POLICYMAN_STATUS_SUCCESS)
        {
          POLICYMAN_MSG_ERROR("Internal error %d", pm_status, 0, 0);
        }
        if (ssn_entry->app_type == MMGSDI_APP_USIM)
        {
          files[0] = MMGSDI_USIM_IMSI;
          files[1] = MMGSDI_USIM_AD;
        }

        file_info.file_list_ptr = files;
        file_info.num_files = PM_MMGSDI_NUM_REGISTRED_FILES;

        mmgsdi_status = mmgsdi_session_register_for_refresh(
                                    pMmgsdi_event->session_id,
                                    file_info,
                                    FALSE,
                                    pm_mmgsdi_refresh_cb,
                                    0
                                    );
        if (mmgsdi_status != MMGSDI_SUCCESS)
        {
          POLICYMAN_MSG_ERROR("mmsgdi error %d", mmgsdi_status, 0, 0);
        }

        policyman_state_set_precondition_met(POLICYMAN_PRECOND_SIMTYPE, ssn_entry->asubs_id);
        policyman_uim_set_sim_type(pState, ssn_entry->asubs_id, ssn_entry->app_type);
      }
      else
      {
        policyman_state_set_precondition_met(POLICYMAN_PRECOND_CDMASIMTYPE, ssn_entry->asubs_id);
        policyman_uim_set_cdma_sim_type(pState, ssn_entry->asubs_id, ssn_entry->app_type);
      }

      policyman_uim_set_subs_active(ssn_entry->asubs_id, TRUE);
      policyman_subs_update_multimode_subs(NULL);
    }

    break;

    case MMGSDI_SESSION_CHANGED_EVT:
    {
      if (!pMmgsdi_event->data.session_changed.activated 
          && ssn_entry->active == TRUE)
      {
        ssn_entry->active = FALSE;
        policyman_uim_set_subs_active(ssn_entry->asubs_id, FALSE);
        policyman_state_delete_uim_info(ssn_entry->asubs_id, ssn_entry->session_type);
      }
    }
    break;

    case MMGSDI_REFRESH_EVT:
    {
      mmgsdi_return_enum_type ret = MMGSDI_ERROR;
      POLICYMAN_MSG_HIGH("PM received MMGSDI_REFRESH_EVT for session %d, mode %d, stage %d",
                         ssn_entry->session_type,
                         pMmgsdi_event->data.refresh.mode,
                         pMmgsdi_event->data.refresh.stage);

      if (pMmgsdi_event->data.refresh.mode == MMGSDI_REFRESH_NAA_FCN)
      {
        if (pMmgsdi_event->data.refresh.stage == MMGSDI_REFRESH_STAGE_START)
        {
          pm_read_imsi_hplmn(ssn_entry->session_type);
        }
        else if (pMmgsdi_event->data.refresh.stage == MMGSDI_REFRESH_STAGE_WAIT_FOR_OK_TO_FCN)
        {
          //Notify MMGSDI it's okay to proceed with FCN refresh
          ret = mmgsdi_session_ok_to_refresh(pMmgsdi_event->session_id,
                                             TRUE,
                                             pm_mmgsdi_refresh_cb,
                                              0
                                             );
        }
      }
      else if ((pMmgsdi_event->data.refresh.mode == MMGSDI_REFRESH_NAA_INIT_FCN) &&
               (pMmgsdi_event->data.refresh.stage == MMGSDI_REFRESH_STAGE_WAIT_FOR_OK_TO_FCN))
      {
        //Notify MMGSDI it's okay to proceed with FCN refresh
        ret = mmgsdi_session_ok_to_refresh(pMmgsdi_event->session_id,
                                           TRUE,
                                           pm_mmgsdi_refresh_cb,
                                           0
                                         );
      }
      if ( ret != MMGSDI_SUCCESS )
      {
        POLICYMAN_MSG_ERROR(" PM: mmgsdi_session_ok_to_refresh returned error  %d",ret,0,0);
      }
    }
    break;

    default:
     break;
  }
}

/*===========================================================================
  FUNCTION policyman_dtor_card_status()

  DESCRIPTION
 ===========================================================================*/
void policyman_dtor_card_status
(
  policyman_cmd_t *pCmd
)
{
   /* Free the memory for the array of items. */
   if (((policyman_uim_evt_cb_s*)pCmd)->mmgsdi_event.evt == MMGSDI_REFRESH_EVT)
     POLICYMAN_MEM_FREE_IF(((policyman_uim_evt_cb_s*)pCmd)->mmgsdi_event.data.refresh.refresh_files.file_path_ptr);
}

/*===========================================================================
  FUNCTION pm_mmgsdi_reg_status_cb()

  DESCRIPTION
 ===========================================================================*/
static void pm_mmgsdi_reg_status_cb(mmgsdi_return_enum_type status,
                                  mmgsdi_cnf_enum_type    data_type,
                                  const mmgsdi_cnf_type  *data_ptr)
{
   pm_mmgsdi_resp_s_type *pCmd = NULL;

   PM_CHECK_PTR_RET(data_ptr, VOID);

   pCmd = (pm_mmgsdi_resp_s_type *) policyman_cmd_new( sizeof(pm_mmgsdi_resp_s_type),
                                                       pm_process_mmgsdi_reg_status,
                                                       NULL,
                                                       NULL
                                                     );

   pCmd->data_type = data_type;
   pCmd->status = status;
   pCmd->data = *data_ptr;

   policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
   ref_cnt_obj_release(pCmd);

}

/*===========================================================================
  FUNCTION pm_mmgsdi_card_status_cb()

  DESCRIPTION
 ===========================================================================*/
static void pm_mmgsdi_card_status_cb(
                                     const mmgsdi_event_data_type   *mmgsdi_event
  )
{
   policyman_uim_evt_cb_s *pCmd = NULL;

   PM_CHECK_PTR_RET(mmgsdi_event, VOID);

   pCmd = (policyman_uim_evt_cb_s *) policyman_cmd_new( sizeof(policyman_uim_evt_cb_s),
                                          pm_process_uim_evt,
                                          policyman_dtor_card_status,
                                          NULL
                                          );

   pCmd->mmgsdi_event = *mmgsdi_event;

   if (mmgsdi_event->evt == MMGSDI_REFRESH_EVT)
   {
      //Init this in case number of files is zero
      pCmd->mmgsdi_event.data.refresh.refresh_files.file_list_ptr = NULL;

      if ((mmgsdi_event->data.refresh.refresh_files.file_path_len > 0) &&
          (mmgsdi_event->data.refresh.refresh_files.file_path_ptr != NULL))
      {
        uint32 list_size = mmgsdi_event->data.refresh.refresh_files.file_path_len;

        //Need to copy FCN refresh path list seperately.
        //Need to copy only paths, since we always get only paths
        pCmd->mmgsdi_event.data.refresh.refresh_files.file_path_ptr = (uint8*)policyman_mem_alloc(list_size);

        PM_CHECK_PTR_RET(pCmd->mmgsdi_event.data.refresh.refresh_files.file_path_ptr, VOID);

        memscpy( pCmd->mmgsdi_event.data.refresh.refresh_files.file_path_ptr,list_size,
                 mmgsdi_event->data.refresh.refresh_files.file_path_ptr,list_size
               );
      }
   }
   
   policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
   ref_cnt_obj_release(pCmd);

}


/*===========================================================================
  FUNCTION pm_open_session_cb()

  DESCRIPTION
 ===========================================================================*/
void pm_open_session_cb (mmgsdi_return_enum_type status,
                                  mmgsdi_cnf_enum_type    data_type,
                                  const mmgsdi_cnf_type  *data_ptr)
{
  uint16 i = 0,j = 0;

  /* data_ptr is NULL for ICC card where open_session returns success */
  PM_CHECK_PTR_RET(data_ptr, VOID);
  PM_CHECK_PTR_RET(data_ptr->session_open_ext_cnf.session_info_ptr, VOID);

  if ( status == MMGSDI_SUCCESS )
  {
    if (data_type == MMGSDI_SESSION_OPEN_EXT_CNF)
    {
      for (i = 0; i < data_ptr->session_open_ext_cnf.num_sessions; i++)
      {
        for (j = 0; j < ARR_SIZE(pm_mmgsdi_session_id_table); j++)
        {
          if(pm_mmgsdi_session_id_table[j].session_type == data_ptr->session_open_ext_cnf.session_info_ptr[i].session_type)
          {
            pm_mmgsdi_session_id_table[j].session_id = data_ptr->session_open_ext_cnf.session_info_ptr[i].session_id;
            pm_mmgsdi_session_id_table[j].app_type = data_ptr->session_open_ext_cnf.session_info_ptr[i].app_info.app_type;
            pm_mmgsdi_session_id_table[j].in_use = TRUE;

            POLICYMAN_MSG_HIGH("num_sessions %d session_id %d session_type %d",
                               data_ptr->session_open_ext_cnf.num_sessions,
                               pm_mmgsdi_session_id_table[j].session_id,
                               pm_mmgsdi_session_id_table[j].session_type
                              );
          }
        }
      }
    }
  }
}

/*===========================================================================
  FUNCTION pm_open_sessions()

  DESCRIPTION
 ===========================================================================*/
static void pm_open_sessions (void)
{
  mmgsdi_return_enum_type result;
  uint32        session_type_mask = (MMGSDI_GW_PROV_PRI_SESSION_MASK|MMGSDI_1X_PROV_PRI_SESSION_MASK|MMGSDI_GW_PROV_SEC_SESSION_MASK);
  result = mmgsdi_session_open_ext(pm_mmgsdi_client_id,
                                    session_type_mask,
                                    pm_mmgsdi_card_status_cb,
                                    FALSE,
                                    pm_open_session_cb,
                                     (mmgsdi_client_data_type)session_type_mask
                                    );

  if ( MMGSDI_SUCCESS != result )
  {
    /* log that error and return */
    POLICYMAN_MSG_ERROR("PM session open failed with error %d",result,0,0);
  }
}

/*===========================================================================
 FUNCTION policyman_uim_get_num_subs()

 DESCRIPTION
===========================================================================*/
uint32
policyman_uim_get_num_subs
(
  void
)
{
  policyman_device_info_t *pInfo = policyman_state_get_state()->pDeviceInfo;

  return (pInfo != NULL) ? pInfo->uimNSubs : 0;
}

/*===========================================================================
 FUNCTION policyman_uim_set_num_subs()

 DESCRIPTION
===========================================================================*/
void policyman_uim_set_num_subs
(
  policyman_state_t  *pInfo,
  uint32                nSubs
  )
{
  if (pInfo != NULL && nSubs <= MAX_SIMS)
  {
    pInfo->pDeviceInfo->uimNSubs = (size_t)nSubs;
  }
}


/*===========================================================================
 FUNCTION policyman_uim_get_imsi_mcc()

 DESCRIPTION
===========================================================================*/
uint32
policyman_uim_get_imsi_mcc(
  policyman_state_t  *pInfo,
  size_t                subsId
  )
{
  return (pInfo != NULL && subsId < MAX_SIMS)?
                policyman_plmn_get_mcc(&pInfo->pSubsState[subsId]->pUimInfo->plmn) : 0;
}


/*===========================================================================
 FUNCTION policyman_uim_get_imsi_plmn()

 DESCRIPTION
===========================================================================*/
boolean
policyman_uim_get_imsi_plmn(
  policyman_state_t  *pInfo,
  size_t                subsId,
  sys_plmn_id_s_type    *pPlmn
  )
{
  if (pInfo == NULL || subsId >= MAX_SIMS)    { return FALSE; }

  *pPlmn = pInfo->pSubsState[subsId]->pUimInfo->plmn;

  return TRUE;
}

/*===========================================================================
 FUNCTION policyman_uim_set_imsi_plmn()

 DESCRIPTION
   Set IMSI PLMN into UIM state
===========================================================================*/
boolean
policyman_uim_set_imsi_plmn(
  policyman_state_t  *pInfo,
  size_t                subsId,
  sys_plmn_id_s_type    *pPlmn
  )
{
  boolean changed = FALSE;

  if (pInfo == NULL)    { return changed; }

  if (
        pPlmn != NULL
     && subsId < MAX_SIMS
     && !policyman_plmns_are_equal(&pInfo->pSubsState[subsId]->pUimInfo->plmn, pPlmn)
     )
  {
    POLICYMAN_MSG_HIGH("updating PLMN for Sub %d",subsId, 0, 0);
    pInfo->pSubsState[subsId]->pUimInfo->plmn = *pPlmn;
    changed = TRUE;
  }

  return changed;
}

/*===========================================================================
 FUNCTION policyman_uim_get_sim_type()

 DESCRIPTION
   Get SIM app type from UIM state
===========================================================================*/
void
policyman_uim_get_sim_type(
  policyman_state_t  *pInfo,
  size_t                subsId,
  mmgsdi_app_enum_type *sim_app_type
  )
{
  if(sim_app_type && pInfo)
  {
    *sim_app_type = pInfo->pSubsState[subsId]->pUimInfo->sim_app_type;
  }
  else
  {
    POLICYMAN_MSG_ERROR("sim_app_type is NULL", 0, 0, 0);
  }
}



/*=============================================================================
  Conditions based on serving system.
=============================================================================*/

#define MAX_UIM_CONDITION_SET_SIZE   50


/*-----------------------------------------------------------------------------
  condition_imsi_mcc
-----------------------------------------------------------------------------*/


typedef struct
{
  POLICYMAN_CONDITION_BASE;

  size_t          subsId;
  policyman_set_t *pMccSet;
  char const      *pSetName;
} policyman_imsi_mcc_condition_t;




/*-------- policyman_condition_imsi_mcc_evaluate --------*/
static boolean policyman_condition_imsi_mcc_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_imsi_mcc_condition_t  *pCond;
  policy_execute_ctx_t            *piCtx = (policy_execute_ctx_t *) pCtx;
  uint32                          uimMcc = 0;
  boolean                         result = FALSE;

  pCond = (policyman_imsi_mcc_condition_t *) pCondition;

  /*  If we don't have a set but *do* have a set name, try to find the named set.
   */

  if (pCond->pMccSet == NULL && pCond->pSetName != NULL)
  {    
    pCond->pMccSet = (policyman_set_t *) mre_named_object_find(
                            pCond->pSetName,
                            POLICYMAN_NAMED_MCC_SET,
                            pCondition->pPolicy
                            );
    POLICYMAN_MEM_FREE_IF(pCond->pSetName);

    if (NULL != pCond->pMccSet)
    {
      ref_cnt_obj_add_ref(pCond->pMccSet);
    }

  }

  /*  Without a set, we can't work.
   */
  if (pCond->pMccSet == NULL)             
  { 
    POLICYMAN_MSG_ERROR_0("named PLMN set not found");
    goto Done;
  }

  uimMcc = policyman_state_get_uim_mcc(piCtx->pState, pCond->subsId);

  result =  policyman_set_contains(pCond->pMccSet, &uimMcc);

Done:
  POLICYMAN_MSG_HIGH_2("condition <imsi_mcc_in> with mcc %d returns %d", uimMcc, result);
  return result;
}


/*-------- policyman_condition_imsi_mcc_dtor --------*/
static void
policyman_condition_imsi_mcc_dtor(
  void  *pObj
  )
{
  policyman_imsi_mcc_condition_t  *pCondition =
                                    (policyman_imsi_mcc_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pMccSet);
  POLICYMAN_MEM_FREE_IF(pCondition->pSetName);
  policyman_condition_dtor(pCondition);
}


/*-------- policyman_condition_imsi_mcc_new --------*/
mre_status_t
policyman_condition_imsi_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                    status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_imsi_mcc_condition_t  *pCondition = NULL;
  policyman_set_t                 *pMccSet = NULL;
  char const                      *pSetName = NULL;
  char const                      *pStr;
  char                            mccStr[4];
  sys_mcc_type                    mcc;
  sys_modem_as_id_e_type          subs;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  /*  See if there is a named MCC list to use for this condition.
   */
  pStr = policyman_xml_get_attribute(pElem, "list");
  if (pStr != NULL)
  {
    pSetName = policyman_str_dup(pStr);
  }
  else
  {
    /* No named set; try and get an inline list
     */
    pStr = policyman_xml_get_text(pElem);
    if (pStr != NULL)
    {
      /* Create a policyman set to hold the MCCs
       */
      pMccSet = policyman_set_new(sizeof(mcc), 5, MAX_UIM_CONDITION_SET_SIZE, NULL, NULL);

      /* Add the MCCs to the set
       */
      while (policyman_get_token(&pStr, mccStr, sizeof(mccStr)))
      {
        mcc = atoi(mccStr);
        if (mcc == 0)
        {
          REF_CNT_OBJ_RELEASE_IF(pMccSet);
          POLICYMAN_UT_MSG_1("<imsi_mcc_in>: invalid MCC: %s", mccStr);
          goto Done;
        }

        policyman_set_add(pMccSet, &mcc);
      }
    }
  }

  /* If there is neither an inline or named list, bail.
   */
  if (pMccSet == NULL && pSetName == NULL)
  {
    POLICYMAN_MSG_ERROR_0("<imsi_mcc_in>: no inline MCC or named list");
    goto Done;
  }

  /* Create the condition
   */
  pCondition = (policyman_imsi_mcc_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_imsi_mcc_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_imsi_mcc_dtor);
  pCondition->pMccSet = pMccSet;
  pCondition->pSetName = pSetName;
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_imsi_mcc_evaluate;
  pCondition->subsId = (size_t) subs;

  status = MRE_STATUS_SUCCESS;

Done:
  *ppCondition = (policyman_condition_t *) pCondition;
  return status;
}



/*-----------------------------------------------------------------------------
  condition_imsi_plmn
-----------------------------------------------------------------------------*/


typedef struct
{
  POLICYMAN_CONDITION_BASE;

  size_t          subsId;
  char const      *pSetName;
  policyman_set_t *pPlmnSet;
} policyman_imsi_plmn_condition_t;



/*-------- policyman_condition_imsi_plmn_evaluate --------*/
static boolean policyman_condition_imsi_plmn_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_imsi_plmn_condition_t *pCond;
  policy_execute_ctx_t            *piCtx = (policy_execute_ctx_t *) pCtx;
  sys_plmn_id_s_type               uimPlmn;
  boolean ret;

  pCond = (policyman_imsi_plmn_condition_t *) pCondition;

  if (pCond->pPlmnSet == NULL && pCond->pSetName != NULL)
  {
    pCond->pPlmnSet = (policyman_set_t *) mre_named_object_find(
                            pCond->pSetName,
                            POLICYMAN_NAMED_PLMN_SET,
                            pCondition->pPolicy
                            );

    if (NULL != pCond->pPlmnSet)
    {
      ref_cnt_obj_add_ref(pCond->pPlmnSet);
    }
  }

  POLICYMAN_MEM_FREE_IF(pCond->pSetName);
  if (pCond->pPlmnSet == NULL)             { return FALSE; }

  policyman_state_get_uim_plmn(piCtx->pState, pCond->subsId, &uimPlmn);

  ret = policyman_set_contains(pCond->pPlmnSet, &uimPlmn);
  POLICYMAN_MSG_MED("IMSI PLMN: ", 0, 0, 0);
  policyman_plmn_print(&uimPlmn);
  POLICYMAN_MSG_HIGH("condition <imsi_plmn_in> returns %d", ret, 0, 0);
  return ret;
}


/*-------- policyman_condition_imsi_plmn_dtor --------*/
static void
policyman_condition_imsi_plmn_dtor(
  void  *pObj
  )
{
  policyman_imsi_plmn_condition_t *pCondition =
                                    (policyman_imsi_plmn_condition_t *) pObj;

  REF_CNT_OBJ_RELEASE_IF(pCondition->pPlmnSet);
  POLICYMAN_MEM_FREE_IF(pCondition->pSetName);
  policyman_condition_dtor(pCondition);
}


/*-------- policyman_condition_imsi_plmn_new --------*/
mre_status_t
policyman_condition_imsi_plmn_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                    status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_imsi_plmn_condition_t *pCondition = NULL;
  char const                      *pStr;
  sys_modem_as_id_e_type          subs;

  /*  Allocate the condition
   */
  pCondition = (policyman_imsi_plmn_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_imsi_plmn_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_imsi_plmn_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_imsi_plmn_evaluate;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }
  pCondition->subsId = (size_t) subs;

  /*  See if there is a named PLMN list to use for this condition.
   */
  pStr = policyman_xml_get_attribute(pElem, "list");
  if (pStr != NULL)
  {
    pCondition->pSetName = policyman_str_dup(pStr);
    status = MRE_STATUS_SUCCESS;
    goto Done;
  }

  /*  No name - read an inline list.
   */
  pStr = policyman_xml_get_text(pElem);
  if (pStr != NULL)
  {
    pCondition->pPlmnSet = policyman_plmn_list_read(pStr);
    if (pCondition->pPlmnSet != NULL)
    {
      status = MRE_STATUS_SUCCESS;
    }
  }


Done:
  if (MRE_SUCCEEDED(status))
  {
    *ppCondition = (policyman_condition_t *) pCondition;
  }
  else
  {
    POLICYMAN_UT_MSG("<imsi_plmn_in>: PLMN list required", 0, 0, 0);
    REF_CNT_OBJ_RELEASE_IF(pCondition);
  }

  return status;
}


typedef struct
{
  POLICYMAN_CONDITION_BASE;

  size_t          subsId;
  mmgsdi_app_enum_type sim_app_type;
} policyman_sim_type_condition_t;



static boolean
policyman_condition_sim_type_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_sim_type_condition_t *pCond = (policyman_sim_type_condition_t*)pCondition;
  policyman_uim_info_t           *pInfo = policyman_state_get_subs_state(pCond->subsId)->pUimInfo;
  boolean ret = FALSE;

  switch(pCond->sim_app_type)
  {
    case MMGSDI_APP_USIM:
      ret =  pCond->sim_app_type == pInfo ->sim_app_type;
      break;

    case MMGSDI_APP_CSIM:
    case MMGSDI_APP_RUIM:
      ret =  pCond->sim_app_type == pInfo -> cdma_app_type;
      break;

   default:
    break;    
  }

  POLICYMAN_MSG_HIGH("condition <sim_type> with SIM type %d CSIM type %d returns %d", pInfo->sim_app_type, pInfo->cdma_app_type, ret);
  return ret;
}

static mmgsdi_app_enum_type
policyman_str_to_sim_type(
  const char  *pSim_type
  )
{
  mmgsdi_app_enum_type sim_type = MMGSDI_APP_UNKNOWN;
  typedef struct
  {
    const char  *pStr;
    mmgsdi_app_enum_type      sim_type;
  } mask_map;

  static mask_map map[] =
  {
    {"3G",   MMGSDI_APP_USIM},
    {"2G",   MMGSDI_APP_SIM},
    {"CSIM", MMGSDI_APP_CSIM},
    {"RUIM", MMGSDI_APP_RUIM},
    {"NONE", MMGSDI_APP_NONE}
  };

  size_t  i;
  char    token[32];

  policyman_get_token(&pSim_type, token, sizeof(token));

  for (i = 0 ; i < ARR_SIZE(map) ; ++i)
  {
    if (strcasecmp(token, map[i].pStr) == 0)
    {
      sim_type = map[i].sim_type;
      break;
    }
  }

  return sim_type;
}


/*-------- policyman_condition_sim_type_new --------*/
mre_status_t
policyman_condition_sim_type_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                    status = MRE_STATUS_ERR_INVALID_CONDITION;
  policyman_sim_type_condition_t  *pCondition = NULL;
  mmgsdi_app_enum_type            simType;
  char const                      *pStr;
  sys_modem_as_id_e_type          subs;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pStr = policyman_xml_get_text(pElem);
  if (pStr == NULL)
  {
    POLICYMAN_UT_MSG("<sim_type>: must specify SIM type", 0, 0, 0);
    goto Done;
  }

  simType = policyman_str_to_sim_type(pStr);
  if (simType == MMGSDI_APP_UNKNOWN)
  {
    POLICYMAN_UT_MSG("<sim_type>: invalid SIM type: %s", pStr, 0, 0);
    goto Done;
  }

  /*  Allocate the condition
   */
  pCondition = (policyman_sim_type_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_sim_type_condition_t));
  ref_cnt_obj_init(pCondition, policyman_condition_dtor);

  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_sim_type_evaluate;
  pCondition->sim_app_type = simType;
  pCondition->subsId = (size_t) subs;
  status = MRE_STATUS_SUCCESS;

Done:
  *ppCondition = (policyman_condition_t *) pCondition;

  return status;
}
