/**
  @file policyman_call_events.c

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_call_events.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/


#include "policyman_dbg.h"
#include "policyman_call_events.h"
#include "policyman_state.h"
#include "policyman_task.h"
#include "policyman_xml.h"

#include "sys.h"
#include "modem_mem.h"
#include <stringl/stringl.h>


/*=============================================================================
 Data Structure for CM CALL Info
==============================================================================*/
struct policyman_cmcall_info_s
{
  cm_call_event_e_type      call_event;       // Indicates the current call event.
  cm_call_type_e_type       call_type;        // Indicates the call type
};

/*=============================================================================
  Call Event callback definition.
=============================================================================*/
struct policyman_cmcall_evt_cb_s
{
  POLICYMAN_CMD_HDR;
  cm_call_event_e_type   evt;
  cm_mm_call_info_s_type info;
};


/*=============================================================================
  APIs for CM Phone Event information
=============================================================================*/
 static policyman_cmcall_info_t*
 policyman_ss_get_call_info(
   sys_modem_as_id_e_type subsId
)
{
  if(subsId < 0 || subsId >= POLICYMAN_NUM_SUBS)
  {
    POLICYMAN_ERR_FATAL("subsId out of range", 0, 0, 0);
  }
 
  return policyman_state_get_state()->pSubsState[subsId]->pCmCallInfo;
}

/*-------- policyman_cmcall_init --------*/
boolean 
policyman_cmcall_init
(
  policyman_cmcall_info_t **ppCallInfo,
  sys_modem_as_id_e_type  asubs_id
)
{
   policyman_cmcall_info_t *pInfo = (policyman_cmcall_info_t *) policyman_mem_alloc(sizeof(policyman_cmcall_info_t)); 
   pInfo->call_event = CM_CALL_EVENT_NONE;

  *ppCallInfo = pInfo;  
  return (*ppCallInfo != NULL);
}

/*-------- policyman_cmcall_deinit --------*/
void
policyman_cmcall_deinit(
  policyman_cmcall_info_t *pInfo
  )
{
  POLICYMAN_MEM_FREE_IF(pInfo);
}

/*===========================================================================
  FUNCTION POLICYMAN_EXECUTE_PROCESS_CM_CALL_EVT()
 
  DESCRIPTION
    Execute CM Call Set update update and policy check 
 
  RETURN VALUE
    None
 
  DEPENDENCIES
    None
 
  SIDE EFFECTS
    None
===========================================================================*/
void 
policyman_execute_process_cmcall_evt(
  policyman_cmd_t *pCmd
)
{
  struct policyman_cmcall_evt_cb_s *call_evt = (struct policyman_cmcall_evt_cb_s*) pCmd;

  if(call_evt)
  {
    POLICYMAN_MSG_HIGH_3("process cm call subs %d: call event %d, call type %d", 
                          call_evt->info.asubs_id,
                          call_evt->evt,
                          call_evt->info.call_type
                        );

    policyman_state_update_call_event(call_evt->info.asubs_id, call_evt->evt, call_evt->info.call_type);
  }
}

/*===========================================================================
  FUNCTION POLICYMAN_CMCALL_EVENT_CB()

  DESCRIPTION
    Handle CM Call event callback

  PARAMETERS
    evt     : CM Call EVENT type
    p_info : actual payload of CM Call EVENT

  RETURN VALUE
    None

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
void policyman_cmcall_event_cb
( 
  cm_call_event_e_type         evt, 
  const cm_mm_call_info_s_type *p_info
)
{
  struct policyman_cmcall_evt_cb_s *pCmd = NULL;

  POLICYMAN_MSG_HIGH_1("received cm call evt %d", evt);

  pCmd = (struct policyman_cmcall_evt_cb_s *) policyman_cmd_new( sizeof(struct policyman_cmcall_evt_cb_s), 
                                                               policyman_execute_process_cmcall_evt, 
                                                               NULL,
                                                               NULL                               // No policy associated with CM Call Event
                                                             );

  pCmd->evt    = evt;
  pCmd->info = *p_info;

  policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
  ref_cnt_obj_release(pCmd);
}


/*-------- policyman_cmcall_update_state --------*/
boolean
policyman_cmcall_update_state(
  policyman_state_t      *pInfo,
  sys_modem_as_id_e_type subsId,
  cm_call_event_e_type   call_event,
  cm_call_type_e_type    type
  )
{
  boolean                 changed    = FALSE;
  policyman_cmcall_info_t *pCallInfo = policyman_ss_get_call_info(subsId);

  /* For MO call, handle call start/end events. For MT calls, handle call incoming/end events
        All other states are intermediate to start-end and they should be treated as in Call.
   */
  if (    
       (   call_event == CM_CALL_EVENT_ORIG
        || call_event == CM_CALL_EVENT_CONNECT
        || call_event == CM_CALL_EVENT_INCOM
        || call_event == CM_CALL_EVENT_END
       )
       &&
       call_event != pCallInfo->call_event)
  {
    pCallInfo->call_event = call_event;
    pCallInfo->call_type  = type;
    changed = TRUE;

    POLICYMAN_MSG_HIGH_3("updated subs %d: call_event %d, call type %d", 
                          subsId, 
                          call_event, 
                          type
                        );
  }

  return changed;
}


/*=============================================================================
  Call-related conditions
=============================================================================*/

/*-----------------------------------------------------------------------------
  Call Event
-----------------------------------------------------------------------------*/

typedef struct
{
  POLICYMAN_CONDITION_BASE;

  sys_modem_as_id_e_type   subsId;
} policyman_call_event_condition_t;


/*-------- policyman_condition_call_event_evaluate --------*/
static boolean
policyman_condition_call_event_evaluate(
  policyman_condition_t const *pCondition,
  void                        *pCtx
  )
{
  policyman_call_event_condition_t const  *pCond = (policyman_call_event_condition_t *) pCondition;
  policyman_cmcall_info_t          const  *pCallInfo = policyman_ss_get_call_info(pCond->subsId);
  boolean                                 result;

  /* Return TRUE only when Voice or E911 call is going on
   */
  result = (   pCallInfo->call_type == CM_CALL_TYPE_EMERGENCY
            || pCallInfo->call_type == CM_CALL_TYPE_VOICE
           )
           &&
           (   pCallInfo->call_event == CM_CALL_EVENT_ORIG
           || pCallInfo->call_event == CM_CALL_EVENT_INCOM
            || pCallInfo->call_event == CM_CALL_EVENT_CONNECT
           );

  POLICYMAN_MSG_HIGH_2("condition <in_call> with status %d returns %d", pCallInfo->call_event, result);
  return result;
}


/*-------- policyman_condition_call_event_new --------*/
mre_status_t
policyman_condition_call_event_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  )
{
  mre_status_t                      status = MRE_STATUS_ERR_MALFORMED_XML;
  policyman_call_event_condition_t  *pCondition;
  sys_modem_as_id_e_type            subs;

  /* Does this action indicate the subscription
   *  Default is AS_ID_1
   */
  status = policyman_util_get_subs( pElem, &subs);
  if (MRE_FAILED(status))
  {
    goto Done;
  }

  pCondition = (policyman_call_event_condition_t *)
                  policyman_mem_alloc(sizeof(policyman_call_event_condition_t));

  ref_cnt_obj_init(pCondition, policyman_condition_dtor);
  pCondition->isTrue = (pfn_evaluate_t) policyman_condition_call_event_evaluate;
  pCondition->subsId = subs;

  *ppCondition = (policyman_condition_t *) pCondition;

  status = MRE_STATUS_SUCCESS;

Done:
  return status;
}

