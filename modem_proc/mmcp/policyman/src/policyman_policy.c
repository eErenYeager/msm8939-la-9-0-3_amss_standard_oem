/**
  @file policyman_policy.c

  @brief Public interface to Policy Manager
*/

/*
    Copyright (c) 2013-2015 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/policyman_policy.c#8 $
  $DateTime: 2016/01/04 21:06:36 $
  $Author: samudrak $
*/

#include "mre.h"
#include "mre_efs.h"
#include "mre_set.h"
#include "mre_util.h"
#include "mre_engine.h"


#include "policyman_call_events.h"
#include "policyman_cfgitem.h"
#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_plmn.h"
#include "policyman_policy.h"
#include "policyman_rat_capability.h"
#include "policyman_rat_order.h"
#include "policyman_state.h"
#include "policyman_subs.h"
#include "policyman_svc_mode.h"
#include "policyman_task.h"
#include "policyman_tech.h"
#include "policyman_timer.h"
#include "policyman_ue_mode.h"
#include "policyman_uim.h"
#include "policyman_util.h"
#include "policyman_xml.h"

#include "policyman_generic.h"
#include "policyman_sglte_i.h"

#include <stringl/stringl.h>

static  mre_set_t *g_pPolicymanNamedObjects;

/*=============================================================================
  Mappings between condition tags and the functions to create a new condition
  from the XML document.
=============================================================================*/


static mre_condition_map_element_t const  policyman_condition_map[] =
{
  /*---------------------------------------------------------------------------
    Serving system conditions
  ---------------------------------------------------------------------------*/
  {"service_status_in",   policyman_condition_service_status_new,     POLICYMAN_PRECOND_NONE},
  {"srv_domain_has",      policyman_condition_serving_domain_has_new, POLICYMAN_PRECOND_NONE},
  {"serving_mcc_in",      policyman_condition_serving_mcc_new,        POLICYMAN_PRECOND_LOCATION},
  {"serving_plmn_in",     policyman_condition_serving_plmn_new,       POLICYMAN_PRECOND_SS},
  {"serving_rat_in",      policyman_condition_serving_rat_new,        POLICYMAN_PRECOND_SS},
  {"serving_band_in",     policyman_condition_serving_rf_band_new,    POLICYMAN_PRECOND_SS},
  {"reg_reject_cause_in", policyman_condition_reg_reject_new,         POLICYMAN_PRECOND_SS},
  {"location_mcc_in",     policyman_condition_location_mcc_new,       POLICYMAN_PRECOND_NONE},
  {"have_service",        policyman_condition_have_service_new,       POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    UIM conditions
  ---------------------------------------------------------------------------*/
  {"imsi_plmn_in",        policyman_condition_imsi_plmn_new,      POLICYMAN_PRECOND_IMSI},
  {"imsi_mcc_in",         policyman_condition_imsi_mcc_new,       POLICYMAN_PRECOND_IMSI},
  {"sim_type",            policyman_condition_sim_type_new,       POLICYMAN_PRECOND_SIMTYPE},
  {"imei",                policyman_condition_imei_new,           POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Current state conditions
  ---------------------------------------------------------------------------*/
  {"ue_mode_is",          policyman_condition_ue_mode_new,          POLICYMAN_PRECOND_NONE},
  {"svc_mode_is",         policyman_condition_svc_mode_new,         POLICYMAN_PRECOND_NONE},
  {"num_subs",            policyman_condition_num_subs_new,         POLICYMAN_PRECOND_NONE},
  {"is_subs_mmode",       policyman_condition_is_subs_multimode_new,POLICYMAN_PRECOND_NONE},
  {"enforce_full_rat",    policyman_condition_enforce_full_rat_new, POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    User preference conditions
  ---------------------------------------------------------------------------*/
  {"user_domain_pref",        policyman_condition_ph_domain_pref_new,             POLICYMAN_PRECOND_NONE},
  {"user_mode_pref_contains", policyman_condition_ph_rat_pref_new,                POLICYMAN_PRECOND_NONE},
  {"user_mcc_in",             policyman_condition_ph_user_mcc_new,                POLICYMAN_PRECOND_NONE},
  {"network_selection_mode",  policyman_condition_ph_network_selection_mode_new,  POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Timer conditions
  ---------------------------------------------------------------------------*/
  {"timer_expired",       policyman_condition_timer_expired_new,  POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Phone Operating Mode conditions
  ---------------------------------------------------------------------------*/
  {"phone_operating_mode",policyman_condition_ph_operating_mode_new,POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Technology conditions
  ---------------------------------------------------------------------------*/
  {"tech_loaded",         policyman_condition_tech_loaded_new,      POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Call conditions
  ---------------------------------------------------------------------------*/
  {"in_call",            policyman_condition_call_event_new,        POLICYMAN_PRECOND_NONE},

  /*---------------------------------------------------------------------------
    Terminator
  ---------------------------------------------------------------------------*/
  {NULL, NULL, POLICYMAN_PRECOND_NONE}
};



/*=============================================================================
  Mappings between action tags and the functions to create a new action
  from the XML document.
=============================================================================*/

static mre_action_map_element_t const  policyman_action_map[] =
{
  /*  Configuration item actions.
   */
  {"device_configuration",    policyman_device_config_action_new},
  {"device_configuration_if", policyman_device_config_if_action_new},
  {"define_config",           policyman_device_config_define_new},
  {"use_config",              policyman_device_config_use_new},
  {"use_config_if",           policyman_device_config_use_if_new},
  {"rat_capability",          policyman_rat_capability_action_new},
  {"rat_capability_if",       policyman_rat_capability_if_action_new},
  {"ue_mode",                 policyman_ue_mode_action_new},
  {"ue_mode_if",              policyman_ue_mode_if_action_new},
  {"rf_bands",                policyman_rf_bands_action_new},
  {"rf_bands_if",             policyman_rf_bands_if_action_new},
  {"svc_mode",                policyman_svc_mode_action_new},
  {"feature",                 policyman_device_config_feature_new},
  {"rat_order",               policyman_rat_order_action_new},

  /*  Actions to define named objects
   */
  {"plmn_list",               policyman_plmn_list_new},
  {"rf_band_list",            policyman_rf_band_list_action_new},
  {"mcc_list",                policyman_mcc_list_new},

  /*---------------------------------------------------------------------------
    Timer actions
  ---------------------------------------------------------------------------*/
  {"define_timer",            policyman_named_timer_action_new},
  {"expired_timer_handled",   policyman_expired_timer_handled_new},
  {"timer_start",             policyman_timer_start_new},
  {"timer_stop",              policyman_timer_stop_new},

  /*  Actions that affect state of system
   */
  {"exclude_tech",            policyman_tech_exclude_new},
};


/*=============================================================================
  Mappings between precondition tags and their corresponding bitmasks
=============================================================================*/

static mre_precond_map_element_t policyman_precond_map[] =
{
  {"IMSI",        POLICYMAN_PRECOND_IMSI},
  {"SS",          POLICYMAN_PRECOND_SS},
  {"SIM_TYPE",    POLICYMAN_PRECOND_SIMTYPE},
  {"CDMASIMTYPE", POLICYMAN_PRECOND_CDMASIMTYPE},
  {"LOCATION",    POLICYMAN_PRECOND_LOCATION},
  {NULL,          0}
};



/*-------- g_policyVtbls --------*/
/**
@brief  Pointers to the vtables of the defined internal policies.
*/
static  policyman_policy_vtbl_t *g_policyVtbls[] =
{
  &policyman_policy_generic_vtbl,
  &policyman_policy_sglte_vtbl,
};

static  policyman_set_t   *g_pPolicySet;
static  atomic_word_t     g_suspendCount;
static  boolean           is_policy_init;




#if defined(TEST_FRAMEWORK)
#error code not present
#endif /* +TEST_FRAMEWORK. */


/*-------- policyman_set_periodic_plmn_search_allowed --------*/
/**
@brief  Present here to unblock QMI_NAS - will get a real implementation
        in policyman_network_events.c when network scan code gets checked in.
*/
void
policyman_set_periodic_plmn_search_allowed(
  sys_modem_as_id_e_type  subsId,
  boolean                 isAllowed
  )
{
}


/*-------- policyman_policy_dtor --------*/
void
policyman_policy_dtor(
  void  *pObj
  )
{
  mre_policy_dtor(pObj);
}


/*-------- policyman_str_to_pin_to_subs --------*/
static sys_modem_as_id_e_type
policyman_str_to_pin_to_subs(
  char const *pStr
  )
{
  typedef struct
  {
    char const              *pStr;
    sys_modem_as_id_e_type  subs;
  } pin_to_sub_elem_t;

  static pin_to_sub_elem_t  str_to_sub[] =
  {
    {"none",      SYS_MODEM_AS_ID_NO_CHANGE}, /* Used as the "not pinned" value */
    {"primary",   SYS_MODEM_AS_ID_1},
    {"secondary", SYS_MODEM_AS_ID_2},
  };
  size_t i;

  for (i = 0 ; i < ARR_SIZE(str_to_sub) ; ++i)
  {
    if (strcasecmp(pStr, str_to_sub[i].pStr) == 0)
    {
      return str_to_sub[i].subs;
    }
  }

  /*  Return a value that will be interpreted as a parse error.
   */
  return SYS_MODEM_AS_ID_NONE;
}


/*-------- policyman_policy_get_subs --------*/
static policyman_status_t
policyman_policy_get_subs(
  policyman_policy_t            *pPolicy,
  const policyman_xml_element_t *pElement
  )
{
  policyman_status_t      status = POLICYMAN_STATUS_SUCCESS;
  const char              *pStr;
  sys_modem_as_id_e_type  subsId;

  pStr = policyman_xml_get_attribute(pElement, "pin_to_subs");

  /*  If there is no attribute, use the "not pinned" value of the subs ID.
   */
  if (pStr == NULL)
  {
    pPolicy->pin_to_subs = SYS_MODEM_AS_ID_NO_CHANGE;
    goto Done;
  }

  /*  Get the subscription to be used.  SYS_MODEM_AS_ID_NONE means there was
   *  a parse error.
   */
  subsId = policyman_str_to_pin_to_subs(pStr);
  if (subsId == SYS_MODEM_AS_ID_NONE)
  {
    status = POLICYMAN_STATUS_ERR_MALFORMED_XML;
    goto Done;
  }

  pPolicy->pin_to_subs = subsId;

Done:
  return status;
}


/*-------- policyman_policy_disable--------*/
void policyman_policy_disable(
  policyman_policy_t              *pPolicy,
  policyman_policy_error_cause     reason
)
{
  MRE_POLICY_ENABLED((mre_policy_t *) pPolicy) = FALSE;
  POLICYMAN_POLICY_ERROR_CAUSE(pPolicy) = reason;
}



/*-------- policyman_policy_initialize_policy --------*/
/**
@brief  Read the policy_info file for this policy and use it to initialize
        the policy.

@param[in]  pPolicy   Pointer to the policy.

@return
  Nothing
*/
static boolean policyman_policy_initialize_policy(
  policyman_policy_t  *pPolicy,
  policyman_state_t   *pState
  )
{
  policyman_set_t       *pItemSet = NULL;
  policy_execute_ctx_t  ctx;

  /*  If we're passed a NULL pointer for the state, we're just running
   *  an XML validation test.  Finish here.
   */
  if (pState == NULL)               { goto Done; }

  /*  Configure the policy, setting any initial configuration items.
   */
  pItemSet = policyman_itemset_new();
  ctx.pItemSet = pItemSet;
  ctx.pState = pState;
  ctx.asubs_id = policyman_get_current_multimode_subs();

  mre_policy_init(
        (mre_policy_t *) pPolicy,
        &g_pPolicymanNamedObjects,
        &ctx
        );

  POLICY_CONFIGURE(pPolicy, &ctx);

  if (!policyman_feature_is_supported_by_hardware(pItemSet))
  {
    POLICYMAN_MSG_HIGH_0("========== Mismatch in XML and device capabilities ==========");
    policyman_policy_disable(pPolicy, POLICYMAN_POLICY_ERROR_FEATURE_MISMATCH);
    goto Done;
  }

  /* Update the database and send client notification */
  policyman_cfgitem_update_items(pItemSet, ctx.asubs_id);
  
  POLICYMAN_MSG_HIGH_0("---------- Initial database after initialization");
  policyman_cfgitem_display_all();
  POLICYMAN_MSG_SEPARATOR(POLICYMAN_SEPARATOR_HYPHEN);


Done:
  
  REF_CNT_OBJ_RELEASE_IF(pItemSet);
  return TRUE;
}


static boolean  policyman_init_succeeded;


/*-------- policyman_policy_init_succeeded --------*/
boolean
policyman_policy_init_succeeded(
  void
  )
{
  return policyman_init_succeeded;
}



/*-------- policyman_policy_files --------*/
static char const  *policyman_policy_files[] =
{
  "/policyman/device_config.xml",
  "/policyman/carrier_policy.xml",
  "/policyman/rat_order.xml",
  "/policyman/restrictions.xml",
  "/policyman/segment_loading.xml",
};


/*-------- policyman_policy_file_enum_init --------*/
void
policyman_policy_file_enum_init(
  policyman_policy_enum_ctx *pCtx
  )
{
  *pCtx = 0;
}


/*-------- policyman_policy_file_enum_next --------*/
char const *
policyman_policy_file_enum_next(
  policyman_policy_enum_ctx *pCtx
  )
{
  policyman_policy_enum_ctx index = *pCtx;
  
  if (index >= ARR_SIZE(policyman_policy_files))
  {
    return NULL;
  }

  *pCtx = index + 1;
  return policyman_policy_files[index];
}



/*-------- policyman_policy_new_policy_by_name --------*/
static policyman_policy_t *
policyman_new_policy_by_name(
  const char  *pName
  )
{
  size_t              i;
  policyman_policy_t  *pPolicy = NULL;

  for (i = 0 ; i < ARR_SIZE(g_policyVtbls) ; ++i)
  {
    pPolicy = g_policyVtbls[i]->newInstance(pName);
    if (pPolicy != NULL)
    {
      pPolicy->pVtbl = g_policyVtbls[i];
      break;
    }
  }

  return pPolicy;
}


/*-------- policyman_new_policy --------*/
static mre_policy_t *
policyman_new_policy(
    char              const *pPolicyName,
    mre_xml_element_t const *pPolicyElem
    )
{
  policyman_policy_t  *pPolicy = policyman_new_policy_by_name(pPolicyName);
  
  if (NULL != pPolicy)
  {
    POLICYMAN_MSG_HIGH_1("Policyman: created policy %d", pPolicy->id);

    /*  Determine whether there is a specific subscription on which this
     *  policy should run.
     */
    policyman_policy_get_subs(pPolicy, pPolicyElem);
  }

  return (mre_policy_t *) pPolicy;
}


/*-------- policyman_policy_init --------*/
boolean
policyman_policy_init(
  policyman_state_t *pState
  )
{
  policyman_policy_enum_ctx fileCtx;
  char const                *pFileName;
  mre_status_t              status;
  size_t                    numFiles = 0;
  policyman_policy_t        *pPolicy;

  /* Return FALSE if policy already initialized
   */
  if (is_policy_init)
  {
    return FALSE;
  }
  
  /*  Initialize g_suspendCount to 0.
   */
  atomic_init(&g_suspendCount, 0);

  /*  Create the set to hold the policies.
   */
  g_pPolicySet = policyman_set_refcnt_new(3, POLICYMAN_SET_NO_MAX_SIZE, NULL);

  /*  Named object set is not present unless objects are added.
   */
  g_pPolicymanNamedObjects = NULL;
  
  /*  Load all XML files that are in the policyman directory.
   */
  policyman_policy_file_enum_init(&fileCtx);
  while ((pFileName = policyman_policy_file_enum_next(&fileCtx)) != NULL)
  {
    POLICYMAN_UT_MSG("Loading policy file %s", pFileName, 0, 0);

    status = mre_policy_create(
                pFileName,
                policyman_condition_map,
                policyman_action_map,
                policyman_precond_map,
                policyman_new_policy,
                (mre_policy_t **) &pPolicy
                );

    if (MRE_STATUS_SUCCESS == status)
    {
      ++numFiles;
      policyman_set_add(g_pPolicySet, &pPolicy);
      ref_cnt_obj_release(pPolicy);

      policyman_policy_initialize_policy(pPolicy, pState);
    }
  }

  is_policy_init = (numFiles == 0 || policyman_set_num_elems(g_pPolicySet) > 0);
  return is_policy_init;
}



/*-------- policyman_policy_deinit --------*/
void
policyman_policy_deinit(
  void
  )
{
  is_policy_init = FALSE;
  REF_CNT_OBJ_RELEASE_IF(g_pPolicySet);
}


/*-------- policyman_policy_are_preconditions_met --------*/
boolean
policyman_policy_are_preconditions_met(
  policyman_policy_t  *pPolicy,
  policyman_state_t   *pState
  )
{
  sys_modem_as_id_e_type  asubs_id = policyman_get_current_multimode_subs();

  return mre_policy_are_preconds_met(
                (mre_policy_t *) pPolicy,
                policyman_state_get_subs_precond(asubs_id)
                );
}


/*-------- policyman_policy_subs_check --------*/
static boolean
policyman_policy_subs_check(
  sys_modem_as_id_e_type subsId
  )
{
  if(  subsId == SYS_MODEM_AS_ID_NONE
     || subsId >= (sys_modem_as_id_e_type) POLICYMAN_NUM_SUBS
    )
  {
    POLICYMAN_MSG_ERROR_1("Unsupported subsId %d", subsId);
    return FALSE;
  }

  return TRUE;
}
  
/*-------- policyman_policy_execute --------*/
void policyman_policy_execute(
  policyman_policy_t    *pPolicy,
  policy_execute_ctx_t  *pCtx
  )
{
  uint32                precondsMet;

  if (    pPolicy->pin_to_subs != SYS_MODEM_AS_ID_NO_CHANGE /* policy pinned to a subs */
      &&  pPolicy->pin_to_subs != pCtx->asubs_id            /* but this isn't the subs */
     )
  {
    POLICYMAN_MSG_HIGH_2("Policy executed for subs %d, but restricted to subs %d",
                          pCtx->asubs_id, 
                          pPolicy->pin_to_subs );
    return;
  }

  /* Don't allow rules to execute if current subs is out of range
   */
  if (!policyman_policy_subs_check(pCtx->asubs_id))
  {
    return;
  }

  precondsMet = policyman_state_get_subs_precond(pCtx->asubs_id);

  if (POLICYMAN_POLICY_ERROR_CAUSE(pPolicy) == POLICYMAN_POLICY_ERROR_FEATURE_MISMATCH)
  {
    POLICYMAN_MSG_ERROR_1("Policyman feature does not match TRM capabilities of %x", policyman_state_get_trm_cap());
    POLICYMAN_MSG_HIGH_3("----------- Error in policy version %d.%d.%d -----------",
                         MRE_POLICY_NUMBER((mre_policy_t *)pPolicy),
                         MRE_POLICY_TYPE((mre_policy_t *)pPolicy),
                         MRE_POLICY_VERSION((mre_policy_t *)pPolicy));
  }

  mre_policy_execute((mre_policy_t *) pPolicy, precondsMet, pCtx);
}

/*-------- policyman_policy_run --------*/
static boolean policyman_policy_run(
  void  *pObj,
  void  *pData1,
  void  *pData2
  )
{
  policyman_policy_t    *pPolicy = *((policyman_policy_t **) pObj);
  policy_execute_ctx_t  *pCtx    = (policy_execute_ctx_t *) pData1;

  policyman_policy_execute(pPolicy, pCtx);

  if (POLICY_CAN_EXECUTE(pPolicy))
  {
    POLICY_EXECUTE(pPolicy, pCtx);
  }

  return TRUE;  
}

/*-------- policyman_policy_can_policy_execute --------*/
STATIC boolean policyman_policy_can_policy_execute(
  void
)
{
  // return FALSE if suspended or in test mode
  if (policyman_is_test_mode() || policyman_policy_is_suspended())
  {
    POLICYMAN_MSG_ERROR_1("test mode is enabled or policy suspended - g_suspendCount %d", g_suspendCount.value);
    return FALSE;
  }

  return TRUE;
}

/*-------- policyman_policyset_execute --------*/
STATIC void policyman_policyset_execute(
  policyman_state_t      *pState,
  sys_modem_as_id_e_type  subsId,
  boolean                 haveService,
  elem_iter_fn_t          pfnIter
)
{
  policy_execute_ctx_t    ctx;
  policyman_set_t        *pItemSet;

  pItemSet        = policyman_itemset_new();
  ctx.pState      = pState;
  ctx.pItemSet    = pItemSet;
  ctx.asubs_id    = subsId;
  ctx.haveService = haveService;

  policyman_set_iterate(g_pPolicySet, pfnIter, &ctx, NULL);
  policyman_cfgitem_update_items(pItemSet, subsId);
  ref_cnt_obj_release(pItemSet);
}

/*-------- policyman_policy_run_policy_check --------*/
/**
@brief  Run a policy check and collect updates into the itemset passed.

@param

@return
  
*/
void policyman_policy_run_policy_check(
  policyman_state_t       *pState,
  sys_modem_as_id_e_type  asubs_id,
  policyman_set_t         *pItemSet
  )
{
 
  if (policyman_policy_can_policy_execute())
  {
    policy_execute_ctx_t  ctx;

  ctx.pState    = pState;
  ctx.pItemSet  = pItemSet;
  ctx.asubs_id  = asubs_id;
    policyman_set_iterate(g_pPolicySet, policyman_policy_run, &ctx, NULL);
  }
}


/*-------- policy_notify_service --------*/
static boolean policy_notify_service(
  void  *pObj,
  void  *pData1,
  void  *pData2
  )
{
  policyman_policy_t    *pPolicy = *((policyman_policy_t **) pObj);
  policy_execute_ctx_t  *pCtx = (policy_execute_ctx_t *) pData1;

  if (    POLICY_IS_ENABLED(pPolicy)
      &&  POLICY_CAN_NOTIFY_SERVICE(pPolicy)
     )
  {
    POLICY_NOTIFY_SERVICE(pPolicy, pCtx);
  }

  return TRUE;
}


/*-------- policyman_policy_notify_service --------*/
void policyman_policy_notify_service(
  boolean haveService
  )
{
  if (policyman_policy_can_policy_execute())
  {
    policyman_state_t      *pState;
    sys_modem_as_id_e_type  mmodeSubs;

    pState    = policyman_state_get_state();
    mmodeSubs = policyman_get_current_multimode_subs();

    policyman_policyset_execute(pState, mmodeSubs, haveService, policy_notify_service);
  }
}


/*-------- policy_handle_user_pref_update --------*/
static boolean policy_handle_user_pref_update(
  void  *pObj,
  void  *pData1,
  void  *pData2
  )
{
  policyman_policy_t    *pPolicy = *((policyman_policy_t **) pObj);
  policy_execute_ctx_t  *pCtx = (policy_execute_ctx_t *) pData1;

  if (  POLICY_IS_ENABLED(pPolicy) 
     && POLICY_CAN_HANDLE_USERPREF_UPDATE(pPolicy)
     )
  {
    POLICY_HANDLE_USERPREF_UPDATE(pPolicy, pCtx );
  }

  return TRUE;
}


/*-------- policyman_policy_handle_user_pref_update --------*/
void  policyman_policy_handle_user_pref_update(
  sys_modem_as_id_e_type asubs_id
)
{
  if (policyman_policy_can_policy_execute())
  { 
    policyman_state_t  *pState;
    boolean             haveService = TRUE;

    pState = policyman_state_get_state();
    policyman_policyset_execute(pState, asubs_id, haveService, policy_handle_user_pref_update);
  }
}


/*-------- policy_handle_uim_update --------*/
static boolean policy_handle_uim_update(
  void  *pObj,
  void  *pData1,
  void  *pData2
  )
{
  policyman_policy_t    *pPolicy = *((policyman_policy_t **) pObj);
  policy_execute_ctx_t  *pCtx = (policy_execute_ctx_t *) pData1;

  if (  POLICY_IS_ENABLED(pPolicy)
     && POLICY_CAN_HANDLE_UIM_UPDATE(pPolicy)
     )
  {
    POLICY_HANDLE_UIM_UPDATE(pPolicy, pCtx);
  }

  return TRUE;
}


/*-------- policyman_policy_handle_uim_update --------*/
void policyman_policy_handle_uim_update(
  policyman_state_t       *pState,
  sys_modem_as_id_e_type  subsId
)
{
  if (policyman_policy_can_policy_execute()) 
   { 
    boolean haveService = TRUE;

    policyman_policyset_execute(pState, subsId, haveService, policy_handle_uim_update);
  }  
}


/*-------- policyman_boolean_get_value --------*/
boolean
policyman_boolean_get_value(
  char const    *pName
  )
{
  boolean       value = FALSE;

  mre_boolean_get_value_from_set(pName, &value, g_pPolicymanNamedObjects);
  
  return value;
}


/*-------- policyman_policy_add_named_object --------*/
boolean
policyman_policy_add_named_object(
  mre_policy_t              *pPolicy,
  policyman_named_object_t  *pObj
  )
{
  return mre_named_object_add(pPolicy, pObj);
}


/*-------- policyman_policy_find_named_object --------*/
policyman_named_object_t *
policyman_policy_find_named_object(
  char const                    *pName,
  policyman_named_object_type_t objType
  )
{
  return mre_named_object_find_in_set(pName, objType, g_pPolicymanNamedObjects);
}


/*-------- policyman_policy_is_suspended --------*/
boolean 
policyman_policy_is_suspended(
  void
 )
{
  return g_suspendCount.value > 0;
}


/*-------- policyman_suspend --------*/
void
policyman_suspend(
  void
  )
{
  atomic_inc(&g_suspendCount);
  POLICYMAN_MSG_HIGH_1("policy manager is suspended, g_suspendCount %d", g_suspendCount.value);
}


/*------- policyman_execute_process_resume --------*/
void policyman_execute_process_resume
(
  policyman_cmd_t *pCmd
)
{
  /* handle update here - force run rules on current multimode subs
    */
   policyman_state_handle_update(policyman_get_current_multimode_subs());
}


/*-------- policyman_resume--------*/
void
policyman_resume(
  void
  )
{
  policyman_cmd_t *pCmd = NULL;

  POLICYMAN_MSG_HIGH_1("policy manager resume called, g_suspendCount %d", g_suspendCount.value);

  if(policyman_policy_is_suspended())
  {
    /* if PM is suspended, then only resume - this is to avoid the conditions where resume is called without suspend
          run rules when count becomes 0
      */
    if(atomic_dec_return(&g_suspendCount)==0)
    {
      pCmd = (policyman_cmd_t *) policyman_cmd_new( sizeof(policyman_cmd_t), 
                                                     policyman_execute_process_resume, 
                                                     NULL,
                                                     NULL
                                                   );

      policyman_queue_put_cmd((policyman_cmd_t *) pCmd);
      ref_cnt_obj_release(pCmd);
    }
  }
  else
  {
    POLICYMAN_MSG_ERROR_0("policy is not suspended so can't be resumed");
  }
}


/*-------- policyman_boolean_is_defined --------*/
boolean policyman_boolean_is_defined(
  char const    *pName
)
{
  boolean        value = FALSE;

  if (MRE_SUCCEEDED(mre_boolean_get_value_from_set(pName, &value, g_pPolicymanNamedObjects)))
  {
    value = TRUE;
  }
  else
  {
    value = FALSE;
  }
  
  return value;
}


