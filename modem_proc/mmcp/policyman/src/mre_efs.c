/**
  @file mre_efs.c

  @brief MRE abstraction layer above EFS
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/src/mre_efs.c#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre_dbg.h"
#include "mre_efs.h"
#include "mre_util.h"

#include "stringl/stringl.h"
#include "stdio.h"
#include "fs_sys_types.h"
#include "fs_diag_access.h"
#include "fs_fcntl.h"


/*---------------------------------------------------------------------------
  Command item to NV.
---------------------------------------------------------------------------*/
static nv_cmd_type  mre_nv_cmd_buf;



/*-------- mre_efs_get_file --------*/
mre_efs_status_t
mre_efs_get_file(
  const char  *pPath,
  void        **ppData,
  size_t      *pSize
  )
{
  int               fd;
  struct fs_stat    st;
  mre_efs_status_t  status = MRE_EFS_STATUS_SUCCESS;
  fs_size_t         bytesRead;
  size_t            bytesToRead;
  size_t            totalRead;
  byte              *pData;

  /* Try to open the file, return error if we can't
  */
  fd = efs_open(pPath, O_RDONLY);
  if (fd == -1)
  {
    status = MRE_EFS_STATUS_NOFILE;
    goto Done;
  }

  /* Get the size of the file.
  */
  if (efs_fstat(fd, &st) == -1)
  {
    status = MRE_EFS_STATUS_ERROR;
    goto Done;
  }

  /* Set our output data, allocating memory for the data.
   * Allocate an extra byte, which will serve as the "end of file" marker
   * (set to '\0' by the allocation) for any files that contain strings.
   */
  *pSize = st.st_size;
  pData = mre_mem_alloc(st.st_size + 1);
  *ppData = pData;

  /* Read the data into our buffer.
  */
  totalRead = 0;
  bytesToRead = st.st_size;
  do
  {
    bytesRead = efs_read(fd, pData, bytesToRead);
    bytesToRead -= bytesRead;
    totalRead += bytesRead;
    pData += bytesRead;
  } while (bytesRead != 0 && bytesToRead > 0);
  
  /*  Make sure that we read what we expected to.
   */
  if (bytesToRead != 0)
  {
    MRE_MSG_ERROR_2(
      "Bytes read from file (%d) not the same as the file size (%d)",
      totalRead, st.st_size
      );
    status = MRE_EFS_STATUS_ERROR;
    goto Done;
  }

Done:
  if (fd != -1)
  {
    efs_close(fd);
  }
  
  return status;
}



/*-------- mre_efs_put_file --------*/
mre_efs_status_t
mre_efs_put_file(
  char const  *pPath,
  void        *pData,
  size_t      dataSize,
  boolean     createDir
  )
{
  mre_efs_status_t  status = MRE_EFS_STATUS_SUCCESS;
  size_t                  result;
  int                     oFlag = O_WRONLY | O_TRUNC | O_CREAT;

  if(createDir)
  {
    oFlag |= O_AUTODIR; 
  }

  result = efs_put( pPath,
                    pData,
                    dataSize,
                    oFlag,
                    ALLPERMS );

  if (result != 0)
  {
    MRE_MSG_ERROR("Error writing file in mre_efs_put_file()", 0, 0, 0 );
    status = MRE_EFS_STATUS_ERROR ;
  }

  return status;
}


/*-------- mre_efs_get_nv_item --------*/
nv_stat_enum_type
mre_efs_get_nv_item
(
  nv_items_enum_type  item_code,       /* Item to get                      */
  nv_item_type        *data_ptr,       /* Pointer where to put the item    */
  uint16              context          /* Context of the subscription */
)
{
  nv_cmd_ext_type   nv_cmd_ext_buf;
 
  memset( &nv_cmd_ext_buf, 0x00, sizeof(nv_cmd_ext_type) );

  /*-------------------------------------------------------------------------
    Prepare command buffer to get the item from NV.
    -------------------------------------------------------------------------*/
  mre_nv_cmd_buf.cmd        = NV_READ_F;                       /* Read request        */
  mre_nv_cmd_buf.tcb_ptr    = NULL;                            /* NULL for making NV read a sunchronous call */
  mre_nv_cmd_buf.sigs       = (rex_sigs_type)NULL;             /* NULL for making NV read a sunchronous call */
  mre_nv_cmd_buf.done_q_ptr = NULL;                            /* Do not enqueue when done */
  mre_nv_cmd_buf.item       = item_code;                       /* Item to get         */
  mre_nv_cmd_buf.data_ptr   = data_ptr;                        /* Where to return it  */
  
  /*-------------------------------------------------------------------------
    Issue the command.
    -------------------------------------------------------------------------*/
  nv_cmd_ext_buf.nvcmd   = &mre_nv_cmd_buf;
  nv_cmd_ext_buf.context = context;

#if defined(TEST_FRAMEWORK)
  #error code not present
#endif /* defined(TEST_FRAMEWORK) */
  
  nv_cmd_ext( &nv_cmd_ext_buf );               /* Issue the request    */
  
  if ( mre_nv_cmd_buf.status == NV_BUSY_S )
  {
    MSG_ERROR("NV Read Failed Item %d status %d", mre_nv_cmd_buf.item, mre_nv_cmd_buf.status, 0 );
  }

  return mre_nv_cmd_buf.status;
}


/*-------- mre_efs_file_exists --------*/
boolean
mre_efs_file_exists(
  char const  *pPath
  )
{
  struct fs_stat  st;
  int             result;

  result = efs_stat(pPath, &st);

  return result != -1;
}

/*-------- mre_efs_get_subs_path --------*/
mre_efs_status_t
mre_efs_get_subs_path(
  char                   *pFullPath,
  size_t                 maxSize,
  const char             *pPath,
  sys_modem_as_id_e_type subsId
)
{
  size_t                 fullPathSize;
  mre_efs_status_t efsStatus = MRE_EFS_STATUS_SUCCESS;

  fullPathSize = strlen(pPath) + sizeof("_Subscription01") + 1;
  if (fullPathSize > maxSize)
  {
    MRE_MSG_ERROR_1("Full Path Size %d too long", fullPathSize);
    return MRE_EFS_STATUS_ERROR;
  }

  /* No errors here so get the Full Path
   */
  switch (subsId)
  {
    case SYS_MODEM_AS_ID_1:
      (void)strlcpy(pFullPath, pPath, fullPathSize);
      break;

    default:      
      snprintf(pFullPath, fullPathSize, "%s_Subscription%02d", pPath, subsId);
      break;
  }

  return efsStatus;
}

