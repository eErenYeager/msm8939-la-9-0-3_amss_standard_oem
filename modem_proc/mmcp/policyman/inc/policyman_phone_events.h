#ifndef _POLICYMAN_PHONE_EVENTS_H_
#define _POLICYMAN_PHONE_EVENTS_H_

/**
  @file policyman_phone_events.h

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_phone_events.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "cm.h"
#include "policyman_i.h"

typedef struct policyman_cmph_info_s  policyman_cmph_info_t;


/*-------- policyman_cmph_init --------*/
boolean
policyman_cmph_init(
  policyman_cmph_info_t **ppInfo,
  sys_modem_as_id_e_type asubs_id
  );

/*-------- policyman_cmph_deinit --------*/
void
policyman_cmph_deinit(
  policyman_cmph_info_t *pInfo
  );



/*-------- policyman_cmph_update_state --------*/
void
policyman_cmph_update_state(
  policyman_cmph_info_t *pCurInfo,
  policyman_cmph_info_t *pNewInfo
  );


/*-------- policyman_ph_get_mode_pref --------*/
/**
@brief  

@param

@return
  
*/
cm_mode_pref_e_type
policyman_ph_get_mode_pref(
  sys_modem_as_id_e_type  asubs_id
  );

/*-------- policyman_ph_get_srv_domain --------*/
/**
@brief  

@param

@return
  
*/
cm_srv_domain_pref_e_type
policyman_ph_get_srv_domain(
  sys_modem_as_id_e_type  asubs_id
  );


/*-------- policyman_ph_get_band_pref --------*/
/**
@brief  

@param

@return
  
*/
void policyman_ph_get_band_pref(
  sys_modem_as_id_e_type  asubs_id,
  cm_band_pref_e_type     *gw_bands,
  cm_band_pref_e_type     *lte_bands,
  cm_band_pref_e_type     *tds_bands
);


/*=============================================================================
  Phone-related rule conditions
=============================================================================*/

/*-------- policyman_condition_ph_domain_pref_new --------*/
mre_status_t
policyman_condition_ph_domain_pref_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

/*-------- policyman_condition_ph_rat_pref_new --------*/
mre_status_t
policyman_condition_ph_rat_pref_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

/*-------- policyman_condition_ph_operating_mode_new --------*/
mre_status_t
policyman_condition_ph_operating_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

/*-------- policyman_condition_ph_user_mcc_new --------*/
mre_status_t
policyman_condition_ph_user_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

/*-------- policyman_condition_ph_network_selection_mode_new --------*/
mre_status_t
policyman_condition_ph_network_selection_mode_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

/*===========================================================================
  FUNCTION POLICYMAN_PH_SET_OPRT_MODE()

  DESCRIPTION
    Set operating mode into PM Phone state

  PARAMETERS
    pPhInfo     : Pointer to PM Phone State info
    oprt_mode : operating mode to be set

  RETURN VALUE
    TRUE if mode_pref changed, FALSE otherwise

  DEPENDENCIES
    None

  SIDE EFFECTS
    None
===========================================================================*/
boolean policyman_ph_set_oprt_mode(
  policyman_cmph_info_t *pPhInfo, 
  sys_oprt_mode_e_type oprt_mode
  );

/*===========================================================================
  FUNCTION POLICYMAN_PH_GET_OPRT_MODE()

  DESCRIPTION
    Get operating mode from PM Phone state

  PARAMETERS
    None

  RETURN VALUE
    TRUE if mode_pref changed, FALSE otherwise
===========================================================================*/
sys_oprt_mode_e_type 
policyman_ph_get_oprt_mode(
  void
  );

/*===========================================================================
  FUNCTION POLICYMAN_DETERMINE_MULTIMODE_ASUBS_ID()

  DESCRIPTION
    Determine Multimode subscription id in phone state

  PARAMETERS
    None

  RETURN VALUE
    TRUE if Multimode subscription id has changed from current value, FALSE otherwise
===========================================================================*/
boolean 
policyman_determine_multimode_asubs_id(
  void
  );

/*===========================================================================
  FUNCTION policyman_init_multimode_subs_id()

  DESCRIPTION
    Determine Multimode subscription id in initialization phase

  PARAMETERS
    None

  RETURN VALUE
    None
===========================================================================*/
/*-------- policyman_init_multimode_subs_id --------*/
void 
policyman_init_multimode_subs_id(
  void
);

#endif /* _POLICYMAN_PHONE_EVENTS_H_ */
