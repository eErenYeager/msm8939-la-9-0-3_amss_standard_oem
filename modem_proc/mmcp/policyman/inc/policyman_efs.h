#ifndef _POLICYMAN_EFS_H_
#define _POLICYMAN_EFS_H_

/**
  @file policyman_efs.h

  @brief PM abstraction layer above EFS
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_efs.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre_efs.h"

#define POLICYMAN_EFS_ROOT_PATH "/policyman/"


#define POLICYMAN_EFS_STATUS_SUCCESS  MRE_EFS_STATUS_SUCCESS
#define POLICYMAN_EFS_STATUS_ERROR    MRE_EFS_STATUS_ERROR
#define POLICYMAN_EFS_STATUS_NOFILE   MRE_EFS_STATUS_NOFILE
#define policyman_efs_status_t        mre_efs_status_t


/*-------- policyman_efs_initialize --------*/
/**
@brief  Initialize the PM EFS subsystem.  Private function for the use of the
        PM task code.

@return
  - POLICYMAN_EFS_STATUS_SUCCESS if intiialization was successful.
  - POLICYMAN_EFS_STATUS_ERROR if unsuccessful.
*/
void
policyman_efs_initialize(
  void
  );


/*-------- policyman_efs_deinitialize --------*/
/**
@brief  Deinitialize the PM EFS subsystem.  Private function for the use of the
        PM task code.

@return
  None
*/
void
policyman_efs_deinitialize(
  void
  );



#define policyman_efs_get_file      mre_efs_get_file
#define policyman_efs_put_file      mre_efs_put_file
#define policyman_efs_get_nv_item   mre_efs_get_nv_item
#define policyman_efs_file_exists   mre_efs_file_exists
#define policyman_efs_get_subs_path mre_efs_get_subs_path

#endif /* _POLICYMAN_EFS_H_ */
