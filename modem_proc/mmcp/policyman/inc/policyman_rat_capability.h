#ifndef _POLICYMAN_RAT_CAPABILITY_H_
#define _POLICYMAN_RAT_CAPABILITY_H_

/**
  @file policyman_rat_capability.h

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_rat_capability.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "policyman_cfgitem.h"
#include "policyman_rules.h"

#define POLICYMAN_RAT_MASK_NV_PATH "/policyman/rat_mask"

typedef struct policyman_policy_rat_capability_t  policyman_policy_rat_capability_t;



/*-------- policyman_policy_rat_capability --------*/
/**
@brief  The capability policy.
*/
extern  policyman_policy_rat_capability_t policyman_policy_rat_capability;



/*-------- policyman_rat_config_parse_rats --------*/
/**
@brief  Utility function to turn a string of RATs into a mask.

@param

@return
  
*/
boolean
policyman_rat_config_parse_rats(
  char const  *pStr,
  uint32      *pMask
  );


/*=============================================================================
  RAT capability APIs
=============================================================================*/


/*-------- rat_capability_info_t --------*/
/**
@brief  Structure to store information about how a rat capability is
        to be computed.
*/
typedef struct
{
  policyman_base_t  base;
  uint32            includeMask;
  uint32            excludeMask;
} rat_capability_info_t;



/*-------- rat_capability_t --------*/
/**
@brief  Configuration item structure for rat_capability.
*/
typedef struct
{
  POLICYMAN_ITEM;
  
  uint32  ratMask;
} rat_capability_t;



/*-------- policyman_rat_capability_item_new --------*/
/**
@brief  

@param

@return
  
*/
policyman_item_t *
policyman_rat_capability_item_new(
  policyman_state_t       *pState,
  uint32                  mask,
  sys_modem_as_id_e_type  subs
  );

/*-------- policyman_rat_capability_get_default_from_efs --------*/
/**
@brief  

@param

@return
  
*/
policyman_item_t *
policyman_rat_capability_get_default_from_efs(
  sys_modem_as_id_e_type subs_id
);


/*-------- policyman_rat_capability_get_default --------*/
/**
@brief  

@param

@return
  
*/
policyman_item_t *
policyman_rat_capability_get_default(
  sys_modem_as_id_e_type subsId
  );


/*-------- policyman_rat_capability_compare --------*/
/**
@brief  

@param

@return
  
*/
boolean
policyman_rat_capability_compare(
  policyman_item_t  *pData1,
  policyman_item_t  *pData2
  );



/*-------- policyman_rat_capability_read_info --------*/
/**
@brief  

@param

@return
  
*/
policyman_status_t
policyman_rat_capability_read_info(
  policyman_xml_element_t const *pElem,
  rat_capability_info_t         *pInfo
  );



/*-------- policyman_rat_capability_evaluate --------*/
/**
@brief  

@param

@return
  
*/
uint32
policyman_rat_capability_evaluate(
  rat_capability_info_t *pInfo,
  policyman_state_t     *pState,  
  policyman_set_t       *pItemSet
  );



/*-------- policyman_rat_capability_action_new --------*/
/**
@brief  API to create a new policyman action for the rat_capability
        action in the policy_info.

@param[in]  pElem     Pointer to the XML document element for the
                      <rat_capability> node.
@param[out] ppAction  Pointer to storage for a pointer to the policyman_action_t
                      object created from the XML.

@return
  Status
*/
mre_status_t
policyman_rat_capability_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  );

/*-------- policyman_rat_capability_if_action_new --------*/
mre_status_t
policyman_rat_capability_if_action_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_action_t            **ppAction
  );


/*-------- policyman_rat_capability_display --------*/
void policyman_rat_capability_display
(
  policyman_item_t  *pItem
);

/*-------- policyman_get_rats_for_ueMode --------*/
rat_capability_t *
policyman_get_rats_for_ueMode(
      sys_ue_mode_e_type current_ue_mode);



/*-------- policyman_rat_capability_full_rat_mode --------*/
/**
@brief  Add the necessary items to an itemset to go to full RAT mode.

Note: This is used by policyman_rat_capability_start_full_rat() as well
as called directly from policies' execute method, depending on how the
policy chooses to handle it.

@param

@return
  
*/
void
policyman_rat_capability_full_rat_mode(
  policyman_set_t       *pItemSet,
  sys_ue_mode_e_type    ue_mode,
  policyman_svc_mode_t  svc_mode
  );


/*-------- policyman_rat_capability_start_full_rat --------*/
/**
@brief  Queue a command to go to full RAT mode with the given UE and SVC modes.

@param

@return
  
*/
void
policyman_rat_capability_start_full_rat(
  sys_ue_mode_e_type    ue_mode,
  policyman_svc_mode_t  svc_mode,
  policyman_policy_t    *pPolicy
  );

/*-------- policyman_rat_capability_str_to_rat --------*/
/**
@brief  Convert Rat capability string to rat mask

@param

@return

*/
uint32
policyman_rat_capability_str_to_rat(
  const char  *pRatName
  );


/*-------- policyman_update_rats_bands_to_efs --------*/
/**
@brief  Update Rats and Bands to EFS file

@param

@return

*/
void
policyman_update_rats_bands_to_efs(
  uint8 subsMask
);

/*-------- policyman_update_rats_bands_to_efs_per_tech --------*/
/**
@brief  Update Rats and Bands to EFS file

@param

@return

*/
void
policyman_update_rats_bands_to_efs_per_tech(
  uint32  rat_include
);

#endif /* _POLICYMAN_RAT_CAPABILITY_H_ */
