/**
  @file policyman_rat_order.h

  @brief  
*/
/*
  Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

*/

#ifndef _POLICYMAN_RAT_ORDER_H_
#define _POLICYMAN_RAT_ORDER_H_

#include "policyman_i.h"

/*=============================================================================
  Technology Order Table APIs
=============================================================================*/

typedef struct 
{
  POLICYMAN_ITEM;

  cm_acq_pri_order_pref_s_type rat_order;
} policyman_rat_order_item_t;


/*-------- policyman_rat_order_item_new --------*/
/**
@brief  

@param

@return
  
*/
policyman_rat_order_item_t * policyman_rat_order_item_new(
  void
);



/*-------- policyman_rat_order_get_default --------*/
/**
@brief  

@param

@return
  
*/
policyman_item_t * policyman_rat_order_get_default(
  sys_modem_as_id_e_type subsId
);


/*-------- policyman_rat_order_compare --------*/
/**
@brief  

@param

@return
  
*/
boolean policyman_rat_order_compare(
  policyman_item_t  *pItem1,
  policyman_item_t  *pItem2
);

/*-------- policyman_rat_order_update_to_efs --------*/
void policyman_rat_order_update_to_efs(
  policyman_item_t const *pItem
);

/*-------- policyman_retrieve_rat_order --------*/
boolean policyman_retrieve_rat_order(
  cm_acq_pri_order_pref_s_type *pRatOrder
);

/*-------- policyman_rat_order_display --------*/
void policyman_rat_order_display(
  policyman_item_t  *pItem
);

/*-------- policyman_rat_order_action_new --------*/
mre_status_t policyman_rat_order_action_new(
  policyman_xml_element_t const  *pElem,
  mre_policy_t                   *pPolicy,
  policyman_action_t            **ppAction
);

#endif

