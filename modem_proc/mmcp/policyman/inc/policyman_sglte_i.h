#ifndef _POLICYMAN_SGLTE_I_H_
#define _POLICYMAN_SGLTE_I_H_

/**
  @file policyman_sglte_i.h

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_sglte_i.h#2 $
  $DateTime: 2015/11/26 02:17:25 $
  $Author: rkarth $
*/

#include "policyman_sglte.h"
#include "policyman_policy.h"
#include "sys.h"


/*-------- policyman_policy_sglte_vtbl --------*/
/**
@brief  The VTbl for the SGLTE policy
*/
extern policyman_policy_vtbl_t  policyman_policy_sglte_vtbl;


/*-------- policy_sglte_get_rat_order_for_ueMode --------*/
policyman_item_t * policy_sglte_get_rat_order_for_ueMode(
  sys_ue_mode_e_type ueMode,  
  policyman_policy_t *pPolicy
);

#endif /* _POLICYMAN_SGLTE_I_H_ */
