#ifndef _MRE_XML_H_
#define _MRE_XML_H_

/**
  @file mre_xml.h

  @brief  XML parser for Modem Rules Engine
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/mre_xml.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

// TODO: Arrange things so that we don't have to include this
#include "policyman_dbg.h"

#include "ref_cnt_obj.h"
#include "mre.h"


typedef struct mre_xml_root_t   mre_xml_root_t;


#if defined(TEST_FRAMEWORK)
#error code not present
#else /* +TEST_FRAMEWORK- */

#define MRE_XML_MSG(str, a, b, c)

#endif /* -TEST_FRAMEWORK. */


/*-------- mre_xml_parse --------*/
/**
@brief  Parse an XML document into a document tree.

@param[in]  pDocument   Pointer to the document to parse.
@param[out] ppRoot      Pointer to storage for a pointer to a reference-
                        counted document tree root.  The caller should
                        call ref_cnt_obj_release() on this pointer when it
                        no longer needs the document tree.

@return
  MRE_STATUS_SUCCESS if the document is successfully parsed.
  MRE_STATUS_ERR_MALFORMED_XML the XML is malformed.
*/
mre_status_t
mre_xml_parse(
  const char            *pDocument,
  const mre_xml_root_t  **ppRoot
  );


/*-------- mre_xml_get_root_element --------*/
/**
@brief  Returns a pointer to the root element of the document.

@param[in]  pRoot   Pointer to the XML document tree root returned by
                    mre_xml_parse().

@return
  Pointer to the root element of the document, or NULL if there is none.
*/
const mre_xml_element_t *
mre_xml_get_root_element(
  const mre_xml_root_t  *pRoot
  );

#endif /* _MRE_XML_H_ */
