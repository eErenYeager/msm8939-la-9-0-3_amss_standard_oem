#ifndef _POLICYMAN_UIM_H_
#define _POLICYMAN_UIM_H_

/**
  @file policyman_uim.h

  @brief  
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_uim.h#2 $
  $DateTime: 2015/06/26 02:43:52 $
  $Author: arkhanna $
*/

#include "policyman_i.h"
#include "policyman_state.h"

#include "sys.h"
#include "mmgsdisessionlib.h" /* Included for MMGSDI Session prototypes */
#include "policyman_dbg.h"


typedef struct 
{
  sys_plmn_id_s_type    plmn;
  mmgsdi_app_enum_type  sim_app_type;
  mmgsdi_app_enum_type  cdma_app_type;
  boolean               subs_active;
} policyman_uim_info_t;

#define MAX_SIMS  3

#define MAX_SUBS  6

/*-------- policyman_uim_init --------*/
boolean
policyman_uim_init(
  policyman_uim_info_t  **pUimInfo
  );

/*-------- policyman_uim_deinit --------*/
void
policyman_uim_deinit(
  policyman_uim_info_t  *pUimInfo
  );


#define PM_CHECK_PTR_RET(x, ret)                             \
  if ((x) == NULL) {                                         \
    POLICYMAN_MSG_ERROR("Unexpected NULL pointer", 0, 0, 0); \
    return ret;                                              \
  }

uint32
policyman_uim_get_num_subs(
  void
  );

void
policyman_uim_set_num_subs(
  policyman_state_t  *pInfo,
  uint32              nSubs
  );

uint32
policyman_uim_get_imsi_mcc(
  policyman_state_t  *pInfo,
  size_t             subsId
  );

boolean
policyman_uim_get_imsi_plmn(
  policyman_state_t  *pInfo,
  size_t              subsId,
  sys_plmn_id_s_type  *pPlmn
  );

boolean
policyman_uim_set_imsi_plmn(
  policyman_state_t  *pInfo,
  size_t              subsId,
  sys_plmn_id_s_type  *pPlmn
  );

void
policyman_uim_get_sim_type(
  policyman_state_t    *pInfo,
  size_t                subsId,
  mmgsdi_app_enum_type *sim_app_type
  );


/*-------- policyman_uim_get_subs_active --------*/
boolean policyman_uim_get_subs_active(
  sys_modem_as_id_e_type subsId
);

/*-------- policyman_condition_sim_type_new --------*/
mre_status_t
policyman_condition_sim_type_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );


mre_status_t
policyman_condition_imsi_plmn_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );


mre_status_t
policyman_condition_imsi_mcc_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  policyman_condition_t         **ppCondition
  );

#endif /* _POLICYMAN_UIM_H_ */
