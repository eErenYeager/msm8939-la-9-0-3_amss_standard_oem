#ifndef _POLICYMAN_RULES_H_
#define _POLICYMAN_RULES_H_

/**
  @file policyman_rules.h

  @brief  Utility APIs to help in interpreting components of rules.
*/

/*
    Copyright (c) 2013,2014 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/mmcp/policyman/inc/policyman_rules.h#1 $
  $DateTime: 2015/01/27 06:42:19 $
  $Author: mplp4svc $
*/

#include "mre_rules.h"
#include "mre_lang.h"
#include "policyman_state.h"




/*-----------------------------------------------------------------------------
  Definitions for base policyman condition objects
-----------------------------------------------------------------------------*/

#define POLICYMAN_CONDITION_BASE  MRE_CONDITION_BASE
#define policyman_condition_t     mre_condition_t
#define policyman_condition_dtor  mre_condition_dtor



/*-----------------------------------------------------------------------------
  Definitions for base policyman action objects
-----------------------------------------------------------------------------*/

#define POLICYMAN_ACTION_BASE       MRE_ACTION_BASE
#define policyman_action_t          mre_action_t
#define policyman_action_dtor       mre_action_dtor
#define policyman_actionset_execute mre_actionset_execute


/*-----------------------------------------------------------------------------
  Other common condition definitions
-----------------------------------------------------------------------------*/
typedef struct
{
  POLICYMAN_CONDITION_BASE;

  mre_set_t         *pMccSet;
  policyman_subs_t  subs;
  char const      *pSetName;
} policyman_mcc_condition_t;


/*-----------------------------------------------------------------------------
  Definition of a policyman rule
-----------------------------------------------------------------------------*/
#define policyman_rule_t  mre_rule_t

#endif /* _POLICYMAN_RULES_H_ */
