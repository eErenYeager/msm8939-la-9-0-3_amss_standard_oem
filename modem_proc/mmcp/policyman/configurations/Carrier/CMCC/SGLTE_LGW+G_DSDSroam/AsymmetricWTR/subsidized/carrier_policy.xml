<?xml version="1.0" encoding="UTF-8"?>

<!-- Policy for SGLTE+G with Asymmetric WTR GWL+G DSDS roaming -->

<policy name        = "sglte"
        enabled     = "true"
        schema_ver  = "1"
        policy_ver  = "20.1.8"
>
 
  <initial>
    <actions>
      <!-- These are the MCCs on which SGLTE is allowed.
           DO NOT RENAME
      -->
      <mcc_list name="sglte_mccs">
        460
      </mcc_list>

      <!-- These are the operators (IMSI PLMNs) for which SGLTE will be allowed -->
      <plmn_list name="sglte_operators">
        460-00 460-02 460-07 460-08 454-12
      </plmn_list>

      <!-- Define the OOS timer
           DO NOT RENAME
      -->
      <define_timer name="sglte_oos" interval="1" units="min" id = "1" />

      <!-- Define the segment-switching OOS timer here in SGLTE XML so that tuning
           of the timers can be done here rather than having to modify both this
           and the segment-loading XML.
           
           This is intended to be used with
           policyman/configurations/SegLoad/No_Timer_Defined/segment_loading.xml,
           which does not define this timer.

           Note that defining this timer without loading segment_loading.xml
           is just a no-op that will not cause segment switching to happen.
      -->
      <define_timer name="segload" units="min" class="backoff" persist="true" id="10">
        2 2 2 2 10 10 20 20 30 30 60
      </define_timer>

      <!-- Define the B39-forbid timer
           DO NOT RENAME
      -->
      <define_timer name="b39forbid" interval="90" units="sec" />

      <!-- Define the SGLTE RF bands
           DO NOT RENAME
      -->
      <rf_band_list name="sglte_rf_bands">
        <!-- GSM Band mapping: SGLTE GSM bands are
                B7->GSM_DCS_1800->Bit-7,
                B8->GSM_EGSM_900->Bit-8
                B9->GSM_PGSM_900->Bit-9.
                B19->GSM_850 -> Bit-19,
                B21->GSM_PCS_1900-> Bit-21
             Refer: sys_sband_e_type in sys.h
        -->
        <gw_bands base="none">
          <include> 7 8 9 </include>
        </gw_bands>
        <!-- LTE Band: 38, 39, 40, 41 are mapped to Band Bits: 37, 38, 39, 40
             respectively.
             Refer: sys_sband_lte_e_type in sys.h
        -->
        <lte_bands base="none">
          <include> 37 38 39 40 </include>
        </lte_bands>
        <!-- TDS Band mapping:
                B34->BANDA->Bit-0,
                B39->BANDF->Bit-5
             Refer: sys_sband_tds_e_type in sys.h
        -->
        <tds_bands base="none">
          <include> 0 5 </include>
        </tds_bands>
      </rf_band_list>

      <rf_band_list name="rf_bands_gwlt">
        <gw_bands base="hardware" />
        <lte_bands base="hardware" />
        <tds_bands base="hardware" />
      </rf_band_list>

      <rf_band_list name="rf_bands_gwl">
        <gw_bands base="hardware" />
        <lte_bands base="hardware" />
        <tds_bands base="none" />
      </rf_band_list>

      <rf_band_list name="rf_bands_gw">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <rf_band_list name="rf_bands_gwt">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="hardware" />
      </rf_band_list>

      <!-- No LTE FDD Bands --> 
      <rf_band_list name = "csfb_rf_bands_with_tdscdma_no_fdd">
        <gw_bands base = "hardware" />
        <!-- LTE Band: 38, 39, 40, 41, 42, 43 are mapped to Band Bits: 37, 38, 39, 40, 41, 42
                      respectively.
                      Refer: sys_sband_lte_e_type in sys.h
        --> 
        <lte_bands base = "none" >
          <include> 37 38 39 40 41 42 </include>
        </lte_bands> 
        <tds_bands base="hardware" />
      </rf_band_list>

      <rf_band_list name="restricted_GSM_bands">
        <!-- GSM Band mapping: SGLTE GSM bands are
                B7->GSM_DCS_1800->Bit-7,
                B8->GSM_EGSM_900->Bit-8
                B9->GSM_PGSM_900->Bit-9.
             Refer: sys_sband_e_type in sys.h
        -->
        <gw_bands base="none">
          <include> 7 8 9 </include>
        </gw_bands>
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <rf_band_list name="expanded_GSM_bands">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- Boolean: Is SGLTE allowed?
           DO NOT RENAME
      -->
      <boolean_define name="sglte_allowed" initial="true" />

      <!-- Boolean: Is the device roaming? -->
      <boolean_define name="roaming" initial="false" />

      <!-- Boolean: Is the device home? -->
      <boolean_define name="home" initial="true" />

      <!-- Boolean: are we supposed to be in DSDA? -->
      <boolean_define name="dsda" initial="true" />

      <!-- Boolean: are we supposed to be using CMCC? -->
      <boolean_define name="nas:SingleIMEI" initial="true" />

      <define_config name="dsda" num_sims="2" max_active="2" specialization="dsds_roam">
        <config>
          <subs id="1" feature="sglte">
            <rat_capability base="none">
              <include> GSM WCDMA TDSCDMA LTE </include>
            </rat_capability>
          </subs>
          <subs id="2">
            <rat_capability base="none">
              <include> GSM </include>
            </rat_capability>
          </subs>
        </config>
      </define_config>

      <define_config name="dsds" num_sims="2" max_active="1" specialization="dsds_roam">
        <config>
          <subs id="1" feature="sglte">
            <rat_capability base="none">
              <include> GSM WCDMA TDSCDMA LTE </include>
            </rat_capability>
          </subs>
          <subs id="2">
            <rat_capability base="none">
              <include> GSM </include>
            </rat_capability>
          </subs>
        </config>
      </define_config>

      <!-- DSDA configuration -->
      <use_config_if name="dsda" />

      <!-- Set initial UE mode as SGLTE_HOME -->
      <ue_mode_if> SGLTE </ue_mode_if>

      <!-- Set initial RF bands for SGLTE_HOME -->
      <rf_bands_if subs="1" list="sglte_rf_bands" />
      <rf_bands_if subs="2" list="restricted_GSM_bands" />

      <!-- Set initial RAT Capability for SGLTE_HOME -->
      <rat_capability_if subs="1" base="config">
        <exclude> WCDMA </exclude>
      </rat_capability_if>

      <!-- Define initial RAT capability as GSM for SUBS2 -->
      <rat_capability_if subs="2" base="none">
        <include> GSM </include>
      </rat_capability_if>

    </actions>
  </initial>

  <!--
  ====================================================================
    Rules to keep the UE in a specific mode.
  ====================================================================
  -->

  <!-- RULE #1 -->
  <!-- If the mode is set to SGLTE_ONLY, do not bother evaluating other
       rules - just keep the UE in this mode.
  -->
  <rule precond="none">
    <conditions>
      <ue_mode_is> SGLTE_ONLY </ue_mode_is>
    </conditions>
    <actions>
      <rf_bands list="sglte_rf_bands" />
      <svc_mode> FULL </svc_mode>
      <use_config name="dsda" />
      <rat_capability base = "config" >
        <exclude> WCDMA </exclude>
      </rat_capability>
      <boolean_set name="home" value="true" />
    </actions>
  </rule>

  <!-- RULE #2 -->
  <!-- If the mode is set to CSFB_ONLY, do not bother evaluating other
       rules - just keep the UE in this mode.
  -->
  <rule precond="none">
    <conditions>
      <ue_mode_is> CSFB_ONLY </ue_mode_is>
    </conditions>
    <actions>
      <rf_bands list = "rf_bands_gwlt" />
      <svc_mode> FULL </svc_mode>
      <use_config name="dsds" />
      <rat_capability base = "config" />
      <boolean_set name="home" value="false" />
    </actions>
  </rule>

  <!--
  ====================================================================
    Rules to handle OOS situations.
  ====================================================================
  -->

  <!-- RULE #3 -->
  <!-- Stay in same config until service is acquired on mmode SUBS. This rule shouldn't be 
           made always_run as SS precondition is required to check for service status.
  -->
  <rule precond="SS">
    <conditions>
      <not> <service_status_in subs="mmode"> FULL LIMITED LIMITED-REGIONAL </service_status_in> </not>
    </conditions>

    <actions />
  </rule>

  <!--
  ====================================================================
    Rules to implement SGLTE
  ====================================================================
  -->

  <!-- RULE #4 -->
  <!-- Set the "sglte_allowed" boolean to "false", service mode to FULL.
       We're roaming if the serving system isn't in sglte_mccs.
       Preference is given to Manual Selection over user location.
       Also, set the RAT capability of the secondary subscription to GSM only,
       in case it was changed during full RAT search.
  -->
  <rule>
    <conditions>
      <true />
    </conditions>
    <actions>
      <boolean_set name="sglte_allowed" value="false" />
      <svc_mode> FULL </svc_mode>
      <rat_capability subs="2" base="none">
        <include> GSM </include>
      </rat_capability>
      <boolean_set name="roaming">
        <any_of>
          <all_of>
            <network_selection_mode> MANUAL </network_selection_mode>
            <not> <user_mcc_in list = "sglte_mccs" /> </not>
          </all_of>
          <all_of>
            <not> <network_selection_mode> MANUAL </network_selection_mode> </not>
            <not> <serving_mcc_in list = "sglte_mccs" /> </not>
          </all_of>
        </any_of>
      </boolean_set>
      <boolean_set name="home">
        <not> <boolean_test name="roaming" /> </not>
      </boolean_set>
      <boolean_set name="dsda">
        <any_of>
          <imsi_mcc_in list="sglte_mccs" />
          <imsi_plmn_in list="sglte_operators" />
        </any_of>
        <not> <boolean_test name="roaming" /> </not>
      </boolean_set>
      <continue />
    </actions>
  </rule>

  <!-- RULE #5 -->
  <!-- Chinese operators use a DSDA configuration when not roaming -->
  <rule>
    <conditions>
      <boolean_test name="dsda" /> 
    </conditions>

    <actions>
      <use_config name="dsda" />
      <rf_bands subs="2" list="restricted_GSM_bands" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #6 -->
  <!-- Other operators, or Chinese operators that are roaming are DSDS -->
  <rule>
    <conditions>
      <not> <boolean_test name="dsda" /> </not>
    </conditions>

    <actions>
      <use_config name="dsds" />
      <rf_bands subs="2" list="expanded_GSM_bands" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #7 -->
  <!-- Chinese operators not in "sglte_operators" list only get GSM -->
  <rule precond="IMSI">
    <conditions>
      <imsi_mcc_in list="sglte_mccs" />
      <not> <imsi_plmn_in list="sglte_operators" /> </not>
    </conditions>
    <actions>
      <ue_mode> CSFB </ue_mode>
      <rat_capability base="none">
        <include> GSM </include>
      </rat_capability>
      <rf_bands list="rf_bands_gw" />
    </actions>
  </rule>

  <!-- RULE #8 -->
  <!-- SGLTE is allowed if all the following are true:
       - domain preference is CS+PS
       - mode preference contains GL
  -->
  <rule precond="none">
    <conditions>
      <true />
    </conditions>

    <actions>
      <boolean_set name="sglte_allowed">
        <user_domain_pref> CSPS </user_domain_pref>
        <user_mode_pref_contains> GSM LTE </user_mode_pref_contains>
        </boolean_set>
      <continue />
    </actions>
  </rule>

  <!-- RULE #9 -->
    <!-- Update SGLTE allowed boolean by checking SIM parameters:
       - SIM type is 3G
       - SIM issuer in sglte_operators
  -->
   <rule precond="SIM_TYPE IMSI">
    <conditions>
      <true />
    </conditions>

    <actions>
      <boolean_set name="sglte_allowed">
        <boolean_test name="sglte_allowed" />
        <sim_type> 3G </sim_type>
        <imsi_plmn_in list = "sglte_operators" />
      </boolean_set>
      <continue />
    </actions>
  </rule>

  <!-- RULE #10 -->
  <!-- If SGLTE is allowed and we are not roaming, operate as SGLTE -->
  <rule>
    <conditions>
      <boolean_test name="sglte_allowed" />
      <not> <boolean_test name="roaming" /> </not>
    </conditions>

    <actions>
      <ue_mode> SGLTE </ue_mode>
      <rat_capability subs="1" base = "config">
        <exclude> WCDMA </exclude>
      </rat_capability>
      <rf_bands subs="1" list="sglte_rf_bands" />
    </actions>
  </rule>

  <!-- RULE #11 -->
  <!-- If SGLTE is not allowed then put UE into CSFB. Device config can stay the same -->
  <rule precond="none">
    <conditions>
      <not> <boolean_test name="sglte_allowed" /> </not>
    </conditions>
    <actions>
      <ue_mode> CSFB </ue_mode>
      <continue />
    </actions>
  </rule>

  <!-- RULE #12 -->
  <!-- All other configurations from here out are CSFB because UE is Roaming -->
  <rule>
    <conditions>
      <true />
    </conditions>

    <actions>
      <ue_mode> CSFB </ue_mode>
      <continue />
    </actions>
  </rule>
      
  <!-- RULE #13 -->
  <!-- If this is an SGLTE operator in China but for some reason we can't
       do SGLTE, do CSFB with all the other settings for SGLTE
  -->
  <rule>
    <conditions>
      <imsi_plmn_in list = "sglte_operators" />
      <not> <boolean_test name="roaming" /> </not>
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> WCDMA </exclude>
      </rat_capability>
      <rf_bands list = "sglte_rf_bands" />
    </actions>
  </rule>

  <!-- RULE #14 -->
  <!-- If this is an SGLTE operator but we're roaming, remove TDSCDMA. -->
  <rule>
    <conditions>
      <imsi_plmn_in list = "sglte_operators" />
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> TDSCDMA </exclude>
      </rat_capability>
      <rf_bands list = "rf_bands_gwl" />
    </actions>
  </rule>
      
      
  <!-- RULE #15 -->
  <!-- Other operators that are not roaming and have the PS domain
       on this subscription use all available RATs.
  -->
  <rule>
    <conditions>
      <not> <boolean_test name="roaming" /> </not>
      <not> <user_domain_pref> CS </user_domain_pref> </not>
    </conditions>

    <actions>
      <rat_capability base = "config" />
      <rf_bands list = "csfb_rf_bands_with_tdscdma_no_fdd" />
    </actions>
  </rule>
  

  <!-- RULE #16 -->
  <!-- Other operators that are roaming and have the PS domain on this
       subscription lose TDSCDMA.
       This is also the configuration for only one subscription available.
  -->
  <rule>
    <conditions>
      <any_of>
        <num_subs> 1 </num_subs>
        <all_of>
          <boolean_test name="roaming" />
          <not> <user_domain_pref> CS </user_domain_pref> </not>
        </all_of>
      </any_of>
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> TDSCDMA </exclude>
      </rat_capability>
      <rf_bands list = "rf_bands_gwl" />
    </actions>
  </rule>

  <!-- RULE #17 -->
  <!-- All other situations get GW -->
  <rule>
    <conditions>
      <true />
    </conditions>

    <actions>
      <rat_capability base = "none">
        <include> GSM WCDMA </include>
      </rat_capability>
      <rf_bands list = "rf_bands_gw" />
    </actions>
  </rule>

</policy>
