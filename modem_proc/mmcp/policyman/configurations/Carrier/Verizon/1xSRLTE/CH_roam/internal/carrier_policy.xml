<?xml version="1.0" encoding="UTF-8"?>

<!-- Device configuration file for SRLTE with CH roaming 
$Header: $
-->
<policy name        = "generic"
        changelist  = "$Change: $" 
        enabled     = "true"
        schema_ver  = "1"
        policy_ver  = "49.4.1"
>

  <initial>
    <actions>

      <!-- List of the MCCs in which SRLTE is allowed -->
      <mcc_list name="sxlte_mccs"> 310 311 312 </mcc_list>

      <!-- These are the serving system PLMNs for which SRLTE will be allowed 
       -->
      <!-- NOTE: Proper functioning of the SXLTE policy requires that there
           be an PLMN list named "sxlte_plmns". Do NOT rename this list.
      -->
      <plmn_list name = "sxlte_plmns">
        311-480 310-01
      </plmn_list>

      <!-- Define the OOS timer with a 2 minute interval -->
      <define_timer name = "noservice" interval = "2" units = "min" id = "1" />

      <!-- Define the hybr_oos timer with a 1 minute interval -->
      <define_timer name = "hybr_oos" interval = "1" units = "min" id = "101" />

      <!-- Define device configuration as Single SIM -->
      <device_configuration num_sims="1" max_active="1">
        <config>
          <subs id="1" feature="srlte">
            <rat_capability base="none" >
              <include> CDMA HDR LTE </include>
            </rat_capability>
          </subs>
        </config>
      </device_configuration>

      <!-- Define initial RAT capability as CHL for SUBS1-->
      <rat_capability_if subs="1" base="none" >
        <include> CDMA HDR LTE </include>
      </rat_capability_if>

      <!-- Define Intial UE mode as 1X_CSFB_PREF -->
      <ue_mode_if subs="1"> 1X_CSFB_PREF </ue_mode_if>

      <!-- Define an RF band list that excludes TDS bands -->
      <rf_band_list name="rf_bands_home">
        <gw_bands base="hardware" />
        <lte_bands base="hardware" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- Define an RF band list that has CH bands -->
      <rf_band_list name="rf_bands_roam">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- On first boot, include FDD LTE bands for SUBS1 -->
      <rf_bands_if subs="1" list="rf_bands_home" />

      <!-- Boolean: Is the device at home? -->
      <boolean_define name="home" initial="true" />
      <boolean_set name="home">
        <ue_mode_is> 1X_CSFB_PREF </ue_mode_is>
      </boolean_set>

      <!-- Boolean: Is the device in Full RAT mode ? -->
      <boolean_define name="full_rat" initial="false" />

    </actions>
  </initial>

  <!--
  ====================================================================
    Rules to keep the UE in a specific mode.
  ====================================================================
  -->

  <!-- RULE #1 -->
  <!-- If the mode is set to 1XSRLTE_ONLY, do not bother evaluating other
       rules - just keep the UE in this mode.
  -->
  <rule precond="none">
    <conditions>
      <ue_mode_is subs="1"> 1XSRLTE_ONLY </ue_mode_is>
    </conditions>
    <actions>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR LTE </include>
      </rat_capability>
      <rf_bands subs="1" list="rf_bands_home" />
      <svc_mode subs="1"> FULL </svc_mode>
    </actions>
  </rule>
  
<!-- RULE #2 -->
  <!-- If the mode is set to CSFB_ONLY, do not bother evaluating other
       rules - just keep the UE in this mode.
  -->
  <rule precond="none">
    <conditions>
      <ue_mode_is subs="1"> CSFB_ONLY </ue_mode_is>
    </conditions>
    <actions>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR </include>
      </rat_capability>
      <rf_bands subs="1" list = "rf_bands_roam" />
      <svc_mode subs="1"> FULL </svc_mode>
    </actions>
  </rule>

<!--
  ====================================================================
    Rules to handle OOS situations and timers.
  ====================================================================
  -->

  <!-- RULE #3 -->
  <!-- This rule only runs when we have service on the mmode subs, and sets
       the "home" boolean based on mmode subs MCC.  As well, stop the timer,
       since we have mmode subs service, move to full service and continue
       evaluation.
  -->
  <rule precond="SS">
    <conditions>
      <service_status_in> FULL LIMITED LIMITED-REGIONAL </service_status_in> 
    </conditions>
    <actions>
      <boolean_set name="home">
        <serving_mcc_in list="sxlte_mccs" />
      </boolean_set>
      <svc_mode subs="1"> FULL </svc_mode>
      <timer_stop name="noservice" />
      <boolean_set name="full_rat" value="false" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #4 -->
  <!-- If UE is in Full RAT mode and timer not expired, stay in this config. 
           Here checking for timer not expired is fine as when boolean is made TRUE 
           then timer should also be running.
  -->
  <rule precond="none">
    <conditions>
      <boolean_test name="full_rat" />
      <not> <timer_expired name="noservice" /> </not>
    </conditions>
    <actions />
  </rule>

  <!-- RULE #5 -->
  <!-- If the oos timer has expired, enable Full RAT mode and restart the timer.
  -->
  <rule precond="none">
    <conditions>
      <timer_expired name="noservice" />
    </conditions>
    <actions>
      <expired_timer_handled name="noservice" />
      <ue_mode subs="1"> CSFB </ue_mode>
      <rf_bands subs="1" list="rf_bands_home" />
      <svc_mode subs="1"> ALL </svc_mode>
      <rat_capability subs="1" base = "config" />
      <timer_start name="noservice" />
      <boolean_set name="full_rat" value="true" />
    </actions>
  </rule>

  <!-- RULE #6 -->
  <!-- If we are in LPM, stop the OOS timer.
  -->
  <rule precond="none">
    <conditions>
      <any_of>
        <phone_operating_mode> SLEEP </phone_operating_mode>
        <phone_operating_mode> OFFLINE </phone_operating_mode>
      </any_of>
    </conditions>
    <actions>
      <timer_stop name="noservice" />
    </actions>
  </rule>

  <!-- RULE #7 -->
  <!-- If we have no service on the multimode subscription and we are ONLINE, 
          start the OOS timer and do not continue to other rules.
  -->
  <rule precond="none">
    <conditions>
      <phone_operating_mode> ONLINE </phone_operating_mode>
      <not> <service_status_in subs="mmode"> FULL LIMITED LIMITED-REGIONAL </service_status_in> </not>
    </conditions>
    <actions>
      <timer_start name="noservice" />
    </actions>
  </rule>
  
<!--
  ====================================================================
    Rules to handle legacy SIM card
  ====================================================================
  -->
  
  <!-- RULE #8 -->
  <!-- If SIM card is CSIM/RUIM and USIM is not available then keep UE in CH in HOME.
  -->
  <rule precond="none">
    <conditions>
      <any_of>
        <sim_type> CSIM </sim_type>
        <sim_type> RUIM </sim_type>
      </any_of>
      <not> <sim_type> 3G </sim_type> </not>
    </conditions>
    <actions>
      <rat_capability subs="1" base="none">
        <include> CDMA HDR </include>
      </rat_capability>
      <rf_bands subs="1" list = "rf_bands_roam" />
      <svc_mode subs="1"> FULL </svc_mode>
      <ue_mode subs="1"> CSFB </ue_mode>
    </actions>
  </rule>

<!--
  ====================================================================
    Rules to implement SRLTE
  ====================================================================
  -->

  <!-- RULE #9 -->
  <!-- If conditions allow, operate as SRLTE with CHL.
  -->
  <rule>
    <conditions>
      <boolean_test name="home" />
      <sim_type> 3G </sim_type>
    </conditions>
    
    <actions>
      <ue_mode subs="1"> 1X_CSFB_PREF </ue_mode>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR LTE </include>
      </rat_capability>
      <rf_bands subs="1" list="rf_bands_home" />
    </actions>
  </rule>

  <!-- RULE #10 -->
  <!-- Anything else is CSFB with CH.
  -->
  <rule>
    <conditions>
      <true />
    </conditions>
    <actions>
      <ue_mode subs="1"> CSFB </ue_mode>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR </include>
      </rat_capability>
      <rf_bands subs="1" list="rf_bands_roam" />
    </actions>
  </rule>
       
</policy>
