<?xml version="1.0" encoding="UTF-8"?>

<!-- Device configuration file for SRLTE with GW roaming -->

<policy name        = "generic"
        enabled     = "true"
        schema_ver  = "1"
        policy_ver  = "22.3.5"
>

  <initial>
    <actions>

      <!-- List of the MCCs in which SRLTE is allowed -->
      <mcc_list name="sxlte_mccs"> 001 002 003 004 005 006 007 008 009 010 011 012 455 460 310 330 374 </mcc_list>

      <!-- List of the non-SVLTE MCCs in which CDMA and HDR are allowed -->
      <mcc_list name="c2k_roaming_mccs"> 440 450 </mcc_list>

      <!-- These are the serving system PLMNs for which SXLTE will be allowed 
       -->
      <!-- NOTE: Proper functioning of the SXLTE policy requires that there
           be an PLMN list named "sxlte_plmns". Do NOT rename this list.
      -->
      <plmn_list name = "sxlte_plmns">
        455-02 460-03 460-11 001-01 330-01 374-01 310-00
      </plmn_list>

      <!-- Define the OOS timer with a 20 minute interval -->
      <define_timer name = "noservice" interval = "20" units = "min" id = "1" />

      <!-- Define the 1x_oos timer with a 1 minute interval -->
      <define_timer name = "1x_oos" interval = "1" units = "min" id = "100" />

      <!-- Define device configuration as Single SIM -->
      <device_configuration num_sims="1" max_active="1">
        <config>
          <subs id="1" feature="srlte">
            <rat_capability base="none" >
              <include> CDMA HDR GSM WCDMA LTE </include>
            </rat_capability>
          </subs>
        </config>
      </device_configuration>

      <!-- Define initial RAT capability as CHL for SUBS1-->
      <rat_capability_if subs="1" base="none" >
        <include> CDMA HDR LTE </include>
      </rat_capability_if>

      <!-- Define Intial UE mode as 1X_CSFB_PREF -->
      <ue_mode_if subs="1"> 1X_CSFB_PREF </ue_mode_if>

      <!-- Define an RF band list that excludes TDS bands -->
      <rf_band_list name="rf_bands_gwl">
        <gw_bands base="hardware" />
        <lte_bands base="hardware" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- Define an RF band list that has GW bands -->
      <rf_band_list name="rf_bands_gw">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- On first boot, include GWL bands -->
      <rf_bands_if subs="1" list="rf_bands_gwl" />

      <!-- Boolean: Is the device at home? -->
      <boolean_define name="home" initial="true" />
      <boolean_set name="home">
        <ue_mode_is> 1X_CSFB_PREF </ue_mode_is>
      </boolean_set>

      <!-- Boolean: Is the device in c2k_roam? -->
      <boolean_define name="c2k_roam" initial="false" />

      <!-- Boolean: Is the device in Full RAT mode ? -->
      <boolean_define name="full_rat" initial="false" />

      <boolean_define name="pm:provide_current_mcc" initial="true" />      

    </actions>
  </initial>

<!--
  ====================================================================
    Rules to handle OOS situations and timers.
  ====================================================================
  -->

  <!-- RULE #1 -->
  <!-- This rule only runs when we have service on the mmode subs, and sets
       the "home" boolean based on mmode subs MCC.  As well, stop the timer,
       since we have mmode subs service, move to full service, and continue
       evaluation.
  -->
  <rule>
    <conditions>
      <service_status_in> FULL LIMITED LIMITED-REGIONAL </service_status_in> 
    </conditions>
    <actions>
      <boolean_set name="home">
        <serving_mcc_in list="sxlte_mccs" />
      </boolean_set>
      <boolean_set name="c2k_roam">
        <serving_mcc_in list="c2k_roaming_mccs" />
      </boolean_set>
      <timer_stop name="noservice" />
      <svc_mode subs="1"> FULL </svc_mode>
      <boolean_set name="full_rat" value="false" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #2 -->
  <!-- If UE is in Full RAT mode and timer not expired, stay in this config. 
           Here checking for timer not expired is fine as when boolean is made TRUE 
           then timer should also be running.
  -->
  <rule>
    <conditions>
      <boolean_test name="full_rat" />
      <not> <timer_expired name="noservice" /> </not>
    </conditions>
    <actions />
  </rule>


  <!-- RULE #3 -->
  <!-- If the oos timer has expired and there is no service on other
       subscriptions, enable Full RAT mode and restart the timer.
  -->
  <rule always_run="true">
    <conditions>
      <timer_expired name="noservice" />
    </conditions>
    <actions>
      <expired_timer_handled name="noservice" />
      <ue_mode subs="1"> CSFB </ue_mode>
      <rat_capability subs="1" base = "config" />
      <rf_bands subs="1" list = "rf_bands_gwl" />
      <svc_mode subs="1"> ALL </svc_mode>
      <timer_start name="noservice" />
      <boolean_set name="full_rat" value="true" />
    </actions>
  </rule>

  <!-- RULE #4 -->
  <!-- If we are in LPM, stop the OOS timer.
  -->
  <rule always_run="true">
    <conditions>
      <phone_operating_mode> SLEEP </phone_operating_mode>
    </conditions>
    <actions>
      <timer_stop name="noservice" />
    </actions>
  </rule>

  <!-- RULE #5 -->
  <!-- If we have no service on the multimode subscription and we are ONLINE/our OOS timer has expired, 
          start the OOS timer and do not continue to other rules.
  -->
  <rule always_run="true">
    <conditions>
      <phone_operating_mode> ONLINE </phone_operating_mode>
      <not> <service_status_in subs="mmode"> FULL LIMITED LIMITED-REGIONAL </service_status_in> </not>
    </conditions>
    <actions>
      <timer_start name="noservice" />
    </actions>
  </rule>

<!--
  ====================================================================
    Rules to handle legacy SIM card
  ====================================================================
  -->
  
<!-- RULE #6 -->
  <!-- If SIM card is CSIM/RUIM and USIM is not available with UE in HOME then keep UE in CHG in HOME.
  -->
  <rule always_run="true">
    <conditions>
      <any_of>
        <sim_type> CSIM </sim_type>
        <sim_type> RUIM </sim_type>
      </any_of>
      <not> <sim_type> 3G </sim_type> </not>
      <serving_mcc_in list="sxlte_mccs" />
        </conditions>
    <actions>
      <rat_capability subs="1" base="none">
        <include> CDMA HDR GSM </include>
      </rat_capability>
      <rf_bands subs="1" list = "rf_bands_gw" />
      <svc_mode subs="1"> FULL </svc_mode>
      <ue_mode subs="1"> CSFB </ue_mode>
    </actions>
  </rule>

<!--
  ====================================================================
    Rules to implement SRLTE
  ====================================================================
  -->

  <!-- RULE #7 -->
  <!-- When home, include FDD LTE
  -->
  <rule>
    <conditions>
      <boolean_test name="home" />
    </conditions>
    <actions>
      <rf_bands subs="1" list="rf_bands_gwl" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #8 -->
  <!-- Only when outside the home area LTE bands are not allowed (CHGW).
  -->
  <rule>
    <conditions>
      <not> <boolean_test name="home" /> </not>
    </conditions>
    <actions>
      <rf_bands subs="1" list="rf_bands_gw" />
      <continue />
    </actions>
  </rule>

  <!-- RULE #9 -->
  <!-- If conditions allow, operate as SRLTE with CHL.
  -->
  <rule>
    <conditions>
      <boolean_test name="home" />
      <sim_type> 3G </sim_type>
      <user_domain_pref> CSPS </user_domain_pref>
      <any_of>
        <user_mode_pref_contains> CDMA LTE </user_mode_pref_contains>
        <user_mode_pref_contains> HDR LTE </user_mode_pref_contains>
      </any_of>
    </conditions>
    
    <actions>
      <ue_mode subs="1"> 1X_CSFB_PREF </ue_mode>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR LTE </include>
      </rat_capability>
    </actions>
  </rule>

  <!-- RULE #10 -->
  <!-- Anything else is CSFB
  -->
  <rule>
    <conditions>
      <true />
    </conditions>
    <actions>
      <ue_mode subs="1"> CSFB </ue_mode>
      <continue />
    </actions>
  </rule>
      
  <!-- RULE #11 -->
  <!-- If in "home" but can't do SRLTE, operate with CHL
  -->
  <rule>
    <conditions>
      <boolean_test name="home" />
    </conditions>
    <actions>
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR LTE </include>
      </rat_capability>
    </actions>
  </rule>
  
  <!-- RULE #12 -->
  <!-- Otherwise we're in a GSM and WCDMA configuration
  -->
  <rule>
    <conditions>
      <true />
    </conditions>
    <actions>
      <rat_capability subs="1" base="none">
        <include> GSM WCDMA </include>
      </rat_capability>
    </actions>
  </rule>
  
</policy>
