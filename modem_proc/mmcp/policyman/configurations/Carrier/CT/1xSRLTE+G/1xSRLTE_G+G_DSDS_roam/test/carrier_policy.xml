<?xml version="1.0" encoding="UTF-8"?>

<!-- Device configuration file for SRLTE+G with G+G DSDS roaming -->

<policy name        = "generic"
        enabled     = "true"
        schema_ver  = "1"
        policy_ver  = "24.3.5"
>

  <initial>
    <actions>

      <!-- List of the MCCs in which SRLTE is allowed -->
      <mcc_list name="sxlte_mccs"> 001 002 003 004 005 006 007 008 009 010 011 012 455 460 310 330 374 </mcc_list>

      <!-- List of the non-SVLTE MCCs in which CDMA and HDR are allowed -->
      <mcc_list name="c2k_roaming_mccs"> 440 450 </mcc_list>

      <!-- These are the serving system PLMNs for which SRLTE will be allowed 
       -->
      <!-- NOTE: Proper functioning of the SRLTE policy requires that there
           be an PLMN list named "sxlte_plmns". Do NOT rename this list.
      -->
      <plmn_list name = "sxlte_plmns">
        455-02 460-03 460-11 001-01 330-01 374-01 310-00
      </plmn_list>

      <!-- Define the OOS timer with a 1 minute interval -->
      <define_timer name = "noservice" interval = "1" units = "min" id = "1" />

      <!-- Define the 1x_oos timer with a 1 minute interval -->
      <define_timer name = "1x_oos" interval = "1" units = "min" id = "100" />

      <!-- Define device configuration as DSDS -->
      <device_configuration num_sims="2" max_active="1" specialization="dsds_roam">
        <config>
          <subs id="1" feature="srlte">
            <rat_capability base="none" >
              <include> CDMA HDR GSM LTE </include>
            </rat_capability>
          </subs>
          <subs id="2">
            <rat_capability base="none">
              <include> GSM </include>
            </rat_capability>
          </subs>
        </config>
      </device_configuration>

      <!-- Define initial RAT capability as CHL for SUBS1 -->
      <rat_capability_if subs="1" base="none" >
        <include> CDMA HDR LTE </include>
      </rat_capability_if>

      <!-- Define initial RAT capability as GSM for SUBS2 -->
      <rat_capability subs="2" base="none" >
        <include> GSM </include>
      </rat_capability>

      <!-- Define Intial UE mode as 1X_CSFB_PREF -->
      <ue_mode_if subs="1"> 1X_CSFB_PREF </ue_mode_if>

      <!-- Define an RF band list that excludes TDS bands -->
      <rf_band_list name="rf_bands_gwl">
        <gw_bands base="hardware" />
        <lte_bands base="hardware" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- Define an RF band list that has GW bands -->
      <rf_band_list name="rf_bands_gw">
        <gw_bands base="hardware" />
        <lte_bands base="none" />
        <tds_bands base="none" />
      </rf_band_list>

      <!-- On first boot, include GWL bands for SUBS1 -->
      <rf_bands_if subs="1" list="rf_bands_gwl" />

      <!-- On first boot, include GSM bands for SUBS2 -->
      <rf_bands subs="2" list="rf_bands_gw" />

      <!-- Boolean: Is the device at home? -->
      <boolean_define name="home" initial="true" />
      <boolean_set name="home">
        <ue_mode_is subs="1"> 1X_CSFB_PREF </ue_mode_is>
      </boolean_set>

      <scan_optimization tgpp="no_auto_camp_only" tgpp2="wrlf" />
						
      <!-- Boolean: Is the device in c2k_roam? -->
      <boolean_define name="c2k_roam" initial="false" />

      <boolean_define name="pm:provide_current_mcc" initial="true" />      
      
    </actions>
  </initial>

  <!-- RULE #1 -->
  <!-- If we are in LPM, stop the OOS timer.
  -->
  <rule precond="none">
    <conditions>
      <phone_operating_mode> SLEEP </phone_operating_mode>
    </conditions>
    <actions>
      <timer_stop name="noservice" /> 
    </actions>
  </rule>

  <!--
  ====================================================================
    Rules to handle OOS situations and timers.
  ====================================================================
  -->

  <!-- RULE #2 -->
  <!-- If the oos timer has expired and there is no service on other
           subscriptions, enable Full RAT mode with DSDS and restart the timer.
  -->
  <rule precond="none">
    <conditions>
      <timer_expired name="noservice" />
      <service_status_in subs="non-mmode"> OOS POWER_SAVE </service_status_in>
    </conditions>
    <actions>
      <expired_timer_handled name="noservice" />
      <ue_mode subs="1"> CSFB </ue_mode>
      <rat_capability subs="1" base="config" />
      <rf_bands subs="1" list = "rf_bands_gwl" />
      <svc_mode subs="1"> ALL </svc_mode>
      <timer_start name="noservice" />
    </actions>
  </rule>

  <!-- RULE #3 -->
  <!-- Stop timer, move to Full Camping and determine location if we have service on any SUBS.
  -->
  <rule precond="none">
    <conditions>
      <have_service />
    </conditions>
    <actions>
      <timer_stop name="noservice" />
      <svc_mode subs="1"> FULL </svc_mode>
      <boolean_set name="home">
        <location_mcc_in list="sxlte_mccs" />
      </boolean_set>
      <boolean_set name="c2k_roam">
        <location_mcc_in list="c2k_roaming_mccs" />
      </boolean_set>
      <continue />
    </actions>
  </rule>

<!--
  ====================================================================
    Rules to keep the UE in a specific mode.
  ====================================================================
  -->

  <!-- RULE #4 -->
  <!-- If SIM card is CSIM/RUIM and USIM is not available with UE in HOME/C2k countries then keep UE in C+G DSDS. 
  -->
  <rule precond="SIM_TYPE">
    <conditions>
      <any_of>
        <sim_type> CSIM </sim_type>
        <sim_type> RUIM </sim_type>
      </any_of>
      <not> <sim_type> 3G </sim_type> </not>
      <any_of>
        <boolean_test name="home" />
        <boolean_test name="c2k_roam" />
      </any_of>
    </conditions>
    <actions>
      <rat_capability subs="1" base="none">
        <include> CDMA HDR </include>
      </rat_capability>
      <rf_bands subs="1" list = "rf_bands_gw" />
      <svc_mode subs="1"> FULL </svc_mode>
      <ue_mode subs="1"> CSFB </ue_mode>
    </actions>
  </rule>

  <!-- RULE #5 -->
  <!-- If we are online and we are OOS on all SUBS then start OOS timer.
  -->
  <rule precond="none">
    <conditions>
      <phone_operating_mode> ONLINE </phone_operating_mode>
      <not> <have_service /> </not>
    </conditions>
    <actions>
      <timer_start name="noservice" />
    </actions>
  </rule>

  <!--
  ====================================================================
    Rules where location info can be used and implement SRLTE.
  ====================================================================
  -->
      
  <!-- RULE #6 -->
  <!-- If we are home, stay in SRLTE configuration with CHL.
  -->
  <rule precond="none">
    <conditions>
      <have_service />
      <boolean_test name="home" />
    </conditions>
    <actions>
      <rf_bands subs="1" list = "rf_bands_gwl" />
      <rat_capability subs="1" base = "none">
        <include> CDMA HDR LTE </include>
      </rat_capability>
      <ue_mode subs="1"> 1X_CSFB_PREF </ue_mode>
    </actions>
  </rule>
  
  <!-- RULE #7 -->
  <!-- If we are Roam in c2k countries, stay in CSFB configuration with RAT capability CHG
  -->
  <rule precond="none">
    <conditions>
      <have_service />
      <boolean_test name="c2k_roam" />
    </conditions>
    <actions>
      <rf_bands subs="1" list = "rf_bands_gw" />
      <rat_capability subs="1" base="none" >
        <include> CDMA GSM HDR </include>
      </rat_capability>
      <ue_mode subs="1"> CSFB </ue_mode>
    </actions>
  </rule>

  <!-- RULE #8 -->
  <!-- If we are Roam outside c2k countries, stay in CSFB configuration with RAT capability G
  -->
  <rule precond="none">
    <conditions>
      <have_service />
    </conditions>
    <actions>
      <rf_bands subs="1" list = "rf_bands_gw" />
      <rat_capability subs="1" base="none">
        <include> GSM </include>
      </rat_capability>
      <ue_mode subs="1"> CSFB </ue_mode>
    </actions>
  </rule>

</policy>
