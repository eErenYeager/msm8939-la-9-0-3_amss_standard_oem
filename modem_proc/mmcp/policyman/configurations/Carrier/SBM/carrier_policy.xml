<?xml version="1.0" encoding="UTF-8"?>

<policy name            = "generic"
        enabled         = "true"
        schema_ver      = "1"
        policy_ver      = "10.2.2"
>

  <initial>
    <actions>

      <!-- Define the OOS timer with a 2 minute interval -->
      <define_timer name = "noservice" interval = "1" units = "min" />

      <!-- Define intial service mode as LIMITED -->
      <svc_mode> LIMITED </svc_mode>

      <!-- Set device configuration to single SIM and specialization to SBM -->
      <device_configuration
          specialization = "sbm_rat_search_order"
          num_sims = "1" max_active = "1"
      >

        <config>
          <subs id = "1">
            <rat_capability base = "none">
              <include> WCDMA LTE </include>
            </rat_capability>
          </subs  >
        </config  >

      </device_configuration>

      <rf_band_list name = "profile_B">
        <!-- GSM Band mapping: WCDMA bands 1, 5, 9 should be disabled
                B1->SYS_SBAND_WCDMA_I_IMT_2000->Bit-22,
                B5->SYS_SBAND_WCDMA_V_850->Bit-26
                B9->SYS_SBAND_WCDMA_IX_1700->Bit-50.
             Refer: sys_sband_e_type in sys.h
        -->
        <gw_bands base = "hardware">
          <exclude> 22 26 50 </exclude>
        </gw_bands>
        <lte_bands base = "hardware" />
        <tds_bands base = "hardware" />
      </rf_band_list>

      <rf_band_list name = "profile_C">
        <!-- GSM Band mapping: WCDMA bands 1, 3, 5 & 11 should be disabled
                B1->SYS_SBAND_WCDMA_I_IMT_2000->Bit-22,
                B3->SYS_SBAND_WCDMA_III_1700->Bit-24.
                B5->SYS_SBAND_WCDMA_V_850->Bit-26
                B11->SYS_SBAND_WCDMA_XI_1500->Bit-61
             Refer: sys_sband_e_type in sys.h
        -->
        <gw_bands base = "hardware">
          <exclude> 22 24 26 61 </exclude>
        </gw_bands>
        <!-- LTE Band: 1-26 & 41 are mapped to Band Bits: 0-25 & 40
             respectively, should be disabled
             Refer: sys_sband_lte_e_type in sys.h
        -->
        <lte_bands base = "none" />
        <tds_bands base = "hardware" />
      </rf_band_list>

      <rf_band_list name = "profile_D">
        <gw_bands base = "hardware" />
        <!-- LTE Band: 1-26 & 41 are mapped to Band Bits: 0-25 & 40
             respectively, should be disabled
             Refer: sys_sband_lte_e_type in sys.h
        -->
        <lte_bands base = "none" />
        <tds_bands base = "hardware" />
      </rf_band_list>

    </actions>
  </initial>

  <!-- RULE #1 -->
  <!-- If the oos timer has expired, enable Full RAT mode.
  -->
  <rule always_run="true">
    <conditions>
      <timer_expired name="noservice" />
    </conditions>

    <actions>
      <expired_timer_handled name="noservice" />
      <rat_capability base = "config" />
      <rf_bands base = "hardware" />
      <svc_mode> LIMITED </svc_mode>
    </actions>
  </rule>

  <!-- RULE #2 -->
  <!-- If we have no service on any subscription, start the OOS timer.
  -->
  <rule>
    <conditions>
      <service_status_in > OOS POWER_SAVE </service_status_in>
    </conditions>

    <actions>
      <timer_start name="noservice" />
    </actions>
  </rule>

 <!-- Rule#3: If Operating mode changes to sleep mode, enable all RATs/Bands and go to LIMITED service-->
  <rule>
    <conditions>
      <phone_operating_mode> SLEEP </phone_operating_mode>
    </conditions>

    <actions>
      <rat_capability base = "config" />  
      <rf_bands base = "hardware" />
      <svc_mode> LIMITED </svc_mode>
    </actions>
  </rule>

  <!-- Rule#4: If the Serving MCC is MCC = 440, serving band is LTE FDD-LTE Band 3, service mode is Full Service, current service status is LIMITED 
          and registration reject cause is 14 or 15 - apply profile [C] 
           For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> LTE </serving_rat_in>
      <serving_band_in> 122 </serving_band_in>
      <serving_plmn_in> 440-00 </serving_plmn_in>
      <svc_mode_is> FULL </svc_mode_is>
      <service_status_in> LIMITED-REGIONAL LIMITED </service_status_in>
      <reg_reject_cause_in> 14 15 </reg_reject_cause_in>
    </conditions>

    <actions>
      <rat_capability base = "config" >
        <exclude> LTE </exclude>
      </rat_capability>
      <rf_bands list = "profile_C" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>

  <!-- Rule#5: If the Serving MCC is MCC = 440, serving band is LTE FDD-LTE Band 3, service mode is Full Service, current service status is LIMITED 
          and registration reject cause is 11 - apply profile [C] 
           For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> WCDMA </serving_rat_in>
      <serving_band_in> 88 </serving_band_in>
      <serving_plmn_in> 440-00 </serving_plmn_in>
      <svc_mode_is> FULL </svc_mode_is>
      <service_status_in> LIMITED-REGIONAL LIMITED </service_status_in>
      <reg_reject_cause_in> 11 </reg_reject_cause_in>
    </conditions>

    <actions>
      <rat_capability base = "config" >
        <exclude> LTE </exclude>
      </rat_capability>
      <rf_bands list = "profile_C" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>

  <!-- Rule#6: If the Serving MCC is Japan MCC (440, 441) and serving band is LTE AXGP or FDD-LTE Band 3 - apply profile [B]
           For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> LTE </serving_rat_in>
      <serving_band_in> 122 160 </serving_band_in>
      <serving_mcc_in> 440 441 </serving_mcc_in>
    </conditions>

    <actions>
      <rat_capability base = "config" />
      <rf_bands list = "profile_B" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>
  
  <!-- Rule#7: If the Serving MCC is Japan MCC (440, 441) and serving band is WCDMA Band 11 - apply profile [B]
           For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> WCDMA </serving_rat_in>
      <serving_band_in> 90 </serving_band_in>
      <serving_mcc_in> 440 441 </serving_mcc_in>
    </conditions>

    <actions>
      <rat_capability base = "config" />
      <rf_bands list = "profile_B" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>


<!-- Rule#8: If the Serving MCC is Japan MCC (440, 441) and serving band is WCDMA Band 9, exclude LTE - - apply profile [C]
       For Serving band, refer to sys_band_class_e_type in sys.h
-->
  <rule>
    <conditions>
      <serving_rat_in> WCDMA </serving_rat_in>
      <serving_band_in> 88 </serving_band_in>
      <serving_mcc_in> 440 441 </serving_mcc_in>
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> LTE </exclude>
      </rat_capability>  
      <rf_bands list = "profile_C" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>

  <!-- Rule#9: If the Serving MCC is not Japan MCC (!= 440, 441) and serving band is LTE AXGP or FDD-LTE Band 3, exclude LTE - - apply profile [D]
       For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> LTE </serving_rat_in>
      <serving_band_in> 122 160 </serving_band_in>
      <not>
        <serving_mcc_in> 440 441 </serving_mcc_in>
      </not>
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> LTE </exclude>
      </rat_capability>  
      <rf_bands list = "profile_D" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>

  <!-- Rule#10: If the Serving MCC is not Japan MCC (!= 440, 441) and serving band is WCDMA Band 1, 5, 9 or 11, exclude LTE - - apply profile [D]
       For Serving band, refer to sys_band_class_e_type in sys.h
  -->
  <rule>
    <conditions>
      <serving_rat_in> WCDMA </serving_rat_in>
      <serving_band_in> 80 84 88 90 </serving_band_in>
      <not>
        <serving_mcc_in> 440 441 </serving_mcc_in>
      </not>
    </conditions>

    <actions>
      <rat_capability base = "config">
        <exclude> LTE </exclude>
      </rat_capability>  
      <rf_bands list = "profile_D" />
      <svc_mode> FULL </svc_mode>
    </actions>
  </rule>

  <!-- Rule#11: Default rule, enable everything and goto LIMITED service-->
  <rule>
    <conditions>
      <true/>
    </conditions>
    
    <actions>                            
      <rat_capability base = "config" />
      <rf_bands base = "hardware" />
      <svc_mode> LIMITED </svc_mode>                              
    </actions>
  </rule>

</policy>

