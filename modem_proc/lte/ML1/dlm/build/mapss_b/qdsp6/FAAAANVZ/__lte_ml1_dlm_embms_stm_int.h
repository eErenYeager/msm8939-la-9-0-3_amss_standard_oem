/*=============================================================================

    __lte_ml1_dlm_embms_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_dlm_embms_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_DLM_EMBMS_STM_INT_H
#define __LTE_ML1_DLM_EMBMS_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_dlm_embms_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_DLM_EMBMS_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_DLM_EMBMS_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_DLM_EMBMS_SM_NUM_STATES 4

/* Define a macro for the number of SM inputs */
#define LTE_ML1_DLM_EMBMS_SM_NUM_INPUTS 9

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_dlm_embms_stm_entry(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_dlm_embms_stm_inactive_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_embms_stm_inactive_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_embms_stm_init_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_embms_stm_pmch_monitor_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_embms_stm_pmch_monitor_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_embms_stm_suspend_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_dlm_embms_stm_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_resume(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_cleanup(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_msi(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_evnt_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_evnt_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_evnt_abort_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_embms_stm_pmch_stop_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
  LTE_ML1_DLM_EMBMS_INIT_STATE,
  LTE_ML1_DLM_EMBMS_PMCH_MONITOR_STATE,
  LTE_ML1_DLM_EMBMS_SUSPEND_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_DLM_EMBMS_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_DLM_EMBMS_STM_INT_H */
