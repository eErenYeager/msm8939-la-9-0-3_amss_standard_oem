/*!
  @file
  __lte_ml1_dlm_embms_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_DLM_EMBMS_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_dlm_embms_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_dlm_embms_stm_type;


/*! @brief Variables internal to module __lte_ml1_dlm_embms_stm.stub
*/
STATIC __lte_ml1_dlm_embms_stm_type __lte_ml1_dlm_embms_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_DLM_EMBMS_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_dlm_embms_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_DLM_EMBMS_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_entry() */


/*===========================================================================

     (State Machine: LTE_ML1_DLM_EMBMS_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_DLM_EMBMS_INACTIVE_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_dlm_embms_stm_inactive_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_inactive_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_inactive_state_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_dlm_embms_stm_inactive_state_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_inactive_state_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_inactive_state_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_cfg

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_EMBMS_CFG_REQ

    @detail
    Called upon receipt of input LTE_ML1_DLM_EMBMS_CFG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_cfg
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_cfg() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_suspend

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_EMBMS_SUSPEND_REQ

    @detail
    Called upon receipt of input LTE_ML1_DLM_EMBMS_SUSPEND_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_suspend
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_suspend() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_resume

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_EMBMS_RESUME_REQ

    @detail
    Called upon receipt of input LTE_ML1_DLM_EMBMS_RESUME_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_resume
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_resume() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_cleanup

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_EMBMS_CLEANUP_REQ

    @detail
    Called upon receipt of input LTE_ML1_DLM_EMBMS_CLEANUP_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_cleanup
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_cleanup() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_msi

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_CPHY_MSI_REQ

    @detail
    Called upon receipt of input LTE_CPHY_MSI_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_msi
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_msi() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_evnt_start_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_PMCH_EVNT_START_IND

    @detail
    Called upon receipt of input LTE_ML1_DLM_PMCH_EVNT_START_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_evnt_start_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_evnt_start_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_evnt_end_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_PMCH_EVNT_END_IND

    @detail
    Called upon receipt of input LTE_ML1_DLM_PMCH_EVNT_END_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_evnt_end_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_evnt_end_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_evnt_abort_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_PMCH_EVNT_ABORT_IND

    @detail
    Called upon receipt of input LTE_ML1_DLM_PMCH_EVNT_ABORT_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_evnt_abort_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_evnt_abort_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_dlm_embms_stm_pmch_stop_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INACTIVE_STATE,
    upon receiving input LTE_ML1_DLM_PMCH_DECODE_STOP_CNF

    @detail
    Called upon receipt of input LTE_ML1_DLM_PMCH_DECODE_STOP_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_dlm_embms_stm_pmch_stop_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_dlm_embms_stm_pmch_stop_cnf() */


/*===========================================================================

     (State Machine: LTE_ML1_DLM_EMBMS_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_DLM_EMBMS_INIT_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_dlm_embms_stm_init_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_INIT_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_init_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_init_state_entry() */


/*===========================================================================

     (State Machine: LTE_ML1_DLM_EMBMS_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_DLM_EMBMS_PMCH_MONITOR_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_dlm_embms_stm_pmch_monitor_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_PMCH_MONITOR_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_pmch_monitor_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_pmch_monitor_state_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_dlm_embms_stm_pmch_monitor_state_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_PMCH_MONITOR_STATE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_pmch_monitor_state_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_pmch_monitor_state_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_DLM_EMBMS_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_DLM_EMBMS_SUSPEND_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_dlm_embms_stm_suspend_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_DLM_EMBMS_SM,
    state LTE_ML1_DLM_EMBMS_SUSPEND_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dlm_embms_stm_suspend_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dlm_embms_stm_suspend_state_entry() */




