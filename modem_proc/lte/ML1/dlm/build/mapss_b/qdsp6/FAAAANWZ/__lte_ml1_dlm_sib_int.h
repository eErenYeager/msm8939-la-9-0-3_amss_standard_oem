/*=============================================================================

    __lte_ml1_dlm_sib_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_dlm_sib.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_DLM_SIB_INT_H
#define __LTE_ML1_DLM_SIB_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_dlm_sib.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_DLM_SIB_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_DLM_SIB_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_DLM_SIB_SM_NUM_STATES 4

/* Define a macro for the number of SM inputs */
#define LTE_ML1_DLM_SIB_SM_NUM_INPUTS 20

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_dlm_sib_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_dlm_sib_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_dlm_sib_stm_init_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_init_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_idle_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_idle_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_sib_decode_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_sib_decode_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_sib_stm_cell_switching_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_dlm_sib_stm_init_sib_sched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_init_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_no_op(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_si_start_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib_cswitch_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_int_sib_sched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_x2l_ho_prep_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_idle_mib_comp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib_dec_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_mib_evnt_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_mib_evnt_abort_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib_sched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib1_win_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_si_msg_win_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_mod_prd_bnd_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_mib_comp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib1_puncture_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_sib_env_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_int_sib1_win_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_srch_mode_change_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_cswitch_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sib_stm_cswitch_sib_sched_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_DLM_SIB_INIT_STATE,
  LTE_ML1_DLM_SIB_IDLE_STATE,
  LTE_ML1_DLM_SIB_DECODE_STATE,
  LTE_ML1_DLM_SIB_CELL_SWITCHING_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_DLM_SIB_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_DLM_SIB_INT_H */
