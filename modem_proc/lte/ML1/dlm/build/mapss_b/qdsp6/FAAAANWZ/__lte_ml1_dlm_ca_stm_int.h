/*=============================================================================

    __lte_ml1_dlm_ca_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_dlm_ca_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_DLM_CA_STM_INT_H
#define __LTE_ML1_DLM_CA_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_dlm_ca_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_DLM_CA_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_DLM_CA_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_DLM_CA_SM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_ML1_DLM_CA_SM_NUM_INPUTS 41

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_dlm_ca_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_dlm_ca_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_dlm_ca_stm_add_recfg_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_stm_delete_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_stm_activate_cc_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_stm_activate_cc_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_stm_deactivate_cc_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_abort_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_ca_stm_rf_tune_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_dlm_ca_stm_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_act_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_conn_rel_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_done_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_tx_tune_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_idle_state_release_res_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_idle_state_acquire_res_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_idle_ca_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_idle_ca_pcc_cswitch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_ho_trigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_tigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_pdcch_rnti_change_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_vrlf_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_stat_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_dl_ch_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_proc_trigger_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_dta_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_trm_acq_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_add_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_init_srch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_slam_time_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_semi_stat_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_state_update_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_obj_abort_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_pend_action_trigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_del_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_dl_deconfig_trigger(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_stat_decfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_mcpm_proc_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_act_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_proc_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_scc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_dl_deactivate_trigger(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_ca_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_deconfig_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_rf_tune_tx_tune_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_rf_tune_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_state_release_res_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_state_scc_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_state_acquire_res_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_state_rf_tune_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_suspend_tx_tune_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_ca_stm_abort_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_DLM_CA_PROC_IDLE_STATE,
  LTE_ML1_DLM_CA_ADD_RECFG_CC_STATE,
  LTE_ML1_DLM_CA_DELETE_CC_STATE,
  LTE_ML1_DLM_CA_ACTIVATE_CC_STATE,
  LTE_ML1_DLM_CA_DEACTIVATE_CC_STATE,
  LTE_ML1_DLM_CA_ABORT_CC_STATE,
  LTE_ML1_DLM_CA_RF_TUNE_CC_STATE,
  LTE_ML1_DLM_CA_SUSPEND_STATE,
  LTE_ML1_DLM_CA_ABORT_TRIGGER_CC_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_DLM_CA_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_DLM_CA_STM_INT_H */
