/*=============================================================================

    __lte_ml1_dlm_rach_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_dlm_rach.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_DLM_RACH_INT_H
#define __LTE_ML1_DLM_RACH_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_dlm_rach.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_DLM_RACH_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_DLM_RACH_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_DLM_RACH_SM_NUM_STATES 4

/* Define a macro for the number of SM inputs */
#define LTE_ML1_DLM_RACH_SM_NUM_INPUTS 18

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_dlm_rach_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_dlm_rach_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_dlm_rach_stm_init_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_rach_stm_msg2_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_rach_stm_msg2_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_rach_stm_msg4_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_rach_stm_msg4_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_rach_stm_complete_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_dlm_rach_stm_cfg_rsp_window(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_init_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_cancel_conn_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_no_op(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_abort_trigger(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_conn_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_obj_dl_rx_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_test_skip_rach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cdrx_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cdrx_evnt_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_cont_res(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_x2l_ho_prep_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_schdl_rar(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_msg2_expiry(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_rar_param(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_ra_timer_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_msg3_tx_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_rach_stm_complete_cfg_rsp_window(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_DLM_RACH_INIT_STATE,
  LTE_ML1_DLM_RACH_MSG2_STATE,
  LTE_ML1_DLM_RACH_MSG4_STATE,
  LTE_ML1_DLM_RACH_COMPLETE_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_DLM_RACH_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_DLM_RACH_INT_H */
