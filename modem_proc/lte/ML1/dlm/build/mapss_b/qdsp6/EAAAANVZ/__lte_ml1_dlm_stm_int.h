/*=============================================================================

    __lte_ml1_dlm_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_dlm_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_DLM_STM_INT_H
#define __LTE_ML1_DLM_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_dlm_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_DLM_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_DLM_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_DLM_SM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_ML1_DLM_SM_NUM_INPUTS 51

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_dlm_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_dlm_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_dlm_stm_init_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_init_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_acq_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_acq_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_aborting_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_aborting_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_init_idle_drx_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_cleanup_idle_drx_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_cswitch_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_stm_cswitch_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_init_connected_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_cleanup_connected_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_init_rx_cfg_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_dlm_cleanup_rx_cfg_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_dlm_stm_init_proc_acq_stage2_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_init_acq_stage2(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_init_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_abort_trigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_irat2l_dl_slam_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_irat2l_ttrans_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_plmn_cswitch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_int_nbr_tdd_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_init_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_suspend_prep_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_pdsch_stat_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_semi_stat_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_x2l_ho_prep_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_trm_acq_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_bimc_switch_obj_start_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_bimc_switch_obj_abort_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_irat2l_dl_slam_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_irat2l_ttrans_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_mcvs_notify_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_init_acq_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_acq_rx_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_acq_state_proc_cell_switch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_acq_state_proc_dl_ch_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_acq_state_proc_cell_dyn_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_acq2_abort_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_aborting_rx_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_tune_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_ca_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_aborting_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_proc_rnti_change_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_ant_switch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_embms_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_com_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_ded_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_cell_switch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_ll_chan_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_gm_recfg_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_sm_recfg_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_pos_prs_recfg_suspend_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_ca_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_ca_act_deact_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_drx_paging_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_po_win_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_po_env_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_block_ns_s_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_block_s_ns_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_sfn_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_move_to_conn_state(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_ant_switch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_ant_mode_chg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_pmch_intra_cswitch_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_rx_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_cswitch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_ll_ch_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_semi_stat_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_cswitch_state_ca_pcc_cswitch_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_proc_ho_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_conn_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_dlm_stm_rx_cfg_no_trm_ind(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_DLM_INIT_STATE,
  LTE_ML1_DLM_IRAT2L_DL_PREP_STATE,
  LTE_ML1_DLM_ACQ_STATE,
  LTE_ML1_DLM_ABORTING_STATE,
  LTE_ML1_DLM_ABORT_TRIGGER,
  LTE_ML1_DLM_IDLE_DRX_STATE,
  LTE_ML1_DLM_CELL_SWITCHING_STATE,
  LTE_ML1_DLM_CONNECTED_STATE,
  LTE_ML1_DLM_RX_CONFIG_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_DLM_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_DLM_STM_INT_H */
