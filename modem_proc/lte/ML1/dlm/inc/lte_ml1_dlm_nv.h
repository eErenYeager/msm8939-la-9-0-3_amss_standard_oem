/*!
  @file
  lte_ml1_dlm_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/inc/lte_ml1_dlm_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_DLM_NV_H
#define LTE_ML1_DLM_NV_H

#include <comdef.h>	
#include "lte_ml1_comdef.h"
/*===========================================================================
	
							   INCLUDE FILES
	
===========================================================================*/
#define LTE_ML1_EMBMS_PRIORITY_CFG_CSG_PREFERRED_OVER_EMBMS     0x01
#define LTE_ML1_EMBMS_PRIORITY_CFG_EMBMS_PREFERRED_OVER_UNICAST 0x02
#define LTE_ML1_EMBMS_PRIORITY_CFG_EMBMS_DISABLED               0x04



#ifndef FEATURE_BOLT_MODEM
/*! Default BIMC\SNOC switching algorithm default values */
typedef struct{
  /*! TB size (in bits) */
  uint32 tb_size_threshold;

  /*! No. of allocations*/
  uint8  allocation_threshold;
  
  /*!MCS*/
  uint8  mcs_threshold;

  // DRX threshold (in ms)
  uint8 drx_threshold;

  // Inactivity threshold (in ms)
  uint8 inact_threshold;

  // Ignore MCS threshold (in bits)
  uint32 ignore_mcs_threshold;
} lte_ml1_nv_cfg_bimc_snoc_default_switching_params_s;
#endif


	
	
/*===========================================================================
	
					  MODULE DEFINITIONS AND TYPES
	
===========================================================================*/

/*===========================================================================
 
   FUNCTION:  lte_ml1_dlm_nv_unknown_mbsfn_cfg_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_dlm_nv_unknown_mbsfn_cfg_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_dlm_nv_unknown_mbsfn_cfg_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

  void lte_ml1_dlm_nv_unknown_mbsfn_cfg_defaultvalue(void *ptr);

/*===========================================================================
 
   FUNCTION:  lte_ml1_dlm_nv_embms_priority_cfg_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_dlm_nv_embms_priority_cfg_size(void);
 
/*===========================================================================
 
   FUNCTION:  lte_ml1_dlm_nv_embms_priority_cfg_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

  void lte_ml1_dlm_nv_embms_priority_cfg_defaultvalue(void *ptr);

/*===========================================================================
	 
	 FUNCTION:	lte_ml1_dlm_nv_embms_priority_cfg_post_efsread
	 
  ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework, 
	 to perform client defined post efsread functionality.
	 
	 @details
	 .
	 
	 @return
 */
 /*=========================================================================*/
 
  void lte_ml1_dlm_nv_embms_priority_cfg_post_efsread(void *ptr);


#ifndef FEATURE_BOLT_MODEM

/*===========================================================================
   
	 FUNCTION:	lte_ml1_dlm_nv_bimc_snoc_cfg_size
   
 ===========================================================================*/
/*!
	 @brief
	 This function returns the size of EFS variable in bytes. 
	 This size information is used by EFS framework for memory 
	 allocation and storing the EFS value.
 		  
	 @details
   
	 @return size in bytes
 */
/*=========================================================================*/

  uint8 lte_ml1_dlm_nv_bimc_snoc_cfg_size(void);

/*===========================================================================
   
	 FUNCTION:	lte_ml1_dlm_nv_bimc_snoc_cfg_defaultvalue
   
===========================================================================*/
/*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
   
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
   
	 @return
*/
/*=========================================================================*/


  void lte_ml1_dlm_nv_bimc_snoc_cfg_defaultvalue(void *ptr);

#endif

/*===========================================================================
   
	 FUNCTION:	lte_ml1_dlm_nv_assert_on_vrlf_cause_size
   
 ===========================================================================*/
/*!
	 @brief
	 This function returns the size of EFS variable in bytes. 
	 This size information is used by EFS framework for memory 
	 allocation and storing the EFS value.
 		  
	 @details
   
	 @return size in bytes
 */
/*=========================================================================*/

  uint8 lte_ml1_dlm_nv_assert_on_vrlf_cause_size(void);

/*===========================================================================
   
	 FUNCTION:	lte_ml1_dlm_nv_assert_on_vrlf_cause_defaultvalue
   
===========================================================================*/
/*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
   
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
   
	 @return
*/
/*=========================================================================*/


  void lte_ml1_dlm_nv_assert_on_vrlf_cause_defaultvalue(void *ptr);


 
#endif

