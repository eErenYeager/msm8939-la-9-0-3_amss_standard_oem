/*!
  @file
  lte_ml1_dlm_embms_stm.h

  @brief
  eMBMS State Machine header
*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/src/lte_ml1_dlm_embms_stm.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/19/13   ss     Initial version


===========================================================================*/

#include "lte_ml1_comdef.h"
#include "lte_variation.h"
#ifdef FEATURE_LTE_ML1_EMBMS



#ifndef LTE_ML1_DLM_EMBMS_STM_H
#define LTE_ML1_DLM_EMBMS_STM_H


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/




/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/



typedef struct lte_ml1_dlm_embms_state_data_s lte_ml1_dlm_embms_state_data_s;
typedef struct lte_ml1_dlm_embms_db_s lte_ml1_dlm_embms_db_s;       

#include "lte_ml1_dlm_pmch_monitor.h"
#include "lte_ml1_dlm_embms_data.h"
#include "lte_ml1_dlm_mbsfn_strength_meas.h"


/*!
  eMBMS state related data
*/
struct lte_ml1_dlm_embms_state_data_s
{  
  /* eMBMS suspend cause */
  lte_ml1_dlm_embms_suspend_resume_cause_e   suspend_resume_cause;

  lte_ml1_dlm_embms_suspend_resume_cause_e   suspend_resume_cause_ext;

  /* Suspend confirmation callback */
  lte_ml1_dlm_embms_suspend_cnf_cb_t         suspend_cnf_cb;

  /* Pending  CNF mask */
  uint32                                     pending_cnf_mask;

  /* MBSFN config status: Default unknown  */
  lte_ml1_dlm_mbsfn_cfg_status_e             cfg_status;

  /* Cell Switch active: Default false */
  boolean                                    cswitch_active;

  lte_ml1_mod_mask_t                         modules_suspend_mask;

  lte_ml1_dlm_embms_suspend_cnf_cb_t         suspend_cnf_cb_ext;

  /* Reselection active: Default false  */
  boolean                                    resel_active;

};



struct lte_ml1_dlm_embms_db_s
{

  /*! instance number */ 
  lte_mem_instance_type                         inst_num;

  /*! DLM eMBMS database */
  lte_ml1_dlm_embms_data_s                      *embms_data_ptr;

  /*! eMBMS state machine related data */
  lte_ml1_dlm_embms_state_data_s                *embms_stm_data_ptr;

  /*! PMCH monitor related data */
  lte_ml1_dlm_pmch_monitor_data_s               *pmch_monitor_data_ptr;

  /*! eMBMS data for 1024 Mod Period*/
  lte_ml1_dlm_embms_data_for_1024_mod_period_s  *embms_data_for_1024_mod_period_ptr;

  /*! MBSFN signal strength monitor data */
  lte_ml1_dlm_mbsfn_signal_strength_params_s    *mbsfn_signal_strength_params;
};


/*===========================================================================

  FUNCTION:  lte_ml1_mgr_get_embms_data

===========================================================================*/
/*!
    @brief
    Get eMBMS data pointer
    
    @return
    eMBMS data pointer
*/
/*=========================================================================*/
lte_ml1_dlm_embms_db_s *lte_ml1_dlm_get_embms_data
(
  lte_mem_instance_type inst_num
);

/*===========================================================================

  FUNCTION:  lte_ml1_embms_data_mem_deinit

===========================================================================*/
/*!
    @brief
    Deinitialize eMBMS structure data
    
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_embms_data_mem_deinit(lte_mem_instance_type instance);
#endif /* LTE_ML1_DLM_EMBMS_STM_H */

#endif
