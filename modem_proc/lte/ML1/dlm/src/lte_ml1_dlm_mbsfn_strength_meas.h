/*!
  @file
  lte_ml1_dlm_mbsfn_strength_meas.h

  @brief
  MBSFN Signal Strength Measurement Header
*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/src/lte_ml1_dlm_mbsfn_strength_meas.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/27/12   ss    Initial version


=================================================================*/
#include "lte_ml1_comdef.h"

#ifdef FEATURE_LTE_ML1_EMBMS



#ifndef LTE_ML1_DLM_MBSFN_STRENGTH_H
#define LTE_ML1_DLM_MBSFN_STRENGTH_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


#include "lte_ml1_dlm_embms_data.h"
#include "lte_ml1_rfmgr.h"
#include "lte_ml1_common.h"
#include "lte_ml1_log.h"
#include "lte_ml1_mdb.h"
#include "lte_cphy_rssi_msg.h"
#include "intf_dl.h"
#include "intf_dl_msg.h"
#include "lte_cphy_msg.h"
#include "lte_ml1_dlm_types.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

//typedef struct lte_ml1_dlm_embms_db_s lte_ml1_dlm_embms_db_s;       

/*!
  Structure defining MBSFN Area cfg for Signal Strength Params
*/
typedef struct{
  /* Valid flag for each area*/
  boolean                                                      valid;

  /* MBSFN area id: 0 - 255 */
  lte_mbsfn_area_id_t                                        area_id;

  /* Average SNR in linear scale per MBSFN area*/
  int32                                                 average_snr;
     
  /*Time in RTC ticks when the last measurement took place*/         
  uint64                                       prev_measurement_time;

}lte_ml1_dlm_signal_strength_mbsfn_area_cfg_s;

/*!
  Structure defining MBSFN SIB2 configuration
*/
typedef struct {

 /*! Number of valid areas */
  uint8                                                                   num_mbms_areas;
  /* Signal Strength MBSFN area cfg  */
  lte_ml1_dlm_signal_strength_mbsfn_area_cfg_s   mbsfn_area_cfg[LTE_EMBMS_MAX_MBSFN_AREA];

} lte_ml1_dlm_mbsfn_signal_strength_params_s;

/*===========================================================================

  FUNCTION:  lte_ml1_dlm_update_mbsfn_measurements

===========================================================================*/
/*!
    @brief
    Update MBSFN signal strength measurements on receiving
    LTE_LL1_DL_EMBMS_WHITENED_MATRICES_IND from FW.
    This indication is received on a per subframe basis

    @return
    None
*/
/*=========================================================================*/

void lte_ml1_dlm_update_mbsfn_measurements
( 
  lte_mem_instance_type instance,
  lte_LL1_dl_embms_whitened_matrices_ind_msg_struct *mbsfn_signal_strength
);

/*===========================================================================

  FUNCTION:  lte_ml1_dlm_calc_mbsfn_area_signal_strength

===========================================================================*/
/*!
    @brief
    This function is  called on receiving LTE_CPHY_MBSFN_SIGNAL_STRENGTH_REQ from QMI.

    @details
    LTE_CPHY_MBSFN_SIGNAL_STRENGTH_REQ is to request the MBSFN signal strength
    measurements from ML1
    
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_dlm_calc_mbsfn_area_signal_strength
(  
  lte_mem_instance_type instance,
  /*! MBSFN strength meas request */
  lte_cphy_mbsfn_signal_strength_req_s  *meas_req_ptr
);
/*===========================================================================

  FUNCTION:  lte_ml1_dlm_clear_signal_strength_params

===========================================================================*/
/*!
    @brief
    Clear signl strength params

    @return
    None
*/
/*=========================================================================*/

void lte_ml1_dlm_clear_signal_strength_params
(
  lte_ml1_dlm_embms_db_s *embms_data_inst
);

/*===========================================================================

  FUNCTION:  lte_ml1_dlm_invalidate_signal_strength_area

===========================================================================*/
/*!
    @brief
    Invalidate signal strength results for a particular area

    @return
    None
*/
/*=========================================================================*/
extern void lte_ml1_dlm_invalidate_signal_strength_area
(
  lte_ml1_dlm_embms_db_s *embms_data_inst,
  /*! MBSFN area id */
  lte_mbsfn_area_id_t     area_id
);

#endif

#endif
