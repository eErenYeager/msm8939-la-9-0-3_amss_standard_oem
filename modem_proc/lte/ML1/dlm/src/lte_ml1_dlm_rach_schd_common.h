#ifndef LTE_ML1_DLM_RACH_SCHD_COMMON_H
#define LTE_ML1_DLM_RACH_SCHD_COMMON_H

/*!
  @file
  lte_ml1_dlm_rach_schd_common.h

  @brief
  Header file for use by rach and schd

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/14/13   anm     BPLMN SIB9 feature support
07/02/13   rc      Initial Version
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
typedef struct lte_ml1_dlm_schdlr_user_data_s lte_ml1_dlm_schdlr_user_data_s;
typedef struct lte_ml1_dlm_cdrx_data_s lte_ml1_dlm_cdrx_data_s;
typedef struct lte_ml1_dlm_obj_usr_data_s lte_ml1_dlm_obj_usr_data_s;
typedef struct lte_ml1_dlm_schdlr_obj_user_data_s lte_ml1_dlm_schdlr_obj_user_data_s;
typedef struct lte_ml1_dlm_rach_stm_data_s lte_ml1_dlm_rach_stm_data_s;
typedef struct lte_ml1_dlm_rach_data_s lte_ml1_dlm_rach_data_s;
typedef struct lte_ml1_dlm_win_obj_schedule_info_s lte_ml1_dlm_win_obj_schedule_info_s;
typedef struct lte_ml1_dlm_schldr_s lte_ml1_dlm_schldr_s;

#include <comdef.h>
#include <lte_ml1_dlm_main.h>
#include <lte_ml1_schdlr_types.h>
#include "lte_ml1_dlm_msg.h"
#include "lte_ml1_comdef.h"
#include "customer.h"
#include "intf_sys.h"
#include "lte_ml1_schdlr_types.h"
#include "lte_ml1_dlm_types.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
#define LTE_ML1_DLM_RNTI_CHANGE_NUM_BUFS 1

/*! @brief DLM obj action type
*/
typedef enum
{
  LTE_ML1_DLM_OBJ_ACTION_SCHDL = 0,
  LTE_ML1_DLM_OBJ_ACTION_DESCHDL,
  LTE_ML1_DLM_OBJ_ACTION_SCHDL_TIMED  
} lte_ml1_dlm_obj_action_e;

/*! @brief CDRX event  Enum
*/
typedef enum{
  CDRX_ON_TO_OFF,      
  CDRX_OFF_TO_ON,
  CDRX_AUTOGAP
}lte_ml1_cdrx_event_e;

/*! @brief Structure for schdlr user data
*/
struct lte_ml1_dlm_schdlr_user_data_s
{
  lte_LL1_dl_pdcch_params_struct rntis;
  uint16                         ra_win_start_time;
  uint16                         ra_win_size;
  lte_ml1_dlm_obj_dl_rx_proc_e   obj_dl_rx_proc;
  //! @todo more needed user data can be added here 
  
};

/*! @brief Structure to keep window based object schedule
*/
struct lte_ml1_dlm_win_obj_schedule_info_s
{
  /*! Enable/disable flag */
  boolean active;
  /*! start time in SUBFN */
  lte_ml1_schdlr_sf_time_t start_time_subfn_cnt;
  /*! window size */
  lte_ml1_schdlr_sf_time_t win_size;
};


/* generic user data structure */
struct lte_ml1_dlm_obj_usr_data_s
{
  // instance number
  // please pass this field in the get data function to get the corresponding db pointer!
  lte_mem_instance_type inst_num;
  // payload pointer
  // please use this field to pass pointer other than the top level db!
  void                  *payload_ptr;
};


/* list all the obj user data */
struct lte_ml1_dlm_schdlr_obj_user_data_s
{
  // LTE_ML1_SCHDLR_OBJ_DL_RACHRAWIN
  lte_ml1_dlm_obj_usr_data_s obj_dl_rachrawin_data;
  // LTE_ML1_SCHDLR_OBJ_DL_RACHRAEXPIRYWIN
  lte_ml1_dlm_obj_usr_data_s obj_dl_rachraexpirywin_data;
  // LTE_ML1_SCHDLR_OBJ_DL_RX
  lte_ml1_dlm_obj_usr_data_s obj_dl_rx_data;
  // LTE_ML1_SCHDLR_GROUP_DL
  lte_ml1_dlm_obj_usr_data_s obj_dl_group_data;
  // LTE_ML1_SCHDLR_OBJ_DL_C_RNTI_ENV
  lte_ml1_dlm_obj_usr_data_s obj_dl_c_rnti_env_data;
  // LTE_ML1_SCHDLR_OBJ_CDRX_ON_OFF
  lte_ml1_dlm_obj_usr_data_s obj_cdrx_on_off_data;
  // LTE_ML1_SCHDLR_OBJ_DL_PCH
  lte_ml1_dlm_obj_usr_data_s obj_dl_pch_data;
  // LTE_ML1_SCHDLR_OBJ_DL_CH_CFG
  lte_ml1_dlm_obj_usr_data_s obj_dl_ch_cfg_data;
  // LTE_ML1_SCHDLR_OBJ_DL_PCHPROC
  lte_ml1_dlm_obj_usr_data_s obj_dl_pchproc_data;
  // LTE_ML1_SCHDLR_OBJ_DL_BLOCK_S_OR_NS_TO_NS
  lte_ml1_dlm_obj_usr_data_s obj_dl_block_s_or_ns_to_ns_data;
  // LTE_ML1_SCHDLR_OBJ_DL_CELL_SWITCH_S_OR_NS_TO_NS
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_switch_s_or_ns_to_ns_data;
  // LTE_ML1_SCHDLR_OBJ_DL_BLOCK_NS_TO_S
  lte_ml1_dlm_obj_usr_data_s obj_dl_block_ns_to_s_data;
  // LTE_ML1_SCHDLR_OBJ_DL_CELL_SWITCH_NS_TO_S
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_switch_ns_to_s_data;
  // LTE_ML1_SCHDLR_OBJ_DL_RX_CFG
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_dl_rx_cfg_data;
  // LTE_ML1_SCHDLR_OBJ_DLM_CA_PROC
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_dlm_ca_proc_data;
  // LTE_ML1_SCHDLR_OBJ_DLM_CA_DTA_UNFRIENDLY
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_dlm_ca_dta_unf_data;
  // LTE_ML1_SCHDLR_OBJ_DLM_CA_PEND_PROC_TICK
  lte_ml1_dlm_obj_usr_data_s obj_dl_cell_dlm_ca_pend_proc_tick_data;
#ifdef FEATURE_LTE_ML1_EMBMS
  // LTE_ML1_SCHDLR_OBJ_PMCH_SCHDL_1
  lte_ml1_dlm_obj_usr_data_s obj_dl_pmch_schdl_1_data;
  // LTE_ML1_SCHDLR_OBJ_PMCH_SCHDL_2
  lte_ml1_dlm_obj_usr_data_s obj_dl_pmch_schdl_2_data;
  // LTE_ML1_SCHDLR_OBJ_MRNTI_EVNT
  lte_ml1_dlm_obj_usr_data_s obj_dl_mrnti_evnt_data;
  // LTE_ML1_SCHDLR_OBJ_MRNTI_EVNT_ENV
  lte_ml1_dlm_obj_usr_data_s obj_dl_mrnti_evnt_env_data;
  // LTE_ML1_SCHDLR_OBJ_MRNTI_EVNT_ENV
  lte_ml1_dlm_obj_usr_data_s obj_dl_pmch_stop_env_evnt_data;

#endif
  // LTE_ML1_SCHDLR_OBJ_DL_SIMOD
  lte_ml1_dlm_obj_usr_data_s obj_dl_simod_data;
  // LTE_ML1_SCHDLR_GROUP_DL_SIB
  lte_ml1_dlm_obj_usr_data_s obj_dl_group_sib_data;
  // LTE_ML1_SCHDLR_OBJ_DL_SIB1
  lte_ml1_dlm_obj_usr_data_s obj_dl_sib1_data;
  // LTE_ML1_SCHDLR_OBJ_DL_SIB1_PUNCTURE
  lte_ml1_dlm_obj_usr_data_s obj_dl_puncture_data;
  // LTE_ML1_SCHDLR_OBJ_DL_EXT_SIB_ENV
  lte_ml1_dlm_obj_usr_data_s obj_dl_ext_sib_env_data;
  // LTE_ML1_SCHDLR_OBJ_DL_CUR_ACTIVE_SIB_ENV_TEARDOWN
  lte_ml1_dlm_obj_usr_data_s obj_dl_cur_active_sib_env_teardown_data;
  // LTE_ML1_SCHDLR_OBJ_DL_INT_SIB1
  lte_ml1_dlm_obj_usr_data_s obj_dl_int_sib1_data;
  // LTE_ML1_SCHDLR_OBJ_DL_INT_SIB_ENV
  lte_ml1_dlm_obj_usr_data_s obj_dl_int_sib_data;
  // LTE_ML1_SCHDLR_OBJ_DL_SIMSG
  lte_ml1_dlm_obj_usr_data_s obj_dl_int_simsg_data[LTE_CPHY_MAX_SI_MSGS];
  // LTE_ML1_SCHDLR_OBJ_DL_MIB_DECODE_TRIGGER
  lte_ml1_dlm_obj_usr_data_s obj_dl_mib_decode_data;
  // LTE_ML1_SCHDLR_OBJ_DL_SIMOD_EARLY_TRIGGER
  lte_ml1_dlm_obj_usr_data_s obj_dl_simod_early_trigger_data;
  // Internal SI decodes: LTE_ML1_SCHDLR_OBJ_DL_SIMSG
  lte_ml1_dlm_obj_usr_data_s obj_dl_int_nbr_simsg_data[LTE_CPHY_MAX_INT_SI_MSGS];
  //LTE_ML1_SCHDLR_OBJ_DLM_ACQ_STAGE2
  lte_ml1_dlm_obj_usr_data_s  obj_dl_acq_2_data;
};


struct lte_ml1_dlm_schldr_s
{
  /*! Double buffer the rnti change message, for when we need to update the
   rntis */
  lte_LL1_dl_pdcch_rnti_change_req_msg_struct  rnti_change_msg[LTE_ML1_DLM_RNTI_CHANGE_NUM_BUFS];
  
  uint32                                       cur_msg_idx;

  /*! This is the ONLY field which should be accessed from the scheduler
    thread */
  lte_LL1_dl_pdcch_rnti_change_req_msg_struct* cur_msg;

  /*! Channel cfg accessed by both ML1 and schdlr thread without any issues as
      both can not access this channel config msg at the same time
  */
  lte_LL1_dl_ch_config_req_msg_struct          ll_ch_cfg_msg;

  /* DL ch cfg start cb called in current subfn */
  boolean                                      dl_ch_cfg_called_in_cur_subfn;

  /* pdcch rnti change related cbs in current subfn */
  uint32                                       rnti_activity_mask_in_cur_subfn;

  /* CDRX event action timed valid if CDRX_EVENT_ACTION_TIME bit
     set in current subframe */
  lte_ml1_schdlr_sf_time_t                     cdrx_action_subfn_cnt;
  
  /*! Schdlr copy of PCH schdl */
  lte_ml1_dlm_win_obj_schedule_info_s          schdlr_pch_schdl;

  /* Schdlr obj user data db */
  lte_ml1_dlm_schdlr_obj_user_data_s           schdlr_obj_usr_data;

  /*! The minimum on time. */
  lte_ml1_schdlr_sf_time_t     min_on_time_subfn_cnt;

 /*! Inactivity Timer value */
   int32                               inactive_timer;
 
 /*!CDRX event enum*/
  lte_ml1_cdrx_event_e                   cdrx_event;

  lte_ml1_schdlr_obj_id_e                last_crnti_schdlr_obj;
  lte_ml1_schdlr_obj_id_e                curr_crnti_schdlr_obj;
};

#define LTE_ML1_DLM_CDRX_ACTION_BUF_MAX 2

/*! @brief DLM CDRX data
*/
struct lte_ml1_dlm_cdrx_data_s
{
  /*! PDCCH monitoring OFF/ON flag */
  boolean                         pdcch_off;
  /*! Double buffer for CDRX action reqs from GM for cases 
    if CDRX ON->OFF and OFF->ON b2b and vice-versa
  */
  lte_ml1_dlm_cdrx_action_e       action_buf[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];
  /*! Next CDRX action idx */
  uint32                          next_action_buf_idx;
  /*! Valid for action/event ON->OFF request only: 
      Expected CDRX wakeup subframe cnt after UE goes from ON->OFF */
  lte_ml1_schdlr_sf_time_t        cdrx_on_subfn_cnt;

  /*! DLM schdlr user data ptr */
  lte_ml1_dlm_schdlr_user_data_s  *rnti_user_data_ptr;

  /*! apply action time */
  boolean                         apply_action_time[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];
  /*! Action subframe cnt */
  lte_ml1_schdlr_sf_time_t        action_subfn_cnt[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];

  /*! Is start of on duration */
  boolean                         is_on_dur_start[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];

  /*! Is wake up to read ACK from UL ReTx */
  boolean                          is_on_dur_ul_retx[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];

  /*! The minimum on time. */
  lte_ml1_schdlr_sf_time_t         min_on_time_subfn_cnt[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];

  /*! Inactivity Timer*/
  int32                            inactive_timer[LTE_ML1_DLM_CDRX_ACTION_BUF_MAX];

};


/*! @brief DLM RACH STM data
*/
struct lte_ml1_dlm_rach_stm_data_s
{
  /*! @brief DLM schdlr user data. RNTI info for DLM
  */
  lte_ml1_dlm_schdlr_user_data_s schdlr_user_data;
  lte_rnti_val_t                 t_rnti_val;
  boolean                        t_rnti_valid;
  lte_rnti_val_t                 c_rnti_val;
  boolean                        c_rnti_valid;
  boolean                        obj_dl_rx_is_registered;
  lte_ml1_dlm_cdrx_data_s        cdrx_data;
  /*! @brief  Flag to indicate if LL SFN is known or not. Can be used internally 
      in DLM files 
  */
  boolean      lte_ml1_dlm_is_ll_sfn_known;
};

struct lte_ml1_dlm_rach_data_s
{
  //instance number 
  lte_mem_instance_type         inst_num;
  
  /*! DLM RACH STM data */
  lte_ml1_dlm_rach_stm_data_s   *rach_stm_data;
};

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/


#endif /* LTE_ML1_DLM_RACH_SCHD_COMMON_H */
