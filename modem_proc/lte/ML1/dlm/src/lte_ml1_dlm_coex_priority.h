#ifndef LTE_ML1_DLM_COEX_PRIORITY_H
#define LTE_ML1_DLM_COEX_PRIORITY_H

/*!
  @file
  lte_ml1_dlm_coex_priority.h

  @brief
  Header file for use for coex priority handling

*/

/*===========================================================================

  Copyright (c) 2007 - 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/src/lte_ml1_dlm_coex_priority.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/03/14   sm      Initial Version
===========================================================================*/

#include "lte_variation.h"

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_dlm_send_paging_priority_inc_req

===========================================================================*/

/*!
    @brief
    Send a request to increase priority 

    @return
    None

*/
/*=========================================================================*/

void lte_ml1_dlm_send_paging_priority_inc_req(  
    /*! Pointer to right instance */
  lte_ml1_dlm_data_s *dlm_data_inst );
/*===========================================================================

  FUNCTION:  lte_ml1_dlm_send_paging_priority_cancel_req

===========================================================================*/
/*!
    @brief
    Send a request to increase priority 

    @return
    None

*/
/*=========================================================================*/
   void lte_ml1_dlm_send_paging_priority_cancel_req(  
    /*! Pointer to right instance */
  lte_ml1_dlm_data_s *dlm_data_inst );
#endif /* LTE_ML1_DLM_COEX_PRIORITY_H */
