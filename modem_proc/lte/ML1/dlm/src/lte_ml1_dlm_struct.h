/*!
  @file
  lte_ml1_dlm_struct.h

  @brief
  Contains all DL module globals structure

*/

/*===========================================================================

  Copyright (c) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/src/lte_ml1_dlm_struct.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/
#ifndef LTE_ML1_DLM_STRUCT_H
#define LTE_ML1_DLM_STRUCT_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_ml1_comdef.h"

typedef struct lte_ml1_dlm_data_s lte_ml1_dlm_data_s;
typedef struct lte_ml1_dlm_stm_rx_cfg_data_s lte_ml1_dlm_stm_rx_cfg_data_s;

/* todo------ include your struct definition header file and
    add it into lte_ml1_dlm_data_s! */
#include "lte_ml1_mem.h"
#include "lte_ml1_dlm_stm_common.h"
#include "lte_ml1_dlm_l1m_if.h"
#include "lte_ml1_dlm_main.h"
#include "lte_ml1_dlm_log.h"
#include "lte_ml1_dlm_cfg.h"
#include "lte_ml1_dlm_schd.h"
#include "lte_ml1_dlm_rach_schd_common.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
struct lte_ml1_dlm_data_s
{
  //instance number 
  lte_mem_instance_type          inst_num; 

  //DLM stm procedure data
  lte_ml1_dlm_stm_data_s         *stm_ptr;

  //DLM acq state data
  lte_ml1_dlm_stm_acq_data_s     *stm_acq_ptr;

  //DLM acq stage2 data
  lte_ml1_dlm_acq_stage2_req_s   *acq_result_ptr;

  //DLM debug info
#ifdef LTE_ML1_DEBUG
  lte_ml1_dlm_debug_info_s       *debug_dlm_ptr;
#endif

  //DLM log data
  lte_ml1_log_dlm_data_s         *log_dlm_ptr;
  
  //DLM cfg data
  lte_ml1_dlm_cfg_data_s         *cfg_dlm_ptr;
  
  //DLM rx cfg data
  lte_ml1_dlm_stm_rx_cfg_data_s  *rx_cfg_data;

  // DLM scheduler data
  lte_ml1_dlm_schldr_s           *schd_cfg_data;

  lte_ml1_dlm_acq_stage2_req_s    *acq_stage2_params;
};


/*===========================================================================

  FUNCTION:  lte_ml1_dlm_get_dlm_data

===========================================================================*/
/*!
    @brief
    Get DLM data pointer
    
    @return
    DLM data pointer
*/
/*=========================================================================*/
lte_ml1_dlm_data_s *lte_ml1_dlm_get_dlm_data
(
  lte_mem_instance_type inst_num
);

#endif /* LTE_ML1_DLM_STRUCT_H */
