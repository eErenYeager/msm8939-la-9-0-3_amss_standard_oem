
/*!
  @file
  lte_ml1_dlm_ca_stm.h

  @brief
  CA state machine handlers
*/

/*===========================================================================

  Copyright (c) 2013 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/dlm/src/lte_ml1_dlm_ca_stm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
   
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_ml1_dlm_ca_types.h"
#include "lte_ml1_dlm_ca.h"
#include "lte_ml1_dlm_pq_utils.h"
#include "lte_ml1_mem.h"

#ifndef LTE_ML1_DLM_CA_STM_H
#define LTE_ML1_DLM_CA_STM_H
/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/
typedef struct {  
  /*! Cnf UMID after end of procedure */
  lte_ml1_dlm_ca_cnf_umid_s           cnf_umid;

  /*! Current add procedure (if any) db index  */
  uint8                               scc_add_db_idx;

  /*! CA activation indication */
  lte_ml1_dlm_ca_scc_act_deact_ind_s  scc_act_ind;

  /*! Current activate procedure (if any) db index  */
  uint8                               scc_act_db_idx;

  /*! CA deativation indication */
  lte_ml1_dlm_ca_scc_act_deact_ind_s  scc_deact_ind;

  /*! Current deactivate procedure (if any) db index  */
  uint8                               scc_deact_db_idx;

  /*! CA delete indication */
  lte_ml1_dlm_ca_scc_del_ind_s        scc_del_ind;

  /*! Current deconfig procedure (if any) db index  */
  uint8                               scc_del_db_idx;
  
  /*! Slam time confirmation LL msg seq id */
  uint8                               slam_cnf_seq_id;

} lte_ml1_dlm_ca_stm_data_s;

/*! Debug timers */
typedef struct {
  lte_ml1_common_timer_s debug_ca_timer;
  uint32                        debug_ca_timer_val;  //default = 0 
  uint32                        debug_ca_scell_idx;
} lte_ml1_dlm_ca_debug_s;

typedef struct{
  lte_mem_instance_type             inst_num;
  lte_ml1_dlm_ca_stm_data_s         *lte_ml1_dlm_ca_stm_data;
  lte_ml1_dlm_ca_data_s             *lte_ml1_dlm_ca_data;
  lte_ml1_dlm_ca_pending_queue_s    *lte_ml1_dlm_ca_pending_queue;
  lte_ml1_dlm_ca_debug_s            *lte_ml1_dlm_ca_debug;
}lte_ml1_dlm_ca_inst_s;

/*===========================================================================

  FUNCTION:  lte_ml1_dlm_ca_get_data

===========================================================================*/
/*!
  @brief
  Returns Pointer to CA DB 
 
  @return
  Pointer to CA DB
*/
/*=========================================================================*/
lte_ml1_dlm_ca_data_s* lte_ml1_dlm_ca_get_data( lte_mem_instance_type instance );


/*===========================================================================

  FUNCTION:  lte_ml1_dlm_ca_get_pending_queue

===========================================================================*/
/*!
  @brief
  Returns Pointer to CA pending queue 
 
  @return
  Pointer to CA pending queue
*/
/*=========================================================================*/
lte_ml1_dlm_ca_pending_queue_s* lte_ml1_dlm_ca_get_pending_queue( lte_mem_instance_type instance );

/*===========================================================================

  FUNCTION:  lte_ml1_dlm_ca_get_instance

===========================================================================*/
/*!
  @brief
  Returns Instance number 
 
  @return
  Returns Instance numer
*/
/*=========================================================================*/
lte_mem_instance_type lte_ml1_dlm_ca_get_instance(  stm_state_machine_t *sm );


/*===========================================================================

  FUNCTION:  lte_ml1_dlm_ca_get_inst
  
===========================================================================*/
/*!
  @brief
  get DLM CA data instance
 
  @return
  None
*/
/*=========================================================================*/
lte_ml1_dlm_ca_inst_s * lte_ml1_dlm_ca_get_inst
(
  lte_mem_instance_type inst_num
);


/*===========================================================================

  FUNCTION:  lte_ml1_dlm_ca_data_dealloc

===========================================================================*/
/*!
  @brief
  Release memory for CA database 
 
  @return
  None
*/
/*=========================================================================*/
void lte_ml1_dlm_ca_data_dealloc(lte_mem_instance_type instance);
#endif /* LTE_ML1_DLM_CA_STM_H */
