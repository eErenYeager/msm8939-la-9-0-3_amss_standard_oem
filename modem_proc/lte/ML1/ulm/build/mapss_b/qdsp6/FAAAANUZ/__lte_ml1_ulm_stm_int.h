/*=============================================================================

    __lte_ml1_ulm_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_ulm_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_ULM_STM_INT_H
#define __LTE_ML1_ULM_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_ulm_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_ULM_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_ULM_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_ULM_SM_NUM_STATES 6

/* Define a macro for the number of SM inputs */
#define LTE_ML1_ULM_SM_NUM_INPUTS 37

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_ulm_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_ulm_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_ulm_init_init_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_init_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_init_prach_msg1_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_prach_msg1_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_init_in_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_in_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_init_out_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_out_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_init_aborting_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_aborting_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_init_suspended_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_ulm_cleanup_suspended_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_ulm_proc_ll_phychan_decfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_prach_setup(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_pucch_setup(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_srs_common_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_srs_dedicated_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_csf_dedicated_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_ll_csf_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_pusch_setup(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_goto_out_sync_state(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_ll_phychan_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_ta_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_skip_rach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_x2l_handover_prep_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_srs_aper_dedicated_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_scell_ca_cfg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_prach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_prach_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_goto_init_state(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_goto_in_sync_state(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_rar_timer_expired_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_gm_rl_fail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_conn_rel_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_cancel_conn_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_ll_csf_decfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_update_ta_for_bcr(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_pdcch_order_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_apply_ta_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_goto_prach_msg1_state(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_cont_res_result(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_out_of_sync_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_handover_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_gm_rach_abort_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_proc_rf_disable_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_stm_suspend_proc_out_of_sync_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_ulm_proc_cont_res_result_in_suspend(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_ULM_INIT_STATE,
  LTE_ML1_ULM_PRACH_MSG1_STATE,
  LTE_ML1_ULM_IN_SYNC_STATE,
  LTE_ML1_ULM_OUT_SYNC_STATE,
  LTE_ML1_ULM_ABORTING_STATE,
  LTE_ML1_ULM_SUSPENDED_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_ULM_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_ULM_STM_INT_H */
