#ifndef LTE_ML1_ULM_COMDEF_H
#define LTE_ML1_ULM_COMDEF_H

/*! 
  @file
  lte_ml1_ulm_comdef.h

  @brief    
  This is the header file that defines the L1M-UL Manager interface for 
  UL channel setup/teardown.
*/

/*===========================================================================

  Copyright (c) 2007 - 12 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/ulm/inc/lte_ml1_ulm_comdef.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/19/13   qih      Mem reorg phase 1 changes 
===========================================================================*/


#include "lte_ml1_prach.h"
#include "lte_ml1_ulm_rfmgr_if.h"
#include "lte_ml1_pos_msg.h"
#include "lte_ml1_ulm_log.h"
#include "lte_ml1_ulm_cfg.h"

typedef struct{

  /*! Instance number */
  uint8 inst_num;
	  
  /* The state to transition to if RACH Procedure fails */
  stm_state_t lte_ml1_prach_fail_state;
  
  /* The pending Start RACH request until RF is tuned */
  lte_cphy_start_rach_req_s lte_ml1_ulm_pending_start_rach_req;

  /* The state in which Suspend Request was received */
  lte_ml1_ulm_suspend_info_s lte_ml1_ulm_suspend_info;

  /* Flag to indicate we're in the middle of handover suspend */
  boolean lte_ml1_ulm_in_handover_suspend;

  /* Flag to indicate LL SFN is known or not */
  boolean lte_ml1_ulm_ll_sfn_known;

  /* Flag to indicate rach request pending from SFN unknown period */
  boolean lte_ml1_ulm_rach_trigger_pending;

  /* MTPL from RFSW */
  int16 lte_ml1_ulm_mtpl_val;
  
  /*! The current RF tune reason */
  lte_ml1_ulm_rf_tune_reason_e lte_ml1_ulm_curr_rf_tune_reason;

  /* Phychan Config DB */
  lte_ml1_ulm_phychan_cfg_db_struct_type       
  lte_ml1_ulm_phychan_cfg_db[LTE_ML1_UL_NUM_PHYCHAN];
  
  /* PRACH config DB*/
  lte_cphy_prach_setup_param_s lte_ml1_ulm_prach_cfg;
  
  /* PUCCH common config DB*/
  lte_cphy_pucch_cfg_common_param_s lte_ml1_ulm_pucch_common_cfg;
  
  /* PUSCH common config DB*/
  lte_cphy_pusch_cfg_common_param_s lte_ml1_ulm_pusch_cfg;
  
  /* SRS Common config DB */
  lte_cphy_srs_cfg_common_param_s lte_ml1_ulm_srs_common_cfg;
  
  /* SRS dedicated config DB */
  lte_cphy_srs_cfg_dedicated_param_s lte_ml1_ulm_srs_dedicated_cfg;
  
  /* CQI dedicated config DB */
  lte_cphy_cqi_reporting_param_s lte_ml1_ulm_csf_dedicated_cfg;
  
  /* Uplink Frequency Config */
  lte_cphy_ul_freq_cfg_s lte_ml1_ulm_freq_cfg;
  
  /* TDD Config */
  lte_cphy_tdd_cfg_param_s lte_ml1_ulm_tdd_cfg;
  
  /* Target cell info received when suspended for HO */
  lte_ml1_ulm_ho_target_cell_cfg_s lte_ml1_ulm_stored_handover_target_cell_cfg;
  
  /* Flag to indicate that we are in Connected mode */
  boolean lte_ml1_ulm_is_in_conn_mode;
  
  /* The received antenna config */
  lte_ml1_ulm_antenna_dedicated_cfg_s lte_ml1_ulm_antenna_dedicated_cfg;

  /* The received CSI-RS config */
  lte_ml1_ulm_csi_rs_dedicated_cfg_s lte_ml1_ulm_csi_rs_dedicated_cfg;

  /* The time when the Last TA command was received */
  lte_l1_cell_systime_s lte_ml1_ulm_ta_rcvd_timing;
  
  /* Timing Advance command to LL */
  lte_LL1_sys_ta_adj_req_msg_struct lte_ml1_ulm_ta_adj_req;
  
  /* GPS MRL TA update request */
  lte_ml1_pos_mrl_update_ta_req_s lte_ml1_ulm_ta_update_req;
  
  /* Pending confirmations to be received from LL */
  lte_ml1_ulm_cfg_pending_cnf_s lte_ml1_ulm_cfg_pending_cnf;
  
  /* PMax */
  int8	lte_ml1_ulm_pmax_cfg;
  
#ifdef FEATURE_LTE_DIME_BRINGUP
  lte_LL1_ul_phychan_config_req_msg_struct lte_dbg_ul_phychan_config_req;
#endif

  /* CarrAgg: CA config info */
  lte_ml1_ulm_ca_info_s                      lte_ml1_ulm_ca_info;

  /* CarrAgg: Saved SCC update params */
  lte_ml1_ulm_saved_scc_update_params_s      lte_ml1_ulm_saved_scc_update;
  
  
  lte_ml1_prach_preamble_seq_tbl_struct_type 
	preamble_seq_set_hs[LTE_ML1_PRACH_MAX_PREAMBLE_SEQ_CNT];
  
  lte_ml1_prach_preamble_seq_tbl_struct_type 
	preamble_seq_set_non_hs[LTE_ML1_PRACH_MAX_PREAMBLE_SEQ_CNT];
  
  lte_ml1_ulm_log_prach_rsp_timing_s   prach_rsp_timing_log;
  
#ifdef FEATURE_LTE_ML1_TDD_LOGGING
  lte_ml1_ulm_log_prach_resource_allocation_s lte_ml1_prach_ra_log;
#endif /* FEATURE_LTE_ML1_TDD_LOGGING */
  
  /* Structure where all the parameters related to BCR are stored */
  lte_ml1_prach_bcr_params_struct_type lte_ml1_prach_bcr_params;

  /* The PRACH Beta Gain. Hardcoded to 242 and can be set by the skip_ulpc_req */
  int16   lte_ml1_prach_beta_gain;
  
  /* The RACH envelope scheduler object */
  lte_ml1_prach_sched_info_s lte_ml1_ulm_rach_sched_info;
  
  /* MSG to send the PRACH info to MMAL */
  lte_cphy_raa_ind_msg_s  lte_ml1_prach_mmal_msg;

#ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY
  lte_ml1_ulm_ant_switch_db_s lte_ml1_ulm_ant_switch_db;
 #endif

 lte_ml1_prach_dsda_prio_handle_s prach_dsda_handle_info;

}lte_ml1_ulm_s;
#endif

