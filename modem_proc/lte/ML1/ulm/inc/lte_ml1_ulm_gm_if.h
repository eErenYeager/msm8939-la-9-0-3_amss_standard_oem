#ifndef LTE_ML1_ULM_GM_IF_H
#define LTE_ML1_ULM_GM_IF_H

/*! 
  @file
  lte_ml1_ulm_gm_if.h

  @brief    
  This is the header file that defines the Grant Manager-UL Manager interface 
  functions.
*/

/*===========================================================================

  Copyright (c) 2007 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/ulm/inc/lte_ml1_ulm_gm_if.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/20/13   dk      Initial Check in

===========================================================================*/

/*===========================================================================

                              INCLUDE FILES

===========================================================================*/
#include <lte_l1_types.h>
#include <lte_ml1_comdef.h>
#include <lte_ml1_mem.h>


/*! PRACH timing and format info */
typedef struct 
{
  /*! Last PRACH timing */
  uint16    prach_timing;

  /*! Previous PRACH timing */
  uint16    prev_prach_timing;

  /*! Preamble duration 1..3 ms*/
  uint8     duration;
} lte_ml1_ulm_prach_time_format_info_s;


/*===========================================================================
 
                               FUNCTIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_ulm_get_prach_time_and_duration

===========================================================================*/
/*!
  @brief
  Function called by the Grant manager to get the PRACH timing and
  preamble format used
  
  @return
  none
*/
/*=========================================================================*/
void lte_ml1_ulm_get_prach_time_and_duration
( 
  lte_mem_instance_type instance,
  lte_ml1_ulm_prach_time_format_info_s *prach_info
);

#endif
