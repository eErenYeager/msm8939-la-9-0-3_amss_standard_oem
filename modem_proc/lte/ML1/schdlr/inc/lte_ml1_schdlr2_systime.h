#ifndef LTE_ML1_SCHDLR2_SYSTIME_H
#define LTE_ML1_SCHDLR2_SYSTIME_H
/*!
  @file
  lte_ml1_schdlr2_systime.h

  @brief
  This module contains the code for ML1 scheduler system time.

*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/schdlr/inc/lte_ml1_schdlr2_systime.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/20/12   tjc     Initial version
===========================================================================*/

/*===========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "lte_ml1_comdef.h"
#include <semaphore.h>
#include "lte_ml1_schdlr_types.h"
#include "lte_ml1_mem.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/


/*! System time */
typedef struct
{
  /*! Semaphore to synchronize the systime management */
  sem_t sem;

  /*! Current system time */
  lte_ml1_schdlr_systime_info_s systime;

} lte_ml1_schdlr_systime_data_s;

/*===========================================================================

                                 FUNCTIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_mem_alloc

===========================================================================*/
/*!
    @brief
    This function allocates the memory for the module.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_mem_alloc
(
  lte_mem_instance_type      instance,         /*!< Scheduler Instance     */  
  lte_ml1_schdlr_systime_data_s **systime_db  
);


/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_mem_free

===========================================================================*/
/*!
    @brief
    This function frees the memory for the module.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_mem_free
(
  lte_mem_instance_type      instance          /*!< Scheduler Instance     */  
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_init

===========================================================================*/
/*!
    @brief
    This function initializes the system time module.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_init
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_sem_init

===========================================================================*/
/*!
    @brief
    This function initializes the system time semaphore.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_sem_init
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_sem_deinit

===========================================================================*/
/*!
    @brief
    This function deinitializes the system time semaphore.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_sem_deinit
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_clear_cell_info

===========================================================================*/
/*!
    @brief
    This function clears the SCELL/CCELL information

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_clear_cell_info
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_scell_is_ccell

===========================================================================*/
/*!
    @brief
    This function returns whether the current cell is the serving cell.

    @return
    Boolean - TRUE if the current cell is the serving cell, otherwise FALSE.
*/
/*=========================================================================*/
boolean lte_ml1_schdlr2_systime_scell_is_ccell
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_scell_is_target_cell

===========================================================================*/
/*!
    @brief
    This function returns whether the target cell is the serving cell.

    @return
    Boolean - TRUE if the target cell is the serving cell, otherwise FALSE.
 
    @note
    This function is currently assumed to only be called in GM thread context
    so there is no cross-thread protection.
*/
/*=========================================================================*/
boolean lte_ml1_schdlr2_systime_scell_is_target_cell
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  lte_ml1_schdlr_cell_info_s *target_cell   /*!< Target cell               */
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_ccell_is_target_cell

===========================================================================*/
/*!
    @brief
    This function returns whether the target cell is the current cell.

    @return
    Boolean - TRUE if the target cell is the current cell, otherwise FALSE.
 
    @note
    This function is currently assumed to only be called in GM thread context
    so there is no cross-thread protection.
*/
/*=========================================================================*/
boolean lte_ml1_schdlr2_systime_ccell_is_target_cell
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  lte_ml1_schdlr_cell_info_s *target_cell   /*!< Target cell               */
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_set_scell

===========================================================================*/
/*!
    @brief
    This function sets the serving cell.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_set_scell
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  lte_ml1_schdlr_cell_info_s *scell_info,    /*!< Serving cell information */
  boolean                    adjust_scell_delta
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_ccell

===========================================================================*/
/*!
    @brief
    This function returns the current cell.

    @return
    lte_ml1_schdlr_cell_info_s * - The current cell information.
 
    @note
    This function is only ever called in GM thread context so no semaphore
    reader is required.
*/
/*=========================================================================*/
lte_ml1_schdlr_cell_info_s * lte_ml1_schdlr2_systime_get_ccell
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);


/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_scell

===========================================================================*/
/*!
    @brief
    This function returns the serving cell.

    @return
    lte_ml1_schdlr_cell_info_s * - The serving cell information.
 
    @note
    This function is only ever called in GM thread context so no semaphore
    reader is required.
*/
/*=========================================================================*/
lte_ml1_schdlr_cell_info_s * lte_ml1_schdlr2_systime_get_scell
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*=========================================================================== 
 
  FUNCTION:  lte_ml1_schdlr2_systime_set_ccell

===========================================================================*/
/*!
    @brief
    This function sets the current cell.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_set_ccell
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  lte_ml1_schdlr_cell_info_s *ccell_info    /*!< Current cell information  */
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get

===========================================================================*/
/*!
    @brief
    This function returns the current system time.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_get_systime
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! System time structure to populate */
  lte_ml1_schdlr_systime_info_s *systime
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_on_fake_tick

===========================================================================*/
/*!
    @brief
    This function updates the current system time on a fake tick.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_on_fake_tick
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! Current SFN */
  lte_ml1_schdlr_sf_time_t curr_sf,
  /*! Tick information */
  lte_ml1_schdlr_sched_fake_tick_info_t *fake_tick_info,
  /*! SCLK value */ 
  uint32 sclk_val
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_fake_tick_info

===========================================================================*/
/*!
    @brief
    This function updates sclk value from the fake tick

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_fake_tick_info
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! Fake tick info */
  lte_ml1_schdlr_sched_fake_tick_info_t *fake_tick_info
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_on_tick

===========================================================================*/
/*!
    @brief
    This function updates the current system time on a FW tick.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_on_tick
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! Current SFN */
  lte_ml1_schdlr_sf_time_t curr_sf,
  /*! Tick information */
  lte_ml1_schdlr_sched_tick_info_t *tick_info 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_on_cswitch

===========================================================================*/
/*!
    @brief
    This function updates the system time on a cell switch.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_on_cswitch
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
   /*! Current SFN */
  lte_ml1_schdlr_sf_time_t curr_sf
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_sfn_on_stat_cfg

===========================================================================*/
/*!
    @brief
    The function updates the system time when scheduler receives the
    SCELL_STAT_CFG_CNF.

    @detail
    The function is used to update sfn/subfn on receiving the
    scell_stat_cfg_cnf. This should eliminate the race condition in
    scheduler on a reselection failure where timing is off unti we get
    the first FW tick.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_sfn_on_stat_cfg
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! New SFN */
  lte_ml1_schdlr_sf_time_t new_sf                
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_update_sfn

===========================================================================*/
/*!
    @brief
    The function updates the system time upon the recovery.

    @detail
    The function is used in the connected state handover scenario or resuming
    from a suspend in idle mode which is used by the scheduler to restore the
    recovered system time. It is triggered by a time slam confirmation.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_schdlr2_systime_update_sfn
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  /*! New SFN */
  lte_ml1_schdlr_sf_time_t new_sf,     
  /*! Whether the slam is frame-only */     
  boolean slam_frame_only,                 
  boolean unknown_timeline_transition                   
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_sf_now

===========================================================================*/
/*!
    @brief
    This function returns the current subframe number.

    @return
    lte_ml1_schdlr_sf_time_t - Current SF number
*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_schdlr2_systime_get_sf_now
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_scell_sf_now

===========================================================================*/
/*!
    @brief
    This function returns the current subframe number for the serving cell.

    @return
    lte_ml1_schdlr_sf_time_t - Current SF number
 
    @note
    This function is only ever called in GM thread context so no semaphore
    reader is required.
*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_schdlr2_systime_get_scell_sf_now
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_target_cell_sf_now

===========================================================================*/
/*!
    @brief
    This function returns the current subframe number for the target cell.

    @return
    lte_ml1_schdlr_sf_time_t - Current SFN number 
 
    @note
    This function is only ever called in GM thread context so no semaphore
    reader is required.
*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_schdlr2_systime_get_target_cell_sf_now
(
  lte_mem_instance_type      instance,         /*!  Scheduler Instance     */ 
  lte_ml1_schdlr_cell_info_s *target_cell   /*!< Target cell               */
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_get_sf_delta_to_scell

===========================================================================*/
/*!
    @brief
    This function returns the current subframe delta to the SCELL.

    @return
    lte_ml1_schdlr_sf_time_t - SFN delta to the SCELL
 
    @note
    This function is only ever called in GM thread context so no semaphore
    reader is required.
*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_schdlr2_systime_get_sf_delta_to_scell
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_translate_to_ocell_sf

===========================================================================*/
/*!
    @brief
    This function translates the specified serving cell SFN to the other cell
    SFN.

    @return
    lte_ml1_schdlr_sf_time_t - Translated SFN for other cell
*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_schdlr2_systime_translate_to_ocell_sf
( 
  lte_mem_instance_type      instance,          /*! Scheduler Instance     */ 
  lte_ml1_schdlr_sf_time_t scell_sf         /*!< Serving cell SFN          */
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_systime_test_get_scell

===========================================================================*/
/*!
    @brief
    This function returns the serving cell.

    @return
    lte_ml1_schdlr_systime_info_s - System time information
 
    @note
    Only used for off-target testing.
*/
/*=========================================================================*/
lte_ml1_schdlr_systime_info_s * lte_ml1_schdlr2_systime_test_get_systime
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

#endif /* LTE_ML1_SCHDLR2_SYSTIME_H */
