#ifndef LTE_ML1_SCHDLR_MAIN_INT_H
#define LTE_ML1_SCHDLR_MAIN_INT_H
/*!
  @file
  lte_ml1_schdlr.h

  @brief
  This module contains the code for ML1 scheduler management.

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/schdlr/src/lte_ml1_schdlr_main_int.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/09/13   ns     Initial version
===========================================================================*/

/*===========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "lte_ml1_comdef.h"
#include "lte_ml1_mem.h"
#include "lte_ml1_schdlr_types.h"
#include "lte_ml1_schdlr_dog.h"
#include "lte_ml1_schdlr_log.h"
#include "lte_ml1_schdlr2_obj.h"
#include "lte_ml1_schdlr2_cswitch.h"
#include "lte_ml1_schdlr_profile.h"
#include "lte_ml1_schdlr2_event_list.h"
#include "lte_ml1_schdlr2_group.h"
#include "lte_ml1_schdlr2_mode.h"
#include "lte_ml1_schdlr2_obj_list.h"
#include "lte_ml1_schdlr2_req_list.h"
#include "lte_ml1_schdlr2_systime.h"
#include "lte_ml1_schdlr_log_types.h"
#include "lte_ml1_common_stm_profile.h"

/*===========================================================================

                       EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/


#define LTE_ML1_SCHDLR_LL_MUTEX

#ifdef FEATURE_MODEM_TARGET_BUILD
#undef  LTE_ML1_SCHDLR_LL_MUTEX_PROFILING
#else 
/* Dont enable profiling for offtarget builds */
#undef  LTE_ML1_SCHDLR_LL_MUTEX_PROFILING
#endif

#ifdef LTE_ML1_SCHDLR_LL_MUTEX

#ifdef LTE_ML1_SCHDLR_LL_MUTEX_PROFILING
#define LTE_ML1_SCHDLR_LL_LOCK(mutex_data)                                     \
  HWIO_OUT( O_STMR_STATUS_DUMP_CMD, 1 );                                     \
  mutex_data->slck_start =                       \
  HWIO_INF( O_STMR_MSTMR_STATUS, MSTMR_TIME );                               \
  pthread_mutex_lock(&mutex_data->mutex);        \
  HWIO_OUT( O_STMR_STATUS_DUMP_CMD, 1 );                                     \
  mutex_data->slck_end =                         \
  HWIO_INF( O_STMR_MSTMR_STATUS, MSTMR_TIME );                               \
  mutex_data->total_time +=                      \
  SUB_MOD( mutex_data->slck_end,                 \
  mutex_data->slck_start,                        \
  LTE_MAX_OSTMR_COUNT );                                                     \
  mutex_data->total_count++;

#define LTE_ML1_SCHDLR_LL_UNLOCK(mutex_data)                                   \
  HWIO_OUT( O_STMR_STATUS_DUMP_CMD, 1 );                                     \
  mutex_data->slck_start =                       \
  HWIO_INF( O_STMR_MSTMR_STATUS, MSTMR_TIME );                               \
  pthread_mutex_unlock(&mutex_data->mutex);      \
  HWIO_OUT( O_STMR_STATUS_DUMP_CMD, 1 );                                     \
  mutex_data->slck_end =                         \
  HWIO_INF( O_STMR_MSTMR_STATUS, MSTMR_TIME );                               \
  mutex_data->total_time +=                      \
  SUB_MOD( mutex_data->slck_end,                 \
  mutex_data->slck_start,                        \
  LTE_MAX_OSTMR_COUNT );                                                     \
  mutex_data->total_count++; 
#else /* LTE_ML1_SCHDLR_LL_MUTEX_PROFILING */
#define LTE_ML1_SCHDLR_LL_LOCK(mutex_data)     pthread_mutex_lock(             \
  &mutex_data->mutex);
#define LTE_ML1_SCHDLR_LL_UNLOCK(mutex_data)   pthread_mutex_unlock(           \
  &mutex_data->mutex);
#endif  /* LTE_ML1_SCHDLR_LL_MUTEX_PROFILING */

#else  /* LTE_ML1_SCHDLR_LL_MUTEX */

#define LTE_ML1_SCHDLR_LL_LOCK(mutex_data)
#define LTE_ML1_SCHDLR_LL_UNLOCK(mutex_data)

#endif /* LTE_ML1_SCHDLR_LL_MUTEX */


#define LTE_ML1_SCHDLR_DEBUG_INFO_CNT                20

  /*! Scheduler linked mutex data */
typedef struct 
{

  /*! Mutex to lock list operations */
  pthread_mutex_t mutex;

  /*! Total time taken for lock operations */
  uint64 total_time;

  /*! Total number of lock operations */
  uint32 total_count;

  /*! Temp variables for storing start and end time of mutex operations */
  uint32 slck_start;
  uint32 slck_end;
}lte_ml1_schdlr2_ll_mutex_data_s;


typedef struct 
{
  uint8 subfn;
  uint16 sfn;
  uint32 stmr_count;
  uint32 stmr_count_diff;
} lte_ml1_schdlr_debug_info_s;

typedef struct 
{
  lte_mem_instance_type      instance;

  /* Schdlr activate status */
  boolean schdlr_activated;

  /* Mutex to protect scheduler linked list operations from external function
     calls which operate on event lists */
  lte_ml1_schdlr2_ll_mutex_data_s mutex_data;

  /* Dog timer enable */
  boolean dog_timer_enable;

  lte_ml1_schdlr_debug_info_s debug_info[LTE_ML1_SCHDLR_DEBUG_INFO_CNT];

  uint8 debug_index;

  /* Pointers to all globals across all module, helpful for debug 
     Please note: Do not miss use the power. 
     These varibales should not be accessed directly from the code. The code 
     should use the instance number and use only globals it has access to. 

     If a module needs perform cross module communication(read/writes), then  
     function calls must be used to make the code readable and maintainable!! */

  lte_ml1_schdlr_profile_s         * profile;
  lte_ml1_schdlr_cswitch_data_s    * cswitch;
  lte_ml1_schdlr_event_list_data_s * event_list;
  lte_ml1_schdlr_group_data_s      * group;
  lte_ml1_schdlr_mode_data_s       * mode;
  lte_ml1_schdlr_obj_data_s        * obj;
  lte_ml1_schdlr_obj_list_data_s   * obj_list;
  lte_ml1_schdlr_req_list_data_s   * req_list;
  lte_ml1_common_stm_profile_s     * stm_profile;
  lte_ml1_schdlr_systime_data_s    * systime;
  lte_ml1_schdlr_dog_timer_data_s  * dog_timer;
  lte_ml1_schdlr_log_buffer_s      * log;

}lte_ml1_schdlr_db_s;

/*===========================================================================

                       EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION: lte_ml1_schdlr_same_thread_service_req

===========================================================================*/
/*!
    @brief
    The function submits a scheduling service to the scheduler by directly 
	pulling the scheduler STM.

    @detail
	This function is expected to be used within the same task as the scheduler.
	This api is not allowed used in the scheduler callback as the STM service
	is not allowed to processing STM inputs during the transfer function.

    @return
    boolean to validate the scheduling request 
*/
/*=========================================================================*/

extern boolean lte_ml1_schdlr_same_thread_service_req
(
  /* Scheduler Instance     */  
  lte_mem_instance_type      instance, 
  /* Service request structure */
  lte_ml1_schdlr_sched_req_s*       sched_req_ptr
);

/*===========================================================================

  FUNCTION: lte_ml1_schdlr_same_thread_objreg_req

===========================================================================*/
/*!
    @brief
    The function submits a object registration request to the scheduler by 
    directly pulling the scheduler STM.

    @detail
	This function is expected to be used within the same task as the scheduler.
	This api is not allowed used in the scheduler callback as the STM service
	is not allowed to processing STM inputs during the transfer function.

    @return
    none
*/
/*=========================================================================*/

extern void lte_ml1_schdlr_same_thread_objreg_req
(
  /* Scheduler Instance     */  
  lte_mem_instance_type      instance, 
  /* Registration information */
  lte_ml1_schdlr_obj_reg_info_s*    reg_info_ptr
);

/*===========================================================================

  FUNCTION: lte_ml1_schdlr_same_thread_objdereg_req

===========================================================================*/
/*!
    @brief
    The function submits a object de-registration request to the scheduler by 
    directly pulling the scheduler STM.

    @detail
	This function is expected to be used within the same task as the scheduler.
	This api is not allowed used in the scheduler callback as the STM service
	is not allowed to processing STM inputs during the transfer function.

    @return
    None
*/
/*=========================================================================*/

extern void lte_ml1_schdlr_same_thread_objdereg_req
(
  /* Scheduler Instance     */  
  lte_mem_instance_type      instance, 
  /* Registration information */
  lte_ml1_schdlr_obj_dereg_info_s*    dereg_info_ptr
);


/*===========================================================================

  FUNCTION: lte_ml1_schdlr_same_thread_setgcb_req

===========================================================================*/
/*!
    @brief
    The function submits a set group request to the scheduler by directly pulling
    the scheduler STM.

    @detail
    This function is expected to be used within the same task as the scheduler.
    This api is not allowed used in the scheduler callback as the STM service
    is not allowed to processing STM inputs during the transfer function.

    @return
    None
*/
/*=========================================================================*/

void lte_ml1_schdlr_same_thread_setgcb_req
(
  /* Scheduler Instance     */  
  lte_mem_instance_type      instance, 
  /* GCB info pointor */
  lte_ml1_schdlr_setgroup_req_s*        gcb_info_ptr
);


/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_ll_mutex_init

===========================================================================*/
/*!
    @brief
    This function intializes the mutex for linked list access 

    @return
*/
/*=========================================================================*/
void lte_ml1_schdlr_mutex_init
(
  lte_mem_instance_type      instance            /* Scheduler Instance     */  
);


/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_ll_mutex_deinit

===========================================================================*/
/*!
    @brief
    This function frees the mutex memory

    @return
*/
/*=========================================================================*/
void lte_ml1_schdlr_mutex_deinit
(
  lte_mem_instance_type      instance            /* Scheduler Instance     */  
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr2_ll_profile_mutex

===========================================================================*/
/*!
    @brief
    This function logs the mutex information

    @return

*/
/*=========================================================================*/
void lte_ml1_schdlr_profile_mutex
(
  lte_mem_instance_type      instance            /* Scheduler Instance     */  
);

/*===========================================================================

  FUNCTION:  lte_ml1_schdlr_get_mutex

===========================================================================*/
/*!
    @brief
    This function returns the mutex to use for the instance

    @return

*/
/*=========================================================================*/
lte_ml1_schdlr2_ll_mutex_data_s * lte_ml1_schdlr_get_mutex
(
  lte_mem_instance_type      instance         /*! Scheduler Instance     */ 
);

#endif /* LTE_ML1_SCHDLR_MAIN_INT_H */

