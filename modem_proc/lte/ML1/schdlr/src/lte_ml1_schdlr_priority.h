#ifndef LTE_ML1_SCHDLR_PRIORITY_H
#define LTE_ML1_SCHDLR_PRIORITY_H
/*!
  @file
  lte_ml1_schdlr_priority.h

  @brief
  This module contains the code for ML1 scheduler priority management.

*/

/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/schdlr/src/lte_ml1_schdlr_priority.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/09/13   ns     Initial version
===========================================================================*/

/*===========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "lte_ml1_comdef.h"


/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==========================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_schdlr_priority_table_validate

===========================================================================*/
/*!
    @brief
    The function validate that the size of the priority table matches to the
    size of the scheduling obj id enum type.

    @detail

    @return
    None
*/
/*=========================================================================*/
extern void lte_ml1_schdlr_priority_table_validate(void);

#endif /* LTE_ML1_SCHDLR_PRIORITY_H */
