#ifndef LTE_ML1_SCHDLR_CFG_H
#define LTE_ML1_SCHDLR_CFG_H
/*!
  @file
  lte_ml1_schdlr_cfg.h

  @brief
  This module implements scheduler configuration functions.

*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/schdlr/src/lte_ml1_schdlr_cfg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/12    ns     Support for scheduler version 2
10/28/11   tjc     Initial version

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "lte_ml1_comdef.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! Scheduler callback profiling configuration structure */
typedef struct 
{
  /*! Whether the specific callback profiling is enabled */
  boolean enabled;

  /*! Ts threshold for reporting */
  uint32 reporting_threshold;

} lte_ml1_schdlr_config_cb_profiling_s;

/*! Scheduler profiling configration */
typedef struct
{
  /*! Whether STM profiling is enabled */
  boolean stm_enabled;

  /*! Whether cb profiling is enabled. Added to optimize the checks needed 
      to determine if any time of profiling is needed. This needs to
      be set for cb profiling to work */
  boolean cb_profiling;

  /*! Action callback profiling configuration 
      Remember to set the cb_profiling flag */
  lte_ml1_schdlr_config_cb_profiling_s action_cb;

  /*! Abort callback profiling configuration 
      Remember to set the cb_profiling flag */
  lte_ml1_schdlr_config_cb_profiling_s abort_cb;

  /*! Group callback profiling configuration 
      Remember to set the cb_profiling flag  */
  lte_ml1_schdlr_config_cb_profiling_s group_cb;

  uint32 last_log_time;  

} lte_ml1_schdlr_profiling_config_s;

/*! Scheduler configuration structure */
typedef struct 
{
  /*! Whether the dog timer is enabled */
  boolean dog_timer_enabled;

  /*! Message mask */
  uint32 msg_mask;

  /*! Scheduler profiling configuration */
  lte_ml1_schdlr_profiling_config_s profiling;
  
} lte_ml1_schdlr_config_s;

/*! Variable that has the current scheduler configuration */
extern lte_ml1_schdlr_config_s lte_ml1_schdlr_config;

/* Variable to save off scheduler configuration to EFS/NV */
extern boolean lte_ml1_schdlr_save_config;

#endif /* LTE_ML1_SCHDLR_CFG_H */
