/*=============================================================================

    __lte_ml1_schdlr2_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_schdlr2_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SCHDLR2_STM_INT_H
#define __LTE_ML1_SCHDLR2_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_schdlr2_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SCHDLR2_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SCHDLR2_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SCHDLR2_STM_NUM_STATES 4

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SCHDLR2_STM_NUM_INPUTS 14

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_schdlr2_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_schdlr2_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_schdlr2_stm_init_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_schdlr2_stm_unknown_timeline_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_schdlr2_stm_sctl_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_schdlr2_stm_octl_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_schdlr2_stm_octl_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_schdlr2_stm_init_activate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_deactivate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_obj_reg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_obj_dereg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_cell_switch_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_suspend_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_resume_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_sched_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_setgroup_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_x2l_ho_prep_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_dog_timer_en_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_init_dog_timer_dis_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_activate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_deactivate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_obj_reg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_obj_dereg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_cell_switch_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_fake_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_suspend_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_resume_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_sched_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_setgroup_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_x2l_ho_prep_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_dog_timer_en_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_unknown_timeline_dog_timer_dis_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_activate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_deactivate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_obj_reg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_obj_dereg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_cell_switch_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_fake_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_suspend_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_resume_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_sched_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_setgroup_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_sctl_x2l_ho_prep_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_activate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_deactivate_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_obj_reg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_obj_dereg_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_cell_switch_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_fake_tick_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_suspend_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_resume_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_sched_req_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_schdlr2_stm_octl_setgroup_req_cmd(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SCHDLR2_STM_INIT_STATE,
  LTE_ML1_SCHDLR2_STM_UNKNOWN_TIMELINE_STATE,
  LTE_ML1_SCHDLR2_STM_SCTL_STATE,
  LTE_ML1_SCHDLR2_STM_OCTL_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SCHDLR2_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SCHDLR2_STM_INT_H */
