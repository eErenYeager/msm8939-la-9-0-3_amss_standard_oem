/*!
  @file
  __lte_ml1_afc_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_AFC_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_afc_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_afc_stm_type;


/*! @brief Variables internal to module __lte_ml1_afc_stm.stub
*/
STATIC __lte_ml1_afc_stm_type __lte_ml1_afc_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_AFC_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  afc_stm_enter

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_AFC_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void afc_stm_enter
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* afc_stm_enter() */


/*===========================================================================

     (State Machine: LTE_ML1_AFC_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: INACTIVE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  afc_inactive_enter

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_AFC_STM,
    state INACTIVE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void afc_inactive_enter
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* afc_inactive_enter() */


/*===========================================================================

  STATE EXIT FUNCTION:  afc_inactive_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_AFC_STM,
    state INACTIVE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void afc_inactive_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* afc_inactive_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_tcxomgr_grant_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state INACTIVE,
    upon receiving input LTE_ML1_AFC_TCXOMGR_GRANT_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_TCXOMGR_GRANT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_tcxomgr_grant_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_tcxomgr_grant_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_tcxomgr_change_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state INACTIVE,
    upon receiving input LTE_ML1_AFC_TCXOMGR_CHANGE_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_TCXOMGR_CHANGE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_tcxomgr_change_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_tcxomgr_change_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_ignore

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state INACTIVE,
    upon receiving input LTE_ML1_AFC_RELEASE_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RELEASE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_ignore
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_ignore() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_inactive_rv_xo_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state INACTIVE,
    upon receiving input LTE_ML1_AFC_RV_XO_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RV_XO_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_inactive_rv_xo_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_inactive_rv_xo_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_inactive_rpush_flag_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state INACTIVE,
    upon receiving input LTE_ML1_AFC_RPUSH_FLAG_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RPUSH_FLAG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_inactive_rpush_flag_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_inactive_rpush_flag_req() */


/*===========================================================================

     (State Machine: LTE_ML1_AFC_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: RV_XO

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  afc_rv_xo_enter

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_AFC_STM,
    state RV_XO

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void afc_rv_xo_enter
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* afc_rv_xo_enter() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_sleep_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_SLEEP_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_SLEEP_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_sleep_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_sleep_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_release_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_RELEASE_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RELEASE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_release_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_release_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_rv_xo_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_RV_XO_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RV_XO_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_rv_xo_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_rv_xo_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_rgs_timer_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_RGS_TIMER_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RGS_TIMER_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_rgs_timer_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_rgs_timer_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_rpush_flag_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_RPUSH_FLAG_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RPUSH_FLAG_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_rpush_flag_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_rpush_flag_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_rpush_sample_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_RPUSH_SAMPLE_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_RPUSH_SAMPLE_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_rpush_sample_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_rpush_sample_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_log_timer_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_LOG_TIMER_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_LOG_TIMER_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_log_timer_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_log_timer_req() */


/*===========================================================================

  TRANSITION FUNCTION:  afc_flush_rpush_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_AFC_STM,
    state RV_XO,
    upon receiving input LTE_ML1_AFC_FLUSH_RPUSH_REQ

    @detail
    Called upon receipt of input LTE_ML1_AFC_FLUSH_RPUSH_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t afc_flush_rpush_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* afc_flush_rpush_req() */




