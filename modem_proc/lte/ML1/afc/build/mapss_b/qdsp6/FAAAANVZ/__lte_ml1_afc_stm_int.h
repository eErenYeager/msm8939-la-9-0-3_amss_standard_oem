/*=============================================================================

    __lte_ml1_afc_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_afc_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_AFC_STM_INT_H
#define __LTE_ML1_AFC_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_afc_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_AFC_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_AFC_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_AFC_STM_NUM_STATES 2

/* Define a macro for the number of SM inputs */
#define LTE_ML1_AFC_STM_NUM_INPUTS 10

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void afc_stm_enter(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void afc_inactive_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void afc_inactive_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void afc_rv_xo_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t afc_tcxomgr_grant_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_tcxomgr_change_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_ignore(stm_state_machine_t *sm, void *payload);
stm_state_t afc_inactive_rv_xo_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_inactive_rpush_flag_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_sleep_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_rv_xo_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_rgs_timer_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_rpush_flag_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_rpush_sample_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_log_timer_req(stm_state_machine_t *sm, void *payload);
stm_state_t afc_flush_rpush_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  RV_XO,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_AFC_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_AFC_STM_INT_H */
