/*!
  @file
  __lte_ml1_coex_dsda_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_COEX_DSDA_SM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_coex_dsda_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_coex_dsda_stm_type;


/*! @brief Variables internal to module __lte_ml1_coex_dsda_stm.stub
*/
STATIC __lte_ml1_coex_dsda_stm_type __lte_ml1_coex_dsda_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_COEX_DSDA_SM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_coex_dsda_sm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_COEX_DSDA_SM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_sm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_sm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_coex_dsda_sm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_COEX_DSDA_SM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_sm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_sm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_COEX_DSDA_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: COEX_DSDA_SINGLE_ACT

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_coex_dsda_single_act_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_single_act_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_single_act_state_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_coex_dsda_single_act_state_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_single_act_state_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_single_act_state_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_dsda_enter_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_dsda_enter_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_dsda_enter_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_nop

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_nop
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_nop() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_single_mcs_prio_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_single_mcs_prio_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_single_mcs_prio_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_blacklist_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_SINGLE_ACT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_blacklist_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_blacklist_proc() */


/*===========================================================================

     (State Machine: LTE_ML1_COEX_DSDA_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: COEX_DSDA_CONFLICT

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_dsda_conflict_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dsda_conflict_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dsda_conflict_state_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_dsda_conflict_state_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_dsda_conflict_state_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_dsda_conflict_state_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_dsda_exit_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_dsda_exit_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_dsda_exit_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_conn_enter_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_conn_enter_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_conn_enter_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_dl_on_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_DL_ON_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_DL_ON_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_dl_on_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_dl_on_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_dl_off_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_dl_off_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_dl_off_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_freqid_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_FREQID_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_FREQID_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_freqid_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_freqid_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_arb_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_arb_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_arb_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_prio_sw_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_prio_sw_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_prio_sw_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_cancel_prio_sw_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_cancel_prio_sw_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_cancel_prio_sw_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_confl_mcs_prio_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_confl_mcs_prio_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_confl_mcs_prio_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_wwcoex_state_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_wwcoex_state_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_wwcoex_state_proc() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_ba_timeout_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT,
    upon receiving input LTE_ML1_COEX_DSDA_STM_BA_TO_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_BA_TO_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_ba_timeout_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_ba_timeout_proc() */


/*===========================================================================

     (State Machine: LTE_ML1_COEX_DSDA_SM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: COEX_DSDA_CONFLICT_CONN

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_coex_dsda_conflict_conn_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT_CONN

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_conflict_conn_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_conflict_conn_state_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_coex_dsda_conflict_conn_state_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT_CONN

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_coex_dsda_conflict_conn_state_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_coex_dsda_conflict_conn_state_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_coex_dsda_stm_conn_exit_proc

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_COEX_DSDA_SM,
    state COEX_DSDA_CONFLICT_CONN,
    upon receiving input LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD

    @detail
    Called upon receipt of input LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_coex_dsda_stm_conn_exit_proc
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_coex_dsda_stm_conn_exit_proc() */




