/*=============================================================================

    __lte_ml1_coex_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_coex_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_COEX_STM_INT_H
#define __LTE_ML1_COEX_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_coex_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_COEX_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_COEX_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_COEX_SM_NUM_STATES 5

/* Define a macro for the number of SM inputs */
#define LTE_ML1_COEX_SM_NUM_INPUTS 19

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_coex_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_coex_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_coex_off_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_off_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_init_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_init_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_on_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_on_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_triggered_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_coex_triggered_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_coex_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_off_update_and_notify_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_off_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_pl_info(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_state_update_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_update_and_notify_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_activation_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_reading_counter_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_buffer_frame_timing(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_phrless_backoff_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_phr_backoff_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_phrless_backoff_ind_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_pending_ta_expiry_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_log_timer_expiry_ind_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_init_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_freqid_list_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_power_ind_report_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_deactivation_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_reading_complete_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_coex_frame_timing_proc(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_COEX_OFF_STATE,
  LTE_ML1_COEX_INIT_STATE,
  LTE_ML1_COEX_ON_STATE,
  LTE_ML1_COEX_TRIGGERED_STATE,
  LTE_ML1_COEX_DEBUG_OFF_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_COEX_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_COEX_STM_INT_H */
