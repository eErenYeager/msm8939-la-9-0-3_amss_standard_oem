/*!
  @file
  lte_ml1_coex_dsda_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/cxm/inc/lte_ml1_coex_dsda_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_COEX_DSDA_NV_H
#define LTE_ML1_COEX_DSDA_NV_H
/*===========================================================================
		
						  MODULE DEFINITIONS AND TYPES
		
===========================================================================*/
#include <comdef.h>	

/*===========================================================================
	 
	FUNCTION:  lte_ml1_coex_nv_enable_dsda_size
	 
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
			
	@details
	 
	@return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_coex_dsda_nv_coex_dsda_enable_size(void);


/*===========================================================================
	 
	FUNCTION:  lte_ml1_coex_nv_coex_enable_dsda_defaultvalue
	 
===========================================================================*/
/*!
	@brief
	This function provides the default value for the efs variable. 
	This data will be stored with the EFS framework.
	 
	@details
	EFS framework sends the pointer to memory(which framework owns), 
	to fill in the default value.
	 
	@return
*/
/*=========================================================================*/

void lte_ml1_coex_dsda_nv_coex_dsda_enable_defaultvalue(void *ptr);


/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_nv_cfg_is_dsda_enabled

===========================================================================*/
/*!
  @brief
  Is general dsda enabled

  @return
  TRUE if genaral dsda is enabled
*/
/*=========================================================================*/
boolean lte_ml1_coex_dsda_nv_cfg_is_dsda_enabled
(
  lte_mem_instance_type            instance
);

/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_nv_cfg_is_ba_enabled

===========================================================================*/
/*!
  @brief
  Is band avoidance enabled

  @return
  TRUE if band avoidance is enabled 
*/
/*=========================================================================*/
boolean lte_ml1_coex_dsda_nv_cfg_is_ba_enabled
(
  lte_mem_instance_type            instance
);

#endif


