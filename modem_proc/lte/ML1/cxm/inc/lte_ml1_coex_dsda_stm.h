/*!
  @file
  lte_ml1_coex_stm.h

  @brief
  Header file for use by all of the unit test state machines

*/

/*===========================================================================

  Copyright (c) 2007 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/cxm/inc/lte_ml1_coex_dsda_stm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/21/1   ama     Inital Checkin
===========================================================================*/

#ifndef __LTE_ML1_COEX_DSDA_STM_H__
#define __LTE_ML1_COEX_DSDA_STM_H__


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

//#include <comdef.h>
#include <stm2.h>

/*===========================================================================

                     TYPES

===========================================================================*/
typedef enum
{
  LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD,
  LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD,  
  LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD,
  LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD,
  LTE_ML1_COEX_DSDA_STM_DL_ON_CMD,
  LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD,
  LTE_ML1_COEX_DSDA_STM_FREQID_CMD,
  LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD,
  LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD,
  LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD,
  LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD,
  LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD,
  LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD,
  LTE_ML1_COEX_DSDA_STM_BA_TO_CMD,
  LTE_ML1_COEX_DSDA_STM_MAX_NUM_CMDS
} lte_ml1_coex_dsda_stm_input_e;


/*===========================================================================

                     FUNCTION PROTOTYPES

===========================================================================*/



/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*! @brief Externalized error number from error function */
extern stm_status_t       stm_test_errno;


extern void lte_ml1_coex_dsda_sm_debug_handler
(
  stm_debug_event_t           debug_event,
  stm_state_machine_t         *sm,
  stm_state_t                 state_info,
  void                        *payload
);


extern void lte_ml1_coex_dsda_sm_error_handler
(
  stm_status_t                error,
  const char                  *filename,
  uint32                      line,
  stm_state_machine_t         *sm
);


boolean lte_ml1_coex_dsda_sm_init(  lte_mem_instance_type ,msgr_id_t,msgr_client_t *);

#endif /*__LTE_ML1_COEX_DSDA_STM_H__*/


