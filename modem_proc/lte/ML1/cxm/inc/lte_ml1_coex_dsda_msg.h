/*!
  @file lte_ml1_coex_dsda_msg.h

  @brief
   A header describing the messages to the co-existence DSDA module.

  Details...
  
*/

/*==============================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/cxm/inc/lte_ml1_coex_dsda_msg.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------

==============================================================================*/

#ifndef __LTE_ML1_COEX_DSDA_MSG_H__
#define __LTE_ML1_COEX_DSDA_MSG_H__
/*==============================================================================
 
  Header files
 
==============================================================================*/
#include "lte_variation.h"
#include <lte_ml1_comdef.h>
#include <lte_ml1_schdlr_types.h>
#ifdef FEATURE_LTE_ML1_COEX_DSDA
/*==============================================================================
 
  Includes
 
==============================================================================*/
#include <wwan_coex_mgr.h>
/*==============================================================================
 
  Types
 
==============================================================================*/
/*==========================================================================
  Number of priorities from MCS
===========================================================================*/
/*! Max number of activities (tiers) supported by LTE */
#define    LTE_ML1_COEX_ACTIVITY_NUM             MAX_ACTIVITY_TIERS

/*! How many ms arb will happen prior to  action time */
#define    LTE_ML1_COEX_DSDA_ARB_SUBFN_OFFSET     4

/*! How many ms prio switch will be sent prior to action time */
#define    LTE_ML1_COEX_DSDA_SEND_FW_SUBFN_OFFSET (LTE_ML1_COEX_DSDA_ARB_SUBFN_OFFSET - 1)

typedef uint32 lte_ml1_coex_dsda_prio_num_t;

typedef uint32 lte_ml1_coex_dsda_prio_t;

/*
 * Activities that needs protection
 */
typedef enum
{
  /*! Invalid activity */
  LTE_ML1_COEX_DSDA_ACTIVITY_INVALID = 0,
  /*! Freqency scan */
  LTE_ML1_COEX_DSDA_ACTIVITY_FREQ_SCAN,
  /*! Initial acquisition search */
  LTE_ML1_COEX_DSDA_ACTIVITY_INITIAL_ACQ,
  /*! Paging */
  LTE_ML1_COEX_DSDA_ACTIVITY_PAGING,
  /*! Rach */
  LTE_ML1_COEX_DSDA_ACTIVITY_RACH,
  /*! DRX (idle/cdrx) */
  LTE_ML1_COEX_DSDA_ACTIVITY_DRX,
  /*! Intra_freq measurement */
  LTE_ML1_COEX_DSDA_ACTIVITY_INTRA_FREQ_MEAS,
  /*! Inter-freq search/measurement (IRAT also) */
  LTE_ML1_COEX_DSDA_ACTIVITY_INTER_FREQ_MEAS,
  /*! PRS */
  LTE_ML1_COEX_DSDA_ACTIVITY_PRS,
  /*! ANT_SWITCH */
  LTE_ML1_COEX_DSDA_ACTIVITY_ASDIV
} lte_ml1_coex_dsda_actitiy_e;

/*
 * Flag for priority switching
 */
typedef enum
{
  /*! switch priority with arbitration */
  LTE_ML1_COEX_DSDA_ACTIVITY_ACT_ARBITRATION = 0, 
  /*! send priroity req to fw asap without arbitration */
  LTE_ML1_COEX_DSDA_ACTIVITY_ACT_ASAP,
  /*! send priroity req to fw asap without arbitration and scheduler delay */
  LTE_ML1_COEX_DSDA_ACTIVITY_ACT_ASAP_NO_SCHDLR
} lte_ml1_coex_dsda_action_time_flag_e;

/*
 * Conflict state from MCS
 */
typedef enum
{
  /*! No conflict between G and L */
  LTE_ML1_COEX_DSDA_CONFLICT_NONE,
  /*! LTE is doing UL and having conflict */
  LTE_ML1_COEX_DSDA_CONFLICT_UL,
  /*! LTE is doing UL and having conflict */
  LTE_ML1_COEX_DSDA_CONFLICT_DL,
  /*! LTE is doing UL and DL and having conflict */
  LTE_ML1_COEX_DSDA_CONFLICT_ULDL,
  /*! Max number of conflict status */
  LTE_ML1_COEX_DSDA_CONFLICT_MAX_NUM
} lte_ml1_coex_dsda_conflict_status_e;

/* Prototype of conflict state notify callback*/
typedef void (*conflict_st_notify_cb_t)(lte_ml1_coex_dsda_conflict_status_e);

/*
 * Results for cancelling prio switch req
 */
typedef enum
{
  /*! Success */
  E_LTE_ML1_COEX_DSDA_SUCCESS = 0,
  /*! Wrong handle */
  E_LTE_ML1_COEX_DSDA_INVALID_HANDLE = -1,
  /*! Unable to cancel because request has been sent */
  E_LTE_ML1_COEX_DSDA_REQUEST_SENT = -2,
  /*! All other errors */
  E_LTE_ML1_COEX_DSDA_OTHER_ERROR = -3
} lte_ml1_dsda_cancel_prio_switch_result_e;

typedef uint32 lte_ml1_coex_dsda_tech_mask_t; /* G or L */

#define LTE_ML1_COEX_DSDA_PRIO_HANDLE_INVALID (void *)(0xFFFFFFFF)

/*! Handle that is received after making a request */
typedef void *lte_ml1_coex_dsda_prio_handle_t; 


/*==============================================================================
 
  Interfaces
 
==============================================================================*/
/*==============================================================================
  ML1 interface to change priority (MSG)
==============================================================================*/
/* LTE_ML1_COEX_DSDA_PRIO_SWITCH_REQ */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                msg_hdr;
  /*!What activity wants to raise priority */
  lte_ml1_coex_dsda_actitiy_e              activity;
  /*! On which dl earfcn */
  lte_earfcn_t                        dl_earfcn;
  /*! When priroity should be raised */
  lte_ml1_schdlr_sf_time_t            action_time;
  /*! For how long priority should be raised */
  lte_ml1_schdlr_sf_time_t            duration;
  /*! What kind of action time type */
  lte_ml1_coex_dsda_action_time_flag_e     action_time_type;  
} lte_ml1_coex_dsda_prio_switch_req_s;

/* LTE_ML1_COEX_DSDA_PRIO_SWITCH_CNF */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                msg_hdr;  
  /*! handle */
  lte_ml1_coex_dsda_prio_handle_t     handle;
} lte_ml1_coex_dsda_prio_switch_cnf_s;

/* LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_REQ */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                    msg_hdr;  
  /*! handle */
  lte_ml1_coex_dsda_prio_handle_t          handle;
} lte_ml1_coex_dsda_cancel_prio_switch_req_s;

/* LTE_ML1_COEX_DSDA_CANCEL_PRIO_SWITCH_CNF */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                     msg_hdr;  
  /*! Result */
  lte_ml1_dsda_cancel_prio_switch_result_e   result;
} lte_ml1_coex_dsda_cancel_prio_switch_cnf_s;

typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                    msg_hdr;

}lte_ml1_coex_dsda_disable_conflict_ind_s;

typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                    msg_hdr;
  lte_LL1_ue_res_mode_e                   res_mode; 
  lte_ml1_schdlr_sf_time_t                action_time;
}lte_ml1_coex_dsda_res_mode_change_ind_s;
/*==============================================================================
  Conflict state update
==============================================================================*/
typedef struct
{
  /*! MSG router header */  
  msgr_hdr_struct_type                   msg_hdr;
  /*! New conflict state */
  lte_ml1_coex_dsda_conflict_status_e    state;
} lte_ml1_coex_dsda_mcs_conflict_state_update_s;

/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_priority_switch_mgr_context

===========================================================================*/
/*!
  @brief
  Send a request to change priority (only called from MGR task context as 
  name suggests)
  
  @return 
  Request handle                        - if success
  LTE_ML1_COEX_DSDA_PRIO_HANDLE_INVALID - if fail
*/
/*=========================================================================*/
lte_ml1_coex_dsda_prio_handle_t lte_ml1_coex_dsda_priority_switch_mgr_context
(
  lte_mem_instance_type       instance,
  lte_ml1_coex_dsda_actitiy_e activity,
  lte_earfcn_t                dl_earfcn,
  lte_ml1_schdlr_sf_time_t    action_time,
  lte_ml1_schdlr_sf_time_t    duration,
  lte_ml1_coex_dsda_action_time_flag_e     action_time_type
);

/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_cancel_priority_switch_mgr_context

===========================================================================*/
/*!
  @brief
  Cancel submitted priority req using a request handle (only called from 
  MGR task context as name suggests)

  @return
  E_LTE_ML1_COEX_DSDA_SUCCESS - if sucess
  E_LTE_ML1_COEX_DSDA_XXX     - if fail
*/
/*=========================================================================*/
lte_ml1_dsda_cancel_prio_switch_result_e 
lte_ml1_coex_dsda_cancel_priority_switch_mgr_context
(
   lte_mem_instance_type           instance,
   lte_ml1_coex_dsda_prio_handle_t handle
);

/*==============================================================================
  ML1 interface to handover priority switching to sleep
==============================================================================*/
/* LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_REQ */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                msg_hdr;  
  /*! At what subframe sleep is taking over */
  lte_ml1_schdlr_sf_time_t            start_subfn;
  /*! How long is sleep taking care of prio switching */
  lte_ml1_schdlr_sf_time_t            duration;
}lte_ml1_coex_dsda_prio_switch_handover_req_s;

/* LTE_ML1_COEX_DSDA_PRIO_SWITCH_TAKEOVER_CNF */
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                msg_hdr;  
  /*! Number of async req to send out on behalf of coex */  
  uint32                              num_async_req;
  /*! Pointer to the database in coex */  
  void                                *aysnc_req_ptr;
}lte_ml1_coex_dsda_prio_switch_handover_cnf_s;


/*==============================================================================
  ML1 interface for querying conflict state
==============================================================================*/
/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_query_conflict_state

===========================================================================*/
/*!
  @brief
  Query coex database to get conflict status, aggressor, and victim. 

  @return
  Conflict status
*/
/*=========================================================================*/
lte_ml1_coex_dsda_conflict_status_e lte_ml1_coex_dsda_query_conflict_state
(
  lte_mem_instance_type instance
);

/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_conflict_state_notify_register

===========================================================================*/
/*!
  @brief
  Register a callback which will be called whenver MCS conflict state changes 

  @return
  False if it's already registered
*/
/*=========================================================================*/
boolean lte_ml1_coex_dsda_register_conflict_state_notify
(
  lte_mem_instance_type     instance,
  conflict_st_notify_cb_t   cb_func_p
);

/*==============================================================================
  ML1 interface to get priority from MCS
==============================================================================*/
typedef struct 
{
  /*! tier */
  cxm_activity_type                tier;
  /*! priority */
  lte_ml1_coex_dsda_prio_t         priority;
} lte_ml1_coex_dsda_prio_entry_s;

typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                   msg_hdr;
  /*! activity prio table from MCS */
  lte_ml1_coex_dsda_prio_num_t           num_entries;
  /*! Tier and priority mapping */
  lte_ml1_coex_dsda_prio_entry_s         activity_prio_tbl_entry[LTE_ML1_COEX_ACTIVITY_NUM];
} lte_ml1_coex_dsda_activity_prio_ind_s;

/*==============================================================================
  Start arbitration
==============================================================================*/
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                   msg_hdr;
} lte_ml1_coex_dsda_arb_ind_s;

/*==============================================================================
  Band Avoidance Timeout
===============================================================================*/
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                   msg_hdr;
} lte_ml1_coex_dsda_ba_timeout_ind_s;

/*==============================================================================
  Result of scheduler
==============================================================================*/
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                   msg_hdr;
  /*! object id */
  lte_ml1_schdlr_obj_id_e                obj_id;
  /*! Are we able to run ? */
  boolean                                success;
  /*! abort reason */
  lte_ml1_schdlr_abort_reason_e          abort_reason;          
} lte_ml1_coex_dsda_schdlr_ind_s;

/*==============================================================================
  Result of scheduler
==============================================================================*/
typedef struct
{
  /*!MSG router header */  
  msgr_hdr_struct_type                                msg_hdr;
  /*! object id */
  lte_LL1_sys_multi_rat_change_cxm_params_req_struct  req;    
} lte_ml1_coex_dsda_prio_sw_cb_ind_s;

/*==============================================================================

                    UMIDs

==============================================================================*/
enum
{
    /*Co-existence DSDA protocol UMIDs */
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, REQ, PRIO_SWITCH, 0x1, 
                    lte_ml1_coex_dsda_prio_switch_req_s
                    ),
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, CNF, PRIO_SWITCH, 0x2, 
                    lte_ml1_coex_dsda_prio_switch_cnf_s
                    ),
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, REQ, CANCEL_PRIO_SWITCH, 0x3, 
                    lte_ml1_coex_dsda_cancel_prio_switch_req_s
                    ),    
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, CNF, CANCEL_PRIO_SWITCH, 0x4, 
                    lte_ml1_coex_dsda_cancel_prio_switch_cnf_s
                    ),    
    MSGR_DEFINE_UMID( 
                    LTE, ML1_COEX_DSDA, REQ, PRIO_SWITCH_TAKEOVER, 0x5, 
                    lte_ml1_coex_dsda_prio_switch_handover_req_s
                    ),
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, CNF, PRIO_SWITCH_TAKEOVER, 0x6, 
                    lte_ml1_coex_dsda_prio_switch_handover_cnf_s
                    ),  
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, ACTIVITY_PRIO, 0x7, 
                    lte_ml1_coex_dsda_prio_switch_handover_cnf_s
                    ),   
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, ARB, 0x8, 
                    lte_ml1_coex_dsda_arb_ind_s
                    ),   
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, SCHDLR, 0x9, 
                    lte_ml1_coex_dsda_schdlr_ind_s
                    ),                       
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, BA_TIMEOUT, 0xA, 
                    lte_ml1_coex_dsda_ba_timeout_ind_s
                    ),
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, PRIO_SW_CB, 0xB, 
                    lte_ml1_coex_dsda_prio_sw_cb_ind_s
                    ),    
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, ASAP_PRIO_SW_CB, 0xC, 
                    lte_ml1_coex_dsda_prio_sw_cb_ind_s
                    ),
    MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, DISABLE_CONFLICT, 0xD, 
                    lte_ml1_coex_dsda_disable_conflict_ind_s
                    ),
      MSGR_DEFINE_UMID(
                    LTE, ML1_COEX_DSDA, IND, RES_MODE_CHANGE, 0xE, 
                    lte_ml1_coex_dsda_res_mode_change_ind_s
                    )
};
#endif /* FEATURE_LTE_ML1_COEX_DSDA */
#endif /* __LTE_ML1_COEX_DSDA_MSG_H__ */
