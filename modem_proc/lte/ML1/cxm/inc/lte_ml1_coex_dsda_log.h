/*!
  @file
  lte_ml1_coex_log.h

  @brief
  ML1-COEX log packet support header

  @detail
*/

/*===========================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/cxm/inc/lte_ml1_coex_dsda_log.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/26/11   ama     Initial checkin
===========================================================================*/

#ifndef __LTE_ML1_COEX_DSDA_LOG_H__
#define __LTE_ML1_COEX_DSDA_LOG_H__



/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_variation.h"
#include <lte_ml1_comdef.h>
#ifdef FEATURE_LTE_ML1_COEX_DSDA

#include <lte_log_codes.h>
#include <log.h>
#include <mcs_timer.h>
#include <lte_ml1_coex_dsda_msg.h>
#include <lte_ml1_mem.h>

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

typedef enum
{
  FREQ_SCAN_RETRY = 0,
  INTER_SEACRH_RETRY,
  INTRA_SEARCH_RETRY,
  EXTEN_ON_DURATION, 
  INIT_ACQ_RETRY
} lte_ml1_coex_dsda_mod_event_e;

typedef struct
{
  /*First word */
  uint32 sfn_subfn:16;
  uint32 event:16;
} lte_ml1_coex_dsda_log_mod_event_s;

/*! @brief Coex Power limit info request */
typedef struct
{
  uint8     version;
  uint8     priority;
  uint8     num_dl_carriers;
  uint8     ul_dl_enable_disable;
  uint8     ul_freq_id;
  uint8     dl_freq_id[LTE_LL1_DL_NUM_CARRIERS];
  uint8     ul_conflict_handling_mode;
  uint8     dl_conflict_handling_mode[LTE_LL1_DL_NUM_CARRIERS];
}lte_ml1_coex_dsda_log_conflict_check_msg_s;

/*===========================================================================

                     FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

FUNCTION LTE_ML1_COEX_LOG_IND

===========================================================================*/
void lte_ml1_coex_dsda_conflict_check_msg_log
(
  lte_LL1_sys_conflict_check_req_struct conflict_check_req
);

/*===========================================================================

FUNCTION LTE_ML1_COEX_LOG_IND

===========================================================================*/
void lte_ml1_coex_dsda_freqid_map_log
(
  coex_freqid_list_s *freqid_list
);

void lte_ml1_coex_dsda_module_event_log
(
 lte_ml1_coex_dsda_mod_event_e event
);

#endif /*FEATURE_LTE_ML1_COEX_DSDA*/
#endif /* LTE_ML1_COEX_LOG_H */



