/*===========================================================================

  Copyright (c) 2007 - 2010 QUALCOMM Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/lte/ML1/gm/main/latest/src/lte_ml1_coex_stm.stm#1 $

when         who     what, where, why
--------   ---     ----------------------------------------------------------
03/21/11   ama       Initial Checkin
===========================================================================*/

/*===========================================================================

                      STATE MACHINE DESCRIPTION

TODO Fill me in

===========================================================================*/
STATE_MACHINE LTE_ML1_COEX_DSDA_SM
{
  ENTRY                                                  lte_ml1_coex_dsda_sm_entry;
  EXIT                                                   lte_ml1_coex_dsda_sm_exit;
  INSTANCES                                              1;
  INPUT_DEF_FILE                                         lte_ml1_coex_dsda_msg.h;  
  INPUT_DEF_FILE                                         lte_ml1_coex_dsda_stm.h;
  ERROR_HOOK                                             lte_ml1_coex_dsda_sm_error_handler;
  DEBUG_HOOK                                             lte_ml1_coex_dsda_sm_debug_handler;
  INITIAL_STATE                                          COEX_DSDA_SINGLE_ACT;

  STATE COEX_DSDA_SINGLE_ACT /* WWAN_COEX_SINGLE_ACT */
  {
    ENTRY                                                lte_ml1_coex_dsda_single_act_state_entry;
    EXIT                                                 lte_ml1_coex_dsda_single_act_state_exit;

    INPUTS
    {
      LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD               lte_ml1_coex_dsda_stm_dsda_enter_proc;
      LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD                lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD               lte_ml1_coex_dsda_nop;              
      LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD                lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_DL_ON_CMD                    lte_ml1_coex_dsda_nop; 
      LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD                   lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_FREQID_CMD                   lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD                lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD                  lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD           lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD                 lte_ml1_coex_dsda_stm_single_mcs_prio_proc;
      LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD                lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD                lte_ml1_coex_dsda_stm_blacklist_proc;
      LTE_ML1_COEX_DSDA_STM_BA_TO_CMD                    lte_ml1_coex_dsda_nop;
    }
  }

  STATE COEX_DSDA_CONFLICT 
  {
    ENTRY                                                 lte_ml1_dsda_conflict_state_entry;
    EXIT                                                  lte_ml1_dsda_conflict_state_exit;

    INPUTS
    {
      LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD               lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD                lte_ml1_coex_dsda_stm_dsda_exit_proc;
      LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD               lte_ml1_coex_dsda_stm_conn_enter_proc;
      LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD                lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_DL_ON_CMD                    lte_ml1_coex_dsda_stm_dl_on_proc; 
      LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD                   lte_ml1_coex_dsda_stm_dl_off_proc;      
      LTE_ML1_COEX_DSDA_STM_FREQID_CMD                   lte_ml1_coex_dsda_stm_freqid_proc;
      LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD                lte_ml1_coex_dsda_stm_arb_proc;
      LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD                  lte_ml1_coex_dsda_stm_prio_sw_proc;
      LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD           lte_ml1_coex_dsda_stm_cancel_prio_sw_proc;
      LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD                 lte_ml1_coex_dsda_stm_confl_mcs_prio_proc;
      LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD                lte_ml1_coex_dsda_stm_wwcoex_state_proc;
      LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD                lte_ml1_coex_dsda_stm_blacklist_proc;
      LTE_ML1_COEX_DSDA_STM_BA_TO_CMD                    lte_ml1_coex_dsda_stm_ba_timeout_proc;
    }
  }
  
  STATE COEX_DSDA_CONFLICT_CONN
  {
    ENTRY                                                 lte_ml1_coex_dsda_conflict_conn_state_entry;
    EXIT                                                  lte_ml1_coex_dsda_conflict_conn_state_exit;

    INPUTS
    {
      LTE_ML1_COEX_DSDA_STM_DSDA_ENTER_CMD               lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_DSDA_EXIT_CMD                lte_ml1_coex_dsda_stm_dsda_exit_proc;
      LTE_ML1_COEX_DSDA_STM_CONN_ENTER_CMD               lte_ml1_coex_dsda_nop;
      LTE_ML1_COEX_DSDA_STM_CONN_EXIT_CMD                lte_ml1_coex_dsda_stm_conn_exit_proc;
      LTE_ML1_COEX_DSDA_STM_DL_ON_CMD                    lte_ml1_coex_dsda_stm_dl_on_proc; 
      LTE_ML1_COEX_DSDA_STM_DL_OFF_CMD                   lte_ml1_coex_dsda_stm_dl_off_proc;          
      LTE_ML1_COEX_DSDA_STM_FREQID_CMD                   lte_ml1_coex_dsda_stm_freqid_proc;
      LTE_ML1_COEX_DSDA_STM_ARBITRATE_CMD                lte_ml1_coex_dsda_stm_arb_proc;
      LTE_ML1_COEX_DSDA_STM_PRIO_SW_CMD                  lte_ml1_coex_dsda_stm_prio_sw_proc;
      LTE_ML1_COEX_DSDA_STM_CANCEL_PRIO_SW_CMD           lte_ml1_coex_dsda_stm_cancel_prio_sw_proc;
      LTE_ML1_COEX_DSDA_STM_MCS_PRIO_CMD                 lte_ml1_coex_dsda_stm_confl_mcs_prio_proc; 
      LTE_ML1_COEX_DSDA_STM_WWCOEX_ST_CMD                lte_ml1_coex_dsda_stm_wwcoex_state_proc;
      LTE_ML1_COEX_DSDA_STM_BLACKLIST_CMD                lte_ml1_coex_dsda_stm_blacklist_proc;
      LTE_ML1_COEX_DSDA_STM_BA_TO_CMD                    lte_ml1_coex_dsda_stm_ba_timeout_proc;
    }
  }

 }

