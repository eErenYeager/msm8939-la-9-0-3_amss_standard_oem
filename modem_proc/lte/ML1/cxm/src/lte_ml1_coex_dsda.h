/*!
  @file lte_ml1_coex_dsda.h

  @brief
  Core header files for coex dsda

*/

/*===========================================================================

  Copyright (c) 2008 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/cxm/src/lte_ml1_coex_dsda.h#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------

===========================================================================*/

#ifndef __LTE_ML1_COEX_DSDA_H__
#define __LTE_ML1_COEX_DSDA_H__
#include "lte_variation.h"
#include <lte_ml1_comdef.h>
#ifdef FEATURE_LTE_ML1_COEX_DSDA
#include <semaphore.h>
#include <lte_ml1_coex_msg.h>
#include <wwan_coex_mgr.h>
#include <lte_ml1_coex_dsda_msg.h>
#include <lte_ml1_coex.h>
#include <sys.h>


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


/*===========================================================================

                    DEFINITIONS AND TYPES

===========================================================================*/
/*! Number of request we can handle */
#define    LTE_ML1_COEX_DSDA_PRIO_SWITCH_REQ_NUM  50

/*! Number of prio raise to send to fw */
#define    LTE_ML1_COEX_DSDA_PRIO_SW_FW_REQ_NUM   1

/*! Number of carrier supported by COEX */
#define    LTE_ML1_COEX_DSDA_DL_CARRIER_NUM       1

/*! Shift 1 ms early to allow FW some time to process request */
#define    LTE_ML1_COEX_DSDA_PRIO_SWITCH_ACT_TIME_SHIFT 1

/* ! Invalid priority */
#define LTE_ML1_COEX_DSDA_PRIO_INVALID            (0xFFFFFFFF)

/* ! Current tuned earfcn used for raise priority request. */
#define LTE_ML1_COEX_DSDA_CURRENT_TUNED_EARFCN    65535

/* ! Action time of conflict check req. */
#define LTE_ML1_COEX_DSDA_CONFLICT_ASAP           65535

/*! @brief Total size (in bytes) of the layer pool; passes in the number of
    layers and the size of each layer struct to a PLIST macro.
*/
#define LTE_ML1_COEX_DSDA_SIZEOF_REQ_POOL PLIST_SIZE(LTE_ML1_COEX_DSDA_PRIO_SWITCH_REQ_NUM, sizeof(lte_ml1_coex_dsda_prio_switch_event_s))

/*! Flags for UL/DL conflict status */
#define LTE_ML1_COEX_DSDA_CONFL_NONE   0x00
#define LTE_ML1_COEX_DSDA_CONFL_UL     0x01
#define LTE_ML1_COEX_DSDA_CONFL_DL     0x02
#define LTE_ML1_COEX_DSDA_CONFL_ULDL   (LTE_ML1_COEX_DSDA_CONFLICT_UL|LTE_ML1_COEX_DSDA_CONFLICT_DL)

/* Max number of debug buffer */
#define LTE_ML1_COEX_DSDA_DEBUG_SIZ   10

/*! Coex power report */
#define LTE_ML1_COEX_DSDA_PWR_RPT_PERIOD          2000 /*ms*/
#define LTE_ML1_COEX_DSDA_PWR_RPT_TX_PERCENTILE   10 /*%*/
#define LTE_ML1_COEX_DSDA_PWR_RPT_RX_PERCENTILE   10 /*%*/
#define LTE_ML1_COEX_DSDA_PWR_RPT_TX_CNT         (LTE_ML1_COEX_DSDA_PWR_RPT_PERIOD/LTE_ML1_COEX_POWER_IND_REPORT_PERIOD)
#define LTE_ML1_COEX_DSDA_PWR_RPT_RX_CNT         (LTE_ML1_COEX_DSDA_PWR_RPT_PERIOD/LTE_ML1_COEX_POWER_IND_REPORT_PERIOD)
#define LTE_ML1_COEX_DSDA_PWR_RPT_TX_HIST        ((LTE_ML1_COEX_DSDA_PWR_RPT_TX_CNT*LTE_ML1_COEX_DSDA_PWR_RPT_TX_PERCENTILE)/100)
#define LTE_ML1_COEX_DSDA_PWR_RPT_RX_HIST        ((LTE_ML1_COEX_DSDA_PWR_RPT_RX_CNT*LTE_ML1_COEX_DSDA_PWR_RPT_RX_PERCENTILE)/100)

/*! Band Avoidance Timeout */
#define LTE_ML1_COEX_DSDA_BA_TIMEOUT                       10 /* sec */

/*! Min threshold of blacklist */
#define LTE_ML1_COEX_DSDA_PWR_MIN        (-32768)     

/*! Max threshold of blacklist */
#define LTE_ML1_COEX_DSDA_PWR_MAX        (32767)   

#define LTE_ML1_COEX_DSDA_MAX_THRESH   LTE_ML1_COEX_DSDA_PWR_MAX

/*==========================================================================
  Conflict status from MCS
===========================================================================*/
typedef uint8 lte_ml1_coex_dsda_conflict_mask_t;

/*==========================================================================
  Priority switch request from other module
===========================================================================*/
typedef struct
{
  /*!What activity wants to raise priority */
  lte_ml1_coex_dsda_actitiy_e           activity;
  /*! On which dl earfcn */
  lte_earfcn_t                          dl_earfcn;
  /*! When priroity should be raised */
  lte_ml1_schdlr_sf_time_t              action_time;
  /*! For how long priority is raised */
  lte_ml1_schdlr_sf_time_t              duration;
} lte_ml1_coex_dsda_prio_switch_event_s;

/*==========================================================================
  Coex DSDA conflict check database
===========================================================================*/
typedef struct 
{
  /*! Priority to send down to FW */
  lte_ml1_coex_dsda_prio_t priority;
  /*! UL conflict check is enabled or not */
  boolean ul_enable;
  /*! DL conflict check is enabled or not */
  boolean dl_enable[LTE_LL1_DL_NUM_CARRIERS];
  /*! UL freqency id from MCS */
  uint32 ul_freq_id;
  /*! DL freqency id from MCS */
  uint32 dl_freq_id[LTE_LL1_DL_NUM_CARRIERS];
  /*! Number of DL carriers */
  uint32 num_dl_carriers;
  /*! Action time of FW to process it.*/
  lte_ml1_schdlr_sf_time_t action_time;
} lte_ml1_coex_dsda_conflict_db_s;

/*==========================================================================
  Coex DSDA priority database for LTE activities
===========================================================================*/
typedef struct
{
  /*! At what sclk we update it */
  uint32 update_sclk;
  /*! How many tiers for LTE */
  lte_ml1_coex_dsda_prio_num_t   num_tiers;
  /*! Entries for each priority */
  lte_ml1_coex_dsda_prio_entry_s tier_prio_tbl_entry[LTE_ML1_COEX_ACTIVITY_NUM];  
} lte_ml1_coex_dsda_act_prio_db_s;

/*==========================================================================
  Buffer to save messages sent to fw
===========================================================================*/
typedef struct 
{
  lte_ml1_schdlr_sf_time_t sent_time;
  lte_LL1_sys_conflict_check_req_struct req;
}conflict_check_dbg_entry_s;

typedef struct
{
  lte_ml1_schdlr_sf_time_t sent_time;
  boolean                  asap;
  lte_LL1_sys_multi_rat_change_cxm_params_req_struct req; 
}prio_sw_dbg_entry_s;

typedef struct
{
  uint8 idx;
  conflict_check_dbg_entry_s entry[LTE_ML1_COEX_DSDA_DEBUG_SIZ];
}conflict_check_dbg_s;

typedef struct
{
  uint8 idx;
  prio_sw_dbg_entry_s        entry[LTE_ML1_COEX_DSDA_DEBUG_SIZ];
}prio_sw_dbg_s;

typedef struct 
{
  uint32 sem_lock_cnt;
  uint32 sem_unlock_cnt;
  conflict_check_dbg_s   conflict_dbg;
  prio_sw_dbg_s          prio_sw_dbg;
} lte_ml1_coex_dsda_debug_buffer_s;

typedef struct 
{
  lte_ml1_schdlr_sf_time_t action_time;
  boolean direct_send;
} lte_ml1_coex_dsda_dl_conflict_on_off_s;

/*==========================================================================
  Priority raising request database
===========================================================================*/
typedef struct 
{
  /*! Pool for storing requests */
  uint8                                 prio_event_pool[LTE_ML1_COEX_DSDA_SIZEOF_REQ_POOL];
  /*! Handle of list */
  lte_ml1_plist_hdl_type                prio_event_list;
  /*! Current DL earfcn */
  lte_earfcn_t                          dl_earfcn;
  /*! Last time we raise until when */
  lte_ml1_schdlr_sf_time_t              raise_until;
  /*! Last asap req action time */
  lte_ml1_schdlr_sf_time_t              asap_req_act_time;
  /*! Last arb req action time */
  lte_ml1_schdlr_sf_time_t              arb_req_act_time;
  /*! Buffer to send out to FW*/
  lte_LL1_sys_multi_rat_change_cxm_params_req_struct asap_req;
  /*! Buffer to send out to FW*/
  lte_LL1_sys_multi_rat_change_cxm_params_req_struct arb_req;  
  /*! Buffer for sleep takeover */
  lte_LL1_sys_multi_rat_change_cxm_params_req_struct sleep_takeover_req;
}lte_ml1_coex_dsda_prio_switch_db_s;

typedef struct
{
  /* Semaphore */
  sem_t                                 sem;
  /* Conflict State */
  lte_ml1_coex_dsda_conflict_mask_t     state;
  /* Conflict callback */
  conflict_st_notify_cb_t               conflict_st_dl_cb;
}lte_ml1_coex_conflict_st_s;

/*==========================================================================
  Coex DSDA band avoidance
===========================================================================*/
typedef int16   pwr_t;

typedef enum 
{
   LTE_ML1_COEX_DSDA_SERV_FREQ_HO    = 1,
   LTE_ML1_COEX_DSDA_SERV_FREQ_CONN_ENTER   = 2,
   LTE_ML1_COEX_DSDA_SERV_FREQ_CONN_EXIT    = 3 
} lte_ml1_coex_dsda_serv_freq_update_reason_e;

typedef struct
{
  /*! serv freq information */
  coex_ba_serv_freq_list_s         serv_freq_info;
  /*! history of sampled power */
  pwr_t                         pwr_hist[LTE_ML1_COEX_DSDA_PWR_RPT_RX_HIST];
  /*! history of sampled power */
  pwr_t                         pwr_avg;  
} lte_ml1_coex_dsda_serv_freq_data_s;

typedef struct
{
  /*! # of serv cell freq */
  uint16                             num_entry;
  /*! Data of serv cell freq (including pwr) */
  lte_ml1_coex_dsda_serv_freq_data_s serv_freq_entry[CXM_MAX_SUPP_FREQ_LINK_BA];
} lte_ml1_coex_dsda_serv_freq_s;

typedef struct
{
  /*! power sample count */
  uint16                               pwr_sample_cnt;
  /*! Serving cell freq */
  lte_ml1_coex_dsda_serv_freq_s        serv_freq;
  /*! Blacklist */
  coex_ba_blist_data                   blacklist;
  /*! BA Resp Timeout */
  boolean                              ba_timer_on;
  /*! BA timer inited */
  uint32                               ba_timer_init;
  /*! BA timer cb func */
  mcs_timer_t1_cb_type                 ba_timer_cb;
  /*! BA Resp Timeout timer */
  mcs_timer_type                       ba_timer;
  /*! Response valid */
  boolean                              ba_resp_valid;
  /*! Responese */
  coex_ba_blist_res_data               ba_resp;
}lte_ml1_coex_dsda_ba_db_s;

/*==========================================================================
  Coex DSDA conflict enum
===========================================================================*/
typedef enum
{
  /*! No conflict need for this device/ue*/
  LTE_ML1_COEX_DSDA_NO_CONFLICT_NEEDED = 0,
  /*! TX_backoff is needed. It's SGLTE or SGLTE+G*/
  LTE_ML1_COEX_DSDA_TX_BACKOFF_NEEDED,
  /*! Blanking is needed. It's NON SGLTE or SGLTE+G with DSDA*/
  LTE_ML1_COEX_DSDA_BLANKING_NEEDED,
  /*! Enable conflict check, and only allow ASDIV to switch priority*/
  LTE_ML1_COEX_DSDA_ASDIV_NEEDED
}lte_ml1_coex_dsda_conflict_type_e;

/*==========================================================================
  Coex DSDA main database
===========================================================================*/
typedef struct
{
  /*! if we are in connected mode */
  boolean                               is_connected;
  /*! device mode */
  sys_modem_device_mode_e_type          dev_mode;
  /*! UE mode type */
  lte_cphy_ue_mode_type_e               ue_mode;
  /*! UE standby_mode type */
  sys_modem_dual_standby_pref_e_type    standby_mode;
  /*! Whether we need blanking/tx_backoff/no conflict*/
  lte_ml1_coex_dsda_conflict_type_e     conflict_type;
  /*! Start RX */
  boolean                               dl_on;  
  /*! Start asdiv_on */
  boolean                               asdiv_on;
  /*! next PO */
  lte_ml1_schdlr_sf_time_t              next_po;
  /*! We are in X2L */
  boolean                               x2l;  
  /*! Offline wakeup */
  boolean                               offline_wakeup_enable;
  /*! Conflict status */
  lte_ml1_coex_conflict_st_s            conflict_st;
  /*! Database for LTE priority from MCS */
  lte_ml1_coex_dsda_act_prio_db_s       prio_db;
  /*! Database for priority switch reqs */
  lte_ml1_coex_dsda_prio_switch_db_s    prio_sw_db;
  /*! Database for conflict check */
  lte_ml1_coex_dsda_conflict_db_s       conflict_db;
  /*! Band Avoidance */
  lte_ml1_coex_dsda_ba_db_s             ba_db;
  /*! Debug buffer */
  lte_ml1_coex_dsda_debug_buffer_s      debug_buffer;
} lte_ml1_coex_dsda_db_s;


/*===========================================================================

                     FUNCTION PROTOTYPES

===========================================================================*/
void lte_ml1_coex_dsda_deinit( lte_mem_instance_type instance );
boolean lte_ml1_coex_dsda_conflict_check(lte_mem_instance_type, boolean, lte_ml1_schdlr_sf_time_t);
void lte_ml1_coex_dsda_db_update_conflict_st(lte_mem_instance_type, cxm_wwcoex_state_update_s *);
void lte_ml1_coex_dsda_db_update_conflict_st(lte_mem_instance_type,cxm_wwcoex_state_update_s *);
lte_ml1_coex_dsda_db_s *lte_ml1_coex_dsda_db_init(lte_mem_instance_type);
void lte_ml1_coex_dsda_process_msg(lte_mem_instance_type ,msgr_umid_type, uint8*); 
boolean lte_ml1_coex_dsda_db_get_dl_on(lte_mem_instance_type);
boolean lte_ml1_coex_dsda_db_is_conn(lte_mem_instance_type );
void lte_ml1_coex_dsda_stm_process_coex_event(lte_mem_instance_type, lte_ml1_coex_int_event_e);
void lte_ml1_coex_dsda_db_set_default(lte_mem_instance_type);
void lte_ml1_coex_dsda_event_list_print_all(lte_mem_instance_type);
void lte_ml1_coex_dsda_clear_conflict_info(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_cxm_prio_tbl_cb(cxm_tech_type tech_id, cxm_activity_table_s activity_tbl[MAX_ACTIVITY_TIERS]);
lte_ml1_coex_dsda_prio_switch_event_s *lte_ml1_coex_dsda_event_list_get_head(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_handle_schdlr_deact(lte_mem_instance_type instance);
lte_ml1_coex_dsda_conflict_type_e lte_ml1_coex_dsda_db_get_conflict_type(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_sleep_takeover_prio_switch_req( lte_mem_instance_type    instance, lte_LL1_change_cxm_params_struct   *req_p);
void lte_ml1_coex_dsda_set_x2l(lte_mem_instance_type instance, boolean flag);
boolean lte_ml1_coex_dsda_get_x2l(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_enter_qta(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_exit_qta(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_db_update_ue_mode(lte_mem_instance_type instance, lte_cphy_ue_mode_type_e ue_mode);
void lte_ml1_coex_dsda_db_update_standby_mode(lte_mem_instance_type instance, sys_modem_dual_standby_pref_e_type  standby_mode);
lte_cphy_ue_mode_type_e lte_ml1_coex_dsda_db_get_ue_mode(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_db_update_dev_mode(lte_mem_instance_type instance, sys_modem_device_mode_e_type  dev_mode);
sys_modem_device_mode_e_type lte_ml1_coex_dsda_db_get_dev_mode(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_update_dsda_needed(lte_mem_instance_type instance);
boolean lte_ml1_coex_dsda_blacklist_query(lte_mem_instance_type ,lte_earfcn_t earfcn,int16 *rxpwr_thresh,int16  *txpwr_thresh,  wwan_coex_desense_mode   *desense_mode);
void lte_ml1_coex_dsda_serv_freq_change_notify(lte_mem_instance_type instance,lte_ml1_coex_dsda_serv_freq_update_reason_e reason);
void lte_ml1_coex_dsda_update_dsda_needed(lte_mem_instance_type nstance);
boolean lte_ml1_coex_dsda_conflict_enabled(lte_mem_instance_type nstance);
pwr_t lte_ml1_coex_dsda_pwr_get_tx(lte_mem_instance_type   instance,  lte_earfcn_t  ul_earfcn);
void lte_ml1_coex_dsda_blacklist_res_rsp(lte_mem_instance_type  instance,  wwcoex_fa_res_type   response);
void lte_ml1_coex_dsda_stm_freqid_change(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_sleep_offline_enable(lte_mem_instance_type ,  boolean); 
void lte_ml1_coex_dsda_obj_arb_tick_schedule(  lte_ml1_schdlr_sf_time_t );
void lte_ml1_coex_dsda_activity_prio_tbl_update(lte_mem_instance_type, lte_ml1_coex_dsda_activity_prio_ind_s *);
void lte_ml1_coex_dsda_blacklist_update(lte_mem_instance_type ,cxm_coex_ba_ind_s *);
void lte_ml1_coex_dsda_prio_switch_arbitrate(lte_mem_instance_type,lte_ml1_schdlr_sf_time_t );
void lte_ml1_coex_dsda_stm_dl_on_off(lte_mem_instance_type instance, boolean dl_on, boolean direct_send, lte_ml1_schdlr_sf_time_t action_time);
void lte_ml1_coex_dsda_register_res_mode_change_notify_cb(lte_mem_instance_type);
lte_ml1_dsda_cancel_prio_switch_result_e lte_ml1_coex_dsda_handle_cancel_prio_switch_req(lte_mem_instance_type,  lte_ml1_coex_dsda_prio_handle_t);
void lte_ml1_coex_dsda_disable_conflict_check(lte_mem_instance_type instance);
void lte_ml1_coex_dsda_send_res_mode_change_ind(lte_mem_instance_type instance, lte_LL1_ue_res_mode_e res_mode, lte_ml1_schdlr_sf_time_t action_time);
void lte_ml1_coex_dsda_res_mode_change(lte_mem_instance_type instance,lte_LL1_ue_res_mode_e res_mode, lte_ml1_schdlr_sf_time_t action_time);
void lte_ml1_coex_dsda_asdiv_on_off(lte_mem_instance_type instance, boolean asdiv_on);
/*===========================================================================

FUNCTION: lte_ml1_coex_dsda_pwr_get_tx

===========================================================================*/
/*!
  @brief
  Get TX power statistics

  @return
  None
*/
/*=========================================================================*/
pwr_t lte_ml1_coex_dsda_pwr_get_tx
(
  lte_mem_instance_type            instance,
  lte_earfcn_t                     ul_earfcn
);
/*===========================================================================

 FUNCTION:  lte_ml1_coex_dsda_blacklist_res_rsp

===========================================================================*/
/*!
  @brief
  Query Coex DB to see if a specific earfcn hits blacklist. Optionally filled
  rx/tx power threashold if rxpwr_thresh or txpwr_thresh is a valid pointer

  @return
  TRUE if earfcn hits blacklist, otherwise FALSE
*/
/*=========================================================================*/
void lte_ml1_coex_dsda_blacklist_res_rsp
(
  lte_mem_instance_type     instance,
  wwcoex_fa_res_type        response
);
#endif /* FEATURE_LTE_ML1_COEX_DSDA */
#endif /* __LTE_ML1_COEX_DSDA_H__ */
