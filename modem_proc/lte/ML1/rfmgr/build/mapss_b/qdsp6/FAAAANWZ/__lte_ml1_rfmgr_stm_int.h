/*=============================================================================

    __lte_ml1_rfmgr_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_rfmgr_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_RFMGR_STM_INT_H
#define __LTE_ML1_RFMGR_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_rfmgr_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_RFMGR_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_RFMGR_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_RFMGR_STM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_ML1_RFMGR_STM_NUM_INPUTS 25

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_rfmgr_stm_enter(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_rfmgr_inactive_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_inactive_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_active_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_active_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_rx_tuned_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_rx_tuned_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_tuning_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_tuning_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_tx_tuned_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_tx_tuned_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_sleep_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_sleep_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_script_exec_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_script_exec_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_script_build_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_script_build_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_suspend_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_rfmgr_suspend_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_rfmgr_start_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_enter_mode_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_handle_tune_script_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_stop_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_inactive_tune_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tune_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_rx_exit_mode_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_fdd_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tdd_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tune_rxlm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_stm_ignore_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_handle_build_script_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_div_cfg_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_div_cfg_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_suspend_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tune_txlm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_fdd_tx_disable_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tdd_tx_disable_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_fdd_tx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_tdd_tx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_wakeup_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_fdd_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_tdd_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_wakeup_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_pend_build_script_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_enter_mode_cnf_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_sleep_stop_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_script_exec_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_script_build_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_run_script_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_resume_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_suspend_tune_proc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_rfmgr_resume_done_proc(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  ACTIVE,
  RX_TUNED,
  TUNING,
  TX_TUNED,
  SLEEP,
  SCRIPT_EXEC,
  SCRIPT_BUILD,
  SUSPEND,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_RFMGR_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_RFMGR_STM_INT_H */
