/*!
  @file 
  lte_ml1_rfmgr_pq.h

  @brief
  The header file contains functions to maniipulate rfmgr pq

  @detail

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/rfmgr/src/lte_ml1_rfmgr_pq.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/26/12   ty      Initial creation

===========================================================================*/

#ifndef LTE_ML1_RFMGR_PQ_H
#define LTE_ML1_RFMGR_PQ_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_variation.h"
#include <lte_ml1_rfmgr_types.h>
#include <lte_ml1_rfmgr_msg.h>
        
#include <string.h>
#include <lte_assert.h>

/*===========================================================================

                   Data types

===========================================================================*/
#define MAX_PQ_MSG                            3


/*! @brief
    Union of msg allowed in pending queue
*/
typedef union
{
  lte_ml1_rfmgr_msg_tune_req_s            tune_req;
  lte_ml1_rfmgr_msg_stop_req_s            stop_req;
  lte_ml1_rfmgr_msg_tune_script_req_s     run_script_req;  
} lte_ml1_rfmgr_pend_msg_u;

/*! @brief
    PQ node 
*/
typedef struct
{
  msgr_umid_type                          umid;         
  lte_ml1_rfmgr_pend_msg_u                msg_payload; 
} lte_ml1_rfmgr_pq_node_s;

/*! @brief
    pending queue
*/
typedef struct 
{ 
  /* Last idx in use */
  uint8                               used_idx;
  /* Current idx being processed */
  uint8                               cur_idx; 
  /* pending message queue */              
  lte_ml1_rfmgr_pq_node_s             msg[MAX_PQ_MSG];
} lte_ml1_rfmgr_pq_s;
/*===========================================================================

                   Functions

===========================================================================*/

/*===========================================================================

                   lte_ml1_rfmgr_pq_add

===========================================================================*/
/*!
    @brief
    Add a msg to pending queue
 
    @return
    void

*/
/*=========================================================================*/
void lte_ml1_rfmgr_pq_add
(
  lte_mem_instance_type       instance,
  msgr_umid_type              umid,
  lte_ml1_rfmgr_pend_msg_u*   msg_ptr     
);

/*===========================================================================

                        lte_ml1_rfmgr_pq_pop

===========================================================================*/
/*!
    @brief
    Pops a msg from PQ

    @return
    Null if nothing left, else msg at current idx

*/
/*=========================================================================*/
lte_ml1_rfmgr_pq_node_s* lte_ml1_rfmgr_pq_pop
(
  lte_mem_instance_type       instance
);

/*===========================================================================

                        lte_ml1_rfmgr_pq_is_empty

===========================================================================*/
/*!
    @brief
    Check if pending queue is empty

    @return
    TRUE if pq is empty, FALSE otherwise

*/
/*=========================================================================*/
boolean lte_ml1_rfmgr_pq_is_empty
(
  lte_mem_instance_type       instance
);

/*===========================================================================

                        lte_ml1_rfmgr_pq_reset

===========================================================================*/
/*!
    @brief
    Reset pq as if it is empty

    @return
    void

*/
/*=========================================================================*/
void lte_ml1_rfmgr_pq_reset
(
  lte_mem_instance_type       instance
);
#endif /* LTE_ML1_RFMGR_PQ_H */
