#ifndef LTE_ML1_SM_IRAT_SCHED_STM_IF_H
#define LTE_ML1_SM_IRAT_SCHED_STM_IF_H
/*!
  @file
  lte_ml1_sm_irat_sched_stm_if.h

  @brief
  This header file contains declarations and definitions needed Search
  Manager to call during the scheduling and descheduling the CDRX off
  duration IRAT measurements.

*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/26/14   vh      Initial Checkin

===========================================================================*/
#include "lte_ml1_mem.h"
#include <msgr_umid.h>

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/


/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/


/*===========================================================================

                         EXTERNAL VARIABLES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

                                FUNCTIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_cancel_schedule

===========================================================================*/
/*!
    @brief Function to cancel the schedule during wakeup.
 
    @detail Function to cancel/deschedule the IRAT envelope object for cdrx off
    measurements during wakeup. IRAT module will send SCHEDULE_CANCEL_CNF when
    everything that is pending, if any, is aborted.
    This will abort the scheduled operations and descehdule the IRAT object
    scheduled for CDRX off IRAT measurements and searches.
    Search module is expected to wait for SCHEDULE_CANCEL_CNF if there is
    some pending IRAT operation scheduled. If there is no IRAT activity
    scheduled this function will return false after descheduling IRAT object.
    This should also be called at the
    time of exiting connnected mode before calling the
    lte_ml1_sm_irat_sched_rat_shutdown which will deinitialize all the
    IRAT submodules
 
 
    @return
    TRUE if any Search Module should wait for SCHEDULE_CANCEL_CNF as IRAT object
    is still not descheduled and deregistered, FALSE if Search should
    not wait for or can ignore SCHEDULE_CANCEL_CNF, since IRAT object is
    descheduled and deregistered.
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_cancel_schedule
(
    lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_build_schedule

===========================================================================*/
/*!
    @brief Function to build the schedule prior to going to sleeep to check if
    any IRAT operations can be scheduled in cdrx off mode.
 
    @detail This function will check if the IRAT search or measurements in the
    cdrx off mode are possible or not. All the IRAT submodules will register the
    schedules if the search or measurement is possible. If the list of registered
    schedule is empty then no IRAT object will be scheduled. If the list is
    non-empty, IRAT Scheduler Sate machine will be initialized and IRAT object
    will be scheduled during CDRX off mode. 
 
    @return TRUE is IRAT Scheduling object will be scheduled as the IRAT activity
    is possible in cdrx off mode, FALSE if IRAT Scheduling object will not be
    scheduled as no IRAT activity possible.
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_build_schedule
(
   lte_mem_instance_type instance,
   uint16 cdrx_wakeup_sf
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_rat_shutdown

===========================================================================*/
/*!
    @brief
    Function to deinitialize all the initialized module at the time of leaving
    connected mode.
 
    @detail This should be called at the time of leaving connected mode,
    all the IRAT submodles will be deinitialized here and everything pending
    will be shutdown. We should also call lte_ml1_sm_irat_sched_cancel_schedule
    before calling this. Cancelling the schedule will abort anything that
    is pending.
 
    @return
    TRUE if Deinit is not pending, FALSE otherwise
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_rat_shutdown
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_schedule_active

===========================================================================*/
/*!
    @brief Function to query if schedule cancel is pending or not.
 
    @detail Function to query if schedule cancel is pending or not.
 
    @return
    TRUE Aborting the schedule in IRAT is pending, FALSE otherwise
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_schedule_active
(
    lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_rat_shutdown

===========================================================================*/
/*!
    @brief Function to query if RATs are aborted/shut or not.
 
    @detail Function to query if RATs are aborted/shut or not.
 
    @return
    TRUE RAT abort/shutdown is pending, FALSE if rat shutdown and
    deinitialization is done and its safe to exit the connected mode
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_rat_shutdown
(
    lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_should_handle_irat_msg

===========================================================================*/
/*!
    @brief
    This function check to see if a particular umid should be handled by
    the IRAT scheduler State Machine.

    @detail

    @return
    TRUE if message is handled by IRAT stm, FALSE otherwise.

*/
/*=========================================================================*/
boolean lte_ml1_sm_should_handle_irat_msg
(
   msgr_umid_type           umid
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_stm_if_get_tune_time

===========================================================================*/
/*!
    @brief
    Function to check the time tuneback is scheduled in the timed
	cleanup for submodule.

    @detail

    @return
    Tune back subfn

*/
/*=========================================================================*/
uint16 lte_ml1_sm_irat_sched_stm_if_get_tune_time
(
	lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_high_prio_schedule_active

===========================================================================*/
/*!
    @brief
    Function to check if the high priority irat scheule is active.
    If this returns true, Gapmgr will skip the next gap.

    @detail

    @return
    TRUE if high prio schedule is active, FALSE otherwise.

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_high_prio_schedule_active
(
	lte_mem_instance_type instance
);
#endif /* LTE_ML1_SM_IRAT_SCHED_STM_IF_H */

