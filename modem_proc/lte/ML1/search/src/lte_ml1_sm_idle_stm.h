/*!
  @file
  lte_ml1_sm_idle_stm.h

  @brief
  This file contains the code for operation of SM idle STM

  @detail
  This file contains the code for operation of SM idle STM.
  This module contains the entry, exit, and transition functions
  necessary to implement the LTE_ML1_SM_IDLE state machine. Only
  one instance of the state machine is created and maintained.


*/

/*===========================================================================

  Copyright (c) 2010 - 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/search/src/lte_ml1_sm_idle_stm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/27/13   jf       Initial version
===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

                        lte_ml1_sm_idle_stm_instance_init

===========================================================================*/
/*!
    @brief
    Initializes dynamically allocated memory.

    @detail
    Initializes dynamically allocated memory.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_idle_stm_instance_init(lte_mem_instance_type instance);

/*===========================================================================

                        lte_ml1_sm_idle_stm_instance_deinit

===========================================================================*/
/*!
    @brief
    Frees dynamically allocated memory.

    @detail
    Frees dynamically allocated memory.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_idle_stm_instance_deinit(lte_mem_instance_type instance);

/*===========================================================================

              lte_ml1_sm_idle_stm_post_activation_process

===========================================================================*/
/*!
    @brief
    Perform necessary actions following Idle opmode activiation.

    @detail
    Perform necessary actions following Idle opmode activiation.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_idle_stm_post_activation_process(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_idle_stm_dump_vars

===========================================================================*/
/*!
    @brief
    Function to print important variables. 

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_idle_stm_dump_vars(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_idle_stm_is_req_from_plmn

===========================================================================*/
/*!
    @brief
    Function to return if an RX CFG was from a BPLMN request

    @return
    boolean
*/
/*=========================================================================*/
boolean lte_ml1_sm_idle_stm_is_req_from_plmn(lte_mem_instance_type instance);

/*===========================================================================

  HELPER FUNCTION:  lte_ml1_sm_idle_stm_is_aborting

===========================================================================*/
/*!
    @brief
    procedure to check if idle is aborting

    @return
    returns true if idle stm is aborting

*/
/*=========================================================================*/
boolean lte_ml1_sm_idle_stm_is_aborting(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_idle_stm_is_curr_state_idle

===========================================================================*/
/*!
    @brief
    Function to check if current state is LTE_ML1_SM_IDLE_IDLE_STATE. 

    @return
    None
*/
/*=========================================================================*/
boolean lte_ml1_sm_idle_stm_is_curr_state_idle(void);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_idle_stm_is_curr_state_dereg

===========================================================================*/
/*!
    @brief
    Function to check if current state is LTE_ML1_SM_IDLE_DEREG_OBJS. 

    @return
    None
*/
/*=========================================================================*/
boolean lte_ml1_sm_idle_stm_is_curr_state_dereg(void);

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_idle_stm_check_next_sm_activity

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_STM,
    state LTE_ML1_SM_IDLE_STATE,
    upon receiving input LTE_ML1_SM_STM_DEACT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_DEACT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
/*! Next layer to search or measure */
lte_ml1_sm_layer_s *
lte_ml1_sm_idle_stm_check_next_sm_activity
(
  lte_mem_instance_type instance,
  /*! Configured requests across which layers have to be checked */
  lte_ml1_sm_idle_req_type_e  req_types
);