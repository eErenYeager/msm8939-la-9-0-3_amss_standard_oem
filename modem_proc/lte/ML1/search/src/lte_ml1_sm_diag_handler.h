/*===========================================================================

  Copyright (c) 2008-2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: 

when       who    what, where, why
--------   ---    ---------------------------------------------------------- 
6/20/2013  cmh    Created file as an interface to receive routed commands from 
                    diag
					
============================================================================*/
	
#ifndef LTE_ML1_DIAG_SEARCH_HANDLER_H
#define LTE_ML1_DIAG_SEARCH_HANDLER_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "lte_ml1_common_diag.h"

/*****************************************************************************
                       INTERNAL DEFINITIONS AND MACROS
*****************************************************************************/

/* enum that holds definition for all search commands, number of bytes for 
 * commands are defined in lte_ml1_diag_search_s as search_cmdcode */
typedef enum
{
  /* LTE_ML1_SM_DIAG_ACK */
  LTE_ML1_SM_DIAG_ACK              = 0x00,  // send_data 71 68 3 64 0  
  /* LTE_ML1_SM_DIAG_SET_EVERY_DRX: 0 sets it to FALSE, otherwise set to TRUE */
  LTE_ML1_SM_DIAG_SET_EVERY_DRX    = 0x01,  // send_data 75 68 3 64 1 []
  /* LTE_ML1_SM_DIAG_SET_ALWAYS_PANIC: 0 sets it to FALSE, otherwise set to TRUE */
  LTE_ML1_SM_DIAG_SET_ALWAYS_PANIC = 0x02,  // send_data 75 68 3 64 2 []
  /* LTE_ML1_SM_DIAG_SET_DISSABLE_OFFLINE_DRX: 0 sets it to FALSE, otherwise TRUE */
  LTE_ML1_SM_DIAG_SET_DISSABLE_OFFLINE_DRX = 0x03, // send_data 75 68 3 64 3 []
  /* LTE_ML1_SM_DIAG_SET_OFFLINE_ALWAYS 0 sets it to FALSE, otherwise TRUE */
  LTE_ML1_SM_DIAG_SET_OFFLINE_ALWAYS = 0x04 // send_data 75 68 3 64 4 []

  /** Add commands that you would like the SM diag handler to
   *  execute here (search_cmdcode)  */

} lte_ml1_sm_diag_cmd_code_e;

/*****************************************************************************
                       EXTERNAL DEFINITIONS AND MACROS
*****************************************************************************/

/*===========================================================================

                             EXTERNAL FUNCTIONS

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_diag_handler

===========================================================================*/
/*!
    @brief
    dispatches command requests from diag to search modules

    @details
    dispatches command requests from diag to search modules
    
    @return
    0 on success, -1 on fail
*/
/*=========================================================================*/
int lte_ml1_sm_diag_handler(PACKED void * data);
	
#endif //LTE_ML1_DIAG_SEARCH_HANDLER_H
