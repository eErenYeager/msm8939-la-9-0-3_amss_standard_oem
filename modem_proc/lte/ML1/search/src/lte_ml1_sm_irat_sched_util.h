#ifndef LTE_ML1_SM_IRAT_SCHED_UTIL_H
#define LTE_ML1_SM_IRAT_SCHED_UTIL_H
/*!
  @file
  lte_ml1_sm_irat_sched_util.h

  @brief
  This header file contains declarations and definitions needed Utility
  functions to be called by scheduler state machine and in APIs for
  search module and irat submodules.

*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/14   vh      Initial Checkin

===========================================================================*/


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_ml1_sm_irat_sched_stm_if.h"
#include "lte_ml1_sm_irat_sched_stm.h"
#include "lte_ml1_mem.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/
#define IS_RES_AVAILABLE(active_bitmask, resource) (!(active_bitmask & resource))

/*===========================================================================

                         EXTERNAL VARIABLES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_is_schedule_empty

===========================================================================*/
/*!
    @brief
    Function to to check if the Schedule list is empty or not.

    @detail
    Function to to check if the Schedule list is empty or not.

    @return
    TRUE if schedule is empty, FALSE otherwise
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_util_is_schedule_empty
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_insert_schedule

===========================================================================*/
/*!
    @brief
    Function to Add schedule to the schedule list.

    @detail
    Function to Add schedule to the schedule list.

    @return
    None

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_util_insert_schedule
(
   lte_mem_instance_type instance,
   lte_ml1_sm_irat_sched_s *schedule,
   boolean add_to_top
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_get_next_schedule

===========================================================================*/
/*!
    @brief
    Function to Get the next schedule from the schedule list.

    @detail
    Function to Get the next schedule from the schedule list.

    @return
    TRUE if the schedule is scheduled, FALSE otherwise

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_util_get_next_schedule
(
   lte_mem_instance_type instance,
   active_bitmask_t active_res_bimask,
   lte_ml1_schdlr_sf_time_t time_available,
   lte_ml1_sm_irat_sched_s *schedule
);
/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_clear_schedule_list

===========================================================================*/
/*!
    @brief
    Function to Clear the schedule list.

    @detail
    Function to Clear the schedule list.

    @return
    None

*/
/*=========================================================================*/
void 
lte_ml1_sm_irat_sched_util_clear_schedule_list
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_get_min_time

===========================================================================*/
/*!
    @brief
    Function to get the minimum time needed for a schedule from the
    schedule list.

    @detail
    Function to get the minimum time needed for a schedule from the
    schedule list.

    @return
    uin16

*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t 
lte_ml1_sm_irat_sched_util_get_min_time
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_get_time_available
 
===========================================================================*/
/*!
    @brief
    Function to get the time available for IRAT object.

    @detail
    Function to get the time available for IRAT object. This will return
    the schdlr time available - time needed for DL enable and disable.
    
    @return
    Time Available for IRAT object
*/
/*=========================================================================*/

lte_ml1_schdlr_sf_time_t lte_ml1_sm_irat_sched_get_time_available
(
   lte_mem_instance_type instance,
   lte_ml1_schdlr_sf_time_t time_to_add
);


/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_alloc

===========================================================================*/
/*!
    @brief
    This function allocates memory for conn meas schedule lis struct

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_util_alloc
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_util_dealloc

===========================================================================*/
/*!
    @brief
    This function releases memory for conn meas schedule lis struct

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_util_dealloc
(
   lte_mem_instance_type instance
);
#endif
