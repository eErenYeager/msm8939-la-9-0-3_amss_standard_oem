/*===========================================================================

  Copyright (c) 2009-2010 QUALCOMM Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/14   vh      Initial Checkin

===========================================================================*/
#include "custglobal.h"
#include "lte_variation.h"
#include <customer.h>
#include <custlte.h>
/*===========================================================================

                      STATE MACHINE DESCRIPTION

This is the IRAT Scheduler state machine definition.

===========================================================================*/

STATE_MACHINE LTE_ML1_SM_IRAT_SCHED_STM
{
  ENTRY                                 lte_ml1_sm_irat_sched_stm_entry;
  EXIT                                  lte_ml1_sm_irat_sched_stm_exit;
  INITIAL_STATE                         LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE;
  INSTANCES                             LTE_MEM_MAX_INST;
  ERROR_HOOK                            lte_ml1_sm_stm_error_handler;
  DEBUG_HOOK                            lte_ml1_sm_stm_debug_handler;
  INPUT_DEF_FILE                        lte_ml1_sm_stm_common.h;
  INPUT_DEF_FILE                        lte_ml1_comdef.h;
  INPUT_DEF_FILE                        intf_sys_msg.h;
  INPUT_DEF_FILE                        lte_ml1_dlm_msg.h;
  INPUT_DEF_FILE                        lte_ml1_sm_msg.h;
  INPUT_DEF_FILE                        custlte.h;

  /*===========================================================================

    State: LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE

  ===========================================================================*/
  STATE LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_sch_wait_entry;        
    EXIT                                NULL;

    INPUTS 
    {
      LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ     lte_ml1_sm_irat_sched_stm_start_object;
      LTE_ML1_SM_IRAT_SCHED_ABORT_REQ     lte_ml1_sm_irat_sched_stm_handle_abort;      
      LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ           lte_ml1_sm_irat_schedule_handle_offline_start_obj;              
    }

  }

  STATE LTE_ML1_SM_IRAT_SCHED_STM_DL_DISABLE_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_dl_disable_entry;
    EXIT                                NULL;

    INPUTS
    {
      LTE_ML1_SM_RX_CFG_CNF               lte_ml1_sm_irat_sched_stm_dl_disable_rx_cfg_rsp;
      LTE_ML1_SM_IRAT_SCHED_ABORT_REQ     lte_ml1_sm_irat_sched_stm_mark_abort_triggered;      
    }
  }

  /*===========================================================================

    State: LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE

  ===========================================================================*/
  STATE LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_meas_entry;
    EXIT                                NULL;

    INPUTS
    {
      LTE_ML1_SM_IRAT_SCHED_ABORT_REQ     lte_ml1_sm_irat_sched_stm_handle_abort;
      LTE_ML1_SM_IRAT_SCHED_TICK_IND      lte_ml1_sm_irat_schedule_handle_tick_ind;      
      LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND  lte_ml1_sm_irat_sched_stm_handle_complete_ind;
      LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND  lte_ml1_sm_irat_sched_stm_end_tick_object;  
    }
  }

  /*===========================================================================

    State: LTE_ML1_SM_IRAT_SCHED_STM_TUNE_BACK_STATE

  ===========================================================================*/

  STATE LTE_ML1_SM_IRAT_SCHED_STM_TUNE_BACK_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_tuneback_state_entry;
    EXIT                                NULL;

    INPUTS
    {
      LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP         lte_ml1_sm_irat_sched_stm_handle_rf_script_cnf;
      LTE_ML1_SM_IRAT_SCHED_ABORT_REQ           lte_ml1_sm_irat_sched_stm_mark_abort_triggered;
      LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND    lte_ml1_sm_irat_sched_stm_end_tick_object;
      LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND        lte_ml1_sm_irat_sched_stm_handle_complete_ind;
      LTE_ML1_SM_IRAT_SCHED_TICK_IND            lte_ml1_sm_irat_schedule_handle_tick_ind;
    }
  }
        
  /*===========================================================================

    State: LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE

  ===========================================================================*/

  STATE LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_dl_enable_entry;
    EXIT                                NULL;

    INPUTS
    {
      LTE_ML1_SM_RX_CFG_CNF                             lte_ml1_sm_irat_sched_stm_dl_enable_rx_cfg_rsp;
      LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP                 lte_ml1_sm_irat_sched_stm_handle_rf_script_cnf;      
      LTE_ML1_SM_IRAT_SCHED_ABORT_REQ                   lte_ml1_sm_irat_sched_stm_mark_abort_triggered;
      LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND                 lte_ml1_sm_irat_sched_stm_end_object;
      LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ           lte_ml1_sm_irat_schedule_handle_offline_start_obj;     
      LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND                lte_ml1_sm_irat_sched_stm_handle_complete_ind;
      LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND            lte_ml1_sm_irat_sched_stm_end_tick_object; 
      LTE_ML1_SM_IRAT_SCHED_TICK_IND                    lte_ml1_sm_irat_schedule_handle_tick_ind;
    }
  }
  
  /*===========================================================================

    State: LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE

  ===========================================================================*/
  STATE LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_desched_entry;        
    EXIT                                NULL;

    INPUTS 
    {
        LTE_ML1_SM_IRAT_SCHED_ABORT_REQ                lte_ml1_sm_irat_sched_stm_handle_abort;
        LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ            lte_ml1_sm_irat_sched_stm_nop;
        LTE_ML1_SM_IRAT_SCHED_OFFLINE_OBJ_END_IND      lte_ml1_sm_irat_sched_stm_end_offline_object;
        LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND              lte_ml1_sm_irat_sched_stm_end_object;
        LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP              lte_ml1_sm_irat_sched_stm_nop;    
        LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND         lte_ml1_sm_irat_sched_stm_end_tick_object;
        LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ        lte_ml1_sm_irat_schedule_handle_offline_start_obj;      
        LTE_ML1_SM_IRAT_SCHED_TICK_IND                 lte_ml1_sm_irat_schedule_handle_tick_ind;
    }

  }

  STATE LTE_ML1_SM_IRAT_SCHED_STM_OFFLINE_STATE
  {
    ENTRY                               lte_ml1_sm_irat_sched_stm_offline_entry;        
    EXIT                                NULL;

    INPUTS 
    {
        LTE_ML1_SM_IRAT_SCHED_ABORT_REQ                lte_ml1_sm_irat_sched_stm_nop;
        LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND             lte_ml1_sm_irat_sched_stm_handle_complete_ind;
        LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ        lte_ml1_sm_irat_schedule_handle_offline_start_obj; 
        LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND              lte_ml1_sm_irat_sched_stm_end_object;
        LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND         lte_ml1_sm_irat_sched_stm_end_tick_object;
        LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP              lte_ml1_sm_irat_sched_stm_nop;  
        LTE_ML1_SM_IRAT_SCHED_TICK_IND                 lte_ml1_sm_irat_schedule_handle_tick_ind;
    }

  }

}

