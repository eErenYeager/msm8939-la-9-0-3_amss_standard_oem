/*!
  @file
  lte_ml1_sm_suspend_stm.h

  @brief
  This file contains the code for the SUSPEND mode of operation.

*/

/*===========================================================================

  Copyright (c) 2010 - 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/search/src/lte_ml1_sm_suspend_stm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/25/13   cmh      Initial version
===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_suspend_instance_init

===========================================================================*/
/*!
    @brief
    Initializes dynamically allocated memory.
 
    @detail
    Initializes dynamically allocated memory.
 
    @return
    None
*/
/*=========================================================================*/

void lte_ml1_sm_suspend_instance_init(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_suspend_instance_deinit

===========================================================================*/
/*!
    @brief
    Frees dynamically allocated memory.
 
    @detail
    Frees dynamically allocated memory.
 
    @return
    None
*/
/*=========================================================================*/

void lte_ml1_sm_suspend_instance_deinit(lte_mem_instance_type instance);
