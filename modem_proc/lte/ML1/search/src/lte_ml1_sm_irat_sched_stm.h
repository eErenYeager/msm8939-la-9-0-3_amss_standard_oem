#ifndef LTE_ML1_SM_IRAT_SCHED_STM_H
#define LTE_ML1_SM_IRAT_SCHED_STM_H
/*!
  @file
  lte_ml1_sm_irat_sched_stm.h

  @brief
  This header file contains declarations and definitions needed
  each IRAT Submodules and Scheduleer State Machine.

*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/14   vh      Initial Checkin
===========================================================================*/


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_ml1_mem.h"
#include "lte_ml1_sm_gsm_sched.h"
#include "lte_ml1_sm_conn_meas.h"
/*===========================================================================

                    UTILITY MACROS

===========================================================================*/
#define LTE_ML1_SM_IRAT_SCHED_RES_NONE          0x0000        

#define LTE_ML1_SM_IRAT_SCHED_RES_G_OFFLINE     0x0004

#define LTE_ML1_SM_IRAT_SCHED_RES_W_OFFLINE     0x0002

#define LTE_ML1_SM_IRAT_SCHED_RES_ONLINE        0x0001

#define LTE_ML1_SM_IRAT_SCHED_RES_VPE           0x0020

#define LTE_ML1_SM_IRAT_SCHED_RES_Y             0x4000 

#define LTE_ML1_SM_IRAT_SCHED_RES_X             0x8000

#define LTE_ML1_SM_CONN_MEAS_DL_ENABLE          1

#define IRAT_SCHED_IS_RAT_BIT_SET(init_mask, rat_type)            (init_mask & (1 << rat_type))

#define ACQUIRE_RES(active_bitmask, resource) (active_bitmask = active_bitmask | resource)

#define RELEASE_RES(active_bitmask, resource) (active_bitmask = active_bitmask & (~(resource)))

#ifndef FEATURE_LTE_ML1_FED_SUPPORT
#define LTE_ML1_SM_CONN_MEAS_SCHED_TUNEBACK_TIME                12288
#else
/*! Delay APP CONFIG after RXLM by 1ms */
#define LTE_ML1_SM_CONN_MEAS_SCHED_APP_CFG_DELAY                1

#define LTE_ML1_SM_CONN_MEAS_SCHED_TUNEBACK_TIME                10752
#endif


#define LTE_ML1_IRAT_INVALID_TICK_CNT                           0

#define LTE_ML1_SM_IRAT_SCHED_APP_CFG_PROC_TIME                  2

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

struct lte_ml1_sm_gsm_sched_schedule_req_s;
typedef uint32 active_bitmask_t;
typedef uint8  init_bitmask_t;

/*!Union for storing the information needed 
  for schedule for each RAT*/
typedef union
{
    /*GSM*/
    struct lte_ml1_sm_gsm_sched_schedule_req_s *gsm_sched_info;
}sched_info;

/*!Schedule related information needed for 
  Registring the Schedule in the Schedule list*/
typedef struct
{
    /*pointer to the information needed for the schedule*/
    sched_info schedule_info;
    /*minimum time needed for schedule*/
    uint16 min_time_for_schedule;
    /*Resources needed for schedule*/
    active_bitmask_t res_needed;
    /*whether current schedule is high priority */
    boolean is_high_prio_sched;
    /*RAT Type*/
    lte_rat_e rat_type;
} lte_ml1_sm_irat_sched_s;

typedef struct
{
  /*!Flag to indicate if tune should 
  be scheduled at spefic time*/
  boolean scheduled_tune_req;

  /*!Time at which tune 
    should be scheduled*/
  uint64 tune_time;

  /*!Resources needed for the schedule 
    None if schedule is cancelled*/
  active_bitmask_t res_acquired;
} scheduled_tune_info_t;
 
/* Callback to check whether any IRAT activity is possible or not*/
typedef void (*lte_ml1_sm_irat_sched_build_schedule_cb_t)
                                            (
                                             lte_mem_instance_type instance,
                                             lte_ml1_schdlr_sf_time_t drx_length
                                             );

/*Callback for initializing measurement driver for 
respective submodule calling*/
typedef active_bitmask_t  (*lte_ml1_sm_irat_sched_init_cb_t)(
                                            lte_mem_instance_type instance
                                            );

/*Callback for de-initializing measurement driver for 
respective submodule calling*/
typedef active_bitmask_t (*lte_ml1_sm_irat_sched_deinit_cb_t)(
                                            lte_mem_instance_type instance
                                            );

/*Callback for scheduling the registered schedule when the 
resources are available and all the measurement that cannot
 be scheduled in cdrx off are completed, return value is 
the time when submodule can free the online resource*/
typedef scheduled_tune_info_t ( *lte_ml1_sm_irat_sched_schedule_cb_t)(
                                            lte_mem_instance_type 
                                            instance,
                                            lte_ml1_sm_irat_sched_s 
                                            *schedule, 
                                            uint64 
                                            sched_start_time,
                                            uint64
                                            sched_end_time
                                            );

/*Callback aborting the scheduled operation when the search is calling 
cancel schedule during wakeup*/
typedef void ( *lte_ml1_sm_irat_sched_abort_cb_t )
                                            ( 
                                             lte_mem_instance_type instance
                                             );

/*!Callbacks to be registered by each 
  IRAT Submodule during registration with gap mgr*/
typedef struct 
{
    /*call back for checkin if any IRAT activity is possible or not*/
    lte_ml1_sm_irat_sched_build_schedule_cb_t build_schedule_cb;
    /* Callback to trigger init*/ 
    lte_ml1_sm_irat_sched_init_cb_t init_trigger_cb;
    /* Callback to trigger de-init*/ 
    lte_ml1_sm_irat_sched_deinit_cb_t deinit_trigger_cb;
    /*callback to schedule*/
    lte_ml1_sm_irat_sched_schedule_cb_t schedule_cb;
    /*Call back for aborting*/
    lte_ml1_sm_irat_sched_abort_cb_t abort_cb;
} lte_ml1_sm_irat_sched_cb;

/*! Callbacks for all RATs*/
typedef struct
{
    lte_ml1_sm_irat_sched_cb cb[LTE_MAX_NUM_RATS];
} lte_ml1_sm_irat_sched_rat_reg_cb_t;

/*! @brief: Conn Meas Sched IRAT SM STM payload */
typedef struct
{
  boolean   is_finished;
  void    * msg;
} lte_ml1_sm_irat_sched_stm_payload_s;

/*===========================================================================

                         EXTERNAL VARIABLES

===========================================================================*/

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/
/*===========================================================================

                    Scheduler State Machine Operations Functions

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_exec_stm_if

===========================================================================*/
/*!
    @brief
    This function executes the active STM state machine

    @detail

    @return
    Return value passed back by STM handler function.

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_exec_stm_if
(
  lte_mem_instance_type instance,
  /*! STM input to send */
  stm_input_t  sm_stm_input,
  /*! STM message payload */
  void        *msg
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_idle_irat_if_is_active

===========================================================================*/
/*!
    @brief
    Function to return whether the Conn Meas Sched state machine is active

    @detail
    Function to return whether the Conn Meas Sched state machine is active

    @return
    TRUE if Conn Meas Sched machine is active, FALSE otherwise

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_if_is_active(void);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_stm_dealloc

===========================================================================*/
/*!
    @brief
    This function releases memory for IRAT SCHED stm globals.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_dealloc
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_stm_alloc

===========================================================================*/
/*!
    @brief
    This function allocates memory for IRAT SCHED stm globals.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_alloc
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_stm_if_dealloc

===========================================================================*/
/*!
    @brief
    This function releases memory for struct in interface for IRAT
    SCHED Stm.
 
    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_if_dealloc
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_stm_if_alloc

===========================================================================*/
/*!
    @brief
    This function allocates memory for struct in interface for IRAT
    SCHED Stm.
 
    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_if_alloc
(
   lte_mem_instance_type instance
);

/*===========================================================================

=============================================================================

          Interfaces for Submodules to Register or Complete the Schedule

=============================================================================

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_register_callbacks

===========================================================================*/
/*!
    @brief
    Function to register the callbacks

    @detail
    Function to register the callbacks. This will be called by each submodule
    while registering with the gap manager.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_register_callbacks
(
 lte_mem_instance_type instance,
 lte_ml1_sm_irat_sched_cb *reg_cb, 
 lte_rat_e rat_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_register_schedule

===========================================================================*/
/*!
    @brief
    Function to Register the schedule in the schedul list.

    @detail
    Function to Register the schedule in the schedul list, Called by
    each submodule for registeringthe operations to the list of
    schedules.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_register_schedule
(
   lte_mem_instance_type instance,
   lte_ml1_sm_irat_sched_s *schedule,
   boolean add_to_top
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_complete_schedule

===========================================================================*/
/*!
    @brief
    Function to remove the current schedule from the list.

    @detail
    Function to remove the current schedule from the list.
    Each submodule will call this function before any other schedule
    from the list of registered schedule is scheduled.

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_complete_schedule
(
   lte_mem_instance_type instance,
   active_bitmask_t sched_res,
   boolean tuneback_needed,
   lte_rat_e rat_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_init

===========================================================================*/
/*!
    @brief
    Function to set the init bitmask.

    @detail
    Function to set the init bitmask. This will be called by each submodule
    when it is initialized. Initialize bit in the Init mask will be set
    for that respective submodule.
    
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_init
(
  lte_mem_instance_type instance,  
  lte_rat_e rat_type,
  active_bitmask_t release_resources
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_init_done

===========================================================================*/
/*===========================================================================*/
/*!
    @brief
    Function to set the init bitmask.

    @detail
    Function to set the init bitmask. This will be called by each submodule
    when it is initialized. Initialize bit in the Init mask will be set for that
    respective submodule.
    
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_init_done
(
  lte_mem_instance_type instance,  
  lte_rat_e rat_type
);
/*===========================================================================

=============================================================================

      Interfaces for Sending Messages to IRAT Scheduler State Machine.

=============================================================================

===========================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_complete_ind

===========================================================================*/
/*!
    @brief
    Function to send Complete Indication to the IRAT sched stm

    @detail
    Function to send Complete Indication to the IRAT sched stm
 
    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_complete_ind
(
   lte_mem_instance_type instance,
   active_bitmask_t res_needed,
   boolean tuneback_needed,
   lte_rat_e rat_type,
   uint8 online_tick_cnt
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_rat_initialized

===========================================================================*/
/*===========================================================================*/
/*!
    @brief
    Function to check if rat is initialized.

    @detail
    Function to check if rat is initialized.
    
    @return
    TRUE of RAT is initialized FALSE otherwise
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_rat_initialized
(
   lte_mem_instance_type instance,
   lte_rat_e rat_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_init_needed

===========================================================================*/
/*===========================================================================*/
/*!
    @brief
    Function to check if Initialization of RAT is needed or RAT is
    registered.

    @detail
    Function to check if Initialization of RAT is needed or RAT is
    registered.
    
    @return
    TRUE of Initialization of RAT is needed, FALSE otherwise
*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_init_needed
(
   lte_mem_instance_type instance,
   lte_rat_e rat_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_build_schedule_for_rat

===========================================================================*/
/*!
    @brief
    Function for deinitializing the submodules 
 
    @detail Function for deinitializing the submodules 
 
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_build_schedule_for_rat
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_deinit_trigger

===========================================================================*/
/*!
    @brief
    Function for deinitializing the submodules 
 
    @detail Function for deinitializing the submodules 
 
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_deinit_trigger
(
   lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_schedule_irat_object

===========================================================================*/
/*!
    @brief
    Function to schedule or deschedule IRAT object
 
    @brief
    Function to schedule or deschedule IRAT object
 
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_schedule_irat_object
(
   lte_mem_instance_type instance,
   /*flag to indicate for schedule or deschedule IRAT object*/
   boolean schedule
);
/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_schedule_irat_offline_object

===========================================================================*/
/*!
    @brief
    Function to schedule or deschedule IRAT offline object
	for offline scheduled operations
 
    @brief
    Function to schedule or deschedule IRAT offline object
	for offline scheduled operations
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_schedule_irat_offline_object
(
  lte_mem_instance_type instance,
  /*! Flag to indicate schedule or de-schedule */
  boolean schedule
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_schedule_irat_tick_object

===========================================================================*/
/*!
    @brief
    Function to schedule or deschedule IRAT tick object
	for offline scheduled operations
 
    @brief
    Function to schedule or deschedule IRAT tick object
	for offline scheduled operations
    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_irat_schedule_irat_tick_object
(
  lte_mem_instance_type instance,
  /*! Time in RTC where the scheduling should occur */
  uint64  online_res_done_time,
  /*! Flag to indicate schedule or de-schedule */
  boolean schedule,
  /*! Counter for ticks scheduled*/
  uint8 online_tick_cnt
);
/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_deactivate_stm

===========================================================================*/
/*!
    @brief
    This function deactivates the state machine

    @detail
    This function deactivates the state machine

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_deactivate_stm(void);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_activate_stm

===========================================================================*/
/*!
    @brief
    This function activates the state machine

    @detail
    This function activates the state machine

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_activate_stm
(
  lte_mem_instance_type instance,
  /*! STM message payload */
  void        *msg
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_irat_sched_is_abort_requested

===========================================================================*/
/*!
    @brief
    This function checks if abort has been requested

    @detail
    This function checks if abort has been requested

    @return
    Whether abort has been requested

*/
/*=========================================================================*/
boolean lte_ml1_sm_irat_sched_is_abort_requested
(
  lte_mem_instance_type instance
);
#endif /* LTE_ML1_SM_IRAT_SCHED_STM_IF_H */
