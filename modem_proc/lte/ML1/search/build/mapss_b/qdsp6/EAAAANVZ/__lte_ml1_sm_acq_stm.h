/*=============================================================================

    __lte_ml1_sm_acq_stm.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_acq_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_ACQ_STM_H
#define __LTE_ML1_SM_ACQ_STM_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include STM framework header */
#include <stm2.h>

/* Begin machine generated code for state machine array: LTE_ML1_SM_ACQ_STM[] */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_ACQ_STM_NUM_INSTANCES 1

/* External reference to state machine structure */
extern stm_state_machine_t LTE_ML1_SM_ACQ_STM[ LTE_ML1_SM_ACQ_STM_NUM_INSTANCES ];

/* External enumeration representing state machine's states */
enum
{
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_INIT_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_RF_TUNE_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_DETECT_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_NBPBCH_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_STAGE2_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_ABORT_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_BAND_SCAN_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_SYSTEM_SCAN_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_CELL_MEAS_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_PLMN_ACQ_STATE,
  LTE_ML1_SM_ACQ_STM__LTE_ML1_SM_ACQ_PLMN_PBCH_STATE,
};

#ifndef STM_DATA_STRUCTURES_ONLY
/* User called 'reset' routine.  Should never be needed, but can be used to
   effect a complete reset of all a given state machine's instances. */
extern void LTE_ML1_SM_ACQ_STM_reset(void);
#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated code for state machine array: LTE_ML1_SM_ACQ_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* __LTE_ML1_SM_ACQ_STM_H */
