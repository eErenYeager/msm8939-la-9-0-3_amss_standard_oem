/*!
  @file
  __lte_ml1_sm_irat_meas_timed_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_SM_IRAT_TIMED_MEAS_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_sm_irat_meas_timed_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_sm_irat_meas_timed_stm_type;


/*! @brief Variables internal to module __lte_ml1_sm_irat_meas_timed_stm.stub
*/
STATIC __lte_ml1_sm_irat_meas_timed_stm_type __lte_ml1_sm_irat_meas_timed_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_SM_IRAT_TIMED_MEAS_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_stm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_stm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_stm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_TIMED_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_meas_timed_starting_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_starting_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_starting_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_sm_irat_meas_timed_starting_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_starting_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_starting_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_starting_app_cfg_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_starting_app_cfg_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_starting_app_cfg_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_stmr_dump_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_LL1_SYS_SYNC_STMR_DUMP_CNF

    @detail
    Called upon receipt of input LTE_LL1_SYS_SYNC_STMR_DUMP_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_stmr_dump_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_stmr_dump_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_slam_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_ML1_SM_TIME_SLAM_DONE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_TIME_SLAM_DONE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_slam_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_slam_done() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_ttrans_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_ML1_SM_TTRANS_DONE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_TTRANS_DONE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_ttrans_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_ttrans_done() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_handle_mcvs_q6_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_ML1_SM_MCVS_Q6_DONE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_MCVS_Q6_DONE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_handle_mcvs_q6_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_handle_mcvs_q6_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_nop

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_LL1_SYS_RECOVERY_IND

    @detail
    Called upon receipt of input LTE_LL1_SYS_RECOVERY_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_stm_nop
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_stm_nop() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_handle_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_handle_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_handle_abort_req() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_TIMED_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_timed_srch_meas_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_timed_srch_meas_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_timed_srch_meas_entry() */


/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_sm_irat_timed_srch_meas_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_timed_srch_meas_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_timed_srch_meas_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_srch_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_SRCH_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_SRCH_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_stm_srch_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_stm_srch_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_meas_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_MEAS_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_MEAS_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_stm_meas_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_stm_meas_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_app_cfg_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_app_cfg_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_app_cfg_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_stm_rf_script_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
    upon receiving input LTE_ML1_SM_RF_TUNE_SCRIPT_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RF_TUNE_SCRIPT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_stm_rf_script_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_stm_rf_script_done() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_handle_recovery_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
    upon receiving input LTE_LL1_SYS_RECOVERY_IND

    @detail
    Called upon receipt of input LTE_LL1_SYS_RECOVERY_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_handle_recovery_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_handle_recovery_ind() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_TIMED_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_TIMED_CLEANUP_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_meas_timed_start_gap_exit

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_CLEANUP_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_start_gap_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_start_gap_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_timed_cleanup_app_cfg_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_TIMED_MEAS_STM,
    state LTE_ML1_SM_IRAT_TIMED_CLEANUP_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_timed_cleanup_app_cfg_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_timed_cleanup_app_cfg_cnf() */




