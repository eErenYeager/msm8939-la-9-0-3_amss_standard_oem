/*=============================================================================

    __lte_ml1_sm_conn_wcdma_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_conn_wcdma_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_CONN_WCDMA_STM_INT_H
#define __LTE_ML1_SM_CONN_WCDMA_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_conn_wcdma_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_CONN_WCDMA_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_CONN_WCDMA_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_CONN_WCDMA_STM_NUM_STATES 12

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_CONN_WCDMA_STM_NUM_INPUTS 14

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_conn_wcdma_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_conn_wcdma_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_conn_wcdma_stm_idle_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_conn_wcdma_stm_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_conn_wcdma_stm_update_shareable_status(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_determine_srch_meas(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_proc_meas_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_send_cleanup(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_clk_on_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_clk_off_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_build_rf_script_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_schedule_srch(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_send_srch_cmd(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_invalid_input_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_proc_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_send_srch(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_send_meas(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_srch_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_proc_rf_tune_back_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_schedule_meas(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_meas_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_proc_meas_rf_tune_back_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_wcdma_stm_handle_cleanup_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_CONN_W_IDLE_STATE,
  LTE_ML1_SM_CONN_W_SRCH_IDLE_STATE,
  LTE_ML1_SM_CONN_W_SRCH_ONLINE_STATE,
  LTE_ML1_SM_CONN_W_SRCH_WAIT_FOR_GAP_TRIGGER,
  LTE_ML1_SM_CONN_W_MEAS_WAIT_FOR_GAP_TRIGGER,
  LTE_ML1_SM_CONN_W_SRCH_RF_TUNE_AWAY_STATE,
  LTE_ML1_SM_CONN_W_SRCH_OFFLINE_STATE,
  LTE_ML1_SM_CONN_W_MEAS_IDLE_STATE,
  LTE_ML1_SM_CONN_W_MEAS_MEAS_STATE,
  LTE_ML1_SM_CONN_W_MEAS_RF_TUNE_AWAY_STATE,
  LTE_ML1_SM_CONN_W_CLEANUP_STATE,
  LTE_ML1_SM_CONN_W_ABORTING_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_CONN_WCDMA_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_CONN_WCDMA_STM_INT_H */
