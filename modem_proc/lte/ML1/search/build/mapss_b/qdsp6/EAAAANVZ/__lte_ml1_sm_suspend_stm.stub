/*!
  @file
  __lte_ml1_sm_suspend_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_SM_SUSPEND_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_sm_suspend_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_sm_suspend_stm_type;


/*! @brief Variables internal to module __lte_ml1_sm_suspend_stm.stub
*/
STATIC __lte_ml1_sm_suspend_stm_type __lte_ml1_sm_suspend_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_SM_SUSPEND_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_sm_suspend_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_SUSPEND_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_suspend_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_suspend_stm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_sm_suspend_stm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_SUSPEND_STM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_suspend_stm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_suspend_stm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_SUSPEND_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_SUSPEND_INIT_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_resume_init_acq_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_ACQ_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_ACQ_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_resume_init_acq_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_resume_init_acq_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_deact_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_DEACT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_DEACT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_deact_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_deact_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_abort_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_invalid_input_fatal

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_RESUME_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RESUME_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_invalid_input_fatal
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_invalid_input_fatal() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_hst_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_INIT_STATE,
    upon receiving input LTE_ML1_SM_HST_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_HST_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_hst_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_hst_req() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_SUSPEND_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_SUSPEND_RX_CFG_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_rx_cfg_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_RX_CFG_STATE,
    upon receiving input LTE_ML1_SM_STM_RX_CFG_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RX_CFG_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_rx_cfg_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_rx_cfg_rsp() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_SUSPEND_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_SUSPEND_ACQ_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_srch_init_acq_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_ACQ_STATE,
    upon receiving input LTE_ML1_SM_STM_SEARCH_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_SEARCH_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_srch_init_acq_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_srch_init_acq_cnf() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_SUSPEND_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_SUSPEND_NPBCH_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_srch_pbch_decode_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_NPBCH_STATE,
    upon receiving input LTE_ML1_SM_STM_NBPBCH_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NBPBCH_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_srch_pbch_decode_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_srch_pbch_decode_cnf() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_SUSPEND_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_SUSPEND_RESUME_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_suspend_stm_resume_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_SUSPEND_STM,
    state LTE_ML1_SM_SUSPEND_RESUME_STATE,
    upon receiving input LTE_ML1_SM_STM_RESUME_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RESUME_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_suspend_stm_resume_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_suspend_stm_resume_req() */




