/*=============================================================================

    __lte_ml1_sm_acq_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_acq_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_ACQ_STM_INT_H
#define __LTE_ML1_SM_ACQ_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_acq_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_ACQ_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_ACQ_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_ACQ_STM_NUM_STATES 11

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_ACQ_STM_NUM_INPUTS 29

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_acq_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_acq_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_acq_abort_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_abort_state_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_band_scan_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_system_scan_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_cell_meas_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_plmn_acq_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_acq_plmn_pbch_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_acq_init_proc_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_proc_acq_req1(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_s2_proc_acq_stage2_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_band_scan_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_system_scan_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_sched_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_fscan_obj_abort_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_deactivate_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_plmn_acq_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_cell_meas_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_move_to_ho_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_timer_expiry(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_pbch_timer_expiry_do_nothing(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_init_proc_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_stm_invalid_input_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_detect_proc_search_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_do_nothing(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_nbpbch_proc_nbpbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_pbch_timer_expiry(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_s2_proc_acq_stage1_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_s2_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_stm_abort_rf_tune_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_proc_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_fscan_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_proc_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_proc_scan_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_abort_object_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_band_scan_ffs_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_scan_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_proc_abort_scan_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_band_scan_sched_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_scan_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_sys_scan_lfs_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_sys_scan_sched_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_cell_meas_srv_meas_resp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_plmn_acq_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_proc_acq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_proc_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_acq_plmn_pbch_timer_expiry(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_ACQ_INIT_STATE,
  LTE_ML1_SM_ACQ_RF_TUNE_STATE,
  LTE_ML1_SM_ACQ_DETECT_STATE,
  LTE_ML1_SM_ACQ_NBPBCH_STATE,
  LTE_ML1_SM_ACQ_STAGE2_STATE,
  LTE_ML1_SM_ACQ_ABORT_STATE,
  LTE_ML1_SM_ACQ_BAND_SCAN_STATE,
  LTE_ML1_SM_ACQ_SYSTEM_SCAN_STATE,
  LTE_ML1_SM_ACQ_CELL_MEAS_STATE,
  LTE_ML1_SM_ACQ_PLMN_ACQ_STATE,
  LTE_ML1_SM_ACQ_PLMN_PBCH_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_ACQ_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_ACQ_STM_INT_H */
