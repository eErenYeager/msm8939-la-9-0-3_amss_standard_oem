/*=============================================================================

    __lte_ml1_sm_conn_inter_freq_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_conn_inter_freq_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_CONN_INTER_FREQ_STM_INT_H
#define __LTE_ML1_SM_CONN_INTER_FREQ_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_conn_inter_freq_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_CONN_IFREQ_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_CONN_IFREQ_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_CONN_IFREQ_STM_NUM_STATES 8

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_CONN_IFREQ_STM_NUM_INPUTS 11

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_conn_inter_freq_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_conn_inter_freq_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_conn_inter_freq_stm_init_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_conn_inter_freq_stm_aborting_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_conn_inter_freq_gap_alloc_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_init_inter_freq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_stm_reset_gap_index(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_stm_do_nothing(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_bld_script_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_stm_invalid_input_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_pre_gap_alloc_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_pre_gap_inter_freq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_rf_tune_away_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_gap_tune_back_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_rf_tune_back_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_ngbr_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_inter_freq_ngbr_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_abort_inter_freq_tune_back_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_conn_abort_inter_freq_handle_cnf_msg(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_CONN_IFREQ_INIT_STATE,
  LTE_ML1_SM_CONN_IFREQ_BLD_SCRIPT_STATE,
  LTE_ML1_SM_CONN_IFREQ_PRE_GAP_STATE,
  LTE_ML1_SM_CONN_IFREQ_RF_TUNE_AWAY_STATE,
  LTE_ML1_SM_CONN_IFREQ_GAP_ONLINE_STATE,
  LTE_ML1_SM_CONN_IFREQ_RF_TUNE_BACK_STATE,
  LTE_ML1_SM_CONN_IFREQ_SRCH_MEAS_OFFLINE_STATE,
  LTE_ML1_SM_CONN_IFREQ_ABORTING_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_CONN_IFREQ_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_CONN_INTER_FREQ_STM_INT_H */
