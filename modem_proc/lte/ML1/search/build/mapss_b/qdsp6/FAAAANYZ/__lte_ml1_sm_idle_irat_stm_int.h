/*=============================================================================

    __lte_ml1_sm_idle_irat_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_idle_irat_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_IDLE_IRAT_STM_INT_H
#define __LTE_ML1_SM_IDLE_IRAT_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_idle_irat_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_IDLE_IRAT_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_IDLE_IRAT_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_IDLE_IRAT_STM_NUM_STATES 10

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_IDLE_IRAT_STM_NUM_INPUTS 21

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_idle_irat_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_idle_irat_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_idle_irat_stm_dl_disable_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_dl_enable_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_aborting_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_tune_back_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_fake_rf_tune_back_state_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_gsm_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_wcdma_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_tds_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_irat_stm_rsm_trans_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_idle_irat_stm_start_meas_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_dl_disable_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_mark_abort_triggered(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_dl_enable_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_handle_dl_enable_tune_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_abort_meas_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_wcdma_cleanup_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_wcdma_srch_meas_result_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_invalid_input_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_aborting_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_g_process_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_tds_process_msg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_run_rf_script_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_fake_run_rf_script_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_irat_stm_rsm_trans_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_IDLE_IRAT_STM_DL_DISABLE_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_DL_ENABLE_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_IDLE_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_ABORTING_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_TUNE_BACK_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_FAKE_TUNE_BACK_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_GERAN_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_WCDMA_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_TDS_STATE,
  LTE_ML1_SM_IDLE_IRAT_STM_DL_ENABLE_MODE_TRANSITION,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_IDLE_IRAT_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_IDLE_IRAT_STM_INT_H */
