/*=============================================================================

    __lte_ml1_sm_con_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_con_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_CON_STM_INT_H
#define __LTE_ML1_SM_CON_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_con_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_CON_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_CON_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_CON_STM_NUM_STATES 4

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_CON_STM_NUM_INPUTS 88

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_con_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_con_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_con_meas_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_con_ho_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_con_stm_dereg_objs_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_con_meas_proc_conn_meas_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_ho_susp_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_ho_compl_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_sfn_upd_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_proc_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_proc_pbch_decode_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_msg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_proc_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_qta_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_auto_pbch_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_auto_pbch_obj_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_handle_recfg_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_handle_recfg_resume(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_tune_back_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_move_to_ho_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_scell_config_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_meas_handle_scc_status_update_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_stm_forward_msg_to_gsm_conn(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_stm_forward_msg_to_wcdma_stm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_stm_forward_msg_to_tds_stm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_handle_msg(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_proc_pbch_decode_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_proc_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_handle_scell_config_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_abort_handle_auto_pbch_obj_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_handle_irat_schedule_cancel_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_acq(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_resume(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_proc_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_proc_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_proc_ho_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_nbr_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_ho_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_stm_obj_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_con_stm_aborting_handle_dereg_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_CON_MEAS_STATE,
  LTE_ML1_SM_CON_MEAS_ABORT_STATE,
  LTE_ML1_SM_CON_HO_STATE,
  LTE_ML1_SM_CON_DEREG_OBJS,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_CON_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_CON_STM_INT_H */
