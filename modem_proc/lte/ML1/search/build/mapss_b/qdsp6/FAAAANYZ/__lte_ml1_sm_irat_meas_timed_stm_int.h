/*=============================================================================

    __lte_ml1_sm_irat_meas_timed_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_irat_meas_timed_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_IRAT_MEAS_TIMED_STM_INT_H
#define __LTE_ML1_SM_IRAT_MEAS_TIMED_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_irat_meas_timed_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_IRAT_TIMED_MEAS_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_IRAT_TIMED_MEAS_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_IRAT_TIMED_MEAS_STM_NUM_STATES 3

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_IRAT_TIMED_MEAS_STM_NUM_INPUTS 14

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_irat_meas_timed_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_irat_meas_timed_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_irat_meas_timed_starting_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_irat_meas_timed_starting_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_irat_timed_srch_meas_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_irat_timed_srch_meas_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_irat_meas_timed_start_gap_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_irat_meas_timed_starting_app_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_stmr_dump_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_slam_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_ttrans_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_handle_mcvs_q6_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_stm_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_handle_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_stm_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_stm_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_app_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_stm_rf_script_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_handle_recovery_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_irat_meas_timed_cleanup_app_cfg_cnf(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_IRAT_TIMED_MEAS_STARTING_STATE,
  LTE_ML1_SM_IRAT_TIMED_SRCH_MEAS_STATE,
  LTE_ML1_SM_IRAT_TIMED_CLEANUP_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_IRAT_TIMED_MEAS_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_IRAT_MEAS_TIMED_STM_INT_H */
