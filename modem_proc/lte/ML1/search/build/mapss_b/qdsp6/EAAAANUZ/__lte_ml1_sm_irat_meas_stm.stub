/*!
  @file
  __lte_ml1_sm_irat_meas_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_SM_IRAT_MEAS_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_sm_irat_meas_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_sm_irat_meas_stm_type;


/*! @brief Variables internal to module __lte_ml1_sm_irat_meas_stm.stub
*/
STATIC __lte_ml1_sm_irat_meas_stm_type __lte_ml1_sm_irat_meas_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_SM_IRAT_MEAS_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_sm_irat_meas_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_MEAS_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_stm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_sm_irat_meas_stm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_IRAT_MEAS_STM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_stm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_stm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_MEAS_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_cleanup_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_CLEANUP_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_cleanup_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_cleanup_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_meas_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_MEAS_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_MEAS_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_meas_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_meas_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_search_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_SEARCH_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_SEARCH_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_search_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_search_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_abort_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_blacklist_cmd

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_BLACKLIST_CMD

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_BLACKLIST_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_blacklist_cmd
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_blacklist_cmd() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_srch_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_SRCH_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_SRCH_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_srch_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_srch_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_meas_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_MEAS_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_MEAS_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_meas_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_meas_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_srch_abort_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_SRCH_ABORT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_SRCH_ABORT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_srch_abort_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_srch_abort_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_abort_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_ABORT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_ABORT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_abort_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_abort_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_nop

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_RF_TUNE_SCRIPT_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RF_TUNE_SCRIPT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_nop
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_nop() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_cleanup_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_STM_X2L_CLEANUP_DONE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_X2L_CLEANUP_DONE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_cleanup_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_cleanup_done() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_ttrans_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_TTRANS_DONE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_TTRANS_DONE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_ttrans_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_ttrans_done() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_app_cfg_done

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_app_cfg_done
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_app_cfg_done() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_MEAS_INIT_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_time_start_rx_cfg

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_time_start_rx_cfg
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_time_start_rx_cfg() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_startup_cmd

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_STARTUP_CMD

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_STARTUP_CMD, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_startup_cmd
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_startup_cmd() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_deinit_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_DEINIT_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_DEINIT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_deinit_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_deinit_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_timed_srch_meas_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_TIMED_SRCH_MEAS_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_timed_srch_meas_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_timed_srch_meas_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_abort_req_in_init_state

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STATE,
    upon receiving input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ

    @detail
    Called upon receipt of input LTE_CPHY_IRAT_MEAS_W2L_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_abort_req_in_init_state
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_abort_req_in_init_state() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_MEAS_INIT_STMR_ON_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_proc_time_stop_rx_cfg

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_INIT_STMR_ON_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_proc_time_stop_rx_cfg
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_proc_time_stop_rx_cfg() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_MEAS_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_MEAS_TIMED_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_meas_timed_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_TIMED_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_meas_timed_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_meas_timed_state_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_meas_stm_forward_to_timed_stm

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_MEAS_STM,
    state LTE_ML1_SM_IRAT_MEAS_TIMED_STATE,
    upon receiving input LTE_LL1_SYS_SYNC_STMR_DUMP_CNF

    @detail
    Called upon receipt of input LTE_LL1_SYS_SYNC_STMR_DUMP_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_meas_stm_forward_to_timed_stm
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_meas_stm_forward_to_timed_stm() */




