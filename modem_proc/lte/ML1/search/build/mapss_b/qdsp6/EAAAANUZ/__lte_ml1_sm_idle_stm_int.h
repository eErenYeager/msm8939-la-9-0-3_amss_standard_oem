/*=============================================================================

    __lte_ml1_sm_idle_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_idle_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_IDLE_STM_INT_H
#define __LTE_ML1_SM_IDLE_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_idle_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_IDLE_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_IDLE_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_IDLE_STM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_IDLE_STM_NUM_INPUTS 61

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_idle_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_idle_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_sm_idle_stm_idle_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_idle_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_sch_wait_start_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_sch_wait_desched_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_rf_tune_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_lte_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_lte_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_lte_bsmp_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_irat_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_aborting_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_sm_idle_stm_dereg_objs_entry(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_sm_idle_stm_meas_cfg_req_delay(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_acq_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_acq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_cell_meas_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_band_scan_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_system_scan_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_scan_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_invalid_input_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_ngbr_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_invalid_input_non_fatal(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_obj_desched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_obj_end_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_ignore_search_meas_resp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_idle_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_handle_hst_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_handle_hst_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_handle_hst_cleanup_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_ignore_ngbr_srch_rsp_all_states(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_start_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_start_obj_desched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_offline_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_wait_start_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_ignore_hst_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_desched_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_desched_obj_desched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_sch_wait_desched_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_wait_desched_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_acq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_bsmp_scan_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_sch_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_meas_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_serv_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_ngbr_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_ngbr_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_pbch_stop_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_samp_rec_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_sch_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_rf_tune_script_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_offline_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_acq_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_serv_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_acq_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_lfs_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_lte_bsmp_ffs_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_srch_meas_obj_start_req_handler(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_sch_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_offline_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_irat_forward_msg_to_irat_sm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_meas_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_cell_select_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_serv_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_ngbr_srch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_ngbr_srch_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_ngbr_meas_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_ngbr_meas_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_pbch_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_pbch_stop_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_acq_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_acq_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_scan_abort_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_deact_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_sch_obj_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_sch_obj_desched_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_sch_obj_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_rx_cfg_rsp(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_forward_msg_to_irat_sm(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_handle_reconfig_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_aborting_handle_dereg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_dereg_pbch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_obj_noop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_dereg_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_idle_stm_dereg_deact_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_IDLE_IDLE_STATE,
  LTE_ML1_SM_IDLE_SCH_WAIT_START_STATE,
  LTE_ML1_SM_IDLE_SCH_WAIT_DESCHED_STATE,
  LTE_ML1_SM_IDLE_RF_TUNE_STATE,
  LTE_ML1_SM_IDLE_LTE_SRCH_MEAS_PBCH_STATE,
  LTE_ML1_SM_IDLE_LTE_BACKGROUND_SRCH_MEAS_PBCH_STATE,
  LTE_ML1_SM_IDLE_IRAT_SRCH_MEAS_STATE,
  LTE_ML1_SM_IDLE_ABORTING_STATE,
  LTE_ML1_SM_IDLE_DEREG_OBJS,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_IDLE_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_IDLE_STM_INT_H */
