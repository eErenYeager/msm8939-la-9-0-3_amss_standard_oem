/*!
  @file
  __lte_ml1_sm_irat_sched_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_SM_IRAT_SCHED_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_sm_irat_sched_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_sm_irat_sched_stm_type;


/*! @brief Variables internal to module __lte_ml1_sm_irat_sched_stm.stub
*/
STATIC __lte_ml1_sm_irat_sched_stm_type __lte_ml1_sm_irat_sched_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_SM_IRAT_SCHED_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_sm_irat_sched_stm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_IRAT_SCHED_STM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_sch_wait_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_sch_wait_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_sch_wait_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_start_object

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_start_object
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_start_object() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_handle_abort

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_handle_abort
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_handle_abort() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_schedule_handle_offline_start_obj

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_SCH_WAIT_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_OFFLINE_START_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_schedule_handle_offline_start_obj
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_schedule_handle_offline_start_obj() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_DL_DISABLE_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_dl_disable_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_DISABLE_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_dl_disable_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_dl_disable_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_dl_disable_rx_cfg_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_DISABLE_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_dl_disable_rx_cfg_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_dl_disable_rx_cfg_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_mark_abort_triggered

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_DISABLE_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_mark_abort_triggered
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_mark_abort_triggered() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_meas_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_meas_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_meas_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_schedule_handle_tick_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_TICK_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_TICK_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_schedule_handle_tick_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_schedule_handle_tick_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_handle_complete_ind

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_COMPLETE_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_handle_complete_ind
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_handle_complete_ind() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_end_tick_object

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_MEAS_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_TICK_OBJ_END_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_end_tick_object
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_end_tick_object() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_TUNE_BACK_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_tuneback_state_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_TUNE_BACK_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_tuneback_state_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_tuneback_state_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_handle_rf_script_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_TUNE_BACK_STATE,
    upon receiving input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_handle_rf_script_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_handle_rf_script_cnf() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_dl_enable_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_dl_enable_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_dl_enable_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_dl_enable_rx_cfg_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE,
    upon receiving input LTE_ML1_SM_RX_CFG_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_RX_CFG_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_dl_enable_rx_cfg_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_dl_enable_rx_cfg_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_end_object

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DL_ENABLE_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_OBJ_END_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_end_object
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_end_object() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_desched_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_desched_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_desched_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_nop

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_OBJ_START_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_nop
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_nop() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_irat_sched_stm_end_offline_object

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_DESCHED_STATE,
    upon receiving input LTE_ML1_SM_IRAT_SCHED_OFFLINE_OBJ_END_IND

    @detail
    Called upon receipt of input LTE_ML1_SM_IRAT_SCHED_OFFLINE_OBJ_END_IND, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_irat_sched_stm_end_offline_object
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_irat_sched_stm_end_offline_object() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_IRAT_SCHED_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_IRAT_SCHED_STM_OFFLINE_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_irat_sched_stm_offline_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_IRAT_SCHED_STM,
    state LTE_ML1_SM_IRAT_SCHED_STM_OFFLINE_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_irat_sched_stm_offline_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_irat_sched_stm_offline_entry() */




