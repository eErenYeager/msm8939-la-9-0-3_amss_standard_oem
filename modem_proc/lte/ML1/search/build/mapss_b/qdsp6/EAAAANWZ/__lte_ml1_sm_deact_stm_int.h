/*=============================================================================

    __lte_ml1_sm_deact_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sm_deact_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SM_DEACT_STM_INT_H
#define __LTE_ML1_SM_DEACT_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sm_deact_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SM_DEACT_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SM_DEACT_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SM_DEACT_STM_NUM_STATES 1

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SM_DEACT_STM_NUM_INPUTS 7

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_sm_deact_stm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_sm_deact_stm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */


/* Transition function prototypes */
stm_state_t lte_ml1_sm_deact_proc_cphy_start_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_deact_proc_irat_init_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_sm_deact_proc_deact_req(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_SM_DEACT_INIT_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SM_DEACT_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SM_DEACT_STM_INT_H */
