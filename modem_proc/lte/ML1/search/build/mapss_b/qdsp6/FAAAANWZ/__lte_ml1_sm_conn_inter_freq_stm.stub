/*!
  @file
  __lte_ml1_sm_conn_inter_freq_stm.stub

  @brief
  This module contains the entry, exit, and transition functions
  necessary to implement the following state machines:

  @detail
  LTE_ML1_SM_CONN_IFREQ_STM ( 1 instance/s )


  OPTIONAL further detailed description of state machines
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

/* Include STM external API */
#include <stm2.h>

//! @todo Include necessary files here


/*===========================================================================

         STM COMPILER GENERATED PROTOTYPES AND DATA STRUCTURES

===========================================================================*/

/* Include STM compiler generated internal data structure file */
#include "__lte_ml1_sm_conn_inter_freq_stm_int.h"

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*! @brief Structure for state-machine per-instance local variables
*/
typedef struct
{
  int   internal_var;  /*!< My internal variable */
  void *internal_ptr;  /*!< My internal pointer */
  //! @todo SM per-instance variables go here
} __lte_ml1_sm_conn_inter_freq_stm_type;


/*! @brief Variables internal to module __lte_ml1_sm_conn_inter_freq_stm.stub
*/
STATIC __lte_ml1_sm_conn_inter_freq_stm_type __lte_ml1_sm_conn_inter_freq_stm;



/*===========================================================================

                 STATE MACHINE: LTE_ML1_SM_CONN_IFREQ_STM

===========================================================================*/

/*===========================================================================

  STATE MACHINE ENTRY FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_CONN_IFREQ_STM

    @detail
    Called upon activation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_stm_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_conn_inter_freq_stm_entry() */


/*===========================================================================

  STATE MACHINE EXIT FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_CONN_IFREQ_STM

    @detail
    Called upon deactivation of this state machine, with optional
    user-passed payload pointer parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_stm_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_conn_inter_freq_stm_exit() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_INIT_STATE

===========================================================================*/

/*===========================================================================

  STATE ENTRY FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_init_entry

===========================================================================*/
/*!
    @brief
    Entry function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_INIT_STATE

    @detail
    Called upon entry into this state of the state machine, with optional
    user-passed payload pointer parameter.  The prior state of the state
    machine is also passed as the prev_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_stm_init_entry
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         prev_state,  /*!< Previous state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( prev_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_conn_inter_freq_stm_init_entry() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_gap_alloc_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ALLOC_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ALLOC_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_gap_alloc_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_gap_alloc_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_init_inter_freq_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_init_inter_freq_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_init_inter_freq_abort_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_reset_gap_index

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_RESET_GAP_INDEX_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_RESET_GAP_INDEX_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_stm_reset_gap_index
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_stm_reset_gap_index() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_do_nothing

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_INIT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_stm_do_nothing
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_stm_do_nothing() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_BLD_SCRIPT_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_bld_script_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_BLD_SCRIPT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_BLD_SCRIPT_CNF

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_BLD_SCRIPT_CNF, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_bld_script_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_bld_script_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_BLD_SCRIPT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_abort_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_invalid_input_fatal

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_BLD_SCRIPT_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_stm_invalid_input_fatal
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_stm_invalid_input_fatal() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_PRE_GAP_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_pre_gap_alloc_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_PRE_GAP_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_PRE_GAP_ALLOC_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_pre_gap_alloc_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_pre_gap_alloc_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_pre_gap_inter_freq_abort_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_PRE_GAP_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_GAP_ABORT_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_pre_gap_inter_freq_abort_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_pre_gap_inter_freq_abort_req() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_RF_TUNE_AWAY_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_rf_tune_away_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_RF_TUNE_AWAY_STATE,
    upon receiving input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_rf_tune_away_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_rf_tune_away_cnf() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_GAP_ONLINE_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_gap_tune_back_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_GAP_ONLINE_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_TUNE_BACK_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_TUNE_BACK_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_gap_tune_back_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_gap_tune_back_req() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_RF_TUNE_BACK_STATE

===========================================================================*/

/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_rf_tune_back_cnf

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_RF_TUNE_BACK_STATE,
    upon receiving input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_rf_tune_back_cnf
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_rf_tune_back_cnf() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_ngbr_srch_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_RF_TUNE_BACK_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_SRCH_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_SRCH_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_ngbr_srch_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_ngbr_srch_rsp() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_inter_freq_ngbr_meas_rsp

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_RF_TUNE_BACK_STATE,
    upon receiving input LTE_ML1_SM_STM_NGBR_MEAS_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_NGBR_MEAS_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_inter_freq_ngbr_meas_rsp
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_inter_freq_ngbr_meas_rsp() */


/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_SRCH_MEAS_OFFLINE_STATE

===========================================================================*/

/*===========================================================================

     (State Machine: LTE_ML1_SM_CONN_IFREQ_STM)
     STATE ENTRY/EXIT/TRANSITION FUNCTIONS: LTE_ML1_SM_CONN_IFREQ_ABORTING_STATE

===========================================================================*/

/*===========================================================================

  STATE EXIT FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_aborting_exit

===========================================================================*/
/*!
    @brief
    Exit function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_ABORTING_STATE

    @detail
    Called upon exit of this state of the state machine, with optional
    user-passed payload pointer parameter.  The impending state of the state
    machine is also passed as the next_state parameter.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_stm_aborting_exit
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  stm_state_t         next_state,  /*!< Next state */
  void                *payload     /*!< Payload pointer */
)
{

  //! @todo Variable declarations go here

  STM_UNUSED( payload );
  STM_UNUSED( next_state );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

} /* lte_ml1_sm_conn_inter_freq_stm_aborting_exit() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_abort_inter_freq_tune_back_req

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_ABORTING_STATE,
    upon receiving input LTE_ML1_SM_STM_CONN_IFREQ_TUNE_BACK_REQ

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_CONN_IFREQ_TUNE_BACK_REQ, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_abort_inter_freq_tune_back_req
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_abort_inter_freq_tune_back_req() */


/*===========================================================================

  TRANSITION FUNCTION:  lte_ml1_sm_conn_abort_inter_freq_handle_cnf_msg

===========================================================================*/
/*!
    @brief
    Transition function for state machine LTE_ML1_SM_CONN_IFREQ_STM,
    state LTE_ML1_SM_CONN_IFREQ_ABORTING_STATE,
    upon receiving input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP

    @detail
    Called upon receipt of input LTE_ML1_SM_STM_RF_TUNE_SCRIPT_RSP, with optional
    user-passed payload pointer.

    @return
    Returns the next state that the state machine should transition to
    upon receipt of the input.  This state must be a valid state for this
    state machine.

*/
/*=========================================================================*/
stm_state_t lte_ml1_sm_conn_abort_inter_freq_handle_cnf_msg
(
  stm_state_machine_t *sm,         /*!< State Machine instance pointer */
  void                *payload     /*!< Payload pointer */
)
{
  stm_state_t next_state = STM_SAME_STATE; /* Default 'next' state */

  //! @todo Additional variable declarations go here

  STM_UNUSED( payload );

  /*-----------------------------------------------------------------------*/

  /* Ensure that the state machine instance pointer passed is valid */
  STM_NULL_CHECK( sm );

  /*-----------------------------------------------------------------------*/

  //! @todo Code goes here

  return( next_state );

} /* lte_ml1_sm_conn_abort_inter_freq_handle_cnf_msg() */




