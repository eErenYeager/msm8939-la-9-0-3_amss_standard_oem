/*!
  @file
  lte_ml1_sm_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/search/inc/lte_ml1_sm_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_SM_NV_H
#define LTE_ML1_SM_NV_H

#include <comdef.h>
#include "lte_as.h"
#include "lte_cphy_msg.h"
#include "lte_l1_types.h"
#include "lte_variation.h"
#include "lte_ml1_comdef.h"
#include "lte_startup.h"
//#include "lte_ml1_common_paging_drx.h"

/* Enable/Disable Serving Measurements */
#define LTE_ML1_SM_NO_SRCH_MEAS              0x00

#define LTE_ML1_SM_SRV_MEAS                  0x01

/* Enable/Disable idle mode Intra Freq Searches */
#define LTE_ML1_SM_INTRA_IDLE_NBR_SRCH       0x02

/* Enable/Disable idle mode Intra Freq Searches and Measurements both */
#define LTE_ML1_SM_INTRA_IDLE_NBR_SRCH_MEAS  0x04

/* Enable/Disable connected mode Intra Freq Searches */
#define LTE_ML1_SM_INTRA_CONN_NBR_SRCH       0x08

/* Enable/Disable connected mode Intra Freq Searches and Measurements both */
#define LTE_ML1_SM_INTRA_CONN_NBR_SRCH_MEAS  0x10

/* Enable/Disable connected mode SCC Searches and Measurements */
#define LTE_ML1_SM_INTRA_CONN_SCC_SRCH_MEAS  0x20


/* Utility macros for Meas and Searches */
#define LTE_ML1_SM_ENABLE_SRV_MEAS(mask)                  (mask |= LTE_ML1_SM_SRV_MEAS)
#define LTE_ML1_SM_SRV_MEAS_ENABLED(mask)                 ((mask & LTE_ML1_SM_SRV_MEAS) ? TRUE : FALSE)

#define LTE_ML1_SM_ENABLE_INTRA_IDLE_SRCH_MEAS(mask)      (mask |= LTE_ML1_SM_INTRA_IDLE_NBR_SRCH_MEAS)
#define LTE_ML1_SM_INTRA_IDLE_SRCH_ENABLED(mask)          ((mask & LTE_ML1_SM_INTRA_IDLE_NBR_SRCH)      ? TRUE : FALSE)
#define LTE_ML1_SM_INTRA_IDLE_SRCH_MEAS_ENABLED(mask)     ((mask & LTE_ML1_SM_INTRA_IDLE_NBR_SRCH_MEAS) ? TRUE : FALSE)

#define LTE_ML1_SM_ENABLE_INTRA_CONN_SRCH_MEAS(mask)      (mask |= LTE_ML1_SM_INTRA_CONN_NBR_SRCH_MEAS)
#define LTE_ML1_SM_INTRA_CONN_SRCH_ENABLED(mask)          ((mask & LTE_ML1_SM_INTRA_CONN_NBR_SRCH)      ? TRUE : FALSE)
#define LTE_ML1_SM_INTRA_CONN_SRCH_MEAS_ENABLED(mask)     ((mask & LTE_ML1_SM_INTRA_CONN_NBR_SRCH_MEAS) ? TRUE : FALSE)

#define LTE_ML1_SM_ENABLE_INTRA_CONN_SCC_SRCH_MEAS(mask)  (mask |= LTE_ML1_SM_INTRA_CONN_SCC_SRCH_MEAS)
#define LTE_ML1_SM_INTRA_CONN_SCC_SRCH_MEAS_ENABLED(mask) ((mask & LTE_ML1_SM_INTRA_CONN_SCC_SRCH_MEAS) ? TRUE : FALSE)

#define LTE_ML1_SM_DISABLE_ALL_SRCH_MEAS(mask)            (mask &= LTE_ML1_SM_NO_SRCH_MEAS)
#define LTE_ML1_SM_ALL_SRCH_MEAS_DISABLED(mask)           ((mask | LTE_ML1_SM_NO_SRCH_MEAS) ? FALSE : TRUE)

/* Number of bands need to update */
#define LTE_ML1_UPDATE_BAND_RANGE_NUM_BANDS 3

/* Maximum number of earfcn,bw pairs that can be configured in earfcn_bw efs file */
#define LTE_ML1_NV_CFG_MAX_EARFCN_BW_PAIRS   6

/*default value for the number of enb tx antenna if efs is not configured.*/
#define LTE_ML1_NV_CFG_DEFAULT_NUM_ENB_TX_ANT 0xFF

/*online rsrp threshold */
#define LTE_ML1_NV_CFG_IDLE_ONLINE_RSRP_THRESHOLD 18

/*offline rsrp threshold */
#define LTE_ML1_NV_CFG_IDLE_OFFLINE_RSRP_THRESHOLD 18

/*online snr threshold */
#define LTE_ML1_NV_CFG_IDLE_ONLINE_SNR_THRESHOLD 50

/*offline snr threshold */
#define LTE_ML1_NV_CFG_IDLE_OFFLINE_SNR_THRESHOLD 50

/* online panic snr threshold */
#define LTE_ML1_NV_CFG_IDLE_PANIC_ONLINE_SNR_THRESHOLD 60

/* offline panic snr threshold */
#define LTE_ML1_NV_CFG_IDLE_PANIC_OFFLINE_SNR_THRESHOLD 60

/*offline snr threshold */
#define LTE_ML1_NV_CFG_IDLE_MIN_NBR_RSRP_THRESHOLD 130

/*sss  threshold db */
#define LTE_ML1_NV_CFG_IDLE_SSS_THRESH_DB 3

/*search prune count */
#define LTE_ML1_NV_CFG_IDLE_SRCH_PRUNE_CNT 2

/*meas prune count  320*/
#define LTE_ML1_NV_CFG_IDLE_MEAS_PRUNE_CNT_320 4

/*meas prune count  640*/
#define LTE_ML1_NV_CFG_IDLE_MEAS_PRUNE_CNT_640 4

/*meas prune count  1280*/
#define LTE_ML1_NV_CFG_IDLE_MEAS_PRUNE_CNT_1280 4

/*meas prune count  2560*/
#define LTE_ML1_NV_CFG_IDLE_MEAS_PRUNE_CNT_2560 2

/*sample rec times search*/
#define LTE_ML1_NV_CFG_IDLE_SAMPLE_REC_TIMES_SRCH 5

/*sample rec times search*/
#define LTE_ML1_NV_CFG_IDLE_SAMPLE_REC_TIMES_MEAS 5

/*sample rec times search*/
#define LTE_ML1_NV_CFG_IDLE_SAMPLE_REC_TIMES_MBSFN_MEAS 5

#define LTE_BW_IDX_MAX 5 /*Revisit*/

/* Threshould used to decide whether we need ACQ when get
 * connection release cnf, default rsrp threshold 1.0dB in Q7 format is 128 */
#define LTE_ML1_SM_CON_ACQ_ON_CONN_REL_RSRP_THRESHOLD_IN_DB 128

/*! Timeout in ms before asserting that a fw req has gone on too long */
#define LTE_ML1_SM_SERV_MEAS_TIMEOUT    50
#define LTE_ML1_SM_MEAS_CNF_TIMEOUT     50
#define LTE_ML1_SM_MEAS_SS_CNF_TIMEOUT  50
#define LTE_ML1_SM_SRCH_CNF_TIMEOUT     50
#define LTE_ML1_SM_SRCH_SS_CNF_TIMEOUT  50
#define LTE_ML1_SM_PBCH_CNF_TIMEOUT     250
#define LTE_ML1_SM_INIT_ACQ_CNF_TIMEOUT 50
#define LTE_ML1_SM_FFS_FDD_CNF_TIMEOUT  30
#define LTE_ML1_SM_FFS_TDD_CNF_TIMEOUT  105
#define LTE_ML1_SM_LFS_FDD_CNF_TIMEOUT  30
#define LTE_ML1_SM_LFS_TDD_CNF_TIMEOUT  105

/*! NV file struct for storing camp band and earfcn */
typedef struct
{
  /* Band user wants UE to camp on */
  uint16  band;

  /* earfcn within the band user wants UE to camp on */
  lte_efs_earfcn_t  earfcn;
} lte_ml1_efs_camp_band_earfcn_s;

/*! NV file struct for band scan thresholds */
typedef struct
{
  uint8 edge_det_max_min_energy_ratio_normal[(uint8)LTE_BW_IDX_MAX + 1];
  uint8 edge_det_max_min_energy_ratio_exception[(uint8)LTE_BW_IDX_MAX + 1];
  uint8 flat_sys_max_min_energy_ratio_normal[(uint8)LTE_BW_IDX_MAX + 1];
  uint8 flat_sys_max_min_energy_ratio_exception[(uint8)LTE_BW_IDX_MAX + 1];
} lte_ml1_efs_bs_thresholds;

/*! Camping - earfcn,bw pair info structure */
typedef struct
{
  /* System earfcn info */
  lte_efs_earfcn_t      earfcn;

  /*! UE BW configuration */
  uint16                bw_cfg;
} lte_ml1_nv_cfg_earfcn_bw_s;


/*! UE LTE systems camp config */
typedef struct
{
  /*! Number of systems UE can camp on */
  uint16             num_camp_systems;
  /*! Earfcn, BW pair info */
  lte_cphy_system_s  camp_system[LTE_ML1_NV_CFG_MAX_EARFCN_BW_PAIRS];
} lte_ml1_nv_cfg_ue_camp_cfg_s;


typedef struct
{
  uint16 srch;
  uint16 meas;
  uint16 mbsfn_meas;
} lte_ml1_nv_cfg_idle_offline_l2l_samp_rec_times_s;

/*! Offline threshold values */
typedef struct
{
  /* Online rsrq threshold*/
  int16 on_rsrq;
  /* Offline rsrq threshold*/
  int16 off_rsrq;
  /* Online snr*/
  int16 on_snr10x;
  /* Offline snr*/
  int16 off_snr10x;
} lte_ml1_nv_cfg_offline_thresh_s;




/*! Default meas cfgs to use in place of SIB3/4 parameters */
typedef struct
{
  /*! Validity of EFS */
  boolean valid;

  /*! Hysteresis for cell reselection (in dB) */
  uint8 q_hyst;

  /*! Set to true to indicate neighbours have same reference signals */
  boolean same_ref_signals_in_neighbour;

  /*! Snonintra */
  lte_l1_srch_meas_threshold_t s_non_intra_search;

  /*! threshServingLow */
  lte_l1_resel_threshold_t thresh_serving_low;

  /*! Cell reselection priority */
  lte_l1_resel_priority_t  cell_resel_priority;

  /*! If serving freq information is present */
  boolean serving_freq_resel_info_r9_present;

  /*! Serving frequency reselection info for Rel 9*/
  lte_cphy_meas_serving_freq_resel_info_rel9_s serving_freq_resel_info_r9;

  /*! Qrxlevmin found in SIB3 */
  lte_l1_q_rxlevmin_t q_rxlevmin;

  /*! Maximum value for output power that the UE should use */
  int8 p_max;

  /*! Sintra */
  lte_l1_srch_meas_threshold_t s_intra_search;

  /*! Bandwidth to restrict measurements on */
  lte_cphy_meas_bandwidth_e meas_bandwidth;

  /*! Whether Rx1 is used */
  boolean antenna_port_1;

  /*! MBSFN config of neighboring cells */
  uint8 neighbourCellConfig;

  /*! t-ReselectionEUTRAN */
  lte_l1_treselection_t t_resel_eutran;

  /*! t-ReselectionEUTRAN-SF-Medium */
  lte_cphy_meas_time_sf_e t_resel_eutran_medium;

  /*! t-ReselectionEUTRAN-SF-High */
  lte_cphy_meas_time_sf_e t_resel_eutran_high;

  /*! If release 9 params are present */
  boolean intra_freq_resel_rel9_param_present;

  /*! Rel9 parameters for intra-freq reselection */
  lte_cphy_meas_intra_freq_resel_info_rel9_s intra_freq_resel_rel9_param;

} lte_ml1_nv_cfg_default_meas_cfg_s;

typedef struct
{
  uint32 num_cells;
  lte_phy_cell_id_t meas_cell_list[16];
}lte_ml1_nv_meas_cell_list_s;

typedef struct
{
  /* Timeout Parameters for FW reponse in MD. Negative values are 
     treated as invalid - defaults to lib defined DEFAULT. */
  int16 lte_ml1_sm_serv_meas_cnf_timeout;
  int16 lte_ml1_sm_meas_cnf_timeout;
  int16 lte_ml1_sm_meas_ss_cnf_timeout;
  int16 lte_ml1_sm_srch_cnf_timeout;
  int16 lte_ml1_sm_srch_ss_cnf_timeout;
  int16 lte_ml1_sm_pbch_cnf_timeout;
  int16 lte_ml1_sm_init_acq_cnf_timeout;
  int16 lte_ml1_sm_ffs_fdd_cnf_timeout;
  int16 lte_ml1_sm_ffs_tdd_cnf_timeout;
  int16 lte_ml1_sm_lfs_fdd_cnf_timeout;
  int16 lte_ml1_sm_lfs_tdd_cnf_timeout;
} lte_ml1_nv_cfg_fw_timers_s;

/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_search_meas_size
 
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
       
    @details

    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_search_meas_size(void);

/*===========================================================================
 
   FUNCTION:  
 
 ===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.

    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.

    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_search_meas_defaultvalue(void *ptr);


/*===========================================================================
  
    FUNCTION:  lte_ml1_sm_nv_serv_meas_freq_size
  
 ===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
       
    @details

    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_serv_meas_freq_size(void);


/*===========================================================================
  
    FUNCTION:  lte_ml1_sm_nv_serv_meas_freq_defaultvalue
  
 ===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns),
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_serv_meas_freq_defaultvalue(void *ptr);

/*===========================================================================
    
    FUNCTION:  lte_ml1_sm_nv_serv_meas_freq_post_efsread
    
  ===========================================================================*/
/*!
       @brief
       This function will be called post efsread success by framework, 
       to perform client defined post efsread functionality.
   
       @details
       .
   
       @return
*/
/*=========================================================================*/

void  lte_ml1_sm_nv_serv_meas_freq_post_efsread(void *ptr);

/*===========================================================================
     
       FUNCTION:  lte_ml1_sm_nv_bs_bw_supported_size
     
 ===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
           
        @details
    
        @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_bs_bw_supported_size(void);


/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_bs_bw_supported_defaultvalue
 
 ===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.

    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.

    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_bs_bw_supported_defaultvalue(void *ptr);

/*===========================================================================
   
   FUNCTION:  lte_ml1_sm_nv_bs_bw_supported_post_efsread
   
 ===========================================================================*/
/*!
       @brief
       This function will be called post efsread success by framework, 
       to perform client defined post efsread functionality.
   
       @details
       .
   
       @return
*/
/*=========================================================================*/
void  lte_ml1_sm_nv_bs_bw_supported_post_efsread(void *ptr);


/*===========================================================================
     
       FUNCTION:  lte_ml1_sm_nv_bs_thresholds_size
     
  ===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
           
        @details
    
        @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_bs_thresholds_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_bs_thresholds_defaultvalue
 
 ===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.

    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.

    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_bs_thresholds_defaultvalue(void *ptr);


/*===========================================================================
  
  FUNCTION:  lte_ml1_sm_nv_bs_thresholds_post_efsread
  
===========================================================================*/
/*!
       @brief
       This function will be called post efsread success by framework, 
       to perform client defined post efsread functionality.
   
       @details
       .
   
       @return
*/
/*=========================================================================*/
void  lte_ml1_sm_nv_bs_thresholds_post_efsread(void *ptr);

/*===========================================================================
      
        FUNCTION:  lte_ml1_sm_nv_disable_gsm_opt_size
      
 ===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
             
    @details
      
    @return size in bytes
 */
/*=========================================================================*/

uint8 lte_ml1_sm_nv_disable_gsm_opt_size(void);


/*===========================================================================

  FUNCTION:  lte_ml1_sm_nv_disable_gsm_opt_defaultvalue

===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.

    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.

    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_disable_gsm_opt_defaultvalue(void *ptr);


/*===========================================================================
       
       FUNCTION:  lte_ml1_sm_nv_camp_band_earfcn_size
       
 ===========================================================================*/
/*!
     @brief
     This function returns the size of EFS variable in bytes. 
     This size information is used by EFS framework for memory 
     allocation and storing the EFS value.
              
     @details
       
     @return size in bytes
 */
/*=========================================================================*/

uint8 lte_ml1_sm_nv_camp_band_earfcn_size(void);


/*===========================================================================
  
    FUNCTION:	lte_ml1_sm_nv_camp_band_earfcn_defaultvalue
  
 ===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_camp_band_earfcn_defaultvalue(void *ptr);

/*===========================================================================
    
    FUNCTION:  lte_ml1_sm_nv_camp_band_earfcn_post_efsread
    
 ===========================================================================*/
/*!
        @brief
        This function will be called post efsread success by framework, 
        to perform client defined post efsread functionality.
    
        @details
        .
    
        @return
*/
/*=========================================================================*/

void  lte_ml1_sm_nv_camp_band_earfcn_post_efsread(void *ptr);

/*===========================================================================
        
          FUNCTION:  lte_ml1_sm_nv_ue_bw_cfg_size
        
 ===========================================================================*/
/*!
      @brief
      This function returns the size of EFS variable in bytes. 
      This size information is used by EFS framework for memory 
      allocation and storing the EFS value.
               
      @details
        
      @return size in bytes
 */
/*=========================================================================*/

uint8 lte_ml1_sm_nv_ue_bw_cfg_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_ue_bw_cfg_defaultvalue
 
===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_ue_bw_cfg_defaultvalue(void *ptr);


/*===========================================================================
    
    FUNCTION:  lte_ml1_sm_nv_ue_bw_cfg_post_efsread
    
 ===========================================================================*/
/*!
        @brief
        This function will be called post efsread success by framework, 
        to perform client defined post efsread functionality.
    
        @details
        .
    
        @return
*/
/*=========================================================================*/

void  lte_ml1_sm_nv_ue_bw_cfg_post_efsread(void *ptr);

/*===========================================================================
        
          FUNCTION:  lte_ml1_sm_nv_earfcn_bw_cfg_size
        
 ===========================================================================*/
/*!
      @brief
      This function returns the size of EFS variable in bytes. 
      This size information is used by EFS framework for memory 
      allocation and storing the EFS value.
               
      @details
        
      @return size in bytes
 */
/*=========================================================================*/


uint8 lte_ml1_sm_nv_earfcn_bw_cfg_size(void);


/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_earfcn_bw_cfg_defaultvalue
 
===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_earfcn_bw_cfg_defaultvalue(void *ptr);

/*===========================================================================
    
    FUNCTION:  lte_ml1_sm_nv_earfcn_bw_cfg_post_efsread
    
 ===========================================================================*/
/*!
        @brief
        This function will be called post efsread success by framework, 
        to perform client defined post efsread functionality.
    
        @details
        .
    
        @return
*/
/*=========================================================================*/
void  lte_ml1_sm_nv_earfcn_bw_cfg_post_efsread(void *ptr);


/*===========================================================================
         
           FUNCTION:  lte_ml1_sm_nv_ml1_ext_dbg_size
         
 ===========================================================================*/
/*!
       @brief
       This function returns the size of EFS variable in bytes. 
       This size information is used by EFS framework for memory 
       allocation and storing the EFS value.
                
       @details
         
       @return size in bytes
 */
/*=========================================================================*/


uint8 lte_ml1_sm_nv_ml1_ext_dbg_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_sm_nv_ml1_ext_dbg_defaultvalue
 
===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_ml1_ext_dbg_defaultvalue(void *ptr);

/*===========================================================================
         
           FUNCTION:  lte_ml1_sm_nv_num_ant_size
         
 ===========================================================================*/
/*!
       @brief
       This function returns the size of EFS variable in bytes. 
       This size information is used by EFS framework for memory 
       allocation and storing the EFS value.
                
       @details
         
       @return size in bytes
 */
/*=========================================================================*/

uint8 lte_ml1_sm_nv_num_ant_size(void);

/*===========================================================================
  
    FUNCTION:  lte_ml1_sm_nv_num_ant_defaultvalue
  
 ===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_num_ant_defaultvalue(void *ptr);

/*===========================================================================
    
    FUNCTION:	lte_ml1_sm_nv_num_ant_post_efsread
    
 ===========================================================================*/
/*!
    @brief
    This function will be called post efsread success by framework, 
    to perform client defined post efsread functionality.
    
    @details
    .
    
    @return
*/
/*=========================================================================*/

void	lte_ml1_sm_nv_num_ant_post_efsread(void *ptr);


/*===========================================================================
            
        FUNCTION:	lte_ml1_sm_nv_offline_thresh_size
            
===========================================================================*/
/*!
   @brief
   This function returns the size of EFS variable in bytes. 
   This size information is used by EFS framework for memory 
   allocation and storing the EFS value.
                  
   @details
           
   @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_offline_thresh_size(void);

/*===========================================================================
  
    FUNCTION:	lte_ml1_sm_nv_offline_thresh_defaultvalue
  
 ===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
 
     @return
*/
/*=========================================================================*/
void lte_ml1_sm_nv_offline_thresh_defaultvalue(void *ptr);


/*===========================================================================
              
      FUNCTION:  lte_ml1_sm_nv_offline_panic_thresh_size
              
 ===========================================================================*/
/*!
      @brief
      This function returns the size of EFS variable in bytes. 
      This size information is used by EFS framework for memory 
      allocation and storing the EFS value.
                     
      @details
              
      @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_offline_panic_thresh_size(void);


/*===========================================================================
    
      FUNCTION:  lte_ml1_sm_nv_offline_panic_thresh_defaultvalue
    
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
    
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
    
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_offline_panic_thresh_defaultvalue(void *ptr);


/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_min_nbr_rsrp_size
                
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                       
        @details
                
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_min_nbr_rsrp_size(void);


/*===========================================================================
      
        FUNCTION:  lte_ml1_sm_nv_offline_panic_thresh_defaultvalue
      
===========================================================================*/
/*!
      @brief
      This function provides the default value for the efs variable. 
      This data will be stored with the EFS framework.
      
      @details
      EFS framework sends the pointer to memory(which framework owns), 
      to fill in the default value.
      
      @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_min_nbr_rsrp_defaultvalue(void *ptr);

/*===========================================================================
                  
          FUNCTION:  lte_ml1_sm_nv_enable_filtering_size
                  
===========================================================================*/
/*!
          @brief
          This function returns the size of EFS variable in bytes. 
          This size information is used by EFS framework for memory 
          allocation and storing the EFS value.
                         
          @details
                  
          @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_enable_filtering_size(void);

/*===========================================================================
        
          FUNCTION:  lte_ml1_sm_nv_enable_filtering_defaultvalue
        
===========================================================================*/
/*!
        @brief
        This function provides the default value for the efs variable. 
        This data will be stored with the EFS framework.
        
        @details
        EFS framework sends the pointer to memory(which framework owns), 
        to fill in the default value.
        
        @return
 */
/*=========================================================================*/

void lte_ml1_sm_nv_enable_filtering_defaultvalue(void *ptr);

/*===========================================================================
                    
    FUNCTION:  lte_ml1_sm_nv_use_rsrp_inst_size
                    
 ===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                           
    @details
                    
    @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_use_rsrp_inst_size(void);


/*===========================================================================
          
    FUNCTION:  lte_ml1_sm_nv_use_rsrp_inst_defaultvalue
          
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
          
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
          
    @return
 */
/*=========================================================================*/

void lte_ml1_sm_nv_use_rsrp_inst_defaultvalue(void *ptr);

/*===========================================================================
                      
      FUNCTION:  lte_ml1_sm_nv_sss_thresh_db_size
                      
===========================================================================*/
/*!
      @brief
      This function returns the size of EFS variable in bytes. 
      This size information is used by EFS framework for memory 
      allocation and storing the EFS value.
                             
      @details
                      
      @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_sss_thresh_db_size(void);


/*===========================================================================
            
      FUNCTION:  lte_ml1_sm_nv_sss_thresh_db_defaultvalue
            
===========================================================================*/
/*!
      @brief
      This function provides the default value for the efs variable. 
      This data will be stored with the EFS framework.
            
      @details
      EFS framework sends the pointer to memory(which framework owns), 
      to fill in the default value.
            
      @return
 */
/*=========================================================================*/

void lte_ml1_sm_nv_sss_thresh_db_defaultvalue(void *ptr);

/*===========================================================================
                        
    FUNCTION:  lte_ml1_sm_nv_one_ifreq_layer_size
                        
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                               
    @details
                        
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_one_ifreq_layer_size(void);

/*===========================================================================
            
      FUNCTION:  lte_ml1_sm_nv_one_ifreq_layer_defaultvalue
            
===========================================================================*/
/*!
      @brief
      This function provides the default value for the efs variable. 
      This data will be stored with the EFS framework.
            
      @details
      EFS framework sends the pointer to memory(which framework owns), 
      to fill in the default value.
            
      @return
 */
/*=========================================================================*/
void lte_ml1_sm_nv_one_ifreq_layer_defaultvalue(void *ptr);

/*===========================================================================
                          
      FUNCTION: lte_ml1_sm_nv_srch_prune_cnt_size 
                          
===========================================================================*/
/*!
      @brief
      This function returns the size of EFS variable in bytes. 
      This size information is used by EFS framework for memory 
      allocation and storing the EFS value.
                                 
      @details
                          
      @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_srch_prune_cnt_size(void);


/*===========================================================================
              
      FUNCTION:  lte_ml1_sm_nv_srch_prune_cnt_defaultvalue
              
===========================================================================*/
/*!
      @brief
       This function provides the default value for the efs variable. 
       This data will be stored with the EFS framework.
              
       @details
       EFS framework sends the pointer to memory(which framework owns), 
       to fill in the default value.
              
      @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_srch_prune_cnt_defaultvalue(void *ptr);


/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_meas_prune_cnt_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/



uint8 lte_ml1_sm_nv_meas_prune_cnt_size(void);


/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_meas_prune_cnt_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable. 
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns), 
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_meas_prune_cnt_defaultvalue(void *ptr);


/*===========================================================================
                            
        FUNCTION:  lte_ml1_sm_nv_sample_rec_times_size
                            
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                                   
        @details
                            
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_sample_rec_times_size(void);

/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_sample_rec_times_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable. 
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns), 
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_sample_rec_times_defaultvalue(void *ptr);

/*===========================================================================
                            
        FUNCTION:  lte_ml1_sm_nv_ignore_tresel_cfg_size
                            
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                                   
        @details
                            
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_ignore_tresel_cfg_size(void);

/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_ignore_tresel_cfg_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable.
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns),
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_ignore_tresel_cfg_defaultvalue(void *ptr);


/*===========================================================================
                            
        FUNCTION:  lte_ml1_sm_nv_offline_panic_use_rsrq_size
                            
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                                   
        @details
                            
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_offline_panic_use_rsrq_size(void);

/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_offline_panic_use_rsrq_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable.
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns), 
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_offline_panic_use_rsrq_defaultvalue(void *ptr);

/*===========================================================================
                            
        FUNCTION:  lte_ml1_sm_nv_Sintra_size
                            
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                                   
        @details
                            
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_Sintra_size(void);


/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_Sintra_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable. 
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns), 
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_Sintra_defaultvalue(void *ptr);


/*===========================================================================
                            
        FUNCTION:  lte_ml1_sm_nv_Snonintra_size
                            
===========================================================================*/
/*!
        @brief
        This function returns the size of EFS variable in bytes. 
        This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
                                   
        @details
                            
        @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_Snonintra_size(void);

/*===========================================================================
                
        FUNCTION:  lte_ml1_sm_nv_Snonintra_defaultvalue
                
===========================================================================*/
/*!
        @brief
         This function provides the default value for the efs variable. 
         This data will be stored with the EFS framework.
                
         @details
         EFS framework sends the pointer to memory(which framework owns), 
         to fill in the default value.
                
        @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_Snonintra_defaultvalue(void *ptr);

/*===========================================================================
                             
     FUNCTION:	lte_ml1_sm_nv_onex_fake_meas_enabled_size
                             
===========================================================================*/
/*!
     @brief
     This function returns the size of EFS variable in bytes. 
     This size information is used by EFS framework for memory 
     allocation and storing the EFS value.
                                    
     @details
                             
     @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_onex_fake_meas_enabled_size(void);


/*===========================================================================
                 
     FUNCTION:	lte_ml1_sm_nv_onex_fake_meas_enabled_defaultvalue
                 
===========================================================================*/
/*!
     @brief
     This function provides the default value for the efs variable. 
     This data will be stored with the EFS framework.
                 
     @details
     EFS framework sends the pointer to memory(which framework owns), 
     to fill in the default value.
                 
     @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_onex_fake_meas_enabled_defaultvalue(void *ptr);


/*===========================================================================
     
     FUNCTION:	lte_ml1_sm_nv_onex_conn_config_pre_efsread
     
 ===========================================================================*/
/*!
         @brief
         This function will be called pre efsread  by framework, 
         to perform client defined pre efsread functionality.
     
         @details
         .
     
         @return
*/
/*=========================================================================*/

void	lte_ml1_sm_nv_onex_conn_config_pre_efsread(char *efsfilepath);

/*===========================================================================
       
       FUNCTION:  lte_ml1_sm_nv_onex_conn_config_pre_efsread
       
===========================================================================*/
/*!
           @brief
           This function will be called pre efsread  by framework, 
           to perform client defined pre efsread functionality.
       
           @details
           .
       
           @return
*/
/*=========================================================================*/

void	lte_ml1_sm_nv_hrpd_conn_config_pre_efsread(char *efsfilepath);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_num_enb_tx_ant_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_num_enb_tx_ant_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_num_enb_tx_ant_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_num_enb_tx_ant_defaultvalue(void *ptr);

/*===========================================================================
       
       FUNCTION:  lte_ml1_sm_nv_onex_conn_config_pre_efsread
       
===========================================================================*/
/*!
    @brief
    This function will be called pre efsread  by framework, 
    to perform client defined pre efsread functionality.
       
    @details
    .
       
    @return
*/
/*=========================================================================*/

void	lte_ml1_sm_nv_num_enb_tx_ant_post_efsread(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_verify_sib8_systime_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes.
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_verify_sib8_systime_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_verify_sib8_systime_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable.
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_verify_sib8_systime_defaultvalue(void *ptr);

/*===========================================================================
                           
   FUNCTION:  lte_ml1_sm_nv_check_for_sfn_mismatch_size
                           
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_check_for_sfn_mismatch_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_check_for_sfn_mismatch_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_check_for_sfn_mismatch_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_ext_cp_cfg_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_ext_cp_cfg_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_ext_cp_cfg_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_ext_cp_cfg_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_acq_use_blacklist_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_acq_use_blacklist_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_acq_use_blacklist_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_acq_use_blacklist_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_meas_san_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_meas_san_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_meas_san_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_meas_san_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_default_meas_cfg_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_default_meas_cfg_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_default_meas_cfg_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_default_meas_cfg_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_rlf_on_ttl_drift_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_rlf_on_ttl_drift_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_rlf_on_ttl_drift_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_rlf_on_ttl_drift_defaultvalue(void *ptr);

/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_cell_list_params_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes.
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_cell_list_params_size(void);

/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_cell_list_params_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable.
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_cell_list_params_defaultvalue(void *ptr);


/*===========================================================================
                            
    FUNCTION:  lte_ml1_sm_nv_ue_camp_cfg_size
                            
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
                                   
    @details
                            
    @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_ue_camp_cfg_size(void);


/*===========================================================================
                
    FUNCTION:  lte_ml1_sm_nv_ue_camp_cfg_defaultvalue
                
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
                
    @details
    EFS framework sends the pointer to memory(which framework owns), 
    to fill in the default value.
                
    @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_ue_camp_cfg_defaultvalue(void *ptr);


/*===========================================================================
                         
 FUNCTION:	lte_ml1_sm_nv_acq_after_conn_rel_threshold_size
                         
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                
 @details
                         
 @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_acq_after_conn_rel_threshold_size(void);


/*===========================================================================
             
 FUNCTION:	lte_ml1_sm_nv_acq_after_conn_rel_threshold_defaultvalue
             
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
             
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
             
 @return
*/
/*=========================================================================*/


void lte_ml1_sm_nv_acq_after_conn_rel_threshold_defaultvalue(void *ptr);



/*===========================================================================
                          
  FUNCTION:  lte_ml1_sm_nv_prune_overlaping_w_freq_size
                          
===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes. 
  This size information is used by EFS framework for memory 
  allocation and storing the EFS value.
                                 
  @details
                          
  @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_prune_overlaping_w_freq_size(void);


/*===========================================================================
              
  FUNCTION:  lte_ml1_sm_nv_prune_overlaping_w_freq_defaultvalue
              
===========================================================================*/
/*!
  @brief
  This function provides the default value for the efs variable. 
  This data will be stored with the EFS framework.
              
  @details
  EFS framework sends the pointer to memory(which framework owns), 
  to fill in the default value.
              
  @return
*/
/*=========================================================================*/


void lte_ml1_sm_nv_prune_overlaping_w_freq_defaultvalue(void *ptr);

/*===========================================================================
                         
 FUNCTION:	lte_ml1_sm_nv_stickon_ltecsg_enable_size
                         
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                
 @details
                         
 @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_stickon_ltecsg_enable_size(void);


/*===========================================================================
             
 FUNCTION:	lte_ml1_sm_nv_stickon_ltecsg_enable_defaultvalue
             
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
             
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
             
 @return
*/
/*=========================================================================*/


void lte_ml1_sm_nv_stickon_ltecsg_enable_defaultvalue(void *ptr);

/*===========================================================================
                           
   FUNCTION:  lte_ml1_sm_nv_enable_vcell_mode_size
                           
  ===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                
 @details
                         
 @return size in bytes
*/
/*=========================================================================*/


uint8 lte_ml1_sm_nv_enable_vcell_mode_size(void);



/*===========================================================================
             
 FUNCTION:  lte_ml1_sm_nv_enable_vcell_mode_defaultvalue
             
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
             
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
             
 @return
*/
/*=========================================================================*/


void lte_ml1_sm_nv_enable_vcell_mode_defaultvalue(void *ptr);

/*===========================================================================
                         
 FUNCTION:  lte_ml1_sm_nv_enable_prsrq_size
                         
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                
 @details
                         
 @return size in bytes
*/
/*=========================================================================*/


uint8  lte_ml1_sm_nv_enable_prsrq_size(void);



/*===========================================================================
             
 FUNCTION:  lte_ml1_sm_nv_enable_prsrq_defaultvalue
             
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
             
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
             
 @return
*/
/*=========================================================================*/


void lte_ml1_sm_nv_enable_prsrq_defaultvalue(void *ptr);

/*===========================================================================
                                                     
     FUNCTION:  lte_ml1_sm_nv_s_criteria_fail_trigger_meas_size
                                                     
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                
 @details
                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_s_criteria_fail_trigger_meas_size(void);

/*===========================================================================
                                                 
 FUNCTION:  lte_ml1_sm_nv_s_criteria_fail_trigger_meas_defaultvalue
                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                         
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                         
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_s_criteria_fail_trigger_meas_defaultvalue(void *ptr);


/*===========================================================================
                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_number_of_cell_select_meas_size
                                                                                                 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                
 @details
                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_number_of_cell_select_meas_size(void);

/*===========================================================================
                                                                                                                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_number_of_cell_select_meas_defaultvalue
                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_s_criteria_fail_trigger_meas_defaultvalue(void *ptr);

/*===========================================================================
                                                                                                                                                                                         
FUNCTION:  lte_ml1_sm_nv_number_of_cell_select_meas_size
                                                                                                                                                                                         
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                                                                                                                                                
 @details
                                                                                                                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_number_of_cell_select_meas_size(void);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_number_of_cell_select_meas_defaultvalue
                                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_number_of_cell_select_meas_defaultvalue(void *ptr); 

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                 
FUNCTION:  lte_ml1_sm_nv_time_delta_cell_select_meas_size
                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 @details
                                                                                                                                                                                                                                                                                                                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_time_delta_cell_select_meas_size(void);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_time_delta_cell_select_meas_defaultvalue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                                                                                                                                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                                                                                                                                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_time_delta_cell_select_meas_defaultvalue(void *ptr);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                 
FUNCTION:  lte_ml1_nv_fw_timers_size
                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 @details
                                                                                                                                                                                                                                                                                                                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_nv_fw_timers_size(void);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
FUNCTION:  lte_ml1_nv_fw_timers_defaultvalue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                                                                                                                                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                                                                                                                                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_nv_fw_timers_defaultvalue(void *ptr);

/*===========================================================================
             
 FUNCTION:  lte_ml1_sm_nv_is_cdrx_off_scheduling_size
             
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                
 @details
             
 @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_is_cdrx_off_scheduling_size(void);

/*===========================================================================
       
 FUNCTION:  lte_ml1_sm_nv_is_cdrx_off_scheduling_defaultvalue
       
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
       
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
       
 @return
*/
/*=========================================================================*/
void lte_ml1_sm_nv_is_cdrx_off_scheduling_defaultvalue(void *ptr);


/*===========================================================================
						   
   FUNCTION:  lte_ml1_sm_nv_rsrp_acq_threshold_size
						   
===========================================================================*/
  /*!
   @brief
   This function returns the size of EFS variable in bytes. 
   This size information is used by EFS framework for memory 
   allocation and storing the EFS value.
								  
   @details
						   
   @return size in bytes
  */
/*=========================================================================*/
uint8 lte_ml1_sm_nv_rsrp_acq_threshold_size(void);

/*===========================================================================
			   
   FUNCTION:  lte_ml1_sm_nv_rsrp_acq_threshold_defaultvalue
			   
===========================================================================*/
  /*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.
			   
   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.
			   
   @return
  */
/*=========================================================================*/
void lte_ml1_sm_nv_rsrp_acq_threshold_defaultvalue(void *ptr);

/*===========================================================================
						   
   FUNCTION:  lte_ml1_sm_nv_rsrp_acq_sf_delay_size
						   
===========================================================================*/
  /*!
   @brief
   This function returns the size of EFS variable in bytes. 
   This size information is used by EFS framework for memory 
   allocation and storing the EFS value.
								  
   @details
						   
   @return size in bytes
  */
/*=========================================================================*/
uint8 lte_ml1_sm_nv_rsrp_acq_sf_delay_size(void);

/*===========================================================================
			   
   FUNCTION:  lte_ml1_sm_nv_rsrp_acq_sf_delay_defaultvalue
			   
===========================================================================*/
  /*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.
			   
   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.
			   
   @return
  */
/*=========================================================================*/
void lte_ml1_sm_nv_rsrp_acq_sf_delay_defaultvalue(void *ptr);

/*=============================================================================
                                                                                                                                                                                                                                                                                                                                                                                 
FUNCTION:  lte_ml1_sm_nv_acq_max_retry_size
                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 @details
                                                                                                                                                                                                                                                                                                                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_acq_max_retry_size(void);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_acq_max_retry_defaultvalue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                                                                                                                                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                                                                                                                                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_acq_max_retry_defaultvalue(void *ptr);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                 
FUNCTION:  lte_ml1_sm_nv_max_search_meas_retry_size
                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 @details
                                                                                                                                                                                                                                                                                                                                                                                                 
 @return size in bytes
*/
/*=========================================================================*/

uint8 lte_ml1_sm_nv_max_search_meas_retry_size(void);

/*===========================================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 FUNCTION:  lte_ml1_sm_nv_max_search_meas_retry_defaultvalue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
                                                                                                                                                                                                 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
                                                                                                                                                                                                 
 @return
*/
/*=========================================================================*/

void lte_ml1_sm_nv_max_search_meas_retry_defaultvalue(void *ptr); 

/*===========================================================================
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 
FUNCTION:  lte_ml1_sm_nv_max_cell_select_meas_retry_dsda_size
 
===========================================================================*/
/*!
 @brief
 This function returns the size of EFS variable in bytes. 
 This size information is used by EFS framework for memory 
 allocation and storing the EFS value.																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																							
																																																																																
 @details
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 
 @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_max_cell_select_meas_retry_dsda_size(void);

/*===========================================================================																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																					
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 
 FUNCTION:  lte_ml1_sm_nv_max_cell_select_meas_retry_dsda_defaultvalue
 
===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable. 
 This data will be stored with the EFS framework.
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 
 @details
 EFS framework sends the pointer to memory(which framework owns), 
 to fill in the default value.
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 
 @return
*/
/*=========================================================================*/
void lte_ml1_sm_nv_max_cell_select_meas_retry_dsda_defaultvalue(void *ptr);

#endif
