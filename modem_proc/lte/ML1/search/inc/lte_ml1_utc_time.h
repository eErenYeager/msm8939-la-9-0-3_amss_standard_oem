/*!
  @file
  lte_ml1_utc_time.h

  @brief
  This file describes the UTC time interface to support upper layer time
  configuration requests.
*/

/*=============================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/search/inc/lte_ml1_utc_time.h#1 $

when       who   what, where, why
--------   ---   -------------------------------------------------------------- 
04/01/13   awj   Initial version - sib16 support
=============================================================================*/
#ifndef LTE_ML1_UTC_TIME_H
#define LTE_ML1_UTC_TIME_H

/*=============================================================================

                                INCLUDE FILES

=============================================================================*/
#include "lte_ml1_comdef.h"
#include "lte_cphy_msg.h"
#include <lte_ml1_mem.h>

/*=============================================================================

                         EXTERNAL DEFINITIONS AND TYPES

=============================================================================*/
/*===========================================================================

                        INTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*! Global GPS system time data structure */
typedef struct
{
  boolean                       enabled;
  /*! GPS time from SIB16 in uints of ms */
  uint64                        gps_systime_in_ms;

  /*! LTE SFN value 0xFFFF indicates invalid value */
  lte_ml1_schdlr_sf_time_t      lte_sfn;

  /*! LTE subframe count */
  uint32                        lte_subf_cnt;
  
  /*! utc time get from SIB16 base from Jan 6th 1980 in units of ms */
  uint64                        utc_time_in_ms;

  /*! UTC leapSeconds in units of ms */
  int16                         leapSeconds_in_ms;

  /*! LTE OSTMR value (uncorrected raw value) */
  uint32                        lte_ostmr;

} lte_ml1_gps_systime_data_s;

typedef struct
{
  /*! Current cell GPS system time */
  lte_ml1_gps_systime_data_s cur_cell;

  /*! Current GPS system time */
  lte_ml1_gps_systime_data_s now;
           
} lte_ml1_gps_systime_s;
/*=============================================================================

                        EXTERNAL FUNCTION PROTOTYPES

=============================================================================*/
/*===========================================================================

  FUNCTION:  lte_ml1_mdb_gps_systime_alloc

===========================================================================*/
/*!
  @brief
  This function allocates the SIB16 gps system time database structures.

  @return
  None

*/
/*=========================================================================*/
void lte_ml1_mdb_gps_systime_alloc
( 
  lte_mem_instance_type  instance 
);
/*===========================================================================

  FUNCTION:  lte_ml1_mdb_gps_systime_dealloc

===========================================================================*/
/*!
  @brief
  This function deallocates the SIB16 GPS system time database structures.

  @return
  None

*/
/*=========================================================================*/
void lte_ml1_mdb_gps_systime_dealloc
( 
   lte_mem_instance_type  instance 
);
/*===========================================================================

  FUNCTION:  lte_ml1_mdb_gps_systime_init

===========================================================================*/
/*!
    @brief
    This function initializes the GPS system time

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_mdb_gps_systime_init
( 
   lte_mem_instance_type  instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_gps_systime_at_sysframe

===========================================================================*/
/*!
    @brief
    This function returns the GPS system time at on the last
    system frame boundary.

    @return
    Pointer to the GPS system time data on the last system frame boundary

*/
/*=========================================================================*/
lte_ml1_gps_systime_data_s * lte_ml1_gps_systime_at_sysframe(void);

/*===========================================================================

  FUNCTION: lte_ml1_utc_time_update

===========================================================================*/
/*!
    @brief
    Called by source RAT to reserve TRM resources for reselection

    @param[in] utc_time_update_req sib16 data from upper layer

    @retval None
*/
/*=========================================================================*/
void lte_ml1_utc_time_update
(
  lte_mem_instance_type instance,
  /*! The eUTRAN RF channel number to lock */
  lte_cphy_utc_time_update_req_s  *utc_time_update_req
);

#endif /* LTE_ML1_UTC_TIME_H */
