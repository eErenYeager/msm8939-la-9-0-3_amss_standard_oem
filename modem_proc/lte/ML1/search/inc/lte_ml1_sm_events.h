
/*!
  @file
  lte_ml1_sm_events.h

  @brief
  ML1-SM events support header

  @detail
*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



when        who       what, where, why
--------    ---       ---------------------------------------------------------- 
11/12/13   lec        reorganized sm_events.h and sm_events.c
09/13/13   lec        added event for DRX cycle activity 

===========================================================================*/

#ifndef LTE_ML1_SM_EVENTS_H
#define LTE_ML1_SM_EVENTS_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "lte_ml1_comdef.h"
#include "lte_ml1_mdb.h"
#include "lte_ml1_mem.h"

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/* Macros for payload IDs of Search Idle Event */
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_INVALID  0

#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_PRSNEED  1
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_RACHFAIL  2
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_PRNTIFAIL  3
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SNRLTTHRSH  4
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_RSRQLTTHRSH  5
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SLTZ  6
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_INTRA  7
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_DRX  8

#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_RESERVED  9
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_TDS  10
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_GSM  11
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_WCDMA  12
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_1X  13
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_HRPD  14
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_INTER  15
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_INTRA  16

#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_RESERVED  17
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_TDSCDMA  18
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_WCDMA  19
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_RESERVED2  20
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_RESERVED3  21
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_PBCH  22
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_INTER  23
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_INTRA  24

#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_MEASCFGUP  25
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_NGBRSRCHMEAS  26
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_BSMP  27
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_PBCH  28
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SERVRESEL  29
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SERVMEAS  30
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_RUDE  31
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_OFFLINE  32


/* Macros for Panic payload shifts of Search Idle Event */
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_PRSNEED  0
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_RACHFAIL  1
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_PRNTIFAIL  2
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_SNRLTTHRSH  3
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_RSRQLTTHRSH  4
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_SLTZ  5
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_INTRA  6
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_PANIC_SHF_DRX  7


/* Macros for Measurement payload shifts of Search Idle Event */
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_RESERVED  0
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_TDS  1
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_GSM  2
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_WCDMA  3
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_1X  4
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_HRPD  5
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_INTER  6
#define   LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_MEAS_SHF_INTRA  7


/* Macros for Search payload shifts of Search Idle Event */
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_RESERVED  0
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_TDSCDMA  1
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_WCDMA  2
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_RESERVED2  3
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_RESERVED3  4
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_PBCH  5
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_INTER  6
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_SRCH_SHF_INTRA  7


/* Macros for Wake payload of shifts Search Idle Event */
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_MEASCFGUP  0
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_NGBRSRCHMEAS  1
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_BSMP  2
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_PBCH  3
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_SERVRESEL  4
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_R_SHF_SERVMEAS  5
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_SHF_RUDE  6
#define  LTE_ML1_SM_SEARCH_IDLE_EVENT_PAYLOAD_WAKE_SHF_OFFLINE  7

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log

===========================================================================*/
/*!
  @brief
  Logs SM Search Idle Event and clears it.  Initializes if unitialized.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log
(
  void
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_set

===========================================================================*/
/*!
  @brief
  Allows fields in the log to be populated.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_set
(
  uint8                 payload
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log_iratmeas

===========================================================================*/
/*!
  @brief
  Handles logging of irat measurements based on layer pointer rat type.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log_iratmeas
(
  /* Layer that was measured */
  lte_ml1_sm_layer_s *  layer_ptr
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log_iratsrch

===========================================================================*/
/*!
  @brief
  Handles logging of irat searches based on layer pointer rat type.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log_iratsrch
(
  /* RAT type that was searched */
  lte_rat_e             rat_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log_ngbrmeas

===========================================================================*/
/*!
  @brief
  Handles logging of ngbr measurements.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log_ngbrmeas
(
  lte_ml1_mdb_cell_struct_s *  serv_cell_ptr,
  lte_ml1_sm_layer_s        *  active_layer_ptr
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log_ngbrsrch

===========================================================================*/
/*!
  @brief
  Handles logging of ngbr searches and TDD search.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log_ngbrsrch
(
  lte_ml1_mdb_cell_struct_s *  serv_cell_ptr,
  lte_ml1_sm_layer_s        *  active_layer_ptr,
  lte_l1_cell_duplex_e         sys_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_search_idle_event_log_ngbrsrch

===========================================================================*/
/*!
  @brief
  Handles logging of ngbr searches and TDD search.

  @return
  None.
*/
/*=========================================================================*/
void lte_ml1_sm_search_idle_event_log_panic
(
  boolean               prnti_fail, 
  boolean               prs_srch_needed, 
  boolean               rach_fail,
  boolean               rsrq_fail,
  boolean               snr_fail
);

#endif
