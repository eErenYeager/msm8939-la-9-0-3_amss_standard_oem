/*!
  @file
  lte_ml1_sm_conn_inter_freq.h

  @brief

*/

/*===========================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/search/inc/lte_ml1_sm_conn_inter_freq.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/02/11   ub     Corrected input nbr srch/meas cnf input data type
11/23/10   nl     Initial version

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#ifndef LTE_ML1_SM_CONN_INTER_FREQ_H
#define LTE_ML1_SM_CONN_INTER_FREQ_H



#include "lte_variation.h"
#include "comdef.h"
#include "lte_ml1_common.h"
#include "lte_ml1_plist.h"
#include "lte_ml1_gapmgr.h"
#include "lte_ml1_sm_conn_meas_inter.h"
#include <lte_ml1_mem.h>
#ifdef FEATURE_LTE_RF
#ifdef FEATURE_RF_LTE_LM_RFM_INTERFACE
#include "rxlm_intf.h"
#endif
#endif

/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief Overall structure containing all gap related information */
typedef struct
{
  /*! Instance number */
  lte_mem_instance_type              instance_num;
  /*! Flag indicating if gap has been assigned and accepted */
  boolean                            gap_accepted; 
  /*! below fields have meaningful info only if gap has been accepted.*/

  /*! The current gap assignment */
  lte_ml1_gapmgr_gap_assignment_s    gap_assignment;

  /*! the purpose of this gap */
  lte_ml1_sm_conn_meas_inter_job_e   gap_purpose;  

   /*! Layer frequency */
  lte_earfcn_t                       earfcn;

  /*! Measurement bandwidth */
  lte_bandwidth_e                    meas_bandwidth;

} lte_ml1_sm_conn_inter_freq_gap_s;

typedef enum
{
  LTE_SM_CONN_INTER_FREQ_TUNE_AWAY = 0,  
  LTE_SM_CONN_INTER_FREQ_TUNE_BACK  
}lte_ml1_sm_conn_inter_freq_tune_type_e;



/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/


/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_instance_init

===========================================================================*/
/*!
    @brief
    This function initializes the instance memory.

    @return
    None.

*/
/*=========================================================================*/

void lte_ml1_sm_conn_inter_freq_instance_init
(
  lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_instance_deinit

===========================================================================*/
/*!
    @brief
    This function deinitializes the instance memory.

    @return
    None.

*/
/*=========================================================================*/

void lte_ml1_sm_conn_inter_freq_instance_deinit
( 
  lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gap_init

===========================================================================*/
/*!
    @brief
    This function  initializes the data structures related to the gap.
    machine

    @detail

    @return


*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_gap_init(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gap_in_progress

===========================================================================*/
/*!
    @brief
    This function  determines if gap is in progress or not.
    note: the procedure returns true from the time the gap gets accepted
    till the time all the gap related activities get completed (which
    may continue till after the gap).
    

    @detail
    This function  determines if gap is in progress or not.
    note: the procedure returns true from the time the gap gets accepted
    till the time all the gap related activities get completed (which
    may continue till after the gap).

    @return
    true if the gap is in progress , false otherwise.


*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_gap_in_progress(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gap_check_current_gap_purpose

===========================================================================*/
/*!
    @brief
    This function  returns the purpose of the gap as evaluated by inter-frequecy
    module (AGC only/search,meas/meas only)    

    @detail
     This function  returns the purpose of the gap as evaluated by inter-frequecy
    module (AGC only/search,meas/meas only)    

    @return
    purpose the gap is being used for


*/
/*=========================================================================*/
lte_ml1_sm_conn_meas_inter_job_e lte_ml1_sm_conn_inter_freq_gap_check_current_gap_purpose(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_get_gap_start_time

===========================================================================*/
/*!
    @brief
     This function  returns the gap start time.    

    @detail
     This function  returns the gap start time.  

    @return
    gap start time.


*/
/*=========================================================================*/
lte_ml1_schdlr_sf_time_t lte_ml1_sm_conn_inter_freq_get_gap_start_time(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_register_with_gapmgr

===========================================================================*/
/*!
    @brief
    this function registers with gap manager with all the call backs.
 

    @detail
    this function registers with gap manager with all the call backs.
    
    @return
    true if registration suceeds,false otherwise.
*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_register_with_gapmgr
(
  lte_mem_instance_type instance,
   /*! number of layers in the RAT */
  uint8    num_layers
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_init

===========================================================================*/
/*!
    @brief
    This function  is called by the inter-freq. module to
    activate the inter-freq stm. it also passes a pointer to the
    search/measumrent request strucutres being maintained by it.    

    @detail
    The stm_process_input function is called, passing in a payload structure.
    This structure contains the pointer to the search/measurement
    request, 

    @return
    true if state machine gets activated correctly, false otherwise.

*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_init 
(
  void
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_deinit

===========================================================================*/
/*!
    @brief
    This function  is called by the inter-freq. module to
    deactivate the inter-freq stm.    

    @detail
    the inter-freq stm is deactivated. 

    @return
    true if state machine gets deactivated correctly, false otherwise.

*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_deinit 
(
    void
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_rf_tune_cnf_cb

===========================================================================*/
/*!
    @brief
    This is RF tune cnf cb to handle RF tune cnf from LL

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_rf_tune_cnf_cb 
(
  /* Nbr srch cnf from LL */
  lte_LL1_sys_run_rf_script_cnf_msg_struct * rf_cnf_msg
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_nbr_srch_cnf_cb

===========================================================================*/
/*!
    @brief
    This is icb to route nbr srch cnf to ifreq stm 

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_nbr_srch_cnf_cb 
(
  /* Nbr srch cnf from LL */
  lte_LL1_srch_neighbor_cell_srch_cnf_msg_struct * nbr_srch_cnf
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_nbr_meas_cnf_cb

===========================================================================*/
/*!
    @brief
    This is icb to route nbr meas cnf to ifreq stm 

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_nbr_meas_cnf_cb 
(
  /* Nbr meas cnf from LL */
  lte_LL1_meas_ttl_ftl_ncell_cnf_msg_struct * nbr_meas_cnf
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gapmgr_rx_cfg_cb

===========================================================================*/
/*!
    @brief
    This is the rx config cb registered to the gapmgr.

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_gapmgr_rx_cfg_cb
( 
  lte_ml1_rx_cfg_req_s *rx_cfg_req
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gapmgr_abort_cb

===========================================================================*/
/*!
    @brief
    This is the abort cb registered to the gapmgr.

    @detail

    @return
    None
*/
/*=========================================================================*/
lte_ml1_gapmgr_rat_abort_value_e lte_ml1_sm_conn_inter_freq_gapmgr_abort_cb
( 
  /* Abort reason */
  lte_ml1_gapmgr_abort_reason_e abort_reason 
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gapmgr_assign_cb

===========================================================================*/
/*!
    @brief
    The is the gap assignment callback register to the gapmgr.

    @detail

    @return
    Enum indicating the gap assignment result 
*/
/*=========================================================================*/
lte_ml1_gapmgr_assign_result_e  lte_ml1_sm_conn_inter_freq_gapmgr_assign_cb
(
  /*! New assignment */
  lte_ml1_gapmgr_gap_assignment_s* assignment_ptr,
  lte_ml1_schdlr_sf_time_t          distance_to_tao 
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_gapmgr_pre_gap_cb

===========================================================================*/
/*!
    @brief
    The is the pre gap gap callback registered to the gapmgr.

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_gapmgr_pre_gap_cb( void );

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_tune_back_cb

===========================================================================*/
/*!
    @brief
    The is the tune back callback registered to the gapmgr.

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_tune_back_cb( void );

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_update_gap_info

===========================================================================*/
/*!
    @brief
    The function updates all the gap related info. associated with the
    the current gap.   

    @detail
    The function updates all the gap related info. associated with the
    current gap.   

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_update_gap_info
(
  lte_mem_instance_type instance,
    lte_ml1_sm_conn_meas_inter_gap_job_data_s*  gap_job_info,
    lte_ml1_gapmgr_gap_assignment_s*            gap_assign_ptr

);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_send_rf_tune

===========================================================================*/
/*!
    @brief
    Send RF tune command to firmware. this procedure will send both
    the tune away and tune back command back to the firmware.
 
     @detail
     Send RF tune command to firmware. this procedure will send both
    the tune away and tune back command back to the firmware.
 

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_send_rf_tune
(
  lte_mem_instance_type instance,
    lte_ml1_sm_conn_inter_freq_tune_type_e tune_type
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_wakeup_ind

===========================================================================*/
/*!
    @brief
    This function triggers an input to IFREQ STM on wakeup

    @detail

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_wakeup_ind( void );

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_state_is_init

===========================================================================*/
/*!
    @brief
    The is function returns TRUE if the inter freq stm is in init state

    @detail

    @return
    boolean
*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_stm_state_is_init
(
  void
);

#if defined(FEATURE_MODEM_TARGET_BUILD) && defined(FEATURE_LTE_RF)

#ifdef FEATURE_RF_LTE_LM_RFM_INTERFACE
/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_get_lm_handle

===========================================================================*/
/*!
    @brief
    This function obtains the RXLM buffer handle for inter-freq. for RX0, RX1

    @detail
    This function obtains the RXLM buffer handle for inter-freq. for RX0, RX1


    @return
    lm_handle_type

*/
/*=========================================================================*/
lm_handle_type lte_ml1_sm_conn_inter_freq_stm_get_lm_handle
(
  lte_ml1_rfmgr_lm_type_e lm_type
); 

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_stm_deallocate_rxlm_buffer

===========================================================================*/
/*!
    @brief
    This function deallocates the allocated RXLM buffers 

    @detail
    This function deallocates the allocated RXLM buffers 


    @return
    none

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_stm_deallocate_rxlm_buffer(void);
#endif

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_send_rf_tune_custom_earfcn

===========================================================================*/
/*!
    @brief
    Send RF tune command to firmware. this procedure will send both
    the tune away and tune back command back to the firmware.
 
     @detail
     Send RF tune command to firmware. this procedure will send both
    the tune away and tune back command back to the firmware.
 

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_send_rf_tune_custom_earfcn
(
    lte_mem_instance_type instance,
    lte_ml1_sm_conn_inter_freq_tune_type_e tune_type,
    lte_bandwidth_e meas_bandwidth,
    lte_earfcn_t earfcn,
    lte_ml1_schdlr_sf_time_t gap_start_time,
    void (*tuneaway_cb)(lte_ml1_rfmgr_msg_cnf_s*, void* ),
    lm_handle_type ant0_rxlm_handle,
    lm_handle_type ant1_rxlm_handle
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_send_build_rf_script_msg

===========================================================================*/
/*!
    @brief
    sends the message to RF to build scripts.
 
     @detail

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_send_build_rf_script_msg(lte_mem_instance_type instance, boolean abort_req); 

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_send_build_rf_script_msg_custom_earfcn

===========================================================================*/
/*!
    @brief
    sends the message to RF to build scripts on a custom earfcn/bw
 
     @detail

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_sm_conn_inter_freq_send_build_rf_script_msg_custom_earfcn
(
  lte_mem_instance_type instance,
  /*TRUE - send tune away/tune back request
    FALSE - send abort request*/
  boolean abort_req,
  /* E-ARFCN for the tune-away process */
  lte_earfcn_t earfcn,
  /* Bandwidth for the tune-away process */
  lte_bandwidth_e bandwidth,
  lm_handle_type ant0_rxlm_handle,
  lm_handle_type ant1_rxlm_handle
);

/*===========================================================================

  FUNCTION:  lte_ml1_sm_conn_inter_freq_rf_build_scripts_cnf_cb

===========================================================================*/
/*!
    @brief
    The is the RF build scripts cnf callback

    @detail

    @return
    None
*/
/*=========================================================================*/
boolean lte_ml1_sm_conn_inter_freq_rf_build_scripts_cnf_cb
(
  void *  msg
);
#endif

#endif  /* LTE_ML1_SM_CONN_INTER_FREQ_H */
