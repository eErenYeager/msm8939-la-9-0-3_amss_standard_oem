
#ifndef LTE_ML1_POS_CRS_H
#define LTE_ML1_POS_CRS_H
/*!
  @file
  lte_ml1_pos_rxtx.h

  @brief
  This module contains the interface for GPS RXTX.

*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/pos/inc/lte_ml1_pos_crs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/19/12   yp      Initial version
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <lte_cphy_msg.h>
#include <intf_sys_msg.h>
#include <cgps_api.h>
#include <msgr.h>

#include <lte_ml1_mem.h>

/*===========================================================================

                       EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief LTE POS RXTX state */
typedef enum {
  LTE_ML1_POS_CRS_STATE_NO_SERVICE, /*!< No service state */
  LTE_ML1_POS_CRS_STATE_IDLE,       /*!< Idle state */
  LTE_ML1_POS_CRS_STATE_CONNECTED,  /*!< Connected state */
  LTE_ML1_POS_CRS_STATE_MAX,        /*!< Max states */
} lte_ml1_pos_crs_state_t;  

/*===========================================================================

  FUNCTION:  lte_ml1_pos_crs_init

===========================================================================*/
/*!
    @brief
    This function initializes LTE GPS CRS module.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_pos_crs_init
(
  lte_mem_instance_type instance,
  msgr_client_t *client_ptr,   /*!< Message router client pointer          */
  msgr_id_t      mq_id         /*!< Lower layer message queue              */
);

/*===========================================================================

  FUNCTION:  lte_ml1_pos_crs_deinit

===========================================================================*/
/*!
    @brief
    This function deinitializes LTE GPS CRS module.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_pos_crs_deinit(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_pos_crs_process_message

===========================================================================*/
/*!
    @brief
    This function processes a POS CRS message.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_pos_crs_process_message
(
  lte_mem_instance_type instance,
  /*! Message UMID */
  msgr_umid_type  msg_umid,
  /*! Message buf pointer */
  uint8          *msg_buf
);

/*===========================================================================

  FUNCTION:  lte_ml1_pos_crs_enable

===========================================================================*/
/*!
    @brief
    This function enables GPS CRS.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_pos_crs_enable(lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_pos_crs_disable

===========================================================================*/
/*!
    @brief
    This function disables GPS CRS.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_pos_crs_disable(lte_mem_instance_type instance);

#endif
