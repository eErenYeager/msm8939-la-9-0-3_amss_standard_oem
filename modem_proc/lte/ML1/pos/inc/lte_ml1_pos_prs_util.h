/*!
  @file
  lte_ml1_pos_prs_util.h

  @brief
  This file provides the interface for utility functions
 
*/

/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.



$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/pos/inc/lte_ml1_pos_prs_util.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/06/11   yp      Initial version
===========================================================================*/

#include "lte_variation.h"
#include <customer.h>
#include <lte_cphy_msg.h>
#include <lte_cphy_test_msg.h>
#include "lte_ml1_comdef.h"
#include "lte_ml1_log.h"
#include "lte_ml1_pos_prs.h"
#include <lte_log_codes.h>
#include <mcs_timer.h>
#include <loc_lte_otdoa_api.h>
#include <intf_prs.h>
#include <intf_dl_msg.h>
#include <log.h>
#include <msgr.h>
#include <stm2.h>
#ifndef T_WINNT//#ifdef FEATURE_MODEM_TARGET_BUILD
#include <lte_LL1_smem.h>
#endif
#include <lte_assert.h>
#include <lte_log_codes.h>

#include "lte_ml1_common_paging_drx.h"
#include "lte_ml1_comdef.h"
#include "lte_ml1_common.h"
#include "lte_ml1_log.h"
#include "lte_ml1_mdb.h"
#include "lte_ml1_pos_msg.h"
#include "lte_ml1_pos_prs.h"
#include "lte_ml1_schdlr.h"
#include "lte_ml1_sm_if.h"
#include <lte_ml1_common_nv_cfg.h>
#include "lte_ml1_pos_prs_plt.h"
#include <lte_ml1_mem.h>
#include "lte_ml1_coex_dsda.h"
#include "lte_ml1_coex_dsda_msg.h"

#ifdef FEATURE_LTE_RF
#ifdef FEATURE_RF_LTE_LM_RFM_INTERFACE
#include "rxlm_intf.h"
#endif
#endif

/*===========================================================================

                     INTERNAL DEFINITIONS AND TYPES

===========================================================================*/
/*! Maximum number cell flag elements (1 reference + neighbors) */
#define LTE_ML1_POS_PRS_MAX_CELL_FLAGS  \
  ( 1 + LTE_CPHY_PRS_MAX_NGHBR_CELLS )

/*! Maximum number hypotheses */
#define LTE_ML1_POS_PRS_MAX_HYP         \
  ( LTE_ML1_POS_PRS_MAX_CELL_FLAGS * LTE_CPHY_PRS_MAX_CELL_HYP )

/*! Reference cell index */
#define LTE_ML1_POS_PRS_REF_CELL_IDX 0xFF

/*! Maximum number of Tprs entries */
#define LTE_ML1_POS_PRS_MAX_TPRS 4

/*! Maxmium Tprs config index */
#define LTE_ML1_POS_PRS_MAX_TPRS_CONFIG_INDEX 2399


/* No bits are set */
#define POS_PRS_FLG_EMPTY  0
/* Return TRUE if set, FALSE otherwise */
#define POS_PRS_BIT_SET(_mask, _query) ((_mask&_query) !=0 ? TRUE : FALSE )
/* Set to TRUE */
#define POS_PRS_SET_BIT(_mask, _query) (_mask |=_query)
/* Set to specified value */
#define POS_PRS_SET_BIT_VAL(_mask, _query, _val)  if ( _val == TRUE ) (_mask |=_query);  else (_mask &= ~_query)
/* Set to FALSE */
#define POS_PRS_CLR_BIT(_mask, _query) (_mask &= ~_query)
/* Set all fields to FALSE */
#define POS_PRS_CLR_MASK(_mask)        (_mask = POS_PRS_FLG_EMPTY)

/*! Structure for PRS state machine */
typedef enum
{  

  /*! Whether an abort has been requested by ML1 manager */
  MGR_ABORT_REQUESTED = (1<<0),

  /*! Whether an suspend has been requested by ML1 manager  */
  MGR_SUSPEND_REQUESTED = (1<<1),

  /*! Whether suspend has been requested by ML1 manager due 
    to a Handover */
  MGR_HO_SUSPENDED_REQUESTED = (1<<2),

  /*! Whether a stop has been requested by OTDOA */
  OTDOA_STOP_REQUESTED = (1<<3),

  /*! Whether a cell switch has been requested */
  CELL_SWITCH_REQUESTED = (1<<4),

  /*! Whether the serving cell has changed */
  SERV_CELL_CHANGED = (1<<5),

  /*! Whether there is a pending start request */
  START_PENDING = (1<<6),

  /*! Whether we have a reference cell */
  HAVE_REF_CELL = (1<<7),
  
  /*! Whether we have the reference cell SF */
  HAVE_REF_CELL_SF_DELTA = (1<<8),
  
  /*! Whether there is an cell update pending */
  UPDATE_PENDING = (1<<9),

  /*! Whether we have sent down the fw measurment request*/
  FW_MEAS_REQ_SENT = (1<<10),
  
  /*! Whether the SCC has been configured and activated */
  SCC_ACTIVATED = (1<<11),

  /*! Whether we are in a PRS session */
  PRS_SESSION_STARTED = (1<12),

  PCC_SCC_MERGED = (1<<13),

  NEXT_OCC_IS_SCC = (1<<14),
  NEXT_SCC_OCC_HAS_MEAS_CELLS = (1<<15),
  NEXT_PCC_OCC_HAS_MEAS_CELLS = (1<<16),

} lte_ml1_pos_prs_flg_bm;

typedef enum
{
  /*! Has gone through LNA gap */
  LNA_COMPLETE = (1<<1),

  /*! Has recieved inter-freq gap config */
  GAP_CFG_RECEIVED = (1<<2),

    /*! Whether we have sent down the fw measurment request*/
  FW_MEAS_IF_REQ_SENT = (1<<3),
  
    /*! Whether we have requested F2 gaps */
  F2_GAPS_REQUESTED = (1<<4),

  /*! Whether we have requested F3 gaps */
  F3_GAPS_REQUESTED = (1<<5),

  /*! Whether a stop has been requested */
  STOP_REQUESTED = (1<<6),

  /*! Whether intra-freq is done measuring cells 
    and therefore can allow any too close frequencies to go*/
  INTRA_FREQ_DONE = (1<<7),

  /*! Whether the current gap configuration is correct.*/
  GAPS_ALREADY_VALID = (1<<8),

  /*! Whether PRS initiated the build script*/
  INITIATED_BUILD_SCRIPT = (1<<9),

  /*! Whether RXLM buffers have been allocated. */
  RXLM_HANDLES_ALLOCATED = (1<<10),
  
    /*! Whether inter frequency is waiting on intra to finish.*/
  IFREQ_WAITING_ON_INTRA = (1<<11),

  /*! Whether the inter-freq session has started */
  IFREQ_STARTED = (1<<12),
  
} lte_ml1_pos_prs_if_bm;

typedef enum
{
  LTE_ML1_POS_PRS_MAX_FREQ = 0, /*!< Used only to signify an invalid mode */
  LTE_ML1_POS_PRS_INTRA_FREQ,
  LTE_ML1_POS_PRS_INTER_FREQ
}lte_ml1_pos_prs_freq_mode_e; 

/*! LTE PRS cell update cell log information structure */
typedef struct 
{
  uint32 earfcn;           /*!< E-ARFCN */
  uint16 phy_cell_id;      /*!< Physical cell id */
  uint8  ant_port_config;  /*!< Antenna port config */
  uint8  bandwidth;        /*!< PRS bandwidth */
  uint8  num_dl_subframes; /*!< Number of downlink subframes */
  uint8  num_hypotheses;   /*!< Number of hypotheses */
  uint8  muting_valid;     /*!< Whether the muting fields are valid */
  uint8  muting_repeat;    /*!< Number of occasions before repeating */
  uint16 muting_pattern;   /*!< Muting pattern */
  uint16 config_index;     /*!< Configuration index */

  struct
  {
    int8   symbol_offset_index;   /*!< Symbol offset index */
    uint8  reserved1;             /*!< Reserved */
    uint16 prune_win_size;        /*!< Prune window size */
    uint16 prune_win_start_index; /*!< Prune window start index */
    uint16 reserved2;             /*!< Reserved */

  } hypothesis[ LTE_CPHY_PRS_MAX_CELL_HYP ]; /*!< Hypotheses */

} lte_ml1_pos_prs_cell_update_cell_log_t;

/*! PRS cell update log information */
typedef struct 
{
  uint8  version;     /*!< Log packet version */
  uint8  num_nghbrs;  /*!< Number of cells */
  uint16 phy_cell_id; /*!< Serving cell id */

  /*! Reference cell */
  lte_ml1_pos_prs_cell_update_cell_log_t ref;

  /*! Neighbor cells */
  lte_ml1_pos_prs_cell_update_cell_log_t nghbrs[ LTE_CPHY_PRS_MAX_NGHBR_CELLS ];

} lte_ml1_pos_prs_cell_update_log_t;

/*! PRS cell hypothesis log information */
typedef struct
{
  uint32 earfcn;      /*!< E-ARFCN */
  uint16 phy_cell_id; /*!< Physical cell id */
  uint8  cell_idx;    /*!< Cell index */
  uint8  hyp_idx;     /*!< Hypothesis index */
  uint8  muted;       /*!< Whether the hypothesis is muted */
  uint8  measured;    /*!< Whether the hypothesis has been measured */
  uint8  meas_req;    /*!< Whether the measurement has been requested */
  uint8  reserved1;

} lte_ml1_pos_prs_cell_hypothesis_log_t;

/*! PRS cell hypotheses log information */
typedef struct 
{
  uint8  version;           /*!< Log packet version */
  uint8  num_hypotheses;    /*!< Number of hypotheses */
  uint16 reserved1;
  uint32 serv_earfcn;       /*!< Serving cell E-ARFCN */
  uint16 serv_cell_id;      /*!< Serving cell id */
  uint16 ref_cell_id;       /*!< Reference cell id */
  uint16 curr_sf;           /*!< Current serving SF number */
  uint16 ref_cell_sf_delta; /*!< Reference cell SF delta from serving cell */
  uint16 next_occ_sf;       /*!< Next PRS occasions SF number */
  uint16 reserved2;
  uint32 duration;          /*!< Duration of PRS occasions */
  uint32 curr_rtc_lo;       /*!< Current RTC value LO */
  uint32 curr_rtc_hi;       /*!< Current RTC value HI */
  uint32 start_rtc_lo;      /*!< Start RTC value LO */
  uint32 start_rtc_hi;      /*!< Start RTC value HI */
  uint32 end_rtc_lo;        /*!< End RTC value LO */
  uint32 end_rtc_hi;        /*!< End RTC value HI */

  /*! Cell hypotheses */
  lte_ml1_pos_prs_cell_hypothesis_log_t hypotheses[ LTE_ML1_POS_PRS_MAX_HYP ];

} lte_ml1_pos_prs_cell_hypotheses_log_t;

/*! Structure for configuring the PRS state machine */
typedef struct 
{
  /*! Whether PRS is enabled */
  boolean enabled;

  /*! Whether fake FW PRS responses are enabled */
  boolean fake_prs;

  /*! Whether reference cell PBCH decode is supported */
  boolean ref_cell_pbch_decode_enabled;

  /*! Number of ticks for ML-LL interaction */
  lte_ml1_schdlr_sf_time_t fw_proc_delay_ms;

  /*! Number of ticks for ML-Scheduler interaction */
  lte_ml1_schdlr_sf_time_t sched_proc_delay_ms;

  /*! Number of ticks for padding before gaps */
  lte_ml1_schdlr_sf_time_t gap_buffer_ms;

  /*! Maximum number of reference cell PBCH decode attempts */
  uint8 max_pbch_decode_attempts;

  /*! Reference cell measurement repeat threshold in ms */
  uint16 ref_cell_meas_timeout_ms;

  /*! Only one cell per occasion */
  boolean one_cell_per_occasion;

  /*! Maximum number of hypotheses supported by FW */
  uint8 max_num_fw_hyps;

  /*! Force all cells to be SFN synchronized */
  boolean force_sfn_sync;

  /*! Time at which we report serving cell change to POS SW*/
  lte_ml1_schdlr_sf_time_t time_to_report;

  /*! Approximate time taken for the PRS occasion to complete */
  lte_ml1_schdlr_sf_time_t prs_duration;

  /*! Time needed between an inter-freq occ and an intra-freq occ */
  lte_ml1_schdlr_sf_time_t inter_intra_min_gap_distance;

  /*! Flag for whether or not inter-freq is enabled */
  boolean inter_freq_enabled;

} lte_ml1_pos_prs_config_s;

/*! LTE PRS Tprs information structure */
typedef struct 
{
  /*! Number of subframes per Tprs */
  uint16 period;

  /*! Minimum config index value */
  uint16 min_config_index;

  /*! Maximum config index value */
  uint16 max_config_index;

} lte_ml1_pos_prs_tprs_s;

/*! Structure for PRS cell flags */
typedef struct
{
  /*! Cell index */
  uint8 cell_idx;

  /*! Whether the cell is muted */
  boolean muted;

  /*! E-ARFCN */
  lte_earfcn_t earfcn;

  /*! Whether the cell has been measured */
  boolean measured;

  /*! Whether the cell measurement has been requested */
  boolean requested; 

  /*! Whether the cell is dirty */
  boolean dirty;

  /*! Time since the last measurement in ms */
  uint32 time_since_last_meas_ms;

  /*! Last measurement time in RTC */
  uint64 last_meas_rtc;

  /*! Number of times this cell has been measured */
  uint8 num_times_measured;

} lte_ml1_pos_prs_cell_flags_s;

typedef struct
{
  /*! Number of PRS cell flags */
  boolean pcc_scc_merged;

  /*! Number of PRS cell flags */
  uint8 num_cell_flags_pcc_scc;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags_pcc_scc[ 2*LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

  /*! Size of the current flag set.*/
  uint8 scc_flag_size;

  /*! Pointer to the nghbr array corresponding to SCC */
  lte_ml1_pos_prs_cell_flags_s* scc_flag_set;

  /*! Whether the SCC has been configured and activated */
  boolean scc_activated;

  /*! Cell info for SCC. Valid only if configured */
  lte_cphy_cell_info_s scc_cell_info;

  /*! SCC_ID. Valid only if configured */
  lte_ml1_cc_id_t scc_id;

  /*! Number of times a valid scc occasion was skipped. */
  uint8 num_skipped_occasions;

} lte_ml1_pos_prs_scc_info_s;

/*! Structure for PRS state machine */
typedef struct
{
  /*! Number of times PBCH decode attempted on reference cell */
  uint8 pbch_decode_attempts;

  /*! Reference cell delta from serving cell SF */
  lte_ml1_schdlr_sf_time_t ref_cell_sf_delta;

  /*! Next PRS occasions SF */
  lte_ml1_schdlr_sf_time_t next_occ_sf;

  /*! Next PRS occasions SF */
  uint64 next_occ_rtc;

  /*! Number of DL subframes for next SF */
  lte_ml1_schdlr_sf_time_t num_dl_subframes;

  /*! Number of PRS cell flags */
  uint8 num_cell_flags;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags[ LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

  /*! Number of times a valid PCC occasion was skipped */
  uint8 pcc_num_skipped_occasions;

  /*! Number of PRS cell flags */
  uint8 num_cell_flags_f2;

  /*! F2 E-ARFCN */
  lte_earfcn_t earfcn_f2;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags_f2[ LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

  /*! Number of PRS cell flags */
  uint8 num_cell_flags_f3;

  /*! F2 E-ARFCN */
  lte_earfcn_t earfcn_f3;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags_f3[ LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

  /*! PRS cell information */
  lte_cphy_prs_update_cells_req_s cells;

  /*! Saved PRS cell information */
  lte_cphy_prs_update_cells_req_s saved_cells;

 /*! Serving cell id */
  lte_phy_cell_id_t serv_cell_id;

  /*! Serving cell E-ARFCN */
  lte_earfcn_t serv_cell_earfcn;

  /*! Type of PRS session */
  lte_cphy_prs_session_type_e session_type;

  /*! Frequency mode for F1*/
  lte_ml1_pos_prs_freq_mode_e f1_mode;

  /*! Frequency mode for F2*/
  lte_ml1_pos_prs_freq_mode_e f2_mode;

  /*! Frequency mode for F3*/
  lte_ml1_pos_prs_freq_mode_e f3_mode;

  /*! Size of the current flag set.*/
  uint8 current_nghbr_flag_size;

  /*! Pointer to the nghbr array currently being accessed for the ifreq PRS session*/
  lte_ml1_pos_prs_cell_flags_s* current_nghbr_flag_set;

  /*! Current State */
  lte_ml1_pos_prs_state_t current_state;

  /*! Keeps track of which module the fw 
      measurement request PRS is waiting on */
  lte_ml1_pos_prs_freq_mode_e pending_fw_requests[ 2 ];

  /*! Number of PRS cell flags */
  uint8 num_cell_flags_pcc_scc;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags_pcc_scc[ 2*LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

  /*! Size of the current flag set.*/
  uint8 scc_flag_size;

  /*! Pointer to the nghbr array corresponding to SCC */
  lte_ml1_pos_prs_cell_flags_s* scc_flag_set;

  /*! Cell info for SCC. Valid only if configured */
  lte_cphy_cell_info_s scc_cell_info;

  /*! SCC_ID. Valid only if configured */
  lte_ml1_cc_id_t scc_id;

  /*! Number of times a valid scc occasion was skipped. */
  uint8 num_skipped_occasions;

  /*! Bitmask to keep track of activities */
  lte_ml1_pos_prs_flg_bm pos_prs_mask;

  /*! Whether the next occasion has it's priority increased */
  boolean next_intra_occasion_prior_increased;

  /*! Whether the next occasion has it's priority increased */
  lte_ml1_coex_dsda_prio_handle_t next_intra_occasion_prior_handle;

} lte_ml1_pos_prs_data_s;

/*! Structure for PRS inter-freq state machine */
typedef struct
{
  /*! Next Inter-freq PRS occasions SF */
  lte_ml1_schdlr_sf_time_t next_inter_freq_occ_sf;

  /*! Correct Gap offset for F2 */
  uint8 f2_gap_offset;

  /*! Correct Gap offset for F3 */
  uint8 f3_gap_offset;

  /*! Size of the current flag set.*/
  uint8 current_nghbr_flag_size;

  /*! Pointer to the nghbr array currently being accessed for the ifreq PRS session*/
  lte_ml1_pos_prs_cell_flags_s* current_nghbr_flag_set;

  /*! nPRS for the upcoming gap, calculated at the time of the meas request*/
  lte_cphy_prs_num_dl_subframes_e nprs;

  /*! Inter-freq PRS network service expiry timer */
  mcs_timer_type prs_gap_cfg_timer;

  /*! Antennae 0 buffer handle */
  lm_handle_type ant0_rxlm_handle;

  /*! Antennae 1 buffer handle */
  lm_handle_type ant1_rxlm_handle;

  /*! The gap starts at the time */
  lte_ml1_schdlr_sf_time_t gap_start_time;

  /*! Current Gap Cfg */
  lte_ml1_gapmgr_gap_cfg_s gap_cfg;

  /*! Bitmask to keep track of activities */
  lte_ml1_pos_prs_if_bm pos_prs_if_mask;

} lte_ml1_pos_prs_inter_freq_data_s;

/*! Structure for PRS cell flag data */
typedef struct
{
  /*! Number of PRS cell flags */
  uint8 num_cell_flags;

  /*! E-ARFCN */
  lte_earfcn_t earfcn;

  /*! PRS cell flags */
  lte_ml1_pos_prs_cell_flags_s cell_flags[ LTE_ML1_POS_PRS_MAX_CELL_FLAGS ];

} lte_ml1_pos_prs_cell_flag_data_s;

/*! Structure for PRS inter-freq state machine */
typedef struct
{
  /*! F1 cell flag data */
  lte_ml1_pos_prs_cell_flag_data_s cell_flags_f1;

  /*! F2 cell flag data */
  lte_ml1_pos_prs_cell_flag_data_s cell_flags_f2;

  /*! F3 cell flag data */
  lte_ml1_pos_prs_cell_flag_data_s cell_flags_f3;

  /*! PRS cell information */
  lte_cphy_prs_update_cells_req_s cells;

} lte_ml1_pos_prs_debug_data_s;

/*! Global Structure containing pointers for all global data for multi-sim*/
typedef struct
{
  /*! Instance number */
  lte_mem_instance_type instance;

  /*! Local data for PRS state machine */
  lte_ml1_pos_prs_data_s *lte_ml1_pos_prs_data;

  /*! Local data for Inter-freq PRS state machine */
  lte_ml1_pos_prs_inter_freq_data_s *lte_ml1_pos_prs_ifreq_data;

  /*! L1M callback functions */
  lte_ml1_pos_prs_cb_s *lte_ml1_pos_prs_cb;

} lte_ml1_pos_prs_s;

/*===========================================================================

                         LOCAL VARIABLES

===========================================================================*/

extern lte_ml1_pos_prs_config_s lte_ml1_pos_prs_config;

/*! Local data for PRS state machine */
extern lte_ml1_pos_prs_s *lte_ml1_pos_prs[LTE_MEM_MAX_INST];

/*===========================================================================

                         FUNCTION DECLARATIONS

===========================================================================*/
void prs_send_msg ( lte_mem_instance_type instance, msgr_umid_type umid, msgr_hdr_struct_type *msgr_hdr, uint32 size );
lte_ml1_schdlr_sf_time_t prs_get_config_period( uint16 config_index );
lte_ml1_schdlr_sf_time_t prs_get_config_offset( uint16 config_index );
void prs_rank_cells( lte_ml1_pos_prs_s *prs_inst, lte_ml1_schdlr_sf_time_t next_occ_sf, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
lte_cphy_prs_cell_info_s * prs_get_cell( lte_ml1_pos_prs_s *prs_inst, uint8 cell_idx );
boolean prs_clear_cells_if_measured( lte_ml1_pos_prs_s *prs_inst, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array);
boolean prs_one_freq_cells_measured(lte_ml1_pos_prs_s *prs_inst,uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
boolean prs_one_freq_all_cells_not_measured(lte_ml1_pos_prs_s *prs_inst,uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
boolean prs_ref_cell_is_serv_cell( lte_ml1_pos_prs_s *prs_inst );
boolean prs_ref_cell_is_inter_freq( lte_ml1_pos_prs_s *prs_inst );
boolean prs_serv_cell_is_empty( lte_mem_instance_type instance );
const lte_cphy_prs_cell_info_s * prs_find_serv_cell_in_neighbors( lte_mem_instance_type instance, lte_cphy_prs_update_cells_req_s *update_cells_req  );
void prs_save_cells( lte_ml1_pos_prs_s *prs_inst, lte_cphy_prs_update_cells_req_s *update_cells_req );
void prs_log_cell_hypotheses( lte_ml1_pos_prs_s *prs_inst, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array, lte_ml1_schdlr_sf_time_t next_occ_sf );
lte_ml1_schdlr_sf_time_t prs_calculate_ref_cell_sf_delta_from_pbch( lte_ml1_pos_prs_s *prs_inst, lte_ml1_sm_pbch_decode_cnf_s *pbch_decode_cnf );
void prs_update_ref_cell_delta( lte_ml1_pos_prs_s *prs_inst, lte_cphy_prs_cell_info_s *ref, uint8 num_nghbrs, lte_cphy_prs_cell_info_s *nghbrs);
void prs_update_cells_to_measure( lte_ml1_pos_prs_s *prs_inst, lte_earfcn_t meas_earfcn, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array  );
void prs_init_last_measured_time( lte_ml1_pos_prs_s *prs_inst, uint64 curr_rtc, lte_ml1_schdlr_sf_time_t config_period );
void prs_clear_cells_to_measure( lte_ml1_pos_prs_s *prs_inst, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s  *cell_flag_array );
void prs_clear_cells_measured( lte_ml1_pos_prs_s *prs_inst,uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array  );
void prs_mark_all_cells_dirty( lte_ml1_pos_prs_s *prs_inst );
void prs_send_otdoa_meas_rsp( lte_ml1_pos_prs_s *prs_inst, lte_LL1_dl_prs_meas_cnf_msg_struct *meas_cnf, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array, lte_ml1_schdlr_sf_time_t action_time );
void prs_update_existing_cells( lte_ml1_pos_prs_s *prs_inst, const lte_cphy_prs_update_cells_req_s *cells );
void prs_delete_dirty_cells( lte_ml1_pos_prs_s *prs_inst );
boolean prs_has_inter_freq_cells( lte_ml1_pos_prs_s *prs_inst );
void prs_start_inter_freq_stm( lte_ml1_pos_prs_s *prs_inst, stm_state_t intra_freq_state );
void prs_log_cell_update( lte_ml1_pos_prs_s *prs_inst );
void prs_add_new_cells( lte_ml1_pos_prs_s *prs_inst );
void prs_send_otdoa_error_rsp( lte_mem_instance_type instance, loc_lte_otdoa_PrsErrorInfoType status );
void prs_send_otdoa_serving_cell_update_rsp( lte_mem_instance_type instance, loc_lte_otdoa_ServingCellContextType context );
boolean prs_scheduler_obj_is_registered( lte_mem_instance_type instance, lte_ml1_schdlr_obj_id_e obj_id );
void prs_send_fw_meas_req( lte_ml1_pos_prs_s *prs_inst, lte_earfcn_t meas_earfcn, lte_ml1_schdlr_sf_time_t action_time_sf, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s  *cell_flag_array );
void prs_mark_measured_cells( lte_ml1_pos_prs_s *prs_inst, lte_earfcn_t meas_earfcn, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
boolean prs_measurement_requested( lte_ml1_pos_prs_s *prs_inst, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
lte_ml1_schdlr_sf_time_t prs_get_next_occasion_sf( lte_ml1_pos_prs_s *prs_inst, lte_ml1_schdlr_sf_time_t serv_sf_now, lte_ml1_pos_prs_cell_flags_s *array_ptr);
boolean prs_freq_has_measureable_cells( lte_ml1_pos_prs_s *prs_inst, lte_ml1_schdlr_sf_time_t next_occ_sf, uint8 num_cell_flags, lte_ml1_pos_prs_cell_flags_s *cell_flag_array );
void prs_configure_for_carrier_aggregation( lte_ml1_pos_prs_s *prs_inst );
lte_cphy_cell_info_s* prs_get_scc_cell_info(lte_ml1_pos_prs_s *prs_inst,lte_ml1_cc_id_t scc_id);
uint16 prs_get_cell_iprs( lte_ml1_pos_prs_s *prs_inst,lte_cphy_prs_cell_info_s *cell );
#ifdef T_WINNT
#error code not present
#endif