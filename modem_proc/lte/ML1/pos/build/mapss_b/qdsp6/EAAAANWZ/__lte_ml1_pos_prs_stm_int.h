/*=============================================================================

    __lte_ml1_pos_prs_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_pos_prs_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_POS_PRS_STM_INT_H
#define __LTE_ML1_POS_PRS_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_pos_prs_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_POS_PRS_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_POS_PRS_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_POS_PRS_STM_NUM_STATES 9

/* Define a macro for the number of SM inputs */
#define LTE_ML1_POS_PRS_STM_NUM_INPUTS 20

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */


/* State entry/exit function prototypes */
void lte_ml1_pos_prs_inactive_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_ref_cell_search_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_ref_cell_pbch_decode_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_ref_cell_pbch_decode_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_wait_for_occasion_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_wait_for_occasion_exit(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_meas_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_cell_switch_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_pos_prs_inactive_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_otdoa_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inactive_update_cells(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_ignore(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inactive_cswitch_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inactive_cswitch_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_mgr_ho_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_error(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_update_cells(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_mgr_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_mgr_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_cswitch_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_cswitch_tick_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_cswitch_tick_obj_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_mgr_ho_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_cswitch_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_save_update_cells(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_set_otdoa_stop_flag(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_set_mgr_abort_flag(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_set_mgr_suspend_flag(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_ref_cell_search_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_set_cell_switch_flag(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_ref_cell_pbch_decode_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_ref_cell_pbch_decode_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_ref_cell_pbch_decode_obj_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_wait_for_occcasion_update_cells(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_wait_for_occasion_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_wait_for_occasion_obj_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_otdoa_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_mgr_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_mgr_suspend(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_obj_abort(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_meas_obj_wait_for_fw(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_abort_meas_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_set_mgr_ho_suspend_flag(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_immed_update_cells(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_suspend_resume(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  UPDATE_CELLS,
  REF_CELL_SEARCH,
  REF_CELL_PBCH_DECODE,
  WAIT_FOR_OCCASION,
  MEAS,
  ABORT,
  SUSPEND,
  CELL_SWITCH_ONGOING,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_POS_PRS_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_POS_PRS_STM_INT_H */
