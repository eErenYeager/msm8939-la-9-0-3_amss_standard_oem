/*=============================================================================

    __lte_ml1_pos_prs_stm_inter_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_pos_prs_stm_inter.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_POS_PRS_STM_INTER_INT_H
#define __LTE_ML1_POS_PRS_STM_INTER_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_pos_prs_stm_inter.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_POS_PRS_INTER_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_POS_PRS_INTER_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_POS_PRS_INTER_STM_NUM_STATES 7

/* Define a macro for the number of SM inputs */
#define LTE_ML1_POS_PRS_INTER_STM_NUM_INPUTS 11

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */


/* State entry/exit function prototypes */
void lte_ml1_pos_prs_inter_freq_inactive_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_inter_freq_wait_for_gap_cfg_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_pos_prs_inter_freq_wait_for_gap_enter(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_pos_prs_ignore(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_inactive_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_inactive_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_inactive_update_scc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_inactive_intra_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_cfg_received(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_cfg_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_cfg_timer_expired(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_cfg_update_scc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_set_intra_freq_flag_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_reconfig(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_lna_gap_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_meas_gap_start(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_update_scc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_wait_for_gap_reconfigure_gaps(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_lna_gap_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_lna_gap_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_lna_gap_update_scc(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_meas_done(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_meas_gap_stop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_meas_gap_end(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_pos_prs_inter_freq_meas_gap_update_scc(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INTER_INACTIVE,
  INTER_WAIT_FOR_GAP_CFG,
  INTER_WAIT_FOR_GAP,
  INTER_LNA,
  INTER_MEAS,
  INTER_ABORT,
  INTER_SUSPEND,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_POS_PRS_INTER_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_POS_PRS_STM_INTER_INT_H */
