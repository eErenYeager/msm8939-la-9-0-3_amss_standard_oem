/*!
  @file
  lte_ml1_gm_nv.h

  @brief
  header file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/gm/inc/lte_ml1_gm_nv.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_GM_NV_H
#define LTE_ML1_GM_NV_H

#include "lte_ml1_comdef.h"	

#define LTE_ML1_GM_PWR_CNTRL_FREEZE_NAK_INVALID  0xFF

/*! 
  @brief Databse to hold Tx freeze parameters 
*/
typedef struct 
{
  uint8  accum_delay;   /* ms to delay power accumulation */
  uint8  max_nak;       /* Naks allowed before acuumulation is stopped */
}lte_ml1_gm_pwr_cntrl_ta_freeze_s;

#ifdef FEATURE_DIMEPM_MODEM
typedef struct
{
  /* Initial TB value when we come into connected state */
  uint16 nv_tb_init_val;
  /* Low threshold value trigger for DL TB*/
  uint32 nv_dl_tb_low_thresh;
  /* High threshold value trigger for DL TB*/
  uint32 nv_dl_tb_high_thresh;
  /* Low threshold value trigger for UL TB*/
  uint32 nv_ul_tb_low_thresh;
  /* High threshold value trigger for DL TB*/
  uint32 nv_ul_tb_high_thresh;
  /* Sampling period to check for the DL TB size */
  uint16 nv_lte_data_sampling_per;
  /* Filtering coefficient to be used 
  This is in multiple of 100 for easy implementation */
  uint16 nv_filt_coef_for_lte_data_dur;
}lte_ml1_gm_lte_data_rate_detect_nv_params_s;
#endif

/*===========================================================================
	 
	FUNCTION:  lte_ml1_gm_nv_search_meas_size
	 
===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes. 
  This size information is used by EFS framework for memory 
  allocation and storing the EFS value.
  		
  @details
   
  @return size in bytes
*/
/*=========================================================================*/
  
uint8 lte_ml1_gm_nv_total_tx_pwr_size(void);
 

/*===========================================================================
     
    FUNCTION:  lte_ml1_gm_nv_tx_blank_before_qta_size
     
===========================================================================*/
/*!
    @brief
    This function returns the size of EFS variable in bytes. 
    This size information is used by EFS framework for memory 
    allocation and storing the EFS value.
            
    @details
     
    @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_gm_nv_tx_blank_before_qta_size(void);

/*===========================================================================
	 
	FUNCTION:  lte_ml1_gm_nv_search_meas_defaultvalue
	 
===========================================================================*/
/*!
  @brief
  This function provides the default value for the efs variable. 
  This data will be stored with the EFS framework.
   
  @details
  EFS framework sends the pointer to memory(which framework owns),
  to fill in the default value.
   
  @return
 */
/*=========================================================================*/

void lte_ml1_gm_nv_total_tx_pwr_defaultvalue(void *ptr);

/*===========================================================================
	  
	FUNCTION:  lte_ml1_gm_nv_total_tx_pwr_post_efsread
	  
===========================================================================*/
/*!
	@brief
	This function will be called post efsread success by framework, 
	to perform client defined post efsread functionality.
	  
	@details
	.
	  
	@return
*/
/*=========================================================================*/

void  lte_ml1_gm_nv_total_tx_pwr_post_efsread(void *ptr);


/*===========================================================================
	
  FUNCTION:  lte_ml1_gm_nv_cdrx_collision_params_size
	
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/


 uint8 lte_ml1_gm_nv_cdrx_collision_params_size(void);

/*===========================================================================
	 
	FUNCTION:  lte_ml1_gm_nv_cdrx_collision_params_defaultvalue
	 
===========================================================================*/
/*!
	@brief
	This function provides the default value for the efs variable. 
	This data will be stored with the EFS framework.
	 
	@details
	EFS framework sends the pointer to memory(which framework owns),
	to fill in the default value.
	 
	 @return
 */
/*=========================================================================*/

  void lte_ml1_gm_nv_cdrx_collision_params_defaultvalue(void *ptr);

/*===========================================================================
	
  FUNCTION:  lte_ml1_gm_qta_power_accum_delay_size
	
===========================================================================*/
/*!
	@brief
    This function returns the size of EFS variable
    LTE_ML1_GM_QTA_PWR_ACCUM_DELAY in bytes. This size information is used
    by EFS framework for memory allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_gm_qta_power_accum_delay_size(void);

/*===========================================================================
	 
	FUNCTION:  lte_ml1_gm_qta_power_accum_delay_defaultvalue
	 
===========================================================================*/
/*!
	@brief
    This function provides the default value for the efs variable:
    LTE_ML1_GM_QTA_PWR_ACCUM_DELAY
 
    This data will be stored with the EFS framework.
	 
	@details
	EFS framework sends the pointer to memory(which framework owns),
	to fill in the default value.
	 
	@return
 */
/*=========================================================================*/
void lte_ml1_gm_qta_power_accum_delay_defaultvalue(void *ptr);

/*===========================================================================
     
    FUNCTION:  lte_ml1_gm_nv_tx_blank_before_qta_defaultvalue
     
===========================================================================*/
/*!
    @brief
    This function provides the default value for the efs variable. 
    This data will be stored with the EFS framework.
     
    @details
    EFS framework sends the pointer to memory(which framework owns),
    to fill in the default value.
     
     @return
 */
/*=========================================================================*/
void lte_ml1_gm_nv_tx_blank_before_qta_defaultvalue(void *ptr);

/*===========================================================================

  FUNCTION:  lte_ml1_gm_get_qta_power_accum_delay

===========================================================================*/
/*!
    @brief
    Returns the pwr accum delay setting for LTE_ML1_GM_QTA_PWR_ACCUM_DELAY

    @return
    Number of ms to delay power accumulation
*/
/*=========================================================================*/
lte_ml1_gm_pwr_cntrl_ta_freeze_s* lte_ml1_gm_get_qta_power_accum_delay
( 
  lte_mem_instance_type instance
);

  #ifdef FEATURE_DIMEPM_MODEM  
  /*===========================================================================
   
    FUNCTION:  lte_ml1_gm_nv_lte_data_rate_detect_params_size
   
  ===========================================================================*/
  /*!
   @brief
   This function returns the size of EFS variable in bytes. 
   This size information is used by EFS framework for memory 
   allocation and storing the EFS value.
       
   @details
   
   @return size in bytes
  */
  /*=========================================================================*/
  
  uint8 lte_ml1_gm_nv_lte_data_rate_detect_params_size(void);

  /*===========================================================================

  FUNCTION:  lte_ml1_gm_nv_lte_data_rate_detect_params_default_val

===========================================================================*/
/*!
    @brief
    Returns the efs setting for lte data rate detection params

    @return
    Populates the structure required to find the data rate detection in LTE
*/
/*=========================================================================*/
  void lte_ml1_gm_nv_lte_data_rate_detect_params_default_val (void *ptr);
#endif

#endif  /* LTE_ML1_GM_NV_H */

