/*=============================================================================

    __lte_ml1_gm_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_gm_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_GM_STM_INT_H
#define __LTE_ML1_GM_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_gm_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_GM_SM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_GM_SM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_GM_SM_NUM_STATES 8

/* Define a macro for the number of SM inputs */
#define LTE_ML1_GM_SM_NUM_INPUTS 33

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void lte_ml1_gm_sm_entry(stm_state_machine_t *sm,void *payload);
void lte_ml1_gm_sm_exit(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void lte_ml1_gm_init_in_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_in_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_init_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_init_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_idle_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_idle_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_prach_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_prach_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_prach_msg3_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_prach_msg3_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_prach_msg4_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_prach_msg4_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_out_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_out_sync_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_init_suspended_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void lte_ml1_gm_cleanup_suspended_state(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t lte_ml1_gm_proc_pdcch_phich_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_pdsch_stat_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_bler_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_send_sr_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_prach_triggered_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_contention_result_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_nop(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_common_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_rach_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_dedicated_cfg_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_handover_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_tx_suspend_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_sfn_updated_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_con_release_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_out_of_sync_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_t310_expiry(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_suspend_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_send_phr_failed_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_rach_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_ant_switch_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_qta_info_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_rec_trigger_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_rec_cleanup_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_handle_rf_avail_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_skip_rach_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_ra_timer_started_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_ra_timer_expiry_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_msg3_grant_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_suspend_req_in_rach(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_ta_rcvd_ind(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_handover_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_resume_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_suspend_out_sync_req(stm_state_machine_t *sm, void *payload);
stm_state_t lte_ml1_gm_proc_contention_result_ind_in_suspend(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  LTE_ML1_GM_IN_SYNC_STATE,
  LTE_ML1_GM_INIT_STATE,
  LTE_ML1_GM_IDLE_STATE,
  LTE_ML1_GM_PRACH_STATE,
  LTE_ML1_GM_PRACH_MSG3_STATE,
  LTE_ML1_GM_PRACH_MSG4_STATE,
  LTE_ML1_GM_OUT_SYNC_STATE,
  LTE_ML1_GM_SUSPENDED_STATE,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_GM_SM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_GM_STM_INT_H */
