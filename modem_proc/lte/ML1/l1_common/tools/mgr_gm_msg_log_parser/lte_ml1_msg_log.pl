
use Math::BigInt;
use File::Find;
use Cwd            qw( abs_path );
use File::Basename qw( dirname );
################################################################################################################################################################################################################################################################
#####################
#    subroutines    #
#####################

sub find_umid_file{
  my $umid_found = 0;
  my $umid_file_path;
  my $dir_path = "$_[0]\\mcs\\msgr\\gen";
  $umid_found = find_umid_file_recursieve($dir_path, $umid_file_path);
  if($umid_found == 0)
  {
    $umid_found = find_umid_file_recursieve($_[0], $umid_file_path);
  }
  if($umid_found == 0)
  {
    $umid_file_path = enter_umid_path();
  }
  return $umid_file_path;
}

sub enter_umid_path{
  my $umid_found = 0;
  my $umid_file_path;
  while($umid_found == 0)
  {
    print "Unable to find __msgr_global_umids.h, please enter its directory location: ";
    my $umid_dir = <STDIN>;
	chomp($umid_dir);
	$umid_file_path = "$umid_dir\\__msgr_global_umids.h";
    if (-f $umid_file_path) 
	{
      $umid_found = 1;
	}
  }
  return $umid_file_path;
}

sub find_umid_file_recursieve{
  my $path = $_[0];
  my $umid_found = 0;
  my $umid_path;
  my $file;
  my $dirh;
  if(opendir($dirh, $path))
  {
    while(($umid_found == 0) && ($file = readdir($dirh)))
    {
	  my $temp_file_path = $path."\\".$file;
      if($file =~ m/__msgr_global_umids\.h/)
      {
        $_[1] = $temp_file_path;
        $umid_found = 1;
      }
	  elsif(!($file =~ m/\.$/))
	  {
	    $umid_found = find_umid_file_recursieve($temp_file_path, $_[1]);
	  }
    }
  }
  return $umid_found;
}

sub print_big_int{
	if($_[0] == 0)
	{
		return "\t\t0\t";
	}
	my $temp = Math::BigInt -> new ($_[0]);
	my $pcycle_upper_bits = $temp>>32;
	my $pcycle_lower_bits = $temp & 0xffffffff;
	return sprintf("  [0x%08x%08x]\t",$pcycle_upper_bits,$pcycle_lower_bits);
}
############################################################################################################################################################################################################################################################

#####################
#   FIND UMID FILE  #
#####################

my $modem_proc_dir;
my $umid_file;
if($0 =~ m/(.*modem_proc).*/)
{
  $modem_proc_dir = $1;
  $umid_file = find_umid_file($modem_proc_dir);
}
else
{
  $umid_file = enter_umid_path();
}

print "\nUsing UMID FILE: $umid_file \n\n";

#####################
#  PARSE UMID FILE  #
#####################

my %umid_name;
my $umid_opened = open(MSG_LOG, $umid_file);
if ( ! $umid_opened ) {
   print "Error Opening UMID FILE: $umid_file";
   exit 1;
}

# Debug code 
# my $num_umid = 0;

while (<MSG_LOG>) {
   /\s+MSGR_UMID_TABLE_ENTRY\s+\(\s+0x([0-9A-Fa-f]+)\s*,\s*\"([0-9A-Za-z_]+)\"\s*.*$/ && do {
      $umid_val = hex($1);
      $umid_name{$umid_val} = $2;

      # Debug code
      # $num_umid++;
      # $umid_val_array[$num_umid] = $umid_val;
      next;
   };
   /\s+MSGR_UMID_TABLE_ENTRY\s+\(\s+([0-9]+)\s*,\s*\"([0-9A-Za-z_]+)\"\s*.*$/ && do {
      $umid_val = $1;
      $umid_name{$umid_val} = $2;

      # Debug code
      # $num_umid++;
      # $umid_val_array[$num_umid] = $umid_val;
      next;
   };
}

close MSG_LOG;

# Below is DEBUG code to ensure UMID file is read properly
# open (UMIDFILE, '>C:\\umid_log.txt');
# print "UMIDS found ---";
# for ($i=1; $i<=$num_umid; $i++) {
#    $umid_val = $umid_val_array[$i];
#    printf(UMIDFILE  "[0x%x]  %s", $umid_val, $umid_name{$umid_val});
# }
# close UMIDFILE;

#########################
#  PARSE MSG LOG file_id  #
#########################

my $timetick_present = 0;

my %mgr_in_msg_sfn;
my %mgr_in_msg_umid;
my %mgr_in_msg_pcycle;
my %mgr_in_msg_done_pcycle;
my %mgr_in_msg_tick;
my %mgr_in_msg_done_tick;
my %mgr_out_msg_sfn;
my %mgr_out_msg_umid;
my %mgr_out_msg_pcycle;
my %mgr_out_msg_tick;
my %mgr_exception_msg_sfn;
my %mgr_exception_msg_umid;
my %mgr_exception_msg_pcycle;
my %mgr_exception_msg_done_pcycle;
my %mgr_exception_msg_tick;
my %mgr_exception_msg_done_tick;
my %gm_in_msg_sfn;
my %gm_in_msg_umid;
my %gm_in_msg_pcycle;
my %gm_in_msg_done_pcycle;
my %gm_in_msg_tick;
my %gm_in_msg_done_tick;
my %gm_out_msg_sfn;
my %gm_out_msg_umid;
my %gm_out_msg_tick;
my %gm_out_msg_pcycle;
my %gm_exception_msg_sfn;
my %gm_exception_msg_umid;
my %gm_exception_msg_pcycle;
my %gm_exception_msg_done_pcycle;
my %gm_exception_msg_tick;
my %gm_exception_msg_done_tick;


my $last_mgr_in_msg = 0;
my $last_mgr_out_msg = 0;
my $last_mgr_exception_msg = 0;
my $last_gm_in_msg = 0;
my $last_gm_out_msg = 0;
my $last_gm_exception_msg = 0;
my $last_suspicious_sf_ind_index = 0;

my $num_mgr_in_msg = 0;
my $num_mgr_out_msg = 0;
my $num_mgr_exception_msg = 0;
my $num_gm_in_msg = 0;
my $num_gm_out_msg = 0;
my $num_gm_exception_msg = 0;
my $num_suspicious_sfn_ind = 0;

my $in_out_msg_type = 0;
my $process_msg = 0;

my %all_msg_umid;
my %all_msg_pcycle;
my %all_msg_done_pcycle;
my %all_msg_sfn;
my %all_msg_tick;
my %all_msg_done_tick;
my %all_msg_type;
my %all_msg_src;
my %all_msg_tgt;
my $all_msg_num;

my $last_subframe_ind_tick = 0;

for ($file_id = 1; $file_id <= 2; $file_id++) {

  if ($file_id == 1) {
    system "touch C:\\temp\\lte_ml1_mgr_msg_log.txt";
    my $opened = open(MSG_LOG, "C:\\temp\\lte_ml1_mgr_msg_log.txt");
    if( ! $opened ){
      print "Error: not finding mgr_msg_log!";
      exit 1;
    }
    print "Parsing lte_ml1_mgr_msg_log.txt\n";
  } else {
    system "touch C:\\temp\\lte_ml1_gm_msg_log.txt";
    my $opened = open(MSG_LOG, "C:\\temp\\lte_ml1_gm_msg_log.txt");
    if( ! $opened ){
      print "Error: not finding gm msg log!";
      exit 1;
    }
    print "Parsing lte_ml1_gm_msg_log.txt\n";
  }

  while (<MSG_LOG>) {

     $process_msg = 0;

     /.*in_last_msg\s*=\s*0x([0-9A-Fa-f]+)/ && do {
        $in_out_msg_type = 1;
        if ($file_id == 1) {
           $last_mgr_in_msg = hex($1);
        } else {
           $last_gm_in_msg = hex($1);
        }
        next;
     };

     /^\s*in_last_msg\s*=\s*([0-9]+)/ && do {
        $in_out_msg_type = 1;
        if ($file_id == 1) {
           $last_mgr_in_msg = ($1);
        } else {
           $last_gm_in_msg = ($1);
        }
        next;
     };

     /^\s*out_last_msg\s*=\s*0x([0-9A-Fa-f]+)/ && do {
        $in_out_msg_type = 2;
        if ($file_id == 1) {
           $last_mgr_out_msg = hex($1);
        } else {
           $last_gm_out_msg = hex($1);
        }
        next;
     };

     /^\s*out_last_msg\s*=\s*([0-9]+)/ && do {
        $in_out_msg_type = 2;
        if ($file_id == 1) {
           $last_mgr_out_msg = ($1);
        } else {
           $last_gm_out_msg = ($1);
        }
        next;
     };
     /^\s*last_exception_msg\s*=\s*([0-9]+)/ && do {
        $in_out_msg_type = 3;
        if ($file_id == 1) {
           $last_mgr_exception_msg  = ($1);
        } else {
           $last_gm_exception_msg = ($1);
        }
        next;
     };
	 	 
     if ($process_msg == 0) {
	     /.*sfn_subfn_cnt\s*=\s*([0-9]+).*umid\s*=\s*([0-9]+).*Q6_pcycle_val\s*=\s*0x([0-9A-Fa-f]+).*Q6_done_pcycle_val\s*=\s*0x([0-9A-Fa-f]+).* timetick_val\s*=\s*([0-9]+).* msg_done_timetick_val\s*=\s*([0-9]+)/ && do {
			$timetick_present = 1;
			$pcycle_present = 1;
			$sfn = $1;
			$umid = $2;
			$pcycle = hex($3);
			$done_pcycle = hex($4);
			$tick = $5;
			$done_tick = $6;
			$process_msg = 1;
		 };
	 }
	 if ($process_msg == 0) {
	     /.*sfn_subfn_cnt\s*=\s*([0-9]+).*umid\s*=\s*([0-9]+).*Q6_pcycle_val\s*=\s*([0-9]+).*Q6_done_pcycle_val\s*=\s*([0-9]+).* timetick_val\s*=\s*([0-9]+).* msg_done_timetick_val\s*=\s*([0-9]+)/ && do {
			$timetick_present = 1;
			$pcycle_present = 1;
			$sfn = $1;
			$umid = $2;
			$pcycle = $3;
			$done_pcycle = $4;
			$tick = $5;
			$done_tick = $6;
			$process_msg = 1;
		 };
	 }
	 
     if ($process_msg == 0) {
		 /.*sfn_subfn_cnt\s*=\s*0x([0-9A-Fa-f]+).*umid\s*=\s*0x([0-9A-Fa-f]+).* timetick_val\s*=\s*0x([0-9A-Fa-f]+).* msg_done_timetick_val\s*=\s*0x([0-9A-Fa-f]+)/ && do {
			$timetick_present = 1;		
			$sfn = hex($1);
			$umid = hex($2);
			$tick = hex($3);
			$done_tick = hex($4);
			$process_msg = 1;
		 };
	 }

     if ($process_msg == 0) {
        /.*sfn_subfn_cnt\s*=\s*([0-9]+).*umid\s*=\s*([0-9]+).* timetick_val\s*=\s*([0-9]+).* msg_done_timetick_val\s*=\s*([0-9]+)/ && do {
           $timetick_present = 1;
           $sfn = ($1);
           $umid = ($2);
           $tick = ($3);
           $done_tick = ($4);
           $process_msg = 1;
        }
     }

     if ($process_msg == 0) {
        /.*sfn_subfn_cnt\s*=\s*0x([0-9A-Fa-f]+).*umid\s*=\s*0x([0-9A-Fa-f]+)/ && do {
           $sfn = hex($1);
           $umid = hex($2);
           $process_msg = 1;
        }
     };

     if ($process_msg == 0) {
        /.*sfn_subfn_cnt\s*=\s*([0-9]+).*umid\s*=\s*([0-9]+)/ && do {
           $sfn = ($1);
           $umid = ($2);
           $process_msg = 1;
        }
     };

     if ($process_msg == 1) {
        $process_msg = 0;

        if ($file_id == 1) {
           if ($in_out_msg_type == 1) {
              $mgr_in_msg_sfn{$num_mgr_in_msg} = $sfn;
              $mgr_in_msg_umid{$num_mgr_in_msg} = $umid;
              if ($timetick_present == 1) {
                 $mgr_in_msg_tick{$num_mgr_in_msg} = $tick;
                 $mgr_in_msg_done_tick{$num_mgr_in_msg} = $done_tick;
              }
			  if($pcycle_present == 1){
				$mgr_in_msg_pcycle{$num_mgr_in_msg} = $pcycle;
				$mgr_in_msg_done_pcycle{$num_mgr_in_msg} = $done_pcycle;
			  }
              $num_mgr_in_msg++;
           } elsif ($in_out_msg_type == 2) {
              $mgr_out_msg_sfn{$num_mgr_out_msg} = $sfn;
              $mgr_out_msg_umid{$num_mgr_out_msg} = $umid;
              if ($timetick_present == 1) {
                 $mgr_out_msg_tick{$num_mgr_out_msg} = $tick;
              }
			  if($pcycle_present == 1){
			     $mgr_out_msg_pcycle{$num_mgr_out_msg} = $pcycle;
			  }
              $num_mgr_out_msg++;
           } elsif ($in_out_msg_type == 3){
             if ($umid != 0) {
                $mgr_exception_msg_sfn{$num_mgr_exception_msg} = $sfn;
                $mgr_exception_msg_umid{$num_mgr_exception_msg} = $umid;
                if ($timetick_present == 1) {
                   $mgr_exception_msg_tick{$num_mgr_exception_msg} = $tick;
                   $mgr_exception_msg_done_tick{$num_mgr_exception_msg} = $done_tick;
                }
                if($pcycle_present == 1){
                  $mgr_exception_msg_pcycle{$num_mgr_exception_msg} = $pcycle;
                  $mgr_exception_msg_done_pcycle{$num_mgr_exception_msg} = $done_pcycle;
                }
                $num_mgr_exception_msg++;
             }
           }else {
              print "MSG type (IN/OUT) is not set ($in_out_msg_type)";
              die;
           }
        } else {
           if ($in_out_msg_type == 1) {
              $gm_in_msg_sfn{$num_gm_in_msg} = $sfn;
              $gm_in_msg_umid{$num_gm_in_msg} = $umid;
              if ($timetick_present == 1) {
                 $gm_in_msg_tick{$num_gm_in_msg} = $tick;
                 $gm_in_msg_done_tick{$num_gm_in_msg} = $done_tick;
              }
			  if($pcycle_present == 1){
			     $gm_in_msg_pcycle{$num_gm_in_msg} = $pcycle;
			     $gm_in_msg_done_pcycle{$num_gm_in_msg} = $done_pcycle;
			  }
              $num_gm_in_msg++;
           } elsif ($in_out_msg_type == 2) {
              $gm_out_msg_sfn{$num_gm_out_msg} = $sfn;
              $gm_out_msg_umid{$num_gm_out_msg} = $umid;
              if ($timetick_present == 1) {
                 $gm_out_msg_tick{$num_gm_out_msg} = $tick;
              }
			  if($pcycle_present == 1){
			     $gm_out_msg_pcycle{$num_gm_out_msg} = $pcycle;
			  }
              $num_gm_out_msg++;
           } 
             elsif ($in_out_msg_type == 3){
               if ($umid != 0) {
                $gm_exception_msg_sfn{$num_gm_exception_msg} = $sfn;
                $gm_exception_msg_umid{$num_gm_exception_msg} = $umid;
                if ($timetick_present == 1) {
                   $gm_exception_msg_tick{$num_gm_exception_msg} = $tick;
                   $gm_exception_msg_done_tick{$num_gm_exception_msg} = $done_tick;
                }
                if($pcycle_present == 1){
                  $gm_exception_msg_pcycle{$num_gm_exception_msg} = $pcycle;
                  $gm_exception_msg_done_pcycle{$num_gm_exception_msg} = $done_pcycle;
                }
                $num_gm_exception_msg++;
               }
			} else {
              print "MSG type (IN/OUT) is not set ($in_out_msg_type)";
              die;
            }
          }
        }
        next;
  }
  close MSG_LOG;
}

##############
#  SFN IND   #
##############

system "touch C:\\temp\\lte_ml1_suspicious_sfn_ind_buffer.txt";
my $opened = open(MSG_LOG, "C:\\temp\\lte_ml1_suspicious_sfn_ind_buffer.txt");
if( ! $opened ){
  print "Error: not finding lte_ml1_suspicious_sfn_ind_buffer!";
  exit 1;
}
print "Parsing lte_ml1_suspicious_sfn_ind_buffer.txt\n";

while (<MSG_LOG>) {
   $process_msg = 0;

   /.*last_suspicious_sf_ind_index\s*=\s*([0-9]+)/ && do {
      $last_suspicious_sf_ind_index = $1;
   next;
   };
   /.*cur_sfn\s*=\s*([0-9]+).*next_sfn\s*=\s*([0-9]+).*Q6_pcycle_val\s*=\s*([0-9]+).* timetick_val\s*=\s*([0-9]+).* time_interval\s*=\s*([.0-9]+)/ && do {
      $sfn_ind_cur_sfn{$num_suspicious_sfn_ind} = $1;
      $sfn_ind_next_sfn{$num_suspicious_sfn_ind} = $2;
      $sfn_ind_Q6_pcycle_val{$num_suspicious_sfn_ind} = $3;
      $sfn_ind_timetick_val{$num_suspicious_sfn_ind} = $4;
      $sfn_ind_time_interval{$num_suspicious_sfn_ind} = $5;
      $num_suspicious_sfn_ind++;
   }
}
close MSG_LOG;

#################
#  DUMP OUTPUT  #
#################

my $index = 0;
my $msg_index = 0;
my $umid = 0;
print "script dir is: ". "C:\\temp\\lte_ml_ll_msg_log.log";
open (LOGFILE, ">", "C:\\temp\\lte_ml_ll_msg_log.log");

  print LOGFILE  "\n";
  print LOGFILE  "===============================\n";
  print LOGFILE  "  LTE ML1 MGR MSG LOG \n";
  print LOGFILE  "===============================\n";
  print LOGFILE  "\n";

  print LOGFILE  "****  IN MESSAGES -- (last msg index $last_mgr_in_msg)  ****\n";

  if ($timetick_present == 0) 
  {
     print LOGFILE  "[INDEX]\t[SFN]\t[UMID]\t\t[LOG_MSG]\n";
     print LOGFILE  "-----------------------------------------------------------\n";
  }
  elsif ($pcycle_present == 0)
  {
     print LOGFILE  "[INDEX]  [Slow Clk (delta ticks/ms)]   [Done Slow Clk (delta ticks/ms)]  [SFN]    [UMID]	  [LOG_MSG]\n";
     print LOGFILE  "---------------------------------------------------------------------------------------------------------------------\n";
  }
  else 
  {
     print LOGFILE  "[INDEX]   [Pcycles] 		  [Done Pcycles]        [Slow Clk (delta ticks/ms)]   [Done Slow Clk (delta ticks/ms)]  [SFN]    [UMID]	        [LOG_MSG]\n";
     print LOGFILE  "-------------------------------------------------------------------------------------------------------------------------------------\n";
  }

  print "Dumping lte_ml1_mgr_msg_log IN messages; num messages $num_mgr_in_msg\n";

  $msg_index = $last_mgr_in_msg;
  if ($timetick_present == 1) 
  {
     $prev_tick = $mgr_in_msg_tick{$msg_index};
  }
  for ($index=0; $index<$num_mgr_in_msg; $index++) {
     print LOGFILE "\[$msg_index\]\t";
     $sfn = $mgr_in_msg_sfn{$msg_index};
     $umid = $mgr_in_msg_umid{$msg_index};
	 if ($pcycle_present == 1) 
     {
	    $pcycle = $mgr_in_msg_pcycle{$msg_index};
	    $done_pcycle = $mgr_in_msg_done_pcycle{$msg_index};
	    print LOGFILE print_big_int($pcycle);
	    print LOGFILE print_big_int($done_pcycle);
     }
     if ($timetick_present == 1) 
     {
        $tick = $mgr_in_msg_tick{$msg_index};
        $done_tick = $mgr_in_msg_done_tick{$msg_index};
        printf (LOGFILE "%10d %10d|%4.2f\t", $tick, ($tick - $prev_tick), (($tick - $prev_tick)/(32.768)));
        if ($done_tick == 0) {
           printf (LOGFILE "%10d %10d|%4.2f\t", 0, 0, 0);
        } else {
           printf (LOGFILE "%10d %10d|%4.2f\t", $done_tick, ($done_tick - $tick), (($done_tick - $tick)/(32.768)));
        }
        $prev_tick = $mgr_in_msg_tick{$msg_index};
     }
     print LOGFILE "\[$sfn\]\t";
     printf (LOGFILE "[0x%08x]\t", $umid);
     if (exists($umid_name{$umid})) {
        printf (LOGFILE "%s\n", $umid_name{$umid});
     } else {
        print LOGFILE "Unknown UMID !!\n";
     }
     $msg_index++;
     if ($msg_index >= $num_mgr_in_msg) {
        $msg_index = 0;
     }
  }

  print LOGFILE  "\n";
  print LOGFILE  "****  OUT MESSAGES -- (last msg index $last_mgr_out_msg)  ****\n";

  if ($timetick_present == 0) 
  {
     print LOGFILE  "[INDEX]\t[SFN]\t[UMID]\t\t[LOG_MSG]\n";
     print LOGFILE  "-----------------------------------------------------------\n";
  }
  elsif ($pcycle_present == 0)
  {
     print LOGFILE  "[INDEX]	 [Slow Clk]   [Slow Clk Delta]  [SFN]	   [UMID]		[LOG_MSG]\n";
     print LOGFILE  "--------------------------------------------------------------------------------------------------\n";
  }
  else 
  {
     print LOGFILE  "[INDEX]   [Pcycles]   		[Slow Clk]    [Slow Clk Delta] 	[SFN]	  [UMID]	[LOG_MSG]\n";
     print LOGFILE  "---------------------------------------------------------------------------------------------------------------------------\n";
  }

  print "Dumping lte_ml1_mgr_msg_log OUT messages; num messages $num_mgr_out_msg\n";

  $msg_index = $last_mgr_out_msg;
  if ($timetick_present == 1) 
  {
     $prev_tick = $mgr_out_msg_tick{$msg_index};
  }
  for ($index=0; $index<$num_mgr_out_msg; $index++) {
     print LOGFILE "\[$msg_index\]\t";
     $sfn = $mgr_out_msg_sfn{$msg_index};
     $umid = $mgr_out_msg_umid{$msg_index};
	 if ($pcycle_present == 1) 
     {
		$pcycle = $mgr_out_msg_pcycle{$msg_index};
		print LOGFILE print_big_int($pcycle);
     }
     if ($timetick_present == 1) 
     {
        $tick = $mgr_out_msg_tick{$msg_index};
        printf (LOGFILE "%10d  %10d|%4.2f\t", $tick, ($tick - $prev_tick), (($tick - $prev_tick)/32.768));
        $prev_tick = $mgr_out_msg_tick{$msg_index};
     }
     print LOGFILE "\[$sfn\]\t";
     printf (LOGFILE "[0x%08x]\t", $umid);
     if (exists($umid_name{$umid})) {
        printf (LOGFILE "%s\n", $umid_name{$umid});
     } else {
        print LOGFILE "Unknown UMID !!\n";
     }
     $msg_index++;
     if ($msg_index >= $num_mgr_out_msg) {
        $msg_index = 0;
     }
  }

  # GM MSG IN LOG

  print LOGFILE  "\n";
  print LOGFILE  "===============================\n";
  print LOGFILE  "  LTE ML1 GM MSG LOG \n";
  print LOGFILE  "===============================\n";
  print LOGFILE  "\n";

  print LOGFILE  "****  IN MESSAGES -- (last msg index $last_gm_in_msg)  ****\n";

  if ($timetick_present == 0) 
  {
     print LOGFILE  "[INDEX]\t[SFN]\t[UMID]\t\t[LOG_MSG]\n";
     print LOGFILE  "-----------------------------------------------------------\n";
  }
  elsif ($pcycle_present == 0)
  {
     print LOGFILE  "[INDEX]  [Slow Clk (delta ticks/ms)]   [Done Slow Clk (delta ticks/ms)]  [SFN]    [UMID]	  [LOG_MSG]\n";
     print LOGFILE  "---------------------------------------------------------------------------------------------------------------------\n";
  }
  else 
  {
     print LOGFILE  "[INDEX]   [Pcycles]		  [Done_Pcycles]  	  [Slow Clk (delta ticks/ms)] [Done Slow Clk (delta ticks/ms)]  [SFN]    [UMID]	        [LOG_MSG]\n";
     print LOGFILE  "---------------------------------------------------------------------------------------------------------------------------------\n";
  }

  print "Dumping lte_ml1_gm_msg_log IN messages; num messages $num_gm_in_msg\n";
  
  $msg_index = $last_gm_in_msg;
  if ($timetick_present == 1) 
  {
     $prev_tick = $gm_in_msg_tick{$msg_index};
  }
  for ($index=0; $index<$num_gm_in_msg; $index++) {
     print LOGFILE "\[$msg_index\]\t";
     $sfn = $gm_in_msg_sfn{$msg_index};
     $umid = $gm_in_msg_umid{$msg_index};
	 if ($pcycle_present == 1) 
     {
	    $pcycle = $gm_in_msg_pcycle{$msg_index};
		$done_pcycle = $gm_in_msg_done_pcycle{$msg_index};
		print LOGFILE print_big_int($pcycle);
		print LOGFILE print_big_int($done_pcycle);
     }
     if ($timetick_present == 1) 
     {
        $tick = $gm_in_msg_tick{$msg_index};
        $done_tick = $gm_in_msg_done_tick{$msg_index};
        printf (LOGFILE "%10d %10d|%4.2f\t", $tick, ($tick - $prev_tick), (($tick - $prev_tick)/(32.768)));
        if ($done_tick == 0) {
           printf (LOGFILE "%10d %10d|%4.2f\t", 0, 0, 0);
        } else {
           printf (LOGFILE "%10d %10d|%4.2f\t", $done_tick, ($done_tick - $tick), (($done_tick - $tick)/(32.768)));
        }
        $prev_tick = $gm_in_msg_tick{$msg_index};
     }
     print LOGFILE "\[$sfn\]\t";
     printf (LOGFILE "[0x%08x]\t", $umid);
     if (exists($umid_name{$umid})) {
        printf (LOGFILE "%s", $umid_name{$umid});
        # Last subframe tick delta info
#       if ($umid_name{$umid} == "LTE_LL1_SYS_SUBFRAME_IND") {
        if ($umid == 0x040a0400) {
           if ($last_subframe_ind_tick != 0) {
              printf (LOGFILE "  %10d|%4.2f\n", ($tick - $last_subframe_ind_tick), (($tick - $last_subframe_ind_tick)/(32.768)));
           } else {
              print LOGFILE "\n";
           }
           $last_subframe_ind_tick = $tick;
        } else {
           print LOGFILE "\n";
        }
     } else {
        print LOGFILE "Unknown UMID !!";
     }

     $msg_index++;
     if ($msg_index >= $num_gm_in_msg) {
        $msg_index = 0;
     }
  }

  print LOGFILE  "\n";
  print LOGFILE  "****  OUT MESSAGES -- (last msg index $last_gm_out_msg)  ****\n";

  if ($timetick_present == 0) 
  {
     print LOGFILE  "[INDEX]\t[SFN]\t[UMID]\t\t[LOG_MSG]\n";
     print LOGFILE  "-----------------------------------------------------------\n";
  }
  elsif ($pcycle_present == 0)
  {
     print LOGFILE  "[INDEX]	 [Slow Clk]   [Slow Clk Delta]  [SFN]	   [UMID]		[LOG_MSG]\n";
     print LOGFILE  "--------------------------------------------------------------------------------------------------\n";
  }
  else 
  {
     print LOGFILE  "[INDEX]   [Pcycles] 		  [Slow Clk]   [Slow Clk Delta] [SFN]	   [UMID]	[LOG_MSG]\n";
     print LOGFILE  "--------------------------------------------------------------------------------------------------\n";
  }

  print "Dumping lte_ml1_gm_msg_log OUT messages; num messages $num_gm_out_msg\n";

  $msg_index = $last_gm_out_msg;
  if ($timetick_present == 1) 
  {
     $prev_tick = $gm_out_msg_tick{$msg_index};
  }
  for ($index=0; $index<$num_gm_out_msg; $index++) {
     print LOGFILE "\[$msg_index\]\t";
     $sfn = $gm_out_msg_sfn{$msg_index};
     $umid = $gm_out_msg_umid{$msg_index};
	 if ($pcycle_present == 1) 
     {
	    $pcycle = $gm_out_msg_pcycle{$msg_index};
		print LOGFILE print_big_int($pcycle);
     }
     if ($timetick_present == 1) 
     {
        $tick = $gm_out_msg_tick{$msg_index};
        printf (LOGFILE "%10d\t%10d\t", $tick, ($tick - $prev_tick));
        $prev_tick = $gm_out_msg_tick{$msg_index};
     }
     print LOGFILE "\[$sfn\]\t";
     printf (LOGFILE "[0x%08x]\t", $umid);
     if (exists($umid_name{$umid})) {
        printf (LOGFILE "%s\n", $umid_name{$umid});
     } else {
        print LOGFILE "Unknown UMID !!\n";
     }
     $msg_index++;
     if ($msg_index >= $num_gm_out_msg) {
        $msg_index = 0;
     }
  }


if ($timetick_present == 1) 
{
   $all_msg_num = 0;
   #Loop over all msg types and fill into one array
   for ($index=0; $index<$num_gm_in_msg; $index++) {
      $umid = $gm_in_msg_umid{$index}; 
      if (exists($umid_name{$umid})) {
         $all_msg_umid{$all_msg_num} = $umid;
         $all_msg_sfn{$all_msg_num} = $gm_in_msg_sfn{$index};
		 if ($timetick_present == 1) 
		 {
			$all_msg_pcycle{$all_msg_num} = $gm_in_msg_pcycle{$index};
			$all_msg_done_pcycle{$all_msg_num} = $gm_in_msg_done_pcycle{$index};
		 }
         $all_msg_tick{$all_msg_num} = $gm_in_msg_tick{$index};
         $all_msg_done_tick{$all_msg_num} = $gm_in_msg_done_tick{$index};
         $all_msg_type{$all_msg_num} = "GM_IN";
         $all_msg_src{$all_msg_num} = "   ";
         $all_msg_tgt{$all_msg_num} = "GM ";
         $all_msg_num++;
      }
   }
   for ($index=0; $index<$num_gm_out_msg; $index++) {
      $umid = $gm_out_msg_umid{$index}; 
      if (exists($umid_name{$umid})) {
         $all_msg_umid{$all_msg_num} = $umid;
         $all_msg_sfn{$all_msg_num} = $gm_out_msg_sfn{$index};
		 if ($timetick_present == 1) 
		 {
			$all_msg_pcycle{$all_msg_num} = $gm_out_msg_pcycle{$index};
			$all_msg_done_pcycle{$all_msg_num} = 0;
		 }
         $all_msg_tick{$all_msg_num} = $gm_out_msg_tick{$index};
         $all_msg_done_tick{$all_msg_num} = 0;
         $all_msg_type{$all_msg_num} = "GM_OUT";
         $all_msg_src{$all_msg_num} = " GM";
         $all_msg_tgt{$all_msg_num} = "   ";
         $all_msg_num++;
      }
   }
   for ($index=0; $index<$num_mgr_in_msg; $index++) {
      $umid = $mgr_in_msg_umid{$index}; 
      if (exists($umid_name{$umid})) {
         $all_msg_umid{$all_msg_num} = $umid;
         $all_msg_sfn{$all_msg_num} = $mgr_in_msg_sfn{$index};
		 if ($timetick_present == 1) 
		 {
			$all_msg_pcycle{$all_msg_num} = $mgr_in_msg_pcycle{$index};
			$all_msg_done_pcycle{$all_msg_num} = $mgr_in_msg_done_pcycle{$index};
		 }
         $all_msg_tick{$all_msg_num} = $mgr_in_msg_tick{$index};
         $all_msg_done_tick{$all_msg_num} = $mgr_in_msg_done_tick{$index};
         $all_msg_type{$all_msg_num} = "MGR_IN";
         $all_msg_src{$all_msg_num} = "   ";
         $all_msg_tgt{$all_msg_num} = "MGR";
         $all_msg_num++;
      }
   }
   for ($index=0; $index<$num_mgr_out_msg; $index++) {
      $umid = $mgr_out_msg_umid{$index}; 
      if (exists($umid_name{$umid})) {
         $all_msg_umid{$all_msg_num} = $umid;
		 if ($timetick_present == 1) 
		 {
			$all_msg_pcycle{$all_msg_num} = $mgr_out_msg_pcycle{$index};
			$all_msg_done_pcycle{$all_msg_num} = 0;
		 }
         $all_msg_sfn{$all_msg_num} = $mgr_out_msg_sfn{$index};
         $all_msg_tick{$all_msg_num} = $mgr_out_msg_tick{$index};
         $all_msg_done_tick{$all_msg_num} = 0;
         $all_msg_type{$all_msg_num} = "MGR_OUT";
         $all_msg_src{$all_msg_num} = "MGR";
         $all_msg_tgt{$all_msg_num} = "   ";
         $all_msg_num++;
      }
   }

#  for ($i=0; $i<($all_msg_num - 1); $i++) {
#     print LOGFILE "$i $all_msg_umid{$i}  $all_msg_src{$i} $all_msg_tgt{$i}\n";
#  }

   #Sort the list
   for ($i=0; $i<($all_msg_num - 1); $i++) {
      for ($j=$i; $j<$all_msg_num; $j++) {
         if ($all_msg_tick{$j} < $all_msg_tick{$i}) {
            $temp = $all_msg_umid{$j}; $all_msg_umid{$j} = $all_msg_umid{$i}; $all_msg_umid{$i} = $temp;
            $temp = $all_msg_sfn{$j}; $all_msg_sfn{$j} = $all_msg_sfn{$i}; $all_msg_sfn{$i} = $temp;
			if ($timetick_present == 1) 
		    {
				$temp = $all_msg_pcycle{$j}; $all_msg_pcycle{$j} = $all_msg_pcycle{$i}; $all_msg_pcycle{$i} = $temp;
				$temp = $all_msg_done_pcycle{$j}; $all_msg_done_pcycle{$j} = $all_msg_done_pcycle{$i}; $all_msg_done_pcycle{$i} = $temp;
			}
            $temp = $all_msg_tick{$j}; $all_msg_tick{$j} = $all_msg_tick{$i}; $all_msg_tick{$i} = $temp;
            $temp = $all_msg_done_tick{$j}; $all_msg_done_tick{$j} = $all_msg_done_tick{$i}; $all_msg_done_tick{$i} = $temp;
            $temp = $all_msg_type{$j}; $all_msg_type{$j} = $all_msg_type{$i}; $all_msg_type{$i} = $temp;
            $temp = $all_msg_src{$j}; $all_msg_src{$j} = $all_msg_src{$i}; $all_msg_src{$i} = $temp;
            $temp = $all_msg_tgt{$j}; $all_msg_tgt{$j} = $all_msg_tgt{$i}; $all_msg_tgt{$i} = $temp;
         }
      }
   }

   #Dump the list 
   print LOGFILE  "\n";
   print LOGFILE  "\n";

   if($pcycle_present == 1){
   print LOGFILE  "  [Pcycles] 			[Done Pcycles]	  [Slow Clk (delta ticks/ms)]   [Done Slow Clk (delta ticks/ms)]    [SFN]  			   [UMID]	[LOG_MSG]\n";
   print LOGFILE  "---------------------------------------------------------------------------------------------------------------------\n";
   } else {
     print LOGFILE  "  [Slow Clk (delta ticks/ms)]   [Done Slow Clk (delta ticks/ms)]  [SFN]    [UMID]	  [LOG_MSG]\n";
     print LOGFILE  "---------------------------------------------------------------------------------------------------------------------\n";
   }
   $prev_tick = $all_msg_tick{0};

   for ($i=0; $i<$all_msg_num; $i++) {
      $sfn = $all_msg_sfn{$i};
      $umid = $all_msg_umid{$i};
      if($pcycle_present == 1){
	    $pcycle = $all_msg_pcycle{$i};
	    $done_pcycle = $all_msg_done_pcycle{$i};
      }
      $tick = $all_msg_tick{$i};
      $done_tick = $all_msg_done_tick{$i};
      $type = $all_msg_type{$i};

#      $ms = ($tick/32.768);
#      $sec = $ms/1000;
#      $min = $sec/60;
#      $hr = $min/60;
#      $ms = $ms % 1000;
#      $sec = $sec % 60;
#      $min = $min % 60;
#      printf (LOGFILE "%3d:%02d:%02d:%06.03f\t", $hr, $min, $sec, $ms);
      if($pcycle_present == 1){
	  print LOGFILE print_big_int($pcycle);
	  print LOGFILE print_big_int($done_pcycle);
      }
      printf (LOGFILE "%10d %10d|%4.2f\t", $tick, ($tick - $prev_tick), (($tick - $prev_tick)/(32.768)));
      if ($done_tick == 0) {
         printf (LOGFILE "%10d %10d|%4.2f\t", 0, 0, 0);
      } else {
         printf (LOGFILE "%10d %10d|%4.2f\t", $done_tick, ($done_tick - $tick), (($done_tick - $tick)/(32.768)));
      }
      if ((($done_tick - $tick)/(32.768)) > 1) {
         print LOGFILE " #  ";
      } else {
         print LOGFILE "    "
      }

      print LOGFILE "\[$sfn\]\t";

      # Print source and target
      if (($umid >= 0x04070200) && ($umid < 0x040c0200))  {
         if (($type eq "GM_IN") || ($type eq "MGR_IN")) {
            $all_msg_src{$i} = "LL1";
         } else {
            $all_msg_tgt{$i} = "LL1";
         }
      }
      print LOGFILE "$all_msg_src{$i} ----> $all_msg_tgt{$i}\t";

      printf (LOGFILE "[0x%08x]\t", $umid);
      if (exists($umid_name{$umid})) {
         printf (LOGFILE "%s\n", $umid_name{$umid});
      } else {
         print LOGFILE "Unknown UMID !!\n";
      }
      $prev_tick = $tick;
   }
}

#close LOGFILE;

############################################################################################################################################################################################################################################################

######################################################
#   Output exception msg and suspicious sfn indication  #
######################################################


print LOGFILE  "\n";
print LOGFILE  "===============================\n";
print LOGFILE  "  LTE ML1 MGR EXCEPTION MSG LOG \n";
print LOGFILE  "===============================\n";
print LOGFILE  "\n";

print LOGFILE  "****  EXCEPTION MESSAGES -- (last msg index $last_mgr_exception_msg)  ****\n";
print LOGFILE  "[INDEX]   [Pcycles] 		  [Done Pcycles]        [Slow Clk]   [Done Slow Clk]  [delta ticksms)]  [SFN]    [UMID]	        [LOG_MSG]\n";
print LOGFILE  "-------------------------------------------------------------------------------------------------------------------------------------\n";


print "Dumping lte_ml1_mgr_msg_log IN messages; num messages $num_mgr_exception_msg\n";

if ($num_mgr_exception_msg < 50) 
{
  $msg_index = 0;
}
else
{
  $msg_index = $last_mgr_exception_msg;
}
for ($index=0; $index<$num_mgr_exception_msg; $index++) {
   print LOGFILE "\[$msg_index\]\t";
   $sfn = $mgr_exception_msg_sfn{$msg_index};
   $umid = $mgr_exception_msg_umid{$msg_index};
   if ($pcycle_present == 1) 
   {
      $pcycle = $mgr_exception_msg_pcycle{$msg_index};
      $done_pcycle = $mgr_exception_msg_done_pcycle{$msg_index};
      print LOGFILE print_big_int($pcycle);
      print LOGFILE print_big_int($done_pcycle);
   }
   if ($timetick_present == 1) 
   {
      $tick = $mgr_exception_msg_tick{$msg_index};
      $done_tick = $mgr_exception_msg_done_tick{$msg_index};
      printf (LOGFILE "%10d\t", $tick);
      printf (LOGFILE "%10d %10d|%4.2f\t", $done_tick, ($done_tick - $tick), (($done_tick - $tick)/(32.768)));
   }
   print LOGFILE "\[$sfn\]\t";
   printf (LOGFILE "[0x%08x]\t", $umid);
   if (exists($umid_name{$umid})) {
      printf (LOGFILE "%s\n", $umid_name{$umid});
   } else {
      print LOGFILE "Unknown UMID !!\n";
   }
   $msg_index++;
   if ($msg_index >= $num_mgr_exception_msg) {
      $msg_index = 0;
   }
}

print LOGFILE  "\n";
print LOGFILE  "===============================\n";
print LOGFILE  "  LTE ML1 GM EXCEPTION MSG LOG \n";
print LOGFILE  "===============================\n";
print LOGFILE  "\n";

print LOGFILE  "****  EXCEPTION MESSAGES -- (last msg index $last_gm_exception_msg)  ****\n";
print LOGFILE  "[INDEX]   [Pcycles] 		  [Done Pcycles]        [Slow Clk]   [Done Slow Clk]  [delta ticksms]  [SFN]    [UMID]	        [LOG_MSG]\n";
print LOGFILE  "-------------------------------------------------------------------------------------------------------------------------------------\n";


print "Dumping lte_ml1_gm_msg_log IN messages; num messages $num_gm_exception_msg, $last_gm_exception_msg\n";

if ($last_gm_exception_msg < 50) 
{
  $msg_index = 0;
}
else
{
  $msg_index = $last_gm_exception_msg;
}
for ($index=0; $index<$num_gm_exception_msg; $index++) {
   print LOGFILE "\[$msg_index\]\t";
   $sfn = $gm_exception_msg_sfn{$msg_index};
   $umid = $gm_exception_msg_umid{$msg_index};
   if ($pcycle_present == 1) 
   {
      $pcycle = $gm_exception_msg_pcycle{$msg_index};
      $done_pcycle = $gm_exception_msg_done_pcycle{$msg_index};
      print LOGFILE print_big_int($pcycle);
      print LOGFILE print_big_int($done_pcycle);
   }
   if ($timetick_present == 1) 
   {
      $tick = $gm_exception_msg_tick{$msg_index};
      $done_tick = $gm_exception_msg_done_tick{$msg_index};
      printf (LOGFILE "%10d\t", $tick);
      printf (LOGFILE "%10d %10d|%4.2f\t", $done_tick, ($done_tick - $tick), (($done_tick - $tick)/(32.768)));
   }
   print LOGFILE "\[$sfn\]\t";
   printf (LOGFILE "[0x%08x]\t", $umid);
   if (exists($umid_name{$umid})) {
      printf (LOGFILE "%s\n", $umid_name{$umid});
   } else {
      print LOGFILE "Unknown UMID !!\n";
   }
   $msg_index++;
   if ($msg_index >= $num_gm_exception_msg) {
      $msg_index = 0;
   }
}

print LOGFILE  "\n";
print LOGFILE  "================================\n";
print LOGFILE  "  LTE SUSPICIOUS SFN INDICATION \n";
print LOGFILE  "================================\n";
print LOGFILE  "\n";

print LOGFILE  "****  SUSPICIOUS SFN INDICATION -- (last msg index $last_suspicious_sf_ind_index)  ****\n";
print LOGFILE  "[INDEX]   [Pcycles]              [Slow Clk]     [Current SFN]   [Next SFN]      [ms Since last SFN indication]    \n";
print LOGFILE  "-------------------------------------------------------------------------------------------------------------------------------------\n";


print "Dumping lte_ml1_gm_msg_log IN messages; num messages $num_gm_exception_msg, $last_gm_exception_msg\n";
if ($num_suspicious_sfn_ind < 50) 
{
  $msg_index = 0;
}
else
{
  $msg_index = $last_suspicious_sf_ind_index;
}
for ($index=0; $index<$num_suspicious_sfn_ind; $index++) 
{
  print LOGFILE "\[$msg_index\]\t";
  $cur_sfn = $sfn_ind_cur_sfn{$msg_index};
  $next_sfn = $sfn_ind_next_sfn{$msg_index};
  $pcycle = $sfn_ind_Q6_pcycle_val{$msg_index};
  $tick = $sfn_ind_timetick_val{$msg_index};
  $time_interval = $sfn_ind_time_interval{$msg_index};

  print LOGFILE print_big_int($pcycle);

  printf (LOGFILE "%10d\t", $tick);

  print LOGFILE "\[$cur_sfn\]\t";
  print LOGFILE "\t\[$next_sfn\]\t";
  print LOGFILE "\t\[$time_interval\]\n";
  $msg_index++;
  if ($msg_index >= $num_suspicious_sfn_ind)
  {
    $msg_index = 0;
  }
}

close LOGFILE;

system "call notepad.exe C:\\temp\\lte_ml_ll_msg_log.log";



