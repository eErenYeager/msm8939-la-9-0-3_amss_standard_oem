

Purpose:

This folder is created to parse recent msg logs from and to msg and gm thread on BOLT/DIME3.0. 


Four files are included in this folder: 

	lte_ml1_msg_log.pl 

	ml_msg_log.cmm

	ml_msg_log_cust.cmm  

	README.txt  



Usage:   

  
	1) Go to the folder of ..\modem_proc\lte\ML1\l1_common\tools\mgr_gm_msg_log_parser\  

	2) Type in follwing cmd in SW T32 window. 
  
		-For internal build dump:     
  
			go ml_msg_log.cmm  
  
		-For customer dump:     

			go ml_msg_log_cust.cmm <address of lte_ml1_debug_log> 

	3) Enter the directory of __msgr_global_umids.h file if asked. 

	4) 3 logs files will be present under the folder of "C:\temp\" 

	And file lte_ml1_ll_msg_log.log will be opened automatically.
	 
	 


