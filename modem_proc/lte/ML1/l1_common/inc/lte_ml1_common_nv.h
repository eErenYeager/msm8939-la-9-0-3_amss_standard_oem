/*!
  @file
  lte_ml1_common_nv.h

  @brief
  source file for efs client related operations for this module.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/l1_common/inc/lte_ml1_common_nv.h#2 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/
#ifndef LTE_ML1_COMMON_NV_H
#define LTE_ML1_COMMON_NV_H

#include "lte_ml1_comdef.h"
#include <comdef.h>	
/*===========================================================================
	
							   INCLUDE FILES
	
===========================================================================*/
	
	
	
/*===========================================================================
	
					  MODULE DEFINITIONS AND TYPES
	
===========================================================================*/
/* Maximum number of states for PUCCH cancel */
#define LTE_ML1_NV_CFG_MAX_PUCCH_CANCEL_STATES  10


/*! Antenna correlation reporting disabled */
#define LTE_ML1_ANT_CORR_RPT_PER_DISABLED 0xFFFF

#define LTE_ML1_UPDATE_BAND_RANGE_NUM_BANDS 3

	
/*! Structure to hold the TX power backoff Info 
*/
typedef struct {

  /* Initial backoff */
  uint16  p_backoff;

  /* Maximum value of the backoff */
  uint16  p_backoff_max;

  /* Time for non-backed-off value of power */
  uint16 t_on;

  /* Time for backed off Value of power */
  uint16 t_off;

  /* Timer for each step of the backoff */
  uint32 step_timer;
} lte_ml1_nv_cfg_tx_power_backoff_info_s;



/*! Structure to hold the PUCCH Throttling Info 
*/
typedef struct {
  /* Number of states */
  uint8 num_states;

  /* Default state for Thermal mitigation */
  uint8 default_state_tm;

  /* Default state for CPU based Flow control */
  uint8 default_state_fc;

  /* Padding */
  uint8 padding;

  struct {
    /* On timer */
    uint16 t_on;

    /* Off timer */
    uint16 t_off;
  } timer_info[LTE_ML1_NV_CFG_MAX_PUCCH_CANCEL_STATES];

  /* Step Timer for each state for thermal mitigation */
  uint32 step_timer_tm;

  /* Step Timer for each state for CPU flow control */
  uint32 step_timer_fc;
}lte_ml1_nv_cfg_pucch_cancel_info_s;


/*! LTE update band range structure */
typedef struct 
{
    /* Band index */
	uint16	   band_id;
	/* Min earfcn supported */
	uint16	   start_freq;
	/* Max earfcn supported */
	uint16	   end_freq;
} lte_ml1_update_band_range_s;
	
/*! LTE update bands structure */
typedef struct 
{
	/* Number of bands */
	uint16						  num_bands;
	/* Band info structure */
	lte_ml1_update_band_range_s	  lte_update_bands[LTE_ML1_UPDATE_BAND_RANGE_NUM_BANDS];
} lte_ml1_update_band_s;



  /*! Optimized CDRX Info populated from EFS config */
typedef struct
{
  /* HARQ optimization enabled */
  uint8                                 harq_opt_enabled;

  /* SR optimization enabled */
  uint8                                 sr_opt_enabled;

  /* Maximim allowed SR delay in ms */
  uint16                                max_sr_delay;
}lte_ml1_nv_cfg_cdrx_opt_info_s;


/*suspend time info: max chain number*/
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_MAX_CC_NUM 2

/*suspend time info: trigger mode*/
enum{
  /* start suspend trigger in connected state, stop when not in connected/suspend state */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_CONN_MODE     = 0,
  /* start suspend trigger in idle_drx state, stop when not in idle_drx/connected/suspend state */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_IDLE_MODE     = 1,
  /* start suspend trigger in inactive state, stop when in stop state */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_INACTIVE_MODE = 2
};

/*suspend time info: tune away mode*/
enum{
  /* suspend trigger as scheduled tune away */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SCHEDULED_TUNE_AWAY = 0,
  /* suspend trigger as rude tune away */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_RUDE_TUNE_AWAY      = 1,
  /* suspend trigger as quick tune away */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_QUICK_TUNE_AWAY     = 2,
  /* suspend trigger as mixed tune away */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_MIXED_TUNE_AWAY     = 3
};

/* suspend time info: skip mask */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_NONE         0x0
/* skip sending RF available/unavailable ind to RRC in TAM */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_RF_UNAVAIL   0x1
/* skip rv.grant assertion in suspend trigger when GSM's requesting TRM */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_TRM_ASSERT   0x2
/* skip notify timer for rude tune away */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_NOTIFY_TIMER 0x4
/* skip period after unlock cancel, will start next cycle immediately */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_UNLOCK_CANCEL_GAP 0x8
/* skip blocking QTA for DSDA */ /* No longer in use! */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_QTA_BLOCK_FOR_DSDA 0x10
/* skip supporting QTA for DSDA */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_SKIP_QTA_SUPPORT_FOR_DSDA 0x20
/* default notify time threshold in ms */
#define LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_NOTIFY_THRES_TIME 100

/*suspend time info: tune away tech */
enum{
  /* tune away tech none */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_TECH_NONE = 0,
  /* tune away tech gsm 1 */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_TECH_GSM1 = 1,
  /* tune away tech 1x */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_TECH_1X   = 2,
  /* tune away tech max */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_TECH_MAX  = LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_TECH_1X
};

/*suspend time info: tune away resource */
enum{
  /* tune away tech gsm 1 */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_RESOURCE_NONE = 0,
  /* tune away tech 1x */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_RESOURCE_BEST = 1,
  /* the max tune away tech enum */
  LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_RESOURCE_ANY  = 2
};

/*! Structure to hold suspend/resume time info 
*/
typedef struct {
  /* Initial suspend/resume trigger delay (ms)*/
  uint16  trigger_delay;

  /* Deliberately release TRM after this delay to trigger unlock_cancel */
  uint16  unlock_cancel_delay;

  /* Suspend period (ms), random period when set to 0xFFFF */
  uint16  suspend_period;

  /* Suspend duration (ms) */
  uint16  suspend_duration;
  
  /* Threshold time for notify (ms) */
  uint16  notify_threshold_time;
  
  /* Tune away mode, scheduled/rude/quick */
  uint16  tune_away_mode;
  
  /* Suspend trigger mode, (start in) connected/idle/inactive state */
  uint16  suspend_mode;

  /* Skip mask */
  uint16  skip_mask;
  
  /* The tune away tech */
  uint16  tune_away_tech;

  /* Resource */
  uint16  resource;
} lte_ml1_nv_cfg_cc_suspend_time_info_s;


/*! Structure to hold suspend/resume time info 
*/
typedef struct {
  /* Suspend info */
  lte_ml1_nv_cfg_cc_suspend_time_info_s suspend_info_per_cc[LTE_ML1_NV_CFG_SUSPEND_TIME_INFO_MAX_CC_NUM];
  
} lte_ml1_nv_cfg_suspend_time_info_s;  


#ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY
/*! Antenna Switch info */
typedef struct
{
  /* Feature enabled */
  uint8    feature_enabled;

  /* RSRP threashold High */
  uint8    rsrp_thresh_hi;

  /* RSRP threshold Low */
  uint8    rsrp_thresh_lo;

  /* RSRP threshold for Type 2 ant switch */
  uint8    type_2_rsrp_thresh;
	
  /* RSRP delta threshold for Type 2 ant switch */
  uint8	   type_2_rsrp_delta_thresh;
	
  /* MTPL gating threshold ant switch */
  uint8	   mptl_gating_thresh;

  /* CA mode antenna config uint8 */
  uint8    ca_mode_ant_config;

  /* RSRP threshold for selecting preferred antenna for DSDS */
  uint8    rsrp_thresh_preferred_antenna;

  /* RSRP threshold for requesting/releasing switch control in DSDA */
  uint8    rsrp_thresh_safe_condition;

  /* Reserved bits */
  uint8    debug_crash_time;

  uint16   reserved_1;
}lte_ml1_nv_cfg_ant_switch_info_s;


typedef struct
{
  uint64 type1_band_mask;

  uint64 type2_band_mask;

  uint64 rev_0;

  uint64 rev_1;
}lte_ml1_nv_cfg_ant_switch_band_info_s;

/* Permissions for the antenna switch conf file */
#define LTE_ML1_ANT_SWITCH_CONF_FILE_PERMISSIONS 0777

#define LTE_ML1_ANT_SWITCH_CONF_FILENAME "/nv/item_files/conf/lte_ml1_ant_switch.conf"

#define LTE_ML1_ANT_SWITCH_BAND_CONF_FILENAME "/nv/item_files/conf/lte_ml1_ant_switch_band.conf"

#endif

#define LTE_ML1_SPV_TIMER_CONN_DFLT  5000

#define LTE_ML1_QCOMM_QRXLEV_MIN_DFLT  140

#define LTE_ML1_QCOMM_QQUAL_MIN_DFLT  128

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_disable_tx_cfg_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_common_nv_disable_tx_cfg_size(void);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_disable_tx_cfg_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

  void lte_ml1_common_nv_disable_tx_cfg_defaultvalue(void *ptr);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_tm_mechanism_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes.
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_common_nv_tm_mechanism_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_tm_mechanism_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/


  void lte_ml1_common_nv_tm_mechanism_defaultvalue(void *ptr);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_tx_power_backoff_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/


  uint8 lte_ml1_common_nv_tx_power_backoff_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_tx_power_backoff_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

  void lte_ml1_common_nv_tx_power_backoff_defaultvalue(void *ptr);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_pucch_cancel_size
 
 ===========================================================================*/
 /*!
	 @brief
        This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
        allocation and storing the EFS value.
        
	 @details
 
	 @return size in bytes
 */
 /*=========================================================================*/

  uint8 lte_ml1_common_nv_pucch_cancel_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_pucch_cancel_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/


  void lte_ml1_common_nv_pucch_cancel_defaultvalue(void *ptr);

	
/*===========================================================================
   
	 FUNCTION:	lte_ml1_common_nv_f3_toggle_size
   
   ===========================================================================*/
   /*!
	   @brief
		  This function returns the size of EFS variable in bytes. 
		  This size information is used by EFS framework for memory 
		  allocation and storing the EFS value.
		  
	   @details
   
	   @return size in bytes
   */
/*=========================================================================*/

  uint8 lte_ml1_common_nv_f3_toggle_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_f3_toggle_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns),
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/


 void lte_ml1_common_nv_f3_toggle_defaultvalue(void *ptr);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_f3_toggle_post_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework,
	 to perform client defined post efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/

 void  lte_ml1_common_nv_f3_toggle_post_efsread(void *ptr);

/*===========================================================================
   
	 FUNCTION:	lte_ml1_common_nv_f3_mask_size
   
   ===========================================================================*/
   /*!
	   @brief
		  This function returns the size of EFS variable in bytes. 
		  This size information is used by EFS framework for memory 
		  allocation and storing the EFS value.
		  
	   @details
   
	   @return size in bytes
   */
/*=========================================================================*/


uint8 lte_ml1_common_nv_f3_mask_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_f3_mask_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns),
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/


void lte_ml1_common_nv_f3_mask_defaultvalue(void *ptr);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_f3_mask_post_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework, 
	 to perform client defined post efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


void  lte_ml1_common_nv_f3_mask_post_efsread(void *ptr);

/*===========================================================================
   
	 FUNCTION:	lte_ml1_common_nv_axgp_size
   
 ===========================================================================*/
 /*!
	   @brief
	   This function returns the size of EFS variable in bytes. 
	   This size information is used by EFS framework for memory 
	   allocation and storing the EFS value.
		  
	   @details
   
	   @return size in bytes
 */
/*=========================================================================*/

 uint8 lte_ml1_common_nv_axgp_size(void);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_axgp_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable.
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns), 
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

 void lte_ml1_common_nv_axgp_defaultvalue(void *ptr);
 
 /*===========================================================================
  
  FUNCTION:  lte_ml1_common_nv_axgp_post_efsread
  
  ===========================================================================*/
  /*!
	  @brief
	  This function will be called post efsread success by framework,
	  to perform client defined post efsread functionality.
  
	  @details
	  .
  
	  @return
  */
  /*=========================================================================*/
 void  lte_ml1_common_nv_axgp_post_efsread(void *ptr);

/*===========================================================================
	
	  FUNCTION:  lte_ml1_common_nv_update_band_range_size
	
 ===========================================================================*/
/*!
		@brief
		This function returns the size of EFS variable in bytes. 
		This size information is used by EFS framework for memory 
		allocation and storing the EFS value.
		   
		@details
	
		@return size in bytes
*/
/*=========================================================================*/

 uint8 lte_ml1_common_nv_update_band_range_size(void);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_update_band_range_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns),
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/

 void lte_ml1_common_nv_update_band_range_defaultvalue(void *ptr);


 /*===========================================================================
   
   FUNCTION:  lte_ml1_common_nv_update_band_range_post_efsread
   
 ===========================================================================*/
/*!
	   @brief
	   This function will be called post efsread success by framework, 
	   to perform client defined post efsread functionality.
   
	   @details
	   .
   
	   @return
*/
/*=========================================================================*/

 void  lte_ml1_common_nv_update_band_range_post_efsread(void *ptr);

 /*===========================================================================
	 
	   FUNCTION:  lte_ml1_common_nv_old_band_scan_size
	 
  ===========================================================================*/
 /*!
		 @brief
		 This function returns the size of EFS variable in bytes. 
		 This size information is used by EFS framework for memory 
		 allocation and storing the EFS value.
			
		 @details
	 
		 @return size in bytes
 */
 /*=========================================================================*/

 uint8 lte_ml1_common_nv_old_band_scan_size(void);
 
 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_old_band_scan_defaultvalue
 
 ===========================================================================*/
 /*!
	 @brief
	 This function provides the default value for the efs variable. 
	 This data will be stored with the EFS framework.
 
	 @details
	 EFS framework sends the pointer to memory(which framework owns),
	 to fill in the default value.
 
	 @return
 */
 /*=========================================================================*/
 
 void lte_ml1_common_nv_old_band_scan_defaultvalue(void *ptr);


/*===========================================================================
	  
		FUNCTION:  lte_ml1_common_nv_lte_3gpp_release_ver_size
	  
 ===========================================================================*/
/*!
		  @brief
		  This function returns the size of EFS variable in bytes. 
		  This size information is used by EFS framework for memory 
		  allocation and storing the EFS value.
			 
		  @details
	  
		  @return size in bytes
*/
/*=========================================================================*/

 uint8 lte_ml1_common_nv_lte_3gpp_release_ver_size(void);

/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_lte_3gpp_release_ver_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/

 void lte_ml1_common_nv_lte_3gpp_release_ver_defaultvalue(void *ptr);

/*===========================================================================
	
	FUNCTION:  lte_ml1_common_nv_lte_3gpp_release_ver_post_efsread
	
 ===========================================================================*/
/*!
		@brief
		This function will be called post efsread success by framework, 
		to perform client defined post efsread functionality.
	
		@details
		.
	
		@return
*/
/*=========================================================================*/

 void  lte_ml1_common_nv_lte_3gpp_release_ver_post_efsread(void *ptr);


/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_ant_corr_rpt_per_size
	   
  ===========================================================================*/
 /*!
		   @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/

 uint8 lte_ml1_common_nv_ant_corr_rpt_per_size(void);
 

 /*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_ant_corr_rpt_per_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns),
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
 
 void lte_ml1_common_nv_ant_corr_rpt_per_defaultvalue(void *ptr);

/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_srl_multiplier_size
	   
  ===========================================================================*/
 /*!
		   @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/

 uint8 lte_ml1_common_nv_srl_multiplier_size(void);

 /*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_srl_multiplier_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns),
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
 
 void lte_ml1_common_nv_srl_multiplier_defaultvalue(void *ptr);

/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_plt_config_size
	   
  ===========================================================================*/
 /*!
		   @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/
  uint8 lte_ml1_common_nv_plt_config_size(void);
  
/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_plt_config_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
  void lte_ml1_common_nv_plt_config_defaultvalue(void *ptr);
 
/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_csf_log_per_size
	   
  ===========================================================================*/
 /*!
		   @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/
  uint8 lte_ml1_common_nv_csf_log_per_size(void);
 
/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_csf_log_per_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
  void lte_ml1_common_nv_csf_log_per_defaultvalue(void *ptr);

/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_vrlm_feature_enable_size
	   
  ===========================================================================*/
 /*!
		   @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/
  uint8 lte_ml1_common_nv_vrlm_feature_enable_size(void);
 
/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_vrlm_feature_enable_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
  void lte_ml1_common_nv_vrlm_feature_enable_defaultvalue(void *ptr);
 
/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_dlm_rx_cfg_app_mask_size
	   
  ===========================================================================*/
 /*!
	          @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/
  uint8 lte_ml1_common_nv_dlm_rx_cfg_app_mask_size(void);

/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_dlm_rx_cfg_app_mask_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
  void lte_ml1_common_nv_dlm_rx_cfg_app_mask_defaultvalue(void *ptr);

/*===========================================================================
	   
	 FUNCTION:	lte_ml1_common_nv_cdrx_opt_info_size
	   
  ===========================================================================*/
 /*!
	          @brief
		   This function returns the size of EFS variable in bytes. 
		   This size information is used by EFS framework for memory 
		   allocation and storing the EFS value.
			  
		   @details
	   
		   @return size in bytes
 */
 /*=========================================================================*/
  uint8 lte_ml1_common_nv_cdrx_opt_info_size(void);
 
/*===========================================================================
  
	FUNCTION:  lte_ml1_common_nv_cdrx_opt_info_defaultvalue
  
 ===========================================================================*/
/*!
	  @brief
	  This function provides the default value for the efs variable. 
	  This data will be stored with the EFS framework.
  
	  @details
	  EFS framework sends the pointer to memory(which framework owns), 
	  to fill in the default value.
  
	  @return
*/
/*=========================================================================*/
  void lte_ml1_common_nv_cdrx_opt_info_defaultvalue(void *ptr);


/*===========================================================================
	
  FUNCTION:  lte_ml1_common_nv_suspend_time_cfg_size
	
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/


 uint8 lte_ml1_common_nv_suspend_time_cfg_size(void);

/*===========================================================================

 FUNCTION:	lte_ml1_common_nv_suspend_time_cfg_defaultvalue

===========================================================================*/
/*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.

   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.

   @return
*/
/*=========================================================================*/

 
 void lte_ml1_common_nv_suspend_time_cfg_defaultvalue(void *ptr);

/*===========================================================================

FUNCTION:  lte_ml1_common_nv_suspend_time_cfg_post_efsread

===========================================================================*/
/*!
 @brief
 This function will be called post efsread success by framework,
 to perform client defined post efsread functionality.

 @details
 .

 @return
*/
/*=========================================================================*/


void  lte_ml1_common_nv_suspend_time_cfg_post_efsread(void *ptr);

#ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY

/*===========================================================================
	
  FUNCTION:  lte_ml1_common_nv_ant_switch_info_size
	
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/


 uint8 lte_ml1_common_nv_ant_switch_info_size(void);

/*===========================================================================

 FUNCTION:	lte_ml1_common_nv_ant_switch_info_defaultvalue

===========================================================================*/
/*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.

   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.

   @return
*/
/*=========================================================================*/

 
 void lte_ml1_common_nv_ant_switch_info_defaultvalue(void *ptr);

/*===========================================================================
	
  FUNCTION:  lte_ml1_common_nv_ant_switch_band_info_size
	
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/


 uint8 lte_ml1_common_nv_ant_switch_band_info_size(void);

/*===========================================================================

 FUNCTION:	lte_ml1_common_nv_ant_switch_band_info_defaultvalue

===========================================================================*/
/*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.

   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.

   @return
*/
/*=========================================================================*/

 
 void lte_ml1_common_nv_ant_switch_band_info_defaultvalue(void *ptr);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_ant_switch_info_post_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework,
	 to perform client defined post efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


void  lte_ml1_common_nv_ant_switch_info_post_efsread(void *ptr);


/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_ant_switch_band_info_post_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework,
	 to perform client defined post efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


void  lte_ml1_common_nv_ant_switch_band_info_post_efsread(void *ptr);

/*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_ant_switch_info_pre_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called pre efsread success by framework,
	 to perform client defined pre efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


 void  lte_ml1_common_nv_ant_switch_info_pre_efsread(char* efsfilepath);
 
 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_ant_switch_band_info_pre_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called pre efsread success by framework,
	 to perform client defined pre efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


 void  lte_ml1_common_nv_ant_switch_band_info_pre_efsread(char* efsfilepath);


#endif

/*===========================================================================
	
  FUNCTION:  lte_ml1_common_nv_rx_on_optz_size
	
===========================================================================*/
/*!
	@brief
	This function returns the size of EFS variable in bytes. 
	This size information is used by EFS framework for memory 
	allocation and storing the EFS value.
		   
	@details
	
	@return size in bytes
*/
/*=========================================================================*/


 uint8 lte_ml1_common_nv_rx_on_optz_size(void);

/*===========================================================================

 FUNCTION:	lte_ml1_common_nv_rx_on_optz_defaultvalue

===========================================================================*/
/*!
   @brief
   This function provides the default value for the efs variable. 
   This data will be stored with the EFS framework.

   @details
   EFS framework sends the pointer to memory(which framework owns), 
   to fill in the default value.

   @return
*/
/*=========================================================================*/

 
 void lte_ml1_common_nv_rx_on_optz_defaultvalue(void *ptr);

 
 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_rx_on_optz_post_efsread
 
 ===========================================================================*/
 /*!
	 @brief
	 This function will be called post efsread success by framework,
	 to perform client defined post efsread functionality.
 
	 @details
	 .
 
	 @return
 */
 /*=========================================================================*/


 void  lte_ml1_common_nv_rx_on_optz_post_efsread(void *ptr);

/*===========================================================================

FUNCTION:  lte_ml1_sm_nv_qcomm_qrxlev_params_size

===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes.
  This size information is used by EFS framework for memory
  allocation and storing the EFS value.

  @details

  @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_qcomm_qrxlev_params_size(void);

/*===========================================================================

FUNCTION:  lte_ml1_sm_nv_qcomm_qrxlev_default_val

===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable.
 This data will be stored with the EFS framework.

 @details
 EFS framework sends the pointer to memory(which framework owns),
 to fill in the default value.

 @return
*/
/*=========================================================================*/
void lte_ml1_sm_nv_qcomm_qrxlev_default_val(void *ptr);

/*===========================================================================

FUNCTION:  lte_ml1_sm_nv_qcomm_qqualmin_params_size

===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes.
  This size information is used by EFS framework for memory
  allocation and storing the EFS value.

  @details

  @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_sm_nv_qcomm_qqualmin_params_size(void);

/*===========================================================================

FUNCTION:  lte_ml1_sm_nv_qcomm_qqualmin_default_val

===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable.
 This data will be stored with the EFS framework.

 @details
 EFS framework sends the pointer to memory(which framework owns),
 to fill in the default value.

 @return
*/
/*=========================================================================*/
void lte_ml1_sm_nv_qcomm_qqualmin_default_val(void *ptr);

/*===========================================================================

FUNCTION:  lte_ml1_common_nv_slte_config_size

===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes.
  This size information is used by EFS framework for memory
  allocation and storing the EFS value.

  @details

  @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_common_nv_slte_config_size(void);

/*===========================================================================

FUNCTION:  lte_ml1_common_nv_slte_config_defaultvalue

===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable.
 This data will be stored with the EFS framework.

 @details
 EFS framework sends the pointer to memory(which framework owns),
 to fill in the default value.

 @return
*/
/*=========================================================================*/
void lte_ml1_common_nv_slte_config_defaultvalue(void *ptr);

/*===========================================================================

FUNCTION:  lte_ml1_common_nv_slte_config_size

===========================================================================*/
/*!
  @brief
  This function returns the size of EFS variable in bytes.
  This size information is used by EFS framework for memory
  allocation and storing the EFS value.

  @details

  @return size in bytes
*/
/*=========================================================================*/
uint8 lte_ml1_common_nv_spv_timer_conn_size(void);

/*===========================================================================

FUNCTION:  lte_ml1_common_nv_slte_config_defaultvalue

===========================================================================*/
/*!
 @brief
 This function provides the default value for the efs variable.
 This data will be stored with the EFS framework.

 @details
 EFS framework sends the pointer to memory(which framework owns),
 to fill in the default value.

 @return
*/
/*=========================================================================*/
void lte_ml1_common_nv_spv_timer_conn_defaultvalue(void *ptr);
 
#endif


