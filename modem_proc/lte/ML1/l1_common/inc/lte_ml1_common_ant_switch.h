
/*!
  @file lte_ml1_common_ant_switch.h

  @brief
  Header file for the antenna swithching feature

*/

/*===========================================================================

  Copyright (c) 2008 - 2012 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/l1_common/inc/lte_ml1_common_ant_switch.h#3 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/13/15    sk     Preferred ant changes for idle and connected mode - CR 790051
11/05/14    sk     DSDA ASDiv changes - CR 615872
10/15/14    ru     Changes to postpone TAO if it collides with AsDiv obj
05/20/14    sk     Add API to lock/unlock switch in TRM
05/19/14    sk     Skip triggering a switch if TAO <= 25ms away
04/25/14    sk     API to get time to next TAO occasion
01/15/13    sk     DSDS ASDiv Changes
10/15/12    dk     Initial Checkin 
 
===========================================================================*/

#ifndef __LTE_ML1_COMMON_ANT_SWITCH_H__
#define __LTE_ML1_COMMON_ANT_SWITCH_H__

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include <lte_ml1_comdef.h>
#include <trm.h>
#include <lte_ml1_common.h>
#include <lte_ml1_common_msg.h>
#include <lte_ml1_gm_msg.h>
#include <lte_ml1_coex.h>

#include "lte_ml1_rfmgr_ant_tuner.h"

#ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY
/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*! @brief Antenna Switch current lte mode type */
typedef enum
{
  LTE_ML1_COMMON_ANT_SWITCH_MODE_INVALID = 0,
  LTE_ML1_COMMON_ANT_SWITCH_MODE_RX,
  LTE_ML1_COMMON_ANT_SWITCH_MODE_TX,
  LTE_ML1_COMMON_ANT_SWITCH_MODE_INACTIVE
} lte_ml1_common_ant_switch_mode_e;

typedef enum
{
  LTE_ML1_COMMON_ANT_SWITCH_REASON_UNSET = 0,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_RACH,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_RF_TUNE,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_GM,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_CA,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_CA_HANDOVER,
  LTE_ML1_COMMON_ANT_SWITCH_REASON_TRM
} lte_ml1_common_ant_switch_reason_e;

typedef enum
{
  LTE_ML1_COMMON_ANT_SWITCH_MGR_READY, 
  LTE_ML1_COMMON_ANT_SWITCH_MGR_PHASE_I,
  LTE_ML1_COMMON_ANT_SWITCH_MGR_PHASE_II
} lte_ml1_common_ant_switch_mgr_state_e;

typedef enum
{
  LTE_ML1_COMMON_MODE_IDLE_WAKEUP,
  LTE_ML1_COMMON_MODE_SLEEP
} lte_ml1_common_ant_switch_trm_tag_type_e;

typedef enum
{
  LTE_ML1_COMMON_ANT_SWITCH_UNLOCK_REQUEST,
  LTE_ML1_COMMON_ANT_SWITCH_LOCK_REQUEST
} lte_ml1_common_ant_switch_update_activity;

typedef enum
{
  LTE_ML1_COMMON_ANT_SWITCH_LOCK_REASON_QTA,
  LTE_ML1_COMMON_ANT_SWITCH_LOCK_REASON_IRAT,
  LTE_ML1_COMMON_ANT_SWITCH_LOCK_REASON_VPE_RELOAD
} lte_ml1_common_ant_switch_update_reason;

/*! @brief Set mode callback */
typedef void (*lte_ml1_common_ant_switch_set_mode_cb_t)(void);

/*! @brief Call back for ant switch when CA is enabled */
typedef void (*lte_ml1_common_ant_switch_ca_cb_t)
  (lte_LL1_ant_setting_enum_t);

/*! @brief Antenna Switch Test Mode database struct type */
typedef struct 
{
  /* Flag to check if Test Mode is enabled */
  trm_ant_switch_div_test_mode_ctrl_type test_mode_ctrl;

  boolean test_mode_enabled;

  /* Dwell time to trigger a switch */
  uint64 dwell_time;

  /* Dwell time for IDLE mode - dwell_time/wakeup freq */
  uint64 idle_dwell_counter;

  /* Timer to trigger a switch if test mode is enabled */
  lte_ml1_common_timer_s ant_switch_timer;
} lte_ml1_common_ant_switch_test_ctrl_s;

/*! @brief Antenna Switch database struct type */
typedef struct
{
  /* Flag to indicate if the feature is enabled or not */
  boolean feature_enabled;

  /* Ant switch type */
  lte_LL1_ant_switch_type_enum_t ant_switch_type;

  /* Flag indicating that an antenna switch has been triggered
     and is pending */
  boolean ant_switch_pending;
  
	/* Flag indicating that an antenna switch envelope has been activated */
  boolean ant_switch_started;

  /* RSRP high threshold to execute a switch */
  uint32  rsrp_switch_threshold_hi_q7;

  /* RSRP low threshold to execute a switch */
  uint32 rsrp_switch_threshold_lo_q7;

  /* RSRP low threshold for Type 2 ant switch */
  uint32 rsrp_switch_threshold_lo_type_2_q7;

	/* RSRP threshold for Type 2*/
	uint32 rsrp_delta_threshold_type_2_q7;

	/* RSRP threshold for Type 2*/
	uint32 mtpl_gating_threshold_q7;

  /* Last RSRP difference */
  int32 last_rsrp_diff;

  /* New antenna state */
  lte_LL1_ant_setting_enum_t new_ant_state;

  /* Current antenna state */
  lte_LL1_ant_setting_enum_t curr_ant_state;

  /* Callback once the set mode is done */
  lte_ml1_common_ant_switch_set_mode_cb_t tx_set_mode_cb;

  /* trm set mode return type */
  trm_ant_switch_set_mode_return_type set_mode_grant;

  /* Buffered antenna switch req */
  boolean   queued_ant_switch;

  /* Buffered antenna switch req state */
  lte_LL1_ant_setting_enum_t queued_ant_state;

  /* Flag to check if set mode is queued */
  boolean is_queued_set_mode;

  /* Pending req to GM */
  lte_ml1_gm_antenna_switch_req_s pending_switch_req;

  /* Scheduler object for antenna switch */
  lte_ml1_schdlr_sched_req_s  schdlr_obj;

  /* ASDiv Manager State to avoid race conditions*/
  lte_ml1_common_ant_switch_mgr_state_e asdiv_mgr_state;

  /* TRM triggered antenna switch */
  boolean is_trm_switch;

  /* Antenna Switch Reason */
  lte_ml1_common_ant_switch_reason_e ant_switch_reason;

  /* Antenna position to move to when CA is active */
  uint8 ca_antenna_position;

  /* Threshold for Preferred Antenna */
  uint32 preferred_ant_thresh_q7;

  /* Preferred antenna config */
  trm_ant_switch_div_config_type preferred_ant_cfg;

  /* Threshold for SAFE condition in DSDS */
	uint32 safe_condition_thresh_q7;

  /* if race condition for tx blanking exists */
  uint32 non_lte_switch_in_progress ;

#ifdef LTE_CUST_ASDIV_CA
  boolean ca_switch_disabled;

  lte_ml1_common_ant_switch_ca_cb_t ca_switch_cb;
#endif

  /* is LTE_ML1_RECOVERY_TRIGGER_IND received */
  boolean   is_rec_tri_ind_received;

  uint8 debug_crash_timer;

} lte_ml1_common_ant_switch_info_s;


#define LTE_ML1_COMMON_ANT_SWITCH_INVALID_ACT_TIME           0xFFFF

/* number of consecutive RSRP thresold conditions after which an antenna switch
   has to be executed */
#define LTE_ML1_COMMON_ANT_SWITCH_RSRP_CONDITION_MAX              3


/* Time(in ms) after which an antenna switch has to be executed with a
   forced DTX */
#define    LTE_ML1_COMMON_ANT_SWITCH_DTX_TIMEOUT                250

#define		 LTE_ML1_COMMON_ANT_SWITCH_BETA_DENOMINATOR						256
#define    LTE_ML1_COMMON_ANT_SWITCH_TAO_THRESHOLD               25
/*===========================================================================

 FUNCTION:  lte_ml1_common_ant_switch_params_init

===========================================================================*/
/*!
   @brief
   Performs initialization of antenna switch params

   @details
   Performs initialization of antenna switch params. This function should be
   called on power up 

   @return
   None
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_params_init(
  lte_mem_instance_type instance);

/*===========================================================================

 FUNCTION:  lte_ml1_common_ant_switch_is_feat_enabled

===========================================================================*/
/*!
   @brief
     Performs initialization of antenna switch params

   @details
     Performs initialization of antenna switch params. This function should be
     called on power up 

   @return
   None
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_is_feat_enabled(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_get_curr_cfg

===========================================================================*/
/*!
    @brief
    Function called by Rf manager/DLM to get the current antenna config

    @return
    none
*/
/*=========================================================================*/
lte_LL1_ant_setting_enum_t lte_ml1_common_ant_switch_get_curr_cfg(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_get_default_cfg

===========================================================================*/
/*!
    @brief
    Function called by Rf manager/DLM to get the default(initial) antenna config

    @return
    none
*/
/*=========================================================================*/
lte_LL1_ant_setting_enum_t lte_ml1_common_ant_switch_get_default_cfg(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_init

===========================================================================*/
/*!
    @brief
    This function initializes antenna switch module.
 
    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_init(
  lte_mem_instance_type instance,
  msgr_client_t *client_ptr,             /*!< Message router client pointer*/
  msgr_id_t      mq_id                   /*!< Lower layer message queue    */
);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_params_deinit

===========================================================================*/
/*!
    @brief
    Performs deinitialization of ant_switch params

    @details
    Performs deinitialization of antenna switching realted params. 

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_params_deinit(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_handle_msg

===========================================================================*/
/*!
    @brief
    Function to handle incoming messages

    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_handle_msg(
  lte_mem_instance_type instance,
  msgr_umid_type umid,
  void *msg);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_update_rsrp

===========================================================================*/
/*!
    @brief
    Function called by search manager to update the filtered RSRPs
    measured on both antennas(Q7 format)

    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_update_rsrp(
  lte_mem_instance_type instance,
  int32 pri_rsrp_q7, int32 sec_rsrp_q7);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_set_cfg

===========================================================================*/
/*!
    @brief
    Function called by Rf manager to set the current antenna config

    @return
    none
*/
/*=========================================================================*/
trm_set_ant_switch_return_type lte_ml1_common_ant_switch_set_cfg(
  lte_mem_instance_type instance,
  lte_LL1_ant_setting_enum_t ant_cfg);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_set_mode

===========================================================================*/
/*!
    @brief
    Function to set the current mode (TX/Rx) at TRM
    @return
    none
*/
/*=========================================================================*/
lte_LL1_ant_setting_enum_t lte_ml1_common_ant_switch_set_mode(
  lte_mem_instance_type instance,
  lte_ml1_common_ant_switch_mode_e mode,
  lte_ml1_common_ant_switch_set_mode_cb_t cb,
  trm_ant_switch_set_mode_return_type *set_mode_retval);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_complete

===========================================================================*/
/*!
    @brief
    Function to Call the ant_switch_complete api in MCS
    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_complete(
  lte_mem_instance_type instance,
  lte_LL1_ant_setting_enum_t setting);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_tx_set_mode_cb

===========================================================================*/
/*!
    @brief
    CAll back function from TRM to indicate completion of TX set mode
    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_tx_set_mode_cb(
  trm_ant_switch_set_mode_type set_mode_info);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_inactive_set_mode_cb

===========================================================================*/
/*!
    @brief
    CAll back function from TRM to indicate completion of Inactive set mode
    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_inactive_set_mode_cb(
  trm_ant_switch_set_mode_type set_mode_info);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_req_cb

===========================================================================*/
/*!
    @brief
    Callback Function(from trm) requesting for an antenna switch 
    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_req_cb(
  /* ant config info type */
  trm_ant_switch_cb_info_type ant_cfg_info);

/*===========================================================================

FUNCTION:  lte_ml1_common_ant_switch_start_cb

===========================================================================*/
/*!
  @brief
  This function is the Callback from the Scheduler to start Asdiv switches
  @detail
 
  @return
  none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_start_cb(
  void *data_ptr);

/*===========================================================================

  FUNCTION:  lte_ml1_common_send_antenna_switch_txagc_mtpl_req

===========================================================================*/
/*!
    @brief
    This function sends the antenna switch request because of txagc_mtpl_cntr
	threshold .

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_send_antenna_switch_txagc_mtpl_req(
  lte_mem_instance_type instance);


/*===========================================================================

  FUNCTION:  lte_ml1_common_send_msg_reset_tx_agc_mtpl_cntr

===========================================================================*/
/*!
    @brief
    This function sends the message to GM to reset the txagc_mtpl_cntr.

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_send_msg_reset_tx_agc_mtpl_cntr(
  lte_mem_instance_type instance);


/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_set_type

===========================================================================*/
/*!
    @brief
    Set the ant switch type based on the band setting

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_set_type(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_reset_all_type_2_params

===========================================================================*/
/*!
    @brief
    Reset Type 2 parameters

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_reset_all_type_2_params(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_return_type

===========================================================================*/
/*!
    @brief
    Return the type of Antenna Switching
 
    @return
    None

*/
/*=========================================================================*/
lte_LL1_ant_switch_type_enum_t lte_ml1_common_ant_switch_return_type(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_get_ant_switch_feature_enabled

===========================================================================*/
/*!
    @brief
    Return if the feature is enabled

    @return
    None

*/
/*=========================================================================*/
boolean lte_ml1_common_get_ant_switch_feature_enabled(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_reset_hysteresis

===========================================================================*/
/*!
    @brief
    Reset the hysteresis counter on Connected state exit

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_reset_hysteresis(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_is_ant_switch_pending

===========================================================================*/
/*!
    @brief
    Check if there is an ant switch pending in manager thread
 
    @return
    None

*/
/*=========================================================================*/
boolean lte_ml1_common_is_ant_switch_pending(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_set_queued_mode

===========================================================================*/
/*!
    @brief
    Set mode to be queued or not

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_set_queued_mode(
  lte_mem_instance_type instance,
  boolean set_queued);

/*===========================================================================

  FUNCTION:  lte_ml1_common_is_lte_switch_ready

===========================================================================*/
/*!
    @brief
    Is LTE ready to perform a switch

    @return
    TRUE/FALSE depending on UMID LTE L1 Manager is processing

*/
/*=========================================================================*/
boolean lte_ml1_common_is_lte_antenna_switch_ready(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_cancel_antenna_switch

===========================================================================*/
/*!
    @brief
    Cancel antenna switch as per TRM - deschedule ASDiv object and unblock
    Manager

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_cancel_antenna_switch(
  lte_mem_instance_type instance,
  boolean tam_suspend);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_send_switch_req

===========================================================================*/
/*!
    @brief
    Send switch req to GM

    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_send_switch_req(
  lte_mem_instance_type instance,
  lte_LL1_ant_setting_enum_t state,
  lte_ml1_common_ant_switch_reason_e switch_reason);

/*===========================================================================

  FUNCTION:  lte_ml1_common_clear_switch_request

===========================================================================*/
/*!
    @brief
    Clear antenna switch as ML1 is not ready

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_clear_switch_request(
  lte_mem_instance_type instance,
  lte_LL1_ant_setting_enum_t state,
  lte_ml1_common_ant_switch_reason_e switch_reason);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_register_obj

===========================================================================*/
/*!
    @brief
    Register the Asdiv envelope object with the scheduler 

    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_register_obj(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_deregister_obj

===========================================================================*/
/*!
    @brief
    Deregister the Asdiv envelope object with the scheduler 

    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_deregister_obj(
  void);

#ifdef LTE_CUST_ASDIV_CA
/*===========================================================================

  FUNCTION:  lte_ml1_common_disable_asdiv_for_ca

===========================================================================*/
/*!
    @brief
    Disable antenna switch algorithm if CA is configured

    @return
    None

*/
/*=========================================================================*/
boolean lte_ml1_common_disable_asdiv_for_ca(
  lte_mem_instance_type instance,
  lte_ml1_common_ant_switch_reason_e switch_reason,
  lte_ml1_common_ant_switch_ca_cb_t callback);

/*===========================================================================

  FUNCTION:  lte_ml1_common_disable_asdiv_for_ca

===========================================================================*/
/*!
    @brief
    Disable antenna switch algorithm if CA is configured

    @return
    TRUE if a switch is required, FALSE if no switch is required

*/
/*=========================================================================*/
boolean lte_ml1_common_disable_asdiv_for_ca_handover(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_enable_asdiv_after_ca

===========================================================================*/
/*!
    @brief
    Enable antenna switch algorithm if CA is de-configured
 
    @return
    None

*/
/*=========================================================================*/
boolean lte_ml1_common_move_to_ca_config(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_enable_asdiv_after_ca

===========================================================================*/
/*!
    @brief
    Enable antenna switch algorithm if CA is de-configured

    @return
    None

*/
/*=========================================================================*/
void lte_ml1_common_enable_asdiv_after_ca(
  lte_mem_instance_type instance);

#endif /* #ifdef LTE_CUST_ASDIV_CA */

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_set_preferred_antenna_cfg

===========================================================================*/
/*!
    @brief
    Set the preferred antenna config before going to sleep
 
    @return
    None

*/
/*=========================================================================*/
lte_LL1_ant_setting_enum_t lte_ml1_common_ant_switch_set_preferred_antenna_cfg(
  lte_mem_instance_type instance,
  boolean is_conn_to_idle_transition,
  trm_ant_switch_set_mode_return_type *set_mode_retval);
  

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_get_preferred_antenna_cfg

===========================================================================*/
/*!
    @brief
    Get the preferred antenna config before waking RF on entering LTE

    @return
    Antenna Config
 
*/
/*=========================================================================*/
lte_LL1_ant_setting_enum_t lte_ml1_common_ant_switch_get_preferred_antenna_cfg(
  lte_mem_instance_type instance
);

/*===========================================================================

  FUNCTION:  lte_ml1_common_preferred_ant_cb

===========================================================================*/
/*!
    @brief
    Callback Function(from trm) granting preferred antenna config 
    @return
    none
*/
/*=========================================================================*/
void lte_ml1_common_preferred_ant_cb(
  /* ant config info type */
  trm_asd_set_mode_and_config_type ant_cfg_info);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_update_trm_switch

===========================================================================*/
/*!
    @brief
    Lock/Unlock the antenna switch in TRM for QTA/IRAT 
 
    @return
    TRUE if TRM is able to update the switch else FALSE
*/
/*=========================================================================*/
boolean lte_ml1_common_ant_switch_update_trm_switch
( 
  lte_mem_instance_type instance,
  lte_ml1_common_ant_switch_update_activity activity,
  lte_ml1_common_ant_switch_update_reason update_reason
);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_get_time_to_tao

===========================================================================*/
/*!
    @brief
    Calculate the time for TAO schedule from now
 
    @return
    Time in ms to next TAO schedule
*/
/*=========================================================================*/
uint16 lte_ml1_common_ant_switch_get_time_to_tao(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_get_time_in_asdiv_env

===========================================================================*/
/*!
    @brief
    Calculate the time remaining for ASDiv env deschedule from now
 
    @details
    Calculate the time remaining in ASDiv env deschedule
    (Start time + Min activation Window) - Current_time 
 
    @return
    Invalid if ASDiv is not started / time in ms remaining in min activation 
    window for ASDiv object 
*/ 
/*=========================================================================*/
void lte_ml1_common_ant_switch_get_time_in_asdiv_env(
  lte_mem_instance_type instance,
  boolean *is_asdiv_env_active,
  lte_ml1_schdlr_sf_time_t *time_remaining,
  lte_ml1_schdlr_sf_time_t start_time_sf);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_is_test_mode_enabled

===========================================================================*/
/*!
    @brief
    Check if ASDiv test mode in NV 70219 is set 
 
    @details
    NV 70219 is used to enable/disable test mode. In test mode, we disable
    normal ASDiv algorithm and trigger switch periodically according to time
    set. This function checks if test mode is enabled.
 
    @return
    TRUE if NV 70219 is set.
    FALSE if NV is not set.
*/ 
/*=========================================================================*/
boolean lte_ml1_common_ant_switch_is_test_mode_enabled(
  lte_mem_instance_type instance);


/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_test_timer_start

===========================================================================*/
/*!
    @brief
    Start the antenna switch timer in test mode 
 
    @details
    Start ASDiv timer in test mode with duration dwell_time set in NV 70219
 
    @return
    None
*/ 
/*=========================================================================*/
void lte_ml1_common_ant_switch_test_timer_start(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_test_timer_stop

===========================================================================*/
/*!
    @brief
    Stop the antenna switch timer in test mode 
 
    @details
    Stop ASDiv timer in test mode
 
    @return
    None
*/ 
/*=========================================================================*/
void lte_ml1_common_ant_switch_test_timer_stop(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_test_timer_stop

===========================================================================*/
/*!
    @brief
    Trigger an antenna switch 
 
    @details
    This function is called when ASDiv test mode timer expires.
    Trigger an antenna switch in this case.
 
    @return
    None
*/ 
/*=========================================================================*/
void lte_ml1_common_ant_switch_timer_expiry(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_read_test_mode

===========================================================================*/
/*!
    @brief
    Read NV 70219 
 
    @details
    Read the test mode enabled and dwell time from NV 70219
 
    @return
    None
*/ 
/*=========================================================================*/
void lte_ml1_common_read_test_mode(
  lte_mem_instance_type instance);

/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_recovery_trigger_ind

===========================================================================*/
/*!
    @brief
    set the is_rec_tri_ind_received to TRUE
 
    @return
    
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_recovery_trigger_ind
( 
  lte_mem_instance_type instance
);
/*===========================================================================

  FUNCTION:  lte_ml1_common_ant_switch_recovery_cleanup

===========================================================================*/
/*!
    @brief
    set the is_rec_tri_ind_received to FALSE
 
    @return
    
*/
/*=========================================================================*/
void lte_ml1_common_ant_switch_recovery_cleanup
( 
  lte_mem_instance_type instance
);

#endif /* #ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY */

#endif /* __LTE_ML1_COMMON_ANT_SWITCH_H__ */




