/*!
  @file
  lte_ml1_common_nv_intf.h

  @brief
  header file for efs related operations across ML1.

  @detail
*/

/*===========================================================================

  Copyright (c) 2009 - 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/l1_common/inc/lte_ml1_common_nv_intf.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
24/10/13    ap      Initial version
===========================================================================*/

#ifndef LTE_ML1_COMMON_NV_INTF_H
#define LTE_ML1_COMMON_NV_INTF_H

#include <lte_ml1_mem.h>
#include "IxErrno.h"
/*===========================================================================

                           INCLUDE FILES

===========================================================================*/



/*===========================================================================

                  MODULE DEFINITIONS AND TYPES

===========================================================================*/

#define LTE_ML1_EFS_ROOT_DIRECTORY      "/nv/item_files/modem/lte/ML1/" 
/*Note: for stack specific efs file directory structure will be  
 "/nv/item_files/modem/lte/ML1/stack0/" */

#define LTE_ML1_EFS_MAX_LEN_FILEPATH  120 
#define LTE_ML1_EFS_MAX_LEN_FILENAME  32

/*EFS Unique Identifiers*/
typedef enum  
{ 
LTE_ML1_MIN_EFS_UIDS = 0,
/*EFS UID 0*/
LTE_ML1_UNKNOWN_MBSFN_EFS = 0,
/*EFS UID 1*/
LTE_ML1_DISABLE_TX_ON_EFS,
/*EFS UID 2*/
LTE_ML1_SPS_TTI_EFS,
/*EFS UID 3*/
LTE_ML1_TM_MECHANISM_EFS,
/*EFS UID 4*/
LTE_ML1_TX_POWER_BACKOFF_EFS,
/*EFS UID 5*/
LTE_ML1_PUCCH_CANCEL_EFS,
/*EFS UID 6*/
LTE_ML1_F3_TOGGLE_EFS,
/*EFS UID 7*/
LTE_ML1_F3_MASK_EFS,
/*EFS UID 8*/
LTE_ML1_RFMGR_RXTOOLCHAIN_EFS,
/*EFS UID 9*/
LTE_ML1_RFMGR_ANT_TUNER_EFS,
/*EFS UID 10*/
LTE_ML1_SEARCH_MEAS_MASK_EFS,
/*EFS UID 11*/
LTE_ML1_AXGP_EFS,
/*EFS UID 12*/
LTE_ML1_UPDATE_BAND_RANGE_EFS,
/*EFS UID 13*/
LTE_ML1_CONN_SERV_MEAS_FREQ_EFS,
/*EFS UID 14*/
LTE_ML1_CAMP_BAND_EARFCN_EFS,
/*EFS UID 15*/
LTE_ML1_OLD_BAND_SCAN_EVAL_ALGO_EFS,
/*EFS UID 16*/
LTE_ML1_BS_BW_SUPPORTED_EFS,
/*EFS UID 17*/
LTE_ML1_BS_THRESHOLDS_EFS,
/*EFS UID 18*/
LTE_ML1_DISABLE_GSM_OVERLAP_OPT_EFS,
/*EFS UID 19*/
LTE_ML1_LTE_MODE_EFS,
/*EFS UID 20*/
LTE_ML1_TOTAL_TX_PWR_EFS,
/*EFS UID 21*/
LTE_ML1_UE_BW_CFG_EFS,
/*EFS UID 22*/
LTE_ML1_UE_EARFCN_BW_CFG_EFS,
/*EFS UID 23*/
LTE_ML1_UE_CAMP_CFG_EFS,
/*EFS UID 24*/
LTE_ML1_UE_EXT_DEBUG_EFS,
/*EFS UID 25*/
LTE_ML1_NUM_NODEB_ANT_EFS,
/*EFS UID 26*/
LTE_ML1_3GPP_REL_VERSION_EFS,
/*EFS UID 27*/
LTE_ML1_EMBMS_PRIORITY_CFG_EFS,
/*EFS UID 28*/
LTE_ML1_SLEEP_TCXO_VOTE_ASSERT_EFS,
/*EFS UID 29*/
LTE_ML1_SLEEP_PWR_STROBE_CFG_EFS,
/*EFS UID 30*/
LTE_ML1_COEX_CHANNEL_PRIORITY_EFS,
/*EFS UID 31*/
LTE_ML1_COEX_STATE_EFS,
/*EFS UID 32*/
LTE_ML1_ANT_CORR_RPT_PER_EFS,
/*EFS UID 33*/
LTE_ML1_SRL_MULTIPLIER_EFS,
/*EFS UID 34*/
LTE_ML1_OFFLINE_THRESH_EFS,
/*EFS UID 35*/
LTE_ML1_OFFLINE_PANIC_THRESH_EFS,
/*EFS UID 36*/
LTE_ML1_MIN_NBR_RSRP_THRESH_EFS,
/*EFS UID 37*/
LTE_ML1_ENABLE_FILTERING_EFS,
/*EFS UID 38*/
LTE_ML1_USE_RSRP_INST_EFS,
/*EFS UID 39*/
LTE_ML1_MIN_SSS_THRESH_DB_EFS,
/*EFS UID 40*/
LTE_ML1_ONE_IFREQ_LAYER_EFS,
/*EFS UID 41*/
LTE_ML1_SRCH_PRUNE_CNT_EFS,
/*EFS UID 42*/
LTE_ML1_MEAS_PRUNE_CNT_EFS,
/*EFS UID 43*/
LTE_ML1_SAMPLE_REC_TIMES_EFS,
/*EFS UID 44*/
LTE_ML1_IGNORE_TRESEL_CFG_EFS,
/*EFS UID 45*/
LTE_ML1_OFFLINE_PANIC_USE_RSRQ_EFS,
/*EFS UID 46*/
LTE_ML1_S_INTRA_SEARCH_EFS,
/*EFS UID 47*/
LTE_ML1_S_NON_INTRA_SEARCH_EFS,
/*EFS UID 48*/
LTE_ML1_FREQ_ERROR_LOG_PERIOD_MSEC_EFS,
/*EFS UID 49*/
LTE_ML1_ONEX_FAKE_MEAS_ENABLED_EFS,
/*EFS UID 50*/
LTE_ML1_ONEX_CONN_CONFIG_EFS,
/*EFS UID 51*/
LTE_ML1_HRPD_CONN_CONFIG_EFS,
/*EFS UID 52*/
LTE_ML1_PRS_CONFIG_EFS,
/*EFS UID 53*/
LTE_ML1_NUM_ENB_TX_ANT_EFS,
/*EFS UID 54*/
LTE_ML1_VERIFY_SIB8_SYSTIME_EFS,
/*EFS UID 55*/
LTE_ML1_AFC_CONFIG_EFS,
/*EFS UID 56*/
LTE_ML1_SCHDLR_CONFIG_EFS,
/*EFS UID 57*/
LTE_ML1_CHECK_FOR_SFN_MISMATCH_EFS,
/*EFS UID 58*/
LTE_ML1_SCHDLR_DOG_CONFIG_EFS,
/*EFS UID 59*/
LTE_ML1_FW_RECOVERY_EFS,
/*EFS UID 60*/
LTE_ML1_PLT_CONFIG_EFS,
/*EFS UID 61*/
LTE_ML1_COMMON_CSF_LOG_PER_EFS,
/*EFS UID 62*/
LTE_ML1_CDRX_OPT_INFO_EFS,
/*EFS UID 63*/
LTE_ML1_COMMON_EXT_CP_EFS,
/*EFS UID 64*/
LTE_ML1_ACQ_USE_BLACKLIST_EFS,
/*EFS UID 65*/
LTE_ML1_SERV_AS_NGBR_EFS,
/*EFS UID 66*/
LTE_ML1_DEFAULT_MEAS_CFG_EFS,
/*EFS UID 67*/
LTE_ML1_TX_CFG_EFS,
/*EFS UID 68*/
LTE_ML1_MCVS_MODE_EFS,
/*EFS UID 69*/
LTE_ML1_RLF_ON_TTL_DRIFT_EFS,
/*EFS UID 70*/
LTE_ML1_VRLM_FEATURE_ENABLED_EFS,
/*EFS UID 71*/
LTE_ML1_DLM_RX_CFG_APP_MASK_EFS,
/*EFS UID 72*/
LTE_ML1_SEARCH_MEAS_CELL_LIST_PARAMS_EFS,
/*EFS UID 73*/
LTE_ML1_SLEEP_CONFIG_IDLE_FDD_EFS,
/*EFS UID 74*/
LTE_ML1_SLEEP_CONFIG_CONN_FDD_EFS,
/*EFS UID 75*/
LTE_ML1_SLEEP_CONFIG_IDLE_TDD_EFS,
/*EFS UID 76*/
LTE_ML1_SLEEP_CONFIG_CONN_TDD_EFS,
/*EFS UID 77*/
LTE_ML1_SLEEP_SETUP_IDLE_FDD_EFS,
/*EFS UID 78*/
LTE_ML1_SLEEP_SETUP_CONN_FDD_EFS,
/*EFS UID 79*/
LTE_ML1_SLEEP_SETUP_IDLE_TDD_EFS,
/*EFS UID 80*/
LTE_ML1_SLEEP_SETUP_CONN_TDD_EFS,
/*EFS UID 81*/
LTE_ML1_SLEEP_DYN_DRX_IDLE_FDD_EFS,
/*EFS UID 82*/
LTE_ML1_SLEEP_DYN_DRX_CONN_FDD_EFS,
/*EFS UID 83*/
LTE_ML1_SLEEP_DYN_DRX_IDLE_TDD_EFS,
/*EFS UID 84*/
LTE_ML1_SLEEP_DYN_DRX_CONN_TDD_EFS,
/*EFS UID 85*/
LTE_ML1_RF_WARMUP_USEC_IDLE_FDD_EFS,
/*EFS UID 86*/
LTE_ML1_RF_WARMUP_USEC_CONN_FDD_EFS,
/*EFS UID 87*/
LTE_ML1_RF_WARMUP_USEC_IDLE_TDD_EFS,
/*EFS UID 88*/
LTE_ML1_RF_WARMUP_USEC_CONN_TDD_EFS,
/*EFS UID 89*/
LTE_ML1_ACQ_AFTER_CONN_REL_EFS,
#ifndef FEATURE_BOLT_MODEM
/*EFS UID 90*/
LTE_ML1_BIMC_SNOC_EFS,
#endif
/*EFS UID 91*/
LTE_ML1_COMMON_SUSPEND_TIME_EFS,
/*EFS UID 92*/
LTE_ML1_PRUNE_W_FREQ_EFS,
/*EFS UID 93*/
LTE_ML1_STICKON_LTECSG_ENABLED_EFS,
/*EFS UID 94*/
LTE_ML1_TX_POWER_COST_EFS,
/*EFS UID 95*/
LTE_ML1_ASSERT_ON_VRLF_CAUSE_EFS,
#ifdef FEATURE_LTE_ANTENNA_SWITCH_DIVERSITY
/*EFS UID 96*/
LTE_ML1_ANT_SWITCH_INFO_EFS,
/*EFS UID 97*/
LTE_ML1_ANT_SWITCH_BAND_INFO_EFS,
#endif
/*EFS UID 98*/
LTE_ML1_RX_ON_OPTZ_FEATURE_ENABLED_EFS,
/*EFS UID 99*/
LTE_ML1_SEARCH_ENABLE_VCELL_MODE_EFS,
/*EFS UID 100*/
LTE_ML1_SEARCH_MEAS_ENABLE_PRSRQ,
/* EFS UID 101 */
LTE_ML1_DELTA_S_CRITERIA_FAIL_TRIGGER_MEAS_EFS,
/* EFS UID 102 */
LTE_ML1_NUMBER_OF_CELL_SELECT_MEAS_EFS,
/* EFS UID 103 */
LTE_ML1_TIME_DELTA_CELL_SELECT_MEAS_EFS,
/* EFS UID 104 */
LTE_ML1_FW_TIMERS_EFS,
/*EFS UID 105*/
LTE_ML1_CDRX_COLLISION_PARAMS,
/* EFS UID 106 */
LTE_ML1_SLTE_CONFIG_EFS,
/* EFS UID 107 */
LTE_ML1_SPV_TIMER_CONN_EFS,
/* EFS UID 108 */
LTE_ML1_IS_CDRX_OFF_SCHEDULING_EFS,
/* EFS UID 109 */
LTE_ML1_RSRP_ACQ_THRESHOLD_EFS,
/* EFS UID 110 */
LTE_ML1_RSRP_ACQ_SF_DELAY_EFS,
/* EFS UID 111 */
LTE_ML1_COEX_DSDA_ENABLE_EFS,
/* EFS UID 112 */
LTE_ML1_SM_ACQ_MAX_RETRY_EFS,
/* EFS UID 113 */
LTE_ML1_SM_MAX_SEARCH_MEAS_RETRY_EFS,
/* EFS UID 114 */
LTE_ML1_SM_MAX_CELL_SELECT_MEAS_RETRY_DSDA_EFS,
/* EFS UID 115 */
LTE_ML1_GM_QTA_PWR_ACCUM_DELAY,
/*EFS UID 116*/
LTE_ML1_QCOMM_QRXLEV_EFS,
/*EFS UID 117*/
LTE_ML1_QCOMM_QQUALMIN_EFS,
/* EFS UID 118 */
LTE_ML1_TX_BLANK_BEFORE_QTA,
#ifdef FEATURE_DIMEPM_MODEM
/* EFS UID 119 */
LTE_ML1_GM_LTE_DATA_RATE_DETECT_EFS,
#endif
/*MAX EFS */
LTE_ML1_MAX_EFS_UIDS
}lte_ml1_nv_item_uid_e;

/*Enum to indicate whether the efs is read from 
  root directory or stack specific directory*/
 typedef enum  
 {    
   LTE_ML1_ROOT_EFS = 0,    
   LTE_ML1_STACK_EFS  
 }lte_ml1_nv_item_folder_type_e;


/*Data structure for each EFS Client*/
typedef struct  
 {  
   /*EFS UID*/
   lte_ml1_nv_item_uid_e            nv_item_uid;    
   
   /*efs client defined functionality pre efsget ifany*/
   
   void  (*nv_item_pre_efsread)     (char* efsfilepath);    
   
   /*efs client defined functionality post efsget ifany*/
	
   void  (*nv_item_post_efsread)    (void *ptr);

   /*Function pointer returns the size in bytes*/
	
   uint8 (*nv_item_size)            (void);

   /*Function pointer to get default vaules*/  
   
   void  (*nv_item_default_value)   (void *ptr);

   /*EFS Filename*/
   
   char  efs_filename[LTE_ML1_EFS_MAX_LEN_FILENAME];
   
 }lte_ml1_nv_table_s;


 /*Framework data structure to hold the read EFS data*/
 typedef struct
 {
   lte_mem_instance_type   instance;/*Stack instance type*/
   
   void *nv_item_mem;
   
   void *nv_item_ptr[LTE_ML1_MAX_EFS_UIDS];
   
   boolean efs_success[LTE_ML1_MAX_EFS_UIDS];
   
 }lte_ml1_nv_cfg_s;


 /*===========================================================================
 
					 EXTERNAL FUNCTION PROTOTYPES
 
 ===========================================================================*/
 
 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_cfg_init
 
 ===========================================================================*/
 /*!
	 @brief
	 This function initializes and obtains the ML1 related configuration 
	 parameters from NV memory. Should be called from L1M during init.
 
	 @details
	 Function called in ML1 context
 
	 @return SUCESS/FAILURE
 */
 /*=========================================================================*/
 errno_enum_type lte_ml1_common_nv_cfg_init(lte_mem_instance_type   instance);

 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_cfg_deinit
 
 ===========================================================================*/
 /*!
	 @brief
	 This function deinitializes and obtains the ML1 related configuration
	 parameters from NV memory
 
	 @details
	 
 
	 @return
 */
 /*=========================================================================*/
 void lte_ml1_common_nv_cfg_deinit(lte_mem_instance_type   instance);

 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_get
 
 ===========================================================================*/
 /*!
	 @brief
	 This function gets the EFS data which is stored with framework,
	 based on EFS UID
 
	 @details
	 Function called in ML1 context
 
	 @return void pointer to EFS data
 */
 /*=========================================================================*/
 void*  lte_ml1_common_nv_get(lte_mem_instance_type instance,lte_ml1_nv_item_uid_e nv_id);

 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_write
 
 ===========================================================================*/
 /*!
	 @brief
	 This function write the data in to NV memory
 
	 @details
	 Function called in ML1 context
 
	 @return
 */
 /*=========================================================================*/
 
 int lte_ml1_common_nv_write(lte_ml1_nv_item_uid_e nv_id, void* data, uint8 size);

  
 /*===========================================================================
 
   FUNCTION:  lte_ml1_common_nv_cfg_get_qcomm_q_rxlevmin
 
 ===========================================================================*/
 /*!
	 @brief
	 This function reads the qcomm qrxlevmin EFS file
 
	 @return
	 None
 */
 /*=========================================================================*/
 int32 lte_ml1_common_nv_cfg_get_qcomm_q_rxlevmin(lte_mem_instance_type instance);

 /*===========================================================================

  FUNCTION:  lte_ml1_common_nv_cfg_get_qcomm_q_qualmin

===========================================================================*/
/*!
    @brief
    This function reads the qcomm qqual min EFS file

    @return
    None
*/
/*=========================================================================*/
int32 lte_ml1_common_nv_cfg_get_qcomm_q_qualmin( lte_mem_instance_type instance );
#endif

