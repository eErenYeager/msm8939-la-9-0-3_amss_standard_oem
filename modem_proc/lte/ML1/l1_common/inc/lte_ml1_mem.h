#ifndef LTE_ML1_MEM_H
#define LTE_ML1_MEM_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

           L T E  M L 1  D Y N A M I C   M E M O R Y   M O D U L E

GENERAL DESCRIPTION
      This module contains functions for LTE ML1 dynamic memory allocation.

EXTERNALIZED FUNCTIONS
      lte_ml1_mem_init
      lte_ml1_mem_deinit
      lte_ml1_mem_allocate
      lte_ml1_mem_release

INITIALIZATION AND SEQUENCING REQUIREMENTS
      Must call lte_ml1_mem_init() before any other functions in this module
      are invoked.

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    Copyright (c) 2013
                  by Qualcomm Technologies, Inc.  All Rights Reserved.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //Commercial/MPSS.DPM.2.0.2.c1/Main/modem_proc/lte/ML1/l1_common/inc/lte_ml1_mem.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/28/13   yp      Initial revision.

===========================================================================*/


/*===========================================================================

                        INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include <msgcfg.h>
#include <modem_mem.h>
#include <msg.h>
#include <err.h>
#include <custlte.h>
#include "msg_mask.h"

/*-------------------------------------------------------------------------
      Constants and Macros
--------------------------------------------------------------------------*/

/* Enable LTE memory debug messages */
/* #define LTE_ML1_MEM_DEBUG */

/* Search memory message macros */
#define LTE_MEM_MSG( lvl, fmt )                                             \
          MSG( MSG_SSID_LTE_ML1, lvl, fmt )

#define LTE_MEM_MSG_1( lvl, fmt, a1 )                                       \
          MSG_1( MSG_SSID_LTE_ML1, lvl, fmt, a1 )

#define LTE_MEM_MSG_2( lvl, fmt, a1, a2 )                                   \
          MSG_2( MSG_SSID_LTE_ML1, lvl, fmt, a1, a2 )

#define LTE_MEM_MSG_4( lvl, fmt, a1, a2, a3, a4 )                           \
          MSG_4( MSG_SSID_LTE_ML1, lvl, fmt, a1, a2, a3, a4 )

#define LTE_ML1_MEM_ALLOCATE( instance, size )                              \
          lte_ml1_mem_allocate( instance, size, msg_file, __LINE__ )

#define LTE_ML1_MEM_RELEASE( instance, alloc_ptr )                              \
          lte_ml1_mem_release( instance, alloc_ptr, msg_file, __LINE__ )

/* Value when no memory is allocated */
#define LTE_ML1_MEM_NONE                 0

/*-------------------------------------------------------------------------
      Typedefs
-------------------------------------------------------------------------*/

/* Bitmask value for assigning each client a bit */
typedef uint8 lte_mem_instance_type;

/*=============================================================================

                      FUNCTION DECLARATIONS FOR MODULE

=============================================================================*/


/*=============================================================================

FUNCTION       LTE_ML1_MEM_QUERY_ALLOC_STATE
DESCRIPTION    Queries the search memory module allocation state. It prints 
               out all the modules which have memory allocated and also all
               those that have released their allocation.

DEPENDENCIES   The lte_ml1_mem_int() routine must have been called previously.

RETURN VALUE   The total count of allocations

SIDE EFFECTS   None

=============================================================================*/
int lte_ml1_mem_query_alloc_state( void );


/*=============================================================================

FUNCTION       LTE_ML1_MEM_QUERY_INSTANCE
DESCRIPTION    Queries the instance number of the module from thread 
               local storage.

DEPENDENCIES   The lte_ml1_mem_init() routine must have been called previously.

RETURN VALUE   None

SIDE EFFECTS   None

=============================================================================*/
lte_mem_instance_type lte_ml1_mem_query_instance( void );


/*=============================================================================

FUNCTION       LTE_ML1_MEM_INIT
DESCRIPTION    Initialize the LT ML1 memory module. This function should
               only be called once during srch task startup and must be
               called before invoking any other routines in the LTE ML1
               memory module. This routine performs all necessary work
               to initialize the search memory client data strucutre
               in preparation for handling memory allocation requests.

DEPENDENCIES   None

RETURN VALUE   None

SIDE EFFECTS   None

=============================================================================*/
void lte_ml1_mem_init( void );

/*=============================================================================

FUNCTION       LTE_ML1_MEM_DEINIT
DESCRIPTION    De-initialize the srch memory module. This function clears
               the search memory client data structure and returns the
               memory module to a pre-initialization state. Once this
               routine is called, lte_ml1_mem_init() again needs to be called
               before any other calls to the search memory module can be
               issued.

DEPENDENCIES   The lte_ml1_mem_init() routine must have been called previously.

RETURN VALUE   None

SIDE EFFECTS   None

=============================================================================*/
void lte_ml1_mem_deinit( void );

/*=============================================================================

FUNCTION       LTE_ML1_MEM_ALLOCATE
DESCRIPTION    Dynamically allocate memory for LTE ML1 data structures.
               This function ensures a valid client and then allocates
               memory appropriately. If the client has already allocated
               memory, then this routine returns the pointer previously
               allocated to the client.

DEPENDENCIES   The lte_ml1_mem_int() routine must have been called previously.

RETURN VALUE   Returns a pointer to the newly allocated memory block.
               If the memory block could not be allocated, this function
               will result in an error fatal as something has gone
               seriously wrong if search is denied the necessary memory.

SIDE EFFECTS   Memory area is zero initialized irrespective of whether
               previously allocated or newly allocated.

=============================================================================*/
extern inline void* lte_ml1_mem_allocate
(
  /* Unique identifier corresponding to each instance of a module */
  lte_mem_instance_type instance,

  /* Number of bytes to allocate */
  size_t size,

  /* Filename */
  char* file,

  /* Line number */
  int32 line_num
);

/*=============================================================================

FUNCTION       LTE_ML1_MEM_RELEASE
DESCRIPTION    Releases memory allocated to the client. This function
               ensures a valid client and then releases memory accordingly.
               If there is any descrepancy between the release pointer and
               the client's allocated memory pointer (such as a client
               attempting to release memory it did not allocate), then
               this routine will issue an error message and proceed without
               releasing the un-owned memory.

DEPENDENCIES   The lte_ml1_mem_int() routine must have been called previously.

RETURN VALUE   None

SIDE EFFECTS   None.

=============================================================================*/
void lte_ml1_mem_release
(
  /* Unique identifier corresponding to each instance of a module */
  lte_mem_instance_type instance,

  /* Pointer to memory to release */
  void *alloc_ptr,

  /* Filename */
  char* file,

  /* Line number */
  int32 line_num
);

#endif /* LTE_ML1_MEM_H */
