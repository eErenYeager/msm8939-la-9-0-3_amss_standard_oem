/*!
  @file
  lte_ml1_common_dump.h

  @brief
  ML1 data dumper header file.
*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/10/13   sc      Initial checkin

===========================================================================*/

#ifndef LTE_ML1_COMMON_DUMP_H
#define LTE_ML1_COMMON_DUMP_H

/*===========================================================================

                           INCLUDE FILES

=========================================================================== */
#include "msgr_umid.h"
#include "lte_ml1_mem.h"

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

  FUNCTION:  lte_ml1_common_dump_proc_msg

===========================================================================*/
/*!
    @brief
    Handles messages sent to common_dump

    @return
    None
*/
/*=========================================================================*/
void lte_ml1_common_dump_proc_msg
(
    /*! umid of the received message */
    msgr_umid_type umid,
    /*! message payload of the message */
    void *payload,
    /*! current instance number */
    lte_mem_instance_type inst_num
);

#endif /* LTE_ML1_COMMON_DUMP_H */

