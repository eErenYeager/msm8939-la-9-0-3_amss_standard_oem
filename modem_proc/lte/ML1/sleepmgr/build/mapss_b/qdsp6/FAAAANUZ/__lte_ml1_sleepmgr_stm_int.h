/*=============================================================================

    __lte_ml1_sleepmgr_stm_int.h

Description:
  This file contains the machine generated header file for the state machine
  specified in the file:
  lte_ml1_sleepmgr_stm.stm

=============================================================================*/

/*===========================================================================

  Copyright (c) 2018 Qualcomm Technologies Incorporated.
  All Rights Reserved
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/


#ifndef __LTE_ML1_SLEEPMGR_STM_INT_H
#define __LTE_ML1_SLEEPMGR_STM_INT_H

#ifdef __cplusplus
/* If compiled into a C++ file, ensure symbols names are not mangled */
extern "C"
{
#endif

/* Include external state machine header */
#include "__lte_ml1_sleepmgr_stm.h"

/* Begin machine generated internal header for state machine array: LTE_ML1_SLEEPMGR_STM[] */

/* Suppress Lint suggestions to const-ify state machine and payload ptrs */
/*lint -esym(818,sm,payload) */

/* Define a macro for the number of SM instances */
#define LTE_ML1_SLEEPMGR_STM_NUM_INSTANCES 1

/* Define a macro for the number of SM states */
#define LTE_ML1_SLEEPMGR_STM_NUM_STATES 12

/* Define a macro for the number of SM inputs */
#define LTE_ML1_SLEEPMGR_STM_NUM_INPUTS 31

#ifndef STM_DATA_STRUCTURES_ONLY
/* State Machine entry/exit function prototypes */
void sleepmgr_enter_stm(stm_state_machine_t *sm,void *payload);
void sleepmgr_exit_stm(stm_state_machine_t *sm,void *payload);


/* State entry/exit function prototypes */
void sleepmgr_enter_inactive(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_online(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_online_sleep_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_exit_online_sleep_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_sleep(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_online_wakeup(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_exit_online_wakeup(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_ttl_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_exit_ttl_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_light_sleep_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_light_sleep(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_exit_light_sleep(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_light_sleep_wakeup(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_exit_light_sleep_wakeup(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_offline_wakeup(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_offline_record(stm_state_machine_t *sm,stm_state_t _state,void *payload);
void sleepmgr_enter_offline_sleep_wait(stm_state_machine_t *sm,stm_state_t _state,void *payload);


/* Transition function prototypes */
stm_state_t sleepmgr_inactive_enable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_inactive_disable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_enable_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_ignore(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_online_enable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_online_disable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_abort(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_enable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_setup_disable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_remember_abort(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_ll1_sleep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_rf_sleep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_sleep_enable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_sleep_disable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_sleep_process_offline_enable_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_abort_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_wakeup(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_rf_less_wakeup(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_wmgr_result(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_wmgr_timer_expiry_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_trm_grant_cb_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_set_enable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_set_disable_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_pullin_wakeup_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_rf_wakeup_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_rf_enter_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_wakeup_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_stmr_on(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_ll1_wakeup_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_rxlm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_dls_exit_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_online_sclk_err(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_wait_sleep_enable(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_wait_sleep_disable(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_wait_abort(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_remember_light_sleep_wakeup_cb(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_rf_sleep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sleep_demmodules_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_dlm_cfg_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_process_delayed_config_app_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_sleep_enable(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_sleep_disable(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_abort(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_wakeup_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_wakeup_rx_cfg_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_rf_wakeup_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_process_enable_fw_req(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_light_sleep_obj_end_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_wakeup_demmodules_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_rf_wakeup_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_rf_enter_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_stmr_on(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_ll1_wakeup_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_remember_sample_rec_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_rxlm_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_sample_rec_done_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_goto_sleep_ind(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_obj_start(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_rf_sleep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_rf_exit_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_ll1_sleep_cnf(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_go_to_sleep(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_process_offline_sclk_err(stm_state_machine_t *sm, void *payload);
stm_state_t sleepmgr_try_mode_change(stm_state_machine_t *sm, void *payload);


/* State enumeration */
enum
{
  INACTIVE,
  ONLINE,
  ONLINE_SLEEP_WAIT,
  SLEEP,
  ONLINE_WAKEUP,
  TTL_WAIT,
  LIGHT_SLEEP_WAIT,
  LIGHT_SLEEP,
  LIGHT_SLEEP_WAKEUP,
  OFFLINE_WAKEUP,
  OFFLINE_RECORD,
  OFFLINE_SLEEP_WAIT,
};

#endif /* !STM_DATA_STRUCTURES_ONLY */

/* End machine generated internal header for state machine array: LTE_ML1_SLEEPMGR_STM[] */


#ifdef __cplusplus
} /* extern "C" {...} */
#endif

#endif /* ! __LTE_ML1_SLEEPMGR_STM_INT_H */
